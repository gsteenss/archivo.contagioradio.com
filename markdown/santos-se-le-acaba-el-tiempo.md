Title: Santos, se le acaba el tiempo
Date: 2016-11-08 16:10
Category: Camilo, Opinion
Tags: Juan Maniel Santos, paz
Slug: santos-se-le-acaba-el-tiempo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: somoslarevista 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### 8 Nov 2016 

Todas las desgracias de los hombres provienen de no hablar claro, escribió, Albert Camus, en La Peste. Esa es nuestra desgracia presente, del presidente que quiere pasar a la historia como el que logró el acuerdo con las guerrillas de las FARC EP y el ELN

Santos no ha hablado claro ni durante cuatro años del proceso con las FARC EP ni del proceso con el ELN, ni en la crisis después del No en el plebiscito, su ambigüedad lingüística es además temerosa, ante un gigante de la demagogia, el cinismo y el arte de hacer creíble la mentira.

Se acaba el tiempo. Aunque la Guerra de 70 años haya podido tener un final sensatamente feliz con lo acordado en La Habana; estamos aún ya distantes de que ese diseño se  implemente, y falta aún resolver la mesa de Quito y sus acuerdos para cimentar las bases de un cambio dentro de lo instituido

El tiempo electoral de 2018 apremia como una sombra y un fantasma, los sucesores de Santos, ya rasgan y arañan. El pacto de élites es inviable,  si y solo sí, Santos se la juega por los acuerdos de la isla y de una sólidas bases de cumplimiento de la palabra para llegar en acuerdos con el ELN. La firma con Santos parece que es lo único posible en el corto plazo para asegurar un proceso transicional de la política en armas, a la política sin armas, con el mantenimiento de las movidas en la calle y la participación en la sociedad en la construcción de otra democracia.

Lo mejor de la ganancia del No en el plebiscito ha sido la expresión espontánea juvenil en las calles, \#PazAlaCalle, su actitud abierta y creativa a proteger la vida en la implementación de los Acuerdos Esa nueva expresión está interactuando de otro modo la relación ciudad campo rural de la guerra, de la manera como esto se acompañe políticamente se puede abrir un escenario renovado para una nueva construcción de poder.

 Ese No, permitió develar los egos de Santos y Uribe. Bueno y también, la de los muchos egos de la izquierda plegados indignadamente a Santos. Santos adobado de una vanidad sin límites fue el responsable del fracaso. Porque mintió como lo hizo Uribe. Santos como Uribe desconectan la paz de las reivindicaciones sociales que exigen profundas transformaciones, que no son posibles en ellos, porque son neoliberales. Tanto Santos como Uribe usaron la mentira, y en ambos casos, los ponchados o los de poner eran las FARC.

 Es muy difícil que la gente comprenda la diferencia entre un Acuerdo de Solución del Conflicto armado con las FARC EP y la solución a todos los problemas de déficit de derechos del país, y eso lo aprovechan los del No de Uribe, para entorpecer, para enredar. Es muy difícil para los que promovemos la solución del conflicto armado lograr hacer creíble a la gente que esta salida dialogada es ajena de las angustias diarias del desempleo, de la marginalidad, y que con el tiempo será rentable en su cotidianidad, mucho más cuando hay una reforma tributaria contra derecho.

 Santos nunca realizó una pedagogía de la solución del conflicto armado, ni de los acuerdos que iban definiendo en la Mesa de La Habana. Siempre mantuvo el secretismo y una visión de castigo para quienes pudieran contribuir en el proceso si no contaban con su visto bueno para llegar a La Habana. Santos asoció el proceso con las FARC EP a su persona, usando tácticamente el discurso de los derechos de las víctimas y la retórica de que las FARC habían cumplido, mientras los iba deslegitimando. Tal cual, cómo sucedió en Cartagena en la firma del Acuerdo Final. Todo allí fue un montaje de aúlicos, de reverencias y comisión de aplausos para el gobierno, dejando de lado a su adversario, con quites de poca cortesía, con negaciones de fondo  y con el mensaje evidente de la falsa victoria militar con el sobrevuelo de esos pájaros metálicos   a baja altura cuando Timoleón Jiménez estaba en su discurso.

Santos sigue cometiendo los mismos errores. Sus voceros en la mesa públicamente guardan silencio ante las contradicciones con el uribismo, le tienen miedo, y dejan en el ambiente que todo depende de las FARC EP, es con esos radicales en los que descansara mentirosamente la responsabilidad del desconocimiento de las apreciaciones de los del No.

 Santos se arriesgó a un plebiscito que era innecesario. El fracaso del mismo solo puede ser enmendado por él, y no depende ni de la actitud de la guerrilla ni de la sociedad que ha salido a la calle. Todo depende de él, de su decisión o no de enfrentar, luego de la simulación del diálogo con Uribe, a los del uribismo, que no quieren respetar el Acuerdo de fondo con las FARC EP.

Si Santos se arriesga y asume que el Congreso de la República, donde cuenta aún  con las mayorías, que pueden esfumarse con la Reforma Tributaria, se abre el mecanismo expedito para que el presidente con sus facultades extraordinarias implemento los Acuerdos con las concesiones al No. De paso, la posibilidad que lo acordado tenga legitimidad se realizaría en el próximo gobierno que sería de transición.

Esa transición supone, sensatamente, reconocer, que la mayoría es indiferente y apática; que los votantes del Sí no todos son progresistas y heredan o comparten visiones uribistas; que todos los que votaron por él No, no  son uribistas; que no todos los demócratas y autoproclamados revolucionarios votaron; y que lo que pueda nacer de nuevo supone un agenciamiento colectivo renovado y abierto a alianzas con lo más liberal del establecimiento para el cumplimiento básico de lo acordado en ambas mesas, si logra arrancar el proceso con el ELN. Es un pacto político que puede sonar mendicante, pero claramente enfrentando a lo sinuosamente articulado entre Uribe, Vargas Lleras y el propio Santos.

 Y esto significa, para el otro sector político, que no es del establecimiento,  enfrentar los obstáculos propios de lo que se llama alternativo o de izquierda que se expresa en lo variopinto del llamado movimientos social tradicional y de los nuevos movimientos sociales: En otras palabras, una ruptura con los paradigmas de antaño que han generado fragmentaciones, sectarismos y caudillismos yoicos.

Santos al otro día de su perdido plebiscito, podría haber colocado las líneas rojas de la conversación con los del No, basado en lo que la Corte Constitucional expresó,  debió haber abierto el cabildo popular, y alinderar a su bancada para dar trámite al mecanismo legislativo expedito para que con las facultades extraordinarias proceder al estudio y promulgación, entre otras de la ley de amnistía e indulto, y a aspectos del Sistema Integral de Verdad y de Justicia.

 Santos se enredó y nos enredó a todos. Santos debe ser claro y dejar ya el carameleo con el No. Santos por su Nobel, su propia vanidad, debe ser alguna vez coherente, ante tanto incumpliendo con las víctimas, con la Cumbre Agraria, con los incumplimientos a las FARC EP y al ELN en las fases exploratorias, debido entre otras, a su esquemático y complejo Alto Comisionado.

 Santos nos está condenando al fracaso, hay poco tiempo, corre el tiempo, hoy debe asumir el riesgo, en medio de su baja popularidad de enfrentar ese No del uribismo para dar paso a solucionar uno de los conflictos políticos armados; resolviendo un nuevo acuerdo entre su gobierno y las FARC EP; y debe iniciar sin más, la mesa con el ELN. Y ojalá esta mesa acordando casi como punto de partida un cese bilateral de fuegos urgente, perentorio y necesario. Este sería por fin un mensaje coherente con lo ya avanzado, una palabra clara de Santos. Santos tiene la posibilidad de llegar a Oslo el próximo 10 de diciembre, arreglado lo que él mismo torció con una palabra clara ante la imagología de terror que ha ido inoculando el uribismo, del que Santos fue parte y del que a veces se desprende.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
