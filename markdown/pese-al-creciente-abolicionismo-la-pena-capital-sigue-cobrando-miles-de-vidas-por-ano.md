Title: Pese al creciente abolicionismo, la pena capital sigue cobrando miles de vidas por año
Date: 2016-10-10 14:08
Category: DDHH, El mundo
Tags: abolición, día internacional, Pena capital, Pena de muerte
Slug: pese-al-creciente-abolicionismo-la-pena-capital-sigue-cobrando-miles-de-vidas-por-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pena-de-muerte.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Doral News Online 

##### 10 Oct 2016 

El 10 de octubre se conmemora el **Día internacional contra la pena de muerte**; fecha en la que diferentes movimientos y organizaciones defensoras de Derechos Humanos, entre las que se encuentra Amnistía Internacional, **luchan por la abolición completa de la pena capital en el mundo**.

La pena de muerte constituye **una seria vulneración y una de las formas de negación más extremas de los derechos fundamentales a la vida y a no ser torturado**, ambos proclamados en la declaración universal de los Derechos Humanos. Las falencias en torno a la supuesta capacidad disuasiva del crimen, su aplicación de forma discriminatoria, y el gran margen de error, señalados por las organizaciones ponen en evidencia lo dañino y vano de su aplicación.

Según informe presentado por Amnistía Internacional, **en 2015,** el total de países donde la pena capital habia sido abolida llegaba a** 102,** incluídos Colombia, Venezuela, Francia, España, Australia y Filipinas; mientras que en **58**, entre los que se cuentan Estados Unidos, China, Arabia Saudí e Irán, **seguian siendo retencionistas**, es decir persistían las ejecuciones legales; **32 mantenían la pena capital**, pero en los últimos 10 años no la habían aplicado y **6 la conservaban sólo para crímenes considerados graves** (asesinatos o durante periodos de guerra), entre ellos Brasil, Perú, Chile, Kazakhstan entre otros.

Si se tienen en cuenta las **cifras de 1997** en las que únicamente **9 países habían abolido la pena de muerte**, **2.375 personas habían sido ejecutadas en 40 paíse**s, y **3.707 se encontraban en proceso de ser condenadas a la pena capital,** se esperaba que para 2015 las estadísticas evidenciaran un avance significativo en cuanto al compromiso de los diferentes Estados por eliminar la pena de muerte como forma de castigo.

Sin embargo, **en países como Irán, Arabia Saudí y Pakistán** se registró un mayor número de personas a las que les fue aplicada la pena capital, **representando el 89% del total de casos**. Sólo en ese año al menos **1.634 sentenciados fueron ejecutados**, cifra que significó **un aumento de más del 50% respecto al año 2014**, además de ser **una de las más altas registradas desde 1989**, según lo señalado en el informe.

2015 finalizó con un total de **1.998 personas condenadas, 1.634 ejecutadas y cerca de 20.292 en el llamado corredor de la muerte**. La situación resulta aún más crítica si se tiene en cuenta que Irán ejecuta personas cuyo delito fue presuntamente cometido aun siendo menores de edad y  en algunos paises se aplica la pena a personas con algún tipo de discapacidad.

Aunque es significativo el incremento de las naciones que han abolido la pena capital como forma de justicia, el no reconocimiento por parte de algunos países a los tratados internacionales que tienen como fin la eliminación de dicha pena capital, hacen que estos sigan incurriendo  en serias violaciones a los derechos de sus ciudadanos. **Los  tratados internacionales  trabajan sobre cuatro protocolos fundamentales**:

-El Segundo Protocolo Facultativo del Pacto Internacional de los Derechos Civiles y políticos adoptado por la Asamblea General de las Naciones Unidas en 1989: abolición total de la pena de muerte, excepto en tiempos de guerra.

-Protocolo de la Convención Americana sobre Derechos Humanos, adoptada en 1990 por la Asamblea General de la Organización de los Estados Americano: abolición total de la pena de muerte, excepto en tiempos de guerra.

-Protocolo número 6 del Convenio Europeo para la Protección de los Derechos Humanos y las Libertades Fundamentales, adoptado por el Consejo de Europa en 1982: abolición total de la pena de muerte en tiempos de paz, manteniéndose para los actos cometidos en tiempo de guerra o de peligro inminente.

-El Protocolo número 13 del Convenio Europeo de Derechos Humanos, adoptada en 2002 por el Consejo de Europa: abolición de la pena de muerte en cualquier circunstancia incluidos los actos cometidos en tiempos de guerra.
