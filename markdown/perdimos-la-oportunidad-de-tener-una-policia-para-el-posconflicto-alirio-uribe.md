Title: "Perdimos la oportunidad de tener una policía para el posconflicto", Alirio Uribe
Date: 2016-06-21 11:52
Category: Nacional
Tags: código de policía, paz
Slug: perdimos-la-oportunidad-de-tener-una-policia-para-el-posconflicto-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: A la luz pública 

###### [21 Jun 2016] 

Se aprobó la conciliación del Código de Policía que quedó **con 156 artículos con la redacción de cámara de los 204**. El código aprobado prioriza las multas sobre la educación ciudadana y da facultades extraordinarias a la policía que bajo su criterio podrá actuar en casos que antes eran excepcionales, así lo denuncian congresistas principalmente del Polo Democrático y la Alianza Verde.

Alirio Uribe, representante a la Cámara, asegura que si bien se ha dicho que el anterior Código de Policía  era un decreto de Estado de sitio, lo cierto es que el nuevo estaría inscrito en la misma lógica y en cambio se habría perdido la oportunidad de generar un debate sobre la policía que necesita el país frente a un escenario de posconflicto.

Es por ello, que varios congresistas han anunciado que **una vez el código aprobado sea sancionado por el presidente Juan Manuel Santos,** será demandado, aunque no en su conjunto pues se resalta que existen algunas medidas buenas que si aportan a la convivencia y seguridad ciudadana, según explica el congresista Uribe Muñoz.

En total el Código consta de 243 artículos, de los cuales 230 quedaron como venían de cámara de representantes. Durante el trámite, s**e presentaron más de 400 proposiciones desde las diferentes bancadas.** “Tratamos de matar un poco de micos y mejorarlo lo más posible, sin embargo 173 artículos del Código fueron votados en bloque y a pupitrazo.

Temas sobre la convivencia, la seguridad, salubridad, espacio público y la movilización son los que se encuentran en el contenido de este nuevo código.  Entre los que más preocupa a algunos congresistas y organizaciones sociales por l**as afectaciones a las libertades de los ciudadanos y a sus derechos** son las siguientes:

### Detenciones 

“La Policía ahora tendrá facultades para detener personas por cualquier cosa, niños, adolescentes, borrachos, habitantes de la calle, etc.”, señala el congresista Alirio Uribe.

### Allanamientos 

Los policías podrán ingresar al hogar de una persona por cualquier tipo de situación, pues se estipularon 14 causales para que así sea, pese a que, según el congresista del Polo Democrático, solo debía haberse aceptado una, y es cuando la vida de la persona esté en peligro. “En ultimas cualquier persona puede ser objeto de allanamiento de la policía, vamos a tener policías reales y falsos”, dice el congresista Uribe Muñoz.

### Protesta social 

El tercer componente que preocupa al representante, tiene que ver con el derecho a la protesta social, pues el código sería “la carta blanca para reprimir la movilización”, pues habrá bastantes restricciones para esta y la represión de la policía y el Ejército sería aún mayor.

### Espacio público 

Las más afectadas serían las personas que viven de las ventas informales o los espectáculos callejeros. Es así como se estaría vulnerando el derecho al trabajo.

### Multas 

De acuerdo con el representante Alirio Uribe, es posible que el uso excesivo de multas, como lo plantea este nuevo código, aumente la corrupción al interior de la policía. Y finalmente todo esto “no va a contribuir a una acercamiento de la sociedad con las autoridades”.

<iframe id="audio_11978541" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11978541_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Código de Policía, Texto Conciliado](https://es.scribd.com/doc/316354547/Codigo-de-Policia-Texto-Conciliado "View Código de Policía, Texto Conciliado on Scribd")

<iframe id="doc_7278" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/316354547/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
