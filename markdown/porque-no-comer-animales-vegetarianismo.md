Title: ¿Por qué no como animales?
Date: 2016-01-12 08:25
Author: ContagioRadio
Category: Dragonfly, invitado, Opinion
Tags: Animales, Consumo de carne, vegetarianismo
Slug: porque-no-comer-animales-vegetarianismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/nino-vegetariano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: vive sano] 

#### **[Dragonfly](http://bit.ly/1QkHTjT) **- **[@[MeVle5]

###### 12 Ene 2016 

[Una pregunta tan frecuente cuando quien la hace quizá no haya salido de la zona de confort que causa un hábito de consumo incuestionado e inconsciente.]

[Desde mi niñez siempre he disfrutado de la compañía de animales, bien sea perros, gatos o aves, porque para entonces eran las “mascotas” de la casa, incluso existía empatía con las gallinas, las vacas, los cerdos, también los peces, sin embargo a la hora de comer, no había discriminación moral de porqué unos si y otros no eran servidos en el plato, era normal para entonces y lo que luego entendería  que se denominaba especismo, no se veía mal en trozos o filetes bien cortados, empacados y comprados en supermercado, no era nada diferente a una fruta o una verdura, al final solo era comida y yo no la preparaba.]

[Ya en la universidad aún sin saber nada sobre vegetarianismo(1) y sin haber considerado siquiera si era ético el admirar los animales vivos pero a la vez poder verlos como alimento y consumirlos sin remordimiento, solo faltaba un detonante que todo animalista tarde o temprano encuentra para hacer ese juicio moral y un examen de conciencia, en mi caso fue encontrar casualmente un programa de televisión sobre el sacrificio de animales, supuse que la curiosidad de verlo pondría a prueba una creencia, de que tanto me conmovían los animales realmente, subconscientemente tal vez necesitaba saberlo porque esa tesis no me la había planteado antes.]

[Fueron tan impactantes las imágenes que no terminé de ver el programa, así llegaría esa consideración ética y moral que habría de sacudirme, luego no fue difícil apartar de tajo el sufrimiento animal de mi plato y que en adelante optaría por ser vegetariano. Lo complicado fue tratar de explicar a las personas que me conocían, familiares y amigos, que mi decisión no era ni una necedad ni tampoco un capricho pasajero, que entendieran ese cambio trascendental para mí, nada fácil cuando debía compartir con ellos en la mesa desde almuerzos y en reuniones hasta asados, las burlas no faltaban ya que en algunas ocasiones se me viera extraño porque no pensaba como los demás, porque habían muchos prejuicios y desconocimiento de las razones que motivaron mi decisión.]

[Sin duda para mí era un cambio tan coherente como inexcusable entre lo que pensaba, decía y hacía, era consciente que no podría cambiar el mundo pero cambiar mi hábito ya era un gran logro, pensar que cada día aportaba algo para que no muriera un animal por mi causa ya era reconfortante. Con el pasar del tiempo comprobé que si se podía vivir sin ser partícipe de ese holocausto animal, sin causar sufrimiento y muerte de otro ser sintiente, simplemente por compasión adoptando una decisión individual, y aún sin pretender llegar a ser un activista, promovía mi opción entre las personas más cercanas empleando los mismos argumentos.]

[Con ello vendría el interés de saber más del vegetarianismo como filosofía, un modo de vida revolucionario dentro de una sociedad antropocentrista y especista, así me plantearía nuevos interrogantes y vendrían otras decisiones en consecuencia.]

[Dentro de esos cuestionamientos estaba el cómo justificar que no eran los humanos amos y señores de los animales como para hacer con ellos lo que se antojara como simples objetos de uso o bienes de consumo, que no existían solamente para la complacencia humana, aunque pareciera lo obvio que dañarlos era una regla general hecha costumbre y era hasta absurdo pensarlo diferente.]

[Las razones serían que los animales, entre ellos los humanos, no existen para ser asesinados, y como seres sintientes que son, no querrían sufrir ni tampoco ser atormentados, así por conmiseración no habrían de tratarse como cosas y no habría excusa válida para servirse de ellos ni como alimento, ni como vestuario, ni divertimento, si existen opciones y que siendo el humano dotado de inteligencia, no pueda encontrar suplencias que siempre han estado ahí, pero por costumbre facilista no se hayan querido elegir.]

[Ya luego habrían otras razones que afianzarían aún más esta convicción como sería por salud, el impacto ambiental y hasta el factor económico, todo sumaría.]

[Encontraría también argumentos más de peso como era que para ser más humano necesariamente se debía ser más animal, porque solo siendo animal se entendería que solo el humano en su vanidad y egoísmo, come sin tener hambre, daña con intención a quien puede ser dañado, mata sin estar en peligro y por complacencia, agrede por gusto y hace sufrir por sadismo entre muchas otras cosas que solo son inherentes al humano, quizá podría haber llegado un punto en que se denominara misantropía, pero no en su definición absoluta, porque lejos de ello la misantropía no equivale necesariamente a una actitud inhumana hacia la humanidad o al desprecio del humano por el simple hecho de serlo.]

[Es más, frente a esto Arthur Schopenhauer quien era considerado un misántropo concluía:]*[«El trato ético hacia los otros era la mejor actitud, pues todos somos sufridores y parte de la misma voluntad de vivir.»]*[, Schopenhauer era defensor de lo que consideraba el derecho de los animales y en esa misma comprensión de la actitud inhumana dijo:]

**[«La compasión hacia los animales está tan estrechamente ligada a la bondad de carácter que se puede afirmar con seguridad que quien es cruel con los animales no puede ser una buena persona.»,]**

*[«Una compasión sin límites por todos los seres vivos es la prueba más firme y segura de la buena conducta moral.»]*

*[«El hombre ha hecho de la Tierra un infierno para los animales.»]*

**Citaba Schopenhauer así también:**

**[«Ni el mundo es un artilugio para nuestro uso ni los animales son un producto de fábrica para nuestra utilidad.»]**

*[«El hombre no debe compasión a los animales, sino justicia.»]*

[Schopenhauer legaría su reflexión en otros, algunos a quienes se les catalogaría también como misántropos, entre ellos: F. Nietzsche, S. Freud, T. Mann, L. Wittgenstein, E. von Hartmann, H. Vaihinger, M. Proust, H, Bergson, É. Cioran, J. L. Borges, etc.]

[Corresponde así que la destrucción paulatina del planeta y la situación actual del cambio climático se debe indiscutiblemente al mayor depredador que existe, el humano, y no obedece de ninguna manera a la actividad de los animales no humanos, que en las guerras humanas (entendiendo por guerra todo conflicto no sólo el bélico) los animales siempre son víctimas invisibilizadas, que la humanidad vendría siendo tan insignificante que el planeta se salvaría si se extinguieran los humanos pero que la destrucción total vendría si acabaran los animales no humanos incluso los insectos como las abejas o las hormigas, la humanidad es animal-dependiente. Jonas Salk (1.914-1.995 - Prestigioso virólogo estadounidense, inventor de la vacuna de poliomelitis), explicaba que:]*[“Si desaparecieran todos los insectos de la tierra, en menos de 50 años desaparecería toda la vida. Si todos los seres humanos desaparecieran de la tierra, en menos de 50 años todas las formas de vida florecerían.”]*[.]

Son tantas las explicaciones del lado de quienes comen carne para invalidar a quienes decidimos no hacerlo, para mencionar unas pocas, como que los animales se comen a otros, pero desconocen que el 75% de los animales en este planeta son herbívoros, que esos animales tienen más características morfológicas coincidentes con los humanos como el movimiento de la mandíbula que difiere totalmente con los carnívoros; o que comer carne es indispensable para vivir, pero no es así, la prueba es que cada día hay más humanos vegetarianos y veganos en el mundo, con hábitos más saludables ajenos a muchas enfermedades como el cáncer, como ya lo ha recomendado la OMS en octubre de 2015 (2). (Marly Winckler, coordinadora de la IVU(3) en América Latina y el Caribe dice que Hay una tendencia mundial al crecimiento del vegetarianismo. Hay más de 600.000 millones (y en aumento) de vegetarianos en el mundo. Si la mitad de la población del planeta se hace vegetariana, eso ya sería una noticia fantástica. Si el 80% de la población del mundo se hace vegetariana, el mundo se volvería en un paraíso en 60 días. La India es el país con mayor población vegetariana, un 40%. Según un informe de la Fundación Foodways, en Estados Unidos actualmente es el 13% de la población. En Inglaterra, el 47% de la población se autodenomina vegetariana. El 28% de brasileños “están procurando comer menos carne”).

\[caption id="attachment\_19038" align="alignright" width="288"\][![cantidad de agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/cantidad-de-agua-.png){.wp-image-19038 width="288" height="219"}](https://archivo.contagioradio.com/porque-no-comer-animales-vegetarianismo/cantidad-de-agua/) Agua necesaria para la producción de alimentos\[/caption\]

[El tema ambiental ha cobrado mucha fuerza en recientes años, hoy es indiscutible que se deban cambiar muchas actividades antes impensables como restringir la producción ganadera por el excesivo uso del agua y los efectos en el cambio climático (la ganadería contribuye con CO2, Metano, Oxido nitroso y Amoníaco), cambiar los hábitos alimenticios en especial reducir tajantemente el consumo de productos de origen animal ha sido recomendación de la FAO(4/5).]

\[caption id="attachment\_19037" align="alignleft" width="324"\][![emisones de metano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/emisones-de-metano.png){.wp-image-19037 width="324" height="198"}](https://archivo.contagioradio.com/porque-no-comer-animales-vegetarianismo/emisones-de-metano/) Emisiones anuales de metano por cada animal\[/caption\]

[Las emisiones contaminantes de la ganadería son equivalentes a las emitidas por todos los autos del mundo; pues contribuye con 15% de los gases de efecto invernadero(6/7).]

[Llegar a ser vegetariano o mejor aún vegano, es una decisión individual que solo puede ser asumible desde la interiorización y la convicción, desde la conciencia en la huella que se deja en este mundo, que no pretende juzgar a quien no lo es pero que se puede compartir, explicar y justificar para sembrar en otros una semilla que probablemente germine aunque en muchos casos se marchite y en otros se siembre en terreno árido.]

[La paz también es con los animales y con el planeta, por eso es que no como animales.]

###### [(1) En este grupo se puede incluir a Vegetarianos, ovo-lacto vegetarianos, lacto vegetarianos, veganos. Antes de 1847, las personas que no comían carne eran conocidos como “Pitagóreos” o simpatizantes del “Sistema Pitagóreo”, por el vegetariano de la Antigua Grecia, Pitágoras.] 

###### [(2) La OMS declara cancerígena la carne procesada <http://elpais.com/elpais/2015/10/26/ciencia/1445860172_826634.html>] 

###### [3 IVU es una creciente red global de organizaciones independientes que promueven veg'ism en todo el mundo. Datos de 2014.] 

###### [[(4) UN urges global move to meat and dairy-free diet][[http://gu.com/p/2hdtj/stw]](http://gu.com/p/2hdtj/stw)] 

###### [[(5) La ONU señala en un informe que una alimentación vegana salvaría al mundo del hambre][[http://www.cuentamealgobueno.com/2014/01/la-onu-insta-con-urgencia-a-llevar-una-alimentacion-vegana/]](http://www.cuentamealgobueno.com/2014/01/la-onu-insta-con-urgencia-a-llevar-una-alimentacion-vegana/)] 

###### [[(6) La larga sombre del ganado. Problemas ambientales y opciones. <ftp://ftp.fao.org/docrep/fao/011/a0701s/a0701s00.pdf> ]] 

###### [[(7) Reducir el consumo de carne, una de las claves en la lucha contra el cambio climático <http://www.animalpolitico.com/2015/12/reducir-el-consumo-de-carne-una-de-las-claves-en-la-lucha-contra-el-cambio-climatico/>  ]] 

###### 

 
