Title: La alimentación en las cárceles otra forma de torturar a la población reclusa
Date: 2018-09-25 16:54
Author: AdminContagio
Category: Expreso Libertad
Tags: alimentación, carceles, prisioneros políticos
Slug: alimentacion-carceles-torturar-poblacion-reclusa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/IMAGEN-16850053-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del pueblo 

###### 25 Sep 2018 

"**En las cárceles de Colombia hay prisioneros de primera, segunda, tercera y cuarta categoría**" esa es la afirmación de Julio Marquetalia, ex prisionero político de FARC, quien relató en los micrófonos del Expreso Libertad las múltiples violaciones a los derechos humanos de la población carcelaria frente a su alimentación.

**Comida en mal estado, con heces fecales de rata y enfermedades gástricas** son las situaciones más alarmantes que denunció Marquetalia en esta emisión del Expreso Libertad.

<iframe id="audio_30634198" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30634198_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
