Title: ESMAD y militares adelantan desalojo de 15 familias víctimas de Montes de María
Date: 2016-03-02 12:52
Category: DDHH, Nacional
Tags: Desalojos, Desplazamiento forzado, montes de maría, Sucre
Slug: 15-familias-victimas-del-conflicto-son-desalojadas-en-montes-de-maria-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Web Sur 

<iframe src="http://co.ivoox.com/es/player_ek_10646129_2_1.html?data=kpWjlpuVdpqhhpywj5aUaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncabHrqaxjd6PscrgytnO1MrXb8LYxtHO0NnFsozYxtjOztTOs4zYxpCel5DKpc7dzc7O1ZDaaaSnhqaxj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ingrid Vergara, MOVICE] 

###### [2 Mar 2016] 

Este miércoles se llevó a cabo una diligencia judicial de desalojo de **15 familias víctimas del conflicto armado** que viven en la finca Nueva Esperanza ubicada en el corregimiento del Yeso del municipio de Morroa, en Sucre.  Se trata de un predio en el que **habitan hace más de 25 años familias víctimas de desplazamiento, asesinatos y desapariciones** en Montes de María por el accionar de los grupos armados ilegales.

“Son 15  familias que se quedan sin nada”, describe Ingrid Vergara, secretaria del MOVICE Capítulo Sucre, quien añade que la  diligencia es custodiada por ESMAD y fuerzas militares, incluidas ambulancias, “Están prevenidos para cualquier cosa”, expresa.

De acuerdo con el Movimiento de Víctimas de Crímenes de Estado, a los 10 años de estas familias poseer el bien solicitan en numerosas cartas a la INCODER que inicie el proceso de sucesión, una solicitud que nunca fue atendida por esta institución. Tiempo después **Eder Navas, quien llegó de La Guajira y se presentó en el predio  como el dueño de inmueble,** habría iniciado el proceso de embargo con el que finalmente se ordenó desde el Juzgado Primero Promiscuo de Corozal el desalojo de las familias.

Ingrid Vergara, secretaria del MOVICE Capítulo Sucre, “era una propiedad privada que intentó ser negociada por el INCODER sin embargo nunca se logró y finalmente los campesinos desplazados lograron un acuerdo para que las familias pudieran protegerse en ese predio”. Durante esos años, **los campesinos han sido blanco de amenazas, “les quemaron las viviendas y hubo bastantes denuncias”**, dice la integrante del MOVICE, agregando que se trata de una zona con presencia paramilitar.

Incluso, hace pocas horas, **se conoció del homicidio de Nando Pérez, sujeto de restitución de tierras** quien fue asesinado por dos hombres propinándole tres disparos que acabaron con su vida. Así mismo, otros líderes políticos y de la comunidad han sido amenazados mientras intentaban recuperar sus tierras.

Vergara también denuncia que pese a las denuncias y acciones legales que han interpuesto las familias, es muy difícil que estos prosperen ya que **“la mayoría de los jueces y fiscales son ganaderos y terratenientes”**, que tienen intereses sobre las tierras en ese departamento.

Desde el MOVICE y la comunidad se exigen garantías teniendo en cuenta las resoluciones de Naciones Unidas y las sentencias de la Corte Suprema de Justicia para proteger los derechos de los niños, niñas, mujeres y personas de la tercera edad quienes esta noche no tendrán donde dormir.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
