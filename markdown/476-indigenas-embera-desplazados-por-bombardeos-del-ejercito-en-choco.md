Title: 476 indígenas Embera desplazados por bombardeos del Ejército en Chocó
Date: 2016-05-11 14:14
Category: DDHH, Nacional
Tags: bombardeos ejército, Chocó, indígenas chocó
Slug: 476-indigenas-embera-desplazados-por-bombardeos-del-ejercito-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Embera-desplazados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Bernardino Dura  ] 

###### [11 Mayo 2016 ]

[Un mes de desplazamiento completan 476 indígenas Embera Wounaan, del resguardo Pichimá, en el Litoral del San Juan, Chocó, **por cuenta de los continuos bombardeos del Ejército que les han impedido contar con garantías para estar en sus territorios**. Las 94 familias denuncian que los ]albergues del casco urbano del municipio son insuficientes, para atender la proliferación de epidemias originadas por el consumo de agua contaminada.

Los bombardeos iniciaron el domingo 10 de abril sobre las seis de la tarde, en el resguardo 'Playa Linda' que limita con 'Pichimá', lo que afectó a la población indígena que, en medio del pánico, **decidió trasladarse a la cabecera del municipio para salvaguardar sus vidas**, según denuncia el líder Bernardino Dura.

Del Ejército las comunidades no han recibido ningún pronunciamiento, según se conoce los bombardeos son contra campamentos del ELN. Hace una semana, algunos pobladores junto con autoridades municipales, regresaron a los resguardos para verificar la situación y **evidenciaron que los bombardeos aún continúan**. Por lo que exigen al Gobierno nacional que ordene el cese de los ataques militares y brinde las garantías para retornar.

Las comunidades indígenas demandan de las autoridades municipales, departamentales y nacionales, acciones concretas que permitan mejorar sus actuales condiciones en términos de seguridad, salud y educación, pues se sienten **acorraladas por la fuerza pública y el hacinamiento en el único albergue es apremiante. **

<iframe src="http://co.ivoox.com/es/player_ej_11495732_2_1.html?data=kpahm5qbd5Ohhpywj5eVaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaPZ09PO1MnNstCfpdrfw5CRb62ZpJiSo6nIqdOfqtPRh6iXaaK4yMrbw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
