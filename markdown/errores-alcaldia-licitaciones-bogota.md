Title: Siguen los errores de la Alcaldía en licitaciones de obras para Bogotá
Date: 2019-07-24 18:39
Author: CtgAdm
Category: Judicial, Política
Tags: Coalición de izquierda, Manuel Sarmiento, Metro de Bogotá, Transmilenio por la Séptima
Slug: errores-alcaldia-licitaciones-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcaldía-de-Peñalosa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @EnriquePenalosa  
] 

Dos recientes decisiones sobre proyectos importantes para Bogotá ponen de nuevo en debate la forma en que la alcaldía de Enrique Peñalosa adelanta las licitaciones de las obras. En primer lugar, la Procuraduría General de la Nación decidió destituir en primera instancia a Juan Pablo Bocarejo, secretario de movilidad, por la contratación del sistema de semaforización; en segundo lugar, las medidas que suspendieron el proyecto de Transmilenio por la carrera Séptima; y tercer lugar, las demandas contra la licitación del proyecto Metro de Bogotá.

### **La semaforización de Bogotá: Otro caso de violación al principio de planeación**

La Procuraduría decidió destituir e inhabilitar en primera instancia a Juan Pablo Bocarejo, y Carolina Pombo Rivera, directora de Asuntos Legales de la Secretaría de Movilidad por "por irregularidades en licitación y adjudicación del contrato para implementación del Sistema de Semaforización Inteligente", según lo que comunicó la Institución. Según el concejal Manuel Sarmiento, este sería "otro caso de violación al principio de planeación", que es evaluar "todos los escenarios posibles" para que el contrato beneficie a la Ciudad y el Proyecto.

Según lo explicó Sarmiento, en el caso de la semaforización hay unos elementos importantes que son los controladores; 535 en total, que aún funcionan y tienen vida útil, pero de acuerdo al Ministerio Público, la Secretaría de Movilidad contrató la compra de nuevos instrumentos con lo que derivaría en un **riesgo patrimonial que asciende a los 13.912 millones de pesos.**

> La [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) destituyó e inhabilitó 10 años el secretario de Movilidad de Bogotá por irregularidades en licitación y adjudicación del contrato para implementación del Sistema de Semaforización Inteligente <https://t.co/EGi1I97ZaL> [pic.twitter.com/VHrVRwssYL](https://t.co/VHrVRwssYL)
>
> — Procuraduría Colombia (@PGN\_COL) [July 24, 2019](https://twitter.com/PGN_COL/status/1153991387987267584?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Aún están vigentes 2 medidas cautelares sobre el proyecto Transmilenio por la Séptima**

El Tribunal Administrativo de Cundinamarca declaró la nulidad de una de tres acciones de tutela contra el proyecto de Transmilenio por la Séptima. Según Sarmiento, en este caso la primera instancia había declarado las medidas cautelares porque el Juez "no encontró la firma que realizó los estudios" para la intervención del tramo de la avenida Circunvalar con calle 85; obra contemplada en el proyecto de Transmilenio. (Le puede interesar: ["Anuncian acciones legales y movilización contra Transmilenio por la Séptima"](https://archivo.contagioradio.com/ciudadanos-marcharan-contra-proyecto-de-transmilenio-por-la-septima/))

Aunque el Concejal dijo que se debía respetar la decisión, aclaró que no significaba dar vía libre al Proyecto porque **aún hay dos acciones de suspensión vigentes.** Por una parte, una acción de la Procuraduría, que se concentró en la licitación del Proyecto, porque encontró que "la forma en que se diseñó la licitación de la troncal no coincide" con las obras de El Pedregal; construcción que se adelanta en la carrera 7ma con calle 100. (Le puede interesar: ["Tribunal suspende intervención de Transmilenio en Parque Nacional"](https://archivo.contagioradio.com/tribunal-suspende-intervencion-de-transmilenio-en-parque-nacional/))

Por otra parte, la suspensión de la licitación que afecta al Parque Nacional, porque el Proyecto no habría contemplado el cuidado de este bien de interés patrimonial. El Cabildante señaló que por estas dos suspensiones, aún está "embolatado el proyecto de Transmilenio por la Séptima", pues "sería una irresponsabilidad contratar los otros tramos ( 6 de 8 en total), y dejar estos tramos suspendidos porque quedarían las obras desconectadas".

### **La demanda en el Consejo de Estado contra el Metro "Alimentador de transmilenio"  
**

Sarmiento fue parte también de la solicitud de medidas cautelares ante el Consejo de Estado para detener la licitación del Metro de Bogotá, que consideró **"un proyecto inconveniente para la Ciudad por el número de pasajeros que va a movilizar (...) y como está estructurado"**. En ese sentido, dijo que el hecho de que el Consejo haya admitido la demanda es un primer paso, pues podría haberlo inadmitido o rechazado.

En la solicitud, los Concejales de la oposición señalan las inconsistencias en la licitación del Proyecto, que habría sido aprobado sin que tuviese los estudios de viabilidad completos, hecho que podría resultar en sobrecostos. Para evitarlo, pidieron dos tipos de medidas cautelares: las de urgencia, que son las que se decretan sin que el juez de la oportunidad al acusado de defenderse; y las ordinarias. (Le puede interesar: ["Frenar el cambio climático en Bogotá no será posible con la nueva flota de Transmilenio"](https://archivo.contagioradio.com/nueva-flota-de-transmi/))

El primer tipo de medidas fueron negadas, pero se están tramitando las ordinarias; "la empresa Metro tiene 5 días para responder a nuestras demandas", explicó Sarmiento. Después de ese tiempo, el Consejo de Estado tendría un mes para responder a la solicitud lo que significaría que podría pronunciarse antes del 27 de octubre; día de las elecciones locales. (Le puede interesar: ["Las cinco modificaciones que tendría incluído el proyecto Metro de Bogotá"](https://archivo.contagioradio.com/modificaciones-proyecto-metro-de-bogota/))

### **Las licitaciones de Peñalosa lo ponen en problemas, pero también a los candidatos a la Alcaldía**

Obras declaradas como importantes para la Alcaldía como el Metro, Transmilenio por la Séptima, el [Embalse San Rafael](https://archivo.contagioradio.com/oposicion-buscara-evitar-construcciones-en-parque-san-rafael/) entre otros, se han visto afectadas por medidas judiciales debido a que sus licitaciones no estaría acorde a lo exigido por la Ley. Misma crítica ha pesado sobre otras licitaciones como el de la nueva flota de buses para Transmilenio. (Le puede interesar: ["A más tardar, la próxima semana habrá acuerdo programático de la izquierda para Bogotá"](https://archivo.contagioradio.com/a-mas-tardar-la-proxima-semana-habra-acuerdo-programatico-de-la-izquierda-para-bogota/))

Pero así como Peñalosa se está viendo afectado hasta el final de su administración por estas licitaciones, los aspirantes a la Alcaldía también han sentido sus repercusiones; en el caso de la coalición que el pasado lunes definió a Claudia López como su candidata única, el Proyecto Metro ha sido objeto de amplios debates. Según Jorge Rojas, todos los integrantes de la coalición están de acuerdo en que el mejor metro para Bogotá debe ser subterráneo, pero hay divergencias sobre lo que puede pasar si el Alcalde deja licitada la obra.

La tensión sobre este tema la terminó aumentando el senador Gustavo Petro, líder de la Colombia Humana, quien el pasado martes afirmó que esta colectividad no tendría cabida en la Coalición mientras el metro elevado sea una opción. Sobre este tema, el concejal Sarmiento marcó su discrepancia con Petro, al señalar que si las obras cuentan ya con licitaciones otorgadas sería difícil que el próximo alcalde, o alcaldesa, termine el contrato; y añadió que en el caso del Metro, sería necesaria una acción judicial similar a la que ya hace curso en el Consejo de Estado.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38940056" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38940056_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
