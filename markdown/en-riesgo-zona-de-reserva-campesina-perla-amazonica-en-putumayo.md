Title: En riesgo Zona de Reserva Campesina Perla Amazónica, en Putumayo
Date: 2020-12-23 16:42
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: agresiones, Denuncias, extractivismo, petroleo, Putumayo
Slug: en-riesgo-zona-de-reserva-campesina-perla-amazonica-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Perla-Amzónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 23 de diciembre **familias víctimas de desplazamiento forzado en Putumayo, denunciaron el riesgo de las comunidades en la Zona de Reserva Campesina Perla Amazónica** ante las actuaciones violentas de frentes armados, unidades militares y la actuación de la empresa petrolera Amerisur.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La denuncia fue recibido en horas de la mañana de este miércoles por la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/grupo-armado-comando-de-la-frontera-ordena-cesar-exigencia-de-derechos-a-empresa-petrolera/), indicando que esta denuncia se hace en medio de la disputa territorial entre las disidencias de las FARC y los Comandos de La Frontera** por las amenazas permanentes, el reclutamiento forzado, las acusaciones falsas y señalamientos. ([Disputa territorial siembra zozobra en las comunidades del Bajo Putumayo](https://archivo.contagioradio.com/disputa-territorial-siembra-zozobra-en-las-comunidades-del-bajo-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una acción que está generando la fragmentación comunitaria además del *"desplazamiento forzoso, la apropiación de tierras, el despojo y la definición de uso de estas"*, **producto del miedo que se apodera de la zona campesina, y en donde recientemente las comunidades han afirmado no retornar al territorio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización defensora de Derechos Humanos, **la serie de actuaciones violentas se desarrolla entre el enfrentamiento del frente Carolina Ramírez de las FARC-EP y la estructura Comandos de La Frontera**, **respaldados por las unidades militares** y su interacción con la empresa petrolera Amérisur.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Intervenciones que durante las últimas dos semanas han llevado a que los campesinos asistan a las reuniones organizadas por Comandos de La Frontera, **en donde dan nuevas órdenes y mensajes a aquellas personas que se opongan a los procesos que adelanta la empresa Amerisur en la Zona de Reserva Campesina**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Comandos de La Frontera vestidos de camuflado y con armas largas expresaron que nadie puede oponerse al proceso sísmico que adelanta la nueva Amerisur dentro de las operaciones del bloque Put. 8 "*
>
> <cite>Comisión de Justicia y Paz </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Dentro de las denuncias de la comunidad, afirman que uno de los integrantes de estos grupos armados les señaló , ***"nosotros ya negociamos directamente con la empresa, y vamos a asegurarle la operación en la zona"***, evidenciando la molestia que genera la figura de la Zona de Reserva Campesina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la Comisión **no es desconocida esta estigmatización, especialmente contra la Asociación de Desarrollo Integral Sostenible Perla Amazónica, organización que representa legalmente la Zona de Reserva Campesina** y junto a ella los liderazgos y la protección del territorio colectivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente reiteraron qué las comunidades de la ZRC, junto con otros procesos organizativos del país **vienen desde el mes de marzo instando de manera urgente que se ejecuta el Acuerdo Humanitario Global que permita el respeto a la vida de las comunidades y los liderazgos**; así como la autonomía de los procesos organizativos y una discusión abierta sobre el uso del suelo. ([Usted puede pasar a la historia como el general de la paz: Víctimas a Rito Alejo del Río](https://archivo.contagioradio.com/general-rito-alejo-del-rio-general-de-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
