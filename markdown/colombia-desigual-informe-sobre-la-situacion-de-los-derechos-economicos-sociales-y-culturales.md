Title: Colombia se raja en Derechos Económicos, Sociales y Culturales ante la ONU
Date: 2017-03-01 17:23
Category: DDHH, Otra Mirada
Tags: DESC, desigualdad, Informe alterno de derechos
Slug: colombia-desigual-informe-sobre-la-situacion-de-los-derechos-economicos-sociales-y-culturales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/DESC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CCJ] 

###### [01 Mar. 2017] 

Diversas organizaciones sociales presentaron este miércoles en Ginebra, Suiza el informe alterno sobre el estado de los derechos económicos, sociales y culturales – DESCA- en Colombia. Ante el Comité de Expertos del Pacto Internacional por los Derechos Económicos Sociales y Culturales de la ONU, las organizaciones sustentaron los **graves retrocesos en materia de garantía a los derechos a la tierra, el aumento de la desigualdad, el trabajo decente, la represión de  la protesta** entre otros derechos en el país.

En el informe, si bien se reconocen los avances al haber logrado un acuerdo de paz con la guerrilla de las FARC y el inicio de diálogos con el ELN, manifiestan que es necesario abordar y superar  las causas estructurales que dieron origen al conflicto armado en Colombia, relacionados con **la falta de garantía de los DESCA de lo contrario “el conflicto armado seguirá reproduciéndose”.**

Para las organizaciones sociales que entregan este informe  la desigualdad persiste en Colombia en donde el 10% de **la población más rica gana 4 veces más que el 40% más pobre. **Le puede interesar: [Defensores de derechos humanos "Contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

### **Derechos económicos** 

Esta desigualdad, reza el informe, también se ve reflejada en la **tenencia de la tierra, en donde un 40.1% del área censada está ocupada por el 0,4% de unidades productivas**, lo que “refleja el incumplimiento de la Recomendación emitida por el Comité DESC de Naciones Unidas, según la cual el Estado colombiano debe adoptar las medidas necesarias para llevar a cabo una auténtica reforma agraria”.

Así mismo, el informe pone de manifiesto que desde el gobierno se han promovido una serie de reformas que, contrario a las recomendaciones, profundizan **la exclusión del acceso a la tierra y el ordenamiento del territorio.**

Por su parte, el desarrollo económico centrado en la explotación de recursos naturales ha multiplicado los conflictos sociales y territoriales lo que no ha permitido el goce de los derechos sociales, culturales y económicos de comunidades indígenas, afro, campesinas y de mujeres en quienes hacen un énfasis especial.

Además de estas afectaciones, el informe dice que el Estado Colombiano **ha desconocido la consulta previa, libre e informada** mediante la no certificación de la presencia de pueblos indígenas en áreas de influencia directa de los proyectos. Le puede interesar: [En riesgo el derecho a la consulta previa](https://archivo.contagioradio.com/proyecto-de-ley-para-reglamentar-consulta-pretende-desconocer-las-comunidades-indigenas/)

### **Derechos Sociales** 

Para los autores y autoras de este informe es importante que ahora que se ha firmado un acuerdo de paz con las FARC, se haga una reducción de los recursos para la guerra – uno de los más altos de la región – y que éstos vayan dirigidos a salud y educación. Puntos incluidos en los acuerdos pactados en La Habana.

De igual modo, seis años después, **el déficit del trabajo decente se mantiene en Colombia** y no se ha cumplido con las recomendaciones entregadas al Estado por la ONU. Según este informe alterno **la vida precaria, la segregación, el desempleo –uno de los más altos en América Latina- sigue siendo parte de las dinámicas sociales en el país.**

Allí, también hacen énfasis en cómo la protesta social continúa siendo una preocupación “en Colombia, el derecho a **la protesta y movilización no cuentan con las garantías necesarias que requiere su libre ejercicio en un Estado democrático**” encontrándose con discursos oficiales que descalifican la protesta social y la criminalizan. Le puede interesar: ["El ESMAD está de más", Día Contra la Brutalidad Policial](https://archivo.contagioradio.com/el-esmad-esta-de-mas-dia-contra-la-brutalidad-policial/)

### **¿Qué es el PIDESC?** 

Es el Pacto Internacional de Derechos Económicos Sociales y Culturales firmado en 1969 y al que se unió Colombia. La realización de esos derechos busca además de la eliminación de la pobreza, las inequidades e injusticia sociales, así como los sufrimientos humanos asociados. De igual modo pretende dignificar la vida de las personas individual y colectivamente.

### **El informe en cifras** 

-   El desempleo continúa siendo unos de los más altos de América Latina , llegando a un 8.9% el desempleo general, 11.8% para las mujeres y 15.8% para los jóvenes, mientras en el desempleo de las mujeres jóvenes es del 20.5%.
-   El 64% de la población (14.1 millones de trabajadores) carece de protección social, en parte porque el 42.4% de los trabajadores trabaja a cuenta propia (10.4 millones de personas) ganan menos del salario mínimo legal estimado en 325 dólares mensuales en 2016.
-   Para los 4´758.000 trabajadores y trabajadoras rurales prima la exclusión de la seguridad social, pues solo el 14,1% cuenta con un seguro de salud y solo el 12,4% está afiliado a un fondo de pensiones. La tasa de informalidad laboral en la zona rural fue de 89,3%, es decir, 4´248.894 ocupados en esta condición.
-   En 2015 se registraron 21 homicidios de trabajadores sindicalizados y 200 violaciones a los derechos a la vida, libertad e integridad de sindicalistas. Es alta la impunidad, en el caso de las amenazas llega al 99.8%.
-   Los delitos de desaparición forzada, desplazamiento forzado, tortura y secuestro, arrojan un porcentaje de impunidad del 91%, 98.1%, 83.3% y 70.8% respectivamente, y en general promedia el 95%.
-   El 42,7% de los hogares tiene inseguridad alimentaria, siendo mayor en las zonas rurales (57,5%) que en las urbanas (38,4%).
-   Según información del Instituto Nacional de Salud, ha sido de aproximadamente 2.000 niños y niñas menores de 5 años fallecidos en la última década.
-   Para el Departamento Nacional de Estadística (DANE) y los gremios de vivienda, de los 13 millones de hogares colombianos hay 3 millones 300 mil viviendas en déficit: 1,3 millones son déficit cuantitativo y los restantes 2,08 millones son déficit cualitativo, es decir, carecen de los estándares básicos de calidad y habitablidad.
-   Colombia tiene una tasa de analfabetismo del 5.8%, es decir, que alrededor de 1.802.637 personas mayores de 15 años en Colombia no saben leer ni escribir.
-   De acuerdo al Censo Nacional Agropecuario, la pobreza multidimensional en el campo equivale al 44.7%, sector en el cual las mujeres representan según el censo, el 36.6% del total de la población rural residente.
-   En el tema ambiental el principal recurso afectado por los conflictos generados ha sido el agua (30% de los casos); seguido por el suelo (24%); el paisaje (24%) y la biodiversidad (22%).Los ecosistemas más afectados han sido los ríos (36%), los bosques (24%); los páramos y el mar (7% cada uno).
-   Varias investigaciones señalan que el principal grupo afectado por proyectos de multinacionales ha sido el campesino, (28% de los casos); seguido por los grupos indígenas (17%); los habitantes de las ciudades (16%), los pescadores (14%), los pequeños mineros con (9%) y la comunidad afrodescendiente (6%).
-   Según el Comité de Solidaridad con Presos Políticos, entre junio del 2011, mes en el cual entró en vigencia la Ley de Seguridad Ciudadana, y abril del 2012, la población penitenciaria aumentó en 13.933 personas. En total, se pasó de 93.387 internos en junio del 2011 a 107.320 internos en abril del 2012.
-   Miembros del ESMAD están sindicados de haber causado desde su creación, por lo menos 20 ejecuciones extrajudiciales, 780 detenciones arbitrarias, 3000 heridos y 80 casos de tortura.

[RESUMEN EJECUTIVO DEL IV INFORME ALTERNO DE LA SOCIEDAD CIVIL AL COMITÉ DEL PACTO INTERNACIONAL DE DERECHOS...](https://www.scribd.com/document/340643961/RESUMEN-EJECUTIVO-DEL-IV-INFORME-ALTERNO-DE-LA-SOCIEDAD-CIVIL-AL-COMITE-DEL-PACTO-INTERNACIONAL-DE-DERECHOS-ECONOMICOS-SOCIALES-Y-CULTURALES-PIDESC#from_embed "View RESUMEN EJECUTIVO DEL IV INFORME ALTERNO DE LA SOCIEDAD CIVIL AL COMITÉ DEL PACTO INTERNACIONAL DE DERECHOS ECONÓMICOS, SOCIALES Y CULTURALES PIDESC on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17324155" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17324155_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_93703" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/340643961/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bl9ijuQalV7jA8YH42PX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
