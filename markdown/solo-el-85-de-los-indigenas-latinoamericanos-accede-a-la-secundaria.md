Title: El 15% de los indígenas latinoamericanos no acceden a la secundaria
Date: 2016-08-09 12:56
Category: DDHH, Nacional
Tags: Desnutrición Pueblos Indígenas, Día internacional de los Pueblos Indígenas, Educación pueblos indígenas, Pobreza pueblos indígenas
Slug: solo-el-85-de-los-indigenas-latinoamericanos-accede-a-la-secundaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Educación-pueblos-indígenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Andina] 

###### [9 Ago 2016] 

Desde 1994 la Asamblea General de las Naciones Unidas declaró el 9 de agosto como el Día Internacional de los Pueblos Indígenas, con el objetivo de reconocer a estas comunidades sus tradiciones, valores, idiomas y costumbres, y cómo han aportado al fortalecimiento de las culturas nacionales. Con esta conmemoración la ONU también busca **llamar la atención de los gobiernos y diversos sectores de la sociedad para solucionar los problemas a los que a diario se enfrentan los indígenas**, en ámbitos como los derechos humanos, el medio ambiente, la educación y la salud.

Para este año la conmemoración hace énfasis en el derecho a la educación, destacando que si bien la Declaración de las Naciones Unidas sobre los Derechos de los Pueblos Indígenas, dispone que tienen derecho a establecer y controlar sus sistemas e instituciones para que impartan educación en sus propios idiomas y métodos culturales, la mayoría de los pueblos indígenas no pueden ejercer plenamente este derecho pues **existen graves disparidades en relación con el resto de la población**.

En América Latina y el Caribe **el 85% de los niños y niñas indígenas asisten a la escuela secundaria, pero solo el 40% completa ese nivel educativo**. En Nunavut, Canadá, solo el 40% de los niños indígenas en edad escolar van a la escuela. En Australia, solo el 60% de los adolescentes indígenas de entre 15 a 19 años acceden al sistema educativo, mientras que en Guatemala el 50% de estos jóvenes no logran terminar sus estudios. En Perú la población no indígena recibe 2 años más de educación que los niños indígenas y en Bolivia la diferencia es de 4 años.

La ONU asegura que entre los obstáculos a los que se enfrentan los estudiantes indígenas, se destacan la estigmatización de su identidad; las **actitudes discriminatorias y racistas en el entorno escolar y en los libros de texto y material docente**; las barreras lingüísticas; la insuficiencia de recursos y la baja prioridad que se da a su educación y que se manifiesta en la escasa formación de los docentes, así como en la falta de material pedagógico.

En otros ámbitos como el de la pobreza la cifras también son alarmantes, pues pese a que los 370 millones de indígenas, que actualmente viven en 90 países, representan menos del 5% de la población mundial, constituyen **el 15% de los más empobrecidos y la tercera parte de los 900 millones de personas que viven en condición de indigencia** en las zonas rurales.

En Estados Unidos, el ingreso medio de los indígenas no llega a la mitad de la media general, en Alaska casi la cuarta parte vive bajo el umbral de pobreza, mientras que en Canadá el 60% de los niños indígenas sobreviven por debajo de este nivel. En Paraguay los pueblos indígenas son 7.9 veces más pobres que el resto de la población, en Panamá 5.9, en México 3.3 y Guatemala 2.8.

Frente a la esperanza de vida de los pueblos indígenas se observan diferencias de por lo menos 20 años en relación con el resto de la población. En Guatemala la esperanza de vida para estas comunidades es de 13 años, mientras que en Panamá es de 10, en México de 6, en Nepal de 20 al igual que en Australia, en Canadá es de 17 y en Nueva Zelanda de 11. Por otra parte la **mortalidad infantil indígena supera el 70% y la desnutrición duplica los registros para los niños no indígenas**, en Honduras el 95% de la población indígena menor de 14 años enfrenta este flagelo.

El desplazamiento forzado es otra de las problemáticas a las que se enfrentan actualmente los pueblos indígenas. El proyecto de la represa de Bakun en Malasia, **desplazó a por lo menos 8 mil indígenas de 15 comunidades, debido a la tala de 80 mil hectáreas** de bosques pluviales. En Tailandia varias comunidades de las tierras altas, entre ellas el pueblo karen, fueron desplazadas de los parques nacionales contra su voluntad.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
