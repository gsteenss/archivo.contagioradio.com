Title: EuroLat censura y estigmatiza a víctimas y defensores de DDHH
Date: 2015-06-04 12:47
Category: DDHH, Entrevistas
Tags: colectivo de Abogados José Alvear Restrepo, EuroLat, FARC, habana, Luis de Grandes Pascual, Partido Popular Europeo, Plataformas defensoras de derechos humanos, proceso de paz, Yessica Hoyos
Slug: eurolat-censura-y-estigmatiza-a-victimas-y-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.europarl.europa.eu]

<iframe src="http://www.ivoox.com/player_ek_4595536_2_1.html?data=lZqml5qXeo6ZmKiak5qJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZef0cbfzsbRqc%2FowtfW0diPqMafzcaYp9rWs63V1ZDS1dnNq8LhwtnW3MbSb9qfxMrb1drWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yessica Hoyos, Colectivo de Abogados José Alvear Restrepo ] 

Este jueves en la Asamblea Parlamentaria Eurolatinoamericana, había cita para que los parlamentarios escucharan la voz de las víctimas y defensores y defensoras de derechos humanos en Colombia sobre el proceso de paz. Sin embargo, **Yessica Hoyos,** vocera de la Plataforma Colombiana de Derechos Humanos, Democracia y Desarrollo, y abogada del Colectivo de Abogados José Alvear Restrepo,  **fue censurada,  estigmatizada y tildada como representante de la FARC-EP,** argumento que usaron **16 parlamentarios para votar en contra de que la abogada hablara**.

La voz de la Yessica Hoyos estaba programa para ser escuchada dentro de la agenda de la Comisión de Asuntos Políticos de Seguridad y Derechos Humanos en la Asamblea Parlamentaria Eurolatinoamericana, en donde hablaría sobre el proceso paz, y estaría compartida con **Rodrigo Rivera, embajador de Colombia ante la Unión Europea.**

Según relata la defensora de derechos humanos, tan pronto inició el programa, **el parlamentario español Luis de Grandes Pascual,** del Partido Popular Europeo, levantó la mano para indicar que no estaba de acuerdo con el orden del día, porque, según él, Hoyos representaba al colectivo de abogados José Alvear Restrepo, que a su vez eran voceros de las FARC, y por lo tanto ese no era el escenario para hablar sobre el proceso de paz, y mucho menos siendo representantes de la guerrilla.

Tras la iniciativa de Grandes Pascual, otros parlamentarios manifestaron lo mismo, y votaron para no permitir que Hoyos hablara como defensora de derechos y como víctima. Ellos, señalaron que** "no se podía equiparar la voz de los defensores de derechos humanos con la del embajador** (… ), incluso  resaltaron que  el debate entre las FARC y el gobierno era en la Habana”, cuenta la abogada.

“Es muy preocupante por la nueva estigmatización que se le hace a los defensores de derechos humanos y a las víctimas De esta manera definitivamente no podremos conseguir la paz en Colombia, **aquí se refleja la persecución que hemos vivido en Colombia, como lo hacía el DAS”**, expresa Yessica, quien fue una de las representantes de las víctimas en la Habana.

Tras esa situación, uno de los presidentes de la EuroLat llamó al embajador para que aclarara ante la audiencia que el Colectivo de Abogados no era representante de las FARC. Ante eso, el **embajador Rivera dijo que “no se podía poner a pelear con un europarlamentario”**. Frente a esa respuesta el mismo presidente pidió que por lo menos se anunciara que Yessica Hoyos fue una de las víctimas que estuvo en la Habana, pero ninguna de las aclaraciones las quiso dar a conocer el embajador, sin tener en cuenta que esos señalamientos de los parlamentarios ponen en riesgo la vida de la abogada.

A nombre de las plataformas de derechos humanos de Colombia, Yessica Hoyos iba a anunciar la necesidad inmediata de que se promueva un desescalamiento del conflicto y se llegue a un **cese al fuego bilateral.** Así mismo, iba a hablar sobre la importancia de **proteger a las víctimas** que siguen siendo blanco de amenazas y vulneraciones. Finalmente, señalaría que se debe crear un **manual para el manejo de la Policía Nacional** y empezar el **desmonte del ESMAD,** que agreden a las personas que reclaman sus derechos.

La abogada señala que los parlamentarios que no votaron en contra de que ella diera su discurso, se mostraron muy preocupados, y una de ellas pidió que se le diera la palabra a Hoyos para que se defendiera frente a esas acusaciones que ponen en peligro su vida. Pese a eso, no se le dio la oportunidad, “**esta acusación termina siendo contra la gran mayoría de las organizaciones de derechos humanos; quedó dicho ante los parlamentarios que yo soy vocera de las FARC”.**

Ante esta situación, se dejó por escrito la intervención de la vocera de las organizaciones de derechos humanos, aclarando también quiénes son y qué hace el Colectivo de Abogados José Alvear Restrepo y el resto de plataformas.

Para Hoyos, “es necesario que se empiece hacer una campaña en el extranjero evidenciando que los defensores de derechos humanos no son terroristas, se tienen que respetar los derechos humanos de las víctimas, tiene que ser una realidad que las víctimas sean el centro del proceso de paz, no puede seguir siendo un discurso”,  y subraya “sentí que **al gobierno colombiano le interesa las víctimas de papel; así nunca se va a alcanzar una paz con justicia social”.**
