Title: Colombia debe exigir que ELN y Gobierno se vuelvan a sentar a dialogar: Iván Cepeda
Date: 2019-01-21 12:51
Author: AdminContagio
Category: Nacional, Política
Tags: Cuba, ELN, Iván Cepeda, Iván Duque, Mesa de diálogos
Slug: sociedad-colombiana-debe-exigir-que-eln-y-gobierno-se-vuelvan-a-sentar-en-la-mesa-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2018-09-03-a-las-12.46.46-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Ene 2019] 

Iván Cepeda, senador por el Polo Democrático, hizo un llamado al presidente Duque para que respete el protocolo de finalización de la mesa con ELN y no viole el Derecho Internacional Humanitario. También** señaló que la sociedad colombiana debe exigirle tanto a la guerrilla como al gobierno, que vuelvan a sentarse en la mesa de conversaciones. **

El senador señaló que insistirán en que los acuerdos alcanzados durante estos 2 años, se pongan al abrigo de la comunidad internacional y alentó a la sociedad colombiana para que continúe en la labor de construir paz. Reiteró que luego de la ruptura de un proceso de paz viene un ciclo fuerte de violencia, "así ha sido y así será siempre y lo digo con tristeza". (Le puede interesar: ["Romper conversaciones contra el ELN implica un nuevo ciclo de violencia"](https://archivo.contagioradio.com/romper-conversaciones-con-eln-implica-un-nuevo-ciclo-de-violencia/))

### **El presidente Duque y el ELN debe tomar acción en pro de la paz** 

Cepeda manifestó su total condena frente a la acción criminal perpetrada el pasado 17 de enero en la Escuela General Santander, y aseguró que no hay aprobación desde el punto de vista político, ético o militar para ese hecho, razón por la cual **"el Estado colombiano tiene que realizar la acción represiva contra quienes pusieron el carro bomba"**.

Sin embargo, expresó que eso no significa que Duque pueda pasar por alto el protocolo establecido en el proceso con el ELN, en caso de que se acabe la negociación, debido a que eso implicaría violar principios elementales en el Derecho Internacional, "hay una obligación de cumplir unos protocolos que **no están sujetos al capricho del Presidente de la República, pero bueno al parecer ese es el camino que el quiere adoptar**".

Del otro lado, advirtió que el Ejército de Liberación Nacional también debe dar muestras reales y fehacientes de que quiere llegar a una construcción de paz, "hay que exigirle al ELN que se siente a la mesa con una voluntad clara, firme y con determinación en miras de paz".

<iframe id="audio_31650414" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31650414_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
