Title: El papel de las mujeres en la Jurisdicción Especial para la paz
Date: 2017-10-29 18:00
Category: Libertades Sonoras
Tags: Comite de Escogencia, JEP, Mujeres y Paz
Slug: mujeres-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [The International Center for Transitional Justice] 

###### [26 Oct 2017] 

[Las mujeres escogidas como magistradas en los tribunales de la Jurisdicción Especial para la Paz, representan el 53% de las y los magistrados seleccionados por el Comité de Escogencia, justamente ese papel de las mujeres en la JEP así como también en la construcción de la paz en el país, fue el tema que tratamos en \#LibertadesSonoras.]

En el programa destacamos los perfiles y reacciones de personas como L[uz Marina Monzón, elegida para el cargo de la Directora Unidad de búsqueda de personas desaparecidas, y el Patricia Linares, ahora presidenta JEP. (Le puede interesar: ["Lo fundamental será la voluntad política de las instituciones": Luz Marina Monzón)](https://archivo.contagioradio.com/?p=47342)]

[Además hablamos con Katherine Ronderos, Vocera de la Cumbre Nacional de Mujeres y Paz, quien analizó este reconocimiento a las mujeres en materia de construcción de paz, aunque señaló que todavía no es suficiente y debe ponerse en marcha otra serie de medidas para garantizar los derechos de las mujeres.]

<iframe id="audio_21711820" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21711820_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
