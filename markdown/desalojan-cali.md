Title: A la fuerza desalojan barrios populares en Cali para construir apartamentos
Date: 2018-11-20 15:53
Author: AdminContagio
Category: DDHH, Nacional
Tags: Agua Blanca, Cali, ESMAD, policia, Valle del Cauca
Slug: desalojan-cali
Status: published

###### [Foto: Contagio Radio] 

###### [20 Nov 2018] 

Habitantes del barrio **Lagos, en el distrito Agua Blanca** de Cali, denuncian que este martes se desarrolló un fuerte operativo policial para desalojar el terreno en el que la comunidad desarrollaba sus actividades culturales y deportivas, con el fin de iniciar  la construcción de apartamentos. Sin embargo, el proyecto no fue concertado con los habitantes del sector, ni planes para construir un nuevo espacio social para las personas.

Según habitantes de la zona, **en el lote funcionaban un poliactivo del adulto mayor, la caseta comunal de Lagos y la cancha de fútbol que usaban más de 300 niños,** inscritos en 3 escuelas de formación. Adicionalmente, era un espacio construido por la misma comunidad hace bastantes años, y hace parte de la zona protectora de la laguna El Pondaje. (Le puede interesar: ["Comunidad de barrio La Paz, denuncian intento de desalojo por parte de fuerza pública"](https://archivo.contagioradio.com/comunidad-de-barrio-la-paz-en-cali-denuncia-intento-de-desalojo-por-parte-de-fuerza-publica/))

De acuerdo a la información que recaudaron los vecinos del sector, la encargada de desarrollar el proyecto es la Constructora Prefabricados I.C., y con el mismo, esperan reubicar a las personas desalojadas del centro de Cali. No obstante, **el proyecto no fue socializado con los residentes del sector**, y solo hasta el día de hoy se hizo pública la ficha técnica del proyecto, que fue emitida por la curaduría urbana.

###### [![Desalojo en Cali](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-20-at-1.09.43-PM-800x449.jpeg)

###### [\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Video-2018-11-20-at-11.24.50-AM.mp4\[/KGVID\]] 

###### [  
Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
