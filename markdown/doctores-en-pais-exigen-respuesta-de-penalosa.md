Title: 14 Doctores de París exigen explicación de Peñalosa sobre su falso doctorado
Date: 2016-04-21 16:26
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Enrique Peñalosa, París
Slug: doctores-en-pais-exigen-respuesta-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Enrique-Peñalosa-e1461273574548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lafarge Holcim Foundation 

###### [21 Abr 2016] 

Luego de que se comprobara que Enrique Peñalosa, alcalde de Bogotá, no tenía ningún doctorado en administración pública de la Universidad de París, 14 doctores y candidatos a ese título, reclamaron al alcalde por esa situación y recordaron que **“por casos similares de falsedad o plagio han renunciado altos funcionarios en otros países”.**

Desde la alcaldía se había contestado a la opinión pública que Enrique Peñalosa nunca había dicho que tenía un doctorado, sin embargo, en varios **medios de comunicación aparece entre sus estudios el doctorado, además existen varios documentos, libros, otras publicaciones  e incluso en la página de la Alcaldía** se señalaba a Peñalosa con ese título.

Ante esa situación y la indignación de quienes realmente son doctores, desde París, un grupo de doctores y aspirantes a ese título enviaron un documento al alcalde basado en la reflexión de Michael Foucault sobre la relación entre “el ejercicio del poder y la manifestación de la verdad”.

“Compartimos esta reflexión a propósito del debate suscitado con motivo de su presunto doctorado en Administración Publica en Paris que, en muchas ocasiones, durante mucho tiempo y ante diferentes audiencias, usted dijo o permitió que dijeran o escribieran como un hecho cierto”, dice la carta donde agregan que “Por supuesto que el falso título de doctor es muy grave, pero **puede ser mucho más grave si esta forma de ser gobernante se traslada a decisiones de política pública que afectan a la sociedad y a la naturaleza”**, refiriéndose a temas como actual polémica que hay con la Reserva Thomas Van Der Hammen.

Finalmente el documento asegura que es injusto que Peñalosa se haya valido de esos falsos estudios para obtener votos, sin tener en cuenta el sacrificio de otras personas para lograr culminar estos estudios de doctorado es por ello que exigen una explicación del alcalde que aún no se ha dado.

### Aquí la carta

Paris, 19 de abril de 2016  
Señor  
Enrique Peñalosa  
Alcalde Mayor de Bogotá  
Señor Alcalde

*En los albores del siglo III el emperador romano, Septimus Severo, anunció a sus súbditos que había llegado al gobierno porque así lo determinaba el alineamiento de los astros en la hora de su nacimiento y que, en consecuencia, todas sus acciones respondían a la necesidad misma del mundo y se justificaban por sí solas, más allá de la razón, por encima del saber o haciendo abstracción del conocimiento que fundamenta una verdad.*

*Este momento de la historia, que sin duda usted conoce, le permitió a Michael Foucault iniciar su extraordinaria reflexión sobre la relación que existe entre “el ejercicio del poder y la manifestación de la verdad”. Decía el filósofo francés que hay “procedimientos, verbales o no, mediante los cuales se saca a la luz -y esta puede ser tanto la conciencia individual del soberano como el saber de sus consejeros o la manifestación pública- algo que se afirma o más bien se postula como verdadero, sea claro está por su oposición a una falsedad que ha sido suprimida, discutida, refutada, pero también tal vez por arrancamiento a lo oculto, por disipación de lo olvidado, por conjuro de lo imprevisible” (Foucault, 2014, pp. 24)\**

*Compartimos esta reflexión a propósito del debate suscitado con motivo de su presunto doctorado en Administración Publica en Paris que, en muchas ocasiones, durante mucho tiempo y ante diferentes audiencias, usted dijo o permitió que dijeran o escribieran como un hecho cierto. Recuerde usted que Foucault se refería a “la noción de gobierno de los hombres por la verdad” y proponía el concepto de aleturgia como espacio de confluencia entre el poder y la verdad. Este asunto de ocultar una verdad o permitir que se mantenga en el tiempo una mentira, debería servir para una reflexión en torno a la ética, al poder en el ejercicio de gobierno que usted inició hace poco más de cien días.*

*Todo parece indicar que usted, al igual que el emperador Septimus Severo, decidió establecer el “orden mismo de las cosas” desde la imaginación. Por supuesto que el falso título de doctor es muy grave, pero puede ser mucho más grave si esta forma de ser gobernante se traslada a decisiones de política pública que afectan a la sociedad y a la naturaleza (el arte de gobernar y el juego de la verdad no son independientes uno de otro decía Foucault).*

*Así, por ejemplo, a las evidencias científicas que ordenan proteger la gran reserva forestal Thomas Van der Hamnen, usted responde que no son ciertas y que la reserva se va a urbanizar, frente a estudios de ingeniería ya elaborados para construir en Bogotá un Metro subterráneo, usted decide cambiar el diseño y anunciar, sin fundamentos técnicos, que ahora se va a hacer un metro elevado.*

*Otra vez Foucault: “No puede haber gobierno si quienes gobiernan no ajustan sus acciones, sus elecciones, sus decisiones, a un conjunto de conocimientos verdaderos, de principios racionalmente fundados o de conocimientos exactos, que no solo dependen de la sabiduría en general del príncipe o de la razón a secas, sino de una estructura racional que es propia de un dominio de objetos posibles, y que es el Estado" (Foucault, 2014, pp. 32)\**

*Ahora bien, el ejercicio del gobierno no empieza ni termina con el gobernante. Cuando mire hacia atrás hágalo sin odio, con objetividad y se dará cuenta que, para bien o para mal, la Bogotá de 1998 no es la misma ciudad de 2016. El emperador Septimus Severo nunca reconoció a sus antecesores, solo creía en su mandato como emperador y en sus decisiones, que por ser suyas, eran simplemente “divinas”.*

*Quienes firmamos esta carta estamos adelantando o ya terminamos estudios de doctorado en diversas universidades del mundo. Usted debe saber, señor alcalde, que lo hacemos con esfuerzo y convicción y con ganas de aportarle al país en la medida de nuestras posibilidades. Por eso nos parece injusto que personas como usted, que hoy gobierna a la capital del país, suplante con falsedades lo que a nosotros y nosotras tanto sacrificio nos cuesta.*

*La comunidad académica en Colombia y en el mundo, pero sobre todo la ciudadanía (quienes votamos y quienes no votamos por usted) merecemos una explicación del Alcalde Mayor de Bogotá. Usted debe saber que por casos similares de falsedad o plagio han renunciado altos funcionarios en otros países.*

*\*Foucault, Michel. (2014). Del gobierno de los vivos: Curso en el College de France (1979-1980). -1ª ed., Ciudad Autónoma de Buenos Aires, Fondo de Cultura Económica, pp 24,32*  
*Atentamente,*  
*Andrea Marcela Barrera Téllez*  
*Estudiante del doctorado en sociología y género - Universidad Paris 7 - Paris Diderot – Francia*  
*Juan Manuel Hernández Vélez*  
*Ecole doctorale d'histoire du droit, philosophie du droit et sociologie du droit, Université Paris II Panthéon-Assas*  
*Gerardo A. Malagon Valbuena*  
*Universidad Paris V*  
*Doctorado en Neurociencias*  
*Lina del Mar Moreno Tovar*  
*Estudiante de Doctorado en Antropología Social*  
*Escuela Nacional de Antropología e Historia (México)*  
*Tomas Andrés Guzmán*  
*Doctor en Filosofía*  
*Universidad Nacional de Colombia*  
*Sally Ann García Taylor*  
*Centro de Investigaciones y Estudios Superiores en Antropología Social, CIESAS, Occidente*  
*Doctorado en Ciencias Sociales con énfasis en Antropología Social*  
*Guadalajara, Jalisco*  
*México*  
*Oscar F. Amaya Ortega*  
*Universidad de Georgetown*  
*Washington, DC. Estados Unidos*  
*Candidato a Doctor (ABD)*  
*Literatura Latinoamericana y Estudios Culturales*  
*Andrés Alarcón Jiménez*  
*Doctor en Historia*  
*Universidad Estadual de Campinhas*  
*Brasil*  
*Andrés Felipe Manosalva Correa*  
*Estudiante de Doctorado en Ciencias Sociales y Humanas*  
*Pontificia Universidad Javeriana*  
*Bogotá*  
*Nataly Camacho*  
*Candidata a doctor en antropología y sociología*  
*Universidad París Diderot VII, París*  
*Gabriel Rojas Andrade*  
*MSc en Teoria Politica*  
*London School of Economics and Political Science*  
*Candidato a doctor en Derecho*  
*Universidad de los Andes*  
*Bogotá*  
*Paula Rodríguez Zorro*  
*Universidad de Göttinen- Alemania*  
*Doctorado en Ecología, Biodiversidad y Evolución.*  
*Department of Palynology and Climate Dynamics*  
*Albrecht von Haller Institute for Plant Sciences*  
*Georg-August University Göttingen*  
*Untere Karspüle 2*  
*Diego F Leal*  
*Estudiante de Doctorado en Sociología*  
*Universidad de Massachusetts-Amherst*  
*Camilo Hernando Perdomo Estrella*  
*Universidad Complutense de Madrid*  
*Doctorando en periodismo UCM*
