Title: FARC pidió perdón a las víctimas en un gesto de paz
Date: 2020-09-15 18:42
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Comisión de la Verdad, FARC, FARC pide perdón, Secuestro
Slug: farc-pidio-perdon-a-las-victimas-en-un-gesto-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Excomandantes-de-FARC.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un grupo de ocho **excomandantes de las FARC-EP envió una carta pidiendo perdón a todas las víctimas de secuestro y sus familias.** La carta se hizo pública luego de un encuentro virtual entre la Comisión de la Verdad e Ingrid Betancourt, víctima de secuestro por parte de este antiguo grupo armado, quien hizo una reflexión pública sobre ese delito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de la carta, **los excombatientes no sólo reconocieron el delito de secuestro, que calificaron como «*un gravísimo error*», sino que afirmaron estar arrepentidos.  **(Le puede interesar: [Implementación del acuerdo es el centro del debate al interior de FARC](https://archivo.contagioradio.com/ubaldo-zuniga-farc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los apartes de la misiva señala que el secuestro sólo dejó una profunda herida en el alma de los afectados e «*hirió de muerte su legitimidad y credibilidad*» como grupo. También señalaron que esa decisión se tomó en las «*circunstancias excepcionales de la guerra*» y ahora tienen que arrastrar ese lastre, que hasta hoy, pesa en «*la conciencia y en el corazón*» de cada exintegrante de las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«*Hoy día, entendemos el dolor que les causamos a tantas familias, hijos, hijas, madres, padres, hermanos y amigos, que vivieron un infierno esperando tener noticias de sus seres queridos; imaginando si estarían sanos y en qué condiciones estarían, siendo sometidos a seguir la vida lejos de sus afectos, de sus proyectos y de sus mundos*", se lee en uno de los apartes de la carta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Una de las cosas que más llama la atención, es que los exjefes guerrilleros le dan el tratamiento de «*secuestro*» a estos hechos y dejan de lado el término de «*retención*»;** que ha sido criticado por muchas víctimas que no participaban de la hostilidad del conflicto, y que aun así, fueron raptadas por el antiguo grupo guerrillero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al finalizar su carta, los excomandantes, reiteraron su compromiso y voluntad de rendir cuentas ante la justicia y contarle la verdad a la sociedad colombiana sobre las violaciones de Derechos Humanos y las infracciones al Derecho Internacional Humanitario, cometidas por las extintas FARC-EP.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los firmantes de la carta se encuentran el excomandante de las FARC, Rodrigo Londoño; y otros miembros del secretariado como Pastor Alape, Rodrigo Granda, Jaime Alberto Parra, Pablo Catatumbo, Milton de Jesús Toncel, Juan Emilio Cabrera y Julián Gallo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PCatatumbo_FARC/status/1305652331938295808","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PCatatumbo\_FARC/status/1305652331938295808

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### FARC da un paso hacia la paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

La [Comisión de la Verdad](https://comisiondelaverdad.co/actualidad/comunicados-y-declaraciones/a-proposito-de-la-carta-publica-de-farc-secuestro) a través de su presidente Francisco de Roux, reconoció el gesto de los excomandantes y señaló que más importante que determinar cuándo se dice la verdad es que esta se diga en algún momento. (Le puede interesar: [Organizaciones sociales convocaron a un Pacto por la Vida y la Paz](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la Comisión, los exjefes de FARC «*han dado un paso que esperábamos hacia la paz basada en la verdad. Un paso en la sinceridad que todos debemos acoger y rodear, en un momento en que reciben el ataque de quienes no les creen y de quienes les echan en cara que vengan a decir la verdad solo ahora*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «*A los hombres y a las mujeres les toma tiempo llegar a reconocer, pero cuando se reconoce, la grandeza no sólo está en quienes lo hacen, sino en quienes aceptan ese reconocimiento*»  
>
> <cite>Francisco de Roux, presidente de la Comisión de la Verdad</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FranciscoDeRoux/status/1305860239598608386","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FranciscoDeRoux/status/1305860239598608386

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Muchos sectores coinciden en afirmar que **este gesto, aunque pequeño, contribuye con la verdad, el perdón y reconciliación con las víctimas y sobretodo con la garantía de no repetición de estos actos de barbarie que se realizaron en medio de la guerra.** (Le puede interesar: [Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político"](https://archivo.contagioradio.com/mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
