Title: Nueva Reforma Rural profundiza acumulación de baldíos
Date: 2018-07-17 16:46
Category: Nacional, Paz
Tags: Acumulacion de tierras, Corte Constitucional, reforma rural, Wilson Arias
Slug: nueva-reforma-rural-profundiza-acumulacion-de-baldios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/BALDIOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Jul 2018] 

El pasado 12 de julio la Corte Constitucional aprobó el decreto 902 de 2017 por la cual se formaliza la Reforma Rural, uno de los puntos más importantes del Acuerdo de Paz que buscaba acabar con el conflicto de la tierra, la acumulación de predios y la redistribución del territorio. Sin embargo, de acuerdo con el experto en temas agrarios, Wilson Arias, esta reforma sería la **puerta de entrada a que grandes empresarios, de forma legal, puedan hacerse acreedores de grandes extensiones de tierra**.

Según Arias, el artículo 6 sobre *“sujetos de formalización a título oneroso”* permite que se formalicen baldíos a “poderosos” del país, que pagaran por ellos, y, al mismo tiempo, facilita que se profundice la concentración de la propiedad rural del país, cerrando **"de un portazo la posibilidad de una Reforma Rural en Colombia"**.

\[caption id="attachment\_54853" align="alignnone" width="549"\]![Texto Reforma Rural](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/arti6-549x455.png){.size-medium .wp-image-54853 width="549" height="455"} Texto Reforma Rural\[/caption\]

“Hasta hoy la norma ha dispuesto lo que especialistas denominan como mercado circular de tierras campesinas a través de campesinos, es decir los campesinos solo pueden venderse tierras entre ellos y cumpliendo con los requisitos de la Reforma Agraria, ahora entra un nuevo sujeto que rompe el mercado circular de tierras”, afirmó Arias refiriéndose a las multinacionales o grandes empresas que ahora podrán comprar estas tierras baldías, superando las ofertas que hagan los campesinos, sin ningún límite.

Para Arias, si bien el decreto 902 de 2017, cuenta con herramientas importantes para la adjudicación de tierras, el artículo 6 es una contraparte al objetivo general de la Reforma Rural Integral “profundamente regresivo”. (Le puede interesar: ["¿Por qué la Ley ZIDRES es anticampesinos?"](https://archivo.contagioradio.com/ley-zidres-es-anticampesinos-willson-arias/))

### **El fallo de la Corte una bofetada al campesinado** 

Frente a la decisión de la Corte Constitucional, Arias señaló que “la Corte esta merced de las presiones sociales y políticas que hay en el país”, sumado a la fuerte presión del lobby de las transnacionales, que de acuerdo con el analista ya estarían negociando grandes extensiones de tierras, **“Cargill se quedó con 52 mil hectáreas, Mónica Semillas Brasilera podría venir por 13 mil”** indicó Arias.

De igual forma afirmó que si la Core Constitucional y el Estado tuviesen un verdadero interés por generar una Reforma Rural Integra que acabe con la acumulación de tierras del país, no habrían permitido este nuevo hueco que se abre en el mercado de tierras, que además fue incluido en las reformas a esta Ley por el Congreso de la República.

<iframe id="audio_27109556" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27109556_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
