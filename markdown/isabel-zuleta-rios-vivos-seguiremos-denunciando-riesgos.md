Title: “seguiremos denunciando a pesar de los riesgos" Isabel Cristina Zuleta
Date: 2019-08-29 13:19
Author: CtgAdm
Category: Líderes sociales
Tags: ambientalistas, Amenazas a opositores de Hidroituango, Hidroituango, liderezas, Movimiento social
Slug: isabel-zuleta-rios-vivos-seguiremos-denunciando-riesgos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/DmbuY2YXgAAqGAl.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

A través de una alerta temprana presentada por el Colectivo de Abogados José Alvear Restrepo (CAJAR),el Movimiento Ríos Vivos Antioquia se declaró en estado de riesgo por la situación de peligro en la que se encuentran varios de sus integrantes, en especial Isabel  Zuleta quien encabeza este movimiento, manifestando que cada vez es más difícil y riesgoso ejercer de la defensa de los derechos humanos, territoriales y ambientales.

Uno de los agravantes que afronta el movimiento en Antioquia, que los hace diferente a la situación de las y los líderes sociales en el país, es afrontar la crisis que ha generado el Estado y las empresas promotoras del proyecto Hidroituango en la comunidad, a raíz de las diferentes investigaciones que ha liderado Rios Vivos Antioquia por ese megaproyecto. (Le puede interesar:[Medidas cautelares priorizan a víctimas y ambiente sobre Hidroituango](https://archivo.contagioradio.com/medidas-cautelares-priorizan-a-victimas-y-ambiente-sobre-hidroituango/))

> Muchas gracias por tantos mensajes de solidaridad La situación es realmente muy díficil para mí como persona y como lideresa Muy doloroso escuchar que me dicen "la única razón que encontramos los que la queremos para que usted quiera seguir siendo lider es porque se quiere morir"
>
> — ISABELCRISTINAZULETA (@ISAZULETA) [August 27, 2019](https://twitter.com/ISAZULETA/status/1166490565325795328?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por medio de la alerta temprana, denuncian que en los últimos meses han recibido múltiples agresiones, por ejemplo aseguran que partidos políticos como el Partido Conservador y el Centro Democrático han recolectado firmas en su contra y convocado reuniones que tienen como fin diseñar estrategias para atacarlos.  "hemos sido fruto de una persecución y estigmatización especialmente por partidos políticos, recogiendo  en el  municipio de Ituango firmas en nuestra contra, EPM ha contratado a mas de 90 lideres que señal, discriminan y calumnian nuestro Movimiento"

> **“tememos lo peor, somos objeto del desprestigio constante con calumnias que conducen al desprecio y rechazo público orquestado por la empresa, funcionarios y líderes pagados por EPM”** señaló Isabel Zuleta.

También denuncian puntualmente a sindicados que promueven actos de rechazo al movimiento, "**Cesar Mira,** quien por redes sociales defiende el proyecto Hidroituango y ataca al proceso de oposición, **Iván Posada** quien dirige al Centro Democrático en Ituango y quien considera un exabrupto la búsqueda de personas dadas por desaparecidas y las denuncias sobre violencia paramilitar en la zona, **Fernando Calle Gallo** funcionario de la Fiscalía que se ha presentado en algunas de las sesiones de la Mesa Técnica como líder social y **Carlos Mario Calle** exalcalde de Ituango".

La persecución ha llegado hasta el punto de afectar a los hijos de los integrantes de Movimiento, "en las instituciones educativas, profesores insisten en que los niños y las niñas no hablen de la realidad que viven, de lo ocurrido con sus padres y con el río Cauca”. Así mismo insistentes investigaciones a todos los integrantes de la familia Zuleta, "han venido persiguiendo a  toda mi familia, esto hace un daño tremendo a los lideres, y saben que el punto débil nuestro es este, muchos pueden soportar calumnia y desprestigio hacia si mismo, pero cuando esto llega a sus familias se ven forzados a desistir de su causa".

A pesar de los múltiples problemas y riesgos aseguran “seguiremos denunciando a pesar de los riesgos, pero hacemos responsable al Estado y en especial a las entidades que integran la Mesa Técnica de lo que nos pueda ocurrir”afirmo Zuleta.(Le puede interesar: [¿Qué oculta EPM tras evacuación de empleados por filtraciones en Hidroituango?](https://archivo.contagioradio.com/que-oculta-epm-tras-evacuacion-de-empleados-por-filtraciones-en-hidroituango/)

Adicionalmente en las últimas horas, tuvieron una sesión en la procuraduría reunidos con entidades como, Empresas Públicas de Medellín (EPM), Fiscalía, Ministerio de Medio Ambiente entre otras, que preocupan al Movimiento pues afectan de manera directa a las comunidades de la región.

### Un reconocimiento a su trabajo

Isabel Cristina está nominada por segunda vez al Premio Nacional a la Defensa de los Derechos Humanos,esta  vez  en la categoría Defensor o Defensara del Año, por su trabajo como "una tejedora de relaciones sociales", y en especial  a su trabajo como líder ambientalista, y  su arduo trabajo por detener el proyecto hidroituango, "para mi es muy gratificante, es un reconocimiento a la labor individual y a la resistencia que es tan poco reconocida, y pensando que un individuo desaparece en el colectivo, cuando al contrario este se potencia en comunidad"(Le puede interesar: [¿Qué oculta EPM tras evacuación de empleados por filtraciones en Hidroituango?](https://archivo.contagioradio.com/finalistas-del-premio-a-la-defensa-de-los-derechos-humanos/)

 

**Síguenos en Facebook:**  
<iframe id="audio_40608827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40608827_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
