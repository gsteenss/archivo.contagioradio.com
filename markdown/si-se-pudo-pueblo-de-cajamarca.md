Title: ¡Sí se pudo!: Cajamarca le dijo NO a la minería en su territorio
Date: 2017-03-26 17:19
Category: Ambiente, Voces de la Tierra
Tags: Cajamarca, Consulta minera, consulta popular
Slug: si-se-pudo-pueblo-de-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-26-at-3.53.22-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de Cajamarca] 

###### [26 mar. 2017] 

Con esta frase y luego de una jornada pasada por lluvia y varios contratiempos, los cajamarcunos celebran a esta hora haber ganado la Consulta Popular, **con un rotundo no de 6165 votos y con 100% de las mesas escrutadas, este municipio del Tolima dice no a la mineria.**

En medio de gritos y aplausos el pueblo de Cajamarca se reúne a esta hora en la sede del Comité promotor de la Consulta Popular para escuchar de manera conjunta los resultados de las mesas escrutadas por la Registraduría. Le puede interesar: [Consulta Popular en Cajamarca pondría fin a La Colosa](https://archivo.contagioradio.com/consulta-popular-en-cajamarca-pondria-fin-colosa/)

En comunicación con Contagio Radio y en medio de una desbordante alegría, integrantes del **Comité promotor manifestaron “el no ha ganado”.** En contexto: [Así va la Consulta Popular en Cajamarca](https://archivo.contagioradio.com/asi-va-la-consulta-popular-en-cajamarca/)

Esta era la pregunta por la que debían votar sí o no los pobladores de Cajamarca: ¿Está de acuerdo sí o no con que en el municipio de Cajamarca se ejecuten proyectos y actividades mineras?

![VOTACIONES CAJAMARCA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/VOTACIONES-CAJAMARCA.jpg){.alignnone .size-full .wp-image-38377 width="822" height="1080"}

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>
