Title: Tras liberación de Salud Hernandez organizaciones exigen inicio de conversaciones con ELN
Date: 2016-05-27 18:35
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, ELN, proceso de paz
Slug: tras-liberacion-de-salud-hernandez-organizaciones-exigen-inicio-de-conversaciones-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/movilizacio-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: medellinantifascista] 

###### [27 May 2016]

En un comunicado de cuatro puntos, varias organizaciones de la sociedad civil, congresistas y otras figuras de la vida pública, saludaron la liberación de la periodista Salud Hernández por parte de sus captores, e instaron al gobierno nacional y al ELN a **iniciar las conversaciones de paz** así como a des escalar el conflicto y evitar más víctimas de le guerra en Colombia.

El comunicado dirigido al presidente Santos, a Frank Pearl y a los negociadores del ELN Antonio García y Pablo Beltrán recuerda también que la agenda pactada incluye el tema de la “privación de la libertad” de personas y por eso es necesario que se acelere el **[inicio de las conversaciones en la ciudad de Quito](https://archivo.contagioradio.com/eln-explica-en-que-va-el-proceso-de-conversaciones-de-paz/) en Ecuador** tal como se había anunciado a finales de marzo pasado.

También instaron a las partes a evitar el escalamiento de la confrontación puesto que la militarización de los territorios es una amenaza para las organizaciones sociales y para la población en general. Además invitaron a la sociedad en general a participar de manera activa en los espacios que la agenda de conversaciones prevé para ello y así **poder plantear todos los temas que crean convenientes** en la [línea de la construcción de la paz con justicia social.](https://archivo.contagioradio.com/la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz/)

[Organizaciones piden por el inicio de conversaciones de paz con el ELN](https://es.scribd.com/doc/314046913/Organizaciones-piden-por-el-inicio-de-conversaciones-de-paz-con-el-ELN "View Organizaciones piden por el inicio de conversaciones de paz con el ELN on Scribd")

<iframe id="doc_48086" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/314046913/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
