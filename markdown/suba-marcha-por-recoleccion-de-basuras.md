Title: Habitantes de Suba marcharán por deficiencias en recolección de basuras
Date: 2018-07-27 17:42
Category: Movilización
Tags: Avenida Cali, Basuras en bogota, marcha, Portal de Suba, Suba
Slug: suba-marcha-por-recoleccion-de-basuras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-27-a-las-4.49.14-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Abiliopena] 

###### [27 Jul 2018] 

Vecinos de la localidad de Suba, al noroccidente de Bogotá, reclaman que no se esta prestando un adecuado servicio de recolección de basuras, hecho que se suma al deficiente sistema de barrido, el aumento de la tarifa de aseo y los retrasos que se presentan por parte de la empresa Área Limpia SAS.

Esperanza Rincón, lidereza de la comunidad, recordó que tras la entrada en funcionamiento del nuevo esquema de aseo en Bogotá, impulsado por el Alcalde Enrique Peñalosa, **la capacidad de los camiones recolectores en Suba se redujo de 15 toneladas a menos de la mitad,** lo que ha significado retrasos en la prestación del servicio.

Rincón, también señaló que el barrido de las calles se realiza de forma deficiente, porque las personas que tienen esa labor no cuentan con los implementos necesarios para realizar su trabajo, y el barrido se está realizando solo dos días a la semana, situación que provoca suciedad en las calles de la zona.

Adicionalmente, hay incongruencias en la facturación del servicio de aseo pues según la líder, **en una misma cuadra hay diferentes valores cobrados por el servicio de limpieza pública.** Aunque la comunidad ha presentado derechos de petición a la empresa para solucionar los cobros excesivos, ésta no ha respondido.

Por estas razones, la población afectada marchará para que se preste un servicio adecuado este sábado a las 8:30 de la mañana **desde la calle 132 con Avenida Ciudad de Cali hasta el Portal de Suba.** (Le puede interesar: ["Situación de trabajadores de aseo de Bogotá ya se había presentado en primera alcaldía de Peñalosa"](https://archivo.contagioradio.com/situacion-de-trabajadores-de-aseo-de-bogota-ya-se-habia-presentado-en-primera-alcaldia-de-penalosa/))

### **Nuevo esquema de basuras, más costos para los bogotanos** 

El nuevo esquema de recolección de basuras, que entró en operación en febrero de este año, consistió en el cambio de la empresa pública Aguas de Bogotá a 5 compañías privadas, lo que representó el despido de más de 3 mil trabajadores, la exclusión de los recicladores del sistema de aseo y una crisis sanitaria que se mantuvo durante casi 15 días en los que no se recogieron los residuos de la ciudad oportunamente.

A pesar de que el Alcalde Peñalosa se comprometió durante su candidatura a reducir los cobros por el sistema de aseo, la implementación del nuevo esquema ya significó un aumento para los bogotanos en sus facturas el año pasado; y de acuerdo con información entregada por el operador del relleno sanitario Doña Juana, tendría que realizarse un nuevo aumento para compensar la cantidad de desechos que recibe el lugar.

<iframe id="audio_27272881" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27272881_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
