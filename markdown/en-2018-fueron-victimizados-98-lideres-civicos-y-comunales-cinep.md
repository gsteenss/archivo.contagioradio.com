Title: Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales
Date: 2019-05-09 19:29
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: CINEP, juntas de acción comunal, Plan de Sustitución de Cultivos de Uso ilícito, violencia contra líderes sociales
Slug: en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CINEP-RESPONSABLES-2018.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/LUGARES-CINEP-2018.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo] 

El **Centro de Investigación y Educación Popular, CINEP** dio a conocer su informe anual, 'Violencia Camuflada, la base social en riesgo', un estudio que denota un incremento en el 2018 de los ataques a integrantes y líderes de movimientos sociales de base, como las juntas de acción comunal (JAC), registrando 648 asesinatos que según la investigación serían consecuencia de la violencia política.

Aunque el CINEP recalca que el informe no fue hecho con el fin de hacer una lectura estadística de la violencia, son alarmantes algunos de los datos documentados, revelando un total de 1418 casos de violaciones de DD.HH, 2252 víctimas individuales y 98 líderes cívicos y comunales victimizados para el periodo comprendido entre el 1 de enero al 31 de diciembre de 2018.

Asimismo, el texto evidencia que la violencia política también dejó 648 muertos, 48 atentados, 1151 personas amenazadas, 304 heridos,  66 torturados, 3 víctimas de violencia sexual, 22 desaparecidos y 243 personas detenidas de formar arbitraria.

En esa misma línea la investigación advierte que **Valle del Cauca, Cauca, Santander, Antioquia, Chocó  y Bolívar** son los departamentos donde más violaciones a derechos humanos se cometieron, y que en comparación de los años 2016 y 2017, a pesar de que se ha reducido el número de heridos, las muertes y amenazas en el país han incrementado en un 25% y 32% respectivamente.

\[caption id="attachment\_66678" align="aligncenter" width="720"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/LUGARES-CINEP-2018.jpeg){.wp-image-66678 .size-full width="720" height="779"} Foto: Contagio Radio\[/caption\]

### El anonimato del victimario ha ganado terreno

Otro de los resultados que arrojó el **CINEP** es que en la mayoría de los casos no ha sido posible establecer quiénes son los responsables de los hechos. Sin embargo los móviles "permiten deducir que se trata de casos perpetrados con motivaciones políticas de acuerdo con el marco conceptual de la Red Nacional de Bancos de Datos".

Al respecto, el **padre Javier Giraldo, investigador del CINEP** expresó su preocupación frente a la no identificación de los autores materiales e intelectuales de los ataques contra líderes sociales y comunales, "estamos viendo una metamorfosis, las estructuras paramilitares se van transformando, volviéndose anónimas,  conservando su metodología de opresión, sin embargo ya no reivindican un nombre y el crimen queda en el completo anonimato con una imposibilidad absoluta de que sean investigados".

### CINEP revela que aumentan ataques contra líderes e integrantes de JAC

Asímismo las víctimas han cambiado de identidad, según el padre Giraldo, en el pasado los ataques eran dirigidos con regularidad a militantes de fuerzas políticas alternativas, mientras en la actualidad y tal como resalta **Guillermo Cardona, de la Confederación Nacional de Acción Comunal**, "ya no importa la militancia política, atacan a los líderes que ejercen derechos constitucionales en sus comunidades o veeduría contra la corrupción, a quienes defienden el territorio o el agua", refiriendose en particular a los integrantes de las más de **64.000 JAC** que existen en el país.

\[caption id="attachment\_66676" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CINEP-RESPONSABLES-2018-1024x641.jpeg){.wp-image-66676 .size-large width="1024" height="641"} Foto: Contagio Radio\[/caption\]

### **La lucha por la tierra** 

El líder comunal también indica que una de las razones por la que se atacan a estos tipo de representaciones, es por el trabajo que llevan a cabo en torno a la defensa e implementación de la sustitución de cultivos de uso ilícito, "existe un interés, no por la sustitución sino por desplazar a los campesinos del territorio para entregárselo a las grandes multinacionales". [(Le puede interesar: Agresiones contra defensores de DD.HH. aumentaron más de 40% en 2018)](https://archivo.contagioradio.com/en-2018-se-presentaron-mas-de-dos-agresiones-diarias-contra-defensores-de-dd-hh/)

El **director del CINEP, Luis Guillermo Guerrero se refirió igualmente al problema histórico de tierras en el país, asegurando que** la concentración de hectáreas está relacionada con el desplazamiento asegurando que Colombia, según el indicador de desigualdad del coeficiente de Gini, es el segundo país más inequitativo de América Latina después de Honduras, revelando que es un problema no resuelto por el Acuerdo de Paz y que ha quedado opacado por otros temas de interés para el Gobierno como el narcotráfico.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
