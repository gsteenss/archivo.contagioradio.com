Title: Prohibir el consumo de sustancias en espacios publicos no solucionará nada
Date: 2019-09-17 18:18
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: consumo, Corte Constitucional, Proyecto de Ley, Sustancias psicoactivas
Slug: ley-contra-el-consumo-de-sustancias-en-espacios-publicos-no-solucionara-nada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Rodrigo-Lara-y-las-sustancias-psicoactivas-e1568757904746.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Palestra  
] 

Recientemente el **Senado  de la República aprobó el Proyecto de Ley que le daría facultades a los mandatarios locales para restringir el consumo de sustancias psicoactivas en espacios públicos.** El Proyecto pasará a conciliación entre las cámaras del Congreso, sin embargo, diferentes voces lo han criticado por considerarlo contrario a decisiones que ha tomado la Corte Constitucional sobre el tema, y por continuar con un enfoque prohibicionista para tratar el tema de consumos que no ha dado resultados.

> Por esta época electoral algunos candidatos mienten para ganar votos [\#NoComaCuento](https://twitter.com/hashtag/NoComaCuento?src=hash&ref_src=twsrc%5Etfw) con el tema de drogas. En 60 años de guerra contra las drogas el consumo no ha dejado de existir. Un mundo libre de drogas NO es posible, necesitamos un mundo que conviva en paz con las drogas. [pic.twitter.com/GCGOUsivbz](https://t.co/GCGOUsivbz)
>
> — Échele Cabeza (@echelecabeza) [September 17, 2019](https://twitter.com/echelecabeza/status/1173960980436574208?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Las implicaciones de insistir en el enfoque prohibicionista**

Marlon Lizarazo, sociólogo especialista en atención integral y prevención del consumo de sustancias psicoactivas, analizó el Proyecto de Ley, y afirmó que una consecuencia visible sería la afectación al comercio, pues lugares de venta de alcohol que están cerca a espacios públicos tendrían que trasladarse y ver afectadas sus ganancias. Adicionalmente, cuestionó el enfoque prohibicionista que tiene la iniciativa, desconociendo la importancia de la pedagogía e información que se debe dar sobre el consumo de sustancias psicoactivas.

En ese sentido, Lizarazo profundizó señalando que no es fácil hablar de consumos problemáticos de estas sustancias ilegales (como marihuana, cocaína u otras) si **cuando los jóvenes regresan a casa hay dinámicas de consumos igualmente problemáticos con sustancias psicoactivas legales** (como alcohol o cigarrillo). En consecuencia, la medida estaría "tapando el problema", y no buscando soluciones de fondo.

### **¿Qué hay de las decisiones de la Corte Constitucional?** 

Lizarazo aseguró que el Proyecto choca definitivamente con la sentencia C-221 de la Corte Constitucional sobre la despenalización del consumo de la dosis personal, por tal razón, es probable que de convertirse en Ley, se presenten tutelas contra el mismo[.][[ No obstante, el experto resaltó que se puede presentar un problema más grande que los choques contra la Corte: **la puesta en marcha, o aplicación de lo reglamentado en la Ley.**]  
]

Apropósito de este tema, Julian Quintero, director de Échele Cabeza, había advertido la complejidad de implementar el decreto del gobierno Duque que permitía la incautación de la dosis personal en espacios públicos. En su momento, Quintero afirmó que el decreto podría prestarse para abusos de autoridad contra consumidores de sustancias. (Le puede interesar: ["«Decisión de C. Constitucional sobre consumo de sustancias en espacios públicos fue sensata»"](https://archivo.contagioradio.com/decision-constitucional-consumo-de-sustancias-sensata/))

Por su parte, Lizarazo dijo que el problema era de corrupción en la cultura colombiana, pues este delito tiene permeadas instituciones como la Policía, lo que en la práctica se traduciría en posibles intentos de soborno de los uniformados a consumidores que sean encontrados en espacios públicos. Según el sociólogo, esto "empuja a los jóvenes a las ollas", haciendo que el expendio se concentre en determinados lugares, o se especialice de manera virtual.

### **Se está generando lo mismo de siempre: Buscar favorabilidad ante la opinión pública** 

Para concluir, Lizarazo declaró que la Ley no va a solucionar nada, y casi que es una pérdida de tiempo porque el expendio de sustancias ilícitas no se verá afectado. En cambio, Rodrigo Lara (autor del Proyecto), sí está buscando ganar favorabilidad ante la opinión pública**, haciendo que la "gente sienta una clase de seguridad en cuanto no vea el problema, pero el problema va a estar ahí latente".**

Lo que se debería hacer, en visión del académico, sería abordar el problema desde una perspectiva de salúd pública y salud mental; realizar un estudio que determine las características sociales y culturales que posibilitan los consumos problemáticos y generar ofertas que permitan a los jóvenes tener posibilidades distintas a consumir este tipo de sustancias. (Le puede interesar: ["Desmitificando la Marihuana en Colombia"](https://archivo.contagioradio.com/desmitificando-la-marihuana-en-colombia/))

<iframe id="audio_41721391" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41721391_4_1.html?c1=ff6600"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
