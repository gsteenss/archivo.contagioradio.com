Title: Información sobre salida de menores de las FARC-EP debe ser "corroborada y contextualizada"
Date: 2017-01-26 18:10
Category: Entrevistas, Paz
Tags: acuerdos, FARC-EP, implementación acuerdos de paz, Menores de edad
Slug: informacion-sobre-salida-de-menores-de-las-farc-ep-debe-ser-corroborada-y-contextualizada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ninos-en-las-farc-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo -  Contagio Radio] 

###### [26 Ene 2017] 

En el marco de la polémica por la salida de menores de edad de las FARC-EP, las cuatro organizaciones que hacen parte del proceso de veeduría al desarrollo de las medidas iniciales establecidas por la mesa de conversaciones, **señalaron serias debilidades, incumplimientos y desfases en los procedimientos que han afectado los derechos de los niños que han sido entregados por la guerrilla**

El comunicado publicado este 26 de enero, señala que hay **debilidades en el proceso de salida y el lugar transitorio de acogida,** también resalta que **hay falta de información clara y suficiente a los menores sobre su proceso de salida y reincorporación “que ha generado” incertidumbre sobre su futuro inmediato.**

Además señalan que ha “habido improvisación técnica y logística para una adecuada recepción y atención” de los 13 menores entregados en una primera fase.

Otra de las preocupaciones que indica problemas estructurales, es que **se ha aplicado la normatividad e institucionalidad ordinaria y no el marco normativo especial** esperado en el proceso de paz. Además denuncian que se excluyó el enfoque comunitario establecido en el acuerdo y señalado en el punto 3.2.2.5 del acuerdo final. Le puede interesar: ["Se acabo la guerra para los menores de las FARC-EP"](https://archivo.contagioradio.com/se-acabo-la-guerra-para-menores-de-las-farc-ep/)

Un último elemento con el que se evalúa el proceso es que **no existe el programa especial de reincorporación para menores de edad establecido en el acuerdo final** para acompañar y hacer seguimiento a los proceso de integración familiar, comunitaria y social, situación que se agrava cuando ya termino el proceso de salida de los 13 primeros menores entregados. Le puede interesar: "[Voces de paz rechaza propuesta para detener debates sobre JEP"](https://archivo.contagioradio.com/voces-de-paz-rechaza-propuesta-para-detener-debates-sobre-jep/)

Las organizaciones que hacen parte de esta veeduría también hacen 6recomendaciones que podrían mejorar la situación actual y salvar la problemática que se está presentando, entre ellas re **definir de manera urgente el protocolo de salida y entrega, elaborar, aprobar e implementar el plan transitorio y el programa de reincorporación al cual ya se hicieron unos aportes en julio de 2016 y enero de 2017.**

De igual forma recomendaron **agilizar el proceso de ajustes normativos para la implementación del programa** así como su enfoque comunitario y producir comunicados conjuntos que informen a la opinión pública acerca de los avances de ese mecanismo y de la salida de menores para facilitar la comprensión y el seguimiento al proceso.

Por último señalan que estas recomendaciones deben ser atendidas a la mayor brevedad y para que el país tenga una lectura ponderada y equilibrada del tema, **llamaron la atención a medios de información, periodistas, políticos y generadores de opinión** para que se difunda información corroborada y contextualizada con el respeto y dignidad que se merecen los niños y las niñas de Colombia.

[Comunicado Niños y Niñas Farc- Veeduria 26012017.PDF](https://www.scribd.com/document/337701465/Comunicado-Ninos-y-Ninas-Farc-Veeduria-26012017-PDF#from_embed "View Comunicado Niños y Niñas Farc- Veeduria 26012017.PDF on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_91422" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/337701465/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pWINifpEHb53BG4qMeA9&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
