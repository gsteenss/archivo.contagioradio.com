Title: Ruta de Reconciliación inicia para demostrar que «la reconciliación sí es posible»
Date: 2020-11-13 18:15
Author: AdminContagio
Category: Actualidad, Nacional
Tags: DIPAZ, Reconciliación, Ruta de reconciliación
Slug: ruta-de-reconciliacion-inicia-para-demostrar-que-la-reconciliacion-si-es-posible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ruta-de-reconciliacion.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Dipaz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 6 de noviembre se dio inicio a la **Ruta de la Reconciliación** con el evento, “Voces de fe y reconciliación desde los territorios” en Santander de Quilichao, departamento del Cauca. La ruta tiene como objetivo, mostrar que **«la reconciliación sí es posible».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según los integrantes del Diálogo Intereclesial por la Paz -DiPaz-, esta es una apuesta que busca visibilizar y fortalecer las experiencias que diversas iglesias y organizaciones basadas en la fe junto a victimas afectadas por el conflicto armado, vienen desarrollando en pro de la reconciliación.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DiPazOficial/status/1325095353201422337","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DiPazOficial/status/1325095353201422337

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Durante este primer encuentro, integrantes de la Nueva Área de Reincorporación de Miranda alzaron sus voces y sellaron un compromiso, para seguir caminando por la reconciliación en la región a pesar de las múltiples violencias que hoy en día enfrenta el país. (Le puede interesar: [JEP hace entrega de restos de personas desaparecidas en Dabeiba](https://archivo.contagioradio.com/jep-hace-entrega-restos-de-personas-desaparecidas-en-dabeiba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, l**os y las participantes de este espacio tuvieron la oportunidad de plasmar sus esperanzas, sueños y alegrías a través de un símbolo** «en el que encontraremos lo que toda esta realidad de guerra y violencia significa, pero también hallaremos los caminos para reconstruir lo fragmentado».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este primer evento, estuvieron presentes la Misión de Verificación de la ONU, Reinciportacion Miranda, iglesias de Toribío y Jambaló, el Concejo Nacional de Paz y las organizaciones que hacen parte del Diálogo Intereclesial por la Paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MisionONUCol/status/1326283855612022785","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MisionONUCol/status/1326283855612022785

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

Continuación de la Ruta de Reconciliación
-----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La [ruta](http://dipazcolombia.org/ruta-de-la-reconciliacion-dipaz/), que contará con diez eventos más, recorrerá regiones del Cauca, Guajira, Antioquia, Choco y Bogotá entre noviembre y diciembre, tiempo en el que se encontraran firmantes del acuerdo de paz en proceso de reincorporación, personas privadas de la libertad, jóvenes y mujeres que se han comprometido con el camino de la reconciliación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su objetivo común será **dar un mensaje de esperanza a la sociedad y un llamado a quienes se mantienen en la lógica de violencia para iniciar un diálogo humanitario.** (Le puede interesar: [DiPaz presenta a la CEV informe que revela afectaciones del conflicto armado a expresiones de fe](https://archivo.contagioradio.com/informe-de-dipaz-a-la-cev-revela-afectaciones-del-conflicto-armado-a-expresiones-de-fe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera continuará la ruta en otras regiones para terminar en Bogotá, en un encuentro con personas privadas de la libertad que han tenido la oportunidad de reconciliarse. Sin embargo, Gloria señala que allí no termina ese proceso si no que «desde Dipaz continuaremos fortaleciendo y visibilizando las diversas iniciativas que promueve la reconciliación, el encuentro el compromiso, el deber de la memoria y la construcción de la paz».

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
