Title: Masacre en Suárez, Cauca deja seis personas muertas
Date: 2017-12-11 12:14
Category: DDHH, Nacional
Tags: Cauca, dicidencias, FARC, Suárz
Slug: masacre-en-suarez-cauca-deja-seis-personas-muertas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/suarez-e1513012368962.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: disfrutandosudamerica.blogspot.] 

###### [9 Dic 2017] 

Este fin de semana, en Cerro Tijeras, municipio de Suárez, norte del Cauca seis personas fueron asesinadas en una vía de la zona rural, entre las veredas Agua Bonita, La Cabaña y El Naranjal. De acuerdo con la información de la ACIN,** se trataría de una masacre de la cual aún se desconocen los autores.**

El Tejido de Defensa de la Vida de Cxhab Wala Kiwe - ACIN señala que la autoridad de Cerro Tijeras y la guardia indígena kiwe thegnas investigan los hechos. Hasta el momento se conoce que las personas muertas fueron atacadas** en el punto conocido Los Tres Puentes entre Altamira y Los Robles donde se registra enfrentamiento entre disidencias de las Farc y un grupo que se hace llamar del EPL.**

Asimismo afirman que debido a los hostigamientos y a las confrontaciones en la zona, la comunidad se vio confinada y posteriormente se vio obligada a desplazarse a Sitio de Asamblea Permanente.

La situación sigue siendo crítica en la zona, pues, según la denuncia, los armados permanecen presentes y no ha sido posible suministrar la ayuda humanitaria, por lo que han hecho el llamado a todos los organismos de del gobierno, de Derechos Humanos y a la comunidad internacional para estar alerta y acompañar a la comunidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
