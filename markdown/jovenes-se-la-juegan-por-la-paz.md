Title: Jóvenes se la juegan por la paz
Date: 2016-08-30 14:21
Category: yoreporto
Tags: futbol, paz, Plebiscito
Slug: jovenes-se-la-juegan-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/futbol-comunidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Gabriel Galindo 

#### [Por: Agencia de Comunicaciones Prensa Alternativa Cauca - Yo Reporto ] 

###### 30 Ago  2016 

[En el barrio Bello Horizonte jóvenes integrantes de la organización Juventud rebelde, desarrollaron un evento deportivo y artístico por la paz de Colombia en el marco de la campaña "Que el único disparo sea gol".]

[En medio de un torneo deportivo, diversos jóvenes de la capital caucana abrieron este importante escenario que le apuesta todo al SÍ en el plebiscito por la paz, así mismo construyeron un mural que tiene como objetivo la democracia juvenil en cada espacio público e institucional.]

[Para este grupo de jóvenes la paz debe ser el motor que rija la sociedad colombiana. Por ello, durante meses han estado presentes en diferentes barrios y municipios del departamento para dar a conocer los acuerdos de la habana y ahora, inician la campaña por el SÍ.]

[Junto a un sin número de organizaciones juveniles, estudiantiles, de comunicaciones, entre otras, vienen trabajando porque los caucanos apoyen de manera directa e incondicional, lo firmado en la Habana.]

[Adriana Tulande, miembro de la Juventud Rebelde plantea que "estamos en un momento histórico. Nosotros los jóvenes somos una generación que podrá crecer en una Colombia donde la confrontación armada no sea el pan de cada día. Hoy, mediante todo tipo de acciones culturales, deportivas, políticas, etc, venimos llamando a la comunidad para que vote por el SÍ".]

[Entre las organizaciones sociales del departamento, es un consenso apoyar desde todos los ámbitos la paz de Colombia. Es así, que para el próximo 2 de Octubre, se espera una victoria contundente en el Cauca por el SÍ y que este triunfo significa, un paso más para derrotar a los amigos de la guerra, aquellos que se lucran con la vida del pueblo.]

######  

 
