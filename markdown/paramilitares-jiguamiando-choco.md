Title: Paramilitares irrumpen en Zona Humanitaria Nueva Esperanza en Jiguamiandó, Chocó
Date: 2017-07-07 22:39
Category: DDHH, Nacional
Tags: AGC, Jiguamiandó, paramilitares, Zona humanitaria
Slug: paramilitares-jiguamiando-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/zona-humanitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunicadores CONPAZ] 

###### [7 Jul 2017]

Los y las habitantes de la Zona Humanitaria Nueva Esperanza en Jiguamiandó denunciaron que hacia las 7 de la mañana dos personas, a bordo de una motocicleta de alto cilindraje entraron en el terreno delimitado como Zona Humanitaria y a través de pintas en algunas de las paredes de las casas anunciaron que las Autodefensas Gaitanistas llegaron “para quedarse”.

Según la información entregada por uno de los comunicadores comunitarios y confirmada por la organización de Justicia y Paz, que tiene presencia permanente en la región, los paramilitares, uno afrodescendiente y otro mestizo arribaron a la comunidad y sin mediar palabra procedieron a realizar graffitis con frases como **“AGC llegaron para quedarse”, algunas de ellas en los carteles de delimitación del lugar exclusico de población civil.**

Según la organización defensora de Derechos Humanos y varios reportes de las comunidades, desde hace varios meses se ha venido denunciando la libre movilidad de las estructuras paramilitares en los territorios colectivos de Jiguamiandó y Curvaradó sin que exista una respuesta eficaz por parte del Estado y de las fuerzas militares que mantienen fuerte presencia en la región.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
