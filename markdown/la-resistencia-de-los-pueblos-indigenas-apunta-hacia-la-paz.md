Title: La resistencia de los pueblos indígenas apunta hacia la paz
Date: 2016-10-12 05:00
Category: DDHH, Movilización
Tags: 12 de octubre, colombia, comunidades indígenas, día de la resistencia indígena, paz
Slug: la-resistencia-de-los-pueblos-indigenas-apunta-hacia-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/dia-de-la-raza-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

###### 12 Oct 2016 

A partir de 2005, cada 12 de octubre se celebra el **Día de la resistencia indígena**, con el propósito de dar viraje al legado tradicional eurocéntrico (colonial), que conmemora en esta fecha el “descubrimiento” y conquista de América en 1492; honrando en su lugar la **histórica lucha de los pueblos y comunidades indígenas por la reivindicación de sus derechos y autonomía territorial**.

De acuerdo con el más reciente informe presentado por la Organización Nacional Indígena de Colombia, ONIC, en la actualidad **106 pueblos indígenas habitan en el territorio colombiano, un 89% en zonas de bosques, mientras que el 10% lo hace en ecosistemas de vital importancia como son los páramos**.

A lo largo de la historia colombiana y debido a los diferentes fenómenos que han sucedido en el país como lo son el narcotráfico, el paramilitarismo, las guerrillas y el conflicto armado, los diferentes pueblos y comunidades indígenas han sido víctimas de la violencia sistemática ejercida por los diversos actores.

A esto se suman las p**roblemáticas generadas por las diferentes transnacionales** que, en aras de favorecer sus intereses económicos y en la mayoría de las ocasiones con la complacencia del Estado, **violan los derechos territoriales y culturales de los pueblos indígenas, establecidos a partir de la constitución de 1991 y reconocidos por la Comisión Interamericana de los Derechos Humanos**.

Para el año 2013, **más de mil indígenas habían sido asesinados y miles fueron víctimas constantes de reclutamientos, desapariciones forzadas, expropiaciones y apropiación de sus tierras**, entre otros.

Según datos de la Agencia de la ONU para los Refugiados ANCUR, **en 2016 más de 400 mil indígenas, equivalentes al 27% de la población, carecen de tierra y aproximadamente 70 mil han sido desplazados**. A pesar de tal situación, el Movimiento Nacional Indígena viene demostrando al país su capacidad de lucha y resistencia; materializados por las comunidades y pueblos originarios tras un arduo proceso de unidad y organización en la **gran movilización de 2013 para la reivindicación de los derechos en la Minga Social Indígena y Popular**.

Según el Consejero y Secretario General de la ONIC, Juvenal Arrieta, algunos de los logros principales de dicha movilización fueron la creación del **decreto 1953 de 2014 en el que se establecieron 14 resguardos indígenas protegidos por la autonomía política y administrativa de las comunidades**, como también lo fue el d**ecreto 2333 en el que se establece la protección de los territorios ancestrales**.

En el marco de los diálogos de paz entre el Gobierno Nacional y las FARC-EP, en 2014, **el Movimiento Indígena Colombiano** bajo los principios de territorio, unidad, cultura y autonomía **se movilizó en la llamada Minga Por La Paz**, dejando constancia de su **pleno y público respaldo a las conversaciones de la Habana** y su rol y compromiso político por la construcción de paz.

Algunos de los resultados más significativos de la Minga, fueron la **construcción de una Agenda Nacional de Paz de los Pueblos Indígenas de Colombia en diciembre del 2014**, y la cr**eación de la Comisión Étnica para la Paz y la Defensa de los Derechos Territoriales**, que promoviera un enfoque étnico y cultural en la implementación de los acuerdos de paz.

Para el 2016 **La Minga Nacional Agraria, Campesina, Étnica y popular** constituyó un nuevo reto para el movimiento indígena, puesto que se trataba de la unificación de fuerzas mediante las alianzas con otros sectores sociales para la construcción de un país de paz, como lo manifestó Luis Fernando Arias Consejero Mayor de la ONIC, en el marco del IX Congreso Nacional de Pueblos Indígenas de esa organización.

Para los pueblos y comunidades, el compromiso con la paz sigue vigente como lo han manifestado en la propuesta de **implementar en sus territorios del acuerdo para el fin del conflicto entre el Gobierno Nacional y las FARC-EP,** reiterando en un comunicado posterior a las elecciones del plebiscito del 2 de octubre, **la importancia sustancial del apoyo al SI por parte de las poblaciones más afectadas por la guerra**.
