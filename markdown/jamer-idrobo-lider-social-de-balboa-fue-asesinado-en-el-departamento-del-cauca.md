Title: Jamer Idrobo, líder social de Balboa, fue asesinado en el departamento del Cauca
Date: 2018-06-27 11:59
Category: DDHH, Nacional
Tags: Cauca, COCCAM, Lider social
Slug: jamer-idrobo-lider-social-de-balboa-fue-asesinado-en-el-departamento-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/COCA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio] 

###### [27 Jun 2018] 

Jamer Idrobo líder social y ex concejal del municipio de Balboa, en el departamento de Cauca, y miembro activo del Comité de Cocaleros de la vereda Joaquina, f**ue asesinado el pasado martes 26 de junio en el sur del Cauca**. Según lo denunció la red de derechos humanos Francisco Isaías Cifuentes.

De acuerdo con la información recolectada, los hechos se presentaron cuando hombres armados interceptaron la ruta que frecuentemente tomaba el líder, entre el municipio de Balboa y una vereda cerca al Bordo Patía, **en donde su cuerpo fue encontrado con dos impactos de bala.**

Hasta el momento, no se conocía ninguna situación de amenaza o riesgo en contra de la vida de Idrobo. Sin embargo, de acuerdo con Deivin Hurtado, integrante de la red de derechos humanos Francisco Isaías Cifuentes “**es una situación frecuente que lo líderes asesinados que vengan realizando trabajos con la comunidad sin recibir amenazas, sean asesinados”,** hecho que evita se activen las alarmas para la protección de la vida de los mismos.

El líder social impulsaba y hacía pedagogía en diferentes comunidades sobre la sustitución de cultivos de uso ilícito. En ese sentido, Hurtado manifestó que diferentes grupos armados están haciendo presencia en algunos departamentos del país, con la finalidad de controlarlos y manejar las rutas del narcotráfico. Razón por la cual los líderes que **“hacen conciencia de la necesidad de acabar con los cultivos ilícitos” terminan siendo víctimas.**

De igual forma las comunidades han denunciado que las estructuras armadas que están actuando en el territorio no se identifican o portan brazaletes de diversos grupos, pero se presentan como integrantes de otros como el ELN o EPL.

**Las denuncias sobre el accionar de la Fuerza Pública**

Frente a las acciones que hace la Fuerza Pública en el territorio para evitar que las comunidades, los líderes y defensores de derechos humanos sean víctimas de estos grupos armados, Deivin Hurtado aseguró que, si bien es cierto que hay una alta presencia de Ejército en el territorio, no hay una respuesta militar frente a disminuir los riegos contra la población civil.

Asimismo, Hurtado expresó que “se han presentado diferentes situaciones en el Patía que las comunidades han decidido **no hacer públicas por el miedo a que haya una presencia más militar del Estado y que en vez de que se generen soluciones, aumente el peligro”**. (Le puede interesar: "[Asesinan a Héctor Anteliz, líder social en Norte de Santander"](https://archivo.contagioradio.com/asesinan-a-hector-anteliz-lider-social-en-norte-de-santander/))

<iframe id="audio_26765268" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26765268_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
