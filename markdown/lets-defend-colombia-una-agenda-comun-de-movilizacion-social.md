Title: Let's Defend Colombia «una Agenda Común de Movilización Social»
Date: 2020-10-13 22:34
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Campaña Lets defend Colombia
Slug: lets-defend-colombia-una-agenda-comun-de-movilizacion-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/lets-defend-colombia.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/comunicado-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @letsdefendcol

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 5 de octubre,[la campaña Let’s Defend Colombia](https://letsdefendcolombia.xyz/2020/10/05/comunicado-de-lets-defend-colombia/), **envió a través de su página web y redes sociales una invitación para establecer «un diálogo de agendas para la construcción de una Agenda Común de Movilización Social**». ([Le puede interesar: Avanza la Minga en defensa de la vida](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/letsdefendcol/status/1315769608406798339","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/letsdefendcol/status/1315769608406798339

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Let's Defend Colombia, realiza esta propuesta, reiterando su compromiso, apoyo y respaldo a las acciones que se vienen realizando a nivel nacional e internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Let's Defend Colombia agrega que el «"Diálogo de Agendas" nos permitirá unir todos nuestros esfuerzos de manera incluyente» y defender la vida, la paz, los derechos humanos y la democracia en Colombia. **Además busca, desde la diversidad, poder manejar los mismos focos temáticos para lograr mayor visibilidad e incidencia en Colombia y el exterior. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las propuestas temáticas para la agenda común están la defensa de la vida y de la paz, defensa de los territorios y de los derechos de la comunidad LGBTI+ y de las minorías étnicas, indígenas, afrocolombianas, raizales y romaníes, reforma estructural inmediata de la fuerza pública y la implementación de los acuerdos de paz.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91437,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/comunicado-1-749x1024.jpg){.wp-image-91437}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Una semana después de presentar la invitación, **anunciaron que se han recibido 36 adhesiones de movimientos, plataformas, colectivos y grupos de Colombia y otros países.** (Le puede interesar: [Avanza campaña LDC mundial para proteger la democracia en Colombia](https://archivo.contagioradio.com/twitteraton-mundial-para-proteger-la-democracia-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, en el marco del inicio de la Minga Indígena del sur occidente del país este 10 de octubre informaron que se inició una alianza acompañando a la Minga e invitando a quienes desean unirse a esta lucha «a compartir esta iniciativa, a utilizar los Hashtags, a volver viral este gesto de unión por la defensa de la Paz, la Vida, los Territorios, la Democracia, la Justicia, nuestros Derechos!».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hashtags que se pueden utilizar son: \#YoTambiénSoyMinga \#IamMingaToo \#IchBinAuchMinga \#AncheIoSonoMinga \#JeSuisAussiMinga \#EuTambémSouMinga \#ΕιμαιΚαιΕγωΜινγα \#ИАзСъмМинга

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
