Title: El cambio climático se frena con voto consciente y movilización social: Susana Muhamad
Date: 2015-09-16 15:12
Category: Ambiente, Educación, Entrevistas, Movilización
Tags: cambio climatico, Cerros Orientales de Bogotá, cumbre de cambio climático Bogotá 2015, Día sin carro, Encuentro de las Américas Frente al Cambio Climático, Humedales, Humedales de Bogotá, Susana Muhamad
Slug: el-cambio-climatico-se-frena-con-voto-consciente-y-movilizacion-social-susana-muhamad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ciclovia_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Bogotá.gov.co 

###### [16 sep 2015]

<iframe src="http://www.ivoox.com/player_ek_8420753_2_1.html?data=mZmfkpyZd46ZmKiakp2Jd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fxMbaxM7Tb8TgytKSpZiJhZLoysjcjdjJb8fmxtPOjcjTsozq0NncjcjTstTXysrb1sqPvYzh0Nuah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El **encuentro de las Américas frente al cambio climático** que se realizará en Bogotá es de vital importancia para que la ciudadanía defina y exija las alternativas y acciones que se van a tomar no solo en Colombia, sino en Bogotá para mitigar el calentamiento global, afirma **Susana Mohamad, Secretaria Distrital de Ambiente de Bogotá.**

Para Susana Muhamad,  "la única garantía de frenar el cambio climático es que la ciudadanía se movilice" y exijan a los políticos que el Plan Nacional de Desarrollo y el Plan de Ordenamiento Territorial contemplen la continuidad de programas que hoy se llevan a cabo.

**Día sin carro, recicladores como partícipes del servicio de aseo, uso de la bicicleta, continuidad del programa “Basura cero”, protección de los cerros orientales y de los humedales**, son algunos de los temas que deben ser priorizados en estos programas y que a la fecha han sido negados y criticados más por intereses políticos, que por su aporte a mitigar el calentamiento global.

Muhamad también indicó que otra de las estrategias y temas que se va a tratar en este encuentro es el de las comunidades, principalmente las más pobres ya que “***los efectos de cambio climático, se van a sentir principalmente entre estas comunidades***” y Bogotá debe asumir su responsabilidad con sus habitantes.

Recordemos que este encuentro se realizará del 20 al 23 de septiembre en la ciudad de Bogotá y tendrá más de 30 eventos de discusión y actividades en donde toda la ciudadanía está invitada a participar contando sus experiencias y propuestas para enfrentar el cambio climático. [Vea Agenda del Encuentro contra el cambio climático en Bogotá](https://archivo.contagioradio.com/bogota-teje-estrategias-para-enfrentar-y-frenar-el-cambio-climatico/).
