Title: Nuevas amenazas contra líderes de víctimas y defensores de la mujer
Date: 2017-06-29 17:15
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas, lideres sociales, paramilitares
Slug: amenazas-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/amenazas-a-mujeres-lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

##### 29 Jun 2017 

En un panfleto firmado bajo el nombre de Aguilas Negras Bloque Capital, se conocieron nuevas **amenazas contra integrantes de movimientos sociales**, entre quienes se encuentra la ex congresista e integrante de Marcha patriótica **Piedad Córdoba**.

En el texto se menciona con nombre propio a 13 personas y organizaciones como las **Mesas Nacional y Distrital de Víctimas**, Viva la Ciudadanía, y a quienes trabajan en defensa de los derechos de la mujer, reclamándoles por su labor con  fuertes calificativos despectivos. (Le puede interesar: [Paramilitares amenazan a víctimas colombianas exiliadas en España](https://archivo.contagioradio.com/amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana/))

Además de Córdoba, la amenaza esta dirigida contra **Antonio Madariaga**, director ejecutivo de la Corporación Viva la Ciudadanía y **Rosa Hernández** líder de víctimas en Córdoba, los otros líderes amenazados son **Mónica Duarte, Dolores Mojica, Virgelina Shala, Gloria Duarte, Luzmarina Camargo Mojica, Ángela Molina, Nini Johana, Luis Enrique Carranza, Luis Murillo y Marcos Codex**.

Desde el Movimiento Marcha patriótica se emitió un comunicado donde responsabiliza al Gobierno nacional por la seguridad de las personas mencionadas y exigió que se adelanten las investigaciones correspondientes, elevando una solicitud de apoyo a las organizaciones internacionales garantes de los derechos humanos.
