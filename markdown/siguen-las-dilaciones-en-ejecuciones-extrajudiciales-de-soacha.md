Title: Siguen las dilaciones en casos de ejecuciones extrajudiciales de Soacha
Date: 2016-10-05 16:12
Category: Otra Mirada
Tags: colombia, crímenes de estado, falsos positivos, madres de soacha
Slug: siguen-las-dilaciones-en-ejecuciones-extrajudiciales-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/madres_soacha-camila_ramirez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 5 Oct 2016 

Indignadas y costernadas se mostraron las **madres de Soacha** tras un nuevo aplazamiento de la audiencia que debía realizarse el 4 de octubre, en la que se iba dar sentido al fallo en contra de **21 militares de la brigada 15 pertenecientes al Batallón de Santander de Ocaña**, implicados en la desaparición y asesinato de varios jóvenes del municipio cundinamarqués.

Las 5 mujeres reaccionaron con evidente molestia por l**a cantidad de dilaciones que se han presentado**, donde **solo en 3 de los 19 procesos adelantados los implicados han recibido condenas** y  manifestaron su inconformidad frente a la propuesta presentada por el Senador y expresidente **Álvaro Uribe Véle**z, en la que se busca otorgar **beneficios judiciales a militares procesados y condenados** por delitos en el marco del conflicto armado. (Lea también [¿Cómo es posible que Alvaro Uribe nos siga revictimizando?](https://archivo.contagioradio.com/si-alvaro-uribe-va-a-hablar-de-nuestros-hijos-que-se-lave-bien-la-boca-maria-sanabria/))

Uno de los casos en que se ha dictado sentencia fue el del joven **Leonardo Porras Bernal**, asesinado en 2008 a manos de la fuerza pública, por el cual se han dictado sentencias de **53 y 54 años de cárcel**. No obstante, lo sucedido el día martes con las 5 madres de Soacha demuestra de nuevo la poca voluntad en condenar de manera ágil estos casos. (Lea también: [Madres de Soacha enfrentan nuevo golpe tras 8 años de impunidad](https://archivo.contagioradio.com/madres-de-soacha-enfrentan-nuevo-golpe-tras-8-anos-de-impunidad/))

De acuerdo con informes del Centro de Investigación y Educación Popular (CINEP), un total de **5.265 ejecuciones extrajudiciales fueron cometidas en Colombia entre los años 2002 y 2010**. En 2015, el Centro Nacional de Memoria Histórica registraba un porcentaje de **impunidad del 95% en los casos de ejecuciones extrajudiciales en el pais**.

##### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
