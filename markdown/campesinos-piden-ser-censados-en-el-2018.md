Title: Campesinos piden reconocimiento de su labor en el censo poblacional
Date: 2017-11-23 16:43
Author: AdminContagio
Category: Nacional, Resistencias
Tags: campesinos, Censo 2018, tutela
Slug: campesinos-piden-ser-censados-en-el-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesinos-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [23 Nov 2017] 

Diferentes organizaciones campesinas interpusieron una tutela que busca proteger los derechos fundamentales del campesinado, exigiendo que estos sean contados como un grupo de personas en el censo que se realizará el próximo año, de esta manera se tendría un registro sobre el estado del campesinado en **términos de salud, educación, vivienda y un número aproximado de campesinos en Colombia.**

Fueron más de 1.100 campesinos los que le hicieron esta solicitud al DANE y al Ministerio del Interior y piden que se tenga un conjunto de preguntas específicas sobre su situación económica, social y su identidad cultural y de esta forma **que se desarrollen políticas públicas que incluyan a los campesinos**. (Le puede interesar: ["Oficina de la ONU en Colombia condenó asesinato de campesinos en Tumaco"](https://archivo.contagioradio.com/oficina-de-la-onu-en-colombia-condena-asesinato-de-campesinos-en-tumaco/))

Para Yaneth Silva, lideresa de la zona de reserva campesina de la Perla Amazónica, ser campesino implica un reconocimiento político y cultural “**somos una parte de Colombia, una población vulnerable y los que prácticamente producimos la alimentación para todo el país, es un reconocimiento**”.

Además, señaló que ese reconocimiento también les permitiría a los campesinos tener una figura política para mandatar y organizar las zonas de reserva campesina y adquirir el valor que hay en la actividad de labrar la tierra, que para Silva debe tener la misma importancia y garantías laborales como lo tienen las otras profesiones del país.

<iframe id="audio_22331797" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22331797_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
