Title: "El Último Bombardeo"
Date: 2016-05-26 17:26
Category: Nacional, Paz
Tags: Bombardeo, proceso de paz, Universidad Javeriana
Slug: este-viernes-sera-el-ultimo-bombardeo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/El-último-bombardeo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 26 May 2016

Como una nueva iniciativa ciudadana para respaldar la paz de Colombia y los procesos de negociación que se llevan a cabo con las guerrillas, **este viernes a las 5 de la tarde en las Escaleras de la Universidad Javeriana** se llevará a cabo “El último bombardeo”.

La invitación es para que los ciudadanos bombardeen a Colombia con alegría, paz y bombas. De acuerdo con una de las promotoras del evento, se trata de una campaña “para empezar a resignifcar las referencias nefastas de la guerra, las bombas, los fusiles, los enfrentamientos. **Lo que queremos es ir ambientando esa posibilidad maravillosa que se abre con el eventual cese bilateral de los acuerdos y la firma del proceso de paz**”.

Es así, como a partir de un juego con globos se busca comenzar a visibilizar un país distinto en el que los bombardeos sean de bombas de colores y las guerras sean de almohadas, como la anterior actividad que se realizó el pasado jueves en la Plazoleta de la Universidad del Rosario.

Para quienes no están en Bogotá, este tipo de iniciativas ciudadanas se adelantarán también en ciudades de Colombia como  Villavicencio, Neiva, Cali, Tunja, Tuluá, Armenia y en otros lugares del mundo como Barcelona.

<iframe src="http://co.ivoox.com/es/player_ej_11690757_2_1.html?data=kpajm5WbeZihhpywj5WbaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncabgjIqwlYqmhc3oytLcjcfTscPV08nS0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
