Title: El primer paso para desmontar el ESMAD es modificar la doctrina de seguridad
Date: 2019-12-16 18:30
Author: CtgAdm
Category: Movilización, Política
Tags: audiencia pública, Desmonte, ESMAD, protesta
Slug: el-primer-paso-para-desmontar-el-esmad-es-modificar-la-doctrina-de-seguridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/22218572054_533c6637d0_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lozanoand] 

Este lunes se adelantó en Bogotá la audiencia pública "¿Qué hacer con el ESMAD?", en la que académicos, líderes políticos e integrantes de organizaciones sociales discutieron sobre qué se puede hacer frente al evidenciado abuso de autoridad que ejerce el Escuadrón Móvil Antidisturbios (ESMAD). En el encuentro, los asistentes estuvieron de acuerdo en la necesidad de desmontar esta institución, así como la doctrina de seguridad que la rige, y permite el tratamiento violento a la protesta.

El evento fue citado por el senador Antonio Sanguino, e inició sobre las 8:30 de la mañana en un auditorio del edificio nuevo del Congreso. En primer lugar, los asistentes escucharon la exposición de Temblores ONG, la organización que presentó un informe reciente sobre los 34 asesinatos que se han registrado por acción del ESMAD en sus 20 años de existencia. (Le puede interesar: ["En veinte años el ESMAD ha asesinado a 34 personas"](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

Aunque el informe documentó los casos de agresión más graves que condujeron a la muerte de los manifestantes, en el evento también se hicieron presentes víctimas directas del Escuadrón, que contaron cómo la agresión de sus armas 'menos letales' había llegado a afectar su visión, movilidad y su vida. Posteriormente, líderes políticos como el senador Iván Cepeda, la ex alcaldesa Clara López o la representante a la cámara María José Pizarro tuvieron la palabra, para hacer una evaluación sobre las acciones de este cuerpo de Policía.

### **"Nosotros necesitamos una discusión sobre la doctrina"** 

En su intervención, el senador Cepeda afirmó que era necesario revisar la doctrina de seguridad porque la misma "parte del enemigo interno, es decir, que en términos de lo que se habla ahora, las personas que protestan son blancos legítimos", lo que conlleva que sean tratados como si fueran actores del conflicto armado. Adicionalmente, el senador dijo que se podría decir lo mismo de los métodos de represión que usa el ESMAD, porque están hechos para confrontar las movilizaciones como si las mismas se dieran en el contexto de una guerra.

Por ambas situaciones, Cepeda aseguró que "esta no es una discusión de cualquier orden, nosotros no hablamos de quitar una institución y reemplazarla por otra", en su lugar, se requiere desmontar el ESMAD, discutir sobre la doctrina de seguridad, "sobre la inteligencia que usan policías y militares, sobre la forma en que actúa la fuerza pública" y sobre la desmilitarización de la sociedad colombiana.

### **¿Qué viene después de desmontar el ESMAD?** 

Luego de las intervenciones de los congresistas, se esperaba la participación de algunas instituciones como la Defensoría del Pueblo, el Ministerio de Interior, Ministerio de Defensa y la Policía; no obstante, solo la Defensoría se hizo presente en la Audiencia pese a que todas las entidades habían confirmado su participación. Tras la intervención de dos delegados de la Defensoría se realizó un panel en el que estuvieron presentes integrantes de organizaciones sociales como Dejusticia, la Fundación Comité Solidaridad con los Presos Políticos y la Unión Nacional de Estudiantes de Educación Superior (UNEES).

Desde estas organizaciones, plantearon que el ESMAD se convirtió en la 'oficina de trámites' donde se atiende el descontento social, lo que cuestiona la atención que tiene el Gobierno hacia los reclamos de los ciudadanos. Adicionalmente, Cristian Moreno, integrante de Dejusticia, sostuvo que antes de preguntarse qué hacer con el Escuadrón, se debe preguntar cómo hacer para que la institución que exista cumpla los protocolos de actuación que ya existen, "porque son relativamente buenos".

En este sentido, propuso cinco reflexiones para orientar el debate, y hacer que la institución que se cree para actuar en el marco de la protesta social garantice este derecho, y lo proteja adecuadamente. En su opinión, se debe conocer la normatividad, los protocolos de actuación de la Policía  y las líneas de mando, para poder controlar sus actuaciones; saber quién envía a los uniformados a atender la protesta, para hacer un control político; revisar la normatividad sobre el uso de las armas para que estas cuiden la integridad de los manifestantes; abrir el debate público sobre el tipo de armamento que se usa, "porque es una discusión que solo la ha abordado la Policía y la empresa que los fabrica"; y por último, supervisar que los agentes porten las armas permitidas en las movilizaciones.

### **"Proyecto de Ley por el cual se ordena el desmonte del Escuadrón Móvil Antidisturbios"** 

Durante la audiencia pública, el senador Sanguino recordó que en la próxima legislatura presentará el Proyecto de Ley "Por el cual se ordena el desmonte del Escuadrón Móvil Antidisturbios", con el que se eliminará esta Unidad, "con el fin de eliminar el uso excesivo de la fuerza, la represión de la protesta social y la violación de los derechos humanos. De igual forma, el Proyecto buscará que las entidades locales implementen acciones de convivencia que garanticen el derecho a la protesta.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
