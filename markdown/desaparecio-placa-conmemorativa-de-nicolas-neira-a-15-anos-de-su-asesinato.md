Title: Desapareció placa conmemorativa de Nicolas Neira a 15 años de su asesinato
Date: 2020-05-01 17:18
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #ESMAD, #MOVICE, #NicolasNeira, #Primerodemayo, #YuryNeira
Slug: desaparecio-placa-conmemorativa-de-nicolas-neira-a-15-anos-de-su-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Nicolas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Yury Neira, Padre de Nicolas Neira, asesinado hace quince años por un integrante del Esucadrón Móvil Antidisturbios, denuncia que la placa conmemorativa en homenaje a la vida su hijo fue retirada en las últimas horas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el Movimiento de Víctimas de Crímenes de Estado, este hecho es repudiable y aseguran que esta acción es parte de un intento de **"borrar la memoria en el país".** (Le puede interesar: "[Nicolás Neira: Persistencia ante la impunidad](https://www.justiciaypazcolombia.com/nicolas-neira-persistencia-ante-la-impunidad/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta placa se había puesto desde hace 10 años, en la calle 7 con 18. Lugar en donde el primero de mayo del 2005 Nicolas fue agredido brutalmente por un miembro del ESMAD, cuando se movilizaba en defensa de los derechos laborales.

<!-- /wp:paragraph -->

<!-- wp:heading -->

A quince años del asesinato de Nicolas Neira, continúa la impunidad
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Yury Neira afirma que el retiro de esta placa, justo el día en el que se conmemora el asesinato de su hijo, hace parte del manto de impunidad que se ha tejido sobre este caso. (Le puede interesar: ["Así avanza la (in)justicia en el caso de Nicolás Neira"](https://archivo.contagioradio.com/asi-avanza-la-injusticia-en-el-caso-de-nicolas-neira/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Quince años tenía Nicolas cuando fue asesinado por la Policía Nacional. Hoy se cumplen quince años de su asesinato. Quince años en total impunidad **orquestada por la Policía Nacional y apoyada por la Fiscalía General de la Nación**". Expresa Yury.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 25 de octubre de 2019 la Fiscalía llamó a juicio a Fabián Mauricio Infante Pinzón ex director del ESMAD. Ello debido a que según dos testigos, Infante habría ordenado encubrir el homicidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, este caso ha tenido distintas dilaciones y actos de violencia en contra de Yury Neira. Razón por la cual asegura que espera que no tengan que pasar otros **quince años más para tener justicia sobre el asesinato de su hijo.** [Caso NIcolás Niera y su desarrollo hacia nuevos crímenes y más impunidad](https://archivo.contagioradio.com/caso-de-nicolas-neira-abre-la-puerta-para-esclarecer-crimenes-del-esmad/)

<!-- /wp:paragraph -->
