Title: Inicia en Bogotá el Encuentro de Mujeres Constructoras de Paz y Ciudadanía
Date: 2016-12-02 13:35
Category: Mujer, Nacional
Tags: Acuerdos de paz en Colombia, Defensa de derechos humanos, mujeres constructoras de paz, violencias de género
Slug: inicia-en-bogota-el-encuentro-de-mujeres-constructoras-de-paz-y-ciudadania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Mujeres-marchan-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [2 Dic 2016] 

Para discutir y construir alrededor del tema de las **violencias de género y las desigualdades entre mujeres y hombres en el ámbito político, económico, cultural y social**, del 2 al 3 de Diciembre se llevará a cabo en Bogotá el Encuentro Nacional e internacional Mujeres Constructoras de Paz y Ciudadanía.

El evento cuenta con la participación de la ex senadora y defensora de Derechos Humanos Piedad Córdoba, Blanca Eekhout, Ministra de la Mujer e igualdad de Género de Venezuela, la brasileña Rita Segato Directora del grupo de investigación Antropología y Derechos humanos e Isabel de Guevara Directora General del Movimiento Salvadoreño.

Esta iniciativa promovida por la organización Colombianas y Colombianos por la Paz y Poder Ciudadano, tiene como objetivo **construir de forma colectiva un panorama sobre las violencias de género y en especial la persecución política contra las mujeres** en América Latina y el Caribe.

Otro de los temas transversales al encuentro será el logro del **enfoque de género en los Acuerdos de Paz entre el gobierno colombiano y las FARC**, pues es un importante referente a nivel latinoamericano y global sobre inclusión de estos temas en un acuerdo para la terminación del conflicto. Le puede interesar: [Acuerdo de paz puede ser inicio para reparar mujeres en Colombia.](https://archivo.contagioradio.com/acuerdo-de-paz-inicio-para-reparar-mujeres/)

Otras panelistas como Yidis Medina, Gloria Cuartas, Gloria Flórez, Aida Quilcue y Marisol Perilla, estarán presentes para hablar sobre algunos **casos emblemáticos de persecución política y violencias de género en el marco del conflicto y los diálogos de Paz.**

###### Reciba toda la información de Contagio Radio en [[su correo]
