Title: 9 personas han sido asesinadas en Arauca tras fin de cese al fuego con ELN
Date: 2018-01-24 13:17
Category: DDHH, Nacional
Tags: Arauca, asesinato de líderes, Cese al fuego
Slug: tras-fin-de-cese-al-fuego-han-sido-asesinadas-a-9-personas-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/asesiantos-lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Ene 2018] 

La Asociación Nacional Campesina José Antonio Galán Zorro denuncio el **incremento de homicidios de líderes sociales** y campesinos en el departamento de Arauca. El 22 de enero fue asesinado Harley Johanny Mogollón, dirigente de dicha organización. Con su muerte se completan 9 asesinatos luego de que culminó cese al fuego entre el Gobierno Nacional y el ELN el pasado 9 de Enero.

Carlos Núñez, representante de ASONALC, indicó que “en el Departamento de Arauca vienen ocurriendo muchos asesinatos **después de que se terminó el cese al fuego**”. Aseguró que de las nueve personas que han sido asesinadas desde entonces, cinco han sido cometidos por la Fuerza Pública.

### **Las denuncias contra la fuerza pública** 

De esas cinco víctimas, **dos fueron indígenas presentados como guerrilleros** por parte de las Fuerzas del Estado. Otro caso se presentó cuando “mataron a una muchacha en la Y de Tame en una instalación militar” y un joven también fue asesinado por la Policía. A estos hechos se suma el asesinato más reciente del joven de 30 años, (Le puede interesar: ["ONIC denuncia dos ejecuciones extrajudiciales en Tame, Arauca"](https://archivo.contagioradio.com/onic_ejecuciones_extrajudiciales_tame_arauca/))

Debido a este caso, la Asociación emitió un comunicado manifestando preocupación por la situación que están viviendo las comunidades en Colombia “ya que **son estas las que están pagando las consecuencias** de los problemas sociales, políticos y económicos del país”. Por esto, le exigieron a las autoridades y a los actores armados que se pronuncien sobre el asesinato más reciente que ha puesto en riesgo a las comunidades de Arauca.

### **Asesinato de Harley Mogollón no fue un delito común** 

El líder social fue asesinado en las horas de la mañana del 22 de enero cuando se transportaba en moto con su esposa y cuñada. De acuerdo con Núñez, **Mogollón fue abordado por varios sujetos** que “sin mediar palabra le dispararon”. Por esto, descartaron que se trate de un asesinato común “porque no le robaron la moto ni nada, sino que simplemente le quitaron la vida”. (Le puede interesar: ["Denuncian que ejército habría asesinado campesino en Tame, Arauca"](https://archivo.contagioradio.com/tame-ejercito-asesinato/))

Por este hecho le han exigido a las autoridades que se hagan las investigaciones pertinentes **“para ver qué fue lo que sucedió”**. Indicaron que es necesario que los actores del conflicto se pronuncien ante una situación que es confusa para las comunidades y los campesinos. Alertaron por la preocupación de la comunidad ente la ausencia estatal y la presencia de diferentes grupos armados.

### **Asesinatos de líderes debilitan el tejido social** 

Nuñez informó que las regiones están confundidas debido a las dinámicas que han ocurrido como el Acuerdo de Paz con las FARC, la negociación con el ELN en donde ya se terminó el cese al fuego y **la continuación de grupos armados**. Dijo que "es necesario que se clarifique lo que está pasando porque los líderes sociales están en riesgo".

Además, afirmó que el Estado no ha brindado las garantías necesarias para la libre asociación que **"son el motor que hace mover un municipio"**. Enfatizó que son las organizaciones y las agremiaciones las que comunican al Estado las necesidades y los problemas por lo que las comunidades deben también trabajar por el fortalecimiento de las actividades sociales.

<iframe id="audio_23344050" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23344050_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
