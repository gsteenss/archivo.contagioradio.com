Title: Nuevos realizadores a participar en el Festival de Cine por los DDHH
Date: 2018-06-29 11:52
Author: AdminContagio
Category: 24 Cuadros
Tags: Festival de Cine por los Derechos Humanos
Slug: nuevos-realizadores-festival-cine-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Festival 

###### 29 Jun 2018

En su 5ta versión, el Festival Internacional de Cine por los Derechos Humanos invita a participar en la **convocatoria para nuevos realizadores**, modalidad que busca difundir, impulsar e incentivar la realización de trabajos de video con enfoque en derechos humanos, particularmente s**obre el impacto y la labor de iniciativas que desde el arte protejan derechos**.

Estudiantes, aficionados y productores pueden participar con un video a manera de teaser ficción, documental, filminuto, entrevista, animación, de **entre 1 y tres minutos de duración**, donde se identifique y documente una iniciativa individual o colectiva que, a través del arte, aporte a la reivindicación de derechos, transformación positiva de contextos y/o al fortalecimiento del Estado de Derecho desde acciones concretas.

Para esta nueva edición, el tema que deberán abordar los concursantes de la categoría Nuevos Realizadores será **“Expresiones artísticas en Latinoamérica para la protección de los derechos humanos”**.

**Cómo participar**

![Festival DDHH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/unnamed-1-600x648.jpg){.alignnone .size-medium .wp-image-54378 .aligncenter width="600" height="648"}

En esta categoría se premiará al realizador con la financiación en 2019, de la producción de un documental sobre la iniciativa escogida y la iniciativa obtendrá una pieza audiovisual donde se evidencie la labor y el impacto de transformación. Así mismo, se contemplarán los alcances y necesidades para poder apoyarla en nuevos escenarios de incidencia, y poder compartir su experiencia en otros países de la región latinoamericana.

El plazo de cierre de la convocatoria es **hasta el 27 de julio de 2018** a la media noche hora  de Colombia[**,** compartimos l]as [bases](http://cineporlosderechoshumanos.co/wp-content/uploads/2018/06/Reglamento-Nuevos-Realizadores-2018-FINAL.pdf) y condiciones de participación y el [formato de inscripción](https://goo.gl/forms/CBLsOBJGrR5QoYcn1) .
