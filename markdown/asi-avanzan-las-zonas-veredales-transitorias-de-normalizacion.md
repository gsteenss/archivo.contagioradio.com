Title: Así avanzan las Zonas Veredales Transitorias de Normalización
Date: 2017-01-10 13:22
Category: Nacional, Paz
Tags: Dejación de armas, FARC, Implementación de Acuerdos, Zonas Veredales Transitorias de Normalización
Slug: asi-avanzan-las-zonas-veredales-transitorias-de-normalizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/zona-veredal-farc-e1484067202705.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [10 Enero 2017] 

Frente a las denuncias hechas por las FARC sobre los retrasos en la construcción de las Zonas Veredales Transitorias de Normalización –ZVTN–, **el mal estado de las sedes del mecanismo de monitoreo y el envío de alimentación en estado de descomposición**, el Gobierno aseguró que para finales de Febrero estarán resueltas las problemáticas.

Rodrigo Londoño Echeverry manifestó que hasta la fecha ni el Gobierno Nacional ni la ONU han dado respuesta a la misiva en la que se denuncia que **“la alimentación enviada a los puntos de preagrupamiento no cumple los mínimos de salubridad** (…) los productos perecederos llegan en estado de descomposición porque los abastecedores no garantizan la cadena de frío, indispensable para su conservación".

Además, en la carta se explica que **ya se han presentado casos de problemas de salud relacionados con la ingesta de estos alimentos,** por lo que “decidimos no recibirlos más hasta tanto no se dé cumplimiento estricto al protocolo” puntualiza Londoño.

Por otra parte, el Alto Comisionado para la Paz Sergio Jaramillo, anunció durante una rueda de prensa que de las 20 Zonas Veredales Transitorias de Normalización y los 7 Puntos Transitorios de Normalización, **sólo 8 ZVTN están construidas en un 80% e integrantes de las FARC las habitan, 17 ya cuentan con predios arrendados y otras 9 ya tienen levantamiento topográfico** y maquinaria para trabajar durante esta semana.

Jaramillo reiteró que “lo acordado es cumplir con el cronograma de 180 días, pero que es posible que las zonas sigan activas después para continuar con la reincorporación que se hará cuando ya todas las armas estén en manos de la ONU”.

### **Avanza la Ley de Amnistía** 

El Alto Comisionado señaló que alrededor de **130 guerrilleros, condenados por el delito de rebelión, ya fueron indultados por parte del Gobierno**, aunque “las conexidades más complejas tendrán que ser revisadas por la Sala de Amnistía de la Jurisdicción Especial para la Paz”.

“Los indultos se han venido aplicando en las últimas semanas (…) y **hay otros 250 o 270 miembros de esta organización que son candidatos posibles a recibir también este beneficio”.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
