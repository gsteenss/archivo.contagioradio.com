Title: Así fueron las visitas de verificación en las zonas veredales desde las comunidades
Date: 2016-08-11 16:24
Category: Nacional, Paz
Tags: Diálogos de paz en Colombia, verificación zonas de concentración, zonas de concentración
Slug: las-visitas-de-verificacion-en-las-zonas-veredales-desde-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Curvaradó___.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: CONPAZ] 

###### [11 Ago 2016] 

"Aquí cesó la horrible noche y el bien germinará ya", afirma Jimy Rodríguez, miembro de la 'Asociación de familias de los Consejos Comunitarios de las cuencas de los ríos Curvaradó, Jiguamiandó, Pedeguita y Mancilla, La Larga Tumaradó y Vigía de Curvaradó', en el Bajo Atrato chocoano, a propósito de las visitas técnicas a las zonas veredales que iniciaron este martes, por parte de delegados del Gobierno y la guerrilla de las FARC-EP, y de observadores de la Misión de Naciones Unidas en Colombia, los países garantes y del Comité Internacional de la Cruz Roja.

Rodríguez asegura que las comunidades indígenas, mestizas y afro que recibieron a los delegados en el corregimiento de Brisas, se sienten **alegres por poder por fin respirar en un ambiente paz** y agrega que las familias le insistieron a las delegaciones en que esperan que en el marco de la implementación de los acuerdos se generen las condiciones en materia de seguridad, participación y derechos humanos, teniendo en cuenta el control paramilitar en la región y las [[dificultades que han tenido para la elección democrática de su representante legal](https://archivo.contagioradio.com/mininterior-impide-participacion-de-14-comunidades-en-eleccion-del-representante-legal/)].

[![Curvaradó\_\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Curvaradó__.jpg){.aligncenter .size-full .wp-image-27774 width="888" height="500"}](https://archivo.contagioradio.com/las-visitas-de-verificacion-en-las-zonas-veredales-desde-las-comunidades/curvarado__/)

En la vereda El Gallo del municipio de Tierra Alta, Córdoba las comunidades aseguran que se sienten muy contentas con la visita y con las declaraciones que dio el alcalde quien afirmó que aportará todo lo necesario para la implementación de los acuerdos. Según el poblador Luis Carlos Herrera teniendo en cuenta que en la vereda se dan las condiciones, [[se están preparando para que todos se sientan a gusto](https://archivo.contagioradio.com/asi-implementan-la-pedagogia-de-paz-las-comunidades-en-tierra-alta/)], y pese a que a muchas personas les parece imposible vivir en paz, **tienen la esperanza de que las cosas cambien y se den todas las garantías de cumplimiento** de lo pactado en La Habana.

Sin embargo, en el corregimiento de La Playa del municipio de Tumaco, Nariño, líderes del Consejo Comunitario como el señor Francisco Jácome aseguran que durante estos recorridos, los **delegados no han resuelto las dudas de las comunidades sobre las características e implicaciones de las zonas** de concentración para sus territorios y condiciones de vida, por lo que exigen al Gobierno reunirse con ellos directamente para resolver sus inquietudes.

<iframe src="http://co.ivoox.com/es/player_ej_12516583_2_1.html?data=kpeik5uZfJShhpywj5WcaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPQy9jHs4y-hqigh6aVp9DhxoqfpZCns8_nxs_cjajTsdbiytnO1M7Tb63VjLXZw97FcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
