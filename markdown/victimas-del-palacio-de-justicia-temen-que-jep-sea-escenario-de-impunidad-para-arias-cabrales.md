Title: Víctimas del Palacio de Justicia temen que JEP sea escenario de impunidad para Arias Cabrales
Date: 2020-05-18 21:12
Author: CtgAdm
Category: Nacional
Slug: victimas-del-palacio-de-justicia-temen-que-jep-sea-escenario-de-impunidad-para-arias-cabrales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0242.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Palacio de Justicia/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La Jurisdicción Especial para la Paz (JEP)** aceptó el sometimiento del general (r) Jesús Armando Arias Cabrales, condenado por los hechos de desaparición forzada durante la retoma del Palacio de Justicia. La decisión que contempla la libertad condicional, ha sido rechazada por los familiares de las víctimas quienes advierten que los aportes del militar a la justicia ordinaria han sido nulos y que podría seguir igual con su ingreso a la JEP.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Arias Cabrales, **quien fue condenado en septiembre de 2019 a 35 años de cárcel por la desaparición de 11 personas -** obtuvo el beneficio de libertad transitoria, condicionada y anticipada, aún cuando la Corte Interamericana prohibía que responsables de los hechos ingresaran a algún tipo de justicia especial. [(Le puede interesar: Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia)](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la JEP, el general (r) "deberá indicar cuál fue la suerte que corrieron las víctimas momentos previos a su desaparición" así como aportar ,información concreta que permita localizar los cuerpos de las víctimas por las que fue condenado, y de las demás personas desaparecidas de manera forzada como resultado del accionar del Ejército.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sandra Beltrán, familiar de Bernardo Beltrán, desaparecido del Palacio de Justicia, considera que la decisión tomada por la [JEP](https://twitter.com/JEP_Colombia) "es profundamente lamentable, pues abre las puertas a la impunidad no solo en el caso del Palacio de Justicia sino en muchos otros casos donde está involucrada la Fuerza Pública",

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta preocupación de los familiares de las víctimas se suman los procesos contra e**l coronel (r) Edilberto Sánchez Rubiano y el sargento (r) Bernardo Alfonso Garzón.** [(Lea también: ¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia)](https://archivo.contagioradio.com/existen-desaparecidos-l-palacio-de-justicia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque admite que el informe de la JEP deja claro algunos puntos que le permite a Árias Cabrales acogerse a la Justicia Especial, advierte que con esta decisión "se están obviando las sentencias a nivel nacional, internacional, perpetuando la impunidad" pues no se estaría teniendo en cuenta a las víctimas y se le habría aceptado sin que el general (r) haya aportado aún alguna verdad al caso. [(Le recomendamos leer: Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia)](https://archivo.contagioradio.com/fiscalia-desaparecidos-palacio-de-justicia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La verdad que debe Árias Cabrales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Sandra Beltrán, **Árias Cabrales, quien fue jefe de la Brigada 13 de inteligencia militar en el año 1985, es responsable directo de lo ocurrido y las decisiones tomadas que condujeron a la desaparición de varias personas** "él debe contarnos desde el momento en que sacan a nuestros familiares vivos y que nos cuente cuando fueron llevados al Cantón Norte, por qué los torturaron, por qué los asesinaron, por qué ese trasteo de cadáveres, eso sería un gran aporte para conocer algo de la realidad de nuestros familiares".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Si Árias Cabrales no dice dónde están los cuerpos, si no cuenta eso, no se haría nada, será una pantomima más del Estado colombiano" - Sandra Beltrán

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

"Tengo claro que a lo que Árias Cabrales le tiene miedo es a morir preso y no creo que vaya a aportar nada a la verdad", afirma Beltrán quien de paso señala que durante este proceso tampoco se ha vinculado a la Policía Nacional de alguna forma, en particular a la figura de personas como Óscar Naranjo, quien era capitán de la Policía en la toma y retoma del Palacio el 5 y 6 de noviembre de 1985 y quien fungía como encargado de la seguridad de los magistrados en ese entonces. [(Le puede interesar: 34 años persistentes en la búsqueda de las y los desaparecidos del Palacio de Justicia)](https://archivo.contagioradio.com/34-anos-persistentes-en-la-busqueda-de-las-y-los-desaparecidos-del-palacio-de-justicia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Avanza la Fiscalía en el caso del Palacio de Justicia ?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde octubre de 2019, la Fiscalía General de la Nación, en cabeza de dos de los investigadores encargados del caso, afirmó que allí no se habían presentado desapariciones forzadas, declaraciones que contradicen incluso sentencias judiciales de carácter internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto Beltrán advierte que incluso en medio de la pandemia del Covid-19 la Fiscalía adelantó durante marzo más exhumaciones que tuvieron que ser canceladas por la contigencia**, según ella la fiscalía quiere "seguir acomodando su versión y ubicar más cuerpos para ver la forma de cómo nos los entregan sin que sea evidente lo que ya está aprobado en el proceso".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A estos hechos se suma lo que ocurre el interior del Centro de Memoria Histórica, que bajo la dirección de Dario Acevedo ha buscado desconocer "la memoria histórica del país, las masacres y la violencia" vivida por las comunidades. [(Lea también: La ruptura de confianza entre la víctimas y el Centro Nacional de Memoria Histórica)](https://archivo.contagioradio.com/victimas-memoria-historica/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
