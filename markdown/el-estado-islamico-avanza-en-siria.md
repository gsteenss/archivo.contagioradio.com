Title: El Estado Islámico avanza en Siria
Date: 2015-02-05 19:42
Author: CtgAdm
Category: El mundo, Política
Tags: Estado Islámico, Lelila Nachawati, Siria
Slug: el-estado-islamico-avanza-en-siria
Status: published

###### Foto:zoomnews 

<iframe src="http://www.ivoox.com/player_ek_4043316_2_1.html?data=lZWhlZiVeo6ZmKiakpyJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYxsrQb6bn1cbR0ZCtt82ZpJiSo5bRrcTjjMrbjbjNtsrVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Leila Nachawati] 

Después  de la revolución **Siria** y la posterior guerra civil, **el país ha quedado inmerso y dividido en regiones controladas por el régimen de Al-Assad, grupos extremistas de carácter islámico como el ISIS y el Estado libre de Siria. **Es en este clima donde se gesta el inicio del ISIS, financiado por iniciativas privadas del propio país y del exterior.

**Leila Nachawati activista hispanosiria** y profesora de Comunicación Social, explica las causas y consecuencias que ha tenido **el poco apoyo internacional a la resistencia** contra el régimen de Al-Assad y que **ha propiciado el auge de grupos extremistas como el Estado Islámico.**

Por otro lado Nachawati** **ratifica la necesidad de que sean **los propios afectados quienes combatan dichos grupos extremistas**, ya que mediante los bombardeos no es posible acabar con ellos y el daño sobre la población civil  incurre en claras violaciones de los derechos humanos.

En este momento es el **Estado libre de Siria es quién está combatiendo el ISIS**, y que apenas está recibiendo ayuda internacional para ello.
