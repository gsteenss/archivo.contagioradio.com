Title: 'Paloma sin nido': cuando el dolor se convierte en poesía
Date: 2016-01-29 17:24
Category: eventos, Nacional
Tags: crímenes de estado, Luz Odilia León, Paloma sin nido, víctimas
Slug: luz-odilia-leon-paloma-sin-nido-cuando-el-dolor-se-convierte-en-poesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/PALOMA-SIN-NIDO-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portada del libro ] 

<iframe src="http://www.ivoox.com/player_ek_10252078_2_1.html?data=kpWfl5eUe5mhhpywj5WcaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5ynca3p25C8xs7QrcKfrcqSpZiJhpTijNXfx9jJstXVjNjijdXTqc7V087cjYqWdrHVzdTaw5DXrc%2Bfr87R0YqWdo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luz Odilia León] 

###### [29 Ene 2016] 

Este viernes Luz Odilia León, mujer víctima de [crímenes de Estado](https://archivo.contagioradio.com/?s=cr%C3%ADmenes+de+Estado), presenta su poemario ‘Paloma sin nido’, una composición literaria que narra una pequeña parte de su historia y la de su familia que fue **“destruida, casi hasta la tercera generación, por la guerra de nuestro país”** y cuya memoria busca “desenterrar” para “mostrar eventos concretos de la historia colombiana con la intención de que no se vuelvan a repetir, pues **ésta es una sociedad en la que se olvida fácil**”.

‘Paloma sin nido’ representa para Luz Odilia un acto de resistencia al olvido y **el logro de un sueño que tenía desde cuando era niña y que dedica a su hijo asesinado, a su esposo y a su madre**, una compilación de poemas que venía escribiendo desde hace cerca de 26 años “de acuerdo a cada momento, al contexto colombiano y a lo que he tenido que vivir”, con lo que busca aportar “a la **construcción de una sociedad respetuosa, justa, digna y en paz**”.

“Ojos grandes, negros, fruto de la selva y del amor fueron arrancados por la furia y la sevicia de un monstruo feroz (…) Ojos que siempre recuerdo, los llevo en el corazón” este fragmento corresponde a uno de los poemas que Luz Odilia presentará **este viernes en la Universidad Santo Tomas desde las 5 pm y el próximo jueves a la misma hora en el Centro de Memoria Paz y Reconciliación**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
