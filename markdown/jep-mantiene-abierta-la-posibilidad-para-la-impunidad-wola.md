Title: "JEP mantiene abierta la posibilidad para la impunidad": WOLA
Date: 2017-03-24 13:26
Category: Nacional, Paz
Tags: Cadena de Mando, FARC-EP, Jurisdicción Espacial de Paz, WOLA
Slug: jep-mantiene-abierta-la-posibilidad-para-la-impunidad-wola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [24 Mar 2017] 

La organización internacional defensora de derechos humanos WOLA, expresó su preocupación por las modificaciones que se hicieron a la Jurisdicción Especial para la Paz, que para ellos “**mantiene abierta la posibilidad para la impunidad **” y la posibilidad de que **ayuda militar que llega a Colombia, desde Estados Unidos pueda ser congelado** si no se da justicia en casos de crímenes cometidos por agentes del Estado.

Gimena Sánchez, coordinadora de WOLA para las Américas, expresó que han seguido de cerca el conflicto al interior de país y el proceso de paz, por ende, cuando se firmó el acuerdo consideraron que se había alcanzado un equilibrio importante para las víctimas y la justicia, que no se ve reflejado en lo reglamentado en la Jurisdicción Especial para la Paz. “**Hay un intento por renegociar lo que se pactó en el acuerdo, pensamos que eso daña el esfuerzo de implementar el acuerdo**” afirmó.

### **El concepto de cadena de mando se diluyo en la JEP** 

Respecto a la responsabilidad por la cadena de mando, la organización advierte que **“la definición de esta se diluye”** atentando contra los compromisos internacionales que tenga Colombia, en términos de derechos humanos, y que en consecuencia lleva a que existan dos categorías de víctimas las de las FARC y las del Estado **“si los crímenes de lesa humanidad son cometidos por Fuerzas Armadas, está bien, pero si son cometidos por grupos guerrilleros, está mal”**.

En la misiva advierten que principales líderes militares y comandantes podrían evitar la sentencia y dar información a la JEP, privando a las víctimas de la verdad, al argumentar que no tenían el **“control efectivo” y exponiéndolas a no ser reparadas, ni a encontrar a los responsables. **Le puede interesar: ["Si Estado no juzga a empresarios y altos mandos que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

Y que se juzgaría en estos casos 1. que las conductas punibles hayan sido cometidas dentro del área de responsabilidad asignada a la unidad bajo su mando; 2. que el superior tenga la capacidad legal y material de emitir, modificar y hacer cumplir órdenes, 3. que el superior tenga la capacidad efectiva de desarrollar y ejecutar operaciones dentro del área donde se cometieron los hechos punibles; y 4. que “el superior tenga la capacidad material y directa de tomar las medidas adecuadas para evitar o reprimir la conducta o las conductas punibles de sus subordinados, siempre y cuando haya de su parte conocimiento actual o actualizable de su comisión.”.

De igual forma evidencian que, pese a las recomendaciones que hizo la Corte Penal Internacional, **la ley termino siendo mucho más suave en el caso de los miembros de las Fuerzas Armadas**, en comparación de los integrantes de las FARC-EP. Le puede interesar:["Impunidad la mayor preocupación de organizaciones de DDHH con la JEP"](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/)

### **Responsabilidad de terceros será más difícil de juzgar en JEP** 

En cuanto a la responsabilidad de terceros en la financiación del conflicto armado, Sánchez señaló que “**los crímenes no suceden solamente por las personas que lo cometen, son crímenes que están dentro de una estructura más amplia**” y la JEP hace más difícil que civiles que ayudaron y fomentaron las operaciones del conflicto armado sean juzgados.

“Es un grave error para Colombia y avanzar en mejorar las instituciones, con sacar los vínculos entre grupos ilegales y civiles y es un gran error para la verdad, es muy importante entender como **fueron las dinámicas de estos crímenes si no se quieren repetir**” expresó Sánchez. Le puede interesar: ["Congresistas de la Unidad Nacional modificaron JEP afectando derechos de las víctimas"](https://archivo.contagioradio.com/congresistas-la-unidad-nacional-modificaron-la-jep-afectado-derechos-las-victimas/)

WOLA afirmó en su comunicado que “cree que estos obstáculos adicionales para juzgar a los civiles se agregaron no para proteger a los partidarios de la guerrilla, sino **para proteger a los partidarios de las milicias paramilitares"**.

### **Colombia no avanza en las garantías para las víctimas** 

Con este panorama, WOLA indicó que le preocupa la posición en la que quedan las víctimas, motivo por el cual insta a la **Corte Constitucional, que será la última instancia que aprobará la JEP**, a que recupere las garantías de los derechos de las víctimas y junto con ella, la verdad.

A su vez, Sánchez señalo que Estados Unidos también puede ejercer presión para alcanzar la justicia al interior de Colombia “**la ayuda militar que llega a Colombia todavía tiene un porcentaje que puede ser congelado** si no se da justicia en casos de crímenes cometidos por agentes del Estado”. Le puede interesar: "[Por décima vez aplazan audiencia de Falsos Positivos de Soacha"](https://archivo.contagioradio.com/por-decima-vez-aplazan-audiencia-por-falsos-positivos-de-soacha/)

###### Reciba toda la información de Contagio Radio en [[su correo]
