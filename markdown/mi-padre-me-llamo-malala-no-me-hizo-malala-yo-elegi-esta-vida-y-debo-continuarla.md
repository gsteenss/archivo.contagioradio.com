Title: "Mi padre me llamó Malala, no me hizo Malala. Yo elegí esta vida y debo continuarla"
Date: 2015-11-12 14:21
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: Davis Guggenheim, Documental Malala, He named me Malala, Malala Yousafzai, Pakistán
Slug: mi-padre-me-llamo-malala-no-me-hizo-malala-yo-elegi-esta-vida-y-debo-continuarla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/MALALA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [12 nov 2015]

Se estrena en salas de cine colombianas el Documental "El me llamó Malala", un retrato íntimo a la vida de la joven activista nacida en el en el Valle de Swat (Pakistán) Malala Yousafzai, y su lucha por los derechos infantiles a la educación, especialmente para las niñas de su país.

El director estadounidense Davis Guggenheim, ganador del Oscar por el aclamado documental sobre el calentamiento global "Una verdad incómoda" (2006), es el encargado de llevar a la pantalla la historia de la niña, perteneciente a la etnia pastún, que desafío el control talibán que desde 2009 prohibia a las mujeres asistir a la escuela.

[  
](https://archivo.contagioradio.com/mi-padre-me-llamo-malala-no-me-hizo-malala-yo-elegi-esta-vida-y-debo-continuarla/malala/) [![Malala](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/55eb0b4b52fe2.jpg){.wp-image-17138 .aligncenter width="561" height="263"}](https://archivo.contagioradio.com/mi-padre-me-llamo-malala-no-me-hizo-malala-yo-elegi-esta-vida-y-debo-continuarla/__55eb0b4b52fe2/)

El mundo conoció su nombre luego de sobrevivir al ataque que recibió en 2012 por parte de hombres armados que le dispararon en la cabeza ante su obstinada decisión de seguir asistiendo a clases, paradojicamente 2 años después, con mucha vitalidad, se convertiría en la persona más joven en recibir el Premio Nobel de Paz.

Con música de Thomas Newman y la fotografía de Erich Roland, el documental recopila algunas de las conferencias que la defensora de derechos humanos ha realizado alrededor del mundo en contra de la opresión a las mujeres y por la educación, llevando a cada lugar un mensaje de esperanza y de acción por el cambio. Además registra pasajes de la historia familiar de la joven, en particular con su padre, fuerte influencia en su activismo.

<iframe src="https://www.youtube.com/embed/Dk_PnIjJJmw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
