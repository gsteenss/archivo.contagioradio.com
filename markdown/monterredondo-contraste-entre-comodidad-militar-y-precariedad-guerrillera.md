Title: Monterredondo: Contraste entre comodidad militar y precariedad guerrillera
Date: 2017-02-08 16:21
Category: Nacional, Paz
Tags: Cauca, FARC, Mecanismo de Monitoreo, Zonas Veredales
Slug: monterredondo-contraste-entre-comodidad-militar-y-precariedad-guerrillera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité del Suroccidente de Acompañamiento] 

###### [8 Feb. 2017] 

Mientras que el puesto de mando militar, la sede del Mecanismo de Monitoreo y Verificación Local y de las unidades militares responsables de la seguridad en la Vereda Monterredondo se encuentran en óptimas condiciones, **los integrantes de las FARC tienen que seguir expuestos a condiciones de salubridad pésimas y no cuentan con sitios de alojamiento dignos.**

Así lo aseguró el **Comité del Suroccidente de Acompañamiento de la Sociedad Civil al Mecanismo de Monitoreo y Verificación en Popayán,** quienes pudieron constatar los más recientes hechos que están viviendo los integrantes de las FARC en su proceso de tránsito en el departamento del Cauca. Le puede interesar: [Paramilitares controlan hasta el transporte de las FARC a Zona Veredal](http://bit.ly/2kZkSXO)

Por ejemplo, en la **Zona Veredal ubicada en Monterredondo no hay condiciones de salud** y si bien existió una brigada, ésta no resolvió los problemas y necesidades de las personas en cuanto a tratamientos, procedimientos y medicamentos.

Otra situación preocupante es que **en las fotografías se puede observar que el campamento militar estaría ubicado junto al del Mecanismo de Monitoreo y Verificación** lo que estaría violando el acuerdo que establece que los militares deben estar a 1 km. Del campamento guerrillero y no en el mismo circulo de protección del mecanismo.

Como si esto no fuera suficiente, **se continúa sin garantizar el suministro de agua potable para las FARC,** ni para las comunidades y no se ha concertado un plan de contingencia. Le puede interesar: [Continúan incumplimientos del Gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/)

**En La Elvira**, municipio de Buenos Aires, la situación no cambia mucho, **las obras se encuentran en fase de adecuación de terreno**, lo que ha hecho que las FARC no puedan ingresar a la Zona Veredal.

Adicionalmente, **la Policía ha instalado un puesto en la comunidad La Ventura en medio de las viviendas lo que se traduce en la violación a la regla** que indica que no se “podrán afectar los derechos y libertades de la población”. Esta situación también se repite en la vereda La Venta en el municipio de Caldono, donde militares han llegado a ocupar predios de campesinos.

Desde que inició el tránsito hacia las Zonas Veredales por parte de las FARC todos los días se conocen noticias lamentables y preocupantes de las situaciones  a las que se han visto expuestos los integrantes de esta guerrilla, por lo que el Comité del Suroccidente de Acompañamiento ha manifestado su preocupación en el cumplimiento de lo pactado en los acuerdos. Le puede interesar: [Estos son los sitios de las Zonas de Concentración de integrantes de las FARC](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)

Así mismo, declararon que es preocupante que no se estén cumpliendo los protocolos en las áreas de seguridad “sobre todo donde habitan las comunidades” quienes manifiestan que **las unidades militares han ingresado a sus predios y a otros lugares de la población civil. **Le puede interesar:  [Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

Preocupa, dice el Comité, que las obras que se requieren para garantizar la permanencia en condiciones adecuadas de los integrantes de las FARC, sigan presentando retrasos “sobre todo teniendo en cuenta que **ya se encuentran la mayoría de sus integrantes en la zona, ubicados en sitios improvisados".**

Los integrantes de **las FARC** le han manifestado al Comité del Suroccidente de Acompañamiento que **continuarán con su compromiso de estar en dichos lugares pese a los incumplimientos. **Le puede interesar: [Mecanismo de Monitoreo pide celeridad en puesta en marcha de Zonas y Puntos Veredales](https://archivo.contagioradio.com/33429/)

El Comité del Suroccidente de Acompañamiento ha recomendado **al Gobierno cumplir con lo acordado, además de retirar las unidades policiales y militares ubicadas en medio de las viviendas.  **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
