Title: Mujeres y punk, ruídos que conspiran contra el silencio
Date: 2017-08-31 14:32
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, Punk
Slug: mujeres-punk-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/19510318_1150325661764551_1957589109140573959_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censuradas 

###### 31 Ago 2017 

Con el propósito de visibilizar las producciones artísticas de las mujeres en el punk desde la perspectiva de género, nace Censuradas, una serie documental producida por Andrea Barajas y dirigida por José Andrés Nieto.

Esta pieza audiovisual, concebida en septiembre del año pasado, es un proyecto auto gestionado que según su productora “busca mostrar la diversa participación de las mujeres en la escena punk”, pero que “está dirigida a todo tipo de público y es una apuesta incluyente”.

"Muchas veces las mujeres hemos estado en un rol de sumisión de obediencia en el que realmente escuchar que uno puede mandar todo a la mierda, y decir mierda y decir no me jodan es algo que muchas mujeres necesitan" asegura la productora sobre las motivaciones que les llevaron a combinar la música punk y el tema de género en una producción documental.

Censuradas Colombia, es la primera parte de lo que se espera sea una serie de cuatro cortos, en los que se recopilarán experiencias de mujeres en la escena punk alrededor del mundo. Este documental se estará presentando en varios festivales en los próximos meses y abrirá un crowdfunding con el fin de continuar las grabaciones y proceso de postproducción.

<iframe src="https://www.youtube.com/embed/kbKHbI1pZWM" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El estreno nacional se llevará a cabo el 5 de Octubre en el evento "Ruído y punk contra la censura, en las instalaciones de B-Bar  Cra 7 No 45-72 a las 7 de la noche, con la presentación en concierto de Rattus Rattus y Anecdotikos.

Puede encontrar más información del evento [aquí](https://www.facebook.com/events/1077653609005019/?acontext=%7B%22action_history%22%3A%22%5B%7B%5C%22surface%5C%22%3A%5C%22messaging%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22attachment%5C%22%2C%5C%22extra_data%5C%22%3A%7B%7D%7D%5D%22%7D) y encontrar más información sobre Censuradas [acá](https://www.facebook.com/censuradasdocumental/).

<iframe id="audio_20627939" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20627939_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
