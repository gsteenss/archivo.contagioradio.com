Title: Deforestación siguen aumentando en la Amazonía colombiana
Date: 2019-03-19 11:25
Author: ambiente y sociedad
Category: Ambiente, Nacional
Tags: Amazonía, Instituto de Hidrología Meteorología y Estudios Ambientale
Slug: deforestacion-siguen-aumentando-la-amazonia-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/amazonas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: miputumayo.com] 

###### [18 Mar 2019] 

Tal como se había previsto, la deforestación sigue incrementando en el país. Así lo confirmó un nuevo informe del Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM) publicado el pasado 16 de marzo que indica que la cantidad de detecciones tempranas de deforestación **aumentó en el último trimestre de 2018** (octubre a diciembre) en comparación con los trimestres anteriores del mismo año. Además, **el 75% de la tala de árboles se concentró en la Amazonía**, la región conocida como el pulmón del mundo.

"En el ámbito departamental, **Caquetá (45,9 %), Meta (13,1%) y Guaviare (9,8 %)** son los que concentran las mayores cantidades de detecciones de deforestación. El Análisis regional, generado en conjunto por el IDEAM, Corpoamazonía, la Corporación para el Desarrollo sostenible del Norte y Oriente Amazónico (CDA) y Cormacarena, identificó cerca de **43.000 hectáreas deforestadas en estos departamentos** durante el trimestre", registra el informe.

Para Mercedes Mejía Leudo, profesora de la Universidad de la Amazonía, **la situación ambiental en esta región ha llegado a un nivel de "catástrofe"**, que tiene implicaciones graves para los habitantes y ecosistemas locales, tanto como para el planeta. El deterioro de estos bosques de la Amazonía **contribuye al cambio climático y vulnera a miles de especies raras de flora y fauna**.

Por otro lado, Mejía indica que **la deforestación al afectar al clima e intervenir en biosferas, también tiene efectos nocivos para comunidades aledañas y hasta otras regiones**. "El país entero tiene que enterarse que la selva Amazónica es importantísima hasta para que se formen muchas de las lluvias que llegan a la Sabana de Bogotá. En la medida que haya deforestación acá esto va a llegar a afectar, en términos de salud, a la Sabana de Bogotá. **No se ha dimensionado los efectos que puede causar la deforestación"**, aseguró Mejía.

En su informe, el IDEAM señala que las causas principales del fenómeno son la explotación ilícita de minerales, los cultivos de uso ilícito, praderización, tala ilegal, conversión a cultivos agrícolas y incendios forestales; sin embargo, como denuncia la docente, **las "economías legales" tales como la industria minero-energético también generan daños ambientales,** los cuales solo han sido promovidos por el nuevo gobierno del presidente Duque. (Le puede interesar: "[El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)")

"**Dentro de la meta del Gobierno, está el extractivismo y el impacto será más deforestación desafortunadamente.** Todo el dinero que se gaste y todos los programas que se han creado para controlar la deforestación no pueden hacer mayor cosa porque esto necesita unas medidas de emergencia. Esto necesita la intervención del orden nacional", manifestó Mejía.

<iframe id="audio_33482273" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33482273_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
