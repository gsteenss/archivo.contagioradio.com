Title: Afros, indígenas y sus visiones de mundo hacen parte del FICCI
Date: 2019-03-08 15:00
Category: 24 Cuadros, Cultura
Tags: Cine, Cine afro, Cine indígena, Ficci
Slug: afros-indigenas-y-sus-visiones-de-mundo-hacen-parte-del-ficci
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Diseño-sin-título-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### :Foto:  MinCultura/Tikal Producciones 

###### 8 Mar 2019 

En su 59 edición, y coincidiendo con el año del bicentenario de la Batalla de Boyacá, el Festival de Cine de Cartagena ha incluido dos muestras que buscan reconocer y visibilizar el cine que producen los **pueblos afro e indígenas de Colombia.**

Son 18 títulos, repartidos por mitad en ambas selecciones, que **demuestran la relevancia de  las producciones realizadas desde las propias comunidades**, para narrar en voz propia sus tradiciones, usos y visiones de mundo desde una perspectiva distinta al intervencionismo cultural, que las ha interpretado desde una mirada externa y en muchas ocasiones sustentada en estereotipos arraigados en la cultura popular.

**Ramón Perea, de la Corporación Carabantu**, asegura que a pesar de la coincidencia histórica, las comunidades afro no le juegan a lo coyuntural, y lo que para la institucionalidad es una festividad, para ellos es una conmemoracion por las apuestas libertarias que han caracterizado a los pueblos afrodescendientes.

Para Perea, parte de esas apuestas, están relacionadas con la promoción de un sentido crítico, frente a los imaginarios y representaciones que desde el cine y la televisión se vienen construyendo sobre los afrocolombianos, "convertidos en clichés enquilosados en la culturar popular". De ahí que considere que el cine sirve como una herramienta que desde lo etnoeducativo pueda romper ese modelo y ayude a reafirmar parte de su identidad.

En lo que compete a los pueblos originarios, **Leiki Uriana, gestora cultural Wayúu y curadora de la muestra**, asegura que la denuncia sigue teniendo un papel fundamental para las producciones indígenas, como lo describe en sus palabras "mas que contar, es una manera de resistir, existir y visibilizar que existimos como pueblos" frente a un pais que históricamente los ha marginado. [(Le puede interesar: Cine en los barrios, el otro festival en el festival ) ](https://archivo.contagioradio.com/cine-en-los-barrios-el-otro-festival-en-el-festival/)

Ante la poca difusión que han tenido las producciones propias de ambas identidades étnicas en los medios masivos y eventos cinematográficos del país, tanto Uriana como Perea recordaron que **han acudido a crear sus propias plataformas de difusión desde las cuales, con mucho trabajo e insistencia**, haciendo contrapeso a la mirada colonialista que durante décadas se ha tenido desde el cine sobre ellos, buscando con esto aportar a la formación de audiencias capaces comprender desde sus complejidades las diferencias y particularidades de cada pueblo.

Algo en lo que coincide **Reyson Velásquez, quien ha trabajado desde el Chocó en el audiovisual,** añadiendo que se debe superar el síndrome de "historia única" entendiendo que la forma de conseguirlo es abriendo espacios que permitan quebrar la barrera mediática que se ha levantado, para que el cine cimarrón permita llevar su voz a diferentes espacios de Colombia y el mundo.

De esa forma, llegando a las masas, Velásquez afirma se puede lograr un empoderamiento de los territorios, enseñando sus practicas y expresiones y evitar caer en los estereotipos, a lo que Ramón Perea agrega, se puede hacer con la comprensión de la dimensión política  que tiene la gente negra, sobre la base de que **los relatos se pueden construir sobre el respeto.**

"Yo creo que todo artista, todo realizador tiene derecho a contar sus historias, no todo cineasta que viene con su película a una comunidad indígena pregunta que te parece, el reto es que nosotros nos empoderemos como indígenas, abrir las alas y empezar a masificar nuestras historias y poderlas mostrar en espacios como este" manifestó la gestora Wayuu a la vez que pidió a quienes realizan películas utilizando sus lenguas tengan un mayor cuidado y respeto porque son parte fundamental de su patrimonio.
