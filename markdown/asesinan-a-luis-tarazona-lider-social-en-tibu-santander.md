Title: Asesinan a Luis Tarazona, líder social en Tibú, Norte de Santander
Date: 2018-11-09 15:21
Author: AdminContagio
Category: DDHH, Nacional
Tags: Catatumbo, Lider social, Luis Tarazona, Norte Santander
Slug: asesinan-a-luis-tarazona-lider-social-en-tibu-santander
Status: published

###### [Foto: Twitter] 

###### [09 Nov 2018] 

La Asociación de Campesinos del Catatumbo denunció el asesinato de Luis Tarazona, líder social de la vereda Miramonte. Los hechos ocurrieron el pasado 8 de noviembre hacia las 9:00pm cuando se encontraba **en su hogar y fue atacado con varios impactos de bala**; en el ataque su compañera también fue víctima de disparos y se encuentra en estado crítico.

Según la denuncia, dos hombres en motocicleta llegaron hasta la fachada de la casa del líder social, allí le dispararon en varias ocasiones. De acuerdo con Luis Evelio Torres, vocero de ASCAMCAT, **ha 20 metros de la vivienda había un puesto militar y no se reportó ninguna reacción por parte del Ejército,** "uno dice, en dónde esta la garantía y para qué esta la Fuerza Pública de la región, si en la cara de ellos se cometen los hechos y no hacen nada" afirmó Torres. (Le puede interesar:["Asesinato de docentes en el norte del Cauca"](https://archivo.contagioradio.com/continuan-asesinatos-contra-docentes-al-norte-del-cauca/))

Tarazona había estado impulsando **la implementación de los Acuerdos de Paz, puntualmente el punto de sustitución de cultivos de uso ilícito** y era el responsable del comité veredal en Miramonte, Norte de Santander, también se desempeñaba como secretario de la Corporación Minera Agroindustrial en su comunidad.

Torres también aseguró que hasta el momento no se conocía ninguna amenaza sobre la vida de Tarazona y se desconocen los móviles detrás del asesinato. Asimismo la organización señaló que en lo corrido del año se han presentado más asesinatos de líderes en la región, sin que existan pronunciamientos o medidas para salvaguardar sus vidas.

<iframe id="audio_29954520" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29954520_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
