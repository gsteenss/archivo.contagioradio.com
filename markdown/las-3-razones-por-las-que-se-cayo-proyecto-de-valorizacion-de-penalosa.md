Title: Las 3 razones por las que se cayó proyecto de valorización de Peñalosa
Date: 2017-06-09 13:07
Category: Movilización, Nacional
Tags: Concejo de Bogotá, Enrique Peñalosa
Slug: las-3-razones-por-las-que-se-cayo-proyecto-de-valorizacion-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cb2cbcb05372f3c0e805aeed82be2db5_1475097756_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [09 Jun 2017] 

**La falta de estudios del proyecto de valorización, que no se presentara una fórmula que determinara que elementos serían tomados en cuenta para realizar el cobro y obras por cuantiosas cifras** que no eran prioritarias para la ciudad, fueron las razones para que el proyecto del alcalde Enrique Peñalosa fuera rechazado en el Concejo.

De acuerdo con el concejal Manuel Sarmiento del partido Polo Democrático, el proyecto “**era tan aberrante que hasta quienes hacen parte de la colación Peñalosista tenían muchas dudas** sobre la conveniencia del proyecto” y la legalidad del mismo. Le puede interesar:["Ya están listas las firmas para revocar a Peñalosa"](https://archivo.contagioradio.com/listas-las-firmas-para-revocar-a-penalosa/)

Una de las razones que han surgido para que Cambio Radical, parte de la coalición Peñalosista, no respaldara en el Concejo la aprobación del proyecto es porque para las elecciones 2018 este podría ser un costo político que le quitará votos a Germán Vargas Lleras, su candidato. Sobre este posible motivo Sarmiento expresó que **el proyecto era tan insensato que ni siquiera se podría defender ante las comunidades.**

Además, al igual que otros proyectos presentando por Peñalosa como el de la venta de la ETB, este tampoco contaba con los estudios suficientes para implementarlo.  Sarmiento señaló que al proyecto le faltaba un estudio sobre la capacidad de pago de los contribuyentes, **tampoco definía bien el cobro de la valorización porque la fórmula para realizar la recaudación no mencionaba muy bien que elementos iba a tener en cuenta** y la infraestructura en la que se invertiría el dinero recaudado no tenía prioridad.

Ejemplo de ello es la construcción de un centro cultural público de 10 pisos, que se ubicaría en una de las zonas más adineradas de la ciudad como lo es la **calle 82 con carrera 11 y tendría un costo de 80 mil millones de pesos**.

### **Las otras propuestas de Peñalosa** 

Se esperaba que, en el proyecto de valorización, el Alcalde incluyera la financiación de la troncal de Transmilenio por la carrera Séptima, sin embargo, esta adición presupuestal no se tuvo en cuenta, ya que Peñalosa aseguró que pedirá un **cupo de endeudamiento de 3 billones de pesos, que cubra el costo de las obras**.

Sin embargo, el concejal Manuel Sarmiento expresó que este proyecto podría generar más daños a la ciudad que beneficios “**el sistema Transmilenio genera un gran impacto urbano en las avenidas que son angostas**, un buen ejemplo de esto es la Caracas en donde se han deteriorado los entornos”.

De igual forma señaló que el sistema que permitiría descongestionar la ciudad sería la construcción del metro que superaría el **transporte de 80 mil pasajeros, que satisfaga la demanda y sin intervenir la carrera Séptima**.

El intento de la venta de la Empresa de Telefonía de Bogotá, se encuentra en este momento frenada debido a las medidas cautelares impuestas por un Juez, por falta de argumentos que respalden la veta de la empresa. Le puede interesar:["Juez ordena la suspensión temporal de la venta de la ETB"](https://archivo.contagioradio.com/juez-ordena-suspension-temporal-la-venta-la-etb/)

<iframe id="audio_19177754" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19177754_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
