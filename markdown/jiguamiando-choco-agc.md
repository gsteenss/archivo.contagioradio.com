Title: Denuncian ingreso de 250 hombres de las AGC a Jiguamiandó
Date: 2017-03-27 15:33
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, Chocó, colombia, jigaumiandó
Slug: jiguamiando-choco-agc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Autodefensas-Gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 27 Mar 2017 

Un informe presentado este lunes por la Comisión Intereclesial de Justicia y Paz, da cuenta del ingreso en las últimas horas **250 de integrantes de las autodenominadas "Autodefensas Gaitanistas de Colombia" al territorio colectivo de Jiguamiandó**, ubicado el el Bajo Atrato Chocoano.

De acuerdo con el documento emitido por la Organización de Derechos Humanos, los hombres **estarían ingresando a la zona desde el día domingo** por el punto conocido como "El Guamal", **pasando como sembradores, aserradores, barequeros**, ubicándose en Urada, Acandía, y Guamal.

Los hombres habrían ingresado al territorio promulgando que "llegaron para quedarse" y que había "llegado el progreso", premisas con las que vienen haciendo copamiento libre, en diferentes zonas como Pavarandó, Uradá y Mutatá, lugares que cuentan con presencia de fuerza policial o militar como denuncia la organización. Le puede interesar: [Nueva incursión de Autodefensas Gaitanistas a Truandó, Chocó](https://archivo.contagioradio.com/nueva-incursion-autodefensas-gaitanistas-truando-choco/).

En la información suministrada, se asegura que la estrategia de los miembros de las AGC, sería **ingresar las armas y uniformes en camiones pequeños y en costales a lomo de mula**, agregando que varios de ellos afirmaron que sus jefes "reciben la información directa de quién y lo que denuncian lo líderes sociales y las Organizaciones no gubernamentales", lo que pone en riesgo su vida e integridad.

Por último, la organización asegura que varias unidades militares se encuentran en el punto conocido como la "Y " **lugar que esta a tan solo cuatro horas a pie de "El Guamal"**, lugar donde están desplegando sus operaciones armadas.
