Title: Animalistas piden nulidad de fallo que suspendió consulta antitarina
Date: 2015-09-28 17:49
Category: Animales, Voces de la Tierra
Tags: Animalistas, Bogotá Sin Toreo, Consejo de Estado, corridas de toros en Bogotá, derechos de los animales, Elecciones 2015, Maltrato animal, Plataforma Alto
Slug: animalistas-piden-nulidad-de-fallo-que-suspendio-consulta-antitarina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/12077371_10153483437765020_1100354758_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Diferentes organizaciones **animalistas solicitaron nulidad del fallo que suspendió la realización de la consulta antitaurina** para el próximo 25 de octubre.

De acuerdo al comunicado de los defensores de la vida de los toros, c**on el fallo del Consejo de Estado se invalidaba por completo el recurso de democracia participativa** como mecanismo para prohibir los actos de crueldad hacia los toros en Bogotá.

En los próximos días se conocerá el resultado de la solicitud de nulidad, con la que se demostraría que el fallo no tuvo en cuenta a la ciudadanía que fue la que tuvo la iniciativa de realizar la consulta popular, **vulnerando el derecho a la participación política de las personas que saldrían dar su opinión frente a las corridas de toros en Bogotá,** para demostrar si existe o no arraigo cultural mayoritario.

[La Consulta Vive, la lucha sigue](https://es.scribd.com/doc/283006057/La-Consulta-Vive-la-lucha-sigue "View La Consulta Vive, la lucha sigue on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_57496" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/283006057/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-sSFIiffw5Z5v0YDjnwcP&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
