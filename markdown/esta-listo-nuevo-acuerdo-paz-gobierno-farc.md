Title: Está listo el nuevo acuerdo de paz entre gobierno y Farc
Date: 2016-11-12 12:57
Category: Nacional, Paz
Tags: #AcuerdosYA, Apoyo al proceso de paz
Slug: esta-listo-nuevo-acuerdo-paz-gobierno-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Jurisdicción-Especial-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [12 Nov 2016] 

A pocas horas del nuevo Acuerdo en La Habana, por Contagio Radio, s**e ha confirmado que se ecerrará el nuevo Acuerdo hacia la Paz entre el gobierno de Juan Manuel Santos y las FARC EP.**

El escenario será distinto por el cierre del gobierno de Obama, el premio de Oslo, un nuevo estancamiento de la mesa con el ELN, una reforma tributaria contra los sectores medios y empobrecidos y una nueva crisis humanitaria y de calamidades por la ola invernal.

Los Ajustes y Precisiones dan respuestas a sectores del No, que distintos al Uribismo han manifestado **reparos a asuntos como la restricción a las libertades y la jurisdicción Especial de Paz, la reparación material, el respeto a la propiedas privada, y el asunto de género.**

Cómo lo escribió recientemente Camilo De las Casas, el tiempo a Santos se le acaba pues el Congreso tiene dos limitaciones: **el cierre de sesiones en los primeros días de diciembre y el calendario electoral de 2018 que rompería a la alianza de gobierno entre otras razones por la Reforma Tributaria.**

Ayer Santos en reuniones con delegados de las Campañas del Sí y algunas organizaciones de víctimas expresó que en lo sustancial se mantiene el Acuerdo Final y reiteró que es el mejor Acuerdo construido en los últimos procesos de solución a conflictos armados.

Santos invito a **estar atentos a la movilización ciudadana para expresar su respaldo a lo acordado**, incluso, aceptando que él mismo se firme en la Plaza de Bolívar.

Contagio Radio conoció que a comienzos de la semana, la delegación del gobierno se levantó de la mesa, más que por el cansancio por sus tensiones internas.

Uno de los aspectos que pretendió un sector de la delegación del gobierno fue volver atrás el tema de sanciones reparadoras por sanciones penales en cárceles con independencia de la verdad.

El nuevo Acuerdo que tendrá mejoras en la redacción de estilo será **conocido a través de un nuevo intento de estrategia comunicativa** del gobierno motivando a sus mayorías parlamentarias a aprobar el Fast Track y a la ciudadanía a salir a las calles.

**El 23 de noviembre es el tiempo límite para la presentación del nuevo Acuerdo al congreso y se abrirá nuevamente el debate con el Uribismo** que nunca estará satisfecho con el proceso. Durante estos días se ha acordado en La Habana limitaciones a que algunos temas queden en el bloque de constitucionalidad. Adicionalmente, el sector castrense se ha claramente deslindado de Uribe. Las propias cúpulas militares activas, los encarcelados y los retirados asociados en ACORE han expresado su respaldo a la JEP. LiNK entrevista Acore Contagio.

**Vienen días de agitación que se aplacarán con la noche de las velas que anunciaran la navidad**, al tiempo saldrán guerrilleros y militares beneficiados por los mecanismos de amnistías e indultos o medios proporcionales legales para los agentes estatales, unos y otros, que no estén indiciados o vinculados en crímenes de Lesa Humanidad o Crímenes de Guerra.

**Vienen días de campañas y contracampañas mediáticas**. ¿Los que apuestan desde la ciuadadanía lograrán llegar al corazón de la gente? Aprenderan los ciudadanos de los resultados del Brexit en Gran Bretaña y la derrota de los Clintos en Estados Unidos?

Como lo expresó Camilo de La Casas es **la creatividad y la inteligencia emocional para desvanecer la indiferencia de los abstencionista e indiferentes el camino.** ¿Será posible rectificar el reciente pasado? La respuesta está en los ciudadanos.
