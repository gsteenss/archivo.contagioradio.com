Title: La "Colosal" vulneración de derechos por megaminería en el Tolima
Date: 2017-01-13 12:24
Category: Ambiente, Nacional
Tags: Cajamarca, Consulta Popular Minera en Ibagué, La Colosa
Slug: 34684-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kavilando] 

###### [13 Enero 2017] 

El proyecto minero La Colosa ha generado múltiples problemáticas tanto sociales como ambientales en el departamento del Tolima. De acuerdo con el informe “Democracia vale más que oro” se ha **restringido los derechos de la ciudadanía** frente a su participación democrática, han **aumentado las amenazas a líderes sociales y activistas**, y se dio una **fuerte llegada de la Fuerza Pública**, con la presencia de la multinacional **Anglo Gold Ashanti.**

**Fuerza Pública**

Uno de los puntos más importantes del documento recae sobre la Fuerza Pública y expone que desde el 2002, año en el que llega la Anglogold Ashanti, el Ejército Nacional tuvo una presencia permanente con la instalación de una base con **40 soldados**, se dispusieron **120  policías más**, se instaló una base del **Batallón de Infantería y un Batallón de Alta Montaña en Cajamarca** y “**proteger la infraestructura en la fase de exploración minera y dar seguridad al personal de Anglogold Ashanti**”.

Estos hechos además coinciden con la llegada del paramilitarismo y el **Bloque Tolima de las Autodefensas Unidas de Colombia** y las masacres de Potosí, realizadas en los años 2003 y 2004, periodo de tiempo en el que también se registraron **1274 casos de desplazamiento**. De igual modo El informe aseguró que miembros del Batallón Rooke de la Sexta Brigada del Ejército y el Batallón Contraguerrillas Pijao **estuvieron en operativos compartidos con paramilitares**. Le puede interesar: ["Organizaciones sociales denuncia reagrupamiento de paramilitares en Tolima"](https://archivo.contagioradio.com/34519/)

**Amenazas a las actividades de líderes Sociales y Activistas**

A su vez, Pax registró actos de violencia en contra de la labor que realizan líderes sociales, activistas y colectivos ambientales, como son las amenazas de muerte que se han registrado desde el año 2014 en panfletos o a través de correos electrónicos, firmados por grupos que se identifican como **Aguilas Negras y que relacionan la actividad de los líderes y activistas con grupos guerrilleros.**

De otro lado se encuentra la estigmatización que ejerce la **Fuerza Pública** hacia los activistas que han sido señalados en varias ocasiones por el Ejército de **ser milicianos a través de panfletos**, obligando a los líderes a abandonar el territorio. Le puede interesar:["Amenazan nuevamente a opositores del proyecto minero La Colosa"](https://archivo.contagioradio.com/amenazan-nuevamente-a-opositores-del-proyecto-minero-la-colosa/)

**Trabas del gobierno para que participe la ciudadanía**

El documento recopiló información durante 5 años frente al relacionamiento que se establecía entre la comunidad de estos municipios del Tolima y la multinacional Anglogold Ashanti, evidenciando que “tanto el gobierno como las autoridades nacionales hicieron varios intentos por centralizar las decisiones, sobre proyectos extractivos”, a través de diferentes **mecanismos jurídicos y de dilatación para evitar el acceso de las comunidades a información.**

Mientras que la multinacional polarizó mucho más el debate, con las **actitudes con las que a veces llegaba a las comunidades y desacreditando las opiniones de las personas**, contexto que produjo condiciones adversas para la participación ciudadana. Le puede interesar: ["Suspendida Consulta popular minera en Cajamarca por fallo del Consejo de Estado"](https://archivo.contagioradio.com/suspendia-consulta-popular-minera-en-cajamarca-por-fallo-de-consejo-de-estado/)

**Comunidades resisten y persisten**

Sin embargo, el informe señala como las comunidades locales “persistieron ante el rechazo de la exclusión a su participación” y lograron que la Corte Constitucional expidiera el fallo sobre el artículo 37 del Código de Minas, avalando el camino para la consulta popular. Resultado de ello fue la victoria para el municipio de **Piedras que rechazo que en su territorio se realice explotación minera.**

Actualmente también se están llevando a cabo procesos de **Consultas mineras en Ibagué  y Cajamarca**, pese a las diferentes problemáticas que siguen denunciado las comunidades, ahora frente a el proceso para efectuar la consulta.

[Informe PAX - Democracia Vale Mas Que El Oro](https://www.scribd.com/document/336479952/Informe-PAX-Democracia-Vale-Mas-Que-El-Oro#from_embed "View Informe PAX - Democracia Vale Mas Que El Oro on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_50800" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336479952/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-2Cw9sFbnFDFKnhllraqa&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
