Title: Eduación virtual por Covid-19, una decisión para la que no estamos preparados
Date: 2020-03-17 18:28
Author: AdminContagio
Category: Actualidad, Educación
Tags: Clases, Covid-19
Slug: eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Educación-y-Covid-19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo, Colegio Agropecuario Máximo Gómez {#foto-archivo-colegio-agropecuario-máximo-gómez .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde este lunes 16 de marzo, y con ocasión del Covid-19, todas las instituciones de educación publica del país dejarán de tener clases de manera presencial, y buscar la educación virtual para proteger a la comunidad académica de nuevos contagios. Para algunos estudiantes, la medida ha develado los problemas a los que se ve enfrentado el sistema de educación en el país, como el cubrimiento o la calidad del mismo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/lee_puj/status/1238512249012211712","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/lee\_puj/status/1238512249012211712

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A través de su cuenta de twitter, el presidente Iván Duque anunció la decisión, agregando que del 16 al 27 de marzo los docentes prepararán las actividades para que estudiantes puedan continuar los programas curriculares en casa. Según la Unión Nacional de Estudiantes de Educación Superior (UNEES), hasta el momento, más de 60 Universidades públicas y privadas también han adoptado la medida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La realidad de la educación virtual en el país**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/lee_puj/status/1237833362024341513","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/lee\_puj/status/1237833362024341513

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El Laboratorio de Economía de la Educación ([LEE](https://twitter.com/lee_puj)) de la Universidad Javeriana informó que el 96% de los municipios del país no podrían implementar la educación virtual porque menos de la mitad de sus estudiantes de grado 11 tienen computador e internet en sus casas para conectarse. La conclusión fue posible gracias a los datos recaudados por el ICFES Saber grado 11 del segudo semestre de 2018.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LeonVaLenciaA/status/1239510579120869377","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LeonVaLenciaA/status/1239510579120869377

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A ello se suma la necesidad de mantener el Programa de Alimentación Escolar (PAE) como una forma de asegurar la nutrición de niños, niñas y adolescentes en algunas regiones del país. Programa que mantuvo la Alcaldía de Bogotá, pero exige a las personas reclamar dicho refrigerio en determinados lugares, obligándolos a salir de sus casas y poniendo barreras al acceso.(Le puede interesar: ["Califícan de absurdo modelo de contratación de refrigerios escolares de Bogotá"](https://archivo.contagioradio.com/califican-de-absurdo-modelo-de-contratacion-de-refrigerios-escolares-de-bogota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Julián Baéz, representante estudiantil de la Universidad Distrital e integrante de la UNEES, por estas razones, es claro que el Coronavirus está demostrando las falencias en trasladar entornos educativos presenciales a virtuales, así como la necesidad de mantener y fortalecer los espacios físicos creados para garantizar el derecho a la educación. (Le puede interesar:["Menos del 10% de quienes presentan el ICFES se gradúan de la Universidad"](https://archivo.contagioradio.com/menos-icfes-graduan-universidad/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La incapacidad de las Instituciones de Educacion Superior (IES)**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Báez recordó que pese a la coyuntura del Coronavirus no se puede olvidar que estamos en un momento de movilización social y crisis política, y que las universidades juegan un papel trascendental en dichos procesos de movilización y pensamiento crítico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto a la virtualidad en las IES, Baez manifestó que "estamos encontrándonos con una gran incapacidad de asumir este escenario de clases no presenciales, porque las herramientas que tienen las universidades no son suficientes y no son eficaces". Por ejemplo, jovenes han cuestionado que la Universidad Nacional haga uso de plataformas creadas por la misma empresa que implementó el sistema de inscripción de asignaturas que hace apenas unas semanas presentó errores.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/pillis_m/status/1232024684683767808","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/pillis\_m/status/1232024684683767808

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El integrante de la UNEES agregó que algunos profesores no están capacitados para hacer uso de las herramientas virtuales, haciendo aún más difícil el proceso. Sumado a ello, sostuvo que en durante las experiencas de paros vividos en la Distitral se ha intentado acudir a la virtualidad para no afectar el calendario académico pero ello solo ha hecho disminuir la calidad académica, porque se reduce "la capacidad de socializar y construir conocimiento conjunto".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Retomando la idea de la criticidad en las Universidades para evaluar el momento por el que atraviesa el país, Baez sostuvo que sí se debían tomar medidas para evitar la propagación del virus, pero también se debe pensar en cómo aprovechar los espacios virtuales para que no se disminuya el elemento crítico de los estudiantes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
