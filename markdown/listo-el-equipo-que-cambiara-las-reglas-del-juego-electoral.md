Title: Listo el equipo que cambiará las reglas del juego electoral
Date: 2017-01-17 16:15
Category: Nacional, Paz
Tags: Alejandra Barrios, Misión de Observación Electoral, Misión Especial Electoral, Sistema Electoral Colombiano
Slug: listo-el-equipo-que-cambiara-las-reglas-del-juego-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG-20170117-WA0015.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Misión de Observación Electoral] 

###### [17 Ene 2017] 

La Misión Electoral Especial –MEE–, cuya conformación hace parte de los Acuerdos de Paz, estará integrada por la Misión de Observación Electoral –MOE– y seis expertos independientes, que tendrán la tarea de presentar al Gobierno y al Congreso **recomendaciones para transformar el sistema político y electoral de Colombia en menos de 3 meses.**

Alejandra Barrios directora de la Misión de Observación Electoral, manifestó que el objetivo de la MEE “es **hacer más transparente el sistema electoral y mejorar la Calidad de la Democracia (…) tenemos problemas muy graves de cultura democrática**, se elaboran normas que hacen el papel de la costurera, van tomando medidas para luego ver a quien le sirven”.

### ¿Cuáles son los temas de trabajo? 

Barrios señaló que hasta el momento la MOE ha construido conjuntamente 4 puntos centrales para hacer una mirada integral al sistema electoral y que se pondrán a consideración de la Misión Especial.

1.  **Establecer un tribunal electoral**, para evitar competencias entre dependencias y demoras en los procesos. Es necesaria una mirada independiente que no sea de origen partidista.
2.  **Financiación de la política**. Se debe conocer el monto verdadero de los costos de una campaña electoral, pues los temas que tienen que ver con financiaciones de campañas indiscutiblemente han estado relacionados con corrupción.
3.  **Calidad de los candidatos** que presentan los partidos políticos. No se pueden presentar candidatos que tengan procesos de investigación, “como ocurrió con ‘Kiko’ Gómez, ex gobernador de La Guajira, condenado a 55 años de prisión”, puntualizó Barrios.
4.  Mecanismos institucionales que permitan crear **nuevas reglas para la conformación de nuevos partidos políticos** legales.

Por su parte, el presidente Juan Manuel Santos explicó que seis de los siete miembros de la Misión electoral son seleccionados por entidades independientes como el Centro Carter, el Instituto Holandés para la Democracia Multipartidaria y los Departamentos de Ciencia Política de las universidades Nacional y de los Andes.

### ¿Quienes conformarán la Misión Electoral Especial? 

El equipo esta integrado por Alejandra Barrios, Directora de la MOE, Elisabeth Ungar, fundadora y Directora del Observatorio Congreso Visible y Directora de Transparencia por Colombia, Alberto Yepes, Consejero de Estado, Jorge Enrique Guzmán, fundador y Director del Departamento de Sicología Social de la Universidad Javeriana, y consultor de la división electoral de las Naciones Unidas, Juan Carlos Rodríguez-Raga, Profesor Asociado del Departamento de Ciencia Política de la Universidad de los Andes y Co-director del Observatorio de la Democracia de esta misma universidad y Salvador Romero, exdirector del Centro de Asesorías y Promoción Electoral del Instituto Interamericano de Derechos Humanos.

Por último, Barrios indicó que aunque el plazo estipulado en los acuerdos es de 4 meses, el equipo procurará entregar resultados en menos de 3 meses y agregó que “estamos seguros que si la Misión Especial logra un diálogo abierto con medios de comunicación y la sociedad civil, **vamos a poder ampliar este debate que tiene que ver con algo fundamental, el poder en el país”.**

<iframe id="audio_16434180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16434180_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
