Title: Poderoso empresario de Palma condenado a 10 años de cárcel
Date: 2017-06-05 13:17
Category: Judicial, Nacional
Tags: Condena, Curvarado, Jiguamiandó, Palma aceitera, Palmeros
Slug: condenado-a-10-anos-de-carcel-uno-de-los-mas-poderosos-empresarios-de-palma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/palma-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### 05 Jun. 2017 

El Tribunal Superior de Medellín condenó a 10 años de cárcel al empresario **Antonio Zuñiga Caballero por los delitos de concierto para delinquir agravado, desplazamiento forzado e invasión de áreas de especial importancia ecológica** en Curvaradó y Jiguamiandó, departamento del Chocó. Zuñiga Caballero es el accionista mayoritario de Urapalma S.A y Palmadó.

Según lo relata **Manuel Garzón, abogado de la Comisión de Justicia y Paz que representa a las víctimas civiles**, de las 35 sentencias acusatorias que se han proferido hasta el momento, Zuñiga Caballero es la persona más importante y el empresario más relevante que ha debido comparecer. Le puede interesar: [Capturan a empresario Darío Montoya por desplazamiento en Curvaradó](https://archivo.contagioradio.com/capturan-a-empresario-ganadero-dario-montoya-por-desplazamiento-en-curvarado/)

“El proceso con él ha sido uno de los más tortuosos. Hacia el 30 de octubre del año 2014 salió la sentencia condenatoria contra 16 empresarios palmeros, notarios y paramilitares de la región. Sin embargo, en este caso hubo particularidades lamentables siempre, como la preclusión de la investigación a su favor y la absolución, decisiones que fueron apeladas” manifestó el abogado.

### **¿Qué significa condenar a Antonio Zuñiga?** 

Garzón asevera que esta condena reafirma que las denuncias que han realizado las comunidades desde el año 1996 y que han sido documentadas por años, son ciertas “esto **muestra los vínculos que existían y existen aún entre sectores empresariales vinculados a la criminalidad**, concretamente a los grupos paramilitares, que operaron allí en complicidad con agentes estatales” recalcó el abogado.

**Los tres delitos que se le imputan a Zuñiga dan cuenta de la pertenencia del empresario a grupos paramilitares** “es importante mostrar que empresarios reconocidos hayan sido condenados por su pertenencia a grupos paramilitares, no son simple alianzas, sino que es una misma organización”. Le puede interesar: [Condenados empresarios palmeros de Curvaradó por paramilitarismo](https://archivo.contagioradio.com/condenados-empresarios-palmeros-de-curvarado-por-paramilitarismo/)

Adicionalmente el jurista resalta que esta condena da cuenta de los daños ambientales ocasionados por estas empresas en lugares considerados como reserva forestal “visibilizar esos 3 delitos es sumamente importante para mostrar los fenómenos acontecidos. Es un aliciente, **la sentencia en sí misma puede representar una forma de reparación a las comunidades”**.

### **¿Cómo llega Antonio Zuñiga a Curvaradó?** 

Zuñiga llega a Curvaradó y Jiguamiandó por invitación del exparamilitar de las AUC, Vicente Castaño. La invitación se hace gracias al amplio conocimiento de los cultivos de palma aceitera con los que cuentan los Zuñiga Caballero, una familia muy reconocida de la Costa Atlántica Colombiana que se han dedicado de manera histórica a este tipo de cultivos.  Le puede interesar: [La macabra alianza entre paramilitares y empresas Bananeras](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/)

“Dada esa experticia que tenían, **Vicente Castaño los lleva, a él y a otros empresarios de la Costa Atlántica a desarrollar allá en Curvaradó,** en las tierras que previamente habían despojado mediante la violación sistemática de derechos humanos, el proyecto agroindustrial” recalca el abogado.

### **Una decisión que puede volver a ser apelada** 

La defensa del empresario Antonio Zuñiga Caballero podrá interponer el recurso extraordinario de casación ante la Corte Suprema de Justicia, sin embargo, la Corte puede aceptar el recurso, pero no está obligada a hacerlo e incluso puede archivar el caso, dejando en firme la condena.

### **Falta que el Estado reconozca su responsabilidad** 

Sin restar la importancia de este fallo, el abogado recalca que se hace necesario que el Estado colombiano reconozca que unas comunidades que estaban denunciando graves hechos que acontecían en Curvaradó, tenían razón. Le puede interesar: [Empresario palmero Irving Bernal podría ser condenado por desplazamiento forzado](https://archivo.contagioradio.com/empresario-palmero-irving-bernal-podria-ser-condenado-por-desplazamiento-forzado/)

“Es importante que **el Estado de Colombia se vea en la obligación de reconocer que amplios sectores empresariales han estado vinculados a la criminalidad** o han pertenecido directamente a grupos paramilitares para imponer un modelo económico de acumulación” concluye Garzón.

<iframe id="audio_19108960" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19108960_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
