Title: Derecho a la alimentación: millones de personas en el mundo siguen pasando hambre
Date: 2019-07-25 11:23
Author: CtgAdm
Category: DDHH, Nacional
Tags: FAO, Hambre, seguridad alimentaria
Slug: derecho-a-la-alimentacionmillones-de-personas-pasan-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Reuters.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### El hambre en cifras

El 26,4% de la población mundial no tiene un plato de comida asegurado. Un porcentaje que resulta alarmante si se tiene en cuenta que equivale a cerca de 2 mil millones de seres humanos los que se encuentran bajo amenaza y padecimiento de hambre a nivel global. La cifra además representa un retroceso a los registros de 2010, como se refleja en el informe de la FAO (aquí poner la sigla completa) sobre el estado de la seguridad alimentaria y la nutrición en el mundo publicado el 15 de Julio.

A pesar del aparente crecimiento económico en algunos de los países más pobres del mundo en los últimos años, aun no se consigue garantizar la seguridad alimentaria. África es la región que reporta las cifras más altas de subalimentación con un 20% de las población, mientras que Asia logra apenas al 11%. Por otro lado en América Latina el panorama es menos alentador y las cifras con tendencia hacia el crecimiento registran un 7%, afectando a 42,5 millones de personas.

Si bien estos datos pueden sonar como supuestos, el hambre ha ido en aumento también en muchos países de ingresos medios cuya economía es en fase de desaceleración o de contracción. Entre el 2011 y el 2017 las naciones que experimentaron un aumento de la subalimentación (65 de 77) no son sólo países de bajos ingresos, dando cuenta de que el hambre socava de manera nociva a todo el mundo. Siendo además la violencia, los conflictos y a la exposición a eventos climáticos extremos más complejos barreras que permitan la eliminación del hambre y la malnutrición.

### Preocupación en organismos internacionales

En el marco de la inseguridad alimentaria también la obesidad representa otra forma de malnutrición. Resulta paradójico que mientras existen datos que revelan que hambre en el siglo XXI sigue afectando a millones de personas y que lleva 3 años sin disminuir; a la par los indíces de obesidad crecen dejando vidas truncadas. El sobrepeso afecta a más de 38 millones de niños menores de cinco años.

El Representante Regional de la FAO, Julio Berdegué afirmó con respecto al informe que es indispensable hasta el 2030 rescatar por lo menos 3,5 millones de personas del hambre como refleja el Objetivo numero 2 de la Agenda 2030 de Desarrollo Sostenible.
