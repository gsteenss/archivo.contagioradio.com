Title: Comunidad indígena en Putumayo dijo NO a actividad petrolera de Amerisur
Date: 2016-05-03 15:15
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Amerisur, comunidades indígenas, Explotación petrolera, Putumayo
Slug: comunidad-indigena-en-putumayo-dijo-no-a-actividad-petrolera-de-amerisur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/derrame-de-petroleo-putumay_4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Contagio Radio 

###### [3 May 2016]

El pasado 29 de marzo el Pueblo Murui Monai de Jitoma, en Puerto Leguizamo en el departamento de Putumayo, hizo legitimo su derecho fundamental a la consulta previa para dar su negativa al proyecto de **exploración y explotación petrolera en 9 pozos de la multinacional Amerisur Exploración Colombia Ltda.**

Serían cuatro las comunidades indígenas las que se verían afectadas por el proyecto petrolero, **“Sísmico 2D dentro del Bloque Put-12”. Las comunidades Nasa, Sinaoa, Murui y kichwa,** habitan sobre las orillas del río Putumayo y Piñula Blanco, que se encontrarían en riesgo de que sus aguas sean contaminadas por los desechos tóxicos que arroja la empresa,  como se ha experimentado en otras zonas cercanas donde la petrolera ya inició sus actividades.

Las comunidades esperan que ese ejercicio de la consulta sea tenido en cuenta para que se detenga la entrada de las petroleras a este departamento, teniendo en cuenta que la comunidad Monai de Jitoma del pueblo Murui, y demás pueblos del Departamento de Putumayo, cuentan **con medidas de salvaguarda en el marco del auto 004 de 2009, por estar declarado en riesgo de desaparecer física y culturalmente.**

De acuerdo con la Organización de los Pueblos Indígenas de la Amazonía Colombiana, OPIAC, en la realización de la ruta metodológica se realizaron actividades de visita a territorios del pueblo Siona e Inga en donde ya se han realizado proyectos sísmicos. Allí la población indígena ha reportado el **desmejoramiento de la calidad de vida, la disminución de la cobertura boscosa, la pérdida de plantas sagradas, problemas culturales debido a la colonización de sus territorios, contaminación de las fuentes hídricas**, entre otros impactos sociales, ambientales y culturales.

De acuerdo con Phanor Guasaquillo, miembro de la  mesa regional de las organizaciones sociales de Putumayo, de no respetarse la voluntad del pueblo indígena que expresó su rechazo a la activdad petrolera en sus territorios, **el caso será llevado hasta instancias internacionales para que se pueda garantizar los derechos de los pueblos indígenas,** teniendo en cuenta que, según lo denuncia Guasaquillo, **el paramilitarismo es una de las estrategias que se usa para desplazar a los indígenas** y poder dar inicio a la exploración y explotación petrolera.

<iframe id="audio_11398305" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11398305_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
