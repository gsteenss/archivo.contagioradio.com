Title: Estados financieros de la ETB reportan utilidades por 370 mil millones
Date: 2016-03-12 09:00
Category: Economía, Entrevistas
Tags: ETB, Sintrateléfonos, Universidad Distrital
Slug: etb-le-aporta-al-distrito-cerca-de-un-billon-de-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/VENTA-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

<iframe src="http://co.ivoox.com/es/player_ek_10792970_2_1.html?data=kpWkm5ede5Ghhpywj5adaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncabn1cbR0diPqsriwtPQy8rWs9SfxcqYzsaPibW2jNfS0tTWuMLijNrhy9HNqMLYxtiY0tTWb5Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Leonardo Argüello, SINTRATELEFONOS] 

###### [12 Mar 2016] 

Desde sus primeros días de posesión el actual alcalde de Bogotá, Enrique Peñalosa, manifestó la intención de vender la Empresa de Teléfonos de Bogotá ETB, [en mil millones de dólares](https://archivo.contagioradio.com/a-un-precio-irrisorio-penalosa-estaria-pensado-vender-la-etb/), Jorge Castellanos, gerente, argumenta que entre 2014 y 2015 la empresa acumuló pérdidas por \$74 mil millones y que debido a su insostenibilidad financiera debe venderse a capital privado. Sin embargo **la versión de los trabajadores es muy diferente**.

Leonardo Argüello, actual secretario del Sindicato de Trabajadores de la ETB SINTRATELEFONOS, afirma que **tanto Castellanos como Peñalosa le están mintiendo a la ciudadanía, pues el dinero que en los estados financieros esta reportado como pérdidas hace parte del capital que la ETB ha invertido en proyectos de fibra óptica**, que en el marco de un plan estratégico diseñado a corto, mediano y largo plazo, se espera que generen en 2022 utilidades de entre 2.5 y 3 billones de pesos.

De acuerdo con Argüello la información que el actual presidente de la ETB sostiene frente a la opinión pública no es acorde con los estados financieros sólidos que fueron publicados en la página web de la empresa a los que cualquier ciudadano o ente de control puede acceder para su correspondiente revisión y que **reportan utilidades superiores a los 370 mil millones de  pesos, crecimiento exponencial del patrimonio de 25.64% y un billón de pesos de ganancias para el distrito.  **

Con la venta de la ETB saldrían afectados no sólo los trabajadores de la compañía sino gran parte de la población estudiantil de la ciudad, pues según afirma Argüello la empresa durante los dos últimos años le ha **representado a la Universidad Distrital ingresos de entre 7 mil y 10 mil millones de pesos**, así como la posibilidad de contar con el servicio de internet gratuito en las bibliotecas y colegios públicos, siendo la ETB la única empresa que oferta internet de 150 megas.

Según Argüello los problemas financieros de la ETB tienen su razón de ser en la tercerización laboral, pues con esta forma de contratación **“los privados son los que se están llevando la tajada más grande”** en detrimento de la compañía; sin embargo, no son tan agudos como para que sus directivas y el actual alcalde de la ciudad pretendan venderla, como se buscó entre 1997 y 2001 durante la primera administración de Peñalosa.
