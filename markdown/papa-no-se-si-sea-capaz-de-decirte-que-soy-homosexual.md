Title: "No se si sea capaz de decirte que soy homosexual" - Cartas desde el closet
Date: 2017-07-04 12:17
Category: Cartas desde el Closet, Opinion
Tags: carta, Comunidad LGBTI
Slug: papa-no-se-si-sea-capaz-de-decirte-que-soy-homosexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/carta-desde-el-closet.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Google 

Junio de 2017

**Hola papá**

[Siento mucha gratitud por todos los cuidados que has tenido conmigo siempre. Entiendo que tener hijos no es fácil y asumirlo con responsabilidad en estos tiempos es un acto heroico. ]

[No se si algún día te entregue esta carta, no se si sea capaz de decirte que soy homosexual.]

[Siento mucho dolor al ver la indiferencia, el desprecio, el odio y la repugnancia con la que te refieres a los homosexuales, mientras yo estoy ahí, escuchando: "los homosexuales van a irse todos al infierno, Dios va limpiar el mundo de ellos a través de desgracias y actos violentos como lo que pasó en Orlando, porque todos eran gays "]

[¿Eso es lo que deseas para mi?. Yo nací en un “matrimonio perfecto”, católico, no soy una aberración ni una maldición, no tuve la oportunidad de elegir mi orientación sexual.]

[Si hubiera podido elegir mi orientación sexual, no sería homosexual ¿Para qué escoger este infierno? No te imaginas lo horrible que es sentirse señalado en tu propia casa y en todas partes como alguien raro, malo y como un ser despreciable.]

[Me duelen profundamente tus palabras y no puedo dejar de sentir que ese “castigo” es lo que quieres para mi.]

[No entiendo esas actitudes al tiempo que haces discursos religiosos sobre del amor de Jesús a todos los seres humanos.]

[Tal vez, Él si nos ama.]

[Tratando de entender tus mensajes de condena a los homosexuales, busqué en los evangelios y descubrí que  Jesús de Nazareth no dijo nada en contra de nosotros.]

[A pesar del dolor que siento, confío en que tú puedas empezar a cambiar de actitud hacia nosotros por el mismo amor de Jesús. Ver: ["Querida Abuela" carta desde el closet ](https://archivo.contagioradio.com/cartas-desde-el-closet/)]

**Tu hijo  - [Parche para la vida](https://archivo.contagioradio.com/cartas-desde-closet/), 2 **

------------------------------------------------------------------------

###### Esta texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida 
