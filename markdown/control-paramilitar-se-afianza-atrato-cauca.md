Title: Control paramilitar se afianza en Bajo Atrato y Cauca
Date: 2017-01-20 12:19
Category: DDHH, Nacional
Tags: Amenazas a defensores de Derechos Humanos, Autodefensas Gaitanistas de Colombia, paramilitarismo en Colombia, Presencia Paramilitar en el Chocó, Presencia Paramilitar en el Norte del Cauca
Slug: control-paramilitar-se-afianza-atrato-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [20 Ene 2017] 

Las comunidades de Cacarica, Llano Rico y Norte del Cauca han denunciado la presencia amenazas y hostigamientos por parte del grupo paramilitar autodenominado Autodefensas Gaitanistas de Colombia en los últimos días, **reuniones con la comunidad, pintas en las casas y panfletos son algunas de las denuncias.**

Líderes comunitarios de Cacarica en el Chocó informaron a la Comisión de Justicia y Paz que desde el 2 de enero  “por lo menos 100 neo paramilitares vestidos de camuflado  se encuentran **asentados y transitando en las comunidades de Bendito Bocachico, San José de Balsa, La Coquera y Varsovia, territorio colectivo de Cacarica”.**

Además revelaron que los armados, han organizado reuniones con los pobladores de estas zonas y “han sostenido que ellos se van a quedar en el territorio”. Desde septiembre de 2015, las comunidades ya habían denunciado que las Autodefensas Gaitanistas, ya tenían consolidada “una fase de control  perimetral de los accesos fluviales a Cacarica y desde Turbo y Río Sucio”.

Las comunidades que habitan la cuenca del río Curvaradó, también en el Chocó, señalaron que el 18 de enero en varias casas de la comunidad de Llano Rico, a 15 minutos de la Zona Humanitaria Las Camelias, amanecieron pintas con las siglas de las Autodefensas Gaitanistas –AGC–. Los hechos se registraron **a 30 minutos del lugar donde será ubicada la Zona Veredal Transitoria de Normalización en dicha región.**

### Amenazas a defensores de derechos humanos en el Cauca 

Por otra parte, organizaciones de derechos humanos del Norte del Cauca manifestaron que recibieron por medio de correo electrónico, un panfleto firmado por un grupo autodenominado Águilas Negras, y aunque hasta el momento no se ha corroborado la veracidad del remitente, en el escrito dicho grupo advierte que **“no permitiremos que este año se realicen manifestaciones llamadas minga social o movilizaciones sociales** (…) son sabotajes a la gente y trabajadora de bien del país”.

También anuncian que iniciarán limpieza social en los territorios y que **“no descansaremos hasta ver a Colombia libre de defensores de derechos humanos que tanto perturban”.**

De igual forma, invitan a jóvenes del territorio que se encuentran sin empleo “a que se sumen a nuestras filas y armemos un solo grupo donde mantengamos el control del país, por un nuevo país (…) **les garantizaremos un sueldo y premios por matar a líderes y defensores de derechos humanos en el norte del Cauca”.**

Distintas organizaciones defensoras de derechos humanos, la vida y el territorio en varias regiones del país, han venido denunciando la presencia de grupos paramilitares y que en lo que va corrido del año, ya son 15 los líderes y lideresas asesinados. Frente a ello el Ministro de Defensa, Luis Carlos Villegas ha negado la existencia de paramilitarismo en Colombia y aseguró que **“hay asesinatos pero no son sistemáticos, si lo fueran sería el primero en aceptarlo”.**

Sobre este asunto, Carlos Lozano resaltó que para el proceso de paz **no es un buen mensaje que el gobierno siga negando la existencia del paramilitarismo** y que "la única respuesta positiva debe ser reconocerlo para poderlo enfrentar".

###### Reciba toda la información de Contagio Radio en [[su correo]
