Title: Alianza Interpartidista le apuesta a reformar la política antidrogas en Colombia
Date: 2019-07-24 17:26
Author: CtgAdm
Category: Nacional, Política
Tags: alianza interpartidista, Glifosato, proyectos, regulación drogas
Slug: alianza-interpartidista-le-apuesta-a-reformar-la-politica-antidrogas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alianza-Interpartidista-Drogas-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[Fueron anunciados tres de los ocho proyectos que adelantan varios senadores como Iván Cepeda, Luis Fernando Velasco, Aida Avella, Gustavo Bolívar, entre otros, quienes han generado una alianza interpartidista para reformar la política de drogas en el país.]

[El senador Gustavo Bolívar, del partido de La Decencia, afirmó que “uno de los motivos para proponer estos proyectos, **es el modelo actual, que ha fracasado y el cual consiste en el prohibicionismo,** aunque el plan que propone la alianza no puede garantizar efectos positivos si trabaja por un cambio para la regulación de las sustancias”.]

[Otro de las motivaciones para este proyecto, según el senador, Luis Fernando Velasco de la bancada Liberal, es la disminución de violencia que se genera entorno al consumo de sustancias, como es el caso de Portugal. Además busca que los pequeños productores campesinos se les de licencia de producción y estas no sean otorgadas únicamente a las industrias privadas. (Le puede interesar: ["Gobierno Duque está en absoluta ignorancia sobre la política de drogas"](https://archivo.contagioradio.com/gobierno-duque-ignorancia-drogas/))]

### **¿En qué consisten los proyectos?** 

[El primer proyecto consiste en **modificar el artículo 49 de la Constitución Política, el cual prohíbe las sustancias psicoactivas**, excepto el uso medicinal de la marihuana. Al reformar este artículo se puede dar vía libre para empezar a regular el uso de esta sustancia, pues según expresa Bolívar, no hay víctimas por sobredosis de marihuana, pero si por la violencia que genera el consumo. ]

[Según Bolívar, por medio de este proyecto se desea “arrancarle el negocio al narcotráfico”  y lograr tres objetivos, primero que la marihuana sea de mejor calidad que la que distribuyen los *jíbaros (vendedor de drogas)*; segundo que el dinero que se genere, sea susceptible de impuestos, lo que traduce que los rubros irían al Estado y lo ideal  es que se inviertan en educación y prevención y el tercer logro del primer proyecto es que los centros encargados de el consumo deberán contar con asistencia de tipo psicológica y medicinal.]

[El segundo proyecto, habla del tratamiento penal diferenciado. Julián Gallo, senador del Partido FARC,  afirma que "se trata de otorgarle el beneficio a los campesinos que firmen el compromiso de]sustitución voluntaria y concertada, donde reconocen que cometieron un delito y que no reincidirán, **a su vez el estado debe cesar la persecución penal"**

[El tercer proyecto habla del glifosato y sus derivados, Cepeda expone que “este proyecto tendría varios artículos consistentes en: primero  la preservación del ambiente, la salud y la vida en las comunidades. **Segundo la prohibición de este herbicida**, Tercero priorizar las políticas voluntarias de erradicación y sustitución de cultivos”]

### **Incidencias de los proyectos en la implementación del acuerdo de paz ** 

[Estos proyectos tienen relación con la implementación de los acuerdos de paz, según Cepeda, el tratamiento penal diferenciado para los campesinos,  se relaciona directamente con la sustitución y erradicación voluntaria de cultivos de coca, que hace parte del punto número cuatro de los acuerdos de paz, donde se regula el tema de drogas. (Le puede interesar: ["Pactos de sustitución deben cumplirse, glifosato no debe ser prevalente"](https://archivo.contagioradio.com/pactos-de-sustitucion-deben-cumplirse-glifosato-no-debe-ser-prevalente/))]

[Además"en los acuerdos cuando se hace referencia a la sustitución de cultivos también se ven involucradas instituciones como la Agencia Nacional de Tierras y la Agencia de Desarrollo Rural, encargadas del desarrollo de proyectos agroindustriales, forestales y pecuarios". La prohibición del glifosato, pues también está en el acuerdo de paz debido a la afectación de tipo ambiental y de salubridad que tiene incidencia en las regiones. ]

### **¿Qué sigue para estos proyectos?** 

[El siguiente paso a seguir por la alianza interpartidista es la **presentación de los proyectos ante la comisión primera del senado,** por fortuna esta iniciativa estaría respaldada por múltiples senadores y partidos. ]

[El senador Temístocles Ortega considera que “estos proyectos son una ruta para el país y para el mundo, pues el narcotráfico no solo incide en Colombia, también es un flagelo a nivel internacional”. (Le puede interesar: ["Campesinos del sur de Córdoba no permitirán fumigación con glifosato ni erradicación forzada"](https://archivo.contagioradio.com/campesinos-del-sur-de-cordoba-no-permitiran-fumigacion-con-glifosato-ni-erradicacion-forzada/))]

[La alianza también quiere alcanzar estándares mayores, para que existan conferencias internacionales donde se conozcan las políticas y logros de cada país en cuanto a la regulación de consumo.  ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
