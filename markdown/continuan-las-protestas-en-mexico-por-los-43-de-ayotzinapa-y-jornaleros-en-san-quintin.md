Title: Continúan las protestas en México por los 43 de Ayotzinapa y jornaleros en San Quintín
Date: 2015-03-24 17:19
Author: CtgAdm
Category: El mundo, Movilización
Tags: 43 desparecidos Ayotzinapa, CIDH, Jornaleros en lucha San Quintín, mexico, Protestas en México
Slug: continuan-las-protestas-en-mexico-por-los-43-de-ayotzinapa-y-jornaleros-en-san-quintin
Status: published

###### Foto:Patriagrande.org 

###### **Entrevista con [Francisco Cerezo], defensor de DDHH:** 

<iframe src="http://www.ivoox.com/player_ek_4257942_2_1.html?data=lZeimZ6Ydo6ZmKiakp2Jd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1c7bh6iXaaO1wtOYzsbXb9Hm0NnS1dnFt4zZz5C6h6iXaaKt2c7Q0ZDUs9OfzdTgjZmXb8XZjKbm0dnecYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Este Domingo **miles de personas marcharon por la capital Mexicana por los 43 desaparecidos de Ayotzinapa** , en contra de la **represión y para exigir la libertad de todos los presos políticos**. A su vez hubo fuertes disturbios entre **campesinos** y fuerzas del orden público en **San Quintín**, donde jornaleros convocaron una jornada de huelga para **exigir derechos laborales**.

Según Francisco Cerezo del la organización social de defensa de los derechos humanos de México **Comité Cerezo**, la jornada de lucha del domingo era la **décima marcha** convocada en la capital para **exigir el esclarecimiento y una investigación por los 43** estudiantes desaparecidos en Ayotzinapa.

También se marchó en contra de la fuerte represión que están teniendo todas las protestas en el país, y para exigir la libertad de todos los presos políticos que han entrado recientemente debido al **clima de protesta generalizado**.

Para el defensor de derechos humanos del Comité Cerezo, “…**toda esta represión es parte de una estrategia del Estado para crear miedo y que la gente no salga a las calles**…”.

Paralelamente en **San Quintín, estado Baja California**, jornaleros realizaron un paro para demandar **derechos laborales mínimos** y, en palabras de Francisco Cerezo, para acabar con la **situación de semi-esclavitud** que están viviendo, y que es similar a la anterior a la independencia. La protesta finalizó con más de **200 detenidos y cientos de heridos** durante los enfrentamientos entre huelguistas y fuerzas de orden público.

Con este paro se pretende también presionar a las multinacionales  y a **EEUU, principal exportador de fresa, cultivos principal en San Quintín**, donde los campesinos han convocado la jornada de lucha.

El Comité Cerezo nos explica que, organismos internacionales están ejerciendo acciones de presión como la de la **CIDH, que en una declaratoria indicó que “…Vemos elementos que estructuran el delito de desaparición forzada...”**, y exigió al estado clasificar a los 43 de Ayotzinapa de desaparecidos forzadamente.

De cara un futuro los defensores de derechos humanos en México interpretan que la protesta va a continuar y que la represión va a ir en aumento, aunque hay que destacar que cada vez esta está más organizada.
