Title: Crece rechazo a posible nombramiento de Pacheco García como director del Centro de Memoria
Date: 2018-10-12 16:58
Author: AdminContagio
Category: Nacional, Sin Olvido
Tags: centro de memoria, memoria de colombia, pacheco garcia, Palacio de Justicia
Slug: crece-el-rechazo-a-pacheco-garcia-como-posible-director-del-centro-nacional-de-memoria-historica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Sin-Olvido-238-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Oct 2018] 

Las **víctimas de la retoma del Palacio de Justicia** enviaron una carta al presidente Duque, en la que le solicitan que no nombre a Pacheco García como director del **Centro Nacional de Memoria Histórica**, debido a que su perfil no es acorde con el objetivo de esta institución y además por sus posturas en contra de la institución.

Rene Guarín, familiar de Cristina del Pilar Guarín, víctima del Palacio de Justicia señaló que García** "tiene una mirada equivocada de lo que significa la memoria"** en el momento actual del país, luego de una firma del Acuerdo de Paz, y de que las víctimas estén a la espera de que se esclarezca la verdad del acciona criminal de las Fuerzas estatales y de los grupos armados ilegales, tanto guerrilleros como paramilitares.

"Creemos que es una revictimización para todos los movimientos de víctimas de crímenes de Estado en Colombia" afirmó Guarín y agregó que para las víctimas del Palacio de Justicia, un hecho que garantiza la no repetición de la historia es la garantía para la memoria, sin embargo, para ellos ha sido ocultada y difícil de esclarecer al** "ocultar" lo que pasó allí el 6 y 7 de noviembre de 1985. **(Le puede interesar:[ "FF.MM debe responder por la desaparición de Magistrado Andrade y Alfonso Jacquin"](https://archivo.contagioradio.com/fiscalia-debe-abrir-investigacion-por-desaparicion-forzada-de-las-ff-mm-del-palacio-de-justicia/))

Además, otras organizaciones defensoras de derechos humanos y políticos del país también expresaron su rechazo a este posible nombramiento debido a que en ocasiones anteriores García hizo afirmaciones como que** el Centro Nacional de Memoria Histórica  estaba "infiltrada" por la guerrilla**.

### Petición en Change 

A través de una petición en la plataforma change.org con el título  "[Memoria o negacionismo: No a Javier Pacheco como director del Centro de Memoria Histórica](https://www.change.org/p/ivan-duque-memoria-o-negacionismo-no-a-javier-pacheco-como-director-del-centro-de-memoria-hist%C3%B3rica?recruiter=24164546&utm_source=share_petition&utm_medium=copylink&utm_campaign=share_petition&utm_term=share_petition)" las personas se están sumando al llamado para que el presidente Duque detenga este posible nombramiento que deslegitima la importancia que la sociedad y el Estado le ha dado a la institución".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
