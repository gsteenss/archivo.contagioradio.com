Title: Todo está calculado para el “espectáculo bochornoso” de venta de ISAGEN
Date: 2016-01-12 16:20
Category: Economía, Nacional
Tags: ISAGEN, Jorge Robledo, RECALCA, Sintraisagen
Slug: todo-esta-calculado-para-el-espectaculo-bochornoso-de-venta-de-isagen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Isagen-2-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ucc 

<iframe src="http://www.ivoox.com/player_ek_10050851_2_1.html?data=kpWdl5WceZKhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbXjxdSYx9jYaaSnhqaejcjFsMTpzcbR0ZDUpdPVjMrZjYqpdoaskYqmpcrXtMbX1YqwlYqldcTpzdSYxNTHrNDmz9Tg0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jorge Robledo, Polo Democrático] 

###### 

<iframe src="http://www.ivoox.com/player_ek_10050855_2_1.html?data=kpWdl5WceZahhpywj5WdaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncaLpxc7cjZeSb7XjxdSYx9jYaaSnhqaejcjFsMTpzcbR0ZDUpdPVjMrZjYqpdoaskYqmpcrXtMbX1YqwlYqldcTpzdSYxJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Elber Castaño, Sintraisagen] 

##### [12 Ene 2016] 

El senador Jorge Robledo afirmó que pareciera que “todo está calculado para que el espectáculo bochornoso de sacrificar el interés nacional en beneficio de unos extranjeros” además porque el fin de año deja sin espacios para apelación legal por la “vacancia judicial”. [Sin embargo, la presión que se ha logrado a través de redes sociales a  nivel podría servir para reversar la decisión del gobierno](https://archivo.contagioradio.com/?s=isagen), señala Robledo.

Algunos puntos que no se han atendido a pocas horas de ser realizada la subasta son: En el reglamento de la subasta no se estableció qué sucedía si había un solo proponente y no se han resuelto de fondo los actos interpuestos ante el Consejo de Estado para intentar frenar la venta de una de las empresas públicas más rentables Colombia.

Así las cosas, **con una sola empresa proponente, no habrá subasta sino que habría una venta al más bajo precio**, es decir, el precio base. Para el caso de los trabajadores de **SINTRAISAGEN,** Elber Castaño, Secretario General del sindicato, afirma que incluso por encima del interés de los propios trabajadores están defendiendo el interés de todos los colombianos.

Según Castaño el sindicato no se ha planteado la posibilidad de que los trabajadores de ISAGEN pierdan su trabajo, que sería otra de las desventajas de dicha venta. “es un error garrafal del gobierno nacional” por las ventajas que le ofrece al país y a los ciudadanos la empresa y “el gobierno, para la historia quedará como una de las peores catástrofes” justo cuando estamos enfrentando el fenómeno del niño”. El sindicato ya interpuso 8 tutelas para intentar frenar el trámite pero hasta el momento no se ha resuelto si son o no aceptadas.

Recientemente por lo menos [40 congresistas enviaron cartas a las empresas que participarían en la subasta](https://archivo.contagioradio.com/gobierno-insiste-en-vender-isagen-y-dejar-de-recibir-460-mil-millones-anuales/), sin embargo, ante el conocimiento de que dos de ellas no participarán en la subasta los congresistas interpusieron un recurso de insistencia para obligar a los tribunales a suspender por segunda vez la venta de la empresa.

Por parte de la ciudadanía se está proponiendo un apagón general a las 8 pm, un plantón a partir de las 6 pm de hoy en la Plaza de Bolívar en Bogotá, el 13 de Enero en Bucaramanga y en Medellín.
