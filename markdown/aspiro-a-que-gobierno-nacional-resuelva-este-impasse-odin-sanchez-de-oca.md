Title: "Aspiro a que gobierno nacional resuelva este impase" Odín Sánchez de Oca
Date: 2017-01-02 06:42
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, Odín Sánchez
Slug: aspiro-a-que-gobierno-nacional-resuelva-este-impasse-odin-sanchez-de-oca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/odin_sanchez_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  El Heraldo ] 

###### [2 Enero 2017] 

El pasado 31 de diciembre la guerrilla del **ELN dio a conocer un vídeo como prueba de la supervivencia del secuestrad0 Odín Sánchez de Oca y la intención de su liberación**, quién se convirtió en los últimos meses en la condición por parte del gobierno nacional para reanudar y establecer la mesa de conversaciones.

El vídeo, de acuerdo con la alocución de Sánchez de Oca, fue grabado el pasado 10 de noviembre de 2016 y en él se refiere al proceso de paz que se estaba adelantando con esta guerrilla y afirmó que “le parece muy extraño que ante una exigencia sobre su liberación, el grupo que hoy lo tiene capturado no ha sido voluntarioso y que **existen condiciones que no se han cumplido**” señalando a que **aspira a que el gobierno nacional con la ayuda de los países garantes, resuelvan la situación  que se ha presentado.**

Además indico que “hay unos testigos de excepción que son los países garantes, ellos son los que deben informarle a la nación quién está diciendo la verdad”. Oca que lleva 9 meses de secuestro debido al cambió que se efectúo por su hermano, asevero “**la angustia se apodera de mí, sobre todo cuando no hago otra cosa que escuchar la radio y veo las dudas que rodean al proceso de diálogos con el Ejército de Liberación Nacional”**

De igual forma Sánchez de Oca, envió un saludo fraterno a sus familiares y envió su voz de solidaridad a la población chocoana que ha sido víctima de la fuerte ola invernal que se registra en el Choco e hizo un llamado al gobierno para que atienda estas problemáticas, que todos los años deja cientos de afectados en esta región del país.

De otro lado, el jefe negociador por parte del gobierno, Juan Camilo Restrepo, expreso que las pruebas de supervivencia son un paso positivo, "Sí (Odín Sánchez) debe ser devuelto, el Gobierno ha puesto como condición para iniciar la mesa formal de negociaciones con el ELN que sea devuelto sano y salvo a su hogar". Le puede interesar:["En riesgo vida de Odín Sánchez y las conversaciones con el ELN"](https://archivo.contagioradio.com/odin-sachez-eln/)

Se espera que el próximo 10 de enero se de una nueva cita entre el gobierno nacional y la guerrilla del ELN que logre destrabar el estado en el que se encuentra este proceso  y de paso a la instauración de la mesa de conversaciones en Quito. Le puede interesar: ["El próximo 10 de enero podrían destravarse conversaciones con el ELN"](https://archivo.contagioradio.com/el-proximo-10-de-enero-podrian-destrabarse-las-conversaciones-de-paz-con-el-eln/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
