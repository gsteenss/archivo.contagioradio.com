Title: Hurtan equipos y material sensible de oficina de la Agencia Prensa Rural
Date: 2017-10-25 13:31
Category: DDHH, Nacional
Tags: Libertad de expresión, medios de comunicación alternativos, Prensa Rural, robo a prensa rural
Slug: hurtan-equipos-y-material-sensible-de-oficina-de-la-agencia-prensa-rural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/prensa-rural.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [25 Oct 2017] 

Los integrantes de la Agencia Prensa Rural **denunciaron un robo en las instalaciones de su lugar de trabajo**. Afirmaron que los delincuentes hurtaron únicamente elementos de trabajo de la agencia como un computador y varios discos duros y atribuyeron estos hechos a un atentado contra la libertad de prensa.

De acuerdo con René Ayala, director de Prensa Rural, en la madrugada del 25 de octubre, **desconocidos entraron a la fuerza**, pues violentaron las chapas y las cerraduras de a la sede donde funciona el medio de información en Bogotá. Allí, Prensa Rural comparte locaciones con el proyecto de educación popular Corpeis y “se dirigieron exclusivamente a las instalaciones de Prensa Rural”. (Le puede interesar: ["Simulación de Saúl Cruz fue un ataque a la libertad de prensa"](https://archivo.contagioradio.com/simulacion-de-saul-cruz-viola-obligaciones-de-libertad-de-prensa-del-estado-fllip/))

Esto inquieta aún más al medio de información en la medida en que **“no ingresaron a ningún otro espacio, no se llevaron equipos y televisores de un auditorio”**. Lo que el equipo de la agencia ha podido establecer es que los delincuentes hurtaron computadores “con material sensible, un dron y varios discos duros con material de archivo”.

### **Robo puede configurarse como una agresión a la libertad de prensa** 

Ayala manifestó que, este hecho es una violación a la libertad de prensa y un **ataque contra los medios de comunicación alternativos** “que hemos venido cubriendo temas de gran trascendencia como los asesinatos sistemáticos de líderes sociales, la movilización social campesina y el paro agrario”. Dijo que esto puede ser una intención manifiesta de querer silenciar al medio de comunicación. (Le puede interesar:["CIDH hace un llamado a proteger a periodistas en Colombia"](https://archivo.contagioradio.com/llamado-de-la-relatoria-de-libertad-de-expresion-al-estado-a-prevenir-violencia-contra-la-prensa/))

Además, el material robado incluye **entrevistas y reporteria de campo que aún no ha pasado por etapa de pos producción** y esto “pone en riesgo a fuentes que tienen que estar protegidas”. Para ellos, es posible que el robo sea parte de una “maniobra como las que ha pasado con la persecución a los proyectos de prensa en Colombia”.

Pese a que los integrantes de Prensa Rural entablaron la denuncia correspondiente y la Policía junto con la Fiscalía hicieron presencia en el lugar, **“han calificado el hurto como un robo más** en el marco de la inseguridad de la ciudad”. Sin embargo, la agencia ha insistido en que “no se trató de un robo cualquiera, hace 20 días ya había habido un intento de ingreso a la casa”.

Finalmente, Ayala indicó que **continuarán al tanto de las investigaciones** y esperan que éstas se hagan de manera oportuna y efectiva. Piden que se garantice la vida y la integridad de las personas que hacen parte de Prensa Rural, pues consideran que el Estado debe garantizar el ejercicio libre de la prensa en el país, así como el derecho a la libertad de prensa y a informar.

<iframe id="audio_21688711" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21688711_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
