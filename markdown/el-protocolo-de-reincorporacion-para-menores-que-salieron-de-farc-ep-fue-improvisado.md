Title: Protocolo de reincorporación para menores de las FARC fue improvisado
Date: 2017-01-27 13:11
Category: Nacional, Paz
Tags: acuerdo de paz, Salida de menores de edad de las FARC-EP, Sergio Jaramillo
Slug: el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ninos-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [27 Ene 2017] 

El protocolo de reincorporación para menores de las FARC que salieron de las filas de este grupo guerrillero fue **"improvisado"**, **no contó con la planeada adecuación de los lugares que los acobijarían, ni con la información suficiente para los menores, son algunas de las** conclusiones del informe de la Comisión de veeduría que hizo el monitoreo y seguimiento a este proceso.

En ese sentido, Marcela Pinto, integrante de ANZORC y de la Comisión de acompañamiento, expresó que le **“sorprenden”** algunas de las afirmaciones  como las de Sergio Jaramillo, el Centro Democrático y la senadora Claudia López, ya que el informe demuestra  que la “**institucionalidad colombiana no está modificada en el  marco del acuerdo de paz y que existe una improvisación técnica** y logística para la recepción de los menores, pero que debe asumirse como una responsabilidad conjunta”.

Además, resaltó que hay una documentación previa frente al trabajo de la veeduría y un **informe final que se radicó el pasado 24 de enero** a la Comisión de Seguimiento Impulso y Verificación a la Implementación del Acuerdo Final (CSIVI) en donde se describían  estas problemáticas. Le puede interesar:["Información sobre la salida de menores de las FARC -EP debe ser corroborada y contextualizada"](https://archivo.contagioradio.com/informacion-sobre-salida-de-menores-de-las-farc-ep-debe-ser-corroborada-y-contextualizada/)

De acuerdo con María Eugenia Mosquera, integrante de CONPAZ y de la Comisión de Veeduría, existieron improvisaciones en este primer protocolo de los 13 menores de las FARC: “Los lugares que se pensaron inicialmente que serían comunitarios y familiares, se adecuaron en muy corto tiempo, sin tener en cuenta el trabajo previo la Comisión” y agregó que **tampoco se tenía toda la información que los niños y niñas requerían** para entender el proceso al que estaban llegando.

A su vez, María Eugenia afirmó que **la mayoría de los niños ya se encuentran con sus familias o en sus comunidades** y que el paso siguiente es verificar que en los lugares en donde están se cumpla con las condiciones que debe garantizárseles en términos comunitarios y familiares.["Se acabó la guerra para los menores de las FARC-EP"](https://archivo.contagioradio.com/se-acabo-la-guerra-para-menores-de-las-farc-ep/)

Esta veeduría se lleva realizando desde hace más de 9 meses y dentro del proceso están tres organizaciones: Comunidades construyendo Paz en los Territorios (CONPAZ), la Asociación Nacional de Zonas de Reserva Campesina (ANZORC) y la Colación contra la vinculación de niños, niñas y jóvenes al Conflicto Armado en Colombia (COALICO), de igual modo ya se especificaron los 10 puntos en donde llegaran los niños, **pero se espera que ambas partes adopten las recomendaciones hechas por la Comisión para el bienestar de los menores de edad.**

<iframe id="audio_16687806" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16687806_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
