Title: La guerra, el muro y el miedo
Date: 2015-11-04 07:00
Category: Cesar, Opinion
Tags: Cascos Azules de las Naciones Unidas, César Torres Del Río, Inmigrantes union europea, Israel, kurdos, libios, migrantes, muro union europea, sirios
Slug: la-guerra-el-muro-y-el-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Laszlo-Balogh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laszlo Balogh] 

#### **[Por [Cesar Torres del Río  ](https://archivo.contagioradio.com/cesar-torres-del-rio/)]** 

###### 4 Nov 2015 

[Hay quienes afirman que la Unión Europea es un ejemplo de civilidad y democracia por cuanto ha logrado eliminar la guerra entre sus integrantes; los mandarines académicos, en especial los colombianos,  son los primeros en defender el absurdo mencionado. Allá ellos, objeto de burlas. Eso sí, con gusto aceptan las guerras que son “externas” - las “humanitarias”, las “justas” - porque ellas son cruzadas contra el Mal que lograrán imponer el orden en los campamentos de “migrantes” (magrebíes, kurdos, sirios, libios …), eliminar a los terroristas (en Siria y en Palestina …), reconstruir países (Haití, Irak) y ayudar a levantar Muros para que las enfermedades de libertad, dignidad,  democracia y justicia no se expandan.]

*[Bienaventuradas]*[, entonces, las misiones de mantenimiento de la paz, encabezadas por los Cascos Azules de las Naciones Unidas, agente del Capital y de sus Estados europeos, asiáticos y del Medio Oriente. Los señores de la guerra encabezando las operaciones de paz que acabaran con las guerras por ellos engendradas. ¡Ironías de la Historia!]

[Pero también debemos considerar que las guerras tienen otros mecanismos para lograr sus fines de sometimiento. El de moda, dispositivo biopolítico, es el Muro.]

[Suma miles de kilómetros; en Europa, Asia, Medio Oriente y África, y claro en Norteamérica y en América Latina.  Es]*[moderno]*[ y producido según necesidades; se construye para]*[anticiparse]*[ al Mal protegiendo a pastores y rebaños inmaculados, para establecer fronteras religiosas y territoriales del Bien, para crear un]*[Nomos]*[ definitivo. El Muro es racionalmente planificado y burocráticamente administrado; segrega a “diferentes”, a  “extranjeros”, a indeseados y a refugiados. Es sinónimo de]*[muerte violenta]*[.]

[El Muro presenta modalidades diversas y por sí mismo es símbolo de guerra. En Israel, sus 700 kilómetros de largo cubren un 80% de territorio palestino, en Cisjordania; es por tanto un muro de]*[ocupación]*[. En India, la “subalterna” clase dirigente ha construido el Muro]*[anti-inmigración]*[ más extenso del planeta: 3.286 kilómetros de alambradas frente a Bangladesh. En Estados Unidos son cerca de 1.500 kilómetros los que “defienden” a los norteamericanos de los mexicanos; indudablemente es el Muro de la]*[“Seguridad Nacional”]*[. En Marruecos, los israelitas asesoraron la construcción de más de 2.700 kilómetros de Muro en el Sahara Occidental. España - la “democrática” de Franco y sus herederos borbones, socialistas democráticos, Aznares y Rajoys - ha construido el Muro en Ceuta, 8-9 kilómetros, y Melilla, con unos 12, para mantenerse separada de Marruecos.]

[El más reciente Muro es producto de las guerras “humanitarias” y “justas” contra Libia, Afganistán e Irak; hace parte también de la guerra del Estado sirio contra su población y contra los kurdos. Sin dudas, ha sido construido con base en exclusiones raciales, éticas y religiosas que provienen del tiempo de posguerra de la segunda contienda bélica. Con la actual ola de refugiados africanos y asiáticos que intenta llegar a los países de la Unión Europea, el Muro se extiende: Hungría frente a Serbia y Croacia, Eslovenia frente a Croacia, Austria frente a Eslovenia … Por supuesto, con otras consideraciones pero de la misma calaña es el Muro religioso que representa el fundamentalista y terrorista “Estado Islámico”, que en nombre del Islamismo decapita para imponer la]*[Sharia]*[.]

[De otra parte, el Muro tiene también que ser analizado como generado por el  Miedo. El Miedo de las castas dirigentes (subalternas o no), de las multinacionales, de la Bolsa, de los Ecocidas y sus firmas empresariales que programan “Cumbres Climáticas” para su tranquilidad financiera y su gracia espiritual, de los guerreristas imperialistas, de los nacionalistas patrioteros, de los Sionistas, de la Unión Europea, y de los mandarines, primeros ministros, reyes y presidentes; el miedo, sí, de todos ellos frente a las “primaveras árabes”, las resistencias populares latinoamericanas, las revueltas asiáticas, los ataúdes marinos de los refugiados, las Intifadas, la iconoclastia.]

[El Miedo no viene de ahora. José Ortega y Gasset, Alexis de Tocqueville (“El antiguo régimen y la Revolución”), August Cochin, Theodor Herzl, J. Bossuet, François Furet (“El pasado de una ilusión”), Pierre Chaunu, entre otros escritores conservadores, lo dejaron consignado en sus obras como una característica de los vencedores: miedo a la “masa”, a la “vulgata”, al “pueblo”, a la “chusma”, a los vencidos.  De ese Miedo Baudelaire dejó constancia, como testigo.]

[Entonces, frente a la Guerra, el Muro y el Miedo ¿tiramos los dados a lo Mallarmé?   ¿Esperamos el “milagro” acontecimental de Alain Badiou? ¿O procedemos con certeza en la cotidianidad del “pesimismo activo” contra la idea ilustrada de progreso, la modernidad y el Estado de Excepción permanente y mundial?]
