Title: Congreso de EEUU escucha a sociedad civil sobre avances del Acuerdo de Paz
Date: 2018-07-18 15:36
Category: DDHH
Tags: acuerdo de paz, colombia, EEUU
Slug: comision-del-congreso-de-eeuu-examinara-avances-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La prensa 

###### 18 Jul 2018 

La Comisión de Derechos humanos Tom Lantos,  del Congreso de los Estados Unidos, **ha convocado a una audiencia centrada en los derechos de las víctimas del conflicto colombiano y la implementación de los acuerdos de paz**, que tendrá lugar este jueves 19 de julio a partir de las 10:30 de la mañana hora local.

En la convocatoria, se resalta que durante los cuatro años de negociaciones, **los derechos de las víctimas estuvieron a la vanguardia**, destacando que para tal fin en la versión final del acuerdo se preveía la creación de la Comisión de la verdad, la unidad para la búsqueda de personas desaparecidas y el proceso de justicia transicional para los responsables.

Además, subrayan **la relevancia que en el acuerdo se dió a la participación de la mujeres  en la construcción de paz en el **[**país**], e incluyó importantes compromisos destinados a abordar las causas del conflicto de 60 años y garantizar así la no repetición, como la reforma rural integral y el desmantelamiento completo de las fuerzas paramilitares.

Por estas y otras razones, **desde la Comisión se examinarán los avances en la implementación del acuerdo a un año y medio de su firma**, particularmente en lo concerniente a los derechos de las víctimas y las mujeres. Será un espacio donde **testigos discutirán la situación de las áreas rurales, los esfuerzos de los opositores al acuerdo para cambiarlo, y la violencia contra los líderes sociales** y defensores de derechos humanos.

La lista de testigos esta conformada por **Elise Ditta**, Investigadora asociada del Instituto para los estudios de paz internacional; **Jenny Neme**, coordinadora de Promoción de Políticas de Diálogo Intereclesial por la Paz DiPaz; **Luis Fernando Arias**, Secretario general de la organización Indígena de Colombia ONIC y **Adam Isacson**, Director para la supervisión de defensa de la oficina en  Washington para Latino América WOLA.8

En la audiencia, que estará abierta a los miembros y personal del Congreso, así como al público y los medios de comunicación, **se buscará identificar algunas formas para que los congresistas puedan contribuir a la consolidación de la paz en un país como Colombia**, al que consideran aliado cercano a los Estados Unidos.

Para quienes deseen seguir la transmisión en directo se realizará a través del canal de la Comisión en Youtube en su [página web](https://humanrightscommission.house.gov) y estará enlazada por Contagio radio.
