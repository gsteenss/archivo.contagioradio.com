Title: Inoperancia de Alcaldía de Buenaventura no ha permitido retorno de comunidades Wounaan
Date: 2015-08-05 13:52
Category: DDHH, Nacional
Tags: Alcaldía de Buenaventura, buenaventura, Chachajo, Chamapuro, comunidad Wounaan, indígenas, Ley de Restitución de Tierras 1448, ORIVAC, Unión Agua Clara, valle
Slug: alcaldia-de-buenaventura-no-permite-retorno-de-comunidades-wounaan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/indigenas-desplazados-buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo, Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_6082036_2_1.html?data=l5WllJWXeo6ZmKiak5eJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3XwtHRh6iXaaK4wpDRx5CmucbiwtvS0NnZtsKfz9SY0srWscroxpDfx9nTts%2FjjMnSjcjTsdbiysmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manyolo Chamapuro, comunidad  Wounaan Unión Agua Clara] 

###### [5 Agosto 2015]

La comunidad indígena desplazada Wounaan decidió suspender el diálogo con la Alcaldía de Buenaventura, debido a que esta **no ha garantizado las condiciones de la Ley de Restitución de Tierras 1448, para que 56 familias** retornen a su territorio tras permanecer desplazados en el coliseo de buenaventura durante 7 meses.

“**El Alcalde no ha tenido voluntad de resolver nuestra situación**”, expresa Manyolo Chamapuro, integrante de la comunidad  Wounaan Unión Agua Clara. Él cuenta que llevan más de 7 meses resistiendo,  en el coliseo de Buenaventura donde viven en condiciones indignas de atención en salud, vivienda, educación y alimentación, lo que genera graves impactos en la identidad cultural y además **ha causado la muerte de dos menores de edad de la comunidad.**

En total son **583 personas entre niños, jóvenes, adultos y mayores** de las comunidades Wounaan Unión Agua Clara, Chachajo y Chamapuro que hacen parte del “Consejo de Gobierno Propio de la Organización Regional indígena del Valle del Cauca ORIVAC”. Ellos le exigen a la alcaldía que cumpla con lo estipulado en la Ley 1448, para que la comunidad pueda retornar a su territorio.

**Las exigencias de la comunidad Wounaan**

“La Alcaldía de Buenaventura nos viene engañando, no quiere garantizar la Ley 1448, exigimos vivienda, salud, educación y proyectos productivos para poder retornar" explica el integrante dela comunidad indígena.

En concreto se trata de la exigencia de condiciones para que los indígenas tengan una vida digna. De manera que se pide la creación de un centro de atención en salud, el mejoramiento de la estructura educativa, proyectos productivos para la alimentación y sustento de las familias, y finalmente se exige la construcción 42 casas para las 56 familias.

Por otro lado, la población exige condiciones de seguridad. Aunque la fuerza pública emitió un documento donde se expresa que hay tranquilidad para el retorno de las comunidades Wounaan, según un comunicado de la población, **“el concepto de seguridad sólo habla de las acciones de la guerrilla, pero no menciona ningún hecho del accionar paramilitar que ha generado más de una decena de desplazamientos en 2014 y otros en 2015”,** por lo que se asegura que hace falta voluntad del gobierno para desmontar las estructuras paramilitares que operan en el río San Juan y en el Casco urbano de Buenaventura.

Desde Consejo de Gobierno Propio de la Organización Regional indígena del Valle del Cauca ORIVAC, se hace un llamado al gobierno nacional y a los organismos internacionales y defensores de derechos humanos para que haya una respuesta efectiva a los planes de retorno de la comunidad.
