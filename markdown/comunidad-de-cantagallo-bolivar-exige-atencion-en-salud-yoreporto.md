Title: Comunidad de Cantagallo, Bolívar, exige atención en salud #YoReporto
Date: 2015-02-23 20:16
Author: CtgAdm
Category: Nacional, yoreporto
Tags: Cantagallo-Bolivar, Derecho a la salud, Derecho a la salud en colombia, EPS's, Ley estatutaria de la salud, protesta
Slug: comunidad-de-cantagallo-bolivar-exige-atencion-en-salud-yoreporto
Status: published

###### Foto: Equipo Jurídico Pueblos 

Según la denuncia del **Equipo Jurídico Pueblos**, desde el pasado 18 de febrero, habitantes del municipio de **Cantagallo, en el Sur del departamento de Bolívar**, están en una protesta pacífica por el pésimo servicio de salud que se presta en su municipio.  La comunidad denuncia  que ya van **dos años sin que ningún médico** atienda en el área rural del municipio y que actualmente solo hay uno en el casco urbano, que no da abasto para una población de 8.000 personas.

[![salud-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/IMG-20150221-WA0000.jpg){.wp-image-5178 .alignright width="303" height="227"}](https://archivo.contagioradio.com/nacional/comunidad-de-cantagallo-bolivar-exige-atencion-en-salud-yoreporto/attachment/img-20150221-wa0000/)

Los cantagalleros afirman que la actual administración municipal ha sido negligente frente a la grave crisis de salud que afrontan y que la alcaldesa Yaneth Cortéz se rehúsa a viabilizar la entrada de fondos para la **ESE Centro de Salud Cantangallo**.  Según dicen, estos fondos solo requieren de la firma de un convenio, pero la burgomaestre se niega a suscribirlo.

Para solucionar esta crisis de salud, la comunidad exige el **establecimiento de mesas de diálogo con la presencia de las distintas autoridades con competencia en la materia, de los entes de control, de la sociedad civil y de distintas figuras y personalidades políticas**.  Sobre el particular piden que se trate de funcionarios del nivel nacional, cuando haya lugar, y con capacidad de decisión.  Sin embargo, hasta el momento de redactar esta nota no se había instalado mesa alguna y la alcaldesa no ha atendido ni siquiera a los medios de comunicación.

Por lo pronto se efectúa un llamado al pueblo colombiano, a la sociedad civil, a las autoridades estatales y a la comunidad internacional para que se solidaricen con esta comunidad campesina que afronta una grave crisis y para que impidan cualquier tipo de represalia contra los cantagalleros que están defendiendo su vida y su salud.

Con información del Equipo Jurídico Pueblos.
