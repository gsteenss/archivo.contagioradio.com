Title: Discusion pública sobre un acuerdo que no es público es "un flaco favor a la paz"
Date: 2015-10-09 16:22
Category: Entrevistas, Paz
Tags: Acuerdo de Justicia, Comunicado FARC, Delegación de las FARC en la Habana, Delegación del gobierno en la Habana, proceso de paz, reparación y no repetición
Slug: discusion-publica-sobre-un-acuerdo-que-no-es-publico-es-un-flaco-favor-a-la-paz
Status: published

###### [Foto: pulzo] 

<iframe src="http://www.ivoox.com/player_ek_8890562_2_1.html?data=mZ2mkpqado6ZmKiak5mJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrnxNrgy9TSb9GZpJiSpKbGsMrXwpDg0cfWqYzpz5DOxdrJtsXjjNbix5DSs4zZ1JDdh6iXaaO1w9HWxdSPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Camila Moreno, ICTJ] 

###### [9 oct 2015] 

**Frente al comunicado de la delegación de las FARC**, que indican “que el gobierno ha faltado a su palabra” y las **declaraciones del gobierno** que desmintieron el comunicado e indicaron que el acuerdo de justicia, reparación y no repetición, no está totalmente terminado, así haya sido firmado y entregado a los países garantes del proceso de paz.

Camila Moreno directora del Centro internacional para la justicia transicional (ICTJ), indica que **esto “no es para sorprenderse”**, debido a que “muchos aspectos que si bien están acordados en el documento que no se conoce públicamente, **hay puntos que ameritan desarrollo** por la complejidad del tema”.

Moreno indica que esta diferencia de posiciones que tiene las partes negociadoras en la Habana “**tiene solución**”, evidenciando que “las dos partes están de acuerdo en que hay que volver a la comisión de juristas”.

La directora del ICTJ indica que al no conocerse la totalidad del documento firmado en la Habana “estamos todos en el terreno de la especulación” y las “partes son los únicos que saben porque dicen una cosa y la otra”.

Uno de los puntos sobre el que hizo hincapié Humberto de la Calle, Jefe negociador de paz del gobierno, fueron **las penas que deben pagar los que se sometan a la verdad y los que no**, sobre lo que Camila Moreno indicó que “esa discusión a la luz pública muestra que no hay un acuerdo absoluto en todos los puntos”, en donde se debe aclarar qué es restricción de la libertad y que es privación de las libertades, las cuales tienen consecuencias distintas.

La directora de la ICTJ indica que al evaluar esta situación se presentan dos realidades “**una realidad es la del comunicado de prensa y otra realidad paralela que no conocemos los demás es la discusión sobre el contenido del documento**”, el cual refleja que “las dos partes se refieren a un documento que la opinión pública no conoce”.

Así Moreno hace un llamado a la opinión pública de que los medios **hacen un "flaco favor" al ventilar discusiones que se están dando en privado y la opinión pública "no conoce”**, ya que Estas declaraciones polarizan la opinión del país, sin siquiera haber dado a conocer la totalidad del acuerdo.

A continuación el comunicado de las FARC...

<div class="itemIntroText">

##### **Una etapa decisiva para la paz** 

##### Terminamos este ciclo navegando contra las olas del tiempo en dirección al Acuerdo Final. Cuando iniciamos en Oslo la marcha de Colombia hacia la paz advertimos que un mal acuerdo podría ser peor que la propia guerra. Hoy, una voluntad de paz fortalecida por el anhelo de millones de compatriotas tendrá que sortear los escollos de una intransigencia inveterada que se resiste a entender que este no es un proceso de sometimiento, sino un diálogo entre partes iguales. Estamos resueltos a alcanzar la reconciliación sobre el cimiento de la verdad que sostiene la Jurisdicción Especial para la Paz, dispuestos a lograrla sobre cambios sociales donde el ciudadano del común sienta que mejoran y se dignifican sus condiciones de vida y que la exclusión política está llegando a su fin. 

</div>

<div class="itemFullText">

##### La paz no se construye con palabras que se lleva el viento ni con promesas que nunca se cumplen. Este proceso, que hoy transita su etapa más decisiva, debe alejar toda veleidad de desconfianza al honrar el compromiso de la palabra empeñada. Nos preocupa que por razones ajenas a la determinación de las FARC se produzca un desfase inconveniente entre la plena vigencia del componente de justicia y los plazos que nos hemos impuesto para la firma de la paz. No es justo que a estas alturas, obstrucciones artificiales dictadas por la avilantez nos hagan perder tiempo valioso. No es admisible que se siga recurriendo al expediente de imponer medidas unilaterales para resolver asuntos propios de la Mesa, como está ocurriendo ahora en el Congreso con el tema de la implementación. 

##### La construcción de la paz en nuestra patria, Colombia, requiere forjar confianza y reconocer que venimos de sesenta años de violencia fratricida sembrada de muerte, desolación, víctimas y victimarios, desajustes institucionales y una progresiva degradación del Estado. 

##### Esas seis décadas de descomposición han conducido al marchitamiento de la sociedad misma. Se han dejado al garete valores elementales. 

##### Desde los albores de nuestra rebelión comprendimos que la lucha sería larga, desgarradora y particularmente difícil. La búsqueda de la justicia y la igualdad y la defensa de nuestros derechos por la vía de las armas nunca fue un compromiso cualquiera. Se trató, ni más ni menos, de entregar la vida a cambio de defender la vida. La incomprensión, el atropello y persecución, y el desconocimiento de la dignidad de cientos de miles de nuestros compatriotas nos condujo a empuñar las armas a lo largo de episodios de historia que al ser recordada y narrada nos pone de presente una inmensa responsabilidad colectiva. 

##### El enfrentamiento a sangre y fuego de los partidos tradicionales, el abuso del poder de parte de gamonales de renombre-hoy como antes de ingrata recordación-, el desconocimiento del derecho al acceso a la tierra para quienes solo tenían como tarea su labranza, el hambre y las puertas cerradas que impedían el acceso a un mejor estar y un mejor porvenir, condujeron al desorden, a un derramamiento de sangre sin fin, a la polarización, y a la pasión irracional. Pero hoy hemos resuelto que la lucha armada entre todos y contra todos debe parar a fin de buscarse caminos de futuro reconociéndole a cada quien lo suyo, con inteligencia y generosidad. 

##### Cuando se cruzan por el camino incertidumbres y tropiezos por caprichos, contradicciones, egoísmos o simples malquerencias, apelamos a la palabra empeñada, a la prevalencia del honor y al valor de la verdad. Es el caso de los 75 puntos y el listado de sanciones que constituyen la Jurisdicción Especial para la Paz, presentados al mundo desde La Habana por el Presidente Juan Manuel Santos y el Comandante de las FARC-EP Timoleón Jiménez, en presencia del Jefe de Estado de la República de Cuba, Raúl Castro Ruz. La historia creadora de paz se escribe cumpliendo. Cumplió la Comisión de Juristas, cumplimos nosotros, seguimos a la espera de que lo firmado sea aceptado sin reticencias. Lo que pudiera faltar corresponde a un desarrollo que no puede variar para nada el contenido y alcance del sistema concebido, que como se ha afirmado, está cerrado. 

##### “La verdad os hará libres”, es el sello indeleble para una sociedad que debe anteponer la verdad por encima de cualquier consideración en momentos en que el arco toral del sistema de justicia acordado son las víctimas que esperan conocer la verdad, toda la verdad, y la aceptación de la responsabilidad de parte de quienes la relaten y asuman. Para esto se requiere valentía, honestidad y disposición para reconstruir el tejido social. De la mentira no nace la justicia, mucho menos la reparación, ni la no repetición. Altas dosis de carácter se necesitan para pararse ante el pueblo y el mundo y decir la verdad después de sesenta años de conflicto interno en el que no se dio un vencedor ni un vencido, pero que dejó, sí, tendidos en el suelo patrio cientos de miles de muertos y miles y miles de víctimas. 

##### Que sean entonces los que mayores responsabilidades dicen tener como representantes de la sociedad por desempeñar cargos públicos de alta jerarquía, quienes aprendan desde hoy lo que significa la palabra verdad. Sea la oportunidad para mencionar como mal ejemplo al señor Procurador de la Nación, quien revestido de unas facultades que pisotea sin consideración ni respeto alguno, se atrevió a afirmar hace apenas unas horas que "Las FARC siempre ha colocado como condición la judicialización del expresidente Uribe”, y a continuación sugiere alianzas de la organización insurgente con instancias del Estado para lograr tal propósito. Porque venimos rechazando el derecho penal del enemigo, las FARC nunca hemos hecho propuesta alguna pensando en un único destinatario sino buscando lo mejor para todo nuestro pueblo y ponerle fin a la impunidad. La sinvergüencería no puede ser un instrumento de paz. El señor Procurador no tiene vergüenza; lo que debiera hacer es posibilitar que se acabe la impunidad de aquellos que se creen vacas sagradas porque detentan el poder. 

##### DELEGACIÓN DE PAZ DE LAS FARC-EP 

------------------------------------------------------------------------

</div>
