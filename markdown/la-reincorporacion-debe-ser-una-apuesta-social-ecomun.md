Title: La reincorporación debe ser una apuesta social: Ecomún
Date: 2019-03-04 12:07
Category: Expreso Libertad
Tags: ECOMÚN, Expreso Libertad, reincorporación
Slug: la-reincorporacion-debe-ser-una-apuesta-social-ecomun
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-27-at-2.43.19-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Mar 2019] 

Desde que se firmaron los Acuerdos de Paz, las y los excombatientes de la entonces guerrilla FARC han decidido dar un paso hacia la reincorporación social, sin embargo, pese que muchos fueron los compromisos por parte del gobierno, para hacer más fácil este tránsito, pocas han sido las garantías.

En este Expreso Libertad hablamos con Fanny Castellanos y Julian Cortés, voceros de Ecomún, la cooperativa de FARC, que comentaron cómo a pensar de los incumplimientos por parte del Estado, la creatividad y el empuje de las personas ha permitido la construcción de proyectos económicos para garantizar un real proceso de reincorporación.

<iframe id="audio_33049948" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33049948_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
