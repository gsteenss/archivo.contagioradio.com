Title: Jayder  Quintana, defensor de DDHH es asesinado en Cajibío
Date: 2020-10-04 09:04
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Cauca, colombia, defensores de ddhh, lideres sociales
Slug: jayder-quintana-defensor-de-ddhh-es-asesinado-en-cajibio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/EaPSR4OXQAAvhUo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo 4 de octubre se denunció el asesinato de **Jayder Quintana integrante de la Asociación de Trabajadores Campesinos del municipio de Cajibío** en el departamento del Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/leonardonzalez/status/1312754855556022274","type":"rich","providerNameSlug":"twitter"} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/leonardonzalez/status/1312754855556022274

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Jayder Quintana además de ser parte de la Asociación, conformaba la Federación Sindical Unitaria Agropecuaria, la Asociación Nacional de zonas de Reserva Campesina del proceso de unidad popular del suroccidente colombiano; además era integrante de la Coordinación Social y política de [Marcha Patriótica](https://www.marchapatriotica.org/) en el departamento del [Cauca](https://archivo.contagioradio.com/dos-adultos-y-un-menor-de-edad-son-asesinados-en-caceres-antioquia/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información preliminar el homicidio Jayder Quintana, **ocurrió en el centro poblado del municipio de Cajibío sobre las 9:00 de la noche del sábado 3 de octubre,** mientras el defensor de DDH se encontraban en una panadería, fue abordado por hombres armados quienes lo atacaron con arma de fuego en 9 oportunidades causándole la muerte de manera instantánea.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
