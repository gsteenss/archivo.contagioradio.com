Title: Familiares de desaparecidos piden que se agilice la búsqueda de sus seres queridos
Date: 2016-01-22 17:03
Category: DDHH, Nacional
Tags: Desaparición forzada, FARC, proceso de paz
Slug: familiares-de-desaparecidos-piden-que-se-agilice-la-busqueda-de-sus-seres-queridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/desapariones-forzadas-e1453500201709.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Amnistía Internacional 

###### [22 Ene 2016] 

Víctimas y organizaciones de derechos humanos, se reunieron en la Mesa de Trabajo sobre Desaparición forzada, como resultado de los compromisos del gobierno nacional y la delegación de paz de las FARC, quienes han asumido la responsabilidad de esclarecer los miles de casos sobre desaparición forzada en medio del conflicto armado en Colombia.

En el encuentro, las organizaciones hicieron  un llamado a las partes a agilizar la implementación de las medidas inmediatas de búsqueda de personas desaparecidas, y a hacerlos participes de todo este procedimiento.

***Comunicado Primera Reunión Regional Autónoma- Diagnostico y recomendaciones sobre el Acuerdo Humanitario de personas dadas por desaparecidas***

*Los días 20 y 21 de enero del 2016, alrededor de 150 personas, pertenecientes a más de 40 organizaciones de víctimas y especializadas de 14 departamentos del país, nos congregamos para realizar un diagnóstico y recomendaciones sobre el contenido del acuerdo 062 de la Mesa de Negociación de La Habana. El acuerdo trata sobre medidas inmediatas en la búsqueda de personas desaparecidas, fortalecimiento institucional y creación de una Unidad Nacional de Búsqueda.*

*Las recomendaciones que surgirán de este encuentro y los posteriores programados para el 4 y 16 de febrero, serán entregadas por las organizaciones participantes a las partes de la mesa de negociaciones y países garantes, en el plazo establecido por la Mesa de diálogos de Paz a mediados de febrero. Estas recomendaciones están dirigidas a garantizar la implementación del Acuerdo cumpliendo el espíritu del mismo que busca promover una amplia participación de los familiares de las víctimas, organizaciones de derechos humanos y especializadas.*

*Con urgencia y ante la sentida necesidad de avanzar en la implementación de este Acuerdo, hacemos un llamado para que antes de terminar el mes de enero, podamos avanzar en los siguientes puntos:*

1.  *Exhortamos a las partes en la Mesa de Diálogos, a concretar y brindar con urgencia al CICR la información pactada en el Comunicado 062 de la Mesa de Negociaciones. Asimismo, hacemos un llamado a las instituciones competentes a expresar una real voluntad política para buscar, identificar y recuperar a las víctimas de desaparición de forma inmediata.*

<!-- -->

2.  *Hacemos igualmente un llamado a las partes a dar a conocer a la Mesa de Trabajo sobre Desaparición Forzada de la Coordinación Colombia Europa Estados Unidos, el Protocolo o Plan Especial de medidas inmediatas de búsqueda, identificación y entrega digna de restos de personas dadas por desaparecidas, acordado por la Mesa de Diálogos con el apoyo técnico del CICR y Medicina Legal. Para las organizaciones de víctimas y de DDHH es importante conocerlo, aportar a su contenido y contribuir en su implementación, partiendo de sus experiencias acumuladas y dar respuesta a las expectativas de los familiares.*

<!-- -->

3.  *Solicitamos también a las partes habilitar un escenario de información periódica sobre los avances que se hayan realizado en la implementación de las medidas inmediatas de búsqueda, identificación y entrega digna de cuerpos de personas desaparecidas, así como información sobre los planes de inversión y recursos financieros comprometidos para este fin. En este marco, consideramos pertinente la realización de informes de seguimiento trimestrales a los avances de medidas inmediatas con posterioridad al 18 de febrero.*

*Para las organizaciones participantes en la reunión, la construcción de confianza necesaria para favorecer el acuerdo pasa también porque cesen las amenazas, hostigamientos y persecución a la labor de los familiares de las víctimas, así como de los defensores y defensoras de derechos humanos que les acompañan.*

*Bogotá, 21 de enero del 2016*
