Title: Denuncian agresiones contra Judit Maldonado, candidata a gobernación de Norte de Santander
Date: 2015-08-18 15:02
Category: Nacional, Política
Tags: Candidatura Gobernación, Derechos Humanos, Elecciones 2015, Judith Maldonado, Norte de Santander, Unión Patriótica, UP
Slug: denuncian-agresiones-contra-judit-maldonado-candidata-a-gobernacion-de-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Judith-Maldonado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Youtube.com 

<iframe src="http://www.ivoox.com/player_ek_7060201_2_1.html?data=mJWjkpeUdY6ZmKiak5uJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb8Lb08rgy9TSqdSfxNTb1tfFb6vpxc7hjbLFsMXjz8bR0YqWh4zXwtPRy8nFuMKfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Judith Maldonado, candidata Gobernación Norte de Santander] 

###### [18 ago 2015] 

La **candidata a la Gobernación de Norte de Santander Judith Maldonado** denunció que el día sábado, mientras viajaba hacia Río de Oro (Norte de Santander) a adelantar una agenda de trabajo para su campaña, **fue detenida alrededor de dos horas por miembros de la brigada treinta alegando que cumplían procedimientos ilegales**.

Inicialmente la candidata junto a su grupo de trabajo pasaron el reten firmando una salvedad por motivos de orden publico, sin embargo cuando regresaron un Cabo y un Coronel principalmente los retuvieron **“querían que bajáramos del vehículo, pedir los documentos, querían requisar todas las pertenencias y hacerle registro al vehículo”**.

Judith Maldonado relata que afortunadamente en el lugar había señal de celular y se pudieron realizar las llamadas correspondientes. Después de dos horas llegó la policía Nacional “a pesar de que tienen la competencia legal no tenían orden de allanamiento y registro. **Violaron derecho a la movilidad y a la participación política**, porque íbamos a otra reunión a la que no se pudo asistir”

**Estos incidentes se presentan en el marco de una campaña oscura, además que se han recibido amenazas muy peligrosas, como indica la candidata** “no es por el programa de gobierno, sino por como se hizo”, puesto que ha sido un trabajo colectivo entre el equipo de campaña y las comunidades.

**Este programa de gobierno tiene tres temáticas principales** que son el desempleo; el acceso a la educación; la construcción de un departamento en paz y donar de los fondos que hay para la alcaldía el 70% a las organizaciones sociales.

La candidata indica que los partidos tradicionales están sintiendo la presión desde abajo, su candidatura “tiene que enfrentar y padecer lo que padece el pueblo” cuestión que influye en esa persecución que denuncia la candidata **“Me preocupa es el miedo que tengan los partidos tradicionales. La forma como ellos asumen el miedo es muy peligrosa”**.
