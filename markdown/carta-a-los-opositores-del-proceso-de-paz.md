Title: Carta a los opositores del proceso de paz
Date: 2016-08-01 09:35
Author: AdminContagio
Category: Cindy, Opinion
Tags: proceso de paz, Proceso de paz en Colombia
Slug: carta-a-los-opositores-del-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/uribe-no-a-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:AP 

#### **[Cindy Espitia Murcia](https://archivo.contagioradio.com/cindy-espitia/) - [@CindyEspitiaM](https://twitter.com/CindyEspitiaM)** 

Estimados,

[Estoy cansada de leer sus comentarios indolentes acerca del proceso de paz. Me fastidia ver que la lucha de la oposición contra Santos ha permeado algo con tanta trascendencia e importancia como lo es el proceso con las Farc que, en términos prácticos, constituye un paso gigante en la búsqueda de la paz del país. Me indigna que ante cualquier embarrada que cometa Santos - con independencia del tema - sea utilizada por la oposición para atacar el proceso y, sobretodo, me impacienta la reacción de muchos, con la pregunta ¿Esa es la paz de Santos?]

¡Un momento! ¿Cuál paz de Santos? ¿Por qué no comprendemos que lo que está de por medio - realmente - es la paz de TODO un país? Pero ¿saben? esa frase ilustra más de lo que imaginan y evidencia por qué es que estamos como estamos. Este cuestionamiento que señala que la paz es de una sola persona demuestra única y exclusivamente que no nos hemos puesto la camiseta. Que reiteramos que la paz es un derecho, pero se nos olvida que demanda de nosotros un esfuerzo; omitimos conscientemente que tenemos el deber de aportar para su construcción. Sin embargo, por supuesto, es mucho más cómodo pelear con el televisor o criticar por redes sociales. Claramente, es menos demandante cruzar los brazos y decir que Santos va a “entregar el país al terrorismo” sin siquiera haber leído los acuerdos o sin haber reflexionado sobre la forma de aportar al cambio.

Ahora bien, muchos critican que Santos utilizó la paz en su campaña para ganar la presidencia. Yo cuestioné en su momento también esta actuación de él. Pero, en últimas, creo que si Colombia no quiere la instrumentalización política de la paz, en su momento podrá evitarlo con el voto en las urnas. Pero no con el voto en contra de la paz en el plebiscito, sino el voto en contra de las personas que en las próximas elecciones busquen alcanzar una curul fundamentando su campaña en lo que nos pertenece a los colombianos. De lo contrario, ¿no creen que es desproporcionado negarle a todo un país la posibilidad de vivir en paz con el fin de que unos pocos no saquen rédito político? Como les digo, eso se puede impedir, pero no a costa de la paz.

Debo decir también que, por mucho tiempo, he tratado de hallarle un significado y un contenido a la frase de “paz sin impunidad”, que es la respuesta a todo argumento a favor del proceso. Pero ¿Qué es la paz sin impunidad? ¿Qué supone? ¿Qué debería hacer el Estado para garantizarla?

Al respecto, tengo que señalar que no he hallado una respuesta coherente a estos interrogantes que puedan derivarse del discurso de los opositores de la paz. Al no hallar una postura consistente, decidí tratar de inferir el contenido de la frase de los propios hechos. Así que, teniendo en cuenta que el Centro Democrático, en cabeza de Álvaro Uribe, ha posicionado esta frase, acudí directamente a los resultados del proceso de desmovilización de las AUC en el 2003, que supuse podía ser definitivamente un referente, un modelo a seguir, completamente intachable, capaz de saciar ese compromiso por la justicia, por la verdad y por los derechos de las víctimas que, hoy dicen liderar y representar los opositores de la paz.

Al revisar, sencillamente encontré 1) que al día de hoy se han fortalecido aún más estos grupos - realidad que no es reciente o nueva pues desde la desmovilización, organizaciones internacionales denunciaron la transformación de estos grupos -; 2) que habiendo transcurrido 6 años de la Ley de Justicia y Paz, sólo se habían emitido 10 sentencias por la flexibilidad e inoperancia propia de la Ley presentada por el gobierno de Uribe; 3) que las víctimas siguieron sufriendo amenazas y atentados y 4) que múltiples órganos de derechos humanos se opusieron a las prerrogativas de la Ley, que eran bastante amplias, y que  Uribe hizo caso omiso a las recomendaciones. Hoy es realmente curioso ver cómo el uribismo cita frases del sistema interamericano o universal de derechos humanos - de manera descontextualizada además - para oponerse al proceso de paz con las Farc, cuando Uribe reprochó y omitió completamente la posición de estos órganos en su Gobierno.

De esto, al menos imagino que lo que Uribe y sus seguidores quieren del proceso de paz con las Farc es que se haga todo lo contrario a lo que él hizo con la desmovilización de las AUC. De no ser así, creo que existen graves problemas conceptuales sobre su percepción de la verdad, la justicia y la impunidad.

Ahora, entiendo que no todo los opositores son uribistas. Y que muchos se oponen al proceso por la posibilidad de que se ofrezcan amnistías o penas alternativas a guerrilleros desmovilizados. Sin embargo, quisiera hacer varias precisiones; 1) no es posible acabar a las Farc militarmente; 2) la negociación es la única salida y al ser una NEGOCIACIÓN supone una concesión de parte y parte; 3) los delitos cometidos durante el conflicto no pueden ser procesados como delitos ordinarios, por lo que figuras como la priorización y la selección de casos - aplicados en contextos similares por otros Estados y Tribunales - responden de manera eficaz al fin de develar y desentrañar estructuras macrocriminales; 4) no se otorgarán amnistías para quienes hayan cometido crímenes de guerra y delitos de lesa humanidad - esto en pleno cumplimiento de los estándares internacionales y 5) la justicia no se puede entender exclusivamente como sinónimo de cárcel. La justicia retributiva debe ser acompañada de justicia restaurativa. De nada sirve tener a todos los guerrilleros en la cárcel, siendo sostenidos por el Estado, si no aportan a la verdad y a la reparación de las víctimas. ¿Por qué no aceptar mecanismos que permitan, por ejemplo, que ellos puedan trabajar y aportar al desarrollo de una actividad económica del país cuyos frutos sirvan para la reparación de las víctimas?.

Sin embargo, mi interés no es convencerlos ni que cambien de opinión. Mi interés con esta carta es que, además de que comprendan que también es responsabilidad de ustedes la paz, así la guerra no los toque, reflexionen sobre su verdadero rol en este momento histórico para Colombia.

[Porque, ¿Están conscientes de que al promover la abstención o el NO en el plebiscito están privando al país - y a ustedes mismos -  de la posibilidad de intentar construir la paz? ¿No creen que con la abstención y el NO, tanto la búsqueda de la paz como sus ideas sobre la justicia se sepultarán pues no existirá un escenario para que ustedes puedan aportar? ¿No consideran que sus ideas son valiosas y que podrían aplicarse? ¿No creen que además de ser críticos pueden ser propositivos? ¿Por qué no orientar todos sus esfuerzos - que actualmente emplean para oponerse a la paz- más bien para hacer un estricto control en el posconflicto, que busque propender por la plena garantía de los derechos de las víctimas?]

[La verdad, a pesar de que no comparto sus argumentos, tengo la plena certeza de que muchos de ustedes actúan, salen a las calles o comentan en redes sociales buscando lo que consideran mejor para Colombia. Y si su objetivo es así de altruista, entonces, no priven a todo un país - a ustedes, a sus padres, a sus hijos o amigos - de la posibilidad de construir la paz. No busquen sepultar la paz, sin haber puesto todo lo que estaba en sus manos para resucitarla. Los invito más bien a que mantengan su convicción, buscando hacer realidad sus exigencias- por supuesto,]**aquellas razonables y justificadas**[- pero en el escenario adecuado, que es la implementación de los acuerdos. Qué valioso sería su aporte y su escrutinio en un eventual escenario de posconflicto.]

¡La paz no es de Santos! ¡La victoria o derrota de la paz no será de Santos, será nuestra!

[Cordial y respetuosamente,]

Cindy Espitia

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
