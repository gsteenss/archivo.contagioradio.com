Title: Listos los 11 integrantes de la Comisión de la Verdad, la convivencia y la no repetición
Date: 2017-11-09 12:45
Category: Nacional, Paz
Tags: comision de la verdad
Slug: listos-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/lista-de-comision-de-la-verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Nov 2017] 

El Comité de escogencia publicó este 9 de Noviembre la lista de las personas que integrarán la Comisión de la verdad, la convivencia y la no repetición. Las 11 personas seleccionadas hicieron parte de un grupo de 193 personas que se presentaron como aspirantes. Además se anunció que el presidente de la Comisión será el Sacerdote Jesuita Francisco de Roux que ha participado en procesos de reconciliación en todo el mundo.

Las personas que harán parte de esta comisión son **Saul Franco, Lucía González, Carlos Beristain, Alejandra Restrepo, Alfredo Molano, Carlos Ospina, Marta Cecilia Ruiz, Angela Murillo, Maria Patricia Tobón, Alejandro Valencia Villa** y tendrá una duración de tres años, con un periodo anterior de preparación de seis meses una vez se pongan en marcha las condiciones necesarias que hacen parte del SIVJRNR.

### **Objetivos de la Comisión de la Verdad** 

En el acuerdo firmado entre el gobierno y las FARC se contemplan tres grandes objetivos. Contribuir al esclarecimiento de lo ocurrido en el marco del conflicto, promover y contribuir en el reconocimiento de las víctimas y promover la convivencia en los territorios.

### **Las funciones de la Comisión** 

El acuerdo incluye una serie de funciones sobre las cuales deberán trabajar los comisionados. Investigar sobre todos los elementos del mandato, implementar una estrategia de difusión, pedagogía y relacionamiento activo, crear espacios en los ámbitos nacional, regional y territorial, en especial audiencias públicas, con el fin de escuchar las diferentes voces y de promover la participación de los diferentes sectores de la sociedad, incluyendo de quienes hayan participado de manera directa o indirecta en el conflicto.

Este escenario también deberá asegurar la transversalidad del enfoque de género en todo el ámbito de trabajo de la Comisión, elaborar un informe final y rendir cuentas de manera periódica sobre las actividades y gestiones desarrolladas para el cumplimiento de sus funciones.[Lea también: la ardua tarea de encontrar la verdad de la guerra en Colombia.](https://archivo.contagioradio.com/comienza-convocatoria-para-integrantes-de-la-comision-de-la-verdad/)

###### Reciba toda la información de Contagio Radio en [[su correo]
