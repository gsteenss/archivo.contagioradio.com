Title: Carrasquilla se equivocó de fórmula para reducir el desempleo
Date: 2019-08-08 18:31
Author: CtgAdm
Category: Economía, Política
Tags: DANE, desempleo, Dólar, economia
Slug: carrasquilla-equivoco-reducir-desempleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Carrasquilla.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MinHacienda  
] 

Luego que el Director del Departamento Administrativo Nacional de Estadística (DANE) diera a conocer que la cifra de desempleo en junio de este año fue de 9,4% (un o,3% más que en junio de 2018), el ministro de hacienda Alberto Carrasquilla, dijo: "no entiendo a cabalidad qué fue lo que sucedió, ni cuáles medidas podemos  tomar para corregir la problemática". Ante la respuesta, el representante a la cámara y magíster en Economía David Racero anunció un debate de control político a Carrasquilla, y explicó por que este fenómeno se presentó.

Racero declaró que en muchas ocasiones se pierde el detalle de lo que sucede y la noticia se reduce a una cifra, en ese sentido, dijo que de la estadística habla de **cerca de 270 mil personas que no lograron conseguir trabajo en junio**, y 140 mil personas más que lo perdieron. Es decir, no solo la demanda del trabajo no aumenta, sino que incluso se pierden empleos. (Le puede interesar: ["La reforma tributaria de Carrasquilla afectaría al 90% de los colombianos"](https://archivo.contagioradio.com/carrasquilla-afectara-colombianos/))

### **¿Por qué Carrasquilla no sabe qué pasa con el desempleo?**

Racero indicó que lo de Carrasquilla tiene que ver con el "prejuicio" económico que usa para abordar la realidad: acudiendo a la idea de que brindando beneficios tributarios a las empresas, estas invertirán más recursos en la generación de empleo. Pero según el Magíster en economía, **no hay ningún documento académico que justifique ese argumento.** (Le puede interesar: ["La reforma tributaria de Duque: Los ricos más ricos y los pobres más pobres"](https://archivo.contagioradio.com/reforma-tributaria-duque-ricos-pobres/))

Adicionalmente, el Representante señaló que en el país aún no se conocen los efectos de la reforma tributaria aprobada en diciembre por el Congreso, llamada Ley de Financiamiento, "cuyo principal objetivo fue reducir el impuesto a las empresas". En esa medida, criticó que este Gobierno se presentará como el de la productividad, la economía naranja y el beneficio para la economía, mientras parece ser lo contrario, y concluyó que "su política económica es un fracaso, pero puede empeorar".

### **"Tenemos una debilidad en nuestra matriz productiva"**

En entrevista con medios de comunicación empresariales, el presidente Duque afirmó que la llegada de migrantes y refugiados venezolanos al país es, en buena medida, lo que explica el crecimiento de la tasa de desempleo. Sobre este tema, Racero aseguró que el proceso de movilidad humana desde el vecino país es de carácter coyuntural, pues sí afecta el que llegue mano de obra dispuesta a emplearse por montos inferiores a los establecidos, pero ello no es la causa del problema.

De acuerdo al Congresista, la causa está en que "tenemos una debilidad en nuestra matriz productiva"; es decir, Colombia depende de la explotación de sus recursos naturales y la mayoría de la riqueza se concentra en pocas manos. Una muestra de ello es que **el sector con mayor crecimiento es el financiero, uno de los que genera menos plazas de empleo**. (Le puede interesar: ["El que la hace la paga no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

Para "hacer frente a un proceso de decrecimiento de la economía", Racero sostuvo que era necesario implementar políticas contracíclicas en las que el Estado tiene el papel "fundamental" de generar inversión económica. En cambio, cuestionó que en el presupuesto anual de 2020 se esté pensansando en una reducción del 14% del presupuesto en inversión, justo lo contrario a lo que el Congresista propuso, razón por la que solicitó la realización de un debate de control político a Carrasquilla.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
