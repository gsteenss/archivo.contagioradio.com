Title: Campesinos del Catatumbo logran que gobierno ratifique compromiso de suspender erradicación
Date: 2017-09-21 12:59
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, COCCAM, cultivos de hoja de coca, Cultivos de uso ilícito, Erradicación Forzada
Slug: gobierno-se-comprometio-a-no-erradicar-forzadamente-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Sept. 2017] 

Luego de una reunión sostenida entre la Asociación Campesina del Catatumbo (ASCAMCAT), COCCAM y el Gobierno Nacional se logró **el compromiso gubernamental de cumplir con lo pactado entre las partes el pasado 9 de septiembre** en el que se aseguran no van a erradicar y que será retirado el ESMAD del corregimiento de Loba, en el municipio de Sardinata.

Debido al alcance de estos nuevos acuerdos “las comunidades definimos levantar el asentamiento campesino de la vereda El Km 30 (…) y se levanta de manera provisional las movilizaciones escalonadas”.

Dicen que evaluarán el avance de los compromisos para **definir si el próximo 12 de octubre deciden continuar con la convocatoria de movilización.  **

Así mismo, las organizaciones campesinas aseguran que el Gobierno se comprometió a **priorizar y socializar el Programa Nacional de Sustitución de Cultivos de Uso Ilícito** - PNIS- en los municipios cocaleros de Norte de Santander. Le puede interesar: [Paramilitares amenazan a líderes de Catatumbo y Santander](https://archivo.contagioradio.com/5-lideres-de-norte-de-santander-fueron-declarados-objetivos-militares-por-los-urabenos/)

Además, el Gobierno le dijo a las comunidades que realizará la **verificación de las violaciones de derechos humanos sucedidas por integrantes de la Fuerza Pública,** de manera especial en La Vereda El Km 30, donde en la actualidad se realiza el asentamiento de campesinos que se oponen a la erradicación forzada.

“**No se tomarán acciones jurídicas en contra de los voceros, líderes y comunidades** en general que hayan participado del asentamiento campesino” dicen el comunicado entregado por ASCAMCAT y COCCAM. Le puede interesar: [Erradicación forzada en el Catatumbo afecta a más de 300 mil personas](https://archivo.contagioradio.com/erradicacion-forzada-en-el-catatumbo-afecta-a-mas-de-300-mil-personas/)

Hasta el próximo 3 de octubre se volverán a reunir las partes para definir el cronograma y coordinar el lanzamiento del Consejo Asesor Territorial del Catatumbo, en el que participarán las organizaciones que hacen presencia en ese territorio.

La fecha máxima establecida para que se haga la firma del acuerdo municipal de sustitución en el municipio de Sardinata en Norte de Santander, será el 4 de noviembre. Le puede interesar: [Comunidades del Catatumbo empiezan a construir agenda de paz](https://archivo.contagioradio.com/42740/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
