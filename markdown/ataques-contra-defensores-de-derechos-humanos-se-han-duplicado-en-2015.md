Title: En 2015 se han duplicado las agresiones contra Defensores de DDHH
Date: 2015-08-18 14:52
Category: DDHH, infografia, Nacional
Tags: Derechos Humanos, Neoparamilitarismo, Paramilitarismo, postconflicto, proceso de paz, Programa Somos Defensores
Slug: ataques-contra-defensores-de-derechos-humanos-se-han-duplicado-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Derechos-humanos-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Elespectador.com 

<iframe src="http://www.ivoox.com/player_ek_7052229_2_1.html?data=mJWilJeWfY6ZmKialJWJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfk5Wel5DXqYzcwtOYxtrUsMrXwsncjdHFt4zVyNfS1c7TssbnjMjc0NnWpYzYxsvS0NjTto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Guevara, coordinador programa "Somos Defensores"] 

###### [18 ago 2015] 

“Vale la pena señalar que en los periodos de gobierno del Presidente Juan Manuel Santos **han sido amenazados (aprox.) 1500 defensores y 320 han sido asesinados**, sin que se haya judicializado a sus responsables”. Este es uno de los datos que arroja el **informe “Los Nadies”**, realizado por el programa Somos Defensores.

Carlos Guevara coordinador de este proceso indica que este informe comparado con el del año pasado “imagina” **muestra un incremento aproximadamente del 105% de ataques contra los defensores de derechos humanos**, comparados con los del año pasado.

Los Nadies, arroja datos alarmantes frente a las condiciones en las que se están llevando estos ataques, debido a que son dirigidos contra personas que están trabajando con las comunidades en pro del proceso de paz “***el año pasado habían 194 agresiones y este año hemos encontrado 399***”.

Somos Defensores reúne las denuncias que colocan los defensores de derechos humanos en las distintas regiones, aunque Guevara indica que muchas no son conocidas debido al temor y a las represalias que puedan ser tomadas por las distintas organizaciones que se dividen entre **paramilitares (72%); desconocidos (22%) y agentes estatales (5%).**

**En lo que va corrido de este primer semestre van 34 homicidios**, de los cuales en 13 de ellos los defensores, quienes trabajaban en las regiones habían denunciado amenazas antes de su asesinato. **Guevara afirma que “no se están teniendo presentes estos lideres para un postconflicto”**, siendo estos fundamentales porque conocen los procesos en los territorios, conocen a la población y a las comunidades, siendo importantes para la paz territorial.

El informe realizado por el Programa Somos Defensores **es de vital importancia en cuanto a que en un escenario de postconflicto esta información será clave, para contar la historia del conflicto en Colombia**, además para que no se pierdan en la historia dichas agresiones y queden en la impunidad.

Sin embargo, otro dato que arroja Carlos Guevara de este informe es que desde el 2010 hasta la fecha la justicia colombiana no ha llevado a ninguno de los responsables a una condena, también convirtiéndose en uno de los agresores también a los defensores de derechos humanos, **dejando un 95% de los casos en la impunidad**.

**Este informe será presentado ante distintas entidades internacionales** como el Departamento de Estados Unidos, la Unión Europea, el Consejo de la ONU, esto para que desde afuera se ejerza un llamado al gobierno frente a estos casos, siendo lamentable que tenga que ocurrir esto para que suceda algo con la justicia.

[![Infografía Somos Defensores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Infografía-Somos-Defensores.jpg){.aligncenter .size-full .wp-image-12347 width="1840" height="3165"}](https://archivo.contagioradio.com/ataques-contra-defensores-de-derechos-humanos-se-han-duplicado-en-2015/infografia-somos-defensores/)

[Los Nadie Informe Semestral Siaddhh2015](https://es.scribd.com/doc/275052880/Los-Nadie-Informe-Semestral-Siaddhh2015 "View Los Nadie Informe Semestral Siaddhh2015 on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_65169" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/275052880/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pj4KqEv1UwCKR55bsWGZ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6872616323417239"></iframe>
