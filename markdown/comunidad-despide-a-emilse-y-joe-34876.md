Title: Comunidad despide a Emilsen y Joe, líderes de Buenaventura
Date: 2017-01-18 13:20
Category: DDHH, Otra Mirada
Tags: buenaventura, Conpaz, Emilsen Manyoma, Joe Rodallega
Slug: comunidad-despide-a-emilse-y-joe-34876
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG-20170118-WA0025.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: CONPAZ ] 

###### [18 Ene. 2017] 

Desde el pasado 17 de enero se llenaron de tristeza y dolor los corazones, y de lágrimas los ojos de familiares y de más de 150 procesos y organizaciones de víctimas que conocieron a **Emilsen Manyoma integrante de *Comunidades Construyendo Paz en los Territorios (CONPAZ)* y a Joe Rodallega, líderes defensores de la vida y el territorio**, que fueron encontrados sin vida en el barrio El Progreso de la comuna 10 en Buenaventura. Le puede interesar: [Asesinan a lideresa de CONPAZ y a su compañero en Buenaventura](https://archivo.contagioradio.com/asesinan-a-lideresa-de-conpaz-y-a-su-companero-en-buenaventura/)

Emilsen tuvo un papel fundamental en su comunidad en lo que respecta al trabajo con jóvenes del consejo comunitario del Bajo Calima, del cual ella hacia parte. Además **ésta líder de 32 años pertenecía al grupo de líderes de la red de CONPAZ en el Valle del Cauca, desde donde se embarcó por la defensa del territorio y la vida.**

**Joe de 36 años, anduvo de la mano de Emilsen este camino en el que en medio de sonrisas, trabajaron por la paz en y desde los territorios y denunciando el paramilitarismo. **Le puede interesar: [CONPAZ propone cese bilateral para iniciar conversaciones de paz con el ELN](https://archivo.contagioradio.com/conpaz-pide-acelerar-negociaciones-de-paz-con-eln-y-farc/)

La última vez que fueron vistos con vida Emilsen y Joe fue el pasado sábado 14, día en el que abordaron un taxi en el barrio Villa Linda en la comuna 12, según algunas versiones entregadas.

La información que ha sido otorgada por fuentes oficiales, es que los cuerpos se encontraron con lesiones que fueron generadas por arma blanca y de fuego, además de serias afectaciones que dan cuenta de haber sido degollados. Le puede interesar: [Paramilitares amenazan a integrantes de CONPAZ](https://archivo.contagioradio.com/paramilitares-amenazan-a-integrantes-de-conpaz/)

A través de un comunicado, **los y las integrantes de CONPAZ manifestaron su profundo dolor por este hecho y declararon “condenamos y rechazamos categóricamente el asesinato de nuestra lideresa Emilsen Manyoma, así como también de su compañero y nuestro compañero Joe Javier Rodallega”.**

Y agregaron que pese a que la violencia les arrebató los cuerpos de Emilsen y Joe, **no podrán borrarles “sus miradas y sonrisas llenas de rio, de montaña, de barrio, de dignidad y de amor a la vida”.**

De igual manera, CONPAZ ha exigido al gobierno colombiano plenas garantías que redunden en la protección de la vida e integridad de los y las líderes en Colombia “no queremos más compañeras y compañeros asesinados, estamos construyendo paz” concluyen. Le puede interesar: [Líderes de CONPAZ son víctimas de amenazas y seguimientos](https://archivo.contagioradio.com/lideres-de-conpaz-son-victimas-de-amenazas-y-seguimientos/).

La comunidad del Espacio Humanitario Puente Nayero realizó una despedida a Emilsen y Joe, que estuvo acompañada de cantos y oraciones. Hacia las 3 p.m. se realizarán las exequias de la líderesa y su compañera** **en Calima, Valle del Cauca, lugar de donde eran oriundos.

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
