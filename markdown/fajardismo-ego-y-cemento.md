Title: Fajardismo: Ego y Cemento
Date: 2015-02-26 16:29
Author: CtgAdm
Category: Opinion
Tags: Hidroituango, megaproyectos en colombia, Sergio Fajardo Valderrama
Slug: fajardismo-ego-y-cemento
Status: published

#### Por**[ [Isabel Zuleta ](https://archivo.contagioradio.com/isabel-cristina/)- ~~@~~ISAZULETA]** 

La crisis socioambiental que vive el departamento de Antioquia sólo es percibida por las comunidades más alejadas que con desespero y angustia ven una gobernación de elites y para elites que se lucra del campo y sus problemas imponiendo su lógica por la fuerza.

En un departamento de víctimas, desarraigo, desigualdad, violencia, concentración de la tierra, de la riqueza económica y natural, se impuso una educación de muros y moles de cemento, abstracta -matemática-.que en nada se acerca a la dura realidad del desplazamiento forzado y los megaproyectos.

Su discurso esta acompañado de una imagen cuidada al limite que controla los medios de comunicación. Fajardo no se pierde foto, su discurso se articula al espectáculo que protagoniza en cada pueblo en donde vende su imagen de académico y político como el referente que los humildes deben seguir. En sus eventos impone un protocolo que como su discurso desencaja con la población y la excluye aunque logra aparentar lo contrario, sin dar cabida a opiniones criticas que sobre su administración surjan. Como modelo de foto su imagen lo es todo y la critica es negada con el uso de la fuerza. Ocurrió en el municipio de Toledo durante un evento público el 25 de septiembre de 2014, a un campesino opositor a Hidroituango se le negó el uso de la palabra porque no era pertinente su opinión.

La política de Fajardo desplaza y anula la posibilidad de reconocimiento de lo que son los antioqueños. Neoliberalismo puro, soberbio, aplastante, de aquel que no olvidó al servicio de que ha estado la ciencia, poder y dominación.

Su ego se concreta en el cemento, le alcanza para pretender inmortalizar la historia de su familia constructora instalando moles por todo el departamento ¿Quienes pagarán el sostenimiento de estas obras, si muchos municipios están quebrados? Hoy los parques bibliotecas de Medellin son elefantes blancos cuyo mantenimiento mensual supera el presupuesto de la mayoría de los municipios de Antioquia. Medellin tiene a EPM ¿Los municipios a quien? El costo de 80 parques educativos, que no superan cada uno 3.000 millones, son un gangoso para lo que estan acostumbrados a ejecutar los fajardistas. Los parques biblioteca en Medellin, descontextualizados como su política, costaron entre 11 mil a 15 mil millones cada uno.

Fajardo y sus herederos se toparon con la pobreza del campo en el departamento con la mayor concentración de tierra en el país, en el que más represas existen, con grandes explotaciones mineras que destruyen el ambiente, con una de las zonas más contaminadas del planeta por mercurio, con quebradas llamadas “La Cianurada”, con la más irracional anulación cultural que insiste en imponer la berraquera que arrasa y destruye y al tiempo resalta un fuerte acento original, se topo con campesinos y campesinas a los que no les llega su discurso de la más educada sin comida, la más inundada sin oportunidades.

El aporte hecho por represas y megaobras a la desigualdad en la tenencia de la tierra no ha sido aclarado aún. Hidroituango, la mayor expresión del ego y el cemento, pretende generar 2.400 MW de energía para la exportación, afecta y concentra 26.060 hectáreas de tierra (más de las que tienen municipios enteros) en el departamento más desigual de Colombia, con Fajardo los campesinos tienen cada vez menos posibilidades del derecho a la tierra.

Ego y cemento para resolver y opacar el pensamiento critico e imaginativo expresado mediante las protestas que desde el año 2010 ha generado Hidroituango. EPM y Fajardo han pretendido resolver la indignación de las comunidades afectadas con una estrategia que combina la represión (persecución, estigmatización, judicialización, ESMAD, ejercito y policía en cada acto publico y cotidiano) el control absoluto de los medios de comunicación para que nada de lo que ocurra con Hidroituango y sus opositores se difunda (el nombre de Ríos Vivos esta prohibido en el canal Teleantioquia). Por ultimo el dinero para todo menos para las problemáticas socioambientales generadas por Hidroituango, cada vez que ocurren movilizaciones por las afectaciones salen al baile 100 millones de dólares para proyectos, parques educativos y canchas sintéticas. Lo anterior queda en evidencia con el desalojo forzoso anunciado para La Arenera, con la ausencia de soluciones de fondo y cumplimiento de protocolos internacionales por otras instituciones y la gobernación siendo juez y parte.

#####  
