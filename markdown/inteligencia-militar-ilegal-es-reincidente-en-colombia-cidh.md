Title: Inteligencia militar ilegal es reincidente en Colombia: CIDH
Date: 2020-10-05 21:13
Author: AdminContagio
Category: Actualidad, Nacional
Tags: CIDH, Espionaje Militar, Inteligencia Militar, interceptaciones ilegales
Slug: inteligencia-militar-ilegal-es-reincidente-en-colombia-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Inteligencia-militar-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este lunes se celebró la **audiencia convocada por la Comisión Interamericana de Derechos Humanos -[CIDH](http://www.oas.org/es/cidh/)- sobre las prácticas de inteligencia militar ilegal** como perfilamientos, seguimientos e interceptaciones ilegales, por parte de los organismos de inteligencia en Colombia en contra de personas defensoras de Derechos Humanos, opositores políticos del Gobierno, magistrados, periodistas, sindicalistas, entre otras personas. (Le puede interesar: [CIDH admite demanda contra el Estado colombiano por masacre de La Rejoya](https://archivo.contagioradio.com/cidh-admite-demanda-contra-el-estado-colombiano-por-masacre-de-la-rejoya/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La audiencia contó con la participación de varias víctimas, organizaciones sociales y representantes de la sociedad civil, quienes fungieron como peticionarios ante la CIDH; y de otra parte, por delegatarios del Estado colombiano como el embajador de Colombia ante la OEA, Alejandro Ordoñez y representantes de la Fiscalía General y la Procuraduría General de la Nación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Iván Cepeda fue el primero en intervenir y **dio cuenta de la sistematicidad, con la que según los peticionarios, operan las prácticas de seguimiento y espionaje ilegal, para lo cual trajo a colación los casos de las «Chuzadas del DAS», entre los años 2003 y 2009; la Operación Andrómeda en 2014;** las carpetas secretas del Ejército Nacional en las que se perfilaba de  manera ilegal a al menos 130 personas, episodio que se dio a conocer en el presente año; y **al menos otros 5 casos más que se habrían dado de manera ininterrumpida durante los últimos 6 años.** (Le puede interesar: [Iván Cepeda es víctima de espionaje militar: Corte Suprema](https://archivo.contagioradio.com/corte-suprema-ivan-cepeda-victima-espionaje/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CIDH/status/1313164273603039233","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CIDH/status/1313164273603039233

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Adicionalmente, Moisés Meza, representante de la Comisión Colombiana de Juristas –CCJ-, intervino también en nombre de la sociedad civil e hizo énfasis en **los vacíos legales y la falta de controles para el ejercicio de actividades de inteligencia ilegal por parte del Estado colombiano,** señalando que pese a la expedición de la Ley 1621 de 2013 que regula las labores de inteligencia y contrainteligencia; y a la supresión del DAS, los seguimientos e interceptaciones ilegales en contra de ciudadanos, que en la mayoría de los casos se muestran en oposición al Gobierno, siguen ocurriendo. Para Meza, esa es una muestra clara de **la infectividad de las medidas adoptadas por el Estado y de unas prácticas militares e institucionales deliberadas para vulnerar derechos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Sebastián Escobar del Colectivo de Abogados José Alvear Restrepo -CAJAR-, intervino haciendo énfasis en **la falta de investigación, judicialización y sanción de los hechos de inteligencia militar ilegal en Colombia; tanto a nivel penal como disciplinario;** lo cual permite que estas prácticas se perpetúen en el tiempo, al crear **un marco de impunidad en el que no se aplican castigos que disuadan a los sujetos para no incurrir en estas conductas.** (Lea también: [No avanza investigación por interceptaciones ilegales del ejército a 4 meses de la denuncia](https://archivo.contagioradio.com/no-avanza-investigacion-por-interceptaciones-ilegales-del-ejercito-a-4-meses-de-la-denuncia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La respuesta del Gobierno frente a la inteligencia militar ilegal

<!-- /wp:heading -->

<!-- wp:paragraph -->

El embajador de Colombia ante la OEA, Alejandro Ordóñez, quien habló en nombre del Gobierno Nacional encabezado por el presidente Iván Duque; no dio crédito a que la intrusión ilegal de los organismos de inteligencia operara de manera sistemática y hasta dejó un manto de duda sobre si estas actividades pudieron llegar a producirse señalando que «*de existir estos hechos, no se trataría de ninguna manera de una práctica sistemática de seguimiento ilegal \[…\] como afirman los peticionarios*». Acogiendo la misma línea negacionista que ha adoptado el Gobierno a nivel interno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, agregó que «*el marco legal \[colombiano\] responde de manera efectiva cuando se producen hechos de esa naturaleza*»; pese a lo denunciado por los peticionarios que dieron cuenta de actos de espionaje, seguimiento e interceptación ilegal, al menos desde el año 2003 hasta la actualidad. )

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los cuestionamientos de la CIDH al Estado colombiano

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los comisionados de la CIDH, hicieron énfasis en la preocupación sobre el despliegue de la **inteligencia militar ilegal en Colombia** y afirmaron que **es una problemática a la que se le viene haciendo seguimiento y monitoreo al menos desde el año 2004 y que se ha documentado a través de varios informes anuales**, lo que demuestra que hay una reincidencia en esa práctica y que los casos persisten.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En particular la comisionada Antonia Urrejola cuestionó a la Fiscalía sobre la participación que tienen las víctimas en los procesos de investigación de estos seguimientos, ya que, varios de los solicitantes expresaron que existía una imposibilidad de acceder a la información y de participar activamente en los procesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, indagó sobre los tiempos de investigación y juzgamiento, sobre los que recalcó que eran una materia importante, si se tiene en cuenta que **estos procesos de sanción penal son uno de los ejes centrales para la no repetición que es uno de los principios fundamentales en materia de Derechos Humanos.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
