Title: 66 empresas, entre ellas 4 bancos, tendrán responderle a reclamantes de tierras
Date: 2020-06-06 19:09
Author: CtgAdm
Category: Actualidad, DDHH
Tags: despojo de tierras, empresas, reclamantes de tierras
Slug: 66-empresas-entre-ellas-4-bancos-tendran-responderle-a-reclamantes-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/2020-06-05.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/2020-06-05-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/11155140_967175476626653_9089835472884672392_o.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Reclamantes de tierras: Foto Forjando Futuros

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Magistrados de restitución de tierras fallaron a favor de campesinos y comunidades étnicas que fueron despojadas de sus predios, **hecho documentado en 5611 sentencias entre 2012 y 2020. Como resultado, un total de 66 empresas en todo el país** incluidas multinacionales, agro industriales, mineras beneficiados en su mayoría por la violencia paramilitar han sido condenadas a restituir la tierra, suspender títulos mineros y anular las hipotecas bancarias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El análisis hecho a 5611 sentencias en toda Colombia con el apoyo de organizaciones como la Comisión Intereclesial de justicia y paz, la Corporación Jurídica Libertad, el Instituto Popular de Capacitación y publicado por [Forjando Futuros](http://@forjandofuturos), revela que dichas 66 empresas condenadas están divididas en tres grupos. Uno del sector agroindustrial, otras que corresponden a empresas extractivistas y otro grupo que son los bancos incluidas entidades como Davivienda, BBVA, Bancolombia y Banco Agrario. [(Lea también: Empresas bananeras de Uraba son investigadas por concierto para delinquir y financiación de grupos armados)](https://archivo.contagioradio.com/empresas-bananeras-de-uraba-son-investigas-por-concierto-para-delinquir-y-financiacion-de-grupos-armados/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/forjandofuturos/status/1268252296233783296","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/forjandofuturos/status/1268252296233783296

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Algunas de las empresas vinculadas incluyen a Ecopetrol, Continental Gold, Petromil, Pacific Mines S.A.S, Anglogold Ashanti, Cementos Argos, Grupo Empresarial Dole, Bananeras de Urabá S,A, y la Cooperativa Comultrasan, Drummond Ltda.

<!-- /wp:paragraph -->

<!-- wp:image {"id":85093,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/2020-06-05-1.png){.wp-image-85093}  

<figcaption>
Imagen: Forjando Futuros

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Los bancos, cómplices del despojo de tierras

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A**l respecto,Gerardo Vega, director de Forjando Futuros explica que en casos como Jiguamiandó y Curbaradó en Chocó, Cesar, Córdoba,** los bancos prestaban dinero a personas que compraban tierras despojadas hipotecando esas hectáreas al banco lo que ha ocasionado que estos se opongan a la restitución. [(Le puede interesar: En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras)](https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No basta con que se hayan hecho estudios de título, tenían que indagar porque eran hechos de conocimiento público y que todo el mundo sabía que eran hechos de violencia generalizada", afirma el director de Forjando Futuros.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como resultado se están cancelando las hipotecas a los cuatro bancos que tuvieron como garantía hipotecaria las tierras de personas desplazadas pues **"un banco no despoja pero si facilita el despojo, prestándole a despojadores o personas que aprovecharon para hacerse a esas tierras".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No existe una voluntad de reparación

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque algunos de los empresarios se han acercado a las organizaciones, desde Forjando Futuros advierten que solo Argos ha tratado de remediar el daño causado en los territorios despojados en Montes de María, sin embargo las otras empresas no existe una voluntad de reparar a las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte las multinacionales sostienen que ellos solo tienen acceso al subsuelo más no a la tierra, argumento que para Vega carece de sentido pues por algún lado se debe acceder a los recursos que se encuentran bajo las tierras despojadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De las 5611 sentencias, concluye, son varias las que han tenido problemas para la entrega en regiones como Urabá, Cesar y Magdalena donde al oponerse a la entrega, los despojadores dañan los cultivos o acuden a segundos ocupantes, explica Vega.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Se sabe que son empresarios, ganaderos que están tomando retaliaciones y presión por esos territorios" explica el dirigente social Julio César León, integrante de la Corporación Nacional de Líderes Sociales y Víctimas de Colombia (Corpvicol) organización social que ha acompañado el proceso de restitución en Antioquia y el Bajo Atrato, un territorio que ha sido históricamente corredor estratégico y de interés por las salidas de afluentes como el río Atrato. [(Lea también: Otorgan casa por cárcel a reclamantes de tierras en Guacamayas)](https://archivo.contagioradio.com/otorgan-casa-por-carcel-a-reclamantes-de-tierras-en-guacamayas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a ello Gerardo Vega es enfático en que la misión de los jueces es seguir devolviendo las tierras, pero es necesario que se investiguen los hechos que llevaron al despojo y se compulsen copias a la Fiscalía, mientras que las comunidades deben continuar organizándose por sus derechos. [(Le puede interesar: En Caño Manso la dignidad enfrenta al despojo)](https://archivo.contagioradio.com/en-cano-manso-la-dignidad-enfrenta-al-despojo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
