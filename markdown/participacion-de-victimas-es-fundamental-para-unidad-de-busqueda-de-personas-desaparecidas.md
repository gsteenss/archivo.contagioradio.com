Title: Participación de víctimas es fundamental para Unidad de Búsqueda de Personas Desaparecidas
Date: 2018-05-22 15:48
Category: Paz, Política
Tags: acuerdos de paz, Unidad de Búsqueda de Personas Desaparecidas, víctimas
Slug: participacion-de-victimas-es-fundamental-para-unidad-de-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [22 May 2018] 

El más reciente informe hecho por el Instituto de derechos humanos de Cataluña y el Colectivo Orlando Flas Borda, sobra la Unidad de Búsqueda de personas dadas por desaparecidas, realizó una serie de recomendaciones para el desarrollo de esta entidad y reveló la necesidad de lograr una participación **entre las víctimas, los actores del conflicto armado y la sociedad en general en la construcción escenarios que permitan encontrar la verdad y la reconciliación**.

### **Los aportes del informe** 

De acuerdo con Cesar Santoyo, uno de los principales aportes que hace este informe a la Unidad de Búsqueda de personas dadas por desaparecidas tiene que ver con que el desarrollo de la misma, en donde consideran que la participación de las víctimas debe ser central, **“se debe poder fortalecer las capacidades y empoderamiento de las víctimas frente a la desaparición forzada”**.

Otro de los aportes que realiza, es que el protocolo que se establezca para la participación de las víctimas en el desarrollo de las actividades de la Unidad de Búsqueda, debe tener como tarea indispensable **un diagnóstico sobre las características particulares de la victimización en cada territorio y población del país**, especialmente en grupos especiales como mujeres, niños y niñas.

De igual forma consideran de vital importancia la participación de las víctimas a la hora de construir un e implementar un registro único de fosas, cementerios ilegales y sepulturas. En ese sentido el informe señala que “las actividades de la UBPD son de naturaleza humanitaria y extrajudicial” aspecto que debe guiar las formas de participación. (Le puede interesar:["Luz verde para funcionamiento de Unidad de Búqueda de Desaparecidos"](https://archivo.contagioradio.com/luz-verde-para-funcionamiento-de-la-unidad-de-busqueda-de-desaparecidos/))

### **El tiempo de la Unidad de Búsqueda** 

De acuerdo con las cifras del Centro de Memoria Histórica, en Colombia han sido desaparecidas forzadamente 82.998 personas, sin embargo, el informe alerta que podrían ser muchas más, razón por la cual, una de las misiones más importantes que tendrá que asumir la **UBPD será establecer el histórico de personas desaparecidas en el contexto y en razón del conflicto armado.**

“Entendemos que la Unidad debe ir generando unas acciones tempranas que faciliten la búsqueda, exhumación y entrega de las personas dadas por desaparecidas en el caso que estén sin vida. Y del otro lado que sean identificadas y ubicadas de manera adecuada en el caso de que se encuentren con vida” afirmó Santoyo.

De igual forma Santoyo manifestó que para blindar la actividad de la UBPD es necesario tener en cuenta las capacidades que tengan las comunidades o víctimas del conflicto para monitorear la implementación de los Acuerdos de Paz de La Habana.

<iframe id="audio_26118856" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26118856_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
