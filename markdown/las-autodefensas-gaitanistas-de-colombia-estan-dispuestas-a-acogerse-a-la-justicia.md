Title: Autodefensas Gaitanistas de Colombia están dispuestas a acogerse a la justicia
Date: 2017-09-05 15:05
Category: Nacional, Paz
Tags: Gaitanistas, paz, sometimiento, Usuga
Slug: las-autodefensas-gaitanistas-de-colombia-estan-dispuestas-a-acogerse-a-la-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/otoniel.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [05 Sept 2017]

Jairo Antonio Úsuga David, comandante de las Autodefensas Gaitanistas de Colombia, envió mensajes al Papa Francisco y a los colombianos, en los que aseguran que el estado mayor de esa organización está unido y que además tendrían la voluntad para alcanzar un acuerdo de paz con el actual gobierno del presidente Juan Manuel Santos.

<iframe src="https://www.youtube.com/embed/mDLyvF8l2Co" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En los materiales fílmicos que conoció el equipo de Contagio Radio, vestido de camiflado y portando un fusil, Usuga David aseguró que estarían dispuestos a contribuir en con el final del conflicto armado y por ello abren la puerta a un posible acuerdo con el gobierno actual. Además señala que en repetidas ocasiones han manifestado esa voluntad y que una vez estén dadas las condiciones cesarían todas las acciones armadas que desarrollan.

En el mensaje al Papa Francisco, el comandante de las AGC, resalta que como representante de la iglesia tiene una autoridad moral para interceder con sus oraciones para que su deseo de paz sea una realidad. “Creemos que con sus oraciones podemos salir adelante en nuestro propósito de dejar las armas” señala.

<iframe src="https://www.youtube.com/embed/A2XYlO5kATI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Algunos defensores de DDHH manifiestan que este paso de las AGC o Clan del Golfo es un avance significativo para construir una sociedad en paz y que un posible sometimiento a la justicia estaría sustentado en el derecho consuetudinario y que la política criminal está dictada por el Ministerio de Justicia que presentaría propuestas de formulas para hacer realidad dicho acogimiento a la justicia.

Según unas primeras aproximaciones las Autodefensas Gaitanistas de Colombia cuentan con un número aproximado entre 5000 y 6500 miembros activos, que se acogerían a las normas que se establezcan para una posible desmovilización.
