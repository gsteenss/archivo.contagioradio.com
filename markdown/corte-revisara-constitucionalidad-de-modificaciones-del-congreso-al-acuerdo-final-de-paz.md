Title: Corte revisará constitucionalidad de modificaciones del Congreso al Acuerdo Final de paz
Date: 2017-03-30 13:05
Category: Entrevistas, Paz
Tags: acuerdos de paz, Corte Constitucional, Jurisdicción Espacial de Paz
Slug: corte-revisara-constitucionalidad-de-modificaciones-del-congreso-al-acuerdo-final-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corte-e1495109388589.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [30 Mar 2017]

Son varias las organizaciones de derechos humanos y congresistas que han manifestado a la Corte Constitucional **fuertes reparos en torno a algunos puntos que fueron modificados en el Congreso de la República en medio del trámite de implementación del Acuerdo** de terminación del conflicto con las FARC-EP y que tienen que ver con las garantías de los derechos de las víctimas.

El Colectivo de Abogado José Alvear Restrepo, el MOVICE y la Coordinación Colombia Europa Estados Unidos plantean que las libertades transitorias y l**a renuncia a la acción penal para agentes estatales no establece plazos para comparecer ante la JEP,** lo que representa un riesgo para los derechos de las víctimas dado que la Jurisdicción Especial de Paz no está todavía en funcionamiento. Le puede interesar: ["JEP mantiene abierta la posibilidad para la impunidad: WOLA"](https://archivo.contagioradio.com/jep-mantiene-abierta-la-posibilidad-para-la-impunidad-wola/)

Por otra parte, se señala que la amnistía y los tratamientos especiales a miembros de las Fuerzas de Seguridad del Estado, contemplados en el acuerdo con las FARC-EP, deben garantizar los derechos de las víctimas, uno de ellos el derecho a la participación en todos los procedimientos de la ley 1820 de 2016. **Para varias organizaciones no se ha garantizado el principio de bilateralidad pues no han participado dichas víctimas.  **

Además, el procedimiento de Fast Track está autorizado para los términos del acuerdo y no artículos adicionales, como el incluido por el gobierno **de manera unilateral en el capítulo 8 de la mencionada ley sobre tratamientos especiales a integrantes de la Fuerza Pública**. Le puede interesar: ["Si Estado no juzga empresarios y altos Mandos Militares que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

En relación con la seguridad jurídica de activistas sociales, procesados en el marco de la Ley de Seguridad Ciudadana, el acuerdo contemplaba que podrían recobrar su libertad, sin embargo, el **Congreso modificó el artículo 24 que ahora exige conexidad con el delito político**. Es decir que la protesta ciudadana deberá tener relación con delitos políticos desconociendo el derecho legal y legítimo a la protesta social.

Otras de los llamados de atención **tienen que ver con el derecho a la reparación de las víctimas que quedo solamente bajo la responsabilidad del victimario y no del Estado** y que se prevé un sistema de asistencia jurídica para militares pero no para las víctimas, entre otras apreciaciones. Le puede interesar: ["Impunidad, la mayor preocupación de las organizaciones de DDHH con la JEP"](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/)

La Coordinación Colombia Europa Estado Unidos, también emitió un concepto en donde señala que la **Comisión de Búsqueda de Desaparecidos no será de alto nivel pues el texto final de la JEP** considera que, no va a estar sujeta a un régimen legal propio, es decir dependerá de la normativa vigente que no ha funcionado durante los 17 años que lleva creada esta unidad.

Así las cosas, la Corte Constitucional tendrá que hacer una revisión exhaustiva tanto del acuerdo como de lo modificado en el Congreso **para garantizar los derechos de las víctimas y la constitucionalidad de las acciones del legislativo**.

<iframe id="audio_17868197" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17868197_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_17868270" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17868270_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
