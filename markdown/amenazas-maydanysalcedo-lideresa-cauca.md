Title: Denuncian nuevas amenazas contra Maydany Salcedo, lideresa de Cauca
Date: 2019-08-14 17:35
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: campesino, Lider social, liderezas, Piamonte
Slug: amenazas-maydanysalcedo-lideresa-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Maydany.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

Desde el pasado 25 de Julio Maydany Salcedo, **dirigente  de la Asociación de Trabajadores Campesinos de Piamonte**, Cauca (ASIMTRACAMPIC); que impulsa el cumplimiento de los acuerdos pactados en La Habana, y especialmente, los planes de sustitución de cultivos de uso ilícito; no ha podido regresar a su territorio por amenazas  cada vez más recurrentes, ello debido a que después de alertar sobre un primer hecho de hostigamiento, s**e han registrado otros tres que ponen en riesgo su vida**.

Según Salcedo ya se han realizado las denuncias de manera formal, pero ni la Fiscalía, ni la Alcaldía, ni la Policía le ha brindado seguridad para el retorno a su territorio. (Le puede interesar:[Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

### **¿QUIÉN LA AMENAZA?** 

La primera amenaza se dio desde el 25 de julio de este año, cuando fue contactada por la delegación de derechos humanos de la Policía, que le informaron que su vida corría riesgo y los presuntos responsables de esta amenaza aún estaban por esclarecerse, situación que **provocó que Maydany se desplace hasta Florencia para protegerse**.

Posteriormente el 8 de agosto recibió un mensaje de texto, donde nuevamente le advertían sobre la intención de asesinarla. Salcedo aseguró que la información aparecía firmada por los grupos armados Cobras, una disidencia de las FARC y los Escorpiones, que operan en el territorio.

La última amenaza se dio el 9 de agosto, a través Jonathan Cuellar Ramírez, secretario de ASIMTRACAMPIC, causándole violencia verbal y fuertes agresiones física, con el fin de que Salcedo y toda la Asociación entendieran que se deben ir o sus vidas puedes ser perpetradas. (Le puede interesar: [Nuevas amenazas de paramilitares contra FARC y líderes sociales](https://archivo.contagioradio.com/nuevas-amenazas-de-paramilitares-contra-farc-y-lideres-sociales/))

### **¿CUÁL ES LA VERDADERA RAZÓN?** 

Estas no son las únicas amenazas que ASIMTRACAMPIC ha recibido. Según Maydany. desde el nacimiento de la organización en el 2012, sus integrantes han sido víctimas de persecución, hostigamientos y montajes judiciales.

De igual forma, para la líder, la posible reelección de un exalcalde de Piamonte, genera para ellos riesgo, debido a que, durante el periodo electoral de ese gabinete se pasaron muchas veces por alto peticiones, quejas y amenazas que recibió la asociación.

Actualmente  Maydany Salcedo está convocando cita a la Alcaldía Municipal, Fiscalía, Defensoría e incluso Presidencia de la República, al igual que medios de comunicación a visibilizar su citación, respaldar el proceso que ellos están llevando por medio de denuncias, entender el trasfondo de las amenazas, los verdaderos voceros y las irregularidades al postergar y respaldar su desplazamiento, y especialmente a agilizar el regreso a su hogar.

<iframe id="audio_39960440" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960440_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
