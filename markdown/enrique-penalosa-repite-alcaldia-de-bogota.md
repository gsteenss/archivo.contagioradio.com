Title: Enrique Peñalosa repite alcaldía de Bogotá
Date: 2015-10-25 19:48
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Bogotá Humana, Clara López, elecciones regionales, Enrique Peñalosa, Francisco Santos, Gustavo Petro, Rafael Pardo
Slug: enrique-penalosa-repite-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/colp_043048.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Patria 

###### 25 Oct 2015

Con el 49% de abstencionismo en Bogotá, Enrique Peñalosa nuevamente fue elegido como alcalde de la capital con 903.764 votos, seguido a él, Rafael Pardo obtuvo 778.050, Clara López quedó tercera con 498.718 y Francisco Santos sigue con 327.852.

El economista y administrador del movimiento ciudadano Equipo por Bogotá en alianza con Cambio Radical, es el nuevo alcalde de Bogotá tras las elecciones celebradas hoy.

Entre las principales propuestas de Enrique Peñalosa se encuentra la realización de un Metro elevado, la construcción de la Avenida Longitudinal y la construcción de un corredor ecológico, teniendo en cuenta que su eje central se basa en la reforma urbana.

Tanto Clara López como Francisco Santos, aceptaron la elección de los bogotanos y bogotanas que decidieron salir a votar, sin embargo, la candidata por el Polo Democrático Alternativo, llamó la atención a Peñalosa para que no deje de lado los programas sociales.

Por su parte, el alcalde Gustavo Petro, dijo a través de su cuenta de twitter, "Como mi deber democrático, desde mañana estará listo el gobierno de la Bogotá Humana para hacer el empalme con el nuevo gobierno de Peñalosa".

Respecto al Concejo de Bogotá, con el 89% de las mesas escrutadas, la votación queda así: Cambio Radical obtuvo 317.837, Partido Liberal 235.427, Alianza Verde 221.808, Centro Democrático 205.238, Polo Democrático Alternativo 200.993, Partido de la U 177.834, Partido Conservador 108.157.
