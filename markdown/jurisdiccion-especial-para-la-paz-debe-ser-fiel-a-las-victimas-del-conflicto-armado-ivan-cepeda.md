Title: "Jurisdicción Especial de Paz debe ser fiel a víctimas del conflicto armado" Iván Cepeda
Date: 2017-01-16 12:09
Category: Entrevistas, Paz
Tags: Acuerdos de La Habana, Jurisdicción Espacial de Paz
Slug: jurisdiccion-especial-para-la-paz-debe-ser-fiel-a-las-victimas-del-conflicto-armado-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Víctimas-de-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Ene 2017] 

En medio de las precarias condiciones en las que aún están los campamentos transitorios para la guerrilla de las FARC-EP y las denuncias que se han hecho de grupos paramilitares cercanos a las zonas veredales, este **martes 17, inician los debates extraordinarios en el Congreso sobre la implementación de los acuerdos, la primera ponencia será el acto legislativo que introduce la Jurisdicción Especial para la Paz.**

Para Iván Cepeda, senador por el Polo Democrático, lo más importante de este debate es que se mantenga lo más **“fiel” a las expectativas que tiene la sociedad colombiana, en especial a las víctimas del conflicto armado** que buscan en esa Jurisdicción Especial para la Paz verdad y justicia, y que sea consecuente  a los acuerdos de La Habana en términos legislativos y constitucionales.

Sin embargo, pese a que ya va iniciar el segundo ciclo de debates extraordinarios, continúan las precarias condiciones de salud y alimentación que se han presentado en los campamentos transitorios, frente a este tema Cepeda aseguró  que “se han visto problemas de orden institucional, de carácter presupuestal que muestran que **no están dadas las condiciones de preparación para la aplicación eficaz de los acuerdos**”.

Además agregó que “el Estado debe garantizar las condiciones políticas, legales, institucionales, presupuestales y de seguridad, necesarias para que se comience a producirse la reincorporación de los guerrilleros a la vida civil”. Le puede interesar: ["Organizaciones sociales denuncia reagrupamiento paramilitar en el Tolima"](https://archivo.contagioradio.com/34519/)

Otra de las alarmas que se han encendido tiene que ver con la denuncia por parte de comunidades sobre la **presencia paramilitar cercana a los campamentos transitorios,** situación que el senador dice puede cambiar si se aplicarán los elementos que ya están en el acuerdo para acabar con el paramilitarismo, entre ellos está la creación de una unidad especial en la Fiscalía que contrarreste las acciones del paramilitarismo, la creación de alertas tempranas que involucra a las comunidades y autoridades para la protección, entre otros. Le puede interesar: ["Zonas veredales bajo la sombra paramilitar"](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

Por otro lado frente a la continuación de los demás debates Cepeda  expresó que se le ha pedido al gobierno que haya claridad sobre cuál será el cronograma legislativo y cada uno de los proyectos de ley lo antes posible, “porque **es primordial que antes de mayo debería esta evacuado lo esencial de la implementación de los acuerdos** y de las reformas constitucionales a las que haya lugar”

<iframe id="audio_16373195" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16373195_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
