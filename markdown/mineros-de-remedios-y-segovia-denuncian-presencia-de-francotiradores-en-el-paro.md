Title: Policía usa francotiradores para reprimir mineros en Remedios y Segovia
Date: 2017-08-16 12:33
Category: DDHH, Nacional
Tags: ESMAD, Mineros Remedios y Segovia
Slug: mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-16-at-3.20.53-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mesa Minera de Remedios y Segovia] 

###### [16 Ago 2017]

Mineros de Remedios y Segovia reportaron el asesinato del joven Brandon Stiven Ochoa, de tan solo 18 años de edad, **debido a impactos de bala disparados pro francotiradores de la Policía**, de igual forma la comunidad ha denunciado la falta de acciones y acompañamiento por parte de la Defensoría del Pueblo u otras instituciones del Estado a la protesta.

Jaime Gallego, vicepresidente de la Mesa Minera, expresó que los sucesos ocurrieron anoche, en medio de confrontaciones entre los manifestantes y el ESMAD, **cuando Brandon estaba en medio de las protestas y fue impactado por una bala en le pecho, otras dos personas también habrían resultado heridas** por impactos de bala, que fueron trasladados a la ciudad de Medellín para recibir atención médica.

A esta situación se suman otras denuncias que hace la comunidad como el ingreso de miembros del ESMAD a las viviendas de los barrios en Segovia, la presencia de francotiradores y la toma el Colegio Liborio Bataller por parte de efectivos de la policía y el Escuadron. (Le puede interesar: ["Continúa paro de mineros en Remedios y Segovia pese a la represión"](https://archivo.contagioradio.com/continua-paro-de-mineros-en-remedios-y-segovia-pese-a-la-represion/))

“Es falso lo que dicen las autoridades, como el alcalde de Segovia y la secretaria de gobierno de Antioquia, que los manifestantes estaban dentro del colegio Liborio Bataller, los que se metieron **allá fue el ESMAD, se tomaron el colegio, instalándose allí y disparándole a la población**” afirmó Gallego.

Sobre las afirmaciones que ha realizado la Policía sobre que los manifestantes en Segovia han demostrado alta hostilidad y han usado vías de hecho, Gallego aseguró “lo que están es justificando sus acciones, pero nosotros no necesitamos mentirle al pueblo” y agregó que **en los 27 días que llevan los mineros en paro, la Defensoría del Pueblo solo ha ido tres veces a realizar acompañamiento**. (Le puede interesar: ["Mineros de Remedios y Segovia cumpletan un mes en paro"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-completan-un-mes-en-paro/))

Hoy se realizará una concentración en la Alcaldía de Segovia, convocada por las mujeres del municipio para exigir que se retire el pie de fuerza del ESMAD y hacer un llamado a la cordura tanto de los manifestantes como de los miembros del ESMAD, **para que Segovia no se convierta en un escenario de conflicto**, sino para que exista pronto una dialogo entre mineros, el gobierno y la multinacional Grand Tierra Gold.

<iframe id="audio_20371203" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20371203_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
