Title: El respeto a la diferencia debe ser patrimonio de los colombianos: Gestores de Paz del Valle
Date: 2018-01-25 14:43
Category: Nacional, Paz
Tags: acuerdos de paz, asesinato de líderes sociales, lideres sociales, Valle del Cauca
Slug: la-personas-deben-poder-expresar-ideas-sin-temor-a-ser-asesinadas-red-de-gestores-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa.hn] 

###### [25 Ene 2018] 

La Red de Gestores de Paz del Valle del Cauca alertó el aumento de las denuncias sobre amenazas y asesinatos a **campesinos, líderes sociales e integrantes del nuevo partido político FARC.** Indicaron que esta situación debilita la implementación de los Acuerdos de Paz y “es un grave procedente para resolver por vías civilizadas los conflictos políticos”.

Jhon Fredy Grisales, integrante de la Red, indicó que los procesos de construcción de paz, compuestos por diferentes organizaciones, requieren garantías para que **“la gente pueda expresar sus ideas libremente** sin el temor de ser asesinado”. Este año en el Valle del Cauca se han difundido dos panfletos amenazantes contra organizaciones sociales que llevan a cabo trabajos de paz en los territorios.

### **Exigen al Estado colombiano garantías para los líderes sociales** 

Teniendo cuenta esta situación, la Red realizó una serie de exigencias que lleven al cese de la violencia en los diferentes territorios. Al Estado le hicieron un llamado para que **cumpla a cabalidad lo Acordado con las FARC** en la Habana especialmente lo relacionado con las garantías de seguridad para los defensores de derechos humanos. (Le puede interesar: ["La falta de implementación de los Acuerdos de Paz impide la paz territorial"](https://archivo.contagioradio.com/la-falta-de-implementacion-de-los-acuerdos-de-paz-impide-la-paz-territorial/))

Hicieron énfasis en la importancia del **desmonte del paramilitarismo** y que "se reconozca la sistematicidad en los asesinatos de los líderes sociales y se hagan todos los esfuerzos necesarios para que se consolide la paz y se respete el derecho a la vida en los territorios”.

Además, le pidieron a la comunidad internacional y en especial a las Naciones Unidas que se haga presente en los territorios para recibir y **tramitar las denuncias** que se han con relación a esta situación. De igual manera pidieron que se realice una sesión en el Consejo Departamental de Paz allí en el Valle del Cauca para que se tomen medidas efectivas en concordancia con el Gobierno Nacional.

Finalmente, le pidieron a la sociedad vallecaucana que se **solidarice con los líderes sociales** y que exija el cumplimiento de los Acuerdos de Paz desde y en los territorios. Así mismo re afirmaron que los procesos de organización social continuarán trabajando por que “el respeto a la diferencia sea un patrimonio de todos los colombianos”.

<iframe id="audio_23364080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23364080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
