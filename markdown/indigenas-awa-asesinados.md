Title: Aún no acaba 2018 y ya son 34 indígenas Awá asesinados
Date: 2018-12-04 17:40
Author: AdminContagio
Category: DDHH
Tags: Awá, indígenas, lideres sociales, ONIC, Vía Panamericana
Slug: indigenas-awa-asesinados
Status: published

###### [Foto: @ONIC\_Colombia] 

###### [4 Dic 2018] 

Tras el asesinato de los líderes Awá Braulio Arturo García y Héctor Ramiro García, las autoridades mayores de la Comunidad decidieron velar sus cuerpos en la vía Panamericana para visibilizar la situación de riesgo a la que está expuesta este Pueblo indígena. (Le puede interesar: ["Asesinato de Gobernador y Líder indígena enluta al pueblo Awá"](https://archivo.contagioradio.com/awa-luto-asesinato-de-lideres/))

Jaime Caicedo, líder del Cabildo Mayor Awá de Ricaurte (CAMAWARI), sostuvo que desde hace algo más de 3 meses han registrado un total de 12 asesinatos, situaciones que ocurren ante la pasividad del Gobierno que “no ha hecho las investigaciones del caso, ni ha estado pendiente”. Por esta razón, las autoridades indígenas decidieron bloquear la Vía Panameriacana para repudiar estos asesinatos, y visibilizar la difícil situación en materia de derechos humanos por la que atraviesa este pueblo.

En ese sentido, el Consejero Mayor de la Unidad Indígena del Pueblo Awá (UNIPA) Rider Pai, afirmó que en lo corrido del año han sido asesinados 22 integrantes de su organización; es decir, que durante 2018 se han registrado 34 homicidios. Adicionalmente, Caicedo denunció que mientras se realizaba la velación de sus compañeros, se enteraron de un panfleto de las Águilas Negras, en el que se amenazaba la vida de los líderes indígenas de la zona.

> Grave e inaceptable, en medio de velación de líderes Awá asesinados en el Resguardo Palmar Imbi Medio (Ricaurte, Nariño), la comunidad recibe nuevas amenazas en contra de su integridad. ¿Donde están las garantías? [\#SOSPuebloAwá](https://twitter.com/hashtag/SOSPuebloAw%C3%A1?src=hash&ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@ForstMichel](https://twitter.com/ForstMichel?ref_src=twsrc%5Etfw) [@CIDH](https://twitter.com/CIDH?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [pic.twitter.com/FjUS4CL90j](https://t.co/FjUS4CL90j)
>
> — Asociación MINGA (@asociacionminga) [4 de diciembre de 2018](https://twitter.com/asociacionminga/status/1069974737579896833?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **“Esto no es un tema de líos de faldas o de riñas entre familiares”** 

Según Oscar Montero, asesor de derechos humanos de la Organización Nacional Indígena de Colombia (ONIC), las declaraciones emitidas por un integrante de la Fuerza Pública según las cuales, los homicidios de los líderes corresponden a una riña entre familiares son irrespetuosas, y son una forma de desprestigiar el proceso organizativo indígena. (Le puede interesar:["En la Orinoquía empresas expropian a 13 comunidades indígenas"](https://archivo.contagioradio.com/empresas-expropian-13-comunidades-denuncian-indigenas-orinoquia/))

A esta situación se suma el hecho de que, como lo expresa Montero, “el Estado no ha garantizado la vida de la gente”, muestra de ello son los 86 líderes indígenas asesinados después de la firma del acuerdo en 2016, hasta el momento. Homicidios relacionados con la disputa por el territorio, la sustitución de cultivos de uso ilícito y la permanencia de actores armados relacionados con el narcotráfico, siendo Nariño, Valle del Cauca, Cauca, Antioquia y Arauca los departamentos en los que se presenta mayoritariamente este fenómeno.

Montero concluyó que matar un líder es acabar la dinámica por la vida y el territorio, por lo tanto, con la protesta en la Vía panamericana esperan llamar la atención de las autoridades locales y nacionales encargadas de proteger la vida en los territorios, así como mostrar a las personas que “a los territorios indígenas la paz no ha llegado”. (Le puede interesar: "La grave crisis humanitaria que afronta el pueblo Awá los obligó a venir a Bogotá")

<iframe id="audio_30506800" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30506800_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
