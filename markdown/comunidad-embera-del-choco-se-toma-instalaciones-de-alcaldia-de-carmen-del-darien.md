Title: Comunidad Embera del Chocó se toma instalaciones de alcaldía de Carmen del Darién
Date: 2015-10-15 12:45
Category: Movilización, Nacional
Tags: Argemiro Bailarín, Bajo Atrato chocoano, Carmen del Darién, Incumplimiento del alcalde del Carmen del Darién, Indígenas Emberá, Radio de derechos humanos, Resguardo Uradá Jiguamiandó, Toma de la alcaldía de Carmen del Darién
Slug: comunidad-embera-del-choco-se-toma-instalaciones-de-alcaldia-de-carmen-del-darien
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Carmen-del-Darién-e1509572467830.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

<iframe src="http://www.ivoox.com/player_ek_9006796_2_1.html?data=mpWdmJydeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb6bhw8rfw5DIqc2fpM3cxYqnd4a2lJDgx5DYs87VjM7b1dnFsMLXytTbx9iPqMafwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Argemiro Bailarín, líder indígena Embera] 

###### [15 Oct 2015] 

[300 indígenas Embera del Bajo Atrato chocoano, se vieron obligados a tomarse la alcaldía del Carmen del Darién ante el **incumplimiento del alcalde** del municipio Antonio Ospina Serna, **en la entrega de \$95 millones** que les fueron **asignados desde 2014** por el Sistema General de Participación.   ]

[Los **300 pobladores indígenas** que se tomaron pacíficamente la alcaldía, **entre ellos 100 menores de edad**, hacen parte de los 19.694 que componen las 7 comunidades en las que se divide el resguardo Uradá Jiguamiandó.]

[El líder de este resguardo, Argemiro Bailarín, asegura que tomaron esta medida pues desde el pasado mes de julio el alcalde les ha pospuesto la entrega de **recursos que no han podido invertir en proyectos de salud, educación y adquisición de ganado**, urgentes para la supervivencia de sus comunidades.  ]

[Ante la **dilatación en la entrega de los recursos y la actuación ineficiente de los servidores públicos de la alcaldía**, los pobladores indígenas han decidido permanecer pacíficamente en sus instalaciones. Mientras que el personero municipal Luis Alberto Rodríguez se ha **negado a participar de las reuniones** entre funcionarios de la alcaldía y líderes indígenas pues según él "No tiene nada que ver con la situación" y afirma que "Los funcionarios han sido secuestrados por los indígenas".  ]

[Pese a que el Personero afirma que el coordinador de asuntos étnicos es el directamente responsable en la entrega y ejecución de los recursos, se percibe ineficacia en sus funciones, pues **desde 2014 las comunidades indígenas vienen exigiendo el pago de los recursos asignados** y este servidor nunca ha brindado las garantías a las que está obligado como parte de sus funciones.  ]

[Las reuniones entre funcionarios de la alcaldía y autoridades indígenas no han dado los resultados esperados, pues se sigue dilatando el desembolso de los dineros para estas comunidades indígenas. Pese a la insistencia, **el alcalde se opone a reunirse hoy en su despacho con las autoridades indígenas** y les propone encontrarse en la Casa de la Cultura, ante lo que estas se oponen.  ]

["Exigimos además de la entrega del dinero, la **inclusión y atención de las necesidades en salud, restitución de tierras, educación de nuestras comunidades**" asegura Bailarín "Los ministerios públicos deben garantizarnos y no violar nuestros derechos como comunidades indígenas"]
