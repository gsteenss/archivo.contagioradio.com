Title: Estudiantes de Universidad Libre en paro por altos costos en matrículas
Date: 2018-01-31 12:39
Category: Educación, Nacional
Tags: Movimiento estudiantil, paro, Universidad Libre
Slug: estudiantes-de-universidad-libre-en-paro-por-altos-costos-en-matriculas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/apoyo-paro-Unilibre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Apoyo Universidad Libre] 

###### [31 Ene 2018] 

Estudiantes de la Universidad Libre de Colombia se declararon en asamblea permanente, luego de que las directivas anunciaran el incremento del 5% en el costo de la matrícula, e**n documentos y certificados de la institución y realizara modificaciones en la inscripción de materias vacacionales, que quedaron disueltos**.

### **Aumento en el precio de matrículas y certificados** 

Los estudiantes denunciaron que en el año 2017 las matrículas aumentaron en un **10% y que para este año el aumento fue de un 5%**, lo que ha generado que una matrícula de carreras como Derecho llegue a costar hasta once millones de pesos o una ingeniería este por los siete millones.

Y pese a que las universidades privadas legalmente pueden fijar el costo de sus matrículas y el alza en las mismas, los estudiantes señalaron que el dinero que pagan **no se ve reflejado ni en la infraestructura de la universidad ni en la calidad de la educación**. (Le puede interesar:["Universidad Distrital sin presupuesto para el 2018 y con déficit de 13 mil millones")](https://archivo.contagioradio.com/universidad-distrital-sin-presupuesto-para-el-2018-y-con-deficit-de-13-mil-millones/)

“Nuestra exigencia es saber en dónde está el dinero que los estudiantes hemos invertido en la universidad porque no se está viendo. Hay aulas saturadas de personas, **no hay capacidad ni planta física para recibir más gente y los laboratorios de ingenierías está totalmente desactualizados**” afirmó Cristian Reyes, estudiante de esta universidad.

De igual forma, una resolución expedida por el centro estudiantil, el pasado 12 de diciembre, desde la Conciliatura, se incrementó el costo de los certificados de **13 mil pesos a 26 mil pesos**. Para saber el motivo de estas alzas los estudiantes enviaron un derecho de petición que hasta el momento no ha sido respondido.

### **El fin de las vacacionales** 

Cristian Reyes, afirmó que esa misma resolución acaba con la vacacionales, que son materias que se podían cursar en vacaciones cuando se habían perdido, ahora si los estudiantes **pierden la materia deberán esperar al siguiente semestre para verla y si la carrera es anual, al año siguiente**.

### **Las exigencias de los estudiantes** 

Frente a esta situación los estudiantes de la Universidad Libre están manteniendo un diálogo con las directivas para poder mediar el alza tanto en las matrículas como en los precios de los certificados. (Le pude interesar:["En los últimos 4 años matrículas de los Andes han aumentado en 4 millones de pesos"](https://archivo.contagioradio.com/en-los-ultimos-4-anos-matriculas-de-los-andes-han-aumentado-4-millones-de-pesos/))

En ese sentido, los estudiantes proponen que no se realice el aumento en las matrículas y los certificados, en vez de ello, plantean que se ejecute un proceso de transición a dos años, en donde se evalúe la inversión y distribución del dinero, **y luego se analice de cuánto debería ser el alza para mejorar la calidad de la educación**.

### **Casos de Acoso Sexual en la Universidad** 

Otras de las exigencias que están haciendo los estudiantes es que crear un mecanismo de protección y denuncia de acoso sexual dentro de la institución, esto debido a que, según Reyes, **alumnas han denunciado públicamente actos de acoso por parte de docentes a las directivas, sin que se tomen medidas para protegerlas**.

Tanto las directivas como los estudiantes han mantenido un diálogo con el que se espera que prontamente se logre establecer un acuerdo que responda al pliego de exigencias de los estudiantes.

<iframe id="audio_23477518" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23477518_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
