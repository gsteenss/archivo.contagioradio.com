Title: Nuevo intento de Peñalosa por vender la ETB no sería aprobado por el Concejo
Date: 2017-07-21 13:20
Category: Nacional, Política
Tags: ETB, Sintrateléfonos, Venta de la ETB
Slug: nuevo-intento-de-penalosa-por-vender-la-etb-no-seria-aprobado-por-el-concejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/VENTA-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Jul. 2017] 

Tras conocer la declaración del Alcalde de Bogotá, Enrique Peñalosa, en la que asegura tener en mente un nuevo proyecto que sería presentado ante el Concejo de la ciudad para vender la Empresa de Telecomunicaciones – ETB –, **Leonardo Argüello, integrante de Sintrateléfonos,** manifestó que de esa forma “el primer mandatario estaría legitimando lo que dijo el juez en el fallo de primera instancia que niega la venta de la ETB"”. Le puede interesar: [Juez ordena suspensión temporal de la venta de la ETB](https://archivo.contagioradio.com/juez-ordena-suspension-temporal-la-venta-la-etb/)

Así mismo, manifestó que pese a la favorabilidad con la que contó Peñalosa en el Concejo cuando presentó el primer proyecto de la venta de la ETB, aprobado con más de 30 votos **“en esta oportunidad no tendría el mismo favorecimiento,** es decir, ya se nota que lo que pretenden es un negociado, se ve la imprecisión, la no planeación y que han engañado a la ciudadanía”. [Lea también: Juzgado Administrativo frena temporalmente la venta de la Empresa de Teléfonos de Bogotá.](https://archivo.contagioradio.com/juez-ordena-suspension-temporal-la-venta-la-etb/)

### **La revocatoria avanza** 

Para el integrante de Sintrateléfonos, el equipo jurídico del Alcalde se ha valido de artimañas en este proceso, sin embargo “como es un proceso constitucional, dentro de la ley, pues no van a tener argumento alguno para no retractarse. Este proceso tiene que seguir y legitimizarse en las urnas para demostrar su descontento”

Cuenta que todavía los comités promotores de la revocatoria siguen recibiendo firmas, aún ya habiendo superado el millón doscientos y siendo acreditadas 700 mil en la Registraduría, **“entonces el proceso de revocatoria va andando”** asevera Argüello. Le puede interesar: ["Apelación de Peñalosa solo busca desacreditar a la ETB": Sintratelefonos](https://archivo.contagioradio.com/juzgado-adminsitrativo-tumba-la-venta-de-la-etb/)

### **El Alcalde Peñalosa miente a la ciudadanía** 

Luego de haber sido negada la posibilidad de la venta de la ETB, el Alcalde y su gabinete, aseguraron que quien saldría muy perjudicada sería Bogotá, puesto que **no se iban a poder realizar las respectivas inversiones para el desarrollo** y la inclusión en la capital.

“El 3% de 90 billones de pesos del Plan de Desarrollo es lo que el Alcalde tiene pensado en invertir en desarrollo social, entonces **él habla de que con ese dinero va a invertir en colegios, jardines, educación, salud, adultos mayores y eso es falso,** porque en un Twitter habla de que ese dinero estaría invertido en una troncal de Transmilenio (…) El alcalde sigue mintiéndole a la ciudadanía”.

### **Sintrateléfonos seguirá realizando acciones** 

Para Argüello es importante aclarar que **seguirán trabajando para “favorecer el patrimonio público de los bogotanos,** porque es que no se puede vender una empresa con una infraestructura tan poderosa, que se le ha invertido una cantidad de dinero (…) estamos ante un posible negociado para las multinacionales o para cumplir compromisos políticos electorales de Peñalosa para llegar a la Alcaldía”.

<iframe id="audio_19931340" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19931340_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
