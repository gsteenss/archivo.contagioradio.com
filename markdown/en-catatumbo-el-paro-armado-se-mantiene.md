Title: En Catatumbo el paro armado se mantiene
Date: 2020-02-18 16:27
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Catatumbo, EPL, Norte de Santander
Slug: en-catatumbo-el-paro-armado-se-mantiene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Paro-armado-del-EPL-en-Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @albepaez {#foto-albepaez .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/junior-maldonado-ascamcat-persiste-paro-armado-en_md_47975482_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Junior Maldonado | integrante de la Asociación Campesina del Catatumbo (ASCAMCAT)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Comunidades que habitan en Catatumbo, Norte de Santander, denuncian una difícil situación humanitaria por cuenta de la confrontación entre grupos armados legales e ilegales, así como por la continuación del paro armado decretado por el EPL desde el pasado 12 de febrero. Esta situación ha generado el desplazamiento de cerca de 400 familias, y el confinamiento de poblaciones como Hacarí o Convención.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El desplazamiento de 300 personas hacia Venezuela**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Junior Maldonado, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT) la situación en Norte de Santander ha estado complicada en términos de seguridad por el paro armado del EPL y del ELN, que inició el 14 de febrero y terminó el pasado lunes 17. A ello se suma la guerra por el control territorial en la que se ven inmersos otros actores armados como Los Rastrojos y Fuerza Pública de Venezuela y Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Precisamente el pasado fin de semana se registró el desplazamiento de cerca de 300 personas por un enfrentamiento entre cerca de 150 integrantes de Los Rastrojos y la Fuerza Pública de Venezuela, y posteriormente entre el grupo paramilitar y la guerrilla del ELN. (Le puede interesar: ["Fernando Quintero Mena y la paz que no llegó a Catatumbo"](https://archivo.contagioradio.com/fernando-quintero-mena-y-la-paz-que-no-llego-a-catatumbo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/adolforivasdiaz/status/1228418377523306498","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/adolforivasdiaz/status/1228418377523306498

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **El paro armado sigue en Catatumbo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El líder de ASCAMCAT recordó que hace dos años en Catatumbo se presenta la puja violenta, luego de la salida de las FARC del territorio producto del Acuerdo de Paz. Ese conflicto se ha extendido por frontera al punto que ha llegado a Cúcuta, y con el paro armado que anunciaron tanto el ELN como el EPL se ha hecho evidente el control territorial de estos grupos, al punto que la vía Ocaña-Cúcuta está cerrada por el EPL.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Maldonado afirmó que en el departamento están confinadas las comunidades de Acarí y Convención, asimismo se presentó un desplazamiento de cerca de 400 familias en la vereda La Arenosa, ubicada entre Hacarí y Bucaratica. A ello se suma el desabastecimiento en Teorama y en Convención porque no hay una ruta comercial, lo que impide la entrada de alimentos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Y la Fuerza Pública de Colombia?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Catatumbo opera la Fuerza de Tarea Vulcano y la Fuerza de Despliegue Rápido FUDRA N°3, pero Maldonado afirma que su presencia no se siente como un respaldo a la población civil. Contrario a ello, comunidades han denunciado que se sienten amenazadas por los militares, como en el caso de Convención, lugar en el que fue asesinado[Dimar Torres](https://archivo.contagioradio.com/el-silencio-complice-de-las-fuerzas-militares-en-caso-dimar-torres/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, el integrante de ASCAMCAT manifestó que están pidiendo al Gobierno que active las rutas humanitarias, y no la militarización del territorio, para encontrar una salida para las poblaciones afectadas. De igual forma, sostuvo la necesidad de insistir en los diálogos de paz con el ELN, para evitar nuevas confrontaciones violentas que afecten a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FCOXPAZ%2Fposts%2F2504192993131886&amp;width=500" width="500" height="620" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78980} /-->
