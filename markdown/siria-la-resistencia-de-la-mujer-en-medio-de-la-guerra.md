Title: Siria: La resistencia de la mujer en medio de la guerra
Date: 2015-03-09 21:08
Author: CtgAdm
Category: El mundo, Mujer
Tags: Leila Nachawati, Liberacion de la mujer, Siria día de la mujer trabajadora, Situación de la mujer en Siria
Slug: siria-la-resistencia-de-la-mujer-en-medio-de-la-guerra
Status: published

##### Foto:Infolibre.com 

<iframe src="http://www.ivoox.com/player_ek_4189422_2_1.html?data=lZalm5mWdo6ZmKiakpuJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiIa3lIqupsaPqMafzcaYz9rOqdOf1dfOxMbOpcXj08aYx9OPl8rmysaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **Entrevista con [Leila Nachawati]:** 

El día de la **mujer trabajadora en Siria** ha sido conmemorado bajo la constante amenaza a los derechos de las mujeres que se viven luego del inicio del conflicto armado en este país, para **Leila Nachawati, activista hispanosiria**, los derechos se vienen arrebatando poco a poco.

Las mujeres han jugaron un papel muy importante en la oposición al régimen de Al-Assad, no solo combatiendo sino documentando todas las violaciones de derechos humanos y monitorizando la situación de la mujer, explica Nachawati a Contagio Radio.

Con el desarrollo del conflicto y la **aparición de las milicias islamistas radicales, la situación para ellas empeoró**. Siendo **doblemente criminalizadas**, la primera por el hecho de ser activistas de la oposición y la segunda por la persecución del islamismo radical.

Nachawati nos da ejemplos sobre **activistas** que colaboraban con Naciones Unidas en la realización de estadísticas de monitoreo de la propia guerra y de la situación de la mujer **que han sido secuestradas y desaparecidas por el Estado Islámico o el ISIS**.

Por último, la activista critica el papel de los **medios de comunicación de masas** los cuales a través de extensas notas hablan del papel de la mujer árabe y la violación de DD.HH, pero en **ningún caso dan voz a las propias mujeres víctimas o en su condición de activistas en defensa de los derechos humanos.**
