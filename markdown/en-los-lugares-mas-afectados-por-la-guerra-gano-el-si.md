Title: En los lugares más afectados por la guerra ganó el Sí
Date: 2016-10-02 20:09
Category: Nacional
Tags: FARC, Gobierno, paz, Plebiscito
Slug: en-los-lugares-mas-afectados-por-la-guerra-gano-el-si
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Plebiscito-e1470157319868.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [2 Sep 2016] 

Con un 50,21% el No le ganó al 49,78% del Sí a los acuerdos de paz pactos en La Habana entre el gobierno y las FARC. Sin embargo, lo que allí se evidenció, fue que los lugares donde se ha vivido de cerca la guerra, dieron un Sí contundente al perdón y a la reconcilización.

   
 

Caloto: Sí 72,9% - No 27%  
Cajibío: Sí 71,1% - No 28%  
Miraflores: Sí 85% - No 14%  
Silvia: Sí 73% - No 23%  
Barbacoas: Sí 73% - No 26%  
Tumaco: Sí 71% - No 28,8%  
San Vicente del Caguán: Sí 62% - No 37%  
Apartadó: Sí 52% - No 47%  
Mitú: Sí 77% - No 22%  
Valle del Guaméz Sí 86% - No 13%  
Macarena: Sí 73% - No 39%  
Puerto Asís: Sí 57% - No 42%  
Turbo: Sí 56% - No 43%  
Toribío Sí 84% -No 15%  
Bojayá Sí: 96% - No:4%

Tibú: Sí 66% - No 34%

Tarra:  Sí 91% - No 8%

Simacota: Sí 56% - No 43%

Charalá: Sí 61% - No 38%

Teorama: Sí 86% - No 13%

La Salina: Sí 88% No 11%

Mompox: Sí 63% - No 36%

San Jacinto: Sí 60% - No 39%

María La Baja: Sí 80% - No 20%

Mutatá: Sí 64%  - No 35%

Arboletes: Sí 65% - No 34%

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
