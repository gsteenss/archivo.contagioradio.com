Title: No cesa la barbarie: 55 masacres en 2020
Date: 2020-09-10 15:45
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Crisis humanitaria, INDEPAZ, Masacres
Slug: no-cesa-la-barbarie-55-masacres-en-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-municipio.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-departamento.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Muestra-artistica-contra-las-masacres.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@CesarMelgarejoA](https://www.instagram.com/cesarmelgarejoa/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la ola de violencia que vive Colombia, en la que **solo el pasado domingo se perpetraran tres masacres en los departamentos de Antioquia y Bolívar, las cuales segaron la vida de 12 personas;** el Instituto de Estudios para el Desarrollo y la Paz –[Indepaz](http://www.indepaz.org.co/)– publicó un [informe](http://www.indepaz.org.co/wp-content/uploads/2020/09/Masacres-en-Colombia-2020-INDEPAZ-8-sept-2020-pdf.pdf) que documenta las masacres que se han registrado durante el año 2020 en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

** El informe documenta la ocurrencia de 55 masacres desde el 1° de enero hasta el 8 de septiembre de 2020, en las que han sido asesinadas 218 personas. **(Lea también: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":89557,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-municipio.png){.wp-image-89557}  

<figcaption>
Mapa de los municipios en los que se han perpetrado masacres en 2020

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Asimismo, Indepaz advierte que «*se entiende por masacre el homicidio colectivo intencional de tres (3) o más personas protegidas por el Derecho Internacional Humanitario (DIH), y en estado de indefensión, en iguales circunstancias de tiempo, modo y lugar*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el estudio **el departamento en el que se han presentado más masacres es Antioquia con doce (12), seguido por Cauca y Nariño con ocho (8) cada uno y Norte de Santander con cinco (5).**

<!-- /wp:paragraph -->

<!-- wp:image {"id":89558,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-departamento.png){.wp-image-89558}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Para construir el consolidado, Indepaz tiene en cuenta los reportes que realizan las comunidades y organizaciones sociales desde los territorios; las cifras de instituciones oficiales como la Defensoría del Pueblo, la Fiscalía General y el Ministerio de Defensa; y los reportes de organizaciones no gubernamentales -ONG- de índole nacional e internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 25 de agosto Indepaz había publicado un [informe](https://archivo.contagioradio.com/45-masacres-han-sido-perpetradas-en-lo-que-va-del-2020-indepaz/) en el que documentaba 45 masacres en las que habían sido asesinadas 182 personas, es decir, en escasos 15 días se perpetraron 10 nuevas masacres que cobraron la vida de 36 personas. (Lea también: ["Si no salimos nos van a masacrar a todos... volveremos a las calles"](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El cuestionado accionar del Gobierno frente a las masacres

<!-- /wp:heading -->

<!-- wp:paragraph -->

La exsenadora Piedad Córdoba, radicó ante el Secretario General de la OTAN, una carta para que este organismo internacional excluya a Colombia, señalando al Gobierno Nacional de violar los derechos humanos y cuestionando «***¿Cómo puede el Gobierno Duque, como socio de OTAN, contribuir a la seguridad internacional cuando en sus propias narices hay masacres cada semana**, y las FF.AA. y su Ministro de Defensa no pasan de los mismos consejos de seguridad con los mismos planteamientos retóricos de siempre?»*.  

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/piedadcordoba/status/1303670508446195713","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/piedadcordoba/status/1303670508446195713

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otro lado, el senador Iván Cepeda cuestionó las **recientes declaraciones del Alto Comisionado para la Paz, Miguel Ceballos, quien afirmó que las masacres en Colombia eran producto de «*algo que no funcionó después de la firma del Acuerdo \[de Paz\]*».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1303406595343364096","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1303406595343364096

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Adicionalmente, en el centro de Bogotá se realizó un acto simbólico en el que artistas tiñeron de rojo el agua de las fuentes del emblemático Eje Ambiental, en señal de protesta por las masacres perpetradas en diferentes zonas del país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jerojasrodrigue/status/1303294228458622976","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jerojasrodrigue/status/1303294228458622976

</div>

<figcaption>
Las aguas rojas simbolizan la sangre que se ha derramado por las víctimas

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
