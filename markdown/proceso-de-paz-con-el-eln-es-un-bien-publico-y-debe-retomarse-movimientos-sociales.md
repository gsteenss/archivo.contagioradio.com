Title: Proceso de paz con el ELN es "un bien público" y debe retomarse: Movimientos Sociales
Date: 2018-02-01 12:49
Category: Paz
Tags: diálogo con el ELN, ELN, Gobierno Nacional, Movimiento social, Organizaciones sociales
Slug: proceso-de-paz-con-el-eln-es-un-bien-publico-y-debe-retomarse-movimientos-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [01 Feb 2018] 

Organizaciones de la sociedad civil, plataformas de paz y derechos humanos e iniciativas de la sociedad civil enviaron un mensaje a las delegaciones del Gobierno Nacional y el ELN para que **retomen la negociación de la mesa de Quito en Ecuador**. Indicaron que los hechos violentos que han ocurrido, sólo ponen en riesgo a la población civil y a los integrantes de la Fuerza Pública.

En un comunicado manifestaron que “el proceso de Quito tiene un gran valor como bien público para la paz de Colombia” por lo que se debe **“honrar la palabra”** para que se active nuevamente la mesa de negociación y se inicie un quinto ciclo de conversaciones teniendo en cuenta un cese bilateral al fuego.

Además, le solicitaron a las Naciones Unidas “para que bajo su mandato **faciliten y verifiquen las garantías** necesarias para este proceso, blinden los territorios y protejan las comunidades”. Pidieron que se dé un diálogo con una delegación representativa de la sociedad colombiana para interlocutar con el presidente Juan Manuel Santos y el ELN.

### **Sociedad civil debe movilizarse por la paz de Colombia** 

De acuerdo con Marylen Serna, integrante de Congreso de los Pueblos, la ciudadanía en general debe estar comprometida con la paz para el país y debe “seguir insistiendo en una **salida negociada al conflicto armado** que vive Colombia”. Además, dijo que la sociedad debe estar comprometida con la movilización como herramienta para rodear la mesa de diálogos. (Le puede interesar: ["No se puede echar a la basura proceso de paz entre el ELN y Gobierno: Iván Cepeda"](https://archivo.contagioradio.com/no-se-puede-echar-a-la-basura-proceso-de-paz-entre-eln-y-gobierno-ivan-cepeda/))

Ante esto, indicó que los movimientos sociales le han pedido al presidente de la República y al ELN que se reúnan con ellos para “expresarle que la participación de la sociedad no tiene que ser solamente en algún punto de los que se está acordando sino en la negociación en general”. Dijo que **hay un momento de crisis** que requiere la participación de la sociedad y de la comunidad internacional enmarcada en organizaciones gubernamentales y movimientos sociales regionales.

### **Suspender los diálogos fue un gran desacierto** 

Para los movimientos sociales, la decisión de Juan Manuel Santos de suspender las negociaciones en Quito fue un **gran desacierto** en la medida en que “el conflicto ha escalonado y no se abrió la posibilidad de un nuevo cese al fuego bilateral”. Así mismo, indicaron que todo proceso de negociación tiene grandes dificultades pero que se debe seguir por el camino del uso de la palabra y no las armas.

Serna indicó que la reacción ante los hechos violentos que se han presentado debe ser “**sentarse nuevamente en la mesa** y evaluar qué está pasando y cómo se continúa”. No hacerlo, según las organizaciones, le da la ventaja a quienes “instigan por acabar el proceso de paz con mensaje irresponsables y provocadores que en el fondo tienen la intención de mantener la guerra para salvaguardar sus propios intereses”. (Le puede interesar: ["Parlamentarios del Reino Unido instan a Gobierno y ELN a retomar diálogos"](https://archivo.contagioradio.com/parlamentarios-de-reino-unido-instan-a-equipos-de-mesa-de-quito/))

### **Incumplimientos de los acordado generan desconfianza** 

En repetidas ocasiones, los movimientos sociales han manifestado que los incumplimientos del Acuerdo de Paz que se firmó en la Habana **“generan desconfianza y una gran incertidumbre”** para procesos y negociaciones como los que se están llevando con el ELN. Por esto, han dicho que los incumplimientos no se dan solamente a las partes involucradas, sino que también repercute en las comunidades que aún viven el conflicto armado en los territorios.

Finalmente, reiteró que la sociedad civil **no puede ser indiferente** a lo que está sucediendo teniendo en cuenta que “lo que se está jugando es el presente y el futuro del país”. Serna recalcó la importancia de la sensibilización social para buscar instancias de participación y movilización que respalden el proceso de paz “que tiene que ver con detener el conflicto armado y construir cambios que necesita el país para una vida digna”.

<iframe id="audio_23499418" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23499418_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
