Title: No se puede postergar la idea de la paz: Alejandra Gaviria a 30 años del asesinato de su padre
Date: 2017-12-06 12:53
Category: DDHH, Nacional
Tags: Alejandra Gaviria, Francisco Gaviria
Slug: no-se-puede-postergar-la-idea-de-la-paz-alejandra-gaviria-a-30-anos-del-asesinato-de-su-padre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQUs4k-W0AATcTP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: HIJOS Bogotá] 

###### [06 Dic 2017] 

El próximo 10 de diciembre se cumplirán 30 años de la desaparición y asesinato de Francisco Gaviria, militante de la Unión Patriótica, y de acuerdo con su hija Alejandra Gaviria, el mayor temor es sentir que **“estamos en un contexto similar”** al que vivió su papá, en términos de agresiones a líderes sociales y defensores de derechos humanos, razón por la cual considera que es hora de reflexionar, para no repetir historias.

“Estamos casi que, en ese mismo punto, y hoy volvemos a tener esos mismos dilemas: una parte de la sociedad que le apuesta a la paz, que cree que en este país debemos vivir todos, y otro gran porcentaje de la **sociedad que continúa pensando que la eliminación física del otro y del pensamiento es la salida**” afirmó Gaviria.

Además, frente a estos 30 años desde el asesinato de su padre, Alejandra afirmó que puede concluirse irrefutablemente que “el conflicto no se acabó nunca a punta de bala”, “que la propuesta de eliminar la diferencia, también ha fracasado”. Se intentó exterminar a **partidos políticos, militantes de izquierda, líderes sociales y defensores de derechos humanos**, hoy más que nunca el proyecto de la paz está en vigencia y hace parte de las exigencias de las personas. (Le puede interesar:["Las víctimas seguirán exigiendo que se cumpla lo pactado"](https://archivo.contagioradio.com/las-victimas-seguiran-exigiendo-que-se-cumpla-lo-pactado/))

Gaviria señaló que una de las conclusiones más importantes tiene que ver con la protección y las garantías para el ejercicio de la política y el libre pensamiento “si no hacemos algo por cambiar el ciclo de violencia en donde se asesina a los líderes sociales y a los líderes de la paz, en donde el Estado como hace **30 años niega la sistematicidad y no opera, tal vez se nos vuelva a postergar la idea de la paz**”.

### **Las víctimas continúan a la sobra de la justicia del Estado** 

Otra de las salvedades que hace Alejandra Gaviria es el accionar del gobierno hacia las víctimas, debido a que los cambios que se han hecho en la JEP sobre las circunscripciones de paz, llevan a pensar que “en este momento como hace **30 años las víctimas aún no son reconocidas, no son reparadas y poco le importan a las personas que están en el podes**”.

En ese sentido Alejandra expresó que la tarea de defender la paz no está en el Congreso sino en los colombianos y las víctimas, “**consideramos que las lecciones del tiempo nos dicen que no podemos parar de pensar, soñar y construir la paz**”. (Le puede interesar: [Las víctimas dimos ejemplo de dignidad y respeto" Alejandra Gaviria"](https://archivo.contagioradio.com/las-victimas-dimos-ejemplo-de-respeto-y-dignidad-alejandra-gaviria/))

<iframe id="audio_22507265" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22507265_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
