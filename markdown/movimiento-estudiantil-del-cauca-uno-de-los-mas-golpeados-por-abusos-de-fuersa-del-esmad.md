Title: 96 heridos por represión de ESMAD durante paro estudiantil en Popayán
Date: 2018-12-14 11:52
Author: AdminContagio
Category: DDHH, Movilización
Tags: Abuso de fuerza, Cauca, ESMAD, heridos, Movimiento estudiantil
Slug: movimiento-estudiantil-del-cauca-uno-de-los-mas-golpeados-por-abusos-de-fuersa-del-esmad
Status: published

###### [Foto: UNEES Cauca] 

###### [14 Dic de 2018] 

Una vez más el movimiento estudiantil del departamento del Cauca denunció la fuerte arremetida por parte del Escuadrón Móvil Antidisturbios, que dejó como saldo nueve estudiantes heridos, **uno de ellos, Esteban Mosquera, perdió su ojo como consecuencia de ser golpeado por una grada aturdidora. Durante los 60 días de paro la cifra asciende a 96 personas.**

El pasado 13 de diciembre, 9 estudiantes resultaron heridos en el marco del paro nacional estudiantil, cuando incluso el Ejército Nacional manifestó que ingresaría al centro educativo, pese a no tener ninguna orden por parte de alguna autoridad. Además el estudiantado afirmó que integrantes del ESMAD les lanzaron piedras y gases hacia sus cuerpos.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/WhatsApp-Video-2018-12-14-at-11.49.16-AM.mp4\[/KGVID\]

Sin embargo, la comisión de derechos humanos de la Universidad del Cauca ha denunciado en múltiples ocasiones los abusos de fuerza por parte del ESMAD, que ya han dejado más víctimas en este departamento, durante las movilizaciones estudiantiles del último semestre.

### **El Cauca víctima del ESMAD durante el paro estudiantil**

Desde la primera movilización del 8 de noviembre, el movimiento estudiantil del Cauca denunció los ataques al campamento  por la educación, instalado por estudiantes en el Parque Caldas en Popayán, que fue atacado y desalojado por el ESMAD y a pesar de que en la zona había niños y adultos mayores, los uniformados hicieron uso de gases lacrimógenos y bombas aturdidoras, dejando un total de** 87 personas heridas, entre las que se encontraron 3 estudiantes que presentaron fracturas en tobillo, clavícula y nariz. **(Le puede interesar:["Gobierno responde con represión a las exigencias de los estudiantes"](https://archivo.contagioradio.com/represion-exigencias-estudiantiles/))

De igual forma la comisión de derechos humanos de la Universidad del Cauca denunció que desde el inicio del paro estudiantil han detectado la presencia de policías infiltrados, que **"inclusive han entrado al lugar en donde está el campamento"**, y la persecución a varios de los estudiantes que conforman esa instancia.

Posteriormente, el movimiento estudiantil denunció que desde las 5 de la mañana del pasado 23 de noviembre, más de 150 miembros del ESMAD, en conjunto con integrantes de la Policía y agentes vestidos de civil, acordonaron el centro universitario y arremetieron en contra del plantel educativo** quebrando los vidrios y poniendo en riesgo a las personas que se encuentran allí.**

Durante estos actos de violencia por parte de la Fuerza Pública, el rector de este centro educativo, José Luis Diago Franco, acompañado de organismos garantes de derechos humanos tuvo que intervenir para garantizar la seguridad de la comunidad académica y la disminución de acciones violentas desde el ESMAD. (Le puede interesar: ["Estudiantes de Uni.Cauca están acordonados por más de 150 integrantes del ESMAD"](https://archivo.contagioradio.com/estudiantes-de-uni-cauca-estaria-acordonados-por-mas-de-150-integrantes-del-esmad/))

Debido a todos estos hechos de violencia, el movimiento estudiantil esta convocando a una velatón en el Parque Caldas, desde las 6:00pm y se recibirán donaciones que se entregarán a las familias de los estudiantes afectados por el ESMAD y graves situaciones de salud.

Hasta el momento no se tiene conocimiento de investigaciones o sanciones a los integrantes de la Fuerza Pública que han participado en estas acciones, que para los estudiantes, son evidentes violaciones a los DDHH y al derecho a la protesta por parte del movimiento a pesar de que se han presentado las denuncias correspondientes ante la fiscalía.

<iframe id="audio_30797709" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30797709_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
