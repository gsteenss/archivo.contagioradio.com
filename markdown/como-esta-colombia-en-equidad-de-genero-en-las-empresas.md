Title: ¿Cómo está Colombia en equidad de género en las empresas?
Date: 2017-08-03 13:52
Category: Mujer, Nacional
Tags: Equidad de género, feminismo, licencia de maternidad, licencia de paternidad, mujeres, paridad
Slug: como-esta-colombia-en-equidad-de-genero-en-las-empresas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Rnking-de-Equidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube] 

###### [03 Ago. 2017] 

El **III Ranking de Equidad de Género en las Organizaciones** en el que participaron 164 organizaciones de Colombia, realizado por la Secretaria Distrital de la Mujer junto a Aequales y el Colegio de Estudios Superiores de Administración - CESA -arrojó varios retos, dado que solo el 39% del sector de empresas privadas ha invertido en prácticas orientadas a alcanzar la equidad de género y las mujeres solo ocupan un 38% de las posiciones de liderazgo.

Si se compara el resultado de este Ranking con los datos arrojados en 2016, no hay muestras de avances suficientes. Por ejemplo en 2016 el sólo el 34% de las mujeres ocupaban cargos de dirección y únicamente el 29,8% de las organizaciones cuentan con políticas claras sobre la equidad de género y/o diversidad.

Este Ranking 2017,  pudo evidenciar que cada vez es más necesario dejar de hablar de paridad, sino que por el contrario **es primordial hacer mucho más trabajo para poder mejorar los indicadores de equidad de género** en el país. En contexto: [Así va la equidad de Género en las Empresas colombianas](https://archivo.contagioradio.com/el-ranking-de-equidad-de-genero-en-las-empresas-en-colombia/).

“No basta con decir yo tengo 50% de mujeres y 50% de hombres en mi junta directiva o que la mitad están en cargos directivos, sino las empresas públicas y privadas tienen que empezar a hacer mucho más trabajo para poder estar entre los primeros puestos de las mejores empresas” aseveró **Cristina Vélez Valencia, Secretaria Distrital de la Mujer de Bogotá.**

### **Entidades públicas y privadas aún tienen mucho por aprender en equidad de género** 

Según este Ranking las empresas privadas tienen la ventaja de que pueden ser más innovadoras en temas como extensión de la licencia de paternidad, cosa que el sector público no puede llevar a cabo y que es uno de los temas que contribuye a la equidad en las organizaciones. Le puede interesar: [A este ritmo "harán falta 81 años más para lograr la equidad de género”](https://archivo.contagioradio.com/a-este-ritmo-harian-falta-81-anos-para-lograr-la-equidad-de-genero/)

Dentro de las prácticas que busca lograr un balance entre la vida personal y la laboral de sus trabajadores y trabajadoras, el Ranking dice que **el 52% de empresas privadas cuentan con la opción de trabajo remoto,** y el 71% de ellas cuentan con días flexibles para fechas especiales, un incremento comparado al 64% en 2016.

### **Proponen crear un Comité de Equidad de Género en las empresas** 

Una de las grandes lecciones del Ranking es que **el tema de equidad de género hay que enunciarlo, es decir hacer explícito** que es necesario hablarlo y empezar a gestionar prácticas que la promuevan.

“Las organizaciones que están puntuando el ranking no lo hacen porque así sucedió o pasó, sino porque están tomando decisiones para construir más equidad en las organizaciones y estos comités permiten lograrlo”

Estos Comités de Equidad de Género son aquellos en donde **hombres y mujeres le hacen seguimiento por ejemplo a que las prácticas de contratación** no tengan dinámicas que discriminen a las mujeres o a la población diversa. Además, están pendientes de que las políticas que se han diseñado se implementen.

### **¿Colombia avanza en equidad de género?** 

Para Vélez, si bien hace falta mucho por hacer, se han dado pasos sobre todo en las ciudades “no tanto en las áreas rurales” para ir cerrando la brecha entre hombres y mujeres. Pero hace la claridad de que **este Ranking solamente hace referencia a organizaciones formales** y la economía colombiana y bogotana sigue siendo mayoritariamente informal.

“En ese sentido **las brechas por las que hay que trabajar en la informalidad son aún más profundas** y ahí tenemos mucho trabajo que hacer”. Le puede interesar: [Las mujeres cultivadoras de coca del sur de Colombia se movilizan por la equidad](https://archivo.contagioradio.com/las-mujeres-cultivadoras-de-coca-del-sur-de-colombia-se-movilizan-por-la-equidad/)

### **Brecha Salarial entre hombres y mujeres sigue siendo muy amplia** 

Para poder analizar este tema, el Ranking midió a hombres y mujeres que estén desarrollando el mismo trabajo y en este tema le va mejor al sector público que al privado porque existen, por ejemplo, tablas de salarios estrictas y reguladas. Mientras que en el sector privado existen grandes brechas incluso en empresas que han trabajado bastante en ese tema.

“Yo creo que **lo que hay que hacer es un trabajo de empoderamiento a las mujeres** porque esta brecha persiste en parte por temas de negociación, porque nos han enseñado que negociar no es algo que debamos hacer y a veces tenemos que tomar más riesgos y negociar duro para lograr una mayor equidad”.

### **Recomendaciones para contribuir a la equidad de género en las organizaciones** 

Seguir ampliando los tiempos de licencia de paternidad, tener prácticas de contratación *ciegas al sexo* y a la identidad de género y hablar del tema de equidad de género, son los tres puntos en los que según la Secretaria Distrital de la Mujer de Bogotá se deben continuar trabajando.

“**Sigamos poniendo este tema sobre la mesa y que sea un tema prioritario,** porque esto no es un tema de las que queremos o trabajamos el tema de mujer sino es un factor que genera más productividad e innovación en las organizaciones”. Le puede interesar: [Los impuestos afectan más a las mujeres que a los hombres](https://archivo.contagioradio.com/los-impuestos-afectan-mas-a-las-mujeres-que-a-los-hombres/)

### **Ranking de las 10 mejores empresas del sector privado con equidad de género ** 

1.  Pfizer
2.  Fundación Plan
3.  Sanofi
4.  Johnson & Johnson
5.  P&G
6.  Citibank
7.  3M Colombia S.A
8.  SAP
9.  Equion
10. Codensa – Emgesa

### **Ranking de las 5 mejores empresas del sector privado con equidad de género ** 

1.  Secretaria de Movilidad
2.  IDIPRON
3.  Personería de Bogotá
4.  Secretaria Distrital de Hacienda
5.  Secretaria Distrital de Integración Social

<iframe id="audio_20154272" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20154272_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
