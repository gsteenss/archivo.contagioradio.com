Title: Enrique Peñalosa: Negocios, ineptitud e indolencia
Date: 2016-05-28 23:12
Category: Javier Ruiz, Opinion
Tags: Bogotá, ETB, Peñalosa
Slug: enrique-penalosa-negocios-ineptitud-e-indolencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/penalosa-enr07pvg003x.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lafarge Holcim 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 31 May 2016 

Alguien dijo una vez que la historia se repite primero como tragedia y luego como farsa. Lo anterior puede aplicar en la actual alcaldía de Enrique Peñalosa en donde su popularidad es paupérrima por las múltiples decisiones impopulares que está tomando y por las salidas en falso que lo han hecho objeto de múltiples críticas y burlas. Cómo mencione en una columna anterior, el Enrique Peñalosa de 2016 no es diferente al Enrique Peñalosa de 1998 y las mismas medidas impopulares que tomo en su primera alcaldía las está repitiendo en su nueva alcaldía generando el rechazo de la ciudadanía y evidenciando la pobreza argumentativa de sus pocos escuderos.

[Enrique Peñalosa volvió a la alcaldía engañándonos con su eufemismo de “recuperar” a Bogotá para todos cuando sus decisiones van en contravía a lo que pregona. A parte de su revanchismo destruyendo toda la política social gestada en la administración anterior quiere imponer una restauración neoliberal en donde lo privado debe imponerse porque, supuestamente, es eficiente. Bajo está excusa se busca vender la Empresa de Telecomunicaciones de Bogotá con el chantaje de que si no se vende no hay recursos para salud y educación.]

[Es de cuestionar el afán del alcalde Peñalosa en vender la ETB porque se duda mucho que la empresa este tan mal cuando ha mostrado mejorías y ofrece más servicios siendo una alternativa frente a otras empresas de telecomunicaciones. Además, está muy mal por parte del alcalde chantajear a la ciudad con la mentira de que si no se vende la ETB no habrá educación y salud cuando precisamente su función, con el gran presupuesto que tiene la ciudad, es garantizar esos derechos y no privatizarlos cómo lo piensa hacer de todas formas.]

[Las privatizaciones no han servido para beneficiar a las personas o para el impulsar el desarrollo sino para hacer negocios con tal de enriquecer a unos pocos que ganan dividendos cuando lo público se privatiza siendo irónico que desprecien lo público y, a la vez, se llenan de dinero con lo público. A Peñalosa toca decirle que Bogotá no es un “negocio redondo” del cual puede sacar ganancias cómo lo ha hecho con su “hijo predilecto” Transmilenio que ha demostrado ser ineficiente y un modelo fracasado de movilidad y para defenderlo sigue engañando a la ciudad diciendo que hace lo mismo que un metro pero más barato. Señor alcalde, Bogotá es una ciudad y no una empresa que toca ponerle orden como solía decir en sus campañas electorales perdedoras del 2007 y 2011.]

En su afán de vender la ciudad al mejor postor Peñalosa se ha olvidado gobernar y las supuestas mejoras que prometió no se han visto dejando en duda al Enrique Peñalosa “gerente” y “visionario”. El alcalde se promociona como un gran líder y por desgracia su figura ha sido cuestionada por sus posibles títulos académicos falsos afectando su legitimidad y sus salidas en falso cada vez son más mostrando su cara indolente y cuando intenta corregir esos errores queda peor que antes. Cómo olvidar lo sucedido con Rosa Elvira Cely, las ambulancias o lo sucedido con el ciclista Estaban Cháves cuando esté reclamo más apoyo al ciclismo en Bogotá.

[Y así podríamos hacer una lista de los errores del alcalde Peñalosa que en su afán de feriar la ciudad se olvida de sus habitantes y olvida su papel de alcalde de los bogotanos para gobernar a una Bogotá mejor para pocos.]

[Y para los escuderos (sobre todo uno muy “irreverente”) de Peñalosa solo les digo que cuando un tonto coge un camino el camino se acaba pero el tonto sigue.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
