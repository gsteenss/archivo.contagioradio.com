Title: "El coronavirus va en cohete, y este Gobierno en mula"
Date: 2020-04-13 18:50
Author: CtgAdm
Category: Actualidad, Entrevistas
Tags: Salud
Slug: coronavirus-va-en-cohete-y-este-gobierno-va-en-mula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/c3a24510-d8da-4ee9-9d64-2c6d25547c0d.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/hospital-de-tadó.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-10.31.55-AM.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-09-at-10.58.57-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Archivo de Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 12 de abril el Gobierno presentó el **[Decreto 538](https://www.minsalud.gov.co/Normatividad_Nuevo/Forms/DispForm.aspx?ID=5982)**, que entre varios puntos, señala que todo el personal de la salud debe estar disponible para atender la emergencia del Coronavirus, una orden legislativa que nace en medio de las 3 primeras muertes de funcionarios de la salud por este virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esta orden legislativa [35 organizaciones](file:///C:/Users/Usuario/Downloads/CARTA%20RECHAZO%20DECRETO.pdf) escribieron una carta al gobierno de Duque rechazando lo allí estipulado, exigiendo garantías para su salud, además de un pago oportuno y justo de los sueldos de los funcionarios en territorios.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CarolinaCorcho/status/1249881701439938560","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CarolinaCorcho/status/1249881701439938560

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, desde la Federación Médica Colombiana (FMC)junto con otras organizaciones médicas presentan una carta al Presidente Duque con 9 puntos que permiten atender la situación que viven los trabajadores de la salud del país, acciones estudiadas y reales, que se pueden aplicar inmediatamente, según lo afirmó Carolina Corcho, vicepresidenta de la FMC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quién además afirmó que es necesario tomar acciones rápidas, pero sin dejar de lado los derechos de los trabajadores de este gremio, *"mientras el Coronavirus va en cohete y este Gobierno va en mula, por eso nosotros propones acciones contundentes desde ya".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Chocó no hay seguridad, no hay protección ni tampoco sueldos para personal de salud

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un ejemplo de porque no están de acuerdo con el decreto muchos de los integrantes de la rama salud, es la deuda que tiene el estado con los médicos, enfermeros, auxiliares y demás personal de la salud en los centros hospitalarios de Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una deuda que supera los \$5.000 millones de pesos, y corresponde a vacaciones, primas, cesantías y salarios sin retribuir desde el 2013 al personal del Hospital San José de Tadó.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83107,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-09-at-10.58.57-AM.jpeg){.wp-image-83107}  

<figcaption>
Foto: María Nancy Cossio | Auxiliar de enfermería

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

María Nancy Cossio, auxiliar de enfermería de este Hospital, denuncia que son más de 60 empleados del hospital de Tadó los que exigen al Ministerio de Salud y a la Gobernación la entrega de estos recursos, *“nosotros sabemos que estos dineros los tiene la gobernación desde el 2017, pero cada vez que los pedimos nos dicen que falta un aval del Ministerio para que se salden las deudas". *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además según la enfermera no hay garantías para un trabajo digno y seguro, *"nos deben meses de sueldo poniendo en riesgo la estabilidad de nuestros hogares, y como si fuera poco, no nos dan insumos de protección que eviten el contagio, en medio de una infraestructura que se cae a pedazos".*

<!-- /wp:paragraph -->

<!-- wp:image {"id":83092,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/hospital-de-tadó-1024x550.jpg){.wp-image-83092}  

<figcaption>
Foto: Cortesía - Hospital de Tadó

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Al incumplimiento de los salarios se le suma el mal estado de la infraestructura del centro médico, en donde es evidente el olvido estatal, *"no tenemos una Unidad de Cuidados Intensivos, las ambulancias están abandonadas por falta de mantenimiento y las paredes día a día se debilitan más".*

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":83091,"width":251,"height":247,"sizeSlug":"medium"} -->

<div class="wp-block-image">

<figure class="aligncenter size-medium is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/c3a24510-d8da-4ee9-9d64-2c6d25547c0d-225x300.jpg){.wp-image-83091 width="251" height="247"}  
<figcaption>
  
  
  
  
*Foto: Cortesía María Cossio*
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

**Según el Departamento Nacional de Planeación;** Actualmente el departamento de [Chocó](https://archivo.contagioradio.com/la-desigualdad-economica-es-nuestra-pandemia/) cuenta con tres Unidades de Cuidados Intensivos (UNCI), y tan solo 27 camas hospitalarias**.** A esta USI, la complementan seis hospitales de primer nivel, que cubren cerca de 30 municipios, y uno de segunda categoría el cual decepciona más de 530.000 habitantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un caso similar al de Tadó, se repite en el hospital San Francisco de Assis, ubicado en Quibdó, lugar donde los funcionarios no reciben sueldo desde noviembre de 2019, además de ser receptor del primer caso del coronavirus en el departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 11 de abril el Ministerio de Salud, confirmó el contagio de una mujer de 47 años en Quibdó, cuyo oficio se desarrolla en este hospital, según el Ministerios aún se estudia como ella puedo contraer el coronavirus.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LUISEROLAVE/status/1246158187499323399","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LUISEROLAVE/status/1246158187499323399

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### "Los médicos que mueren por coronavirus no son héroes, sino mártires": Federación Médica Colombiana 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según 35 organizaciones, sindicatos, colegios y asociaciones que enviaron una carta al Presidente duque este 12 de abril, rechazando el decreto 538 de 2020 busca forzar a los profesionales y trabajadores de la salud a concurrir a la atención de la pandemia sin contar con los recursos adecuados de bioseguridad y condiciones laborales dignas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"A pocas horas de la muerte de dos médicos y un conductor de ambulancia en el marco de la pandemia el Gobierno hace pública una norma inconsulta y autoritaria, mientras se niega a negociar sobre las condiciones laborales así como ignora los comunicados que desde varios días las agremiaciones del sector salud hemos venido realizando"*
>
> <cite>Señala el Comunicado</cite>

<!-- /wp:quote -->

<!-- wp:video {"autoplay":false,"id":83099,"loop":false,"muted":false,"playsInline":false,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-10.31.55-AM.mp4"} -->

<figure class="wp-block-video aligncenter">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-10.31.55-AM.mp4">
</video>
  

<figcaption>
*Video: Dr. Jorge Dario Mendez | Cirujano general en Htal. Departamental de Florencia Caquetá*

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Cecilia Vargas, presidenta de la Organización Colegial de Enfermería y Fiscal de la Asociación Nacional de Enfermeras de Colombia; señaló *" hoy la situación es caótica, y en medio del coronavirus sale un decreto qué en vez de ayudar lo que hace es profundizar la problemática en el sector salud un decreto que desconoce la realidad que enfrenta el personal de salud cómo ejército de primera línea"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además hace un especial énfasis ante la posibilidad de adelantar los grados, *"la tarjeta profesional es un requisito infaltable para poderse graduar, con ella en los colegios le damos el aval y le decimos a la sociedad este profesional está apto para trabajar y tiene estándares altos, al adelantar este tramite se omite este estudio y se genera solo un ejercito sin armas para atacar esta crisis"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "A pesar de que el Ministro de Salud, diga que nosotros no necesitamos un tratamiento especial, hoy se demuestra que sí; porqué estamos enfrente, y necesitamos saber si estamos contagiados o no, y no solo un decreto que en vez de solucionar va a profundizar la crisis"
>
> <cite>Cecilia Vargas | Pta. Organización Colegial de Enfermería</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Al final del comunicado las agremiaciones firman, "*queremos cumplir con nuestro deber con entrega y vocación solidaria por el país, hoy no sirve una larga lista de personas de salud muerto y sin poder detener la pandemia, los invitamos a escucharnos y no seguir como hasta el momento haciendo caso omiso a las peticiones de los diferentes gremios*".

<!-- /wp:paragraph -->
