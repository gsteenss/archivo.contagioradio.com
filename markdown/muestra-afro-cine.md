Title: La herencia africana en Colombia se ve en la Muestra de cine Afro
Date: 2017-05-25 12:08
Author: AdminContagio
Category: 24 Cuadros
Tags: afro, Cine, Cinemateca Distrital, colombia
Slug: muestra-afro-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/afro_opt.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:IDARTES 

###### [25 May 2017] 

Desde el **23 hasta el 31 de mayo** se realiza en Bogotá la **Muestra de cine Afro**, un espacio dedicado a la música, la memoria, la cultura, las luchas, y en general, la herencia africana en la producción cinematográfica nacional e internacional, que tiene como escenario central la Cinemateca Distrital.

El evento busca reflexionar sobre la **red de significados que se trenzan en las historias y conflictos de las poblaciones negras, palenqueras y afrodescendientes**, a partir de la revisión de la producción que sobre la diáspora africana se ha realizado en el país y la memoria de los pueblos en la narrativa del cine colombiano contemporáneo que en ellas reside.

La programación que inicia con el estreno de la película colombiana "**Keyla" de Viviana Gómez Echeverry** , recoge temas como las **herencias de la esclavitud, el colonialismo, el racismo, las migraciones y las maneras como los pueblos han reafirmado su identidad**, además de una curaría invitada: África, Cine y revolución y un ciclo sobre cine experimental e inmigración del FCAT – Festival de Cine Africano de Tanger.

Adicional a las actividades de exhibición, la muestra incluye un componente académico entre los que destacan la proyección de **"La marimba de los espíritus" y "San Pacho, un santo blanco para un pueblo negro"**, ambos dirigidos por **Gloria Triana y Jorge Ruiz Ardila** que  contará con la presentación de Eduardo Restrepo y Urian Sarmiento.

En la Biblioteca Pública El Tintal, con presencia de curadora brasileña Lúcia Monteiro, se proyectará "**Mujeres en la guerra**" de Ike Bertels. También se rendirá **homenaje a Manuel Zapata Olivella** con la proyección de la película "Tierra amarga" de Roberto Ochoa en un evento que contará con la participación de Edelmira Zapata y Fulgencio Yanguiz.

El próximo 30 de mayo, se realizará la presentación de la **muestra "África(s). Cine y revolución"**, donde las independencias de Guinea-Bissau, Angola y Mozambique son retratadas en películas sobre lucha y memoria. Las[actividades de la Muestra Afro](http://www.cinematecadistrital.gov.co/sites/default/files/2017/prog%20web%20AFRO(1).pdf), son de entrada libre hasta completar el aforo.
