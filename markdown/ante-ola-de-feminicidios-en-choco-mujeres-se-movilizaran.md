Title: Mujeres se movilizarán en Chocó ante ola de feminicidios
Date: 2017-07-21 15:45
Category: Mujer, Nacional
Tags: Asesinato de Mujeres, Chocó, feminicidios, genero, Violencia de género
Slug: ante-ola-de-feminicidios-en-choco-mujeres-se-movilizaran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Jul. 2017] 

El asesinato de más de **10 mujeres en el departamento de Chocó tiene en alerta a la Ruta Pacífica de las Mujeres**, quienes han lanzado una alerta ante los continuos feminicidios y diversas violencias a las que están expuestas las mujeres a diario sin que haya una respuesta efectiva por parte de las instituciones ni las autoridades.

“Toda esta situación es muy preocupante, porque se han presentado varios asesinatos en contra de las mujeres y no entendemos los motivos porqué se está llegando a esta situación. Desde la institucionalidad se debe hacer todo un trabajo de incidencia, de prevención y no se está haciendo” relató **Claudia Palacios integrante de la Ruta Pacífica de las Mujeres, seccional Chocó.**

Además, dijo que cuando las mujeres toman la decisión de denunciar a su agresor la ley las revictimiza “cuando las mujeres colocan las denuncias no se les brinda la mayor protección para que no sean foco de feminicidios”.

### **Plantón para exigir investigaciones prontas en casos de feminicidios** 

Este martes 25 de julio, en la ciudad de Quibdó, Chocó, s**e está convocando a un plantón en el Parque Centenario a las 3 p.m.** para rechazar todos los tipos de violencia que afectan a las mujeres y para exigir sean investigados los casos de feminicidios.

“Porque las mujeres que no están siendo asesinadas, siguen siendo victimizadas por todo el tema de la violencia intrafamiliar o por las instituciones”. Le puede interesar: [Colombia se disputa segundo lugar en Feminicidios en América Latina](https://archivo.contagioradio.com/colombia-se-disputa-segundo-lugar-en-feminicidios-en-america-latina/)

Hace más de 15 días **se conoció del asesinato de la docente Lesvia Ivonne Andrade** de quien se sabe su autor material se entregó, pero no se han dado mayores avances de las investigaciones.

Así mismo, el pasado 16 de julio se supo **del feminicidio de Yurleidy González Palacios de 22 años y el de su hijo Neider González Mena**, presuntamente a manos del patrullero de la Policía Nacional Hamilton Mena Palacios “en vez de ir mostrando cifras y avances, tratan como de ocultar la información. Son los medios los que resultan dando información y no las autoridades”.

### **Existe un subregistro en las cifras** 

Manifiesta Palacios que pese a que existen unas cifras que son manejadas desde Medicina Legal, como Ruta Pacífica de las Mujeres consideran que **existe un subregistro, dado que en muchos lugares del país no hay presencia de las instituciones** y no hay la posibilidad de verificar cuántas mujeres han sido víctimas de feminicidio.

“Algunos sucesos que pasan en los municipios sino los divulgan por los medios de comunicación esos casos no son registrados, por que en Chocó Medicina Legal solo está en el municipio de Quibdó, en los otros municipios no. A través de ellos es que tenemos algunas cifras. Pero en el departamento han aumentado los homicidios, la violencia sexual e intrafamliar”.

Dice Medicina Legal que las cifras manejadas **en lo corrido de 2017, son** 2**04 casos de asesinatos de mujeres en diferentes regiones del país** y han recibido 345 casos de denuncias de feminicidios que son materia de investigación. Le puede interesar: [413 mujeres fueron víctimas de feminicidio en los últimos 3 años](https://archivo.contagioradio.com/413-mujeres-han-sido-victimas-de-feminicidio-en-colombia-en-los-ultimos-3-anos/)

<iframe id="audio_19933990" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19933990_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="ssba ssba-wrap">

</div>

<div class="osd-sms-wrapper">

</div>
