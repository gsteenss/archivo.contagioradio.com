Title: Por primera vez veo a Néstor Humberto Martínez acorralado: Ramiro Bejarano
Date: 2018-11-15 16:47
Author: AdminContagio
Category: Nacional, Política
Tags: Nestor Humberto Martínez, Odebrecht
Slug: por-primera-vez-veo-a-nestor-humberto-martinez-acorralado-ramiro-bejarano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Nestor-Humberto-Martinez-e1468345311281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Primicidiario 

###### **15 Nov 2018** 

Mientras algunos sectores del Congreso exigen la renuncia del Fiscal General, una decisión que podría llevar al uribismo a postular a un nuevo candidato para dirigir este organismo;  en lugar de ejercer una posición crítica para el esclarecimiento del caso, la mayoría de bancadas guarda silencio ante las pruebas que dejan en evidencia el conocimiento de **Néstor Humberto Martínez** sobre las irregularidades en la contratación de la Ruta del Sol II, así lo señala el jurista **Ramiro Bejarano.**

Según el Fiscal, entonces asesor jurídico de **Corficolombiana**, **(socia de Odebrecht en la contratación de la Ruta del Sol II), **no tenía conocimiento del pago de coimas sino que podría tratarse de pagos a paramilitares, una explicación que según el abogado, "no resiste el menor análisis pues significaría que el hecho de pagarle a paramilitares no es un delito y si el de pagar coimas", por lo que el Fiscal debió suministrar la información que poseía en ese momento.

Bejarano aseguro que por primera vez Martínez está acorralado, pues sus explicaciones  y la incoherencia en las fechas que ha indicado el propio Fiscal y el **Grupo Aval, propiedad de Luis Carlos Sarmiento, así como de Corficolombiana,** quien en marzo de 2017 declaró por primera vez que no estaba al tanto de los hechos de corrupción en la contratación.

Sin embargo fue en agosto de 2015 cuando el consultor Jorge Enrique Pizano sostuvo una conversación con Néstor Humberto Martínez advirtiéndole la posibilidad del pago de sobornos al interior de la contratación, fecha para la cual incluso **Marcelo Odebretch,** cabeza de la controvertida constructora  ya había sido imputado por corrupción.

**¿Y si el fiscal renuncia?**

Para Bejarano, las recientes muertes de Jorge Enrique Pizano, las extrañas circunstancias en el posterior fallecimiento de su hijo Alejandro y la información que vincularía al Fiscal general son una muestra de la debilidad del sistema adoptado por el ente investigador al asumir el caso Odebrecht, además carecen  de autonomía al debido proceso.

Bejarano es enfático en que la salida del actual fiscal no contribuiría en nada al caso en particular, sino que, de renunciar, la bancada uribista pediría al presidente Duque que integre una terna nueva para nombrar su remplazo, decisión que “le entregaría la **Fiscalía General de la Nación** a un hombre de Álvaro Uribe”, lo que cambiaría el panorama político del país.

Anticipando el riesgo de dicho escenario, el jurista señala que la opción más viable sería la de dictar un proyecto de ley o apartar al fiscal de los asuntos que tienen que ver con Odebrecht y las investigaciones en el caso de la familia Pizano para que venga otro fiscal independiente que no dependa de Néstor Humberto Martínez y se pueda adelantar la investigación.

La hipótesis que contempla que la muerte de Jorge Enrique Pizano no se debió a causas naturales y la muerte de su hijo al ingerir una botella de agua con cianuro sobre el escritorio del fallecido consultor es una circunstancia que según Bejarano abren la posibilidad de estar “**en la mitad de una gigantesca conspiración criminal que buscaría silenciar al principal testigo de hechos que tienen incomodos a varias personas vinculadas con Odebrecht”.**

El rol del Congreso y los medios de comunicación es fundamental para el avance del caso, sin embargo Ramiro Bejarano señala que muchos de los congresista están intimidados de lo que pueda suceder en las investigación con la Fiscalía y no están interesados en ventilar información que podría perjudicarles mientras que algunos medios han preferido ser partidarios de la labor de Néstor Humberto Martínez.

###### Reciba toda la información de Contagio Radio en [[su correo]
