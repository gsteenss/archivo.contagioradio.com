Title: Expedición BIO vulnera el derecho a la consulta previa de comunidad indígena Inga
Date: 2016-05-26 14:35
Category: Ambiente, Nacional
Tags: Alexander Von Humbolt, Colciencias, comunidades indígenas, Expedición BIO, Putumayo
Slug: expedicion-bio-viola-derecho-a-la-consulta-previa-de-comunidad-indigena-inga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Inga-Putumayo-e1464290974842.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ACIMVIP 

###### [26 May 2016]

Se ha anunciado con gran expectativa que el Instituto Alexander Von Humboldt y Colciencias enviarán cerca de 30 expedicionarios a Altos del Tigre, en el departamento del Putumayo, para adelantar investigaciones científicas en la primera expedición Colombia BIO, sin embargo, comunidades indígenas asentadas en esos territorios como el **pueblo indígena Inga de Villa Garzón aseguran que estas actividades no se las han socializado y además se les está violando el derecho a la consulta  previa.**

Carlos López, gobernador del cabildo mayor de la Asociación de Cabildos Indígenas de Villa Garzón, explica que las comunidades indígenas exigen su derecho a la consulta previa para expresar su contundente rechazo a este tipo de actividades, pues a las comunidades les preocupa que pueda existir un **fin comercial y empresarial,** **buscando obtener información científica para establecer productos cosméticos y farmacéuticos**, a partir de la riqueza natural de esos territorios y afectando la conexión espiritual que los indígenas tienen con su territorio.

**“**Este es un territorio ancestral de uso y conservación del pueblo Inga de Villa Garzón, teniendo en cuenta los recursos y  nuestra tradición, vemos con preocupación que hoy se pretenda adelantar una investigación que amenaza los conocimientos tradicionales con los que cuenta el pueblo indígena y que se han conservado de manera milenaria”, dice el gobernador.

La comunidad indígena del pueblo Inga, que de acuerdo a la Corte Constitucional se encuentra en alto riesgo de desaparecer física y culturalmente, denuncia que **ni Conciencias ni el Instituto Von Humboldt han adelantado algún tipo de acercamiento con la población** y tampoco han sido invitados a algún espacio de socialización.

Además, según López, las autoridades indígenas Inga, “han mandatado que ese territorio debe ser para conservación”, de hecho, en un acta de ampliación de Ordenamiento del Río San Juan de **Corpoamazonía  se estableció que para cualquier proceso que tiene que garantizar el derecho a la consulta previa** y además, se establece que no se puede realizar ningún tipo de estudio o investigación en la zona.

Colombia Bio contempla 20 expediciones en Caquetá, el Urabá, el Pacífico y San Andrés y Providencia, donde se **busca describir 5.000 nuevas especies, mejorar 20 colecciones biológicas y fortalecer la librería genética documentando al menos 40.000 secuencias** para los diferentes grupos taxonómicos en el país. En Putumayo, la expedición iría del 1 al 18 de junio en la zona de Altos del Tigre hasta la Quebrada Salado de los loros.

Las autoridades indígenas aseguran que adelantarán las acciones jurídicas necesarias para defender sus territorios. Exigen que se genere un espacio de diálogo con las autoridades tradicionales del pueblo inga de Villagarzón, con el fin de establecer el mecanismo adecuado que garantice el derecho  a la consulta previa, y hasta que no sea así, **se solicita suspender de manera inmediata la ejecución del proyecto.**

<iframe src="http://co.ivoox.com/es/player_ej_11677793_2_1.html?data=kpajmZybfZShhpywj5aUaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCwaaSnhqeg0sreb46fotjcxc7Fp8qZpJiSpJjSb8XZjKjOxM7QqNDnjK7bxoqnd4a1pczS0MbXb8XZjLuah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
