Title: Boletín Informativo Junio 19
Date: 2015-06-19 16:12
Category: Uncategorized
Tags: Antonio Navarro Wolf, Carta abierta de CONPAZ, Change.org, comision de la verdad, Enciclica Papal Medio ambiente, Papeleta fecha límite al proceso de paz, Partido Verde, Protestas Gobernación de Santander por Hidrosogamoso, Yulin China Festival Come carne de perro
Slug: boletin-informativo-junio-19
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4664707_2_1.html?data=lZujlpyUe46ZmKiakpqJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZadcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El Partido **Alianza Verde**, propuso que para las elecciones de octubre de 2015, se anexe una **papeleta** para que los colombianos decidan si quieren o no **ponerle fecha límite al proceso de paz**, que deacuerdo con la propuesta sería el 9 de abril de 2016. el senador **Antonio Navarro Wolf**, explica la iniciativa. Por otra parte, el **exmagistrado** de la Corte Constitucional, **Alfredo Beltrán** asegura que la propuesta del Partido Verde, **no** está prevista para que "**tenga fuerza vinculante**".

-El próximo 21 de junio, en **Yulin**, China, se celebra el festival “**Come Carne de Perro**”, donde las personas torturan, queman, matan y comen caninos. Desde la plataforma **Change.org** se desarrolla una campaña para recoger firmas con el fin de que **se prohíba esa cruel traidición** con animales. Las personas interesadas en hacer parte de esta iniciativa pueden ingresar a **www.contagioradio.com** y firmar la petición.

-Tras la publicación de la **encíclica del Papa Francisco** sobre el ambiente, **Margarita Flórez**, directora de **Ambiente y Sociedad**, asegura que es importante que el Papa haya retomado la problemática ambiental, con el fin de que los **líderes religiosos conduzcan a sus organizaciones** para que se generen **cambios** en los estilos de vida de las personas, y esto ayude a **En** los impactos en la tierra.

-Tras **4** días de protestas, mujeres y hombres que hacían presencia frente a la Gobernación de Santander en contra la **represa de Hidrosogamoso**, fueron **desalojadas** de manera violenta, recibiendo **agresiones** por parte de la **Policía Nacional**.

-Las Comunidades Construyendo Paz en los Territorios, **CONPAZ**, enviaron una **carta abierta** al presidente Juan Manuel Santos y a los comandantes guerrilleros de las FARC y el ELN, con **aportes para la definición** de los objetivos y metodologías de la **Comisión de la Verdad**, anunciada al finalizar el ciclo 37 de diálogos de paz entre la insurgencia de las FARC y el gobierno nacional. Habla **Janni Silva** integrante del Consejo directivo de **CONPAZ**.
