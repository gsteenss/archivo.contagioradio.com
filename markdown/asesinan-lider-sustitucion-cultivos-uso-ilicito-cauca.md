Title: Asesinan líder de sustitución de cultivos de uso ilícito en Bajo Cauca
Date: 2019-01-03 15:33
Author: AdminContagio
Category: DDHH, Nacional
Tags: Amenaza a líderes sociales, Antioquia, Asesinato a líderes sociales, Bajo Cauca, PNIS
Slug: asesinan-lider-sustitucion-cultivos-uso-ilicito-cauca
Status: published

###### Foto: Marcha Patriotica 

###### 03 Ene 2019 

[Javier Enrique Tapias, facilitador de sustitución de cultivos de uso ilícito en el Bajo Cauca, fue asesinado el 26 de diciembre en Tarazá, Antioquia, crimen que ha **generando alarma sobre la seguridad de promotores de los acuerdos de paz en la subregión**.]

[En la tarde del miércoles, 26 de diciembre, Tapias se trasladaba en motocicleta con Irma Restrepo, de 36 años, en el corregimiento de El Doce, cuando los dos fueron ultimados con arma de fuego por hombres desconocidos que se transportaban en motocicleta.]

[Tapias, de 56 años, era integrante de la Junta de Acción Comunal de la vereda Ocabajo y de la Asociación de Campesinos del Bajo Cauca (Asocbac), además hacía **parte del Plan Nacional Integral de Sustitución (PNIS) en el Bajo Cauca**. ([Le puede interesar: Asesinan a Norberto Jamarillo en Tarazá](https://archivo.contagioradio.com/asesinan-norberto-jaramillo-taraza/))]

### Riesgo para líderes sociales en el Bajo Cauca 

Durante el 2018 organizaciones sociales denunciaron el aumento de presencia de grupos armados en esta región del Bajo Cauca, entre ellos; las autodenominadas Autodefensas Gaitanista de Colombia (AGC) y los Caparrapos, grupos que se enfretan por el control del territorio en zonas cocaleras.

Entre enero 1 y noviembre 17 del 2018, fueron asesinados 33 líderes sociales en Antioquia, 14 de los cuales eran habitantes del Bajo Cauca, según el [último informe](http://www.indepaz.org.co/wp-content/uploads/2018/11/Separata-de-actualizaci%C3%B3n-de-INFORME-ESPECIAL-Todos-los-nombres-todos-los-rostros.-19-de-noviembre-2018-2.pdf) del Instituto de estudios sobre paz y desarrollo (Indepaz).

###### Reciba toda la información de Contagio Radio en [[su correo]
