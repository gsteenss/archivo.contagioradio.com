Title: Mecanismo de Monitoreo y Verificación desmintió al gobernador de Antioquia
Date: 2016-12-30 13:41
Category: Nacional, Paz
Tags: Antioquia, FARC, Mecanismo Tripartito, ONU
Slug: mecanismo-de-monitoreo-y-verificacion-desmintio-al-gobernador-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/mecanismo-de-verificacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: noticiasuno] 

###### [30 Dic 2016]

El informe del Mecanismo Tripartiro de Monitoreo y Verificación comprobó que en el Punto de Preagrupamiento Temporal de Yondó, en Antioquia, **no hubo ejercicios de reclutamiento de menores, o de prostitución a los cuales se había referido Luis Perez**, gobernador de Antioquia. Según el mecanismo se presentó una salida sin permiso por parte de uno de los integrantes de las FARC ubicados en dicho sitio.

Según el comunicado del Mecanismo, desde el momento en que se recibieron las denuncias del caso, la oficina regional de Medellín asumió la investigación y sostuvo reuniones con autoridades de la zona, civiles e integrantes de las FARC en las que se permitió establecer lo sucedido y se llegó a la conclusión de que h**ubo un hecho violatorio del reglamento pero no en los términos afirmados por el gobernador de ese departamento**. ([Le puede interesar: Primer informe de verificación del Mecanismo Tripartito](https://archivo.contagioradio.com/mecanismo-tripartito-entrega-informe-sobre-cese-al-fuego/))

### **Las conclusiones del Mecanismo** 

En el punto dos del comunicado, el mecanismo aclara que se pudo establecer que un integrante de las FARC salió de la Zona y tuvo una discusión con una joven “*La verificación determinó que un integrante de las FARC-EP sin uniforme y desarmado, salió del PPT “San Francisco” sin conocimiento del responsable del mismo, y tuvo una discusión verbal hostil con una joven”.*

Por otra parte también se indago sobre el supuesto ejercicio de la prostitución y se concluyó que no existe indicio alguno sobre un hecho como este u otro violatorio de los derechos fundamentales de la población civil “*no existe ningún **indicio de hechos de esa naturaleza, ni se evidencian actos que constituyan violaciones a los derechos fundamentales de la población civil**, conforme a informaciones recolectadas” (*[lea también: Así arranca operación del Mecanismo)](https://archivo.contagioradio.com/arranca-la-verificacion-tripartita-al-cese-bilateral-de-fuego-en-colombia/)

En ese mismo sentido se pronunciaron las FARC e hicieron un llamado a que este tipo de declaraciones no se repitan para favorecer un ambiente de construcción de paz, que es lo que se necesita en este momento.

Este es el comunicado completo...

##### *Bogotá,  30 de diciembre de 2016*

##### *Con relación a las informaciones referidas a los municipios de Yondó y Dabeiba, en el departamento de Antioquia, respecto al supuesto comportamiento de algunos integrantes de las FARC-EP, en los Puntos de Pre agrupamiento Temporal- PPT, y en zonas  aledañas, el Mecanismo de Monitoreo y Verificación  (MM&V) se permite informar lo siguiente:*

##### ***1.****Desde el primer momento en que se tuvo conocimiento del caso de la vereda San Francisco del municipio de Yondó y se recibió la información ante la Sede Regional del MM&V en Medellín, el Mecanismo inició su tarea y se desplazó al terreno, en donde sostuvieron reuniones con las autoridades civiles, las comunidades de la zona e integrantes de las FARC-EP para verificar los presuntos hechos.*

##### ***2.****La verificación determinó que un integrante de las FARC-EP sin uniforme y desarmado, salió del PPT “San Francisco” sin conocimiento del responsable del mismo, y tuvo una discusión verbal hostil con una joven, lo que constituyó una transgresión de carácter “relevante” al protocolo, por haber sido cometido “individualmente por un subordinado en forma excepcional y sin consentimiento de un superior”. Se recomienda a las FARC-EP ejercer el debido control sobre sus estructuras acorde a lo que establece el Protocolo y en especial en lo referido a la presencia de sus miembros en zonas pobladas sin previa coordinación con el MM&V.*

##### ***3.****Con relación al eventual ingreso o salida de personas ajenas al PPT “San Francisco”, y al ejercicio de la prostitución en el mismo se estableció que no existe ningún indicio de hechos de esa naturaleza, ni se evidencian actos que constituyan violaciones a los derechos fundamentales de la población civil, conforme a informaciones recolectadas de las autoridades locales, de la población del lugar, y de las FARC-EP.*

##### ***4.****Una vez se reciba la información por parte de la Sede Regional Medellín, sobre la verificación de los supuestos hechos ocurridos en el municipio de Dabeiba, esta será analizada y la Conducción Nacional del Mecanismo se pronunciará al respecto.*

##### *Por otro lado la Conducción Nacional informa a la opinión pública que a partir del mediodía de hoy se encontrará disponible en la página web de la Misión de la ONU en Colombia, el Primer Informe Mensual del Mecanismo de Monitoreo y Verificación, dando cumplimiento a los establecido en el Protocolo CFHBD y DA.*

##### *Finalmente, la Conducción Nacional del Mecanismo de Monitoreo y Verificación en nombre de todos sus integrantes, les desea un muy feliz año nuevo a todos las colombianas y colombianos.* 
