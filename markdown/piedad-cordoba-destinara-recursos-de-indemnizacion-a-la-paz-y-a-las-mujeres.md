Title: Piedad Córdoba destinará recursos de indemnización a la paz y a las mujeres
Date: 2016-10-12 15:37
Category: Uncategorized
Slug: piedad-cordoba-destinara-recursos-de-indemnizacion-a-la-paz-y-a-las-mujeres
Status: published

###### [Foto: ]

###### [12 Oct 2016]

La congresista que fue favorecida por la decisión del Consejo de Estado que levantó la inhabilidad en su contra aseguró que los recursos que recibirá por concepto de indemnización como contraprestación por los daños causados los destinará a aportar en la **construcción de la paz y a fortalecer las luchas de las mujeres por el respeto a sus derechos**. Además manifestó que Ordóñez debería ser sancionado por los daños que ha provocado a muchas personas.

En una rueda de prensa ofrecida en Bogotá la ex congresista aseguró que es necesario que se recupere la ética en la política y que la justicia ha sido una de las instituciones mas golpeadas en nuestro país y por ello será necesario trabajar para que se recuperen los valores en el poder legislativo y en el poder judicial. Agregó que la decisión del **Consejo de Estado devuelve la esperanza en esa institución.**

Córdoba que fue restituida en sus derechos políticos también en esta segunda sanción asegura que trabajará por estar en una Asamblea Nacional **Constituyente que, según ella, debería darse en 2018 “Para poder organizarnos y poderle ganar a los del no**”, iniciativa que irá de la mano con los avances en materia de estatuto de la oposición y reforma política y electoral que deberán tramitarse en el marco de los acuerdos de paz logrados en la Habana por el gobierno nacional y las FARC.

La líder política y defensora de Derechos Humanos, agregó que hay una confluencia muy fuerte de personas que pretende formar una **gran coalición en la que podría estar el movimiento político que resulte luego de la implementación de los acuerdos con las FARC**, sin embargo aclaró que más importante que los candidatos o candidatas será la definición de programas políticos para proponerle al país y a sectores que se manifiestan en iniciativas como Paz a la Calle.

Por último la ex congresista aseguró que el ex procurador Alejandro Ordoñez debería ser sancionado y las funciones concedidas a ese ente deberían ser debatidas y revisadas por el congreso de la república para que no se repitan los excesos y las arbitrariedades cometidas en la anterior administración. En ese sentido Córdoba aseguró que **revisarán las posibilidades de una demanda contra el ex procurador.**

Por su parte Alfredo Beltrán, uno de los abogados de la ex congresista aseguró que en el caso de que se defina una indemnización pecuniaria a favor de una persona afectada, el responsable de la sanción que vulneró los derechos de la víctima debería tener parte en la indemnización que definan los tribunales.

<iframe id="audio_13290465" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13290465_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
