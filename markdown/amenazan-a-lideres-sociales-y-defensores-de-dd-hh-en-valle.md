Title: Amenazan a líderes sociales y defensores de DD.HH en Valle
Date: 2017-07-18 15:14
Category: DDHH, Nacional
Tags: Amenazas, Movimiento social
Slug: amenazan-a-lideres-sociales-y-defensores-de-dd-hh-en-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [18 Jul 2017] 

Integrantes del Comité de Solidariad con Presos Políticos en Valle, líderes sindicales y organizaciones movimiento social, fueron a amenazados a través de un mensaje de texto en el que se les manifiesta que los tienen ubicados y **que serán “dados de baja” y se les acusa de pertenecer a las guerrillas de las FARC-EP y el ELN**.

La amenaza llegó sobre las 2:45 pm al celular del coordinador de la seccional del Valle del Comité Solidariad con Presos Políticos, Walter Agredo.“**Nuestras estructuras militares han interceptado sus mensajes de guerrilleros de las FARC y el ELN camuflados de líderes sindicales** y sociales en los que imparten su pensamiento castro chavista, confunden a trabajadores y comunidades con frases comunistoides”, esta es una de las amenazas que llegó a líderes y defensores de derechos humanos.

De igual forma, a través de un comunicado de prensa el Comité de Solidariad con Presos Políticos señaló que esta amenaza hace parte de la estigmatización a la labor que realizan los defensores de derechos humanos en el país y agregó que en la intimidación se evidencia que “e**sta nueva acción obedece a una estrategia nacional, de amedrentar a través de las amenazas**, pero también de asesinar a los líderes del movimiento social en Colombia". (Le puede interesar: ["Asesinan a Ezequiel Rangel líder de ASCAMCAT en Norte de Santander"](https://archivo.contagioradio.com/asesinan-a-ezequiel-rangel-lider-de-ascamcat-en-norte-de-santander/))

Dentro de la lista de personas amenazadas se encuentra Francia Márquez, líder de procesos afros en el Valle del Cauca, Hernán Arciniegas, Yelby Ramírez, Diego Escobar, Otoniel Ramírez, Enrique Guetio, Meraldino Cabiche, Adelina Vázquez, Licifreddi Ararat, las plataformas políticas Marcha Patriótica, Congreso de Pueblos, la Central Unitaria de Trabajadores, la ACIN, ASONAL, SINTRAMUNICIPIO Yumbo, SINALTRAINAL, SINALTRACAMPO, SUTIMAC y ACIN Serro tijeras.

Sin embargo, de acuerdo con Camilo González Posso, director de Indepaz, esta estrategia y política del miedo que se ha venido desarrollando, **se ha encontrado con esfuerzos muy grandes por parte del movimiento social que buscan que los impulsos a la paz sean mucho más contundentes**, como lo son la creación de las guardias campesinas, cimarronas e indígenas para presionar y visibilizar la situación que se vive en las comunidades a las ves que le exigen al Estado mecanismos de respuesta eficaces para detener la violencia. (Le puede interesar: ["Política del miedo del paramilitarismo debe ser afrontada con movilización social"](https://archivo.contagioradio.com/paramilitarismo-es-una-politica-del-miedo-impuesto/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
