Title: Secretarías de Educación ya habían advertido grave situación de a educación en Colombia
Date: 2017-05-29 19:05
Category: Educación, Nacional
Tags: eduación, paro educadores
Slug: secretarias_educacion_paro_educadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/fecode1-e1496014686486.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Daniel Reina 

###### [28 May 2016] 

El paro indefinido de maestros ya cumple dos semanas, y aún parece no haber soluciones a la situación que viven los docentes pero también los alumnos. Unas condiciones sobre las cuales ya se habían manifestado **nueve secretarías de educación del país, más sin embargo el gobierno no atendió dichas preocupaciones.**

Se trata de una carta dirigida al presidente Juan Manuel Santos,  con fecha del 8 de mayo, sobre la cual, las secretarias de educación **solicitaban al gobierno atención en distintos aspectos, alegando que ni los niños y niñas estaban recibiendo clase en condiciones óptimas,** debido a los constantes incumplimientos del Ministerio de Educación.

Las y los secretarios  de nueve departamentos y municipios, pedían acciones concretas frente a diferentes deficiencias del sector educativo.  Entre ellas se menciona la falta de recursos para cubrir la planta educativa de las instituciones, los incumplimientos de la implementación del Banco de la Excelencia, la falta de infraestructura,y la ausencia de condiciones para establecer la jornada única.

Además, el documento también habla de las deudas la laborales, la falta de planeación presupuestal, las deficiencias en el transporte escolar y los refrigerios a los estudiantes.

La carta dirigida también al ministerio de educación, de hacienda y al presidente del Congreso, fue firmada por las secretarías de **Antioquia, Huila, Neiva, Zipaquirá, Cundinamarca, Tunja, Pasto, Jamundí, y Santa Marta**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
