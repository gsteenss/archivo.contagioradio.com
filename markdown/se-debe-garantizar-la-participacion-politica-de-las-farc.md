Title: "Se debe garantizar la participación política de las FARC"
Date: 2015-11-09 18:06
Category: Nacional, Política
Tags: FARC, Iván Cepeda, jurisdicción especial para la paz, La Habana, Polo Democrático, proceso de paz
Slug: se-debe-garantizar-la-participacion-politica-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/2X4MQMGdiXBKIAN1VeyBd1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [noticias.lainformacion.com]

###### [9 Nov 2015]

La guerrilla de las FARC publicó un comunicado titulado: “Diez propuestas mínimas para garantizar el fin del conflicto, la reconciliación nacional y la construcción de la paz estable y duradera”, entre ellas reiteran nuevamente su intensión de iniciar la vida política, y la importancia de que se de garantías a los guerrilleros para volver a la vida civil.

Según el senador Iván Cepeda, **“se debe garantizar la posibilidad de que todas las guerrillas participen en política, de eso se trata el proceso de paz”**, y agrega que es necesario ofrecer las condiciones para que eso se dé.

En ese mismo sentido, el senador propone que se abra la discusión frente a cómo se va a garantizar la participación política de las FARC que deben empezar a ejercer desde el momento en que firmen el proceso de paz y dejen las armas, teniendo en cuenta que el próximo año, que es cuando se tiene previsto que se logre la paz, no habrá elecciones.

**COMUNICADO**

El “Fin del conflicto” implica que se dé el paso hacia cambios estructurales que normalicen la vida nacional, e incluye la decisión política de las FARC-EP de emprender un proceso de transformación integral colectivo e individual, que permita el tránsito a la vida civil, y propósitos esenciales como:

1.  En primer lugar, la participación activa en la política abierta a través del movimiento político que se conformará para tal efecto para promover la democracia verdadera, directa, comunitaria y autogestionaria, con plenos derechos para todos y cada uno de sus integrantes, incluida la asignación directa de curules en el Congreso de la República durante al menos dos períodos, así como en Asambleas Departamentales y Concejos Municipales en lugares de comprobada presencia e influencia por iguales períodos.
2.  En segundo lugar, desarrollar economías del común, de carácter asociativo y comunitario, en los diferentes campos del proceso económico y articuladas entre sí con el objetivo de contribuir a la reconstrucción de la base productiva del país y al mejoramiento de las condiciones de vida y de trabajo y al buen vivir de los pobres y desposeídos.
3.   En tercer lugar, promover y apoyar procesos organizativos sociales y con fundamento en el reconocimiento pleno de los derechos económicos y sociales, especialmente al trabajo digno, la salud y la seguridad social, la vivienda, de todos y cada uno de sus integrantes. Es condición necesaria del proceso de normalización de la vida nacional y transformación de las FARC-EP en organización abierta, una solución confiable en lo que concierne al tema de “Justicia especial para la paz”, que atendiendo los derechos de las víctimas del conflicto, sea consecuente con el reconocimiento de la rebelión y sus conexidades en el sentido más amplio, se fundamente en los principios de la justicia restaurativa, prospectiva y transformadora, y se haga extensiva a las prisioneras y prisioneros políticos y de guerra condenados o con procesos en trámite. Así mismo, que garantice la protección constitucional frente a la extradición de cualquier integrante de la organización. Lo acordado para la reincorporación de las FARC-EP en lo político, lo económico y lo social.

