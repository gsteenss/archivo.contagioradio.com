Title: Chile dice sí a la unión civil entre parejas del mismo sexo
Date: 2015-01-30 22:17
Author: CtgAdm
Category: El mundo, LGBTI
Tags: Chile, LGBTI, matrimonio igualitario
Slug: chile-dice-si-a-la-union-civil-entre-parejas-del-mismo-sexo
Status: published

##### [Foto: www.sdpnoticias.com]

Chile da un paso hacia el respeto y la igualdad al hablar de la **unión civil para parejas del mismo sexo y aprobar el proyecto de ley** que hace algunos años se había propuesto, esto, según diversas organizaciones, mejora las condiciones jurídicas y sociales tanto de parejas homosexuales como heterosexuales.

En principio el proyecto se llamó **Acuerdo de Vida en Pareja** y después de once años de incluir la primera ley de uniones civiles en el senado, se aprueba el **Acuerdo de Unión Civil**, con 25 votos a favor, 6 en contra y 3 abstenciones.

De esta manera se dá vía libre a las parejas del mismo sexo para convivir sin la necesidad de tener un vínculo matrimonial, dejar de lado los **estigmas del significado de vivir en unión libre** pero sobretodo para incluir varios modelos de familia en su país.

**El 28 de enero de 2015 es una fecha histórica para los chilenos**, pues con este avance se demuestra que están en busca de proteger a todo tipo de parejas con la ley y están comprometidos con el respeto hacia los derechos humanos.

 
