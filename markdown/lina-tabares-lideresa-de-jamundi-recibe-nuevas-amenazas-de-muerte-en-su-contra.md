Title: Lina Tabares lideresa de Jamundí, recibe nuevas amenazas de muerte en su contra
Date: 2019-12-27 15:45
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Cali, Jamundi, Lider social, Lina Tabares, violencia
Slug: lina-tabares-lideresa-de-jamundi-recibe-nuevas-amenazas-de-muerte-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-Pantalla-2019-12-27-a-las-2.46.01-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@LinaMangaita] 

Este 26 de diciembre Lina Tabares líder social en Jamundí (Valle del Cauca), denunció una nueva amenza de muerte en su contra. Tabares durante este año ha reportado más de 3 llamadas telefónicas donde números desconocidos la contactan y amenazas a ella y su familia, esta vez el mensaje no duró más de 3 segundos:*"Contá las horas que te vamos a matar".*

La líder ha sobrevivido a dos atentados en su contra, y según denuncias anteriores se ha vuelto objeto de intimidaciones por ser acompañante en procesos de restitución de tierra, y ser testigo clave en un caso en contra de un grupo de 34 militares del batallón Pichincha envueltos en un procesos de ejecuciones extrajudiciales.

Muñecas bañadas en sangre, sufragios, panfletos, mensajes de WhatsApp, hombres rondando su casa y en el colegio de su hijo menor, cuentan dentro de las amenazas que han llegado a su casa para intimidarla y alejarla de su oficio como líder social. (Le puede interesar:[Indígenas del Norte del Cauca se unen a movilización nacional](https://archivo.contagioradio.com/indigenas-del-norte-del-cauca-se-unen-a-movilizacion-nacional/))

### ¿Por qué la vida de Lina Tabares ha corrido riesgo durante este 2019?

La lideresa nació en Cali, Valle del Cauca, y ha vivido durante 21 años en Victoria al norte del departamento; su esposo murió hace 11 años en un accidente aéreo, del matrimonio tiene dos hijos menores de edad que cuentan solo con ella. (Le puede interesar: [Asesinan a excombatiente de Farc, Carlos Alberto Montaño en Cali](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali/))

Desde hace ocho años tomó la decisión de ser defensora de Derechos Humanos, y de los territorios; en septiembre de 2019 se postuló como candidata al Concejo de Jamundí por la coalisión Verde-Mira, a pesar de no ganas sus ideales y metas en pro de su comunidad no pararon.

Hace algunos meses declaró ante un medio de comunicación nacional que su vida corría riesgo, la razón, su labor en la defensa de los derechos, al igual que el acompañamiento que a hecho a casos de restitución de tierras, denuncias a crímenes ambientales de minería ilegal e legal de oro, y las acusaciones en contra de varios funcionarios de la administración municipal de Jamundí que se han visto involucrados en procesos irregulares.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
