Title: Las mujeres ecuatorianas y el derecho a decidir sobre su sexualidad
Date: 2017-03-14 13:10
Author: AdminContagio
Category: 24 Cuadros
Tags: Derechos, Documental, ecuador, genero, mujer
Slug: mujeresviolenciaabortoecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/las-mujeres-deciden.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Las mujeres deciden 

###### 14 Mar 2016 

**“Las mujeres deciden”** es un documental de la directora e investigadora española Xiana Yago, que aborda el tema de la **violencia sexual contra las mujeres, el embarazo adolescente y el aborto clandestino en Ecuador**. Una producción que pretende crear conciencia, fomentar el debate y la movilización social ante una problemática que trasciende las fronteras de Latinoamérica.

Con más de un año de investigación, **realizado en las ciudades de Quito, Esmeraldas y en El Coca** con entrevistas y testimonios reales, el documental pone en evidencia la difícil situación de tres mujeres que han sido víctimas de violencia sexual o se han realizado abortos clandestinos, poniendo en riesgo su vida y su integridad física y su condición psicosocial.

Yago, quien llegó al país vecino en 2007 para realizar unas prácticas académicas como médica en un hospital de Quito, se interesó en la temática al conocer de primera mano varios casos de mujeres que llegaban en medio de abortos en curso, o víctimas de acceso carnal violento. Una realidad bastante dura pero común en Ecuador, país en el que se estima **hay más de 3.600 niñas menores de 15 años que son madres producto de una violación**.

En conversación con Contagio Radio, la directora señala que su intención con el documental es “**mostrar la situación, acercarnos a estas mujeres e identificarnos con ellas, entender lo que les está pasando, y que la sociedad civil empiece a cuestionarse soluciones para estas problemáticas**”, teniendo en cuenta que el aborto es la quinta causa de muerte materna en Ecuador según datos del Instituto Nacional de Estadísticas y Censos (INEC).

<iframe src="https://www.youtube.com/embed/Vmx8bx45ZNI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**La legislación al respecto sigue siendo retrograda y revictimizante**, situación en el que Xiana piensa el documental pueda aportar: “Yo quiero que a nivel social tenga lugar este debate para que la gente se mueva y demande un cambio”. Le puede interesar: [Los retos de 2017 para enfrentar la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/).

Como parte de la intención inicial del proyecto, después de su estreno nacional el próximo mes de mayo, la directora pretende provocar un circuito de exhibición alternativo, para que la película pueda llegar a todo tipo de lugares como: colegios, provincias, auditorios públicos, pues “la idea principal es que la película sea vea” manifestó la directora Española, extendiendo la invitación para personas que estén interesadas en hacer la proyección en su ciudad, en su barrio o espacio cultural, puedan hacerlo a través de la página del documental.

<iframe id="audio_17537349" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17537349_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
