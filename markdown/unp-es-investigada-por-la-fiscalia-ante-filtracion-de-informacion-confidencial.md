Title: UNP es investigada por La Fiscalía ante filtración de información confidencial
Date: 2019-09-23 16:31
Author: CtgAdm
Category: DDHH, Judicial
Tags: CTI, Filtraciones, investigaciones, Unidad Nacional de protección, UNP
Slug: unp-es-investigada-por-la-fiscalia-ante-filtracion-de-informacion-confidencial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-28-at-4.49.53-PM-e1548712266586-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

 

El domingo 22 de septiembre desde las 5 de la tarde, integrantes del Cuerpo Técnico de Investigación (CTI) de la Fiscalía citaron a funcionarios de la Unidad Nacional de Protección (UNP), para indagar sobre las posibles filtraciones en la base de datos de la entidad, denunciado por Pablo Elías González, director de esta entidad. Durante dicha reunión,  se llevaron a cabo declaraciones juramentadas hasta altas horas de la madrugada y la incautación de varios discos duros. (Le puede interesar: [Por malas condiciones laborales escoltas de la UNP cesarían actividades](https://archivo.contagioradio.com/escoltas-unp/))

Según **Wilson Devia integrante del Sindicato de la UNP**, está bien que La Fiscalía haga investigaciones y verificaciones sobre la filtración de información, sin embargo, lo que generó mayor malestar en los funcionarios fue la citación bajo “engaños” y a destiempo, “estas declaraciones juramentadas se pueden hacer cualquier día en la semana, y no un domingo cuando todos descansan y menos con engaños, diciendo que el director de la unidad  los necesita, y al llegar se tienen que enfrentar a dar declaraciones y entregar archivos”.

> *“desde el sindicato nos preocupa que se esté filtrado información, porque es algo que afecta no solo a los protegidos, sino a todos, no es posible que una organización de estas presente este tipo de falencias”. Afirmó Devia*

Al mismo tiempo el funcionario reconoce la posibilidad de la efectiva existencia de estas filtraciones, “como sindicato no descartamos que esta acción se diera, de hecho es muy probable. Muchos funcionarios acá entran a dedo, y  la información se maneja desde Excel, así que puede ser manipulada por cualquier contratista, y asimismo puede ser extraída de manera libre”. (Le puede interesar: [Unidad Nacional de Protección adjudicó contratos a empresas endeudadas con sus trabajadores](https://archivo.contagioradio.com/unidad-nacional-de-proteccion-adjudico-contratos-a-empresas-endeudadas-con-sus-trabajadores/))

### **¿Qué está pasando con el Director de la UNP?**

Algunos integrantes de la UNP han presentado posturas de indignación frente al director de Unidad **Pablo Elías González,** quien afirmó por medio de un comunicado en la página web y a través de un medio de comunicación, que algunos de los integrantes de grupos de seguridad estarían aliados con grupos terroristas. (Le puede interesar: [Por malas condiciones laborales escoltas de la UNP cesarían actividades](https://archivo.contagioradio.com/escoltas-unp/))

Frente a esto, Devia dijo que esas afirmaciones incomodaron a funcionarios y al mismo sindicato, "esta es una acusación gravísima, porque nos podemos volver blanco de grupos disidentes por pensar que estamos aliado con terroristas”, ante ello el sindicato empezará investigaciones que permitan verificar y desmentir estas acusaciones.

<iframe id="audio_41959625" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41959625_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
