Title: Activista italiano en coma tras ataque fascista contra centro social
Date: 2015-01-21 20:13
Author: CtgAdm
Category: DDHH, El mundo
Tags: Ataque fascista contra centro social italiano, Casa Pound, Cremona, CSA Dordoni, Emilio resiste
Slug: activista-italiano-en-coma-tras-ataque-fascista-contra-centro-social
Status: published

###### Foto:animalpolitico 

Emilio, de 49 años, activista del centro social fue golpeado en la cabeza y más tarde reprendido a puñetazos en el suelo, siendo este hospitalizado, para más tarde entrar en coma hasta el día de hoy.

El martes 20 de Enero fue atacado por grupos fascistas pertenecientes al movimiento italiano de ultraderecha Casa Pound, el CSA Dordoni (centro social autogestionado), en la localidad de Cremona, al sur de la región de Lombardia en Italia.

El movimiento "Casa Pound" perteneciente a la ultra derecha se crea en Italia en 1987 con el objetivo de dar casa a ciudadanos italianos sin techo. Sus principales acciones son la ocupación de edificios abandonados y la creación de centros sociales fascistas y ataques racistas contra inmigrantes e integrantes de movimientos autónomos de izquierda.

Ante lo sucedido se ha convocado una jornada de movilizaciones para el lunes 26 en todo el país, para rechazar el ataque y el auge del fascismo.
