Title: La fuerza de la Minga se une al Paro Nacional
Date: 2019-04-25 15:55
Author: CtgAdm
Category: Movilización, Nacional
Tags: Bogotá, Cauca, CRIC, Minga Nacional, ONIC, Paro Nacional
Slug: la-fuerza-de-la-minga-se-une-al-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-25-at-10.58.16-AM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Catatumbo-Paro-Nacional.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Pueblo-Pijao-Paro.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Minga-Cauca-Paro.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Después de casi un mes de movilizaciones en que la **Minga del Suroccidente del país intentó llegar a un acuerdo con el Gobierno que fuera incluyente en el Plan Nacional de Desarrollo (PND)**, las comunidades indígenas del país se han unido al Paro Nacional exigiendo, además de las modificaciones al PND, medidas inmediatas para poner fin al paramilitarismo, realizar ajustes a las políticas de seguridad en los territorios, el desmonte del ESMAD y el respeto y derecho a la vida.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw)| Los Pueblos Indígenas vamos [\#DeLaMingaAlParoNacional](https://twitter.com/hashtag/DeLaMingaAlParoNacional?src=hash&ref_src=twsrc%5Etfw). ¡Este [\#ParoNacional](https://twitter.com/hashtag/ParoNacional?src=hash&ref_src=twsrc%5Etfw) es por la paz, la vida, nuestros territorios! [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@FelicianoValen](https://twitter.com/FelicianoValen?ref_src=twsrc%5Etfw) [@MPCindigena](https://twitter.com/MPCindigena?ref_src=twsrc%5Etfw) [@CNTI\_Indigena](https://twitter.com/CNTI_Indigena?ref_src=twsrc%5Etfw) [@concipmpc](https://twitter.com/concipmpc?ref_src=twsrc%5Etfw) [@AcivarpV](https://twitter.com/AcivarpV?ref_src=twsrc%5Etfw) [@OIA\_COLOMBIA](https://twitter.com/OIA_COLOMBIA?ref_src=twsrc%5Etfw) [@CumbreAgrariaOf](https://twitter.com/CumbreAgrariaOf?ref_src=twsrc%5Etfw) [@C\_Pueblos](https://twitter.com/C_Pueblos?ref_src=twsrc%5Etfw). [pic.twitter.com/ybmDvG2RfE](https://t.co/ybmDvG2RfE)
>
> — Organización Nacional Indígena de Colombia - ONIC (@ONIC\_Colombia) [25 de abril de 2019](https://twitter.com/ONIC_Colombia/status/1121434872940257281?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **En el territorio nacional**

A la movilización se han sumado diversas comunidades en todo el país que marchan por la defensa de los territorios destinados a la producción agropecuaria y reivindicando la exigencia de una reforma agraria y un nuevo ordenamiento territorial que dé prioridad a la protección del ambiente, los ecosistemas y del agua como un derecho humano fundamental, de igual forma en defensa de la consulta previa vinculante para los proyectos mineros-energéticos. [(Lea también: Duque tendrá que hablar con la Minga Nacional del 25 de abril: CRIC) ](https://archivo.contagioradio.com/duque-tendra-que-hablar-con-la-minga-nacional-del-25-de-abril-cric/)

\[caption id="attachment\_65624" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Catatumbo-Paro-Nacional-1024x498.jpeg){.wp-image-65624 .size-large width="1024" height="498"} Foto: Archivo\[/caption\]

En **Buenaventura,** indígenas de ACIVA RP se concentraron en el Puerto Cena, mientras en Antioquia, provenientes del municipio de Valparaiso, el resguardo indígena Embera Chamí respalda el Paro Nacional y en Cundinamarca el Cabildo Muisca de Suba se ha concentrado en la Plaza Fundacional.

Por su parte en El Catatumbo, en la planta extractora de Río Flores, cerca a  **Tibú,** diversos procesos sociales como el Pueblo Barí que junto a otros conforman la Comisión por la Vida, la Reconciliación y la Paz del Catatumbo también han salido a movilizarse.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Buenaventura-1024x768.jpg){.aligncenter .wp-image-65559 .size-large width="1024" height="768"}

**Según Rubiel Liss del equipo de comunicaciones del CRIC** las movilizaciones en Cauca, acompañadas por la Minga del Sur Occidente la jornada ha avanzado con tranquilidad reuniéndose cerca de 3.000 indígenas en Popayán y otros 2.000 que se unieron al paro desde la ciudad de Cali, "la Minga del suroccidente ha dicho que continúa y en este momento nos encontramos en la disposición de reunirnos con el presidente en el mes de mayo, seguimos en Asamblea Permanente", afirma.

<iframe src="https://co.ivoox.com/es/player_ek_34997648_2_1.html?data=lJmmm5yaeJmhhpywj5aVaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbPpw87SzpCwrdTnjMnSzpDJtdbd0dSYxsqPp9Dh1tPWxcbHrdDixtiYxsrQb6TGqqiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcric.colombia%2Fvideos%2F2314430975549656%2F&amp;show_text=0&amp;width=560" width="560" height="308" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En departamentos como **Chocó**, desde el 24 de abril indígenas  Embera  se concentraron en tres puntos cercanos a Tadó en cercanías a la vía que de Quibdó conduce a Pereira – Risaralda, manifestándose para que se cumplan los Acuerdos de Paz, finalmente en Caldas el Pueblo Embera Chamí de Caldas se  moviliza en **Riosucio y Manizales** mientras en otros departamentos como **La Guajira**, la comunidad Wayúu se han unido para manifestar en Riohacha.

\[caption id="attachment\_65626" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Pueblo-Pijao-Paro-1024x768.jpg){.wp-image-65626 .size-large width="1024" height="768"} Foto: Onic\[/caption\]

[English Version](https://archivo.contagioradio.com/the-minga-joined-actively-the-national-strike/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
