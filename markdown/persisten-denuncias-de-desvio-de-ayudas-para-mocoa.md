Title: Persisten denuncias de desvío de ayudas para Mocoa
Date: 2017-04-10 13:00
Category: DDHH, Nacional
Tags: Ayudas para Mocoa, Mocoa
Slug: persisten-denuncias-de-desvio-de-ayudas-para-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [10 Abr 2017] 

Personas inescrupulosas se estarían quedando con las ayudas que envían los colombianos a la población de Mococa, afectada por la avalancha que se presentó el pasado primero de abril y que hasta el momento deja un saldo **de 315 personas muertas, 103 personas desaparecidas y continúa realizándose el censo de damnificados.**

Carlos Fernández, defensor de derechos humanos e integrante de la Comisión de Justicia y Paz, señala que han recibido **denuncias por parte de los damnificados que afirman que las ayudas que llegan para Mocoa se están trasladando a Puerto Asís, en una camioneta**, y que el sistema de centralización de la Unidad de Riesgo demora más el tiempo de llegada de las donaciones.

En los últimos días se presentó un intento de ingreso de cargamento de marihuana en uno de los camiones que transportaba ayuda para Mocoa, sin embargo, las autoridades se percataron de este hecho e iniciaron la respectiva investigación, esto genero una reacción por parte de la Fuerza Pública que **ahora deben conducir todos los camiones al centro de acopio que ha dispuesto la Unidad de Riesgo, e incrementando los tiempos para repartir las ayudas.**

Por otro lado, organizaciones de derechos humanos han solicitado a la institucionalidad que los tiempos del censo sean mucho más largos, **debido a que muchas de las personas han salido desplazadas a otras regiones o continúan en la búsqueda de sus familiares** desaparecidos y no se encuentran registradas. Le puede interesar: ["Habitantes de Mocoa denuncian que las ayudas no estarían llegando a personas fuera de los albergues"](https://archivo.contagioradio.com/habitantes-de-mocoa-denuncian-que-ayudas-no-estarian-llegando-a-personas-fuera-de-los-albergues/)

Fernández afirmó que hasta el momento el único censo que se tiene es el de las afectaciones a las comunidades indígenas que dio como resultado **859 familias afectadas para un total de 2879 personas, hay 10 pueblos indígenas damnificados de los 15 que habitan en el Putumayo**, no obstante, estas personas no han sido censadas aún por el gobierno. Le puede interesar:["Pueblo Nasa denuncia que no esta recibiendo ayudas del Gobierno en Mocoa"](https://archivo.contagioradio.com/pueblo-nasa-denuncia-que-no-esta-recibiendo-ayudas-del-gobierno/)

Frente a los menores de edad, en este momento hay **40 niños y niñas que se encuentran en el Hospital José María Hernández y en el Instituto Tecnológico de Putumayo**, están plenamente identificados y las autoridades correspondientes están en la búsqueda de sus núcleos familiares, comunidades o barrios a los que pertenecían para ser devueltos a sus hogares.

<iframe id="audio_18067697" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18067697_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
