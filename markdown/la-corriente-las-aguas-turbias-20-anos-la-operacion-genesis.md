Title: “Contra la Corriente de las Aguas Turbias, a 20 años de la operación Génesis”
Date: 2017-03-10 17:08
Category: DDHH, Viaje Literario
Tags: Operacion genesis
Slug: la-corriente-las-aguas-turbias-20-anos-la-operacion-genesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Soy-genesis-45.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Mar 2017] 

**“Contra la Corriente de las Aguas Turbias, a 20 años de la operación Génesis”** una novela de Alexandra Huck, inspirada en hechos reales sobre las vivencias de las víctimas de la Operación Génesis. El libro cuenta la historia de Mariela, una mujer afrocolombiana que con su comunidad sufre el desplazamiento forzado de su territorio y emprende la lucha para regresar a su hogar.

El lanzamiento se realizará en la Fundación Henrich Boell, a las 6:30 pm y contará con la participación de la poetisa y docente de la comunidad negra de Cacarica, **Yahaira Salazar, quien nació en la comunidad de Nueva Esperanza en Dios** y que vivió el retorno al territorio en Cacarica. Le puede interesar:["In Memorian Marino López a 20 años de la Operación Génesis"](https://archivo.contagioradio.com/in-memoriam-marino-lopez-a-20-anos-de-la-operacion-genesis/)

De igual forma **Constanza Viera Quijano, periodista y traductora de la obra, estará presente durante el evento** y participará con aportes sobre la novela en la reconstrucción de memoria y moderará Florian Huber.
