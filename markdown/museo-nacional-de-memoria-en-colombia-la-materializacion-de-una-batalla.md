Title: Museo Nacional de Memoria en Colombia: la materialización de una batalla
Date: 2015-04-09 00:46
Author: CtgAdm
Category: Nacional, Paz
Tags: 9 de abril, calle 26, eje, eje de la memoria, historia, historica, jose antequera, memoria, museo, paz
Slug: museo-nacional-de-memoria-en-colombia-la-materializacion-de-una-batalla
Status: published

###### Foto: Centro de Memoria 

<iframe src="http://www.ivoox.com/player_ek_4327280_2_1.html?data=lZifmZecdI6ZmKiak5mJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbnxtSYsMbHrdDiwtGYxsqPsMKfrsra0dfNpYzZ1JDZw5DRpdXZ087Ozs7epcTdhqigh6eXsozYxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Antequera, Hijos e hijas por la memoria y contra la impunidad] 

Este jueves 9 de abril, en un acto público que se llevará a cabo a las 9 de la mañana, el presidente Juan Manuel Santos presentará el predio de 20 mil metros cuadrados en el que se construirá el futuro Museo Nacional de la Memoria de Colombia.

El Departamento Nacional de Planeación ha destinado la cifra nada despreciable de \$44.000 millones de pesos para la construcción de este Museo que se entregaría en el año 2018. El debate sobre sus contenidos, perspectivas, formas de funcionamiento, gestión y propuesta arquitectónica, entonces, no podría ser menor, y plantea un nuevo campo de discusión de la historia y la memoria en Colombia.

Para José Antequera, de la organización Hijos e hijas por la memoria y contra la impunidad, quien ha trabajado alrededor de políticas de la memoria histórica y conflicto, la decisión de construir el Museo Nacional de la Memoria significa el avance en una reivindicación que vienen haciendo hace mucho tiempo las víctimas en Colombia porque existan lugares públicos en los cuales se reconozca su propia existencia como consecuencia del conflicto interno del país.

Colombia estaría avanzando además, en sintonía con otros países de América Latina, en el reconocimiento de la existencia histórica de violaciones a los Derechos Humanos, tal como ocurriera en Chile o Argentina con la propuesta de construir museos de la memoria en lugares que otrora fueran epicentros de asesinatos, desapariciones forzadas y torturas durante las dictaduras militares, como la Escuela de Mecánica de la Armada en Argentina.

Pero si bien, el camino transitado es similar, en Colombia se plantea un reto para el conjunto de la sociedad: En los países del cono sur los procesos de memoria se dieron una vez finalizaron condiciones victimizantes determinadas: las dictaduras militares. En Colombia, por el contrario, el conflicto armado aún se vive diariamente, tanto como los diálogos de paz y la implementación de políticas de verdad, justicia y reparación como la Ley 1448; Además, las organizaciones sociales han hecho y hacen esfuerzos mancomunado por evidenciar la relación entre conflictos sociales y políticos estructurales con el conflicto armado. El contenido de los escenarios de memoria e historia aún está en construcción, como evidenció la presentación del Informe de la Comisión Histórica del Conflicto Armado y sus Víctimas el pasado mes de febrero del 2015.

Para Antequera, es precisamente en este punto donde la batalla por la memoria demuestra su vigencia. La discusión sobre los contenidos debería darse para el Museo Nacional de la Memoria, pero además “el espacio debe estar dispuesto para la discusión abierta, debe reconocer perspectivas diferenciales" permanentemente.

“Solo a partir de una ciudadanía desde los movimientos sociales es posible construir una historia favorable a la garantía de los DDHH, y no a su instrumentalización por parte de ciertos sectores políticos y de poder, como por supuesto es posible que ocurra”, señala José Antequera.

Un ejemplo de ello sería lo ocurrido con la Avenida/Calle 26, que con la presencia del Centro de Memoria, Paz y Reconciliación, los murales de la memoria, el Monumento a los Héroes Caídos, y recientemente la propuesta de generar un memorial en el Aeropuerto El Dorado sobre Bernardo Jaramillo, José Antequera Antequera y Carlos Pizarro, se ha convertido paulatinamente en un corredor de la memoria. Precisamente esta avenida fue recientemente decretada por la Alcaldía Mayor de Bogotá como eje de la memoria: y aunque sectores del país quisieran “que vinieran los visitantes internacionales a ver el progreso en oficinas, y lo que está ocurriendo en cambio es que los visitantes podrán ver que hay un país que avanza en dignidad, que avanza en democracia, que puede reconocer su memoria y que trabaja fuertemente por los DDHH”, señala Antequera.  
La construcción del Museo Nacional de la Memoria, también en la Calle 26, se sumará a estos espacios públicos y gratuitos para promover, como exigen las víctimas, las memorias vivas en Colombia.
