Title: A un paso de la penalización del maltrato animal
Date: 2015-11-11 13:43
Category: Animales, Nacional
Tags: Código Procedimiento Penal, Crueldad animal en Colombia, Juan Carlos Losada, Juan Manuel Galán, Maltrato animal, Proyecto de Ley 172
Slug: a-un-paso-de-la-penalizacion-del-maltrato-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-26-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9365533_2_1.html?data=mpijl5qXd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhYzpz5Ddw9jTb8XZjNHOjdXJssLgyt%2FOxc6Jh5SZo5jbjcnJsIzhwtHh1MbYs4zVz87aw9GRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, Plataforma ALTO] 

Este miércoles por unanimidad la comisión Primera del Senado aprobó en tercer debate el proyecto de Ley 172 con el que los animales dejarían de ser bienes muebles, penalizando el maltrato hacia ellos.

La iniciativa fue impulsada por el representante a la Cámara Juan Carlos Losada, y en senado el ponente es el congresista Juan Manuel Galán. "**Es otra votación unánime a favor de los animales,** hay que reconocer el trabajo extraordinario del ponente Juan Manuel Galán", dijo Losada.

Natalia Parra, directora de la Plataforma ALTO y Secretaria Técnica de la Bancada Animalista del Congreso de la República, señaló que la **senadora Claudia López presentó dos importantes proposiciones,** la primera se trata de fortalecer el tema de la sintiencia de los animales en el Código Civil; y la segunda está dirigida que no se puedan destinar recursos públicos para espectáculos donde se maltrate animales. "No es exactamente prohibir, pero por lo menos con los impuestos de los ciudadanos no se podrá financiar esos eventos crueles, **así que no hay razón para el pataleo del lobby taurino",** expresa Parra.

Con esta Ley las conductas como **el maltrato animal, la zoofilia, los tratos crueles y denigrantes serían castigadas.** Además, se reformaría el Estatuto Nacional de Protección de Animales con el objetivo de actualizar el valor de las multas y facilitar la competencia de las autoridades de policía. Para su cumplimiento se le otorgaría a la Policía Nacional la facultad de realizar aprehensiones preventivas de animales que están siendo objeto de maltrato.

Por otra parte, se crearía un título en el **Código Penal que consagre el tipo penal de maltrato animal y cinco circunstancias de agravación punitiva** y además se modificaría el Código Procedimiento Penal.

“El proyecto sale enormemente fortalecido, cada vez está más cerca la penalización del maltrato animal y **el nuevo estatus que van a tenerlos animales, siendo considerados como seres sintientes”,** agregó el representante Losada.
