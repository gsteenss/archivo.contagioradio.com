Title: Cubanos conmemoran 56 años de la victoria en Playa Girón
Date: 2017-04-19 12:13
Category: El mundo, Otra Mirada
Tags: Cuba, Fidel Castro, playa girón, Revolución Cubana
Slug: playa-giron-victoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/playa-giron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: 

##### 19 Abr 2017 

Con el lema "**Girón, victoria del socialismo**", en una sentida ceremonia los pobladores de la localidad costera de Playa Girón, conmemoraron **56 años de la victoria militar contra la invasión Estadounidense** a la Isla de Cuba, ocurrida en ese mismo lugar el 19 de abril de 1961.

Al acto de memoria asistieron pobladores, miembros del Partido Comunista y principales autoridades de Matanzas, provincia a la que pertenece la Ciénaga de Zapata, lugar donde tuvieron lugar los combates contra un facción de **500 mil mercenarios, en su mayoría cubanos**, reclutados con aprobación y financiación (4.4 millones de dólares) del gobierno Eisenhower.

**Idobaldo Díaz**, miembro del buró territorial matancero del Partido Comunista Cubano (PCC), fue el encargado de pronunciar el discurso central, destacando la importancia de esa victoria del socialismo ante un ataque imperialista, que dejó un costo significativo para los combatientes y la población civil con  176 muertos, 300 heridos y 50 discapacitados.

'Las generaciones de hoy **preservaremos las conquistas revolucionarias y continuaremos con la decisión de morir antes que volver al pasado** y carecer de independencia y soberanía', aseguró el dirigente a la multitud que le escuchaba desde la plataforma engalanada de rojo, blanco y azul, colores del pabellón nacional cubano.

En el evento **52 de los combatientes protagonistas de la gesta fueron reconocidos con la medalla aniversario 60 de las Fuerzas Armadas Revolucionarias**. Además se escuchó la voz de varios oradores que destacaron la relevancia histórica de los hechos ocurridos ese miércoles como símbolo de la autonomía de la isla para decidir su destino.

Durante la conmemoración **fue exaltada la figura del fallecido líder Fidel Castro**, en cuatro momentos culturales, en el que se presentaron dos proyectos artísticos comunitarios, un declamador, sexteto musical, solistas y repentistas. Le puede interesar: [15 fotografías icónicas de Fidel Castro](https://archivo.contagioradio.com/15-fotografias-iconicas-de-fidel-castro/).

   
 

En 1961 en la localidad ubicada al sur occidente de la provincia de Matanzas y a 220 kilómetros de La Habana, se encontraban los últimos invasores de la llamada brigada 2506, quienes fueron doblegados en menos de 72 horas, acabando con el propósito Estadounidense de establecer un punto de partida para un gobierno contrarevolucionario provisional en ese lugar.

1.200 hombres custodiados por buques, habían desembarcado en Playa Girón y Playa Larga el 17 de abril. A pesar que lograron avanzar en la zona, no contaron con el apoyo aéreo efectivo por los estadounidenses, por lo cual **el miércoles 19 de abril los invasores quedaron rezagados y cercados por las fuerzas armadas cubanas** quienes los llevaron a la rendición.

Las tropas cubanas, dirigidas por el mismo Fidel en persona, se componían en su mayoría por m**ilicianos sin experiencia militar que de manera voluntaria participaron de la contra toma**,  respaldados por hombres del Ejército Rebelde y la Policía Nacional Revolucionaria.

 
