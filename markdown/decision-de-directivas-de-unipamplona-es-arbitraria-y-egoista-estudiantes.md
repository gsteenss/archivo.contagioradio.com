Title: "No vamos a dejar en manos del olvido, la lucha estudiantil" Estudiantes de UniPamplona
Date: 2017-10-17 17:19
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad de Pamplona
Slug: decision-de-directivas-de-unipamplona-es-arbitraria-y-egoista-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Pamplona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.Net] 

###### [17 Oct 2017] 

La decisión de las directivas de la Universidad de Pamplona de suspender clases en la Institución fue arbitraria y egoista, según los estudiantes, que no lograron llegar a ningún consenso con los estudiantes, los alumnos han manifestado que se mantendrán en asamblea permanente y que esta medida no solo atenta contra la autonomía estudiantil, **sino que es arbitraria y egoísta, tomada como herramienta de presión para acabar con la movilización estudiantil.**

Los estudiantes iniciaron el paro universitario exigiendo a las directivas en primera medida exponer el estado financiero del centro educativo, en términos administrativos, de acuerdo con Sergio Hoyos, estudiantes de la carrera de Filosofía señaló “si bien es cierto que tenemos una universidad desfinanciada por el Estado, **también hay una administración que no ha proyectado bien los pocos recursos**”.

De igual forma, los estudiantes están exigiendo un proceso de democratización al interior de la Universidad para transformar los estatutos y así permitir una **mayor participación de los cuerpos colegiados, a través de una constituyente universitaria**. (Le puede interesar:["U de Pamplona otra víctima de la desfinanciación pública de las universidades"](https://archivo.contagioradio.com/universidad-de-pamplona-en-asamblea-permanente/))

Hoyos, señaló que el siguiente paso para los estudiantes es intentar mantener la movilización al interior del campus universitario. Además, hay una comisión de 40 estudiantes que representan a la comunidad que están a la espera de tener un escenario de interlocución con el Ministerio de Educación en Bogotá.

“No vamos a dejar en manos del olvido, la lucha estudiantil, estamos invitando a que los estudiantes se informen, **la suspensión puede ser mínima si logramos acuerdos contundentes con las directivas de la universidad**” afirmó Sergio Hoyos. (Le puede interesar: ["Estudiantes de la Universidad Pedagógica Nacional llaman a un nuevo paro estudiantil"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-pedagogica-llaman-a-un-nuevo-paro-nacional-estudiantil/))

<iframe id="audio_21543398" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21543398_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
