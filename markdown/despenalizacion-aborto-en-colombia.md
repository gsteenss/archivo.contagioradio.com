Title: ABC de la despenalización del aborto en Colombia
Date: 2017-09-20 17:15
Category: Libertades Sonoras, Mujer
Slug: despenalizacion-aborto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lit - Cl] 

###### [20 Sept. 2017] 

[La despenalización del aborto en Colombia ha cumplido ya 10 años, por ello **en \#LibertadesSonoras hablamos con dos invitadas sobre las sentencias, los obstáculos y jurisprudencia en el país.** ¿qué normatividad hay en Colombia que regule el derecho de las mujeres al aborto?¿qué diferencias hay entre legalizar y despenalizar? entre otros temas fueron dialogados en este programa. ]

[Así mismo, conocimos las **iniciativas que se han trabajado en pro de la  defensa de la despenalización del aborto** como condición indispensable para el ejercicio del derecho a decidir acerca de la vida y la maternidad. Le puede interesar: [A diez años de la despenalización del aborto mujeres siguen enfrentando barreras](https://archivo.contagioradio.com/40023/)]

Además, salimos a las calles para preguntarle a la sociedad **¿qué piensan sobre el aborto?¿qué dudas tienen al respecto?**¿qué debe hacer una paciente si un médico objeta conciencia para no realizar el aborto? y en nuestro programa lo contestamos. Le puede interesar: [Fiscal propone despenalizar el aborto en las primeras 12 semanas de gestación](https://archivo.contagioradio.com/fiscal-montealegre-propone-despenalizar-el-aborto-en-primeras-12-semanas-de-gestacion/)

<iframe id="audio_21003041" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21003041_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
