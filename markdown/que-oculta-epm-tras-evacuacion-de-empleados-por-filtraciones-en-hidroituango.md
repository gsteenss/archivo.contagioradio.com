Title: ¿Qué oculta EPM tras evacuación de empleados por filtraciones en Hidroituango?
Date: 2019-05-14 17:55
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Filtraciones, Hidroituango, Ríos Vivos Antioquia
Slug: que-oculta-epm-tras-evacuacion-de-empleados-por-filtraciones-en-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Hidroituango-Filtraciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Foto: @EPMestamosahi] 

Después de los hechos del 11 de mayo, cuando 105 empleados de Hidroituango fueron evacuados debido a una filtración de agua en el tunel 4 y pese a que Empresas Públicas de Medellín (EPM) afirma que la situación no representa un riesgo, las comunidades aledañas han expresado su preocupación pues desconocen cuál es la verdadera magnitud de lo que sucede al interior de la represa.

**Isabel Cristina Zuleta, integrante de Ríos Vivos Antioquia** expresó con preocupación que no existe información clara ni oportuna para con las comunidades sobre lo que ocurre en el Túnel 4 donde se presentaron tales filtraciones,  "EPM empezó hablando de un simulacro y terminó hablando de una evacuación de trabajadores", apunta.

"Era evidente que se estaba presentando una situación de gravedad al interior de los túneles" indicó Zuleta haciendo referencia a los videos compartidos a través de redes sociales por los trabajadores que no habían sido evacuados del lugar oportunamente y que según la lideresa no pueden referirse al respecto pues su trabajo estaría en riesgo.

> El túnel 4 de captación está fuera de control. No es claro de dónde vienen las filtraciones que no han dejado de crecer. En este momento se aplican inyecciones desde el interior del tunel como medida desesperada. EPM debe informar sobre lo que pasa en Hidroituango. [pic.twitter.com/u5PsX4XiwU](https://t.co/u5PsX4XiwU)
>
> — Daniel Quintero Calle (@QuinteroCalle) [14 de mayo de 2019](https://twitter.com/QuinteroCalle/status/1128324527312609280?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### EPM  no nos da información clara

La integrante de Ríos Vivos Antioquia manifestó que las comunidades vieron con preocupación cómo salían cientos de trabajadores de los túneles, mientras la población seguía sin saber qué estaba ocurriendo **"no se nos dice qué está pasando, hace cuánto está pasando, cuál es el estado de los túneles y realmente cuál es la magnitud de lo que se está presentando"**, añadió.

<iframe src="https://www.youtube.com/embed/VntzrmOAMKI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Adicionalmente al estado del túnel 4, Zuleta también se refirió a un socavón descubierto en los 5,6,7 y 8, "EPM sostiene que lo que se presenta es la alteración de las rocas, situación que es preocupante pues el mineral tendría que estar firme, dudamos que nos esté diciendo la verdad" afirma.

Las personas que fueron desalojadas del Coliseo Jaidukamá la semana pasada, fueron llevados a unas viviendas donde podrán permanecer durante los próximos seis meses, sin embargo se desconoce qué pasará después de pasada esta fecha. [(Lea también: Damnificados de Hidroituango serán desalojados por orden de Alcaldía)](https://archivo.contagioradio.com/damnificados-de-hidroituango-seran-desalojados-por-orden-de-alcaldia/)

<iframe id="audio_35833586" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35833586_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
