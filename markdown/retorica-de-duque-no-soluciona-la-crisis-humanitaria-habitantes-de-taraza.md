Title: Retórica de Duque no soluciona la crisis humanitaria: habitantes de Tarazá
Date: 2020-01-22 17:56
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, Desplazamiento forzado, Los Caparros, Tarazá
Slug: retorica-de-duque-no-soluciona-la-crisis-humanitaria-habitantes-de-taraza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Duque.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 21 de enero, se denunció el asesinato de 2 personas y el desplazamiento masivo de 36 familias conformadas por 107 personas, incluidos 41 menores de edad de la vereda La Pipiola en el corregimiento El Doce de Tarazá, Antioquia debido a la presencia de grupos armados que obligaron a la población a marcharse. **Los hechos, se dieron paralelamente a la visita del presidente Iván Duque a una zona dominada por estructurales paramilitares** que se disputan el Bajo Cauca antioqueño y el Nudo de Paramillo mientras el Estado pese a estar presente no ejerce soberanía sobre el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Óscar Yesid Zapata**, integrante del Nodo Antioquia de la Coordinación Colombia Europa Estados Unidos afirma que se viene advirtiendo sobre la extrema situación que se vive en el Bajo Cauca, una zona donde existen intereses estratégicos y una confrontación entre grupos como las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y Los Caparros.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desplazamiento afecta mayoritariamante a familias que acogieron el plan de sustitución

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Siendo las 9:00 am un grupo por identificar llega a la vereda La pipiola donde intimidaron a los habitantes y les ordenaron abandonar el territorio, acto seguido las familias decidieron abandonar sus casas y sus animales de pastoreo, cabe resaltar que la mayoría de las personas afectadas son beneficiarias del Programa Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS). [(Lea también: Aumenta control por parte de grupos ilegales en Tarazá Antioquia)](https://archivo.contagioradio.com/enfrentamientos-de-grupos-armados-en-taraza/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este hecho, es tan solo una de las réplicas de desplazamiento forzado que viene ocurriendo en Antioquia**. Ye en abril del 2019 cuando cerca de 50 personas campesinas que habitan el sector de Las Flechas, en Ituango, o cuando 120 familias campesinas del corregimiento de La Caucana** tuvieron que abandonar el territorio en el mismo mes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Hay un Estado paramilitar en Tarazá"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Lo verdaderamente preocupante de este nuevo desplazamiento es que ocurrió mientras el presidente Iván Duque se encontraba en la subregión planteando estrategias para otorgar garantías a sus pobladores, "no es la primera vez que Iván Duque acude al Bajo Cauca, creemos que llega a hacer las mismas promesas de siempre, ocultas tras una retórica que no brinda seguridad a las comunidades campesinas", expresa Zapata.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que el problema está en que las soluciones que ofrece el presidente, no obedecen a los intereses de las comunidades, menos cuando desde las organizaciones sociales han denunciado la existencia de nexos entre el Estado y estructuras paramilitares, mientras "**la Fuerza Pública no ha querido depurar ni investigar quienes son las personas que trabajan para estas estructuras**", afirma.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El defensor de DD.HH. resalta que lo clave es identificar la facilidad con la que se movilizan estas agrupaciones pese a la gran presencia del Ejército y la Fuerza Pública, "el Bajo Cauca es la zona más militarizada del departamento de Antioquia con por lo menos 5.000 hombres de Fuerza de Tarea Conjunta Aquiles y 2.000 de la operacón Agamenón 2", además está la presencia del Batallón Rifles en Caucasia, e incluso con la llegada del presidente en la región en el mismo día, los hechos victimizantes siguen ocurriendo. [(Lea también: Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca)](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos ocurridos en La Pipiola se suman a varios casos, incluida la masacre del Guaimaro el pasado 17 de enero, que dejó cinco personas asesinadas, dentro de los que se contaron dos integrantes del PNIS y otros dos integrantes de Juntas de Acción Comunal. [(Le puede interesar: Continúan desplazamientos masivos en Bajo Cauca Antioqueño)](https://archivo.contagioradio.com/continuan-desplazamientos-masivos-bajo-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las razones de la guerra para provocar el desplazamiento
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Zapata explica que en medio de la disputa territorial, los actores armados terminan por e involucrar a la[población civil,](https://twitter.com/UnidadVictimas/status/1220083198002192386) al considerar que todas esas familias son funcionales a ellos, " dichos grupos creen que cuando dominan un territorio dominan a la población, cuando la verdad es que existe un sometimiento de las comunidades, mientras, la estructura rival piensa que los pobladores son aliadas del otro grupo. [(Le puede interesar: En el Bajo Cauca Antioqueño la violencia se está tomando el poder)](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma se plantea la posibilidad que son desplazados como una forma de distracción, dichos grupos ilegales podrían "**hacer algún operativo especial y necesitan distorsionar lo que está ocurriendo en alguna zona, para que se concentren las acciones militares en otra"**, una forma de operar que según Zapata fue común en esta zona durante el 2018 para así desviar la atención de las autoridades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Zapata destacó la importancia de retomar la Comisión de Garantías de Seguridad, instancia, que busca detener el asesinato de líderes sociales y construir políticas públicas de desmantelamiento de estructuras paramilitares sin embargo, apunta que desde el Estado "no se ha visto el interés para reactivarla junto a sus funciones" por lo que hasta que no se tome una decisión positiva al respecto, continuará este incremento en la violencia al interior del departamento de Antioquia. [(Le puede interesar: El líder social Humberto Londoño, es asesinado en Tarazá Bajo Cauca)](https://archivo.contagioradio.com/el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46925522" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46925522_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
