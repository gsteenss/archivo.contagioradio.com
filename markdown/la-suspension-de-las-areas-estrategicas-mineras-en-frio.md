Title: La suspensión de las áreas estratégicas mineras, en frío
Date: 2015-05-30 10:15
Category: Ambiente, Nacional
Tags: Consejo de Estado, Corte Constitucional, Mineria, páramos
Slug: la-suspension-de-las-areas-estrategicas-mineras-en-frio
Status: published

#### **Por [Andrée Viana Garcés]** 

Se ha dicho de todo sobre la decisión del Consejo de Estado que se conoció el pasado 15 de mayo, que suspendió 516 áreas estratégicas mineras declaradas en 2012 y 2013 sobre más del 20% del territorio nacional.

Se dijo que es un freno de mano a la minería, que es un ataque a la política minera del gobierno Santos, y que es una decisión agridulce porque, aunque envía un mensaje al Gobierno para que no siga pasando por alto el derecho a la consulta previa, también suspende el efecto benéfico de ordenar y hacer más estricta la entrega de títulos mineros.

Después de unos días, ya lejos del calor de las emociones de triunfo de unos y de las voces de alarma de otros, vale la pena revisar el significado de esa decisión.  Así, en frío, las consecuencias y los mensajes de ese auto se deberían leer –todos- en clave positiva. De una parte, esa medida del Consejo de Estado es una señal de que la consulta previa empieza a tener los efectos que le reconoció el nuevo código de procedimiento administrativo y de lo contencioso administrativo, de otra parte esa suspensión provisional no supone el fin de la política de ordenación de la minería, y finalmente, le da al gobierno un margen para hacer un giro y apuntar hacia la corrección constitucional del ordenamiento territorial.

**El rigor de la consulta previa más allá de la tutela**

El Código de Procedimiento Administrativo y de lo Contencioso Administrativo, por primera vez en el Derecho colombiano, define que es nulo todo acto administrativo expedido sin observar las exigencias derivadas del derecho a la consulta previa.  Es decir, que si la consulta es procedente y no se realiza dentro de los términos legales y constitucionales, el acto administrativo no es válido y debería salir de la vida jurídica.

Si esta norma es una novedad dentro del ordenamiento colombiano, su aplicación es una auténtica sorpresa. De hecho, ni la propia Corte Constitucional se ha atrevido a anular un acto administrativo cuando se ha violado el derecho a la consulta previa en su proceso de expedición.

Es cierto que aún las resoluciones no han sido anuladas, pero por el proceso de análisis que debió preceder su suspensión provisional, el auto de la semana pasada es un aviso. El  Consejo de Estado entendió que mientras se decide (en una sentencia) si las resoluciones son nulas o no, debe evitarse cualquier menoscabo de los derechos fundamentales de esas comunidades, pues esas resoluciones no fueron sometidas a consulta previa. Y aunque aclaró que esa decisión no condiciona el fallo definitivo, lo cierto es que desde el 2011 para que un acto sea suspendido provisionalmente es necesario que el juez haga un estudio de fondo muy similar al que debe hacerse para adoptar la decisión final.

**El “poco a poco” del ajuste extractivista**

Como se dijo arriba, el auto del Consejo de Estado no acaba con la política minera de Santos sino que encaja perfectamente en esta especie de proceso del “poco a poco” que ha ido dando al sector minero las dimensiones que le corresponden en el mapa del Estado Social de Derecho colombiano.

Algunos recordarán que en 2013 el mismo Consejo de Estado conoció en segunda instancia una acción de tutela interpuesta por un Consejo Comunitario del Chocó en contra de dos de las resoluciones ahora suspendidas. En ese momento, el Consejo de Estado actuaba como juez de tutela, no como máximo tribunal de control de legalidad de los actos administrativos y tal vez por eso no anuló esos actos administrativos. Ddesde la perspectiva de la tutela entendió que se trataba de normas generales que no afectaban directamente los derechos de los pueblos étnicamente diferenciados.

Esta  posición surge de varios errores valorativos en que incurre la sección cuarta del Consejo de Estado al interpretar la jurisprudencia de la Corte Constitucional.  En todo caso, esa sentencia de 2013 sí conminó “a las autoridades demandadas a que se aseguren de adelantar el trámite de la consulta previa con las organizaciones étnicas demandantes antes de que se ejecuten los actos preparatorios del proceso de concesión minera especial de las áreas mineras estratégicas creadas y delimitadas en el departamento de Chocó”.

Esa decisión del Consejo de Estado se sumó a las muchas de la Corte Constitucional que han dicho que la concesión misma de los títulos mineros debe ser sometida a consulta previa, sólo que fue “un poco” más allá y, colaborando con la intensión gubernamental de ordenar el proceso de entrega de títulos, ubicó el derecho a la consulta en el mecanismo de las rondas mineras o procesos de licitación de los bloques mineros que ofrecerá el gobierno nacional a los empresarios del sector.

Sin embargo lo hizo con timidez y sin suficiente claridad, lo que motivó en su momento a la Agencia Nacional de Minería, que abrió la discusión con una aptitud de recepción más amplia que en el pasado, a preguntar a la Dirección de Consulta Previa del Ministerio del Interior si debería adelantarse el proceso de consulta en la fase de definición de las áreas estratégicas mineras o en la de adjudicación de los contratos especiales de concesión, y cómo debería ser el proceso en cada caso.

Y, he aquí otro avance del “poco a poco”: aunque el Ministerio no dijo nada sobre las resoluciones, sí dijo que la consulta había que hacerla al momento de la licitación.

Eso significa que después de muchos litigios, el gobierno admitió expresa y oficialmente la procedencia de la consulta del título minero.  Esa nueva posición del gobierno implica que ha asumido que la sola concesión del título sí tiene capacidad de generar impactos sobre los pueblos indígenas y negros, que el particular no sólo adquiere expectativas, y que el derecho a explorar que se concede con ese título impactará derechos de los pueblos relacionados con su territorialidad y su pervivencia étnica y cultural.

Hace pocos días, la sección tercera del mismo Consejo de Estado, fue otro “poco” más allá y dio la voz de alerta más clara de la última década acerca de la necesidad de consultar las decisiones sobre el territorio que puedan afectar los derechos de los pueblos étnicamente diferenciados y mostró el norte para que el Gobierno avance en la tarea de ajustar la apuesta extractiva a los mandatos constitucionales.

**Un giro posible hacia el único norte sostenible**

Tanto la Corte como el Consejo de Estado han dicho que la minería tiene potencialidad de transformar de manera notable la tierra y la vida de sus habitantes. Por eso, la declaración de las áreas estratégicas mineras es una decisión de ordenación del territorio que, si no se ajusta al principio democrático constitucional, puede convertirse en un acto absolutamente centralizado y homogeneizador.

La declaración de las áreas estratégicas mineras trae consigo la decisión de cambiar la vocación natural o tradicional de los suelos cubiertos por ellas.  Por eso, el auto que suspende sus efectos otorga al gobierno un tiempo para repensar el giro que puede dar si al final esas resoluciones son declaradas nulas.  El giro podría evitar que la idea de ordenar el sistema minero mediante áreas mineras estratégicas, termine sirviendo de plataforma para una serie de decisiones adoptadas sobre la idea de que el suelo es un lienzo en blanco sin más función que la de separar a las grandes compañías de una riqueza (efímera) atrapada en los minerales del subsuelo.

De la gestión del ordenamiento del territorio dependen muchos derechos de sus habitantes, como el derecho a no ser desplazados forzosamente, a la soberanía alimentaria, a la salud, a la vida y al mínimo vital, de esa gestión dependen también casi todos los derechos de los pueblos indígenas y de comunidades negras, y, como si fuera poco, de ella dependen tambián los derechos de todos los colombianos al agua y a un ambiente sano.

Desde esa certeza, el giro hacia el único norte posible que alejará las decisiones sobre minería del fantasma del centralismo radical y la homogeneización antidemocrática, supone una modificación del procedimiento administrativo que debe surtirse antes declarar un área minera estratégica.

En ese procedimiento el gobierno nacional debería integrar a las autoridades municipales (como le dijo la Corte que hiciera) y a las autoridades indígenas y negras mediante procesos de consulta previa adecuados para ese tipo de decisiones.

En efecto, la consulta previa de las áreas estratégicas mineras tendría inmensas ventajas que no tiene la consulta de cada concesión minera, entre las que se cuentan las siguientes:

1.  a\) En esas consultas se discutiría a fondo la destinación y uso de los territorios étnicos,
2.  b\) Con sus resultados se iría configurando un mapa de planeación –gradual- del ordenamiento territorial minero del país teniendo en cuenta  bienes y derechos constitucionalmente protegidos adicionales a los que solo se refieren a la eficiencia del sector minero.
3.  c\) Al ser consultas sobre la planeación del territorio, permitiría un verdadero diálogo entre el modelo de desarrollo nacional y las opciones propias de los pueblos, y
4.  d\) Permitiría a las autoridades mineras llegar a las rondas mineras con ofertas de bloques libres de objeciones culturales irreductibles, en los que el diálogo de la Consulta se centraría en el cómo y dónde, y dejaría de ser un cruce de monólogos entre posiciones irreconciliables que terminan impidiendo el ejercicio real de los derechos étnicos y la consecución de las metas proyectadas para el sector.

**La aplastante ceguera del futuro**

En conclusión, el auto del Consejo de Estado permite a los colombianos un plazo breve para la reflexión.  El pasado reciente ha dejado imágenes de alerta. No sólo sobre la escasez y fragilidad de los elementos naturales con que comercia el ser humano afanosamente, ni solo sobre las consecuencias de las sequías y las inundaciones que generan los excesos que se cometen en nombre del desarrollo y el crecimiento económico.  También sobre la cercana relación entre el extractivismo y los modelos de Estados Rentistas y autoritarios.

Los mensajes y aterradoras imágenes que nos ha concedido el pasado están al frente de los ojos de todos, de los ciudadanos y de los tomadores de decisiones.

El giro en la forma de ordenar el territorio y la minería  debe basarse en esas lecciones.  El pasado es la única fuente cierta de conocimiento. Y por eso el lapso que el Consejo de Estado ha regalado al gobierno y a la ciudadanía debería servir para pensar acerca de la sabiduría aymara y maorí, dos culturas milenarias que entienden que el pasado está delante de nuestros ojos para guiarnos, y que el futuro, en cambio, está detrás porque nadie puede verlo.

La ceguera de la ambición extractivista podría curarse si, durante este lapso que concede el Consejo de Estado, sus agentes miraran hacia adelante y consultaran a consciencia y por una vez el pasado reciente.
