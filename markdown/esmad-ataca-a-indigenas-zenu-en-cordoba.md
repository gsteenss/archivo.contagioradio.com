Title: ESMAD ataca a indígenas Zenú en Córdoba
Date: 2016-06-01 14:58
Category: Nacional, Paro Nacional
Tags: Chinú, cordoba, ESMAD ataca población Chinú, Minga Nacional, Paro Nacional
Slug: esmad-ataca-a-indigenas-zenu-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ESMAD-Chinú-Córdoba.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: COMOSOC ] 

###### [1 Junio 2016]

En el marco de la Minga Nacional, miembros del ESMAD atacaron a los pueblos indígenas Zenú que se movilizaban en la troncal del Caribe a la altura de Chinú, Córdoba. **Una mujer embazada fue herida, un guardia está desaparecido y otro fue detenido.** A estos hechos se suma el ataque con gases lacrimógenos de este martes, del que resultaron cinco personas heridas, dos de ellas ya fuera de peligro y las otras tres aún hospitalizadas.

De acuerdo con el líder indígena Martín Moreno, estos ataques se dan en el marco de la movilización pacífica que vienen adelantando, para exigir al gobierno cumplir con los acuerdos pactados en años anteriores, por lo que no desestiman la necesidad de bloquear la Troncal del Caribe, pues se trata de una protesta. Pese a que han exigido la presencia de los gobernadores de Sucre y Córdoba, las autoridades no se han presentes, mientras que **por lo menos 30 unidades del ESMAD y más Policías los tienen rodeados**.

<iframe src="http://co.ivoox.com/es/player_ej_11746150_2_1.html?data=kpaklpuVeZGhhpywj5WYaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nW0JCxs9PZz9SYj5CwaaSnhqaxxsrWb6rixYqwlYqliMjZz8aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 
