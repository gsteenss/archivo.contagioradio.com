Title: Campesinos reclaman su derecho a vivir y cuidar del páramo de El Cocuy
Date: 2017-08-07 12:36
Category: Ambiente, Nacional
Tags: campesinos, Cocuy, páramos
Slug: cocuy-ampesinos-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/img_2241.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tyler Broeren 

###### 07 Ago 2017 

Con una proclama por el derecho a permanecer en su territorio, campesinos y campesinas del Cocuy, Güican, Chiscas y Chita, en la sierra nevada de El Cocuy en Boyacá, manifestaron su rechazo a las políticas nacionales de delimitación de páramos donde se prohíbe todo tipo de actividades agropecuarias en esas areas protegidas, por considerar que con estas se pretende legalizar el despojo de sus tierras.

De acuerdo con la comunicación fechada el 4 de agosto, los agricultores manifiestan que desde antes de la fundación como república del país sus antepasados labraron la tierra "contribuyendo a la seguridad y soberanía alimentaria sin causar daño a la naturaleza" y que tales determinaciones del gobierno los convertirían en "errantes pordioseros"

En consideración de los firmantes la existencia de los municipios del El Cocuy, Chita, Chiscas y Güican como entes territoriales se encuentran bajo amenaza por leyes que pretenden su desaparición jurídica, que para el caso de El Cocuy, sería "inviable si se permite que se le arrebate el 93% de su territorio". Le puede interesar: [Naciones Unidas insta al gobierno colombiano a proteger el Nevado del Cocuy](https://archivo.contagioradio.com/naciones-unidas-insta-al-gobierno-colombiano-a-proteger-el-nevado-del-cocuy/))

Los campesinos aseguran que el gobierno nacional utiliza como pretexto la defensa del ambiente, creando leyes que están "al servicio de las transnacionales y capital financiero internacional", con las que se pretende "convertir en mercancía los bienes naturales comunes tales como el agua, la biodiversidad, la genética, el aire y el medio ambiente haciéndolos objeto del mercado" manifestaron.

Por lo anterior, las organizaciones firmantes hicieron un llamado a los habitantes de páramos en Colombia a unir esfuerzos "para enfrentar el despojo que aspira el gobierno nacional a imponernos y a luchar por la defensa de la permanencia en el territorio y de nuestras actividades agropecuarias", declarando además su solidaridad con las reivindicaciones del pueblo U´wa, asumiéndolas como propias. Le puede interesar: [Nevado de El Cocuy en riesgo por ecoturismo desmedido](https://archivo.contagioradio.com/nevado-del-cocuy-en-riesgo-por-ecoturismo-desmedido/)

[Proclama Campesina y Popular de El Cocuy Por La Permanencia en El Territorio y La Vida Digna](https://www.scribd.com/document/355743570/Proclama-Campesina-y-Popular-de-El-Cocuy-Por-La-Permanencia-en-El-Territorio-y-La-Vida-Digna#from_embed "View Proclama Campesina y Popular de El Cocuy Por La Permanencia en El Territorio y La Vida Digna on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd  
<iframe id="doc_68088" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/355743570/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-J3ZmGjfqzHPsDNjlhrr9&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
