Title: 334.000 profesores entran en paro nacional convocado por FECODE
Date: 2015-04-22 12:38
Author: CtgAdm
Category: Educación, Nacional
Tags: Derecho a la educación en Colombia, Evaluación docente, fecode, gina parody, Luis Alberto Grubert, Ministerio de Eduación, Nivelación salarial, Paro Nacional, Plan Nacional de Desarrollo 2014 - 2018, Protección para profesores
Slug: 334-000-profesores-se-suman-al-paro-nacional-de-fecode
Status: published

###### Foto: eluniversal.com 

<iframe src="http://www.ivoox.com/player_ek_4393686_2_1.html?data=lZimlZuceo6ZmKiakp2Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRd5Soj5WdkpDUttDaxtjc1MrXb9TZjNjiz8bSb8LgjNXO1NSPssLXytTbw9GPqMafp6qwsampcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Alberto Grubert, presidente de FECODE] 

Cerca de 334.000 profesores y profesoras del sector público en Colombia iniciaron la jornada de Paro Nacional indefinido convocado por la Federación Colombiana de Educadores FECODE, a pesar de las declaraciones de la Ministra de Educación, **Gina Parody,** en las que advertía que quienes no se presentaran en sus puestos de trabajo recibirían sanciones como la retención de sus aportes salariales y la negativa a dialogar en medio del  paro.

**Luis Alberto Grubert,** presidente de FECODE afirma que las reclamaciones del sector del Magisterio en Colombia son apenas justas, ya que el pliego de exigencias que se está reivindicando es el mismo desde hace varios años y no ha tenido ninguna respuesta efectiva por parte del gobierno.

**Las exigencias de FECODE se basan en cuatro puntos**

1.  **Nivelación salarial:** Según Grubert los profesores y profesoras de Colombia reciben un pago inferior al de los trabajadores públicos en general y la promesa de un aumento del 10% solo serviría para nivelar el pago, sin embargo esta propuesta del gobierno está supeditada al PND que no contempla un crecimiento del presupuesto para la educación.
2.  **Mejoramiento en el servicio de salud:** Según los propios maestros, el derechos a la salud de los profesores es uno de los más precarios en medio de la crisis del sistema de salud en Colombia, “las condiciones deben ser dignas como las de cualquier ser humano”, señala Grubert.
3.  **Evaluación docente:** Según las directivas de FECODE el gremio no se opone a la evaluación que siempre debe estar destinada al mejoramiento de la calidad. El problema es que el gobierno diseña un plan de evaluación que frena la posibilidad de ascender en el escalafón y en ningún momento plantea la posibilidad de evaluar para corregir los errores y avanzar.
4.  **Protección para profesores** y profesoras amenazados o que ejercen en zonas de alta peligrosidad: FECODE señala que el gremio de los profesores es uno de los más victimizados en Colombia, en los últimos años han sido asesinados cerca de 1000 profesores y no se han aprobado los traslados de los docentes que están amenazados.

\

Así las cosas el paro indefinido que comienza hoy, tiene como principal responsable al gobierno nacional que no plantea soluciones a pesar de haber conformado una mesa de negociación que lleva 55 días funcionando sin alcanzar resultados reales, puesto que lo acordado está sometido a aprobación por parte del Ministerio de Hacienda, punto de clave que provoca también la indignación puesto que se está supeditando el derecho a la educación a un problema netamente económico, señala Luis Albero Grubert.
