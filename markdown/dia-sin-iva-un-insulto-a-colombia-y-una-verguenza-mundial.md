Title: Día sin IVA, un insulto a Colombia y una vergüenza mundial.
Date: 2020-06-23 19:02
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Centro Democrático, Covid-19, dia sin iva, pandemia, Presidente Ivan Duque
Slug: dia-sin-iva-un-insulto-a-colombia-y-una-verguenza-mundial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/fotodeabcpolítica.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: ABC Política {#foto-de-abc-política .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El día sin IVA se configuró en uno de los grandes insultos a la ciudadanía colombiana. Ciudadanía que ha afrontado sola y con dignidad los efectos de una pandemia en la que el gobierno decidió favorecer solamente a las **FFMM** y al sector financiero, propietario de las grandes empresas del país, entre ellos los supermercados de cadena que fueron los únicos ganadores de esta jornada de muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Hay que decir que la gestión del gobierno de Iván Duque y el Centro Democrático ya era un fracaso y una vergüenza internacional antes de que llegara la pandemia del COVID 19.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La política exterior de desconocimiento a tratados internacionales, llegando hasta negar la entrada del relator de las naciones unidas para los derechos humanos, el asesinato de líderes sociales y excombatientes, las proliferación de estructuras ligadas al narcotráfico, el retorno de estrategias paramilitares, la caída en el empleo, la bajísima legitimidad de las instituciones como la fiscalía en manos de un amigo personal del presidente son solo una muestra de tal fracaso político, económico y social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una vez llegado el COVID con sus consecuencias en torno a aislamiento obligatorio, hambre y quiebra de las economías informales de las que sobreviven el 80% de los habitantes de nuestro país entre quienes viven en la informalidad de las ciudades, campesinos, mujeres, ancianos y hasta niños.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: Según la FAO el COVID 19 ha afectado al 87% del campesinado en Colombia](https://archivo.contagioradio.com/segun-la-fao-el-covid-19-ha-afectado-al-87-del-campesinado-en-colombia/) {#le-puede-interesar-según-la-fao-el-covid-19-ha-afectado-al-87-del-campesinado-en-colombia .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sin embargo, el episodio de este 19 de junio fue la gota que debería rebosar la copa. Esta medida, considerada como un alivio para la ciudadanía afectada por la reforma tributaria impulsada por el Ministro Carrasquilla se convirtió en un golpe mortal y una burla de magnitud catastrófica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las últimas semanas de marzo se le pidió a la ciudadanía guardar distancia, quedarse en casa, evitar la propagación del COVID para darle tiempo al gobierno de alistar el precario sistema de salud. Pero ni lo uno ni lo otro. Ni se evitó la propagación del virus, pues el 19 de junio tuvimos el pico más alto de número de muertes por el Coronavirus –cifra que seguirá creciendo según los análisis estadísticos-, ni se aprestó el sistema de salud pues ni las camas UCI, ni los ventiladores, ni los elementos de bioseguridad llegaron a hospitales de ciudades, municipios y zonas alejadas de los centros.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Un claro ejemplo de ello es el departamento del Amazonas con uno de los índices más altos de contagios y muertes.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además de ello, el gobierno tiene la desfachatez de burlarse del esfuerzo de la ciudadanía, encerrada por tres meses, programando un Día Sin IVA que provocó más de 30 aglomeraciones con más de ochenta mil personas en varias ciudades, en almacenes de cadena y centros comerciales. El espectáculo transmitido en directo a través de las redes sociales y provocado por el gobierno nacional fue vergonzoso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La gente salió de sus casas para intentar adquirir unos productos que, supuestamente tendrían el 19% de descuento. Salieron a pesar de las evidencias de que almacenes de cadena como Alkosto, Éxito y Falabella habían decidido, días antes, subir los precios para quedarse con la ganancia que aun en pandemia, nunca han dejado de recibir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Cómo se le ocurre a un presidente, a un gobierno, provocar semejantes desórdenes aprovechándose de las necesidades de la gente? En un país donde la mayoría de la población vive en condiciones precarias con salarios indignos es apenas comprensible que quieran aprovechar que el gobierno decidió no cobrar un impuesto por un día, para sentir que tantos días de trabajo valen la pena, para sentir que a pesar de la precariedad pueden ver un partido de fútbol o una película con un televisor de 50 pulgadas aunque no tengan a veces para el almuerzo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El presidente Iván Duque salió a decir que no se trata de buscar culpables y podemos estar de acuerdo en ello, ¿para qué buscar culpables cuando la responsabilidad de lo sucedido está tan claramente ubicada en él y en su gobierno? No vamos a encontrar a culpables entre las cientos de miles de personas que decidieron salir a comprar sus cositas con descuento, allí no están. **Están en la casa de Nariño, en el gobierno y en el congreso.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Claro, preferiríamos ser una ciudadanía más consciente de que un día sin impuestos no es realmente significativo para nuestra economía o nuestro buen vivir. Tal vez sería mejor salir masivamente a exigir nuestros derechos a la salud, a la educación, a un ambiente sano o a unos salarios dignos, pero para eso evidentemente nos falta mucho, estamos por buen camino pero nos falta mucho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero tampoco vamos a culpar a la ciudadanía de un posible aumento en el número de personas contagiadas o muertas por COVID, el responsable de que una pandemia como esta pueda matarnos es el gobierno, los gobiernos que desmantelaron un sistema de salud y lo tienen cada vez peor. Es hora de que exijamos que este gobierno, que Iván Duque y su gabinete asuman la responsabilidad política por lo que pasa en nuestro sufrido país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### La Ñapa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Era de esperarse que el gobierno, a través de su bancada en el congreso, hundieran el proyecto de renta básica que habían apoyado más de 50 congresistas. Ingenuo sería que los atenidos del gobierno permitieran que la ciudadanía tuviera un pequeño porcentaje de lo que le han aportado al país. En este gobierno no.

<!-- /wp:paragraph -->
