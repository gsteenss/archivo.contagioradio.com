Title: "Libertarias": Vicente Aranda contra el olvido.
Date: 2015-05-26 19:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Ana Belén, Ariadna Gil, Guerra Civil española en el cine, Libertarias película, Vicente Aranda, Victoria Abril
Slug: libertarias-vicente-aranda-contra-el-olvido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/LIBERTARIAS-14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Fotografía: Pipo Fernández.] 

[*Un aporte a la memoria histórica desde la visión del cineasta español, fallecido la madrugada de hoy a los 88 años de edad.*]

La idea de realizar una cinta sobre la **Guerra Civil española** rondaba la cabeza del director Barcelonés **Vicente Aranda** desde mitad de los años 80, mientras trabajaba en su película "Danny Pelopaja". Sin embargo, tendrían que pasar poco más de una década para que su proyecto quedara plasmado en la pantalla y por poco una más, para que fuese estrenada internacionalmente.

En colaboración con el crítico José Luis Guarner y el novelista Antonio Rabinad, se escribió un primer guión de "**Libertarias**", de 1996; con la idea de recuperar y reivindicar la memoria histórica de la Guerra Civil española y todos aquellos que vivieron y fueron parte de ella, representados por un grupo de mujeres anarquistas y su lucha destinada a ser derrotada.

Antes de rodar "**Libertarias**", Aranda, uno de los referentes de la llamada Escuela de Barcelona, afirmaba en entrevista concedida a Fotogramas de España "Somos hijos, hermanos y nietos de la gente que vivió aquello, y yo no estoy dispuesto a ser colaborador de la amnesia del país. La utopía sólo será algo absolutamente irrealizable si dejamos de pensar en ella**"**, y la manera de luchar contra el olvido fue precisamente narrando la lucha de **"**unas mujeres que prefirieron morir de pie, como los hombres, a vivir de rodillas como criadas**"**.

Es entonces una doble reivindicación la que retrata "**Libertarias**": l**a memoria de la Guerra Civil y el papel de la mujer como parte de la misma**, la manera anarquica en que se levanta y empuña el fusil. Aranda pone el mensaje en la boca de Pilar, una de las mujeres libres, cuando manifiesta **"**Somos anarquistas, somos libertarias, pero también somos mujeres y queremos hacer nuestra revolución. Queremos pegar tiros para poder exigir nuestra parte a la hora del reparto**"**.

<iframe src="https://www.youtube.com/embed/1NLfEnfKBmw" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Al igual que muchas de las cintas que reviven épocas específicas de la historia y de los conflictos internos y externos, donde hay lugar para diversas lecturas e interpretaciones desde la posición de cada espectador, la cinta del hoy fallecido director pasa a ser una referencia dentro de la lista de producciones que han pretendido, desde diferentes miradas, retomar temas que por años fueron censurados en ese país y hacia afuera.

Explotar la personalidad del grupo de mujeres libertarias, requirió de todo el carácter que Aranda encontró en un casting de primera línea encabezado por Ariadna Gil (María), Victoria Abril (Floren) y Ana Belén (Pilar), a quienes se suman Laura Mañá, Blanca Apiláñez, Claudia Gravy y Loles León, esta última nominada a los premios Goya del año como mejor actriz secundaria, y los actores Jorge Sanz, José Sancho, Miguel Bosé, Joan Crosas, Antonio Dechent, y Francisco Maestre .

La cinta fue nominada, además, por mejor vestuario, maquillaje, sonido y efectos especiales en los Goya. En el festival de Tokio, Vicente Aranda, logró por Libertarias especial del jurado, y en Colombia recibió el premio de la crítica en el Festival de Cine de Cartagena de 1997 .

##### Fuente: elpaís.com 

[![Libertarias](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Libertarias.jpg){.aligncenter .size-full .wp-image-9194 width="520" height="390"}](https://archivo.contagioradio.com/libertarias-vicente-aranda-contra-el-olvido/libertarias/)

Sinopsis:  
La Guerra Civil Española (1936-1939) vista por un grupo de milicianas anarquistas. Cuando el 18 de julio de 1936 el ejército español se sublevó contra el Gobierno de la República, un grupo de mujeres reivindicó su derecho a luchar en el frente. En nombre de la libertad, las mujeres libran su propia batalla para equipararse a los hombres en la lucha armada. (FILMAFFINITY)
