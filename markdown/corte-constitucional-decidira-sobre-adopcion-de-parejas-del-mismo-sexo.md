Title: Corte Constitucional decidirá sobre adopción de parejas del mismo sexo
Date: 2015-01-23 22:26
Author: CtgAdm
Category: closet, LGBTI
Slug: corte-constitucional-decidira-sobre-adopcion-de-parejas-del-mismo-sexo
Status: published

###### foto: yahoo.es 

En las siguientes semanas se espera la decisión de la **Corte Constitucional en torno a la** adopción por parte de parejas del mismo sexo. Muchas son las expectativas que se han tejido frente al caso, dado que la misma **Corte falló a favor de una pareja de lesbianas una acción de tutela** que podría ser base para la discusión, aunque el caso que la Corte está estudiando actualmente es una demanda de inconstitucionalidad.

Según la organización **“Colombia Diversa”** la adopción de parejas de mismo sexo se puede dar si una sola persona es la que solicita la adopción, pues según un fallo de la “Corte Constitucional, con ponencia del magistrado Pretelt, dijo que l**a orientación sexual no es un impedimento para adoptar como soltero**” cita la organización.

Además las parejas del mismo sexo pueden adoptar al hijo biológico de una de las dos personas. Según la **Sentencia SU-617 de 2014** en que se establece que "la orientación sexual o el sexo de la pareja no pueden ser un impedimento en el **proceso administrativo de adopción consentida**".

Aunque hay varios puntos a favor de la adopción también están las pujas políticas detrás del tema, una de las partes la iglesia católica, el procurador y sectores conservadores del congreso de la república.
