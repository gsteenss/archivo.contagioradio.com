Title: Llega la primera versión del Festival de Educación Popular
Date: 2016-12-01 15:31
Category: Educación, Nacional
Tags: educación popular en Colombia, Movimiento estudiantil Colombia
Slug: llega-la-primera-version-del-festival-de-educacion-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Festival-de-educación-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Centro Cultural Chipacuy] 

###### [1 Dic. 2016] 

Del 2 al 4 de diciembre se llevará  a cabo en Bogotá, el primer Festival de Educación Popular, una iniciativa que surge desde el Centro Cultural Chipacuy, ubicado en la localidad de Suba y que **pretende generar un espacio de reflexión en torno a la educación popular en la ciudad.**

Para Crístian Castaño, representante legal del Centro Cultural Chipacuy, la importancia de la pedagogía y educación popular se encuentra en **“crear o facilitar la construcción de nuevos sujetos históricos**”. Además el Festival también busca bridar herramientas para que se pueda construir un nuevo país, “La idea es que los sujetos que han sufrido la discriminación, la exclusión pero también la guerra, sean capaces por si mismos de construir de manera autónoma nuevos caminos a través de la educación” agrego Castaño.

El evento tendrá dos paneles en los que participarán invitados como **Lola Cendales, German Mariño, Alfonso Torres y Marco Raúl Mejía**, quienes interactuarán sobre sus experiencias en torno a la Educación Popular. A su vez se tratarán temas como la didáctica de la educación popular y la emancipación y un contexto de cómo se encuentra este campo de acción en Latinoamérica.

Se espera que al escenario también lleguen diferentes procesos de educación popular que tienen procesos en Bogotá. El evento se desarrollará en la Casa Cultural Chipacuy,      que queda ubicad en la dirección calle 146 b \#128-11, barrio Suba Compartir, la entrada es totalmente gratis. Le puede interesar: ["Hay que trabajar para la esperanza y la utopía: Lola Cendales"](https://archivo.contagioradio.com/hay-que-trabajar-para-la-esperanza-y-la-utopia-lola-cendales/)

<iframe id="audio_14391802" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14391802_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
