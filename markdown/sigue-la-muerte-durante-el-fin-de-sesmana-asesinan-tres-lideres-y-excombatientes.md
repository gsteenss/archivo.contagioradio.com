Title: Sigue la muerte, durante el fin de semana asesinan tres líderes y excombatientes
Date: 2020-06-16 22:59
Author: PracticasCR
Category: Nacional
Slug: sigue-la-muerte-durante-el-fin-de-sesmana-asesinan-tres-lideres-y-excombatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Universal-Rights-Group-.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Ni-un-muerto-más.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ATL

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana, continuando la preocupante ola de violencia contra líderes, lideresas, personas defensoras de DD.HH. y excombatientes firmantes del Acuerdo de Paz,  **se registraron tres asesinatos en los territorios de Antioquia, Cauca y Norte de Santander.** (Lea también: [Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El sábado en horas de la mañana **fue asesinado Mauricio Antonio Uribe miembro de la Junta de Acción Comunal (JAC) de la vereda El Carmen del municipio de Remedios, Antioquia.** El homicidio ocurrió en la vereda Rancho Quemado, ubicada a unas cuatro horas del casco urbano, donde se encontró el cuerpo sin vida de Mauricio con varios impactos de arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mismo sábado en horas de la tarde, **autoridades indígenas del resguardo Huellas en Caloto, Cauca denunciaron el** **asesinato de Jesús Antonio Rivera de 32 años de edad.** El comunero fue asesinado mediante disparos de armas de fuego sobre la 1:00 p.m.; luego de lo cual, la Guardia Indígena emprendió labores de búsqueda para ubicar a los responsables del homicidio dando captura a siete hombres fuertemente armados que ya están a disposición de la jurisdicción indígena local. (Lea también: [167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A estos dos hechos violentos, se sumó el día domingo, el **asesinato del excombatiente y firmante del Acuerdo de Paz, Mario Téllez Restrepo** a quien desconocidos le dispararon en cercanías de su parcela en el municipio de Tibú, Norte de Santander, **dejando a tres de sus hijos menores de edad huérfanos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El partido FARC denunció que con él ya son **200 los excombatientes asesinados desde la firma del Acuerdo.** (Le puede interesar: [Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:heading {"textColor":"vivid-cyan-blue"} -->

[Líderes  y excombatientes en alerta máxima]{.has-inline-color .has-vivid-cyan-blue-color} {#líderes-y-excombatientes-en-alerta-máxima .has-vivid-cyan-blue-color .has-text-color}
------------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estos crímenes confirman la alarmante tendencia de asesinatos que se viene presentando durante el 2020 donde se contabilizan al menos [160 muertes](http://www.indepaz.org.co/paz-al-liderazgo-social/) entre líderes, lideresas, personas defensoras de DD.HH. y excombatientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mes de junio no es la excepción, cabe recordar que **solo en el primer día del presente mes se registraron tres asesinatos en contra de estos liderazgos y la primera semana cerró con la desalentadora cifra promedio de una persona asesinada por día.** (Lea también: [Liderazgos sociales en alerta máxima. En lo corrido de Junio van 7 asesinatos](https://archivo.contagioradio.com/liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
