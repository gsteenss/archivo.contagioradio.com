Title: Tres comuneros indígenas fueron asesinados en Santander de Quilichao, Cauca
Date: 2020-08-02 22:05
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de indígenas, Santander de Quilichao
Slug: tres-comuneros-indigenas-fueron-asesinados-en-santander-de-quilichao-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/asesinato-de-lider-indigena-en-tacueyó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Comunidades Indígenas/ CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El **Tejido Defensa de la Vida y los DDHH de la Asociación de Cabildos Indígenas del Cauca** (ACIN) denunció que en la madrugada de este 2 de agosto fueron asesinados tres comuneros pertenecientes al territorio Canoas en Santander de Quilichao. Según el más reciente informe del Instituto de Estudios para el Desarrollo y la Paz han ocurrido 250 homicidios contra los pueblos ancestrales desde la firma del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La ACIN señala que los tres comuneros fueron atacados en la vereda California en un establecimiento público cuando, según testigos, hombres armados llegaron hasta el lugar de los hechos, preguntaron por las víctimas y dispararon sus armas ocasionando la muerte de 3 de ellos. [(Lea también: Tras atentando en Jamundí, comunidades indígenas reiteran que la paz no ha llegado a los territorios)](https://archivo.contagioradio.com/tras-atentando-en-jamundi-comunidades-indigenas-reiteran-que-la-paz-no-ha-llegado-a-los-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Autoridades y guardia indígena han acompañado el proceso de levantamiento de los cuerpos que corresponden a **Brayan Stiven Guetio Ipia de 18 años, Lizardo Collazos Findo de 23 años y Manuel David Larrahondo de 24 año**s quien aunque no se encuentra en el censo de la comunidad sí es reconocido como habitante del territorio. [(Lea también: El racismo permanece en las instituciones del Estado: CRIC)](https://archivo.contagioradio.com/el-racismo-permanece-en-las-instituciones-del-estado-cric/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Continúa violencia contra comunidades indígenas del Cauca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Segú el informe semestral de DD.HH. de la [ACIN](https://twitter.com/ACIN_Cauca) para 2020 la situación se ha vuelto más compleja, evidenciando un cremento sustancial de afectaciones en todas las variables, para 2019 los primeros seis meses del año concluyeron con 21 asesinatos, para 2020 en la misma fecha el registro de homicidios se ubicó en 45. [(167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz)](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que departamentos como el Cauca pese a su alta militarización viven en una continua situación de disputa entre grupos armados por el narcotráfico y los corredores que durante años han servido a grupos criminales para sus propósitos, corredores que son defendidos por las comunidades indígenas en medio de su labor de control territorial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Indepaz además del **Cauca; Nariño, Valle del Cauca Caquetá y Putumayo** son otras de las regiones donde más se atenta contra la vida de los integrantes de las comunidades indígenas..

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
