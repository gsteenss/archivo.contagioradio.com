Title: Defensores de DDHH fueron hostigados y perseguidos en Bogotá
Date: 2017-02-03 15:44
Category: DDHH, Nacional
Tags: Amenazas, cpdh, Justicia y Paz, víctimas
Slug: defensores-ddhh-asedio-bogota
Status: published

###### [Foto: Archivo Particular] 

###### [3 Ene. 2017] 

[Los ]Defensores de Derechos Humanos **Erika Gómez  y Danilo Rueda,** denunciaron este jueves haber sido **víctimas de hostigamiento y persecución en la ciudad de Bogotá** por parte de una mujer y un hombre vestidos de civil. En un primer momento una mujer hizo presencia para escuchar lo que hablaban en una reunión que sostenían y horas más tarde un hombre emprendió el seguimiento tras la defensora.

En comunicación con Contagio Radio, Erika Gómez, actual Secretaria Ejecutiva del Comité por la Defensa de los Derechos Humanos – CPDH-, aseguró que “**este hecho pretendió limitar su labor legítima de atención y apoyo a los derechos de las víctimas y la construcción de la paz**”. Le puede interesar: [Reconocimiento al CPDH por sus Escuelas de Paz en clave de justicia transicional](https://archivo.contagioradio.com/reconocimiento-al-cpdh-por-sus-escuelas-de-paz-en-clave-de-justicia-transicional/)

Según la defensora, los hechos iniciaron hacia las 4:45 p.m de este jueves cuando se reunió con el integrante de la Comisión de Justicia y Paz, Danilo Rueda al norte de la capital “**estuvimos expuestos a una situación de vigilancia y seguimiento por parte de una mujer** que después de unos metros logra darse cuenta que el defensor Danilo Rueda cuenta con esquema de protección  y desiste del seguimiento”. Le puede interesar: [Roban información de víctimas en sede de la Comisión de Justicia y Paz](https://archivo.contagioradio.com/roban-informacion-de-victimas-y-propuestas-de-paz-en-la-sede-de-la-comision-de-justicia-y-paz/)

Erika asegura que, luego de sostener una reunión con unas víctimas, se despidió del defensor de derechos humanos y al esperar un vehículo que la movilizaría hacia otro punto de la ciudad logra percatarse de la presencia de un hombre que la perseguía.

“Yo creo que **durante un trayecto de 6 cuadras y cerca de 15 minutos un hombre me siguió**, por eso me tuve que ver obligada a ingresar a un centro comercial con el fin de evadir la persecución por parte de este sujeto” y agrega “era fácil quitarme el bolso de  un raponaso (...) o hubieran podido decirme “piropos”, pero en realidad no pasó eso. Confirme tiempo después que era un seguimiento por el accionar del hombre y decidí entrar al CC”.

El CPDH ya instauró las denuncias pertinentes para que se esclarezca la situación y aunque temen que “algunos funcionarios de Fiscalía usen hipótesis para no desviar y no prestar atención a las denuncias de líderes sociales y defensores de DH”.

Y asegura que la Unidad Nacional de Protección - UNP - ya estaba realizando un estudio de la situación pero hasta el momento no han tenido respuesta “L**a UNP ha dicho que este viernes se dará solución al esquema de seguridad** pero lamentablemente deben suceder estos hechos para que den respuesta”.

Según la organización Somos Defensores en 2016 fueron asesinados 70 líderes y defensores de derechos humanos, hubo 279 amenazas y 28 atentados. Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

<iframe id="audio_16816546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16816546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
