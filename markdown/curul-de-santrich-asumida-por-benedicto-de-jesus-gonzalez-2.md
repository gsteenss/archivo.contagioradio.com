Title: Curul de Santrich es asumida por Benedicto de Jesús González
Date: 2018-12-13 13:07
Author: AdminContagio
Category: Nacional, Política
Tags: Benedicto De Jesús González, Jesús Santrich
Slug: curul-de-santrich-asumida-por-benedicto-de-jesus-gonzalez-2
Status: published

###### Foto: Contagio Radio 

###### 13 Nov 2018 

[Por orden del Tribunal Administrativo de Cundinamarca, la curul de Zeuxis Pausias Hernández, "Jesús Santrich", será ocupada de forma transitoria en el Congreso en representación del partido FARC, por **Benedicto De Jesús González Montenegro,** **"Alirio Córdoba"**, segundo en la lista a la Cámara por parte del movimiento político en el Atlántico.  
  
A través de una tutela presentada por el partido, sustentaron que la detención de Santrich no ha permitido completar la cinco curules negociadas como parte del Acuerdo de paz, razón que motivó la acción legal del movimiento político, como lo expreso en su cuenta de twitter el congresista **Pablo Catatumbo.**]

[Ante tal petición, el Tribunal aceptó la tutela considerando que la pérdida de investidura únicamente soluciona el asunto disciplinario de Santrich más no resuelve el derecho a la representación política que tiene **FARC** en el Congreso de la República.   
  
En consecuencia, el Tribunal ordenó a la Mesa Directiva de la Cámara de Representantes, en el lapso de 48 horas, llamar al segundo candidato en la región Caribe, Benedicto De Jesús González para tomar posesión. “Creemos que lo nuestro será un paso transitorio hasta que el titular de esta curul asuma a finales del mes de enero cuando estará en libertad y él podrá hacer uso de su derecho político", afirmó González a través de redes sociales.]

[González nació en Mompóx, Bolívar, estudió Derecho en la Universidad del Atlántico, hizo parte de la Juventud Comunista y del Partido Comunista Colombiano e** ingresó a las Farc en 2001 hasta el proceso de dejación de armas del exgrupo guerrillero en 2017**. Fue incluido en la lista a la Cámara de Representantes para representar a su partido en en el Atlántico.]

[**Un espaldarazo  a González**  
  
A través de un comunicado, la Fuerza Alternativa Revolucionaria del Común, expresó su respaldo a la decisión del Tribunal Administrativo de Cundinamarca de posesionar a González para que ocupe la curul mientras se resuelve el proceso de Santrich en el **Consejo de Estado**, “Sabemos que él hará un papel digno en la Cámara de Representantes como lo haría también Jesús Santrich, a quien reiteramos nuestro apoyo irrestricto” indicó el partido político.   
  
"Solo esperamos que las directivas de la Cámara hagan cumplir de manera eficiente y prontamente este fallo del Tribunal Superior de Cundinamarca" expresó Catatumbo, responsable de presentar la tutela en la que se pedía proteger el derecho a la participación política del partido FARC en el Congreso.  
  
]**Duque, firme con la extradición**[  
  
Por su parte, el presidente Duque ratificó que **está dispuesto a firmar la extradición de Santrich,** pues asegura que las dudas que existen en la JEP sobre el caso en particular, pondrían en riesgo la cooperación judicial que ha existido entre Colombia y los Estados Unidos a lo largo de la historia.   
  
Duque manifestó que firmará la extradición una vez la Corte Suprema de Justicia la apruebe si es remitida por la JEP, por tal motivo aseguró que es necesario que se le dé celeridad a la investigación que busca demostrar que los delitos por los cuales se detuvo a Santrich habrían sido cometidos después de la firma de los acuerdos.[(Le puede interesar ¿Por qué se suspendió la audiencia contra Jesús Santrich? )](https://archivo.contagioradio.com/por-que-se-suspendio-la-audiencia-contra-jesus-santrich/)]

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
