Title: La injuria es un juego de inequidades
Date: 2015-07-17 07:00
Category: Opinion, Schnaida
Tags: Código Penal, Difamación, Fariñoas Matoni, Libertad de expresión, Reporteros Sin Fronteras
Slug: la-injuria-es-un-juego-de-inequidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Informe-Anual-2014-FLIP-SQ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [Foto: FLIP] 

#### [**[[Ian Schnaida](https://archivo.contagioradio.com/ian-schnaida/) - [[~~@~~**IanSchnaida**]<small class="time"></small>**] 

###### [17 Jul 2015] 

Un proceso informativo sano y constante le permite a la sociedad tomar posiciones y decisiones frente a las cuestiones que se desarrollan en su entorno social, cultural y gubernamental. Como elemento clave de este proceso está la libertad de información, pilar principal de una democracia como la que pretende Colombia, y de dicho deber ser del mecanismo informativo; sin embargo, este proceso, además de garantizar la difusión de la información que los gobernantes tienen para el pueblo, debe mostrar aspectos que pueden llegar a molestar u ofender al pueblo y su gobierno.

Este proceso tiene que garantizar que en el devenir propio de su labor se preserve el derecho a la intimidad y al buen nombre de las personas, elementos que se ven en juego fácilmente en la búsqueda de la verdad.

La línea divisoria entre la libertad de expresión y los posibles daños que se pueden causar a una persona en cuanto a su imagen es muy delgada, ya que se tienen en la balanza dos grandes derechos de la persona. Con los artículos 220 y 221 del Código Penal se tipifican los delitos de injuria y calumnia. Aunque allí el concepto queda tan amplio y tan poco conciso que permite perseguir todo tipo de ejercicio periodístico de opinión, como la crítica política, ya que no se establece de manera clara la conducta que se reprocha.

Se define la injuria como el atentado contra el honor o la buena fama de cualquier persona y la calumnia como la imputación falsa de cualquier individuo. ¿Quién determina entonces esa diferencia? ¿Cómo y con base en qué?

Pese a la ‘buena fe’ de dichos artículos, estos van en contravía de La Constitución colombiana, la cual reza en su artículo 20: “Todo individuo tiene derecho a la libertad de opinión y de expresión; este derecho incluye el de no ser molestado a causa de sus opiniones, el de investigar, recibir informaciones y opiniones, difundirlas, sin limitación de fronteras, por cualquier medio de expresión”.

Fariñoas Matoni apunta que “es imprescindible distinguir la noción de intimidad de la de honor -lo que nosotros conocemos como ‘buen nombre’-, para lo cual hay que diferenciar la esfera privada, dominada por la idea de secreto, de la esfera pública, dominada por la idea de difamación. La anterior distinción adquiere especial relevancia en cuanto a la veracidad o falsedad de lo afirmado; porque, mientras en el caso de la intimidad y la privacidad, la veracidad de la información comunicada no justifica la divulgación de la misma, en el caso del derecho al honor la veracidad opera, en principio, como circunstancia legitimadora de lo aseverado".

Existe una inviolabilidad fundamental establecida en la Constitución que tiene que ver con la vida privada de las personas como son el ámbito afectivo y sexual, aspectos en los que el periodista no está legitimado para intervenir, y cualquier intromisión puede ser castigada por la ley; pero si hacemos referencia a actos públicos, discursos o conferencias el periodista puede tomar toda la información que requiera ya que esta persona, por el solo hecho de estar expuesto a situaciones que se consideran públicas, destila información que puede usarse según la necesidad del interesado en pleno ejercicio de su deber de informar.

Países como Alemania, Argentina, Brasil y Chile tipifican los delitos de injuria y calumnia de manera similar a la legislación colombiana. Mientras que en otros como México, los delitos contra el Honor “Difamación, Calumnia e Injurias” entraron a despenalizarse a partir del 13 de abril de 2007 a nivel federal, y pese a que a la fecha tal despenalización no ha tenido los alcances necesarios para su aplicación, ya que de los 31 Estados y el Distrito Federal, algunos todavía siguen regulando en sus disposiciones tales delitos, es un proceso constante en el cual se unen cada vez más.

En el momento en que se realiza un proceso legal por injuria y calumnia, el demandante, digamos que es un político influyente y respetado, debe demostrar con hechos claros y concisos dónde se entró en difamación de su nombre y/o dónde se le faltó a la verdad; el proceso no se puede resumir en la alteración y el respaldo político y social de la figura demandante para desacreditar lo que el periodista dice, al igual que este no puede esperar a que con solo su palabra un juzgado le dé la razón en su ejercicio periodístico.

Es un juego de poderes que muchas veces gana la mano con más dinero; pero donde debería primar el bienestar público que puede hallarse comprometida por la intervención de intereses personales.

La intimidad debe ser velada por los servidores públicos, ya que si bien los periodistas tenemos una función social, no tenemos una misión social. Los periodistas no tenemos que proteger la intimidad de las personas, lo nuestro es todo lo contrario, revelar los secretos personales que dan fe de actos indebidos que comprometen el interés y el bienestar general; de modo que recae sobre la ley la responsabilidad de velar por el sano ejercicio y actuar de las partes.

Tristemente, aquí ese respaldo político, legal y judicial no se ve. Quienes están en obligación de prestarle seguridad al periodista para el sano desarrollo de su profesión, lo abandonan y lo dejan expuesto a esos grandes mecanismos ilegales que matan por deporte y que pagan por muerto.

Según Reporteros Sin Fronteras, en México han asesinado a 81 periodistas entre enero del 2000 y septiembre del 2014. Sigue Colombia con 56 casos; Brasil, con 38 y Honduras con 27. Cifras escabrosas que hablan por sí solas de la corrupción en la que viven sus países y del poco interés que tienen en dar a conocer los 'buenos manejos' a las ciudadanías. Indignante. Mucho.
