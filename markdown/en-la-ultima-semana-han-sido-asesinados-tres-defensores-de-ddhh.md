Title: En lo corrido del 2015 han sido asesinados 55 defensores de DDHH
Date: 2015-11-18 13:04
Category: DDHH, Nacional
Tags: colombia, Defensores Derechos Humanos, Derechos Humanos, proceso de paz, Radio derechos Humanos, Somos defensores
Slug: en-la-ultima-semana-han-sido-asesinados-tres-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Defensores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:minuto30.com 

<iframe src="http://www.ivoox.com/player_ek_9432494_2_1.html?data=mpmglJmdeI6ZmKiakpyJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfzdSYxdTWtsrY0JDRx9GPdpGllpDVw9OPt8rY0JDO1crXrc%2FVxdTgjZqZb8XZx8rb1dTWqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Guevara, Somos defensores] 

###### [18 Nov 2015]

Este 18 y 19 de noviembre se realiza en **Bogotá el primer Encuentro Internacional de Protección a Defensores de DDHH en las Américas con miras a identificar y evaluar e**strategias en materia de protección a defensores y defensoras en el marco de un posible escenario de **posconflicto.**

Al inicio del Encuentro se puso en conocimiento la situación de los líderes y lideresas sociales en Colombia. Allí se denunció que en la última semana **3 defensores han sido asesinados y en lo corrido del año se han registrado un total de 55,** todos ellos en medio de contextos rurales donde se desarrolla el conflicto armado, donde existe extracción minera o hay problemas alrededor de la restitución de tierras, como lo explica Carlos Guevara, coordinador del programa Somos Defensores.

En el evento se** **presentan experiencias de Argentina, Guatemala, El Salvador, Burundi y Filipinas, con el objetivo de aportar y fortalecer las medidas de protección para el posconflicto**,** ya que actualmente "la situación sigue siendo compleja  y **encontramos un silencio de parte del gobierno**”, dice Guevara, quien agrega que el mayor problema continúa siendo la impunidad.

Este encuentro cuenta con el acompañamiento de la Oficina del Alto Comisionado de Derechos Humanos en Colombia, la embajada de Suecia, Suiza y otras organizaciones internacionales. Las conclusiones y la ruta de trabajo para fortalecer la medidas de protección de defensores de DDHH serán enviadas a los entes gubernamentales una vez finalice el evento.

Según Guevara, las agresiones por parte de la guerrilla han disminuido, pero preocupa que mientras avanza el proceso de paz, aumentan los ataques y la persecución por parte de grupos neoparamilitares e incluso de actores estatales en contra los defensores y defensoras de DDHH.

**Es reiterado el llamado a que "no se de la espalda a los defensores de Derechos Humanos**, ya que son piezas fundamentales en la aplicación de los acuerdos de paz en los territorios", concluye el coordinador de Somos Defensores.
