Title: Por malas condiciones laborales escoltas de la UNP cesarían actividades
Date: 2019-01-28 16:51
Author: AdminContagio
Category: Entrevistas, Movilización
Tags: asamblea permanente, Escolta, paro, UNP
Slug: escoltas-unp
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-28-at-4.49.53-PM-e1548712266586-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Ene 2019] 

Los diferentes movimientos sindicales que hacen parte de la **Unidad Nacional de Protección (UNP)** han anunciado que de no establecer un canal de comunicación con las directivas, que permita discutir los problemas que enfrenta la Institución, **cerca del 50% de los escoltas entrarían en cese de actividades este martes 29 de enero**. (Le puede interesar: ["Silencian la voz de denuncia de Samuel Gallo, líder comunitario en Antioquia"](https://archivo.contagioradio.com/samuel-gallo-lider/))

**Wilsón Devia,** presidente del sindicato de la UNP y uniones temporales, explicó que los escoltas que trabajan en la Unidad tienen contratos de tercerización a través de empresas de seguridad; esto significa que las **4.500** personas encargadas de cuidar la vida de políticos, defensores de derechos humanos y líderes sociales están subcontratados. Por esa razón, durante este lunes se citó una asamblea informativa frente a la Entidad para discutir los problemas de la misma.

### **Los escoltas no tienen garantías para cuidar la vida de sus protegidos** 

El primer problema que identifican los escoltas tiene que ver con el prepliego para contratación emitido por la Unidad en noviembre de 2018; en esa ocasión, se esperaba hacer la subcontratación de personal por 2 años, pero la Institución afirmó que no tenía recursos garantizados para operar todo el año. Entonces decidió emitir un pliego similar, pero que redujo la contratación a 6 meses y permite usar armas más viejas, "abriendo espacio a empresas que no tiene la experiencia en el sector de seguridad".

A esta situación se suma el manejo que hace la Unidad de los viáticos de los escoltas, puesto que la UNP tercerizó el pago de los mismos a través de una Fiducia; pero gracias a dicha figura le descuenta a la Unidad como a los escoltas un monto por cada transacción. Adicionalmente no existe un protocolo técnico para determinar cuando debe o no viajar un escolta, ni el monto o número de días que se pagan por el viaje.

Ante estas situaciones, los escoltas deben poner parte de su dinero para poder desplazarse junto a su protegido, o permitir que se desplace solo, **generando un riesgo para el mismo y exponiéndolo a que sea asesinado en cualquier parte del país.** (Le puede interesar: ["CIDH presenta recomendaciones para proteger los líderes sociales"](https://archivo.contagioradio.com/cidh-presenta-recomendaciones-proteger-los-lideres-sociales/))

Por estas razones, y si la directiva de la Unidad no entabla un proceso de dialogo que permita resolver de fondo las problemáticas de la entidad, cerca del 50% de los escoltas que la componen entrarían en cese de actividades. Entre tanto, Pablo Elías González, director de la Institución ha insistido en que el cese es ilegal, y ha llamado a todos los protegidos por la UNP a informar sobre el cese de actividades que presenten sus esquemas de protección.

> Creo que la [@UNPColombia](https://twitter.com/UNPColombia?ref_src=twsrc%5Etfw) debe hacer respetar los derechos laborales por parte de las empresas que subcontrata para prestar el servicio. [@cutcolombia](https://twitter.com/cutcolombia?ref_src=twsrc%5Etfw) [@SINPROSEGN](https://twitter.com/SINPROSEGN?ref_src=twsrc%5Etfw) [@sinprosegbogota](https://twitter.com/sinprosegbogota?ref_src=twsrc%5Etfw) [pic.twitter.com/V973DIHP2Q](https://t.co/V973DIHP2Q)
>
> — Alirio Uribe Muñoz (@AlirioUribeMuoz) [28 de enero de 2019](https://twitter.com/AlirioUribeMuoz/status/1089870445548060674?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_31889337" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31889337_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
