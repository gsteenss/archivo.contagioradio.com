Title: 8 comuneros han sido asesinados en proceso de liberación de la madre tierra en Cauca
Date: 2017-05-10 14:56
Category: DDHH, Entrevistas
Tags: Acaparamiento de tierras, Cauca, Comunero, INCAUCA, indígenas
Slug: 8-comuneros-han-sido-asesinados-en-proceso-de-liberacion-de-la-madre-tierra-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/liberacion-madre-tierra-_-policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACIN] 

###### [09 May. 2017] 

La comunidad indígena del Norte de Cauca hoy se encuentra de luto por **el asesinato de uno de sus comuneros por parte del Escuadrón Móvil Antidisturbios  (ESMAD)** este 9 de mayo, mientras hacían labores de liberación de la madre tierra en la Hacienda Miraflores en Corinto, proceso que deja desde el 2009 a la fecha 8 comuneros asesinados.

Los hechos iniciaron hacia las 11 de la mañana, cuando un camión, un bus y una tanqueta de la Policía Nacional llegaron al lugar y desde uno de los carros comenzó una ráfaga de disparos, **alcanzando al comunero Daniel Castro de 17 años, quien falleció cuando era trasladado al hospital** e hiriendo a Pedro García Leal periodista, quien se encuentra en una clínica en Cali.

Jairo Tamayo, consejero de la comunidad indígena aseguró que “los carros iban de Miranda hacia Corinto y el ESMAD estaba haciendo la persecución a los comuneros, ellos entran a la finca y simplemente sin decir nada comienzan a disparar (…) **los disparos salen del camión en el que estaba el ESMAD.** La Policía Judicial y los soldados estaban cerca”.

### **Liberación de la madre tierra** 

Durante este 8 y 9 de mayo, más de 4500 indígenas pertenecientes a la Asociación de Cabildos Indígenas del Norte del Cauca - ACIN- desarrollaron una de las acciones del proceso de **liberación de la madre tierra en la Hacienda Miraflores, que tiene unas 20 mil hectáreas, la mayoría con siembras de caña.** Según la ACIN, esta minga se da porque las tierras pertenecen ancestralmente a ellos y deberían ser tituladas.

“Lo que decide la comunidad es volver a liberar la madre tierra, porque **son tierras ancestrales que le pertenecieron a todos los indígenas y en este caso al pueblo Nasa”** recalcó Tamayo. Le puede interesar: [Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca](https://archivo.contagioradio.com/continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca/)

Además, las comunidades han afirmado que para poder subsistir **la cantidad mínima de tierra que una familia necesitaría son 4 hectáreas**, es decir que el gobierno está en deuda con cerca de 144 mil hectáreas que son las exigidas en el proceso de liberación.

“Hemos dicho que por cada comunero que fallezca en este proceso de liberación de la madre tierra son 15 mil hectáreas, en este momento si sumamos eso más lo que estamos solicitando pues el compromiso que tiene el Gobierno va aumentando”.

### **Acaparamiento de tierras en Cauca ** 

Debido al acaparamiento de tierras, el Cauca es el segundo departamento más pobre de Colombia. Según cifras de la ACIN **el 56% de los niños y niñas sufren de algún grado de desnutrición o padecen hambre**, y cerca de 6 mil familias de las 25 mil que están en el territorio no tienen tierra o como subsistir.

Además muchas de las tierras han sido entregadas a  empresas que tienen sus proyectos agroindustriales "algunas fincas han sido compradas por INCODER, antes INCORA, y en **este año con la Agencia Nacional de Tierras no hemos podido adquirir tierras** en el norte de Cauca" manifiesta Tamayo. Le puede interesar: [Trabajadores de INCAUCA serían responsables del asesinato de comunero indígena](https://archivo.contagioradio.com/incauca-responsables-del-asesinato-de-comunero-indigena-del-cric/)

Del total de las tierras del Cauca, **solo el 12% está en manos de los resguardos indígenas** y son utilizadas para la producción pecuaria y agrícola. El 82% de las tierras tienen vocación forestal.

Otros territorios como la **Hacienda Miraflores ha sido arrendada por 15 años a INCAUCA** "lo que uno mira hoy es que prácticamente los que administran, quitan y ponen a las empresas. Yo diría uno ya no es el dueño de la finca sino que son estos administradores que se han asociado con los empresarios y agro-industriales" manifiesta Tamayo.

### **1200 acuerdos incumplidos del gobierno a los indígenas** 

Desde 1971 los indígenas del Cauca están realizando la liberación de la madre tierra a raíz del acaparamiento existente en el departamento, que según cifras del Instituto Agustín Codazzi  es uno de los lugares con las cifras más altas por este fenómeno.

Ante estas situaciones **las comunidades aseguran que han logrado hacer 1200 acuerdos en relación con el acceso al territorio** y la garantía de derechos con diversos Gobiernos, de los cuales ninguno se ha cumplido, pese a las demandas internacionales que han sido interpuestas.

Dice Tamayo, que el fin de semana pasado se reunieron con el vicepresidente Óscar Naranjo en donde plantearon la posición política del movimiento indígena sobre la liberación de la madre tierra. Le puede interesra: [La resistencia del pueblo Nasa en "A sangre y Tierra"](https://archivo.contagioradio.com/la-resistencia-del-pueblo-nasa-en-a-sangre-y-tierra/)

“**El Vicepresidente fue claro, él dijo que no se responsabilizaba por la Minga del 8 y 9 acá en Corinto** y de hecho ayer lo demostró. Lo que estamos viendo es que no hay voluntad del Gobierno referente a la reclamación de los derechos de los pueblos indígenas” relató Tamayo.

### **Heridos del ESMAD** 

Según cifras de la Policía Nacional, dos uniformados habrían resultado heridos en medio de los hechos, sin embargo, Tamayo manifiesta que **“eso es lo que sale en los medios de comunicación, pero uno no sabe porque a veces se dicen cosas** (…) por ejemplo en las noticias dicen que estamos con el ELN o que nos infiltraron y eso es una mala información que nos ponen en peligro”.

Ante esta situación, la Guardia Indígena se encuentra en alerta por cualquier hecho que pueda presentarse nuevamente, dado que seguirán haciendo liberación de la madre tierra y “el ESMAD, la Fuerza Pública siempre llega a eso de las 10 u 11 de la mañana” relató Tamayo. Le puede interesar: [Indígenas de Cauca denuncian hostigamientos por combates entre el ELN y Policía](https://archivo.contagioradio.com/indigenas-de-cauca-denuncian-hostigamientos-por-combates-entre-el-eln-y-policia/)

**Las comunidades indígenas se han declarado en Asamblea Permanente** por esta situación y han dicho que realizarán una audiencia pública para dar cuenta de la graves situación que atraviesa el pueblo Nasa en el Cauca.

<iframe id="audio_18616861" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18616861_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
