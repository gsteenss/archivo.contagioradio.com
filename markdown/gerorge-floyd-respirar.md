Title: "No puedo respirar”
Date: 2020-06-01 08:57
Author: CtgAdm
Category: Camilo, Opinion
Slug: gerorge-floyd-respirar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/george-floyd-respirar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

[Camilo De Las Casas](https://archivo.contagioradio.com/categoria/opinion/camilo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“No puedo respirar, (I Cant breathe), no puedo respirar, por favor señor” expresaba George Floyd al policía Derek Chaivon y los otros tres que le acompañaban. El video demuestra la ceguera, la incapacidad de escuchar, de percibir la tragedia de la muerte causada por la “autoridad”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No puedo respirar es el grito de E Much, el pintor noruego. Es la dimensión de la vida biológica en su etapa final por la fuerza en George. No puedo respirar. Es el grito existencial a la humanidad ante el neofacismo, las miles de fobias y métodos de racismo y discriminación que sostienen la exclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta la imagen real de la muerte en la vida y la vida que se desata entre la muerte, en este caso, la dolorosa muerte violenta a la vista de miles. Es el retrato de la quizás olvidada María del Pilar, enfrente de su hijo, de vecinos silenciados o temerosos,  a manos del sicario en Córdoba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Son los casi dos cientos firmantes de la paz, asesinados en estado de indefensión. Son los más de 800 líderes asesinados desde la firma del Acuerdo de Paz, en noviembre de 2016- Los 25 asesinados en tiempo de pandemia, los dos últimos indígenas del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como es George son las comunidades étnicas (indígenas, negras, campesinas) violentadas, masacradas, desplazadas y con despojos territoriales que ven durante el aislamiento naranja del Centro Democrático su criminalización, su negación y la continuidad de operaciones de despojo y definición del uso del suelo y de su subsuelo por empresarios sin contemplación alguna en medio de la pandemia. Es heroico respirar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En George está el grito silenciado de la Vida por la fuerza. La imposición de ideas. El autoritarismo a flor de piel a través del uso desbordado de la fuerza. La imagen expresa el sostenimiento de la exclusión histórica y estructural. La fuerza a nombre de la democracia es la expresión de la introyección de prejuicios, de precomprensiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la escena se desatan abiertamente los odios inoculados sobre la mente y el cuerpo del otro por su color de piel. Queda clara la necesidad de mantener la desigualdad o de imponer el blanqueamiento con el ejercicio de poder represivo  o de controlar represivamente para aleccionar. No todos somos iguales en dignidad, no tenemos igualdad de derechos. La desigualdad étnica y social (de clase) es el orden natural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El escenario  represivo oculta el prejuicio justificando el supuesto uso de un billete falso, en realidad una cochina mentira. En cualquier caso, la banalización del derecho a la vida subordinado al papel moneda. Nadie debería ser ejecutado por ningún Estado ni por autoridad alguna por un billete. El sentido dado es que el billete, el estiércol del demonio justifica el asesinato de presunto villano, malandro, un negro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Derek es la expresión del subordinado, del obediente, del adoctrinado, según, revelan las imágenes. En la formación desde quizás, la familia esta inoculado el racismo. ¿Qué educación familiar y formal crearon una mentalidad que sostiene unos valores para asesinar al otro en razón de su raza? ¿Quién forman a las fuerzas policiales allá y acá con esa mentalidad que no es otra que la de enemigos internos?.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy circulas decenas de videos por las redes sociales en que se demuestra que este es uno más de los abusos policiales en los Estados Unidos. La prácticas de selección de los sospechosos, de destrucción del cuerpo al que se enseña a odiar, de negación de la palabra del que per se es “delincuente” está en el implícito de los manuales e instrucciones policiales son los marcos doctrinales maniqueos en la sociedad occidental democrática, capitalista, cristiana. Es cultura judeo cristiana de unos buenos que se aseguran ante unos malos. Las doctrinas de enemigos internos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Son millones los George. No pueden respirar. En Colombia son asfixiados los líderes y líderesas sociales rurales y los firmantes de la paz. La cocción a fuego lento de un campamento nazi que extermina a lo largo y ancho del país con la muerte violenta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

O esa cocción a fuego lento en el campamento nazi en que se intimida, se seduce y se impone el silencio como ocurre en el bajo y medio Atrato y en casi todo el pacífico a manos de un cualificado paramilitarismo más eficaz que las expresiones guerrilleras. En ese pacifico de la exclusión de indígenas, negros y campesinos colonos desheredados de Sucre y Córdoba, el racismo y la discriminación campea.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las operaciones armadas aparentemente contra insurgente de mediados de los 90 y de comienzos del 2000 en el Chocó, el Valle y Nariño fueron etnocidas y hoy siguen siendo sofisticadamente del exterminio de una población por el hecho de vivir y coexistir de una u otro sentido de vida con el suelo, el subsuelo, el aire y otras especies vivas. Un plan de imposición de un modelo de nación que desconoce lo pluriétnico, que concibe a esos otros, como perezosos, atrasados, sucios, ignorantes o medicantes a los que es posible blanquear con la violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y luego, como ha sucedido con los mecanismos de cooptación institucional de Consultivas y otros perversos métodos que deshacen los movimientos sociales. O con las viejas prácticas clientelares de partidos y políticos que incluso a nombre de la paz,  desatan dinámicas de corrupción y complacencia con lo dominante.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las identidades indígenas, negras, campesinas se desconocen en todo su valor como parte de un estado pluri étnico y plurinacional,como cuidadores de territorios que proveen el agua y el aire, los alimentos. Se les violenta para blanquearlas. Se les destruye como aleccionamiento a un cuerpo social que debe ser excluido de un concepto de país plural. Se les mata con desplazamiento y despojo material, y con el desplazamiento/despojo del alma. Son eventualmente folclor.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En los planes criminales hay que destruir el sentido colectivo. Las identidades étnicas son estorbo para el progreso. Los planes criminales pretendieron arrasar con identidades étnicas que carecían de valor espiritual, de otro modo de la economía, de otro modo de convivencia con el agua, los bosques, los animales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esos tribales poco saben de desarrollo. Hay que limitar, una forma bonita de decir, terminemos con esos adornos de Consulta Previa. Esos tribales desconocen lo que son y lo que tienen. Con algo de dinero y una escuela o unos balones de fútbol a esos “hijos de puta” se les calla. Tienen tantos derechos insatisfechos, negación bien planificada, para que cualquier moneda les sirva para que se callen. Así dejan de respirar, de gritar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A los Georges de acá se les blanquea desde Bogotá, Medellín, Cali, Barranquilla y Cartagena. La asfixia es del alma. Los señores que regentan el gobierno y las instituciones del Estado, cada cuatro años, y sectores empresariales, con sectores militares y policiales,   a nombre de la paz o de la guerra, cuando no les usan, los reprimen; les callan o les seducen con algún puestico o dinero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la imposibilidad del despojo total porque ha existido la resistencia, hoy con esa fuerza de las armas para estatales y estatales se les silencia, se les fragmenta para callen, para que no griten que se están quedando sin aire, sin oxígeno, sin forma de respirar. Para eso se les reparten dulces. Recordemos a Duque en Bojayá hace cinco meses. Los negros, los campesinos, los indígenas son sujetos de caridad más que derechos. Hay que evitar que respiren.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todos los medios son legítimos, , incluso algunos de ellos que se acicalan con el lenguaje de la paz. En realidad es mejor dejarlos sin aire. Es mejor que no puedan respirar. Un homenaje a la muerte en vida. Ellos son poderosos. Ellos desconocen que seguimos respirando. Honramos la Vida

<!-- /wp:paragraph -->
