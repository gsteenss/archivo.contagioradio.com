Title: Crímenes contra defensores aumentaron 17% en 2017 con respecto a 2016
Date: 2018-01-29 14:13
Category: DDHH, Nacional
Tags: acuerdos de paz, defensores de derechos humanos, Gobierno vasco, misión de observación
Slug: crimenes-contra-defensores-aumentaron-17-en-2017-con-respecto-a-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Ene 2018] 

La delegación Vasca de Observación de derechos humanos en Colombia envió un comunicado tras haber presentado sus resultados en el que manifestaron que, tras la firma del Acuerdo de Paz la violencia contra los defensores de derechos humanos ha aumentado. En 2017 fueron **asesinados 98 líderes y lideresas** y han sido desplazadas más de 60.000 mil personas.

La delegación de Observación de Derechos Humanos que hace parte del gobierno Vasco, viajó a Colombia entre el 19 y 26 de enero en el marco del **Programa Vasco de Protección Temporal a Defensores y Defensoras de Derechos Humanos**. En esta visita se reunieron con diferentes organizaciones sociales, las Naciones Unidas e instituciones colombianas para evaluar la situación que viven los defensores de derechos humanos.

### **Observaciones de la misión** 

La delegación constató que la violencia contra defensores y defensoras de Derechos Humanos se ha agravado, “especialmente contra líderes indígenas y aquellos que trabajan activa y visiblemente **en favor del proceso de paz**”. Explican que los asesinatos de defensores de derechos humanos, durante el 2017, aumentaron en un 17% con respecto al 2016. Por esto, indicaron que la protección a quienes defienden los derechos humanos “es un indicador de éxito en el proceso de paz”.

Además, manifestaron que es preocupante la situación que viven las personas de aquellos territorios que fueron **abandonados por las FARC** en la medida que están siendo ocupados por actores armados “que se disputan su control principalmente para el narcotráfico o la minería ilegal”. Afirman que estos grupos violan sistemáticamente los derechos humanos de las mujeres y de las comunidades indígenas, campesinas y afrodescendientes. (Le puede interesar: ["Congresistas de Estados Unidos le exigen a Santos proteger a líderes sociales"](https://archivo.contagioradio.com/congresistas-eeuu-exigen-proteger-a-lideres-sociales/))

Ante esta situación, instaron a las instituciones a desarrollar **mecanismos de protección**, a visibilizar el trabajo de los defensores de derechos humanos y a prestar especial atención en las zonas y colectivos más amenazados. Para la delegación hay retos pendientes como el reconocimiento de los pueblos ancestrales como el Sinú y “la presencia institucional en las zonas rurales, no solo a través de la seguridad, sino también, y principalmente, mediante el desarrollo social, económico y cultural, con enfoque diferencial”.

Finalmente, informaron que se debe continuar trabajando por la **erradicación de la violencia** ejercida por los grupos armados, que se debe garantizar ña efectiva reinserción con condiciones de seguridad de los ex combatientes de las FARC, así como establecer una nueva mesa de negociación que tenga como prioritario un cese al fuego con el ELN.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
