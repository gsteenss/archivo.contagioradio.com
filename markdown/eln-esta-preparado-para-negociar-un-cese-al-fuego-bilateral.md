Title: ELN está preparado para negociar un cese al fuego bilateral
Date: 2020-09-30 18:35
Author: AdminContagio
Category: Actualidad, Paz
Tags: ELN, Gobierno Duque
Slug: eln-esta-preparado-para-negociar-un-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/PB_UP-scaled-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ELN Voces

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una entrevista con la Agencia Alemana de Prensa, **el jefe negociador del ELN en La Habana, Pablo Beltrán** reiteró la voluntad que existe de retomar un diálogo con el Gobierno y extender la propuesta de un cese al fuego bilateral y rechazó los señalamientos que se han hecho de su participación en las movilizaciones del mes de septiembre.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Beltrán expresó que pese a la "especie de congelamiento" que existe en las relaciones con la Casa de Nariño desde enero de 2019, - cuando ocurrió el atentado contra la Escuela de Cadetes Francisco de Paula Santander, en Bogotá - se ha dicho al Gobierno q**ue se establezca un alto al fuego que "permita manejar la pandemia"**, tal como lo solicitó la ONU en los meses de marzo y julio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que las comunidades más afectadas por la violencia y en particular el suroccidente y el pacífico colombiano **han reiterado la necesidad de una salida negociada al conflicto a través de un Pacto por la Vida y la Paz** y al cese al fuego unilateral que el ELN realizó como "gesto humanitario" ante la Covid-19, el Gobierno rechazó la proposición de un "cese bilateral al fuego por 90 días". [(Lea también: Comunidades claman por la verdad y por acuerdos humanitarios que alivien el dolor de la guerra)](https://archivo.contagioradio.com/comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ELN aguarda a que el Gobierno dé el siguiente paso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el jefe negociador, en la actualidad aguardan a que el gobierno de Iván Duque designe a los delegados que trabajarían en "reuniones iniciales de carácter confidencial y conocer "las expectativas mutuas de un proceso". [(Lea también: ELN niega vinculación a manifestaciones y reitera voluntad de diálogo)](https://archivo.contagioradio.com/eln-niega-vinculacion-a-manifestaciones-y-reitera-voluntad-de-dialogo/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Estamos disponibles aquí o donde sea. Lo importante es que empecemos a mirar las posibilidades de relanzar las negociaciones", subrayó.
>
> <cite>Pablo Beltrán</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha postura se mantiene pese a los frecuentes cuestionamientos del presidente Duque e integrantes de su gabinete, quienes insisten en la solicitud de extradición desde La Habana de los negociadores del ELN. [(Le puede interesar: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, desde Cuba, Rogelio Sierra Díaz, viceministro de la [Cancillería](https://twitter.com/CubaMINREX)expresó que desde el país insular se espera que "**el Estado colombiano reconozca la vigencia de los acuerdos suscritos con otros estados y cumpla con esos compromisos, en particular con el protocolo de ruptura del diálogo con el ELN"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El país garante ha resaltado además que la delegación de paz del ELN viajó con autorización del Estado colombiano y al romperse el diálogo se desconoce el protocolo previamente acordado para este caso, pues además de dictar órdenes de captura, acogió una «conducta contradictoria» que desconoce los acuerdos suscritos en el proceso de paz. [(Le recomendamos leer: Esperamos que Colombia reconozca acuerdos suscritos con otros Estados: Cuba)](https://archivo.contagioradio.com/esperamos-que-colombia-reconozca-acuerdos-suscritos-con-otros-estados-cuba/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
