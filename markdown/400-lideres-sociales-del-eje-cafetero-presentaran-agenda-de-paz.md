Title: 400 líderes sociales del Eje Cafetero presentarán agenda de paz
Date: 2016-04-12 15:12
Category: Nacional, Paz
Tags: Cumbre Agraria, Eje Cafetero, paz
Slug: 400-lideres-sociales-del-eje-cafetero-presentaran-agenda-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cumbre-Agraria-Eje-Cafetero-e1460491883715.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Opinador Global 

###### 12 Abr 2016 

El próximo 16 y 17 de abril, la Cumbre Agraria, Étnica y Popular organiza en Manizales la Cumbre Regional de Paz del Eje Cafetero, donde **400 representantes provenientes de Caldas, Quindio, Risaralda y del Norte del Valle,** trabajarán en una agenda común para la Paz.

Durante la cumbre se analizará los diferentes conflictos territoriales, políticos y sociales que hay en la región para luego construir un pliego de peticiones  que se presentará ante el gobierno Nacional y una hoja de ruta frente a las **movilizaciones para este primer semestre del 2016.**

La realización de este evento surge de la negociación del movimiento social con el Gobierno Nacional, generado por la movilización social iniciada en 2013.

Desde **La Cumbre de Paz del Eje Cafetero** se hace un llamado a las personas que hacen parte de la región para que participen de este proceso, que busca, que la Paz con Justicia Social sea producto de una **construcción desde las realidades del territorio** y con el protagonismo de todos los colombianos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
