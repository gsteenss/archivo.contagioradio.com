Title: Paramilitares amenazan a líder por reunirse con organismos internacionales
Date: 2017-07-30 11:30
Category: DDHH, Nacional
Tags: Amenazas, buenaventura, paramilitares
Slug: paramilitares-amenazan-a-lider
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/BUENAVENTURA-03-10-2014-BN-51.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Indígenas-del-Norte-del-Cauca-e1464380469118.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [29 Jul. 2017] 

El pasado 26 de julio 4 hombre armados, integrantes de las autodenominados Autodefensas Gaitanistas de Colombia – **AGC -  ingresaron por la fuerza a la residencia del líder comunitario Eison Valencia Sinisterra** quien vive en el Espacio Humanitario de Punta Icaco, en la Comuna 4 de Buenaventura, así lo aseguró a través de una denuncia la Comisión Intereclesial de Justicia y Paz.

Según el comunicado, **uno de los paramilitares colocó el arma en la cabeza de líder y lo amenazó** diciéndole que “no lo queremos volver a ver en reuniones con los gringos, porque si te volvemos a ver te vamos a matar”. Todo esto sucedió frente a su hijo de cuatro años que se encontraba en la vivienda. Le puede interesar: [Denuncian que Paramilitarismo y daños ambientales van de la mano en Buenaventura](https://archivo.contagioradio.com/43782/)

Las comunidades le aseguraron a la ONG que **estos paramilitares hacen parte de la estructura neoparamilitar que ejerce control** en el sector Piedras Cantan, barrio Viento Libre, comuna 4 y Alfonso López de la comuna 3; uno de ellos responde al alias de “Oscar”.

Dice la Comisión de Justicia y Paz que **esta amenaza se da como respuesta a una reunión que el líder sostuvo el 17 de julio** en las instalaciones de la ludoteca del Espacio Humanitario Punta Icaco con las organizaciones internacionales de Witness For Peace, Organización Mundial Contra la Tortura, y la Federación Internacional de Derechos Humanos.

**Hasta el momento no se conoce pronunciamiento por parte del Gobierno** y manifiestan que "las operaciones de presión y control son permanentes y sistemática de integrantes de las AGC sin que el gobierno haya adoptado medidas eficaces de desestructuración del neoparamilitarismo". Le puede interesar: [Lideresa afrocolombiana de Buenaventura es víctima de amenazas](https://archivo.contagioradio.com/lideresa-afrocolombiana-doris-valenzuela-es-victima-de-amenazas/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
