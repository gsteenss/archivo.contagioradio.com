Title: La guerra de Colombia se retrata en la tragedia de Tumaco
Date: 2017-11-08 13:00
Category: DDHH, Nacional
Tags: Banco de Datos CINEP, CINEP, crisis en tumaco, Tumaco, violación a los derechos humanos
Slug: la-guerra-de-colombia-se-retrata-en-la-tragedia-de-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Nov 2017] 

En rueda de prensa, el Banco de Datos de Derechos Humanos y Violencia política, que hace parte del Programa por la Paz del Centro de Investigación y Educación Popular (CINEP), presentó** en un informe la alarmante situación de violación a los Derechos Humanos** que se vive en el país, pero especialmente en Tumaco, Nariño.

De acuerdo con el Banco de Datos del CINEP, la violación a los derechos humanos en los diferentes departamentos del país **registra un aumento alarmante** teniendo en cuenta que se firmó un Acuerdo de Paz. Por esto, el informe y edición número 55 de la revista Noche y Niebla establece que, “comparado con el primer semestre de 2016, este periodo de 2017 registra un incremento de 26% respecto a victimizaciones cometidas por paramilitares y una leve disminución del 2% de autor no identificado”.

Y es que, en el primer semestre del año en curso, el Banco de Datos ha **identificado 389 victimizaciones que se le atribuyen a los paramilitares** y 484  atribuidas a actores armados no identificados. Esto, teniendo en cuenta la disputa territorial por parte de grupos armados y la falta de institucionalidad y presencia estatal en los diferentes territorios del país. (Le puede interesar: "[Especial: Tumaco llora una masacre en "tiempos de paz"](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/))

### **“A Tumaco nunca le han dado una oportunidad”.** 

De acuerdo con el Vicario de la Diócesis de Tumaco, Arnulfo Mina, Tumaco ha sido una población que **ha vivido en el olvido estatal históricamente**. Dijo que allí confluyen diferentes problemas que han sido advertidos, como la presencia paramilitar, y aun así no han sido atendidos de manera efectiva.

Afirmó que **“el problema del narcotráfico le ha quedado grande al Estado”** y que va a seguir habiendo más muertes “hasta tanto el Estado no se comprometa con las comunidades y con la inversión social”. Además, indicó que la solución del Gobierno ha sido siempre “mandar más Ejército y Policía como lo hace con el Plan Atlas”. (Le puede interesar: ["Conclusiones de los medios sobre masacre en Tumaco revictimizan: Asominuma"](https://archivo.contagioradio.com/47979/))

### **Abandono del Estado ha sido demasiado antiguo** 

Según el padre Javier Giraldo, vocero del Banco de Datos, **el olvido estatal en las zonas periféricas del país ha permitido la situación de violencia** contra las comunidades que hoy se vive en los territorios. Indicó además que el problema de los cultivos de coca, el narcotráfico, se suma la presencia de la fuerza pública y la proliferación de grupos armados “han complicado la violencia y hacen de Tumaco una tragedia”.

Manifestó además que **esta situación se pude replicar en otros territorios** pues en el Catatumbo se está viviendo una situación similar en donde hay paramilitares, cultivos ilícitos y problemáticas muy parecidas. Sin embargo, argumentó que la situación en departamentos como Putumayo o Caquetá, no se acerca aún a lo que se vive hoy en Tumaco. (Le puede interesar:["Comunidades Awá en Tumaco: En medio del abandono estatal y la militarización"](https://archivo.contagioradio.com/comunidades_awa_tumaco/))

### **Acciones de la fuerza pública han aumentado la crisis en Tumaco** 

Los voceros del CINEP manifestaron que a la situación ya critica que se vive por la disputa de los territorios, las acciones de la fuerza pública, específicamente del ESMAD, **contribuyen a que aumenten los niveles de violencia**. Por ejemplo, en el primer trimestre de 2017, el Banco de Datos registró 273 agresiones por parte del ESMAD, como medidas represivas de la protesta social.

Finalmente, el CINEP indicó que la falta de institucionalidad se refleja en el nivel de **impunidad que hay alrededor de los asesinatos** y agresiones a líderes sociales y le hicieron un llamado al Gobierno Nacional y a la Fiscalía General de la Nación para que  reconozcan la violencia en estos territorios como un patrón sistemático que requiere de acciones urgentes. (Le puede interesar: ["Policía sigue actuando de "manera criminal" en Tumaco"](https://archivo.contagioradio.com/policia-sigue-actuando-de-manera-criminal-en-tumaco/))

También indicaron que la ciudadanía debe hacer un **proceso de veeduría constante** para presionar al Gobierno a que cumpla con los Acuerdos de Paz de la Habana y que se termine el desorden administrativo que sólo contribuye a la desinformación y favorece a procesos ilegales como el robo de tierras.

<iframe id="audio_21963421" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21963421_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
