Title: ELN y Gobierno incluyen el cese al fuego en agenda de conversaciones
Date: 2017-06-10 15:58
Category: Nacional, Paz
Tags: ELN, Equipo de Paz, paz, proceso de paz, Quito
Slug: eln-y-gobierno-incluyen-el-cese-al-fuego-en-la-mesa-de-conversaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-e1476309678834.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 10 Jun. 2017 

Según un breve comunicado emitido por el ELN en Quito, Ecuador se han logrado avances signficativos entre las partes con miras a llegar a la paz, uno de los más importantes es que **se pudo incluir en la agenda de la mesa el punto concerniente al Cese Bilateral,** aunque hasta el momento no han entrado a discutir sobre este. Le puede interesar: [ELN y Gobierno alcanzarían punto de no retorno en Diciembre: Carlos Velandia](https://archivo.contagioradio.com/aun-no-hay-condiciones-para-pactar-cese-al-fuego-entre-eln-y-gobierno-carlos-velnadia/)

Así mismo, aseguraron que **esta semana se firmó el acuerdo sobre Pedagogía y comunicaciones para la paz,** Grupo de países de apoyo, acompañamiento y cooperación, y "seguimos trabajando hacia el Desminado Humanitario". Le puede interesar: [ELN y gobierno logran 3 acuerdos para continuar en la mesa de conversaciones de paz](https://archivo.contagioradio.com/eln-y-gobierno-logran-3-acuerdos-para-continuar-en-la-mesa-de-conversaciones-de-paz/)

Contrario a ello, asegura la Delegación de Diálogos de Paz del ELN que **en lo que se refiere al tema de Dinámicas y Acciones Humanitarias, no lograron llegar a un acuerdo** según ellos “debido a que la delegación del Gobierno argumenta que tiene capacidades limitadas para hacer acuerdos que prevengan el Genocidio en curso. Además, eluden tratar el paramilitarismo en este punto”.

Por estos argumentos y “al persistir las distancias”,** según el comunicado el Gobierno decidió unilateralmente, congelar la implementación de los acuerdos firmados y pactados esta semana** con lo que pretenden según ellos “posesionar otros criterios, diferentes a los acordados hasta ahora, que son la celeridad y la rigurosidad en las conversaciones”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
