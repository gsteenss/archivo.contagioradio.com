Title: El Cerrejón incumple tutela fallada a favor de un menor de 2 años
Date: 2016-05-17 12:59
Category: DDHH, Nacional
Tags: El Cerrejón, enfermedades causadas por minería, Tutela contra Cerrejón
Slug: el-cerrejon-incumple-tutela-fallada-a-favor-de-un-menor-de-2-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cerrejón....jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio ] 

###### [17 Mayo 2016 ]

En noviembre del año pasado, la Corte Constitucional falló una tutela en favor de Moisés Guette, niño indígena Wayuu de 2 años quien padece graves problemas respiratorios y de hemoglobina, por cuenta de las **fuertes explosiones producidas para la extracción carbonífera** de la empresa minera Carbones del Cerrejón. Pese a la decisión, la compañía ha incumplido, ni siquiera se ha comunicado con la familia del menor, según afirma Luz Ángela Uriana, su madre.

La tutela ordenó al Cerrejón garantizar que Moisés viva en un ambiente sano, sin embargo su madre asegura que **continúan respirando carbón, actualmente su vivienda está a punto de caer, y los servicios médicos para la atención del niño siguen corriendo por cuenta de su familia**. "Ellos no se han pronunciado con nada, mi hijo es atendido en Barranquilla y todo eso son citas particulares que pagamos cada vez que vamos", afirma Uriana.

"Ellos no nos han ayudado con nada, ni con reducir la contaminación, siguen explotando a diario, no han cumplido con nada", expresa la madre, quien insiste en que hay muchos padres de familia que sienten temor de enfrentarse a la minera, y reclamar por los derechos de los niños y niñas que nacen y deben enfrentarse a las **infecciones pulmonares, diarreas, vómitos y hongos que se producen por la extracción de carbón**.

Las autoridades municipales y departamentales, como **Corpoguajira han aseverado que no tienen conocimiento del fallo de tutela**, situación que indigna a la madre de Moises, quien insiste en que [[persistirá en sus exigencias y demandas](https://archivo.contagioradio.com/?s=el+cerrej%C3%B3n+)] para permanecer en su territorio.

<iframe src="http://co.ivoox.com/es/player_ej_11563059_2_1.html?data=kpaimJiUeZqhhpywj5aVaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5ynca3p25Cu0MzJsMKfttfWw9PFb46frsbR1MqPm8Lt1tqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
