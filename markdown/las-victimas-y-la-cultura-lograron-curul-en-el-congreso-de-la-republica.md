Title: Las víctimas y la cultura lograron curul en el Congreso de la República
Date: 2018-03-12 13:09
Category: Nacional, Política
Tags: Congreso de la República, elecciones, Gustavo Bolivar, lista de los decentes, Maria Jose PIzarro, Senado y Cámara
Slug: las-victimas-y-la-cultura-lograron-curul-en-el-congreso-de-la-republica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Diseño-sin-título-1-e1520876648974.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Gustavo Bolívar/La silla vacía ] 

###### [12 Mar 2018] 

De la mano de María José Pizarro en la Cámara de Representantes por Bogotá y de Gustavo Bolívar en el Senado, **tanto las víctimas del conflicto armado como la cultura cuentan con una representación más en el nuevo Congreso de la República por el periodo 2018-2022.** Pizarro**,** de la lista de los Decentes, alcanzó 77.800 votos y Bolívar de la misma colectividad alcanzó su curul con 1016.9505 votos.

Luego de que las y los colombianos ejercieran su derecho al voto este 11 de marzo, **fueron repartidas las 102 curules del Senado y las 166 de la Cámara de Representantes**. Para esta ocasión, la lista de los Decentes, que apoyan al candidato presidencial Gustavo Petro, obtuvieron un total de curules para el Congreso.

### **Víctimas estarán representadas en la Cámara por Bogotá con María José Pizarro** 

<iframe src="https://co.ivoox.com/es/player_ek_24357185_2_1.html?data=k5mgl5yVfJahhpywj5WbaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfq9Tgh6iXaaKtjLXW3MbWttCZk6iY1dTGtsaf1NqYx9HJp8Tdhqigh6eXsozVjMbZjaiJh5SZopbaw9fFb9Ghhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con la hija de Carlos Pizarro, su elección **“es producto de una estrategia diferente pues estuvimos en la calle con la gente todos los días y fue una campaña muy austera”.** Manifestó que durante su campaña pudo evidenciar que “hay un despertar de la gente joven en el tema de político” y que “hay una esperanza para avanzar en una política diferente mucho más concertada”.

Manifestó que seguirá trabajando por la representación de las víctimas, especialmente  en un momento donde se necesita de su presencia en el legislativo. Por esto reiteró su compromiso con las víctimas pues **“hay una deuda histórica muy grande”, **y afirmó que trabajará por sacar las Circunscripciones Especiales de Paz adelante. (Le puede interesar: ["Acabar con la lista de los decentes es un atentado contra la democracia: María José Pizarro"](https://archivo.contagioradio.com/acabar-con-lista-de-los-decentes-es-un-atentado-contra-la-democracia-maria-jose-pizarro/))

Pizarro enfatizó en que la lista de los Decentes **“cumplió con la paridad entre hombres y mujeres”,** en cuanto que hay representación de ambos tanto en el Senado como en la Cámara. Indicó que va a seguir trabajando por lograr que la paridad no se dé como una ley de cuotas y que las mujeres legislen “garantizando la agenda política de las mujeres”.

En cuanto a la participación de la sociedad civil, la nueva representante manifestó que estará “con un pie en el Congreso y otro en la calle” tratando de fortalecer el concepto de democracia ciudadana que **“nos permita recuperar la participación política con otras dinámicas que nos permitan dialogar con la ciudadanía”. **(Le puede interesar: ["Colombia polarizada"](https://archivo.contagioradio.com/colombia-polarizada/))

### **La cultura estará representada con Gustavo Bolívar** 

<iframe src="https://co.ivoox.com/es/player_ek_24357248_2_1.html?data=k5mgl5yWeJmhhpywj5aVaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPhtDghqigh6aousLmjN6Y1dqPqc3ZxMjWh6iXaaOnz5DOzpC3qc_VxdSYxsqPsMKf08rdh6iXaaO1w9Gah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, el escritor Gustavo Bolívar hará parte del Senado trabajando, entre otras cosas, por los intereses culturales en el país. Manifestó que, a pesar de su corta carrera política, **su elección responde al trabajo que realizó con el candidato presidencial Gustavo Petro** además de su labor en la lucha contra la corrupción.

Bolívar enfatizó en que va a trabajar por que se cree un sistema de becas como “Ser Pilo Paga” con el arte. Asimismo, afirmó que en su campaña se evidenció la necesidad de sacar a los artistas de Colombia del anonimato pues **“descubrí una gran cantidad de personas con talentos diferentes que merecen mejores oportunidades”, expresó.**

Además, señala que el reto inmediato es ayudar a que Gustavo Petro alcance la presidencia. Por su parte, en la legislatura trabajará por desarrollar diferentes proyectos de ley en los que se encuentra, por ejemplo, acabar con el ICETEX. Dijo que **“es una vergüenza que a los estudiantes no se le brinde la educación y se les haga prestamos además con fiadores”.**

Finalmente, en cuanto a la implementación de los Acuerdos de la Habana, dijo que es necesario respetar el acuerdo pues **“se le debe cumplir a la FARC lo que se les prometió para evitar que la gente de abajo se escurra a las bandas criminales”.** Y concluyó que el Gobierno Nacional y el Congreso de la República deben tener un compromiso más grande con la implementación de lo acordado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
