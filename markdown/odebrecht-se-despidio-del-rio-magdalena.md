Title: Odebrecht se despidió del Río Magdalena
Date: 2016-06-14 07:00
Category: Ambiente y Sociedad, Opinion
Tags: gran mineria, minera, Odebrecht, Río Magdalena
Slug: odebrecht-se-despidio-del-rio-magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/odebrech.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ISB 

[ Por](https://archivo.contagioradio.com/margarita-florez/)**[Vanessa Torres](https://archivo.contagioradio.com/margarita-florez/) \* - [Asociación Ambiental y Sociedad ](https://archivo.contagioradio.com/margarita-florez/)**

***¿Se continuará permitiendo en Colombia la participación de empresas con antecedentes negativos a nivel internacional en materia de corrupción, daños ambientales\[1\] y sociales dándole prioridad a su eventual solidez económica?***

El proyecto de Recuperación de la navegabilidad del Río Magdalena se conoció en el último año en Colombia, por su magnitud en como la primera Asociación Público Privada – APP y además por las consecuencias en materia de daño ambiental, ecológico y social que se causarían en el río más importante del país, de otro lado se expresó preocupación por parte de diferentes sectores\[2\] debido a la adjudicación del proyecto a Odebrecht socio mayoritario en el consorcio que ganó la licitación del proyecto en el año 2014. La empresa se había visto envuelta en un grave escándalo de corrupción que permeó a las más altas esferas del poder público y privado en Brasil, actualmente el Presidente y propietario de la empresa, Marcelo Odebrecht se encuentra cumpliendo una condena de 19 años de prisión.

En medio de este agitado y difícil momento para el vecino país, se conoció este jueves 12 de mayo que la empresa brasilera cederá su participación accionaria mayoritaria el cual es del 87% dentro del Consorcio Navelena S.A.S a la que CORMAGDALENA adjudicó mediante el proceso de Asociación Publico Privada APP – 001 – 2013  contrato 001 de 2014 del Proyecto de Recuperación de la Navegabilidad del Río Magdalena\[3\], por un valor de US\$ 2500 millones de dólares esto de acuerdo con la información del contrato y todo el proceso licitatorio contenido en el SECOP I, de conocimiento público\[4\].

Es importante mencionar que el contrato mencionado de esta APP se celebró después de una competencia entre nueve empresas chinas, españolas, brasileras y colombianas, que participaron en el inicio del proceso. El consorcio NAVELENA S.A.S fue declarado como único oferente precalificado el 25 de julio de 2014, faltando aún dos meses para la terminación del proceso de adjudicación. Este contrato contó con el acompañamiento de la Procuraduría General de la Nación, y de una comisión de expertos designada por la Presidencia.

Uno de los criterios cumplidos por el consorcio de acuerdo con la información en el acta de audiencia pública de cierre del proceso, es que cumplía con todos los requisitos solicitados, dentro de los cuales y uno de los principales, era la solidez financiera del consorcio que se encargaría de ejecutar un proyecto de tan alta envergadura. Esa solidez se basó  en el respaldo de una empresa como Odebrecht, tal como lo manifestó el entonces director ejecutivo de Cormagdalena, Augusto García “Estamos muy complacidos de que se haya presentado la firma Odebrecht para ejecutar este proyecto que no tiene antecedentes en la historia de la infraestructura nacional y que concretará el anhelo de todos los colombianos que por tantos años hemos querido presenciar la recuperación de nuestro más importante y querido río, que es el Magdalena”\[5\].

Cabe destacar que las investigaciones en torno a la empresa brasilera, y su posible involucramiento en el escándalo de corrupción iniciaron en 2014\[6\] mismo año en que se realizó el proceso de Asociación Publico Privada AAP para adjudicar el contrato por la navegabilidad del Río Magdalena,  lo cual deja un interrogante en torno al control existente en el país frente a los antecedentes de una empresa a nivel internacional que tiene una relevancia al interior de la dinámica económica de Colombia. Es importante que los antecedentes de estas empresas, no solo al interior del país sino a nivel internacional, sean tomados en cuenta al momento de otorgar tan alta responsabilidad.

Odebrecht y otras empresas que han sido financiado por el Banco Nacional de Desarrollo de Brasil BNDES, se han visto involucrados en escándalos de corrupción, sobrecostos, retrasos en las obras de los proyectos a su cargo, los cuales causan grandes impactos sociales y ambientales al territorio. A partir del escándalo la empresa que ha tenido participación en la mayoría de países de la región, ha renunciado a  megaproyectos  como el  de Navegabilidad del Río Magdalena en Colombia y en Perú el proyecto Gasoducto Sur Peruano.

Dado el retiro de Odebrecht del proyecto en Colombia, el cual inició en enero de 2015 en medio de duras críticas por el inminente daño ambiental que generaría esta intervención directa al río a partir del dragado hidráulico y el posterior encauzamiento del mismo, los colombianos quedamos con diferentes cuestionamientos y preocupaciones: ¿En manos de quien recaerá esta responsabilidad al momento de realizarse una eventual cesión de contrato?  ¿Cómo se realizará el dragado del río, proceso que inicialmente estaba pactado para nueve meses desde enero de 2015 y que no ha sido finalizado, y en el cual Odebrecht fue la única empresa participante en el proceso de licitación que se comprometió a fabricar las dragas especiales debido a la composición del mismo? ¿Cómo se manejarán los daños causados a las comunidades ribereñas las cuales no se encuentran conformes desde el inicio del proyecto con la intervención directa que se está realizando en sus territorios?  ¿Se continuará permitiendo en Colombia la participación de empresas con antecedentes negativos a nivel internacional en materia de corrupción, daños ambientales\[7\] y sociales dándole prioridad a su eventual solidez económica?

###### \*Coordinadora del Área de infraestructura y energía sostenibles de la Asociación Ambiente y Sociedad. 

------------------------------------------------------------------------

 

</p>
###### \[1\] <http://elclarin.com.mx/odebrecht-es-multada-por-danos-ambientales-en-jalcomulco-trabaja-sin-permisos/> 

###### \[2\] Ver: Foro Nacional Ambiental. <http://www.foronacionalambiental.org.co/actividades/detalle/foro-publico-para-donde-va-el-rio-magdalena-riesgos-sociales-ambientales-y-economicos-del-proyecto-de-navegabilidad-2/> 

###### \[3\] Contrato 001 de 2014 “para la ejecución por su cuenta y riesgo, del Diseño, la Construcción, la Operación, el Mantenimiento, la Financiación y Reversión del Proyecto de Recuperación de la Navegabilidad del Río Magdalena” 

###### \[4\] <https://www.contratos.gov.co/consultas/detalleProceso.do?numConstancia=13-19-1390566> 

###### \[5\] <http://m.portafolio.co/economia/finanzas/colombobrasilenos-rio-magdalena-66478> 

###### \[6\] <http://elcomercio.pe/mundo/actualidad/suiza-investiga-odebrecht-corrupcion-video-noticia-1828086> 

###### \[7\] <http://elclarin.com.mx/odebrecht-es-multada-por-danos-ambientales-en-jalcomulco-trabaja-sin-permisos/> 

######  

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
