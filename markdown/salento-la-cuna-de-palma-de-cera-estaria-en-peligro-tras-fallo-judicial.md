Title: Salento, cuna de la palma de cera, estaría en peligro tras fallo judicial
Date: 2019-06-05 15:26
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Tribunal Administrativo del Quindío
Slug: salento-la-cuna-de-palma-de-cera-estaria-en-peligro-tras-fallo-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Valle_de_Cocora2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Peterfitzgerald] 

El Tribunal Administrativo de Quindío falló el pasado 30 de mayo en contra de un Acuerdo municipal del Concejo de Salento, por medio del cual se prohibieron las actividades de minería de metales y piedras preciosas, con el fin de proteger el patrimonio cultural y ecológica del territorio. Según los ambientalistas, la decisión del Tribunal abriría la puerta a la megaminería en el territorio.

"Es un revés para la ciudadanía en el Quindío que desde hace más o menos 10 años estamos movilizados acá en defensa del agua, la vida y el territorio y **en particular frente a los proyectos de gran minería que está comprometiendo el 61,8% del área del departamento**", afirmó frente a la decisión Nestor Ocampo, director de la Fundación Ecológica Cosmos.

Como lo indicó el ambientalista, este municipio es reconocido por sus paisajes de palma de cera, el árbol nacional de Colombia, que atrae hasta 10.000 turistas durante la temporada alta. Además, es una región de gran biodiversidad dado que existen unas 564 especies de aves registradas en la región, entre las que destaca el loro orejiamarillo. También, nace allí el Río Quindío. (Le puede interesar: "[Anglo Gold Ashanti desconoce acuerdo municipal que prohíbe la minería](https://archivo.contagioradio.com/anglo-gold-ashanti-desconoce-acuerdo-municipal-que-prohibe-la-mineria/)")

"Esto es una maravilla de la naturaleza. Todo esto está en peligro con la posibilidad de que en la región se adelante un solo proyecto de gran minería", señaló el director de la Fundación, en referencia a que **el 17.5 por ciento del municipio tiene títulos mineros otorgados y otro 17.5 por ciento del territorio cuenta con títulos solicitados.**

Al respecto, Ocampo indicó que tomarían las medidas legales posibles para proteger el patrimonio ecológico de la región, además de que consideran presentar este caso a la Organización de las Naciones Unidas. Adicionalmente, el próximo 7 de junio, los salentinos también se unirán a la movilización nacional en contra del fracking y la minería contaminante.

<iframe id="audio_36741639" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36741639_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
