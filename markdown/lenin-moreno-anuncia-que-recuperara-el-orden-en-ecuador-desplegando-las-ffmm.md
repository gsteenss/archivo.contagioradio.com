Title: Lenin Moreno anuncia que recuperará el orden en Ecuador, desplegando las FFMM
Date: 2019-10-12 19:01
Author: CtgAdm
Category: El mundo
Tags: Confederación de Nacionalidades Indígenas del Ecuador, Crisis en Ecuador, Lenín Moreno, Toque de queda
Slug: lenin-moreno-anuncia-que-recuperara-el-orden-en-ecuador-desplegando-las-ffmm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Quito.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Lenin.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Lenin] 

El presidente de Ecuador, Lenin Moreno decretó el toque de queda en Quito a partir de este sábado a las 3:00 de la tarde, restringiendo la movilidad en lugares considerados "áreas sensibles y de importancia estratégica", dando facultad a Fuerzas Militares y Policía Nacional para detener a quienes permanezcan en espacios públicos y así dispersar la protesta popular. Pese a la medida, los manifestantes [continúan llegando a las cercanías de la Asamblea y la Contraloría.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Según un comunicado del Comando Conjunto de las Fuerzas Armadas del Ecuador, en **Quito se ha decretado esta medida en las zonas comerciales, centros financieros, sedes del Estados y áreas e instalaciones estratégicas** mientras en el resto del país la medida se aplica a puertos, aeropuertos, instalaciones militares y policiales, antenas repetidoras de telecomunicaciones, refinería y represas.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Felreversoecu%2Fvideos%2F759806797869241%2F&amp;show_text=0&amp;width=560" width="560" height="313" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Desde el Gobierno, se advirtió a la ciudadanía para que regresara a sus viviendas antes de que iniciara el toque de queda que regirá únicamente en Quito, ["no debió haber un toque de queda, debieron escuchar a su pueblo porque todos somos un pueblo que lucha por un mismo ideal" afirman desde las calles.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

> [\#URGENTE](https://twitter.com/hashtag/URGENTE?src=hash&ref_src=twsrc%5Etfw) acá la voz del pueblo de Ecuador y su mensaje para Lenin Moreno: escuchen, difundan. La gente no se irá a la casa por el toque de queda. ¿Va a masacrar al pueblo movilizado? ¿Cuántos muertos busca? [\#12Oct](https://twitter.com/hashtag/12Oct?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/mglwAbcPZZ](https://t.co/mglwAbcPZZ)
>
> — Marco Teruggi (@Marco\_Teruggi) [October 12, 2019](https://twitter.com/Marco_Teruggi/status/1183127129631924224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por su parte, la Confederación de Nacionalidades Indígenas del Ecuador (CONAIE), después de consultar con comunidades y organizaciones sociales aceptaron  establecer una mesa de diálogo propuesta por el presidente Lenin Moreno para llegar a un acuerdo, sin embargo resaltan que **"el diálogo que el Gobierno dice promulgar se ha sustentando durante este proceso de resistencia en una de las peores masacres en la historia del Ecuador, una violencia exacerbada interpuesta por la Fuerza Pública y militar".**

Asímismo, la CONAIE señala que a lo largo del día sábado se han concentrado en en el agora de la Casa de la Cultura y que no fueron partícipes de los hechos ocurridos en la Contraloría y la sede de Teleamazonas, entidades que fueron incendiadas en medio de las manifestaciones. Por su parte, la cadena TeleSUR afirma que su señal televisiva en este país ha sido retirada sin justificación.

En días pasados el Gobierno de Lenín Moreno ordenó la suspensión de las transmisiones de otros medios de comunicación en el país, entre estos emisoras como Radio Pichincha Universal. [(Le puede interesar: ¿Levantar el paquetazo, o buscar la salida de Lenín Moreno en Ecuador?)](https://archivo.contagioradio.com/levantar-el-paquetazo-o-buscar-la-salida-de-lenin-moreno-en-ecuador/)

### Continúa el uso desmedido de la fuerza en las calles de Ecuador 

En las últimas horas se conoció a través de redes sociales, un video en que quedó registrado cómo fue asesinado uno de los manifestantes tras ser impactado mientras se protegía en una barricada, este es el reflejo de la represión que se vive en Ecuador y de la que también fueron víctimas **Raúl Chilpe, Marco Otto, Chaluisa Cusco José Daniel, Inocencio Tucumbi, José Rodrígo Chaluisa**, personas quienes también murieron en medio de las manifestaciones según los datos recopilados por la Defensoría del Pueblo.

Estos hechos violentos se suman a las siete muertes, 855 heridos y cerca de 1070 personas detenidas desde el pasado 3 de octubre, por su parte el presidente Lenin Moreno ordenó el toque de queda y la militarización de la capital tras cumplirse 11 días de manifestaciones.

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
