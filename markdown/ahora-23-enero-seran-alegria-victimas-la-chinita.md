Title: “Ahora el 23 de Enero será de alegría” víctimas de La Chinita
Date: 2017-01-24 13:33
Category: DDHH, Nacional
Tags: FARC, La Chinita, Masacre La Chinita, víctimas
Slug: ahora-23-enero-seran-alegria-victimas-la-chinita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/victimas-la-chinita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Ene. 2017] 

Este 23 de Enero las familias de las 35 víctimas de la masacre de La Chinita cumplieron 23 años de memoria y resistencia, fecha que estuvo acompañada por una conmemoración que mostró cambios sustanciales luego del acto de perdón de las FARC del mes de septiembre de 2016. Uno de ellos es que ellas no se reconocerán más como víctimas sino como constructoras de paz y el segundo es que comenzarán a recordar con alegría, esperanza y no con dolor este hecho.

**Silvia Berrocal una de las testigos y afectadas por esta masacre** manifestó que “no van a olvidar que el 23 de Enero de 1994 fallecieron nuestros familiares, los vamos a recordar siempre, pero para el próximo año solo vamos a hacer una misa y actos culturales”.

Según Silvia, realizar cada año actos muy grandes por dicha masacre “es doloroso”, por lo que hacer conmemoraciones más pequeñas es una forma de mostrarle al país que en La Chinita **“ya sanamos nuestros corazones y dijimos no más, no somos víctimas, somos personas, sujetos de derechos y por eso este 23 de Enero de 2017 fue el cierre de todo este sufrimiento”.**

Las personas de La Chinita continuarán haciendo ejercicios de memoria y actividades lúdicas con toda la población, pero enfocándose en los jóvenes para contarles lo que sucedió en la zona **“lo que queremos es recordar con alegría** e incluso pensamos en que ese día puedan ser inaugurados parques, placas en honor a las víctimas” dijo Silvia.

**Silvia contó que de esta manera estarán trabajando para que la fecha no sea recordada con dolor o tristeza** sino que por el contrario sea la oportunidad de progresar “aquí estamos con las banderas adelante para seguir trabajando por la población y por la paz”.

La Chinita fue testigo el 30 de Septiembre de 2016 de un **acto de perdón y reconocimiento de responsabilidad por parte de las FARC** como parte de los acuerdos que se hicieron en La Habana con las víctimas. En contexto: ["Perdonamos y decimos sí al plebiscito": víctimas de La Chinita](https://archivo.contagioradio.com/perdonamos-y-decimos-si-al-plebiscito-victimas-de-la-chinita/)

“Teníamos la necesidad de mirar a los ojos a las FARC y preguntarle en mi caso ¿Por qué mataron a mi hijo con solo 16 años? ¿Qué les hizo mi hijo? ¿Qué les habíamos hecho para matar 35 personas? Y luego de eso **vimos la sinceridad, la vergüenza y el dolor porque son personas (…) Nos equivocamos, nos dijeron**” contó Silvia.

Silvia recuerda con tranquilidad dicho acto, cuenta que pudieron ver incluso lágrimas en la delegación de las FARC por haber cometido esa masacre y dice “**eso nos ayudó a entender que somos humanos y que nos podemos equivocar, de manera grave (…) pero es hora de decir basta, ya basta, están arrepentidos”**. Le puede interesar: ["Las víctimas de la masacre de La Chinita sí perdonamos"](https://archivo.contagioradio.com/las-victimas-de-la-masacre-de-la-chinita-si-perdonamos/)

Aunque las cifras de víctimas en la región de Urabá teniendo en cuenta el número de afectados y de casos presentados son disimiles, el número es amplio. Según ACNUR en promedio de 6 a 9 personas fueron asesinadas en cada masacre cometida en el país.

Y según datos del proyecto "Rutas del Conflicto" de 1982 a 2013 los grupos paramilitares fueron responsables de de 1.166 masacres, seguidos por grupos armados no identificados con 295 masacres, la guerrilla de FARC con 238, y 139 masacres fueron cometidas por las fuerzas de seguridad.

Por ello, Silvia concluye diciendo “**queremos ser el apoyo y el ejemplo para esas otras víctimas. Queremos trabajar con esas otras personas para ayudarles a dar ese paso del perdón”.**

<iframe id="audio_16621984" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16621984_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
