Title: Así funcionará la Unidad de Búsqueda de personas Desaparecidas
Date: 2018-06-22 13:09
Category: DDHH, Nacional
Tags: acuerdo de paz, Luz Marina Monzon, Unidad de Búsqueda de Personas Desaparecidas
Slug: asi-funcionara-la-unidad-busqueda-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/marcha-por-la-paz25.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Jun 2018] 

La Unidad de Búsqueda de Desaparecidos esta lista para comenzar sus labores, luego de que la Corte Constitucional diera vía libre a sus funciones. Para Luz Marina Monzón, directora de esta Unidad, este es un buen paso, lo siguiente será concretar el plan de diseño y funcionamiento total de este organismo **para que inicie su ardua labor de encontrar e identificar a las personas desaparecidas en Colombia, en al marco de la guerra**.

Monzón aseguró que en este momento la Unidad ya presentó el diseño institucional con los requerimientos económicos al Ministerio de Hacienda y a Función Pública en dónde están a la espera de que se firmen los decretos, por parte del actual presidente Santos, para que se avalen los presupuestos de la planta y estructura final de la Unidad, **“estamos en el diálogo de la propuesta que hemos presentado”.**

### **El funcionamiento de la Unidad** 

La Unidad de Búsqueda de personas desaparecidas funcionaría con al **menos 529 personas** que tendrían una presencia permanente en el territorio. Además, la finalidad es que exista un punto de contacto de la Unidad en cada uno de los departamentos para muchos en efectos: en primera instancia para desarrollar la búsqueda y en específico para tener la información más precisa y completa sobre quiénes son, cuantos son y en dónde están los desaparecidos y así determinar el universo de las personas dadas por desaparecidas y de las fosas que hay en el país.

Los puntos de contacto tendrán la posibilidad de ser movilizados a cualquier parte del país y de acuerdo con Monzón serán los encargados de **“generar la confianza, la comunicación y la interrelación con las víctimas de una forma más regular”** y no como se ha venido manejando actualmente con los organismos estatales. (Le puede interesar: ["Luz verde para funcionamiento de la Unidad de Búsqueda de personas desaparecidas"](https://archivo.contagioradio.com/luz-verde-para-funcionamiento-de-la-unidad-de-busqueda-de-desaparecidos/))

### **Duque y la Unidad de Búsqueda** 

Referente a la llegada a la presidencia de Iván Duque y las posibilidades de que se frené el trabajo de investigación que llevaría a cabo la Unidad, Monzón manifestó que “El mandato de la Unidad es una labor humanitaria por hallar y encontrar a las personas desaparecidas que es una expectativa de los familiares, no creo **que vaya a ser algo respecto a lo cual este presidente o cualquier otro pueda estar en contra**”.

En ese sentido señaló que no considera que se va a afectar el desarrollo de sus funciones ni que existirá alguna oposición frente al cumplimiento que se le ha dado a la misma en el marco de los Acuerdos de paz. (Le puede interesar: ["La Unidad de Búsqueda una oportunidad para los desaparecidos"](https://archivo.contagioradio.com/la-unidad-de-busqueda-una-oportunidad-para-los-familiares-de-desaparecidos/))

<iframe id="audio_26683188" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26683188_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
