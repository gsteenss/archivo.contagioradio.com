Title: ONIC rechaza abuso contra menor Embera en el Quindío
Date: 2019-06-01 19:31
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Abuso sexual, Indígenas Emberá, niñez
Slug: onic-rechaza-abuso-contra-menor-embera-en-el-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/6e8a6db835582e589c7df1febbc50f9d-1132x670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La razón 

Por medio de un comunicado, la Organización Nacional Indígena de Colombia (ONIC), manifestó su rechazo y repudio frente a las acciones en las que presuntamente **el dirigente Bernabé Ramírez González**, habría **accedido carnalmente a una menor de la comunidad Embera** del resguardo ubicado en Quebrada negra, municipio de Calarcá.

Desde la Consejería de mujer, familia y generación de la organización, demandaron frente al asunto "la **coordinación y coadyuvancia con la justicia ordinaria**" para atender el caso "donde se atentó contra la integridad y dignidad de la niñez y la adolescencia".

Desde la organización aseguran que "es evidente que dicho comportamiento es sancionado por la gravedad donde hay prevalencia superior de los derechos de los menores y los derechos de las víctimas" particularmente de niños y jóvenes que consideran son "**las semillas de vida de los pueblos**".

En consonancia, la ONIC manifestó su respaldo a la decisión del Comité ejecutivo Nacional del Movimiento Alternativo Indígena y social- MAIS, de **suspender a Ramírez González de manera indefinida de su cargo como Presidente del Comité Ejecutivo Departamental del Quindío**, hasta que se esclarezcan los hechos .

##### Acción con la justicia ordinaria

La Consejería manifestó suy exigencia a las autoridades tradicionales para que **en coordinación con la justicia ordinaria y la misma ONIC implementen las acciones oportunas durante la investigación** y sanciones para este tipo de delito, actuando con celeridad y eficacia, evitando cualquier asomo de impunidad.

Adicionalmente, anuncian que acompañarán las acciones del Instituto Colombiano de Bienestar Familiar (ICBF) y las Fiscalía General, y la Procuraduría y la Defensoría del pueblo **sean garantes en la investigación de los hechos,** y para que en compañía de las autoridades indígenas se propensa por el restablecimiento de derechos.

Del mismo modo, instaron a las comunidades a **no guardar silencio frente a estos hechos y se denuncie culquier tipo de violencia**, y particularmente aquellas que atentan contra los derechos fundamentales de los menores.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](https://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](https://bit.ly/1ICYhVU)[.]{.s1}

 
