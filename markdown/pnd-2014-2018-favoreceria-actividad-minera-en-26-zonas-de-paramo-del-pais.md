Title: PND 2014 - 2018 favorecería actividad minera en 26 zonas de páramo del país
Date: 2015-04-23 11:30
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Anglo Gold Ashanti, Congreso, Ecominas Paz del Río, Grey Star, hidrocarburos, Iván Cepeda, Mineria, páramos, paz, Plan Nacional de Desarrollo 2014 - 2018, Polo Democrático Alternativo
Slug: pnd-2014-2018-favoreceria-actividad-minera-en-26-zonas-de-paramo-del-pais
Status: published

##### Foto: [www.olapolitica.com]

<iframe src="http://www.ivoox.com/player_ek_4397164_2_1.html?data=lZimmZaaeI6ZmKiakp2Jd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0NiY1dTSb83j1JDay8jTt4zYxtGYsrOob5OkkpmalJWVfIzl1sqYy9eJh5SZoqnO0JDJsozX0NOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Iván Cepeda,  senador del Polo Democrático Alternativo] 

El senador del Polo Democrático Alternativo, Iván Cepeda,  realizó el pasado viernes, 17 de abril, una audiencia pública donde se denunció que **en el Plan Nacional de Desarrollo “2014-2018”, existe un decreto que impediría parar la actividad minera,** que tiene unos 347 títulos en 26 zonas de páramo del país.

En el general el PND, tiene una serie de disposiciones que indican un fortalecimiento y una expansión muy significativa del proyecto minero-energético en Colombia, particularmente llama la atención un parágrafo escondido, del Plan, el decreto 1 del Artículo 159 que se dice que *“las actividades para la exploración y explotación de recursos naturales no renovables que cuenten con contrato y licencia ambiental o con el instrumento de control y manejo ambiental equivalente, **que hayan sido otorgados con anterioridad al 9 de febrero de 2010 para las actividades de minería, o con anterioridad al 16 de junio de 2011 para la actividad de hidrocarburos, respectivamente, podrán seguir ejecutándose hasta su terminación,** sin posibilidad de prórroga y estarán sujetas a un control, seguimiento y revisión por parte de las autoridades mineras, de hidrocarburos y ambientales, en el marco de sus competencias y aplicando las directrices que para el efecto defina el Ministerio de Ambiente y Desarrollo Sostenible’’.*

Para Cepeda, este parágrafo, significaría “dar un cheque en blanco de legitimización a la explotación minera en los páramos”, lo que causaría un **desastre a la riqueza hídrica,** de la que muchas poblaciones sustentan su fuente de agua.

Serían tres las compañías internacionales que se verían beneficiadas por este decreto, **Anglo Gold Ashanti, Grey Star, y Ecominas Paz del Río.** “Estamos ante una entrega de nuestra soberanía y riqueza hídrica al gran capital transnacional en zonas de paramo”, expresa el senador.

De acuerdo Cepeda, la próxima semana se cuestionará la situación del PND “de una manera muy franca y abierta”, por lo que según el senador del **Polo Democrático Alternativo,** este partido **ha preparado más de cien artículos para contrarrestar la carga neoliberal que tiene el Plan.**

Así mismo, Iván Cepeda, afirma que la ciudadanía debe prestar atención a este tema, y además deberá movilizarse en contra de la minería y en general del Plan que, pese a que el gobierno afirma que se trata de una herramienta en favor de la paz, realmente no existe ningún tipo de estrategia para apoyarla, y en cambio esta palabra es “decorativa pero no tiene contenido”, dice Cepeda.
