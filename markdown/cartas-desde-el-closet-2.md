Title: A mi querida hermana - Cartas desde el Closet
Date: 2017-09-17 18:22
Category: Cartas desde el Closet, Opinion
Tags: comunidad gay, Comunidad LGBTI
Slug: cartas-desde-el-closet-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cartas-desde-el-closet.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: chiguy66 

###### Agosto 2017 

[Te quiero contar que para mí ha sido muy difícil todo esto. Yo lo descubrí por accidente un día que limpiaba el armario de mi hija. Allí ella guardaba una caja con varias cartas, mensajes y regalos de la que yo creía que era su “mejor amiga”; pero todas decían: te amo, te extraño… y otras cosas que me da pena y mucho dolor expresar.]

[No entendía que pasaba. Esto no podía estar pasándome a mí. ¿A mi hija le gustaban las mujeres? ¿Tenía una “amiga”? ¿Cómo era posible que pasara eso en mi casa? Empecé a pesar que no debía haberla dejado salir, darle tanta libertad, meterla en un colegio femenino, dejarla que se pusiera pantalones en vez de faldas, o dejarla jugar con balones  porque no le gustaban las muñecas.]

[¿Sería que mi hija se iba a volver hombre? ¿Era un castigo de Dios por haber incumplido alguno de sus mandamientos? Me sentí muy culpable, desorientada, perdida y muy sola. No podía decírselo a nadie, era demasiada la vergüenza que sentía y estaba segura que se iban a burlar de mí; me iban a decir que había sido una mala madre; tal y como yo lo pensaba. No iba a aceptar por ningún motivo que mi hija no estuviera con un hombre, no tuviera hijos, no tuviera la bendición de Dios.]

[Enfrenté a mi hija y desde allí comenzaron los problemas. Comencé a controlar sus tiempos, le pedí que botara todo eso y que se alejara de esa vieja. Su amiga nunca volvió a la casa.]

[A pesar de esto, esperaba que cuando entrara a la universidad y conociera muchachos, volviera a ser una persona normal. Recé mucho para que eso pasara. Por ese tiempo decidí volver con más frecuencia a la iglesia. Todos los domingos iba y siempre pedía lo mismo: “señor haz que mi hija deje sus malos pasos y sea una persona normal”. Pedí mucho por eso pero no pasó.]

[Con el paso del tiempo ella se fue alejando de mí, no me contaba sus cosas, lo que sentía; además perdió su fe, abandonó a Dios. Finalmente, ella se fue de casa. Yo no la saqué, pero no podía permitir que mi hija tuviera una relación en mi casa como ella quería.]

[Han pasado los años, ella es profesional, tiene un buen trabajo y puedo decir que es una buena hija conmigo y con su papá. Está viviendo con su amiga, llevan varios años juntas y en ese tiempo, ha sido su fiel compañera; la ha cuidado y respetado. Han construido muchas cosas juntas. No te niego que para mí es raro y al principio era muy difícil, pero para mi esposo y para mí ha sido más fuerte el amor de padres y por eso hemos permitido que vengan a nuestra casa.]

[Hermana, a pesar que para mí sigue siendo muy difícil yo sigo pidiendo a Dios por ella; me duele tu actitud hacia mi hija y su amiga. La manera en la que te expresas hacia ellas no es la adecuada. No pretendas sacarlas de mi propia casa, comprendo que te genera incomodidad como a mí me la generó en otro momento, pero te pido por favor que te abstengas de opinar sobre la manera en la que yo debería tratarlas. Esto te lo pido con mucho respeto porque te quiero, y espero que podamos seguir con nuestra buena relación, también porque sé, en carne propia, que no es fácil de entender y por eso preferiría el menor escándalo posible.]

[En mi interior sigo buscando la respuesta. Un caluroso abrazo]

#### [**[Parche para la vida, ](https://archivo.contagioradio.com/cartas-desde-closet/)6**] 

------------------------------------------------------------------------

###### [Esta texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida] 
