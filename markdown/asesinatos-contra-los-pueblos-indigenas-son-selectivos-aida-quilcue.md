Title: Asesinatos contra los pueblos indígenas son selectivos: Aida Quilcué
Date: 2019-10-22 17:15
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinatos, Cauca, indígenas, Lideres
Slug: asesinatos-contra-los-pueblos-indigenas-son-selectivos-aida-quilcue
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Los pasados 19 y 20 de octubre, cuatro integrantes de los pueblos indígenas fueron asesinados en Quindío, Cauca y La Guajira, elevando la cifra de homicidios contra integrantes de los pueblos ancestrales a 117. Para la Organización Nacional Indígena de Colombia (ONIC), los asesinatos responden a una forma sistemática de atacar la autoridad y autonomía de los pueblos originarios, ante lo cual se debe responder con la unión de todo el pueblo colombiano en defensa de la vida. (Le puede interesar:["Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca"](https://archivo.contagioradio.com/tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca/))

### **"El ataque es a la autonomía, a la autoridad y al ejercicio de control territorial"** 

Aida Quilcué, consejera de derechos humanos de la ONIC, afirmó que desde esta organización están preocupados ante el avance de la **"sistematicidad y el genocidio de los pueblos indígenas"**, sobre todo en departamentos como Chocó, Antioquia, Nariño, Cauca "y ahora vemos que se extiende a otras regiones como Quindío y La Guajira". La Líder afirmó que **los ataques están dirigidos contra autoridades, líderes representativos y guardias, razón por la cuál considera que se está buscando atacar "la autonomía, la autoridad y el ejercicio de control territorial**" que realizan los pueblos indígenas.

Quilcué dijo que se debe analizar más allá de la pérdida de vidas, puesto que detrás de ello, **"vienen por los derechos que hemos conquistado para los pueblos indígenas y el resto de la sociedad"**. La Consejera manifestó que los pueblos indígenas sienten el respaldo del país, que ha rechazado las acciones violentas en su contra, en parte porque lo que están viviendo sectores como el de estudiantes, trabajadores, líderes sociales y campesinos "tiene que ver también con la vulneración de derechos humanos". (Le puede interesar:["Lilia García, secretaria del Cabildo Awá fue asesinada en Nariño"](https://archivo.contagioradio.com/lilia-garcia-secretaria-del-cabildo-awa-fue-asesinada-en-narino/))

### **En contexto: 117 líderes indígenas asesinados durante el Gobierno de Iván Duque** 

Según la ONIC, en el último año han sido asesinados 117 líderes indígenas, los últimos casos son los de[Oneida Epiayú en La Guajira y Constantino Ramírez, en Quindío](https://archivo.contagioradio.com/con-oneida-epiayu-y-constantino-ramirez-son-mas-de-115-indigenas-asesinados-en-el-ultimo-ano/). Mientras Epiayú era una reconocida líder Wayúu que había denunciado hechos de corrupción en su departamento, Ramírez era reconocido por ser defensor del territorio y fundador de la organización indígena ORIQUIN. (Le puede interesar:["66% de los pueblos originarios está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43539740" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43539740_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
