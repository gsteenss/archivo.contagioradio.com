Title: La doble opresión del pueblo afro-palestino
Date: 2017-08-11 11:00
Category: Onda Palestina
Tags: aparheid Israel, BDS, Palestina
Slug: afro-palestino-israel-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/afropalestinos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:*[aljazeera.com]* 

###### 8 Ago 2017 

Una parte de la población palestina es descendiente de personas que vinieron del África, muchas de las personas llegaron como peregrinas religiosas durante el mandato británico de Palestina y otras fueron parte del movimiento de resistencia Palestina desde el establecimiento de Israel en 1948. Las que llegaron más recientemente lo hicieron como voluntarias con el ejército egipcio para luchar contra Israel durante la guerra de 1967.

Abdallah Balalawi, de 16 años, dijo a Al Jazeera: “en Jerusalén es difícil que no te detengan". Claro, para él, un afro-palestino, es mucho más difícil. Abdallah dice “tengo que estar conciente de mi apariencia, hasta de mi forma de caminar, para poder evitar la sospecha de los israelíes”. Abdallah es uno de al menos 350 afro-palestinos de Nigeria, Chad, Senegal y Sudán que residen en el Barrio Musulmán de la Ciudad Vieja de Jerusalén, adyacente al complejo de la Mezquita Al-Aqsa. A este barrio solo se puede llegar a través de un puesto de control de la policía israelí donde los agentes interrogan a cualquier persona que no pertenezca a la comunidad local.

El primo de Abdallah, Jibrin, solo tiene 17 años pero ya ha sido detenido cinco veces por las fuerzas israelíes, sobre todo por acusaciones de que lanzó piedras contra la policía y los oficiales militares israelíes. Mientras él y sus amigos enfrentan el mismo hostigamiento que otros palestinos, dijo, a veces experimentan "doble racismo", tanto por ser palestinos como por tener piel oscura.

Jibrin cuenta que los soldados siempre lo insultan o interrogan cada vez que pasa cerca de ellos, y denuncia como "tratan de provocarme para que haga algo por lo que podrían meterme en problemas". Señala que ha sido golpeado varias veces por policías y soldados israelíes cuando ha sido detenido. Añadió que la mayoría de los de su generación tienen experiencias similares.

Eso ha inspirado en la población afro-palestina el impulso por resistir de forma creativa: hablan por ejemplo del baile tradición Dabke. La joven afro-palestina Shaden Qous con una sonrisa en su cara afirma que "Es una manera creativa de resistir, mantiene nuestras tradiciones palestinas vivas, y todos sabemos que a los israelíes no les gusta." Es solo un ejemplo, entre muchos posibles, de las diversas formas de resistencia no violenta que practica el pueblo palestino.

<iframe id="audio_20273610" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20273610_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
