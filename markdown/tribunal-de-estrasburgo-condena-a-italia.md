Title: Tribunal de Estrasburgo condena a Italia
Date: 2015-04-07 16:14
Author: CtgAdm
Category: Judicial, Otra Mirada
Tags: Arnaldo Cestaro, Escuela Díaz Génova, Estrasburgo condena Italia por torturas, G8 Carlo Giuliani, Italia, Tribunal de Estrasburgo
Slug: tribunal-de-estrasburgo-condena-a-italia
Status: published

###### Foto:Mueralainteligencia.com 

**Italia** ha sido **condenada por el tribunal de Estrasburgo** para los derechos humanos por **torturas en la Escuela Díaz** en el marco de las protestas contra **el G8 en Génova**, y por no contemplar en su jurisprudencia leyes que condenen firmemente la tortura como práctica violatoria de los DDHH.

En 2001, en el marco de las protestas contra la cumbre del G8 en Italia, el **Foro social de Génova organizó una jornada de resistencia** contra las políticas neoliberales y contra el capitalismo globalizado. Activistas venidos de todas partes del mundo y de distintas organizaciones sociales mundiales se hospedaban en una escuela llamada Díaz.

**El 21 de Julio de 2001 la policía allanó la escuela torturando y apaleando a los miembros de las organizaciones sociales** que se encontraban durmiendo en el lugar, también se practicaron detenciones, aunque después de los juicios todas las sentencias reconocieron la inocencia de los mismos.

Una de las personas torturadas fue **Arnaldo Cestaro, de 62 años**, a quién la **policía rompió** **10 costillas y un brazo**, por lo que tuvo que ser operado de urgencia.

El Tribunal de DDHH de Estrasburgo condena a Italia por no tipificar este caso como tortura, pero también añade que la **legislación vigente no permite tipificar la tortura como delito y tampoco el procedimiento necesario para su identificación**, es decir no brinda de los recursos a la justicia necesarios para su judicialización.

En la movilización de la cumbre contra el G8 en Génova, también **murió el activista Carlo Giuliani, a manos de la policía**. Fue una de las más grandes movilizaciones del movimiento antiglobalización mundial y marco un antes y un después en la desproporcionada represión ejercida por el estado Italiano.
