Title: Dos militares irán a juicio por asesinato de joven en 2008
Date: 2018-12-11 17:27
Author: AdminContagio
Category: DDHH, Sin Olvido
Tags: altos mandos, Edgar Cuello, ejercito, Falsos Positivos Judiciales, Fuerza Pública
Slug: dos-militares-iran-a-juicio-por-asesinato-de-joven-en-2008
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/edgar-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fo][Foto: Colectivo de Abogado José Alvear Restrepo] 

###### [11 Dic 2018]

La Fiscalía quinta, de la Unidad de Derechos Humanos formulo una acusación en contra del soldado profesional José Alberto Pizón, y el teniente coronel Pedro José Pachón Contreras, quien actualmente se desempeña como jefe del Batallón Juan del Corral en Rionegro, Antioquia; **por el homicidio y la desaparición forzada de Edgar Fabian Cuello, asesinado el 4 de enero de 2008**.

Cuello se desempañaba como albañil en el barrio Santa Fe, en Bogotá y fue presentado como muerto en combate por integrantes del Batallón Silva Plazas, de la primera Brigada, con sede en Duitama, que habían manifestado que era un delincuente con antecedentes penales. Sin embargo, la **Fiscalía logró establecer que el joven no disparó el arma con la que fue encontrado, y que no se trataba de un combatiente, sino de una persona humilde**.

De acuerdo con el abogado Eduardo Carreño, defensor de las víctimas, la participación de los dos integrantes de la Fuerza Pública fue  clara y contundente, "dado que el soldado Pinzón Jaimes trajo el dinero que suministró Pachón Contreras para pagarle aquí al reclutador, que entregó a Edgar Fabián para que lo asesinaran y lo llevaran hasta Duitama" .

### **Los altos miembros de la Fuerza Pública que podrían estar implicados** 

El pasado 10 de febrero de 2017, el Juzgado segundo penal del Circuito de Duitama, condenó al ex-suboficial del Ejército sargento primero Oscar Darío Goyes Polo, integrante del Batallón Silva Plazas, a 211 meses (17.5 años) **de prisión por el homicidio y porte de armas, en el caso de la ejecución de Cuello**. (Le puede interesar: ["Queremos saber quién ordenó los falsos positivos: Madres de Soacha"](https://archivo.contagioradio.com/queremos-saber-quien-ordeno-los-mal-llamados-falsos-positivos-madres-de-suacha/))

Goyes señaló durante el juicio la participación de altos mandos, como el actual general Jaime Rivera Jaimes y el actual Brigadier general, comandante de la II División del Ejército, Fernando Rojas Espinoza. (Le puede interesar: ["Informe probaría responsabilidad de General (R) Torres Esacalante en Falsos Positivos"](https://archivo.contagioradio.com/el-antagonista-del-informe-ni-delincuentes-ni-combatientes/))

El abogado Carreño, del Colectivo de Abogados "José Alvear Restepo", quien ya solicitó a la Fiscalía investigar la responsabilidad del General Jaimes y el Brigadier General Rojas en este asesinato, señaló que “las investigaciones deben reconocer el aparato organizado de poder y el concierto para delinquir que operaron detrás de este crimen, para aprovechar este tipo de declaraciones de un militar que participó de los hechos, y que son una situación excepcional para profundizar en esta investigación”.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
