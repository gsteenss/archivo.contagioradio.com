Title: Razones por las que Carrasquilla no es idóneo para decidir sobre Santurbán
Date: 2019-10-08 16:24
Author: CtgAdm
Category: Ambiente
Tags: Ambiente, Min. Carrasquilla, Páramo de Santurbán, páramos, Presidente Duque, Santander
Slug: razones-de-los-santandereanos-no-a-carrasquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo] 

Este jueves 4 de octubre el presidente Duque anunció que el encargado de dar el concepto técnico de licenciamiento del Proyecto Minesa será el ministro de hacienda Aberto Carrasquilla, ahora Ministro *Ad hoc* de Ambiente; acción que fue de inmediato rechazada por grupos ambientalistas y gran parte de la comunidad Santandereana.

> [\#Atención](https://twitter.com/hashtag/Atenci%C3%B3n?src=hash&ref_src=twsrc%5Etfw) nombran a Alberto Carrasquilla para dar concepto técnico en licenciamiento del proyecto de Minesa.
>
> El mismo que endeudó al país con los bonos del agua[@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) tendrá que enfrentarse con la fuerza del pueblo santandereano que no dejará destruir su fuente de agua! ? [pic.twitter.com/WskTObiZVV](https://t.co/WskTObiZVV)
>
> — Comité Santurbán (@ComiteSanturban) [October 7, 2019](https://twitter.com/ComiteSanturban/status/1181331395639173120?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Este nombramiento de Carrasquilla se da luego que Ricardo Lozano, Ministro actual de ambiente, se declarara impedido para realizar acciones ante la Autoridad Nacional de Licencias Ambientales (ANLA). Mayerly López integrante del Comité por la defensa del Páramo de Santurbán, señaló “Carrasquilla no tiene conocimientos en temas ambientales y además negoció con el agua que necesitan comunidades en el país y ahora Duque le entrega el poder de decidir sobre el agua de Santander”.

Ante esto López resaltó varios problemas que traería el nombramiento de Carrasquilla; los dos principales son, el favorecimiento de intereses empresariales reconociendo que como Ministro de Hacienda su prioridad siempre va ser la economía y no los daños ambientales de estas intervenciones, y por otro lado las delimitaciones que tiene Santurbán con el gran número de asentamientos humanos que llevan siglos viviendo allí.

### ¿Por qué dicen NO a Carrasquilla? 

López como vocera y defensora del páramo hace un recuento de los casi 10 años en los que han luchado para que la defensa de **Santurbán** sea un tema recurrente en la agenda de Colombia; y no solo por su importancia ambiental sino por las delimitaciones que podrían generar una enorme afectación en las comunidades, dado que el páramo es la principal fuente de agua en la región y de la qu se surten acueductos tan importantes como el de Bucaramanga que alimenta más de 2.5 millones de personas.

Así mismo la ambientalista resalta que, “el presidente no puede desconocer las voces de las más de 150.000 personas que salimos el pasado mes de mayo a exigir el gobierno que se le niegue la licencia ambiental a este proyecto”. (Le puede interesar:[Grupos ambientales buscan prorrogar delimitación del Páramo de Santurbán](https://archivo.contagioradio.com/grupos-ambientales-buscan-prorrogar-delimitacion-de-paramo-de-santurban/))

Ante esto vale la pena destacar que el país lleva un proceso de años en apelaciones, modificaciones y protestas en contra de estas delimitaciones, las cuales se presentaron desde el año 2014 y han recaído alrededor de 35 complejos de paramos en el país, siendo Santurbán uno de los primeros.(Le puede interesar:[Tres desafíos para defender el páramo de Santurbán](https://archivo.contagioradio.com/tres-desafios-para-defender-el-paramo-de-santurban/))

Fallos que fueron apelados principalmente por las comunidades, que defendían su participación en estas delimitaciones, ya que habían sido dejados por fuera de las consultas y concertaciones territoriales, por estos antecedentes y como señaló reiteradamente López, “necesitamos a una persona capacitada técnicamente y éticamente que regule y proteja el páramo”.

Y aunque las razones que cuestionen el nombramiento de Carrasquilla sean cada vez más, "y aunque no se pueda hacer nada para revocar este nombramiento los santanderinos no vamos a aceptar a Carrasquilla por eso estamos convocando a los defensores, ambientalistas y organizaciones a una reunión extraordinaria”, agregó López; junta que se unirá a una movilización inicialmente en Bucaramanga y que invita a las demás regiones a rechazar esta acción.

> [\#TransparenciaBGA](https://twitter.com/hashtag/TransparenciaBGA?src=hash&ref_src=twsrc%5Etfw)
>
> ¡UNA AFRENTA AL PUEBLO BUMANGUES! Carta del Alcalde Manuel Francisco Azuero al Presidente Duque sobre sus decisiones que ponen en peligro el Páramo de Santurbán. [pic.twitter.com/KYqZNiVoHr](https://t.co/KYqZNiVoHr)
>
> — AlcaldiaBGA (@AlcaldiaBGA) [October 8, 2019](https://twitter.com/AlcaldiaBGA/status/1181571221605031936?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_42880422" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42880422_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
