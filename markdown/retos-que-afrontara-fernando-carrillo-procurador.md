Title: Los grandes retos que afrontará Fernando Carrillo, nuevo Procurador
Date: 2016-10-28 13:32
Category: Nacional, Política
Tags: Alejandro Ordoñez, Derechos, ex procurador, Fernando Carrillo, Nuevo Procurador Nuevo procurador, Procurador, procuraduria
Slug: retos-que-afrontara-fernando-carrillo-procurador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Procurador-nuevo-e1477679295210.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Portafolio 

###### 28 Oct de 2016 

Con una votación que ha sido considerada por muchos como histórica, **Fernando Carrillo Flórez es el nuevo Procurador de Colombia. La cifra de votación conocida fue 92 votos a favor y 3 en blanco.** Luego de un periodo que ha sido catalogado po muchas organizaciones como “sombrío” en la Procuraduría, por cuenta de las diversas actuaciones del ex Procurador Alejandro Ordoñez, los retos que se avecinan para Carrillo son amplios.

**Alberto Yepes,** abogado y defensor de derechos humanos de la Coordinación Colombia, Europa, Estados Unidos, en entrevista con Contagio Radio **aseguró que la Procuraduría debe volver a ser la institución que vela por los derechos humanos y que es ese el gran reto que tiene Carrillo durante su periodo** “confiamos que el nuevo Procurador este del lado de las víctimas y nos ayudé a luchar contra la impunidad” aseveró.

Por su parte, los movimientos sociales, defensores de derechos humanos y en general **las víctimas han manifestado que guardan esperanzas en el nuevo Procurador, pese a algunos cuestionamientos** que ha recibido Fernando Carrillo por hechos ocurridos mientras se desempeñaba como ministro de justicia en el periodo de César Gaviria.

**Acabar el nido de corrupción en el que se convirtió la Procuraduría, cesar la persecución política contra distintos sectores, las violaciones que se realizaron contra sectores de las minorías, las mujeres, la población LGBT son los grandes retos** con los que Carrillo recibe esta cartera. Adicional  estos temas, Yepes manifiesta que es preciso “volver a encarrilar la Procuraduría en la defensa activa de los derechos humanos, que fue un canal que se perdió en los 12 años de nefasto mandato del señor Ordoñez”. Le puede interesar: [Procurador Ordoñez “no estaba a la altura del cargo”](https://archivo.contagioradio.com/procurador-ordonez-no-estaba-a-la-altura-del-cargo/)

Por su parte, Yepes afirmó que se espera que en la nueva fase que se acerca del **proceso de paz el nuevo procurador** “sea un defensor de los derechos de las víctimas de los sectores más marginados y **ayude a aclarar la responsabilidad disciplinaria y penal de quienes han tenido responsabilidades en las graves violaciones de derechos humanos”.**

Según el jurista y defensor de derechos humanos, podría hablarse de que aproximadamente en 6 meses habría algún tipo de avance en dicha cartera. Y aseveró, que **la mejor estrategia que puede usar Carrillo es “tener gestos de buena voluntad como por ejemplo, dando apoyo decidió al proceso de paz con el ELN y la conclusión con las FARC** o abriendo los archivos de la Fiscalía a la comisión de la verdad y trasladando procesos a la jurisdicción especial de paz sobre graves violaciones a los derechos humanos, en que han estado implicados altos agentes del estado y que están en impunidad” concluyó.

**¿Qué le espera a Fernando Carrillo en la Procuraduría?**

Para la representante a la Cámara, Ángela María Robledo, el nuevo procurador deberá verse enfrentado a un contexto complejo en el que se encuentran diversos actores como el Centro Democrático y Conservadores, quienes sumaron una alta votación para Carrillo.

“Fernando Carrillo le va a tocar enfrentar toda esta campaña de ciertas iglesias cristianas, que más que iglesias parecen sectas, la ideología de género, **le va a tocar enfrentar debates contundentes sobre el tema del aborto, interrupción voluntaria del embarazo y todo lo que se refiere al tema de la paz** que debe ser la tarea prioritaria de un procurador” agregó la representante Robledo.

En la entrevista, Ángela **Robledo también hace referencia al tema de las mujeres en la terna y la casi nula participación** de ellas en la posibilidad de ocupar este cargo “hay que tener en cuenta que siendo mayoría nos tratan como minoría” aclaró al respecto.

Según la cabildante, **los gestos que emprenda el nuevo procurador harán que se pueda valorar su gestión en dicha cartera** “será muy importante demostrar cuáles son los criterios y quienes serán las personas que lo van a acompañar en las procuradurías delegadas que son muy importantes” expresó. Le puede interesar: [4 maneras de politizar la procuraduría general de la nación según Sintraproan](https://archivo.contagioradio.com/4-maneras-de-politizar-la-procuraduria-general-de-la-nacion-segun-sintraproan/)

<iframe id="audio_13521845" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13521845_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_13522082" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13522082_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
