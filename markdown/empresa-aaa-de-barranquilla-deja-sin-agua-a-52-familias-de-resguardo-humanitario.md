Title: Empresa AAA de Barranquilla deja sin agua a 52 familias de espacio Humanitario
Date: 2015-02-12 20:42
Author: CtgAdm
Category: Nacional, Resistencias
Tags: Agua, Barranquilla, Espacio Humanitario, Triple A
Slug: empresa-aaa-de-barranquilla-deja-sin-agua-a-52-familias-de-resguardo-humanitario
Status: published

##### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4075592_2_1.html?data=lZWkl5qddo6ZmKiakpuJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw5DHs9Pohqigh6eXb8bgjMbU18aPpYzcwsfW1sbSuMbnjMnSzpCpt9HVxM7cja3ZscLiytmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Dary Rosi habitante del Refugio Humanitario] 

**La empresa de aguas de Barranquilla Triple A, cortó el agua a la comunidad del Espacio Humanitario El Mirador,** en Tamarindo, Atlántico. Son **52 familias las afectadas** por las actividades de la empresa de acueducto,  alcantarillado y aseo de Barranquilla.

Luz Dary Rosi, habitante de el resguardo humanitario "El Mirador", afirma que la comunidad se encuentra en conversaciones con el Estado para que se les garanticen sus derechos, pero la acción de la empresa viola el derecho humano al agua. Para Luz Dari, es claro que la empresa pretende cobrar el servicio sin tener en cuenta su condición de víctimas y sin respetar los conductos regulares de interlocución con el Estado “**lo que quieren es cobrarnos... y nosotros somos pobres”,** puntualizó la mujer.

Precisamente, los funcionarios de la empresa Triple A, pretenden instalar a la fuerza una máquina, con la que se mediría el consumo del agua. El problema, es que **no se ha abierto un escenario de diálogo y acuerdos con las familias** afectadas.

En la actualidad, no existe un abastecimiento de agua que garantice un mínimo vital del líquido a las personas del Tamarindo. Por su parte, Luz Dari Rosi, expresó **“no vamos a permitir quedarnos sin agua” y agregó que “los niños están pasado necesidades.**

Hasta el momento, solo se ha concretado una reunión con la personería de Tamarindo, pero al momento no se ha llegado a ningún acuerdo para restaurar el servicio del agua.
