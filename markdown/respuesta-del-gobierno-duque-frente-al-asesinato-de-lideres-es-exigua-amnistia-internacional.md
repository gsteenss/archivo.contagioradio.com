Title: Disminuir crímenes contra líderes no es prioridad de gobierno Duque: Amnistía Internacional
Date: 2019-02-08 13:21
Author: AdminContagio
Category: DDHH, Líderes sociales, Política
Tags: Amnistía Internacional, asesinato de líderes sociales
Slug: respuesta-del-gobierno-duque-frente-al-asesinato-de-lideres-es-exigua-amnistia-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/DuF5omeXcAEy7EP-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 08 Feb 2019

La directora para las Américas de **Amnistía Internacional, Érika Guevara**, se pronunció sobre el aumento de homicidios de líderes sociales en América Latina, y la evidente falencia del Estado en la protección de quienes defienden los derechos de las comunidades,  en particular se refirió al caso de Colombia donde han muerto **566 líderes sociales desde 2016 hasta el 10 de enero de 2019** y señaló que las políticas de Iván Duque no tienen dentro de sus prioridades la disminución de estos asesinatos.

**“Lo que vemos es una respuesta exigua por parte del Estado colombiano. El mecanismo de protección de los defensores de DD.HH. no cumple con las expectativas ni responde al contexto tras la firma del acuerdo”** indicó Guevara a la agencia de noticias **Anadolu** y agregó que observan con preocupación cómo llegan gobernantes al poder con  promesas simplistas y  respuestas “inefectivas y negligentes”.

Guevara expresó que los defensores de la tierra y los recursos naturales como los pueblos indígenas, afrodescendientes y comunidades son quienes más han estado expuestos a la violencia al enfrentarse “a los intereses económicos y políticos de una minoría que ha sido protegida por los estados” poniendo los intereses económicos y políticos de unos cuantos por encima de la mayoría.

Frente a la impunidad en Colombia aseguró que el sistema judicial del país **“le ha fallado a todas las comunidades afectadas”** además agregó que aunque la Fiscalía sostenga que ha avanzado en por lo menos el 50% de los casos de defensores asesinados, los resultados no se ven reflejados ni representan un avance real.

También señaló que aunque no existe un índice en particular, con la documentación de los casos recolectados, en el caso de Colombia según información recolectada por organizaciones como Indepaz o Somos Defensores, el índice de impunidad frente al asesinato de líderes sociales esta alrededor del 95%.

###### Reciba toda la información de Contagio Radio en [[su correo]
