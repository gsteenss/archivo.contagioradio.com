Title: Hay fallas pero hay voluntad para implementar el acuerdo de paz: Unión por la Paz
Date: 2017-04-21 13:07
Category: Entrevistas, Paz
Tags: Acuerdos de La Habana, Fast Track
Slug: hay-fallas-pero-hay-voluntad-para-implementar-el-acuerdo-de-paz-union-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/union-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: semana] 

###### [21 Abr 2017]

La colectividad Unión por la paz, entregó su **primer informe sobre la implementación del acuerdo de paz en el que hace un balance positivo pero realista de los incumplimientos**, los retrasos y las perspectivas de superar los errores. Uno de los principales puntos que resalta el colectivo es que hay voluntad de las partes hay comprensión en las dificultades.

El periodista Ramón Jimeno, integrante de Unión por la Paz, junto al expresidente Ernesto Samper, León Valencia y Ariel Ávila, elaboraron un informe en el que dan cuenta de que desde el mes de Noviembre, cuando se firmó el acuerdo, **se han presentado 31 asesinatos de líderes sociales y 69 amenazas**, sin embargo Jimeno resalta que esta cifra ha disminuido debido que la presión social ha hecho que el gobierno enfrente, en cierta medida, esa situación.

Además el periodista e investigador social resalta cuatro temas en los que también hace énfasis el informe y que tienen especial relevancia en el momento actual, por ejemplo los avances en la construcción y **entrega de las Zonas Veredales, los procesos de amnistía para integrantes de las FARC, el Fast Track en el congreso y la presencia de la sociedad** para apoyar la implementación. Le puede interesar:["Abril el plazo final para completar la implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/abril-el-mes-para-completar-la-implementacion-de-los-acuerdos-de-paz/)

### **Las Zonas Veredales Transitorias de Normalización** 

Uno de ellos es el cumplimiento de lo pactado en torno a las Zonas Veredales de Normalización, a ese ramón Jimeno reconoce que **hay incumplimientos por parte del gobierno y por el contrario un cumplimiento cabal por parte de las FARC**. Sin embargo, se han comprendido las dificultades y se están corrigiendo los problemas que se han presentado como lo apartado de las regiones y la inexistencia de infraestructura así como la ausencia de presencia estatal en las zonas.

**No es fácil construir una vía terciaria completamente nueva en una zona apartada**, resalta el periodista, y tampoco es fácil llegar con el Estado de un momento a otro a lugares en que históricamente no ha habido presencia. Le puede interesar: ["Hay un 80% de incumplimientos por parte del Gobierno en Zonas veredales: FARC"](https://archivo.contagioradio.com/gobierno-ha-incumplido-en-un-80/)

Recientemente el gobierno se había comprometido a entregar las zonas comunes de 10 zonas veredales, sin embargo, Iván Márquez, jefe del equipo de paz de las FARC afirmó, a través de Twitter, que parecía que el gobierno no estaba en capacidad de cumplir ahora ni próximamente. “**No se cumplió con la finalización de las obras en las ZVTN anunciada para mediados de abril. Siguen y seguirán inconclusas**” afirmó.

### **La amnistía no avanza** 

Según un reciente informe de la Coalición Larga Vida a las Mariposas, desde la entrada en vigencia de la Ley se han presentado **724 peticiones de Amnistía de Iure, Libertades Condicionadas y Traslados a ZVTN las cuales inicialmente fueron negadas o suspendido su trámite** hasta la expedición del Decreto 277 de 2017, a partir del cual se han concedido 138 amnistías de iure, 22 libertades condicionadas y 38 traslados a ZVTN, negándose 14 libertades condicionadas, el resto de peticiones se encuentra en trámite.

Según explica Ramón Jimeno, este tema está atravesado por varias situaciones, una la **lentitud histórica de las instituciones encargadas de resolver los temas de la justicia**, el paro judicial en lagunas regiones y posibles los celos porque muchos altos magistrados y jueces consideran que la JEP los desplazó. Le puede interesar: ["Asesinado guerrillero de las FARC que había sido beneficiado con Ley de Amnistía"](https://archivo.contagioradio.com/asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia/)

Sin embargo, en medio de esta situación, el gobierno emitió un decreto con el que, se agilizó, aunque de manera insuficiente, la aplicación de esta ley. **Está en manos de los jueces y de la agilidad de la puesta en marcha de la JEP y sus instituciones, que la amnistía se aplique de manera pronta y eficaz.**

### **El Fast Track dejó de ser una vía rápida** 

Aunque cuando el congreso refrendó el acuerdo de fin del conflicto del Teatro Colón, se afirmó que la vía rápida podía reglametar todo el acuerdo con las FARC en 6 meses, ya han trascurrido casí 4 meses y solamente se ha avanzado en parte de la puesta en marcha del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, es decir, falta parte de este punto y **la totalidad de los otros 5 presentes en el acuerdo y que deberían ser tramitados en los próximos dos meses.**

Adicionalmente los temas relacionados con escándalos de corrupción y el ausentismo evidente ha retrasado las votaciones de diversos puntos, demorando aún más el trámite rápido. A esto se suma que **ya está en marcha la campaña electoral y eso pone sobre la mesa una serie de intereses politiqueros** evidentes en la cultura política de los congresistas colombianos. Le puede interesar: ["Los riesgos del Fast Track Ambiental"](https://archivo.contagioradio.com/los-riesgos-del-fast-track-ambiental/)

Ante ese análisis el periodista, integrante de Unión por la Paz, afirma que hay un compromiso del gobierno para la siguiente semana, en que se realizaría una **reunión con los partidos para que se clarifiquen posturas como la de Cambio Radical que dice respaldar el acuerdo**, pero a la hora de votar no se define, igualmente con el partido Conservador.

En ese escenario es que se realizaría la reunión y esperando que, de cara al país**, se defina una postura y se pueda agilizar el trámite del acuerdo en el Congreso**. Incluso existe la posibilidad de que se realice una especie de conclave de congresistas para que se recupere el espíritu del Fast Track.

### **El respaldo y vigilancia de la sociedad sigue siendo clave** 

Una de las situaciones más importantes para que se firmara el acuerdo, se refrendara y ahora se vaya avanzando, ha sido la presión de la sociedad. Jimeno, resalta que la persistencia de movimientos como “Ojo a la paz” y otros que han estado pendientes de las votaciones, ha sido crucial, sin embargo, **estas acciones también deberían dirigirse a presionar al gobierno para que presente ágilmente y apegada al espíritu del acuerdo los proyectos de ley.**

En los próximos meses se espera que Unión por la Paz emita otro informe acerca del avance los compromisos que se han venido pactando entre el gobierno y las FARC. Le puede interesar:["Congreso de Paz una iniciativa para proteger los Acuerdos de Paz".](https://archivo.contagioradio.com/congreso-de-paz-una-iniciativa-para-proteger-los-acuerdos-de-paz/)

<iframe id="audio_18272250" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18272250_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
