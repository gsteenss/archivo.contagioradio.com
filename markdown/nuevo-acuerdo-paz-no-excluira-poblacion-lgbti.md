Title: Nuevo Acuerdo de Paz no excluirá a población LGBTI
Date: 2016-11-03 13:37
Category: Nacional, Paz
Tags: acuerdos de paz ya, implementacion acuerdos, LGBTI
Slug: nuevo-acuerdo-paz-no-excluira-poblacion-lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti_flag-e1478191764857.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FIDH] 

###### [3 Nov de 2016] 

Luego de trece días de conversaciones, el pasado 2 de Noviembre se conoció el comunicado conjunto de las delegaciones de Paz en La Habana y líderes de las principales organizaciones LGBTI, que viajaron a Cuba con el fin de reivindicar los derechos de lesbianas, gays, bisexuales y trans que **fueron víctimas en el conflicto armado colombiano.**

Líderes de las organizaciones LGBTI, manifestaron que es fundamental mantener la transversalidad del enfoque de género en el acuerdo, para **hacer visibles los impactos diferenciados del conflicto armado sobre las mujeres y hombres**; entre estos, las lesbianas, gays, bisexuales, trans e intersex. Le puede interesar: [Internas LGBTI en Medellín son víctimas de discriminación sexual.](https://archivo.contagioradio.com/internas-lgbti-victimas-discriminacion-sexual/)

Además, resaltan que los Acuerdos hasta el momento no han abarcado temas como la definición de familia, matrimonio o adopción por parejas del mismo sexo, así como otros derechos de las personas LGBTI reconocidos en la Constitución Política, pues **no es el espacio pertinente para estas discusiones, que deben darse de forma democrática en las instituciones correspondientes.** Le puede interesar: [Informe da cuenta de victimización LGBTI en el conflicto armado colombiano.](https://archivo.contagioradio.com/informe-da-cuenta-de-victimizacion-lgbti-en-el-conflicto-armado-colombiano/)

Estos fueron los puntos sobre los cuales discutieron en La Habana:

1.  Lesbianas, gays, bisexuales, trans e intersex han sido afectadas desproporcionadamente por la violencia en el marco y con ocasión del conflicto armado, lo cual exacerbó las condiciones de exclusión y discriminación que han sufrido históricamente.

<!-- -->

2.  El Acuerdo de Paz debe mantener transversalmente el enfoque de género. La adopción del enfoque de género no desconoce derechos fundamentales de ninguna población, las colombianas y los colombianos tienen los mismos derechos. El uso de este enfoque en el acuerdo es necesario para hacer visibles los impactos diferenciados del conflicto armado sobre las mujeres y hombres; entre estos, las lesbianas, gays, bisexuales, trans e intersex.

<!-- -->

3.  El Acuerdo de Paz no excluirá a las personas LGBTI y deberá mantener las disposiciones necesarias para asegurar que sus derechos humanos no sean desconocidos en el texto del acuerdo y en su implementación.

<!-- -->

4.  El Acuerdo de Paz no ha abarcado ni deberá abarcar discusiones que no estén vinculadas al origen y desarrollo del conflicto armado, tales como la definición de familia, matrimonio o adopción por parejas del mismo sexo, así como otros derechos de las personas LGBTI reconocidos en la Constitución Política de Colombia y en la jurisprudencia constitucional. Estos temas deberán ser debatidos libremente y en democracia en las instituciones  
   correspondientes.

<!-- -->

5.  Ningún contenido del acuerdo de paz, ni su implementación, podrá contener disposiciones que impliquen la negación, restricción o menoscabo de los derechos fundamentales de ningún colectivo social. Tampoco el Acuerdo de Paz deberá incluir nuevas interpretaciones de derechos fundamentales que tengan ese mismo propósito.

Las organizaciones LGBTI firmantes son:  
CARIBE AFIRMATIVO  
CESAR MENDOZA -REPRESENTANTE DE VÍCTIMAS ARAUCACOLOMBIA  
DIVERSA  
DARLA CRISTINA GONZALEZ –REPRESENTANTE VÍCITMAS  
NARIÑOLGBTI  
POR LA PAZ  
ANGELICA LOZANO-REPRESENTANTE A LA CAMARA  
LIZA GARCÍA REYES  
MAURICIO ALBARRACIN  
NANCY PRADA PRADA  
NIXON PADILLA  
VIVIANA BOHORQUEZ MONSALVE

**DELEGACION DE PAZ FARC-EP**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
