Title: ¡A marchar por la paz para los animales y la naturaleza!
Date: 2016-09-23 13:53
Category: Animales, Voces de la Tierra
Tags: Animales, marcha, Movilización, naturaleza, paz
Slug: a-marchar-por-la-paz-para-los-animales-y-la-naturaleza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14125085_650320668462805_5818675855126368728_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

------------------------------------------------------------------------

###### Foto: Agencia Prensa Rural 

###### [23 Sep 2016]

Este domingo se realiza la **IX Marcha por los derechos de los animales,** desde las 9 de la mañana en la Plaza La Santamaría hasta la Plaza de Bolívar. Esta vez las organizaciones ambientalistas y animalistas saldrán a las calles para que la paz también sea para los animales y la naturaleza.

**“Queremos poner sobre la mesa esas víctimas que históricamente han sido invisibilizadas durante la guerra en Colombia. Toda la riqueza en biodiversidad del país se ha visto afectada por el conflicto armado”**, dice Catalina Reyes, integrante de la Plataforma ALTO, una de las organizaciones que hizo parte de la puesta en marcha de esta movilización, de la mano de Libera Colombia, y la Fundación Franz Weber, quienes a través de la marcha buscan alzar la voz, para que todas las víctimas del conflicto sean reparadas.

Bogotá, será el epicentro de la marcha en Colombia, pero también habrá movilizaciones en Bucaramanga, Popayán, Tunja, Mosquera, Funza, Madrid, San Juan de Pasto, Duitama Vélez y Fusagasugá. Los defensores de los animales se movilizarán usando camisetas blancas y verdes, pancartas, carteleras, instrumentos musicales, máscaras, y muchos llevarán sus animales de compañía.

**La Orquesta Filarmónica Juvenil de Bogotá, recibirá a los caminantes en la Plaza de Bolívar,** y durante todo el recorrido habrá otro tipo de actividades culturales, donde se encontrarán todo personas diversas sin importar religión o ideología política. “Sin duda alguna debemos ampliar nuestra visión de una paz completa”, concluye la integrante de la Plataforma ALTO.

![Marcha animalista](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14322208_571588403042335_6833478832226492873_n.jpg){.alignnone .size-full .wp-image-29741 width="852" height="317"}

<iframe id="audio_13032356" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13032356_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
