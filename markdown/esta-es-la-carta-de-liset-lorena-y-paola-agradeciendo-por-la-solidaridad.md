Title: Esta es la carta de Liset, Lorena y Paola agradeciendo por la solidaridad
Date: 2015-09-11 18:18
Category: DDHH, Nacional
Tags: congreso de los pueblos, Jovenes capturados en Bogotá, Paola Salgado
Slug: esta-es-la-carta-de-liset-lorena-y-paola-agradeciendo-por-la-solidaridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/carta_paola_salgado_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imagen carta 

###### [11 Sep 2015] 

Antes de conocer la decisión de la judicatura Liset, Lorena y Paola, las tres jóvenes mujeres detenidas en el grupo de los 13 jóvenes, escribían esta carta de agradecimiento por la solidaridad recibida durante 2 meses de prisión...

*"11 de Septiembre de 2015*

*Cuando el sol parecía ocultarse, cunado la alegría parecía difuminarse y nuestros sueños desaparecieron apreció entre nosotros la sonrisa amable y solidaria de un sin número de amigas y amigos que desde el 8 de julio se han traducido en fortaleza y aguante para mantener nuestra cabeza siempre en alto durante estos difíciles días en que vivimos en carne propia la injusticia.*

*En este 11 de septiembre rememoramos el sol saliente que cubrió el Chile pueblo, el Chile Allende. Así como en esos días, hoy esperamos que el sol se pose ante nuestra Colombia con una sonrisa eterna que resplandezca y nos acompañe hasta que los sueños de libertad, justicia y paz se vean realizados.*

*Queremos que sepan que nosotros hacemos parte de la sonrisa de ese sol y que sientan que nos mantenemos con la misma tenacidad con la que nos conocen, que nuestra alegría, esa maravillosa temida por los poderosos crece y crece, pues sabemos que ustedes mantienen viva la llama que sol los humildes y solidarios pueden tener, esa llama que no claudica aun cuando la muerte aceche.*

*Aprovechamos esta oportunidad para extenderles nuestro más sinceros agradecimientos por la compañía incondicional que nos han brindado a nosotras y a nuestros familiares, insistimos que su fuerza es nuestra fuerza y que ansiamos llegue pronto el día en que volvamos a gozar de nuestra LIBERTAD.*

*Les llevamos en nuestro pensamiento y en los pasos que damos*

*¡Su fuerza es nuestra fuerza!*

*¡No merecemos estar presos por pensar!*

*¡No estamos todos!*

*Liset, Lorena y Paola"*
