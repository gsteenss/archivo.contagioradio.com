Title: Juez falla a favor de proteger la vida de población reclusa de Cárcel de Villavicencio
Date: 2020-05-04 13:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Cárcel, #covid19, #INPEC, #Pandemia, #Tutela
Slug: juez-falla-a-favor-de-proteger-la-vida-de-poblacion-reclusa-de-carcel-de-villavicencio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Cárcel-de-Villavicencio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 29 de abril, el juez de familia del Circuito de Villavicencio falló a favor de una tutela para garantizar el derecho a la vida y salud de la población reclusa de la Cárcel de Villavicencio. Lugar que se ha convertido en uno de los focos de la pandemia del Covid 19, **con 508 personas contagiadas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el centro de reclusión desde hace un mes, se registró la primera muerte por el virus, que en total ya suma tres personas. Además entre los afectados actualmente hay trabajadores del penal y guardías del INPEC.  (Le puede interesar: «[Rumbo a un genocidio carcelario](https://www.justiciaypazcolombia.com/rumbo-a-un-genocidio-carcelario/)«)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta tutela fue interpuesta por la Comisión de Justicia y Paz, con la finalidad de proteger los derechos a la salud y vida de la población privada de la libertad de ese centro penitenciario.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La emergencia en la Cárcel de Villavicencio
-------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el Juez, en un plazo de 48 horas el director nacional del INPEC, es decir el general Norberto Mojica, deberá gestionar que se realice la prueba del covid 19 a toda la población reclusa. De igual forma ordena que se dote de los implementos de bioseguridad a los reclusos hasta que se logre mitigar los impactos del virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la población reclusa, si bien las pruebas se han venido realizado, no han sido suficientes para controlar la situación en la cárcel. El pasado 2 de mayo se informó también sobre la muerte de la enfermera **Andrea Sotelo**. La mujer era la compañera de uno de los guardias del INPEC del centro de reclusión que padece el virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, las condiciones en las que están las personas que ya padecen el covid 19, continúan siendo precarias, **debido a que siguen en celdas muy pequeñas y sin atención médica.** (Le puede interesar: "[Covid-19 y crisis carcelaria colombiana](https://archivo.contagioradio.com/covid-19-y-crisis-carcelaria-colombiana/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta ya son cuatro las decisiones de jueces en defensa de los derechos a la vida y la salud de la población reclusa en Colombia. Las cárceles favorecidas son la de Manizales, Coiba-Picaleña y Cómbita. (Le puede interesar: "[Jueces fallan a favor de población reclusa en 3 cárceles](https://archivo.contagioradio.com/jueces-fallan-a-favor-de-poblacion-reclusa-en-3-carceles/)")

<!-- /wp:paragraph -->
