Title: Adiós, a unas armas
Date: 2017-07-11 08:59
Category: Camilo, Opinion
Tags: acuerdo de paz, Alvaro Uribe, Dejación de armas, FARC
Slug: adios-a-unas-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Naciones Unidas 

#### Por C[amilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

###### 11 Jul 2017 

<div dir="auto">

<div dir="auto" style="text-align: justify;">

</div>

<div dir="auto" style="text-align: justify;">

Los núcleos más duros de transformar están en el alma y ellas se expresa en el ser actuado o dicho. Emocionante para muchos la dejación de armas de las FARC EP. Así lo vivieron excombatientes de otras guerrillas,  algunos exmilitares y ciudadanos. La mayoría quizás no, movidos por la costumbre de la violencia, atascados en los imaginarios del Uribismo y en las realidades insalvable de dolores sin cicatrizar de la guerra con sus múltiples actores y facetas.  
María Fernanda Cabal aseguró días antes que la Misión I del Consejo de Seguridad carecía de credibilidad en la certificación de dejación de armas de las FARC EP, porque de ella forma parte China y la URSS. Ella es parte de esa expresión confusa del alma pero profundamente  ignorante que cabalga en las pasiones de los poderosos y seduciendo la inocencia de la gente del común.  
La expresión parlamentaria es el desconocimiento de la geopolítica y es otra manifestación del maniqueismo judeocristiano heredado en la política. Cómo olvidar ese rasgo totalitario con el maltrato a víctimas de Estado cuando reclamaban en el Congreso que se reconociera la verdad de apuño del Estado victimario. Su defensa, "vagos vayan a estudiar", expresa ese clasismo excluyente.  
Y sin mirar tan lejos, la ofensa de Cabal, toma fuerza para sí, uno del Centro Democrático, Ernesto Matías, está sin acreditar la terminación del bachillerato. ¿Vagos?.  
AUV semanas atrás,  en un Foro en Grecia,  amargando lo que se hubiéra podido celebrar, también mintió asegurando que las FARC EP en transición viven de la impunidad. La lectura actualiza del Acuerdo Final entre el gobierno y la guerrilla de las FARC EP en el punto de víctimas cuenta con modificaciones al plebiscitado el 2 de octubre, que él incluyó. Él  sabe que hay juicios y cárcel para los responsables de la violencia que se nieguen a la verdad y que quiénes la reconozcan y expresen plenamente,  derivan en sanciones alternas.. Asumir que las FARC EP gozan de impunidad es falaz.  
Así nos encontramos en ambos casos con  asuntos estructurales sin resolver que son parte de almas oscurecidas por el ejercicio del poder para sus intereses.  
Si bien, es un asunto del ideologismo de los 60, esa mentalidad e ideología de la guerra fría y traducida en centro y sur América como Doctrina de la Seguridad Nacional hay otros aspectos más del alma de la condición humana que se desata en pasiones ciegas, así lo comprendió Nelson Mandela.  
La  DSN es una inversión ideologizada de la realidad. La ideologización despierta pasiones esquizoides, visiones maniqueas, que desconocen la universalidad. En ella  son enemigos en occidente comunistas, ateos, y la propiedad colectiva, chinos, coreanos, sirios, orientales, musulmanes...  
Tras de esa ideologización de la realidad se mata el alma respetuosa, la posibilidad de la escucha a los otros en sus diferencias, el origen propio de la humanidad. La visión totalizante lejos de asumir la universalidad como polvo de estrellas, es que ellos son las estrellas, como el catolicismo de la inquisición, cómo el judaísmo y todos los ismos que imponen y que coinciden y se expresan con Cabal.  
Así reconocer que existen quiénes buscan otra democracia más social y justa, que respetan las libertades religiosas y de creencias, o quiénes postulan un modelo económico diferente al que se sostiene sobre el capital, o quiénes asume  que hay otros modelos de familia, deben ser declarados anatemas, enemigos.  
Aunque el mundo bipolar terminó hace mucho tiempo, esto no significa desconocer que existe un imperio y modos de dominación en que se crean con nuevos modelos doctrinales y técnicas de represión,  evitando resolver  la multicausalidad de un problema, atribuyendo a otros unicausalmente la responsabilidad, evitando resolver la contradicción.  
Desconocer que la voz de Uribe es una expresión de un sector del alma ciudadana que debe ser reconocida para se transformada es un error craso. Los seres humanos optamos muchas veces por aquello que nos da seguridad aunque sea una mentira, tememos a la propia verdad, dejamos de ser nosotros para delegar en un otro. Uribe en sus diversos nombres Ordoñez, Ramírez, Pérez, Nieto, representan en parte un fragmento de nuestro país y el riesgo de enfrentar las causas de nuestra violencia.  
La ideologización de la realidad sirve adicionalmente para desconocer racionalmente asuntos estructurales del ser consigo mismo y con los otros.  Los rasgos de autoritarismo o ceguera reflejan ausencia de amor verdadero por sí mismo, maltratos infantiles,  exclusiones, robos, crímenes diversos, un desequilibrio en el alma, de ahí el miedo a la verdad a la propia y a la de la historia.  
Seguir creyendo que avanzamos atacando también ciegamente, sin una estrategia comunicativa basada en la ética de la verdad, es contribuir a la apuesta de la seguridad, llamada sociedad guerrerista.  
Romper la ceguera que quiere empañar la decisión histórica y valiente de las FARC EP, de decir adiós a las armas y abrir el escenario político con sus palabras, sus mentes y sus cuerpos,  expresión de una parte del país nacional, es una apuesta por la creación sanadora del alma en una democracia que ha carecido de un proyecto de país decente.  
Viene el tiempo de un escenario como la Comisión de la Verdad y la JEP,  la de reincorporación y apuestas básicas de inclusión,  en medio de las dinámicas de la guerra con el ELN y las nuevas formas de paramilitarismo de Estado y de grupos de narcoterratenientes empresariales privados que puede despertarnos o afirmar la insana ceguera del autoritarismo o de la Pax Neoliberal.  
Reconocer el paso de las FARC EP no es exculparlos de sus dolores y de los causados por ellos. Se trata de comprender el riesgo asumido por sus siete mil integrantes de dejar libremente aquello que les sirvió para expresar sus ideas. Su adiós a las armas un gesto para descubrir nuestra propia ignorancia y ceguera, para descubrirnos en un nuevo proyecto de país, sanamente deseable y posible en el que es superable la fantasía que nos hace felizmente dominados: la Seguridad Nacional. Ella una forma de expresar la ceguera del alma de la vida y de los pueblos.

</div>

<div dir="auto" style="text-align: justify;">

</div>

<div dir="auto">

####  

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 

</div>

</div>
