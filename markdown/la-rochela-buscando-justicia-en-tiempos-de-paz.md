Title: "La Rochela, buscando justicia en tiempos de paz”
Date: 2015-08-22 12:36
Author: AdminContagio
Category: 24 Cuadros, Sin Olvido
Tags: 26 años de la Rochela, aias negro Vladimir, Corte Interamericana de Derechos Humanos, Documental Masacre de la Rochela, Ley de justicia y paz, Manuel Libardo Díaz, RTVC Señal Institucional
Slug: la-rochela-buscando-justicia-en-tiempos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/rochela2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: documentalamarillo.blogspot.com 

###### [21, ago, 2015]

Hablar de verdad, justicia o reparación en la denominada "Masacre de la Rochela", es referirse a 26 años de contradicciones, dilaciones, persecución y dolor para las familias de los 15 funcionarios judiciales que padecieron en carne propia, las atrocidades cometidas por el grupo paramilitar denominado “Los Masetos”, en macabra alianza con miembros de las fuerzas armadas y narcotraficantes.

Como parte de la sentencia proferida el 11 de Mayo de 2007 por la Corte Interamericana de Derechos Humanos (CIDH) al Estado colombiano por su responsabilidad en la masacre, ocurrida el 18 de enero de 1989 en camino al corregimiento La Rochela, en Simacota Santander; RTVC Sistema de medios públicos en asocio con la Sala Administrativa del Consejo Superior de la Judicatura presenta el documental "*La Rochela, buscando justicia en tiempos de paz*”.

Un testimonio audiovisual en el que familiares de las víctimas y Manuel Libardo Díaz, uno de los 3 sobrevivientes, relatan de manera desgarradora los pormenores de la matanza, ocurrida cuando la comisión de funcionarios se desplazaba para investigar el asesinato de 19 comerciantes provenientes de Cúcuta, desaparecidos en la región, y la forma en que, de manera premeditada,  fueron acribillados y rematados con total sevicia.

[![laroche](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/laroche-e1440185842756.jpg){.size-full .wp-image-12557 .aligncenter width="590" height="332"}](https://archivo.contagioradio.com/la-rochela-buscando-justicia-en-tiempos-de-paz/laroche/)

El documental, pone en evidencia la impunidad persistente en el caso, donde paradojicamente no hay justicia en un crimen contra la justicia, denuncia reiterada en las intervenciones de funcionarios de organizaciones que vienen acompañando el proceso y en las voces de los familiares, quienes exigen además que exista un proceso de reparación real por su pérdida y las consecuencias familiares, psicológicas y económicas, por las que el Estado debe responder de acuerdo con la sentencia de la CIDH.

Mario Arturo Salgado, en representación de los familiares de las víctimas, agradeció al equipo de producción el "abordaje humano, respeto y compresión en virtud de la sensibilidad del tema" en su trabajo, agregando que fue una oportunidad para hacer una catarsis de lo vivido, una terapia a diferencia del apoyo psicológico que el estado pudo aportar de manera oportuna.

A pesar de las declaraciones de Alonso de Jesús Baquero Agudelo alias el "Negro Vladimir"  entregadas en el marco de la llamada "ley de Justicia y Paz" durante el gobierno de Alvaro Uribe Vélez, en las que tomaba responsabilidad por los hechos ocurridos en la Rochela e involucraba altos mandos militares y políticos, las investigaciones han sido desviadas y las responsabilidades encubiertas ante lo que Salgado reclama una "gestión eficaz en la investigación que arroje resultados contundentes" por parte de la Fiscalía general de la nación.

"*La Rochela, buscando justicia en tiempos de paz*”, dirigido por Frank Chavez, recrea a través de pasajes animados las duras escenas de la masacre, además de incluir imágenes y sonidos de archivo como complemento del relato. **El documental, será emitido a las 9 de la noche del sábado 22 de agosto, y tendrá repetición el domingo 23, por el canal Institucional de Radio Televisión Nacional de Colombia (RTVC)**.

<iframe src="https://www.youtube.com/embed/f5E1zIi69YU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

\#SinOlvido con las víctimas de la Rochela en nuestra memoria:

##### Mariela Morales Caro, Jueza 4 de Instrucción Criminal de San Gil///Pablo Antonio Beltrán, 40 años Juez 16 de Instrucción Criminal de San Gil ///Samuel Vargas, 44 años, Conductor del Cuerpo Técnico de Policía Judicial de San Gil /// Gabriel Enrique Vesga, 23 años, Miembro del Cuerpo Técnico de Policía Judicial de San Gil /// Cesar AugustoMorales, 28 años, Miembro del Cuerpo Técnico de Policía Judicial de San Gil /// Yul Germán Monroy, 28 años, Investigador del Cuerpo Técnico de Policía Judicial de Bogotá /// Carlos Fernando Castillo, 24 años, Secretario del Cuerpo Técnico de Policía Judicial de Bogotá /// Orlando Morales, 21 años, Investigador del Cuerpo Técnico de la Policía Judicial /// Virgilio Hernández, 59 años, Secretario del Cuerpo Técnico de Policía Judicial de Bogotá ///Benhur Iván Guasca, 24 años, Investigador del Cuerpo Técnico de la Policía Judicial /// Luis Orlando Hernández, 29 años, Investigador del Cuerpo Técnico de Policía Judicial de Bogotá ///Arnulfo Mejía, 24 años, Conductor del Cuerpo Técnico de Policía Judicial de San Gil. 
