Title: Ciudadanos realizarán la "Peregrinación por los dolores del Sur" en Cundinamarca
Date: 2017-09-01 15:05
Category: Ambiente, Nacional
Tags: Botadero de Doña Juana, Embalse del Muña, soacha
Slug: ciudadanos-realizaran-la-peregrinacion-por-los-dolores-del-sur-en-cundinamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Rio-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blogger] 

###### [01 Sept 2017] 

Este sábado 2 de septiembre se llevará a cabo una peregrinación desde Mochuelo y hasta Soacha, denominada “Peregrinación por los dolores del Sur” una iniciativa que busca **visibilizar la crítica situación que afrontan los ecosistemas que hacen parte de las fronteras entre Bogotá, Sibaté y Soacha**.

De acuerdo con Jorge Ángel, la movilización está impulsada por organizaciones ambientalistas, sociales, algunos integrantes de iglesias y ciudadanos preocupados por problemáticas ambientales que hasta el momento no tienen freno como lo son el basurero Doña Juana, las basuras del río Bogotá, la Represa del Muña, las ladrilleras en la periferia de Bogotá y Soacha, que directamente **afectan al Sumapaz, una de las mayores fuentes de agua en el departamento de Cundinamarca**.

“Todos estos dolores son los que queremos poner en conocimiento y el daño que se está haciendo con la ampliación de la frontera agrícola y urbana al Sumapaz, y si no nos ponemos todos a cuidar esto, dentro de 10 o 20 años no vamos a tener agua” afirmó Ángel. (Le puede interesar:["Sibaté prepara consulta popular contra minería"](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/))

La peregrinación se tendrá 5 paradas e iniciará en el parque central de Sibaté, pasará por el Embalse del Muña, atravesando Soacha y finalizará en el Basurero de Doña Juana, en Mochuelos, **en donde habrá un acto final en el que participarán integrantes de comunidades indígenas**.

Esta actividad, además, se desarrolla en el marco de lo que se ha denominado como “Caminando el paro desde el Sur- Tunjuelo” una iniciativa de la ciudadanía que habita en las localidades del sur de Bogotá y que han anunciado que el próximo 27 de septiembre cera la hora cero del paro indefinido. (Le pude interesar: ["Distrito ampliaría hasta 2070 la existencia del Relleno Sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

De igual forma, Ángel afirmó que se espera que durante la peregrinación se pueda realizar un documento que se **presente al Papa Francisco en respaldo a la ciudadanía que busca proteger el ambiente en Cundinamarca**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
