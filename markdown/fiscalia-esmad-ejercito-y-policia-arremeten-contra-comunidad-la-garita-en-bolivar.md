Title: Fiscalía, ESMAD, Ejército y Policía arremeten contra comunidad La Garita en Bolívar
Date: 2016-05-13 17:00
Category: DDHH, Nacional
Tags: bolivar, Centro y Sur del Cesar CISBCSC, ejercito, ESMAD, Fiscalía, la Garita, omisión de Interlocución del Sur de Bolívar, Policía Nacional
Slug: fiscalia-esmad-ejercito-y-policia-arremeten-contra-comunidad-la-garita-en-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Esmad-Campo-rubiales-pag-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **[ ]**[Foto: Semanario Voz] 

###### [13 May 2016] 

Este jueves la comunidad campesina La Garita, ubicada en la parte alta del municipio de Rioviejo, departamento de Bolívar, **fue víctima de allanamiento, amenazas y agresiones verbales por parte de miembros de la Fiscalía, el ESMAD, la Policía y el Ejército Nacional,** según denuncia la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar CISBCSC.

Entre las 9:30 a.m y 3:00 p.m, las fuerzas armadas y la Fiscalía **ingresaron de forma ilegal a la residencia de líderes de la comunidad, profiriendo amenazas a dos de sus miembros;** Wilfrido Gamarra y Anival Mendoza, y preguntando por Anardo Arrieta y Leiner Mesa, quienes aparecían en una lista, asegura la CISBCSC.

Igualmente, habitantes de la comunidad sufrieron robos y saqueos por parte de las fuerzas policiales. "**En la tienda del Sr Dionisio Arrieta hurtaron \$6.2000.0000 y los documentos de identidad de la señora Martha Gonzáles** (...) Ingresaron a la vivienda del señor Darinel Rodríguez que en el momento se encontraba sola y hurtaron los documentos de identidad de él y su compañera y un bolso, de igual forma de la caseta de comunicación hurtaron un celular y una máquina de motilar", afirma en el comunicado.

La comunidad y la Comisión denuncian y responsabilizan al gobierno colombiano por la actuación de las autoridades. **Además exigen el cese de la persecución, estigmatización,, así como la protección y garantía de la integridad física y mental** de las comunidades campesinas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
