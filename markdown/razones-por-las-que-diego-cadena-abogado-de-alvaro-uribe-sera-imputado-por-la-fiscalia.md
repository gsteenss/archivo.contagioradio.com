Title: Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía
Date: 2020-07-27 13:38
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Álvaro Uribe, Corte Suprema de Justicia, Diego Cadena
Slug: razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Diego-Cadena-y-Álvaro-Uribe.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Diego-Cadena-y-Álvaro-Uribe-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Diego Cadena y Álvaro Uribe

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes a las 2 de la tarde, después de varios aplazamientos, está programada para su realización la audiencia de imputación de cargos en contra del **abogado de Álvaro Uribe, Diego Cadena; protagonista en un caso de presunta fabricación de testigos que tiene al propio expresidente Uribe siendo procesado por la Corte Suprema de Justicia.** (Le puede interesar: [Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe](https://archivo.contagioradio.com/piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se espera que de realizarse la audiencia, **el fiscal del caso impute a Cadena y su socio, Juan José Salazar, los delitos de fraude procesal y soborno en actuación penal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Cada uno de esos cargos, contempla una pena de 6 a 12 años de prisión y se prevé que la Fiscalía solicite la detención preventiva de Cadena junto con la imputación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Fiscalía busca probar** que **Cadena es responsable directo de haber tratado de sobornar a testigos para que entregaran versiones convenientes para Uribe ante la Corte Suprema de Justicia**, en el caso  que se remonta al año 2018, cuando la Sala de Casación Penal decidió archivar una denuncia del expresidente en contra del senador Iván Cepeda, y a su vez abrir una investigación formal contra Uribe al advertir que los testigos con los que impulsó la denuncia inicial podrían ser fabricados. (Lea también: [Uribe Vélez violó la reserva del sumario: Iván Cepeda](https://archivo.contagioradio.com/uribe-velez-violo-la-reserva-del-sumario-ivan-cepeda/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1287701302756220928","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1287701302756220928

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Los pagos y testimonios que involucran al abogado Diego Cadena

<!-- /wp:heading -->

<!-- wp:paragraph -->

**El exparamilitar, Carlos Enrique Vélez, quien fue contactado por Cadena, para testificar en favor del expresidente Uribe en el caso, ha manifestado que recibió varios giros y dinero en efectivo de manos de este** y según señala tiene recibos de Supergiros S. A., por más de 4 millones de pesos, girados a nombre de María Elena Vélez Ramírez, una familiar suya.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **está el pago a Eurídice Cortés, alias Diana, jefa política de las Autodefensas en Caldas**, que según ella, se trató de unos «*viáticos*» para ir a buscar otros potenciales testigos que pudieran comparecer en favor de Álvaro Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estos dos pagos, se sumaría el de otro preso, en la cárcel de Cómbita, Boyacá, que habría recibido las mismas visitas y ofertas, del cual no se ha revelado su identidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Cadena y su defensa admitieron haber desembolsado dineros, aunque sin confirmar la cuantía**, argumentando que se entregaron no para influir en la versión de los testigos, sino para «*viáticos y ayuda humanitaria*». Al mismo tiempo que afirman, que de ello, no se enteró nunca al expresidente Álvaro Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, el periodista [Daniel Coronell](https://twitter.com/DCoronell/status/1287453554651930626), señaló en una de sus [columnas](https://losdanieles.com/daniel-coronell/el-desplome/) que esto era algo contradictorio, si se tiene en cuenta que en su momento **Cadena pidió el permiso y la autorización de Uribe para elaborar un recursos en favor de Juan Guillermo Monsalve, testigo con el que también se contactaron para nutrir la defensa del expresidente**, siendo que este actuar ni siquiera comprometía entregas de dinero.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=yU7Kz_8I8tI","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=yU7Kz\_8I8tI

</div>

<figcaption>
Youtube: Los Danieles

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
