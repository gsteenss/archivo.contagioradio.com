Title: Las claves del discurso de Trump que cautivan a conservadores en EEUU
Date: 2016-02-28 11:11
Category: El mundo, Política
Tags: Donald Trump, Estados Unidos, Partido Demócrata, Partido Republicano
Slug: las-claves-del-discurso-de-trump-que-cautivan-a-conservadores-en-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/trump-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cnn] 

<iframe src="http://co.ivoox.com/es/player_ek_10589130_2_1.html?data=kpWimp6Vd5Ghhpywj5eVaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDQzsbaqdSfxcrZjcnNt8Tp09jcjcnJb7Xm1tLdjdbZqYzXwtrhy9vFsozVjMjc0NjJttehhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gustavo Vásquez, Analista Político] 

###### [26 Feb 2016] 

Por una parte el éxito de Trump en la campaña se debe a que en las elecciones primarias solamente votan un segmento de la población que se han caracterizado por ser conservadores o ultra conservadores, pero además ha usado el discurso de un candidato anti sistema que rompe la reglas y eso lo hace muy atractivo “maneja la campaña como un reality Show” lo cual es muy atractivo señala Gustavo Vásquez, periodista y analista político.

### **Trump maneja la campaña política como un reality show** 

Según Vásquez, uno de los elementos que ha servido a los intereses electorales de Trump ha sido el saber manejar las “audiencias”. Las personas a las que se pretende llagar no son electores puros, sino que también son televidentes y consumidores de shows como los reality y la población de EEUU es muy dada a ese tipo de espectáculos.

Esta manera de manejar el discurso sirve a Trump porque se presenta como un candidato “anti sistema y que rompe los esquemas de los protocolo políticos” lo cual resulta muy atractivo en medio de una sensación de quietud y de falta de funcionalidad del sistema político, generado en parte, por la polarización al interior del congreso controlado por el partido republicano y que se ha dedicado a bloquear las iniciativas del partido demócrata y del gobierno Obama.

### **Un discurso dirigido a conservadores y ultra conservadores** 

Gustavo Vásquez afirma que para analizar esta línea del discurso de Trump es necesario recordar que la cultura de EEUU estuvo mucho tiempo dirigida por el discurso de la guerra fría y tras la caída del bloque soviético, un sector de la población creció con ese discurso y ve en los últimos tiempos una especie de “pérdida de la grandeza” de la nación.

A ese sector de la población se dirigen gran parte de los mensajes de Trump que intenta capitalizar ese descontento como uno de los principales pilares políticos republicanos. Muchos creen que con una presidencia republicana de ese estilo podrían recuperarse los años perdidos. Sin embargo ese sector de la población no es el joven que no cree ya en ese tipo de mensajes.

### **La política del miedo** 

Para nadie es un secreto que hay una temporada de crisis que Trump también intenta aprovechar, además la situación de inseguridad generada por los actos de terrorismo alrededor del mundo están sirviendo como caldo de cultivo para un discurso basado en el temor de los ciudadanos que solo una política de seguridad interna puede resolver, algunos comparan ese elemento de campaña como el de Netanyahu en Israel.

En este sentido, los mensajes sobre el control de las fronteras, la política anti inmigrante al interior de Estados Unidos y una línea dura frente a lo que pasa en otros países ha servido a los intereses electorales de Trump. Todo ello afecta a quienes no son “puramente blancos” y vende la idea de una especie de raza pura, señala Vásquez.

### **La recuperación de los valores del cristianismo** 

Trump se vende como un cristiano, y esto le ha servido para captar a otro sector de la población que cree en la necesidad de preservar los valores del cristianismo con la oposición a algunas políticas orientadas por el gobierno actual y que son consideradas como liberales, algunas de ellas en la línea de una política de las migraciones, algunos derechos de la comunidad LGBTI, entre otras.

Lo considerable de esta línea discursiva es que llega con mayor fuerza a una población pobre, sin acceso a recursos que radica principalmente en las llamadas minorías, que van creciendo de manera exponencial y que, en el mediano plazo, pueden llegar a ser mayorías que controlarían gran parte de las decisiones a través del voto. Es decir que aunque Trump no se comporte como un cristiano, vende la idea de poder recuperar los valores perdidos.

A esas consideraciones, y lo que tiene que ver este discurso con la posibilidad de que Trump sea candidato a la presidencia por el partido republicano, hay que sumar que ese partido no está dando un apoyo cerrado a Trump, lo que dejaría abierta la posibilidad de que muchos de los votos vayan a la candidatura demócrata una vez se den las elecciones generales.
