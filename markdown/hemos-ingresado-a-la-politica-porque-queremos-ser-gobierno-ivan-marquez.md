Title: “Hemos ingresado a la política porque queremos ser Gobierno” Iván Márquez
Date: 2017-09-01 17:46
Category: Nacional, Paz
Tags: FARC, Iván Márquez, Partido político de las FARC, paz
Slug: hemos-ingresado-a-la-politica-porque-queremos-ser-gobierno-ivan-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/FARC-Partido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Rodrigo Londoño] 

###### [01 Sep. 2017] 

Con rosas rojas en las manos se dió apertura a la alocución de Iván Márquez integrante del Secretariado de las FARC, quien manifestó que luego de intensas jornadas de trabajo durante los 4 días de Congreso Nacional de esa guerrilla **nació ese partido que, para ellos, representa la posibilidad de construir colectivamente** y a través de los votos una paz estable, duradera y un buen vivir.

**“Hemos dado uno de los pasos más importantes de nuestro proceso de reincorporación** a la vida civil, con la creación del Partido Fuerza Alternativa Revolucionaria del Común –FARC-. De esa manera damos continuidad a través de la vía exclusivamente política a los propósitos y aspiraciones históricas por un nuevo orden de justicia social y democracia para nuestro país”.

Si bien **hasta el momento no han sido elegidos formalmente los candidatos a las listas para la Cámara de Representantes y Senado**, se escogieron 111 personas que harán parte de la dirección del partido y serán ellos y ellas quienes decidan quienes irán a las urnas, de un grupo de precandidatos, dentro de los que se encuentra Iván Márquez, Victoria Sandino, Pablo Catatumbo.

“Ante el contexto que vivimos se pone de manifiesto la necesidad de un cambio político profundo en nuestra sociedad que solo puede ser encarnado por las fuerzas políticas y sociales que tengan el deseo y la voluntad de erigirse en verdadera alternativa política”. Le puede interesar: ["Luego de reunión de exparamilitares y FARC-EP"](https://archivo.contagioradio.com/la-reunion-de-exparamilitares-y-las-farc-ep-si-van-a-decir-la-verdad/)

**Así mismo, aseguró que toda su plataforma y sus ideas las ponen a disposición de la paz y de cara a las elecciones del 2018** porque “hemos ingresado a la vida política legal porque queremos ser Gobierno o hacer parte de el (…) nos queremos coordinar y articular para aproximar colectivamente propósitos comunes y transitar hacia la definición de una plataforma común que nos unifique”.

### **¿Por qué conservar las siglas e incluir una rosa roja en el logo del partido político de las FARC?** 

Ante los debates suscitados por la permanencia de las siglas de las FARC para el partido político de esa organización, por lo que podría significar una "carga negativa" resaltan que esas siglas representan un acumulado histórico, su pasado revolucionario que no se va a desdibujar. Le puede interesar: [Partido de las FARC empezará vida política el primero de septiembre](https://archivo.contagioradio.com/partido-de-las-farc-empezara-vida-politica-el-primero-de-septiembre/)

**“Nosotros solamente vamos a continuar el conflicto, pero ya en otro terreno, el de la vía política legal.** Quisimos encontrar un símbolo que fuera conocido por todo el mundo, que se encontrara en todas partes y queremos que cuando vean una rosa roja vean a las FARC. La rosa tiene una carga muy positiva”.

Además, según lo relató Pablo Catatumbo, integrante del Secretariado de las FARC, durante las jornadas de debate se llegaron a acuerdos del carácter del partido, los estatutos, el programa o plataforma política, la dirección.

**“Definimos que este partido político será de carácter amplio, un nuevo partido para una nueva Colombia**, un partido comprometido con las luchas del pueblo colombiano por alcanzar la paz, la justicia social, la soberanía, por una reforma agraria. Nuestro partido va a renovar las costumbres políticas de nuestro país y va a aportar en la democratización” puntualizó Catatumbo.

También anunciaron que realizarán una consulta a nivel nacional para definir los criterios y seleccionar las regiones de las cuales saldrían los representantes para las 5 curules de la Cámara y las 5 del Senado.

**“Queremos que todos los militantes puedan opinar acerca de la composición de la lista nacional al Senado”** recalcó Carlos Lozada del Secretariado de las FARC. Le puede interesar: [Gobierno le hace 'conejo' a las FARC: Andrés París](https://archivo.contagioradio.com/gobierno-le-hace-conejo-a-las-farc/)

### **FARC espera poder reunirse con el Papa en su visita a Colombia** 

Para las FARC la visita del sumo pontífice es un hecho muy significativo y trascendental porque trae un mensaje de reconciliación, que coincide con el momento histórico que vive Colombia, por ello esperan poder reunirse con el Papa para dialogar y pedirle que apoye e impulse el proceso de implementación de los Acuerdos de Paz en el país.

“Hemos solicitado la posibilidad de **una entrevista con el Papa, porque consideramos que el aporte** que dió a través de los distintos pronunciamientos a lo largo del proceso de La Habana y el que puede hacer de aquí en adelante que comienza la etapa más dura del proceso, que es buscar cerrar las heridas y construir ese proceso de reconciliación, las palabras y las oraciones y todo lo que pueda hacer el Santo Padre es bienvenido” aseveró Carlos Lozada.

### **“Nosotros ya no reclutamos niños, ahora reclutamos jóvenes y viejos para el partido” Rodrigo Granda** 

Frente a las noticias que aseguran que las disidencias de las FARC estarían reclutando niños y niñas, Rodrigo Granda del Secretariado de las FARC, manifiesta que ya no lo hacen y que no pueden responder por lo que hagan las disidencias puesto que ellas son responsabilidad del Estado.

**“Ahora estamos reclutando ancianos ahora que puedan tener cédula para votar, estamos los de la tercera edad que tengamos cédula.** En todos los procesos del mundo, ha habido desertores, no vamos a tener temor de eso y las FARC ha tenido la menor disidencia”. Le puede interesar: ["FARC confía en que se cumpla liberación de presos políticos"](https://archivo.contagioradio.com/44063/)

### **“Estamos dispuestos a hablar con Álvaro Uribe” Pablo Catatumbo** 

Asegura Catatumbo que están en toda la disposición de sostener un diálogo con Álvaro Uribe en aras de la reconciliación **“hemos dicho que estamos dispuestos a hablar hasta con el diablo si es necesario para hacer la paz de este país** y a él le hacemos un llamado de que acepte que Colombia debe hacer el paso a la paz, los tiempos de la guerra y el odio tenemos que dejarlos atrás” Catatumbo.

Y añadió que hacen también un llamado a todos los sectores políticos, al ELN, al EPL para seguir buscando una Colombia en paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
