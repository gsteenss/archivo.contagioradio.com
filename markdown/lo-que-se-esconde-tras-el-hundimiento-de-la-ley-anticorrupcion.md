Title: Lo que se esconde tras el hundimiento de la ley anticorrupción
Date: 2019-06-20 15:52
Author: CtgAdm
Category: Nacional, Política
Tags: Congreso de la República, Ley Anticorrupción, Plan Tortuga
Slug: lo-que-se-esconde-tras-el-hundimiento-de-la-ley-anticorrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ley-Anticorrupcion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: CamaraColombia] 

Aunque nadie asume la responsabilidad de lo ocurrido, durante el último día del periodo legislativo y mientras el país estaba expectante por el desarrollo de la Copa América, en la **Cámara de Representantes** se levantaba la sesión, dejando de lado la conciliación final para aprobar la ley anticorrupción que evitaría la casa por cárcel para funcionarios públicos vinculados a corrupción. Con el hundimiento de este proyecto también naufragaron otras iniciativas como la que buscaba prohibir el cobro de cuota de manejo de servicios bancarios y la ley que promovía etiquetar la'comida chatarra'.

A su vez, junto al proyecto de ley Anticorrupción contra la casa por cárcel, también se hundía la prohibición para que empresas corruptas vuelvan a contratar con el Estado, ahora, para que ambos proyectos vuelvan a la legislatura deben ser discutidos nuevamente por Cámara y Congreso, lo que significa una demora de al menos otros seis meses.

Para el **representante David Racero**, lo sucedido con los proyectos de ley que no fueron aprobados, demostró que existen bancadas que trabajan para dilatar proyectos que van en contra de los gremios que los han financiado, "evidentemente le responden a sus financiadores, a sus patrones", reflexionó Racero invitando a la ciudadanía a elegir a congresistas que verdaderamente representen sus intereses.

### Un plan tortuga premeditado contra la ley anticorrupción 

Racero agregó que lo sucedido fue un plan tortuga premeditado que empezó en el Senado al ser engavetado o no ser agendado, "aquí los principales responsables del fracaso de los proyectos de ley que tenían que ver con la Consulta Anticorrupción se llaman Centro Democrático, el senador Álvaro Uribe y el presidente Duque", señalando que siempre actuaron en contradicción de un clamor ciudadano.

**"El presidente Duque siempre dice en los medios que está comprometido contra la corrupción pero en la práctica no vemos que meta el acelerador"** agregó Racero quien resaltó que esto no ha sido el único proyecto vinculado a la consulta que se ha caído y que ya han sido seis los que han sido hundidos.[(Lea también: Referendo uribista va más allá de una simple venganza contra el sistema judicial: Beltrán)](https://archivo.contagioradio.com/referendo-uribista-va-mas-alla-de-una-simple-venganza-contra-el-sistema-judicial-beltran/)

### **¿Es Alejandro Chacón un chivo expiatorio?**

Desde su postura, David Racero indicó que la presidencia de Alejandro Chacón, quien ha sido señalado como uno de los responsables de este incidente, contrastada con la de Ernesto Macías en el Senado, ha dado garantías y abierto los micrófonos a la oposición y al debate,  sin embargo aclaró frente a este tema que es necesario asumir responsabilidades, **"si en verdad hubiese voluntad sincera de fondo, se hubiera hecho lo posible para lograrlo",** pues más allá de la decisión de Chacón, el proyecto representaba un avance en la lucha contra la corrupción.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
