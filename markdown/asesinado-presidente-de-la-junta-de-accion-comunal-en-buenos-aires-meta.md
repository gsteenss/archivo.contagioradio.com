Title: Asesinado presidente de la Junta de Acción Comunal en Buenos Aires, Meta
Date: 2017-04-03 17:16
Category: DDHH, Nacional
Tags: Asesinado, Mesetas, Meta, SINTRAGRIM
Slug: asesinado-presidente-de-la-junta-de-accion-comunal-en-buenos-aires-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/asesinatos-lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [03 Abr. 2017]

Continúan los asesinatos de campesinos y líderes sociales en el país, esta vez en la Vereda Buenos Aires del municipio de Mesetas, departamento del Meta, **fue asesinado Eliver Buitrago, quien era el Presidente de la Junta de dicha vereda.** Aunque los hechos apenas comienzan a ser materia de investigación, hasta el momento se sabe que **a Eliver le propinaron 3 impactos de bala.** El hecho ha causado el repudio y la indignación de la comunidad, que recuerda a Eliver como un campesino trabajador.

**Alfonso Camacho, secretario de derechos humanos de Sintragrim**, relató que lo que ha manifestado la comunidad es que **“llegaron personas armadas y lo ultimaron en su lugar de trabajo.** La comunidad, lo recuerda como un campesino, dedicado a su trabajo, líder. Sin filiación política ni credo religioso, dedicado a sus labores. Razón por la cual la población está en una incertidumbre total y espera que el compromiso que hay de comenzar una investigación se dé rápidamente”. Le puede interesar: [Persiste la presencia paramilitar en cercanías a Zonas Veredales](https://archivo.contagioradio.com/persiste-la-presencia-paramilitar-en-cercanias-a-zonas-veredales/)

Aunque se manifestó que se había dado **una quema de un carro transportador de leche a raíz del asesinato del señor Eliver, esta versión fue desmentida por la comunidad** “este hecho ocurrió al mismo tiempo que el asesinato del señor, no se trata de retaliaciones por el hecho. Lo que sí se puede decir es que el carro fue quemado, el señor asesinado y que hoy en Mesetas hay paro por falta de garantías” relató Camacho.

Hasta el momento **en la cabecera municipal del Mesetas, Meta, más exactamente en el sitio El Limón hay paro de los transportadores de leche,** no hay tráfico de carros y el comercio está cerrado en una “actitud de protesta por los hechos ocurridos” contó Camacho.

Con este asesinato en lo que va corrido del 2017 en Mesetas, Meta van 3 personas asesinadas sin que se conozca hasta el momento avances en las investigaciones de los 2 primeros casos quienes eran **dos hermanos integrantes del Partido Comunista y del Sindicato de Trabajadores Agrícolas Independientes del Meta (SINTRAGRIM). **Le puede interesar: [Asesinados dos integrantes del Sindicato de Trabajadores Agrícolas en el Meta](https://archivo.contagioradio.com/asesinados-dos-integrantes-del-sindicato-de-trabajadores-agricolas-en-el-meta/)

<iframe id="audio_17962923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17962923_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
