Title: Joven nasa de 19 años es asesinado en Caloto, Cauca
Date: 2015-04-11 18:15
Author: CtgAdm
Category: Nacional, Resistencias
Tags: Caloto, Cauca, Gobierno Nacional, Indigenas Nasa, liberación de la madre tierra, Norte del Cauca, resistencia indígena
Slug: joven-nasa-de-19-anos-es-asesinado-en-caloto-cauca
Status: published

##### Foto: santanderextremo.com 

En finca La Emperatriz en Caloto, Cauca, donde la comunidad indígena Nasa se encuentra adelantando acciones de liberación por la madre tierra, fue asesinado **Fiderson Guillermo Pavi Ramos, un joven de 19 años** que murió por impacto de arma de fuego en el abdomen.

La comunidad Nasa, no sólo ha tenido que soportar la muerte del joven Siberston, además de este hecho, por cuenta de la represión de la fuera pública ya hay **152 indígenas heridos, y se han presentado varios intentos de desalojo.**

Cabe recordar que el  proceso de liberación de la Madre Tierra se viene desarrollando desde finales del mes de febrero en tres puntos de la región del Norte del Cauca, donde las comunidades indígenas reclaman el cumplimiento de diferentes acuerdos que había prometido el gobierno, como reparación por la masacre del Nilo, sin embargo, el gobierno ha incumplido sus promesas con los Nasa.
