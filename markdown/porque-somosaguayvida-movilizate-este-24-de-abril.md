Title: Porque #SomosAguayVida movilízate este 24 de abril
Date: 2015-04-23 13:51
Author: CtgAdm
Category: Ambiente, Movilización
Tags: #SomosAguayVida, ambiantalistas, Ambiente, Bogotá, Bucaramanga, Megamineria, Mineria, Plan Nacional de Desarrollo, PND
Slug: porque-somosaguayvida-movilizate-este-24-de-abril
Status: published

##### Foto: El Tiempo 

<iframe src="http://www.ivoox.com/player_ek_4397249_2_1.html?data=lZimmZeYfY6ZmKiakpmJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOntNTa0dilq9bV2rvWxsaPt8af1dTaw5DQpdSfxMbZzsrXb8XZjKfcydTYaaSnhqaejcrXuMafk5mYxpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Erik Jerena, Colectivo Agenda Bogotá] 

**Este viernes 24 de abril la ciudadanía se tomará las calles de Bogotá y Bucaramanga** para protestar en contra del “Plan Nacional de Desarrollo 2014-2018 Todos por un nuevo país”, que propicia  la minería y los megaproyectos que atentan contra los ecosistemas.

De acuerdo a Erik Jerena, del Colectivo Agenda Animal, diferentes organizaciones a nivel nacional que defienden sus territorios y el agua, expresarán su inconformismo respecto a la **locomotora minero energética que va en contravía de la sostenibilidad de los ecosistemas del país, y así mismo le dirán no al fracking y la destrucción de los páramos.**

El punto de encuentro en Bogotá será la **Plaza de Bolívar a las 12:00 del medio día, allí los defensores del ambiente se desplazarán hacia el Parque Nacional** donde David Kawooq de  Doctor Krápula, Palenque Blues, Charanga Campesina, Canto vital, Canto y Memoria, la Severa Matacera, Reincidentes Bta, Planeta Casa Nativa y Ecomusic y 2Aces crew, los recibirán con un gran concierto “para hacer la reflexión sobre lo que es la apuesta por defender el agua y la vida”, expresa Jerena.

En Bucaramanga, la cita es en **La Puerta del Sol a las 2:00 de la tarde**, donde las personas se movilizarán en defensa del páramo de Santubán.

#####  
