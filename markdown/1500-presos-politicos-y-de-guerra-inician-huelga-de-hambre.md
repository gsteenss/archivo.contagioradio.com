Title: 1500 presos políticos y de guerra inician huelga de hambre
Date: 2015-10-14 12:46
Category: Movilización, Nacional
Tags: Jornada de los presos políticos, presos politicos, Profesor Miguel ángel Beltrán, Universidad Nacional
Slug: 1500-presos-politicos-y-de-guerra-inician-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/presos-politicos-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_8982596_2_1.html?data=mZ6llJqdeo6ZmKiakp2Jd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZakkZDd1MrXs9Sf0dTZh6iXaaK41c7Q0diPvYzYxpDU18rWtsKfytPWxc7Fsozc1srZycaPqMafycaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [**Miguel Ángel Beltrán**] 

###### [14 oct 2015] 

En la jornada por los **más de 10 mil presos políticos** que están en las cárceles colombianas se realizarán diferentes actividades en el país, además con una huelga de hambre indefinida de cerca de 1500 de ellos, en 6 centros de reclusión esperan **visibilizar sus problemáticas.**

El **profesor Miguel Ángel Beltrán**, vocero de los presos políticos, indicó que la primera reivindicación es **la visibilización del delito político**, "somos más de 10 mil" que se divide en categorías de presos políticos detenidos por **montajes judiciales y presos de guerra**, quienes han sido capturados en combate, esto ya que “en Colombia se ha desdibujado el delito político y ha sido tipificado con otros delitos” como terrorismo.

La segunda exigencia **es la salud** la cual según el profesor Beltrán es “Crítica en los centros de reclusión del país”, debido a que “no hay atención y existe una discriminación contra el preso político”, que el “INPEC lo concibe como un enemigo político”. Beltrán menciona casos como el de Virviescas que ha perdido el 85% de su visión por negligencia del INPEC y la empresa de Salud.

La tercera exigencia gira entorno a **la extradición** sobre la cual afirma el profesor Beltrán es una “violación a la soberanía nacional”, ya que los colombianos “**no pueden ser juzgados por otros sistemas de justicia**” y en estas cárceles extranjeras “se les violan sus derechos”.

A la huelga de hambre que se realizará desde hoy por parte de los presos políticos, se suman **distintas actividades en la Universidad Nacional de Colombia y el Centro de Memoria, Paz y Reconciliación**, todo esto para “reivindicar la situación que hoy vivimos los presos políticos y políticas colombianas”, llamando a la sociedad para que “se vinculen a los diferentes eventos por nuestra libertad, ya que somos víctimas del conflicto armado colombiano”, afirmó el profesor Miguel ángel Beltrán.

Miguel Angel Beltrán fue acusado en el 2014 de ser supuestamente ‘Jaime Cienfuegos’, ideólogo de la comisión internacional de las Farc por lo que fue condenado por la Sala Penal del Tribunal Superior de Bogotá  a 100 meses de prisión por el delito de rebelión. Esto después de que en el 2011 fuera absuelto por el mismo caso.

Este es el comunicado público:

\[embed\]https://www.youtube.com/watch?v=JGck\_EtHefY\[/embed\]

###### 
