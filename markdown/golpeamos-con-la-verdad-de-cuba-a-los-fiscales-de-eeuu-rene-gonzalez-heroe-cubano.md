Title: "Golpeamos con la verdad de Cuba a los fiscales de EEUU" René Gonzalez, Héroe cubano
Date: 2015-03-18 20:57
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: René gonzalez héroe cubano, René Gonzalez héroe cubano valora negocición entre EEUU y Cuba
Slug: golpeamos-con-la-verdad-de-cuba-a-los-fiscales-de-eeuu-rene-gonzalez-heroe-cubano
Status: published

###### Foto:Lapatilla.com 

###### **Entrevista con [René Gonzalez], héroe Cubano:** 

<iframe src="http://www.ivoox.com/player_ek_4232466_2_1.html?data=lZeglJmaeo6ZmKiakp6Jd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYtMrSqYy70NPnw9HJvoampJDVh6iXaaKt09TSjcjZpsLi0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**René Gonzalez, uno de los 5 cubanos detenidos en Miami** acusados **de espionaje**, relata cómo fue su captura en Miami. La violencia utilizada en el momento con el objetivo de intimidarles y el inmediato **confinamiento solitario en “el hueco”, sección de castigo** de las prisiones en EEUU por 17 meses.

Según González, con el aislamiento intentaron privarles de la posibilidad de defenderse, **intimidándolos para que declararan que eran culpables**. El juicio duró 7 meses y paso inadvertido para los medios de comunicación estadounidenses, pero no para los millones de cubanos y cubanas que nunca dejaron de manifestarse dignamente contra la injusticia que se cometía contra sus "héroes".

Para René González es importante la **satisfacción de haber podido “golpearles con la verdad de Cuba” ante la fiscalía que les estaba juzgando**.

A pesar de las **condenas** como en el caso de René, **13 años, y la de los otros 4 cubanos detenidos, que pagaron entre 15 y 17 años de prisión**, los cinco esperaban dicha sentencia por la intimidación por parte del gobierno de EEUU al propio juzgado de Miami. Luego de la sentencia todos fueron dispersados por distintos estados.

**El 17 de Diciembre de 2014** fueron **liberados** los **últimos 3 detenidos**, iniciando una **nueva época para las relaciones entre Cuba y EEUU**.

René opina que las negociaciones entre ambos gobiernos, iniciadas a partir de la liberación de los héroes cubanos, es positiva. Sin embargo, EEUU debe modificar sus posiciones y abrir pasos para cambiar su forma de ver a América Latina. Según René ha **sido clave la posición unánime de toda Latinoamérica con Cuba**, igual que está sucediendo con Venezuela, y **califica la amenaza de EEUU contra ese país como “ridícula”.**

Es importante que **finalice el bloqueo económico contra Cuba**, aunque hay otras materias importantes en la negociación como la inmigración, la lucha contra el crimen organizado o las relaciones económicas entre ambos países.

También valora que en Norteamérica haya un movimiento que haya entendido la importancia de la normalización de las relaciones con Cuba, y de que **“los estadounidenses puedan visitar la isla para ver lo que realmente sucede allí”.**
