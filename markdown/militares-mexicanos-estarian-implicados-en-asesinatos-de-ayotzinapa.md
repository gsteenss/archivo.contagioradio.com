Title: Militares mexicanos estarían implicados en asesinatos de Ayotzinapa
Date: 2016-08-10 16:07
Category: DDHH, El mundo
Tags: desaparición estudiantes ayotzinapa, estudiantes Ayotzinapa, militares mexicanos
Slug: militares-mexicanos-estarian-implicados-en-asesinatos-de-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Ayotnizapa..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [10 Ago 2016 ] 

[Militares mexicanos estarían implicados en el asesinato de los estudiantes de Ayotzinapa, de acuerdo con]una investigación adelantada por tres periodistas en la que se establece que los registros telefónicos del normalista Julio César Mondragón dan cuenta de que su **celular estableció comunicación desde el Campo Militar Número Uno y desde el Centro de Investigación y Seguridad Nacional**, después de su fallecimiento y se mantuvo activo hasta el 4 de abril de 2015.

[La investigación hace parte del libro 'La guerra que nos ocultan' escrito por los periodistas Francisco Cruz, Félix Santana y Miguel Ángel Alvarado, quienes aseguran que en el crimen de Julio Cesar Mondragón **se usaron técnicas quirúrgicas para desprenderle el rostro** y con esto enviar un mensaje de terror. De acuerdo con algunos periódicos locales, pese a que el estudiante llevaba su celular cuando fue asesinado, al encontrar su cuerpo el móvil no fue encontrado.  ]

Vea también: [[GIEI no podrá continuar investigando caso Ayotzinapa](https://archivo.contagioradio.com/giei-no-podra-continuar-investigando-caso-ayotzinapa/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

###### 
