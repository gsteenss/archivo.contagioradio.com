Title: Paneleros no levatarán paro hasta no lograr soluciones de fondo
Date: 2019-07-04 17:02
Author: CtgAdm
Category: Movilización, Nacional
Tags: Boyacá, Ministro de Agricultura, Panela, paro, Santander
Slug: paneleros-paro-soluciones-fondo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/paro-panelero-e1562277676296.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Oro Noticias  
] 

Luego de 7 días de paro, los paneleros reconocen la buena disposición del Gobierno para sentarse a una mesa de negociación; pero **dicen que necesitan soluciones de fondo a la crisis que atraviesa el sector**, y no sienten afán por una negociación que sea rápida, pero no solucione sus problemas. Los manifestantes han desarrollado su protesta en Cité, Santander, mediante interrupciones en la vía. (Le puede interesar: ["Productores de panela irán a Paro Nacional el próximo 28 de junio"](https://archivo.contagioradio.com/paneleros-iran-a-paro-nacional-el-proximo-28-de-junio/))

**Luis Fernando Paipilla, presidente ejecutivo de Dignidad Agropecuaria,** declaró que el ministro de agricultura Ricardo Lozano llegó el pasado miércoles a Barbosa sobre las 9 de la mañana junto a los gobernadores de Boyacá y Santander, así como 16 alcaldes de ambos departamentos para iniciar la discusión sobre la crisis del sector. La discusión se prolongó hasta las 11:30 de la noche, momento en que decidieron levantar la reunión para poder descansar.

Por parte de los paneleros, en la Mesa participaron representantes de Santander, Boyacá, Cundinamarca y Nariño; "y seguramente llegarán más delegados", afirmó Paipilla, en la medida en que otras agremiaciones de panela se enteren del paro que se adelanta en esta región del país. Adicionalmente, el líder sostuvo que otros gremios como los cafeteros, y de transportadores están pensando unirse a la protesta, porque hay afectaciones comunes a todo el sector del agro que los afectan.

### **Paneleros no tienen afán, necesitan soluciones de fondo** 

Los paneleros han denunciado que están produciendo a pérdidas puesto que mientras el kilo de panela cuesta 1.200 pesos, la producción está sobre los 3 mil pesos; de allí que se vean afectadas todas las familias que derivan su sustento del proceso de siembra, cosecha y posterior farbicación. Todo ello, mientras un solo trapiche puede dar trabajo hasta a 40 familias.

Con miras a resolver la crisis de fondo y tomando en cuenta la llegada de más delegaciones, Papilla aseveró  que **los paneleros "no tenemos ningún afán de irnos, el único afán que tenemos es que se solucione la grave crisis panelera"**. Por su parte, manifestó que el Gobierno debería llevar propuestas, tomando en cuenta que "aquí se están defendiendo la comida y trabajo de muchos". (Le puede interesar: ["Tras 5 años del Paro Agrario la situación del campo no ha mejorado"](https://archivo.contagioradio.com/hay-una-crisis-acumulada-en-varios-sectores-agricolas-cesar-pachon/))

Sobre la propuesta del Ministro, para crear un plan de ordenamiento de producción, Paipilla expresó que en la Mesa de Negociación se debe definir qué pasará con la producción de panela a nivel nacional, "**pero vemos que se nos están yendo por las ramas sobre lo que se debe hacer"** con este tema. Asimismo, pidió que el Gobierno aporte propuestas con fundamento e ideas claras, para poder avanzar en el pliego de peticiones.

### **Se estaría produciendo panela falsa, a partir de azúcar y melaza** 

En medio de la protesta, las familias paneleras han denunciado que se está produciendo panela en derretideros de azúcar principalmente en Valle del Cauca. De acuerdo al Presidente de Dignidad Agropecuaria, "casi se puede decir que es una banda de mafiosos haciendo panela falsa con azúcar y melaza"; todo ello en pequeños trapiches, pero sin que autoridades como el INVIMA, o entes de control tomen acciones sobre el asunto.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38021991" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38021991_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
