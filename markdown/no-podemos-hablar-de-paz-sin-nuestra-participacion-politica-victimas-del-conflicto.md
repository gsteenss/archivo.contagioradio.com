Title: No podemos hablar de paz sin nuestra participación política: víctimas del conflicto
Date: 2019-06-27 19:55
Author: CtgAdm
Category: Entrevistas, Paz
Tags: circunscripciones especiales de paz, Defendamos la Paz, Festival de las Memorias, JEP
Slug: no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/victimas-la-chinita-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Contagio Radio] 

Más de cien organizaciones, comunidades y cabildos indígenas,  víctimas del conflicto reafirmaron su compromiso con la implementación del Acuerdo de La Habana por medio de un comunicado en el que expresaron su apoyo a las propuestas hechas  por la plataforma **Defendamos la Paz,** entre las que se incluyen de las circunscripciones especiales para las víctimas, propuesta que fue hundida en el Congreso y que podría revivir si existe voluntad política.

El comunicado también expresa su respaldo a  la **Jurisdicción Especial para la Paz (JEP),** y la promoción de iniciativas como los **Festivales de las Memorias** promovidos por la Comisión de Justicia y Paz donde participan actores responsables de las afectaciones en procesos de justicia restaurativa.

Sobre la intención de revivir las 16 circunscripciones especiales, **Martha Aguirre, presidenta de la fundación Sonrisas de Colores y representante de los familiares de víctimas de la masacre de concejales de Rivera en Huila**, señala que es lo mínimo que el Gobierno y el Congreso pueden dar a las víctimas, "no podemos seguir hablando del Acuerdo de Paz y del conflicto sin tener en cuenta nuestra participación y sobre todo sin una representación política".

### Es necesario el diálogo afirman las víctimas

De igual forma afirman la necesidad de reanudar los diálogos con el ELN contando con la comunidad internacional y las  **Naciones Unidas** como garantes además de comenzar a realizar un trabajo con enfoque diferencial en los territorios donde además de presentarse desplazamientos forzosos y prácticas como la intimidación, se están imponiendo formas de pensar y sentires alentados por sectores políticos.

"En las ciudades no se ve, mientras nosotros que viajamos a las comunidades, nos damos cuenta que están temerosos porque ya se ve cierta representación de grupos armados y nos da miedo que empiece una nueva ola de terror", afirma Martha Aguirre. [(Lea también: Plataforma Defendamos la Paz recogerá firmas para curules de víctimas)](https://archivo.contagioradio.com/defendamos-la-paz-2/)

Consecuencia de estas prácticas surgen **"la ausencia de verdades"**, los silenciamientos forzosos  y por consiguiente el asesinato de líderes sociales, "no podemos denunciar o nos matan. En comunidades rurales se han instalado personas en cada comunidad al estilo de las famosas CONVIVIR o de informantes pagos", señala el comunicado que evidencia la carencia de libre expresión, movimiento y derecho a la asociación.

<iframe id="audio_37725536" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37725536_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
