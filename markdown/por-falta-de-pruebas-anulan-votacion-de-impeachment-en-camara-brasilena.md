Title: Por falta de pruebas anulan votación de Impeachment en Cámara brasileña
Date: 2016-05-09 12:27
Category: El mundo, Política
Tags: Anulan votación impeachment, Dilma Roussef impeachment, Golpe de estado Brasil
Slug: por-falta-de-pruebas-anulan-votacion-de-impeachment-en-camara-brasilena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/dilma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [9 May 2016]

Ante la Camara de Diputados deberá ser votado nuevamente el proceso de juicio político en contra de la presidenta Dilma Rousseff, por falta de pruebas para imputar a la mandataria.

Asi lo dio a conocer el presidente de la Camara (e) Wlaldir Maranhao, del Partido Progresista, atendiendo a la solicitud presentada por el abogado Jorge Eduardo Cardozo, entendiendo que “efectivamente ocurrieron vicios que tornaron nula de pleno derecho a la sesión en cuestión”.

En reacción a la determinación, que anula el proceso de votación del pasado 17 de abril, la presidenta aseguro que se debe tener "cautela" ante las consecuencias que esta pueda traer, añadiendo que "vivimos una coyuntura de mañas y artimañas, tenemos que continuar atentos a los que está en curso".

De acuerdo con el informe de Maranhao, después de retornar el proceso a la Cámara de Diputados, se deberán realizar hasta cinco sesiones para llegar a una nueva votación que determine la admisibilidad de la solicitud de Juicio Político.
