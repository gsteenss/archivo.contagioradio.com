Title: En lo corrido de mayo han sido asesinados cinco integrantes de juntas de acción comunal
Date: 2020-05-21 08:20
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Ejército, juntas de acción comunal
Slug: en-lo-corrido-de-mayo-han-sido-asesinados-cinco-integrantes-de-juntas-de-accion-comunal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Juntas de Acción Comunal

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cinco crímenes contra integrantes de Juntas de Acción Comunal se han cometido durante el mes de mayo; **Aramis Arenas Bayona en Becerril Cesar, Digno Buendía en Cúcuta, Taylor Gil en Cáceres Antioquia, Carlos Andrés Sánchez Villa en Catatumbo y Rubén Guerrero en Mercaderes, Cauca**, han sido las víctimas de la escalada de violencia atribuida a las jornadas de erradicación forzada que se han dado en medio de la cuarentena. [(Le recomendamos leer: Ejército habría asesinado a Emerito Digno Buendía: Ascamcat)](https://archivo.contagioradio.com/ejercito-habria-asesinado-a-emerito-digno-buendia-ascamcat/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Guillermo Cardona fundador de la Confederación Nacional de Acción Comunal afirma que se trata de un problema estructural, vinculado a la clase política local, "**cuando el comunal no se somete, los políticos lo estigmatizan**" asegura, pero enfatiza que a partir del octubre de 2017 se ha dado un incremento en la violencia por el compromiso que han demostrado las **Juntas de Acción Comunal** y sus integrantes en la sustitución voluntaria de cultivos de uso ilícito. [(Lea también: Asesinan a líder Aramis Arenas Bayona en Becerril, Cesar)](https://archivo.contagioradio.com/asesinan-a-lider-aramis-arenas-bayona-en-becerril-cesar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El dirigente campesino atribuye el incremento de la violencia durante el mes de mayo a la intensificación erradicación forzada y la persecución del campesino a través del accionar del Ejército, "estamos ante una intensificación del conflicto en los territorios y la mayor víctima es el campesinado. [(Le recomendamos leer: "La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo)](https://archivo.contagioradio.com/la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Juntas de acción comunal busca soluciones estructurales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Le insistimos al Gobierno que lo que tiene que hacer es un plan de emergencia de producción agropecuaria, que se subsidie al campesino y se le apoye", pero en su lugar, considera que el interés del Gobierno "es expulsar al campesino del territorio y entregarle las tierras a las multinacionales". [(Le puede interesar: Serranía de las Quinchas lleva en sus entrañas el dolor de una guerra que nunca para")](https://archivo.contagioradio.com/la-serrania-de-las-quinchas-lleva-en-sus-entranas-el-dolor-de-una-guerra-que-nunca-para/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cardona señala que este problema se ha intentado solucionar con el Gobierno, sin embargo este solo responde con esquemas de seguridad para los líderes comunales, **"qué esquemas para un campesino si el 90% jamás recibió una amenaza, '¿como sabemos a qué campesino tienen en la lista?"**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El problema del campesino es que trabaja o muere"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cardona afirma que la sustitución forzada es el verdadero problema pues "está demostrado por la [ON](https://twitter.com/ONU_es)U que cuando se sustituye concertadamente con el campesino la resiembra de coca disminuye", así lo confirma el informe elaborado por las Naciones Unidas sobre el monitoreo y la verificación del programa de erradicación voluntaria, **mientras que la resiembra de cultivos de uso ilícito es del 35% con la erradicación forzada, cuando las comunidades lo hacen de forma voluntaria la reincidencia es solo el 0,6%.**  
  
"El problema del campesino es trabajar o morir", pues es el mismo Estado el que está generando estos factores de conflicto, ese es el origen de la violencia, expresa el dirigente que considera, se tendrá que adelantar un paro en medio de la pandemia, **"nos tienen amordazados pero los grandes dinero se si movilizan, ¿qué hacemos nos vamos a morir y nos van a seguir matando y nosotros vamos a seguir inmovilizados?".** Reducción de cultivos de uso ilícito es un mérito de las comunidades.[(Le puede interesar: Reducción de cultivos de uso ilícito es un mérito de las comunidades)](https://archivo.contagioradio.com/reduccion-de-cultivos-de-uso-ilicito-es-un-merito-de-las-comunidades/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
