Title: EPS adeudan $170 mil millones a Hospital Universitario de Santander
Date: 2016-11-29 13:38
Category: Movilización, Nacional
Tags: Deudas de EPS, Hospital Universitario de Santander, Ley 100, Manifestación por no pago de salarios
Slug: eps-adeudan-170-mil-millones-a-hospital-universitario-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/HUS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Resander] 

###### [29 Nov 2016] 

Más de 70 empleados del Hospital Universitario de Santander dieron inicio el pasado lunes sobre las 7:00pm a **una manifestación pacífica frente a las instalaciones del Hospital, exigiendo el pago de sus salarios.** Sin embargo la problemática va hasta la imposibilidad de comprar medicamentos e insumos hospitalarios necesarios para la atención de más de 600 pacientes debido al incumplimiento de las EPS.

José Orlando Quintero gerente encargado del HUS subrayó que existe una deuda por parte de las EPS de más de  \$170 mil millones, de los cuales “\$45 mil millones corresponden a **E.P.S que ya fueron liquidadas** como es el caso de Solsalud, Saludcoop, Caprecom y Comfenalco y **esta plata es muy dudable que se logre recuperar”.**

El gerente agregó que “otros \$ 45 mil millones son deudas que ya tienen más de 360 días, lo que quiere decir que también son cuentas de dudoso recaudo” y que **sólo queda vigente la posibilidad de recuperar \$80 mil millones”.**

Juan Pablo Serrano subgerente de servicios quirúrgicos, aseguró que comprenden la grave situación de los y las trabajadoras y las directivas han adelantado trámites con la Superintendencia de Santander **“ellos dijeron que iban a desembolsar algunos recursos, pero hasta el momento no nos han llegado”.**

Una de las trabajadoras reveló que las directivas del HUS se habían comprometido a pagarles la totalidad de los salarios atrasados, en Noviembre “pero sólo nos dieron el 50% en Septiembre y otro 50% en Noviembre, eso no alcanza para nada, **tenemos hijos que alimentar, cuotas que pagar, la vivienda y además venir a trabajar así sea a pie”.**

### **¿Cómo está funcionando el Hospital?** 

Debido al no pago, muchos empleados han renunciado y **“el hospital no da abasto, quienes hemos decidido continuar tenemos que asumir una recarga laboral y sin ninguna garantía,** lo hacemos porque somos humanos y pensamos en el bienestar de los pacientes”.

Denunciaron también que hace 6 años se dio inicio a una reforma estructural en las UCI y urgencias, que hasta el momento no ha tenido ningún avance, **“nos preguntamos que pasó con todo ese dinero invertido y porque después de tanto tiempo no están terminadas las obras”, además la renuncia de empleados tiene a esos servicios funcionando de manera precaria.**

La exigencia es que haya celeridad en sus pagos y con ello hacen un llamado a las empresas prestadoras de salud que no han saldado cuentas con el hospital para que lo hagan y **garantizar así su estabilidad laboral y el funcionamiento del centro hospitalario.**

Así la situación los manifestantes resaltan que se mantendrán en las instalaciones del HUS hasta que sea resuelta su situación y les den razón de sus salarios y el futuro del centro de salud, **"porque nos tienen de un lado para otro y nadie da razón, entonces aquí estaremos hasta que nos respondan".**

###### Reciba toda la información de Contagio Radio en [[su correo]
