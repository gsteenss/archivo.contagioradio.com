Title: Animalistas lamentan retroceso de la C.Constitucional en protección animal
Date: 2018-08-23 16:55
Category: Ambiente, Nacional
Tags: corridas de toros, Corte Constitucional, Plataforma Alto, tortura
Slug: lamentan-retroceso-proteccion-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/toros-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Ago 2018] 

Ante la decisión de la Corte Constitucional de anular la sentencia **C-041 de 2017,** que fijaba un plazo de **dos años para prohibir las corridas de toros,** peleas de gallos y demás actos de violencia que involucran animales en lugares en Colombia, miembros de organizaciones animalistas estudian nuevos pasos a seguir para frenar este tipo de actos crueles contra aquellos que consideran seres sintientes.

**Natalia Parra, vocera de la Plataforma ALTO,** señaló que no se sentía sorprendida por el fallo, puesto que quienes integran la Corte Constitucional son "personas muy conservadoras, de la linea del ex procurador Ordoñez", sin embargo, calificó como **anacrónico,** el hecho de que en el país se "continue la tortura contra los animales". (Le puede interesar: ["En manos del Congreso quedó la prohibición de las corridas de toros"](https://archivo.contagioradio.com/prohibicion_corridas_toros_congreso/))

Con esta nueva intención, el Congreso no está obligado a legislar sobre el tema como se contempló en la sentencia inicial, ni serán sancionados quienes participen de peleas de gallos, por ejemplo, en lugares donde está practica tiene un arraigo cultural. A pesar de este anuncio, Parra indicó que por ahora, lo mejor es esperar que salga el fallo para poder estudiarlo y determinar como actuar frente al mismo.

### **"En este momento la cautela es nuestro mejor aliado"** 

Aunque en la actual legislatura se radicó un Proyecto de Ley para prohibir las corridas de toros, la vocera de la Plataforma Alto afirmó que dicho Proyecto se ha caído en reiteradas ocasiones en el pasado, particularmente porque "ahora hay algunos congresistas que por temas propagandísticos apoyan estas iniciativas", hecho que mantiene a **Colombia, como uno de los 8 países dónde las corridas son legales** junto con Portugal, España, Francia, México, Perú, Ecuador y Venezuela.

Para Parra es claro que al Congreso no le interesa legislar sobre el asunto porque allí, hay muchos parlamentarios que tienen apoyos de sectores ganadores o que obtienen sus recursos "a costa del maltrato animal", también, porque **eliminar las corridas de toros sería una transgresión al 'statu quo'.** (Le puede interesar: ["Centro Democrático busca volver patrimonio las corridas de toros"](https://archivo.contagioradio.com/centro_democratico_corridas_toros/))

### **"Tenemos que recordarle a la Corte que Caballos, Gallos y Toros son sujetos de derechos"** 

Sí bien la integrante de la Plataforma hizo un llamado a la cautela, subrayó que las personas juegan un rol muy importante, pues "si la Corte nota que la ciudadanía no recibió bien el dictamen, puede matizar el fallo y hacer que no venga tan desfavorable como podría serlo", e igualmente motivó para que la movilización en redes sociales y en las calles continúe.

Por último, Parra recordó que la Corte Constitucional ya había tenido una linea favorable a las iniciativas ambientales y animalistas como **el reconocimiento del Río Atrato como sujeto de derechos,** de forma tal, que también habría que recordarle que "Caballos, Gallos y Toros" también caben dentro de la misma figura.

<iframe id="audio_28050920" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28050920_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
