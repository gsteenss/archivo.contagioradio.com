Title: Yo prefiero la oposición de Uribe y no un acuerdo de paz con él: Molano
Date: 2016-11-15 12:39
Category: Entrevistas, Paz
Tags: Alfredo Molano, Juan Manuel Santos, Nuevo Acuerdo de Paz, paz
Slug: yo-prefiero-oposicion-de-uribe-y-no-acuerdo-de-paz-con-el
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Farc-Semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [15 Nov. 2016] 

Luego de haber conocido el nuevo acuerdo final de paz el pasado lunes,  las numerosas reacciones que se han generado alrededor de este no se han hecho esperar. **El escepticismo, la incertidumbre, la confusión así como la esperanza han sido las palabras usados por diversos analistas**, por la sociedad civil e incluso por los negociadores de las Farc y del Gobierno nacional.

Para Contagio Radio, Alfredo Molano sociólogo y periodista aseguró que “todavía no sabemos qué pueda pasar, creo que no se ha definido nada, entonces hay una gran incertidumbre. Sin embargo, **no podemos dejar de decir que el acuerdo es válido. Este es el final, que no es susceptible de ponerse en consideración de los del No”.**

Y es que ya varios sectores que se opusieron al anterior acuerdo, como el Centro Democrático, el ex procurador Alejandro Ordoñez y Claudia Wilches representante de algunos sectores cristianos, se han manifestado en contra de este segundo y definitivo acuerdo de paz, argumentando que debió haberse dado a conocer a ellos antes de mostrarlo ante toda Colombia.

Este, que se ha manifestado es el acuerdo final, tuvo cambios sustanciales y según el propio presidente de Colombia, Juan Manuel Santos **se lograron precisiones y cambios en 56 de los 57 temas que fueron abordados en el nuevo acuerdo de paz. **Le puede interesar: [Fue un “error no haber incluido a magistrados internacionales” en jurisdicción de paz](https://archivo.contagioradio.com/fue-un-error-no-haber-incluido-a-magistrados-internacionales-en-jurisdiccion-de-paz/)

“Este acuerdo tiene punto final así diga lo que diga el señor –Álvaro – Uribe. De todas maneras una democracia necesita oposición. **Yo prefiero la oposición de Uribe y no un acuerdo con él, que parece un frente nacional nuevo. Así que haga oposición, eso está bien, tiene derecho" **aseveró Molano.

Frente a las diversas formas que se han planteado para poder implementar el nuevo acuerdo de paz, Molano manifestó que hacer cabildos sería una “locura”, el plebiscito necesita tiempo y quedaría el Congreso “no se puede desconocer que en esta institución hay un sector que se opone además de la indisciplina de los de Sí, es decir puede convertirse en un proceso lento, difícil y costoso”.

Molano afirma, que en las Farc ha visto una gran madurez **“veo un pragmatismo histórico por parte de las Farc. Me parece que han logrado entender claramente los nuevos vientos de la historia y aceptar algo que era muy difícil aceptar hace unos años”** y concluye diciendo que en esta guerrilla se está teniendo una nueva “ideología fresca, de banderas nuevas, con una mirada ideológica distinta y esa es la gran promesa que le han hecho a los colombianos”. Le puede interesar: [Discurso de Iván Márquez tras nuevo acuerdo de paz](https://archivo.contagioradio.com/discurso-ivan-marquez-tras-nuevo-acuerdo-paz/)

<iframe id="audio_13768206" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13768206_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
