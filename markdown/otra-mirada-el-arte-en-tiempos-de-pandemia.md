Title: Otra Mirada: El arte en tiempos de pandemia.
Date: 2020-07-07 10:43
Author: PracticasCR
Category: Nacional, Otra Mirada, Otra Mirada, Programas
Tags: arte, cultura y arte, Patricia Ariza, teatro la candelaria
Slug: otra-mirada-el-arte-en-tiempos-de-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/4478936_orig.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: revistaelsotano

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la llegada del COVID-19 a nuestro país todas las actividades laborales y educativas se han detenido, asistir a actividades culturales y artísticas ya no es una posibilidad. Aún así, todos los actores y actrices, músicos y demás artistas han reinventado su forma de crear. Sin embargo para muchos, asistir a estos eventos virtualmente no hace justicia a la belleza y al sentimiento de presenciarlos en persona. (Le puede interesar: [Pedimos a la jep que se abra un caso de violencia sexual: humanas](https://archivo.contagioradio.com/pedimos-a-la-jep-que-se-abra-un-caso-de-violencia-sexual-humanas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, aun cuando la opinión de los espectadores es bastante clara, no se ha conocido qué opinan los artistas de este nuevo formato. Por esto, invitamos a Edson Velandia, compositor y cantante Festival de la Tigra, Cesar Badillo, director de teatro e integrante de La Candelaria, Patricia Ariza, dramaturga y fundadora del Teatro La Candelaria y Santiago Rivas director y presentador de Puntos Capitales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ya que muchos festivales y obras dirigidas por estos artistas se han presentado con mucho éxito, era pertinente preguntar cómo ha sido gestionar los eventos por internet y si presentarlos por esta vía es realmente uno de los mayores obstáculos para crear arte. Además, nos comentan qué significa pasar a la virtualidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, nuestros invitados comparten cuál es el deber del artista en medio de la pandemia, cómo se están preparando para lo que viene para el arte y cuál va a ser el mayor de los retos, en miras de que la situación de emergencia no está pronta a acabar.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para finalizar, explican cómo se puede seguir apostando a algo que es importante pero que, en una economía que está en crisis, es lo primero que se ha olvidado. (Si desea escuchar el programa del 2 de julio: [¿Colombia necesita una reforma tributaria?](https://bit.ly/2ZBmY5u))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El coronavirus nos va a permitir crear y construir de otra manera.
>
> <cite>Cesar Badillo</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/855105355018635","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/855105355018635

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
