Title: Al Gobierno no le gusta que se hable de impunidad: Plataformas de DD.HH.
Date: 2020-03-06 13:39
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Michel Forst, Naciones Unidas, ONU
Slug: al-gobierno-no-le-gusta-que-se-hable-de-impunidad-plataformas-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Michel-Forst-e-Iván-Duque.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este jueves 5 de marzo, organizaciones defensoras de derechos humanos convocaron a un evento con periodistas para socializar su análisis sobre la respuesta del Gobierno a los informes en materia humanitaria que presentaron la alta comisionada de las Naciones Unidas para los DD.HH. Michelle Bachelet y el relator especial para las Naciones Unidas sobre la situación de personas defensoras de estos derechos, Michel Forst.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **En contexto: Dos informes que señalan las vulneraciones a los derechos humanos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras el primer informe es un balance que se realiza anualmente y es de carácter general sobre la temática, el segundo se produjo luego de una visita realizada en 2018 por invitación del entonces presidente, Juan Manuel Santos. (Le puede interesar:["Cinco claves para entender el informe de Michel Forst sobre la situación de DD.HH. en Colombia"](https://archivo.contagioradio.com/cinco-claves-para-entender-el-informe-de-michel-forst-sobre-la-situacion-de-dd-hh-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como lo reveló el mismo Relator, su intención era realizar una segunda visita en 2019 para hacer una revisión de lo constatado en Colombia durante el año anterior, pero el Gobierno no autorizó su ingreso al país. La intención de las visitas es verificar la situación de personas defensoras de DD.HH., que es el enfoque de su oficina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las razones del Gobierno para rechazar el informe de Forst**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diana Sánchez, vocera de la Coordinación Colombia-Europa-Estados Unidos (CCEEU), señaló que el gobierno tiene la obligatoriedad de acatar las recomendaciones que hacen tanto el Relator como la Alta Consejera; pero señaló que informes como los que presentaron, Colombia los ha recibido hace mucho tiempo porque en el país son comunes las violaciones a los DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Sánchez resaltó que el informe de Forst es especial en tanto señala una violencia estructural que tienen que enfrentar los defensores de derechos humanos y hace recomendaciones en ese sentido. De esta forma, el relator reconoce (como las plataformas de DD.HH.) que hay conflictos en cuanto a tierras, ambiente y derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En contraste, la vocera de la CCEEU declaró que el Gobierno ha insistido en que si en Colombia asesinan a líderes sociales, es por que hay Grupos Armados Organizados (GAO), minería ilegal y narcotráfico. Otro punto en disputa respecto a la mirada del Gobierno son las garantías, porque las plataformas reclaman que la única forma en que el Gobierno brinda garantías es mediante la seguridad, un enfoque reducido e ineficaz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Al Gobierno no le gusta que se hable de impunidad**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El otro tema que "no le gusta al Gobierno" del informe de Forst es la impunidad, porque mientras la Fiscalía insiste en que hay más de un 50% en esclarecimiento de los asesinatos a líderes sociales, Sánchez aseguró que podría hablarse de justicia cuando se llega a la última instancia. Pero en los mismos informes de la Fiscalía, de los 323 casos registrados sobre asesinatos, sólo 23 tienen sentencia (es decir, un 11%).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alirio Uribe, vocero de la Plataforma Colombiana de Derechos Humanos, Democracia y Desarrollo (PCDHDD), se sumó a esta idea, añadiendo que el Gobiernoda por esclarecido un caso cuando se archiva la investigación o matan al autor material del asesinato. A ello se suma que la mayoría de sentencias que hay sobre asesinatos a defensores de derechos humanos es sobre autores materiales, lo que aumenta la impunidad sobre autores intelectuales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El debate público sobre el informe se desvió, eso quería el Gobierno**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uribe aseveró que la estrategia del Gobierno cuando cuestionó el informe de Forst, lo que buscaba de fondo es que no se discuta el contenido del mismo, porque "si hay algo que hay que mirar es el contenido del informe y las recomendaciones", pero "el debate público se desvió". Para el ex congresista, es claro que el Gobierno sabe que [el informe tiene legitimidad](https://asociacionminga.co/index.php/2020/03/02/carta-de-la-sociedad-civil-respaldamos-la-labor-de-la-oacnudh-en-colombia-y-a-alberto-brunori/)y las Naciones Unidas seguirán produciendo este tipo de documentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, Uribe invitó a que se revisen las recomendaciones que realizar, las cuales resumió en tres ideas centrales Lida Núñez, de la Alianza de Organizaciones Sociales y Afines por una Cooperación Internacional para la Paz y la Democracia en Colombia. (Le puede interesar: ["En 2019 se registraron 25.303 nuevos desplazados en Colombia: CICR"](https://archivo.contagioradio.com/en-2019-se-registraron-25-303-nuevos-desplazados-en-colombia-cicr/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde su visión, la primera recomendación que hacen los dos informes es que el Estado, y cada una de sus instituciones, cumpla con las funciones constitucionales que están encargadas. En segundo lugar, los informes recomiendan la implementación con decisión y de forma integral del Acuerdo de Paz; por último, el tercer elemento de recomendaciones tiene que ver con tomar medidas para entender los conflictos ambientales que están tomando lugar en el país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
