Title: Cunha es condenado a 15 años de prisión
Date: 2017-03-30 16:04
Category: El mundo, Judicial
Tags: Brasil, Dilma Rouseff, Eduardo Cunha, Impeachment, Lava Jato
Slug: cunha-lavajato-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/67019.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Gaceta Mercantil 

###### 30 Mar 2017 

Eduardo Cunha,  expresidente de la Cámara de diputados brasileña y principal impulsor del juicio político en contra de Dilma Rousseff, fue condenado este jueves a **15 años y cuatro meses de prisión por corrupción,lavado de dinero y evasión de divisas**.

De acuerdo con la sentencia, el imputado dentro del caso ´Lava Jato´ r**ecibió 1,5 millones de dólares en sobornos por un contrato de Petrobras**, investigación por la cual tuvo que dimitir a su cargo como parlamentario. Por orden del juez Sergio Moro el pasado mes de octubre fue enviado a prisión, mismo que hoy dictó la sentencia contra en su contra.

El argumento del colegiado es que el parlamentario por el Partido del Movimiento Democrático Brasileño (PMDB), al que también pertenece el presidente designado Michel Temer, **recibió en 2011 dineros por dar cobertura política a la compra de una explotación petrolífera en el país africano de Benín**.

De acuerdo con las investigaciones, un pago de 10 millones fue consignado a una cuenta en Suiza, bajo la titularidad del ciudadano João Augusto Rezende Henriques, un lobbista del PMDB, de los cuales 1,5 millones fueron transferidos a la cuenta de **Orion, una empresa de Cunha domiciliada en el mismo país europeo**.

Además del presente proceso, **Cunha deberá responder las acusaciones por el supuesto cobro de cinco millones de dólares en otro contrato de Petrobras y por los sobornos en un fondo público de inversión**. Le puede interesar: [23 ciudades de Brasil se movilizan contra reformas de Temer](https://archivo.contagioradio.com/23-ciudades-de-brasil-se-movilizan-contra-reformas-de-michel-temer/)
