Title: Concejo de Bogotá "parece un comité de aplausos para Peñalosa": Manuel Sarmiento
Date: 2018-06-01 13:17
Category: Nacional, Política
Tags: Concejo de Bogotá, Enrique Peñalosa, Manuel Sarmiento, rendición de cuentas Peñalosa
Slug: concejo-de-bogota-parece-un-comite-de-aplausos-para-penalosa-manuel-sarmiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/concejo_de_bogota_-_ee-e1527876994847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [01 Jun 2018] 

Durante la presentación del informe de Gestión del alcalde de Bogotá, Enrique Peñalosa, en el Concejo de la ciudad, el concejal de oposición, Manuel Sarmiento, denunció que **no les fue permitido controvertir dicho informe**.Por esto, calificó la sesión como un “comité de aplausos” por lo que algunos concejales se retiraron del recinto.

El concejal indicó que la labor de Peñalosa no ha sido beneficiosa para la ciudad teniendo en cuenta sus políticas que “generan daños irreparables a la ciudad". Se refirió al metro elevado, la urbanización de la reserva Thomas Vander Hammen, la política de privatización de la educación y la salud. Dijo que en los dos años de gobierno del alcalde, la pobreza ha pasado del 10.5% al 12.2%.

### **Presidente del concejo no permitió que hubiese debate al rededor de la presentación de Peñalosa** 

De acuerdo con el concejal del Polo Democrático, Manuel Sarmiento, es la primera vez que la rendición de cuentas del alcalde **“la hace en el Concejo de Bogotá”**. Esto, teniendo en cuenta que generalmente la hace en algún auditorio o colegio. Este año, la rendición se hizo en el marco de una de las sesiones del concejo.

Con esto en mente, los concejales le preguntaron al presidente del mismo si había posibilidad de que, cuando Peñalosa terminara de dar su informe, se abriera un tiempo para controvertir el mismo. Sin embargo, **el debate que propuso el concejal**, no fue tenido en cuenta por el presidente que manifestó que, en presencia del alcalde no habría debate.

Por esto, el concejal Sarmiento tomó la decisión de **retirarse del recinto** y argumentó que el Concejo “no se puede burlar de esa manera y no se le puede arrodillar al alcalde mayor de Bogotá”. Recordó que a los concejales de oposición “se les debe garantizar el uso del derecho a la palabra para poder controvertir las declaraciones del alcalde”. (Le puede interesar:["Peñalosa desconoce institucionalidad en materia ambiental frente a la Van Der Hammen"](https://archivo.contagioradio.com/penalosa_van_der_hammen_propuesta_distrito/))

### **Esto dijo Peñalosa en su rendición de cuentas** 

Por su parte, el alcalde Enrique Peñalosa señaló en la rendición de cuenta que su alcaldía ha destinado **250 millones de pesos** para el diseño de proyectos viales de la capital. Afirmó que el próximo año entregará 12 obras de infraestructura incluyendo las nuevas troncales de Transmilenio que realizará.

<iframe id="audio_26305742" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26305742_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

######  {#section .osd-sms-wrapper}
