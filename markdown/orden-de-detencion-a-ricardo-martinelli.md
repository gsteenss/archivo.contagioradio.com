Title: Escuchas ilegales de Ricardo Martinelli podrían quedar en la impunidad
Date: 2015-12-22 15:18
Category: El mundo, Judicial
Tags: interceptaciones ilegales, Panama, Riocardo Martinelli
Slug: orden-de-detencion-a-ricardo-martinelli
Status: published

###### Foto: laprensa 

<iframe src="http://www.ivoox.com/player_ek_9816849_2_1.html?data=mp2emJ2YfY6ZmKiak5mJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTX1sjVw9iPrc3ZyMbZx9iPqMafs87Qw9fIs4zBwtfhy9PJsM3djNXcxteJh5SZoqnO0JDVucbYwteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Walid Sayed, defensor de DDHH] 

###### [22 Dic 2015]

La solicitud de arresto del ex presidente panameño tiene que ver con el escándalo de interceptaciones ilegales a más de 150 personas entre las que se encuentran desde empresarios hasta defensores de DDHH, sin embargo, **no se permitió que las víctimas directas hicieran parte del proceso** de demanda al Estado panameño, además la CSJ ha mencionado a Martinelli en calidad de imputado sin tener claridad de los delitos cometidos lo cual podría ser causal de nulidad.

También habría una desviación de la justicia, explica Walid Sayed, defensor de DDHH y víctima, puesto que **no se ha demandado al Estado, que realmente es el causante del crimen representado en la persona del ex presidente**, y hay una ausencia de acusaciones concretas dentro del delito marco, además no hay una demanda civil porque la comisión no ha aceptado la gran mayoría de las peticiones a las que solamente les otorgó 5 días para ser presentadas.

Aunque el pleno de la asamblea de la Corte Suprema de Justicia de Panamá ordenó la detención del ex presidente Ricardo Martinelli, **es el juez de garantías quien debe proceder con la restricción de la libertad, lo que significa que deberá tramitar ante interpol el arresto de Martinelli o su solicitud de extradición**, sin embargo, esta medida podría demorarse indefinidamente puesto que la dilación del proceso ha sido, en muchas ocasiones, [propiciado por el mismo juez de garantía](https://archivo.contagioradio.com/corte-suprema-de-panama-investigara-a-ex-presidente-martinelli-por-corrupcion-y-espionaje/)s.
