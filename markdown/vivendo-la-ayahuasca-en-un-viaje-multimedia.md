Title: Viviendo la Ayahuasca en un viaje multimedial
Date: 2015-10-21 13:50
Category: Hablemos alguito
Tags: Ayahuasca Iquitos y Monstruo Voraz, Carlos Suárez Álvarez escritor, Cosmogonía, Relatos transmedia, Viaje Yahé
Slug: vivendo-la-ayahuasca-en-un-viaje-multimedia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/426128_309782962406337_100001239074172_916170_1027471590_n-e1445452474666.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_9115193_2_1.html?data=mpael5add46ZmKial56Jd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb4a5k4qlkoqdh6Ltws3iw9jHpYampJC209rNuNDnjN6Yr9TSt9Xm1tSYuNTWpduZppeSmpWJfaWZk6iYxsqPh8LmzdTgjbiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [21 Oct 2015] 

Miles de occidentales viajan a la selva amazónica en América del sur, con el fin de experimentar “el poderoso remedio visionario” del Yahé; tradición indígena en la que se basa la publicación transmedial, **“Ayahuasca, Iquitos y Monstruo Voraz**”, obra del periodísta español Carlos Suárez Álvarez, quien a través de su trabajo guía al lector/espectador por un sensorial recorrido cosmogónico.

Se trata de un cuento etnográfico con videos, fotos, texto y música, construido por muchas voces, que en singular mixtura expresa las tensiones de un mundo ancestral que se enfrenta a un presente globalizado comandado por las diferentes formas del despojo y la destrucción por propia mano del hombre.

Es un libro-relato que invita a volver una vez más a sus personajes y sus historias, a sus imágenes, un libro que logra interpelar la forma de vida que los pobladores urbanos han asumido y amenaza mundos desconocidos para ellos, experiencias registradas por el autor durante su estadía en la amazonía colombiana.

**“[Ayahuasca, Iquitos y Monstruo Voraz](http://ayahuascaiquitos.com/es/)**” es un valioso documento que deja abierta la puerta a la reflexión y en el que en torno al mundo de la ayahuasca, su autor nos trae noticias de un mundo azotado por las diferentes formas que al día de hoy asume el extractivismo y, que con voracidad inusitada amenaza la vida de la amazonia.

#### Producción: María del Pilar Suárez y Contagio Radio 
