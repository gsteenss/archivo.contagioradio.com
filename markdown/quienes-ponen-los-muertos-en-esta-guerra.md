Title: ¿Quiénes ponen los muertos en esta guerra?
Date: 2016-09-12 06:21
Category: Andres Alba, Opinion
Tags: paz, proceso de paz, Teologia
Slug: quienes-ponen-los-muertos-en-esta-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Si-a-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **¡Por todos los Malco, dijo Sí!** 

#### **Por [Andrés Alba](https://archivo.contagioradio.com/andres-alba/)** 

###### 12 Sep 2016 

Hace unos días camino a mi trabajo tome un taxi. Hable con el taxista todo el recorrido. El tema: Si o No al plebiscito por la paz. Primero escuche sus preocupaciones acerca del Acuerdo Final, la dificultad de leer la “letra menuda”, decía él, y lo difícil de pensar en ver a un guerrillero en política. Trate de argumentar lo que significa dejar de matarnos para empezar a construir paz en los territorios sin la mediación armada de uno u otro actor. Le indiqué que la implementación de lo acordado entre el Gobierno Nacional y las FARC-EP es una tarea de toda la sociedad colombiana. Insistí en la centralidad de las victimas en el acuerdo y en el carácter transicional y en el enfoque de la justicia restaurativa que sostenía los acuerdos. También intenté aclararle que el Acuerdo en ningún momento contenía algo así como “la ideología de género”, pero si un enfoque de género que era parte de la necesaria justicia en un país que pretende ser democrático para todos y todas.

El taxista, muy respetuosos y dialógico, seguía firme por el no. Finalmente le pregunte si era creyente. Con entusiasmo me dijo que sí. Entonces le recordé aquella memoria bíblica escrita en el marco del arresto de Jesús en la que Pedro reacciona: “Simón Pedro, que iba armado de espada, la desenvainó, dio un tajo al sirviente del sumo sacerdote y le corto la oreja derecha –el sirviente se llamaba Malco-. Jesús dijo a Pedro: -Envaina la espada: ¿Acaso no beberé la copa que me ha ofrecido mi Padre?” (Jn 18: 10-11, BP). El sirviente tiene nombre, rostro, es hijo de un solo pueblo. Se llama Malco.

Le dije que para mi decirle sí al plebiscito era una forma de afirmar la necesidad de “envainar” las espadas modernas. Era un paso fundamental para construir paz en forma conjunta sin negar nuestras diferencias políticas o matarnos en nombre de ellas. Atiné a decirle: deberíamos hacerlo al estilo de Jesús: apostarle o elegir la Acción Noviolenta.  
Acto seguido el taxista, un hombre ya avanzado en edad, sin necesitarlo, freno el taxi y me dijo: “yo fui soldado y sé lo que es la guerra. No había visto que Pedro estaba armado. Es cierto, Jesús le invita a envainar su espada. Claro, los zelotes estaban presentes en tiempos de Jesús”.

El espíritu zelote, le recordé, animaba la facción más violenta del judaísmo en la época de Jesús, Los Zelotes. Éste espíritu intentó permear su movimiento. ¿Es casualidad que Pedro tuviera una espada consigo o que uno de los doce sea llamado Simón el rebelde (zelote: Lc 6:15, BP) o que sus discípulos, ante la turba en el momento del arresto de Jesús, digan: “-Señor, ¿usamos la espada?” (Lc 22: 49, BP). Rematé diciéndole: puede que el plebiscito sea un paso necesario para pasar por la cruz y aguardar la resurrección. “¿Qué dice?” –le pregunte-. Me dijo: “vamos por la paz”. Le dije, con un pie en la acera y otro en el taxi,  que para mí el plebiscito no era lo fundamental, pero sí es un momento importante para reclamar mi derecho a la paz y el de mi hijo, Gabo.

Cerré la puerta del taxi y sentí los tantos muertos que ha dejado esta guerra. Pensé en la necesidad de dejar de matarnos y en la oportunidad única que tenemos como país. Y me pregunté: ¿Por qué no decirle sí a un Acuerdo Final que, realmente, demandará de todo nuestro servicio y compromiso para asegurar su implementación? ¿Por qué no desarmarnos como cristianos y como discípulos y discípulas de Jesús para decirle sí al proceso de paz que pasa por el sí al plebiscito? ¿Quiénes ponen, finalmente, los muertos en esta guerra?.

------------------------------------------------------------------------

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
