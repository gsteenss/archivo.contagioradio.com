Title: Reclamantes de tierras en inminente riesgo de desplazamiento por paramilitares
Date: 2016-06-14 17:14
Category: DDHH, Nacional
Tags: Antioquia, Paramilitarismo, Restitución de tierras, Turbo
Slug: reclamantes-de-tierras-en-inminente-riesgo-de-desplazamiento-por-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [14 Junio 2016 ]

Desde hace tres años, las familias que retornaron sin acompañamiento estatal a la vereda Guacamayas en Turbo, Antioquía, vienen siendo **amenazadas y hostigadas por estructuras paramilitares de las Autodefensas Gaitanistas** quienes hace ocho días arremetieron contra las comunidades y las obligaron a desplazarse a la cabecera del municipio.

Según afirma Marta Lucia Peña, del Instituto Popular de Participación de Antioquia, estas familias fueron desplazadas entre los años 96 y 97 y decidieron retornar a sus territorios por cuenta propia, pues no tenían acompañamiento del gobierno, y pese a que en la Ley 1448 vieron la oportunidad de ser restituidos, **el proceso avanza muy lento y la presencia paramilitar dificulta su permanencia**.

A algunas de estas familias les han negado la restitución; otras han sido cobijadas por dos tipos de fallos, uno de un juez civil de restitución de Quibdó, que **ordena que las familias sean protegidas y no desalojadas, hasta que sean emitidos los fallos de restitución**; sin embargo, hasta hace poco la Sala de Restitución de Tierras del Tribunal de Medellín notó que se trataba de un territorio colectivo, y como afirman las familias, el proceso va muy lento.

Pese a que se emitieron las alertas, las autoridades municipales no han actuado. El Ejército fue a la zona pero hasta que las familias solicitaron por segunda vez su presencia, y hasta este lunes se realizó un Comité de Justicia Transicional en el que se estudiaron medidas para iniciar el proceso de asistencia y atención a las comunidades. **Este martes un grupo de campesinos de la vereda llegaron a Bogotá** para reunirse con el Ministro del Interior, la Alta Consejera para los Derechos Humanos, la Oficina del Alto Comisionado para los Derechos Humanos de la ONU y con una delegación de congresistas de Estados Unidos.

<iframe src="http://co.ivoox.com/es/player_ej_11900737_2_1.html?data=kpamkpWbd5ihhpywj5WdaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nOjbHZp8rVjLXSh6iXaaOlwpCaja7St9Xd1drh0ZC0s9HpzcbfjcnJb7HV09nWxc7UpcTdhqigh6eXso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
