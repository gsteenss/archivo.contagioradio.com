Title: Ley de Amnistía urgente para avanzar en implementación de Acuerdos
Date: 2016-12-05 13:55
Category: Nacional, Paz
Tags: Implementación de acuerdo
Slug: ley-de-amnistia-urgente-para-avanzar-en-implementacion-de-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz-contagioradio-e1476125455134.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [5 Dic 2016] 

Después de la refrendación de los acuerdos de paz tanto en Cámara como en Senado, se ha generado un debate en torno a la implementación de los mismos y la activación de los debidos mecanismos, uno de ellos tiene que ver con la puesta en marcha de **la Ley de Amnistía, que genera indultos a los guerrilleros que hayan cometido delitos.**

Sin embargo, para conceder la amnistía a los guerrilleros de las FARC es necesario **incluir otros delitos comunes que sean conexos**, trámite que es aprobado por el Congreso.  De igual modo debe crearse el **Tribunal de paz, instancia que servirá para dar amnistías en casos específicos** y que según el ex magistrado Alfredo Beltrán la creación de este se dará hasta conocer la decisión de la Corte Constitucional sobre el Fast Track, decisión que se aplazó hasta el 12 de diciembre.

Plazo que para las FARC-EP pone en duda la **voluntad política de las instituciones gubernamentales y que imposibilita que acciones como las del día D+5**, en donde las tropas de la guerrilla comenzarían a desplazarse hacia las zonas veredales, se lleven a cabo. Le puede interesar:["Llego el momento de la implementación de los Acuerdos de Paz"](https://archivo.contagioradio.com/llego-el-reto-de-la-implementacion-de-los-acuerdos-de-paz/)

Además, de acuerdo con los convenios de Ginebra, los Estados pueden conceder las amnistías más amplias posibles con el fin de buscar el re establecimiento de la paz en su territorio. Razón por la cual el gobierno de **Colombia puede comenzar a tramitar ya la ley de amnistía, aún sin esperar a que se decida el Fast Track.**

Finalmente Bejarano explica que la  Ley de Amnistía permite conceder indultos generales por  delitos políticos y es aprobada por dos tercios de los votos tanto en Cámara como en Senado. Solo puede ser expedida por el congreso y se hace **“por graves motivos de conveniencia pública** y tiene la característica de cuando es amnistía recibir el olvido por parte del Estado sobre los delitos políticos cometidos por aquellos a quienes se les otorga”.

<iframe id="audio_14637174" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14637174_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
