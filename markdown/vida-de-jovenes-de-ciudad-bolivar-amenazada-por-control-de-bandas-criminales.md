Title: Vida de jóvenes de Ciudad Bolívar amenazada por control de bandas criminales
Date: 2018-03-26 11:49
Category: DDHH, Movilización
Tags: ciudad bolivar, Derechos Humanos, jovenes
Slug: vida-de-jovenes-de-ciudad-bolivar-amenazada-por-control-de-bandas-criminales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Ciudad-bolivar-ovario-fuerte-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Mar 2018] 

Organizaciones defensoras de derechos humanos en la localidad de Ciudad Bolívar, denunciaron el aumento de la violencia en el territorio que se ha reflejado en la acentuación de las bandas de microtráfico, **el asesinato a jóvenes y las amenazas a líderes comunales**, sin que haya algún tipo de acción por parte de la Fuerza Pública o las instituciones distritales.

De acuerdo con Chrístian Robayo, edil de la localidad, el asesinato a jóvenes está estrechamente relacionado con el control de las bandas del microtráfico y la falta de garantías a derechos básicos como la educación, sumado a la problemática de desempleo, “varios de los jóvenes de diferentes barrios, prácticamente la oferta que tienen, es encontrar en cada esquina este tipo de sustancias, **generando una rivalidad entre las organizaciones que se dedican a este tema”** afirmó.

Además, las organizaciones afirmaron que desde enero hasta este mes se han registrado 15 asesinatos de jóvenes, el último en el barrio Potosí, situación que para Robayo, pese a que se ha presentado como un conjunto de hechos aislados **“implican un tema de asesinatos sistemáticos, en donde lo que se viene posicionando es la intimidación a los jóvenes**”.

Los líderes comunales también han sido víctimas de amenazas en los últimos meses, debido a la acción que vienen adelantando en el territorio de protección y denuncia frente a los actos de las bandas criminales.

### **Los panfletos de los diferentes grupos armados** 

Otra de las dificultades con las que tiene que convivir la juventud en Ciudad Bolívar, es con la presencia de panfletos de diferentes grupos armados, algunos se han identificado y firman los carteles como Autodefensas Unidas de Colombia, **mientras que otras, según Robayo, hacen parte de las disidencias de la FARC**.

Estos panfletos restringen la movilidad de los jóvenes y amenazan con que de no cumplir con lo que se les ordena serán asesinados. Estos hechos, de acuerdo con el edil, ya son conocidos por parte de la Fuerza Pública, “la gente ya está demasiado indignada en sus barrios, demasiado intimidada y **queremos es ver resultados concretos para defender la vida de nuestros jóvenes”**. (Le puede interesar: ["Víctimas de Ciudad Bolívar se toman edifico Clav en Bogotá"](https://archivo.contagioradio.com/victimas-de-ciudad-bolivar-realizan-toman-de-edificio-para-la-atencion/))

### **Oportunidades para los jóvenes de Ciudad Bolívar** 

El llamado más importante que están realizando las organizaciones, es que exista una presencia clara y efectiva, de forma integral, “se necesita inversión de diferentes instituciones como el IDRD, que debe verse en el territorio, la inversión también de Integración Social, debe ser más efectiva, no pueden ser eventos, tiene que ser formación permanente con escuelas deportivas, de cultura, de defensa de derechos humanos” manifestó Robayo.

Otra de las apuestas, según el edil, debe ser recuperar los espacios y zonas en donde conviven los jóvenes y que actualmente son ocupados por las bandas del microtrafico, para “ganarle la apuesta por la vida” y no dejar que los jóvenes sean parte de esa máquina. Al mismo tiempo piden una estrategia contundente por parte de la Policía para que desmonte estas organizaciones y las autoridades, que inicien pronto las investigaciones sobre por qué están siendo asesinados los jóvenes.

<iframe id="audio_24809936" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24809936_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
