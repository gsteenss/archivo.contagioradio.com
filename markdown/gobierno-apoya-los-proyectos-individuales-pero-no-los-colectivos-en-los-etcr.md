Title: Gobierno apoya los proyectos individuales pero no los colectivos en los ETCR
Date: 2019-10-29 08:26
Author: CtgAdm
Category: Nacional, Paz
Tags: ETC, excombatientes, Tierra Grata, Unión europea
Slug: gobierno-apoya-los-proyectos-individuales-pero-no-los-colectivos-en-los-etcr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EIACgWQX0AE5CiK.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]@MisionONUCol 

 

La comunidad del Espacio Territorial de Capacitación y Reincorporación (ETCR) de Tierra Grata, Cesar, denuncia la existencia de un interés del Gobierno por desacreditar diferentes procesos que se desarrollan allí, uno de ellos son los proyectos productivos que han visto su mayor obstáculo en el desfinanciamiento y segregación por parte de algunos sectore en la entrega de recursos de cooperación internacional.

Abelardo Caicedo, Coordinador del espacio territorial de Tierra Grata afirmó que desde la firma del Acuerdo se ha visto una falta de implementación e incumplimiento con lo pactado en la Habana respecto a los temas agrarios, de seguridad, conflicto, cultivos de uso ilícito y víctimas, “este gobierno tiene dos discursos uno hacia el exterior para buscar dineros paradójicamente para la paz y otro interno que pretende romper lo pactado”.

Otro tema que resaltó el coordinador del espacio, fue la falta de entrega de tierras que garanticen el trabajo de campesinos y excombatientes, “es lamentable ver que no se ha hecho nada con la Reforma Rural Integral y que en los Espacios Territoriales no se haya entregado la primera hectárea” (Le puede interesar: [La paz no la detiene nadie, afirman excombatientes de FARC](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/))

### Así funcionan los proyectos en los ETCR 

Desde la firma del Acuerdo los excombatientes han presentado 13.000 proyectos, de los cuales solo 500 han sido aprobados, 35 de carácter colectivo y solo 19 de estos con dinero ya desembolsado; según Ubaldo Zuñiga presidente de Economias Sociales del Común (Ecomun), “la Agencia Colombiana para la Reintegración(ARN), tiene un discurso público diciendo que le interesa lo colectivo, pero en verdad le apuestas a lo individual y evitar que tengamos una propuesta económica alternativa”.

Para Caicedo este mecanismo de aprobar menos del 5% de los proyectos colectivos, “es una estrategia del gobierno para disolver, separarnos como organización y movimiento de excombatientes”; al mismo tiempo, expuso uno de los caso más relevantes que permite dar constancia de las dificultades en trámites, documentación e incluso aceptación por parte de los bancos para desembolsar los fondos para ejecutar los proyectos. (

Luego de haber ganado una convocatoria nacional que entregaba recursos dados por la Unión Europea (EU) para la promoción de proyectos asociados a vivienda en Colombia, tuvieron que dirigirse al banco asignado para solicitar el primer desembolso, “el problema se dio desde la primera entrega de dinero en el Banco Davivienda, quienes se negaron a entregarnos la primera parte de este fondo. Asumimos que se debe por los altos niveles de estigmatización que existen en el país hacia nosotros” señaló Caicedo.

Al apelar a esta negativa por parte del banco, quienes se comunicaron directamente con la oficina de la Unión Europea preguntando las razones y orígenes de este dinero, les fue entregado. El segundo desembolso según el Coordinador fue más evidente la estigmatización, “nuevamente llamaron a nuestro representante legal, diciendo que Davivienda había decidido devolver el dinero y la única razón fue que se reservan el derecho de ingreso de recursos”.

La resolución de esta entrega se da con la presentación de una denuncia ante el Consejo Nacional de Reincorporación, y posteriormente la acción de la Superintendencia Bancaria para que este estímulo fuese entregado; incertidumbre, decepción e indignación es lo que expresan los integrantes del ETCR en Tierra Grata al ver la falta de respuesta y olvido por parte del Gobierno, y la discriminación y señalamiento que hay por parte de algunos sectores.

**¿En realidad funcionan los proyectos ya** **desembolsados ?**

La Agencia Colombiana para la Reintegración (ARN) ha aprobado alrededor de 500 proyectos productivos individuales de  peluquería, panadería y carpintería; cada uno con 8 millones de pesos disponibles para ejecutarlo;  “es difícil imaginar la creación de un proyecto producto con solo este dinero,estos son proyectos que de alguna manera están condenados al fracaso, en ellos no hay rentabilidad ni mucho menos garantías de durabilidad”, afirmó  Zuñiga.

Así mismo reconoció que la presencia de proyectos individuales representa un riesgo para la reincorporación,“juegan con las necesidades de nuestros compañeros y compañeras, los han sacado de las cooperativas para intentar proyectos de carácter individual, hacen todo esto para mostrar que los acuerdos están siendo efectivos, cuando la realidad es otra”, dijo Zuñiga.

Asimismo, afirmó también que desde hace un año los integrantes de Farc decidieron pensar iniciativas de manera colectiva, “queremos generar proyectos más rentables, productivos y sostenibles con el tiempo, y esto es más fácil pensarlo con 120 millones reunidos entre todos que con ocho de solo de uno”, agregó Zuñiga.

Según el presidente de Ecomún, de los 19 proyectos desembolsados no hay ninguno que esté en estable funcionamiento, incluso algunos apenas están comprando equipos; destacando los proyectos de piscicultura en Miravalle, y uno de reciclaje de madera en Chocó. (Le puede interesar:[La Roja, una cerveza artesanal fruto de la reincorporación)](https://archivo.contagioradio.com/roja-cerveza-artesanal-reincorporacion/)

A pesar de esto se han logrado avances, especialmente gracias a aportes de organizaciones internacionales, que han hecho seguimiento y apoyo a los proyectos, “nosotros seguimos trabajando, desde ECOMUN  promoviendo la idea de que otras organizaciones solidarias se vinculen a los proyectos que hemos avanzado, de manera lenta, pero  trabajamos para que nos reconozcan”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
