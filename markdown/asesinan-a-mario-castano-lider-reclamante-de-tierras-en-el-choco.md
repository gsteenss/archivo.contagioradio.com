Title: Asesinan a Mario Castaño, líder reclamante de tierras en el Chocó
Date: 2017-11-27 18:48
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, Chocó
Slug: asesinan-a-mario-castano-lider-reclamante-de-tierras-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/mario-castano-bravo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CONPAZ 

###### [27 Nov 2017]

Según la denuncia de la Comisión de Justicia y Paz, hacia las 7 de la noche de este 26 de Noviembre, fue asesinado Mario Castaño Bravo, reclamante de tierras de Curvaradó, en su propia finca, ubicada en la vereda Florida, territorio de La Larga Tumaradó. Desconocidos ingresaron hasta su casa y le dispararon en repetidas ocasiones.

Mario, a quién sus amigos le decían “el loco” por la forma en que denunciaba a empresarios, paramilitares e integrantes de la fuerza pública que son cómplices del despojo al que se resistió durante tres años y dos desplazamientos forzados; había sido llamado la semana pasada por integrantes de las llamadas AGC a una reunión que el mismo denunció.

Castaño, en su trabajo como reclamante de tierras, venía participando activamente en la construcción de una propuesta de reparación colectiva en el territorio que incluye el reconocimiento de las comunidades como legítimos propietarios de las tierras colectivas e impide la continuidad de las operaciones empresariales inconsultas y que atentan contra la vida, la tierra y la dignidad de los habitantes.

Según la organización de Derechos Humanos y la red de comunidades CONPAZ, Mario había sido desplazado de sus tierras en dos ocasiones por resistirse a la puesta en marcha proyectos agroindustriales, madereros y ganaderos. En 1996 había sido víctima junto a cientos de personas de operaciones paramilitares y militares que pretendían hacerse con los territorios.

Por su labor en defensa del territorio y de denuncia de las estructuras paramilitares Mario contaba con medidas en protección de la UNP que habían sido denunciadas por inoficiosas pues constaban de un celular, un chaleco antibalas y la presencia de un escolta que se limitaba a sus traslados. Varios líderes han denunciado que estas medidas son insuficientes frente a la complejidad de las amenazas y de la situación de los territorios.

<iframe id="audio_22332457" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332457_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
