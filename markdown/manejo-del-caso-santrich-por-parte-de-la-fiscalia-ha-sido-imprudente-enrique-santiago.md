Title: Manejo del caso Santrich por parte de la Fiscalía ha sido imprudente: Enrique Santiago
Date: 2018-04-11 13:32
Category: Entrevistas, Paz
Tags: acuerdo de paz, detención de jesus santrich, FARC, Fiscalía General de la Nación, Jesús Santrich, proceso de paz
Slug: manejo-del-caso-santrich-por-parte-de-la-fiscalia-ha-sido-imprudente-enrique-santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/enrique_santiago-e1510854914241.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: websur.net] 

###### [11 Abr 2018] 

Luego de que se produjera la captura de Seusis Pausivas Hernández, conocido como Jesús Santrich, quien es integrante del partido político Fuerza Alternativa Revolucionaria del Común FARC, se han prendido las alarmas sobre la forma como se ha manejado el hecho y el momento en el que ocurre,  pues esta situación tiene  en **riesgo el futuro del Acuerdo de Paz.**

De acuerdo con el abogado Enrique Santiago, quien fue asesor jurídico en las negociaciones de La Habana, la captura de Santrich se da en un momento de grandes **dificultades para la **[**implementación**] de lo acordado entre las FARC y el Gobierno Nacional. Afirmó que no se ha avanzado en las políticas de reincorporación y reforma rural, y en cambio señala que lo sucedido con Santrich aparta la atención de los problemas de la implementación.

Recordó que “va a ser muy difícil que se **garanticen los mínimos** para que el Acuerdo no fracase antes del cambio de la administración presidencial”. Además, dijo que la captura de Santrich se dio “como si fuera una persona peligrosa con riesgo evidente de fuga" para el abogado, esa circunstancia con correspondía con la realidad y agrega que "pudo haber sido notificado garantizando su presencia ante la justicia”. (Le puede interesar:["Captura de Jesús Santrich es un montaje": Iván Márquez"](https://archivo.contagioradio.com/captura-de-jesus-santrich-es-un-montaje-ivan-marquez/))

### **Fiscalía General de la Nación no ha dado el trato correspondiente al caso** 

Como los integrantes del partido político, Santiago recordó que es la Jurisdicción Especial para la Paz la que tiene la competencia de **“realizar un primer examen** de este proceso para comprobar su veracidad y así definir quién es la justicia competente”. Por esto calificó de imprudente el manejo que le ha dado la Fiscalía General de la Nación a este caso diciendo que “la Fiscalía nos tiene acostumbrados a estas actuaciones que le ponen obstáculos al proceso de paz.”

En cuanto a las pruebas presentadas en contra del futuro representante a la Cámara, dijo que **“han sido ocultadas a la defensa de Santrich”.** Cuestionó el hecho de que las pruebas se encuentren en manos de los medios de comunicación del país teniendo en cuenta que la única institución que tiene las pruebas en Colombia es la Fiscalía y son pruebas “que requieren reserva”. (Le puede interesar:["Farc teme nuevas capturas y allanamientos en los próximos días"](https://archivo.contagioradio.com/farc_teme_nuevos_allanamientos_capturas/))

### **Se debe evaluar la veracidad de las pruebas contra Santrich** 

En cuanto a las afirmaciones del partido FARC, quien asegura que la detención de Santrich se trata de un montaje judicial, el abogado reiteró que se debe evaluar la veracidad de las pruebas remitidas por los Estados Unidos teniendo en cuenta que **“no son consistentes”**. Afirmó que es cuestionable que “de una supuesta operación de esta envergadura de los grandes carteles del mundo, no exista ninguna investigación en Colombia”.

El abogado enfatizó en que la actitud de Estados Unidos frente al Acuerdo de Paz, cambió con la llegada de Donald Trump y **“han desacreditado el proceso”.** Con la nueva administración “han demostrado que hacen parte del bloque que está en contra del Acuerdo de Paz”.

Finalmente, indicó que la captura de Santrich abre la posibilidad a un “despliegue de actividades de criminalización” que terminarían **por sepultar el Acuerdo de Paz**. Enfatizó en que detrás de las actuaciones de la Fiscalía “responden a una intención de desacreditar el proceso de paz y esto va a tener un impacto de contribuir al desarrollo de la campaña presidencial de los sectores contrarios al Acuerdo de Paz”.

<iframe id="audio_25299996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25299996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
