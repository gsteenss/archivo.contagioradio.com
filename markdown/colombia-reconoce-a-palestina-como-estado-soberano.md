Title: Colombia reconoce a Palestina como Estado soberano
Date: 2018-08-08 15:54
Category: Otra Mirada
Tags: estado, Palestina
Slug: colombia-reconoce-a-palestina-como-estado-soberano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bandera_palestina_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radio uchile 

###### 08 Ago 2018

En un comunicado la Misión Diplomática de Palestina en Colombia informó del reconocimiento otorgado por parte del Gobierno de Colombia, como un Estado libre, independiente y Soberano.

En la misiva, se muestran altamente agradecidos con la decisión, asegurando que siempre han visto a Colombia como "hermanos infatigables en búsqueda de la paz" y añaden que es sinónimo de "un profundo trabajo de acercamiento entre los gobiernos colombiano y palestino" esfuerzo que afirman tendrá sus resultados en el futuro.

Por último, agregan que esta decisión "aportará significativamente para generar las condiciones necesarias en la búsqueda de la paz en el Medio Oriente", sumándose a la voz de la comunidad internacional que busca soluciones al conflicto que lleva más de 70 años.

A pesar que el comunicado se expide con fecha de hoy, el documento enviado por la Cancillería colombiana, tiene fecha del 3 de agosto, donde expresan que "somos conscientes de las dificultades y sufrimientos que ha enfrentado la población palestina" y añaden "esperamos que las condiciones internas sigan dándose para superar los desafíos que se presentan en el camino".

**Gobierno Duque examinará la decisión**

El nuevo Ministro de Relaciones Exteriores de Colombia, Carlos Holmes Trujillo, manifestó frente a la decisión tomada por el gobierno saliente que se "examinará cuidadosamente sus implicaciones y obrará conforme al Derecho Internacional".

El argumento del nuevo gobierno para anunciar esta evaluación parte su calificación de "prioritario" de mantener relaciones de cooperación con quienes considera como aliados y amigos, " y el compromiso de contribuir, como siempre lo ha hecho, a la paz y la seguridad internacional”.

Se espera que la declaración del canciller, responda a la voluntad manifestada por el Presidente Duque, quien durante la campaña electoral señalo en medio que en un eventual gobierno suyo aportaría lo necesario para lograr la paz en medio oriente, incluyendo el reconocimiento de dos estados, una israelí y otro palestino.

![carta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/DkGTTL8XoAIrSQV-535x800.jpg){.alignnone .size-medium .wp-image-55469 .aligncenter width="535" height="800"}
