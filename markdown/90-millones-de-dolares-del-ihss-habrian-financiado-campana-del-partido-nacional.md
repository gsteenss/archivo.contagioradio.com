Title: 90 millones de dólares del IHSS habrían financiado campaña del Partido Nacional
Date: 2015-07-01 11:43
Category: El mundo, Movilización
Tags: Comisión Internacional contra la Impunidad, David Romero, Frente Nacional de Resistencia Popular, honduras, Instituto Hondureño de Seguros Sociales, Juan Orlando Hernández, Otto Pérez Molina, Partdio Nacional, Radio Globo
Slug: 90-millones-de-dolares-del-ihss-habrian-financiado-campana-del-partido-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/protestas-honduras-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Abya Yala 

<iframe src="http://www.ivoox.com/player_ek_4709957_2_1.html?data=lZydm56Ze46ZmKiak5aJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiY1dTHrcLgxtiYx93Nq8bijM%2Fiy8jNs4zVjNXfx9jNqMbi1cqYxsqPjI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Romero, director de Radio Globo] 

###### [1 Jul 2015] 

Cerca de 330 millones de dólares del **Instituto Hondureño de Seguros Sociales**, IHSS, habrían sido sacados de la institución por altos funcionarios, algunos de ellos, ya detenidos por estos hechos. Sin embargo, también se denuncia que de ese dinero por lo menos **90 millones de dólares fueron a parar a las arcas del oficialista Partido Nacional**, para financiar la campaña del actual presidente Juan Orlando Hernández.

Según la investigación, dirigida por Radio Globo, varios de los funcionarios del **IHSS**, están detenidos actualmente y están siendo investigados por cargos de corrupción, pero hasta el momento, en la **Fiscalía Genera**l, no existe ninguna investigación abierta formalmente contra los integrantes del **Partido Nacional**, ni contra el actual presidente Hernández.

**David Romero, director de Radio Globo,** afirma que el movimiento de indignación sigue creciendo, no solamente para denunciar la parcialidad de la Fiscalía, sino por el alto nivel de impunidad y violencia estatal que se han generado desde el golpe de Estado contra Manuel Zelaya y que se han mantenido constantes con la complicidad de gobiernos extranjeros.

Una de las protestas más visibles, ha sido la huelga de hambre del llamado **“Movimiento de los Indignados de Honduras”**. Un grupo de un centenar de jóvenes que permanecen acampando en **Tegucigalpa**, y que temen una arremetida policial en las próximas horas. El pasado 30 de Junio, organizaciones campesinas de la región del Copan, protagonizaron el cierre temporal de una de las principales vías del departamento.

A estas protestas se suman las realizadas por los y las integrantes del **Frente Nacional de Resistencia Popular**, que el pasado 29 de Junio conmemoró el sexto aniversario del **golpe de Estado**, sumándose así a la serie de protestas que por todo el país, exigen un juicio en contra de **Juan Orlando Hernández**.

Según David Romero, es necesario que en Honduras haga presencia la **Comisión Internacional contra la Impunidad**, CICIG, que ha jugado un papel muy importante en el actual juicio contra altos funcionarios del gobierno de **Otto Pérez Molina**, por las mismas condiciones de impunidad frente a la corrupción. Otra de las peticiones del movimiento social es realizar un juicio al actual presidente Hernández.
