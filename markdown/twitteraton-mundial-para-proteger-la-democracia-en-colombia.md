Title: Avanza campaña LDC mundial para proteger la democracia en Colombia
Date: 2020-08-27 10:17
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Campaña internacional, twitteratón
Slug: twitteraton-mundial-para-proteger-la-democracia-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Campana-lets-defend-Colombia.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Campana-lets-defend-Colombia-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Twitteraton-Campana-lets-defend-Colombia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: El Espectador

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El movimiento **«[Let’s Defend Colombia: Campaña Internacional por la Defensa de las Cortes y la Democracia](https://www.facebook.com/campana.letsdefendcolombia.9)**» ha convocado a una twitteratón mundial y en todos los idiomas invitando a toda la ciudadanía colombiana y a la comunidad internacional a defender la independencia de la rama judicial y proteger la democracia de Colombia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La twitteratón se llevará a cabo el próximo jueves 27 de agosto a partir de las 8 a.m., hora Colombia. Los hashtags que se estarán utilizando son \#LetsDefendColombia \#YoApoyoALasCorte \#NadiePorEncimaDeLaLey

<!-- /wp:paragraph -->

<!-- wp:image {"id":88830,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Twitteraton-Campana-lets-defend-Colombia-724x1024.jpg){.wp-image-88830}  

<figcaption>
[Campaña Letsdefendcolombia](https://www.facebook.com/campana.letsdefendcolombia.9?__cft__%5B0%5D=AZXL7QYQtRVRTLtwobn6dH92HuUExm7k8DDL0CTCdEEAnrDbMjHlVkg1Nt8Wy2m-DKFPvT9wab-vAiirZdzEgnPMZAJoKXejpxl6UI4R7ZqltNG3G3HrijerxXcZx-cJRwM&__tn__=-UC*F)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Esta campaña se lleva a cabo por los obstáculos y los "intentos de entorpecer" la labor de la Corte Suprema por parte de varios miembros del partido Centro Democrático.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto tras la decisión tomada por la Sala Especial de Instrucción de imponer detención preventiva domiciliaria al ex-presidente y ahora ex-senador Álvaro Uribe Vélez. (Le puede interesar: [Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Principios de los que parte la Campaña LDC
------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el comunicado liberado por Let's Defend Colombia, señalan que "todas estas acciones y reacciones dejan claro que **la Corte Suprema de Justicia está siendo objeto de una campaña de desligitimación, desprestigio, desinformación e intimidación por cumplir su función constitucional de impartir justicia con apego a la ley".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, explican que la iniciativa parte de unos principios básicos como **"nadie está por encima de la ley"** de manera que, los ciudadanos, sin excepción alguna, deben cumplir las normas y responder por su incumplimiento según lo indiquen las leyes, por lo que tampoco es posible modificar la reglamentación solo porque se considere que los intereses de una persona están en riesgo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**"Los Magistrados no son politiqueros, la Corte es responsable de la Justicia"** y debe juzgar e investigar a quien esté incumpliendo las leyes; por lo que también es "una obligación dejar actuar a la Corte con independencia".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
