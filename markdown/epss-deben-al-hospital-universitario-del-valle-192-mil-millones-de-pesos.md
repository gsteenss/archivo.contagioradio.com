Title: EPS´s deben al Hospital Universitario del Valle 192 mil millones de pesos
Date: 2015-09-06 00:48
Author: AdminContagio
Category: DDHH, Movilización, Nacional, Resistencias
Tags: Chocó, Crisis de la salud en Colombia, Guajira, Hospital Universitario del Valle, Ministerio de Salud, nariño, Tumaco
Slug: epss-deben-al-hospital-universitario-del-valle-192-mil-millones-de-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/h-u-v1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [[Foto: revistamirelea.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_7923530_2_1.html?data=mJ6flZqXdI6ZmKiak5qJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Pd1M7gjc3Tt9Hd1cbZjbrSrdfZ09jW1sbWrdCfxcrZjbvFsM3ZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [José Joaquín Jamauca, Hospiclínicas] 

En alerta amarilla se encuentra el Hospital Universitario del Valle, que actualmente vive una grave crisis financiera debido a que, por un lado, las E**PS´s le deben 192 mil millones de pesos, y por otro, el hospital debe 35 mil millones de pesos a empresas publico-privadas,** que están cobrando hasta un 300% más de lo que deben cobrar por insumos hospitalarios.

José Joaquín Jamauca, coordinador de Derechos Humanos del Centro Hospiclínicas en el Valle, afirmó que se trata de seis empresas que están cobrando precios exagerados por los insumos necesarios para prestar los servicios de salud, lo que ha generado que el hospital en este momento se encuentre s**in insumos de sangre y con médicos especialistas a los que los últimos cinco salarios.**

Según Jamauca, la crisis se debe a las alianzas público-privadas que llevó a cabo el director  del hospital **Jaime Rubiano**, quien actualmente se encuentra suspendido por investigaciones de la Contraloría, debido a los sobrecostos de alguons medicamentos y mal manejo administrativo.

Por cuenta de esta situación, **se ha dejado de atender a niños que padecen de cáncer,** pues no se cuenta con los elementos necesarios para atenderlos, así como ha sucedido con personas que llegan desde Tumaco, Chocó, Nariño e incluso la Guajira.

“Lo que les importa es ganar y tener toda la plata del mundo como todas las empresas público-privadas, les importa la plata no les importa la vida”, expresa el coordinador de DDHH, quien denuncia que pese a que el Ministerio de Salud había anunciado recursos para atender la crisis del Hospital Universtario del Valle, lo cierto, es que aún no han llegado las ayudas, por lo que **se anuncia una jornada de paro de aproximadamente 600 funcionarios del hospital, desde el lunes a las 7 de la mañana.**
