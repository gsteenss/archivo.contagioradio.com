Title: La figura transgresora de Sor Juana Inés de la Cruz en el teatro
Date: 2017-06-15 16:22
Category: eventos
Tags: Bogotá, la Candelaria, teatro
Slug: sor-juanateatrobogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/MLP6628.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto Teatro Tierra 

###### 15 Jun 2017 

El teatro La Candelaria presenta en corta temporada "Los Ritos del Retorno", obra teatral inspirada en la vida y obra de una de las mujeres más trasgresoras del siglo XVII: Sor Juana Inés de la Cruz. Una producción que ha sido desarrollada en diferentes escenarios del continente con gran aceptación por parte del público.

La pieza teatral dirigida por Juan Carlos Moyano y producida por el grupo Teatro Tierra cuenta la vida, obra y febrilidad reveladora de la poetisa, donde el espectador podrá presenciar los principales acontecimientos que conforman la memoria de una mujer excepcional, que se confiesa consigo misma y con los fantasmas de Dios sobre la tierra.

"Los Ritos del Retorno" es un homenaje a lo femenino, al conocimiento y esplendor del verbo, de la expresión estética y las fuerzas vitales de uno de los seres más llamativos de los últimos siglos, construida a partir de  a partir de varios relatos de Eduardo Galeano, documentos eclesiásticos y literarios de la época, el ensayo "Las trampas de la fé" de Octavio Paz y los poemas y cartas escritos por la religiosa y poetisa mexicana.

<iframe src="https://www.youtube.com/embed/UnCCy7UKNvI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La obra se presentará al público en corta temporada los días 15, 16 y 17 de julio en el teatro La Candelaria, Calle 12\# 2-69 a las 7:30 p.m. con un costo de entrada de \$ 12.000 para estudiantes y \$ 24.000 para público general. (Le puede interesar: [Cortometrajes hechos por jóvenes se verán en Incinerante](https://archivo.contagioradio.com/cine-colombiano-incinerante/)).
