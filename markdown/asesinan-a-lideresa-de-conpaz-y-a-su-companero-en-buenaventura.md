Title: Asesinan a lideresa de CONPAZ y a su compañero en Buenaventura
Date: 2017-01-17 11:32
Category: DDHH, Nacional
Tags: buenaventura, comunidades, Conpaz
Slug: asesinan-a-lideresa-de-conpaz-y-a-su-companero-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/emislsen-manyoma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### 17 Ene 2017

En la mañana de este martes fue reportados los asesinatos de** Emilsen Manyoma, lideresa del Bajo Calima e integrante de la red CONPAZ, junto a us esposo**  **Joe Javier Rodallega**.

Según las primeras versiones, la pareja tomó un taxi en barrio Villa Linda de la comuna 12 de la ciudad de Buenaventura el pasado sábado y desde allí no se volvió a ver hasta el día de hoy cuando fueron encontrados sin vida en **el barrio el Progreso de esa ciudad.**

De acuerdo con la Comisión de Justicia y Paz,  los cuerpos fueron encontrados con diversos golpes, heridas con arma blanca y armas de fuego. Además, un reporte oficial indicaría que ambos fueron degollados. Joe apareció con las manos atadas.

Cabe resaltar, que en días pasado, Rodallega, había sido amenazado. Una camioneta estuvo rondando el lugar, donde vivían Emilse y Joe, quienes aportaron a las denuncias que iban en contra de los intereses empresariales y el control paramilitar en el Distrito de Buenaventura.

Emilsen igualmente acompañó el retorno del resguardo Santa Rosa de Guayacán, las apuestas de construcción de paz en el **Espacio Humanitario Puente Nayero,** lideraba las propuestas de la red CONPAZ en Buenaventura y desde mediados del 2016 estaba apoyando la documentación de asesinatos y desapariciones para la Comisión de la Verdad.

###### Reciba toda la información de Contagio Radio en [[su correo]
