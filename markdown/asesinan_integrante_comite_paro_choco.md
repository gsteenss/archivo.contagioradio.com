Title: Asesinan a Eugenio Rentería, integrante del Comité Cívico por la Salvación del Chocó
Date: 2017-07-02 14:43
Category: DDHH, Nacional
Tags: Chocó, Paro cívico
Slug: asesinan_integrante_comite_paro_choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/quibdo-manifestacion-e1499024570134.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Silla Vacía 

###### [2 Jul 2017] 

Eugenio Rentería Martínez, integrante del Comité Cívico por la Salvación del Chocó fue hallado muerto. De acuerdo a la información que se conoce, **el cuerpo del joven de 27 años fue encontrado con distintos golpes y puñaladas.**

Según las autoridades, el asesinato se habría cometido en **El Caraño, Quibdó. Rentería habría promovido los paros del Chocó del 2016 y el último de este año que ya finalizó con un acuerdo con el gobierno.** Incluso a inicios de junio, se realizó la primera sesión en la Asamblea General del Comité Cívico por la Salvación y Dignidad del Chocó, quienes habían decidido crear un grupo de transparencia y anticorrupción.

El joven era conocido como "Colombianito". Su asesinato ha prendido las alarmas del Comité quienes exigen pronta investigación y esclarecimiento sobre los hechos.

Por el momento, la primera hipótesis de **la Policía indica que [se tratría d]e una riña.** No obstante, cabe señalar que este hecho se enmarca en una serie de asesinatos contra lideres sociales y defensores de derechos humanos. [(Le puede interesar: Asesinan al presidente de SINTRAINAGRO)](https://archivo.contagioradio.com/presidente-de-sintrainagro-fue-asesinado-en-el-valle-del-cauca/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
