Title: Los desafíos del periodismo en la era del click
Date: 2020-11-18 22:13
Author: AdminContagio
Category: Actualidad, Nacional
Tags: periodismo, redes sociales, Revista Semana, Vicky Dávila
Slug: los-desafios-del-periodismo-en-la-era-del-click
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Diseno-sin-titulo-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los cambios editoriales que desde 2019 viene realizando la Revista Semana propiedad del grupo Gillinski, el nombramiento de Vicky Dávila como directora de la edición impresa de la revista y la posterior salida de al menos 16 integrantes del equipo han llevado a cuestionarse si la transición al interior de Semana simboliza la muerte del periodismo tradicional en Colombia y si marcará un antes y un después en los medios de Colombia que al igual que en el resto del mundo busca adaptarse y sobrevivir en medio de una era digital.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras la decisión de fusionar las redacciones digitales, del canal de televisión y la edición impresa y la pérdida de autonomía que esto conllevaría, fue inminente la salida de figuras como Alejandro Santos, presidente de Publicaciones Semana; y periodistas como **Rodrigo Pardo, Mauricio Sáenz, Ricardo Calderón, María Jimena Duzán, Antonio Caballero o Vladdo** entre otros, quienes no comulgaban con el control que tendría Vicky Dávila, quien llegó a la revista en 2019 y que ahora no solo decidiría sobre los contenidos digitales sino editoriales del medio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras parece que Gillisnki optó por la viralidad y el tráfico que Dávila aportó mediante su forma de hacer periodismo y que estaría más cercana a un modelo más conservador que muchos señalan buscaría emular a Fox News y CNN, los más antiguos al interior de la revista, incluidos periodistas como Daniel Coronell ya habían advertido sobre el cambio en los contenidos que venían sufriendo las publicaciones de un medio que a menudo mantuvo una posición crítica frente al Gobierno.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/revistasemana/status/1157970182721003520","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/revistasemana/status/1157970182721003520

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La enfermedad del click en el periodismo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Óscar Durán, periodista, investigador y docente de Comunicación y Periodismo de la Universidad Jorge Tadeo Lozano afirma que con el cambio en Semana estamos siendo testigos de una transformación que en lugar de dar prioridad a "un procedimiento ético de entender la función social de un medio de comunicación" ha decidido invertir en el click, el sensacionalismo, la chiva sin contrastar y los gritos" como una opción rentable; señala, deja una sensación de que "la fama y las menciones" han sido priorizadas sobre la inteligencia o la investigación de largo aliento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque para Omar Rincón, analista y catedrático la verdadera discusión debe centrarse en el modelo periodístico, hace la salvedad de que el ' clickbait' como alternativa periodística es una formula válida. que requiere "una capacidad histriónica y un saber", como los que ha adoptado Vicky Dávila, considera que este a su vez conlleva un efecto contraproducente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Afirma que el 'clickbait', adoptado por los grandes medios empresariales resulta un modelo de negocio asesorado por banqueros quienes consideran al click como la unidad mínima de negocio y que busca generar ingresos a partir de un bien intangible y simbólico como lo es el periodismo y su objetivo de narrar la sociedad; lo que lo lleva a cuestionarse si para Semana valió la pena sacrificar su prestigio ante este nuevo modelo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el vació que dejan las investigaciones y las columnas que eran el mayor atractivo de la publicación, el analista concluye que "Semana se basaba no en cuantos lectores tenía sino en cuanta incidencia tenía en la toma de decisiones del poder del Gobierno, ahora a decidido pasar a un modelo con cero incidencia política pero con mucha audiencia". [(Lea también: ‘Breaking News’ y el caso Uribe en medios tradicionales)](https://archivo.contagioradio.com/breaking-news-y-el-caso-uribe-en-medios-tradicionales/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La cultura digital de hoy es la de todo por un click, es la nueva enfermedad de las redes, nos morimos por un click, es una adicción y creo que también hay que analizarla
>
> <cite>ómar Rincón, analista</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte **María Paula Martínez, docente, politóloga y asesora de la FLIP** considera que se trata de un modelo facilista y que si bien tiene una estrategia detrás, resulta sencillo apostarle, "creo que hacer periodismo de investigación y de largo aliento e indagar en lo que está pasando no se puede contar en cuatro minutos de video", afirma en defensa de formatos más rigurosos de investigación como a los que le apuesta Ricardo Calderón, periodista de Semana durante 26 semanas y quien dio un paso al costado ante la nueva dirección que tomó la directiva de la publicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“El periodismo de investigación es lo que más necesita una sociedad porque es ir donde nadie quiere ir, preguntar lo que nadie quiere preguntar, e investigar a la gente que nadie quiere investigar por distintas razones, y eso es lo que hay que hacer"; manifestó a [EFE](https://www.youtube.com/watch?v=DngqlRfZ05Q) el periodista en medio de una entrevista en 2019.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La relación del medio con las audiencias

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Durán señala que sería un error censurar este tipo de modelo enfocado en las cifras, considera que el periodismo debe centrarse no en el éxito sino la reputación y el respeto", propuesta del psicólogo Howard Gardner.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que en realidad debe prestársele atención al rol que tendrá la audiencia, esto en particular en un escenario de polarización que vive el país y ad portas de un año preelectoral. que podría no solo definir la transformación de medios como Semana sino convertirlos en **"altoparlantes de ideas y aspiraciones políticas, en las que muchas veces la mentira y la manipulación pueden encontrar un lugar".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Rincón considera que con la llegada de las nuevas tecnologías, existió un rompimiento de la audiencia con quien era considerado un líder cognitivo intelectual por su trabajo al publica una columna, dar una conferencia o publicar un libro y quien en medio de la conversación que establecía con el público no buscaba satisfacer a las audiencias, por el contrario, buscaba provocarlas, una figura, que entró en decadencia según el analista a partir del siglo XXI.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de esta decadencia, explica, surge la figura del "influencer", una persona que a diferencia del intelectual no quiere provocar a la audiencia, por el contrario la aplaude, la quiere e incluso le pide perdón y está en función de las mismas. Es ahí donde detecta la principal diferencia en el impacto en que tiene un periodista y su intencionalidad con la audiencia, mientras el intelectual pone a pensar a la sociedad, el influencer la lleva al consumo como parte de un modelo capitalista.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La apuesta Gillinski periodística y política

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras Durán considera que la inversión hecha por Jaime Gillinski de 15.000 millones de pesos busca obtener algún redito económico, Martínez y Rincón consideran que con su decisión, lo que hizo fue "consolidar el cambio de élites de Colombia".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Rincón esta transición se evidenció en el país con la llegada al poder del expresidente Álvaro Uribe que apostó a una élite "basada en la tierra, el ganado, la religión y lo rural", un cambio que se produjo desde las altas cortes hasta el último nicho tradicional que solía ser Semana y que hoy ha expresado su apoyo al exsenador vinculado a un caso de falsos testigos y al partido de Gobierno a través de las posturas de figuras como Vicky Dávila y Salud Hernández, además de la influencia de Sandra Suárez, gerente de la revista y exministra de Medio Ambiente de la administración Uribe Vélez.[(Le puede interesar: Medios masivos "han sido incapaces de apartar su línea editorial del oficialismo" O Rincón)](https://archivo.contagioradio.com/medios-masivos-han-sido-incapaces-de-apartar-su-linea-editorial-del-oficialismo-o-rincon/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Con el triunfo de la Sergio Arboleda hay una transformación de una élite que no cree en lo valores de la modernidad o en los DD.HH", explica el analista quien expresa su preocupación ante el país que se está construyendo. "El pensamiento moderno del feminismo y los DD.HH. van a quedar en los medios independientes y en pequeños nichos pero estos medios masivos son los que se ponen una visión completa de sociedad".. [(Lea también: Tras la verdad, hay un sector que no quiere perder el poder del control ideológico)](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Duran complementa y señala que la problemática está en que si bien existen muchos medios se trata de nichos más pequeños que ante una multiplicada de voces, no tienen la capacidad de réplica que tienen figuras como el exsenador Uribe quien tiene un mayor impacto, por lo que sugiere que **"el fenómeno en términos de información política es mucho más delicado en la medida en que el parlante es mucho mayor así la noticia sea muy pequeña".** [(Le recomendamos leer: La estrategia mediática de los implicados en el caso Uribe Vélez)](https://archivo.contagioradio.com/la-estrategia-mediatica-de-los-implicados-en-el-caso-uribe/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/egonayerbe/status/1327069280031170562","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/egonayerbe/status/1327069280031170562

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Alternativas que consoliden el ejercicio del buen periodismo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la salida de Semana de plumas como Daniel Coronell y la posterior creación del portal Los Danieles junto al también columnista Daniel Samper Ospina, se ha replicado la fundación de nuevos medios que buscan la independencia o enfoque que no existió para sus creadores en el pasado, los recientes lanzamientos de portales como Vorágine y Papel Periódico son muestra de esta tendencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante este escenario Rincón destaca que si bien es bueno crear nuevos medios, la sumatoria de todos los medios independientes no alcanza para formar ese discurso colectivo del que tienen el control en la actualidad los grandes medios, pues cada uno de los alternativos tiene "su iglesia en particular, un problema que no contribuye a la narración de un país como sociedad y que se ve más afectado aún por la negativa de algunos periodistas a establecer redes, "los medios independientes de los periodistas buenos son tan buenos que no se juntan con otros, hay una especie de superioridad moral que lleva a que el hacer buen periodismo se vuelva algo de élite".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como solución, ante la sostenibilidad de los medios y su dependencia de sus financiadores quienes podrían definir la agenda del medio, señala que se deben buscar modelos de negocios que no dependan solo de la filantropía sino que permitan que la reportería sea financiada con el único fin de contar las historias donde más se necesita.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Martínez destaca además, que no basta con evaluar los medios de las grandes ciudades, también se deben tener en cuenta la conectividad, y la relevancia que existe del trabajo comunitario y el impacto de medios como la radio en los territorios, "**no se trata no solo de los medios urbanos sino de lo que siguen haciendo proyectos que emergen en los territorios resistiendo a la censura y el silencio"**, afirma la politóloga quien recuerda que si bien no son masivos es necesario ver el papel que juegan en sus entornos y que no dependen de discursos como el de Semana porque no hace parte de su realidad. [(Lea también: Celebremos la radio: Mecanismo de resistencia en las comunidades)](https://archivo.contagioradio.com/en-el-dia-de-la-radio-unesco-destaca-la-importancia-del-pluralismo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de la creación de redes y ventanas que permitan fortalecer la forma de compartir contenidos y trabajos periodísticos, la politóloga expresa que es necesario empezar a cultivar audiencias que sustenten a los medios a los que les creen y les legitimen a partir de un aporte financiero. Los académicos concluyen que en medio de esta lucha permanente en la búsqueda de recursos la única apuesta verdadera sea la de hacer buen periodismo

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
