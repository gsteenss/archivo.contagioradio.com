Title: Santos se compromete con las mujeres a defender enfoque de género
Date: 2016-10-25 18:04
Category: Mujer, Nacional, Uncategorized
Tags: acuerdos de paz, genero, Juan Manuel Santos, mujeres, paz
Slug: santos-se-compromete-con-las-mujeres-a-defender-enfoque-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/reunion-mujeres-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia de Colombia 

###### 25 Oct de 2016 

El **enfoque de género** incluido en los acuerdos de La Habana, ha suscitado diversas posiciones  en todo el país y ha sido el tema del cual diversos sectores se han "abanderado", para decir que no están de acuerdo con la paz que se está pactando.

Por ello, **mujeres de la sociedad civil y de diversas plataformas que hacen parte de la comisión de género de Paz a la Calle,** han estado trabajando en este tema y recientemente **se han reunido con el Presidente Juan Manuel Santos, para manifestarle sus posturas frente a lo que ha acontecido con el tema en el país. **Le puede interesar: [Las mujeres: gestoras en las políticas e implementación de la paz](https://archivo.contagioradio.com/las-mujeres-gestoras-en-las-politicas-e-implementacion-de-la-paz/)

**Giga Sandoval,** integrante de dicha comisión aseguró para Contagio Radio que la reunión con el presidente Santos se enfocó en realizar un llamado para que se evite permitir que, diversos sectores sigan tomando el tema de género en los acuerdos de paz como “caballito de batalla”.

Por otro lado, según Sandoval, se hizo un especial énfasis al **apoyo que tiene el tema de género en los acuerdos de paz por parte de las mujeres**, además aseguró que han exigido a Juan Manuel Santos “**no se toque el texto del enfoque diferencial y de género** y que se respeten las garantías constitucionales nacional e internacionales, que dicen que Colombia es un estado laico”. Leer acerca de este tema en: [Enfoque de género no se negocia](https://archivo.contagioradio.com/enfoque-de-genero-no-se-negocia/)

Se asegura, que este encuentro, que no fue muy largo, tuvo un primer momento en el que presidente y las mujeres se escucharon “**tuvo la oportunidad (el presidente) de escuchar los discursos de víctimas que llevan tatuada en su piel lo que ha sido la guerra”** manifestó Giga.

Posteriormente, asegura Giga, **el primer mandatario les manifestó a las mujeres presentes en dicha reunión, que va a defender el enfoque de género** y que va a respetar la perspectiva del conocimiento histórico de los derechos de las mujeres y las disidencias sexuales. Además, le **pidió a las organizaciones de mujeres y al movimiento social “seguir en las calles defendiendo y exigiendo la implementación del acuerdo integral ya”. **Le puede interesar: [Incorporación del enfoque de género: “un hecho histórico que ningún proceso de paz ha logrado”](https://archivo.contagioradio.com/incorporacion-del-enfoque-de-genero-un-hecho-historico-que-ningun-proceso-de-paz-ha-logrado/)

Giga Sandoval aseguró que las reuniones en la calle continuarán para discutir en espacios públicos los puntos a trabajar en la época que sigue “continuaremos haciendo pedagogía de paz a la calle” aseveró.

Pese a que el tiempo pasa y aún no se sabe con certeza que la firma del acuerdo final sea prontamente, **todas las plataformas y organizaciones han manifestado que continuarán en las calles** “si bien diciembre es un bajón en la movilización por las fiestas navideñas y los viajes, podemos seguir exigiendo paz…pensamos por ejemplo novenas a la calle, la idea es reflexionar y no dejar de lado el tema” aseveró Giga.

Por último, Giga aseguró que en dicha reunión sostenida con las mujeres, el presidente **Juan  Manuel Santos "se comprometió a resolver esta situación de aquí a diciembre”** y concluyó “**hemos escuchado que en unas dos semanas podría despejarse esta traba al tema del acuerdo de paz”.**

<iframe id="audio_13475366" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13475366_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
