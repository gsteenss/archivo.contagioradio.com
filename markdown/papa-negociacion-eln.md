Title: Organizaciones sociales piden al Papa que intervenga en negociación con ELN
Date: 2019-01-28 18:23
Author: AdminContagio
Category: Movilización, Paz
Tags: ELN, Iglesia, Organizaciones sociales, Papa, paz
Slug: papa-negociacion-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-28-a-las-5.58.53-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AlfonsoBECERRA] 

###### [28 Ene 2019] 

El primer llamado al Papa lo emitieron el viernes 25 de enero más de 85 comunidades de diferentes regiones del país que han padecido el conflicto, y a ellas se han sumado 20 congresistas de la bancada de oposición; quienes han pedido al sumo Pontífice que nuevamente interceda por la paz de Colombia. (Le puede interesar: ["Más de 300 organizaciones y personalidades piden al P. Francisco interceder por la paz en Colombia"](https://archivo.contagioradio.com/organizaciones-piden-papa-interceder-paz/))

Las organizaciones recuerdan el papel clave que jugó Francisco para que el Acuerdo de Paz de La Habana gozará de mayor aceptación por parte de la ciudadanía, adicionalmente, entienden la necesidad de profundizar el alcance de la paz mediante nuevos acuerdos que permitan superar la violencia física que se percibe en Colombia, para poder resolver las demás violencias que la sostienen.

En el [pedido de las comunidades](https://www.justiciaypazcolombia.com/solicitud-de-intervencion-dialogica-con-el-presidente-duque-carta-de-comunidades-organizaciones-y-personas-en-fe-al-papa-francisco/) se lee su temor a regresar a momentos de guerra, así como la esperanza de que el Papa logre entablar un diálogo con el presidente Duque para seguir en el camino del diálogo, que "puede ser más extenuante, pero honra la vida, más que la fuerza que destruye, que mata, que hiere y profundiza huellas dañinas en el alma". En torno a esta esperanza se juntan comunidades y diferentes iglesias que claman por la paz.

Por su parte, congresistas de la Bancada de la Oposición recordaron las palabras del santo padre cuando afirmó que, “Favorecer el diálogo -cualquier diálogo- es una responsabilidad de la política”, razón por la que le pidieron interceder con sus "buenos oficios" ante el Gobierno, con el fin de que se continúe el diálogo y se abandone toda idea de derrota militar o violenta con el ELN.

El sumo pontífice respondió a la situación en Colombia el pasado domingo al cerrar su visita a Panamá, cuando recordó a los cadetes de policía que perdieron la vida en el atentado de la Escuela General Santander, y los llamó "testimonios de voluntad de paz", y al tiempo que rezó por los Cadetes, pidió por la paz del país. (Le puede interesar: ["El pedido de perdón de la iglesia después de la visita del P. Francisco"](https://archivo.contagioradio.com/el-pedido-de-perdon-de-la-iglesia-despues-de-la-vista-del-papa-francisco/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
