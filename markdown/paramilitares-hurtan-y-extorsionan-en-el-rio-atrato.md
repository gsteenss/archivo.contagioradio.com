Title: Paramilitares hurtan y extorsionan en el Río Atrato
Date: 2015-09-25 11:33
Category: DDHH, Nacional
Tags: afrocolombianos, autodefensas, Autodefensas Gaitanistas, Batallón fluvial Atrato, Bocas del Atrato, cacarica, Chocó, Embarcaciones hurtadas en Bajo Atrato, La Honda, neoparamilitares, Neoparamilitarismo, Paramilitares en Bajo Atrato, Paramilitares en Chocó, Presencia paramilitar Bajo Atrato, Radio de derechos humanos, Truandó, Turbo, Yarumal
Slug: paramilitares-hurtan-y-extorsionan-en-el-rio-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Reten-Atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.ipsnoticias.net ] 

###### [25 Sept 2015] 

[Según la denuncia publicada por la Comisión Intereclesial de Justicia y Paz, el pasado jueves **3 embarcaciones** en las que se movilizaban pobladores afrocolombianos con alimentos y recursos de los programas de asistencia del Gobierno, fueron **hurtadas por neoparamilitares** en el punto conocido como Yarumal, **bajo Atrato chocoano**.]

[Los pobladores aseguran que en medio de su retención los **paramilitares** los presionaron para que asistieran a las reuniones que han convocado en Truandó, insistiendo en que **no están dispuestos a irse de estos territorios** y que están a la espera de resultados de La Habana.]

[Luego del hurto los paramilitares dejaron seguir las embarcaciones que se encontraron con **efectivos regulares del batallón fluvial** en la cabecera municipal de Río Sucio, quienes solicitaron documentos a los afrocolombianos y les **permitieron seguir su camino luego de que ellos lograron comunicarse con el Gobierno en Bogotá**.]

[El **control neoparamilitar Gaitanista**, cómo se autoidentifica, se ha extendido **dese hace tres semanas** por el río Atrato en puntos conocidos como Bocas del Atrato, León, Sautatá, Puente América, Yarumal y La Larga, Tumaradó, anunciando que se encuentra en el bajo Atrato para construir el desarrollo y el progreso, **con el apoyo de policías y militares**.]

[Pese a las denuncias adelantadas por líderes comunitarios y organizaciones de derechos humanos los **paramilitares** están asentados permanentemente en Truandó, se han movilizado por La Honda y Travesía en Cacarica, sobre Tumaradó y León, Bocas del Atrato, donde desde el pasado 14 de septiembre **cobran** un **"impuesto" extorsivo a los pobladores de Cacarica que transporten madera hacia Turbo**.]

[Hasta el momento, de acuerdo con las fuentes se sabe que los paramilitares se retiraron de Yarumal.]
