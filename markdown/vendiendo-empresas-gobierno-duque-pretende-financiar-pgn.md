Title: Vendiendo 8 empresas, gobierno Duque pretende financiar el PGN
Date: 2019-09-12 17:29
Author: CtgAdm
Category: Economía, Política
Tags: educacion, empresas, ISAGEN, Presupuesto General de la Nación
Slug: vendiendo-empresas-gobierno-duque-pretende-financiar-pgn
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Fotos-editadas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Las comisiones económicas conjuntas del Congreso aprobaron el Presupuesto General de la Nación (PGN) para el próximo año, establecido en 271,1 billones de pesos. Pese a su aprobación por mayorías, la bancada de oposición ha denunciado una serie de "maquillajes" que se han incluído y se está preparando para presentar ponencia negativa ante el Proyecto que viabiliza el Presupuesto. (Le puede interesar: ["Gobierno quiere hacerle mico a consultas populares con el presupuesto general de la Nación"](https://archivo.contagioradio.com/gobierno-quiere-hacerle-mico-a-las-consultas-populares-con-el-presupuesto-general-de-la-nacion/))

### **PGN: Una discusión de espalda al país** 

En principio, el senador Wilson Arias señaló que en el próximo PGN se ve que el país "está pagando las consecuencias del neoliberalismo", con una balanza comercial que es negativa y depende, casi exclusivamente, de la exportación de materias primas. En ese sentido, recalcó que en el pasado el incremento de precios de los 'commodities' (como el petróleo) permitió palmear la situación, pero ahora, con los precios a la baja, la verdadera situación de la economía colombiana se ve con claridad.

Algo relevante de esta situación, según Arias, es que la discusión sobre este tema en relación al PGN se está dando de forma privada. "Ahora han comprado edificaciones completas al lado del Ministerio de Hacienda donde arruman a los congresistas para que se discutan estos temas", aseguró el Senador, que ante la situación, ha decidido junto a la oposición no acudir a estos lugares, esperando que se convoque a las sesiones plenarias y de comisión en el Congreso.

### **¿El presupuesto más grande para educación?** 

Después del segundo mandato de Santos, cada año se ha hecho popular la idea de que educación es el sector que más rubro recibe, una afirmación que el Senador desmintió. Según él, el primer puesto en el Presupuesto se lo lleva el pago de la deuda, con más de 50 billones de pesos, luego sigue educación y muy cerca, defensa. Además, Arias destacó que vale la pena estudiar el rubro para educación, porque el aumento de inversión en este sector no sería el esperado.

Según su explicación, el actual Gobierno decidió unir el presupuesto que se destinaba para "Capacitación técnica no profesional", es decir para el SENA, junto al presupuesto de Educación. En total, serían unos 3,6 billones de pesos que antes aparecían en rubros diferentes, pero que ahora se juntan para que Educación siga teniendo más recursos que defensa, sin que el Gobierno tenga que hacer un esfuerzo fiscal. (Le puede interesar: ["El presupuesto para el 2019 de Colombia es el de un país en guerra"](https://archivo.contagioradio.com/presupuesto-pais-guerra/))

### **Maquillajes y grises en el Presupuesto General de la Nación** 

Arias declaró que en el Presupuesto apareció un rubro por disposición de activos que asciende a los 8,5 billones de pesos, es decir, una fuente de financiación del Estado, "al parecer, la disposición de activos correspondería a ventas al público, osea, una venta al propio Estado". Según explicó, con esta disposición, el gobierno Duque está intentando comprar a través del Fondo Nacional para el Desarrollo de la Infraestructura (FONDES), algunas empresas y presentarlas como si fueran recursos nuevos.

Es decir, el Estado se está comprando a sí mismo, con una característica más: El FONDES se constituyó gracias a los recursos de la venta de ISAGEN, otra empresa que estaba en manos del Estado. En cuanto a los 'grises' del presupuesto, Arias dijo que este Gobierno "ni siquiera nos ha confesado lo que quiere vender: nos ha pasado un listado de 150 empresas, de las cuales, al parecer, las que son más susceptible de vender son las del sector energético".

### **"Esperamos una gran movilización social que defienda el patrimonio público"** 

El siguiente paso en materia legislativa para la aprobación del presupuesto se dará durante los próximos días: se espera que se presenten las ponencias sobre el Proyecto, se discuta en los próximos 10 días y luego se vaya a votación en plenarias. Antes que eso, la oposición buscará presentar una ponencia negativa, y "esperamos una gran movilización social que defienda el patrimonio público". (Le puede interesar: ["Presupuesto 2018 es un «conejo gigante» a los sectores sociales: Libardo Sarmiento"](https://archivo.contagioradio.com/presupuesto_2018_partidos_politicos/))

<iframe id="audio_41570786" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41570786_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
