Title: Vicepresidente Naranjo le incumplió a las víctimas de la masacre en El Tandil, Tumaco
Date: 2018-04-18 11:54
Category: DDHH, Política
Tags: masacre en Tumaco, nariño, Oscar Naranjo, Registro Único de Víctimas, Tumaco
Slug: masacre_tumaco_impunidad_naranjo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asociación Minga] 

###### [18 Abr 2018] 

En total incertidumbre e impunidad continúa la masacre de los campesinos e indígenas que fueron asesinados el pasado 5 de octubre de 2017, en la vereda El Tandil, zona rural de Tumaco, Nariño. **Han pasado 6 meses desde que sucedieron los hechos, y aún no se ha llevado a cabo la audiencia de imputación de cargos contra los integrantes de la Policía Antinarcóticos** que habrían disparado contra la población civil que en la mañana de ese jueves decidieron hacer un cordón humanitario para impedir la erradicación forzada.

"Es un interrogante diario. Hemos intentado interlocutar con la Fiscalía para que haya agilidad para la citación de la audiencia de imputación de cargos pero no pasa nada", señala Diana Montilla, Vocera de la  Asociación de Juntas de Acción comunal de los ríos Nulpe y Mataje, ASOMINUMA. De acuerdo con ella, en un inicio se les había comunicado que se llevaría a cabo dicha audiencia, y **aunque hubo fecha, en la primera ocasión la cita  fue aplazada, y en la segunda ocasión fue cancelada.** (Le puede interesar: [Los testimonios que comprometen a la policía con masacre en Tumaco)](https://archivo.contagioradio.com/los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco/)

### **Naranjo le incumplió a las víctimas** 

Ninguno de los familiares de las víctimas mortales, ni los heridos ha tenido algún tipo de acompañamiento de la institucionalidad. **"Vemos con tristeza que todas las promesas que realizó el vicepresidente en su momento, fueron compromisos que se han incumplido**". De hechos,aseguran que a las víctimas no les han otorgado algún tipo de reparación, y tampoco han recibido atención psicosocial. (Le puede interesar: [Aplazan audiencia de imputación de cargos contra policías relacionados con masacre de Tumaco)](https://archivo.contagioradio.com/tumaco_masacre_policia_campesinos_asesinados/)

Según la vocera de ASOMINUMA, hasta hoy ni quiera se ha realizado la declaración para que las personas afectadas por esta situación sean **declaradas como víctimas en el Registro Único de Víctimas.** Incluso, la **Personería** no ha ido al lugar de los hechos a tomar las declaraciones de la comunidad y los testigos del hecho, pero además, la **Unidad de Víctimas** ni si quiera hizo acompañamiento de emergencia ante la ocurrencia de los hechos.

Montilla asegura que la Fiscalía cuenta con un acervo probatorio amplio y tiene todas las evidencias para poder realizar la imputación de cargos, sin embargo, para ellos resulta muy inquietante que hasta no haya respuesta alguna. Ante tal situación, la comunidad y las víctimas hacen el llamado a las autoridades para que este no sea una caso más en Colombia en el que las fuerzas militares violan derechos humanos, y se queda en total impunidad. (Le puede interesar: [Tumaco llora una tragedia en tiempos de paz)](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/)

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
