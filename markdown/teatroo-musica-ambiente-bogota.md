Title: Tarde de cultura por el ambiente en Bogotá
Date: 2017-01-27 16:15
Category: Cultura, eventos
Tags: Bogotá, Burning caravan, Cultura, teatro
Slug: teatroo-musica-ambiente-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/momo5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Congregación Teatro 

###### 27 Ene 2017 

Este sábado 28 de enero, Bogotá vivirá una tarde llena de teatro y música en un espacio cultural, familiar y de esparcimiento, que busca llevar a la reflexión sobre un tema de gran importancia para la sociedad: la crisis medioambiental que vive el mundo.

La jornada empieza con la presentación de **'Buscando el Carnaval'** de Producciones Moriath, una comparsa de 12 marionetas gigantes de animales, narraciones orales, danza, música, y circo el público podrá conocer de una forma distinta, creativa y dramatizada temas como recursos no renovables, naturaleza y biodiversidad, acompañados de historias donde niños y adultos tendrán que completar la historia mientras interactúan con los místicos animales que allí participan.

Luego de la comparsa y los relatos, los payasos de la **Congregación Teatro** invitarán a los espectadores a disfrutar de **Momo**, espectáculo musical familiar y teatral, basado en la novela fantástica de Michael Ende que cuenta la historia de una niña de 120 años que debe salvar a su pueblo de un ejército de hombres grises, los cuales se alimentan del tiempo, convirtiendo a los hombres en seres sin luz.

El evento finalizara con la presentación de la banda de rock gitano **Burning Caravan**, quienes ofrecerán un espectáculo teatral de sonidos intensos y vertiginosos para celebrar la vida de una manera distinta, tal como se refleja en su más reciente producción discográfica 'Las historias de los hombres'.

<iframe src="https://www.youtube.com/embed/CedZ-kX0S8U" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La cita es el día sábado 28 de febrero a las **3: 00 p.m.** en la **Carrera 34B \# 36-04 sur Teatro Villa Mayor**, la entrada es totalmente gratis hasta completar el aforo.
