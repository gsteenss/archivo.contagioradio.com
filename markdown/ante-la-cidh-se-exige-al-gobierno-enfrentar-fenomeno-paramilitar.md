Title: Organizaciones demostraron la vigencia del paramilitarismo ante la CIDH
Date: 2016-04-05 12:12
Category: DDHH, Nacional
Tags: CIDH, Danilo Rueda, Periodo 157 sesiones CIDH
Slug: ante-la-cidh-se-exige-al-gobierno-enfrentar-fenomeno-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/CIDH-ORGANIZACIONES-DERECHOS-HUMANOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH ] 

###### [5 Abril 2016 ] 

Organizaciones defensoras de derechos humanos, denunciaron ante la CIDH la **vigencia del paramilitarismo en Colombia, con la complicidad, anuencia y tolerancia de estructuras institucionales**, incluidas la fuerza pública y los poderes políticos y económicos regionales. Situación que evidencia una nueva fase de consolidación paramilitar, y que se sustenta en el control territorial de estas estructuras en regiones altamente militarizadas, así como en el incremento de la violencia paramilitar contra líderes sociales, el asesinato de 13 de ellos y las agresiones contra otros 54 en lo corrido de 2016.

Danilo Rueda, abogado integrante de la Comisión de Justicia y Paz, citó como ejemplos las acciones violentas que se dieron en el marco del reciente [[paro armado](https://archivo.contagioradio.com/comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas/)] decretado por las Autodefensas Gaitanistas; así como el control paramilitar de 82 barrios de [[Buenaventura](https://archivo.contagioradio.com/alertan-aumento-de-la-desaparicion-forzada-y-paramilitarismo-en-buenaventura/)], pese a la constante militarización del municipio; la presencia de más de 300 paramilitares armados en el [[Bajo Atrato](https://archivo.contagioradio.com/?s=Bajo+Atrato)], quienes se movilizaron pasando por retenes militares; y la libre movilidad de por lo menos 200 hombres armados y vestidos de camuflado en [[Putumayo](https://archivo.contagioradio.com/?s=putumayo)], a escasos metros de una base militar financiada por los Estados Unidos.

Estas **acciones violentas son parte de una estrategia de terror y zozobra para atemorizar a las comunidades**, según denunció Franklin Castañeda, quien precisó que [[durante 2015](https://archivo.contagioradio.com/577-agresiones-contra-defensores-de-ddhh-se-han-registrado-en-2015/)] 682 defensores de derechos humanos fueron agredidos, y de ellos 63 asesinados. De acuerdo con investigaciones, la responsabilidad de grupos paramilitares en esos delitos fue del 66%, y 7% de agentes del Estado, frente a 0.5% de grupos guerrilleros.

De acuerdo con la Defensoría del Pueblo, en 27 de los 32 departamentos de Colombia hay presencia de grupos paramilitares, y en Cauca, Santander, Putumayo, Antioquia, Arauca, Atlántico, Chocó, Córdoba, Cundinamarca, Meta, Norte de Santander, Sucre, Sur de Bolívar y Buenaventura, se han presentado las acciones de violencia paramilitar durante estos primeros meses de 2016, afirmó Castañeda, e insistió en que estas regiones **coinciden con los departamentos y municipios claves para la implementación de los acuerdos de paz**.

Según se afirmó por parte de la Corporación Jurídica Libertad, hay serias falencias en la judicialización de miembros paramilitares, pues **de los 3.500 que se suscribieron a la Ley 975, únicamente hay 15 sentencias en firme.** Así mismo, se denunció la continuación entre el accionar de las denominadas Bacrim y el de los grupos paramilitares que pese a que se agogieron a la desmovilización, continúan atentando contra los derechos humanos de quienes reclaman sus tierras, con la omisión y en articulación con agentes de la fuerza pública y miembros de la institucionalidad; fenómeno que debe abordarse para garantizar la no repetición en el marco de los acuerdos con las FARC y el  ELN.

Otros de los elementos que fueron denunciados ante la CIDH tienen que ver con factores institucionales que han permitido la vigencia del paramilitarismo en Colombia. De acuerdo con los peticionarios, hay falencias en las investigaciones contra quienes han vulnerado derechos humanos. La Fiscalía ha designado 1.355 millones de dolares para los 800 investigadores de las 56 mil investigaciones que cursan contra miembros del ELN y de las FARC, mientras que los procesos contra agentes estatales, políticos y económicos, infractores de los derechos humanos, **no han contado con igual distribución de recursos humanos, económicos y logísticos**.

Frente a los hechos denunciados y las pruebas presentadas, las organizaciones insistieron en la necesidad de consolidar una **política coherente para enfrentar el fenómeno del paramilitarismo**, que permita reconocer su naturaleza y estrategia, y que lleve a que los altos mandos en la planificación y ejecución de los delitos y sus beneficiarios, sean sancionados. En esa medida, destacaron la importancia de crear una comisión de alto nivel que logre identificar con claridad este fenómeno y posibilite espacios de interlocución entre el Gobierno, las dos mesas de negociación y la sociedad civil, para **definir estrategias colectivas que permitan generar garantías de no repetición**.

En tal sentido, se insistió en que parte de los propósitos de la negociación con las guerrillas es el fortalecimiento de la democracia en Colombia, lo que requiere que haya un desmonte del paramilitarismo; que se construya una **doctrina militar para la paz y los derechos humanos**; que se establezcan garantías para la protesta social y un protocolo nacional para la actuación de la fuerza pública; que se fortalezca la **separación entre las fuerzas militares y la Policía Nacional**, y que se reforme la administración de justicia para investigar, judicializar y sancionar violaciones a derechos humanos.

Escuche toda la sesión:

https://www.youtube.com/watch?v=2MnBjG4dVI4

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
