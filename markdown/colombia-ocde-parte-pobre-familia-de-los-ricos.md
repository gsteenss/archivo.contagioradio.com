Title: "Colombia en la OCDE sería la parte pobre de la familia de los ricos": Libardo Sarmiento
Date: 2018-01-26 13:13
Category: Economía, Nacional
Tags: OCDE
Slug: colombia-ocde-parte-pobre-familia-de-los-ricos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/pensionados-e1500487692958.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: colombialegalcorporation] 

###### [25 Ene 2018] 

En el marco de los anuncios acerca de la posible entrada de Colombia en la OCDE y de la realización del Foro Económico en Davos, Suiza, el analista económico Libardo Sarmiento señala que Colombia sería la parte pobre de una familia rica, dadas las condiciones económicas de la nación. Además, de los 35 criterios que requiere esa organización, al país le hace falta cumplir las tres más difíciles.

### **¿Qué hace falta para que Colombia haga parte de la OCDE?** 

El economista señala que los tres criterios tienen que ver con reformas estructurales en materia de infraestructura para el comercio. Entre esos cambios está el mejoramiento de las vías, sin embargo teniendo en cuenta  los constantes retrasos en las obras parece una meta difícil de lograr. Un ejemplo de ello, es la construcción de la vía al llano que comenzó a construirse en 1974, pero han pasado 43 años sin que se haya terminado. Otro ejemplo, es la tragedia acontecida en el Puente de Chirajara.

Otra de los factores tiene que ver con las condiciones de empleo. La OCDE exige una tasa baja de desempleo y cierto nivel de formalización, y Colombia no cumple con esas exigencias, dados los bajos niveles salariales y la amplia franja de trabajo informal que ronda el 60%.

Por último es necesario que el país tenga altos niveles de aplicación de ciencia y tecnología en los proyectos de desarrollo, y sobre ese tema Colombia también está en deuda.  Los gobierno se vienen rajando en inversión de recursos a ciencia y tecnología por habitante. De acuerdo con esa misma institución, en 2013 el presupuesto de Colciencias fue de 430 mil millones de pesos, en 2014 éste fue de 376 mil millones, en 2015 el presupuesto fue de 354 mil millones y en 2016 fue reducido a 306 mil millones, es decir que esa inversión el gobierno la ha venido reduciendo con el paso de los años. (Lea también: Colombia reprueba examen de **¿Qué le pasará al bolsillo de los Colombianos si el país ingresa a la OCDE?)**

Aunque ingresar a la OCDE traería consigo algunas ventajas, como la implementación de un sistema contable acorde a los estándares internacionales y unas obligaciones del Estado para el mejoramiento de algunas condiciones económicas, según el análisis de Sarmiento, son más los impactos negativos sobre el sector productivo al que pertenecen la gran mayoría de los trabajadores.

### **Carga tributaria y reforma pensional** 

Para el país ingresar a la OCDE implica que habrá una mayor exigencia tributaria. Señala Sarmiento que a pesar de las seis reformas que ya ha implementado el gobierno Santos, sería necesario tramitar otra en la que seguramente se ampliaría la base gravable a otros productos e incrementaría el porcentaje del IVA que ya subió al 19%. Estos aumentos se equipararían a los de otros países en los que la carga podría recaer hasta en un 60% de cobro de impuestos, sin embargo el nivel salarial es mucho mayor y hay sistemas de educación y salud que garantizan una devolución del impuesto.

Por otra parte debería darse una reforma pensional para garantizar la sostenibilidad de los fondos de pensiones. Actualmente, en Colombia los fondos aseguran que en el mediano plazo serían insostenibles dado el bajo nivel de aportes, además de las altas pensiones de un número reducido de personas siguen desangrando el sistema. [Lea también: Proponen igualar edad de pensión de hombres y mujeres](https://archivo.contagioradio.com/ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres/)

En esa línea, Sarmiento asegura que la reforma pensional no estaría orientada a reducir las llamadas “pensiones millonarias” sino que apostaría por un aumento en la edad, en la cotización y una reducción de la carga pensional actual. Adicionalmente otra de las exigencias sería la privatización total del sistema, es decir, la liquidación de COLPENSIONES, que actualmente asume la carga de la gran mayoría de las personas pensionadas del sector público en Colombia.

Todas estas situaciones, de acuerdo con la conclusión del analista, hacen que sea peor el remedio que la enfermedad. Es decir que la situación económica de la gran mayoría de las familias colombianas no verían los beneficios del ingreso de Colombia al Club de los Ricos.

<iframe id="audio_23383649" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23383649_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
