Title: ELN propone misión de verificación para cultivos de uso ilícito
Date: 2020-10-11 12:06
Author: AdminContagio
Category: Actualidad, DDHH
Tags: ELN, Gobierno Duque, política antidrogas
Slug: eln-propone-mision-de-verificacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ELN

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una carta dirigida al **Departamento de Estado, la Fiscalía Federal de EE.UU. y al Gobierno nacional**, el ELN propuso la creación de una comisión de verificación que detalle la vinculación de la guerrilla con actividades de narcotráfico, a propósito de la política antinarcóticos que ha dado prioridad a la erradicación forzada y la fumigación. A su vez reiteraron la necesidad de establecer un cese al fuego bilateral y temporal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La carta se refiere a las fumigaciones con glifosato y la erradicación forzada, afirmando que **"son salidas carentes de sentido común"** y que la lucha contra las drogas únicamente puede darse con iniciativas políticas, económicas, sociales y culturales, en lugar de medidas policiales que además desconocen "la realidad de los campesinos y sus comunidades".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La carta agrega que no están vinculados "con ninguna fase de su cadena: no tenemos cultivos, laboratorios, cocinas, pistas, rutas ni exportaciones de cocaína", explicando que su forma de financiación proviene del "cobro de impuestos a los compradores, porque son los que se lucran del negocio" en zonas donde hace presencia. En ese sentido proponen realizar un debate nacional e internacional, para analizar las actividades del grupo ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha propuesta incluye conformar una comisión internacional para verificar si el ELN tiene cultivos, laboratorios, infraestructuras o rutas para el narcotráfico y **que incluya una invitación al Consejo de Seguridad de la ONU y a un emisario del Secretario General a participar del debate y la Comisión de verificación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A su vez reiteran su ánimo de pactar un cese el fuego bilateral y temporal con el Gobierno** como lo han exhortado el Secretario General de la ONU, el papa Francisco y el Consejo de Seguridad de la ONU, en medio de la crisis de la Covid-19. [(Lea también: ELN está preparado para negociar un cese al fuego bilateral)](https://archivo.contagioradio.com/eln-esta-preparado-para-negociar-un-cese-al-fuego-bilateral/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de la misiva señalaron la importancia de esclarecer cómo se financió la campaña del presidente Iván Duque, esto con relación a las evidencias que han surgido con el paso del tiempo y que revelarían como el «Neñe» Hernández habría sido el brazo político del narcotráfico al norte del país que habría financiado de forma ilegal la compra de votos en la segunda vuelta presidencial del 2018. [(Le puede interesar:](https://archivo.contagioradio.com/corte-suprema-conserva-competencia-para-investigar-a-uribe-por-implicacion-en-compra-de-votos/)[Marta Lucia Ramírez y otros escándalos del narcotráfico en el gobierno Duque](https://archivo.contagioradio.com/marta-lucia-ramirez-y-otros-escandalos-del-narcotrafico-en-el-gobierno-duque/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez plantea la inquietud sobre cómo se ha permitido la salida de la cocaína en el país a través de "aeropuertos, carreteras, ríos y mares" y de qué forma se beneficia la Fuerza Pública de los embarques.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
