Title: Por persecución judicial se presentaron líderes del Congreso de los Pueblos ante fiscalía
Date: 2017-06-12 13:02
Category: DDHH, Nacional
Tags: Asesinatos a líderes comunitarios, comunidades, congreso de los pueblos, Derechos Humanos
Slug: persecucion-judicial-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/20170612_124927-e1497290209975.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jun 2017] 

**“Ser líder y lideresa social no es un delito”.** Con estas palabras se presentaron hoy ante la Fiscalía General de la Nación 25 líderes y lideresas defensores de los derechos humanos, integrantes del Congreso de los Pueblos, que aseguran están siendo perseguidos por el Estado. Algunos tienen órdenes de captura y sobre otros se encuentran en vigencia procesos de investigación.

<iframe src="http://co.ivoox.com/es/player_ek_19215671_2_1.html?data=kp6fk5qae5Khhpywj5WWaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5ynca7V097Zx9OPl8bmz8aSlKiPt9DW08qY0srWt8bX1tjWh6iXaaOnz5DOjdGJh5SZoqnRx9fJt4zn0MjWw9HJt4zZjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Marylen Serna, vocera de Congreso de los Pueblos, “Hoy nos presentamos ante la Fiscalía para que nos digan de qué se nos acusa. **Estamos siendo perseguidos y necesitamos saber en qué van las investigaciones contra los paramilitares** en las regiones y las investigaciones de los asesinatos de nuestros líderes y lideresas”.

Acompañados de un equipo jurídico y diferentes organizaciones y comunidades, los líderes sociales y campesinos de Colombia insisten en que la **Fiscalía General de la Nación hace falsos montajes para judicializar a defensores de Derechos Humanos**. Ellas y ellos han mantenido una movilización constante para protestar por los abusos del Esmad en Buenaventura, la presencia paramilitar en las regiones, la defensa de los territorios y el abandono estatal. (Le puede interesar: ["en 2017 han sido asesinados 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/))

<iframe src="http://co.ivoox.com/es/player_ek_19215717_2_1.html?data=kp6fk5qbdZihhpywj5WXaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncbXZhqigh6eXqsrg0JCuxdqJh5SZo5bOh5enb9Tjw9fSjdLTstXVy8rgjc_ZqMrXysbZx9iPp9Di1dfOjdGJh5SZoqnRx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Teófilo Acuña, vocero de la Cumbre Agraria Campesina Étnica y Popular, se presentará ante la Fiscalía para **“demostrar que no le debemos nada a la justicia colombiana”.** Según Acuña, “sabemos que han hecho falsos montajes judiciales a los líderes que denunciamos persecución política y presencia de paramilitares en nuestros territorios”.

Hay que recordar que en el país se han presentado detenciones arbitrarias como la Milena Quiroz, líder social de Arenal, Bolívar. **Ella fue detenida por la fiscalía bajo el argumento de que organiza marchas y moviliza gente**. Teófilo Acuña afirmó que “a Milena se le imputan cargos por delitos que no ha cometido y se le viola su derecho constitucional a movilizarse”. (Le puede interesar: ["Más de 100 organizaciones del mundo piden aplicación de amnistía a líderes sociales"](https://archivo.contagioradio.com/amnistia-a-lideres-sociales-colombia/))

**Hay una persecución sistemática a los líderes de Colombia**

<iframe src="http://co.ivoox.com/es/player_ek_19215746_2_1.html?data=kp6fk5qbeJehhpywj5WYaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbeZpJiSo6nHuNDmjKjc1NfJpYzKhqigh6adsMbuhpewjdjTptPZjMbQ1s7YucWfxcrZjczTpsrZ09PcjcvWqc_ojoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Algunos congresistas y senadores de la República estuvieron presentes en el plantón que realizaron las comunidades y van a acompañar el proceso de presentación de los líderes y lideresas ante la Fiscalía General de la Nación. Para Víctor Correa, Represente a la Cámara por el Polo Democrático, **“hay una acción de valentía de los líderes de Congreso de los Pueblos que hoy vienen a la fiscalía a decirle que no se van a esconder** y que no le tienen miedo a este gobierno”. (Le puede interesar: ["Fiscalía no tiene argumentos para mantener en prisión a líderes sociales del sur del Bolívar"](https://archivo.contagioradio.com/fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa/))

Correa aseguró que los asesinatos de los defensores y campesinos del país son sistemáticos y “el gobierno quiere ocultar esta realidad”. El representante estableció además que hará un **proceso de mediación y acompañamiento de las comunidades que se presentan ante la Fiscalía** para pedirle a esta institución que “no conteste con falsos positivos judiciales porque estas personas están defendiendo sus derechos y no son delincuentes”.

Víctor Correa le hizo un llamado a la fiscalía para que estas personas enfrenten su proceso en libertad y afirmó que **“no se les debe dictar medidas de aseguramiento porque están demostrando que van a responder”.** Igualmente, el representante fue enfático en manifestar que “en Colombia vivimos en una condición permanente de violencia donde hay una clase dominante que quiere silenciar a aquellos que no están de acuerdo con mantener las cosas como están”.

Hacia las 3 de la tarde se conoció que todas las personas que entraron al recinto de la fiscalía salieron. Se desconoce, hasta el momento, si serían expedidas medidas de aseguramiento contra ellos y ellas.  Por el momento los equipos de abogados evalúan la situación de quienes tienen ordenes de captura y los cargos que se les imputan.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
