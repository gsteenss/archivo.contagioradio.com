Title: Generaciones:  ¿Responsabilidad sin esperanza?
Date: 2015-08-03 12:04
Category: Cesar, Opinion
Tags: César Torres Del Río, economia mundial, Max weber, Nuevas generaciones
Slug: generaciones-responsabilidad-sin-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Protesta_del_movimiento_15-M_Córdoba_2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Toni Castillo Quero] 

#### **[[César Torres Del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/)]** 

###### [3 Ago 2015]

Tiempos de austeridad económica y de eclipse de razones estratégicas vivimos tanto las generaciones de mayores como las de millones de jóvenes. A las primeras se les acaba el tiempo sin haberse podido jubilar, o habiéndolo logrado sienten con angustia que su mesada no les alcanza para las necesidades básicas. Para las segundas el desempleo y la imposibilidad práctica de pensionarse en los próximos 40-50 años son las más inmediatas certezas de sus existencias. El espectro de lo político y sus proyecciones socio-ambientales pareciese no tener colores; el “principio esperanza” (Ernst Bloch, 1959) ha sido reemplazado por el de “responsabilidad”. Hoy, para unas y otras generaciones no hay futuro.

El siglo XX comenzó con la tragedia de la primera guerra mundial. El triunfo de la revolución rusa fue su contrapeso. La esperanza se desplegó con energía. Que el gran acontecimiento deviniera en el gulag totalitario no se debió a su origen ideocrático sino a la captura del partido y del Estado por parte de Stalin y la burocracia soviética; en breve, el Termidor no estaba inscrito en el acta de fundación de 1917. La teleología retrospectiva es el ejercicio de los vencedores; la crítica a 1789 tiene como blanco político a 1917.

La destrucción del muro de Berlín en 1989 acabó con el “modelo” del totalitarismo soviético, y selló el fin del siglo XX; paralelamente se asistió al desmonte glocalizado de los focos clasistas de resistencia: partidos y sindicatos, debido parcialmente a  la finalización del fordismo - sistema de explotación laboral y mecanismo de producción en serie de mercancías -. Veinticinco años después el balance del experimento del socialismo en un solo país (con sus reediciones), de su diplomacia, de su opresión, de su política  de alianzas y de su actitud ante la guerra no ha sido abordado con la extensión necesaria y la profundidad exigida; entre tanto, Bruselas, Berlín y Washington aparecen hoy como los íconos de la libertad, la democracia y la memoria histórica.

La memoria antifascista ha cedido ante la memoria del genocidio judío, *Shoah*, y ésta se ha universalizado como paradigma. Como bandera ondea en el frontispicio de la Corte Penal Internacional; como símbolo lo encontramos, entre otros, en los sitios de Yad Vashem, en Jerusalem; Holocaust Mahnmal, en Berlin; o en Auschwitz-Birkenau, Polonia. Y rescatamos, claro, el valor de tales lugares; pero no al precio de marginar la memoria clasista.

Así, iniciado el siglo XXI de la cristiandad vemos angustiados cómo las generaciones caminan desorientadas; la esperanza ha perdido su aura y los vencidos mantienen su condición.  El “principio de responsabilidad” (Hans Jonas, 1979) es ahora el lema de la acomodación al orden existente, del “soportemos el presente” porque no hay alternativa. Frente a un futuro-pasado oscuro, de “noche polar” (Max Weber) totalitaria, de universo concentracionario, asumamos individualmente la responsabilidad de defender lo que queda del planeta pues en el escenario sólo hay víctimas y victimarios.

Sin embargo, no todo está perdido. La alternativa no es responsabilidad frente a esperanza. Es la esperanza con responsabilidad social y eco-ambiental. Miles, millones de personas tenemos un imperativo ético frente a la *barbarie civilizada* de la modernidad. La movilización democrática, colectiva y solidaria del pueblo griego en el reciente referendo del 5 de julio, cuyo resultado fue un no rotundo a las políticas de austeridad, es ejemplo de esperanza con responsabilidad. ¡Generaciones: que esto no se pierda de vista!
