Title: Decir que en La Guajira no se compran o venden votos es querer tapar el sol con un dedo
Date: 2020-03-18 21:42
Author: CtgAdm
Category: Actualidad, Política
Tags: corrupción, Ñeñe Hernández
Slug: la-guajira-compran-o-venden-votos-tapar-sol-con-un-dedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2211/10/4021032_n_vir3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: La Guajira/ Ñeñe Hérnandez

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 15 de marzo se dieron a conocer apartes de las interceptaciones hechas al 'Ñeñe' Hernandéz que involucran al senador Álvaro Uribe, a su asesora Claudia Daza y otros intervinientes en aportes para la campaña de Iván Duque, dichos audios que fueron entregados a la Corte Suprema de Justicia, al Consejo Electoral y a la Comisión de Acusación, **hacen referencia además a la suma de 1.000 millones de pesos para la campaña y a la búsqueda de ayudar de empresarios y alcaldes en sus cargos en departamentos como La Guajira o Magdalena.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En las grabaciones, un diálogo entre María Claudia Daza y el ganadero, se revela que José Guillermo Hernández se reunió con el representante a la Cámara, Edward Rodríguez del Centro Democrático para coordinar lo que sería un encuentro privado entre el candidato Duque y 15 alcaldes de Magdalena; aunque Daza le recuerda que ese encuentro sería ilegal, el 'Ñeñe' responde que la reunión se haría en su casa. [(Lea también: En Colombia no hay postconflicto, en Colombia hay un Narcoestado Paramilitar.)](https://archivo.contagioradio.com/en-colombia-hay-un-narcoestado-paramilitar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a las regiones involucradas como Magdalena y La Guajira, si se revisa la intención de voto en las elecciones presidenciales de 2018, durante la primera vuelta el ganador fue Gustavo Petro con 85.149 votos mientras Iván Duque un total de 76.137. Sin embargo en la segunda vuelta donde fueron instaladas 1,585 mesas de votación registrándose 215.913 votantes, Iván Duque ganó con 106, 328 votos mientras Gustavo Petro recibió 103.271 votos, descartando el voto anulado o el voto en blanco.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a esta variación, Alejandra Barrios, directora de la [Mision de Observación Electoral](https://twitter.com/moecolombia), asegura que desde la organización se realizó un análisis cuantitativo sobre cómo fueron las variaciones de votos en la primera y segunda vuelta en La Guajira, afirmando que los recursos en una campaña, se pueden usar para "comprar votos o para mantenerlos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

pero no es fácil afirmar si existió o no una compra de votos pues al pasar de siete candidatos de la primera vuelta, incluido el voto en blanco, a dos, no hay forma matemática o cuantitativa de hacer un seguimiento a la recomposición de las votaciones al depender de las preferencias electorales de cada ciudadano., "la única afirmación que podemos hacer es que sí hubo una modificación de resultados", agrega.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La Guajira: un departamento vulnerable a la compra de conciencias

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

José Silva, director de la ONG Nación Wayúu se refirió a cómo se viven las jornadas electorales en La Guajira, un departamento que históricamente ha sido afectado por la corrupción de sus funcionarios públicos y entidades y que ahora se ve relacionado al caso de la 'Ñeñe' Política.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es un departamento vulnerable y fácil de manipular por las condiciones en las que vivimos nosotros" afirma, haciendo énfasis en la comunidad indígena Wayúu y la vulneración que han sufrido sus derechos a la salud, al agua potable, a la alimentación y a la participación étnica y una población que explica, vive a kilómetros de los cascos urbanos y que es susceptible a que "sean compradas sus conciencias con mercados de comida y carro tanques de agua, únicamente en épocas de elecciones" . [(Le puede interesar: Tres elementos claves para garantizar unas elecciones transparentes)](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Cuando se habla de la 'Ñeñe' política, Silva afirma que en el departamento existen caciques políticos "enquistados desde hace años que deciden el destino de La Guajira" y son quienes contactan a los líderes políticos en los diferentes municipios que manejan un potencial electoral, en este caso a "las comunidades indígenas que a diario se mueren de hambre y sed y es así como los sacan en vehículos para que salgan a votar, a Uribia, Manaure, Maicao y a Riohacha", relata. [(Lea también: El ‘Ñeñe’, Sanclemente y la degradación institucional en Colombia)](https://archivo.contagioradio.com/el-nene-sanclemente-y-la-degradacion-institucional-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de un contexto donde en los últimos nueve años, el departamento ha tenido 12 gobernadores suspendidos por decisiones judiciales y vínculos con la corrupción, menciona Silva que uno de los pocos condenados por este delito es Wilmer González, exgobernador del departamento quien completa más de un año prófugo de la justicia acusado y procesado por los delitos de cohecho, corrupción, falsedad en documento y fraude procesal en medio de su campaña electoral en noviembre de 2016.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tipos de corrupción en el departamento

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto a la sociedad civil, Ángel Ortiz, líder social y político del municipio de Hatonuevo, explica que en La Guajira existen diversas formas de corromper a los votantes pasando desde la trashumancia hasta la compra de votos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el líder, la trashumancia ha sido un problema histórico en municipios como Hatonuevo donde según el censo electoral fue de 12.309 votantes y se hallaron un total 3320 cédulas inscritas de manera irregular o en Barrancas donde el censo superó los 20.000 votantes y existieron 4.600 demandas de trashumancia.  
  
A su vez, y con el interés de favorecer a determinado candidato, Órtiz se refirió a otro modo de trashumancia de cédulas en el que candidatos buscan a personas próximas a cumplir los 18 años y los trasladan de municipios del Cesar como La Jagüa de Ibirico, Curumaní o Valledupar y llevarlos hasta La Guajira, para que así en el momento en que exista una demanda por trashumancia, su cédula nunca va a caer porque fue expedida zonas como Hatonuevo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuando se trata de una cifra como 1.000 millones de pesos, valor mencionado en los audios vinculados al 'Ñeñe' Hernández, el líder explica que dichos recursos podrían destinarse a la compra de votos mediante métodos como el del 'carrusel' donde el sufragante entra con un tarjetón marcado y sale con uno sin marcar para repetir la acción o la misma compra de documentos de identidad de las personas para así debilitar a un contendor. Tampoco descarta que los recursos puedan permear instituciones y sean destinados a las personas que fungen como veedoras o incluso a funcionarios del ámbito judicial.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Si la corrupción se dan en elecciones de alcaldes y gobernadores también puede darse en el Senado, Cámara y Presidencia". - Ángel Órtiz

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de este proceso cabe recordar que el fiscal Francisco Barbosa fue recusado para que se aparte de las investigaciones por su cercanía al primer mandatario mientras la Corte Suprema de Justicia abrió una indagación preliminar contra el senador Uribe, y la Comisión de Acusación recibió dos denuncias contra el presidente Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
