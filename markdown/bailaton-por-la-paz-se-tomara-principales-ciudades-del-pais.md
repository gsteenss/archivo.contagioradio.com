Title: Bailatón por la paz se tomará principales ciudades del país
Date: 2017-01-12 11:06
Category: Movilización, Nacional
Tags: Bailatón por la Paz, Zonas Veredales
Slug: bailaton-por-la-paz-se-tomara-principales-ciudades-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Enero 2017] 

Después de la sanción que impuso la ONU y el retiro de los delegados de la comisión de verificación en El Conejo - la Guajira, ciudadanos de todo el país decidieron hacer un **bailatón por la paz en apoyo a la implementación de los acuerdos de paz de La Habana y al tránsito de los miembros de la guerrilla de las FARC-EP a la vida civil.**

De acuerdo con Miryam Ojeda, una de las ciudadanas que está apoyando este evento, “es una respuesta de la **ciudadanía que quiere invitar a la reconciliación y a la paz,** a que le demos la bienvenida al proceso de paz que está en transición.  Nos parece que **el baile es una reafirmación de nuestro deseo de fraternidad y de abrazar al otro”**

A su vez Ojeda se refirió a los diferentes comentarios negativos que surgieron por estos actos y señalo que “la reincorporación a la vida civil significa tomar una actitud de querer conversar con el otro” y añadió que **fue bastante desproporcionado el valor que se le dio a esta información**. Por el contrario Ojeda considera que hechos como este son pasos hacia la paz. Le puede interesar:["Zonas veredales bajo la sombra Paramilitar"](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

El evento se realizará el **próximo 13 de enero**, en ciudades como **Cali**, en donde el punto de encuentro será el Parque de los Estudiantes, mientras que en ciudades como **Santa Marta, Bogotá, Barranquilla y Cartagena**, el lugar se mencionará en las próximas horas. Además se espera que ciudades del mundo como **París, Nueva York, Buenos Aires**, entre otros se unan a esta iniciativa.

<iframe id="audio_16184967" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16184967_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
