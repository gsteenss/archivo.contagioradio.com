Title: Asesinato de Juan Santos es un nuevo ataque contra la libertad de expresión en México
Date: 2015-08-14 15:57
Category: DDHH, El mundo
Tags: Asesinato fotoperiodìsta Rubèn Espinosa, Asesinatos de periodistas en México, elecciones generales México, Javier Duarte, mexico, Veracruz
Slug: asesinato-de-juan-santos-es-un-nuevo-ataque-contra-la-libertad-de-expresion-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Heriberto-Santos-Contagio-Radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BBCMundo.com 

###### [14 ago 2015]

Ayer en la madrugada se presentó un ataque de un grupo armado en la ciudad de Orizaba (Veracruz), en donde murió el periodista **Juán Heriberto Santos Cabrera** quien hace unos meses era **corresponsal de Televisa y cubría las noticias sociales y policiales.**

Según la información oficial, Santos Cabrera se encontraba con dos colegas más cuando la policía ingresó al lugar persiguiendo a Márquez Balderas, jefe de los paramilitares de "Los Zetas" y durante el enfrentamiento resultó asesinado el periodista. Analistas afirman que muchas veces la **información oficial tiende a encubrir la actuación de elementos paramilitares afines al gobierno del Estado.**

En el hecho que dejó seis personas muertas y dos policías heridos. Con este asesinato ya son **15 los periodistas asesinados en el estado de Veracruz**, desde que Javier Duarte del PRI, es el gobernador.

Recientemente fue asesinado Ruben Espinosa en la capital mejicana, en un apartamento de DF. Espinosa había salido de Verecruz tras reiteradas amenazas luego de varias denuncias en contra del actuar criminal de las estructuras policiales y paramilitares al mando de Duarte.
