Title: Mirar solo una cara de la realidad, actitud poco cristiana
Date: 2019-09-27 13:25
Author: A quien corresponde
Category: Opinion
Tags: cristianismo., Cristianos, Iglesias Cristianas, realidad, Religión
Slug: mirar-solo-una-cara-de-la-realidad-actitud-poco-cristiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones.  
***

(Romanos 2,24)

Bogotá, 17 de septiembre del 2019

 

*"Me gusta tu Cristo... No me gustan tus cristianos. *

*Tus cristianos son muy diferentes a tu Cristo"*

 Mahatma Gandhi.

 

Estimado

**Hermano en la fe**

Cristianos, cristianas, personas interesadas

En las comunicaciones anteriores, recordaba las afirmaciones frecuentes que hacías para descalificar a quienes teníamos ideas y comprensiones diversas a las tuyas en cuestiones científicas, sexuales, políticas, religiosas y sociales, igualmente, a quienes apostamos por la paz (a pesar de sus limitaciones), a quienes insultabas llamándonos “corruptos”, “castrochavistas”, “cómplices del terrorismo”, “santistas”, “promotores de la homosexualidad”, etc.

(le puede interesar[: A quien corresponde: La actitud contradictoria de muchos cristianos y cristianas](https://archivo.contagioradio.com/a-quien-corresponde-la-actitud-contradictoria-de-muchos-cristianos-y-cristianas/))

Retomo esta comunicación para reconocer que a la mayoría de los colombianos y colombianas nos han acostumbrado a ver un solo lado de la realidad, nos han convencido que los otros son los malos, los equivocados y “nos han enceguecido” para negarles derechos, para verlos como enemigos, para negarnos a escuchar sus argumentos.

La inmensa mayoría de quienes apostamos por la paz tenemos razones profundas para hacerlo, nos basamos en las evidencias vividas en los territorios, en las estadísticas de militares y policías que se han salvado de morir gracias al acuerdo, en los aportes de expertos internacionales, y en el reconocimiento de los observadores imparciales, además de las convicciones religiosas, sociales e históricas, podemos dar razones de ello.

Conocemos testimonios directos de víctimas de todos los lados de la guerra. Reconocemos las limitaciones de los acuerdos, son una realidad. Pero tambien es una realidad que “es mejor un acuerdo imperfecto a una guerra perfecta”.

La  mayoría de quienes queremos la paz no somos santistas, pero reconocemos el valor de los acuerdo para la terminación del conflicto armado entre el gobierno de Colombia y la guerrilla de las FARC; acuerdo valorado positivamente por expertos mundiales en procesos de paz, por gobiernos de diversas tendencias políticas del mundo, por los máximos líderes religiosos de la mayoría de las religiones.

Esa manera de ver las cosas blanco o negro, sin matices; los de aquí y los de allá, sin las ambigüedades propias de los seres humanos, nos está impidiendo ver las fallas y equivocaciones de los nuestros, la falta de coherencia de los nuestros:

#### nuestro partido, nuestra religión, nuestra familia, nuestro país, nuestro grupo étnico, nuestro colectivo 

…. Y exigimos a los otros la coherencia y perfección que no tienen los nuestros.

 

He repasado en mi mente esas acusaciones, sus razones o sin razones, los argumentos o mejor la carencia de argumentos y veo una tendencia muy común y recurrente en la mayoría de los seres humanos: mirar solo un lado de la realidad, recurrir a una sola fuente, creerle a un solo líder o generador de opinión, no ver la otra cara de la realidad, no reconocer que hay diversas miradas y que cada persona habla y opina desde su comprensión parcial y desde su vision de la realidad. La mayoría de cristianos y cristianas hemos caído en esa actitud, lo peor, poco confrontamos la realidad con las palabras, los hechos y los pensamientos de Jesús de Nazarert.

¿Usted se puede imaginar a Dios legitimando esas actitudes y descalificaciones, desentendiéndose del dolor y el sufrimiento de los seres humanos causados por la guerra y las violencias, del drama generado por los daños sociales y ambientales? Al mismo Dios que dice:

*¿Puede una madre olvidarse de su creatura, dejar de querer al hijo de sus entrañas? Pero, aunque ella se olvide yo no te olvidaré*” (Isaías 49, 15); “*Como un padre se enterneces con sus hijos, asi se enternece el Señor con sus fieles”* (Salmo 103,13)

Ordinariamente, quien está mirando una cara de la realidad cree tener toda la razón, cree estar viendo todo el panorama, desconoce a quienes miran otras caras de la realidad y cree que su lado es el correcto, el bueno y que los otros son los equivocados, los malos.

#### Como sociedad y como religiones nos parecemos a dos grupos de personas: 

el uno mira el lado de un muro donde está pintado un paisaje de alta montaña, el otro, mira el lado del muro donde está pintado un atardecer en la playa; cada uno habla de lo que ve y se sorprende que el otro grupo no hable de lo mismo, lo trata de ignorante, lo descalifica por equivocado; lo insulta porque no le concede toda la razón, reclama tener toda la verdad.

 

En la vida cotidiana poco nos tomamos el trabajo de averiguar qué hay al otro lado, de reconocer las otras maneras de ver, otros paisajes, otras comprensiones. Tenemos muchas dificultades para dar la razón a otra persona cuando la tiene o negamos la posibilidad que la otra persona pueda tener razón, creemos que, si la otra persona tiene razón, nosotros estamos equivocados, sin darnos cuenta que estamos mirando un lado distinto, otra cara de la realidad.

 Esta manera de ver las cosas nos está impidiendo ver la corrupción, los intereses torcidos, la falta de transparencia y la doble moral que hay en los nuestros. Esto impide que como sociedad caminemos hacia un país más justo, fraterno, respetuoso de las diferencias, que protege lo público, lo de todos, la naturaleza, la biodiversidad…

Lo más doloroso es que estamos permitiendo que la religión cristiana este siendo usada para dividir, para negar derechos, para oponerse en nombre de Dios a políticas públicas que busquen unas mejores condiciones de vida para todas las personas, para el cuidado de la casa que todos los seres humanos habitamos, del agua que todos bebemos, del aire que todos respiramos y la tierra que a todas y todos nos alimenta, de la que todos dependemos.

Fraternalmente, su hermano en la fe

Alberto Franco, CSsR, J&P

<francoalberto9@gmail.com>

 
