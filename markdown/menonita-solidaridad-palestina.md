Title: Iglesia Menonita de EEUU retira inversiones en empresas de Israel
Date: 2017-07-16 11:05
Category: Onda Palestina
Tags: BDS, boicot, Israel, Palestina
Slug: menonita-solidaridad-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Iglesia-menonita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: abouna.org 

###### 16 Jul 2017 

La iglesia menonita de los Estados Unidos votó la semana pasada a favor de **vender sus acciones en las compañías que se beneficien de la ocupación por parte de Israel del territorio palestino**. En el comunicado dejan claro que condenan el antisemitismo y animan a reforzar los lazos entre los miembros de esta iglesia y los judíos.

Este anuncio de desinversión se da en medio de acciones por parte de **diversas iglesias a nivel global que condenan la ocupación colonial israelí**. Una de estas acciones fue llevada a cabo por la Comunión Mundial de Iglesias Reformadas, quienes tienen más de **225 iglesias asociadas a nivel global**.

En la práctica, esto significa que la iglesia lanza una directriz al fondo de inversiones para que **revise periódicamente que ninguno de los 3 billones de dólares que poseen sean utilizados para apoyar las políticas de Israel en Palestina**.

Además el 7 de julio el consejo general de la iglesia menonita, llamó a sus afiliadas a que **examinen sus relaciones con Israel y Palestina** pensando por sus hermanos cristianos palestinos. Esa revisión sería en el marco de la defensa de los derechos humanos y la protección del Derecho Internacional.

Todas estas acciones se han dado luego de que la Coalición Nacional de Organizaciones Cristianas Palestinas dirigieron una carta abierta a las distintas iglesias cristianas para que **reconozcan que Israel es un Estado de Apartheid y apoyen al movimiento Boicot, Desinversiones y Sanciones, BDS**.

######  Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/category/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en [Contagio Radio](http://bit.ly/1ICYhVU). 
