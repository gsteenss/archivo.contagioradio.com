Title: Cumbre Agraria y gobierno definen calendario de acuerdos
Date: 2016-06-16 14:33
Category: Movilización, Paro Nacional
Tags: Cumbre Agraria, Movilización, Paro Nacional
Slug: cumbre-agraria-y-gobierno-definen-calendario-de-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/marcha-catatumbo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: congresodelospueblos] 

###### [16 Jun 2016] 

Tras 13 días de movilización nacional, la Cumbre Agraria logró establecer unos acuerdos iniciales con el gobierno nacional que deberán obedecer a un cronograma riguroso en que los primeros pasos se medirán en 30 días contados a partir de la firma de las actas. Los acuerdos son en torno a las garantías de DDHH para la movilización, territorio y el modelo minero energético.

Una de las exigencias de la movilización campesina fue definir  un calendario serio y urgente para el cumplimiento de los acuerdos. Como fruto de las discusiones de 3 días, se [logró que el gobierno se comprometiera en fechas concretas en torno a los diferentes puntos de exigencia.](https://archivo.contagioradio.com/?s=paro+agrario)

**Garantías de Derechos Humanos y justicia individual y colectiva**

El primer acuerdo en esta materia instauró que todas las personas detenidas en el marco de la Minga Nacional sean investigadas o juzgadas, sin embargo, el gobierno nacional se comprometió a establecer de manera permanente una mesa de garantías y seguimiento a casos concretos.

El gobierno es el encargado de definir la logística y recursos de las verificaciones de DDHH a que haya lugar en diferentes puntos del territorio.

**17 de Junio:** El gobierno se compromete a enviar una circular ministerial para el reconocimiento a defensores y defensoras de DDHH y dirigida a su protección.

**8 de Julio:** Reunión entre sub comisión de DDHH de la Cumbre Agraria y los garantes para evaluar y definir los mecanismos de funcionamiento permanente y la metodología.

**11 al 15 de Julio:** Gobierno convocará jornada de trabajo para revisar la directiva nacional sobre protección a defensores de DDHH. Al término de la jornada se evaluará la actualización de la directiva. En esta reunión participará la Sub Comisión de DDHH de la Cumbre Agraria, los garantes y plataformas y organizaciones de DDHH.

**15 de Julio:** Reunión de la sub comisión de DDHH y garantes para revisión de la normatividad vigente.

### **En materia de Justicia** 

**7 de Julio:** El vice ministerio de justicia se comprometió con la ONIC a evaluar y establecer mecanismos para la vigencia de la jurisdicción especial indígena.

**22 de Julio:** Se realizará una reunión en la que participarán delegados de la Sub Comisión de DDHH de la Cumbre Agraria, los ministerios de interior y justicia, el INPEC , la judicatura y la fiscalía para buscar soluciones a la crisis carcelaria.

### **En materia de protección colectiva** 

**11 de Julio:** Se realizará una mesa de trabajo convocada y organizada por la Unidad Nacional de Protección para evaluar las medidas de protección solicitadas.

Tanto el gobierno como la Cumbre Agraria se comprometieron a que el 30 de Septiembre habrá un borrador de decreto para protección individual y colectiva de los integrantes de las organizaciones sociales que hacen parte de esa organización.

**Víctimas**

En octubre del 2016 será el plazo para conformar un Consejo Nacional de Política Económica y Social (CONPES), el cual debe ser concertado por las diferentes mesas que integran la Cumbre Agraria, Campesina, Étnica y Popular.

**El 27 de junio** la UARIV, definirá la metodología para la creación de la coordinación nacional étnica, para así garantizar derechos a grupos étnicos, desde un enfoque diferencial.

**27 y 28 de junio** del 2016, 10 delegados de la Cumbre agraria, étnica y popular construirá el camino hacia el CONPES, donde se fijará un presupuesto  que será costeado por la UARIV y el Ministerio del Interior.

**11 y 12 de julio**: la UARIV junto con la asistencia de 6 delegados de la cumbre, en Santiago de Cali, quienes establecerán  protocolos de participación, reparacion integral, retorno y reubicación  que se aplican a grupos étnicos.

**31 de agosto** será la fecha límite para que la UARIV, para construir un  plan de financiamiento, que debe especificar los modos de inversión, para así implementarse en el año 2017.

**Hasta el 31 de diciembre** del 2016,  la UARIV, tiene plazo para presentar un proyecto de inversión concertada con las diferentes partes y enfocada para el cumplimiento de los decretos ley 4633 y 4635 de 2011.

**Desde el 12 de junio**, la UARIV, tiene una reforma normativa, que debe ser justificada bajo un un estudio técnico, jurídico y financiero. Este proceso se debe dar a partir de la concertación entre el ente institucional del Gobierno Nacional y la cumbre agraria, étnica y popular. Esta reforma debe ser coherente con el eje de víctimas expuesto por la cumbre.

### **Tierras campesinas.** 

En cuanto al eje de tierras campesinas, en el marco de la Minga nacional agraria, los días 11 y 12 de junio de 2016, en Santander de Quilichao, los sectores campesinos de CACEP, junto con el Ministerio de Agricultura y Desarrollo Rural, establecieron 22 puntos de acuerdo con respecto a 4 ejes fundamentales: territorios colectivos y ordenamiento territorial, territorios de especial protección, economía propia, y por último el tema de vivienda rural. Cada uno con fechas límites para su desarrollo y cumplimiento.

### **1.Tierras, territorios colectivos y ordenamiento territorial.** 

Según el gobierno nacional, en cabeza del Ministerio de Agricultura, junto con las CACEP, hasta el **12 de julio es el plazo para conformar un equipo en conjunto con el GN y hasta el 12 de agosto**  para trabajar  en la reforma de la ley 160 de 1994. En cuanto a materia de financiamiento de este plan de resolución de conflictos, estará a cargo el Ministerio de Agricultura y el Ministerio del Interior.

Mientras tanto, la Agencia Nacional de Tierras, establecerá reuniones con la CACEP, para así dar continuidad al punto de zonas rurales campesinas,  por lo tanto la AGT convoca el 11 de julio un consejo directivo, quienes determinarán por medio del voto la constitución de las ZRC.

**12 de julio** convocar instituciones y entidades que tengan alguna responsabilidad en el plan de desarrollo sostenible de las ZRC, para así días después establecer con ANZORC, un cronograma de audiencias públicas pendientes de las ZRC.

Se tiene previsto para el segundo periodo del 2016 hacer un censo que incluya la categoría campesino, trabajo que está enfatizado en la tenencia  y propiedad de la tierra, proceso que se llevará en conjunto con DANE y el Ministerio de Agricultura.

### **2.Territorios de especial protección** 

**12 de junio:** el gobierno se compromete a dar seguimiento del caso de la Hacienda Bella Cruz y de igual forma adjudicar las zonas baldías recuperadas  para el campesinado reclamante.

**3.Economía propia.**

**27 de junio**, las políticas públicas que se establecen por un UPOV 91 (Unión Para la Protección de Obtentores Vegetales) la cual construyó decretos del instituto colombiano de agricultura (ICA) como la 970, será discutido con por delegados de la CACEP, el ICA y demás instituciones involucradas en la materia.

Se realizará cada 15 días sesiones de logística y funcionamiento de la sub-comision de economía propia, que contará con la participación de 2 técnicos competentes del gobierno, y otros 2 de la CACEP.

Según los resultados arrojados por la sub-comisión de economía propia, se implementarán planes pilotos en seis ciudades-región, en los que se harán compra de cosechas para abastecer instituciones y programas de gobierno, mientras que la CACEP, velará por el cumplimiento de este proceso.

### **Vivienda rural.** 

Entre la CACEP y el Viceministerio de desarrollo rural, se socializarán  las políticas públicas referentes a la vivienda rural, para así concretar alternativas de programas de vivienda rural.

### **Tierras Indígenas** 

**El 24 de junio** se presentarán las propuestas de constitución y ampliación de 6 resguardos.

A partir del 27 de junio, el gobierno Nacional, reubica recursos del sector agropecuario con el fin de generar una inversión con el sector desplazado, y, también para cumplir otros acuerdos pendientes, se le destinará un monto de 40.000 millones de pesos.

En este mismo día,  junto con los delegados de la minga Nacional, la Agencia Nacional de Tierras y el Ministerio de Agricultura,  se estudiará el decreto 2333 de 2014 para así definir la delimitación y demarcación de territorios ancestrales, a los cuales se les destinará anualmente 3.000 millones de pesos.

Así mismo, se establecerá una evaluación de 400 millones de expedientes pendientes de legalización de predios, los cuales tienen predestinados 16.000 millones de pesos.

**Hasta el 12 de julio**, se tendrá para conformar un comité de seguimiento a los acuerdos pactados en materia territorial.

### **Tierras de comunidades negras** 

**12 de julio** de 2016 se concretará una reunión en la agencia nacional de tierras (ANT) para así, evaluar y analizar los resultados de tierras, con el fin de definir una metodología y presupuesto.

**En Junio de 2016** se establecerá una cumbre de cultivos de uso ilícito en territorios del pueblo negro, al igual que la implementación, erradicación y sustitución de los  cultivos ilícitos.

### **Minero energético Afro** 

En Julio 13 de 2016, se conforma la comisión mixta, para así, junto con el ministerio de cultura,  implementar la hoja de ruta y expedición del decreto reglamentario.

El 13 de junio de 2016 se definirá una agenda ambiental para atender los territorios en conflicto.

**Acuerdo en materia Minero energético**

El 12 de junio se establecerá una comisión conjunta entre la CACEP y el Gobierno Nacional, para así analizar títulos, contratos y autorizaciones minero energéticas, tanto en territorios urbanos como rurales.

El 12 de julio el Ministerio de Minas y Energía y otras instituciones, financiarán foros regionales sobre las diversas propuestas frente al desarrollo del sector minero energético. Así mismo, la CACEP construirá un diálogo ambiental y minero-energético del cual harán parte  el ANLA y el Ministerio de Minas, teniendo en cuenta que los resultados arrojados serán el insumo para la construcción de políticas públicas.

### **Paz** 

Hasta el 12 de septiembre, el Gobierno Nacional, junto con la CACEP,  creará un mecanismo de participación efectiva, por lo que se concertará una ruta tanto de trabajo como financiera.

Desde el 14 de junio el Gobierno Nacional debe concertar una reunión con el Ministerio del Interior y las CACEP una política pública de comunicaciones para los sectores indígenas, campesinos y populares.

**Acuerdos incumplidos**

Iniciando el mes de julio de 2016, el Gobierno Nacional tramitará los ajustes necesarios para remodelar la distribución del sistema general  participativo, que está estipulado en el acto legislativo 04 del 2007, que tiene en cuenta propuestas del pueblo Indígena.

Hasta el 12 del mes de octubre de 2016, el DNP y el Ministerio de Hacienda,  presentará una propuesta  en la que se faciliten  proyectos y su financiación, que son asignados a comunidades indígenas, ante la identidad de la Comisión Rectora del Sistema General de Regalías.

A partir del 28 de junio,  el Ministerio del Interior, el DNP, Organizaciones Indígenas y Entidades del Gobierno Nacional, realizará de forma conjunta la revisión de proyectos específicos para Pueblos Indígenas, y de igual forma, la formulación de 5 proyectos de inversión que serán presentados a las fuentes de financiación del Plan de Desarrollo Nacional.

Entre el 13 y el 18 de junio, el Ministerio del Interior, el Ministerio de Hacienda  y Crédito público, participará en las mesas de trabajo de CONTCEPI (Sistema Educativo Indígena Propio) del cual hace parte las organizaciones CIT, OPIAC, ONIC Y AICO.
