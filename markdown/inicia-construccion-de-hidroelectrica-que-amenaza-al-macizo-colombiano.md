Title: Inicia construcción de Hidroeléctrica que amenaza al Macizo colombiano
Date: 2020-02-12 18:19
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Cauca, Hidroelectrica, macizo colombiano
Slug: inicia-construccion-de-hidroelectrica-que-amenaza-al-macizo-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Macizo-Colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Diego Alejandro Reyes Cruz {#foto-diego-alejandro-reyes-cruz .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/oscar-salazar-sobre-construccion-hidroelectrica-amenaza_md_47744862_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Óscar Salazar | Líder ambiental y campesino

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Habitantes de dos de los nueve municipios que integran el núcleo del Macizo Colombiano, entre Cauca y Huila, están en alerta ante el inicio de obras para la construcción de una Hidroeléctrica que desviará dos fuentes hídricas de la cuenca del Río Patía. La preocupación se explica en la importancia de este territorio, del que nace el 70% del agua dulce de Colombia en los Ríos Magdalena, Cauca, Caquetá y Patía.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Qué es el Macizo Colombiano?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura (UNESCO) denominó al territorio como Reserva de biosfera del cinturón andino del Macizo Colombiano, señalando su importancia ambiental. (Le puede interesar: ["La Vega en Cauca lista para empezar proceso de consulta popular"](https://archivo.contagioradio.com/municipio-de-la-vega-en-el-cauca-mas-que-lista-para-consulta-popular/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el líder ambiental y campesino Óscar Salazar, todo el macizo comprende 89 municipios de 7 departamentos, pero su núcleo está en 9 municipios entre los departamentos de Cauca y Huila. De allí se calcula que nace el 70% del agua dulce en Colombia, que fluye a través de 4 grandes ríos (Cauca, Magdalena, Caquetá y Patía) hacia el norte, oriente y occidente del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La hidroeléctrica en el corazón del Macizo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades de La Vega y La Sierra, en Cauca, han señalado su preocupación ante el inicio de las obras para construir una hidroeléctrica a filo de agua, es decir, desviando afluentes para surtir las turbinas que generarán energía. El Proyecto afectará los caudales de los ríos Guachicono y Putis. (Le puede interesar: ["¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar"](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este hecho es relevante, porque de acuerdo a Salazar, la hidroeléctrica tomará un 75% del afluente del Río Putis, que recorre 25 veredas de La Vega, que hacen parte de una zona que es especialmente seca. Sumado a ello, está la prevención por el caudal del Patía, puesto que el río Putis surte al río Guachicono, cuya desembocadura termina en el gran río con salida al Pacífico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta obra cuenta con licencia ambiental desde 2013, pero solo hasta ahora inició su construcción, que será en un predio entre La Vega y La Sierra, pero sobre el que no se ha hecho publicidad. De hecho, las comunidades de la zona denuncian que hay poca publicidad en el territorio sobre la obra, y no se les ha consultado sobre la misma, como está estipulado para este tipo de proyectos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **No se trata solo de la Hidroeléctrica, es el riesgo del extractivismo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Salazar señala su preocupación porque actualmente "el 85% del Macizo está en proceso de concesión", y se está intentando cambiar su matriz productiva de agrícola a extractiva. En ello se explica la intención de construir una gran generadora de energía (como una hidroeléctrica) que, como lo han denunciado comunidades cercanas a Anchicayá o Hidroituango, surtirá la demanda de energía de las empresas extractivas y no de quienes habitan el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, el líder campesino pide que se contagie la defensa de los colombianos por el páramo de Santurbán para proteger la estrella hídrica de Colombia, así como a las comunidades que tradicionalmente la han habitado y cuidado. (Le puede interesar: ["Nada ha cambiado a 18 años del "ecocidio" en el río Anchicayá"](https://archivo.contagioradio.com/nada-ha-cambiado-a-18-anos-del-ecocidio-en-el-rio-anchicaya/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
