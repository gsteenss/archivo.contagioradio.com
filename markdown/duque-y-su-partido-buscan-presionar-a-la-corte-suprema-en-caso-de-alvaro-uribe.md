Title: «Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»
Date: 2020-08-05 22:09
Author: AdminContagio
Category: Judicial, Nacional
Tags: Álvaro Uribe, Corte Suprema de Justicia, Diego Cadena, Iván Cepeda, Sobornos
Slug: duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Álvaro-Uribe-ante-la-Corte-Suprema-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La investigación que cursa en contra del expresidente **Álvaro Uribe ante la Corte Suprema de Justicia** y el juicio que se encuentra en fase de imputación contra su abogado **Diego Cadena**; han suscitado el pronunciamiento de múltiples sectores en torno al caso. (Lea también: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Las Altas Cortes** a través de un comunicado conjunto firmado por los presidentes de la Corte Suprema de Justicia, la Corte Constitucional, el Consejo de Estado, el Consejo Superior de la Judicatura y la Jurisdicción Especial para la Paz; **pidieron «*respeto y garantía a la independencia judicial*»** y expresaron enfáticamente que **son «*inaceptables \[las\] descalificaciones a decisiones judiciales \[…\] sin que aún se conozca su contenido, sentido o alcance*».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1290493557833256960","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1290493557833256960

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Aunque no se señaló expresamente, el documento emitido por las Altas Cortes pareció ser un claro pronunciamiento frente al comunicado de prensa emitido por el Partido Centro Democrático y las declaraciones del presidente Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por una parte, el comunicado del Partido Centro Democrático, **cuestionaba las posibles decisiones que en el futuro cercano pudiese adoptar  la Corte Suprema en relación con el caso del hoy senador Álvaro Uribe Vélez** y apuntaba a muchos de los ítems luego despejados por las Altas Cortes en su pronunciamiento escrito.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1290356765025079298","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1290356765025079298

</div>

<figcaption>
(José Miguel Vivanco, director de Human Rights Watch para la Américas se pronunció también sobre el comunicado del Partido de Álvaro Uribe)

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otra parte, el presidente **Iván Duque en una entrevista concedida a un medio de comunicación, aunque señaló que «*no se pronunciaba sobre decisiones judiciales*»** aseguró que «*sería muy triste ver a las personas que lucharon por la seguridad y la legalidad en un proceso que les límite y les cercene sus derechos*» refiriéndose a Uribe y al caso; cayendo en una evidente contradicción.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1290315538212806657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1290315538212806657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La posición del senador Iván Cepeda quien funge como víctima en el proceso contra Álvaro Uribe

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador [Iván Cepeda](https://twitter.com/IvanCepedaCast) también se pronunció señalando que «*desde el partido Centro Democrático, la Presidencia de la República y los seguidores del uribismo se ha desatado **una agresiva campaña que busca sembrar confusión en la opinión pública, y presionar a los magistrados de la Corte con amenazas veladas o abiertas***». (Lea también: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el congresista afirmó que no era la primera vez que eso ocurre ya que «*en octubre de 2018, ante la citación a indagatoria al senador Uribe se hizo una campaña con el mismo libreto y propósito. En ese entonces **se intentaba convencer a la ciudadanía que Uribe está por encima de la justicia y que de darse esa diligencia judicial, sobrevendría un estado de conmoción nacional de consecuencias impredecibles*****»**. (Le puede interesar: [Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe](https://archivo.contagioradio.com/piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1290708604920750081","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1290708604920750081

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
