Title: Cierre de convocatoria  para Daupará, Muestra de Cine y Video Indígena en Colombia
Date: 2015-09-18 11:14
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine indígena, Convocatorias cine en Colombia, Dumpará muestra de Cine y Video Indígena
Slug: cierre-de-convocatoria-para-daupara-muestra-de-cine-y-video-indigena-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1458569_4945834742408_167666233_n-e1442592716446.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [18 Sep 2015] 

Hasta hoy 18 de septiembre tienen plazo para enviar sus trabajos audiovisuales los interasados en participar de la **Muestra de Cine y Video Indigena de Colombia, DAUPARÁ**, un espacio de exhibición, discusión e intercambio cultural que tendrá lugar entre el 24 y el 28 de noviembre en la ciudad de Bogotá.

Las obras postuladas, deben provenir del pensamiento indigena, bien sea por su escritura, dirección, o realización, por parte de comunidades indígenas o por productores externos que logran capturar, representar y dar voz al pensamiento de los individuos, pueblos u organizaciones.

Los **ideales** que busca sean representados por las producciones que integrarán la muestra son la **resistencia, soberanía y autodeterminación de los pueblos originarios**, a través de herramientas audiovisuales que promuevan la cultura, excluídas aquellas que presenten vinculación directa con prtidos políticos, instituciones gubernamentales y no guberanamentales o movimientos religiosos.

Los criterios de selección de la Organización de la muestra, incluyen la necesidad que las obras aporten al **fortalecimiento de la identidad y las culturas de los pueblos indígenas**, que promuevan el **diálogo intercultural**,  resalten las expresiones culturales en situaciones de desaparición y que aporten **estéticamente** al lenguaje audiovisual.

Además de participar de la edición 2015, las producciones integrarán el archivo audiovisual siendo exhibidas de manera itinerante cuando asi sea requerido por las comunidades y organizaciones, sin el propósito de obtener algún tipo de lucro con las obras seleccionadas.

La duración de las producciones asi como el formato de presentación son libres;  aquellas que sean enviadas en lenguas originarias deben estar subtituladas a Castellano. Ingrese en el enlace para conocer más detalles de los [requisitos y plataforma](http://daupara.org/index.php/historial-muestra-daupara/muestra-daupara1-2015/2015-06-25-16-14-40) de envío de las producciones audiovisuales.

[![logo-daupara](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/logo-daupara.png){.aligncenter .size-full .wp-image-14238 width="5140" height="1660"}](https://archivo.contagioradio.com/cierre-de-convocatoria-para-daupara-muestra-de-cine-y-video-indigena-en-colombia/logo-daupara/)

**Sobre la muestra.**

Bianualmente, la muestra se presenta en la ciudad de Bogotá con el objetivo de aportar al proceso de reconocimiento y fortalecimiento de los pueblos indígenas en Colombia, desde la mirada que los pueblos realizan de las realidades de su entorno.

Es la principal muestra de cine y video indígena de Colombia, cuenta con 6 años existencia y un importante número de producciones audiovisuales de contenido indígena tanto nacionales como internacionales.

**Daupará** ha recolectado películas de 42 pueblos indígenas a nivel nacional los cuales son: Nasa, Misak, Pastos, Awá, Quillasinga, Inga, Kamentsä, Arhuaco, Kogui, Wiwa, Kankuamo, Wayúu, Zenú, Emberá Dovida, Embera Katío, Emberá Chamí, Kofán, Uitoto, Siona, Bora, Muinane, Tikuna, Cubeo, Barasana, Tatuyo, Makuna, Andakíes, Chimilas, Pijaos, Kichwas, Mokaná, Muisca, Yeral, Yaruro, Coreguaje, Masiware, Siona, Tubú, Sikuani, Sáliva, Piapoco y Kuna Tule.

 
