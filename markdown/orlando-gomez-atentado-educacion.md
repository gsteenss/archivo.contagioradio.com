Title: Asesinato del profesor Orlando Gómez en Cauca es un atentado contra la educación
Date: 2019-08-14 10:51
Author: CtgAdm
Category: Educación, Líderes sociales
Tags: asesinato de líderes sociales, Asesinato de profesores, ASOINCA, Cauca
Slug: orlando-gomez-atentado-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/José-Orlando.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo Particular] 

El pasado lunes 12 de agosto, hombres armados llegaron al colegio Huasanó, en Caloto Cauca, hacia la 1:00 pm, y se llevaron al rector de la Institución Educativa Afro Empresarial Huasanó, Orlando Gómez que fue hallado horas después muerto en la vereda El Jagual en Corinto. Orlando también se desempeñó por más de una década como rector del colegio Miguel Zapata en El Plateado, Argelia.

### El docente por su razón de ser es un líder comunitario 

Según **Víctor Hugo Jiménez, directivo de la Asociación de Institutores y trabajadores de la Educación del Cauca (ASOINCA)**, Orlando Gómez no tenía amenazas en su contra, sin embargo, los móviles podrían ser la situación misma que vive el sector educativo en el departamento donde  en 2018 fueron asesinados 8 maestros, y en 2019 ya se han conocido 60 amenazas contra docentes. [(Le puede interesar: Miguel Morales, sexto profesor asesinado en Cauca este año)](https://archivo.contagioradio.com/miguel-morales-profesor-asesinado/)

Según indica el maestro, hay espacios del territorio nacional donde la única presencia que tiene el Estado es el docente, quien se convierte en **"el padrino de casi todos los niños, al que lo consultan para todo pero también se convierte en ese que le ayuda a despertar la conciencia a la comunidad",** una condición que lo vuelve vulnerable frente a grupos armados en particular en las zonas rurales.

### **"Estamos contando los muertos"  
** 

El integrante de ASOINCA advierte que la mayoría de amenazas provienen de grupos como el Bloque Suroriental de las Águilas Negras quienes no solo intimidan a la población y al profesorado al el sur del Cauca sino también en el macizo, una situación que denuncia Víctor, la Secretaria de Educación viene desconociendo, "**hay una negliglencia por parte de la  Secretaría al no entender que esto es una realidad".**

Víctor advierte que los profesores no quieren regresar a la institución, pues con la muerte de Orlando se ha golpeado con fuerza a los habitantes de El Jagual, "cualquier persona que en la Secretaria de Educación sea trasladada a esa zona no va a querer ir y a la finalidad es la comunidad la que pierde", afirma. (Le puede interesar: ["Miguel Morales, sexto profesor asesinado en Cauca en 2018"](https://archivo.contagioradio.com/miguel-morales-profesor-asesinado/))

<iframe id="audio_39960597" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960597_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
