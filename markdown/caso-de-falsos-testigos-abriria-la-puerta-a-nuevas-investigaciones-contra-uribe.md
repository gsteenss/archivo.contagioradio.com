Title: Caso de falsos testigos abriría la puerta a nuevas investigaciones contra Uribe
Date: 2018-09-04 17:33
Author: AdminContagio
Category: DDHH, Política
Tags: Alvaro Uribe, Caso de Falsos Positivos, Iván Cepeda
Slug: caso-de-falsos-testigos-abriria-la-puerta-a-nuevas-investigaciones-contra-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/alvaro_uribe_29.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Herald ] 

###### [4 Sept 2018] 

Frente a la orden de la Corte Constitucional de dejar en firme la investigación al senador Álvaro Uribe, por el caso de falsos testigos, Iván Cepeda, manifestó que es un avance alentador que podría develar **"una cadena de hechos"**,  y señaló que de todas formas hacen falta aún dos decisiones más, en este caso, para fijar la fecha de las rendiciones a indagatorias, tanto para Uribe como para Prada.

La primera de ellas tiene que ver con una petición de nulidad que presentó el representante a la Cámara Hernán Prada y la segunda, una solicitud que presentó Cepeda para conformarse como parte civil en este proceso.

### **Las nuevas pruebas de Uribe** 

Referente a las nuevas pruebas que presenta el senador Uribe, Cepeda aseguró que estas van a ser desechadas, "como lo fueron las que presentó en su momento durante el litigio en la Corte, **para intentar demostrar algo que es totalmente falso, y es que yo habría estado manipulado testigos para enredarlo jurídicamente**".

Uno de esos testimonios es el de alias "Monoleche", quien afirmó que a través de un tercero, Cepeda habría hecho un ofrecimiento para que testificara en contra de los hermanos Uribe Vélez. (Le puede interesar: ["¿Quién debe investigar a Álvaro Uribe?"](https://archivo.contagioradio.com/quieninvestigaraaalvarouribe/))

Para Cepeda la importancia de este proceso, en comparación a otros en los que el senador Uribe ha sido implicado, tiene que ver con que, en esta oportunidad el ex presidente es la persona directamente relacionada con los delitos de soborno y fraude procesal, que podrían traer como condenas la reclucion carcelaria. Además podría dar paso a otras investigaciones **como la masacre de El Aro, La Granja y el asesinato del defensor de derecho humanos Jesús María Valle**.

######  Reciba toda la información de Contagio Radio en [[su correo]
