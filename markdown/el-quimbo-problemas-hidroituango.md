Title: La hidroeléctrica El Quimbo, con los mismos problemas de Hidroituango
Date: 2019-08-05 13:36
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: El Quimbo, Hidroelectrica, Huila, Vía
Slug: el-quimbo-problemas-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/El-Quimbo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @EmgesaEnergia  
] 

El pasado domingo EMGESA (constructora de la obra) declaró la alerta amarilla en la represa de El Quimbo por el nivel del embalse; para ambientalistas, **el peligro en la zona se evidencia en el derrumbe de la carretera que conduce de Gigante a Garzón (Huila)** presentado en el sector Bengala. La emergencia deja incomunicado a una parte del Departamento, y es la segunda ocasión en que un hecho similar sucede como resultado de la construcción de la Hidroeléctrica.

Según **Miller Dussán, profesor de la Universidad Surcolombiana y defensor del ambiente**, las obras para construir El Quimbo dañaron el puente Paso del Colegio, "que dejó incomunicado a Huila con el suroccidente por cerca de un año, lo que causó daños graves".  Asimismo, referenció la destrucción de una ataguía (construcción) que desviaba el río Magdalena, y el "hundimiento en el área donde se construyó la zona de máquinas, además de fallas que se presentaron en el vertedero y las filtraciones en los túneles de galerías".

> [\#AlertaEnElQuimbo](https://twitter.com/hashtag/AlertaEnElQuimbo?src=hash&ref_src=twsrc%5Etfw)  
> No existen estudios fiables, ni suficientes sobre el riesgo sísmico, geológico y la estabilidad de la presa. [@EmgesaEnergia](https://twitter.com/EmgesaEnergia?ref_src=twsrc%5Etfw), [@EnelGroup](https://twitter.com/EnelGroup?ref_src=twsrc%5Etfw) y [@ANLA\_Col](https://twitter.com/ANLA_Col?ref_src=twsrc%5Etfw) han vulnerando los derechos de los pescadores artesanales y las poblaciones rivereñas. [\#EnelYAnlaRiesgoParaHuila](https://twitter.com/hashtag/EnelYAnlaRiesgoParaHuila?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/Z7Ta7BApJF](https://t.co/Z7Ta7BApJF)
>
> — Asoquimbo (@Asoquimbo) [July 29, 2019](https://twitter.com/Asoquimbo/status/1155897383030579200?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Estudios insuficientes y medidas judiciales que llegan tarde** 

Dussan referenció un informe de 2014, a cargo de la Contraloría General de la Nación en el que se señaló que los estudios hechos por EMGESA para construir El Quimbo "presentan insuficiencias técnicas", lo que no permitió una adecuada caracterización ambiental, generando una amenaza por el riesgo de fallas. Tras el informe de la Contraloría, la Asociación de Afectados por el Proyecto Hidroeléctrico del Quimbo (ASOQUIMBO) solicitó al gobernador del Huila, Carlos Julio González en 2016 adelantar un estudio sobre el tema.

En la investigación, que es la más reciente sobre el lugar, se concluye que EMGESA no contempló todas las amenazas para la construcción del Proyecto; que según el profesor, incluyen la evaluación de la tierra, porque la zona es un "escenario de fallas geológicas y alta sismicidad". Ante la advertencia hecha por la Contraloría, ASOQUIMBO interpuso una demanda ante el Consejo de Estado en 2014, pidiendo una medida cautelar para evitar la construcción de la obra; el Tribunal aceptó la demanda y 5 años después determinó que no había lugar a una medida cautelar porque El Quimbo ya estaba construido.

### **"Lo que está pasando en El Quimbo es similar a lo que ocurrió en Hidroituango"** 

El Consejo de Estado también convocó a una audiencia para dar seguimiento a la demanda interpuesta por ASOQUIMBO, luego de 2 aplazamientos, **el 9 de agosto se realizará a las 2 pm este evento.** Mientras en Bogotá se desarrollará la audiencia, los ambientalistas citaron a un plantón en la sede principal de EMGESA en Garzón, Huila, para exigir la nulidad de la licencia ambiental de El Quimbo, y se suspenda la generación de energía hasta que se resuelvan las dudas sobre el Proyecto. (Le puede interesar:["Audiencia pública buscar frenar la construcción del Quimbo"](https://archivo.contagioradio.com/audiencia-publica-busca-frenar-quimbo/))

Como lo resumió Dussán, **todo el Huila es víctima de El Quimbo porque están paralizadas las vías del Departamento, y es la misma empresa constructora la que debe dar solución al problema.** En su lugar, "todavía no se han entregado estudios de plan de contingencia, que debió haber presentado la Empresa antes de construir la hidroeléctrica"; y el Gobernador se limita a decir que la carretera se habilitará en un carril en 21 días, mientras la reapertura total de la vía tomará 6 meses. (Le puede interesar: ["¿Realmente se necesita al Quimbo con pleno fenómeno de El Niño?"](https://archivo.contagioradio.com/realmente-se-necesita-el-quimbo-con-pleno-fenomeno-de-el-nino/))

Situación que se suma a la sospechas de filtraciones en las galerías que incrementan la presión sobre la obra a medida que aumenta el nivel del embalse; en ese sentido, el Profesor declaró que "lo que está ocurriendo en **El Quimbo es similar a lo que ocurrió en Hidroituango**", ante lo cual **la única solución es desmantelar la obra.** (Le puede interesar: ["Muro de contención de Hidroeléctrica Quimbo se completó con &lt;arcilla y arena&gt;"](https://archivo.contagioradio.com/muro-de-contencion-de-hidroelectrica-el-quimbo-se-completo-con-arcilla-y-arena/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39648573" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39648573_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
