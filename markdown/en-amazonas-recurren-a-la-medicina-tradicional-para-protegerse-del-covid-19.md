Title: En Amazonas recurren a la medicina tradicional para protegerse el covid-19
Date: 2020-05-26 17:36
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: amazonas, Covid-19, indigenas, medicina tradicional
Slug: en-amazonas-recurren-a-la-medicina-tradicional-para-protegerse-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/28427051610_8104be4f38_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Ayahuasca brewing\_03*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 20 de mayo la Organización Nacional Indígena de Colombia **(ONIC) , reporto el contagio por Covid-19 de uno de los consejeros de salud y medicina ancestral, en Puerto Nariño** [Amazonas](https://archivo.contagioradio.com/en-el-amazonas-por-cada-diez-mil-personas-hay-182-contagios/), días después el Consejero reportó una recuperación satisfactoria, según él, gracias a la medicina tradicional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1263296793930010624","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1263296793930010624

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Rosendo Ahué**, es [consejero de salud](https://archivo.contagioradio.com/guerra-y-covid-19-comunidades-indigenas-y-afro-bajo-doble-confinamiento-en-valle-del-cauca/) , medicina tradicional y salud occidental de la ONIC; **tiene 52 años**, **pertenece al pueblo Ticuna, en Puerto Nariño, Amazonas;** en entrevista con Contagio Radio, reveló como adquirió el virus y las acciones que tomó para su recuperación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahué a inicios del mes de mayo, decidió realizar una recorrido ***por los 22 cabildos que hay en el municipio,*** ***brindando orientaciones a la comunidad sobre el autocuidado, y la importancia del conocimiento tradicional como medida contra el olvido estatal.***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**La idea era echar mano a lo que nosotros teníamos al alcance como medicina propia, conocimiento y saberes de nuestras comunidades indígenas,** y para ello era necesario explorar los diferentes territorios y así conocer qué materia prima tenían para mitigar las acciones de contagio" .*
>
> <cite>**Rosendo Ahué** | Experto en medicina tradicional </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Luego de este recorrido de prevención y contención, Ahué señala que llegó una comisión de salud pública al territorio, *"**solicité una prueba libremente, teniendo en cuenta que había recorrido varios territorios donde posiblemente habían contagios**, luego de esto al segundo día empecé a sentirme mal" .*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa misma noche Rosendo empezó a sentir fiebre y un fuerte dolor en sus extremidades, **sin embargo no tuvo dificultad respiratoria ni dolor de cabeza**, síntomas de alarma ante la presencia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los síntomas se intensificaron varios días, *"al sentirme tan mal junto con mi esposa empezamos a aplicar los conocimientos de la madre tierra, conseguimos jengibre, ajo, cebolla, limón y **algunas otras plantas, las hervimos y las tomé tres veces, tan caliente como me fuera posible**"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" **Las vaporizaciones ayudan a descargar cosas negativas que hay en el cuerpo, la idea de sudar hasta resistir,** esto ayuda descargar la enfermedad y ayuda a matar el virus que hay en el cuerpo, esto me alivió muchísimo junto a las bebidas calientes".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Algunas de las plantas que recuerda el consejero son, hojas de limoncillo, naranjo, guanábana y guayaba, *"**usamos la medicina tradiconal como última herramienta que nos queda a los pueblos indígenas para proteger y cuidar nuestras vidas** en medio de la pandemia" .*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre el 20 de mayo Ahué **recibió una llamada desde un comité de salud en Bogotá, quiénes le notificaron que que su prueba había salido negativa**, a pesar de ello él continúo en su casa realizando los cuidados y evitando salir atendiendo la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tres días después **recibió una visita de la brigada de salud del municipio donde le notificaron que la prueba realizada días anteriores había salido positiva**, a su vez le entregaron un certificado que lo identificaba como persona contagiada con el [coronavirus](https://archivo.contagioradio.com/50-anos-de-promesas-fallidas-a-los-embera/).

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Finalmente yo acepté el diagnóstico, seguí en mi casa y continúe tomando la medicina tradicional, algunos días después mi esposa enfermó** con síntomas similares a los míos e incluso peores, tenía un fuerte dolor de cabeza y malestar general"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Nora Castillo, compañera de Rosendo tiene 48 años**; los síntomas que manifestó fueron dolor de cabeza, continuo mareo y fuertes dolores estomacales. **Sin un diagnóstico positivo o algún tipo de prueba Nora continúen su casa acatando los mismo cuidados** que había hecho días anteriores a su esposo.

<!-- /wp:paragraph -->

<!-- wp:heading -->

"Trabajar con lo que se tiene es el eje de la medicina tradicional"
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

"**Hoy trabajamos con los conocimientos que nos heredaron nuestros ancestros,** conocimientos que les ayudaron a sobrevivir a otras pandemias tiempo atrás"señaló Ahué.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma reconoce que así como el Amazonas tiene una amplia variedad de productos que por años han servido para mejorar la salud de las comunidades, hay otros territorios en Colombia campesinos, indígenas y afro, que tienen estos conocimientos y que **ante la desatención estatal deberían empezar aplicarlos y valorarlos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de los esfuerzo de médicos ancestrales como Rosendo para mitigar la crisis en el Amazonas, el más reciente [Boletín 027](https://www.onic.org.co/boletines-osv/3892-boletin-027-sistema-de-monitoreo-territorial-smt-onic-informacion-para-proteger-la-vida-y-los-territorios) presentando por la ONIC registra un total de** 330.927** **familias indígenas en alerta por probabilidad de contagio, y un total de 10 fallecidos**

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![Boletin027 Image001](https://www.onic.org.co/images/comunicados/covid19/Boletin027_Image001.jpg)  

<figcaption>
Crédito: Onic

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Desde el inicio de la pandemia **las organizaciones indígenas han aplicado diferentes estrategias, construyendo un documento de prevención Y contención de la pandemia a la luz de las orientaciones técnicas y científicas expuestas por expertos del Gobierno**, realizando cambios para que se adecuen al contexto socio cultural de los pueblos indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de esto según Ahué, *"el virus tiene diferentes síntomas que lamentablemente atacan con mayor fuerza a nuestra población mayor y a las personas que padecen otras enfermedades, **personas que requirieron apoyo respiratorios, y atención occidental la cual aún seguimos sin recibir**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente hace un llamado al Gobierno al cumplimiento de las exigencias y obligaciones para el cuidado de las comunidades indígenas, y agrega que aunque **el valor de la medicina tradicional por siglos a salvado vidas como la él, la desatención medica occidental y la falta hospitales en el Amazonas no puede seguir siendo parte del olvido.**

<!-- /wp:paragraph -->
