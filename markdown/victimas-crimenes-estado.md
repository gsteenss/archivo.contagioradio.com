Title: 18 mil víctimas de crímenes de Estado deben ser reconocidas públicamente
Date: 2018-10-26 17:21
Author: AdminContagio
Category: Nacional
Tags: crímenes de estado, MOVICE, SIVJRNR
Slug: victimas-crimenes-estado
Status: published

###### Foto: Contagio Radio 

###### 26 oct 2018

El Movimiento de Víctimas de Crímenes de Estado MOVICE, presentó cuatro informes ante Sistema Integral de Verdad, Justicia, Reparación y no Repetición **( SIVJRNR)** en los que se el Proyecto Colombia nunca más **documentó más de 17.852 víctimas de crímenes cometidos por agentes estatales**, considerando que su situación debe reconocerse pública y nacionalmente.

Según los informes, sobre los hechos tendrían responsabilidad **2.909 funcionarios del Estado**, entre los que se cuentan **31 Mayores, 10 Generales y 16 Comandantes de los presuntos 818 responsables del Ejército**. Adicionalmente se identificaron como presuntos responsables **856 agentes de la Policía, funcionarios del DAS, del B2 y del F2**, y se describe la relación entre varias de estas personas e instituciones con estructuras paramilitares.

La información recopilada en los 3 primeros informes, comprende los **crímenes cometidos entre 1966 y 1998**, diferenciados por zonas, el primero hace referencia a la **Zona VII**, que abarca los departamentos de Meta y Guaviare; en el segundo la **Zona XIV**, correspondiente al Magdalena Medio y el Nordeste Antioqueño; y la **Zona V**, conformada por Santander, Norte de Santander, Sur de Bolívar y Sur del Cesar.

El cuarto informe esta relacionado con los crímenes cometidos específicamente en la Comuna 13 de Medellín en el periodo de 2001 a 2003, que incluye la **operación Orión** cometida durante la comandancia del **General (r) Mario Montoya** de la IV Brigada, quien hace pocos días  firmó el sometimiento ante la JEP, pero **afirmando el y sus abogados que no aportaran a la verdad ni al reconocimiento de las víctimas**.

Martha Giraldo, integrante del MOVICE Capítulo Valle del Cauca, explica que para cada una de las instancias que conforman el ** **SIVJRNR existe una intención particular con los informes "para la JEP es que pueda impartir justicia sobre los máximos responsables de esos crímenes; a la Comisión de la verdad par que pueda identificar la responsabilidad selectiva del Estado en el desarrollo del conflicto armado" incluyendo las formas de colaboración con el paramilitarismo.

En cuanto al aporte que pueden hacer a la Unidad de búsqueda de personas dadas por desaparecidas, Giraldo asegura que al suministrar información sobre **1170 casos de desaparecidos por el Estado** "nuestra estrategia es documentar y poder entregar esa información a la Unidad de Búsqueda" que en algunos de los casos se ha venido recopilando hasta **por más de 30 años con familiares de las víctimas.**

Por último, Giraldo asegura que desde la organización esperan que todos los informes, "sean un aporte del MOVICE pero que ellos lo puedan tener en cuenta en el desarrollo de su mandato, para poder desarrollar todas las acciones, los planes metodológicos, los planes de trabajo a nivel nacional" **en favor de esclarecer y reconocer publicamente a las víctimas de crímenes de Estado en el país**.

<iframe id="audio_29638375" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29638375_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
