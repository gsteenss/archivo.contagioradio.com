Title: Comunidad Wounaan desplazada en Buenaventura sigue exigiendo garantías para el regreso
Date: 2015-02-19 20:48
Author: CtgAdm
Category: DDHH, Nacional
Tags: buenaventura, Comunidad Wounaan desplazada en Buenaventura, Wounaan, Wounaan retorno de Buenaventura
Slug: comunidad-wounaan-desplazada-en-buenaventura-sigue-exigiendo-garantias-para-el-regreso
Status: published

###### **Foto:Contagio Radio** 

<iframe src="http://www.ivoox.com/player_ek_4107656_2_1.html?data=lZadmZuZeo6ZmKiak5iJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRm9Dpz8bO0JCmucbiwtvS0NnZtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Edison Málaga] 

**A pocos días de cumplirse tres meses de desplazamiento en el Coliseo de la ciudad de  Buenaventura**, comunidades indígenas wounaan, continúan trabajando en la planificación del retorno, en la que exigen mayor compromiso del  gobierno nacional para agilizar los pasos en el regreso al territorio.

Según Edinson Málaga, vocero del resguardo Unión Aguas Claras, para poder realizar **el retorno primero se debe cumplir un cronograma con la comisión de verificación**, después se debe realizar una evaluación de la seguridad y del transporte. Las propias comunidades han demandado más tiempo para poder realizar bien el documento y garantías de seguridad sobre el terreno.

**Las condiciones en el Coliseo continúan siendo precarias** y los afectados reclaman **atención sanitaria** y una **alimentación diferenciada**, así como la **educación** de sus propios hijos que no pueden acceder por falta de transporte.
