Title: Así funciona la Guardia Campesina
Date: 2017-08-14 13:02
Category: Movilización, Nacional
Tags: Cauca, defensores de derechos humanos, guardia campesina, guardia cimarrona, lideres sociales
Slug: asesinatos-de-lideres-sociales-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cauca-lideres-sociales-e1500314265502.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [14 Ago 2017] 

Ante la inoperancia del Estado para garantizar la seguridad de quienes defienden los derechos humanos y los derechos del territorio, los habitantes del sur occidente colombiano manifestaron la necesidad de fortalecer la guardia campesina y cimarrona. Esto en medio del asesinato de** Fernando Asprilla, vicepresidente de la Junta de Acción Comunal de la Tigra en Piamonte** y miembro de Marcha Patriótica.

Según Deivin Hurtado, miembro de la Red de Derechos Humanos del Suroccidente Colombiano, Francisco Isaías Cifuentes, “el asesinato de Fernando Asprilla es otro hecho que lamentar, **solamente el 8 de agosto fueron amenazados 3 líderes en el Cauca**”.

De igual manera, Hurtado indicó que Asprilla **no fue asesinado dentro de una situación de hurto** como se había manifestado en la medida que “tenía todas sus pertenencias cuando fue hallado el cuerpo sin vida”. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

**En menos de un mes asesinaron 5 líderes sociales en el Cauca**

La situación de los defensores de derechos humanos en esta región del país se ha agudizado en la medida que **en lo que va corrido del 2017 han asesinado a 20 líderes**. Es por esto que han venido exigiéndole al Gobierno Nacional que establezca políticas de protección que se ajusten a los territorios en las zonas rurales.

De igual manera, han decidido **fortalecer la guardia campesina y la guardia cimarrona como procesos de seguridad** para evitar que sigan ocurriendo asesinatos en los territorios. Según Hurtado “la policía de los municipios no tiene la capacidad para hacer presencia en las zonas rurales y cuando la seguridad campesina hizo control en algunos territorios disminuyó la presión hacia los líderes”. (Le puede interesar: ["Informe de Fiscalía sobre asesinatos a defensores de DD.HH es "otro falso positivo"](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo/))

### ** Procesos de seguridad de la Guardia Campesina ** 

-   En los municipios se realiza un censo de las veredas con la participación de los y las habitantes.
-   Luego se realiza un calendario en el que se asigna un día de vigilancia a cada vereda. El número de "guardias" depende de la densidad poblacional de la vereda y de acuerdo a ello se unen una o más veredas para poder atender los puestos de control que se han ubicado.
-   Cuando una vereda realiza la vigilancia, los demás le llevan alimentos a las personas.
-   En los puestos de control, la guardia revisa quienes entran a los territorios en la medida que ya han podido identificar vehículos y personas sospechosas.
-   Igualmente, tienen una coordinación y una comunicación constante con la fuerza pública y con instituciones como la defensoría del pueblo.

Deivin Hurtado afirma que con este tipo de mecanismos de auto protección **ha sido posible unir a las comunidades** para recorrer los territorios y así salvaguardar la vida de los defensores de derechos humanos. Así mismo, aseguró que "es un mecanismo que está al servicio de la comunidad y que responde a la falta de medidas de seguridad que no ha brindado el Estado”.

Finalmente, la Red de Defensores Francisco Isaías Cifuentes manifestó que **han intentado tener presencia con delegados en las veredas** y zonas donde las guardias campesinas y cimarronas están realizando controles. De igual manera esperan que se reconozcan los derechos de los campesinos para se puedan fortalecer los procesos de seguridad de la guardia campesina.

<iframe id="audio_20325036" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20325036_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
