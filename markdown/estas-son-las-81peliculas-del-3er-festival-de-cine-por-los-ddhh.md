Title: Estas son las 81 películas del 3er Festival de Cine por los DDHH
Date: 2016-05-11 17:59
Author: AdminContagio
Category: 24 Cuadros
Tags: 3er Festival de cine por los DDHH, La semilla del silencio, Selección Oficial Festival de Cine por los DDHH
Slug: estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/la-semilla-del-silencio-con-andres-parra-angie-cepeda-y-julieth-restrepo_opt2_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Fotograma: La semilla del Silencio 

##### 11 May 2016

Luego de visualizar y evaluar, aspectos tanto técnicos como de contenido, de las cerca de 1063 películas provenientes de 52 países del mundo, el equipo de curaduría del 3[er Festival Internacional de Cine por los Derechos Humanos](https://archivo.contagioradio.com/asi-luce-la-3ra-edicion-del-festival-internacional-de-cine-por-los-ddhh/) de Bogotá, presentó la lista de producciones que integran la Selección Oficial del evento.

Stephanie Gómez, realizadora audiovisual, docente y curadora del Festival, asegura que el trabajo de escoger las producciones es "exigente" considerando en primera instancia la "relación que tienen la obras con el tema de los Derechos Humanos" y a partir de ahí si con su contenido aborda la "defensa, la denuncia, o la promoción" de estos Derechos.

Una vez determinado el contenido, el equipo evalúa aspectos "técnicos y de producción, de fotografía, de arte " que cumplan con los requerimientos establecidos para su exhibición,  y que logren provocar algún impacto y aporten propositivamente en materia de Derechos Humanos.

Estas son las 81 películas de 27 países, que encontrarán los asistentes en las diferentes pantallas que acogerán desde el 24 de Mayo la presente edición, en las categorías Largometraje, Documental internacional, Documental nacional, Cortometraje internacional, Cortometraje nacional y Animación.

[![progra 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/progra-1.png){.size-full .wp-image-23834 .aligncenter width="593" height="242"}](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/progra-1/)

[![progra 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/progra-2.png){.size-full .wp-image-23835 .aligncenter width="569" height="327"}](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/progra-2/)

[![progra 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/progra-3.png){.size-full .wp-image-23836 .aligncenter width="367" height="352"}](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/progra-3/)[![progra 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/progra-4.png){.size-full .wp-image-23837 .aligncenter width="501" height="598"}](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/progra-4/)

<iframe src="http://co.ivoox.com/es/player_ej_11505051_2_1.html?data=kpaikpqUeZKhhpywj5WZaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncbToxtXVw9PNqYy7hqigh6eXscbujJKYpdrWpcXj08aYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
