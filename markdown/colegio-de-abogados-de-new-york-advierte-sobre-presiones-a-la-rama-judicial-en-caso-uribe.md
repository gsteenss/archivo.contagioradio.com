Title: Colegio de abogados de New York advierte sobre presiones a la rama judicial en caso Uribe
Date: 2020-12-07 21:55
Author: PracticasCR
Category: Actualidad
Slug: colegio-de-abogados-de-new-york-advierte-sobre-presiones-a-la-rama-judicial-en-caso-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Alvaro-Uribe-Corte-Suprema-de-Justicia-Colegio-de-abogados-de-New-York.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**El Colegio de Abogados de la Ciudad de New York publicó un informe en el que expresa su preocupación por las amenazas a la independencia del poder judicial colombiano** que han surgido luego de que la Corte Suprema de Justicia decidiera decretar orden de captura con prisión domiciliaria en contra del expresidente Álvaro Uribe por la presunta comisión de los delitos de fraude procesal y soborno a testigos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe hace [un recuento del caso del expresidente Álvaro Uribe](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/) ante la justicia y resalta las preocupaciones de líderes de derechos humanos, de Human Rights Watch y de varios sectores que aseguran que la justicia en Colombia se está politizando alrededor de este caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“*Específicamente, el expresidente Uribe y sus seguidores, incluido el actual presidente de Colombia, han cuestionado públicamente la imparcialidad de la Corte que supervisa su caso y han propuesto reformas al poder judicial, tanto en respuesta a la prisión preventiva de Uribe como en oposición a la investigación en curso*”, señaló la asociación estadounidense. (Le puede interesar: [«Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que el pasado 4 de agosto la Corte Suprema de Justicia emitió orden de captura en contra del expresidente a través de un auto que superaba los 200 folios en el que exponía varias de las pruebas en las que fundamentaba su decisión. Esto llevó a Uribe a renunciar a su curul en el Senado lo que provocó que perdiera su fuero parlamentario y por lo tanto a que la Corte perdiera competencia sobre su caso. (Le puede interesar: [Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe luego de la orden de detención, sectores afines al expresidente, especialmente militantes del Centro Democrático empezaron a cuestionar la legitimidad de la Corte **lanzando “*ataques políticos y públicos alegando, a pesar de la evidencia, que los jueces no estaban basando sus decisiones en los hechos y la ley, sino más bien en bases ideológicas o políticas*”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Colegio de Abogados de New York también citó en su informe las voces de organismos internacionales como Human Rights Watch y el relator Especial de la ONU sobre la Independencia de Magistrados y Abogados, Diego García Sayán, quien señaló que existía un posible riesgo de amenaza o injerencia en la independencia del poder judicial y que las normas internacionales de administración de justicia deben respetarse.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la organización estadounidense instó a las autoridades en Colombia en relación con los anuncios de reforma estructural a la Justicia expresando que “*El Colegio de Abogados de la Ciudad de New York hace un llamado urgente al gobierno colombiano y a sus funcionarios electos, junto con los partidos y gobiernos extranjeros, a que cesen las declaraciones públicas y los esfuerzos legislativos diseñados para favorecer a un acusado penal en particular, incluso uno tan popular como el ex presidente Uribe, o para crear impedimentos a la justicia imparcial”.* (Le puede interesar: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Colegio de Abogados de New York es una reconocida asociación que reúne a los juristas de esa ciudad desde hace 150 años, de ahí que la publicación de este [informe](https://www.nycbar.org/member-and-career-services/committees/reports-listing/reports/detail/guarantees-of-judicial-independence-in-colombia) y las preocupaciones esgrimidas en este hayan trascendido. En dicho informe también expresan que “*ningún individuo debe estar por encima de la ley y el expresidente Uribe merece que se establezca su inocencia o culpabilidad de conformidad con las pruebas y la ley, de conformidad con el debido proceso y sin injerencias externas*”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1335296784101412865","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1335296784101412865

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
