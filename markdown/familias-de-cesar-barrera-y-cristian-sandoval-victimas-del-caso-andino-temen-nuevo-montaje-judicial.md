Title: Familias de César Barrera y Cristian Sandoval, víctimas del caso Andino, temen nuevo montaje judicial
Date: 2020-06-03 15:09
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Cárcel, #CasoAndino, #INPEC, #Prisionerospolíticos
Slug: familias-de-cesar-barrera-y-cristian-sandoval-victimas-del-caso-andino-temen-nuevo-montaje-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/JOVENES-ANDINO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Las familias de César Barrera y Cristian Sandoval temen un nuevo montaje judicial en contra de los jóvenes. El hecho se da luego de que no se produjera la libertad de las personas, tras la orden de un juez que daba libertad inmediata por vencimiento de términos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con las familias, la nueva recaptura de los hombres estaría relacionada con el atentado en el Centro Comercial Andino, cometidos en junio de 2017. Mañana se realizará la audiencia de legalización de captura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este mismo hecho actualmente hay otras 8 personas privadas de la libertad:** Alejandra Méndez, Lizeth Rodríguez y Lina Jiménez**. Las tres mujeres se encuentran en la cárcel de Picaleña, en Ibagué, en condiciones de total vulneración a los derechos humanos según los han denunciado abogados y familiares.  (Le podría interesar: [«Colectivo Libres e inocentes»](https://www.facebook.com/LibreseInocentes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Boris Rojas, Andrés Bohorquez, Juan Camilo Pulido e Iván Ramírez, que se encuentran en la cárcel La Picota, en Bogotá, y Natalia Trujillo**, que se encuentra en otra reclusión. (Le puede interesar:[ «La versión de los jóvenes capturados por el atentado en el Centro Comercial Andino»](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La defensa de las diez personas ha sido reiterativa en señalar que, tras más de dos años de audiencias, la Fiscalía no ha enseñado ningún tipo de material probatorio que implique a los jóvenes en el atentado en contra del Centro Comercial Andino.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
