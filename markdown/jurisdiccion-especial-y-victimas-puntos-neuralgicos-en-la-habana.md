Title: Jurisdicción Especial y víctimas: puntos neurálgicos en la Habana
Date: 2016-11-08 15:02
Category: Nacional, Paz
Tags: conversaciones de paz, FARC, Jurisdicción Especial
Slug: jurisdiccion-especial-y-victimas-puntos-neuralgicos-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Jurisdicción-Especial-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

###### [08 Nov 2016]

Luego de las elecciones del plebiscito por la paz, las conversaciones con los promotores del NO y los promotores de las campañas del SI, las discusiones en la Habana se están centrando en los cambios que exigen a la **Jurisdicción Especial de Paz**, punto que afecta directamente los derechos de las víctimas, la elegibilidad y la cárcel como mecanismo de justicia y no con justicia restaurativa como se planteó en el acuerdo firmado.

Según algunas versiones que han sido publicadas en medios y luego de la alocución del presidente Santos, se ha conocido que las discusiones están en un punto muy difícil. Uno de los puntos clave es la Jurisdicción Especial de Paz en que se **plantea volver el tema de la prisión efectiva para integrantes de las FARC y se restringiría su movilidad para atender asuntos que tengan que ver con el proceso de paz** y respaldo logístico a la concentración y la dejación de armas. ([Le puede interesar Militares detenidos respaldan la jurisdicción de paz](https://archivo.contagioradio.com/militares-detenidos-respaldan-el-proceso-y-la-jurisdiccion-especial-de-paz/))

Otro de los puntos clave en la discusión tiene que ver con la incorporación del acuerdo al bloque de constitucionalidad. Mientras que el acuerdo firmado el 26 de Septiembre se plantea como **acuerdo especial que lo incluye a la carta constituyente y solamente deja en manos del congreso la necesidad de implementar lo ya acordado**. Si no se incluye el acuerdo en el bloque, el congreso tendrá la potestad de modificarlo para el proceso de implementación.

Adicionalmente en la mañana de este martes Caracol Radio afirmó que la delegación de paz de las FARC se había levantado de la mesa de conversaciones lo cual no es cierto. **Según nuestras fuentes el gobierno pidió un receso para unificar posturas de cara al punto 5 o de víctimas**, las conversaciones se reanudarían a  las 9 de la mañana, sin embargo solamente hasta las 11 am se volvieron a sentar las partes para continuar con la discusión, que está en uno de los momentos más problemáticos.

Por su parte, Carlos Medina, integrante del **Centro de Pensamiento de la Universidad Nacional** afirma que un nuevo acuerdo no va a desconocer los resultados del plebiscito pero que también es necesario tener en cuenta que la diferencia fue muy pequeña y por lo tanto el **acuerdo pactado con las FARC también cuenta con un muy buen margen de legitimidad**.

<iframe id="audio_13670222" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13670222_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
