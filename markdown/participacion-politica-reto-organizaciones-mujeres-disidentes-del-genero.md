Title: Participación Política, el reto de organizaciones de Mujeres y Disidentes del Género
Date: 2017-02-19 22:45
Category: Mujer, Nacional
Tags: conversaciones de paz con el ELN, Implementación de los Acuerdos de paz, LGBTI y Disidencias de género, Mujeres y Feministas
Slug: participacion-politica-reto-organizaciones-mujeres-disidentes-del-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-21-at-1.14.13-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Feb 2017] 

Más de 40 integrantes de distintas organizaciones de mujeres, feministas y disidentes del género, reunidas en el municipio de Santander de Quilichao Cauca, plantearon una agenda conjunta para hacer frente al panorama que dejó el resultado del plebiscito, lo que suponen los incumplimientos por parte del gobierno en la vigente fase de implementación de los acuerdos con las FARC y la apertura al proceso de negociación entre la guerrilla del ELN y el Gobierno, siendo uno de los mayores retos y oportunidades para la participación de las mujeres.Participación

De acuerdo con ello, se ha propuesto trabajar en pro de unos lineamientos orientados a la construcción de paz para lograr una mayor participación de incidencia en la toma de espacios de poder, exigencia de garantías de seguridad para líderes y lideresas en los territorios, acceso equitativo de los medios de producción, formación política con y para las poblaciones campesinas, étnicas, de víctimas, mujeres y LGBTI para hacer veeduría y control a la implementación de los acuerdos de La Habana, son algunos de los resultados del 1er Encuentro de Mujeres y Disidencias de Género.

### Formación y garantías para una participación real 

Afianzar conocimientos sobre la administración pública, el derecho y lo referente al aparataje político y legislativo, que asegure mayor y mejor participación e incidencia de las mujeres y sectores LGBTI, en espacios de toma de decisiones, y gobernabilidad a nivel local, regional y nacional, es uno de los puntos iniciales para aterrizar el tema de la participación. ([Le puede interesar: Mujeres serán veedoras de la implementación de los acuerdos](https://archivo.contagioradio.com/mujeres-seran-veedoras-de-la-implementacion-de-los-acuerdos/))

Exigir a través de acciones legales y la movilización social, garantías efectivas de protección a los derechos humanos de lideresas y líderes comunitarios, que defienden la vida, la tierra y el territorio, como blindaje al derecho a la protesta, la oposición, la veeduría y el control a proyectos que afectan al grueso de la sociedad colombiana y en especial a la población vulnerada en el marco del conflicto armado.

### Conversaciones con el ELN, otra oportunidad para participar 

Que el acceso a los medios de producción económicos, de información y políticos sea una realidad para las mujeres y disidentes del género en Colombia, dicho ejercicio permitirá la transformación de políticas públicas que han promovido la desigualdad, la brecha entre clases, géneros y los monopolios, obstaculizando el acceso a la tierra y la consecución de condiciones de trabajo dignas.

Resaltaron la urgencia de iniciar un trabajo formativo transversal y coherente con las necesidades de cada territorio, que fomente la participación ciudadana, la apropiación de los acuerdos de La Habana y la posible incidencia en el nuevo escenario entre el ELN y el Gobierno. (Le puede interesar: [“Con más mujeres en la política Colombia sería un paraíso](https://archivo.contagioradio.com/mas-mujeres-en-la-politica-33857/)”)

Por último, uno de los aspectos más sobresalientes, resultado de la agenda y los lineamientos planteados, fue la veeduría ciudadana como mecanismo y posibilidad de participación efectiva de las mujeres y disidentes del género, en territorios donde ha sido evidente la ausencia estatal, la corrupción, amenazas, hostigamientos, estigmatizaciones, persecuciones y asesinatos por parte de la institucionalidad y estructuras paramilitares.

###### Reciba toda la información de Contagio Radio en [[su correo]
