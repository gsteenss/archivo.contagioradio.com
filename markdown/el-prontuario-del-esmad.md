Title: El prontuario del ESMAD
Date: 2016-08-03 17:23
Category: DDHH, Otra Mirada
Tags: Derechos Humanos, ESMAD, policia
Slug: el-prontuario-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/esmad-prontuario.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [03 Ago 2016]

Las denuncias contra el ESMAD han incrementado durante los últimos años. Videos y fotografías corroboran las agresiones que han sufrido jóvenes, adultos y niños por la fuerza desmedida que utilizan los integrantes de este escuadrón. Por esta razón se programó para este jueves 4 de agosto una jornada de control político en el Congreso de la República.

Según el  **Banco de Datos de Derechos Humanos y Violencia Política del CINEP** en la revista 100 días \# 80, entre 2002 y 2012, se presentaron 132 casos de presuntas ejecuciones extrajudiciales por abuso de autoridad atribuibles a efectivos de la Policía Nacional y durante el mismo periodo **fueron documentados 512 casos de detención arbitraria, 596 heridos y 73 casos de tortura**.

En una reciente intervención sobre el papel de las Fuerzas Militares de cara a la firma de un acuerdo de paz, el representante a la Cámara, **Alirio Uribe** afirmó que entre 2002 y 2014 se registraron 448 casos de **agresiones a civiles que al parecer fueron responsabilidad del Esmad, **y de estos casos, 137 son de personas heridas**, **91 detenciones, 107 amenazas, 13 ejecuciones extrajudiciales y dos casos de violencia sexual, entre otros.

Citando la misma fuente, Uribe señaló que las denuncias que se registran contra el grupo especial de la Policía fueron **interpuestas por 248 campesinos, 94 menores de edad, 50 mineros, 35 estudiantes, 21 periodistas, 17 líderes sociales, 15 obreros y 8 conductores** de transporte público o de carga. Adicionalmente afirmó que solamente en 2016 hay denuncias por 680 violaciones de Derechos Humanos y 6 [asesinatos atribuibles al ESMAD](https://archivo.contagioradio.com/?s=esmad+). Tras estas denuncias se han abierto cerca de 39 mil investigaciones pero hasta el momento ninguna tiene una sanción efectiva.

Respecto de los gastos del ESMAD, el CINEP realizó una investigación en la que encontró que en 10 años el gasto para el sostenimiento de ese cuerpo de policía ha costado más de 95.382 millones de pesos sin contar con el salario de los uniformados que ronda los 2 millones mensuales para un número mayor de 2.00o efectivos en todo el país.

[![esmad gastos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/esmad-gastos.jpg){.aligncenter .wp-image-27293 width="458" height="346"}](https://archivo.contagioradio.com/el-prontuario-del-esmad/esmad-gastos/)

### **El paro agrario de 2013, un ejemplo de la agresión contra la movilización social** 

Según la Comisión Nacional de Derechos Humanos de la Mesa Nacional Aagropecuaria y Popular de Interlocución y Acuerdos, durante los 20 días de protestas del Paro Agrario de 2013 se produjeron denuncias sobre **660 casos de violaciones a los derechos humanos individuales y colectivos, 262 detenciones arbitrarias, 485 heridos, 21 personas heridas con arma de fuego, 52 casos de hostigamientos y amenazas,  y 51 casos de ataques indiscriminados.**

### **13 asesinatos en los que estaría directamente implicado el ESMAD** 

El 8 de noviembre del 2001, **Carlos Giovanni Blanco**, estudiante de la Universidad Nacional fue  asesinado con arma corta en Bogotá, ningún agente del ESMAD presente en los disturbios de aquel 8 de noviembre del 2001 ha sido investigado.

El 21 de noviembre de 2002, el estudiante **Jaime Alfonso Acosta** de la Universidad Industrial de Santander, murió en medio de una protesta reprimida por el ESMAD. Varios estudiantes manifestaron que el disparo que impactó a Jaime en el tórax provino de los uniformados.

El primero de Mayo de 2005, **[Nicolas Neira](https://archivo.contagioradio.com/?s=nicolas+neira),** de quince años de edad, fue golpeado brutalmente por el ESMAD causando graves lesiones en su cráneo. Fue trasladado a un centro de atención médica, entró en coma y falleció a causa de las severas lesiones.

El 22 de septiembre de 2005, **[Jhonny Silva Aranguren](https://archivo.contagioradio.com/?s=jhonny+silva)**, estudiante de Química de la Universidad del Valle, fue asesinado por el ESMAD tras el ingreso de ese cuerpo policial a la institución académica. En medio de un corte de luz se genera una persecución en el campus. Mientras Jhonny intentaba huir, recibió un disparo en la nuca  que le quitó la vida. Aunque las primeras conclusiones de las autoridades forenses concluyeron que Silva fue asesinado por un agente del ESMAD sin identificar, el hecho continúa en la impunidad.

Años después del asesinato, la familia del estudiante denunció una serie de amenazas, hostigamientos y maltratos por parte de los mismos agentes de la policía y la fiscalía que archivó el caso.

El 10 de noviembre de 2005, **Belisario Camayo Guetoto**, indígena que protestaba por la recuperación de tierras en la hacienda “el Japio”, murió a causa de un disparo proveniente de la zona del ESMAD.

El 8 de marzo de 2006, **Óscar Leonardo Salas**, estudiante de la Universidad Distrital de Bogotá; murió por el disparo de una canica que se alojó en su cerebro y perforó uno de sus ojos. Un  agente del ESMAD declaró que uno de sus superiores -Mayor Rafael Mendez- autorizó utilizar toda clase de artefactos, como municiones recalzadas, cápsulas de gas reutilizados, rellenos de pólvora negra y metralla. El caso también sigue en la impunidad.

El 11 de abril de 2015, **Siberston [Guillermo Pavi Ramos](https://archivo.contagioradio.com/?s=Pavi+Ramos)** de 19 años de edad, fue asesinado por la fuerza pública en medio de las acciones de Recuperación de la madre tierra, en el departamento del Cauca. Siberston murió tras recibir dos impactos de bala sin que hubiese confrontación, y con armas especiales de asalto, según la denuncia el CRIC.

El 2 de junio de 2016  **[Gersai Ceron](https://archivo.contagioradio.com/?s=Gersai+Ceron), Wellington Quibarecama Nequirucama y Marco Aurelio Diaz,** fueron asesinados por el ESMAD en el cuarto día del Paro nacional agrario, en la zona de Las Mercedes en Cauca. Los integrantes del CRIC sostienen que los disparos provinieron de la zona en que las FFMM y de policía se apostaron para repeler la manifestación.

El 21 de abril de 2016, **[Miguel Angel Barbosa](https://archivo.contagioradio.com/?s=Miguel+Angel+Barbosa)** murió a causa del impacto de una granada de gas lacrimógeno lanzada por el ESMAD en medio de la represión a estudiantes de la Universidad Distrital en la sede tecnológica de Ciudad Bolívar. El artefacto golpeó directamente en la cabeza de Miguel Angel, causando así un trauma craneoencefálico provocando su muerte el 21 de junio. En medio de la investigación desaparecieron los videos de las cámaras que apuntaban hacia el lugar del los hechos.

La familia de Miguel se ha negado a emitir declaraciones; sin embargo, se conoció que durante los primeros días, luego de la agresión, recibieron amenazas por parte de desconocidos que los instaban a no denunciar y dejar de insistir en la búsqueda de justicia.

El 19 de mayo de 2016, **Brayan Mancilla,** niño de 12 años, muere al recibir un proyectil en su cabeza por parte del ESMAD. El niño transitaba por el lugar cuando se realizaba un desalojo forzado a una familia que habitaba el sector. Según el relato de los testigos, uno de los agentes disparó contra la multitud entre la que se encontraba Brayan.

Luego de impactarlo en la nuca, el mismo ESMAD ayudó en la evacuación; sin embargo, en lugar de asumir la responsabilidad por la muerte del menor, los mismo agentes intentaron encubrir el delito cambiando la versión y responsabilizando a la comunidad.

El 12 de junio de 2016 **[Naimen Lara](https://archivo.contagioradio.com/?s=Naimen+Lara) y un Mototaxista** fueron asesinados.  Los efectivos agredieron a quienes se estaban movilizando en contra del cierre del hospital de tercer nivel de Chiriguaná y dispararon contra el docente Naimen Lara pasando una de las motos sobre un mototaxista. Las dos personas murieron. La denuncia relata que mientras intentaban escapar, los efectivos hirieron con cuchillos a varios campesinos y a otros los golpearon con bolillos en la cabeza.

El 12 de julio de 2016, **[Luis Orlando Saiz](https://archivo.contagioradio.com/?s=Luis+Orlando+Saiz)**, constructor, murió tras recibir una granada propinada y dirigida hacia su rostro por el ESMAD en medio de las protestas del paro camionero en el departamento de Boyacá sobre la vía Tunja – Duitama. Aunque el dictamen de medicina legal encontró que la causa de la muerte fue una granada de gas del ESMAD, la policía** no asumió la responsabilidad directa por el asesinato** e intentaron justificar su muerte como un accidente.

<iframe src="http://co.ivoox.com/es/player_ej_12431488_2_1.html?data=kpehlZaYfJmhhpywj5WcaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxoqfpZC2qdHmxtjS0NnFstXZjMaYzsaPh4a3lIquk9LFtsKZk6iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
