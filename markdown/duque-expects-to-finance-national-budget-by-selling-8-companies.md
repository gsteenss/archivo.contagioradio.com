Title: Duque expects to finance national budget by selling 8 companies
Date: 2019-09-13 16:51
Author: CtgAdm
Category: English
Tags: budget, Education, ISAGEN
Slug: duque-expects-to-finance-national-budget-by-selling-8-companies
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Fotos-editadas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Congress’ economic subcommittees approved the Nation’s General Budget or PGN for 2020, approximately 271.1 billion pesos. Despite the budget’s approval by the majority of Congress, the opposition denounced a series of embellishment that has been included. In response, they plan to present a report that argues why the bill is inconvenient.]

### **PGN: A debate behind the country’s back**

[Senator Wilson Arias signaled that in the next national budget, it’s evident that the country is “paying the consequences of neoliberalism,” due to an unfavorable trade balance that depends, almost exclusively, on Colombia’s exportation of prime materials. While in the past, an increase in commodity prices (such as oil) created support for the policy, but now, with prices low, the true situation of the Colombian economy will become clear, Senator Arias said.]

[According to the congressman, this debate is occurring behind closed doors. “Now they’ve bought buildings next to the Ministry of Finance where congresspeople come together to discuss these topics,” the senator said. According to him, politicians belong to the opposition have decided not to attend these places and instead, are waiting until congressional sessions are opened to debate these issues.]

### **The largest budget for education?**

[After former President Juan Manuel Santos’ second term, it’s become popular to believe that financial resources for education take up the largest slice of the budget. But the senator affirmed that debt is actually the top priority of the budget, followed by education and defense spending. Senator Arias added that it would be worth it to study the budget allotted for education because an increase in investment is not expected.]

[According to the senator, the government decided to join the budget for “non-professional training,” that is for the National Training Service, and the budget for education. In total, there are 3.6 billion pesos that previously appeared in different items, but that now are grouped in with the educational budget. This allows for education to appear to have more investment than defense without any additional financial support from the government.]

### **Embellishments and gray areas  in the Nation’s General Budget**

[The senator declared that in the budget, there is an item for the disposal of assets that surpasses 8.5 pesos, that is, a source of financing for the State “appears to correspond with the sales to the public, or sales to the same State.” According to this analysis, the Duque administration would be attempting to buy companies from the National Fund for the Development of Infrastructure or FONDES and present them as if they were new resources.]

[In other words, the State is buying itself. There’s another detail: The FONDES was created thanks to the funds from the sale of ISAGEN, another company formerly in the hands of the State. Meanwhile, Senator Arias said that the government “has not even confessed what it wants to sell: they’ve given us a list of 150 companies, of which it seems those that are most susceptible to being sold are those from the energy sector.”]

### **"We expect a great social mobilization to defend the public patrimony"**

The following step to approve the budget bill will take place during the upcoming days: It's expected that reports for and against the bill will be discussed during the next 10 days and then the bill will be up for a vote. Before the vote, the opposition will present a report against the bill and is "expecting a great social mobilization that will defend the public patrimony."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
