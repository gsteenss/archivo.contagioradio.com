Title: ¿Cómo es posible que Alvaro Uribe nos siga revictimizando? Madres de Soacha
Date: 2016-05-23 15:54
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, madres de soacha
Slug: si-alvaro-uribe-va-a-hablar-de-nuestros-hijos-que-se-lave-bien-la-boca-maria-sanabria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/madres-de-soacha1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ElPaís 

###### [23 May 2016] 

El pasado 20 de mayo se llevo a cabo en la Corte Suprema de Justicia  la audiencia de conciliación entre el senador Alvaro Uribe Vélez y las Madres de Soacha, por una denuncia debido a un tuit publicado por el senador el pasado 25 de julio en donde **revictimiza los jóvenes ejecutados extrajudicialmente de Soacha**, en el tuit el senador dice que algunos de los hijos de las Madres de Soacha estaban involucrados con actividades delincuenciales.

“En reunión con las madres de Soacha varias me expresaron que sus hijos estaban infortunadamente involucrados en actividades ilegales, lo cual **no es excusa para asesinarlos, pero la hipótesis no fue examinada por la justicia”**

**María Sanabría**, madre de Estiven Valencia, víctima de ejecución extrajudicial, aseguró que ellas no están en la disposición de conciliar con el senador Uribe y que lo que buscan es que se acabe la impunidad luego de [8 años de haber emprendido la luch](https://archivo.contagioradio.com/madres-de-soacha-enfrentan-nuevo-golpe-tras-8-anos-de-impunidad/)a por la verdad en el caso de su hijo. Además, índica que **Senador es quien ha buscado la conciliación.**

"*Con qué derecho o con qué moral acusa a mi hijo de delincuente, **en el mandato de él fueron más de 5.800** ejecuciones extrajudiciales. Nos coloca a nosotras como unas mentirosas y si Alvaro Uribe va a hablar de nuestros que se lave bien la boca porque no son delincuentes. Por todo lo que ha sucedido:* ***la persecución, las amenazas y el seguimiento han hecho que mi familia se aleje de mi, pero no por esto voy a dejar de denuncia*r**" asevera María Sanabría.

Se espera que en los próximos meses se cite a una segunda audiencia de conciliación, sin embargo, María Sanabría dice que **el proceso de los falsos positivos se ha dilatado de diferentes formas** como lo es el cambio del Físcal que lleva el caso, y que por este motivo aún ni siquiera se conocen los nombres de los autores intelectuales de estos crímenes.

<iframe src="http://co.ivoox.com/es/player_ej_11636087_2_1.html?data=kpajlZuUfJihhpywj5WdaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKftMbbw8fWrcKfjpC6w8nWqdSfxcqYtdTFp8nVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
