Title: Con fútbol comunidad logra desalojar al ESMAD
Date: 2020-02-24 17:47
Author: CtgAdm
Category: DDHH
Tags: ESMAD
Slug: con-futbol-comunidad-logra-desalojar-al-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ESMAD.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto ESMAD: Justicia y Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En una inusual protesta, un grupo de personas logró desalojar al Escuadrón Móvil Antidisturbios (ESMAD) de un [parque público](https://twitter.com/Justiciaypazcol/status/1232036075239084032) que viene ocupando esa fuerza policial desde hace varios meses y con mayor presencia desde que se inició el Paro Nacional.  
  
Se trata del parque del Barrio El Recuerdo ubicado a unos 300 metros de la entrada de la Universidad Nacional por la calle 26 donde es usual que el ESMAD o la fuerza disponible de la Policía se ubique desde tempranas horas de la mañana y hasta entrada la noche. [(Le puede interesar: Ingreso de Fuerza Pública a las universidades escala la violencia)](https://archivo.contagioradio.com/ingreso-de-fuerza-publica-a-las-universidades-escala-la-violencia/)  
  
Los deportistas quisieron realizar un momento de juego en medio de sus actividades cotidianas cuando se encontraron con la sorpresa de que con escudos y cascos del ESMAD su parque había sido ocupado.  
  
Testigos afirman que, incluso esta fuerza de Policía ocupa el parque desde la mañana hasta la tarde y que allí realizan tanto maniobras de vigilancia del sector como entrenamiento e instrucción del personal. [(Lea también: Aplazan por tercera vez el debate de control político al ESMAD en Bogotá)](https://archivo.contagioradio.com/aplazan-por-tercera-vez-el-debate-de-control-politico-al-esmad-en-bogota/)  
  
Según vecinos del barrio, la presencia de esta fuerza de la Policía no solamente afecta la tranquilidad de uno de los barrios mas antiguos de Bogotá en días normales, sino que también se ven afectados por el uso desproporcionado de los gases lacrimógenos hasta el punto de tener que encerrarse en sus casas y recurrir al uso del tapabocas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es así como llegaron al parque del barrio y a punta de tiros al arco los vecinos del sector lograron recuperar su parque y tener unos minutos de esparcimiento ante la mirada de **cerca de 100 uniformados que tuvieron que retirarse ante la inusual pero efectiva protesta**. [(Le recomendamos leer: ESMAD no está capacitado para usar Escopeta calibre 12: Procuraduría)](https://archivo.contagioradio.com/esmad-no-esta-capacitado-para-usar-escopeta-calibre-12-procuraduria/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
