Title: Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino
Date: 2019-11-05 17:08
Author: CtgAdm
Category: Memoria, Nacional
Tags: Centro de Memoria Histórica, conflicto, Dario Acevedo, memoria
Slug: con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-e1572991495683.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SenadoGovCo  
] 

Este martes se adelantó en el Congreso de la República un debate de control político a Darío Acevedo, donde se cuestionó la dirección que le está dando al **Centro Nacional de Memoria Histórica (CNMH)**. Desde su llegada, organizaciones sociales y de víctimas advirtieron que Acevedo no era la persona idónea para estar al frente de esta institución por su visión negacionista del conflicto, ahora estas advertencias se tornaron en hechos que deberán ser explicados ante el legislativo.

### **La visión negacionista del conflicto** 

El **senador Antonio Sanguino, uno de los cuatro citantes al debate de control político**, explicó que como lo advirtieron las organizaciones sociales y expertos en temas de memoria, el cuestionamiento a Darío Acevedo no se deriva de su afinidad política ni su militancia, "sino por su opinión y mirada del conflicto colombiano". En ese sentido, resaltó que **Acevedo en diferentes ocasiones ha señalado que en Colombia no existió algo llamado conflicto armado, sino un ataque terrorista al Estado**.

Sanguino afirmó que no se puede nombrar como director de una institución encargada de la memoria, "y sobre todo memoria fundamentada en los derechos de las víctimas", a una persona que piensa que no existe el conflicto en Colombia. Hecho que se ha transformado en acciones que revelan esa postura negacionista por parte de Acevedo, "y que configurarían faltas disciplinarias a su obligación como funcionario de una institución que tiene un mandato legal derivado de la Ley 1448 de 2011, o Ley de Víctimas y de la Ley 975 de 2005, o Ley de Justicia y Paz".

### **"La memoria sobre el paramilitarismo en Colombia está en serio riesgo"** 

El Senador manifestó que su preocupación es que "la memoria sobre el paramilitarismo en Colombia está en serio riesgo con la dirección de este señor al frente del Centro de Memoria", porque 12 informes que se debían realizar con base en sistematizaciones de testimonios entregados por más de 13 mil exintegrantes del paramilitarismo (de los 18 mil que debían testificar) han sufrido un retroceso, "e incluso, se encuentran amenazadas esas verdades". (Le puede interesar: ["Ruben D. Acevedo nuevo director del Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/ruben-acevedo-es-el-nuevo-director-del-centro-nacional-de-memoria-historica/))

> [\#ProtejamosLaMemoria](https://twitter.com/hashtag/ProtejamosLaMemoria?src=hash&ref_src=twsrc%5Etfw)| En el [@CentroMemoriaH](https://twitter.com/CentroMemoriaH?ref_src=twsrc%5Etfw) existe la Dirección de Acuerdos para la Verdad, creada en proceso de paz con AUC y encargada de esclarecer el paramilitarismo. Desde que Darío Acevedo asumió como director redujo la contratación y no ha publicado los informes pendientes [pic.twitter.com/caWg3dciwd](https://t.co/caWg3dciwd)
>
> — Antonio Sanguino Senador ?? (@AntonioSanguino) [November 5, 2019](https://twitter.com/AntonioSanguino/status/1191789838388908032?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Adicionalmente, Sanguino sostuvo que Acevedo no quiso mantener el equipo que estaba desarrollando ese trabajo de investigación, y sometió a un proceso de revisión dichos documentos "para negarle al país y al mundo la memoria sobre el paramilitarismo". Todo ello, pese a que el proceso de construcción de este documento se hizo, según el Congresista, 'como debe hacerse': acudiendo a las víctimas y a los responsables en los territorios. (Le puede interesar: [" Buscar La memoria histórica del país no se puede crear desde la ambigüedad de D. Acevedo"](https://archivo.contagioradio.com/la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo/))

> 11\. [\#ProtejamosLaMemoria](https://twitter.com/hashtag/ProtejamosLaMemoria?src=hash&ref_src=twsrc%5Etfw) director Acevedo desconoce las memorias y relatos de las víctimas sobre el conflicto armado. Sus posturas son revictimizantes y se apartan del objetivo de propender a la reconciliación y establecer garantías de no repetición, como señala la Ley 1448 /2011 [pic.twitter.com/bJbTJBY7Eo](https://t.co/bJbTJBY7Eo)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [November 5, 2019](https://twitter.com/IvanCepedaCast/status/1191774345980514304?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En un audio que se difundió en medios de comunicación, se escuchaba a Acevedo planteando la reformulación de algunos de los productos del CNMH, lo que para Sanguino expresa "la actitud deliberada del señor Acevedo en relación con asuntos del museo de la memoria, la pretensión de volverlo **un museo que no haga referencia del conflicto armado".**  Esta situación cuestiona, a su vez, la forma en que se maneja la información en la Institución, "porque el país no puede permitir que la información que ha recopilado el Centro se pierda, se dilapide o se extravíe, y eso es una falta disciplinaria muy grave".

> El museo no tiene por qué aclararle a nadie, porque no es su función, cuál fue la naturaleza del conflicto y cual es la caracterización que se haga del mismo.

> Dario Acevedo en audio revelado.

### **¿Cómo proteger la memoria del conflicto?** 

El Senador de la Alianza Verde explicó que la memoria "es un consustancial en un proceso de transición de conflicto a la paz", de allí la importancia que el Centro de Memoria cuente con una persona idónea y sin cuestionamientos en su dirección. Por lo tanto, expresó que **el presidente Duque debe repensar la permanencia de Acevedo en el CNMH.** (Le puede interesar: ["La ruptura de confianza entre la víctimas y el Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/victimas-memoria-historica/))

Más allá de Acevedo, Sanguino también declaró que era necesario avanzar en la creación de una política pública de memoria histórica que establezca una arquitectura institucional, se articule de una mejor manera con el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), y que establezca un mecanismos de selección para el CNMH que garantice su autonomía, para que su trabajo de memoria "no esté sometido al vaivén de las opiniones políticas o de las decisiones ideológicas de quien ocupe el cargo".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44241107" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_44241107_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
