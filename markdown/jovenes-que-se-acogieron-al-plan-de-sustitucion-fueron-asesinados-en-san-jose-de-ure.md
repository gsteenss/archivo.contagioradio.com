Title: Jóvenes que se acogieron al plan de sustitución fueron asesinados en San José de Uré
Date: 2019-05-24 17:09
Author: CtgAdm
Category: Comunidad, Nacional
Tags: cordoba, Plan de Sustitución de Cultivos de Uso ilícito
Slug: jovenes-que-se-acogieron-al-plan-de-sustitucion-fueron-asesinados-en-san-jose-de-ure
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/jovenes-asesinados-en-san-jose-de-urre.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/comunicado-3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/comunicado-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/comunicado.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Asociación de Campesinos del sur de Córdoba 

El pasado 23 de mayo al menos 10 hombres del grupo armado Virgilio Peralta Arenas, o Caparros, llegaron al corregimiento Brazo izquierdo, en San José de Uré, allí amedrentaron a la población y sacaron de las casas a las familias. Una vez reunidos apartaron a **Jader Polo y Jader Pertuz**  y les dispararon frente a la comunidad, los jóvenes de 24 años eran beneficiarios del Plan Nacional Integral de Sustitución de Cultivos de uso ilícito y  hacían parte de la **Asociación de Campesinos del sur de Córdoba.  
**  
En horas de la tarde de este viernes, también fue confirmado el asesinato de Luis Fernándo Velásquez quien también era integrante del PNIS en el corregimiento de Batatalito a manos del mismo grupo armado.

\

> Condenamos y rechazamos el homicidio de nuestros compañeros Jader Polo y Jader Pertuz integrantes de [@ascsucor\_org](https://twitter.com/ascsucor_org?ref_src=twsrc%5Etfw) [@marchapatriota](https://twitter.com/marchapatriota?ref_src=twsrc%5Etfw) e inscritos en el PNIS.[@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) [@CancilleriaCol](https://twitter.com/CancilleriaCol?ref_src=twsrc%5Etfw) [@UNPColombia](https://twitter.com/UNPColombia?ref_src=twsrc%5Etfw) y cuando concertamos las medidas cautelares de [@CIDH](https://twitter.com/CIDH?ref_src=twsrc%5Etfw)?[\#Yason202](https://twitter.com/hashtag/Yason202?src=hash&ref_src=twsrc%5Etfw) vidas arrebatadas [pic.twitter.com/cDkjUojxAd](https://t.co/cDkjUojxAd)
>
> — Cristian Delgado (@CristianRDelgad) [24 de mayo de 2019](https://twitter.com/CristianRDelgad/status/1132037934268522497?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

 
