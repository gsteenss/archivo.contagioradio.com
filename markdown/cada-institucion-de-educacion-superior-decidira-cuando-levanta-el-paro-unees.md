Title: Cada Institución de Educación Superior decidirá cuándo levanta el paro: UNEES
Date: 2018-12-17 14:38
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, Movimiento estudiantil, Paro Nacional, UNEES
Slug: cada-institucion-de-educacion-superior-decidira-cuando-levanta-el-paro-unees
Status: published

###### [Foto: Contagio Radio] 

###### [17 Dic 2018] 

Con la firma del preacuerdo entre el Gobierno Nacional y el movimiento Estudiantil, una nueva tarea se viene para la comunidad académica, que durante las próximas semanas se reunirá en asambleas generales **para debatir si lo firmado corresponde a las necesidades de la Educación superior** o si por el contrario hacen falta medidas por parte del Estado para superar la crisis.

Si bien el movimiento estudiantil logró pactar 4.5 billones de pesos adicionales al presupuesto educativo, algunas representaciones estudiantiles, como la de la Universidad de Caldas, han manifestado que esta cifra no es suficiente y que por ende continuarán en paro. (Le puede interesar: ["Gobierno y estudiantes logran acuerdo en mesa de negociacion"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

Frente a estos hechos, Cristian Reyes, vocero de la UNEES explicó que el próximo encuentro nacional de vocerías se llevará a cabo los primeros días de enero y allí cada IES se pronunciará referente a si se acoge o no a lo firmado con el gobierno, de cara a levantar el paro Nacional.

"Vamos a empezar un semestre cargado de movilización, empezando las asambleas en cada universidad" afirmó Reyes y agregó que para el próximo año también se estaría programando la realización del Encuentro Nacional y Popular, que se está construyendo con otros sectores sociales como las centrales de trabajadores.

<iframe id="audio_30848968" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30848968_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
