Title: "Vamos a asumir responsabilidades": Timochenko
Date: 2015-09-30 14:29
Category: Nacional, Paz
Tags: carlos medina gallego, defensores de derechos humanos, diálogos en la Habana, ELN, FARC, jurisdicción especial para la paz, justicia, Mesa de conversaciones de paz de la habana, piedad cordoba, proceso de paz, Radio derechos Humanos, Timochenko
Slug: vamos-a-asumir-responsabilidades-timochenko
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Timo-Piedad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las2orillas.com 

<iframe src="http://www.ivoox.com/player_ek_8710556_2_1.html?data=mZyekpqZeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZS6hqifh6elusLh0NiYw5DFt9bhyteY1MrXtNDi1MbPy9HNqMLYxtiSlauJh5OZmqmSlaaPuMrh0MjVx9PPs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Medina Gallego, Analista UN] 

###### [30 Sep 2015] 

La entrevista entre Piedad Córdoba , defensora de derechos humanos; Timoleón Jiménez, Máximo comandante de las FARC y Patricia Villegas, directora del canal Telesur, despertaron distintas reacciones en el país alrededor de los inicios del proceso, el papel de del presidente Chávez, la democratización de los medios y el desmonte de paramilitarismo.

#### [**Paz por “convicción”:**] 

Timochenko inició con una intervención acerca de **lo importante que es la paz para el país**, indicando que es una cuestión de “*convicción, que no es problema de hombres, ni de personas sino de convicción*”, además que la única forma para alcanzar la paz es que la sociedad en su mayoría esté comprometida.

El jefe máximo de las FARC también indicó que la muerte de Alfonso Cano al inicio del proceso, en el año 2011, fue un fuerte golpe para toda la organización debido a que la **gestación y voluntad de diálogo él la había gestado** y con este ataque la posibilidad de diálogos se puso a prueba, sin embargo primó la voluntad de continuar las conversaciones para sentarse a dialogar con el gobierno.

#### [**Paramilitarismo:**] 

Piedad Córdoba compartió su preocupación por el paramilitarismo que ha vuelto a tomar fuerza en el país, a lo que Timochenko respondió indicando que las **conversaciones respecto a este tema también se están dando** y que en la subcomisión encabezada por Pablo Catatumbo y el General Naranjo, se ha “*trabajado fuertemente*”. **ver también**: [Fin del paramilitarismo](https://archivo.contagioradio.com/la-condicion-basica-de-no-repeticion-es-que-el-gobierno-enfrente-al-paramilitarismo/)

También aseguró que este problema es muy complejo y que l**a solución está en manos del Estado** “*el Estado colombiano siempre ha tenido la concepción de obtener el país a través de las armas*”.

Gallego afirma que en este proceso los responsables de la impunidad de procesos anteriores a paras tienen que ir cediendo para “*poder transitar un camino de acuerdos entre las partes que logre construir un escenario de paz estable y duradera*”

#### [**No pueden quedar semillas de nuevos conflictos**] 

Rodrigo Londoño aseguró que ve con optimismo que "*quienes apoyan el proceso cada día están creciendo más*”, pero llamó la atención porque “*hay que caminar con cuidado, ya que todavía no hemos firmado la paz*”, “*con la voluntad de ambas partes*” todo continuará por buen camino.

El jefe de las FARC le dijo a Piedad Córdoba que el proceso “***es un llamado a la esperanza***” y esta a su vez “*debe traducirse en la unión de voluntades*” y así “*no dejar semillas para nuevos conflictos*”.

Timochenko afirmó que todas las unidades de las FARC están dispuestas a dejar las armas a lo que el comandante indicó que “*ni un solo guerrillero fariano, ni combatiente, ni comandante está en desacuerdo*”.

Invitó a todos los partidos políticos y sectores a unirse al proceso, además que invitó a entes y otros países, como la CELAC y la UNASUR, para “*apoyar e implementar los acuerdos*”.

#### [**Democratización de los medios:**] 

Se refirió a la democratización de los medios en cuanto a que “*es necesario poder controvertir ideas distintas en los medios*”, lo cuál debe ser un objetivo de todos los sectores “*el objetivo común es generar un ambiente de paz*”, indicó el Jefe guerrillero.

Con el documento de justicia que se firmó la semana pasada en mano , Timochenko indicó  ante las las cámaras que lo que se debe hacer con el documento es “***difundirlo***”, ante la opinión pública. vea también: [Sistema integral de verdad justicia y reparación](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/)

El académico Carlos Gallego de la UN afirma que los **medios empresariales del país con sus cuestionamientos al proceso** y a lo que gira en torno a él “*crean un ambiente que no es favorable a la sociedad en su conjunto" *en la búsqueda de la paz, también indicó  Medina que los **medios alternativos** “*están comprometidos y han sido factores fundamentales de la construcción del imaginario de este país entorno a la paz*”.

Gallego indicó en su análisis, que esta entrevista muestra “l*a disposición que tiene la organización de las FARC en su conjunto, para sacar el proceso de paz adelante*”, además dijo sobre las declaraciones de Timochenko de “***no vamos a pedir perdón, lo que vamos asumir es responsabilidades***”,  que esto “*es totalmente válido, desde que asuman responsabilidades frente a lo ocurrido en el desarrollo de la guerra*”.

#### [**"Fase exploratoria con el ELN está cocinada"**] 

Por otra parte Gallego indicó que **estando a seis meses de la terminación de un conflicto, se suele estar en medio de dificultades** ya que “*todas las fuerzas están buscando acomodarse, para terminar de la mejor manera*”, sin embargo con la voluntad mostrada por el jefe guerrillero en la entrevista, estamos ad portas de concretar este proceso, el cuál tendrá que **coincidir en aspectos como el tema de justicia** el cual “*pueden compartir las dos organizaciones*”, además de los temas de rutas para la finalización del conflicto, pero en donde se respete la identidad del ELN.

Indicó que la agenda con esta organización ya "está prácticamente cerrada" y "el proceso de la etapa preliminar y cerrada en la fase exploratoria con el ELN está cocinada"  que hace falta es la mesa pública para darlos a conocer, lo cual se hará después del ***25 de octubre*** ya que el ELN no quiere que este proceso “*se instrumentalice políticamente*” y que no esté condicionado a lo que se defina con las FARC, pero se respete la identidad de esta organización.

\[embed\]https://www.youtube.com/watch?v=XYItNCog-OM\[/embed\]
