Title: Colombia se compromete internacionalmente a proteger lagunas de Otún y Sonso
Date: 2017-02-14 18:38
Category: Ambiente, Nacional, Voces de la Tierra
Tags: Humedales Colombia, Ramsar
Slug: 36306-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/laguna-de-otun.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [a2ruedasco]

###### [14 Feb 2017] 

Aunque el 26% del territorio colombiano está compuesto por humedales, varios de ellos se encuentran en amenaza, ya sea por la puesta en marcha de proyectos minero-energéticos, la construcción de vías o por el turismo irresponsable. Sin embargo, el país da algunos pasos significativos para proteger zonas ecológicamente importantes como **La laguna de Otún en Risaralda y la de Sonso en el Valle del Cauca.**

Ambas lagunas fueron **introducidas este fin de semana en la lista RAMSAR,** es decir que se obliga a ampliar la cooperación internacional y las acciones gubernamentales para garantizar su conservación, teniendo en cuenta que **son esenciales para el equilibrio ambiental frente a los nefastos impactos del cambio climático, como las inundaciones y sequías.**

El Ministro de Ambiente, Luis Gilberto Murillo asegura que con esta declaratoria, se “generan unos compromisos de parte del Gobierno Nacional, de la región y de la comunidad de poder conservar estos ecosistemas”.

Se trata de un decreto presidencial firmado el pasado sábado por el mandatario Juan Manuel Santos. Los ambientalistas esperan que se brinden verdaderas garantías a la vida de estas dos lagunas, pues recuerdan casos como el de la Ciénaga Grande de Santa Marta, que pese a estar incluida en la lista RAMSAR, hoy es el complejo de humedales más importante del país pero el más destruido. [(Le puede interesar: La última oportunidad de la Ciénaga Grande de Santa Marta)](https://archivo.contagioradio.com/visita-de-convencion-ramsar-la-ultima-oportunidad-de-la-cienaga-grande-de-santa-marta/)

### ¿Por qué son importantes la Laguna de Otún y la de Sonso? 

Según la Presidencia de la República, en la Laguna de Otún hay diferentes fuentes de agua que aportan a los ríos que abastecen acueductos municipales, tales como como el complejo de la cuenca alta del río Quindío. Desde allí **se garantiza el suministro de agua para aproximadamente 300 mil habitantes**, es decir el 56% de la población de Salento, Armenia, Circasia y La Tebaida. Además En esa zona hay 253 especies de aves, de las cuales 53 se encuentran dentro de alguna categoría de amenaza, 371 especies de plantas y 54 especies de mamíferos.

Por otra parte, la Laguna de Sonso es el humedal más extenso del Valle del Cauca. De acuerdo con datos de la presidencia, tiene un área de 2045 hectáreas, de las cuales 745 corresponden al espejo de agua y 1300 a la zona amortiguadora contra inundaciones. En este ecosistema conviven 217 especies de árboles, 179 especies de aves (de las cuales 55 son migratorias), 40 especies de peces y 26 especies de reptiles. Esta laguna **ha sido declarada como sitio de importancia para la observación de aves y patrimonio de los vallecaucanos.**

Hace un año, pobladores de la zona observaban que más del 50% de la laguna estaba seca. No solo a causa de los efectos del Fenómeno de El Niño, sino también por la construcción de un jarillón de 2,5 kilómetros de longitud y el desvío de un caño de drenaje natural que, a futuro, abastecería algunos cultivos de caña de azúcar.

De acuerdo con la presidencia, “actualmente, el Ministerio de Ambiente avanza en el proceso de **designar cuatro nuevos humedales Ramsar en el país. **Entre ellos están: la ciénaga de Ayapel, en Córdoba; los lagos de Tarapoto, en el Amazonas; el complejo de humedales del bajo Atrato, en el Chocó, y el río Vita, en el Vichada”.

Con la designación del séptimo sitio Ramsar en Colombia, se espera que **se fortalezcan los mecanismo para proteger un total de 710,000 hectáreas de humedales del país. **Como parte de un proceso de rehabilitación un chigüiro y una polla azul fueron liberados en la Laguna de Sonso.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
