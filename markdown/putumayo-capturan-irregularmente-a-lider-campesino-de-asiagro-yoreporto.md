Title: Putumayo: Capturan irregularmente a líder campesino de ASIAGRO #YoReporto
Date: 2015-04-01 18:37
Author: CtgAdm
Category: Comunidad, Nacional
Tags: 8 de marzo día de la mujer en la Perla Amazónica ZRZ del Putumayo, Captura irregular campesino ASIAGRO
Slug: putumayo-capturan-irregularmente-a-lider-campesino-de-asiagro-yoreporto
Status: published

###### Foto:Sincuento.com 

La Mesa Regional de Organizaciones Sociales y La comisión de Derechos Humanos;  Denunciamos ante la Comunidad Local, Departamental, Nacional e Internacional, mediante el presente documento,  que:

1.  El pasado 31 de Marzo siendo la 01:30 de la tarde, en el barrio las colinas del Municipio de Puerto Asís, se efectúa captura irregular del señor: **ANTÓN JEINS SOLARTE** identificado con cc \# 18.185638 habitante de la vereda San Ignacio, perteneciente a la organización **ASIAGRO**.
2.  2 personas (Hombre y Mujer) se presentaron como parte de la Fiscalía Municipal, se acercaron al compañero ANTÓN quien se encontraba en la casa del Señor Raúl Eduardo Zambrano (Compadre del detenido). Los Señores de la fiscalía le preguntan al compañero Antón por su nombre, paso seguido le solicitan el documento de identidad, luego de varios segundos; la Fiscalía le informa al compañero, que tiene **Orden de Captura** sin mostrarle el documento que demuestra lo dicho y de inmediato es trasladado al **Batallón de Ingenieros \# 27**, Kilómetro 5 del Municipio, sin permitirle comunicarse con su familia.
3.  Su **esposa madre de 2 Hijos**, el mayor de 13 años y el menor de 1 año; se comunicó inmediatamente con la Comisión de Derechos Humanos, en un momento de desesperación y miedo, ya que el en estos momentos se encontraba todavía en el batallón.

Informamos que el Compañero Capturado es Sobrino de la Compañera ELVIA SOLARTE Presidenta de **ANUC Puerto Asís** y Vocera de la Mesa Regional de Organizaciones Sociales, por lo tanto este hechos como todos los que hemos denunciado desde la Mesa Regional, hace parte de la Irregular acción de la Fuerza Pública en nuestra Región.

Agradecemos la puntual atención por parte de la **Defensoría Regional**, quienes desde el momento en que se informó están pendientes apoyando a la Familia y el Detenido, sin embargo es necesario que se busque de manera urgente por parte de cada uno de los Órganos de Control Departamental y Nacional, acciones Constitucionales que den solución definitiva a la no repetición de estos hechos.

Exigimos Veracidad y celeridad, por parte de las instituciones competentes, recalcando que hechos como estos son el pan nuestro de cada día de nuestros pobladores campesinos, **indígenas y afrodescendientes** y que sin olvidar la margen constitucional y labor de Jurisprudencia de cada entidad en este caso Fuerza Pública, debe prevalecer el Derechos  a la Libertad y el Debido Proceso.
