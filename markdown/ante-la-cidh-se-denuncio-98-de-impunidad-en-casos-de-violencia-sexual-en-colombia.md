Title: Ante la CIDH se denunció 98% de impunidad en casos de violencia sexual en Colombia
Date: 2015-10-22 12:35
Category: DDHH, Mujer, Nacional
Tags: audiencias CIDH, Casa de la Mujer, CIDH, Corporación Humanas, garantías de justicia, impunidad en casos de violencia sexual, La Habana, paramilitarismo en Colombia, periodos de sesiones CIDH, verdad y reparación, Violencia Sexual en colombia
Slug: ante-la-cidh-se-denuncio-98-de-impunidad-en-casos-de-violencia-sexual-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/CIDH.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CIDH 

###### [22 Oct 2015] 

Durante el periodo 156 de audiencias de la CIDH, organizaciones como la Corporación Humanas y Casa de la Mujer fueron las peticionarias para hablar sobre la atención a las niñas y mujeres víctimas de violencia sexual en Colombia. Allí, las abogadas representantes aseveraron que esta práctica es **constante, habitual y extendida** no sólo en escenarios de confrontación armada, afectando principalmente a **niñas entre los 10 y 14 años de edad**, sino también, en otros contextos rurales y urbanos.

De acuerdo con estudios adelantados por estas organizaciones, el **82% de las mujeres víctimas no denuncian, **y quienes lo hacen deben someterse a **procesos judiciales de más de 15 años** en los que la inoperancia del sistema judicial colombiano las revictimiza, teniendo en cuenta que el 23% de los funcionarios públicos considera que las mujeres son las principales responsables en la violación de sus derechos, lo que provoca cerca de **98% de impunidad**.

Entre **2012 y 2014** se registraron un total de **51.000 mujeres víctimas de violencia sexual** de las que el **40% no recibió atención en salud y** el porcentaje restante no fue atendido oportunamente ni con niveles de calidad.

Estas organizaciones aseguran que la actual **atención en salud** para estas víctimas **perpetúa** **estereotipos de género, asociados a la orientación sexual o a la pertenencia étnica**, siendo las mujeres indígenas, afrodescendientes, lesbianas y transexuales, las principales afectadas tanto por la vulneración de sus derechos sexuales como por la  baja calidad de la atención en salud.

Entre los principales victimarios se ubican paramilitares e integrantes de la fuerza pública, y con un más bajo porcentaje los guerrilleros, quienes han confesado la comisión de este delito.

Pese a que se reconocen los avances del sistema jurídico colombiano en esta materia, las organizaciones defensoras de los derechos de las mujeres denuncian el **gran déficit en la sanción de violaciones cometidas por integrantes de las Fuerzas Militares**, así como la impunidad subyacente en el proceso de Justicia y Paz, ya que **sólo 19 de las 33 sentencias contra paramilitares se refieren a violencia sexual**.

El registro de mujeres víctimas desde 1985 hasta el presente arroja un total de **12 mil víctimas**, estas corporaciones denuncian un amplio nivel de subregistro, principalmente por que la mayor parte de las víctimas no denuncia los casos.

Ante la falta de garantías de justicia, verdad y reparación por parte del Estado colombiano, tanto la Corporación Humanas como Casa de la Mujer, han articulado un plan de “5 claves” que han enviado a La Habana y presentaron ante la CIDH. A través de este, insisten en que la violencia sexual es un crimen que debe ser castigado, pues las mujeres tienen derecho a vivir una vida libre de todo tipo de violencias, de manera que debe pactarse que el **cese de la guerra sea tanto en los territorios como en los cuerpos femeninos**.

https://www.youtube.com/watch?v=l9C7ncFw\_AQ
