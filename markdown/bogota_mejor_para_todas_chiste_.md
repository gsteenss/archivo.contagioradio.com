Title: ¿Por qué una Bogotá incluyente no es un chiste?
Date: 2017-12-16 07:13
Category: Mujer, Nacional
Tags: Bogotá mejor para todos, Enrique Peñalosa, Sisma Mujer
Slug: bogota_mejor_para_todas_chiste_
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio - Luis Galindo] 

###### [14 Dic 2017] 

"Yo no me imaginé que esto fuera a generar semejante debate, me han enviado unos mensajes de apoyo y muchísimos ridiculizando mi acción, tras las declaraciones del alcalde", expresa Alirio Uribe, representante a la Cámara, sobre el fallo de un juez del Consejo de Estado, por el cual se le exige a la administración del alcalde Enrique Peñalosa, tener en cuenta el lenguaje incluyente en su eslogan, 'Bogotá mejor para todos'.

<iframe src="https://co.ivoox.com/es/player_ek_22685997_2_1.html?data=k5ejmpqdfZihhpywj5WaaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpDmjdHFt4zmxsbQxc7TssbnjNXc1JDJsIzawtHZ0ZDXs8PmxpDZx9PLucKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Básicamente lo que se exige mediante esta decisión es aplicar el Acuerdo 381 del 2009, establecido por el Concejo de Bogotá, por medio del cual se busca promover el uso del lenguaje incluyente, en el marco de la obligación que tiene Colombia por su adhesión a la **Convención** sobre la **Eliminación de Todas las Formas de Discriminación contra la Mujer, **tratado internacional de las Naciones Unidas firmado en 1979.

No obstante, pese a lo que han sido los compromisos de Colombia respecto a ese tema, y a la larga lista de leyes en torno a la promoción de los derechos de las mujeres que se han pactado en el Congreso de la República, hasta el momento poco o nada se ha materializado, y por eso la acción interpuesta por el representante, aunque Enrique Peñalosa, ha salido a burlarse en los micrófonos de los medios de comunicación ante el fallo del Consejo, y ha afirmado que apelará la decisión. "Es una posición autoritaria, machista y misógina, no lo quiere aplicar porque no le gusta", expresa el congresista Uribe Muñoz.

Por su parte, Claudia Mejía, directora de Sisma Mujer expresa que a las organizaciones de mujeres los argumentos de la alcaldía y demás "razones" que se han puesto sobre la mesa frente a este debate, las tienen "perplejas", pues son comentarios irrespetuosos, y según sus palabras **se trata de meros "chistes y expresiones de imbecilidad".**

<iframe src="https://co.ivoox.com/es/player_ek_22686034_2_1.html?data=k5ejmpuUd5Whhpywj5eUaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncaTgwtrRy8aPkcbehqigh6aopYampJDAy9jRpYzh1s_S1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En esa línea, explica que "hasta el momento los Estados se han centrado en políticas materiales, pero si no transformamos los imaginarios, nunca se enfrentará la cultura de discriminación". De acuerdo con Mejía, [la discriminación significa no ser escuchado, no existir, y por tanto, no tener los mismo derechos de todas las personas, y tanto la importancia de entender que **"lo que no se nombra, no existe".**]

### **El debate sobre la RAE** 

[La polémica, en muchos de los casos se ha centrado en las normas de la Real Academia de la Lengua Española (RAE). Incluso, una columna del escritor Héctor Abad Faciolince señala que le parece que en este debate hay un exceso de susceptibilidad de parte de algunas mujeres, y que, gramaticalmente es en palabras simples una pérdida de tiempo. ]

["Como el género, insisto, es un asunto gramatical y no sexual, hay una convención en varias lenguas occidentales (español, francés…) según la cual ante un número plural de personas, se usará, por economía verbal, el género masculino, lo cual no excluye a las integrantes de ese grupo específico que tengan sexo femenino", dice Abad, en su columna publicada en Revista Semana '**¿Colombianos y colombianas, ridículos y ridículas?'.**]

[Sin embargo, contrario al argumento del escritor, la directora de Sisma Mujer indica que en absoluto se trata de un debate que sea una pérdida de tiempo, entendiendo que "los seres humanos somos seres de lenguaje y eso habla de quienes somos y de lo que creemos como sociedad". Y sobre la polémica gramatical, pregunta: "¿]Quién conforma la Real Academia de la Lengua? personas que portan y defienden el estatus quo, ahora bien, el lenguaje va cambiando".

Según Mejía, si lo que preocupa es la economía del lenguaje**, ¿porqué no se feminiza el lenguaje?**.  No obstante, reconoce que para los hombres no se resulta fácil, y por tanto en lo que se puede trabajar es en un lenguaje en el que se tenga en cuenta tanto a hombres como a mujeres. En esa línea, no hablar de ciudadanos y ciudadanas, sino de ciudadanía; o no decir, "los invito y las invito", sino "les invito", por dar algunos ejemplos.

**"No proponemos una hiperutilización de todo. No estamos diciendo que se hable de microbios y microbias.** Entendemos la resistencia al cambio, y por ende el rechazo, pero se trata de ir construyendo", expresa Claudia Mejía, quien agrega que con las transformación del lenguaje es como se "empieza a quebrar un principio de la discriminación y nos reconocen la existencia como mujeres".

### **¿Sobrecostos por 'todos y todas'?** 

El fallo señala que se trata de un cambio que será progresivo, por tanto el argumento del alcalde sobre los miles de millones que costaría dicha decisión no sería cierto. "Cuando hice la demanda reflexioné en que esto no debería generar gasto, es decir que la papelería actual se sigue utilizando. Se trata de un fallo que se hace para inversiones futuras. No hay que echar  a la basura todos los materiales publicitarios que hay actualmente", explica el congresista Uribe Muñoz.

En línea con lo que asegura el representante, Mejía expresa: **"¿Y si costara qué? siempre cuesta el cumplimiento de políticas públicas **[**en contra de la discriminación.** El cumplimiento del Estado siempre cuesta dinero, pero en esta ocasión no es así".]

El debate sigue abierto, y por el momento, en medio de lo que ha significado la burla a la situación, las organizaciones de mujeres como el congresista, celebran que el juez 22 administrativo del circuito judicial de Bogotá, Luis Octavio Mora Bejarano, haya dado el visto bueno a la acción interpuesta por Alirio Uribe, y con esto se haya puesto sobre la mesa la importancia de fondo que tiene la implementación del lenguaje incluyente, en el marco de enfrentar la discriminación por el género.

###### Reciba toda la información de Contagio Radio en [[su correo]
