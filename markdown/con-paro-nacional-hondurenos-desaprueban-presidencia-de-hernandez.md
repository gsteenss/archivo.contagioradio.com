Title: Con paro nacional hondureños desaprueban presidencia de Hernández
Date: 2017-12-18 17:21
Category: El mundo, Movilización, Nacional
Tags: elecciones Honduras, honduras, Juan Orlando Hernández, Salvador Nasralla
Slug: con-paro-nacional-hondurenos-desaprueban-presidencia-de-hernandez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/0001234149.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [18 Dic 2018] 

[Ante la declaración del Tribunal Electoral de Honduras, en la que proclama como presidente electo a Juan Orlando Hernández, la Alianza de Oposición contra la dictadura llamó a paro general a partir de este lunes 18 de Diciembre, pese a la represión que ha sufrido la movilización social que ha dejado al menos 22  personas muertas.]

Este fin de semana la Misión de Obervación Electoral de la Unión Europea (MOE UE), anunció que** el conteo no presenta "ninguna diferencia" con los resultados publicados** por el Tribunal Supremo Electoral (TSE), que dejarían a Juan Orlando Hernández reelegido como presidente de Honduras.

"Tras el cotejo de una gran muestra aleatoria de las copias de las actas entregadas por la Alianza y de los originales publicados en la web del Tribunal, la Misión constató que los resultados de ambos grupos de actas son idénticas, no existe una diferencia de resultados", dijo **José Antonio de Gabriel, jefe adjunto de la MOE UE.**

De acuerdo con Rodolfo Pastor, vocero de la Alianza de Oposición contra la Dictadura, se trata de un anuncio del jefe adjunto, pero no de la entidad tomo tal. No obstante, se trata de una situación que tildan como "decepcionante", en medio de lo que han sido las múltiples denuncias de las irregularidades que si ha decidido evidenciar la** MOE OEA, en un detallado informe, y mediante el cual han solicitado que se realicen nuevas elecciones, como única vía de salida a esta crisis.**

### **No se ha completado el trámite legal del nombramiento de Hernández** 

Por otra parte, el Tribunal Superior Electoral declaró como ganador de las elecciones a Juan Orlando Hernández con el 42,95 por ciento de los votos. No obstante, estos resultados no sorprenden a la oposición ni al pueblo hondureño, pues como señala Pastor **se trata de una declaración "de hecho y no de derecho".**

Según explica el vocero de la Alianza, fue un anuncio hecho por el presidente magistrado del Tribunal, sin estar acompañado de los demás magistrados, lo que hace que no sea un anuncio oficial, así quiera presentarse de esa forma en los medios de comunicación. Asimismo, agrega que no reconocen ese anuncio del TSE, ya que es una entidad controlada por el gobierno de Hernández.

"Es una estrategia del régimen para que efectivamente se anuncie el triunfo de Hernández, pero legalmente no se ha anunciado", dice Pastor. Además, para que Juan Orlando sea declarado como presidente debe seguirse un protocolo legal que no se ha hecho, y por ende, **sigue sin aparecer su posesión en la gaceta oficial del TSE.** (Le puede interesar: [Honduras rechaza conteo de Tribunal Electoral)](https://archivo.contagioradio.com/honduras-muertos-fraude/)

### **Continúa la movilización social** 

En ese sentido, la movilización social se ha fortalecido, pese a las** 22 víctimas mortales de lo que ha sido la represión** por parte de las autoridades a las distintas manifestaciones. Desde este lunes la Alianza de Oposición contra la Dictadura ha convocado a la población para que se tome las principales vías del país. Aseguran que seguirán protestando en las calles hasta que se haga caso de la voluntad popular.

Por el momento, **hay una fuerte presión por parte de la OEA, el Departamento de Estado de los Estados Unidos y el pueblo hondureño** que sigue firme en rechazar y deslegitimar a Hernández como presidente.  "No estamos dispuestos a que se anuncie la victoria de Juan Orlando, cuando claramente la voluntad popular fue a favor de Salvador Nasralla. Nos mantenemos firmes en las calles defendiendo la dignidad del pueblo", concluye el vocero de la Alianza.

<iframe id="audio_22720372" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22720372_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
