Title: La absurda modalidad de negociar en medio de la guerra
Date: 2015-06-12 15:30
Category: Entrevistas, Paz
Tags: Bombardeo, Carlos Lozano, Cese al fuego, paz, Putumayo, Santos
Slug: la-absurda-modalidad-de-negociar-en-medio-de-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Colombia_drogas_WEB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ViveHoy 

#####  

<iframe src="http://www.ivoox.com/player_ek_4633517_2_1.html?data=lZuglZqVe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkMKfwsfg19fIpYzh0MnOzs7IpcWfxcqY0MrLs8TdwteYx9OPscbYytSYxsrQb8Tjz8vZy8jYs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Lozano, Semanario Voz] 

###### 

###### 

###### [12 jun 2015] 

"Todos los hechos de la guerra que están sucediendo son trágicos, y nos conmueven. Ojalá todo eso se pudiera detener", señala Carlos Lozano, director del Semanario Voz, ante la información que se ha conocido en las últimas semanas, en que el escalamiento del conflicto armado ha dejado decenas de víctimas fatales tanto en las filas guerrilleras como en las del las fuerzas militares, y desastres ambientales con impacto sobre civiles.

Para Lozano, este es el resultado previsible de que el gobierno haya decidido bombardear los campamentos guerrilleros, pues estos últimos, a su vez, incrementaron las acciones de guerra contra lo que consideran, son las fuentes de financiación de las fuerzas militares. "Tras la suspensión del cese al fuego unilateral, Santos dijo que el gobierno y los militares estaban preparados para lo que viniera, y lo que estamos viendo es que no estaban preparados, y que la guerra no exige una preparación minuciosa y arrogante, sino hechos políticos de paz, que es lo que no está ocurriendo", declaró Lozano.

El escalamiento del conflicto sólo puede, para Lozano, ser resultado de la absurda decisión del gobierno de dialogar en medio de la guerra: "Lamentablemente este es el conflicto: son actos de sabotaje, son actos de guerra, y a todos nos inquieta, pero es el resultado de semejante equivocación del gobierno nacional".

Sin embargo, para el periodista, estos hechos no serían motivo para suspender el proceso de paz, en tanto no ha sucedido lo mismo cuando los actos de guerra han afectado a la insurgencia, e insiste en la necesidad de que se acuerde un cese bilateral de hostilidades. "El proceso está maduro para un cese bilateral", concluyó Lozano.
