Title: Camioneros de Colombia se unen a Minga Nacional
Date: 2016-06-05 19:45
Category: Paro Nacional
Tags: Asociación Colombiana de Camioneros, Minga Nacional, Paro de Transportadores
Slug: camioneros-de-colombia-se-unen-a-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/camioneros-contagioradio-e1474909758381.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: noticiasdecolombia] 

###### [5 Jun 2016] 

Por lo menos **15 mil camioneros en Colombia** se unirán a las protestas que protagoniza la Cumbre Agraria en la Minga Nacional y que completan una semana y que han dejado ya 1[90 heridos y 152 detenidos](https://archivo.contagioradio.com/aumenta-a-152-el-numero-de-heridos-y-a-145-los-detenidos-en-la-minga-nacional/), algunos de ellos liberados en la noche de ayer. Con la presencia del gremio de los transportadores se sumará más presión al gobierno nacional para que atienda las exigencias de múltiples sectores.

Representantes de la Asociación Colombiana de Camionero ACC de Ipiales, Florencia, Pitalito, Valle del Cauca, Nariño, Cauca, El Eje Cafetero, Cundinamarca, Casanare, Meta, Antioquia, la Costa Caribe y los Santanderes confirmaron que **inician cese de actividades a partir de las cero horas del 6 de Junio.**

Se espera que con el gremio de los transportadores se refuerce la presión para que el gobierno nacional prohíba la represión violente de los manifestantes y el presidente Santos se siente con los [voceros de los campesinos](https://archivo.contagioradio.com/category/programas/paro-nacional/).

Dentro de las exigencias específicas del gremio de los camioneros se exige que el precio de los combustibles acorde a los demás indicadores del transporte de carga, el valor de los **fletes coherente con las exigencias de las carreteras del país y que no se favorezca el monopolio a través de esos cobros.**

Además exigen la no presentación de un proyecto del Ministerio de Transporte que reemplaza la orden de la chatarrización por el pago de una póliza. Según los camioneros **todos los acuerdos firmados con el gobierno del presidente Juan Manuel Santos han sido incumplidos** y por ello se ven en la necesidad de volver a las calles.
