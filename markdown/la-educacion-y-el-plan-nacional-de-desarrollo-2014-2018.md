Title: La educación y el Plan Nacional de Desarrollo 2014-2018
Date: 2015-05-04 14:05
Author: CtgAdm
Category: Educación, Nacional
Tags: educacion, fundacion para el desarrollo, ilich ortiz, Plan Nacional de Desarrollo, PND
Slug: la-educacion-y-el-plan-nacional-de-desarrollo-2014-2018
Status: published

¿Cuáles son los elementos más complicados del Plan Nacional de Desarrollo para la educación en Colombia?

Responde Ilich Ortíz, de la Fundación Escuela para el Desarrollo.

\[embed\]https://www.youtube.com/watch?v=3U1P7sp9xFo&feature=youtu.be\[/embed\]
