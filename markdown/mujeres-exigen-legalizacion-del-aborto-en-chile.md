Title: Mujeres exigen legalización del aborto en Chile
Date: 2016-11-15 15:34
Category: El mundo, Mujer
Tags: aborto en Chile, legalización del aborto, Tres causales de Aborto
Slug: mujeres-exigen-legalizacion-del-aborto-en-chile
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Humanas Chile] 

###### [15 Nov 2016 ] 

Chile, Nicaragua y El Salvador son los únicos tres países de América Latina que aún establecen una prohibición total del aborto. **El proyecto radicado en Marzo para la legalización del aborto en Chile obtuvo en el Congreso 66 votos a favor y 44 en contra y está a la espera de ser aprobado en el Senado. **Le puede interesar: [Organizaciones y activistas conmemoraron el día por la despenalización del aborto.](https://archivo.contagioradio.com/organizaciones-y-activistas-conmemoraron-el-dia-por-la-despenalizacion-del-aborto/)

Distintas organizaciones de mujeres defensoras de derechos sexuales y reproductivos afirman que **la actual legislación es herencia de la dictadura**, “vulnera los derechos de las mujeres y ahora observada con preocupación por organismos internacionales, puesto que **viola los derechos humanos de las mujeres y las niñas al imponer, bajo la amenaza del castigo penal, el embarazo”.**

### **La dictadura y el Aborto en Chile** 

El aborto estaba permitido en Chile hasta 1989, cuando el gobierno de Augusto** Pinochet lo penalizó en todas sus formas**. Durante 27 años las mujeres chilenas no han tenido opción a decidir sobre la interrupción voluntaria de embarazos **no deseados, que ponen en riesgo sus vidas, la del feto o son producto de abusos sexuales. **

Laura Albornoz, exministra del Servicio Nacional de la Mujer y abogada experta en equidad de género aseguro que en los últimos años el país recupero la democracia pero "**se olvidaron de las mujeres**, salvo para llevar adelante reformas que reforzaran la **división sexual del trabajo y los roles tradicionalmente realizados por nosotras".**

Albornoz resalta que las encuestas de opinión evidencian que cerca de un 70% de la población está de acuerdo en legislar al menos respecto de las tres causales. "Hemos debido pasar por un verdadero 'purgatorio', para aprobar leyes que confieran mayores **derechos civiles, políticos y de autonomía personal,** como en este caso, reconocer en la mujer el derecho a decidir practicarse un aborto en **tres situaciones tan excepcionales"**, concluyó la exministra.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
