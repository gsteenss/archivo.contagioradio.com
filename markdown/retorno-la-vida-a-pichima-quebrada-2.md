Title: Retornó la vida a Pichimá Quebrada
Date: 2019-12-16 17:37
Author: CtgAdm
Category: Especiales Contagio Radio
Tags: Comunidad, Desplazamiento, indígenas, Pichimá, pichima quebrada, retorno
Slug: retorno-la-vida-a-pichima-quebrada-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-1-.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-4.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-5.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-6.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-7.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-8.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-9.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0806.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0856.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0798.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0733.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0727.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0999.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0916.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-7-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0880.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-10.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-11.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-12.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-13.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-14.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-15.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-16.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-17.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-18.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-19.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-19-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-20.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-21.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-21-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-21-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-22.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-22-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-24.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-25.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0656.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-26.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/pichima-quebrada.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-29.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-27.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-30jpg.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-31jpg.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-32jpg.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-33.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-Portada.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-34-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-35.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-35-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-36.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-24-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-37.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-38.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### RETORNÓ LA VIDA A PICHIMÁ QUEBRADA

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/luis-galindo.png){width="70" height="70"}

</figure>
L. Gabriel Galindo
==================

Fotógrafo Contagio Radio

La comunidad indígena de **Pichimá Quebrada** vivió [5 meses y 29 días desplazada de su territorio](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/) debido a enfrentamientos entre actores armados. El municipio de Santa Genoveva de Docordó  fue su refugio hasta el 30 de noviembre, fecha que en la que decidieron retornar sin garantías.

Las más de **90 familias Wounaan** tomaron esta determinación ante la pasividad del Gobierno e instituciones como la Personería Municipal, Defensoría del Pueblo, Procuraduría y Ministerio de Defensa, en cabeza de la infantería de Marina,  y los constantes incumplimientos de la administración municipal, a cargo del  alcalde Willington Ibargüen.

Estas entidades desde octubre les aseguraron que habría una reunión en  la que se buscaría, a través de un  comité, abordar el retorno de la comunidad, pero esto nunca sucedió y se convirtió a su vez en una de las razones para regresar sin garantías.

Algunas de las difíciles situaciones que tuvieron que afrontar los integrantes del resguardo fueron la falta de asistencia médica, hacinamiento, escasez de alimentos, espacios no dignos para habitar y el incumplimiento de sentencias y tutelas que exigen velar por el bienestar de la comunidad.

Estos hechos llevaron a que el **30 de noviembre**, antes de partir en las embarcaciones hacia Pichimá Quebrada, la comunidad expresara a través de un comunicado que hacía responsable  al Estado de cualquier hecho que pusiera en riesgo la vida e integridad personal de los miembros del resguardo.

Hacia las 8:30 a.m.  la comunidad Nonaam  comenzó su travesía junto a organizaciones de derechos humanos, entre ellas la Comisión Intereclesial de Justicia y Paz, el Diálogo Intereclesial por la Paz (Dipaz), Katios y acompañantes internacionales desde el municipio de Docordó. Con ellos, banderas y pañoletas blancas, como signo de paz,  se unieron al desplazamiento por el río San Juan por cerca de tres horas.

Sobre las 11 a.m. empezaron a llegar las primeras embarcaciones al resguardo,  regresando el color y la vida a la comunidad. Desde su llegada y hasta entrada la noche,  el rostro de felicidad en los habitantes iluminó el resguardo. Las estufas se encendieron nuevamente y  a partir de esa noche cada familia pudo dormir en su casa.

**Pichima Quebrada**  significa para los Nonaam un nuevo inicio, ellas y ellos mantienen la esperanza y las ganas de trabajar, sin embargo, el temor no desaparece en las familias que esperan que el conflicto armado no los vuelva a afectar y que el Estado cumpla con las peticiones para salvaguardar la vida de este resguardo.

LOS PREPARATIVOS

<figure>
![PICHIMA QUEBRADA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0656.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-4.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-6.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-5.jpg)

</figure>
El 28 de noviembre en asamblea, la comunidad decidió retornar a Pichimá Quebrada, desde ese día iniciaron los preparativos de lo que seria su retorno. Antes de irse del pueblo dejaron limpias las casas que habitaron por cerca de seis meses.

**EN EL PUERTO**

En la mañana del 30 de noviembre las más de 90 familias llegaron al puerto de San Genoveva de Docordó con las pertenencias que tenían, junto a ellas llevaban los animales y el poco mercado que pudieron comprar.

<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-31jpg.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-12.jpg)

</figure>
<figure>
![\_GAB0727](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0727.jpg)

</figure>
<figure>
![\_GAB0798](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0798.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-7](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-7-1.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-13](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-13.jpg)

</figure>
VOLVIMOS AL RÍO

<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-11.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-16](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-16.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-34-scaled.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-15](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-15.jpg)

</figure>
<figure>
![\_GAB0856](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0856.jpg)

</figure>
Con banderas blancas partieron  las embarcaciones, que  durante tres horas navegaron por el río San Juan, afluente que ha sido testigo de la violencia que se vive en esta parte de Colombia, pero que en esta ocasión abría sus aguas para acoger nuevamente a las más 400 personas de la comunidad Wounaan.

REGRESAMOS

Hacia las 11 a.m empezaron a llegar los primero botes a Pichima Quebrada, lo primero que hicieron los más pequeños fue lanzarse al agua, mientras los mayores y los jóvenes subían a sus casas los enseres que traían.

<figure>
![Pichima-quebrada-retorno-26](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-26.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-38](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-38.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-17](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-17.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-19](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-19-1.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-32jpg](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-32jpg.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-35-1.jpg)

</figure>
EN CASA

<figure>
![Pichima-quebrada-retorno-30jpg](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-30jpg.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-24](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-24-1.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-27.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-36](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-36.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-33](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-33.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-37.jpg)

</figure>
<figure>
![Pichima-quebrada-retorno-22](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-22-1.jpg)

</figure>
<figure>
![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-quebrada-retorno-21-2.jpg)

</figure>
<figure>
![\_GAB0999](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0999.jpg)

</figure>
Ya en casa todas las familias empezaron a dar vida a sus hogares, la comunidad se reunió para limpiar las calles del pueblo. En la noche bajo el cielo estrellado se encontraron nuevamente para bailar y así festejar el retorno al hogar.

![Luego de cinco meses y 29 dìas la comunidad de Pichima Quebrada retorna a su territorio donde fueron desplazados de la violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/Pichima-quebrada-retorno-2-ohxjwibjryxtkuwfgve543yd3bek3kwn0cx2mrnelw.jpg "Retorno Pibhima Quebrada – Regreso a casa")

**Pichimá Quebrada, **es uno de los resguardos indígenas del pueblo Wounaan que ha habitado el Litoral de San Juan por más de 50 años, su territorio abarca una extensión de 9.024 hectáreas.

Este es un lugar estratégico ubicado a pocos minutos del río San Juan, afluente que durante décadas ha estado en disputa por ser un corredor crucial para los grupos armados que por allí transitan como disidencias de las FARC, grupos paramilitares y ELN.

Conozca más sobre la comunidad de Pichimá Quebrada
--------------------------------------------------

[![Retornó la vida a Pichimá Quebrada](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Pichima-Portada-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)

#### [Retornó la vida a Pichimá Quebrada](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)

RETORNÓ LA VIDA A PICHIMÁ QUEBRADA L. Gabriel GalindoFotógrafo Contagio Radio La comunidad indígena de Pichimá Quebrada vivió…  
[![Sin garantías del Estado, comunidad indígena de Pichimá Quebrada regresa a su territorio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pichimá-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/pichima-quebrada-retorno/)

#### [Sin garantías del Estado, comunidad indígena de Pichimá Quebrada regresa a su territorio](https://archivo.contagioradio.com/pichima-quebrada-retorno/)

La comunidade de Pichima Quebrada manifestó que hace responsable al Estado de cualquier hecho que ponga en riesgo…  
[![50 días de desplazamiento del resguardo Pichimá Quebrada](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB8780-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/50-dias-de-desplazamiento-del-resguardo-pichima-quebrada/)

#### [50 días de desplazamiento del resguardo Pichimá Quebrada](https://archivo.contagioradio.com/50-dias-de-desplazamiento-del-resguardo-pichima-quebrada/)

El resguardo Pichimá Quebrada lleva 50 días de desplazamiento en pésimas condiciones y sin garantías para el retorno…  
[![Pichima Quebrada, dos veces desterrada por la guerra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/PICHIMA-QUEBRADA-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

#### [Pichima Quebrada, dos veces desterrada por la guerra](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

PICHIMA QUEBRADA, DOS VECES DESTERRADA POR LA GUERRA   La comunidad indígena de Pichima Quebrada completa 30 días en…[Leer más](https://archivo.contagioradio.com/estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad/)
