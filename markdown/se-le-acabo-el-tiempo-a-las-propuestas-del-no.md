Title: Se le acabó el tiempo a las propuestas del No
Date: 2016-10-19 14:13
Category: Entrevistas, Paz
Tags: Alvaro Uribe, FARC, Gobierno, Iván Cepeda, Juan Manuel Santos, Juan Manuel Santos en Cuba, paz
Slug: se-le-acabo-el-tiempo-a-las-propuestas-del-no
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/CEpeda-No.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

###### 19 de Oct 2016

Después de la reciente alocución del presidente Juan Manuel Santos en la cual manifestó que existe **la posibilidad de usar algún recurso constitucional para destrabar la implementación de paz**, varios sectores y personalidades como el senador Iván Cepeda, se han pronunciado positivamente y han saludado el plazo impuesto por Santos para la recepción de propuestas por parte de los promotores del NO. Le puede interesar: [Gobierno y Farc deben buscar alternativas para implementar acuerdos](https://archivo.contagioradio.com/gobierno-y-farc-deben-buscar-alternativas-para-implementar-acuerdos/)

El Senador aseguró que es importante destacar el trabajo que se ha realizado desde las comisiones de paz, tanto del Gobierno como de la guerrilla de las Farc, para poder destrabar la implementación de los acuerdos de paz y manifestó que **es muy importante el mensaje enviado por el primer mandatario en el que fija un plazo para la consultas con los sectores del No** “contrario a lo que se especulaba el gobierno ha asumido la tarea de recepcionar todas las propuestas, los documentos e ir a negociar con quien se debe negociar…que son el Estado colombiano representado por su presidente y la guerrilla de las Farc”.

Frente a las propuestas recepcionadas y el proceso que se llevará a cabo en La Habana, Cepeda dejó claro que **las propuestas que se entregaron serán convertidas en un texto sobre el cual se dialogará entre las partes implicadas en el proceso**.

“Se abren distintos escenarios” así lo aseguro el Senador ante la posibilidad de pensar qué podrá hacer el presidente Juan Manuel Santos para poder implementar los acuerdos “**un nuevo plebiscito, la posibilidad de una refrendación por otras vías que indica la constitución  y que incluye mecanismos de participación ciudadana local y también la posibilidad de que sea a través del Congreso de la República**” y recordó “el presidente tiene la facultad de emprender negociaciones con los grupos armados insurgentes y tiene la posibilidad de firmar los acuerdos y de aplicarlos directamente. El plebiscito no es perentorio”.

Otro de los temas de los cuales se ha hablado, es la situación de “interinidad” en la que se encuentran cientos de guerrilleros mientras el acuerdo puede aplicarse. Al respecto, Cepeda manifestó que **se hace necesario actuar con celeridad,** aplicando los mecanismos necesarios y estudiando cuál será el mecanismo que deba usarse en el Congreso. Lea mas: [“Dilación puede poner en riesgo el proceso de paz”: Iván Cepeda](https://archivo.contagioradio.com/dilacion-puede-poner-en-riesgo-el-proceso-de-paz-ivan-cepeda/)

Ante la posibilidad de una reunión entre las Farc y el senador Álvaro Uribe para poder dialogar, Cepeda fue enfático en manifestar que el tiempo para eso ya pasó y que no es el momento para comenzar discusiones “**Este no es el momento, estuvo bueno ya de juegos. Aquí vinimos a lo que vinimos. Han habido 4 años de largas discusiones con el Centro Democrático. De propuestas, de ataques que se han hecho al proceso incluso de invitaciones que se le han hecho al senador Álvaro Uribe, las cuales fueron despreciadas**” y concluye diciendo que “no estamos para dilaciones, ya lo dijimos claramente y es la posición del gobierno”. Leer Columna de opinión: [La paz tiene su tiempo](https://archivo.contagioradio.com/la-paz-tiene-su-tiempo/)

Finalmente el senador Cepeda manifestó que debido al momento por el que pasa el país y el proceso de paz, es fundamental la movilización de la gente “es necesario que las movilizaciones se arrecien para acompañar los momentos definitivos como la instalación de la mesa con el ELN” y se mostró optimista frente a la reducción del apoyo que en un principio había recibido la campaña del No, según él debido a que muchas personas se dieron cuenta de las mentiras que hay detrás “**es necesario mostrar en qué consiste la paz y hasta qué punto están haciendo una campaña de engaños y manipulación psicológica los señores del Centro Democrático**” señaló.

<iframe id="audio_13389271" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13389271_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>
