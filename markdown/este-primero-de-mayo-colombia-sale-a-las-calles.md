Title: El Primero de Mayo Colombia sale a las Calles
Date: 2016-04-27 12:48
Category: Nacional
Tags: colombia, Movilización, Primero de Mayo
Slug: este-primero-de-mayo-colombia-sale-a-las-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/1-de-mayo-colombia-marchas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Abril 2016] 

Este Primero de Mayo se conmemora el día mundial del trabajó. En Colombia se llevarán a cabo diferentes movilizaciones nacionales que reivindicaran **los derechos de los trabajadores y visivilizaran las condiciones de persecución y estigmatización a los diferentes sindicatos y organizaciones sociales.** Sin embargo en Colombia estas no son las únicas razones para movilizarse.

Las movilizaciones se llevarán a cabo en diferentes ciudades como Cartagena, Cúcuta, Neiva, Medellín, Pereira, entre otras.

### **Medellín:** 

Habrán dos puntos de concentración: El Parque de los Deseos desde los 9:ooam y el Planetario , la marcha culminará en el Parque de las Luces, se espera que en la movilización participen las centrales obreras y sindicatos.

### **Neiva:** 

La concentración se hará en el Hospital Universitario Hernándo Moncaleano Perdomo, a partir de las 10:00 am y culminará en el Parque Leesburg o antiguo Parque del Amor, con un acto central a cargo del Partido Comunistas Colombiano, la Central Unitaria de Trabajadores  y la Unión Patriótica.

### **Barranquilla:** 

Punto de encuentro será en la carrera 14 con calle 47, desde las 9:00am, la marcha recorrerá la calle "La Cordialidad", posteriormente tomará la Av Murillo para culminar en el Polideportivo de "La Magdalena" donde se realizará un sancocho colectivo. Esta concentración esta convocada por el Frente Unido del Atlántico.

### **Cúcuta:** 

La concentración se llevará a cabo en la Plaza de las Banderas, desde las 8:00am, la movilización recorrerá el centro de la ciudad hasta la Redoma de Ventura Plaza, se espera que en esta marcha participen todos los sindicatos, organizaciones campesinas, cívicas y populares de Cucúta.

### **Pereira:** 

Diferentes sectores del movimiento social se encontrarán desde las 9:00 am en las instalaciones de Gaseosas Postobón en Dos Quebradas y finalizaran la movilización en la Plaza Simón Bolivar, donde culminarán con una jornada cultural.

### **Montería:** 

El punto de encuentro será la Plaza Roja, en el Barrio P5 y finalizará en el Centro Recreacional de Comfacor, lugar en donde se realizará un almuerzo comunitario.

### **Cartagena:** 

Dos movilizaciones, la primera será una marcha de antorchas el 28 de abril a las 6:00pm   que recorrerá el centro histórico de la ciudad amurallada, la segunda movilización se hará el primero de mayo, el punto de concentración será la Plaza de Bazurto, desde las 10:00am y culminará en la Plaza de la Paz en la Torre del Reloj, se espera que los demás municipios de cercanos a Cartagena se sumen a la movilización.

### **Boyacá:** 

Se realizarán a cabo movilizaciones en la ciudad de Sogamoso y Tunja. En Sogamoso el punto de encuentro será el Coliseo Alfonso Patiño Roselli a partir de las 9:00am y finalizará en la Plaza Villa del Sol.

### **Ibague:** 

La concentración se hará desde las 9:00am en la Casa del Maestro, ubicada en la Av 37 con la carrera 4 y culminará en la plazoleta de "Santa Librada" con una jornada político - cultural.

**Cali:**  
El punto de encuentro en esta ciudad será la autopista Sur con calle 27 en Comfandi del Prado, a las 8:00am, se espera que se movilicen los sectores que conforman el movimiento social, las centrales unitarias y los sindicatos.

### **Bogotá:** 

Se tendrán diferentes movilizaciones que iniciarán en : El Parque Nacional, la Plaza Eduardo Umaña y la Torre Colpatria serán los puntos de encuentro para la movilización que finalizará en la Plaza de Bolivar. En el sur de Bogotá el punto de encuentro será la Casa de la Cultura de Ciudad Bolivar, desde las 9:00am y finalizara en la Plaza de mercado La Aurora en Usme.

### **Suacha:** 

La movilización  se concentrará desde las 9:00am en el puente de la estación León XIII y finalizará en el Parque Principal. Esta marcha esta convocada por sectores del movimiento social como el Partido Comunista Colombiano, la Juventud Comunista, la Red Juvenil de Suacha, Sintracementerios, entre otros. Esta es la primera vez que este municipio conmemora la marcha del Primero de Mayo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
