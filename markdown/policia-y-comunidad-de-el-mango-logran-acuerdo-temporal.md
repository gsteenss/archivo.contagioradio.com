Title: Policía y comunidad de El Mango, Cauca logran acuerdo temporal
Date: 2015-06-30 13:09
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Argelia, Cauca, comunidad resiste, cordón humanitario, Defensoría del Pueblo, el mango, ESMAD, guerrilla, policia
Slug: policia-y-comunidad-de-el-mango-logran-acuerdo-temporal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11693065_10153294338975020_251830246_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4705808_2_1.html?data=lZydl52UfI6ZmKiakpqJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDgysiSpZiJhaXVjNXS1NLFssbXxpDOjdHTt4zVzdfSxsrIs9PZ1JDRx5CpsIzBwtPU0YqWh4y3wtrQw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Paola, vocera de la comunidad El Mango, Cauca] 

La tensión aumentó durante el fin de semana en el corregimiento "El Mango" en Argelia Cauca, debido a la presencia de la **policía y el ESMAD que llegaron al pueblo a tomarse las casas para “usarlas como trincheras”**, sin embargo, con la mediación de la Defensoría del Pueblo se acordó que los agentes de la policía permanecerían 15 días en un lote mientras se define un sitio permanente para instalar una nueva estación, a las afueras del corregimiento.

De acuerdo con lo manifestado por Paola, vocera de la comunidad, el momento **15 familias de El Mango han decidido desplazarse** por miedo a que haya enfrentamientos entre la guerrilla y la policía.

**Las mujeres son las que han liderado la situación** con el fin de evitar agresiones por parte del Escuadrón Móvil Antidisturbios ESMAD, teniendo en cuenta que generalmente son los hombres a quienes golpea la policía cuando aumenta la tensión.

Desde el sábado, helicópteros descargaron decenas de agentes que llegaron a la zona urbana, por lo que **la comunidad decidió crear un** **cordón humanitario** en la cancha de fútbol del pueblo, a manera de rechazo a la presencia de efectivos del ESMAD que estaban ubicados en las casas de los pobladores. Pero tras la presencia de la Defensoría del Pueblo, en este momento la población vive en un estado de relativa calma, **esperando que la Policía continúe cumpliendo con lo pactado.**
