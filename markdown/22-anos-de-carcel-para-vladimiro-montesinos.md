Title: 22 años de cárcel para Vladimiro Montesinos
Date: 2016-09-28 14:44
Category: El mundo, Judicial
Tags: Condena, Derechos Humanos, Fujimori, Montesinos, Perú
Slug: 22-anos-de-carcel-para-vladimiro-montesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/vv.source.prod_affiliate.84.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Nuevo Herald 

##### [28 Sep 2016] 

**Vladimiro Montesinos**, exasesor presidencial durante el gobierno Fujimori, fue condenado este martes a 22 años de prisión, por su participación en el secuestro y asesinato del profesor **Justiniano Najarro Rúa** y los estudiantes **Martín Roca Casas y Kenneth Anzualdo**, ocurrido en el año 1993.

La determinación tomada por un tribunal de Lima, se da tras comprobarse la existencia de un horno en los sótanos del **Servicio de Inteligencia del Ejército (SIE)**, ubicado en el distrito de San Borja, donde habrían sido asesinados y luego quemados los cuerpos de las víctimas. Un crimen tipificado como delito de lesa humanidad.

La Segunda Sala Penal Liquidadora, encargada del caso, argumenta que la vinculación del ex-asesor se da porque el Servicio de Inteligencia del Ejército, era un organismo que hacia parte del **Servicio de Inteligencia Nacional (SIN)** dirigido "en la sombra" por Montesinos, quien conocia de la existencia y funcionamiento de las mencionadas instalaciones.

Junto a Montesinos, fue condenado con la misma penalidad el excomandante del Ejército **Nicolas Hermoza** y el exjefe de Dirección de Inteligencia del Ejército (Dinte) **Jorge Nadal Paiva**, quien deberá permanecer 15 años en prisión.

En 2009, la Corte Interamericana de Derechos Humanos, CIDH, determinó la **culpabilidad del Estado peruano** en la desaparición de Kenneth Anzualdo y ordenó indemnizar a sus familiares y fijar una placa conmemorativa como desagravio, la cual fue instalada en 2013. Familiares de los estudiantes manifestaron estar conformes con la decisión judicial y solicitaron continuar con la búsqueda de los restos de sus seres queridos.

La presente condena se suma a las que ya cumple Montesinos por delitos de **corrupción y violación de Derechos Humanos**; una de 20 años por su participación en un intercambio ilegal de armas y otra de 15 años por cargos de corrupción.
