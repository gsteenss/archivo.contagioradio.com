Title: Paramilitares asesinan a líder indígena en Medio Baudó
Date: 2017-10-07 22:06
Category: DDHH, Nacional
Tags: comunidades indígenas, Medio Baudó
Slug: paramilitares-asesinan-delante-de-la-comunidad-a-lider-indigena-ezquivel-manyoma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ezquivel-Manyoma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

###### 7 Oct 2017 

La Mesa de Concertación de los Pueblos Indígenas del Chocó, denunció el asesinato de **Ezquivel Manyoma**, gobernador y promotor de salud del Pueblo Embera Dóbida del Resguardo Dabeiba Queracito del municipio del Medio Baudó.

**Ezequiel** fue raptado de su casa el día 6 de octubre, posteriormente amarrado al Cepo y asesinado en la mañana del 7 de octubre,  por personas que la comunidad identificó como paramilitares.

Para la comunidad el asesinato se da en un contexto en que grupos paramilitares están llegando al territorio e irrespetando la autonomía de los pueblos indígenas, igualmente señalan que estas acciones  han provocado el desplazamiento de varias comunidades entre ellas la de Queracito, que desde hace cerca de un mes se resguardaban en otro territorio y preparaban su retorno.

En el comunicado la organización señala "No solo acaban con la vida de un ser humano, arrancan de nuestro territorio, la sabiduría de un líder, conocimientos ancestrales, que no se volverán a recuperar. Los grupos armados, desequilibran la armonía de nuestras Comunidades, a pesar de ello, seguimos anhelando que la tranquilidad llegue a nuestros territorios"

Según una conversación del diario el Tiempo con Luis Enrique Murillo, defensor del Pueblo de la Regional Chocó, el funcionario explicó que la comunidad donde ocurrió el homicidio tiene 63 familias y que desde septiembre estaban desplazadas y se resguardaban en la comunidad afro Querá.

### **Paramilitares controlan territorios indígenas y de comunidades negras** 

Esta misma situación la afrontan comunidades habitantes del Bajo San Juan que desde hace cerca de un año sufren desplazamientos intermitentes en los que deben abandonar sus fincas y las pocas siembras para resguardar sus vidas en comunidades vecinas durante un periodo de tiempo indeterminado. **Ver: [Un mes y medio completan 400 familias desplazadas del San Juan del Litoral](https://archivo.contagioradio.com/un-mes-y-medio-completan-400-familias-desplazadas-del-san-juan-del-litoral/)**

Para las comunidades indígenas y negras del Chocó, la ausencia estatal en materia de atención en salud, vías, educación, incluso presencia de organismos de control como la Personería o la Defensoría del Pueblo  son insuficientes para atender las necesidades y las exigencias de las comunidades indígenas y negras en el departamento.

Adicionalmente la acción de la Fuerza Pública es practicamente nula, ya que a pesar de que muchas de las denuncias contienen lugares de campamentos militares, puntos de control, rutinas entre otros datos, la acción de las Fuerzas Militares no es efectiva contra grupos paramilitares y narcotraficantes que controlan el sector.
