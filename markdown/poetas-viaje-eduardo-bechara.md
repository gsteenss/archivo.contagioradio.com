Title: El viaje de un escritor colombiano por suramérica en busca de poetas
Date: 2018-02-22 14:59
Category: Viaje Literario
Tags: Latinoamérica, poetas
Slug: poetas-viaje-eduardo-bechara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/En-busca-de-poetas-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Eduardo Bechara Navratilova 

###### 22 Feb 2018 

El escritor Colombiano Eduardo Bechara Navratilova, quien en el 2013 inicio un recorrido de más de 26 mil kilómetros a través del continente suramericano, desde Ushuaia (Argentina) hasta el Cavo de La Vela (Colombia), con el objetivo de descubrir poetas, documentarlos y fomentar la lectura de este género literario, presenta el primer tomo de la **Antología Poética** resultado de la primera etapa de este viaje por la Patagonia:  **Breve Tratado Del Viento Del Sur.**

“Este es un libro cuyo criterio de selección está dictado por el azar, el viaje y la locura. Eduardo Bechara Navratilova, - quien es una especie de detective salvaje -, al peor estilo de los viajeros europeos, con poco presupuesto, pero con mucha imaginación, recorre ciudad por ciudad del sur de Argentina En Busca de Poetas. Encuentra – no descubre- para asombro de él y de los que viajamos a través de estas páginas, una de las estéticas más atractivas y extraña del continente”, afirma sobre la antología, el poeta colombiano radicado en Nueva York, Fredy Yezzed.

**Breve Tratado del Viento del Sur, **lleva al lector a través de una poesía donde irrumpe el viento y el frio, elementos que moldean a cada personaje que la escribe.  Como afirma Eduardo “La crudeza del clima se traslada al interior del poeta. A su mundo”. Y lo complementa Yezzed “Quizá como personaje principal de esta literatura, para hablar de la condición humana en uno de los paisajes más inhóspitos y hermosos del mundo”. * *Y es que, en esta antología, es imposible desasociar la obra del escritor, de su vida y su ambiente, así como la experiencia vivida para recopilar el material que aparecen en estas páginas por parte del escritor colombiano.

Los métodos de búsqueda variaron de una ciudad a otra. “Los poetas más representativos del sitio me llevaron a los inéditos. Si no conocía a nadie, me dirigía a la Casa de Cultura o a las bibliotecas. Ahí me ayudaban a entrar en contacto con ellos”*,* comenta Eduardo. Pero en general, fue a través de los poetas conocidos que logro “unir un eslabón con otro a fin de construir la cadena” que se convierte hoy, en la primera entrega de esta Antología.

El Lector que decida compartir este viaje poético con Eduardo, descubrirá otra Argentina, otra forma de soledad, otro idioma para nombrar la belleza. La Antología estará disponible en la librería Nacional, Lerner y Wilborada entre otras, un viaje que cubre la Patagonia Argentina y convoca a 90 poetas de diferentes generaciones, inéditos y publicados que viven o viajan por esta geografía.

El lanzamiento se realizará este 22 de febrero en el Gimnasio Moderno a las 7:30 p.m. con la presentación del poeta colombiano Federico Diaz Granados y el poeta chileno Enrique Winter.

<iframe id="audio_23978314" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23978314_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
