Title: Feminicidios estarían directamente relacionados con multinacionales
Date: 2016-04-27 15:25
Category: Mujer, Nacional
Tags: buenaventura, feminicidio, feminicidios colombia
Slug: feminicidios-estarian-directamente-relacionados-con-multinacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/FEMINICIDIOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: mientrastantoenmexico] 

###### [27 Abril 2016]

Según Medicina Legal durante 2014 se presentaron 1.007 feminicidios, **119 en el Valle del Cauca, 118 en Bogotá, 114 en Antioquía, 54 en Cundinamarca y 49 en Meta**, por citar los departamentos más críticos. De acuerdo con la entidad esta práctica es la expresión más extrema de violencia contra la mujer, diversas organizaciones la relacionan directamente con el machismo y el patriarcado, y hay quienes la vinculan con la acumulación de capital.

A este último grupo pertenecen las mujeres que hacen parte de 'Otras Negras y Feministas' 'El Chontaduro Casa Cultural' y el 'Colectivo Sentipensar Afrodiásporico' quienes de forma autogestionada convocaron a la italiana Silvia Federeci, a la guatemalteca Aura Estela Cumes, a la africana Patricia Godinho, a la iraní Shahrzard Mojab, a la boliviana Helen Álvarez y a la argentina Rita Segato, para liderar el 'Foro Internacional sobre Feminicidios en Grupos Étnicos Racializados Asesinato de Mujeres y Acumulación Global'.

Encuentro del que también han hecho parte por lo menos 200 mujeres que desde Cauca, Chocó y Nariño han viajado a la ciudad de Buenaventura para discutir la **relación entre los feminicidios y la acumulación de capital**, que se ha hecho evidente no sólo en este municipio, sino en distintas urbes del mundo y con diversos sectores étnicos.

"La acumulación de capital es un factor determinante en el feminicidio en grupos étnicos racializados. En ciudades como Buenaventura la relación es evidente. Quienes siembran el terror y apoyan a grupos alzados en armas son precisamente quienes hoy manejan las instituciones del Estado", así como **las multinacionales que usan el asesinato de las mujeres para generar los desplazamientos que les permiten entrar a los territorios**,** **afirma Marta Rivas, una de las asistentes al foro.

Varias de las discusiones durante el foro han girado en torno al cómo la **interiorización de prácticas heredadas del colonialismo y ancladas al patriarcado**, han sido caldo de cultivo para las violencias perpetradas contra las mujeres en distintas partes del mundo, pese a que en cada urbe se exprese de forma distinta. Las demás reflexiones producto de este foro serán socializadas a través de esta [[Fan Page](http://bit.ly/1WqtOTO)].

<iframe src="http://co.ivoox.com/es/player_ej_11329961_2_1.html?data=kpaglJ6depKhhpywj5aUaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nOjbfNusLnjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
