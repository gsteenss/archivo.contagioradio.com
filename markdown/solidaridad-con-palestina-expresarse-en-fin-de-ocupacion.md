Title: Solidaridad con Palestina debe expresarse en el fin de la ocupación
Date: 2016-11-29 16:09
Category: El mundo
Tags: Invasión, Israel, Palestina, Solidaridad con Palestina
Slug: solidaridad-con-palestina-expresarse-en-fin-de-ocupacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Palestina1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre ] 

###### [29 Nov. 2016] 

*“**Gaza sigue viviendo una situación de emergencia humanitaria; hay 2 millones de palestinos afectados por el desmoronamiento de las infraestructuras y la parálisis de la economía, y decenas de miles que siguen desplazados”**,* de esta manera Ban Ki-Moon, secretario general de la ONU se refirió al **Día de la Solidaridad con el pueblo Palestino** que se conmemora este martes en todo el mundo.

En 1977 la Asamblea General de la Organización de las Naciones Unidas (ONU) decretó como Día Internacional de la Solidaridad con el Pueblo Palestino el 29 de Noviembre. Día en el que entre otros temas, se **rinde un homenaje y se honra a los palestinos y palestinas que han sido expulsados de sus tierras por Israel.**

El año pasado, en un acto simbólico, la bandera de Palestina se izó a media asta, haciendo parte así de todas las demás que se erigen en este recinto en Nueva York. Le puede interesar: [Más de 100 organizaciones se solidarizan con palestina en la ‘semana contra el apartheid israelí’.](https://archivo.contagioradio.com/mas-de-100-organizaciones-se-solidarizan-con-palestina-en-la-semana-contra-el-apartheid-israeli/)

En todo el mundo diversas organizaciones continúan realizando actividades que pretenden **hacer un llamado a los diversos países, y a la sociedad en general a exigir que no continúe la ocupación  de Gaza y Cisjordania por parte de Israel.**

**Hasta el momento, Israel ha seguido con su política de ocupación y de limpieza étnica contra el pueblo palestino** haciéndolo a través de asesinatos, arrestos, bloqueos, devastación de cultivos, destrucción de viviendas, la construcción de un muro separatista y el intento de la judaización de Jerusalén. Le puede interesar: [Una mirada a Palestina desde el cine](https://archivo.contagioradio.com/una-mirada-a-palestina-desde-el-cine/)

Según cifras presentadas por la Agencia de Naciones Unidas para los Refugiados de Palestina en Oriente Próximo (UNRWA, por sus siglas en inglés), **la ocupación israelí ha dejado ya más de 5 millones de refugiados palestinos, siendo así el grupo poblacional del mundo que más tiempo ha durado como refugiado.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
