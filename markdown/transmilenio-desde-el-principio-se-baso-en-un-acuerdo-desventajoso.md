Title: "Transmilenio desde el principio se basó en un acuerdo desventajoso"
Date: 2015-10-16 13:14
Category: Economía, Nacional
Tags: Ganancias para los privados en Transmilenio, José Stalin Rojas, Movilidad y Territorio de la Universidad Nacional, Observatorio de Logística, Transmilenio
Slug: transmilenio-desde-el-principio-se-baso-en-un-acuerdo-desventajoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Transmi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tapioca na Colômbia] 

<iframe src="http://www.ivoox.com/player_ek_9025180_2_1.html?data=mpWfl5acdI6ZmKiak52Jd6KolZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjMjOxsaPtMLnws%2FSjcnJb7XmwtPgz87Qqc%2Fd0JCSlJ2JdpWlj52dkoqWfYzZzZCxy9jYtsro0JDgx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Stalin Rojas, Universidad Nacional] 

###### [16 Oct 2015]

[Transmilenio, el sistema de transporte masivo capitalino implementado hace cerca de 15 años durante la administración de Enrique Peñalosa, actualmente es el negocio en el que el Distrito invierte gran parte de su presupuesto, pero del cual obtiene ganancias casi nulas. Por cada **\$100** que **entran al sistema**, **\$90** **llegan a manos de los dueños de los articulados, \$5 a los recaudadores y solo \$5 obtiene el Distrito**.]

[José Stalin Rojas, director del Observatorio de Logística, Movilidad y Territorio de la Universidad Nacional OLMT, asegura que el modelo de operación de **Transmilenio desde el principio se basó en un acuerdo desventajoso**]**[, en el que las ganancias son para los privados.]**[ "El Distrito coloca mucho pero recibe poco", lo que implica un mayor esfuerzo de su parte para mantener la infraestructura y se asocia al conjunto de problemáticas que actualmente enfrenta el sistema.  ]

[Estos **problemas**, de acuerdo con Rojas, tienen que ver con **3 factores principalmente: infraestructura, aumento poblacional y crecimiento del parque automotor**. El plan de expansión con el que inició la construcción de Transmilenio dejó de aplicarse desde la administración de Luis Eduardo Garzón, el sistema no se modificó frente a los cambios en las dinámicas poblacionales de la ciudad y el parque automotor ha aumentado significativamente.]

["Transmilenio no ha sido el principal objetivo de las administraciones, se desarrolló la infraestructura pero no a los niveles que se necesitaban y **no se avanzó en cultura ciudadana**", agrega Rojas.]

[Para el experto en movilidad, dado que las inversiones con entes privados exigen ganancias, es necesario pensar asociaciones mixtas para la prestación de servicios, que permitan **ganancias iguales** para las partes. Para ello, los servicios deben ser de calidad para satisfacer las necesidades de los usuarios, **"Deben existir indicadores en torno al margen de ganancia y a la calidad del servicio"**, concluye Rojas.]
