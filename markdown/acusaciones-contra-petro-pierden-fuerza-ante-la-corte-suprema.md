Title: Acusaciones contra Petro pierden fuerza ante la Corte Suprema
Date: 2018-12-19 13:30
Author: AdminContagio
Category: Nacional, Política
Tags: Colombia Humana, Gustavo Petro
Slug: acusaciones-contra-petro-pierden-fuerza-ante-la-corte-suprema
Status: published

###### Foto: 

###### 19 Dic 2018 

El pasado martes, Juan Carlos Montes, excontratista del Distrito rindió declaración ante la Corte Suprema de Justicia para responder sobre el video en el que se ve al senador Gustavo Petro recibir varios fajos de billetes. A  pesar que se desconoce el testimonio de Montes, la defensa del senador afirma que ha quedado claro que dicho dinero no tiene un origen ilícito.

Daniel Prado, abogado de Petro indicó que con cinco declaraciones rendidas ante la Corte de distintas personas vinculadas al video, además del comunicado hecho por el **‘Loco Barrera’**, en el que desmiente haber financiado la campaña del senador, tal como lo afirmo el abogado **Abelardo de La Espriella**, ha quedado claro que el dinero no tiene una procedencia ilícita y que al recibirlo no se estaría cometiendo ningún delito.

El abogado además, aclaró que el video en el que Petro recibió la suma de **20 millones de pesos** destinados a gestionar su campaña electoral, habría sido grabado en el **2005** y no en el 2009 como se ha hecho creer, información que para el abogado  tiene la intención de "hacerle daño  a a imagen política" del senador de la Colombia Humana.

### **Abelardo de la Espriella, ausente ** 

Se espera que para el próximo **21 de enero**, la **Corte Suprema de Justicia siga escuchando los testimonios de personas involucradas **con la publicación del video, por tanto no se descarta que sean llamados a declarar a Abelardo de la Espriella quien ya ha sido citado y aún no ha comparecido o la senadora del Centro Democrático, Paloma Valencia.

### **El objetivo es dejar sin recursos a la Colombia Humana ** 

Por su parte, el abogado Prado también declaró que las investigaciones que se adelantan contra Blanca Durán, gerente de la campaña de Colombia Humana son paralelas al denominado "Petrovideo" pero que tienen como fin dejar sin financiamiento al movimiento para las próximas contiendas electorales y afectar a quienes apoyaron económicamente a la Colombia Humana. [(Le puede interesar Cuatro acciones con las que se quiere atacar a la Colombia Humana)](https://archivo.contagioradio.com/atacar-colombia-humana/)

<iframe id="audio_30910571" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30910571_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
