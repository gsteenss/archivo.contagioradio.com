Title: De 1994 a 2018 tejiendo defensa territorial
Date: 2018-09-28 07:00
Author: AdminContagio
Category: CENSAT, Opinion
Tags: Asprocig, Rios Vivos
Slug: de-1994-a-2018-tejiendo-defensa-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/67920077_2249258751790002_2861589769903669248_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carlos Amaya 

##### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

##### 28 Sep 2018 

#### [**Cronología del proceso de construcción de Ríos Vivos.**] 

[En octubre de 1994, alrededor de mil indígenas embera katíos navegaron en 42 balsas desde el Resguardo Karagaví en la cuenca alta del río Sinú hasta Lorica, en la Ciénaga Grande (Córdoba). Este hecho político para despedir a su río se llamaría:]*[Do Wabura Dai Bia Ozhirada]*[(Adiós río, el que hacía todos nuestros beneficios).]*[Do Wabura]*[fue la primera movilización embera para denunciar las implicaciones que tendría la construcción de la hidroeléctrica Urrá I, logrando así atraer la atención de la opinión pública y motivar la primera visita del Ministerio de Ambiente a la zona e instalar el 2 de noviembre de 1994, una Mesa entre instituciones y afectados.]

[Cansados de los incumplimientos del gobierno, cientos de embera - katíos y campesinos-pescadores del Sinú marcharon hacia Bogotá, el 29 de noviembre de 1999. Dos semanas les tomó recorrer 700 kilómetros para llegar a la capital, justo el mismo día que iniciaba el proceso de llenado de la represa. Por más de un año, estuvieron los indígenas embera-katio asentados en el jardín frente al Ministerio de Ambiente visibilizando y denunciando las implicaciones que la construcción de la represa estaba teniendo sobre su vida y cultura. La movilización a Bogotá generó una sólida e importante solidaridad hacia este movimiento de defensa territorial.]

[En 1999, miembros de Asprocig, también afectados por Urrá 1, participaron en la audiencia regional de la Comisión Mundial de Represas en Brasil, posicionando las afectaciones aguas abajo provocadas por las represas, hasta ese entonces, poco reconocidas como daños derivados de estos proyectos. En el año 2000, se realizó la Misión Internacional a los territorios de los pueblos indígenas Embera Katío que luchaba contra Urrá y Uwa que se oponían  a un proyecto petrolero en Boyacá. La misión organizada por la Organización Nacional Indígena de Colombia –ONIC contó con delegados y delegadas de International Rivers Network de EE.UU., Oilwatch International, World Rainforest Movement, Sobrevivencia de Paraguay, la Coordinación Alemana por los Derechos Humanos, el Congreso General Kuna de Panamá, la Confederación de Nacionalidades Indígenas de Ecuador-CONAIE, el Movimiento de Afectados por las Presas de Brasil-MAB y Coalición Ríos Vivos de Argentina.]

[Más de 15 años de lucha lograron juntar a indígenas, pescadores, campesinos, ambientalistas y defensores de derechos humanos, y llevar a decenas de países las denuncias de las afectaciones ocasionadas por las represas. Sin embargo la defensa del Sinú tendría un enorme costo de vidas de embera-katíos e intelectuales, los cuales fueron asesinados por los grupos paramilitares como fue el caso de Lucindo y Kimy Pernía Domicó, indígenas embera-katío y el profesor Alberto Alzate de la Universidad de Córdoba.  ]

[La lucha contra Urrá I será recordada como la más contundente oposición a una hidroeléctrica en Colombia. Aunque si bien no logró frenar el proyecto, sepultó las posibilidades de su segunda fase: Urrá II. En esos mismos años, se realizaron los]*[Paneles por la Energía Sustentable]*[ en la que participaban afectados, ambientalistas y académicos, además se hicieron encuentros, talleres y debates que, poco a poco, fueron sumando y recogiendo las denuncias de los pueblos que ya habían sufrido la construcción de hidroeléctricas en otras regiones del país donde también se había resistido a las represas. De esta manera, empezaría a tejerse un fuerte entramado social que propiciaría la articulación de afectadas y afectados por las represas a nivel nacional. Así se dio lugar en 2006 en el municipio de Buenos Aires (Cauca), a la constitución de la Red Nacional de Afectados por Represas, transvases y proyectos MDL, primera expresión nacional organizativa de los afectados de represas con la participación de procesos de los ríos Ranchería, Guatapurí, Anchichayá, Sinú, Salvajina y la Miel.]

[En septiembre de 2008, la Red Nacional que hacía parte de la Red Latinoamericana de Afectados por Represas y por los Ríos, sus comunidades y el agua - REDLAR, organizó en cabeza de Asprocig y Censat Agua Viva, el]*[IV Encuentro Latinoamericano de la Redlar]*[ en Lorica, Córdoba. Como antesala del Encuentro se realizó una navegación por el río Sinú desde el resguardo Karagaví hasta la desembocadura en el Golfo de Morrosquillo para rendir homenaje a los cientos de indígenas embera-katio que dieron su vida por la defensa del Río. Con más de 200 delegados nacionales e internacionales, el Encuentro debatió situaciones particulares y comunes de un modelo energético que se imponía en el continente a costa de la vida y las culturas de los pueblos ribereños.]

[En los años siguientes se fueron sumando otros procesos, primero el Movimiento Social en Defensa del Río Sogamoso y Chucurí de Santander, luego la Asociación de Afectados por El Quimbo - Asoquimbo del Huila y finalmente la Mesa por la Defensa del Territorio de Ituango de Antioquia, que posteriormente adoptó la denominación Ríos Vivos Antioquia. En este trasegar la Red se transformó y dio un gran paso al cambiar su nombre y estructura por la del Movimiento Nacional en Defensa de los Territorios y Afectadxs por Represas - Movimiento Ríos Vivos.]

[Aquí hacemos un alto para homenajear el papel de las personas que forzaron en los inicios este proceso: Licifrey Ararat, Teresita Lasso, Tatiana Roa Avendaño, Iván Correa, Ever Grondona, Silvano Caicedo, Hildebrando Vélez, y resaltamos la labor de Juan Pablo Soler, que ha tejido con otras y otros latinoamericanos relaciones de hermanamiento durante muchos años para construir propuestas en torno a la libertad de los ríos y los derechos de los pueblos y comunidades afectadas por represas.]

[Han pasado 6 años del]*[I Encuentro Nacional del Movimiento Ríos Vivos]*[, en Girardota (Antioquia) y “mucha agua ha pasado debajo del puente”. Ríos Vivos no sólo ha logrado posicionarse como un movimiento nacional que representa los afectados por las represas, también ha contribuido a la construcción del]*[Movimiento Latinoamericano de Afectados por Represas – MAR]*[. Ríos Vivos ha caminado defendiendo los derechos de los pueblos ribereños y visibilizando sus múltiples formas de vida y, denunciando las implicaciones que conllevan las represas. El Movimiento ha logrado desenmascarar el imaginario de energía limpia de las megarepresas y las irregularidades asociadas a estos proyectos, le ha apostado a la construcción de propuestas de vida en los territorios y ha avanzado en iniciativas como las de transición energética y la memoria histórica ambiental.]

[Con esa historia de lucha y resistencia, el Movimiento Ríos Vivos se prepara para realizar en octubre, en la ciudad de Barrancabermeja su II Encuentro Nacional con la participación de invitados de más de 15 países de América Latina y más de 300 delegados y delegadas de las regiones donde hace presencia en Colombia. Allí se espera reflexionar sobre los retos actuales de los afectados y las afectadas por las represas, avanzar en propuestas en torno a la transición energética y juntar fuerzas para enfrentar la agresiva política nacional minero-energética.]

[Esta es una invitación para sumarnos a la construcción histórica de este importante Encuentro.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
