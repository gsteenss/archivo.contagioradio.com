Title: Militares continúan con la erradicación forzada en Putumayo
Date: 2018-01-03 19:09
Category: DDHH, Nacional
Tags: lideres sociales, Putumayo, Sustitución de cultivos de uso ilícito
Slug: militares-continuan-con-erradicacion-forzada-en-putumayo
Status: published

###### [Foto: latincorrespondent.com] 

###### [3 Ene 2018] 

Militares de la Brigada 27 Selva ingresaron este miércoles sobre las 11 de la mañana al caserío de la Zona de Reserva Campesina Perla Amazónica y erradicaron forzadamente cultivos de coca de los campesinos, pese a que estos están inscritos al Plan de Sustituciónvoluntaria de cultivos de uso ilícito. Así lo ha denunciado mediante un comunicado la Comisión de Justicia y Paz.

Desde hace meses los mismos campesinos han empezado a generar proyectos de soberanía alimentaria para realizar la sustitución de los cultivos y para fortalecer el comercio, sin embargo, como lo asegura la organización defensora de Derechos Humanos, **el gobierno sigue incumpliendo el punto 4 de los acuerdos de paz.** (Le puede interesar: [Guerra contra los líderes de sustitución se está tomando Putumayo)](https://archivo.contagioradio.com/lideres_sociales_putumayo_puerto_asis_asesinatos/)

"Nuevamente la disposición de la sustitución voluntaria de las comunidades es desconocida. **Una es la respuesta del gobierno civil del gobierno que recibió el listado de preinscripciones para el ingreso formal al PNIS y  otra la de las fuerzas militares**, desconociendo el punto 4 del Acuerdo entre el gobierno Nacional y las FARC que claramente favorecía salidas estructurales protegiendo los derechos de los campesinos", dice la Comisión.

Además, el pasado 31 de diciembre, **Janeth Silva,** representante legal de la Asociación de Desarrollo Integral Sostenible Perla Amazónica, ADISPA, organización que representa la Zona de Reserva Campesina Perla Amazónica fue víctima de una nueva amenaza contra su vida. “Perra sapa aprobecha este ano por que no duraraz el 2018”, decía el mensaje de texto que le llegó a su celular.

Cabe recordar que el pasado mes de julio, cuando las comunidades del Putumayo lograron pactar un acuerdo de sustitución con el gobierno, entre los puntos acordados se incluía que **si el gobierno llega a incumplir lo pactado, los campesinos retomarán la movilización y convocarán a paro.** (Le puede interesar: [Cultivadores de coca con más de 3.8 hectáreas no son narcotraficantes: Campesinos)](https://archivo.contagioradio.com/campesinos-del-putumayo-logran-acuerdos-de-sustitucion-de-cultivos/)

###### Reciba toda la información de Contagio Radio en [[su correo]
