Title: De Caquetá a Bogotá: 550 kilómetros caminando por la educación superior
Date: 2018-11-19 12:20
Author: AdminContagio
Category: Educación, Nacional
Tags: Caminantes, Caquetá, educacion, Hijos de la Manigua
Slug: 550-km-caminando-educacion-superior
Status: published

###### [Foto: Unees] 

###### [19 Nov 2018] 

Durante el fin de semana llegaron a Bogotá **“Los Hijos de la Manigua”**, un grupo de estudiantes que desde el 6 de noviembre iniciaron una caminata de más de 550 kilometros entre  Florencia, Caquetá, hasta la Capital del país, **en defensa de la educación superior y del territorio amazónico**. (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el paro nacional"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

Según **Felipe Trujillo,** uno de los estudiantes que caminó desde Florencia, con la marcha, **los estudiantes quieren apoyar el movimiento por la educación superior y sus exigencias**, así como recoger insumos de la región sur-occidental de Colombia, para transmitir sus conocimientos a la delegada de la región que está en la Mesa de Negociación con el Gobierno. (Le puede interesar: ["119 estudiantes fueron capturados tras la movilización estudiantil en Bogotá"](https://archivo.contagioradio.com/119-estudiantes-fueron-capturadas-tras-la-movilizacion-estudiantil-en-bogota/))

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fvideos%2F183360995867042%2F&amp;show_text=0&amp;width=560" width="560" height="308" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La caminata inició el pasado 6 de noviembre con 38 personas, pero a lo largo del recorrido se unieron otros estudiantes de la Universidad Surcolombiana en Neiva, de la Universidad del Tolima en El Espinal, y de la Universidad de Cundinamarca en Girardot y Fusagasuga; por lo tanto, **a Bogotá arribaron más de 90 personas**. (Le puede interesar: ["30 estudiantes resultaron heridos durante movilización en Bogotá"](https://archivo.contagioradio.com/al-menos-30-estudiantes-resultaron-heridos-durante-movilizacion-en-bogota/))

Trujillo señaló que otra de las motivaciones de la caminata es la defensa del territorio amazónico, esperando que en Bogotá se conozca la riqueza ambiental de esta zona, y se la proteja como es debido. Entre tanto, **otras regiones han comenzado sus propios trayectos hacía la capital, uniéndose a la defensa de la educación superior pública**. (Le puede interesar: ["Con movilizaciones estudiantes presionarán al Gobierno para negociar"](https://archivo.contagioradio.com/movilizaciones-estudiantes/))

<iframe id="audio_30175633" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30175633_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
