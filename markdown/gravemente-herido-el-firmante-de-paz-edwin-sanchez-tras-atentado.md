Title: Gravemente herido el firmante de paz Edwin Sánchez tras atentado
Date: 2020-10-16 14:56
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Edwin Andrés Sánchez, excombatientes, Firmante de paz, Partido FARC, San José del Guaviare
Slug: gravemente-herido-el-firmante-de-paz-edwin-sanchez-tras-atentado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Atentan-contra-la-vida-de-Edwin-Andres-Sanchez-excomabtiente-de-FARC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Asociación Nacional de Zonas de Reserva Campesina -[ANZORC](https://twitter.com/ANZORC_OFICIAL)-, denunció un atentado en contra del firmante de paz **Edwin Andrés Sánchez,** quien fue atacado con arma de fuego el pasado 13 de octubre, en la vereda Termales de San José del Guaviare. (Le puede interesar: [Asesinan a Nelson Ramos integrante de ASIMTRACAMPIC en Piamonte, Cauca](https://archivo.contagioradio.com/asesina-campesino-nelson-ramos-piamonte/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ANZORC_OFICIAL/status/1317068960970530817","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ANZORC\_OFICIAL/status/1317068960970530817

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Edwin Andrés Sánchez es excoordinador del Espacio Territorial de Capacitación y Reincorporación -ETCR-** Las Colinas, situado a una hora de **San José** del Guaviare. La víctima habría recibido **dos disparos de arma** de fuego, uno en una pierna y otro en un brazo y según la información suministrada, después del ataque, fue dirigido en una ambulancia fuertemente custodiada  al hospital del municipio donde actualmente se recupera de las heridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este ataque se produjo apenas horas después de que fuera asesinado el también firmante de paz, Plutarco Cardozo de 46 años, quien fue atacado apenas a cuatro kilómetros del ETCR “Jaime Pardo Leal” del mismo municipio de San José del Guaviare. (Lea también: [Firmante de paz, Plutarco Cardozo fue asesinado en cercanías de ETCR en Guaviare](https://archivo.contagioradio.com/plutarco-cardozo-asesinado-guaviare/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El Partido FARC ha denunciado el asesinato de** **232 excombatientes** desde la firma del Acuerdo de Paz y ha solicitado a través de sus líderes, que el Gobierno Nacional atienda esta situación con un **enfoque en Derechos Humanos y reconciliación, y no solo con movilización de efectivos de la Fuerza Pública hacia los territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte,** una comisión conformada por líderes y lideresas regionales del Partido FARC en las diversas regiones del país; organiza **una gran movilización para el mes de noviembre** en rechazo del asesinato de 49 excombatientes en lo que va corrido del 2020, lo cual ha sido denominado como un “extermino sistemático” contra los firmantes de paz. (Lea también: [Reincorporados de FARC se movilizarán para exigir el derecho a la vida y la paz](https://archivo.contagioradio.com/excombatientes-farc-movilizaran-rechazo-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
