Title: Minminas y Mininterior generan señalamientos a líderes ambientales del Meta
Date: 2016-11-02 16:45
Category: Ambiente, Nacional
Tags: Alto Ariari, Señalamientos a Defensores Ambientales, Violaciones a los DDHH
Slug: minminas-mininterior-generan-senalamientos-lideres-ambientales-del-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-castillo-meta-14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [2 Nov de 2016] 

Luego de conocer un documento emitido por el Ministerio de Minas y el Ministerio del Interior, en el que se vincula a algunos líderes comunitarios con grupos armados, defensores del agua, la vida y el territorio en la región del Ariari en el Meta, denuncian que se han generado **señalamientos y amenazas a quienes se** **oponen a las actividades extractivas adelantadas por Ecopetrol y Repsol**, en los municipios de Acacias, Guamal, Castilla la Nueva, Cubarral, San Martín y El Dorado.

Desde el año 2010, iniciaron las exploraciones en los seis municipios que hacen parte del Bloque denominado CPO 9, y con ellas, también empezaron las amenazas. Edgar Cruz, líder comunitario de esta región, reveló que en meses pasados, empresas, instituciones gubernamentales y el Programa de las Naciones Unidas para el Desarrollo en Colombia, **"pactaron una reunión con el Ministerio de Defensa para diseñar estrategias de intervención a los territorios".**

Añade que los señalamientos generados por el documento de los ministerios, sumados a una posible arremetida de la Fuerza Pública **"representa una clara sentencia de amenaza que, pone en riesgo nuestras vidas y la de nuestros territorios".**

Frente a ello, organizaciones y líderes exigieron la aclaración de lo enunciado en el documento, para su sorpresa, la respuesta que recibieron fue: “tenemos comprobado que la protesta social esta permeada por grupos al margen de la ley tanto de izquierda como de derecha”, Cruz, añade que “incluso se atreven a decir que **algunos líderes tienen vínculos con paramilitares y que cuando las comunidades salen a defender el territorio lo hacen porque están presionadas por estos grupos”.**

Además, el defensor del territorio, añade que el departamento de planeación en las ultimas tres semanas “esta desarrollando unos pilotos para realizar una encuesta en 4 zonas del país entre esas los municipios del bloque CPO 9, (…) quieren establecer que organizaciones sociales son las que se oponen a los proyectos, los nombres de líderes y las actividades que desarrollan”, señaló también que **“parece que esto coincide con la estrategia que planea la ANH junto al Ministerio de Defensa”.**

Ocho de los líderes amenazados, ya adelantaron las respectivos acciones de denuncia ante la **Fiscalía, la defensoría del pueblo del Meta, la Oficina del Alto Comisionado para los Derechos Humanos de la ONU**, colectivos de abogados defensores de Derechos Humanos y ante el Comité Cívico por los Derechos Humanos del Meta. Le puede interesar: [Hay 177 amenazas contra líderes reclamantes de tierras en Colombia.](https://archivo.contagioradio.com/periodo-de-sesiones-156-cidh-restitucion-de-tierras-y-violencia-sexual/)

Hasta el momento, no han iniciado las investigaciones pertinentes para poder establecer de donde se origino el documento y el por qué. Cruz manifiesta que desde la Agencia Nacional de Hidrocarburos **“nos están diciendo que no le prestemos atención al documento, pero nos alarma porque realmente parece un documento de inteligencia militar”. **Le puede interesar: [2444 defensores de DDHH han sido agredidos en 5 años de gobierno Santos. ](https://archivo.contagioradio.com/2444-defensores-de-ddhh-han-sido-agredidos-en-5-anos-de-gobierno-santos/)

Por otra parte, Edgar Cruz resalta que las exigencias de las comunidades de estos municipios, van encaminadas a que **“gobernadores, alcaldes y concejales se tomen en serio la sentencia T445, donde se ratifica la autonomía de los territorios** y la posibilidad de blindar los territorios de la minería indiscriminada”.

Cruz llama la atención sobre el grave daño que estos proyectos petroleros significan para un ecosistema tan sensible e importante, ya que **el piedemonte llanero es una zona de recarga hídrica, que garantiza el flujo del valioso recurso para toda la Orinoquia.**

Diferentes organizaciones defensoras del territorio, han “exigido que el Gobierno realice un inventario de las zonas de reserva de agua dulce **para saber y conocer cómo debemos defender estos territorios, (…) las empresas extractivas tienen esa información pero, por supuesto no es pública”** concluyó Cruz.

<iframe id="audio_13587745" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13587745_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
