Title: Nairo Quintana pedalea por la Equidad de Género
Date: 2016-09-14 09:54
Category: Mujer, Nacional
Tags: Equidad de género, Nairo Quintana
Slug: nairo-quintana-pedalea-por-la-equidad-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ENREDATE-9-e1473859161131.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Miguel Angel Blanco] 

###### [14 de Sept de 2016] 

El boyacense Nairo Alexander Quintana, ganador de la reciente vuelta a España e ícono nacional no sólo del deporte, sino también de tenacidad y humildad, sorprende de nuevo. Esta vez siendo embajador de las Nuevas Masculinidades\* y la Equidad de Género en el país. **Bajo el eslogan *“Enrédate con la equidad de género”* el ciclista alza la voz a favor de la igualdad.**

En una serie de vídeos divulgados por la Alcaldía del municipio de Tunja, el deportista extiende una **invitación a [*“ganarle a la desigualdad humana”*]** construyendo una sociedad en la que hombres y mujeres tengan acceso a las mismas oportunidades. Nairo también resalta la importancia de **educar en igualdad desde casa y la necesidad de la corresponsabilidad\* en las tareas domésticas y de cuidados\***.

Y pese a que los vídeos se publicaron hace más de un año en el marco de la campaña liderada por la Alcaldía de Tunja, es ahora cuando se han vueltos virales.

Esta iniciativa constituye un **aporte muy importante en la lucha contra la violencia de género en el país**, pues según declaraciones de la secretaria de Equidad del municipio: *“La campaña busca transformar estereotipos y patrones culturales tradicionalmente aceptados en la sociedad que atentan contra las mujeres y niñas”.* Evidencia de esto es que [el 85% y el 86% de víctimas en casos de violencia sexual son niñas y mujeres](https://archivo.contagioradio.com/a-este-ritmo-harian-falta-81-anos-para-lograr-la-equidad-de-genero/)(Instituto Nacional de Medicina Legal y Ciencias Forenses & DIJIN, 2015).

Vale la pena recordar que esta no es la primera vez que Nairo participa en campañas a favor de causas sociales. En Mayo del 2015 fue nombrado Embajador del Agro Colombiano, donde **reiteró que se siente orgulloso de sus orígenes campesinos** y recientemente hizo público que **votará SÍ en el plebiscito el próximo 2 de Octubre y pidió a los colombianos que también apoyen la paz.**

#### (\*1)Nuevas Masculinidades: El concepto se puede entender como un movimiento incipiente de hombres o grupos de hombres que abogan por la igualdad entre hombres y mujeres en todos los aspectos de la vida y que luchan por erradicar la violencia física o psicológica a mujeres, niños, ancianos y hombres. 

#### (\*2)Tareas de cuidados: Aquellas actividades que se realizan para el mantenimiento de la vida y la salud, históricamente invisibilizados, relegados al ámbito doméstico y atribuidos a las mujeres, 

#### (\*3) Corresponsabilidad: establece una visión clara que pone en evidencia las desigualdades de género con el objetivo de revocar la responsabilidad tradicionalmente asumida por las mujeres en la reproducción social. 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
