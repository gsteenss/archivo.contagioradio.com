Title: Nacer es nuestro primer desarraigo
Date: 2018-09-05 07:00
Author: AdminContagio
Category: Opinion
Tags: nacimiento, vida
Slug: nacer-es-nuestro-primer-desarraigo
Status: published

###### [Foto: Damia Díaz](http://damiadiaz.com/index.php/dibujos-rumores-desarraigo/) 

######  

#### **Por: Ángela Galvis Ardila.** 

###### 5 Sep 2018 

La vida es una constante mudanza de sentimientos, de personas, de lugares. Desde que nacemos sabemos lo que eso significa: nos expulsan –sin nuestro consentimiento, así como tampoco nos lo pidieron para traernos– de ese espacio tibio que es el útero de nuestras madres, y somos lanzados a un lugar hostil que nos obliga a llorar si queremos algo o necesitamos de alguien. Y así descubrimos que el llanto es un camino que, por lo general, no falla para conseguir esa atención que se alimenta del reconocimiento de los demás. Pero también vamos descubriendo que somos seres llenos de deseos que, una vez satisfechos, conocemos el hastío.

[Vamos de amor en amor. Pasamos del amor de los padres al amor de los hijos. Volamos de ese primer amor ingenuo del que creímos que su final sería también el nuestro, a los amores pasionales que nos han hecho sentir que el corazón queda en el páncreas y, de ahí, aterrizamos en esos amores maduros a los que ya no queremos ver partir, esos amores que, como una canción que nos gusta, no queremos que se acabe jamás, pero que el tiempo, ese dictador implacable, nos arrebata en una enseñanza perversa de la finitud humana.  Vamos dejando pedazos vida con cada despedida.]

[Nos despedimos de amigos, de sueños, de ilusiones, pero también de enemigos, de miedos, de frustraciones, de odios, y nos liberamos y nos volvemos a atar en un círculo volátil de apegos y desapegos, de amores y desamores, de alegrías y sufrimientos, y así va transcurriendo la vida mientras pensamos que es tan corta para unas cosas, pero tan larga para otras.]

[Esa misma vida que juega burlonamente y que pocas veces nos deja saborear la felicidad en el preciso instante de vivirla, sino que, dentro de sus múltiples trampas, nos pone en cada esquina un espejo retrovisor para que nos reconozcamos que fuimos felices, y recordemos el momento con ese tinte de nostalgia empalagosa que nos invade la garganta en forma de nudo y que hace que deseemos visceralmente que el tiempo se devuelva y repetir cada segundo feliz sin saber que lo estamos siendo.]

[Vamos de desalojo en desalojo. Siendo niños queremos ser grandes y cuando ya nos estamos acostumbrando a la infancia, arriba con toda su magnitud ese tsunami de hormonas que nos dicen se llama adolescencia y que gozamos intensamente; pero de ahí también nos arrojan sin contemplación alguna y llega eso que le dicen adultez que, básicamente, no es otra cosa que pagar facturas y buscar excusas para justificar vivirla bajo la comodidad de lo cotidiano. Y cuando ya no tenemos o, mejor, no necesitamos decirnos más mentiras para soportar la realidad, cuando el sentimiento es más cercano al alivio, cuando en una especie de epifanía creemos que ni la vida nos debe ni le debemos nada a la vida, hace su aparición la muerte y de un corte certero con la hoz nos arranca el último suspiro y nos lleva a esa nada en la que habitábamos antes de nacer.]

[Y es que no podía ser de otra manera, pues llegamos a este mundo condenados a pasar, a ser fugaces, a desprendernos y que se desprendan de nosotros, a ver que se alejan los sentimientos, las personas, los lugares. Y cómo no iba a ser así si nacer es nuestro primer desarraigo y de ahí en adelante casi todo es rutina.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio 
