Title: "Hay que trabajar para la esperanza y la utopía": Lola Cendales
Date: 2016-08-10 13:28
Category: Educación, eventos
Tags: educación popular en Colombia, Lola Cendales, Universidad Pedagógica Nacional
Slug: hay-que-trabajar-para-la-esperanza-y-la-utopia-lola-cendales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Lola-Cendales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Saber Popular ] 

###### [10 Ago 2016 ] 

Durante este viernes y sábado, docentes, estudiantes, investigadores y líderes sociales se darán cita en la Universidad Pedagógica Nacional para reflexionar sobre los aportes de la educación popular, las **experiencias actuales y los retos y desafíos frente al nuevo contexto nacional** y latinoamericano, en el marco del foro 'Pasado presente y futuro de la educación popular en Colombia'.

Desde las ideas pioneras de Paulo Freire, la educación popular se fue configurando para responder a las complejas situaciones de América Latina, a través de reflexiones teóricas y **prácticas educativas formales e informales, comprometidas con la transformación social**, asegura la maestra Lola Cendales, quien agrega que esta corriente es vigente porque las condiciones de injusticia, desigualdad y violencia en las que surgió, aún se mantienen.

"**Nadie puede convenir con el desastre ético y ambiental que estamos viviendo** (...) ese es el reto que tiene la educación popular, es decir, cómo se plantea el país por ejemplo frente a la construcción de la paz, frente a las injusticias y frente a la exclusión social que se vive (...) lo que tiene que ver con cuestionar el poder, pero también construir poder, con la inclusión de muchos sectores excluidos (...) y el sentido de emancipación y libertad para lograr otras condiciones de vida", afirma la maestra.

Cendales asegura que para aportar a la construcción de paz en Colombia, es necesario que se reflexionen y transformen los conceptos históricos que día a día se tratan en las escuelas y espacios educativos informales. Para la maestra **los docentes tienen un gran compromiso en la consolidación de la paz** a partir del diálogo cultural y el énfasis en la ciudadanía.

De acuerdo con la maestra "hay experiencias muy esperazandoras (...) que no tienen la visibilidad necesaria", pero en ellas están las claves para las reflexiones críticas sobre la construcción de poder para la transformación social a largo plazo, y éste es justamente el lugar de la educación popular, "que no puede perder de vista (...) **trabajar también en términos de la esperanza y la utopía**".

<iframe src="http://co.ivoox.com/es/player_ej_12504328_2_1.html?data=kpeikpmXdpmhhpywj5eZaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca7Vxtjh1MaPkNDgwpCwx9PIpc3Z1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

######  

 
