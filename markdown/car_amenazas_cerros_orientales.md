Title: Tres amenazas sobre los cerros orientales que se deben frenar según la CAR
Date: 2017-07-30 20:11
Category: Ambiente, Voces de la Tierra
Tags: CAR, cerros orientales, Enrique Peñalosa
Slug: car_amenazas_cerros_orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/CERROS-DE-LOS-ALPES-ARQUITECTURA-Y-CONCRETO-6-e1477934986558.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ImaginaBogota 

###### [30 Jul 2017] 

En el 2013 el Consejo de estado emitió una sentencia mediante la cual se obliga al distrito a proteger los cerros orientales, sin embargo este poco o nada de ha cumplido pues las más de 13.000 hectáreas que comprende la reserva natural de los Cerros Orientales están en peligro por tres factores específicos: **las actividades asociadas al uso inadecuado del suelo, la ocupación e intervención de fuentes hídricas o áreas protegidas y afectaciones a la fauna y flora del lugar**, como lo constató la Corporación Autónoma Regional, CAR de Cundinamarca a través del análisis de 178 procesos sancionatorios.

[Y es que desde 1974 está prohibido construir en esa zona, sin embargo, en la actualidad “El principal problema se origina en los asentamientos ilegales o invasiones, una infracción, en su mayoría, de tipo urbanístico”, dice  Patricia Mora, directora operativa de la Dirección Regional Bogotá La Calera de la CAR, quien agrega **“los asentamientos ilegales y urbanizaciones, por ejemplo, generan además de la invasión a la zona protegida**, otro tipo de afectaciones como tala de árboles y pérdida de cobertura vegetal, captación ilegal de agua, desviación de los cauces, contaminación de fuentes hídricas por vertimientos, daños al suelo, así como la pérdida de hábitat de los animales que viven en los cerros orientales”.]

[De hecho, organizaciones ambientalistas agrupadas en la Mesa de los Cerros Oriental han alertado que la administración de Enrique Peñalosa  ha desacato el Acuerdo 030, mediante el cual se estipula que los Cerros orientales son una Zona de Reserva Protectora. Selene Lozano, integrante de la Mesa, manifesta que **actualmente el distrito “intenta revivir licencias en los Cerros de las grandes constructoras”**, para distintos proyectos que afectarían la Reserva Thomas Van Der Hammen.]

[Sin embargo no se trata de una situación nueva. De acuerdo con la normativa, la única actividad permitida en los Cerros es la obtención de frutos secundarios del bosque, y otras que no modifiquen los suelos ni su uso. Pero en  2005, Sandra Suarez, Ministra de Ambiente **durante el gobierno de Álvaro Uribe, avaló la figura de derechos adquiridos de particulares, constructoras y personas que tenían proyectos urbanísticos** en la Franja de Adecuación de los cerros, de allí la gran cantidad de urbanizaciones que se pueden observar hoy día.]

[Y ahora, en medio de las advertencias de la CAR, el panorama no parece mejorar. El abogado Rodrigo Negrete aseguró que lo que pretende la actual alcaldía es “desdibujar el área de Reserva Forestal Protectora”. El denominado **Plan Zonal del Norte, promovido por el distrito incluye nuevas autopistas y proyectos de urbanización afectaría l**a conectividad entre los Cerros Orientales, los humedales de Torca y Gaymaral, la Reserva Thomas van der Hammen y el Río Bogotá.]

### [Fauna y flora en peligro] 

[De acuerdo con la CAR, las afectaciones al suelo suman 105 procesos sancionatorios abiertos, discriminados así: 44 corresponden a la disposición inadecuada de basuras y escombros; 21 casos son el resultado de demoliciones y construcciones totales o parciales de edificaciones, explotación agrícola y pecuaria, almacenamiento y depósito de materiales; otros 21 por ocupación e intervención de zonas de ecosistemas estratégicos y violación a las normas del uso del suelo (POT, Pomcas y Planes de Manejo Ambiental); 13 entre minería, vertimientos y ampliación de fronteras agrícolas; y 6 más por incumplimientos actos administrativos relacionados con planes de manejo en áreas protegidas.]

[A su vez establece que del total de procesos sancionatorios **69 de ellos corresponden al manejo inadecuado de fuentes hídricas; 40 expedientes relacionados con vertimientos, intervenciones en áreas de ronda, ocupación de cauces y captación ilegal de agua.** Por su parte, existen otros 29 procesos relacionados con afectaciones a la flora del lugar; es decir, tala de árboles nativos en ecosistemas estratégicos o áreas protegidas y aprovechamiento forestal sin permiso.]

Finalmente la CAR explica que actualmente existen 4 procesos sancionatorios relacionados con la tenencia ilegal de fauna y contaminación del aire en los cerros orientales, es decir que** las más de 120 especies de aves, los anfibios y unas 63 especies de mamíferos están en riesgo.**

### Audiencia pública 

Teniendo en cuenta esa realidad, los congresistas Alirio Uribe, Ángela María Robledo e Iván Cepeda, realizarán una **audiencia pública este jueves 3 de agosto desde las 8 de la mañana**. Allí se espera hacer una auditoria con expertos, académicos, ambientalistas y las autoridades distritales para que se explique cómo se está aplicando la sentencia del Consejo de Estado, teniendo en cuenta las principales problemáticas identificadas como la propuesta de un sendero peatonal, las licencias para construcción y las edificaciones de carácter legal e ilegal que se adelantan en la zona.

![DGBSrkhWAAAiUd8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DGBSrkhWAAAiUd8.jpg){.alignnone .size-full .wp-image-44507 width="750" height="550"}

###### Reciba toda la información de Contagio Radio en [[su correo]
