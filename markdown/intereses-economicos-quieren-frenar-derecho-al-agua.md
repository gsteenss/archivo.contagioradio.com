Title: Intereses económicos quieren frenar el Derecho al agua
Date: 2016-11-17 16:03
Category: Ambiente, Nacional
Tags: Claudia López, defensa del ambiente, derecho al agua
Slug: intereses-economicos-quieren-frenar-derecho-al-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Páramo-Macizo-Colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Nov 2016] 

El proyecto de Ley que busca hacer constitucional el Derecho al Agua se **ha enfrentado a fuertes intereses políticos y económicos de algunos sectores estatales y privados**, ya que de ser  aprobado establecería el uso prioritario del recurso para consumo humano y obligaría al Gobierno colombiano a proteger y recuperar ecosistemas importantes en la producción de agua.

**La Senadora Claudia López** de la Alianza Verde, una de las gestoras de esta iniciativa aseguró que la situación del agua priorizada para represas, distritos de riego e intereses de multinacionales extractivas y no para las comunidades **“de aquí en adelante será inconstitucional, no se puede priorizar el progreso de una región por encima de de las personas”.**

El Estado deberá recuperar y proteger de manera muy especial todos los ecosistemas relacionados con la producción del recurso hídrico como lo son **bosques, páramos, ríos y selvas.** Las comunidades podrán gozar de ambientes saludables y **“podrán acceder en condiciones de progresividad a este recurso fundamental para la vida que es el agua”** apunta la Senadora**.**

### **¿Quiénes se han opuesto al proyecto?** 

Durante el quinto debate, en la Comisión Primera del Senado, el Ministro de Ambiente, Luis Gilberto Murillo, según comenta la Senadora, **“vino a pedir que se hundiera el proyecto porque le imponía unas obligaciones a su cartera que no estaban en capacidad de cumplir”**. Sin embargo, durante la plenaria en el Senado tuvieron más fuerza los argumentos a favor del agua como derecho fundamental.

Los **ministerios de Vivienda y de Hacienda, y el Departamento Nacional de Planeación** han argumentando que al decretar este derecho **se desataría "una avalancha de tutelas"** y que el Estado estaría obligado a garantizar en un 100% el agua. Frente a ello la Senadora Claudia López, manifestó que **"eso es imposible en un año y además eso no se podría hacer con este derecho ni con ningún otro”.**

Otra de las preocupaciones del Gobierno es que este proyecto pudiera implicar un mínimo vital de agua y agua gratuita, “pero **este acto legislativo en ningún momento establece la gratuidad ni hace alusión al mínimo vital, por eso creo que son excusas” aclara la Senadora.**

Para López “este gobierno prioriza la locomotora minera y decide que ese uso es más prioritario que el consumo humano, eso es lo que no podrá seguir haciendo, estamos cambiando las prioridades de la política pública**”.**

Frente a este panorama se desarrollará en Bogotá el** ‘Quinto Encuentro por el Agua’, **para discutir los retos que tiene el Gobierno Nacional en la **protección y recuperación de los ecosistemas** **y las garantías del derecho fundamental al agua, a la vida y la salud de los colombianos. **El evento será el próximo 22 de Noviembre en el Hotel Marriot de Bogotá a partir de las 7 a.m.

<iframe id="audio_13805015" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13805015_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
