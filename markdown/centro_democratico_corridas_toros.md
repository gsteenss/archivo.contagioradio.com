Title: Centro Democrático busca volver patrimonio cultural las corridas de toros
Date: 2017-03-23 19:47
Category: Animales, Voces de la Tierra
Tags: Centro Democrático, corridas de toros
Slug: centro_democratico_corridas_toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/corridas-de-toros-e1486324945371.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

###### [23 Mar 2017]

**La representante a la Cámara Margarita María Restrepo y la senadora Paola Holguín, ambas del Centro Democrático,** radicaron este jueves un proyecto de Ley por medio de la cual se expide el reglamento nacional taurino, que entre otras cosas, reforzaría las corridas de toros obligando al gobierno a proteger la actividad taurina y a declararla patrimonio del país.

"Dará protección a quienes divulgan, promueven y participan en la fiesta de los toros, apoyará las escuelas taurinas y cooperará con los toreros de a pie y de a caballo, picadores, banderilleros y mozos de espada con todo lo concerniente a su Seguridad Social", dice el texto de la iniciativa legislativa, que pretende** que se pague Seguridad Social a los taurinos.** Según Andréa Delgado, abogada e integrante de la Plataforma ALTO, se estaría obligando al Estado a "cubrir la atención en Seguridad Social a un grupo de interés que no tiene el rango constitucional para ser especialmente protegido, y además ese dinero se sacaría del bolsillo de todos  los colombianos".

Por otra parte, el proyecto de Ley **No. 237 de 2017** **estaría desacatando las ordenes de la Corte Constitucional** que hace unas semanas obligó al Congreso de la República a desincentivar esas prácticas taurinas, y por tanto, "cualquier forma de promoción estatal, iría en contravía de la jurisprudencia actual", señala la abogada.

Esta iniciativa del Centro Democrático, busca impedir que los funcionarios públicos puedan manifestarse en contra de las corridas de toros y demás espectáculos taurinos por medio de un artículo que así lo determinaría. "Los funcionarios del municipio donde se celebre cualquier clase de espectáculo o festejo taurino se abstendrán de patrocinar y/o promover cualquier campaña en contra de la fiesta de los toros, respetando así a los aficionados a ésta, quienes tienen derecho a disfrutarla como tal y a considerar el toreo como arte". Frente a esto, Delgado explica que ese artículo **vulnera el derecho fundamental a la libre expresión y la protesta,  y a su vez, vulnera la autonomía territorial.**

"Quienes hicimos la tarea de elaborar este proyecto le dimos la bienvenida a la ley 916 de noviembre 26 de 2004 - nuestro punto de partida - porque ésta unificó la fiesta de los toros en Colombia; además, sin su vigencia, tal vez estaríamos enfrentando la abolición atropellante de este grandioso espectáculo", dicen las congresistas antioqueñas que radicaron el proyecto quienes argumentan, entre otras cosas que: "A diferencia del fútbol, por ejemplo, los toros no generan violencia**; el espectáculo es cruento más no cruel; no hay tortura en el estricto sentido de la palabra".**

#### Conozca el texto del proyecto: 

<iframe id="doc_16811" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342864579/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-T4HVcLcW4jKNkN0g4mi7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
