Title: Conversaciones con el ELN apuntan a democratizar Colombia: Pablo Beltrán
Date: 2016-10-15 11:44
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, ELN, proceso de paz eln
Slug: conversaciones-con-el-eln-apuntan-a-democratizar-a-colombia-pablo-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Pablo-beltran-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 15 Oct 2016 

En un mensaje del ELN al **III Seminario sobre delito político** que se desarrolla en Bogotá, **Pablo Beltrán, jefe de delegación de paz por el ELN**, aseguró que las conversaciones de paz están encaminadas a conseguir los cambios mínimos para garantizar la democratización del país y sacar la violencia de la política,  agregó que están dispuestos a reconocer sus responsabilidades en la guerra pero también exigen lo mismo por parte de las FFMM y el Estado.

Respecto a las **conversaciones de Paz que iniciarán el próximo 27 de Octubre en Quito,** Beltrán señala que la agenda acordada tiene un punto central en la participación de la sociedad y tendrá su conclusión en el desarrollo de un listado de cambios básicos sobre los cuales deberá pronunciarse el gobierno y esa "va a ser la prueba para ver si estas clases dominantes tienen la voluntad para ceder en  algo en sus privilegios".

Agregó que ese punto de las conversaciones no está apuntando a la construcción del socialismo sino solamente a conseguir cambios básicos para lo que llaman una democratización. Por otra parte aseguró que están dispuestos a reconocer sus responsabilidades e invitan a que las FFMM también reconozcan su responsabilidad en hechos como genocidios o reconocer que hicieron persecución ilegal y que garanticen que no lo van a volver a hacer.

Beltrán, señaló que durante los últimos 30 años se ha reducido el concepto de delito político a su mínima expresión y que solamente dejarán de haber presos políticos cuando se termine la persecución política, hecho que solamente se logrará con la movilización social en pro de la democratización.

Habló también acerca de los cambios que deberían hacerse y recordó que uno de los primeros es la **reforma política que debería desembocar en la eliminación del paramilitarismo.** Además resaltó que el avance de las negociaciones de paz tendrán que conducir a que se acabe la rebelión que ha existido porque no se han facilitado las vías democráticas para la expresión de las diversas posturas, solamente ahí dejaría de existir la rebelión.

Por otra parte reiteró que es necesario que terminen los "malos mensajes" que se lanzan cuando se siguen persiguiendo defensores de derechos humanos o se sigue señalando a ambientalistas o líderes sociales. Según Beltrán las conversaciones de paz van a conducir a "una cultura de paz basada en la resistencia".

Por último, como aporte al seminario, asegura que tiene la certeza de que "nada de lo que necesite el pueblo se lo van a regalar las clases dominantes, todo hay que arrebatárselo, todo hay que forzarlo para que lo entreguen" y asegura que para eso hace falta una gran confluencia que se está gestando y que se manifiesta en la movilización social por la solución política del conflicto.

Resaltó que ya hay consensos en que la paz son cambios y no solamente el silencio de las armas, una segunda premisa es si será posible o no acabar con el uso de la violencia para hacer política, en tercer lugar señaló que es necesario mantener a América Latina como zona de paz, haciendo referencia a uno de los objetivos señalados por la CELAC.

<iframe id="audio_13331794" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13331794_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
