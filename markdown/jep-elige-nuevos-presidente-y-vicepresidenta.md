Title: A la JEP llegan nuevo presidente y vicepresidenta
Date: 2020-10-22 16:15
Author: CtgAdm
Category: Actualidad, Paz
Tags: JEP, magistrados, paz, verdad
Slug: jep-elige-nuevos-presidente-y-vicepresidenta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/BuscarlasHastaEncontrarlas-3.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Captura-de-Pantalla-2020-10-22-a-las-7.32.39-p.-m..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Captura-de-Pantalla-2020-10-22-a-las-8.10.42-p.-m..png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: JEP

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 22 de octubre la sala plena de la Jurisdicción Especial para la Paz (JEP) seleccionó a **[Eduardo Cifuentes Muñoz y Alexandra a Sandoval, como nuevos presidente y vicepresidenta de la Jurisdicción](https://www.jep.gov.co/Sala-de-Prensa/Paginas/Sala-Plena-de-la-JEP-elige-nuevo-presidente-y-vicepresidenta-de-la-Jurisdicci%C3%B3n-Especial-para-la-Paz.aspx)**, cargos que asumirán durante los próximos dos años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una elección que se llevó a cabo por medio de una plataforma virtual, que no solamente permitió **la participación de 33 de los 39 magistrados y magistradas de la sala plena de la JEP**, sino que también permitió que el voto de cada uno y cada una se mantuviera en secreto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo esta decisión y a petición de la presidencia de la JEP estuvo acompañada por delegados de la Procuraduría General de la nación, quienes hicieron las veces de veedores. ([El trabajo de la JEP para llegar a la verdad histórica en Colombia](https://archivo.contagioradio.com/el-trabajo-de-la-jep-para-llegar-a-la-verdad-historica-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El magistrado **Eduardo Cifuentes y la magistrada Alexandra Sandoval entrarían a reemplazar, desde el próximo 4 de noviembre** a las actuales, presidenta Patricia Linares y vicepresidencia a Xiomara Balanta quiénes estuvieron en estos cargos durante un período de 3 años.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91805,"width":313,"height":414,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Captura-de-Pantalla-2020-10-22-a-las-7.32.39-p.-m..png){.wp-image-91805 width="313" height="414"}  

<figcaption>
**Eduardo Cifuentes Muñoz, nuevo presidente de la JEP**

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El recién seleccionado presidente de la Jurisdicción Eduardo Cifuentes Muñoz, se desempeña como magistrado de la JEP desde enero del 2018, haciendo **parte de la sección de apelación, de la cual también fue presidente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Oriundo **de Popayán y abogado de la Universidad de los Andes**, se ha desempeñado como magistrado y presidente de la **Corte Constitucional, en el período de 1991 al 2000**. 9 años en los que trabajó por el derecho al mínimo vital, el derecho a la igualdad, la acción de tutela contra sentencias judiciales, la justiciabilidad de los derechos económicos y sociales; y la titularidad de derechos en cabeza de las comunidades étnicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, desde la Corte trabajó por el control de la declaratoria de los estados de excepción y la autonomía territorial, un período que fue seguido por su **nombramiento como** **Defensor del Pueblo el cual se desarrolló del 2000 y al 2003.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí **creó el sistema de alertas tempranas** que hasta hoy sigue vigente en la Defensoría del Pueblo como un mecanismo de exigir por parte de las comunidades atención y protección de sus vidas y sus territorios, así como el cumplimiento a los Acuerdos con el Gobierno,*"defendió activamente a las comunidades y poblaciones vulnerables y solicitó y promovió la ratificación del estatuto de Roma".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro de los cargos que ha desempeñado es como, **director** **de la División de Derechos Humanos** de la Organización de las Naciones Unidas para la Educación la Ciencia y la Cultura (**UNESCO**), cargo al que respondió del 2003 hasta el 2005, periodo en el que demás ejerció como **miembro del grupo Nacional de la Corte Penal de Arbitraje de la Haya.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo fue **presidente del Consejo Andino de Defensores del Pueblo**, y decano y profesor de la facultad de derecho de la Universidad de los Andes, un trabajo que fortaleció el movimiento que construyó la Constitución Política de 1991.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91807,"width":326,"height":444,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Captura-de-Pantalla-2020-10-22-a-las-8.10.42-p.-m..png){.wp-image-91807 width="326" height="444"}  

<figcaption>
**Alexandra Sandoval Mantilla, nueva vicepresidenta**

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Por su parte **Alexandra Sandoval Mantilla**, en su cargo como magistrada de la Jurisdicción conformaba desde enero del 2018, la **sala de amnistía o indulto de la JEP**, además de ser la coordinadora de la comisión de género la cual trabaja por la implementación del enfoque de género al interior de la jurisdicción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sandoval, nació en la ciudad de Bogotá en donde estudió derecho en la Universidad de los Andes posterior a ello realizó una maestría en Derechos Humanos y derecho penal internacional en la Universidad de Utrecht en Holanda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y al igual que el magistrado Cifuentes, a ejercido como docente de la Universidad de los Andes y la Universidad Javeriana en su sede de Cali . ([Presupuesto del 2021 se puede convertir en un nuevo ataque a la JEP](https://archivo.contagioradio.com/presupuesto-del-2021-se-puede-convertir-en-un-nuevo-ataque-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las tareas que deben asumir los dos magistrados es, por su parte la de Cifuentes ser el puente entre entidades como la Fiscalía y los demás mecanismos que conforman el Sistema Integral de Verdad Justicia Reparación y No Repetición, además de asumir la única vocería de la Jurisdicción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Representaciones que en caso de faltar la presidencia, la comisionada Sandoval asumiría durante el tiempo restante de la ejecución de su cargo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/NohraHeredia/status/1319395152276971528","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/NohraHeredia/status/1319395152276971528

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Con este nuevo cambio de mandato en la JEP diferentes personas se han manifestado a través de redes sociales, reconociendo y exaltando el trabajo que hasta la fecha ha encabezado la magistrada Patricia Linares, algunos de estos comentarios resaltan que, durante los tres años de su periodo respondió con, *"competencia, dignidad y resistencia, la responsabilidad asumida en la JEP, pese a los hostigamientos y múltiples oposiciones a las decisiones que tomó en cabeza de la Jurisdicción".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"black","fontSize":"normal"} -->

*Le puede interesar ver también la entrevista que realizó Contagio Radio a la actual presidenta de la JEP Patricia Linares: [Ver aquí](https://www.facebook.com/watch/?v=222729388963459)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
