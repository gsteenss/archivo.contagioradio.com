Title: Águilas Negras amenazan a defensores de DDHH de la Costa Atlántica
Date: 2015-10-15 16:53
Category: DDHH, Nacional
Tags: Aguilas Negras, eudaldo diaz, Ingrid Vergara, Juan David Diaz, MOVICE, Paramilitarismo, Sincelejo, Sucre
Slug: aguilas-negras-amenazan-a-defensores-de-ddhh-de-la-costa-atlantica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/amenaza_sucre_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [15 Oct 2015]

Mediante un panfleto firmado por paramilitares de las Águilas Negras, **nuevamente defensores de Derechos Humanos y líderes sociales del departamento de Sucre son objeto de amenazas de muerte**. Entre las personas amenazadas se encuentran Juan David Díaz, hijo de Eudaldo Díaz, alcalde del Roble asesinado en 2003 y la defensora de DDHH Ingrid Vergara.

En el panfleto, que fue abandonado en la puerta de la casa de Ingrid Vergara, se señala a cerca de 10 personas más, todos ellos y ellas vinculados con organizaciones sociales del departamento. Esta amenaza no es la primera que reciben muchas de estas personas ya que el departamento de Sucre es uno de los más azotados por la persistencia de paramilitares y de amenazas de este tipo.

Hasta el momento no hay un pronunciamiento oficial por parte de las autoridades ni de la Unidad Nacional de Protección que debe aplicar y ajustar las medidas de protección para defensores de DDHH.

Otra de las situaciones preocupantes y que según las personas amenazadas, puede explicar las amenazas es que el pasado mes de Mayo en la Villa de San Benito de Abad Ciénaga de Sispataca, el Movimiento de Víctimas de Crímenes de Estado, denunció que **cuatro  hombres armados ingresaron a ese lugar y rompieron la cerca que protegía los cultivos, y los destruyeron.**

Lo particular de este caso es que la comunidad [enfrenta una fuerte lucha contra ](https://archivo.contagioradio.com/ganaderos-amenazan-la-vida-de-campesinos-para-apoderarse-de-tierras-en-sucre/)**[ganaderos que se han apropiado de los baldíos](https://archivo.contagioradio.com/ganaderos-amenazan-la-vida-de-campesinos-para-apoderarse-de-tierras-en-sucre/),** por lo que los campesinos hoy viven **una grave crisis alimentaria,** sumado a que tradicionalmente, las comunidades viven de la pesca pero la minería ha contaminado el río y las especies de peces han desaparecido.
