Title: En medio de crisis económica Grecia comprará misiles antiaéreos a Rusia
Date: 2015-04-18 11:01
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Chipre, Conflicto Grecia Macedonia, conflicto Grecia Turquía Chipre, Grecia negocia compra de misiles antiaéreos a Rusia, Guerra Turquía Grecia
Slug: grecia-comprara-misiles-antiaereos-a-rusia
Status: published

###### Foto:Infomx7.com 

A una semana de la visita del primer ministro griego a Rusia, **Grecia ha anunciado que negociará con Rusia la compra de misiles antiaéreos S-300.**

Según declaraciones del ministro de defensa **Panos Kamenos, "Nos limitaremos a cambiar los viejos misiles por los nuevos"** , de tal forma quedaría asegurada la defensa antiaérea del país con la última tecnología. Estas han sido las únicas explicaciones que han dado desde el gobierno.

**Tsiripas se reunión con el presidente ruso** la semana pasada para establecer **lazos comerciales** y realizar intercambios como la salida del gas ruso a través del país heleno por un nuevo gaseoducto que enlazaría con el ya en funcionamiento desde Rusia hasta Turquía, a cambio de pagar un impuesto de salida por parte de Rusia.

Los **misiles S-300** son los mismos que el gobierno **ruso había contratado vender a Irán** por una suma de 800 millones de dólares, y que tuvo que cancelar debido a las sanciones de la ONU.  A penas conocida la noticia del acuerdo nuclear con el G5+1 e Irán, Rusia reanudó su venta.

**El ejercito Griego, actualmente es el más capacitado** de todo el **Mediterráneo** después del de Israel, debido a la tensión entre el país heleno y Turquía por la isla de Chipre, dividida en dos y donde aún no ha habido ningún movimiento por ambos países en pro de un acuerdo de paz.

Por otro lado también existe un **conflicto territorial e identitario con Macedonia**, por llamarse de igual forma que la región colindante griega. Aunque en el fondo está la **geopolítica** del propio país y las ansias de control del **comercio de Asia menor hacia Europa,** entrando así en **conflicto con Turquía y con los demás países balcánicos.**

Además en Grecia aún existe el servicio militar obligatorio y es el **país de la UE con más militares por habitante.** Estas son algunas de las hipótesis por las que el gobierno griego ha decidido comprar los misiles en medio de una crisis económica y financiera que aún no se ha resuelto y mantiene al país en las peores cifras de desempleo y pobreza máxima de la UE.
