Title: Asesinan a  Yissela Trujillo, lideresa en el norte de Caquetá
Date: 2019-07-27 22:07
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Caquetá, lideresa social
Slug: asesinan-a-yissela-trujillo-lideresa-en-el-norte-de-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/asesinato_de_lideresa_yissela_trujillo_conmociona_a_puerto_rico_2C_caqueta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Entorno Inteligente] 

Un día después de la gran movilización por la  protección de la vida de  los líderes sociales en Colombia fue asesinada una lideresa, en Puerto Rico, Caquetá. Yissela Trujillo, se encontraba con su esposo, quien también falleció. Los sospechosos propinaron varios impactos  de bala contra Trujillo y su pareja.

(Le puede interesar:["Asesinan a excombatiente de FARC, Carlos Alberto Montaño en Cali"](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali/))

Herner Careño, personero de Puerto Rico Caquetá,  afirma que "La Unidad para la Atención Integral de Víctimas consideró a  Trujillo como víctima del conflicto armado, por despojo de tierras, amenazas y desplazamiento forzado". Según el personero del municipio hay  presencia de grupos armados organizados y delincuencia común.

Noticia en desarrollo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
