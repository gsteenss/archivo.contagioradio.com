Title: Festival 'Off', cuando el teatro habla de Postconflicto
Date: 2016-03-11 16:20
Category: Cultura, Hablemos alguito
Tags: Festival Off de Teatro, Teatro Ditirambo, Teatro la mama
Slug: festival-off-cuando-el-teatro-habla-de-postconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/FESTIVAL-OFF-Página-Web-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teatro La Mama 

<iframe src="http://co.ivoox.com/es/player_ek_10768520_2_1.html?data=kpWkmJ2ZdpGhhpywj5qWaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncafZ1NnW2MbQb4ammLTTyIqWe4ampJDQ18bSqNCfxtGY1srFuNPjjM3OxNHFb8XZjLXc1dnHs8%2Fazc7Q1tSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Hablemos Alguito, Teatro la Mama] 

###### [11 Mar 2016] 

Más de 50 compañías, entre nacionales e internacionales, se darán cita desde este 11 y hasta el 27 de Marzo en la primera versión del ‘Festival Off de teatro’ de Bogotá, una apuesta común de los teatros La Mama y Ditirambo por aportar al postconflicto desde las artes escénicas.

“Del post conflicto estamos hablando los hacedores de teatro desde hace unos años”, asegura Rodrigo Sánchez, actual director del Teatro La Mama, histórica compañía que ha venido trabajando temas sensibles para la visibilización y la memoria en obras como “Andite impulso al retorno” que aborda la problemática del desplazamiento forzado en el país.

La creación de un festival “Off”, denominación existente en varias ciudades del mundo, responde a la necesidad de reunir y mostrar esas obras que se han hecho y desde las que se abre “una puertica a una posibilidad de solución” aprovechando la oportunidad histórica que según el dramaturgo brinda el estar “adportas de una firma de paz”.

La unión con Rodrigo Rodríguez, director del Teatro Ditirambo, facilitó el que hoy el Festival Off sea una realidad, el pensamiento común fue “hagamos un Festival sin ánimo de hacer competencia a nadie” teniendo en cuenta que por las mismas fechas se desarrollan los Festivales Iberoamericano y Alternativo de Teatro en la ciudad de Bogotá.

Serán más de 100 presentaciones en 8 escenarios de la ciudad entre salas y espacios alternativos como el Teatro de los Sueños, ubicado en la estación de trenes de la sabana y la Escuela Taller de Bogotá al Aire libre, Los teatros La baranda, Casa Tea, Ditirambo en sus dos sedes y La Mama, donde las delegaciones de España y Argentina, países invitados de honor, compartirán escena con agrupaciones provenientes de Cartagena, Cali y Pereira entre otras.

Alternando con la [programación teatral](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Programación-Festival-Off.jpg), se realizarán varias charlas relacionadas con el tema del postconflicto por parte de diferentes personalidades de la cultura y la Academia, buscando entablar diálogos que aporten a la construcción de caminos, con la inclusión de diferentes pensamientos y perspectivas de realidad.

La entrada general para las funciones, tiene un costo de 25.000 pesos y 15.ooo para estudiantes con carnet. Para mayor información ingrese a www.teatrolamama.com.co y www.teatroditirambo.org

   
 
