Title: Atentan contra la vida de líder indígena Wayúu, en Rioacha-Guajira
Date: 2018-04-30 16:43
Category: DDHH, Política
Tags: Desnutrición Infaltil, Lider social, Pueblo Wayúu
Slug: atentan-contra-la-vida-de-lider-indigena-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/lider.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Guajira] 

###### [30 Abr 2018]

El movimiento indígena Nación Wayúu denuncio, a través de un comunicado de prensa, que Edwin Ceballos Sijana, líder e integrante de esta organización, fue víctima de un atentado el pasado 29 de abril en el barrio los Nogales, la ciudad de Rioacha, Guajira, cuando se desplazaba en su esquema de seguridad que **fue atacado por dos hombres que se movilizaban en una moto y dispararon contra el vehículo**. Ceballos salió ileso del atentado.

Los hechos se registraron, según la misiva, tres días después de que Ceballos de manera pública, denunciara al Instituto Colombiano de Bienestar Familiar regional Guajira  por su omisión al no prestar ayuda oportuna a una niña con síntomas graves de desnutrición que se encontraba en su comunidad y afirmó que la misión de esta organización, no se percató de la situación de la menor, debido a que **"fueron a pasear y tomarse fotos"**. La menor de edad fue trasladada al centro hospitalario más cercano, en donde fue atendida.

Para el Movimiento Nación Wayúu, esta situación hacen parte de una persecución sistemática en contra de los líderes y las autoridades tradicionales, por denunciar la muerte de niños debido a la desnutrición y el tráfico de cupos de niños al interior del ICBF.

A estos hechos se suman las denuncias realizadas por el Defensor del Pueblo, Carlos Negret, quien a mediados de abril afirmó que en lo corrido del año se han presentando 24 casos por desnutrición infantil y afirmó que esta cifra, si no se toman medidas urgentes podría aumentar a final del año a 64 niños. (Le puede interesar: ["Pueblo Wayúu a punto de desaparecer por incumplimiento Estatal"](https://archivo.contagioradio.com/pueblo-wayuu-al-borde-del-exterminio-por-incumplimiento-estatal/))

###### Reciba toda la información de Contagio Radio en [[su correo]
