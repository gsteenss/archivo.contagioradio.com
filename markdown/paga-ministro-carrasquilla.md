Title: 'El que la hace la paga' no aplica para el ministro Carrasquilla
Date: 2018-09-19 13:20
Author: AdminContagio
Category: Entrevistas, Política
Tags: Bonos Agua, Carrasquilla, deuda, Robledo
Slug: paga-ministro-carrasquilla
Status: published

###### [Foto: @PoloURosario] 

###### [19 Sep 2018] 

Tras el debate de control político promovido por congresistas del **Polo Democrático**, en cabeza del senador **Jorge Enrique Robledo,** algunos parlamentarios y figuras políticas pidieron que el Ministro de Hacienda saliera de su cargo por renuncia del propio Alberto Carrasquilla, o aplicando la frase del gobierno **Duque "El que la hace la paga".** (Le puede interesar: ["La radiografía del derecho fundamental al agua en Colombia"](https://archivo.contagioradio.com/la-radiografia-del-derecho-fundamental-al-agua-colombia/))

En el evento, Robledo afirmó que los 'Bonos del Agua' fueron un negocio de acuerdo a los términos legales, pero del que se obtuvo un lucro de mala manera, porque los préstamos se hicieron con **porcentajes de interés entre el 11 y 17%** teniendo en cuenta la inflación, mientras la utilidad del mercado en la época era de casi la mitad (6-8%). Adicionalmente, **el préstamo se acordó a 19 años sin posibilidad de pagar la deuda de forma anticipada.**

Los "Bonos del Agua" son créditos para la construcción de acueductos en 117 municipios del país, que según el senador Gustavo Petro, fueron impulsados por el **Grupo Financiero Infraestructura (GFI)** del cual fue asesor Carrasquilla. Por su parte, Robledo sostuvo que **la nación ya tuvo que pagar 495 mil millones para rescatar de la deuda a estos municipios**, a pesar de que el total de dinero prestado fue de 440 mil millones.

\[caption id="attachment\_56822" align="aligncenter" width="617"\]![Municipios afectados Bonos Carrasquilla](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-19-a-las-11.14.00-a.m.-800x601.png){.wp-image-56822 width="617" height="464"} Foto: @JERobledo\[/caption\]

### **Carrasquilla aumentó su patrimonio liquido en un 1.700%** 

Otra de las denuncias que formularon los congresistas que participaron del debate se centró en el beneficio[ obtenido por el GFI y por Carrasquilla en razón de la comisión recibida por] los Bonos. En el debate se afirmó que l**a ganancia del grupo estuvo cercana a los 9 mil millones de pesos (8.800),** pero no quedó resuelta la duda sobre cuánto de ese dinero llegó a manos de Carrasquilla.

La explicación la[[ pidió además de Robledo, **la representante a la Cámara por Bogotá, Katherin Miranda** qui[en]] reclamó a Ca]rrasquilla, a través de su cuenta en twitter, que aclare las sus declaraciones de renta, pues **entre 2007 y 2016, su patrimonio líquido aumentó en 2.625 millones de pesos**; hecho que lleva a pensar que el negocio de los Bonos del Agua fue muy bueno, o hubo otros negocios de los que obtuvo tal rentabilidad.

> El Ministro Carrasquilla a la salida de su ministerio tenía un patrimonio liquido de 75 millones y en el 2016 había aumentado a 2.700 millones, incremento de 1700% su patrimonio. Quisiera conocer la razón de ese aumento en su patrimonio. [pic.twitter.com/rWCR2lsjS9](https://t.co/rWCR2lsjS9)
>
> — Katherine Miranda (@MirandaBogota) [19 de septiembre de 2018](https://twitter.com/MirandaBogota/status/1042219706671869952?ref_src=twsrc%5Etfw)

### **"El que la hace no la paga"** 

<h3>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</strong>

</h3>
El presidente Iván Duque anunció que su programa de choque "El que la Hace la paga" sería el plan para atacar a bandas crimínales y la corrupción en el país, sin embargo, en el marco de ese plan, organizaciones sociales han denunciado la captura y judiciliazación de líderes, mientras Robledo señaló que **en el caso de que Carrasquilla continue ejerciendo como ministro de economía, el mensaje que el Gobierno mandaría al país es que "El que la hace, no la paga".** (Le puede interesar: "[7 líderes sociales son estigmatizados y encarcelados por Ecopetrol"](https://archivo.contagioradio.com/7-lideres-estigmatizados-ecopetrol/))

Durante el debate quedo abierta la forma en que podría tratarse la llamada "puerta giratoria" en la que ministros, durante su ejercicio, formulan leyes y posteriormente, asesoran a empresas que tienen intereses en los asuntos sobre los cuales legislaron. En el caso de Carrasquilla, Robledo denunció que c**uando fue ministro de Hacienda en el gobierno Uribe (2003-2007), promovió una modificación a la Constitución que permitió crear el Sistema General de Participaciones,** para que los municipios adquirieran créditos pagando tasas de interés a intermediarios, es decir, el **GFI.**

Mientras congresistas y figuras públicas piden la renuncia de Carrasquilla, y de parte del Centro Democrático se defiende al Ministro, según el periodista Daniel Coronell, **sólo 30 de los 117 municipios tienen algún avance en sus acueductos** y, seguirán pagando la deuda a su acreedor. (Le puede interesar: ["En la Guajira viven con menos de un litro de agua al día"](https://archivo.contagioradio.com/42837/))

<iframe id="audio_28732894" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28732894_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
