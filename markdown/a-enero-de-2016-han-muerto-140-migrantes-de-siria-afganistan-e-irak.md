Title: 140 refugiados de Siria, Afganistán e Irak murieron en el primer mes de 2016
Date: 2016-01-31 08:10
Category: El mundo
Tags: ACNUR, Alemania, crisis migratoria, Europa, migraciones
Slug: a-enero-de-2016-han-muerto-140-migrantes-de-siria-afganistan-e-irak
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/08-07-2015Syria_Greece.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONU Noticias 

###### [31 Ene 2016.] 

Según cifras de **la ACNUR y la OIM, en lo que va corrido del 2016, han muerto 140 refugiados provenientes de Siria, Afganistán e Irak,** en un intento por cruzar el mar Egeo y entrar a Europa. Es decir, diez veces más que en enero del 2014 y dos veces más que en enero del año pasado, aproximadamente.

**En lo que va del año, han llegado 44.040 personas a las islas griegas y 2.200 a las costas italianas, para un total de 46.240 personas,** según cifras oficiales. Declaraciones de los refugiados señalan que para estos días los traficantes han bajado las tarifas de transporte, lo cual actúa como incentivo a asumir tal travesía sin importar los riesgos.

En medio de esta crisis de refugiados, el papel que ha desempeñado Grecia ha sido reconocido y admirado por diversos entes internacionales, pues su compromiso y atención humanitaria con los migrantes se ha destacado entre todos los países de Europa, respondiendo también a su condición de ser el puerto ideal para los refugiados por su cercanía a Turquía y el bajo costo de la ruta hasta su territorio.

Lastimosamente otros países de la Unión Europea han expresado su inconformismo por el tratamiento que Grecia le ha dado a la crisis y la han acusado de no respetar su obligación de controlar sus fronteras en forma estricta y abrieron la posibilidad de retirar a ese país del acuerdo Shengen.

Esta actitud se suma a una serie de medidas legales y actitudes adoptadas en varios países de Europa **entre las que se encuentra la ley de confiscación de bienes a refugiados, aprobada por el parlamento danés, la prohibición de entrada a refugiados en bares y discotecas** del suroeste de Alemania, la implementación de puertas rojas en las casas de refugiados con el fin de ser identificados y el ataque continuo a centros de albergue para migrantes en Alemania.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
