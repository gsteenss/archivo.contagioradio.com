Title: Asociación campesina del Catatumbo sin garantías para participar en paro nacional
Date: 2019-04-23 17:46
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: Asociación Campesina del Catatumbo, Catatumbo, ELN, Paro Nacional
Slug: asociacion-campesina-catatumbo-sin-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-22-at-2.13.59-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

La Asociación Campesina del Catatumbo (ASCAMCAT) anunció, mediante un comunicado, que saludaba el paro del próximo jueves 25 de abril, pero denunció que no podrá hacer parte del mismo porque las condiciones de seguridad de la región impiden que los integrantes de la Asociación ejerzan su derecho a la protesta. Incluso, según el relato de Juan Carlos Quintero quien integra la dirección de la organización, la última amenaza recibida ocurrió mientras hacían la convocatoria al paro.

De acuerdo al testimonio del Directivo, en el Catatumbo no hubo un cierre del conflicto: después de la firma del Acuerdo de Paz hubo una "tensa calma", y tras la salida de las FARC de los territorios, el Ejército de Liberación Nacional (ELN) y el Ejército Popular de Liberación (EPL) entraron en una puja por la zona. La situación derivó en enfrentamientos desde marzo de 2018, que según la ONU terminaron afectando a 150 mil personas. (Le puede itneresar: ["Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres"](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/))

Hacía final del año pasado el EPL se replegó, y las personas tuvieron que sufrir nuevamente por enfrentamientos entre el ELN y el Ejército, que tiene en operación a cerca de 15 mil hombres adscritos a la Fuerza de Tarea Vulcano y la Fuerza de Despliegue Rápido (FUDRA) 3. Sin embargo, la presencia militar no ha mejorado las condiciones de vida de los habitantes del Catatumbo; situación a la que se suma el precario avance del Programa Nacional Integral de Sustitución (PNIS) que no ha cumplido con las expectativas de los cultivadores de Coca.

### **Físicamente ASCAMCAT no puede participar en el paro**

> [\#DENUNCIA](https://twitter.com/hashtag/DENUNCIA?src=hash&ref_src=twsrc%5Etfw) ataque el 20 de abril Comisión de líderes ascamcat [\#Cisca](https://twitter.com/hashtag/Cisca?src=hash&ref_src=twsrc%5Etfw) y lider comunal en [\#SanCalixto](https://twitter.com/hashtag/SanCalixto?src=hash&ref_src=twsrc%5Etfw) x [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) a eso se suma grave denuncia d comunidad de Campo Alegre del asesinato e intento de desaparición de Dimar Torres excombatiente de [\#FARC](https://twitter.com/hashtag/FARC?src=hash&ref_src=twsrc%5Etfw) al parecer por [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) [pic.twitter.com/Lz0UaarcAP](https://t.co/Lz0UaarcAP)
>
> — AscamcatOficial (@AscamcatOficia) [23 de abril de 2019](https://twitter.com/AscamcatOficia/status/1120718465978052608?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En una región con tanta conflictividad, la Asociación decidió apoyar el Paro y sus reivindicaciones, entre ellas, el cumplimiento de los acuerdos alcanzados entre movimientos sociales y el Gobierno Nacional; la implementación del Acuerdo de Paz y la concertación del Plan Nacional de Desarrollo que responda efectivamente a las necesidades de la mayoría de colombianos. No obstante, reconocen que les es físicamente imposible participar activamente del paro. (Le puede itneresar: ["25 de abril, paro nacional por la paz y contra el Plan Nacional de Desarrollo"](https://archivo.contagioradio.com/25-abril-paro-nacional-paz-pnd/))

Como lo explicó Quintero, "la mayoría de la dirigencia de ASCAMCAT se encuentra desplazada desde octubre, hemos sufrido el asesinato de cinco de nuestros miembros, el atentado de dos más y las amenazas reiteradas", la última de ellas hace una semana, cuando hacían la convocatoria para participar en el paro. Por estas razones, a través de sus mecanismos internos, la Asociación decidió que no tenía las garantías ni los medios físicos para participar de la convocatoria.

\[caption id="attachment\_65360" align="aligncenter" width="612"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-22-at-2.13.59-PM-214x300.jpeg){.wp-image-65360 width="612" height="858"} ASCAMCAT señala que no tiene garantías para participar en el paro nacional del 25 de abril\[/caption\]

###### [English Version](https://archivo.contagioradio.com/peasant-association-of-catatumbo-has-no-guarantees-to-participate-to-the-national-strike/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
