Title: Marcha Patriótica definirá el 15 de mayo candidatos a elecciones del 2018
Date: 2017-04-21 18:28
Category: Entrevistas
Tags: elecciones, elecciones 2018, FARC, marcha patriotica
Slug: 15-de-mayo-marcha-patriotica-definira-candidatos-a-elecciones-del-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/marcha-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: annalisa melandri] 

###### [21 Abr 2017]

En el marco del quinto aniversario del Movimiento Social y Político Marcha Patriótica, durante el cual se hace una evaluación y una proyección del trabajo de esta plataforma de cara a la construcción de la paz, David Florez, uno de los voceros, anunció que a partir del 15 de Mayo se comenzará la elección de los **candidatos y candidatas con que participarán en la contienda electoral de 2018.**

Flórez destacó que esta decisión se enmarca en un proceso que desde el movimiento se ha venido gestando en el que están participando a nivel, local, regional y nacional. A partir de esa fecha habrá una reunión de la junta directiva que definirá los nombres de las personas y el tipo de listas aprovechando las nuevas reglas electorale**s** y abiertos a coaliciones con otros movimientos sociales y partidos políticos.

Flórez aseguró también que, en la idea de corregir errores del pasado, **están abiertos a la construcción de alianzas para superar la tradicional crítica a la izquierd**a sobre la incapacidad de unirse. El líder asegura que lo importante es que se tiene un objetivo claro y es no permitir que sigan gobernando los mismos de siempre y generar un cambio que permita la construcción de la paz.

De cara a la presidencia **“es necesario construir una confluencia por la paz, pero también contra la corrupción**” y en ese marco hay candidatos y candidatas muy importantes que bajo un panorama común podrían ser objeto de alianzas y de una propuesta de gobierno conjunta. Sin embargo, señalan que para las costumbres electoreras apostarán por unos principios éticos distintos, aunque son conscientes de que se enfrentan a una maquinaria electoral enraizada.

Además el dirigente recordó que Marcha Patriótica se ha convertido en un referente de construcción de paz y en ese marco de ideas **tienen abiertas las puertas a trabajar conjuntamente con el partido político de las FARC** que se definirá en Agosto próximo.

<iframe id="audio_18275630" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18275630_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
