Title: Con Néstor Humberto Martínez la Fiscalía quedaría en manos del poder empresarial
Date: 2016-04-21 17:13
Category: Entrevistas, Política
Tags: Fiscal General de la Nación, Fiscalía General de la Nación, Luis Carlos Sarmiento, Nestor Humberto
Slug: con-nestor-humberto-martinez-la-fiscalia-quedaria-en-manos-del-poder-empresarial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/nuevo-fiscal-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

<iframe src="http://co.ivoox.com/es/player_ek_11257211_2_1.html?data=kpafl5yWdZKhhpywj5aVaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaLgw8rf1tSPncbkxtiYj5Cns9DmxZOYpdTQs87WysaYp9rWs9HVjKrg1sbIs9SfttPWxtTXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Yepes] 

<iframe src="http://co.ivoox.com/es/player_ek_11257238_2_1.html?data=kpafl5yWd5mhhpywj5WdaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPi8LgzYqwlYqmd8%2BfjpCwpa%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gustavo Gallón] 

###### [21 Abril 2016 ]

De ser elegido Néstor Humberto Martínez como Fiscal General, el ente acusador quedaría **en manos de quienes representan el gran poder económico nacional y transnacional**, así lo aseguran el defensor de derechos humanos Alberto Yepes y el abogado Gustavo Gallón. Lo anterior debido a los estrechos vínculos de Néstor Martínez con Germán Vargas Lleras y empresarios como Luis Carlos Sarmiento Ángulo.

De acuerdo con Yepes, el proceso de transparencia y concurso de méritos para la elección de la terna fue una farsa, pues desde hace meses se sabía quienes iban a figurar. Por su parte Gallón asegura que Martínez debería declararse impedido ética y políticamente para ejercer el cargo, pues ser el "abogado de los grandes capitales nacionales e internacionales" no le da la imparcialidad necesaria para las investigaciones relacionadas con las graves violaciones a los derechos humanos, cometidas por empresas colombianas y extranjeras.

Para Yepes, la elección de Yesid Reyes, Mónica Cifuentes y Néstor Humberto Martínez, es resultado de la "puja" entre los partidos políticos que conforman la unidad nacional de la que salió victorioso 'Cambio Radical'.

En un Estado Social de Derecho el poder político no puede estar a favor del poder económico, porque puede suceder algo [[similar a lo que pasa con el Procurador](https://archivo.contagioradio.com/organizaciones-proponen-acciones-para-fortalecer-la-restitucion-de-tierras/)], **se debe rechazar que la persona encargada de investigaciones y sanciones esté "estrechamente comprometida con los sectores económicos"**,[** **pues ]la garantía de paz implica el desmonte de los grupos paramilitares. No han investigado a empresarios, ganaderos y políticos que han orquestado las acciones armadas, y justamente "ese es el gran reto, que no creo que él lo vaya a cumplir", asevera Yepes.

"El poder del dinero termina sometiendo a los ciudadanos y a la democracia" afirma el defensor, y expresa la incertidumbre por la decisión que pueda tomar la Corte Suprema de Justicia, de cara al posconflicto y teniendo en cuenta que **"el poder corruptor del dinero ha permeado todos los poderes del Estado"** y que "es muy difícil confiar en la Fiscalía cuando su encargado es el defensor de los poderosos", como concluye Gallón.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
