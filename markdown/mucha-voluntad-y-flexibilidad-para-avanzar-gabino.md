Title: "Mucha voluntad y flexibilidad para avanzar": Gabino
Date: 2017-01-23 17:54
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, ELN Nicolas Rodríguez Bautista, Negociaciones con el ELN
Slug: mucha-voluntad-y-flexibilidad-para-avanzar-gabino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/gabino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:La Silla Vacía] 

###### [23 Ene 2017] 

En respuesta a una carta enviada al Comando Central del ELN por intelectuales, académicos y personalidades democráticas, Nicolás Rodríguez Bautista alias Gabino comandante del ELN, emitió una misiva en la que señala que para avanzar en las negociaciones es fundamental “mucha voluntad y flexibilidad” y además asegura que “para el ELN colocarle tiempos a la paz es obstruirla”.

Para 'Gabino' es clave dar cumplimiento a los acuerdos y evitar “justificar conductas que solo ponen palos en la rueda” para lograr avances sustanciales. En la misma línea fueron las declaraciones de Juan Camilo Restrepo, jefe de la delegación del Gobierno quien coincide en que es mejor trabajar sin pausa pero sin imponer intereses y plazos a las negociaciones.

### La sociedad civil jugará un papel fundamental 

En la carta, Nicolás Rodríguez ratifica que la salida política a través del diálogo y la paz “es el único camino para Colombia” y que el ELN no escatimará en “ningún esfuerzo para ello”.

Sobre la participación de la sociedad, llama la atención sobre la importancia de que “la democracia entendida como el poder de las mayorías y los desarrollos de la paz, se ejerza para que ella defina la Colombia futura que se quiere construir para vivir en justicia social, democracia y soberanía” y que más allá de los intereses de las delegaciones deben ser “las mayorías siempre marginadas y excluidas quienes vean que sus sueños y esperanzas avanzan en ese proceso de paz, porque solo así el futuro dejará de ser incierto”.

Por último, el comandante del ELN señala que “la participación activa de la sociedad, sobre todo de la excluida” jugará un papel preponderante en las negociaciones y extiende la invitación a intelectuales, académicos y personalidades democráticas para que participen en la instalación de la fase pública de la Mesa de Diálogo en Ecuador el próximo 7 de Febrero.

[Carta de Nicolás Rodríguez-ELN a Intelectuales, académicos y personalidades democráticas](https://www.scribd.com/document/337356024/Carta-de-Nicola-s-Rodri-guez-ELN-a-Intelectuales-academicos-y-personalidades-democraticas#from_embed "View Carta de Nicolás Rodríguez-ELN a Intelectuales, académicos y personalidades democráticas on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_77521" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/337356024/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-zoCQ01PMcQVuFMmOMA6U&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
