Title: 35 mil hectáreas de tierras en Casanare pasarían a manos de empresas por Ley #Zidres
Date: 2016-03-02 21:23
Category: Movilización, Nacional
Tags: Casanare, Monocultivos, Zidres
Slug: campesinos-de-casanare-defienden-el-agua-y-dicen-no-a-ley-zidres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Campesinos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Noticias La Información 

<iframe src="http://co.ivoox.com/es/player_ek_10657192_2_1.html?data=kpWjl5yVfZOhhpywj5aaaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncZSpjNLWzpDMqcTohqigh6aVtsbV1JDRx5DYrcbm08bgjcrSb6TV1Mbbw9fJb9HV1Mbfh6iXaaK4wtOYw5DRpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cristóbal Torres, Defendamos a Casanare] 

###### [2 Mar 2016] 

Este miércoles campesinas y campesinos del departamento de Casanare, se concentraron en distintos puntos de la región, para movilizarse y mostrar su rechazo frente a la **Ley Zidres sancionada por el presidente Juan Manuel Santos.**

Desde las 7 de la mañana los manifestantes se reunieron en el aeropuerto de Yopal y en el sector de “La nevera”, al norte de Casanare con el objetivo de decir **NO a la entrega de sus tierras a terratenientes y, a su vez, para defender el agua de la región,** que ya está escaza por la acción de las empresa petroleras que habrían originado la muerte de cientos de animales que murieron por la falta de agua, como lo expresa el vocero de “Defendamos a Casanare”, Cristóbal Torres.

Según Torres, con la Ley Zidres, **35 mil hectáreas de esa zona pasarían a ser propiedad de multinacionales y transnacionales,** que usarían los suelos agrícolas del departamento para realizar monocultivos de palma, soya y maíz, para satisfacer necesidades de otros países, sin que el propio gobierno nacional aún le haya dado alternativas a los campesinos de Casanare para que tengan soberanía alimentaria.

El líder campesino, asegura que las movilizaciones continuarán pues sostiene que “sino tomamos acciones ya, **las plantaciones que van a hacer con la Ley van a provocar hambruna en Casanare”,** y añade que no se van a dejar desplazar por las empresas que lleguen a apropiarse de tierras gracias a la Zidres.
