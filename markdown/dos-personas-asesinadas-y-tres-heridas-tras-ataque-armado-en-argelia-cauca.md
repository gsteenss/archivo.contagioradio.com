Title: Crece a tres la cifra de  personas asesinadas en ataque armado en Argelia, Cauca
Date: 2020-11-10 11:51
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Argelia, Argelia Cauca, Cauca, Paéz, Paéz Cauca
Slug: dos-personas-asesinadas-y-tres-heridas-tras-ataque-armado-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Argelia-Cauca.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes se confirmó **la muerte de Yeison Fabián Jiménez Botina, un joven de apenas 15 años que resultó gravemente herido en el ataque registrado en la madrugada del lunes, en el municipio de Argelia, Cauca;** quien se encontraba internado en el Hospital Universitario de San José.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con Yeison se incrementa a tres, la cifra de personas asesinadas en el ataque en el que también perdieron la vida **Reinel Ijají de 31 años y Luis Evelio Quiroz Patiño de 46 años,** quien al parecer tendría algún tipo de parentesco con el alcalde de Argelia, Jhonatan Patiño; configurando así, una nueva masacre perpetrada en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El reporte inicial de la comunidad, hablaba de **dos personas asesinadas y otras tres que resultaron gravemente heridas -entre ellas Yeison Fabián Jiménez- tras un ataque armado perpetrado en el casco urbano del municipio de Argelia**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/marthaperaltae\/status\/1325802930721665025","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1325802930721665025

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho ocurrió hacia las 3:00 de la mañana, cuando, según testigos, llegaron cuatro hombres fuertemente armados en dos motocicletas, portando armas largas y fusiles, disparando de manera indiscriminada contra las personas que salían de un establecimiento ubicado a pocos metros de la estación de Policía del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Autoridades civiles como el alcalde, Jhonatan Patiño y el Secretario de Gobierno del Cauca, Luis Ángulo; rechazaron el crimen y anunciaron la realización de un nuevo consejo de seguridad en ese municipio; para estudiar medidas contra la **creciente ola de violencia, que solo en Argelia, ha dejado al menos 54 personas asesinadas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace apenas unos días, en el mismo municipio de Argelia, se registró el homicidio del líder social **Carlos Navia; miembro de** Asocomunal, a quien, irónicamente, asesinaron mientras coordinaba **la Caravana por la Vida del Cañón del Micay. (Lea también:** [Carlos Navia líder social de Asocomunal en Cauca, es asesinado](https://archivo.contagioradio.com/carlos-navia-lider-social-de-asocomunal-en-cauca-es-asesinado/)**)**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Argelia ha sido un territorio especialmente golpeado por la violencia, su cercanía con El Tambo y el Cañón del Micay, que han sido convertidos en una ruta estratégica de la criminalidad para llegar a las aguas del Pacífico, es un factor determinante para que **allí se libre una guerra entre el frente disidente de las FARC Carlos Patiño, el frente José María Becerra del ELN y grupos narcotraficantes asociados al grupo paramilitar del Clan del Golfo (autodenominado Autodefensas Gaitanistas de Colombia -AGC-).** **(Le puede interesar:** [Caravana Humanitaria del Cañón del Micay: “un canto por la vida y la paz del territorio”](https://archivo.contagioradio.com/caravana-humanitaria-del-canon-del-micay-un-canto-por-la-vida-y-la-paz-del-territorio/)**)**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Violencia desbordada en Argelia y el resto del Cauca

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este crimen se suma a la muerte del niño, José Pascue de tan solo 11 años asesinado con una bala perdida, en medio de enfrentamientos entre grupos armados y tropas del Ejército Nacional en el municipio de Páez, Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/FelicianoValen\/status\/1325629937936723968","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1325629937936723968

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Incluso, antes de conocerse sobre el ataque armado en Argelia, los ciudadanos del municipio de Patía en el Cauca, salieron a manifestarse en la noche de este domingo, ante la ola de violencia y los asesinatos presentados en su municipio y en el departamento.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/Ruben723\/status\/1325625428984418310","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ruben723/status/1325625428984418310

</div>

<figcaption>
[Ruben Zuñiga](https://twitter.com/Ruben723)

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
