Title: Ayacucho y La Mata en Cesar completan 37 días de paro cívico
Date: 2017-08-03 13:29
Category: DDHH, Nacional
Tags: Ayacucho, cesar, Derechos Humanos, Ecopetrol, la Mata, medio ambiente
Slug: comunidades-de-ayacucho-y-la-mata-en-cesar-completan-37-dias-de-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/WhatsApp-Image-2017-07-05-at-8.35.45-AM-e1501783033607.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilón] 

###### [03 Ago 2017] 

Las comunidades de Ayacucho y La Mata en Cesar **completan 37 días de paro indefinido por los incumplimientos de la empresa Ecopetrol** ante un pliego de exigencias de responsabilidad social, ambiental y laboral. Desde el 2011 han venido realizando paros y movilizaciones exigiendo que se satisfagan sus necesidades básicas y continúan denunciando las actividades “irresponsables de Ecopetrol”.

Por medio del Equipo Jurídico Pueblos, los habitantes de estas poblaciones denunciaron que **continúan sin trabajo, lo que ha dificultado la manutención de sus familias**. Igualmente, manifestaron que "la empresa se había comprometido a construir una carretera como parte de sus obligaciones sociales y no han cumplido, por el contrario, el territorio se ha deteriorado". (Le puede interesar: ["Continúa movilización de habitantes por incumplimientos de Ecopetrol en Ayacucho, Cesar"](https://archivo.contagioradio.com/defensor-de-dd-hh-denuncia-torturas-del-esmad-durante-movilizacion/))

“**Dentro de la zona de influencia los colegios están en pésimo estado,** como el de la vereda Las Nubes, el Callo Guayabo y otros, los cuales llevan dos años de construidos sin que las administraciones hayan terminado el proceso de equipamiento”. Denuncian que estos colegios “reportan inversiones de millones de pesos dejando por el contrario unos elefantes blancos”.

En el tema ambiental, las comunidades han sido enfáticas en que la planta de Ecopetrol, **“vierte sus residuos sin ningún permiso y contamina la quebrada San Martín”**. Aseguraron que desde hace 10 años han deforestado la región “dejando la quebrada El Cuaré totalmente seca y ahora es un vertimiento de aguas residuales en Ayacucho”. (Le puede interesar: ["Abogado y periodista golpeados por Esmad en protesta de Puerto Ayacucho en Cesar"](https://archivo.contagioradio.com/esmad-habria-golpeado-y-detenido-una-periodista-un-abogado-y-un-campesino-en-cesar/))

Finalmente, las comunidades aseguraron que el **paro continúa hasta tanto se cumpla lo pactado en el pliego**. Señalaron que la respuesta de Ecopetrol, de la Alcaldía de la Gloria, de las autoridades policiales y militares “ha sido siempre la misma: represión y más represión”. Igualmente manifestaron que ha habido montajes judiciales, detenciones arbitrarias y deslegitimación de la población al asociarlos con la guerrilla.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
