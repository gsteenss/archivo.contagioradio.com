Title: La otra verdad sobre la muerte de Pedro Juan Moreno
Date: 2018-02-25 13:48
Category: DDHH, Judicial
Tags: Alvaro Uribe, compulsa de copias contra Uribe, Fabio Echeverri, Pedro Juan Moreno
Slug: la_otra_verdad_pedro_juan_moreno_uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/join-us-for-a-ride-21-e1519582845283.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Otra Cara / Kienyke] 

###### [ 25  Feb 2018] 

["Me duele en el alma", fue lo que expresó el senador Álvaro Uribe, frente a las nuevas pruebas en su contra relacionadas con su posible participación en la muerte de Pedro Juan Moreno, quien lo acompañó durante tres años en la secretaría cuando fue gobernador de Antioquia.]

[Han pasado doce años del supuesto accidente pero cada vez son más las preguntas. ¿Por qué versiones de paramilitares señalan una manipulación del helicóptero? ¿Para qué hacer un señalamiento como ese sin ninguna justificación? ¿Qué razones podría tener el ex presidente para provocar la muerte de Moreno? ¿Qué pasó con la relación entre Moreno y Uribe? ¿Por qué resulta asesinada una trabajadora del aeropuerto?]

### **El papel de Moreno en la gobernación de Antioquia** 

[Pedro Juan Moreno era un político de corte conservador, cuyos inicios se dieron como concejal de Medellín. Luego para el año 1990 obtuvo una curul en la Cámara de Representantes por Antioquia, y **en 1995 llega a ser el secretario de la gobernación de ese mismo departamento por invitación del entonces gobernador elegido Álvaro Uribe Vélez,** periodo durante el cual se recrudecieron la actuaciones paramilitares en toda la región.]

[Moreno junto al expresidente **crearon y promocionaron las CONVIVIR** que, según investigaciones, luego mutaron a grupos paramilitares. Pedro Juan fue clave en el diseño del modelo promovido en Antioquia para enfrentar a la insurgencia, que luego se extendió en todo el país, con la llamada seguridad democrática cuando el hoy senador, fue elegido como presidente de Colombia en el año 2002.]

[**“Esas reuniones de área de inteligencia de seguridad que a mi me tocaba organizar y presidir en muchas ocasiones fue una experiencia muy importante para mi,** porque comprendí la problemática de orden público del departamento, me tocaba coordinar todas las operaciones para derrotar a los violentos”, manifestó Pedro Juan en la última entrevista con Juan Gonzalo Ángel y Charlie Echavarria en el canal antioqueño Paisa Visión.]

### **Sobre el asesinato de Juan María Valle** 

[En la sentencia contra Jaime Alberto y Francisco Antonio Angulo, por el asesinato del defensor de Derechos Humanos, Jesús María Valle, se señala que, “Frente a la materialidad del homicidio esta no admite discusión, el asesinato del doctor VALLE JARAMILLO está plenamente demostrado, la razón del mismo es clara: las denuncias hechas por El de la coparticipación en las masacres de la GRANJA y EL ARO por parte de grupos paramilitares, el Ejército, la Policía y la misma Gobernación de Antioquia de ese entonces”.]

![sentencia 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/sentencia-1-600x531.png){.wp-image-51759 .aligncenter width="544" height="481"}

[Incluso más adelante se habla de la responsabilidad directa de Pedro Juan Moreno, y sus posibles vínculos con grupos **paramilitares que operaban** **con la omisión tanto de la Gobernación de Antioquia como de la Policía y el Ejército.**]

[En el documento también se lee que Diego Fernando Murillo, alias don Berna, habría dicho que el asesinato del defensor de derechos humanos, se coordinó por petición de **Pedro Juan Moreno al jefe paramilitar Carlos Castaño,** con quien Moreno se habría reunido en la hacienda “53” del corregimiento EL Volador, y junto a varios militares y funcionarios de la Gobernación, se decidió acabar con la vida de Valle con una orden a la banda de la Terraza.]

[“Obvio que las denuncias realizadas por VALLE en nada le convenían a los Paramilitares, como tampoco a la Gobernación, la Policía y el Ejército, por ello la orden de CASTAÑO fruto de una reunión de SAN JOSÉ DE LA MONTAÑA, en presencia de los ANGULO, de darle muerte al DR. VALLE”, dice la sentencia.]

[En esa línea también se han investigado los vínculos de Pedro Juan y Álvaro Uribe con las masacres de El Aro y La Granja teniendo en cuenta que Jesús María Valle había denunciado la colaboración de militares de la IV Brigada en dichas masacres. La misma semana de su asesinato, Valle había rendido versión libre en el proceso por injuria y calumnia que en su contra iniciaron miembros del Ejército por estas mismas denuncias.]

[De hecho, una columna reciente de Daniel Coronel titulada “Aquí no hay muertos”, da cuenta de que tres semanas antes de que los sicarios acabaran con la vida del defensor de derechos humanos,  éste había declarado ante un fiscal regional: “Yo siempre vi y así lo reflexioné que había como un acuerdo tácito o como un ostensible comportamiento omisivo, hábilmente urdido e**ntre el comandante de la IV Brigada, el comandante de la Policía de Antioquia, el doctor Álvaro Uribe Vélez, el doctor**]**Pedro Juan Moreno y Carlos Castaño”.**

[Esta versión de la historia concuerda con declaraciones de “Don Berna”, quien en una versión libre había dicho que el asesinato de Jesús María Valle había sucedido “a petición del doctor Pedro Juan Moreno, ya que él (Valle) estaba haciendo una investigación sobre los hechos que ocurrieron en El Aro”. [( Le puede interesar: Compulsan copias contra Uribe por dos masacres en Antioquia)](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/)]

[Esa masacre ocurrió en 1997 el 22 de octubre. Según la Comisión Interamericana de DDHH, en ese hecho fueron asesinadas 15 personas. Declaraciones del paramilitar Francisco Barreto, también asesinado, afirman que el entonces gobernador de Antioquia, es decir, Uribe Vélez, habría usado un helicóptero de la gobernación en medio de la masacre y habría corroborado el hecho personalmente.]

[Ese modelo de seguridad en Antioquia impulsado por **Moreno, le generó fama de  “matón, de loco y atravesado”,** como había contestado en una entrevista con María Isabel Rueda para SEMANA, en donde además señalaba que no le molestaba que lo calificaran así.]

### **¿Qué pasó entre Uribe y Pedro Juan Moreno?** 

[Aunque se esperaba que Moreno fuera integrante del gabinete de Uribe, cuando este fue elegido presidente, no fue así, ahí empezó a evidenciarse el distanciamiento entre los dos.]

[Incluso en la entrevista del canal Paisa Visión,  Moreno asegura que Uribe fue buen patrón mientras él fue su secretario de gobierno**.“En ese tiempo era buen patrón, pero la gente cambia con el tiempo”,** y después afirma que cuando el hoy senador fue elegido presidente, Pedro Juan no quiso aceptar ser embajador y prefirió develar la corrupción del país a través de su revista ‘La Otra Verdad’.]

[No es un secreto que Moreno fue uno de los principales promotores de la primera elección presidencial de Álvaro Uribe. Pero manifestó que no le gustó que Uribe estuviera buscando su reelección en 2006 cuando en campaña se había mostrado contrario a esta medida.]

https://www.youtube.com/watch?v=\_S9-nQOmPBo

[Moreno decidió dedicarse a su revista dirigida y financiada por él, en donde el político conservador denunció varios casos de corrupción del gobierno de Uribe Vélez. Uno de ellos fue el precio exagerado del avión presidencial asegurando que esa adquisición le costaba 1700 millones de pesos mensuales a los colombianos. Un costo que según Moreno no se justificaba “Más andariego que el Papa no hay”, dijo Pedro Juan en la entrevista ya citada.]

[Asimismo, **realizó otras denuncias contra los asesores, ministros y viceministros del  mismo  gobierno.** Por ejemplo contra el el empresario Fabio Echeverri, uno de los principales  asesores presidenciales, y quien, según las investigaciones de ‘La Otra Verdad’, se habría robado 39 mil millones de pesos con la compra de unos caballos para la policía traídos desde Argentina. De acuerdo con Moreno, se trataría de caballos que ya estaban muy viejos y una vez llegaron a Colombia habían muerto.]

[Denuncias de casos de corrupción que en **dos años le habrían costado al país 2 billones de pesos,** decía. “Me retiré de la campaña de Bogotá porque no me gustó la gente que aterrizó allá. (Uribe) recurrió a las mismas prácticas politiqueras o peores”, aseveraba el entonces candidato al senado, y agregaba, “No quiero estar en el gobierno ni quise”.]

[Ante esa situación, cabe resaltar que en una entrevista a la emisora 'La W’ Álvaro Uribe expresó sobre Moreno,  “Yo lo aprecié, le tuve mucho cariño, él se distanció del Gobierno, fue muy duro con el Gobierno, pero procuré nunca responderle y me quedó la conciencia tranquila, que por muchos años, la única voz que defendía a Pedro Juan Moreno fui yo, y a él se le volteó el escenario. El día que ya **Pedro Juan empezó a convertirse en el gran acusador del Gobierno Nacional,** ya ese día para algunos sectores de los medios nacionales dejó de ser villano y pasó a ser héroe”.[(Le puede interesar: La telaraña de los falsos testigos de Álvaro Uribe)](https://archivo.contagioradio.com/la-telarana-de-falsos-testigos-y-colaboradores-de-uribe-velez/)]

### **Los hechos alrededor de la muerte** 

[La muerte de Pedro Juan Moreno se dio en extrañas circunstancias un 24 de febrero de 2006, en un accidente aéreo cuando se dirigía a Apartadó a continuar con su campaña para las elecciones legislativas de ese año. En el accidente murió Juan Gilberto Moreno, hijo del candidato al congreso, además de su asistente Ana María Palacio y el piloto.]

[Aunque en un principio la Aeronáutica Civil dijo que se había tratado de un accidente, cuatro años después, el 2 de junio de 2010, durante una audiencia pública contra el general (r) Rito Alejo del Río, procesado por vínculos con paramilitares, desmintió las versiones anteriores **sobre el fallecimiento de Pedro Juan Moreno y afirmó que había sido un asesinato.** Ante esas declaraciones la investigación se reabrió por orden del director seccional de fiscalías de Antioquia.]

[Según ha dado a conocer el periodista Daniel Coronel, en su más reciente columna, cuatro años pasaron y un informe del CTI de agosto de 2014 afirmaría que efectivamente, no había sido ningún accidente. “Hasta la fecha se ha avanzado en aspectos que eran desconocidos para la presente investigación y se avizora que es factible que la causa de la caída del helicóptero marca Bell, modelo 206B, de matrícula HK2496, de la empresa Helicargo S.A. piloteado por el capitán Jaime Taborda Botero, obedezca a acciones criminales que se hayan producido dentro del Aeropuerto Olaya Herrera, con anterioridad al despegue de la aeronave en mención”.]

[En enero del 2016, ante las declaraciones de Don Berna en las que aseguraba que Uribe hecho parte de un supuesto sabotaje al helicóptero de Moreno, la Fiscalía General compulsó copias ante la Sala Penal de la Corte Suprema de Justicia y la Comisión de Acusaciones de la Cámara de Representantes para que se investigara al hoy senador del Centro Democrático. [(Le puede interesar: "Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar")](https://archivo.contagioradio.com/?p=40551)]

[Pero además, Daniel Coronel, reveló en otro columna que en correos entre del exjefe paramilitar, alias "Don Berna" y la investigadora Maria McFarland  autora del libro  'There Are No Dead Here: A Story of Murder and Denial in Colombia (Aquí no hay muertos: una historia de asesinato y negación en Colombia)', se mostraría que efectivamente si había sido asesinado Pedro Juan Moreno.]

[“Sobre Pedro Juan Moreno, lo conocí personalmente ya que era un asiduo visitante de los campamentos de las Autoefensas, concretamente de un sitio llamado 21 donde funcionaba el cuartel general de Carlos Castaño. El era uno de los consejeros de dicho comandante. La muerte de El fue producto de un saboteo al Elicoptero (sic) donde se movilizaba. Acción llebada (sic) a cabo por órdenes de Uribe”, decía dicho correo. No obstante este 14 de febrero, Uribe dio a conocer una carta cuya autoría atribuyó a "Don Berna", en la que este afirmaba que el expresidente no había tenido nada que ver con el asesinato de Moreno, y que todo había sido producto de “un chisme”.]

\[caption id="attachment\_51761" align="alignnone" width="600"\]![Daniel Coronel / Revista Semana](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/don-berna-600x114.png){.size-large .wp-image-51761 width="600" height="114"} Daniel Coronel / Revista Semana\[/caption\]

[Finalmente cabe recordar en la columna de Daniel Coronel ya mencionada, se tiene también el hecho del asesinato de **Nancy Esther Zapata** ocurrido en noviembre de 2007. Esta mujer era la propietaria de un almacén de repuestos de aviación del Aeropuerto Olaya Herrera, de donde partió el helicóptero en el que iba Pedro Juan Moreno. Ella, **había manifestado en un correo electrónico la posibilidad de que el helicóptero hubiera sido saboteado.** [(Le puede interesar: Álvaro Uribe rehén de su propia táctica)](https://archivo.contagioradio.com/?p=21656)]

[ “Acá se roban todo, los repuestos de las avionetas y helicópteros se los quitan y los venden sin importarles que se caigan con gente adentro, el vendedor de seguros tiene todo arreglado y les pagan. Me da miedo contarlo a la policía y militares porque ellos vienen acá por comisión por cruces de los repuestos que les roban a los mismos aviones y heli de ellos”, decía el testimonio de Zapata. Sin embargo, días después apareció asesinada con un disparo en la nuca y con una nota en el que se leía **“aquí no se aceptan sapas”.**]

###### Reciba toda la información de Contagio Radio en [[su correo]
