Title: Postergan juicio contra Milagro Sala por pedido de prescripción
Date: 2016-04-20 15:50
Category: El mundo, Otra Mirada
Tags: Argentina, milagro Salas
Slug: postergan-juicio-contra-milagro-sala-por-pedido-de-prescripcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/milagro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Infobae 

###### [20 Abr 2016]

El juicio que debía iniciar este jueves por una causa iniciada en 2009 en contra de la líder social argentina Milagro Sala, fue postergado para la semana que viene, por existir un 'pedido de prescripción' que no ha sido resuelto.

El proceso legal, **diferente al que provocó su detención a principios de año**, obedece a la denuncia formulada hace 7 años  por el entonces senador y **actual gobernador de la provincia de Jujuy, Gerardo Morales**, acusándola por daño agravado y amenazas.

La denuncia, por la que también deben responder los militantes Graciela López y Ramón Salvatierra, esta relacionada con **el escrache con huevos** que habrían realizado durante un acto político de Morales en el Consejo Profesional de Ciencias Económicas.

De acuerdo con lo publicado por el portal minutouno.com, a pesar haber sido señalada como instigadora, Sala no habría estado presente en el momento de los hechos.

Cabe recordar que **Gerardo Morales, es el mismo impulsor de la causa por la que Milagro Sala permanece en prisión**, luego que esta encabezara una protesta contra los cambios impuestos desde la gobernación al sistema de cooperativas, que fueron compulsados como "instigación a cometer delitos", "tumulto en concurso real" y "presunta malversación de fondos".

A pesar de haber sido solicitada la excarcelación de la líder de Tupác Amaru y de las diferentes movilizaciones y acampadas de organizaciones sociales por su libertad, la  Corte Suprema de Justicia Argentina, rechazo el pedido presentado por el abogado Emilio Villar, sustentado en los fueros de Sala como parlamentaria de Mercosur.
