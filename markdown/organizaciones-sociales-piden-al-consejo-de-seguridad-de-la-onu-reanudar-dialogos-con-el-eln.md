Title: Organizaciones sociales piden al Consejo de Seguridad de la ONU reanudar diálogos con el ELN
Date: 2019-07-13 20:29
Author: CtgAdm
Category: Nacional, Paz
Tags: Consejo de la ONU, diálogos con el ELN, Organizaciones sociales
Slug: organizaciones-sociales-piden-al-consejo-de-seguridad-de-la-onu-reanudar-dialogos-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-sociedad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MisionONUCol  
] 

Durante la visita que el **Consejo de Seguridad de las Naciones Unidas (ONU)** está realizando al país para verificar la implementación del Acuerdo de paz, diversos representantes de la sociedad civil y la iglesia católica plantearon a los embajadores la posibilidad de reanudar los diálogos con la guerrilla del ELN y reiteraron la necesidad de que el Gobierno se siente a negociar.

**“En varias intervenciones se planteó la necesidad de que el Gobierno y el ELN retomen la mesa de negociaciones y que eso sea acompañado de Naciones Unidas”,** mencionó monseñor Héctor Fabio Henao, quien también les expresó su preocupación al Consejo sobre los asesinatos de líderes sociales y excombatientes de FARC, sucesos que como indicó el religioso, han impedido la implementación integral del acuerdo.

### ONU debe continuar con su misión en Colombia

Por su parte, el director de la **Comisión de la Verdad, el padre Francisco de Roux** quien se reunió con el Consejo junto a junto a los demás representantes del Sistema de Verdad, Justicia , Reparación y no Repetición, se dirigió a los embajadores de la ONU y les pidió “que no se retiren de la tarea que han hecho hasta ahora de tanta solidaridad con Colombia, que continúen al lado de este país y de las victimas".

> Representantes del Sistema de Verdad, Justicia y Reparación con el Consejo de Seguridad insistieron en importancia de respetar integralidad del [\#AcuerdoDePaz](https://twitter.com/hashtag/AcuerdoDePaz?src=hash&ref_src=twsrc%5Etfw), superar polarización violenta y asegurar recursos para el desarrollo de sus acciones a nivel territorial. [pic.twitter.com/fzuyGnC3oj](https://t.co/fzuyGnC3oj)
>
> — Misión de la ONU en Colombia (@MisionONUCol) [July 13, 2019](https://twitter.com/MisionONUCol/status/1149840402289188865?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Representando a la plataforma Defendamos la Paz, el exministro Rodrigo Rivera indicó que todo los sectores coincidieron en que **la Misión de Naciones Unidas debe permanecer en Colombia acompañando la implementación del acuerdo de paz el tiempo que requiera esta labor,** manifestando que un año es un tiempo muy corto para hacer la verificación. [(Lea también: Fundación Cultura de Paz llama al Estado colombiano a cumplir lo pactado con el ELN)](https://archivo.contagioradio.com/fundacion-cultura-de-paz-colombiano/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
