Title: Encuentros juveniles: Motores del paro nacional
Date: 2019-12-06 16:52
Author: CtgAdm
Category: Movilización, Nacional
Tags: ANJECO, estudiantes, jovenes, juventud, Paro Nacional, Universidades
Slug: encuentros-juveniles-motores-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ESTUDIANTES-MARCHAN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo.] 

[Este jueves 5 de Diciembre se realizó en Bogotá  el Encuentro Juvenil Distrital, o Asamblea Juvenil Distrital, en la que se discutieron varios temas respecto al papel de los jóvenes y su responsabilidad dentro de la situación actual y más en concreto frente al Paro Nacional que completa 15 días. Para Alejandra Sanchez, integrante de ANJECO, una de las organizaciones participantes, es importante resaltar la «descentralización» que se ha vivido en este momento al integrar “los encuentros asamblearios de distintos lugares de la ciudad”. ]

[“Los jóvenes, por un lado, hemos aportado gran fuerza a la movilización —explica Alejandra— pero hemos sido relegados de la disputa política dentro de ese pliego nacional”. Las distintas organizaciones de trabajadores y demás sectores que se han unido al Paro Nacional no han tenido en cuenta las exigencias de los estudiantes. ]

### **Las solicitudes de la juventud en el Paro Nacional**

[Para remediar eso, la Asamblea integró los puntos centrales para agregar al pliego nacional, entre los que se encuentran **el derecho a un trabajo digno, el derecho a la educación, la soberanía económica y alimentaria, el tema de democracia y participación, juventud y territorio, entre otros. **]

[Uno de los ejes centrales fue la oposición a la brutalidad policial y la exigencia al Gobierno de Iván Duque de desmontar el Escuadrón Móvil Antidisturbios (ESMAD). Todo esto por respeto a las víctimas del ESMAD, el derecho a la protesta y en  memoria de Dylan Cruz que, como menciona Sánchez, “nos lo arrebató la Policía Nacional”.]

Dentro de las exigencias de los jóvenes, además del derecho a la educación gratuita y de calidad, también están la oposición a las corridas de toros, la protección de los humedales en las zonas urbanas, el trabajo digno y con remuneración justa para los menores de 25 años entre otras que conciernen a este y otros sectores poblacionales.

### Así seguirá la movilización de los jóvenes durante Diciembre 

[Para la Asamblea, lo más importante es que “los jóvenes aporten desde sus particularidades”, ya que desde el movimiento estudiantil y la ANJECO se propone seguir con las movilizaciones hasta el otro año. Los eventos que siguen son los siguientes:]

-   [Siete de diciembre se realizará una velatón en el Centro de Memoria Histórica y]
-   [Diez de diciembre un plantón frente al Ministerio de Justicia, este último en contra de la brutalidad policíaca y en homenaje a Dilan Cruz. ]

### **Plnatean proceso para vincular a la juventud más allá de la universidad**

[Los “comités de paro por localidades tienen que nutrirse y fortalecerse con los escenarios más grandes”, afirma Alejandra, en busca de unir todas las voces de la ciudad que no se encuentran únicamente dentro de las universidades. [(Le puede interesar: Desfinanciación del SENA afecta a más de 7000 estudiantes)](https://archivo.contagioradio.com/33-billones-debe-minhacienda-sena/)]

[El problema más grave para la juventud, dentro de las dinámicas del Paro Nacional, es el problema de la representatividad. “Actualmente dentro del ejercicio de comité de paro existe un sector solamente representativo de lo estudiantil. No hay una vocería general”. ]

[La importancia de tomar a la juventud como un conglomerado, en vez de incluirla únicamente dentro del movimiento estudiantil, es que la participación colectiva surge de distintas fuentes. “Hay algo muy positivo del movimiento estudiantil y es que sale de la universidad y va a los barrios a hacer trabajo desde adentro, **atrayendo otros jóvenes lejos de la dinámica universitaria, como aquellos que trabajan o están inmersos en otro tipo de escenarios”. **]

[Lo más importante aquí, en materia de la Asamblea, es “ampliar el llamado a todos los jóvenes para que podamos participar”. Esto con el objetivo de empezar a generar un “balance de la realidad, buscando que los jóvenes vayan más allá de las movilizaciones, convirtiéndose en sujetos políticos”. ]

### **Representación política joven**

[Para Alejandra, las bases de una representación ejemplar necesitan tiempo. “Nosotros decimos que este es un movimiento nuevo que tiene que empezar a forjarse progresivamente. No podemos atarlo a unas representaciones sin construirlo desde las bases, y así poder replicar estos escenarios asamblearios, lograr una legitimidad como liderazgo en los territorios y pensar en unos representantes a futuro”.]

[“Los jóvenes aportan en este Paro una gran fuerza y nos plantean un debate como juventud que trasciende las universidades”. Para la Asamblea Distrital, como para las asambleas locales, es momento de **seguir trabajando en busca de una representación clara** frente a las demás organizaciones y un pliego de solicitudes que enmarque a toda la juventud del país y a sus problemas. ]

[Por ultimo, Sánchez resalta la importancia de detener la estigmatización que sufren los jóvenes en las movilizaciones y las protestas sociales, ya que “cuando se asocia juventud se termina estigmatizando a los marchantes”.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
