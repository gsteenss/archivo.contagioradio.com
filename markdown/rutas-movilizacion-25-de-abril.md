Title: Las rutas de la movilización que paralizará el país este 25 de abril
Date: 2019-04-24 16:59
Author: CtgAdm
Category: Paro Nacional
Tags: Bogotá, Marchas, Paro Nacional, Ruta
Slug: rutas-movilizacion-25-de-abril
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-24-at-3.55.39-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[  
Especial Paro Nacional  
](http://bit.ly/especialparonacional)

###### Foto: Contagio Radio 

[  
](https://archivo.contagioradio.com/paro-nacional-6/)Este 25 de abril se desarrollarán marchas y plantones en diferentes regiones del país en apoyo al paro, al que ya se han sumado organizaciones de trabajadores, campesinos, transportadores, pensionados, estudiantes y profesores; por esta razón, se espera que las marchas tengan una gran cantidad de personas convocadas. Durante la jornada se tomarán capitales y ciudades principales de los departamentos, aquí algunos de los puntos de concentración:

**En Bogotá** la concentración será a partir de la 1 p.m. en la Plaza de Bolivar, lugar al cual llegarán las marchas convocadas desde algunas universidades, y principalmente desde el Centro Administrativo Distrital (Carrera 30 con Calle 26) desde las 9 de la mañana.

**En Medellín** la concentración será en el Parque de los deseos desde las 9 de la mañana.

**En Cali** el lugar de encuentro será la Glorieta de Sameco a las 8 de la mañana.

**Barranquilla** se concentrará para una marcha en el INEM desde las 9 de la mañana.

**En Popayán,** la jornada iniciará en el Claustro de Santo Domingo a las 9 de la mañana.

**En Yopal** la movilización iniciará en el Parque El Resurgimiento, a las 8 de la mañana.

**En Buenaventura** la marcha iniciará a las 8 de la mañana en el Puente del SENA y culminará en la Alcaldía.

De igual forma, este 25 de abril a nivel internacional se desarrollarán eventos en apoyo al paro nacional en algunos países de américa latina como Venezuela, Argentina, Brasil, Perú, Chile y Cuba.

\[caption id="attachment\_65472" align="aligncenter" width="630"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-24-at-3.55.39-PM-212x300.jpeg){width="630" height="892"} La agenda internacional en apoyo al paro cívico de Colombia\[/caption\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
