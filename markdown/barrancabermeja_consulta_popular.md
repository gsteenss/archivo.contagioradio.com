Title: En Barrancabermeja esperan que propuesta de consulta popular no sea un acto de "politiquería"
Date: 2018-04-19 16:07
Category: Ambiente, Nacional
Tags: Barrancabermeja, Ecopetrol, La Lizama
Slug: barrancabermeja_consulta_popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Barrancabermeja-e1524171756862.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gobernación de Santander] 

###### [19 Abr 2018]

Bastantes dudas causa en la población de Barrancabermeja y en los ambientalistas la propuesta del alcalde de Barrancabermeja, Darío Echeverry, sobre la realización una consulta popular con la que se buscaría impedir la explotación y exploración petrolera en ese municipio.

De acuerdo con Óscar Sampayo, habitante de Barrancabermeja y líder ambientalista, si bien la comunidad aplaude la ola de consultas en el país para defender los territorios,  temen que, **al ser una iniciativa de un alcalde con un prontuario de corrupción, sea un anuncio que únicamente se de con fines de politiquería.**

Sampayo, recuerda que Echeverry fue capturado luego de ser señalado por impedir que la ciudadanía participara en la jornada de su revocatoria.  Aunque quedó en libertad a inicios de abril, la Fiscalía continúa investigándolo por los delitos de perturbación al certamen democrático, constreñimiento al sufragante, corrupción de sufragante y concierto para delinquir.

"Nos genera inquietud, y tomamos con cautela esta propuesta del alcalde que tiene ese prontuario. Vemos con escepticismos esto, ya que puede ser una estrategia politiquera del Partido Liberal, para buscar réditos políticos", dice Sampayo, quien resalta que **eso no quiere decir que se opongan estos mecanismos de participación ciudadana.**

Por su parte, el alcalde, en entrevista en la emisora Blu Radio, manifestó que se trata de una propuesta con la que se busca impedir el desarrollo de métodos de exploración convencionales y no convencionales como el fracking. Asimismo expresó que en Barrancabermeja, su gobierno ha enfocado su plan en "**despetrolizar la economía, pero mientras tengamos petróleo debemos aprovecharlo".**

### **100 de años de industria petrolera ** 

Actualmente, Barrancabermeja tiene todo el territorio adjudicado a bloques petroleros. De hecho, **en abril se cumplen 100 años de la llegada de la industria petrolera, por lo que Sampayo asegura que desde**  siempre en el municipio se han impuesto los proyectos minero-energéticos.

Además, en ese territorio también preocupa que no solo se realizan actividades petroleras, sino que en el Parque Nacional Natural Serranía de los Yariguíes, desde hace varios años, empresas canadienses han buscado realizar exploración y explotación minera. Por eso destacan la importancia de las consultas populares, aunque para el líder ambientalista es necesario analizar la situación al interior del concejo municipal, y "ser muy prudentes y observadores para ver cómo se puede concretar eso en al realidad".

<div>

Finalmente, Sampayo recuerda que el desastre con crudo causado por las actividades de hidrocarburos por parte de Ecopetrol continúan. Allí todavía sigue el equipo traído desde Estados Unidos para sacar el petróleo, aunque siguen esperando el reporte de todo el plan de contingencia por parte de la empresa. **"Nos cuesta confiar en la empresa, pero no queremos entrar en la confrontación. No queremos que luego Ecopetrol diga que no los dejamos trabajar**", dice recalcando que fueron cerca de tres mil personas las afectadas por cuenta de esa tragedia ambiental.

<p>
<iframe id="audio_25575720" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575720_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>

</div>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
