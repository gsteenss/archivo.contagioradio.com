Title: Cada días más lejos de la verdad sobre masacre de La Rochela
Date: 2018-09-26 15:22
Author: AdminContagio
Category: Nacional, Sin Olvido
Tags: justicia, La Rochela, paramilitares
Slug: caso-la-rochela-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/LaRochela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 26 Sep 2018 

La decisión de la Fiscalía de **archivar el caso** contra los exgenerales **Alfonso Vacca Perilla y Juan Salcedo Lora**, acusados de ser coautores de la **Masacre de La Rochela**, es un paso hacia atrás en el esclarecimiento de la verdad para las familias de los 12 funcionarios judiciales asesinados el 18 de enero de 1989.

La determinación del ente investigador, concluye que los oficiales en mención no tuvieron nada que ver con el hecho y ordenó la preclusión del caso, c**ontradice los testimonios brindados tan sólo meses después de ocurrida la masacre por Alfonso de Jesús Baquero Agudelo, alias “Vladimir”** o el “Negro Vladimir”; líder del grupo paramilitar de los Mesetos, que operaban en el Magdalena Medio.

El sindicado en su momento señalo que la masacre fue promovida y planificada por ganaderos, comerciantes y miembros del Ejército Nacional, mencionando entre otros al congresista Tiberio Villareal, los generales Farouk Yanine Díaz, Carlos Julio Gil Colorado, **Alfonso Vacca Perilla** y **Juan Salcedo Lora.** Fue a partir  de su testimonio y de tres de los sobrevivientes que se orientó la investigación y se dio apertura al caso.

En el 2010, hace ocho años, se inició el proceso judicial donde un fiscal de la Unidad de Derechos Humanos vinculó a los oficiales. El general **Farouk Yanine** había fallecido y **Carlos Julio Gil** murió en medio de acciones de la guerrilla, de modo que, aunque en retiro, se acusó a los generales Vacca y Salcedo Lora.

En su decisión la Fiscalía declara la preclusión argumentando que, aunque Vacca no contuvo, ni neutralizo las acciones del paramilitarismo en el lugar, consideró que **no existian pruebas suficientes para determinarlo como coautor**; y en el caso de Salcedo se consumó el proceso judicial porque según el Ministerio Publico este **había dejado la comandancia de la Brigada 14**, que operaba en la región, dos años antes de los hechos.

**La masacre de La Rochela**

La masacre ocurrió el 18 de enero de 1989, cuando **una comisión judicial de 15 funcionarios investigaba masacres y asesinatos selectivos en el Magdalena Medio**, incluyendo la desaparición forzada de 19 comerciantes que fueron asesinados por paramilitares con la justificación de que estos transportaban armas para la guerrilla. (Le puede interesar: [La Rochela, buscando justicia en tiempos de paz](https://archivo.contagioradio.com/la-rochela-buscando-justicia-en-tiempos-de-paz/))

Ese día la comisión conformada por **dos jueces, secretarios, dos conductores y seis miembros del CTI**, se desplazaban por Simacota, Santander, cuando fueron abordados por paramilitares, quienes haciéndose pasar por guerrilleros les aseguraron que no estaban en contra de su labor, pero que se retiraran de la zona por enfrentamientos con el Ejercito, más adelante **fueron de nuevo abordados en el lugar conocido como 'La Rochela', donde 12 de los integrantes murieron y 3 sobrevivieron por azar.**

Según las declaraciones de 'Vladimir', la masacre fue legitimada por el congresista Tiberio Villareal, puesto que los funcionarios dentro de sus investigaciones tenían pruebas que lo incriminaban por nexos con el paramilitarismo en contratos públicos. Según el testimonio este había solicitado al paramilitar Henry Pérez, la ejecución del acto violento. L**uego de casi 18 años, la Corte Interamericana de Derechos Humanos** (CIDH) el 11 de mayo del 2007 **condeno al Estado colombiano por la acción y omisión de los hechos**, en donde, se ordenó continuar con la búsqueda de los culpables.

El Estado admitió la responsabilidad sobre el asesinato a los funcionarios, pero se solicitó al Tribunal Interamericano que no se hiciera referencia el contexto en el que ocurrieron los hechos, lo que fue rechazado, puesto que **se evidencio una acción del Estado para eliminar funcionarios judiciales**, quienes sufrieron ataques, represiones y asesinatos por diversos actores, entre 1979 y 1991.

En 2014 **Viviana Kristicevic**, directora ejecutiva del Centro para la Justicia y el Derechos Internacional (Cejil), que llevo el caso ante el CIDH con el Colectivo de Abogados José Alvear Restrepo (CAJAR), afirmó que **la reparación a las víctimas y los familiares de las mismas no ha tenido efecto**, pese a que se adelantaron acciones para prestar salud, educación e indemnizaciones, no hay una verdadera manifestación de la justicia. Ahora, después de casi 30 años de investigaciones continua la impunidad.

**Las víctimas mortales de La Rochela**

En la memoria quedarán los nombres de **Mariela Morales Caro**, de 36 años, Jueza 4 de Instrucción Criminal de San Gil; **Pablo Antonio Beltrán**, de 40 años Juez 16 de Instrucción Criminal de San Gil; **Samuel Vargas**, de 44 años y **Arnulfo Mejía**, 24 años, conductores del Cuerpo Técnico de Policía Judicial de San Gil; **Gabriel Enrique Vesga**, de 23 años y **Cesar Augusto Morales**, de 28 años, miembros del Cuerpo Técnico de Policía Judicial de San Gil; **Yul Germán Monroy**, de 28 años y **Orlando Morale**s de 21 años, Investigadores del Cuerpo Técnico de Policía Judicial de Bogotá; **Carlos Fernando Castillo**, de 24 años, y **Virgilio Hernández**, de 59 años, Secretarios del Cuerpo Técnico de Policía Judicial de Bogotá; **Benhur Iván Guasca**, de 24 años y **Luis Orlando Hernández** de 29 años, Investigadores del Cuerpo Técnico de la Policía Judicial.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
