Title: Continúa estigmatización contra integrantes del Congreso de los Pueblos
Date: 2019-08-21 13:37
Author: CtgAdm
Category: DDHH, Judicial
Tags: Alvaro Uribe, Centro Democrático, congreso de los pueblos, DDHH, Persecución a defensores de derechos humanos
Slug: nuevas-acusaciones-contra-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los pueblos] 

Tras los hechos ocurridos el pasado 17 de agosto en **Itagüí, Antioquia**, cuando diferentes organizaciones sociales - entre ellos el  Congreso de los Pueblos -** se manifestaron en contra del senador Álvaro Uribe**, quien visitaba el municipio, surgieron nuevas acusaciones contra el movimiento a través de Twitter, esta vez por parte de dos simpatizantes del senador, quienes los acusaron de ser una organización perteneciente al ELN y causante de vandalismo y criminalidad, un hecho que demuestra las continuas persecuciones en contra de este movimiento.(Le puede interesar:[Caso de falsos testigos abriría la puerta a nuevas investigaciones contra Uribe](https://archivo.contagioradio.com/caso-de-falsos-testigos-abriria-la-puerta-a-nuevas-investigaciones-contra-uribe/))

Este movimiento social y político **ha sido desacreditado en numerosas ocasiones,  por la fuerza pública  por partidos como el Centro Democrático**, siendo vinculados o señalados de **hacer parte del ELN,**mientras ejercen su derecho a la movilización social. En su defensa y a través de un comunicado, El Congreso resaltó que “hay una insistencia y persistencia por parte de los organismos del Estado de esta estigmatización y señalamientos que han cobrado vidas de muchos compañeros y compañeras”. (Le puede interesar: [Congreso de los Pueblos denunció la captura de cerca de 40 de sus integrantes](https://archivo.contagioradio.com/congreso-de-los-pueblos-denuncio-la-captura-de-cerca-de-40-de-sus-integrantes/))

### Congreso de los Pueblos ha sido estigmatizado desde su creación

Marylen Serna, vocera del Congreso de los Pueblos, asegura que **desde 2010, fecha en que surgió el movimiento, sus integrantes se han visto afectados por asesinatos, amenazas contra sus lideres, creación de falsas acusaciones, encarcelamiento  y  persecución.** Según la defensora de derechos, han sido más de 60 miembros de este colectivo en todo el país quienes se han visto vulnerados, “el caso más reconocido es el de Julián Gil, quien lleva un año en prisión y hasta el momento su proceso se encuentra estancado y no se ha podido hacer nada para entablar una defensa”. (Le puede interesar:[Julián Gil ¿Un falso positivo Judicial?](https://archivo.contagioradio.com/julian-gil-un-falso-positivo-judicial/))

Para el Congreso de los Pueblos, los nuevos señalamientos, por parte de Ani Avello y Paola Guerrero, simpatizantes del Centro Democrático representan una “**guerra sistemática contra movimientos sociales,** democráticos y en general todos lo que piensan distinto y luchan por la dignidad y democracia, una guerra que se expresa en muerte a líderes, amenazas, atentados y demás, que en este momento afecta a su organización”.

Serna manifestó que **ya se presentó el caso** ante la Comisión Interamericana de DDHH, y posteriormente lo harán ante la Fiscalía General; solicitando el esclarecimiento de estas acusaciones, así mismo responsabilizan a  Paola Guerrero , Ani Avello y el senador Álvaro Uribe de cualquier agresión en contra de  los miembros del Congreso y otras organizaciones sociales, teniendo en cuenta los antecedentes de señalamiento que han tenido en la organización varios miembros.

La vocera concluye que **el Congreso de los Pueblos siempre ha respaldado el derecho a la protesta,** así mismo la protección de líderes sociales y en consecuencia, “el pueblo ahora está más despierto y no hay absolutismo en un solo partido”, e  invitó a todos los grupos sociales y políticos a exponer sus casos y permitir un diálogo que podrá verse reflejado  en la Semana por la Paz, evento en el que el Congreso liderará  una movilización el próximo 9 de septiembre.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
