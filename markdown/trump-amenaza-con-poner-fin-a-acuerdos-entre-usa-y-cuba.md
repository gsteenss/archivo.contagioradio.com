Title: Donald Trump amenaza con poner fin a acuerdos entre USA y Cuba
Date: 2016-11-28 17:37
Category: El mundo, Política
Tags: Cuba, Donald Trump, Fidel Castro, Raul Castro
Slug: trump-amenaza-con-poner-fin-a-acuerdos-entre-usa-y-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/donal-trump-nh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telemundo] 

###### [28 Nov. 2016] 

Donald Trump sigue dando de qué hablar, luego de conocer el fallecimiento del líder de la revolución cubana Fidel Castro, **el electo primer mandatario de Estados Unidos, aseguró que revocaría "las medidas ejecutivas que fueron tomadas por el presidente Barack Obama de no encontrar que Raúl Castro quiera regresar las libertades en la isla".**

A través de su cuenta de Twitter, **Donald Trump manifestó que trabajará por “asegurar que el pueblo de Cuba pueda iniciar su camino hacia la prosperidad y libertad".**

Y añadió que "**si Cuba no está dispuesta a hacer un mejor acuerdo para los cubanos y los cubano-estadounidenses pondré punto final al acuerdo".**

**Cabe recordar que desde el año 2014 el actual presidente de Estados Unidos, Barack Obama y el de Cuba, Raúl Castro, trabajaron para restablecer las relaciones diplomáticas,** lo que dio como resultado entre otras cosas, la re-apertura de embajadas en las capitales de ambas naciones, así como diversos vuelos entre dichos países.

Tras las pasadas legislaturas del 8 de noviembre el Congreso de Estados Unidos quedó nuevamente en manos de los republicanos en ambas Cámaras, y serán precisamente ellos los encargados de eventualmente desmontar las leyes que hacen parte del embargo a Cuba.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
