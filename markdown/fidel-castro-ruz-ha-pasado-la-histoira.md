Title: Fidel Castro Ruz ha pasado a la historia
Date: 2016-11-26 06:59
Category: El mundo, Otra Mirada
Tags: Cuba, Fidel Castro
Slug: fidel-castro-ruz-ha-pasado-la-histoira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/fidel-castro-united-nations-1960.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: repeatingislands] 

###### [26 Nov 2016] 

El presidente Raúl Castro en nombre del gobierno y a través de la televisión estatal, anunció que el **comandante en Jefe de la Revolución Cubana, Fidel Castro Ruz, falleció a las 10:29 Pm del viernes 25 de Noviembre de 2016.**

\[embed\]https://www.youtube.com/watch?v=oYwqMZP-Sbw&feature=youtu.be\[/embed\]

"Querido pueblo de Cuba:

Con profundo dolor comparezco para informar a nuestro pueblo, a los amigos de nuestra América y del mundo, que hoy 25 de noviembre del 2016, **a las 10:29 horas de la noche, falleció el Comandante en Jefe de la Revolución Cubana, Fidel Castro Ruz.**

En cumplimiento de la voluntad expresa del compañero Fidel, sus restos serán cremados.

En las primeras horas de mañana sábado 26, la Comisión Organizadora de los funerales brindará a nuestro pueblo una información detallada sobre la organización del homenaje póstumo que se le tributará al fundador de la Revolución Cubana.

**¡Hasta la victoria siempre!"**

Con estas palabras  Cuba se despide del hombre de la revolución Cubana, que durante 47 años estuvo al mando de uno de los regímenes más importantes del mundo, que resistió golpes de potencias como Estados Unidos, la Guerra Fría y la caída del Bloque de la Unión Sovietica.

######  Reciba toda la información de Contagio Radio en [[su correo]
