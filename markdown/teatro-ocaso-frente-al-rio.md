Title: Poética teatral en "Un ocaso frente al río"
Date: 2018-08-03 17:23
Category: eventos
Tags: Bogotá, teatro
Slug: teatro-ocaso-frente-al-rio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/IMG_5780-e1533334759101.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teatro Estudio 87 

###### 03 Ago 2018 

Llega a las tablas bogotanas la obra **"Un Ocaso frente al río"**, una pieza sensible, poética,misteriosa y actual de la compañía **Teatro Estudio 87**, escrita y dirigida por Moisés Ballesteros, en corta temporada hasta el 4 de agosto.

En la puesta en escena, se cuenta la historia de un pelotón de soldados al mando del **Mayor Clauss y Héctor**, quienes esperan terminar una misión para regresar pronto a casa, pero los días pasan, la rutina y la sensación de peligro hacen evidente sus delirios, llevándolos a **debatirse entre un encierro en la intemperie y la libertad de sus pensamientos que los lleva tan lejos como su imaginación lo permite**.

"Un Ocaso Frente al río" se dedica a observar y mostrar **como los personajes son intimidados por una sombra, las encrucijadas de la vida y los temores**, los cuales se expresan poéticamente en un grupo de mujeres que son hombres, que también padecen por su condición humana los vejámenes de la guerra y el olvido.

En escena, el elenco conformado por Natalia Montes, Devora Roa, Paola Díaz, Sonia Parada, Kim Arévalo y Diego Fajardo, se encarga de dar vida a los personajes entre quienes se desata **un conflicto en el que no se derrama sangre, real o ficticia,no hay víctimas ni victimarios, ambos roles se encarnan en cada uno**.

La pieza teatral es fruto del Taller Iberoamericano de Dramaturgia (Punto Cadeneta Punto) de Umbral Teatro, contó con la asesoría de la escritora uruguaya **Marianella Morena** y el dramaturgo español **Sergi Belbel**.

La temporada se presentara del **1 hasta el 4 de agosto en La Sala Fábrica de Hechos Culturales**, ubicada en la localidad de Teusaquillo de miércoles a sábados a las 8:00p.m. con un costo en su boletería de 30.000 general. Si desea saber más información sobre el teatro u otra inquietud puede ingresar al siguiente link www.lasala.co
