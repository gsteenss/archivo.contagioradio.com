Title: Líderes del Cauca esperan que ONU se lleve una visión real de lo que sucede a nivel territorial 
Date: 2019-07-15 18:25
Author: CtgAdm
Category: Comunidad, Nacional
Tags: asesinato de líderes sociales, Consejo de Seguridad de la ONU, Líderes sociales del Cauca
Slug: lideres-del-cauca-esperan-que-onu-se-lleve-una-vision-real-de-lo-que-sucede-a-nivel-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-líderes-Cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Onu-líderes.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MisionONUCol] 

Durante la visita del Consejo de Seguridad de la ONU en territorio colombiano, la delegación tuvo la oportunidad de escuchar a cerca de 20 líderes sociales del Cauca quienes les expresaron su preocupación frente a las crecientes amenazas y homicidios contra las y los defensores de derechos humanos, hechos que abren una brecha en el tejido social de las comunidades.

**Ariel Palacios, vocero de la Comisión Etnica para la Paz, señaló que en este espacio evidenciaron ante el Consejo** la necesidad de que en el país exacta un mecanismo eficiente para garantizar la vidas de los líderes y las consecuencias al interior de una comunidad cuando se asesina a un líder o a una lideresa.

"El asesinato de líderes étnicos no es un simple asesinato, constituye la sistemática desaparición y destrucción cultural de un pueblo que ha venido siendo arrasado como lo son los indígenas y los afrodescendientes" **afirma el vocero, quien agrega que el 40% de los asesinatos de dirigentes en el Pacífico obedece a muertes contra líderes étnicos. **

### "No le pedimos a la ONU que vaya más allá de sus funciones"

Durante la reunión, los líderes del Cauca también se refirieron a **"la ambivalencia y el doble discurso en temas de implementación"** por parte del Gobierno, solicitándole a la ONU que este sea más preciso con relación a los presupuestos que existen para implementar las políticas de paz en el territorio.  [(Lea también: Sabemos que hay desafíos para la paz, los hemos visto y escuchado: Consejo de Seguridad de la ONU)](https://archivo.contagioradio.com/sabemos-que-hay-desafios-para-la-paz-los-hemos-visto-y-escuchado-consejo-de-seguridad-de-la-onu/)

> "No le pedimos a la ONU que vaya más allá de sus funciones sino que sea un actor acompañante y vigilante que siempre esté exhortando al Estado colombiano a cumplir con sus obligaciones"

Palacios afirma que ahora, la expectativa  de este encuentro con el Consejo **"es que tengan una visión real de lo que está pasando a nivel territorial"**, algo que confirmarán el próximo 19 de julio cuando el jefe de la Misión de Verificación de la ONU, Carlos Ruiz Massieu presente su informe.

Ariel concluyó que esta fue un espacio de carácter político en el que, además de expresar los inconvenientes que existen en el territorio, también se informó al Consejo de Seguridad en qué nivel de avance se encuentran las comunidades, con la expectativa de que los siga apoyando como mecanismo observador para garantizar la vida de los pueblos étnicos en Colombia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38473360" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38473360_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
