Title: Recortes presupuestales frenan la posibilidad de una Reforma Rural Integral
Date: 2019-10-08 16:20
Author: CtgAdm
Category: DDHH, Economía
Tags: Agencia Nacional de Tierra, Agencia para el Desarrollo Rural, Fondo de Tierras, Implementación del Acuerdo, Programas de Desarrollo con Enfoque Territorial (PDET), reforma rural integral
Slug: recortes-presupuestales-frenan-la-posibilidad-de-una-reforma-rural-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Reforma-Rural-Integral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AgenciaTierras] 

Un reciente análisis de la organización Dejusticia revela que para el 2020, el Gobierno redujo los recursos de entidades claves para la **Reforma Rural Integral (RRI)** otorgando, según el Departamento Nacional de Planeación, \$7,3 billones de un total de \$271,7 billones del Presupuesto General. Tales recortes presuponen una ralentización en proyectos destinados a la transformación del campo colombiano, elemento clave del Acuerdo de Paz.

Según Alejandro Rodríguez, investigador de DeJusticia, las organizaciones afectadas por esta reducción son la **Agencia Nacional de Tierras (ANT)**, encargada del acceso y la formalización de la tierra para los campesinos, que tendrá un recorte de 18,9% , la **Agencia para el Desarrollo Rural (ADR)**, que se le redujo un 10,4% y que debe estructurar y cofinanciar los planes integrales de desarrollo rural y finalmente la **Agencia de Renovación del Territorio (ART)**, responsable de la implementación de los Programas de Desarrollo con Enfoque Territorial (PDET) que tendrá una reducción del 12,9%.

El investigador señala que estos recortes obedecen a la decisión de un Gobierno que sin la obtención de recursos, debe reducir sus gastos en los próximos cuatro años para cumplir la regla fiscal y mantener el déficit por debajo del 2.6 del Producto Interno Bruto, afectando directamente los proyectos de inversión que en **2019 recibieron respectivamente \$516.628 millones la ADR, \$315,727 millones la ART y \$141.134 millones para la ANT.**

### ¿Qué otras entidades claves para la Reforma Rural Integral serán afectadas?

También se evidenció un recorte en la Agencia de Desarrollo Rural, que pese a no tener competencia con el Acuerdo, es la entidad encargada del desarrollo rural del país, asímismo se redujeron los recursos del Ministerio de Agricultura, responsable de uno de los 16 planes nacionales de la RRI y del Ministerio de Vivienda. [(Le puede interesar: Cierre de la vía al Llano ha dejado \$2 billones de pesos en perdidas)](https://archivo.contagioradio.com/afectaciones-cierre-kilometro58-via-llano/)

Finalmente se conoció de una reducción al **Instituto Nacional de Vias (INVÍAS) ** de cerca del 50%, que según se conoció, se verá compensado con un aumento en el presupuesto de la Agencia Nacional de Infraestructura, la diferencia radica en la que esta última se encarga de grandes proyectos, mientras INVÍAS de las redes terciarias, lo que supondría una reducción en la creación de carreteras en las zonas rurales.

### ¿Qué pierde el campo colombiano con estos recortes?

La Reforma Rural Integral es considerada como uno de los componentes clave del Acuerdo de Paz pues debe sentar las bases para la transformación del campo y ofrecer las condiciones sociales óptimas para los campesinos, lo que le otorga un agregado de justicia social, pues según el investigador, **"no solo debe restaurar a quienes vivieron hechos victimizantes sino garantizar la solución de problemas rurales"** como la poca tenencia de la tierra, la ausencia de servicios públicos y el mal estado de las vías.

En ese sentido, y con los recortes de presupuesto que advierte Dejusticia, el acceso a la tierra, la seguridad alimentaria e ingresos para quienes viven en el campo, la creación de vías terciarias que permitan la comercialización y la reducción de costos de transporte, estimulando la productividad del campo continuarán estancados.

De igual forma, un correcto desarrollo de la RRI, señala Rodríguez permitiría un aumento en la exportaciones  y el consumo de productos locales. Asímismo los PDET en teoría afianzarían la llegada del Estado a los territorios afectados por el conflicto y que nunca han tenido una presencia gubernamental, entablando un diálogo entre comunidades y Gobierno. [(Lea también: "Reforma Rural Integral debe proteger semillas nativas")](https://archivo.contagioradio.com/reforma-rural-integral-debe-proteger-semillas-nativas/)

En términos de implementación, el investigador resalta que pese a que los rezagos se han visto desde el último año de la administración Santos, han observado problemas en particular en el Fondo de Tierras, entidad creada con el propósito de democratizar la tierra a favor de los campesinos, que además de  no contar con un rubro específico, congeló siete planes de desarrollo, tres de ellos por falta de recursos.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_42879816" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42879816_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
