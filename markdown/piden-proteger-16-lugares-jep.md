Title: Piden a la JEP proteger 16 lugares donde se hallarían personas desaparecidas
Date: 2018-08-30 12:26
Category: DDHH, Paz
Tags: desaparecidos, escombrera, JEP, medidas cautelares
Slug: piden-proteger-16-lugares-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/escombrera-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ago 2018] 

La **Corporación Jurídica Libertad**, en cabeza de su directora Adriana Árboleda, anunció que hoy radicarán ante la Jurisdicción Especial para la Paz **(JEP),** una solicitud de medidas cautelares sobre **16 sitios del país en los que se presume, hay víctimas de desaparición forzada** con la intensión de protegerlos y garantizar que se realicen los procedimientos necesarios para investigar este flagelo.

Las 16 zonas que buscaría cobijar la medida cautelar son, La escombrera y La Arenera en Medellín; Hidroituango, el cementerio de Puerto Berrio y algunos puntos de Tulia en Antioquia; los cementerios de Samaná, Riosucio y Norcasia en Caldas;  los cementerios de Sincelejo, Corozal y San Marcos así como la finca La Alemania y El Palmar en Sucre; algunos lugares en Lebrija, San Vicente de Chucurí y Cimitarra en Santander; y en Aguachica en Cesar.

Acudiendo a cifras del Centro Nacional de Memoria Histórica, la abogada aseguró que hay más de **82 mil personas "dadas por desaparecidas** en el marco y con ocasión del conflicto, dato que resulta escandaloso". Sin embargo, debido al Acuerdo de Paz, firmado en 2016, y a la creación del Sistema de Verdad, Justicia, Reparación y No Repetición (SVJRNR), hoy es posible que entidades como la JEP y la Comisión de Búsqueda de Personas Desaparecidas (CBPD) trabajen mancomunadamente y protejan los lugares en los cuales podrían ser halladas algunas víctimas de este crimen.

Según Arboleda, sí bien en 2.000 se tipificó el delito y se creo la CBPD, hasta el momento **no hay una estrategia para la búsqueda de personas dadas por desaparecidas,** y las investigaciones terminan dependiendo de los testimonios de los victimarios. Adicionalmente, hace poco fueron expedidos los decretos y garantizados los recursos para que entre en funcionamiento la Unidad Especial para la Búsqueda de Personas Dadas por Desaparecidas, que es la institución del SVJRNR encargada de desarrollar dicha estrategia.

### **¿Qué pretenden con la medida cautelar?** 

La Directora de la Corporación afirmó que aunque **deberían protegerse todos los lugares en los cuales podrían haber víctimas de desaparición,** con la medida cautelar se busca resguardar estos 16 sitios, del modelo de país en materia de infraestructura, desarrollo minero-energético y de manejo de los cementerios que no garantiza la protección estos lugares. (Le puede interesar: ["Así funcionará la Unidad de Búsqueda de personas desaparecidas"](https://archivo.contagioradio.com/asi-funcionara-la-unidad-busqueda-personas-desaparecidas/))

Para Arboleda, **Hidrituango** es un ejemplo de ello, donde a pesar de que cerca al proyecto "la Fiscalía determinó que existen al rededor de 600 personas desaparecidas", las obras de llenado continuaron; ocurre lo mismo con **La Escombrera,** en la Comuna 13 de Medellín, que sigue siendo utilizada a pesar de que se ha pedido su cierre en diferentes ocasiones. (Le puede interesar: ["Así debería ser la búsqueda de personas desaparecidas en La Escombrera"](https://archivo.contagioradio.com/asi-deberia-ser-la-busqueda-de-personas-desaparecidas-en-la-escombrera/))

Con la medida cautelar, que es un acción judicial previa al inicio de las labores de búsqueda, Arboleda espera que sea una forma de aportar a la Unidad de Búsqueda, y manifestó que la **Ley 1922 de este año** estableció la competencia del mecanismo de Justicia transicional en estos casos. (Le puede interesar: ["¿Qué cambia en la JEP con la aprobación de la Ley Estatutaria"](https://archivo.contagioradio.com/que-cambia-en-la-jep-con-la-aprobacion-de-la-ley-estatutaria/))

Finalmente, Arboleda manifestó que de ser aceptada la solicitud de medidas cautelares, **la JEP ordenaría a las autoridades competentes tomar medidas sobre estos lugares,** por ejemplo, para preservar los cuerpos que están en los cementerios mencionados, y de esa forma, garantizar el derecho de víctimas y familiares al acceso a la verdad.

<iframe id="audio_28219139" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28219139_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
