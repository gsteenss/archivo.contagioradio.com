Title: La Paternidad al otro lado de las rejas
Date: 2019-07-02 16:57
Author: CtgAdm
Category: Expreso Libertad
Tags: carceles, Derechos Humanos, Expreso Libertad, Parentesco
Slug: la-paternidad-al-otro-lado-de-las-rejas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Paternidad-Carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

César Barrera es el papá de Cesar, una de las 10 personas capturadas por los atentados en el Centro Comercial Andino, que denuncian haber sido víctimas de un falso positivo judicial. Desde el momento de la captura de su hijo, César asegura que han vivido momento difíciles en su familia, sin embargo afirma que su relación con sus hijos, y en especial con Cesar, han cobrado otro sentido.

Asimismo, Iván Ramírez, también víctima de este mismo hecho, y quien se encuentra en la Cárcel La Picota, recientemente es padre de una pequeña que nació mientras el se encuentra en reclusión.

Para él, su paternidad ha sido agridulce, por un lado la distancia ha dificultado su rol como padre, no obstante, asegura que este nuevo papel le ha significado un nuevo respiro de esperanza. Escuche estas historias sobre las paternidades que se viven en las cárceles de Colombia.

https://www.facebook.com/contagioradio/videos/849974295379413/

<!-- Load Facebook SDK for JavaScript -->

<!-- Your embedded video player code -->

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
