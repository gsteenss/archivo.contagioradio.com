Title: Seis argumentos cuestionan las objeciones de Duque a la JEP
Date: 2019-03-11 18:32
Author: ContagioRadio
Category: Paz, Política
Tags: Duque, JEP, Ley estatutaria, Objeciones
Slug: desmienten-objeciones-duque-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Duque-y-sus-objeciones-a-la-JEP-e1552341356510-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República] 

###### [3 Mar 2019] 

En la noche del pasado domingo el presidente Iván Duque presentó sus objeciones a 6 artículos de la Ley Estatutaria de la **Jurisdicción Especial para la Paz (JEP)**, lo que para el **abogado Enrique Santiago significa una nueva ofensiva de los enemigos del Acuerdo de Paz contra la verdad**. Adicionalmente, para el activista y defensor de derechos humanos, se trata de objeciones de índole político que no tienen sustento jurídico.

Santiago sostiene que la ofensiva contra la JEP se debe a hechos importantes que han ocurrido en torno a la verdad, como la carta firmada por más de 300 reclusos de Cárcel La Picota que han pedido comparecer ante la Jurisdicción para ofrecer verdad, así como la declaración del General (r) Rito Alejo del Río criticando al Gobierno por modificar la justicia transicional. El defensor de derechos humanos añade que estos ataques provienen del "consorcio" Martínez Neira, Uribe Vélez y la DEA.

### **6 Objeciones a la Ley Estatutaria, 6 sofismas contra la verdad** 

**La primera de las objeciones tiene que ver con la reparación a las víctimas,** pues el Presidente sostiene que no está clara la obligación principal de los victimarios en ese sentido; sin embargo, Santiago dice que las FARC ya entregaron sus bienes, y  se observa que no son suficientes para realizar la reparación material, por lo que presionar en ese sentido es fallarle a las víctimas en ese sentido.

Al tiempo, otros de los victimarios que podrían hacer esta reparación son los agentes del Estado, pero el mismo Gobierno obstaculizó esta acción evitando que se utilizara su patrimonio; adicionalmente, para el Abogado, es una forma utilizada por el Estado que busca evadir su propia responsabilidad de reparación. (Le puede interesar: ["Víctimas piden toda la verdad a reclusos que se acogerían al SIVJRNR"](https://archivo.contagioradio.com/victimas-verdad-carceles/))

**La segunda objeción presentada por Duque, se refiere al alcance del Comisionado para la Paz** que definiría a quienes integraron el grupo alzado en armas que se acogió al proceso de paz, situación que para el defensor de derechos humanos resulta sorprendente, porque sería igual a que el Gobierno (y no la exguerrilla o los tribunales de paz) decidiera quien hizo o no parte de las FARC. (Le puede interesar: ["Buscamos la paz con todas sus complejidades, u optamos por la guerra con todo su dolor"](https://archivo.contagioradio.com/paz-o-guerra-con-dolor/))

Santiago expone que según lo establecido en la norma, el Gobierno puede reconocer a quien integró el grupo armado, y si un ex-integrante no es reconocido, puede acudir a la justicia "como ocurre en cualquier Estado de derecho", para que, luego de un proceso judicial, se decida su condición. Por lo tanto, esta objeción pretende "no respetar la división de poderes, poniendo el ejecutivo por encima del judicial".

**Con la tercera objeción, Duque dijo que buscaba aclarar la competencia de la Fiscalía en hechos que sean objeto de la Jurisdicción Transicional**; pero como lo recuerda el Abogado, las acciones delictivas no pueden ser juzgadas por dos jurisdicciones al tiempo. (Le puede interesar: ["Las falacias del fiscal Néstor Humberto Martínez para objetar la Ley Estatutaria de la JEP"](https://archivo.contagioradio.com/falacias-para-objetar-jep/))

"Llama la atención tanto afán del Fiscal por investigar, cuando tiene 15 mil compulsas de copias de Justicia y Paz, es decir, crímenes cometidos por paramilitares que no ha investigado y por lo visto, no piensa investigar", y añade el Abogado que, esta supuesta competencia entre jurisdicciones fue resuelta por la Corte Constitucional en su sentencia C-080 de 2018, que le permite a la Fiscalía avanzar en investigaciones hasta que la JEP se pronuncie sobre el caso.

**Frente a la cuarta objeción, sobre la impunidad para pequeños perpetradores de delitos de lesa humanidad**, el Abogado sostiene que es en realidad una acción que garantiza precisamente la impunidad. La razón por la que la Jurisdicción toma macro casos, es que resulta imposible investigar y sancionar tantas violaciones de derechos humanos como hay en Colombia, "todas las jurisdicciones transicionales hacen estas priorizaciones", clarifica Santiago.

A ello se suma la existencia del marco jurídico para la paz aprobado en 2012, con el que se pensó que la no concentración de la justicia en los grandes responsables produciría impunidad, pues retrasaría investigaciones de victimarios involucrados en múltiples violaciones a los derechos humanos; un ejemplo de ello se puede encontrar en los efectos de la Ley de Justicia y Paz.

**La quinta y sexta objeciones apuntan a los procesos de extradición**: En la objeción al artículo 150 se argumenta que no se precisa lo ya dicho por la Ley de Procedimiento de la JEP respecto a la práctica de pruebas cuando se pidan personas en extradición; mientras se dice que el artículo 153 condiciona la extradición de otras personas al ofrecimiento de verdad que hagan.

En el primer caso, el Abogado dice que la Corte Constitucional ya aclaró que la JEP sí debía hacer la revisión de las pruebas para evitar que cualquier Estado extranjero presente pruebas falsas y termine afectado la implementación del proceso de paz, "como finalmente parece que está ocurriendo ahora en el caso del señor Santrich", y que en Colombia, la justicia ordinaria no está investigando ni parece querer investigar.

En el segundo caso, Santiago se remite a lo mencionado en el Acuerdo de Paz, que garantiza que una persona que está ofreciendo verdad no se la extradite antes que lo haga; de esta forma se evita lo ocurrido con los jefes paramilitares que fueron enviados a Estados Unidos, evitando de lleno su participación en la Ley de Justicia y Paz. (Le puede interesar: ["¿Qué podría pasar con la Ley Estatutaria de la JEP en el Congreso?"](https://archivo.contagioradio.com/que-podria-pasar-con-la-jep-en-el-congreso/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F259965454880293%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
