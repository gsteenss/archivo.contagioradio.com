Title: "Venezuela respeta los poderes constitucionales y desvirtúa golpe de Estado"
Date: 2017-04-02 10:55
Category: El mundo
Tags: Nicolas Maduro, Venezuela
Slug: venezuela-respeta-los-poderes-constitucionales-y-desvirtua-golpe-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/TSJ.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Radio del Sur] 

###### [2 Abril 2017] 

Tras la decisión del del Tribunal de Justicia, el presidente Nicolas Maduro   convocó para e al Consejo de Defensa de la Nación para resolver lo que el denomina un “impasse” entre poderes del Estado, de igual forma convocó al poder popular a defender la s**oberanía nacional  e insiste en que el diálogo político con la oposición es la clave para lograr la paz en Venezuela.**

De acuerdo con Jessica Do Santos, periodista de la Red Radial del Sur, el funcionamiento institucional y constitucional de Venezuela, está conformado por 5 poderes: el ejecutivo, legislativo, judicial, ciudadano y electoral razón por la cual los señalamientos que han hecho medios de información, sobre un posible **“golpe de estado” no son ciertos ya que se rigen por una Constitución que le establece las funciones así como a los demás poderes públicos.**

Además señaló que el órgano contralor y máximo intérprete de la Constitución es la Sala Constitucional del **Tribunal Supremo y que, es de este órgano de donde han emanado las sentencias controversiales,** que han sido emitidas a solicitud de recurso de interpretación sobre los acontecimientos institucionales que vive el país, a partir del momento en el cual la oposición pasó a ser mayoría en el parlamento venezolano.

El Tribunal indicó que considera que el **“Parlamento se ha extralimitado en sus funciones, ha desviado el poder y la autoridad** que como institución tiene, ha usurpado en algunos casos las funciones del poder ejecutivo". Le puede interesar: ["Venezuela rechazó informe de la OEA"](https://archivo.contagioradio.com/37789/)

Do Santos, explicó que estas sentencias pretenden ordenar jurídica e institucionalmente el estado frente a un poder que desobedece al Tribunal Supremo y crea conflictos con los otros poderes, y añadió que en la última sentencia la Sala Constitucional ha establecido que temporalmente hasta cuando el Poder **Legislativo vuelva a la normalidad algunas de las funciones de ese poder, necesarias para el funcionamiento del estado, serán asumidas por el Tribunal Supremo.**

Finalmente la periodista expresó que es de importancia "entender que la constitución está intacta, el parlamento no ha sido disuelto, hay total respeto a los derechos humanos, existe plena libertad de expresión dentro del país, la participación política es plural dentro del país, los partidos existen y **hay libre acceso a la justicia**. Estos hechos demuestran que no hay golpe de estado en Venezuela".

<iframe id="audio_17928504" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17928504_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
