Title: Debe haber diligencia en investigaciones por crímenes contra líderes en Colombia: CIDH
Date: 2017-10-23 16:09
Category: DDHH, Nacional
Tags: asesinatos de defensores, CIDH, defensores de derechos humanos, lideres sociales, sesiones de la CIDH
Slug: situacion-de-lideres-y-defensores-en-colombia-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cidh-3-e1508781805751.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio ] 

###### [23 oct 2017] 

 

El Estado colombiano como diferentes organizaciones de la sociedad civil, expusieron sus argumentos para **evidenciar ante la Comisón la situación de los líderes sociales y los defensores de derechos humano en el país**. Los comisionados de la CIDH, que presidieron la sesión, manifestaron que debe haber una diligencia eficaz para sancionar a los responsables de crímenes contra líderes.

La Comisión Interamericana expresó su preocupación por la situación de asesinatos y violencia contra líderes sociales, **“en donde las personas agredidas advierten que hay mucha probabilidad de impunidad”**. La Comisión le pidió Estado colombiano una disposición mayor para investigar y sancionar a los responsables de los crímenes contra líderes y defensores. (Le puede interesar:["Cuatro modificaciones a la JEP que preocupan a la Corte Penal Internacional"](https://archivo.contagioradio.com/cuatro-modificaciones-a-la-jep-que-preocupan-a-la-corte-penal-internacional/))

Igualmente, manifestaron que hay unos factores de riesgo hacia las defensoras y lideresas por su condición de mujer y que representan “patrones de persecución” por lo que enfatizaron en que se deben especificar los patrones de riesgo. Además, le pidieron al Estado que **señale los obstáculos para las investigaciones** de homicidios a líderes, teniendo en cuenta las altas cifras de impunidad.

### **Organizaciones sociales continúan registrando casos de líderes asesinados** 

Por el lado de las organizaciones sociales, indicaron que **no hay un avance significativo frente a las solicitudes** que se realizan en los espacios de participación e interlocusión con el Gobierno Nacional. Recordaron que, “muchas de las solicitudes que hacen en el tema de la persecución, seguimiento y estigmatización en contra de los defensores, no cuentan con receptividad por parte del Estado”.

Afirmaron que en las audiencias ante la CIDH son reiterativas, en la medida que las **organizaciones continúan realizando informes sobre “más líderes asesinados”**. Recordaron que los asesinatos, en el marco del pos acuerdo, han aumentado “específicamente en las regiones donde ya han dejado de hacer presencia las FARC, porque otras organizaciones al margen de la ley están entrando en estos territorios”.

Adicionalmente, fueron enfáticos en manifestar que **hay una falta de institucionalidad** para atacar este problema y que “la solución no se puede reflejar en el aumento de la fuerza pública dejando a un lado la intervención integral que tenga en cuenta las dinámicas de los diferentes territorios”. (Le puede interesar: ["CIDH hace un llamado a proteger a periodistas en Colombia"](https://archivo.contagioradio.com/llamado-de-la-relatoria-de-libertad-de-expresion-al-estado-a-prevenir-violencia-contra-la-prensa/))

### **Colombia carece de articulación institucional para proteger e investigar asesinatos de líderes y lideresas** 

Los delegados de las organizaciones afirmaron que las investigaciones que realiza la Fiscalía General de la Nación, en lo referente a asesinatos de líderes sociales, **“no tienen en cuenta la labor de los líderes** e indican que son problemas personales de los defensores”. Además, debido a la falta de articulación institucional entre las autoridades locales y nacionales, “muchas de las denuncias terminan archivadas en las oficinas regionales”.

También, hicieron alusión a la **deficiencia que existe en los mecanismos de protección a los líderes sociales** en “donde existe un programa de protección material sin protocolos de protección colectiva que se le ha solicitado a la unidad Nacional de Protección y al Ministerio del Interior”. (Le puede interesar: ["Colombia debe eliminar el racismo en el tratamiento a la protesta social: CIDH"](https://archivo.contagioradio.com/colombia-debe-eliminar-el-racismo-en-el-tratamiento-a-la-protesta-social-cidh/))

Para esto, instaron al Estado a que implemente mecanismos de auto protección, **medidas diferenciadas que correspondan con las problemáticas de los territorios**, que reconozca los patrones de sistematicidad en la violación de derechos humanos a los líderes y defensores y que cumpla lo pactado en el acuerdo de paz.

### **Estado colombiano considera que es importante mantener la fuerza pública en los territorios** 

En cuanto a la intervención en la audiencia por parte del Estado colombiano, los delegados del mismo indicaron que hay una gran preocupación desde el Estado por la situación de los líderes sociales. Indicaron que la **presencia de la fuerza pública es necesaria** en los territorios debido al aumento de bandas criminales y estructuras de narcotráfico. (Le puede interesar: ["CIDH y organizaciones de la sociedad civil condenan asesinato de Bernardo Cuero"](https://archivo.contagioradio.com/cidh-y-organizaciones-de-la-sociedad-civil-condenan-asesinato-de-bernardo-cuero/))

Los delegados indicaron que **se están desarrollando avances en las investigaciones judiciales** “la política de Estado ha buscado prevenir la violación a los derechos humanos de los líderes sociales”. Indicaron que todos los agentes estatales están comprometidos con “éste desafío”.

Fueron enfáticos en manifestar que, en los casos que recibe la Fiscalía, **hay una relación entre el hecho y la labor de defensa** de los derechos humanos. Indicaron que hay una articulación al interior de las dependencias de la Fiscalía “para poner al servicio de la investigación penal, toda la fortaleza de la identidad, así como la articulación con la Policía Judicial y la Procuraduría General de la Nación”.

Finalmente, recordaron que el Estado **tiene un plan de trabajo conjunto con organizaciones sociales** como Marcha Patriótica, “con quienes hemos ido a los territorios a conversar con representantes y familiares de víctimas", para dar respuesta a la grave situación de los líderes en el país.

<iframe src="https://www.youtube.com/embed/T6Yfz6HqEwQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
