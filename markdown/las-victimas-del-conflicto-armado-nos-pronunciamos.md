Title: "Las victimas del conflicto armado nos pronunciamos"
Date: 2014-12-16 15:37
Author: CtgAdm
Category: Video
Slug: las-victimas-del-conflicto-armado-nos-pronunciamos
Status: published

\[embed\]https://www.youtube.com/watch?v=a\_puGepM6Lg\[/embed\]  
Organizaciones de víctimas del conflicto armado se movilizaron el 24 de octubre de 2014, para exigir asignación de becas para las víctimas y sus hijos, vivienda digna y subsidiada, justicia, reparación y que la restitución de tierras sea real y efectiva.
