Title: Jóvenes piden una alianza de candidaturas de centro izquierda
Date: 2018-04-03 14:44
Category: Nacional, Política
Tags: Angela Maria Robledo, centro izquierda, Clara López, Claudia López, elecciones, Elecciones presidenciales
Slug: jovenes-piden-a-formulas-vice-presidenciales-una-alianza-de-candidaturas-de-centro-izquierda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/jovenes-e1522783426835.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 Abr 2018] 

Las fórmulas vice presidenciales, Clara López y Ángela María Robledo, de los candidatos a la presidencia Humberto de la Calle y Gustavo Petro, se tomaron un café con jóvenes de 28 diferentes organizaciones sociales que han pedido una unión para trabajar por una Colombia diferente. La gran ausente fue **Claudia López,** fórmula vice presidencial de Sergio Fajardo.

Los jóvenes del movimiento El País Primero, convocaron a un café para ir forjando el camino y llegar a tener **una alianza política** que escuche las voces de los jóvenes de cara a las elecciones presidenciales del 27 de mayo. Para esto, le entregaron a cada una de las fórmulas vice presidenciales una franja de cada color de la bandera de Colombia.

Ellos y ellas enfatizaron en la necesidad de poner por encima la **"pedagogía de la ternura" y el "reconocimiento del otro"** para de esta forma llegar a una segunda vuelta en los comicios presidenciales "con menos heridas". Recordaron que sea quien sea el candidato de la centro izquierda, que llegue a la segunda vuelta, contará con el apoyo de los jóvenes en las urnas.

### **Hay una necesidad de tener una alianza política ** 

Clara López manifestó que "hay una necesidad de crear una **convergencia democrática** con inclusión social que garantice la satisfacción de los derechos humanos para todos y todas". Por su parte, Ángela María Robledo indicó que es necesario tejer una alianza por la paz y la vida digna. Afirmó que un país que han empezado a silenciar los fusiles debe buscar "politizar la vida que empieza en los espacios cotidianos como la casa y las calles".

De igual forma, ambas fórmulas vice presidenciales recalcaron que hay varios temas en las que hay una convergencia para así poder **tejer una alianza política**. Por ejemplo, coincidieron en que la defensa de la paz es una de las principales banderas que tienen al igual que la defensa de la vida de líderes sociales y defensores de derechos humanos. (Le puede interesar:["Lucha contra la corrupción, la desigualdad y la defensa de la paz: coincidencias entre Petro, De la Calle y Fajardo"](https://archivo.contagioradio.com/coincidencias_entre_fajardo_de_la_calle_petro/))

Argumentaron que es necesario fortalecer la democracia en el país a través de la participación de los jóvenes de todas las regiones del país. A esto se suma la necesidad de trabajar por la **paz con la naturaleza** y por una educación que sea la columna vertebral de la integración en la sociedad.

### **Preocupaciones de los jóvenes** 

Ante esto, los jóvenes hicieron públicas sus preocupaciones e insistieron en que es necesario que se establezca una alianza “tripartita” que represente un cambio real para conseguir **“un país más justo, transparente e incluyente”**.  Por esto, manifestaron sus preocupaciones en temas como la defensa del Acuerdo de Paz, la negociación con el ELN, la educación y la protección del medio ambiente, preocupaciones sobre las cuales coinciden las fórmulas vice presidenciales.

Finalmente, invitaron una vez más a la Claudia López a que participe de las discusiones para lograr una alianza y recordaron que **“ninguna candidatura de la centro izquierda tiene opciones reales de ganar la presidencia si lo hace sola”**. Por esto, recalcaron su compromiso “con la vida, con las prácticas de diálogo y respeto para impulsar la convergencia democrática”.

<iframe id="audio_25060174" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25060174_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
