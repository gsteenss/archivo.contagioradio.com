Title: Desaparecido líder afrocolombiano de Marcha Patriótica
Date: 2016-12-07 14:40
Category: DDHH, Nacional
Tags: Lider desaparecido, marcha patriotica, Rio Calima
Slug: desaparecido-lider-afrocolombiano-del-rio-calima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/armando-torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [7 Dic. 2016.] 

Según información recibida por Contagio Radio, **desde el pasado viernes 2 de diciembre cerca del medio día no se conoce del paradero del líder afrocolombiano Armando Torres Lozano, de 61 años de edad,** de la comunidad de Guadual, Consejo Comunitario del Bajo Calima.

Actualmente Armando habitaba en el Barrio El Triunfo, comuna 12 de Buenaventura, lugar en el cual fue visto por última vez.

**Armando ha sido directivo de la organización Oncaproteca en el Consejo Comunitario del Bajo Calima y actualmente es parte de ASTRACAVA, asociación de trabajadores campesinos del Valle del Cauca, además de militante del movimiento social y político Marcha Patriótica.**

Según las informaciones recibidas, en el Barrio El Triunfo donde vive Armando,  al igual que en toda el área urbana de Buenaventura existe en este momento un control permanente de la estructura neoparamilitar de los urabeños.

**Según cifras del movimiento Marcha Patriótica, más de 120 integrantes de este movimiento han sido asesinados en cuatro años, de los cuales cuatro han sido en Cauca.** Le puede interesar: [Asesinado en el Cauca defensor de DDHH de Marcha Patriótica](https://archivo.contagioradio.com/asesinado-en-el-cauca-defensor-de-ddhh-de-marcha-patriotica/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
