Title: Presencia paramilitar y minería, una amenaza constante para comunidades afro e indígenas de Alto Guayabal
Date: 2019-08-21 10:43
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Alto Guayabal, proyectos mineros
Slug: presencia-paramilitar-y-mineria-una-amenaza-consante-para-las-comunidades-indigenas-de-alto-guayabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Alto-Guayabal.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/alto-guayabal-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Comunidades afro e indígenas de Alto Guayabal denuncian actividad de militarización en cercanías al cerro Care Perro brindando protección a compañías mineras, adicionalmente el pasado 20 de agosto el guardia ambiental de la comunidad de Alto Guayabal, **Óscar Carupia Bailarín** fue herido por paramilitares de las AGC en el Resguardo Urada,  Jiguamiando en Chocó.

Según la Comisión de Justicia y Paz, Óscar logró escapar tras escapar de un forcejeo en el que recibió una herida de bala y un golpe propinado por una pistola en su cabeza, debido a los retenes de grupos armados, el hombre no ha podido ser desplazado a un centro médico de la cabecera municipal.

Óscar vive con su familia en Alto Guayabal y trabaja como agricultor y en las labores de protección ambiental y comunitaria, "la comunidad está preocupada, uno no entiende por qué están atentando contra la vida de los indígenas" expresó el indígena.

### Continúa militarización para la minería 

A su vez, el habitante de la región señala que tropas del Ejército han hecho fuerte presencia en el Cerro Cara de Perro, en medio de anuncios de una multinacional minera de iniciar trabajos, "hay presencia de militares pero la seguridad que están brindando es para la empresa que tiene intención de ingresar al cerro", relata.

Cabe resaltar que desde 2005, empresas como Muriel Mining Corporation de Estados Unidos, Rio Tinto de Australia, y Rio Tinto Mining and Exploration Colombia, han mostrado interés en este territorio y pese a las reuniones de seguimiento de medidas cautelares en la **Comisión Interamericana de Derechos Humanos, (CIDH),**es evidente que se persiste en el inicio de proyectos sin existir medidas eficaces para proteger la integridad de las comunidades aledañas. [(Le puede interesar: Paramilitares están retomando el control del Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
