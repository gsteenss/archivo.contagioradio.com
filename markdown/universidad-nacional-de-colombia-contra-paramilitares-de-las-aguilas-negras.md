Title: Universidad Nacional de Colombia rechaza amenazas de Águilas Negras
Date: 2015-05-11 13:48
Author: CtgAdm
Category: Educación, Nacional
Tags: aceu, Aguilas Negras, estudiantes, feu, Ignacio Mantilla, leopoldo munera, mane, mario hernandez, miguel angel beltran, oce, Paramilitar, simon ladino, sociologia, universidad, Universidad Nacional
Slug: universidad-nacional-de-colombia-contra-paramilitares-de-las-aguilas-negras
Status: published

###### Foto: Sociología 

<iframe src="http://www.ivoox.com/player_ek_4478812_2_1.html?data=lZmkmp2Vdo6ZmKiak5yJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmc%2Fd18rf1c7IpcWfr8bQy9TSpc2fxcqYpdTQs87WysaYxdTSuNPVjNXO1MbRrc3d1cbfx9iPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Simón Ladino, Estudiante de Sociología] 

El paramilitarismo volvió a lanzar amenazas contra los y las estudiantes universitarios en Colombia, en esta oportunidad, contra estudiantes del Departamento de Sociología, de la Facultad de Ciencias Humanas de la Universidad Nacional de Colombia.

Esta amenaza, sin embargo, presenta algunos patrones diferentes a los que acostumbran utilizar las denominadas "Águilas Negras". A pesar de mantener como modus operandi la utilización de cuentas de correo electrónico para realizar las amenazas (como señaló el informe de Somos Defensores para el 2014), en esta oportunidad el panfleto amenazante no presenta errores de ortografía o redacción. Por el contrario, evidencia el conocimiento de la vida de la comunidad universitaria, y un reconocimiento de figuras académicas emblemáticas para el alma mater, y para la vida política del país.

El panfleto menciona, por ejemplo, el papel de Gerardo Molina como ex-rector de la Universidad, el paso de Camilo Torres como uno de los fundadores del Departamento de Sociología, e incluso el paso por sus aulas de Guillermo León Saenz antes de convertirse en el líder guerrillero Alfonso Cano.

Hace alusión a la reciente candidatura del profesor Mario Hernández para ser rector de la institución, del también candidato Leopoldo Múnera, y del destituido profesor de Sociología, Miguel Ángel Beltrán. El panfleto configura, por lo tanto, una suerte de amenaza contra el *que hacer* de la universidad, y de los académicos que en ella realizan un trabajo comprometido con las realidades del país.

Para Simón Ladino, estudiante de sexto semestre de Sociología, es precisamente por este motivo que las amenazas de las Águilas Negras no son únicamente contra los y las 11 estudiantes que se mencionan puntualmente en el documento, sino también contra la Universidad. "Concierne a la ética misma de la universidad, porque compromete sus fines misionales, y por lo tanto es una amenaza contra la universidad", indicó el estudiante.

También lo consideró así el Consejo Académico de la Universidad, que reunido en viernes 8 de mayo emitió un comunicado en el que rechazan las amenazas, y e invitan a la comunidad universitaria (por primera vez en muchos años) a solidarizarse y movilizarse en defensa de la Universidad, convocando a una marcha de antorcha que se realizó el mismo viernes en horas de la noche.

"Consideramos que en un contexto de construcción de paz este tipo de prácticas se constituyen en claros impedimentos para terminar con el conflicto y son atropellos a la integralidad de la Universidad Nacional de Colombia, por tanto deben ser rechazadas por el conjunto de la sociedad colombiana" reza el comunicado. ( [Comunicado No. 001 de 2015 del Consejo Académico](https://archivo.contagioradio.com/universidad-nacional-de-colombia-contra-paramilitares-de-las-aguilas-negras/comunicado-no-001-de-2015-del-consejo-academico-acmorenorunal-edu/) )

En el año 2008, el colectivo de Derechos Humanos Jaime Pardo Leal presentó un informe sobre las amenazas, asesinatos y violaciones en general a los Derechos Humanos del movimiento estudiantil colombiano, evidenciando que este tipo de violaciones se presentan como respuesta al fortalecimiento de la organización del estudiantado, con la pretensión de asustar y desarticular la movilización que pueda estarse desarrollando.

El panfleto de las Aguilas Negras sería una muestra más de este modus operandi. Según señala Simón Ladino, "Para nosotros es muy importante remarcar que estas amenazas se presentan en un momento en que la movilización universitaria viene en asenso". En el Departamento de Sociología "hace 6 meses empezamos a movilizarnos en defensa de la libertad de cátedra y de la autonomía universitaria con el caso del profesor Miguel Ángel Beltrán; a principio de este semestre nos movilizamos en defensa de las ciencias sociales y humanas debido a la crisis financiera y presupuestal de la Facultad; posteriormente estuvimos denunciando la anti-democracia universitaria por la imposición nuevamente del rector Ignacio Mantilla; y ahora hemos inspirado -si se quiere- algunas de las formas de movilización que se han desarrollado en esta coyuntura, con el tema de las 'tomas de edificios'," indicó.

4 días han pasado desde que llegaron los correos electrónicos, y a la fecha, las autoridades competentes no se han puesto en contacto con los y las estudiantes para iniciar un proceso de investigación por los hechos presentados.

Sin embargo, esto no intimida a la comunidad universitaria. "El compromiso que hemos asumido colectivamente es continuar con este proceso de movilización -afirma Simón-. Continuar profundizando esta pelea por el pliego de exigencias al interior de la UN, y no caer en la trampa de quienes amenazan a la universidad pública, y es que dejemos de movilizarnos. Lo que vamos a seguir haciendo es continuar tratando de acercar a mas personas a este movimiento universitario en defensa de la Universidad Nacional, para que en ultimas podamos construir una universidad del tamaño de nuestros sueños."

#### Vea aquí el panfleto enviado por las [Aguilas Negras](https://archivo.contagioradio.com/universidad-nacional-de-colombia-contra-paramilitares-de-las-aguilas-negras/amenazaan/) 
