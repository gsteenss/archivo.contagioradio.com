Title: 27 de Diciembre será la "prueba de fuego" para la ley de amnistía
Date: 2016-12-20 13:30
Category: Entrevistas, Política
Tags: Cámara, Congreso, FARC, Ley de Amnistia, paz
Slug: 27-de-diciembre-prueba-de-fuego-ley-de-amnistia-33926
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/congreso-e1478108708937.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Rico Piñeres] 

###### [20 Dic. 2016] 

**El Senado y la Cámara en Colombia aprobaron el pasado lunes en primer debate el proyecto de ley de amnistía** para otorgar el perdón a guerrilleros acusados de delitos menores que se acojan al acuerdo de paz. **La iniciativa alcanzó 15 votos a favor en el Senado y 30 en la Cámara** y estará en plenarias la próxima semana.

Para el Senador del Polo Democrático, Iván Cepeda el balance de la jornada en el Congreso y Cámara para la ley de amnistía fue positivo, sin embargo, aseguro que **se han introducido cambios polémicos “como por ejemplo un tema que tiene que ver con los agentes del Estado”.**

La propuesta a la que hace referencia el Senador Cepeda es aquella en la que se planteó la posibilidad de que agentes del Estado se reintegren a las filas de las fuerzas militares, una vez sean beneficiados con esta ley **“lo que se dice es que se tenga en cuenta a personas que han cometido delitos de menos de 5 años, dado que no sería tan grave. Sin embargo yo pienso que esa situación es discutible**. Es a mi modo de ver un elemento problemático y hay que discutir”.

**Según Cepeda, aún quedan discusiones en Cámara y Senado por lo que esa puede ser la oportunidad para que organizaciones de víctimas den su concepto** “porque hay que hacer una ley que de amnistía, pero respetar los derechos de las víctimas” agregó.

Cabe recordar que **la amnistía es aplicable para los guerrilleros y un tratamiento especial hace referencia a agentes del estado.** De igual modo estos son solamente aplicables a delitos que no sean de lesa humanidad, crímenes de guerra, genocidio  o graves violaciones de DH. Le puede interesar: ["Gobierno tiene que disponer toda su voluntad para implementar acuerdos" Carlos Medina](https://archivo.contagioradio.com/gobierno-tiene-que-disponer-toda-su-voluntad-para-implementar-acuerdos-carlos-medina/)

**La presencia de “Voces de Paz” no se hizo esperar y algunos sectores la consideraron como “significativa”.** Para el cabildante “las intervenciones de Imelda Daza y de Jairo Estrada han sido, cada una en su estilo y perspectiva política, muy importantes en el contexto de esta discusión” puntualizó.

En términos generales la discusión y la ley ha sido fiel al acuerdo de paz, sin embargo para Cepeda, “se viene la prueba de fuego el 27 de Diciembre en ambas cámaras y eso es un gran desafío, se requieren las dos terceras partes, es una votación calificada como ordena la Constitución”.

Y concluye diciendo **“espero que en el Senado y la Cámara se pueda aprobar la ley de amnistía sin mayores tropiezos”. **Le puede interesar: [Ley de Amnistía urgente para avanzar en implementación de Acuerdos](https://archivo.contagioradio.com/ley-de-amnistia-urgente-para-avanzar-en-implementacion-de-acuerdos/)

\[embed\]https://youtu.be/gDiAmbb4eo4\[/embed\]

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
