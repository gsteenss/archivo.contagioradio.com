Title: Líderes sociales denunciarán internacionalmente sistematicidad de agresiones en su contra
Date: 2019-03-19 13:08
Author: CtgAdm
Category: Judicial, Líderes sociales
Tags: asesinato de líderes sociales, Corte Penal Internacional, Fiscalía General de la Nación, Somos defensores
Slug: lideres-sociales-denunciaran-internacionalmente-sistematicidad-agresiones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-20-at-10.28.26-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: jeanmariefarmian.fr] 

###### [18 Mar 2019] 

El próximo 5 de abril, **40 líderes sociales colombianos** presentarán una denuncia ante la Corte Penal Internacional (CPI) sobre las agresiones sistemáticas que vienen sufriendo en el país. En su visita a La Haya, los líderes instarán al tribunal transnacional a investigar y juzgar los delitos que ellos califican como **crímenes de lesa humanidad**.

Para Alexander Morea-van Berkum, ciudadano que promueve la apertura de este nuevo caso en La Haya, "es **urgente y necesario** que la Fiscal Fatou Bensouda conozca de primera mano como y por qué estos casos de exterminio al liderazgo social han pasado por alto para la justicia ordinaria y la Fiscalía General de la Nación".

Mientras que los asesinatos de líderes sociales aumenta, organizaciones denuncian que las investigaciones de la Fiscalía no han dado resultados. Según Somos Defensores, **91,4 % de estos crímenes **cometidos **entre 2009 y 2017 quedaron impunes **y solo 49 casos (el 8,6 %) tuvieron una sentencia condenatoria o absolutoria. (Le puede interesar: "[Fiscal General niega impunidad en asesinato de líderes sociales](https://archivo.contagioradio.com/fiscal-general-niega-impunidad-en-asesinatos-a-lideres-sociales/)")

Frente a estas cifras alarmantes, Morea-van Berkum sostiene que sólo un tribunal de guerra como la CPI, podría investigar efectivamente estos delitos. El activista espera que el tribunal pueda tomar en cuenta las evidencias que presentarán los líderes sociales a la Fiscal Bensouda que vinculan estos presuntos crímenes de lesa humanidad con crímenes ambientales.

"La tesis que se sostendrán frente a la Fiscal sobre estos casos será que el asesinato de líderes sociales en las áreas rurales obedecen no a la economía ilegal como lo ha querido sostener el Gobierno colombiano sino que **está ligado al establecimiento de megaproyectos y crímenes ambientales**", asegura Morea-van Berkum.

Desde junio de 2004, la CPI tiene en curso un examen preliminar sobre cuatro casos de crímenes en Colombia relacionados con el conflicto armado. Los procedimientos bajo la mira del alto tribunal incluye algunos relacionados con ejecuciones extrajudiciales conocidos como "falsos positivos", desplazamientos forzosos, delitos sexuales y la promoción y expansión de los grupos paramilitares.

<iframe id="audio_33512500" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33512500_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
