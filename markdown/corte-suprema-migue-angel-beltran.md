Title: Corte Suprema de Justicia tiene 60 días para definir situación de Miguel Ángel Beltrán
Date: 2016-01-26 12:57
Category: Judicial, Nacional
Tags: FARC, miguel angel beltran
Slug: corte-suprema-migue-angel-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/profesor-miguel-angel-beltran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_10207326_2_1.html?data=kpWfkpyXdpehhpywj5aUaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncaTj09nSjbjZtNPZzsaYxsqPjtbn1c7Qy8aPuMrZz8qYmJWPqIa3lIqupsbXb9HV08aYxsrKrc%2Fd05Dgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [26 Ene 2016.] 

**La** **Corte Suprema de Justicia tiene 60 días para definir** la demanda de casación interpuesta por la defensa del profesor de la Universidad Nacional, [Miguel Ángel Beltrán](https://archivo.contagioradio.com/?s=Miguel+Angel+Beltr%C3%A1n)que actualmente se encuentra preso en la cárcel de máxima seguridad el Eron - La Picota, en Bogotá.

Durante la audiencia, el abogado del profesor, David Albarracín, señaló ante la Corte que la pruebas presentadas no tienen validez, ya que se trató de documentos que alguna vez estuvieron en una USB, que no podrían ser interpretados sin que se conozcan los computadores que se encontraron en el operativo en el murió Raúl Reyes, miembro del Secretariado de las FARC.

Así mismo, se evidenciaron las condiciones en las que se encuentra recluido Miguel Ángel Beltrán, que de acuerdo a su abogado, **lleva 6 meses sin recibir luz natural, no lo dejan ingresar prensa o libros, las visitas conyugales las debe atender en el piso, y además, no le suministran el agua suficiente.**

Por su parte, la Procuraduría y la Fiscalía General de la Nación solicitaron a la Corte Suprema de Justicia ratificar la pena de 8 años y 4 meses de prisión impuesta al profesor de Sociología a quien señalan de ser ‘Jaime Cienfuegos’, integrante de la guerrilla de las FARC.

Frente a ese argumento, Albarracín aseguró que **“Miguel Ángel fue condenado con prueba ilegal y aparte de eso, su sentencia,** condena el pensamiento diferente, la forma de expresar, la divergencia política e ideológica y en un país que quiere ir rumbo a la paz condenar esto, es exclusión política y un mensaje negativo para cualquier proceso de paz”.

Según las personas presentes en el plantón citado ayer en las instalaciones de la Corte Suprema, el caso del catedrático  representa una acusación contra la libertad de cátedra y la autonomía universitaria, como lo expresó Luis Felipe Millán, Secretario Nacional de Trabajo Intersindical de la Asociación Sindical de Profesores Universitarios de Colombia , ASPU, “**Miguel Ángel fue juzgado por estudiar y escribir sobre los movimientos sociales, y condenarlo a él por su derecho a la libre opinión es una violación a la Constitución Política”**.

Por el momento, la defensa, los familiares y amigos del profesor, esperan que Beltrán vuelva a gozar de su libertad dentro de los próximo 2 meses, “para poder hacerle un acto de desagravio, por este falsos positivo”, dicen los integrantes de la ASPU.

[![20160125\_171308](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/20160125_171308.jpg){.aligncenter .size-full .wp-image-19686 width="3264" height="1836"}](https://archivo.contagioradio.com/corte-suprema-migue-angel-beltran/20160125_171308/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
