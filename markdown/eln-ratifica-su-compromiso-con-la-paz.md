Title: ELN ratifica su compromiso con la Paz
Date: 2016-12-27 21:34
Category: Paz
Tags: conversaciones de paz con el ELN, Diálogos de paz ELN, ELN
Slug: eln-ratifica-su-compromiso-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LaPrensa] 

###### [27 Dic 2016] 

A través de su página web, el Ejército de Liberación Nacional -ELN- ratificó su disposición al diálogo, **anunció que asistirá al encuentro con el Gobierno el próximo 10 de Enero** en Quito y **condenó la falta de voluntad y negligencia estatal frente a la ola de asesinatos contra líderes y lideresas en los últimos meses. **

Pese al complejo panorama social y la falta de voluntad política por parte del Gobierno, señalada por el ELN, el comando central ha dicho que "nunca pretenderemos imponerle al gobierno condiciones unilaterales ni las aceptamos, porque **consideramos que las partes discuten diferentes propuestas, para llegar a la construcción de aproximaciones y acuerdos".**

En el comunicado, el ELN manifiesta que "a puertas de instalar la mesa en Quito, Juan Manuel Santos con descaro, colocó públicamente, condición unilateral que no era parte del acuerdo (…) empezaron a **sobreponer trabas, e imponer condiciones por fuera de lo acordado y firmado en la mesa”.**

Además, el ELN condenó la **negligencia estatal frente a la ola de amenazas, hostigamientos y asesinatos contra líderes, lideresas y defensores de Derechos Humanos** y agregó que "nuestros esfuerzos por la paz de Colombia siguen de aquí en adelante, por lo que queremos solicitar a los seis estados garantes y a sus representantes en la mesa de diálogo que nos sigan acompañando y brindando sus profesionales esfuerzos".

Por último, la guerrilla asegura que desde el anuncio de los diálogos con el Gobierno de Santos, **“hemos defendido y reiterado la urgencia de pactar un cese al fuego bilateral que creará un ambiente político y humanitario favorable** al proceso de paz, pero el gobierno se ha negado a ello”.

###### Reciba toda la información de Contagio Radio en [[su correo]
