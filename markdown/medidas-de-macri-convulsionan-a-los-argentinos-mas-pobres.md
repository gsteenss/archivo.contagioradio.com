Title: Medidas de Macri perjudican a los argentinos más pobres
Date: 2016-01-05 13:49
Category: DDHH, El mundo
Tags: Argentina, DNU Macri, Gobierno Macri, inflación en Argentina
Slug: medidas-de-macri-convulsionan-a-los-argentinos-mas-pobres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/protesta-argentina-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:elperiodicodemonagas.com.ve 

##### [05 Ene 2016] 

En tres semanas de haber asumido como presidente de la Argentina, las medidas implementadas por Mauricio Macri, como decretos de necesidad y urgencia (DNU), continúan provocando convulsiones por el fuerte impacto ocasionado en el bienestar de las clases más vulnerables del país.

Decisiones como la eliminación de las retenciones a la exportación industrial y a la exportación de productos agropecuarios, sumada a la devaluación en un 30% del peso argentino, consecuencia de la promesa estatal de liberar el control cambiario (cepo), han permitido la especulación en el precio de los productos de la canasta básica entre un 20% y un 40%.

Los llamados desde el ejecutivo a las empresas a sostener los valores que los productos presentaron a 29 de noviembre de 2015, no sirvieron y quedaron relegados en el olvido, ante lo cual el ministro de finanzas Alfonso Prat Gray aseguró que “con la ley y con el diálogo” se buscará contener la tendencia al alza, de la que responsabiliza a la “zona liberada” dispuesta durante la saliente administración.

Las medidas de Macri han incidido directamente en la capacidad adquisitiva de los argentinos en plenas festividades de fin de año, situación que el gobierno pretendió palear con la entrega de un bono navideño destinado a los hijos menores de los desempleados, de los trabajadores informales y para los pensionados de 400 pesos (unos 97 mil pesos colombianos), y evitar nuevas manifestaciones como las ocurridas el 22 de diciembre en las que varios obreros resultaron heridos.

Jonathan Gilbert, corresponsal de The New York Times, afirmó de manera crítica que “los cambios económicos radicales de Macri agitan a la Argentina, acentuando las divisiones para las que quería construir puentes y llevando a algunos argentinos a dudar si el cambio será para mejor”, apenas con 3 semanas de un mandato de 4 años.

**Los DNU y la ley de medios**

El uso de los DNU por parte del gobierno ha estado en la vista crítica, por la manera desmedida en que se toman decisiones sin el aval del Congreso, con lo que se violentan las leyes sancionadas por el legislativo, en particular en el caso de la Ley de Servicios de Comunicación Audiovisual, con la que se aprobó el día de hoy la creación del ENACOM, para absorber las funciones del Afsca y Aftic. ([Leer nota](https://archivo.contagioradio.com/ley-de-medios-amordazada-en-argentina/))

La determinación oficial, presentada por encima de la medida precautelar dictada por el juez platense Luis Arias el miércoles pasado, pretende crear un ente único “autárquico y descentralizado”, modificando sustancialmente artículos con los que se favorece la prevalencia y hegemonía de los medios empresariales de comunicación.

Artículos como el 40, donde se establece la prórroga de licencias establece que desde ahora serán 5 años de manera automática y luego 10 años por concurso; el 41 determina que ahora se permitirá la venta de medios audiovisuales y facilita la integración de cadenas privadas de radio y televisión y el 45 establece nuevas reglas para la cantidad de licencias, 15 de servicios de comunicación audiovisual cuando se trate de televisión abierta o radio en el orden nacional y en el provincia no se podrá exceder la cantidad de 4 licencias, es decir que ya no habrá topes a la cantidad de ciudades en las que pueden operar las empresas de cable, que pasarán a regirse por la ley de telecomunicaciones.
