Title: FARC inicia dejación de armas "sin pesar ni llanto"
Date: 2017-06-08 13:30
Category: Nacional, Paz
Tags: Dejación de armas, entrega de armas, FARC, ONU
Slug: el-20-de-junio-las-farc-habra-entregado-todas-sus-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dejacion-de-armas-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Naciones Unidas] 

###### [08 Jun. 2017] 

A través de una breve comunicación el máximo jefe de las FARC, Rodrigo Londoño alias “Timochenko”, aseguró que **el proceso de dejación de armas por parte de las FARC a la Misión de la ONU en Colombia ha comenzado** y terminará el próximo 20 de junio. Le puede interesar: [ONU y Celac participarán en la verificación del Cese al fuego y dejación de armas](https://archivo.contagioradio.com/onu-y-celac-verificaran-cese-al-fuego-y-dejacion-de-armas/)

“Se estableció que la Organización de las Naciones Unidas recibirán el armamento, lo depositarán en los contenedores y certificarán la dejación de armas de cada uno de los integrantes de las FARC-EP. En reunión celebrada dos días después se determinó que **la dejación de armas se hará en tres pasos, el 7 de junio el 30%, el 14 de junio el 30% y el 20 de junio el restante 40%”** manifestó Londoño en un vídeo conocido en redes sociales.

> El 30% de nuestro armamento ha sido puesto en manos de la [@MisionONUCol](https://twitter.com/MisionONUCol). ¡Las [@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) [\#EstamosCumpliendo](https://twitter.com/hashtag/EstamosCumpliendo?src=hash) con nuestra palabra! [pic.twitter.com/LfB0z1rfr7](https://t.co/LfB0z1rfr7)
>
> — Rodrigo Londoño (@TimoFARC) [7 de junio de 2017](https://twitter.com/TimoFARC/status/872574279522689025)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
Así mismo, Londoño manifestó que las FARC le están cumpliendo a Colombia y que **“jamás volveremos a emplear la violencia, nuestra única arma será la palabra,** lo prometimos y lo cumplimos, sin pesares ni llantos”. Le puede interesar: [Inició proceso de dejación de armas y “es irreversible”: FARC](https://archivo.contagioradio.com/ya-comenzo-el-proceso-de-dejacion-de-armas-y-es-irreversible/)

</p>
### **Las fechas para dejación de armas y retorno a la vida civil** 

La Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final (Csivi), que cuenta con participación del Gobierno y de la guerrilla de las FARC expresó que luego de una reunión **se definió extender hasta por 60 días la expedición de normas que garanticen la seguridad jurídica de los integrantes de las FARC,** así como la entrega de las armas y los proyectos productivos que garanticen la reincorporación a la vida civil.

Por esa razón las FARC cumplirán el siguiente cronograma:

1.  Las Zonas Veredales Transitorias de Normalización (ZVTN) no pasarán a ser Espacios Territoriales de Capacitación y Reincorporación sino hasta el 1 de agosto. Es decir, las Zonas seguirán vigentes por otros 2 meses más.
2.  **Antes del 20 de junio se deben expedir decretos ley por parte del Gobierno naciona**l para permitir a quienes entreguen las armas contar con una seguridad jurídica. En esas mismas fechas serán suspendidas las órdenes de captura que estén expedidas y se dará amnistía y libertad condicional a los guerrilleros y guerrilleras que se encuentren en las cárceles.
3.  La **dejación de armas finalizará el 20 de junio** y comenzará la documentación y el paso a la legalidad de los y las integrantes de las FARC.
4.  La destrucción de los explosivos y demás materiales que se encuentren en las caletas entregadas por las FARC serán destruidas máximo en 3 meses a partir del 1 de junio.
5.  El **15 de junio deberá estar en marcha el plan para el desmantelamiento de los paramilitares** o Bandas Criminales en Tumaco y Buenaventura.

![Fechas de la dejación de armas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DBU3c5-XYAEQLAo-e1496946910737.jpg){.alignnone .size-full .wp-image-41970 width="1200" height="817"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
