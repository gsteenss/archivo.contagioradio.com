Title: De 2010 a 2015 se cerraron 750 camas pediátricas en Colombia
Date: 2016-05-05 14:54
Category: Nacional
Tags: Crisis hospitalaria, niños y niñas, Paseo de la muerte
Slug: desde-el-ano-2010-hasta-el-ano-2015-se-han-cerrado-750-camas-pediatricas-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/pediatria-salud-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: rcn] 

###### [5 may 2016] 

Desde el año 2010 , en total se han cerrado más de **750 camas de pediatría en el país. En Bogotá suman 321 con cierres en hospitales como el Hospital Infantil Lorencita Villegas de Santos (170 camas) o  La Clínica del Niño Jorge Bejarano (120 camas),** estos cierres han provocado una falta de atención a tiempo en los niños y niñas del país y los ha expuesto al paseo de la muerte.

\[caption id="attachment\_23627" align="alignleft" width="151"\][![camas hospitales 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/camas-hospitales-2.png){.wp-image-23627 width="151" height="399"}](https://archivo.contagioradio.com/desde-el-ano-2010-hasta-el-ano-2015-se-han-cerrado-750-camas-pediatricas-en-el-pais/camas-hospitales-2/) Tablas Asociación Colombiana de Pediatría\[/caption\]

Debido a esto el Tribunal de Cundinamarca aceptó la acción popular interpuesta por el cierre masivo de camas pediátricas para niños con enfermedades de alto costo, la acción popular pretende que el Ministerio de Salud diseñe una **política pública de atención en salud para niños y niñas**, a su vez, que se **diseñe un sistema de alertas tempranas y establezca criterios claros referente a los servicios pediátricos de salud**.

De acuerdo con Germán Rincón Perffeti, abogado demandante, el cierre de las camas pediátricas se presenta debido a que el FOSIGA le paga a las diferentes [EPS la prestación de los servicios mensualmente](https://archivo.contagioradio.com/alcaldia-de-bogota-entregara-recursos-publicos-a-las-eps/), una de las tarifas de mayor pago son los niños y niñas menores de un año de edad, en razón de que si llegaran a enfermarse los costos son altos, sin embargo las EPS al momento de contratar directamente con hospitales y clínicas trabajan a la baja, es decir quién da menos y no trasladan el valor, por este motivo los hospitales y clínicas se **dieron cuneta que no era rentable tener camas para niños y niñas porque lo que pagaban las EPS era muy poco frente a los gastos** y por esto comenzaron a cerrar las camas pediátricas.

Además argumenta que "es de publico conocimiento que en Colombia hay problemas muy grandes con el sistema de salud ,y uno de los grandes inconvenientes esta en la atención a niños y niñas que inclusive dentro del [paseo de la muerte han tenido que entregar sus vidas sin ningún tipo de servicios". ](https://archivo.contagioradio.com/gobierno-penalosa-bogota-sin-compasion-con-los-vulnerables/)

Esta situación se presenta en todo el país, pero ha venido profundizándose en ciudades como [Bogotá, Medellín, Cali, Manizales y Barranquilla.](https://archivo.contagioradio.com/40-recorte-presupuestal-bogota-hospitales/)Por otro lado entidades internacionales como como la Organización Mundial de la Salud promueve como estándar internacional que al interior de los países existan por lo menos **26 camas pediátricas por cada 10.000 mil habitantes. En Colombia se registran solo 14 camas a pesar de que el 32% de la población pertenece a menores de 18 años.**

[![camas hospitales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/camas-hospitales.png){.wp-image-23628 .aligncenter width="439" height="349"}](https://archivo.contagioradio.com/desde-el-ano-2010-hasta-el-ano-2015-se-han-cerrado-750-camas-pediatricas-en-el-pais/camas-hospitales/)

<iframe src="http://co.ivoox.com/es/player_ej_11424973_2_1.html?data=kpahlJmde5Shhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncajZ09LO0JCsuc7Wxtfh0ZC2rc%2FXhqigh6eXsoyhjKbP0czFqNChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
