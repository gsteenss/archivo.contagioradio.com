Title: Corte Constitucional admite demanda contra el Fuero Penal Militar
Date: 2015-08-12 17:08
Category: DDHH, Nacional
Tags: alirio uribe, Angela Maria Robledo, colectivo de Abogados José Alvear Restrepo, Coljuristas, Corte Constitucional, Derecho Internacional Humanitario, fuero penal militar, Iván Cepeda
Slug: corte-constitucional-admite-demanda-contra-el-fuero-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/corte_constitucional-Fuero_Penal_Militar_contagioradio-e1485870742527.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cmi 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Mateo-Gomez.mp3"\]\[/audio\]

###### Entrevista a Mateo Gómez Abogado de la CCJ 

###### [12 Ago 2015]

El pasado 15 de Julio, cerca de 10 organizaciones sociales y de Derechos Humanos presentaron ante la Corte Constitucional **una demanda en contra de 7 iniciativas de ley y de reforma constitucional que amplían el Fuero Penal Militar**. Este 12 de Agosto la Corte Constitucional admitió la demanda que actuaría en conjunto para todos los asuntos que tengan que ver con el FPM.

Luego de la admisión de la demanda la Corte deberá fijar el proceso para que en un término de **10 dias intervengan expertos, ciudadanos e instituciones para pronunciarse sobre las pretensiones de quienes interponen la demanda**. Según el abogado Mateo Gómez, de la Comisión Colombiana de Juristas, la demanda procura que las reformas e iniciativas de ley estén alineadas con el Derecho Internacional de los DDHH y el DIH.

**La decisión de la Corte tendrá un efecto directo sobre todas las iniciativas que el congreso u otras entidades quieran presentar para referirse al mismo objetivo** en cuanto al Fuero Penal Militar. Gómez afirma que el periodo de 10 días inicia hoy y concluye el próximo 26 de Agosto y todos aquellos interesados en intervenir podrán hacerlo inscribiendo sus posturas en la secretaría de la Corte o atendiendo el llamado que ya se ha hecho a varias instituciones.

Vea también [Esta es la demanda contra el Fuero Penal Militar](https://archivo.contagioradio.com/se-presento-demanda-contra-fuero-penal-militar/)
