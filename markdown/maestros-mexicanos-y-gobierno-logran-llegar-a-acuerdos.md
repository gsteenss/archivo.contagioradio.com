Title: Maestros mexicanos y Gobierno logran llegar a acuerdos
Date: 2016-07-28 14:37
Category: El mundo
Tags: CNTE, movilización maestros México, Reforma educativa en México
Slug: maestros-mexicanos-y-gobierno-logran-llegar-a-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CNTE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telesur] 

###### [28 Julio 2016] 

Tras mes y medio de [[intensas movilizaciones](https://archivo.contagioradio.com/30-maestros-detenidos-en-michoacan-cientos-de-heridos-y-desaparecidos-en-chiapas/)], la Coordinadora Nacional de Trabajadores de la Educación logró llegar a acuerdos con el gobierno de Enrique Peña Nieto en siete puntos, relacionados con la **derogación de la reforma educativa, la construcción de un proyecto de educación democrática**, la salida de maestros que están en la cárcel, el pago de salarios, la restitución del puesto de quienes han sido despedidos, la devolución de cuotas sindicales y la justicia frentee  los hechos ocurridos el pasado 19 de junio en Nochixtlán.

Los voceros aseguran que se comprometieron a ofrecer los enlaces necesarios para con el legislativo, establecer la ruta de derogación de la reforma; por su parte el Gobierno Federal se comprometió a **liberar a los líderes que permanecen en la cárcel y suspender las órdenes de captura que hay contra varios de los maestros** que participaron de las movilizaciones, además de pagar todos los salarios que le adeudan a los docentes. Estos acuerdos fueron logrados en las mesas de negociación que se establecieron con la Secretaría de Gobernación.

<div>

Este viernes iniciará la reparación integral y atención inmediata a los afectados de la [[masacre perpetrada en Nochixtlán](https://archivo.contagioradio.com/a-8-asciende-la-cifra-de-asesinatos-tras-represion-policial-en-oaxaca/)] el pasado 19 de junio, en la que se espera haga presencia el Subsecretario de Derechos Humanos de la Secretaria de Gobernación, así mismo para el próximo 3 de agosto se planea la realización de un **foro educativo en el que se discutirá la construcción de un proyecto de educación democrática**.

[![Acuerdo CNTE](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Acuerdo-CNTE.jpg){.aligncenter .size-full .wp-image-26975 width="729" height="1136"}](https://archivo.contagioradio.com/maestros-mexicanos-y-gobierno-logran-llegar-a-acuerdos/acuerdo-cnte/)

</div>

######  

 
