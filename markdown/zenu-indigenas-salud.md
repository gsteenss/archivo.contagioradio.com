Title: Desmonte de EPS indígena genera protestas del pueblo Zenú en Córdoba y Sucre
Date: 2017-04-03 15:51
Category: Nacional
Tags: Derechos Humanos, indígenas, Salud, Zenú
Slug: zenu-indigenas-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/6a88fa41-96b6-491d-99c8-6cf7a331e8e8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Konuco & Chagras 

###### 03 Abr 2017 

Cientos de indígenas del Pueblo Zenú protagonizan varias protestas en las vías de los departamentos, porque según ellos, **el Estado está desmontando el sistema de salud que desde hace varios años viene funcionando para esas comunidades**. Ante las protestas la reacción de la policía ha sido la represión que hasta el mediodía del lunes ya deja 7 detenidos y 5 heridos en los indígenas.

Desde que se conoció que se liquida la **EPS Manexka**, de propiedad de las comunidades indígenas, se despertó la indignación puesto que esta entidad atiende a los integrantes de las comunidades en sus propios territorios y el traslado de entidad los afecta puesto que **tendrían que buscar la asistencia médica en municipios lejanos** lo que implica un detrimento de las condiciones de salud de los Zenues.

Según **Celedonio Padilla**, capitán mayor del Pueblo Zenú en Cordoba y Sucre en declaraciones para el medio de información Konuco.com “**Se nos han violado los derechos a la defensa, a la presunción de inocencia y al debido proceso**, se está desconociendo la Constitución, los acuerdos internacionales y la obligatoriedad de la aplicación de la ley indígena que por bloque Constitucional se debe adoptar”

Otro de los consultados por esta situación fue **Misael Suarez** quien aseguró que “Nos quieren ver cómo hace 30 años atrás, cargando a nuestros enfermos en hamacas, los políticos decían que nos tenían en el bolsillo, pero nos levantamos y ya no es así. N**o permitiremos que a través del servicio de salud nos vuelvan a suprimir**” y además anunció que l**a minga se extenderá indefinidamente** hasta que el gobierno garantice el cumplimiento de la constitución y los acuerdos.

Por su parte la ONIC a través de su cuenta en Twitter y la del Consejero Mayor de esa organización Luis Fernando Arias, aseguró que **la represión del ESMAD ya deja por lo menos 5 indígenas heridos, uno de ellos por arma de fuego y otros 7 detenidos.** Sin embargo las comunidades se mantienen en la decisión de declararse en minga permanente y en los bloqueos de varias de las vías de los departamentos en los que tienen presencia. Le puede interesar: [Asesinado Jairo Chilito, profesor de Asoinca en Sucre, Cauca.](https://archivo.contagioradio.com/asesinado-jairo-chilito-profesor-de-asoinca/)
