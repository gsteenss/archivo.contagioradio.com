Title: Niegan participación de líderes sociales en Consejo de Seguridad
Date: 2016-11-22 17:02
Category: Nacional, Paz
Tags: amenazas paramilitares, asesinatos de líderes sociales, Caquetá, COORDOSAC
Slug: niegan-participacion-lideres-sociales-consejo-seguridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/WhatsApp-Image-2016-11-22-at-1.01.20-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Nov 2016] 

Mientras el Gobierno Santos hizo pública su intención de reunirse con los gobernadores de los departamentos más afectados por la **oleada de violencia en contra de defensores de derechos humanos** y distintos líderes sociales, dirigentes de la Coordinadora Departamental de Organizaciones Sociales, Ambientales y Campesinas del Caquetá, COORDOSAC **denunciaron que no se les permitió la entrada al Consejo de Seguridad de su Departamento.**

El presidente de la coordinadora y otros integrantes recibieron una invitación por parte de la Gobernación para que participaran del Consejo de seguridad, llegaron a las 8:30am, hora pactada para la reunión y **luego de 45 minutos de espera sin recibir alguna información,** el personal de seguridad se acercó para decirles que **por ordenes de superiores no tenían permitida la entrada al recinto.**

En dicha reunión se discutiría la grave situación de seguridad que se ha presentado en el Caquetá con los **hostigamientos y asesinatos de líderes sociales y la conformación de un plan de seguridad para mitigar las violaciones de derechos humanos** que han ocasionado decenas de muertos y heridos.

### **¿Cuáles son las denuncias?** 

Integrantes de la Coordinadora han señalado que la respuesta de los militares frente a los asesinatos de líderes en días pasados no es clara y **"niegan constantemente la presencia de grupos paramilitares o neo paramilitares en la zona".**

También denuncian que han encontrado en distintas veredas y pueblos de la región panfletos amenazando de muerte a otros líderes indígenas, campesinos e integrantes de la coordinadora, y **hasta el momento los entes policiales no han iniciado acciones investigativas para esclarecer los hechos.**

Manifiestan que estos hostigamientos contra la Coordinadora se deben a que dicha organización se ha comprometido con la **defensa del territorio, oponiéndose a la entrada de empresas extractivas y a las erradicaciones ilegales por parte de la fuerza pública.**

Por otra parte dirigentes de COORDOSAC anunciaron que en los próximos días van a adelantar acciones legales para exigir una respuesta de la Gobernación, que les den garantías de protección a sus derechos y adelantarán denuncias públicas a nivel nacional e internacional “para que todos conozcan la problemática situación de la región y **el país, y sepan que se puede avecinar otro conflicto de sangre sobre uno que no se ha solucionado** **porque el paramilitarismo no se ha ido, está en nuestros territorios”.**

Hasta el momento y a pesar de la insistencia de los integrantes de la Coordinadora, la Gobernación no les ha dado una respuesta del por qué no se les permitió el ingreso y participación en este crucial encuentro, al que fueron invitados por la misma entidad estatal y **hacen un llamado a organizaciones de Derechos humanos para que se les garantice su integridad.**

###### Reciba toda la información de Contagio Radio en [[su correo]
