Title: Donald Trump no es nuestro presidente: Latinos en USA
Date: 2016-11-10 13:06
Category: El mundo, Entrevistas
Tags: California, Deportaciones, Donald Trump, Elecciones Estados Unidos, Elecciones USA, Hillary Clinton, Latinos, Latinos en Norte America, Norte America, San Francisco
Slug: trump-no-es-nuestro-presidente-latinos-en-usa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Movilizaciones-USA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bez ] 

###### [10 Nov 2016] 

Las reacciones de cientos, miles de ciudadanos a lo largo y ancho de Norte América no se han hecho esperar luego de la elección de Donald Trump como presidente. **Las movilizaciones en las ciudades, las manifestaciones en redes sociales que expresan el descontento, el miedo y la tristeza por el nuevo mandatario de esta nación se hacen presentes conforme pasan las horas**. De la misma manera, comienzan a surgir las preocupaciones de los latinos.

En San Francisco por ejemplo, se han movilizado más de 1000 ciudadanos, quienes espontáneamente comenzaron a convocarse en las calles para mostrar su descontento frente a las implementaciones de las diversas políticas propuestas por Donald Trump.

Francisco Herrera, líder comunitario en San Francisco en Estados Unidos y participante de la movilización afirmó que “lo más bonito es que el número de jóvenes fue amplio y las manifestaciones culturales no se han hecho esperar”.

Según los resultados **en California, Donald Trump perdió.** La cifra fue de un 69% de favorabilidad de Hillary Clinton contra un 31% para el nuevo mandatario **“nos sentimos en shock, luego de la elección las calles y carreteras estaban vacías, los almacenas no abrieron, porque hubo una especie de golpe psicológico muy fuerte” manifestó Francisco. **Le puede interesar: [El mundo está en shock con la elección de Donald Trump](https://archivo.contagioradio.com/el-mundo-en-shock-con-eleccion-de-trump/)

Shock, rabia y una cierta vergüenza colectiva ante el mundo, asegura Francisco fueron los sentimientos que pudieron percibirse durante las movilizaciones **“No entendemos cómo es posible que en este país exista la habilidad de que una persona tan racista, violenta y vulgar pueda ser elegida”** y agregó que la vergüenza que se siente también es por “la ridiculez con la que se llevó a cabo esta elección en general”.  Le puede interesar: [Xenofobia y racismo marcan coyuntura electoral en Estados Unidos](https://archivo.contagioradio.com/xenofobia-y-racismo-marca-coyuntura-electoral-en-estados-unidos/)

Según el líder comunitario, luego de elegir a Donald Trump como el nuevo mandatario de Estados Unidos, se está mostrando al mundo lo que ha sido el liderazgo de dicha nación. Para Francisco, el presidente Obama ha permitido la deportación de más de 2 millones de personas “cuando él pudo haberlo detenido y me refiero específicamente contra la población latinoamericana” añadió.

**Miedo en la población latina**

“**Hay mucho temor. Tengo más de 7 padres de familia que me contaron que sus niños de  y 7 años se despertaron llorando con la pregunta ¿oye mamá ahora van a deportarnos a todos o que va a pasar con nuestra familia?**” así relata Francisco lo que han sido estas horas luego de la elección de Donald Trump como nuevo mandatario de Estados Unidos.

Adicionalmente, cuenta que la comunidad latina y LGBT está con mucho temor e incertidumbre de ¿qué hacer ahora, cómo nos juntamos, cómo nos organizamos? “es una combinación de sentimientos” añade.

Frente a las diversas políticas propuestas por Donald Trump durante toda su campaña la comunidad latina ha manifestado que “no hay que desconocer que Trump ha tenido un discurso en el que ha estado hablando muy  machito, en el que ha dicho que va a deportar a todos, que va a echar a todos a la cárcel, que va a castigar y **la gente de los campos, de las minas que se sienten muy abandonados por la política en general ha hecho un voto de castigo y se han ido con el que habla más macho”.**

**Comunidades más organizadas**

Según la población latina las redadas y represiones podrían aumentar considerablemente y perjudicarlos, por lo que las personas van  comenzar a organizarse. “Aquí dicen que el gigante dormido y lo que les quiero contar es que no hay ningún gigante dormido, al contrario está muy despierto, lo que pasa es que está ocupado con 3 trabajos al día y pierde la capacidad de estar con sus hijos. Se vive para trabajar” y agrega “en Estados unidos se vive una especia de esclavitud donde te atrapan las cuentas”.

Este tipo de situaciones harán entonces que las comunidades se organicen más, según Francisco será la oportunidad para crecer de la mano de organizaciones sociales, políticas y de los jóvenes “la expresión cultural nos acompañará en esta lucha que emprenderemos” concluyó.

<iframe id="audio_13700954" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13700954_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

######  
