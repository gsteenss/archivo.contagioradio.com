Title: Informe sobre acumulación de baldíos se quedaría sin piso si se aprueba ley de tierras
Date: 2017-05-10 15:01
Category: DDHH, Entrevistas
Tags: Acaparamiento de tierras, Banco de tierras, Ley Zidres
Slug: 123-mil-hectareas-de-tierras-habrian-sido-adquiridas-irregularmente-contraloria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Incoder-vichada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 May 2017] 

La Contraloría General de la Nación expidió una solicitud en la que insta a la Agencia Nacional de Tierras que active mecanismos jurídicos para anular la venta a terceros en 33 casos, que habrían adquirido **322 predios baldíos y que suman 123 mil hectáreas de tierra en 5 departamentos del país.**

La institución señaló que estas solicitudes son producto de investigaciones realizadas durante los años 2013 al 2015, en departamentos como Córdoba, Meta, Caquetá, Vichada y Antioquia, y agregó que hay 10 casos más, que concentran **138 predios y que equivaldrían a 199.871.43 hectáreas en proceso de estudio.**

Willson Arias, ex representante a la Cámara y quien adelanto investigaciones sobre el acaparamiento de tierras baldías en la altillanura, afirmó que la decisión de la Contraloría es satisfactoria, debido a que reconoce que **capitalistas internacionales, del sector financiero, corporaciones nacionales y agentes cercanos al gobierno Nacional, aprovecharon su oportunidad para quedarse con tierras**.

“El tema del Uberrimo, de Álvaro Uribe, de los Ingenios de Colombia, de la familia Santo Domingo, de Luis Carlos Sarmiento Angulo, fue un asunto en su oportunidad debatido y en esta ocasión se le pide a la Fiscalía que haga la investigación correspondiente”.

No obstante, Arias manifestó que el acaparamiento de tierras tiene una cifra mucho más grande y que este tema podría ser mucho más grave que Reficar, Odebrecht y otros casos de corrupción, “**el acaparamiento de baldíos en los últimos decenios es el hecho de corrupción más grande del país**” señaló.

Estos casos fueron trasladado a la Fiscalía y a la Procuraduría para que comiencen las investigaciones necesarias y **establezca las sanciones penales y disciplinarias que puedan tener funcionarios y personas particulares involucrados en este proceso**. Le puede interesar: ["Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz"](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

### **Ley de tierras podrá blanquear el fraude de la acumulación de baldíos** 

Sin embargo, Arias alertó sobre el proyecto de Ley que pasaría el gobierno, sobre las disposiciones del Ordenamiento social de la propiedad y tierras rurales, que tendría un decreto que legalizaría las adquisiciones “**mal habidas” y que incluso con la sola declaración del propietario de regresar los predios**, se le pueden a su turno, otorgar nuevamente a esos propietarios esas tierras baldías.

Con este decreto “los poseedores de tierras tendrían que explicar la tenencia de los predios, y en ese momento podrían postularse para recibirlos, nuevamente, ahora en otra condición**. La nueva ley permite que a título parcialmente oneroso se devuelvan las tierras**” afirmó Arias. Le puede interesar:["Proyecto de ley del gobierno profundiza modelo de acaparamiento de tierras"](https://archivo.contagioradio.com/proyecto-de-ley-de-gobierno-profundizaria-modelo-de-acaparamiento-de-tierras/)

### **Los Acaparadores de la Tierra en Colombia** 

Entre las empresas y personas naturales que figuran en la lista de los 33 casos se encuentran: La Sociedad Agropecuaria El Uberrimo, propiedad de Álvaro Uribe Vélez, en Córdoba, con **11 predios que tienen una extensión de 103.00 hectáreas**; esta la multinacional Cerro Matoso S.A con **5 predios que equivalen a 223.00 hectáreas, también en Córdoba.**

En el Meta, se encuentra El Palmar de Santa Barbara con tres predios que tienen una extensión de 3948.00 hectáreas, la Sociedad Comercial POLIGROW, con **tres predios equivalente a 5.577.00 hectáreas**, la Organización Luis Carlos Sarmiento Angulo con **21 predios con una superficie de 22834.00 hectáreas.**

En Vichada Familiares del ministro Iragorri tendrían **tres predios con una extensión de 3,816,61 hectáreas**, al igual que familiares del Ministro de Agricultura Lizarralde tendrían **4 predios equivalentes a 5.064 hectáreas. **Le puede interesar: ["20 empresas deberán devolver 53.821 hectáreas de tierras despojadas"](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/)

### **El arrendamiento de tierras una nueva forma de acaparar más benéfica para los empresarios** 

Un fenómeno que cada vez es más frecuente en departamentos como el Cauca, es el arrendamiento de territorios, a largo plazo, sobre el que aún no se ha pronunciado ninguna autoridad. Para Arias, **el arrendar, en vez de comprar, hace parte de una mutación por parte de los grandes capitalistas**.

El interés, provendría, de acuerdo con Arias, de desligarse de las responsabilidades que se adquieren al comprar grandes extensiones de tierras como las funciones sociales, ecológicas, **el pago de impuestos como el predial y finalmente afrontar el hecho de “volver fértil las tierras que se des fertilizan”** por el uso inadecuado.

**Ejemplo de ello es la Hacienda Miraflores en Cauca, que de acuerdo con las comunidades indígenas que actualmente la habitan, fue arrendanda al Ingenio Incauca**. Igualmente sucede con la recién desalojada hacienda el Brasil en Meta que se presenta como propiedad de una sola, desalojando a campesinos que serían legítimos propietarios, pero en que las tierras figuran a nombre de una sola persona. Le puede interesar: ["8 Comuneros han sido asesinados en el proceso de liberación de la madre tierra en Cauca"](https://archivo.contagioradio.com/8-comuneros-han-sido-asesinados-en-proceso-de-liberacion-de-la-madre-tierra-en-cauca/)

<iframe id="audio_18616945" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18616945_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
