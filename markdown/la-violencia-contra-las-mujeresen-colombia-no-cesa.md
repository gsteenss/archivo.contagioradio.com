Title: La violencia contra las mujeres en Colombia no cesa
Date: 2016-11-24 14:50
Category: Mujer, Nacional
Tags: 25 de noviembre día de la no violencia contra la mujer, feminicidio en Colombia, Violencia contra las mujeres, Violencia contra las mujeres en Colombia
Slug: la-violencia-contra-las-mujeresen-colombia-no-cesa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/violencia-contra-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EVG3.com] 

###### [24 Nov 2016] 

El 25 de noviembre se conmemora el día internacional de la no violencia contra las mujeres. En esta fecha histórica se recuerda **el asesinato cometido en 1960 en República Dominicana de las hermanas Mirabal, opositoras al régimen de aquel entonces de Leónidas Trujillo,** además de hacer imprescindible la lucha por el cumplimiento y garantías del derecho de las mujeres a una vida libre de violencias.

En este sentido, unos de los objetivos  del **Primer Encuentro Feminista Latinoamericano y del Caribe** llevado a cabo en **Colombia en 1981** y en el que participación organizaciones de mujeres provenientes de **México, República Dominicana, Chile, Venezuela y Perú** consistieron en abordar aspectos cruciales para las mujeres como son las **luchas sociales, la participación política y la sexualidad.** También se planteo promover el establecimiento de esta fecha para la eliminación de todas las formas de violencia cometida en contra de las mujeres en el mundo.

Así mismo, la **resolución 54/134** emitida por la **Asamblea General de las Naciones Unidas en 1999** **declara esta fecha como día internacional,** además de estipular 16 días de activismo en el que se busque visibilizar y concientizar sobre la obligatoriedad de poner fin a la violencia de género.

La Asamblea General de la ONU definió en 1993 que “todo acto de violencia basada en el género que le ocasione a la mujer muerte, daño, o sufrimiento físico, sexual o psicológico, tanto en la esfera pública como privada” es un grave delito. Sobre este aspecto, vale la pena hacer mención sobre **los tipos de violencia como son la doméstica, laboral, económica, mediática, simbólica y obstétrica** a la que son sometidas las mujeres en la vida cotidiana y en la que les son vulnerados sus derechos.

Según Naciones Unidas y la Organización Mundial de la Salud en **2015 un 35% de mujeres han declarado haber sufrido algún tipo de violencia física o sexual principalmente por parte de su compañero sentimental,** en una proporción 1 de cada 3 mujeres. De igual forma en 2012 el responsable del asesinato de 1 de cada 2 mujeres resultó ser la pareja de las mismas.

En el informe *Datos y Cifras Claves Para La Superación De La Violencia Contra Las Mujeres *presentado por la **Corporación Humanas de Colombia en 2016**, el principal origen de la violencia cometida contra las mujeres **se debe a las relaciones desiguales de carácter histórico y estructural entre hombres y mujeres.**

Aunque en Colombia actualmente existen leyes como la **1257 de 2008** cuyo objetivo consiste en adoptar normas que permitan garantizar a las mujeres **una vida libre de violencias** de igual forma que establece los **daños de tipo psicológico, sexual, físico o patrimonial generado**; la **1719 de 2014** en la que se busca **garantizar el acceso a la justicia a las víctimas de violencia sexual, de manera prioritaria a las mujeres y niños;** y la reciente ley **1761 de 2015** conocida como la ley Rosa Elvira Cely en la que **se tipifica y castiga el feminicidio,** "los altos índices de impunidad y de violencia continúan transmitiendo un grave mensaje de que se puede matar a las mujeres" ha señalado la escritora, poeta y jurista Isabel Agatón Santander.

### **¿Por qué continúan las violencias?** 

En el estudio realizado por las Corporación también se evidenció la persistencia de imaginarios culturales los cuales **naturalizan y normalizan la violencia basada en el género;** en una encuesta realizada se observó que el **37%** de las personas **consideran que las mujeres que se visten de forma provocativa se exponen a que las violen** y un **45%** cree que las mujeres **que siguen con sus parejas después de ser golpeadas es porque les gusta,** esto es alguno de las realidades que reflejan los altos niveles de tolerancia que sostienen las agresiones y violencias en contra de ellas.

De acuerdo con los datos emitidos en 2015 por la Fiscalía General de la Nación, en los **últimos 10 años en Colombia se han abierto 34.571 procesos relacionados con feminicidio, de los cuáles sólo en 3.658 casos se han presentado condenas** lo que indica un porcentaje de impunidad del 90%.

En este mismo sentido, el último comunicado presentado por la Red Nacional De Mujeres Porque Vivas Nos Queremos puso de manifiesto los obstáculos que afrontan las mujeres en los casos de violencia; el **desconocimiento de la normativa, las dificultades para el acceso a la justicia, la re-victimización** de las mujeres por parte de los funcionarios, **la legitimación social y naturalización de las violencias, la falta de acciones concretas por parte del Estado,** entre otros.

Según los datos Oficiales del Instituto Nacional de Medicina Legal **en 2015 cada 3 días una mujer murió en el país a manos del hombre con quien compartió o había compartido su vida.** Para este año las mujeres representaban el 86% de la población más afectada por la violencia ejercida por su pareja: **47% propiciada por el compañero permanente y un 29% por su excompañero.**

Al respecto, La Comisión Interamericana de Derechos Humanos de la Organización de los Estados Americanos en su *relatoría sobre los Derechos de la Mujer *afirma sobre el patrón de impunidad sistemática que en los casos de violencia contra las mujeres en el que se carece de investigación, sanción y reparaciones efectivas es **lo que incrementa los niveles de desconfianza de las mujeres en el sistema de administración de la justicia.**

De igual manera se resalta la doble discriminación a la que son sometidas las mujeres que dificultan el acceso a acciones judiciales ante los actos de violencia y discriminación perpetrada en contra de ellas, **es el caso de las afrocolombianas las cuales sufren las formas de exclusión por ser mujeres y por ser afro.**

Según el informe presentado por ONU Mujeres en 2016, los niveles de impunidad frente a casos de feminicidio en el mundo son del 98%.**De los 25 países en los que mayores niveles de violencia es ejercida contra las mujeres, 14 son latinoamericanos.**

[Resolución 54/134 de la Asamblea General de la ONU: Día Internacional de la Eliminación de la Violencia con...](https://www.scribd.com/document/332193338/Resolucion-54-134-de-la-Asamblea-General-de-la-ONU-Dia-Internacional-de-la-Eliminacion-de-la-Violencia-contra-la-Mujer#from_embed "View Resolución 54/134 de la Asamblea General de la ONU: Día Internacional de la Eliminación de la Violencia contra la Mujer on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_79576" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332193338/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LsSuH4Jp7EHAadbEp5UQ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
![libertades](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/libertades.png){.alignnone .size-full .wp-image-37001 width="600" height="400"}

###### Reciba toda la información de Contagio Radio en [[su correo]
