Title: US$100 mil millones del Fondo Verde Climático en manos de entidades que violan DDHH
Date: 2015-07-15 15:59
Category: Ambiente, Otra Mirada
Tags: Ambiente y Sociedad, Banco de Desarrollo Interamericano, Banco de Desarrollo Multilateral del Reino Unido, Banco de Desarrollo Regional, Banco de Inversión Internacional de Alemania, Banco Mundial, Calentamiento global, Corea del Sur, Fondo Verde Climático, la Fundación de Conservación Internacional, multinacionales y derechos humanos, países en vías de desarrollo
Slug: us100-mil-millones-del-fvc-en-manos-de-multinacionales-que-violan-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/economia_verde2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [semanaeconomica.com]

<iframe src="http://www.ivoox.com/player_ek_4829235_2_1.html?data=lZ2fm5eXeY6ZmKiak5aJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmbSZk5mekpWPscrgjNLWztHTssbnjMnSzpCqmqSfxtjhh6iXaaKlz5DS0JDRpc%2Fj1JDRx5DRuc3oytPOxZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Milena Bernal, Ambiente y Sociedad] 

###### [15 de Julio 2015] 

Tras la reunión de la junta directiva del Fondo Verde Climático, la preocupación de la sociedad civil ahora se enfoca en que** **algunas de **las entidades acreditadas** que van centralizar los recursos que llegan al Fondo, son entes financieros que **“no tienen un historial limpio, han desarrollado proyectos con combustibles fósiles y tienen un historial de violaciones a Derechos Humanos”,** señala Milena Bernal, abogada e integrante de la organización Ambiente y Sociedad.

El Fondo Verde Climático debe ser el mecanismo que aporte económicamente al desarrollo sostenible de los países en vías de desarrollo, logrando una reducción de emisiones de dióxido de carbono y mitigación del cambio climático, especialmente de las naciones más pobres, explica Bernal.

Sin embargo, **“contar con entidades que no son transparentes en el manejo financiero va a ser un problema** para que se pueda contar con la credibilidad necesaria y poner en funcionamiento el Fondo”, asegura la abogada.

**Son US\$100mil millones de dólares los que se deberán reunir para el 2020. **Hasta el momento se cuenta con US\$ 10 mil millones que ya pueden empezar a financiar proyectos que presenten las naciones.

Por ejemplo, la abogada indica que el Banco Mundial es una de las entidades financieras que han violado derechos humanos y que ahora será una de las entidades catalizadoras de los recursos para contribuir con los países en vías de desarrollo.

De acuerdo con la integrante de Ambiente y Sociedad, las mismas entidades financieras internacionales fueron las que solicitaron al FVC que fueran acreditadas como entes de catalización de recursos, y la junta del Fondo lo avaló. “El problema es que se trató de **una decisión que se tomó a puerta cerrada sin que la sociedad civil supiera”.**

Además, no existen criterios claros para la acreditación de las entidades, lo que significaría que **“se está acreditando de manera ligera. Debería ser un mecanismo financiero transparente, abierto al público** para que la gente sepa qué se financia para que el FVC no se convierta  una entidad financiera similar a las que se están acreditando”, expresa Milena Bernal.

Son 24 delegados de países desarrollados y en vías de desarrollo lo que hacen parte de la junta directiva, mas un mecanismo de observación conformado por la sociedad civil, sin embargo, esta no tiene poder de decisión.
