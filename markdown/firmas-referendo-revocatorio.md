Title: Inicia el trámite para el referendo revocatorio contra Iván Duque
Date: 2020-10-30 16:13
Author: PracticasCR
Category: Nacional
Tags: referendo
Slug: firmas-referendo-revocatorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Referendo-revocatorio-contra-Ivan-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves **se dio inicio al trámite para el referendo revocatorio que pretende, entre otras cosas, revocar el mandato del Presidente de la República, Iván Duque.** (Le puede interesar: [En dos años del Gobierno Duque se ha derrumbado institucionalidad](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la Registraduría fue inscrito el comité promotor de este proceso, el cual está liderado por el senador [Roy Barreras](https://twitter.com/RoyBarreras) quien se ha mostrado bastante crítico frente al gobierno Duque. (Le puede interesar: [Abren incidente de desacato contra Duque por operaciones militares de EEUU](https://archivo.contagioradio.com/incidente-desacato-ivan-duque-brigadas-estadounidenses/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Hace unas semanas el ex presidente y exsenador Álvaro Uribe anunció un referendo que propone revocar las Cortes, revocar la JEP y revocar la paz, yo les digo que prefiero revocarlos a ellos, al gobierno Uribe-Duque.”
>
> <cite>Roy Barreras, Senador y promotor de la revocatoria de Iván Duque</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La iniciativa que se pretende impulsar a través del referendo revocatorio, es **la posibilidad de someter ante la ciudadanía la posibilidad de dar por terminado el mandato de los presidentes luego de que estos hayan cumplido al menos dos años en el cargo**, lo cual permita dar cuenta de su gestión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente, la legislación colombiana no contempla la figura de revocatoria del mandato para el Presidente, sino únicamente para autoridades del orden local (alcaldes) y regional (gobernadores); por lo que su eventual aprobación significaría una gran reforma al ordenamiento jurídico del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **para que la propuesta tenga viabilidad jurídica se requiere que el comité promotor reúna alrededor de 1’800.000 firmas**; no obstante la meta de los convocantes es recaudar al menos 2 millones en los próximos 6 meses.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los otros temas del referendo revocatorio

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otra parte, se prevé que el referendo no solo aborde el tema de la revocatoria del mandato, sino algunas otras iniciativas sociales como el acceso a una **renta básica universal, la “matricula cero” para los estudiantes universitarios de instituciones públicas, un mínimo vital en materia pensional y la extensión de la implementación de los acuerdos de paz hasta el año 2034.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/RoyBarreras\/status\/1321874904761946112","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1321874904761946112

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Otros de los integrantes del comité revocatorio son la escritora y periodista Patricia Lara Salive, el experto en resolución de conflictos David Murcia, los líderes juveniles David Suárez Rojas, Jonathan Valencia, Javier González y Lizeth Sierra, la animalista Natalia Sierra Saleh y el dirigente afro Paulino Riascos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Referendo revocatorio alcanzaría a ser aplicado a Duque?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque Roy Barreras ha señalado que el referendo revocatorio tiene como propósito sacar del mandato al presidente Duque; los tiempos empleados por la Registraduría para  este tipo de procesos dificultan, que de ser aprobada, la iniciativa pudiese aplicarse contra el actual mandatario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, **el comité espera realizar la recolección de firmas en el menor tiempo posible para agilizar el trámite y remitir los formatos a la Registraduría para que inicie el proceso de verificación y conteo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En todo caso, si no se dieran los tiempos, de ser aprobada, la figura podría empezar a aplicarse para futuros gobiernos, poniendo en igualdad de condiciones al jefe de Estado, con los mandatarios locales y regionales a quienes actualmente sí se les puede revocar el mandato.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
