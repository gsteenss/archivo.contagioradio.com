Title: Puerta a puerta armados persiguen a excombatientes y líderes en Argelia, Cauca
Date: 2020-04-15 10:27
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Argelia, Cauca
Slug: puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Líderes/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La red de DD.HH. Francisco Isaías Cifuentes denunció que en veredas, corregimientos y viviendas del municipio de Argelia, han incursionado hombres armados que pertenecerían al "'Frente Carlos Patiño” de las disidencias de las FARC – EP en búsqueda de los líderes sociales y excombatientes que habitan en la zona, ofreciendo recompensas por sus cabezas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos vienen repitiéndose con el mismo patrón desde el sábado 12 de abril en diversos corregimientos **y veredas del municipio de Argelia incluidos el Encanto, Santa Clara, El Sinaí y el Mango**. Según la denuncia también hecha por el Partido FARC se trataría de cerca de 100 hombres armados vestidos de camuflado y botas que van de casa en casa de en búsqueda de firmantes de la paz y líderes de la Asociación campesina de trabajadores de Argelia (ASCAMTA) y de la Asociación de Juntas de Acción Comunal del Corregimiento el Plateado. [(Le recomendamos leer: ¿Quién fue Policarpo Guzmán? El histórico líder social de Argelia, Cauca que fue asesinado)](https://archivo.contagioradio.com/quien-fue-policarpo-guzman-el-historico-lider-social-de-argelia-cauca-que-fue-asesinado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los armados sostienen que han declarado a los líderes como objetivo militar pues tanto los firmantes de la paz, como los campesinos se han opuesto a que en el territorio existan grupos armados, además de manifestar su apoyo para impulsar el acuerdo de sustitución de Cultivos de uso ilícito. Dichos sucesos **se dan en un lugar con amplia presencia de la XXIX Brigada del Ejército Nacional** y en donde en ocasiones anteriores la Defensoría del Pueblo ha emitido Alertas Tempranas de Inminencia en el 2018 y 2020. [(Le puede interesar: Se agudizan amenazas contra defensores de DD.HH. en Putumayo durante cuarentena)](https://archivo.contagioradio.com/se-agudizan-amenazas-contra-defensores-de-dd-hh-en-putumayo-durante-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Violencia contra líderes y excombatientes ya había sido alertada

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde febrero de 2019 se ha advertido sobre la ola de violencia en el municipio en contra de excombatientes, ejemplo de ello fueron los asesinatos de Jhon Cleiner López Castillo, integrante de las Nuevas Áreas de Reincorporación de Santa Clara y de Fernando Iles y Jhon Jairo Hoyos Córdoba quienes también se encontraba en proceso de desmovilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que Argelia además cuenta con un cañón sobre el río Micay que comunica con el municipio de López de Micay, un cañón que históricamente ha funcionado para el cultivo de hoja de coca en gran parte de su extensión, lo que lo convierte en un lugar estratégico [(Lea también: Argelia, hasta mil personas desplazadas por combates y el olvido del Estado)](https://archivo.contagioradio.com/argelia-hasta-mil-personas-desplazadas-por-combates-y-el-olvido-del-estado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los pobladores han declarado a medios locales que desde el 3 de febrero el Frente Carlos Patiño ocupó Argelia y desplazó al ELN, grupo que hacía presencia en el lugar hace más de 12 años. Al respecto, Deivin Hurtado, defensor de DD.HH. de [Fensuagro](https://twitter.com/Fensuagro1976)señaló que no se ha podido obtener mayor información sobre las incursiones pues los armados continúan en la zona amedrentando a la población civil. [(Lea también: Hallan restos de Elder Daza, desaparecido por paramilitares en 2008 en Argelia, Cauca)](https://archivo.contagioradio.com/hallan-restos-de-elder-daza-desaparecido-por-paramilitares-en-2008-en-argelia-cauca/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
