Title: 26 años sin Bernardo Jaramillo Ossa
Date: 2016-03-22 12:03
Category: DDHH, Sin Olvido
Tags: Bernardo Jaramillo Ossa, Genocidio UP, Unión Patriótica
Slug: 26-anos-sin-bernardo-jaramillo-ossa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/IMAGEN-15442318-1-e1458665791678.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El  Tiempo 

###### [22 Mar 2016] 

##### [Texto: Sin Olvido](http://sinolvido.justiciaypazcolombia.com/2014/03/bernardo-jaramillo-ossa.html)

Hace 26 años, el 22 de marzo de 1990, fue asesinado Bernardo Jaramillo Ossa, segundo candidato de la Unión Patriótica a la Presidencia de la República de Colombia.

Bernardo, nació en Manizales, a sus 35 años como abogado había trabajado en el Urabá antioqueño, donde asesoró al Sindicato de los Trabajadores del Banano, hasta que por las amenazas de muerte tuvo que trasladarse a Bogotá. “Garfield”, como era llamado por sus familiares y amigos más cercanos en alusión a su bigote poblado, se mantiene en la memoria del pueblo colombiano, quien lo lloró y junto a él cantó el estribillo “Yo te daré, te daré una rosa, una rosa hermosa, y esa rosa se llama UP".

Con su popular slogan “Venga esa mano país”, Bernardo fue llenando plazas, parques, auditorios, mostrando la propuesta política de la Unión Patriótica como una alternativa ante una sociedad amalgamada e injusta. Desde la palabra, el pensamiento, la creatividad a flor de piel fue planteando nuevas posibilidades, nuevos caminos, denunciando permanentemente el paramilitarismo como una estrategia del Estado y nombrando a sus beneficiarios.

Bernardo, el “Perestroiko”, como era conocido cariñosamente al interior del partido por su insistente búsqueda de vincularlo a la Internacional Socialista, fue asesinado como miles de miembros de la Unión Patriótica y junto a ellas y ellos se mató una esperanza, él continúa en la memoria de muchas mañanas bogotanas que hacen recordar a esa misma mañana de hace 26 años en el Puente Aéreo de esa ciudad, donde sicarios de la estrategia militar encubierta dispararon en múltiples ocasiones sobre su humanidad. Las balas de la estrategia militar encubierta de tipo paramilitar segaron su vida el 22 de marzo de 1990 en el aeropuerto El Dorado de Bogotá poco después de las 8:00 de la mañana.

Cuatro disparos bastaron para que el país se estremeciera con la noticia de un nuevo magnicidio. Andrés Arturo Gutiérrez Maya, de 16 años, trabajador de una fábrica que hacía tizas para tacos de billar en Medellín, fue su asesino. Por ser menor de edad, el asesino no fue encarcelado y el capo Escobar, de forma indignante, desmintió ser el autor del crimen y se declaró adolorido por la muerte del candidato presidencial.

Al día siguiente del homicidio, el entonces director del DAS, Miguel Maza Márquez, le atribuyó el crimen a Pablo Escobar y exhibió dos grabaciones en las que el jefe del Cartel de Medellín y su lugarteniente El Zarco hablan sobre el pago de \$300.000 a un sicario para ejecutar un atentado. La conversación fue interceptada el 21 de marzo de 1990. Casi de inmediato, Escobar, desde la clandestinidad negó la autoría del asesinato, se declaró admirador de Jaramillo y dijo que, por el contrario, él había mediado varias veces para que socios suyos no lo mataran.

Lo paradójico es que el mismo día, desconocidos se comunicaron a varias estaciones radiales para decir que el promotor del asesinato había sido Fidel Castaño y que ahora este coloso de la guerra era el reemplazo de Rodríguez Gacha, abatido por la Policía en diciembre de 1989. No obstante, esta hipótesis fue poco investigada. Además, el joven sicario fue asesinado junto a su padre semanas después del crimen, cuando hacía uso de un permiso para salir de su sitio reclusión.

El asesinato de Bernardo Jaramillo Ossa sigue en la absoluta impunidad, muy pocos avances se han hecho para esclarecer el caso. Como otro de los tantos asesinatos cometidos con el fin de exterminar la unión patriótica, éste representó una gran pérdida, desesperanza ante un sueño de democracia, justicia y equidad. El sistemático asesinato de los militantes de la UP marco una gran ola de violencia desatada por el narcoparamilitarismo y su alianza con el aparato militar del estado.

Para que su muerte no quede impune la Fiscalía determinó que su asesinato es un crimen de lesa humanidad. Sin embargo hasta el momento, nada se avanzado en el proceso judicial, este crimen esta en absoluta impunidad, pero la muerte sólo llega realmente cuando se olvida, hoy recordamos a Bernardo Jaramillo Ossa y su labor política como una esperanza aún vigente en un país de injusticia, violencia y desigual, lo recordamos, y recordamos su voz fuerte, esperanzadora, en las cadencias sublimes de un tango de Gardel, que tanto le gustaba cantar.

<iframe src="http://co.ivoox.com/es/player_ej_10896346_2_1.html?data=kpWlm5uXeJehhpywj5WdaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncaPZ09PO1MnTb6vV08bay9HQs4zD1NjOj5C3rc%2BfsNHjy8nTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
