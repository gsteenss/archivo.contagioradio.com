Title: Policía Nacional impuso 168.146 medidas correctivas en primeras seis semanas del año
Date: 2019-03-27 18:02
Category: DDHH, Nacional
Tags: código de policía, Dejusticia, Policía Nacional
Slug: policia-nacional-impuso-168-146-medidas-correctivas-en-primeras-seis-semanas-del-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/codigo-de-policia-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [27 Mar 2019] 

Un nuevo estudio de la organización Dejusticia indica que la Policía Nacional impuso **168.146 medidas correctivas** entre el **1 de enero de 2019 y el 14 de febrero de 2019**, que afectaron un promedio de **3.600 personas al día**. Los comportamientos principales sujeto de estas sanciones fueron **el consumo de sustancias prohibidas en el espacio público**, seguido por **el porte de armas o sustancias peligrosas** y la **ocupación del espacio público en violación de las normas vigentes**.

Otras medidas fueron impuestas por irrespetar a la autoridad policial; incumplir, desacatar, desconocer e impedir la función o la orden de la Policía; y por propiciar la ocupación indebida del espacio público. Para **Alejandro Jiménez Ospina**, abogado de Dejusticia y realizador del estudio, estas medidas en particular generaron "sospechas directas de arbitrariedad" por parte de los uniformados. (Le puede interesar: "[Código de Policía no promueve la convivencia: Ángela María Robledo](https://archivo.contagioradio.com/codigo-de-policia-no-promueve-la-convivencia-angela-maria-robledo/)")

Estas cifras se revelan tras el polémico caso de Stiven Claros, quien recibió una multa de 833.000 pesos por consumir una empanada en un puesto callejero. Este incidente fue seguido por otros actos de aparente arbitrariedad policial que atrajeron atención mediática, como el caso de un ciudadano que fue multado por comprar chontaduro y otro por tomarse una cerveza sin alcohol en público. Esta semana unas 60 palenqueras salieron a las calles en Cartagena después de que una de ellas fuese multada en el Centro Histórico por vender frutas, como lo había hecho en los últimos 20 años.

Con las cifras disponibles, no se puede determinar si el número de medidas arbitrarias incrementaron en 2019 como parecen indicar los reportes mediáticos en recientes semanas. Sin embargo, Jiménez sostiene que los cambios en el nuevo Código de Policía, sumado a una ciudadanía más consciente, podría ser las razones por las cuales se registran más denuncias en los medios y las redes sociales.

### **Los problemas de fondo** 

El investigador sostiene que **el problema de fondo no es una mala capacitación** como lo indicó el senador Germán Varón ponente del nuevo Código de Policía. Al contrario, Jiménez asegura que **los oficiales fueron entrenados en línea con lo que la Policía entiende ser su deber y que para lograr cambios sustantivos, se requiere una transformación de la cultura adentro de la institución**.

"El problema es que entienden que es su función y como se deberían relacionar con los ciudadanos. Tiene que haber un cambio de cultura de nivel institucional de la Policía Nacional y que la Policía entienda que su labor es de garantizar la convivencia, la tranquilidad, las buenas relaciones entre las personas. Esto no se puede basar en una esquema en que ellos son la autoridad que no tiene ningún tipo de control", aseguro el abogado. (Le puede interesar: "[Están volviendo a la policía una enemiga de los ciudadanos: Alirio Uribe Muñoz"](https://archivo.contagioradio.com/policias-enemigos-ciudadanos/))

Por otro lado, el nuevo Código de Policía le concede a los agentes de calle **un amplio ámbito de interpretación de las normas**, lo cual refleja dos problemas concretos. "Uno, hay funcionarios de la Policía que creen que pueden hacer lo que quieran y que creen que su trabajo es imponer su voluntad sobre los ciudadanos a cualquier costa. El segundo problema es que no hay quien les haga rendición de cuentas", manifestó el abogado.

### Jiménez sostiene que se requiere un cambio de largo plazo, sin embargo sí existen medidas que pueden tomar la ciudadanía si se encuentren en situaciones en que se consideren vulnerados por parte de la Policía. Los ciudadanos pueden acudir a** la Procuraduría* *o la Defensoría del Pueblo** para que estas entidades puedan interceder en el caso. También, reconoce que algunos ciudadanos han recurrido a procesos legales para proteger sus derechos. 

<iframe id="audio_33786086" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33786086_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
