Title: Entre cantos y sonrisas mujeres construyen paz
Date: 2015-08-04 14:14
Category: Mujer, Nacional
Tags: buenaventura, Cristian Aragón, Espacio Humanitario, habana, memoria, mujeres, mujeres constructoras de paz, paz, proceso de paz
Slug: asi-fue-el-encuentro-regional-de-mujeres-construyendo-paz-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11831968_10153367888875020_1915110492_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

<iframe src="http://www.ivoox.com/player_ek_6005151_2_1.html?data=l5Wdl5aZdY6ZmKiak5uJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdSZpJiSo6mPqtbZjMrZjarSp9bZz9nf0ZDWqcjd0NPOzpDIqYzh1s%2FS1MrXb8Tjz9jh1Nrdqc%2FY0JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Nieves Torres, Espacio Humanitario Buenaventura] 

**Entre bailes, cantos y risas se celebró el primer Encuentro Regional de Mujeres construyendo paz en los territorios,** en Buenaventura del  30 de julio al primero de agosto.

El encuentro se realizó en el marco del primer año del asesinato de **17 mujeres asesinadas** en Buenaventura, el asesinato de **Óscar Fernando Hernández Torres**, además de los recientes hechos donde el joven **Christián Aragón** fue asesinado por grupos paramilitares.

Alrededor de **60 mujeres de diversas partes del país participaron en este encuentro**, donde se resaltó temas que tiene que ver con la memoria, la defensa del territorio y las propuestas de las mujeres en torno a las conversaciones que se desarrollan en la Habana entre el gobierno y la guerrilla de las FARC.

El primer día se refirió a la **memoria.** En conmemoración al asesinato de Óscar Fernando y Cristian, durante el 30 y 31 de julio se realizó una marcha de antorchas donde también participaron mujeres indígenas. Pese al miedo para realizar esta actividad por la situación que se vive en Buenaventura, **“fue importante ver cómo otras personas se fueron uniendo a la marcha”**, resalta María Nieves Torres, lideresa de la zona humanitaria en el barrio la Playita.

El segundo día se enfocó en el tema de **mujer y territorio,** entre las participantes se realizó una Cartografía en el marco de la participación y creación de vida, con el fin de resaltar el rol de las mujeres en la defensa del territorio. Ese mismo día se llevó a cabo un acto simbólico con vasijas de barro decoradas por las mujeres, donde se reflexionó sobre la importancia de que las comunidades y específicamente las mujeres estén atentas para que no los despojen de sus tierras, “**esa actividad nos sirvió para saber que debemos estar preparadas y defendernos  para cuando lleguen los paramilitares**”, explica la lideresa.

El último día las mujeres expusieron los retos que se tienen en la construcción de la paz y se desarrollaron propuestas en torno a la paz en términos de justicia, verdad, reparación y no repetición.

"**La conclusión a la que llegamos es que tenemos que seguir unidas para ayudar a construir la paz desde las mujeres…** No queremos colocar un muerto más menor de edad, no queremos niñas en la prostitución, no nos da miedo la muerte, nosotras seguiremos adelante para dejar nuestro legado”, concluyó María Nieves.
