Title: Zarigüeyas, 'guatines', ardillas e iguanas víctimas del "desarrollo" en Cali
Date: 2019-07-24 16:36
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Humedal El Cortijo, Movilidad, Valle del Cauca
Slug: zarigueyas-lechuzas-ardillas-e-iguanas-victimas-del-desarrollo-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/El-Cortijo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

El pasado 21 de julio se dio a conocer a través de redes sociales el desplazamiento de un grupo de zarigüeyas como resultado de las intervenciones que está realizando el sistema de transporte Masivo Integrado de Occidente (MIO) en cercanías al sector del Valle de Lili y al humedal de El Cortijo, donde hay un corredor ecológico que viene desde Los Farallones de Cali al Río Cauca y en el que conviven cerca de 127 especies que hoy son afectadas por las obras para construir  la terminal sur del MIO.

Rocío Selene Ruíz, presidenta de la Junta de Acción Comunal de la urbanización Valle de Lili, afirma que desde el 2017 vienen defendiendo el **Bosque seco tropical y el humedal El Cortijo** frente a las obras del Metro Cali, que según la lideresa, no consideró un plan de impacto adecuado para la fauna y flora del lugar, causando el desplazamiento de animales que hoy son vistos en barrios como **[Valle]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0}**[ **del Lili, El Caney, Capri y Ciudad 2.000. **]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Ruíz afirma que como consecuencia de estos proyectos, aprobados por la Corporación Autónoma Regional del Valle del Cauca,  han desaparecido más de **3.000 árboles**, llevando a escenarios en los que "**se atraviesan en las vías: ardillas, 'guatines', iguanas y zarigüeyas"**. Pese a que algunas personas adoptan a los animales o se ha optado por reubicarlos, Ruíz  advierte que es común en la ciudadanía confundir a animales como la zarigüeya con ratas, por lo que son atacadas o en algunos casos, cazadas para luego ser comidas.

> Cuidemos nuestra fauna y flora de [@cortijocali](https://twitter.com/cortijocali?ref_src=twsrc%5Etfw) donde están siendo ahuyentadas las zarigüeyas por obras de [@METROCALI](https://twitter.com/METROCALI?ref_src=twsrc%5Etfw) con autorización [@CvcAmbiental](https://twitter.com/CvcAmbiental?ref_src=twsrc%5Etfw) . Solicitamos [@personeriacali](https://twitter.com/personeriacali?ref_src=twsrc%5Etfw) [@ContraloriaCali](https://twitter.com/ContraloriaCali?ref_src=twsrc%5Etfw) [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) nos ayuden a salvar este Patrimonio ambiental [@ONUMedioAmb](https://twitter.com/ONUMedioAmb?ref_src=twsrc%5Etfw) [@MinAmbienteCo](https://twitter.com/MinAmbienteCo?ref_src=twsrc%5Etfw) [pic.twitter.com/BSFHXplYHb](https://t.co/BSFHXplYHb)
>
> — Jac Valle del Lili (@jacvalledellili) [July 22, 2019](https://twitter.com/jacvalledellili/status/1153129814418698241?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por su parte la Universidad del Valle ya ha dado a conocer **[un estudio](https://tubarco.news/wp-content/uploads/2019/05/prueba-pericial-univaesidad-del-valle-2017.pdf)** sobre el impacto ambiental en el humedal El Cortijo por la obra de Metro Cali en Valle del Lili indicando que en el **sector habitan 15 especies de mamíferos, 6  de anfibios, doce de reptiles y 88 especies de aves que se podrían ver afectados**.

Uno de los datos más reveladores es el que demuestra que **“el área boscosa del año 2001 era de 22.93 hectáreas y para 2017 había 17.79"** lo que revela una reducción de 22% en su área boscosa lo que podría provocar la extinción progresiva del bosque seco tropical y los humedales en la zona plana del Valle. [(Lea también: En riesgo más de 15 especies animales en Humedal Tibaguya)](https://archivo.contagioradio.com/en-riesgo-mas-de-15-especies-animales-por-ampliacion-de-planta-de-tratamiento-en-humedal-tibaguya/)

**Liliana Ossa, defensora de los derechos de los animales y creadora de la fundación de Paz Anima**l apunta que esta es una problemática que no solo se ve en este sector, sino en general y que es común ver a búhos y torcazas desplazadas de la zonas arbóreas que han sido taladas. Para ello es necesario que la comunidad se informe sobre el trato que hay que dar a las especies que han sido desplazadas y por parte de las autoridades ambientales que se realicen lo planes de contención de una situación que tiende a agravarse si no se aplican los correctivos necesarios.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38940026" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38940026_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
