Title: Implementación de los acuerdos de paz no va por buen camino
Date: 2017-09-25 13:28
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, Gobierno Nacional, Implementación de los Acuerdos de paz
Slug: implementacion-de-los-acuerdos-no-va-por-buen-camino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [25 Sept 2017] 

En una carta enviada a la ciudadanía, Henry Acosta Patiño, facilitador de los diálogos de paz entre el Gobierno Nacional y las FARC, manifestó su **descontento y preocupación por los incumplimientos** que ha habido en torno a la implementación de los acuerdos de paz suscritos en la Habana, Cuba.

Acosta puntualizó 5 aspectos que hacen parte de lo que él denomina la **No implementación total del Acuerdo Final**. A continuación, se muestran textualmente las preocupaciones que manifiesta. (Le puede interesar: ["Implementación de los acuerdos requiere movilización social: Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/))

-   El Tribunal y Salas de la JEP están siendo seleccionadas para que lo conformen exmagistrados y exjueces de diferentes estamentos de las FFMM. Y ese Tribunal y Salas deberá juzgar a todos los actores del Conflicto. Será entonces Una JEP parcializada. Y la Unidad Nacional Investigativa, de esa JEP, ya está en manos de la Fiscalía General, quien se ha convertido en la punta de lanza contra el Acuerdo Final.
-   Las 16 Zonas Electorales creadas en el Acuerdo Final, para fortalecer la participación política y democrática de los territorios abandonados; están en discusión en el Parlamento nacional, con serias posibilidades que las autoricen pero con menos 175 municipios. Es decir, Zonas Electorales Vacías.
-   Se está discutiendo en la Corte Constitucional la posibilidad legal de SI o NO, pueden la FARC hacer política. Se sabe que el NO está teniendo posibilidades. Eso sería el fin del fin. Imagínense, NO Reincorporación sumada a NO poder hacer política!
-   La Reincorporación colectiva, social, Económica, permanente y segura de los casi 14.000 insurgentes, quedó solo en que cada uno haga lo que pueda hacer con los \$8 millones individuales que les están entregando. No se quiso hacer la Reincorporación Colectiva, disque para no fortalecer el nuevo partido de la insurgencia. El riesgo de que muchos insurgentes se embandoleren, ante la ninguna alternativa de empleo, de autogestión o autoayuda, es inminente y muy posible.
-   Las medidas de seguridad son nulas. Bandolas andan diseminadas por todo el área rural, asesinando líderes y miembros de organizaciones sociales contestatarias.

Igualmente, Rodrigo Londoño, indicó también en una carta **su preocupación por la falta de cumplimientos por parte del Gobierno Nacional** de lo acordado en Cuba. Argumentó que las FARC le han “rendido culto a la palabra empeñada” en la medida que las unidades se trasladaron a las Zonas Veredales “por encima de los retrasos del gobierno en la generación de las condiciones pactadas”. (Le puede interesar: ["Crece el riesgo para los acuerdos de paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/))

Dijo que aun cuando hay leyes y decretos sobre amnistía e indultos, **son centenares los prisioneros que hay en las cárceles del país**. Además, indicó que “Las garantías para la vida y el ejercicio de la protesta social y política, previstas rigurosamente en el Acuerdo Final, riñen con la suma de asesinatos de dirigentes sociales y populares, así como de militantes nuestros reincorporados. El ESMAD continúa triturando compatriotas”.

También manifestó que los “mecanismos expeditos para la promulgación de las reformas legislativas necesarias para la implementación, **han sido objeto de serias mutilaciones institucionales**”. Por ejemplo, “la Fiscalía General de la Nación se empeña en atravesar toda clase de obstáculos a la creación de la Jurisdicción Especial para la Paz, al igual que a la Unidad Especial que se encargará de las indagaciones judiciales por paramilitarismo y sus vínculos”.

Finalmente dijo que **hay una campaña de odio y difamaciones en contra de los integrantes de las FARC**. Por lo tanto, dijo que los colombianos y la comunidad internacional no puede permitir que “se le arrebate la paz a nuestra patria. Hemos recorrido un trecho importante, los enemigos de un país diferente y en paz, no pueden salir triunfantes”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
