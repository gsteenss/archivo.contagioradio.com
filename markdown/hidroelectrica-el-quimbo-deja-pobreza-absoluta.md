Title: El Quimbo ha dejado 1500 familias en la pobreza absoluta
Date: 2016-01-12 15:00
Category: Ambiente, Nacional
Tags: El Quimbo, EMGESA, Hidroeléctricas
Slug: hidroelectrica-el-quimbo-deja-pobreza-absoluta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/El-Quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario de Huila 

<iframe src="http://www.ivoox.com/player_ek_10050069_2_1.html?data=kpWdl5WUepqhhpywj5aYaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncabgjLbiy9LGs4zcwpDRx8%2FFqNCfkpqdkpDKpc7dzc7O1ZDJsozgwpDd0cfWqdvVjMbP1dTQudWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussan, Asoquimbo] 

###### [12 Enero 2016]

Los pescadores artesanales afectados por la hidroeléctrica [El Quimbo](https://archivo.contagioradio.com/?s=El+quimbo) realizan hoy una movilización pacífica para rechazar las últimas decisiones jurídicas frente a la represa, exigiendo que se mantenga la medida cautelar que implica **la prohibición de la generación de energía; además, aseguran que es necesario vaciar el embalse y piden que un equipo interdisciplinario independiente** valore los impactos sociales, económicos y ambientales que ha producido la construcción de la represa.

Tras la reapertura de El Quimbo, EMGESA ha contabilizado  381 peces muertos que han aparecido a las orillas del río Magdalena. Miller Dussán, investigador de Asoquimbo, asegura que **si la represa retoma sus actividades en un 100% todos los peces morirán,** resaltando que se trata de una tragedia anunciada de la que ya se había advertido desde el año 2012.

Según Dussán, “una de las razones por las cuales el gobierno de Santos presionó para que ordenara la generación de energía fue el requerimiento del 5% del Quimbo para evitar el racionamiento y un incremento del precio” sin embargo, “según reporte de Emgesa la represa genera en la actualidad 84 megavatios de energía", y no los 400 MGW que corresponden al 5% del total que se produce en el país y que reclamaba el Gobierno para evitar el supuesto apagón.

Es decir que, según la información del investigador de Asoquimbo, **Emgesa está generando aproximadamente el 20% de MGW equivalente al 1% del total y no lo que se necesitaría.** Esta situación se debe a que la empresa “es consciente que de utilizar un caudal por encima de los 40 metros cúbicos de agua descompuesta (bajos niveles de oxígeno y altos niveles de acidez) al ingresar a la represa de Betania, la calidad del agua registraría valores inferiores a 4 miligramos por litro y causaría la mortandad de 19 mil toneladas de peces de Betania”.

Lo  anterior, significaría que la reapertura de El Quimbo se debe más a intereses económicos de los privados que al bien común, en cambio, no se ha teniendo en cuenta que hay más de 1500 familias que han quedado en la pobreza absoluta porque se destruyó el 100% de la pesca artesanal, **“había 42 especies de peces, hoy no está quedando nada… El Quimbo destruye la vida y el empleo”**, expresa Miller Dussán.

Es por eso que los pescadores artesanales de los municipios de Hobo, Campoalegre y Yaguará, realizan una movilización pacífica para rechazar las últimas decisiones jurídicas frente al El Quimbo, pero además, para exigir a la justicia colombiana el reconocimiento de los derechos y las afectaciones ocasionadas a la comunidad. Los pescadores caminan desde el sector de Surabastos de la capital del Huila hasta el Palacio de Justicia.
