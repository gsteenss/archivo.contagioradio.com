Title: Queman sitio sagrado de comunidad kankuama en la Sierra Nevada de Santa Marta
Date: 2017-08-17 18:01
Category: DDHH, Nacional
Tags: autoridades indígenas, SERANKUA
Slug: 45459-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Maloka-quemada.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### fotos: Resguardo Indígena kankuamo 

###### 17 Ago 2017 

En la madrugada de hoy desconocidos prendieron fuego al lugar sagrado de la comunidad kankuama de la Sierra Nevada de Santa Marta, compartimos el comunicado de la **Comunidad de Ramalito.**

#### **Hermanas y Hermanos kankuamos** 

Con una tristeza enorme más allá del alma, con mucho respeto y pidiendo permiso a los Padres espirituales y a la Divina Madre Naturaleza, comparto con ustedes la lamentable perdida de nuestro sitio tradicional en la comunidad de Guatapuri

![maloka](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/maloka.jpeg){.alignnone .size-full .wp-image-45462 width="720" height="432"}

Antes de empezar los invito hoy a pensar, a tomar un algodón, que en nuestros pensamientos y que este elemento vaya limpiando ese fuego que hoy destrozo un lugar tan importante para nuestra parte tradicional y espiritual de un Pueblo sumergido en el dolor y en la resistencia, mi pueblo kankuamo.

Dirijamos nuestra mirada a SERANKUA, acompañemos a nuestros mayores, a nuestros ancestros a quienes le hemos llevado alimento, palabra dulce y pensamientos, a quienes nos ayudado, nos han guiado y nos han fortalecido cada vez que lo necesitamos.

Hoy es un día para estar en SILENCIO , que de nuestra boca no salga palabras que nos dañen más, hoy solo hablemos lo estrictamente necesario.

Te pedimos fortaleza para nuestros mayores kankuamos, para el jate JUAN, a los hermanos y hermanas tradicionales, a nuestros hermanos y hermanas kankuamos que hoy nuestros ojos tienen que ver como ha quedado kankurua hombre- kankurua mujer.

![maloka kankuamos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/maloka-kankuamos.jpeg){.alignnone .size-full .wp-image-45463 width="880" height="660"}

Con lagrimas en mis ojos...

**Att. Yidid Jhohana Ramos Montero**  
**Indígena kankuama**  
**Comunidad de Ramalito**  
**Resguardo Indígena kankuamo**
