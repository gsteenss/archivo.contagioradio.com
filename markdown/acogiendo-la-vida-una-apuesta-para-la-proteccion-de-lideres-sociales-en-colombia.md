Title: "Acogiendo la vida" una apuesta para la protección de líderes sociales en Colombia
Date: 2018-10-04 16:23
Author: AdminContagio
Category: yoreporto
Tags: Corporación Claretiana, lideres sociales
Slug: acogiendo-la-vida-una-apuesta-para-la-proteccion-de-lideres-sociales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 Oct 2018] 

La Corporación Claretiana Normán Pérez Bello lanzó su campaña "Acogiendo la vida", una apuesta que busca recolectar fondos para continuar con su programa de protección, acogida y acompañamiento para líderes y **lideresas sociales, defensores y defensoras de DDHH, que ya lleva más de 20 años en marcha.**

Hasta el momento este programa ha acogido a más de mil personas, junto con sus familias. Además, en este espacio también se realizan talleres de formación integrales para las diferentes poblaciones que llegan como mujeres, primera infancia y jóvenes; y se gestiona acompañamiento internacional e **incluso se preparan re ubicaciones en el exterior, que en muchas ocasiones terminan siendo exilios**.

Este proyecto surgió como una respuesta a la persecución, estigmatización y asesinato a los líderes en el país, que para Jaime León, coordinador de la iniciativa, han tenido que huir sin garantías de sus territorios, **"los líderes sociales en Colombia, han sido históricamente perseguidos y asesinados**, es cómo una cruz que deben llevar por apoyar a sus comunidades, por reclamar derechos y por hacer oposición muchas veces a políticas de corrupción" afirmó León.

Asimismo, señaló que con los actos de violencia a los líderes sociales, no solo están en riesgo ellos sino también sus familiares, que en muchas ocasiones han sido también víctimas de quienes intentan frenar el trabajo de los activistas.

<iframe id="audio_29120850" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29120850_4_1.html?c1=ff6600"></iframe>

Las personas que deseen colaborar podrán ingresar a la página www.corporacionclaretiana.org, allí encontrarán el número de cuenta al que se pueden hacer donaciones o comunicarse con la organización para tener otro tipo de información sobre esta apuesta.
