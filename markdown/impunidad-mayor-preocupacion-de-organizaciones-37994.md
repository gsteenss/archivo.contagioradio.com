Title: Impunidad, la mayor preocupación de organizaciones de DDHH con la JEP
Date: 2017-03-21 15:40
Category: Nacional, Paz
Tags: CIDH, impunidad, JEP, jurisdicción especial para la paz, paz, víctimas
Slug: impunidad-mayor-preocupacion-de-organizaciones-37994
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cidh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [21 Mar. 2017]

**“Que la paz no nos cueste la impunidad”,** fue la consigna que se escuchó durante la audiencia sobre las Obligaciones estatales contra la impunidad y la Justicia Especial para la Paz, en la cual las organizaciones de derechos humanos peticionarias reiteraron que la JEP debe **investigar y sancionar a quienes hayan sido parte del conflicto, debe garantizar los derechos de las víctimas y la lucha contra la impunidad** en casos de graves violaciones de derechos humanos y crímenes de guerra.

La Comisión Colombiana de Juristas, la Corporación Jurídica Libertad y el Comité Permanente por la Defensa de los Derechos Humanos, manifestaron que estas audiencias se solicitaron en virtud de las **preocupaciones que varias organizaciones y víctimas están teniendo, con relación a la posible perpetuación de la impunidad** en el marco de la implementación de los Acuerdos de Paz.

“Hemos advertido de la existencia de peligros concretos que significarían un franco desconocimiento de la obligación de investigación y sanción por todos los hechos y responsables en el conflicto” aseguró **Alejandro Malambo integrante de la Comisión Colombiana de Juristas.**

En dicha audiencia se aprovechó para recalcarle al Estado colombiano que tiene la obligación de sancionar y castigar a todos los responsables de graves violaciones a los DD.HH. Según las organizaciones participantes, dicho principio quedaría en riesgo por acciones cometidas por agentes del Estado **“quienes no recibirían un tratamiento simétrico, equilibrado y equitativo en la Jurisdicción Especial para la Paz** sino uno que privilegia su posición en detrimento de la verdad y la justicia de las víctimas de sus crímenes” recalcó Malambo. Le puede interesar: [Congresistas de la Unidad Nacional modificaron la JEP afectando derechos de las víctimas](https://archivo.contagioradio.com/congresistas-la-unidad-nacional-modificaron-la-jep-afectado-derechos-las-victimas/)

De igual modo, se hizo especial hincapié en la importancia de conocer la responsabilidad de mando, tal y como está consagrado en el artículo 20 del Estatuto de Roma y se hizo una grave denuncia “es posible que por las más de 5 mil víctimas en los casos de ejecuciones extrajudiciales, conocidos como “falsos positivos” sucedidas en el periodo 2002-2010, **la cadena de mano no responda por los crímenes cometidos por las tropas”** dijo Malambo.

Y se insistió en la importancia de juzgar a terceros como por ejemplo empresarios, que hayan tenido parte en la financiación de crímenes o en general del conflicto armado interno en Colombia “entendemos que las garantías de no repetición son un proceso, pero se requieren actuaciones de manera inmediata (…) **pedimos que la CIDH formule una lectura jurídica respecto a la sustracción del Artículo 28 del Estatuto de Roma en la JEP por parte de las Fuerzas Militares”** recalcó Danilo Rueda integrante de la Comisión Intereclesial de Justicia y Paz aseguró que.

### **Participación de las víctimas ha sido menoscabada** 

Durante esta audiencia también se hizo una fuerte crítica al tema de la participación de las víctimas en la implementación de los acuerdos de paz **“resarcir a las victimas está en el centro de los acuerdos de paz entre FARC y Gobierno” recordó Harold Vargas de la Corporación Jurídica Libertad.**

Según el jurista, se hubiese esperado que las víctimas participarán en su desarrollo legal en el Congreso así como en el funcionamiento de todo el sistema de verdad, justicia, reparación y garantías de no repetición.

Hasta el momento, dicen las organizaciones sociales, **la participación de las víctimas ha sido menoscabada en las diversas fases de implementación del acuerdo. **

"No se convocó a las víctimas para participar en los debates legislativos del Congreso y al llamar la atención sobre este punto, el Congreso decidió delegar una sola persona, desconociendo la pluralidad de víctimas que existen” aseveró Vargas.

Voluntad, coherencia, buena fe y decisión fueron las características que Verónica Hölker integrante del Comité Permanente por la Defensa de los Derechos Humanos, aseguró se deben tener para que adicional a la firma de los Acuerdos, estos puedan llegar a buen término.

En consecuencia “**modificar estándares internacionales de imputación y rendición de cuentas de agentes del Estado obstaculiza la satisfacción de los derechos de las víctimas** a la justicia, a la verdad y a la reparación” manifestó Hölker. Le puede interesar: [Listado de posibles militares beneficiados con la JEP](https://archivo.contagioradio.com/min-defensa-entrego-listado-de-817-militares-que-quedaran-a-la-espera-de-jep/)

Las organizaciones hicieron un llamado a la CIDH para que pregunte al Estado por los mecanismos que pretende implementar para tratar los graves crímenes cometidos por el Estado y los terceros. Así como aclarar **cómo va a ser el mecanismo que utilizará el Estado para garantizar la participación de las víctimas. **Le puede interesar: ["Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/)

### **¿Qué dijo el Estado?** 

Por su parte, el Estado hizo referencia a cifras que, según ellos, dan cuenta de los significativos avances que han permitido el proceso de paz firmado con las FARC y el trabajo que se ha estado realizando desde la institucionalidad.

El **Ministro del Interior**, Juan Fernando Cristo, se dirigió a las organizaciones aseverando que **“las cifras son la mejor demostración y la más contundente, de cómo mediante la negociación política vamos avanzando** en la mejor garantía para los derechos de las víctimas (…) por ejemplo podemos hablar de una reducción del 13% de homicidios en Colombia entre enero y marzo de 2016 vs. 2017”.

Sin embargo, durante la intervención estatal no se hizo manifiesto, además de las cifras, propuestas o respuestas frente a las preocupaciones entregadas por las organizaciones sociales participantes.

<iframe id="audio_17706164" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17706164_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
