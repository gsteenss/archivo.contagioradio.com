Title: Video Section
Date: 2018-05-11 13:07
Author: AdminContagio
Slug: video-section
Status: published

-   Edit Widget
-   Duplicate Widget
-   Remove Widget

#### Video {#video data-elementor-setting-key="title"}

-   Edit Widget
-   Duplicate Widget
-   Remove Widget

</iframe>"' data-height='600' data-video\_index='1'&gt;  
1  
![Living the Life Full of Wonders and Beauty](https://i.ytimg.com/vi/78RUGbKkMFI/hqdefault.jpg "Living the Life Full of Wonders and Beauty")  
Living the Life Full of Wonders and Beauty01:43</iframe>"' data-height='600' data-video\_index='2'&gt;  
2  
![Bright and Sunny Autumn Days to Remember](https://i.ytimg.com/vi/n8d79M0LtfY/hqdefault.jpg "Bright and Sunny Autumn Days to Remember")  
Bright and Sunny Autumn Days to Remember03:16</iframe>"' data-height='600' data-video\_index='3'&gt;  
3  
![Train Harder, Become Smarter, Live Happier](https://i.ytimg.com/vi/mg67iIFivDo/hqdefault.jpg "Train Harder, Become Smarter, Live Happier")  
Train Harder, Become Smarter, Live Happier06:25</iframe>"' data-height='600' data-video\_index='4'&gt;  
4  
![Enjoy Every Happy Moment of Your Life Anew](https://i.ytimg.com/vi/mhzudo2VrGg/hqdefault.jpg "Enjoy Every Happy Moment of Your Life Anew")  
Enjoy Every Happy Moment of Your Life Anew04:45</iframe>"' data-height='600' data-video\_index='5'&gt;  
5  
![Sweet Blooms and Sunny Spring Days](https://i.ytimg.com/vi/YPMOce2VdKQ/hqdefault.jpg "Sweet Blooms and Sunny Spring Days ")  
Sweet Blooms and Sunny Spring Days 02:11</iframe>"' data-height='600' data-video\_index='6'&gt;  
6  
![All Sparkling Rainbow Colors in One Short Minute](https://i.ytimg.com/vi/c5nhWy7Zoxg/hqdefault.jpg "All Sparkling Rainbow Colors in One Short Minute")  
All Sparkling Rainbow Colors in One Short Minute01:01
