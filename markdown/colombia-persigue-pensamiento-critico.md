Title: En Colombia se persigue al pensamiento crítico
Date: 2019-05-20 12:49
Author: CtgAdm
Category: Expreso Libertad
Tags: pensamiento crítico, presos politicos, Profesor Miguel ángel Beltrán
Slug: colombia-persigue-pensamiento-critico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

En la última rueda de prensa emitida por la Fiscalía, antes de la renuncia de Nestor Humberto Martínez, el ente investigador anunció la posible **creación de una policía universitaria conformada a partir de las colaboraciones de integrantes de la academia**, en los centros universitarios públicos, propuesta que no solo puede considerarse una violación a la autonomía universitaria, sino que recuerda al movimiento estudiantil, las afectaciones de las que han sido víctimas por parte de la Fuerza Pública.

En este programa del Expreso Libertad, escuche las voces de **Miguel Ángel Beltrán**, **María Teresa Pinto, Rosember Ariza** profesores, y **César Barrera**, padre de una de las personas capturadas por los hechos del Centro Comercial Andino, que afirma ser víctima de un falso positivo judicial; hablando de **la persecución al pensamiento crítico en Colombia, el uso de inteligencia e infiltrados para montar falsas acusaciones contra estudiantes** y la falta de garantías para ejercer un libre pensamiento en el país.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F598009780704989%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<iframe id="audio_36093324" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36093324_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
