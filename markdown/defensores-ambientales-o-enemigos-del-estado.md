Title: Son defensores ambientales no enemigos del Estado
Date: 2019-08-08 19:07
Author: CtgAdm
Category: Ambiente, Líderes sociales
Tags: asesinatos de líderes sociales, defensores ambientalistas, Defensores ambiente, Global Witness
Slug: defensores-ambientales-o-enemigos-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Victor-MoriyamaGetty-Images.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/7554040a-aa61-4a24-bca6-07c7460c5687.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Schermata-2019-08-02-alle-09.06.35.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Schermata-2019-08-02-alle-09.06.48.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/715db73d-9994-4bd4-ac59-8bcbf97ea92c.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Victor Moriyama/Getty Images] 

La organización *Global Witness *presentó un informe **sobre 164 asesinatos de defensores ambientales y de la tierra en el mundo**. **Latinoamérica, **en este sentido, resulta ser la región mas peligrosa para estos defensores **al reportarse 83 homicidios**, numero que podría ser mayor, debido a innumerables casos que habrían sido silenciados a través de amenazas de muerte, demandas judiciales, por falta de informaciones o por  los estrictos procesos de verificación.

*Global Witness* es una asociación del Reino Unido que se ocupa de campañas e investigaciones para terminar con los abusos ambientales, como de derechos humanos, provocados ​​por la explotación de los recursos naturales y de la corrupción en el sistema político y económico global.

### **Los recursos naturales en Latinoamérica** 

Según declara la investigación, los Estados latinoamericanos, muy a menudo, **no toman en cuenta la importancia que tienen estos recursos naturales para la supervivencia de las comunidades indígenas o de la población rural**.  En el marco de la llegada de las multinacionales la voz de las comunidades no se escucha, por ejemplo, mediante procesos como la consulta previa, o simplemente, cuando expresan su oposición a los proyectos.

Las empresas multinacionales tienen mucha influencia sobre los gobiernos, y por esta razón las instituciones no estarían garantizando el derecho a decidir sobre el uso de sus territorios y los recursos naturales. "**Lo más grave es que** **ponen en peligro las comunidades indígenas, que deberían ser las más protegidas por el derecho internaciona**l y en cambio, se encuentran como las más vulnerables (...) y las personas que intentan defenderlas siguen siendo amenazadas y asesinadas» declaró Ben Leather, integrante de *Global Witness*.

Hay una doble falta en la responsabilidad de parte de los que deben proteger a estos líderes: la primera es del Estado que tendría que regular las actividades de las empresas según las normativas ambientales, pero también los financiadores internacionales que están respaldando y apoyando empresas que trabajan con proyectos asociados con abusos de los derechos y ataques contra de defensoras y defensores.

### **Colombia: una ventana única** 

![Países donde asesinaron defensores ambientales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/715db73d-9994-4bd4-ac59-8bcbf97ea92c-1024x1024.jpg){.aligncenter .wp-image-71819 width="493" height="493"}

Colombia, ubicada solamente después de Filipinas en la estadística del informe, representa un caso particular tratando del tema ambiental debido a variables particulares que contribuyen  a empeorar el estado del ambiente.

El país posee, de hecho, una **sociedad civil muy activa **por lo que concierne a la lucha ambiental, y en muchos casos** se vuelve exitosa **en asegurar una rendición de cuentas cuando las empresas y el Estado están cometiendo abusos. **Ejercer este poder puede representar también un riesgo**.

### La reacción a cadena que llevó Colombia a estos resultados

Vale la pena decir que Colombia es un país que tiene muchos recursos naturales y este patrimonio está amenazado por una reacción en cadena de razones que tienen que ver principalmente con los antecedentes históricos del País. **El primer anillo es representado por el conflicto armado que, después de los acuerdos de paz y el desmantelamiento de los territorios ocupados por la guerrilla, ha significado para muchos territorios ser “tierra de nadie**”.

Estos territorios afectados por el desplazamiento de los habitantes locales se quedaron libres y por lo tanto, se encontró el pretexto, también gracias al aval del Estado, para otorgar licencias a las multinacionales para que los exploten.

Asimismo, afirmó Leather: "Muchas empresas en Colombia como las mineras, las de agro negocios y las hidroeléctricas están llevando a cabo actividades que son dañinas para el medio ambiente (...) Colombia ha estado encima de nuestras cifras por varios años también por las amenazas y asesinatos de lideres y lideresas, y esto tiene que ver con el rol del Estado".

Un ejemplo que se puede usar para explicar como los Estados se involucran negativamente, son las falsas promesas que se hacen en las campañas electorales como la del presidente Duque, donde declaró que no emprendería estudios sobre el fracking o la fumigación con glifosato. De la misma manera, en la campaña de Bolsonaro se prometió que no se iba a poner en riesgo la reserva indígena;  ambas, promesas que fueron totalmente olvidadas.

De igual forma, la falta de acceso a la justicia es un factor importante que se encuentra en Colombia, y **el hecho que estos crímenes queden en el impunidad actúa como una “luz verde” para los perpetradores, que piensan que pueden hacer su ataques sin consecuencias**.

### **Un llamado a la protección del ambiente en el mundo** 

Aun si las empresas del exterior están aumentando su propia conciencia sobre los daños que han ocurrido en los diferentes países del mundo debido a la explotación de los recursos ambientales, es necesario acelerar una respuesta e implementar políticas más eficaces y menos invasivas para los territorios, porque lo que se ha hecho hasta ahora no es suficiente.

Otro aspecto que se evidencia en la lucha por la defensa de los recursos naturales anivel mundial es que los defensores del ambiente a menudo son  encarcelados y discriminados, precisamente, por aquellos organismos que deberían garantizarles protección. El informe registra un **aumento en la criminalización del activismo ambiental** en el que **quienes se están oponiendo a proyectos de explotación ambiental, han recibido sentencias muy largas por llevar a cabo protestas pacificas**.

Es evidente también la sensación de solidaridad entre los defensores alrededor del mundo, particularmente en este momento histórico de crisis climática. Es cada vez mas claro, afirmó el activista, que para las diferentes poblaciones del mundo “**los mayores e importantes aliados en la lucha para el futuro del planeta son precisamente estos defensores de la tierra y del medio ambiente que están siendo atacados y asesinados, **y todos tenemos el deber de protegerlos, como a  y su importante esfuerzo para la comunidad mundial”. (Le puede interesar: [Países de América Latina alistan protección legal para defensores ambientales](https://archivo.contagioradio.com/paises-de-america-latina-alistan-medida-de-proteccion-legal-para-defensores-del-ambiente/))

![Defensores derechos ambientales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Schermata-2019-08-02-alle-09.06.35-751x1024.png){.wp-image-71808 .aligncenter width="553" height="754"}![Defensores derechos ambientales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Schermata-2019-08-02-alle-09.06.48-728x1024.png){.wp-image-71809 .aligncenter width="553" height="778"}

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
