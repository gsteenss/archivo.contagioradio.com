Title: En Bogotá atentan contra Gustavo Gallardo, abogado defensor de Jesús Santrich
Date: 2019-08-08 13:30
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Atentado, Bogotá, Gustavo Gallardo, Jesús Santrich
Slug: bogota-atentan-gustavo-gallardo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Gustavo-Gallardo-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Action Peace ORG  
] 

La noche del pasado miércoles 7 de agosto, el abogado **Gustavo Gallardo fue víctima de hombres armados que dispararon en repetidas ocasiones contra el vehículo de protección** en el que se encontraba junto a sus escoltas en Bogotá. La acción no generó daños humanos, pero según el partido FARC, "enciende las alarmas sobre las condiciones, garantías y riesgos de seguridad que rodea los equipos de defensa del Partido y de los exjefes guerrilleros en proceso de reincorporación".

### **El atentado ocurrió en Bogotá, en "pleno centro administrativo"**

Gallardo estuvo en horas de la tarde del miércoles en el Espacio Territorial de Capacitación y Normalización (ETCR) Amaury Rodríguez, en Pondores (La Guajira), acompañando una diligencia adelantada por fiscales de la Jurisdicción Especial para la Paz (JEP). En la visita al ETCR,  **los fiscales revisaron temas de seguridad de algunos comparecientes en el caso 01 abierto por la Jurisdicción**, sobre retención ilegal de personas por parte de las Farc-EP.

A su llegada a Bogotá en horas de la noche, mientras el Abogado se desplazaba en su esquema de seguridad hacia su casa, **una camioneta azul lo interceptó en la zona del Centro Internacional, donde hombres armados dispararon en su contra**. Gracias a la pronta reacción de los escoltas, los sujetos se dieron a la huída sin causar daños humanos. Sin embargo, llamó la atención sobre la seguridad, porque "es en la ciudad capital que ocurre esto, en pleno centro administrativo, donde uno presumiría que hay ciertas condiciones de seguridad".

Gustavo Gallardo asumió la defensa de Santrich en el marco del caso 01 ante la JEP, así como el que abrió la Corte Suprema de Justicia por presuntamente cometer delitos posteriores a la firma del Acuerdo de Paz en 2016. Ello ha significado, riesgos para su seguridad física, la interceptación de sus comunicaciones y seguimientos, por lo que declaró que "el atentado es un hecho más de lo que cotidianamente vengo siendo víctima". (Le puede interesar: ["Piden cancelar audiencia contra Santrich en la Corte Suprema de Justicia"](https://archivo.contagioradio.com/piden-cancelar-audiencia-contra-santrich-en-la-corte-suprema-de-justicia/))

### **¿Y los entes encargados de preservar la vida de defensores y defensoras de DD.HH.?**

Gallardo sostuvo que tratándose de la Fiscalía, "no ha habido investigaciones serias", lo que genera que atentados y denuncias queden perdidos en el tiempo. En cuanto a la protección denunció que "desde hace más de tres meses vengo solicitando la ampliación de mi esquema, y **la respuesta institucional de la Unidad Nacional de Protección (UNP), es que no hay escoltas ni recursos"**. (Le puede interesar: ["Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich"](https://archivo.contagioradio.com/denuncian-dea-violo-soberania-santrich/))

**Síguenos en Facebook:**

> Alertamos sobre el atentado del cual fue víctima en la noche de ayer, el abogado defensor de Derechos Humanos y del [@PartidoFARC](https://twitter.com/PartidoFARC?ref_src=twsrc%5Etfw) , Gustavo Gallardo.
>
> Exigimos garantías para quienes contribuimos a la construcción de paz. [pic.twitter.com/0e5lloHV8f](https://t.co/0e5lloHV8f)
>
> — Benedicto González (@BenedictoFARC) [August 8, 2019](https://twitter.com/BenedictoFARC/status/1159492387363786752?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no">
</iframe>
  

<iframe id="audio_39648972" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39648972_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
