Title: Control de AGC se consolida en cuarentena en Bajo Cauca Antioqueño
Date: 2020-03-31 21:20
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: AGC, Covid-19, Tarazá
Slug: control-de-agc-se-consolida-en-cuarentena-en-bajo-cauca-antioqueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/AGC-en-Bajo-Cauca-A.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: La Chiva de Urabá {#foto-la-chiva-de-urabá .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales denunciaron que en días pasados, vía mensajes de WhatsApp, las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), también llamados por el Gobierno como Clan del Golfo, decretaron una completa restricción a la movilidad en el municipio de Tarazá, subregión del Bajo Cauca Antioqueño, en lo que algunos califican como una muestra de poder territorial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GarantiasPSG/status/1244310007862251524","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GarantiasPSG/status/1244310007862251524

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Por el Covid-19 "hay reducción en los fenómenos de violencia, pero no en las formas de control"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Óscar Yesid Zapata, integrante de la Coordinación Colombia, Europa, Estados Unidos, recordó que para nadie es un secreto el control que ejercen grupos denominados por ellos como paramilitares, en este caso las AGC del Bajo Cauca. En ese sentido, explicó el toque de queda impuesto por esta organización en Tarazá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque Zapata sostuvo que hay una consolidación de la Fuerza Pública en la subregión, añadió que Tarazá está bajo el control de los ilegales al punto que las comunidades han acatado el mensaje de las AGC por temor a las amenazas. El defensor de DD.HH. dijo que por efectos del Covid-19 han visto una "reducción en los fenómenos de violencia, pero no en las formas de control", y ello se evidencia en el toque de queda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En tal sentido, explicó que la preocupación es que los ilegales "están suplantando una función del Estado" y esto puede generar hechos de violencia contra las comunidades, que ante necesidades básicas tendrían que movilizarse por el territorio. (Le puede interesar: ["En el Bajo Cauca persiste el reclutamiento forzado de niños"](https://archivo.contagioradio.com/en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El control territorial de las AGC en el Bajo Cauca**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En lugares como el sur de Córdoba o el Bajo Atrato [organizaciones defensoras de DD.HH.](https://www.justiciaypazcolombia.com/operativos-militares-sin-conocimiento-de-protocolos-de-prevencion-covid19-2/) han señalado que las autodenominadas AGC instalan puntos (lugares de inteligencia) en las comunidades para controlar lo que hacen, igualmente han señalado que hacen reclutamientos forzados y controlan los lugares de tránsito. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zapata sostuvo que algo similar ocurre en el Bajo Cauca, porque en zonas rurales el control de las AGC llega al punto en que hacen reuniones con las comunidades, hacen censos de las mismas y establecen directrices sobre cómo deben actuar. En zonas Urbanas, "se instalan lugares para hacer inteligencia", afirmó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El integrante de la Coordinación concluyó que este grupo tiene una facilidad de movilidad en el territorio y control del mismo porque "presumiblemente tienen alianzas con otros grupos armados legales o ilegales", razón por la que recordó la importancia de que el Estado asuma la labor de protección de las comunidades, y desmonte este tipo de estructuras criminales.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GarantiasPSG/status/1244808174697426944","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GarantiasPSG/status/1244808174697426944

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [En el Bajo Cauca persiste el reclutamiento forzado de niños](https://archivo.contagioradio.com/en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
