Title: Nuevo atentado contra líder indígena Wayuu
Date: 2016-12-04 09:06
Category: DDHH, Nacional
Tags: Implementación de Acuerdos, Líderes indígenas asesinados, Pueblos Wayuu en Colombia
Slug: nuevo-atentado-lider-indigena-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las2Orillas] 

###### [4 Dic 2016] 

El pasado 3 de Diciembre Carlos Ramírez líder comunitario del proceso Indígena Wayuu de Fonseca al sur de la Guajira, recibió 3 disparos a manos de hombres en moto,  luego de una reunión entre el Resguardo Mayamangloma, del que era integrante, y el ICBF sobre trabajar temas organizativos. **Según las autoridades indígenas es el segundo líder indígena abaleado en menos de 15 días en territorio Wayuu, y las mismas aseguran que el líder se encuentra fuera de peligro en el Hospital San Juan del Cesar.  
**

A través de un comunicado, las autoridades manifestaron que varios de los integrantes del Resguardo de Mayamangloma y otros resguardos del sur de la Guajira ubicados en los municipios de La Jagua del Pilar, Urumita, Villanueva, El Molino, San Juan del Cesar, Distracción, Fonseca, Barrancas, Albania y Hatonuevo, **“hemos recibido amenazas de esta índole, pero seguiremos trabajando de manera abierta y decidida por nuestras comunidades”.**

Además, señalan que “rechazamos estos ataques a nuestros hermanos Indígenas y dejamos claro que seguimos en pie de lucha por nuestros derechos”, exigen a través de la misiva que “las autoridades **den garantías de seguridad en nuestros territorios a todas las comunidades y a nuestras organizaciones hermanas”.**

Por último resaltan que los **entes correspondientes deben adelantar los procesos de investigación y esclarecer los hechos**, y hacen un llamado a las organizaciones sociales y medios de comunicación para que apoyen con la difusión de los sucesos y estén atentos, pues **“no queremos lamentar más estos hechos irresponsables que atentan contra la vida de nuestros compañeros Wayuu”.   **

###### Reciba toda la información de Contagio Radio en [[su correo]
