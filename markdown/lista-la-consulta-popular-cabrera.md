Title: Lista la Consulta Popular en Cabrera
Date: 2017-02-24 15:29
Category: Ambiente, Nacional
Tags: "No al Paso", Cabrera Cundinamarca, Zona de Reserva Campesina de Cabrera
Slug: lista-la-consulta-popular-cabrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/sumapáz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PNN] 

###### [24 Feb 2017] 

Este 26 de Febrero las comunidades de Cabrera Cundinamarca harán Consulta Popular para decirle No al proyecto ‘El Paso’, que pretende construir **14 microcentrales hidroeléctricas sobre el río Sumapáz**, y al avance de exploraciones para los bloques petroleros COR33 y COR 4, en la Consulta Popular.

Son 1200 personas las que deberán votar a la pregunta ¿Está usted de acuerdo si o no, que en el municipio de cabrera como ZRC, se ejecuten proyectos mineros y/o hidroeléctricos, que transformen o afecten el uso del suelo, el agua o la vocación agropecuaria del municipio?, para que dicho ejercicio de participación ciudadana sea válido.

Paola Bolaños vocera e integrante de la Zona de Reserva Campesina de Cabrera, manifestó que de llevarse a cabo los proyectos extractivos, otros municipios como **Venecia, Pandi e Icononzo en el Tolima, también se verían gravemente afectados.** ([Le puede interesar: Habitantes de](https://archivo.contagioradio.com/35010/)Cabrera[decidirán sobre micro-hidroeléctricas en Sumapáz](https://archivo.contagioradio.com/35010/))

###  

El proyecto hidroeléctrico abarca toda la cuenca media del río Sumapáz, desde el nacimiento, en la parte alta que es zona de amortiguamiento del páramo, hasta la desembocadura en el Boquerón de Icononzo y dichas “actividades nocivas” afectan lo ambiental y también lo cultural, **“la identidad campesina y la vocación agropecuaria de nuestros municipios se está viendo vulnerada”** puntualizó Bolaños**.**

Por otra parte, Rafael Acosta integrante de la ZRC de Cabrera e impulsor de la iniciativa, aseguró que en varias ocasiones se han dirigido al Gobierno Nacional para que invierta en proyectos agropecuarios, **pero no han obtenido respuesta ni solución alguna.**

Por último Bolaños y Acosta, extienden la invitación a diferentes organizaciones ambientales y defensoras del agua, la vida y el territorio, para que acompañen **el próximo 26 de febrero la Consulta Popular para decirle “No al Paso”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
