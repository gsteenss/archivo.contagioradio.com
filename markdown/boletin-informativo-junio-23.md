Title: Boletín informativo junio 23
Date: 2015-06-23 17:12
Category: datos
Tags: 45 millones de personas respiran aire contaminado, abitantes del municipio El Mango, asesinado en el municipio de Inzá campesino Enrique Bastidas, ASOQUIMBO, audiencia pública ante la Autoridad Nacional de Licencias Ambientales ANLA, Cauca, decidieron sacar la estación de policía del casco urbano, en Argelia, medida de aseguramiento contra coronel (r) Plazas Acevedo por masacre de Mapiripán, Miller Dussán investigador de ASOQUIMBO
Slug: boletin-informativo-junio-23
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4679311_2_1.html?data=lZukm5iVdY6ZmKiakpqJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZeXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El Gobierno del Huila comunicó a **ASOQUIMBO**, que atendiendo a las **denuncias de las comunidades**, se ha solicitado dos veces audiencia pública ante la **Autoridad Nacional de Licencias Ambientales ANLA**, sin embargo, la respuesta siempre ha sido **negativa**, razón por la que la **Contraloría General de la Nación** cuestiona el accionar de la autoridad ambiental. Habla **Miller Dussán**, investigador de **ASOQUIMBO.**

-El lunes en la madrugada en el **resguardo de San Andrés**, en el municipio de **Inzá**, Cauca, un campesino del **resguardo indígena Santa Rosa,** identificado como **Enrique Bastidas**, fue asesinado a manos de militares, que según la comunidad, mantienen intimidados a los pobladores, por lo que los indígenas decidieron retener a los militares acusados del asesinato. Habla **Emigdio Velasco**, coordinador del programa de derechos humanos del **Consejo Regional Indígena del Cauca, CRIC**.

-De acuerdo con el **Informe Estatal de Calidad del Aire** en 2014, de Ecologistas en Acción, cerca de **45** millones de personas respiran **aire contaminado,** 474.000 kilómetros cuadrados soportan una **contaminación** que daña los bosques y cultivos; y el **95%** de la población y el **94%** del territorio estuvieron expuestos a unos niveles de contaminación que superan las recomendaciones de la **Organización Mundial de la Salud**.

-Fiscalía dictó medida de aseguramiento contra **coronel (r) Plazas Acevedo** por masacre de Mapiripán. Habla **Eduardo Carreño**, Abogado del Colectivo **José Alvear Restrepo**.

-En un acto de resistencia y desobediencia civil, los habitantes del municipio **El Mango**, en Argelia, Cauca, decidieron **sacar la estación de policía** del casco urbano del corregimiento. Afirman que se han **recrudecido los enfrentamientos** entre la guerrilla y las fuerzas militares, y que ha habido hostigamiento que ponen en riesgo la vida de la población civil.
