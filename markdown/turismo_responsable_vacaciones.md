Title: Cinco claves para hacer turismo responsable en temporada de descanso
Date: 2017-12-27 12:49
Category: Ambiente, Animales, Nacional
Tags: ecoturismo, tráfico de fauna silvestre, turismo responsable
Slug: turismo_responsable_vacaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/turismo-responsable-e1514396732696.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [27 Dic 2017] 

Para esta época seguro estás pensado en el mejor lugar para descansar y salir de la rutina diaria. Muchos piensan en un hotel, otros en acampar, o en planes de un día como una caminata ecológica. Lo importante es, que sea cual sea tu plan, debes tener en cuenta respetar la naturaleza y no promover ciertas actividades o acciones que muchas veces impactan de manera irreversible los ecosistemas. Aquí te presentamos esta lista para hacer turismo responsable.

### **1. No promover el maltrato animal**

Muchas veces cuando viajamos a ciertos lugares, algunos de los principales atractivos suelen ser los zoológicos, acuarios, carruajes empujados por caballos u otros animales, o incluso consumir ciertos animales como tiburones o huevos de tortuga. Son actividades que se venden como algo que no se puede dejar de hacer.

Sin embargo, aunque parecen normales, inocentes, tradicionales y lúdicas, es necesario ser consientes del impacto que causamos a los animales y debemos procurar minimizar, o mejor, no realizar actividades turísticas que pueden causar graves consecuencias en otros ser vivos.

Detrás de algunas actividades en las que participan animales, suele esconderse sufrimiento y maltrato animal. Es mejor invertir nuestro dinero en atracciones sin crueldad. En ese sentido es vital entender que siempre será mejor observar a los animales en su hábitat natural, respetar su entorno y no interferir en el desarrollo natural de sus comportamientos.

Lo anterior implica no acercarnos demasiado a los animales con el objeto de tocarlos, obtener la mejor fotografía sin tener en cuenta la distancia o ofrecerles comida. También es importante no trasladar animales silvestres. Por más pequeño que sea puede generar un desequilibrio en el ecosistema.

Si quieres ver aves silvestres, no las observes enjauladas, mucho menos las compres y denuncia a quienes las comercialicen. Hay lugares donde se hace avistamiento de aves, así que puedes ir a esos espacios a verlas. Finalmente es recomendable contratar viajes y vacaciones con agentes turísticos que cuenten con una política de bienestar animal.

### **2. No dejes huellas a tu paso** 

Procura que no haya evidencia alguna de que pasaste por allí. La regla es simple, no dejes residuos de basura, sino hay donde botarlos, carga una bolsa donde los puedas ir acumulando o guárdala en tu bolso o maleta hasta que encuentres donde dejarla.

Asimismo es esencial que no recolectes plantas ni dejes nada fuera de su lugar original. No enciendas fuegos no autorizados, y si es permitido hacer alguna fogata, moja siempre el suelo cuando termines tu actividad. También es importante conocer los niveles de ruido permitidos, usualmente si se trata de un hábitat natural, los altos niveles de sonido pueden causar efectos en el ecosistema.

Otra recomendación, es no usar bloqueadores y bronceadores grasos que pueden contaminar los ecosistemas acuáticos. Además, si vas a playas se cuidadoso con tus residuos y no dejes envolturas de plástico, latas o vidrio en la arena, el mar se lo lleva y afecta a la fauna marina, además los perros, aves, gatos y humanos pueden herirse con ese tipo materiales.

### **3. Reduce tu huella de Carbono** 

Para reducir al mínimo tu huella de carbono intenta no abusar de las piscinas; renuncia al cambio de toallas diario; báñate con la misma frecuencia que en casa; evita dañar zonas de montaña que sufren los efectos del cambio climático si quieres esquiar, y elige pistas que hayan adoptado medidas para reducir al mínimo su impacto ambiental. Además puedes ahorrar energía, es decir  disminuir el uso de la calefacción, aire acondicionado, luces, etc.

Es importante tratar de no comprar tantas cosas que tengan plástico y en esa línea, procura no usar pitillos; si vas a comprar cosas lleva tu bolsa de tela y no compres bolsas de plástico para cada artículo adquirido. Al comprar regalos y recuerdos busca productos locales, así contribuyes a la economía  y a la cultura de los pueblos que te acogen.

### **4. Respeta los ecosistemas que visites** 

Al planificar tu viaje, elije agencias, hoteles y planes turísticos que ofrezcan garantías de calidad y de respeto a los derechos humanos y al ambiente. Por eso es importante evitar algunos lugares que, por la gran cantidad de consumo de agua que suponen, ruidos generados e impacto sobre la biodiversidad y el paisaje, carecen totalmente de alternativas sostenibles, es decir que es mejor evitar los sitios muy concurridos.

Si vas a caminatas ecológicas,  hazlo con empresas certificadas que sepan de la capacidad  de visitantes del lugar, ya que muchas veces el exceso de personas en lugares sensibles, puede generar daños en el suelo.

Si visita ecosistemas sensibles, como arrecifes de coral o selvas, infórmate de cómo hacerlo para causar el menor impacto posible y no degradarlos. No adquieras flora y fauna protegida por el Convenio de Comercio Internacional de Especies Amenazadas de Fauna y Flora Silvestres (CITES), ni productos derivados de dichas especies. Es un delito y contribuye a su extinción.

### **5. Evalúa tus viajes en autobús, bicicleta, carro o avión...**

En la medida de lo posible utiliza el transporte público, bicicleta o incluso camina para trasladarte por los lugares cercanos a tu destino. Recuerda que el carro es un foco de emisiones contaminantes para la atmósfera y por ende para la salud humana, el precio de los combustibles fósiles es alto y poco seguro.

Por otra parte, los vuelos en avión son responsables del 5% de las emisiones contribuyentes al calentamiento global. Un vuelo intercontinental produce más emisiones de CO2/pasajero que un conductor medio al año y en cambio te perderás todo el paisaje del recorrido.

Si a donde vas, hay posibilidad de usar tren, hazlo pues debes saber que los trenes producen 20 veces menos emisiones de CO2 por km. Los trenes y buses descongestionan calles y carreteras.

Si pones en práctica estas recomendaciones podrás tener la satisfacción de hacer un turismo responsable.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
