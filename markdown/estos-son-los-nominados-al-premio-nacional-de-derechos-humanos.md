Title: Estos son los nominados al Premio Nacional de Derechos Humanos
Date: 2015-09-02 07:00
Category: DDHH, Nacional
Tags: colectivo de Abogados José Alvear Restrepo, Diakonia, Francia Elena Márquez Mina, José Alfredy Galvis Torres, Judit Maldonado, la Asociación Campesina del Valle del Río Cimitarra y el Proceso de Comunidades Negras, Monseñor Héctor Epalza Quintero, Mujeres Caminando por la Verdad, Patricia Ariza, Premio Nacional de Derechos Humanos, Presbítero Adriel José Ruíz Galván, Red de Consejos Comunitarios del Pacífico Sur –Recompas-
Slug: estos-son-los-nominados-al-premio-nacional-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/premio_DDHH_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imágen: premio nacional de DDHH 

###### [1 Sept 2015]

El Premio es un gran **acto de homenaje a defensores y defensoras de Derechos  Humanos en Colombia, concedido por la comunidad internacional** para reconocer el aporte que las personas o las organizaciones realizan en el camino por la construcción de paz y de una sociedad en democracia y busca visibilizar las luchas de cientos de personas, que algunas veces invisibles, trabajan por la defensa y promoción de los DDHH.

Este premio se entrega anualmente desde hace 4 años y ha reconocido la trayectoria de diversas organizaciones y personas que han dedicado su vida a la defensa de los derechos humanos. Algunos de los ganadores en versiones anteriores han sido **Judit Maldonado, el Colectivo de Abogados José Alvear Restrepo, la Fundación Casa de la Mujer y Patricia Ariza en la versión 2014.**

El premio incluye varias categorías, la primera es a **Defensor o Defensora del año**, para este año los nominados en esta categoría son Francia Elena Márquez Mina, Monseñor Héctor Epalza Quintero, Presbítero Adriel José Ruíz Galván, José Alfredy Galvis Torres.

En el **proceso colectivo del año** los nominados son Mujeres Caminando por la Verdad, Red de Consejos Comunitarios del Pacífico Sur –Recompas-, la Asociación Campesina del Valle del Río Cimitarra y el Proceso de Comunidades Negras – Palenque Buenaventura -. **Proceso colectivo a nivel ONG** los nominados son Colombia Diversa, Diócesis de Tumaco, Corporación Social para la ayuda y capacitación – COSPAC – y la Fundación Forjando Futuros.

En la categoría a **Toda una Vida** Fabiola Lalinde, Venus Carrillo, Benjamin Cardona y Emma Doris López. En la categoría a **toda una vida en el nivel de organizaciones** está nominada Consejo Comunitario Mayor de la Asociación Campesina del Atrato –COCOMACIA-, Corporación Vamos Mujer, Comité Permanente por la Defensa de los Derechos Humanos y el Colectivo 16 de Mayo.

Este reconocimiento es organizado por Diakonía Colombia y cuenta con la participación de jurados tanto nacionales como internacionales entre los que se encuentran defensores y defensoras de los DDHH reconocidos mundialmente. La ceremonia de premiación este próximo miércoles **9 de Septiembre, día internacional de los Derechos Humanos en el Centro de Memoria, Paz y Reconciliación de Bogotá a partir de las 8 de la mañana.**
