Title: Crimen organizado, violencia y narcotráfico acechan Bogotá
Date: 2019-09-11 18:11
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Aguilas Negras, Amenazas, Bogotá, narcotrafico
Slug: crimen-organizado-violencia-narcotrafico-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Bogotá-e1568243280950.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia  
] 

En un mensaje de Wahtsapp, las autodenominadas Águilas Negras, Bloque Capital, enviaron una amenaza en la que declaraba objetivo militar a organizaciones como MOVICE, Colombia Humana, FARC y personas como Ariel Ávila, León Valencia, Hollman Morris y Alirio Uribe. Allí se los señala como objetivo militar por parte de este grupo, algo que ha generado alarma, **dada la situación de violencia por la que está pasando Bogotá.**

### **"Esas amenazas siempre llegan a las personas que alzamos la voz y denunciamos la corrupción"** 

**León Valencia, director de la Fundación Paz y Reconciliación** y uno de los mencionados en la amenaza, afirmó que dichos mensajes siempre llegan "a las personas que alzamos la voz y denunciamos la corrupción"; sobre quienes firman el mensaje, Valencia afirmó que son en realidad un actor mediático, pero cuya estructura no se conoce, no se sabe quienes son, ni cómo operan. "Algunos señalan que incluso son miembros de las fuerzas militares que utilizan ese nombre para amenazar", añadió el experto.

Adicionalmente, Valencia destacó su preocupación sobre la situación en general de violencia que se está creando en Bogotá, y recordó que "se han visto fosas, se vió esta semana un ataque a una tractomula que venía con cocaína, y se viene descubriendo la presencia de organizaciones del crimen organizado en Bogotá", tomando en cuenta que además estamos en medio de un contexto electoral. (Le puede interesar: ["La violencia neoparamilitar en la frontera sur de la capital"](https://archivo.contagioradio.com/neoparamilitar-frontera-bogota/))

### **Bogotá: Una ciudad atractiva para los negocios ilegales** 

Paz y Reconciliación ha documentado algunas de las situaciones de violencia en la capital, por eso, Valencia explicó que a raíz de lo que han podido observar se puede decir que "este es un lugar muy importante para el crimen organizado" porque es una ciudad con clases medias y sectores con buenos ingresos, en los que ha aumentado el consumo de drogas. El experto añadió que también es un sitio de salida de droga para el exterior, y un lugar grande para el lavado de activos, "eso hace de Bogotá una ciudad muy atractiva para los negocios ilegales".

Uno de los hechos que permiten que Bogotá sea objeto de disputa para el narcotráfico es el **aumentó del consumo de sustancias, y la corrupción**, "porque es muy difícil entrar aca droga si la policía de carreteras mantiene un estricto control de lo que pasa, pero a veces hay complicidad", concluyó Valencia. (Le puede interesar: ["La violencia neoparamilitar en la frontera sur de la ciudad"](https://archivo.contagioradio.com/violencia-occidente-de-bogota/))

### **¿Qué hacer y qué no hacer para enfrentar la situación?** 

Ante la serie de hechos violentos como las amenazas o la aparición de cuerpos desmembrados, Valencia aseguró que se debía informar el tema por parte de las autoridades civiles, y **presentar a los ciudadanos la gravedad de los hechos para evitar crear sensaciones de zozobra**. En cuanto a la propia amenaza recibida, dijo que ya entregó detalles a las autoridades encargadas, y espera una respuesta que permita establecer la procedencia del mensaje.

<iframe id="audio_41570076" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41570076_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
