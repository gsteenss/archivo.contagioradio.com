Title: "Paro armado del ELN tensiona más el ambiente para la mesa en Quito": Luis Eduardo Celis
Date: 2018-02-07 12:43
Category: Otra Mirada, Paz
Tags: ELN, gobierno colombiano, Mesa de Quito, paro armado del ELN
Slug: paro_armado_mesa_quito_eln_gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Feb 2018] 

[**"No es una buena noticia  para un ambiente de reanudación de las conversaciones",** señala Luis Eduardo Celis, integrante de la Mesa Social por la Paz, ante el anuncio de la guerrilla del ELN sobre la realización de un paro armado en todo el territorio nacional. Según el comunicado divulgado este miércoles por ese grupo armado, el paro arrancaría el 10 de febrero e iría hasta el próximo martes 13 de febrero. ]

Para Celis se trata de un anuncio desafortunado, ya que, de acuerdo con él esto no contribuye a la posibilidad de continuar el quinto ciclo de conversaciones en Quito, Ecuador. Asimismo, aunque el ELN asegura que se trata de una medida en protesta por los asesinatos de líderes sociales en todo el país, el analista asegura que "con esa medida no se para la violencia contra los líderes. **Eso es bastante distanciado de la realidad, en cambio lo que se hace es afectar a la población".**

Sin embargo, se trata de una situación normal en medio de un conflicto, teniendo en cuenta los bombardeos de las fuerzas militares contra el ELN, que además afectan a poblaciones indígenas como sucedió en pasados días con un resguardo en Chocó, este panorama de recrudecimiento de la guerra lo único que está logrando es "**tensionar el ambiente entre las partes, alejando la posibilidad de iniciar el quinto ciclo"**. explica Celis.

[![comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/comunicado-545x800.png){.alignnone .size-medium .wp-image-51264 .aligncenter width="545" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/comunicado-e1518023986825.png)

### **¿Cómo avanzar hacia la mesa en Quito?** 

De acuerdo con el integrante de la Mesa Social para la Paz, es evidente que en este momento chocan dos lógicas: la del ELN,  desde donde se afirma que se había pactado negociar en medio del conflicto, por lo cual no se estaría incumpliendo nada; y la perspectiva del presidente Santos, dirigida **"a una lógica de sensibilidad política en medio de una campaña electoral".**

"Nadie dijo que resolver un conflicto armado era fácil.  **Toca seguir buscando un camino, pero eso no pasa sin fórmulas viables",** dice Celis quien propone que la sociedad civil, desde las organizaciones sociales deben continuar en el diálogo con la guerrilla y el gobierno del presidente Juan Manuel Santos, con el fin de encontrar un camino para que se reanuden las conversaciones. (Le puede interesar: ["Proceso de Paz con ELN es un bien público y debe retomarse")](https://archivo.contagioradio.com/proceso-de-paz-con-el-eln-es-un-bien-publico-y-debe-retomarse-movimientos-sociales/)

<iframe id="audio_23618966" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23618966_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
