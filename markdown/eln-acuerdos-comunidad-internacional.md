Title: ELN depositó acuerdos parciales de paz ante la comunidad internacional
Date: 2019-07-03 17:11
Author: CtgAdm
Category: Paz, Política
Tags: acuerdo de paz, CICR, ELN, ONU
Slug: eln-acuerdos-comunidad-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ELN  
] 

Un comunicado del Ejército de Liberación Nacional (ELN) difundido este miércoles 3 de julio anuncia que la delegación de paz de **este grupo depositó los avances de los acuerdos alcanzados con el gobierno de Juan Manuel Santos ante instancias internacionales** como la ONU, países garantes de ese proceso de paz y otros integrantes de la comunidad internacional como el Comité Internacional de la Cruz Roja (CICR).

En la comunicación, el grupo afirma que los documentos de los acuerdos alcanzados se radicaron en Nueva York (EE.UU.), ante la Secretaría General y el Consejo de Seguridad de la Organización de las Naciones Unidas (ONU) con el apoyo de instituciones ecuménicas como el Consejo Mundial de Iglesias y el Diálogo Intereclesial por la Paz (DIPAZ), entre otras entidades comprometidas con la paz y los derechos humanos.

La guerrilla señaló en el documento que el proceso ha tomado muchos años de esfuerzos políticos y diplomáticos para seguir el "anhelo de paz y cambios que tiene el pueblo de colombia", razón por la que **estos avances alcanzados durante el gobierno anterior "para el ELN mantienen vigencia independientemente que la contraparte los niegue"**.

Asímismo, el ELN "reitera el compromiso de llegar a Acuerdos Humanitarios para regular la confrontación armada así como para rebajar la intensidad del conflicto y pactar un nuevo Cese al Fuego Bilateral, como el desarrollado entre octubre de 2017 y enero de 2018, que fue verificado por Naciones Unidas y la Iglesia Católica”. (Le puede interesar:["Avances de diálogos de paz deberán ser consignados ante la ONU"](https://archivo.contagioradio.com/avances-de-dialogos-con-el-eln-deberan-ser-consignados-en-la-onu/))

Por su parte, el **senador Iván Cepeda** celebró esta noticia y agregó que depositar estos Acuerdos ante entidades internacionales "es un signo de que es posible reiniciar las conversaciones de paz porque **hay una voluntad de seguir en ese proceso a partir del punto en que fue suspendido".** (Le puede interesar: ["¿Qué implicaciones tiene el cese al fuego unilateral por semana santa decretado por el Ejército de Liberación Nacional?"](https://archivo.contagioradio.com/implicacionesl-cese-al-fuego-unilateral-del-eln/))

En ese sentido, un Tribunal de Cundinamarca había fallado favorablemente el pasado mes de mayo un derecho de petición interpuesto por el **abogado Álvaro Leyva y el senador Cepeda,** en el que le pedían al presidente Duque **proteger la agenda de seis puntos que delimita los temas de discusión con el ELN y los dos pre acuerdos para desescalar el conflicto que fueron pactados en junio de 2017.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
