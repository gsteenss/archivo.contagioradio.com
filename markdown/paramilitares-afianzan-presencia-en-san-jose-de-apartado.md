Title: Paramilitares afianzan presencia en San José de Apartadó
Date: 2017-07-11 12:43
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, San josé de Apartadó
Slug: paramilitares-afianzan-presencia-en-san-jose-de-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/paramilitares-san-jose-de-apartado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de Paz de San José de Apartadó] 

###### [11 Jul 2017] 

Los habitantes de la comunidad de paz de San José de Apartadó denunciaron que sus viviendas y algunos locales comerciales aparecieron con grafittis alusivos a las autodenominadas Autodefensas Gaitanistas de Colombia**, pese a que la estación de Policía se encuentra a 100 metros y hay una base Militar a 300 metros.**

Los diferentes grafittis que aparecieron tienen mensajes que dicen “AGC llegamos para quedarnos”, de acuerdo con Esneda López, representante de la Unión Patriótica, regional Urabá, la respuesta de las autoridades ha sido expresar que “no han podido verificar la información", **aunque los campesinos hayan llevado pruebas de la presencia de estos grupos en sus territorios**. (Le puede interesar: ["Así es el control paramilitar en San José de Apartadó"](https://archivo.contagioradio.com/paramilitares-controlan-veredas-de-san-jose-de-apartado/))

Además, López señaló que esta no es la primera vez que aparecen los grafittis “**el año pasado aparecieron dos viviendas con estas pintas, la semana pasada la vereda La Cristalina de San José de Apartadó apareció con esas pintas, hoy amanecieron todas las casas del casco urbanos con las mismas pintas**”. Actos que se suman a la aparición de grupos que se autonominan como Autodefensas Gaitanistas de Colombia y a amenazas a líderes campesinos y comunitarios firmados por el mismo grupo

De igual forma agregó que en cercanías a la comunidad hay una estación de policía a **menos de 100 metros y una base militar a 300 metros**, razón por la cual no se explica cómo suceden estos actos sin que la Fuerza Pública se entere de ello.

“Están es esperando a que se vuelvan y se comiencen a hacer asesinatos, masacres, desplazamientos, los despojos de tierra con los campesinos de San José de Apartadó y eso si nos preocupa”, afirmó Esneda López. (Le puede interesar: ["Sigue asedio de paramilitares contra comunidad de Paz de San José de Apartadó"](https://archivo.contagioradio.com/paramilitares-atacan-a-la-comunidad-de-paz-de-san-jose-de-apartado/))

El próximo 15 de Julio se llevará a cabo en San José de Apartadó el Encuentro Paz y Reconciliación por un pacto político y social que pretende que organizaciones sociales, campesinas, empresas, **líderes comunitarios y la comunidad en general firmen un pacto por la construcción de paz**, sin embargo, para Esneda que estos graffitis aparezcan a tan pocos días del evento significa un acto de “sabotaje” que no afectará la continuidad del evento.

\

<iframe id="audio_19744362" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19744362_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
