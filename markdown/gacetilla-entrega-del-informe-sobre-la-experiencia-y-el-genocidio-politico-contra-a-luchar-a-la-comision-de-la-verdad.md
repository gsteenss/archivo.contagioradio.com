Title: Entrega del informe sobre la experiencia y el genocidio político contra ¡A Luchar! a la Comisión de la Verdad.
Date: 2020-09-02 10:49
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: a luchar, comision de la verdad colombia, entrega informe
Slug: gacetilla-entrega-del-informe-sobre-la-experiencia-y-el-genocidio-politico-contra-a-luchar-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/lucharrr.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/lucharr.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Colectivo por la recuperación de la  memoria de ¡A Luchar! hará entrega a la Comisión de la Verdad del informe **“Esta generación está en peligro. Experiencia y genocidio político contra ¡A Luchar!”** el **jueves 3 de septiembre, a las 3 de la tarde**, por el canal[de Youtube de la entidad](https://www.youtube.com/channel/UCCYEPSzpYaPqs3ajvn0yL5A). ¡A Luchar! Fue uno de los movimientos políticos alternativos más influyentes de la segunda mitad de los años ochenta e inicios de los noventa, adelantando una agenda política, extrainstitucional y abstencionista, activa entre 1985 y 1993. Las violencias cometidas en su contra fueron contemporáneas a las de otras colectividades como la UP, el Frente Popular o la Alianza Democrática M-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde 2016, exmilitantes e investigadoras/es han trabajando por la preservación y dignificación de la memoria de este movimiento político de oposición, su trayectoria y las personas que le dieron vida. Este informe y la documentación a entregar sintetizan este esfuerzo, esperando que la Comisión de la Verdad promueva y dinamice espacios públicos de dignificación del movimiento, así como de esclarecimiento del genocidio político del que fue objeto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La entrega da cuenta de **723 violaciones a Derechos Humanos** en contra de ¡A Luchar!, cometidos contra **474 hombres y 55 mujeres**, y que recogen entre otras, **260 asesinatos**, **80 desapariciones forzadas**, al menos **12 masacres** y otros múltiples delitos cometidos contra este movimiento político que lideró importantes movilizaciones de carácter regional y nacional como el Paro del Nororiente (1987) y las Marchas de Mayo (1988). Esta campaña sistemática de represión da cuenta de prácticas genocidas de parte de estructuras estatales y paramilitares con la intención de **exterminar** este movimiento, prácticas que coadyuvaron y fueron definitivas en el final abrupto de este proyecto.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89234,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/lucharrr-1024x818.jpg){.wp-image-89234}

</figure>
<!-- /wp:image -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
