Title: En Chocó, indígenas y afrodescendientes marchan contra el paramilitarismo
Date: 2017-05-04 12:47
Category: DDHH, Nacional
Tags: afrodescendientes, Chocó, indígenas, Minga, Río Sucio
Slug: en-choco-indigenas-y-afrodescendientes-marchan-contra-el-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Choco-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Natalie Mistral] 

###### [04 May. 2017] 

Ante la grave crisis humanitaria que viven las comunidades del municipio de Rio Sucio, en el departamento del Chocó, desde este jueves **3 mil personas entre afrodescendientes e indígenas se han declarado en minga por la defensa del territorio **para solicitar, entre otros temas, sea solucionada la situación de desplazamiento en la que se encuentran por presencia paramilitar y del ELN.

“En nuestro territorio **hay gran inseguridad, el territorio está minado, no podemos realizar las actividades cotidianas,** no tenemos derecho ni a cortar un racimo de plátano, por esa razón la gente se tuvo que salir, pero hay varias personas confinadas” manifestó uno de los líderes de esta minga.

Agrega el poblador que **comunidades como La Nueva, Taparal y Boca de León se encuentran confinadas por la presencia paramilitar** “ellos no pueden salir a realizar sus actividades tradicionales, ni a cazar ni a nada, el problema es complejo”. Le puede interesar: [Confinadas comunidades indígenas y afros en el Litoral de San Juan Chocó](https://archivo.contagioradio.com/san-juan-choco-paramilitares/)

En el pliego de peticiones que han dado a conocer, las comunidades insisten en un acuerdo humanitario ya, para que no sigan sufriendo afectaciones por el conflicto armado. Le puede interesar: [AGC incursionan en territorio colectivo de Jiguamiandó, Chocó](https://archivo.contagioradio.com/agc-jiguamiando-choco/)

**“Es necesario que ya haya un acuerdo humanitario con el ELN y que el Gobierno tome las medidas con los paramilitares.** Ya hemos dicho que necesitamos el territorio libre de desminado y de grupos al margen de la ley” añade el líder.

De allí, se desprenden otras exigencias como **agilizar la reparación a las víctimas del conflicto, un retorno digno con garantías de no repetición** y con apoyo psicosocial a sus tierras, proyectos productivos, servicios básicos como agua y luz, entre otras. Le puede interesar: [600 personas del Río Truandó en Chocó fueron desplazadas por presencia paramilitar](https://archivo.contagioradio.com/38661/)

Ante estas situaciones que se presentan, la minga también ha denunciado que **no ha habido ningún tipo de actuación por parte de los militares** para garantizar la permanencia en el territorio y el derecho a la vida, ni del Gobierno Nacional ante el pliego de peticiones.

“Hemos decidido hacer esta movilización para que el gobierno nacional y departamental se siente con nosotros y negociemos las exigencias que estamos haciendo” relató el líder.

La minga espera que el presidente Juan Manuel Santos, el Ministro de Defensa, de Ambiente, de Justicia y todas las entidades competentes ante quienes han denunciado la situación, puedan hacerse presentes en el territorio para dar una solución.

**“Queremos que venga gente con poder de decisión y se sienten con nosotros a mirar lo que estamos exigiendo** porque estamos dentro de la normatividad” concluye el líder. Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

###### [Pliego de Peticiones de la Minga](https://www.scribd.com/document/347326149/Pliego-de-Peticiones-de-la-Minga#from_embed "View Pliego de Peticiones de la Minga on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_27783" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/347326149/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ols9MbW6r2ZaEKBtjDxz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379">  
<iframe id="audio_18505450" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no" height="200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18505450_4_1.html?c1=ff6600"></iframe></iframe><iframe id="audio_18505450" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18505450_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
