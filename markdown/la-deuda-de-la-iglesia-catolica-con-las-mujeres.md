Title: La deuda de la iglesia católica con las mujeres
Date: 2017-09-06 09:00
Category: Libertades Sonoras, Mujer
Tags: aborto, catolicas por el derecho a decidir, feminismo, iglesia católica, mujer, mujeres
Slug: la-deuda-de-la-iglesia-catolica-con-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Mujeres-e-Iglesia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: argentina.indymedia.org] 

###### [04 Sept. 2017]

La historia de violencia y discriminación que han vivido las mujeres de manera histórica ha sido replicada por instituciones como la iglesia católica, por tal motivo **mujeres creyentes y feministas han solicitado al Papa Francisco,** en el marco de su visita a Colombia, reconocer que la mujeres tienen los mismos derechos y rechazar los feminicidios cometidos desde tiempos históricos.

Por su parte, otro grupo de mujeres y hombres **ateos ven como impertinente la visita del Papa Francisco**, puesto que Colombia se enuncia como un país laico, es decir que debe ser neutral a todas las confesiones religiosas y no debería privilegiar a ninguna. Le puede interesar: [Católicos piden perdón por implicación de la iglesia en conflicto armado colombiano](https://archivo.contagioradio.com/35728/)

Para abordar estas dos posturas **tuvimos en \#LibertadesSonoras a Laura Torres, Comunicadora Social,** i[ntegrante de la organización Católicas por el Derecho a Decidir Colombia y de la Red Latinoamericana y del Caribe de Católicas por el Derecho a Decidir. Le puede interesar: [Especial: ¿Es la Biblia un libro machista?](https://archivo.contagioradio.com/especial-es-la-biblia-un-libro-machista/)]

Así como a **Paola Nieto, l**[**icenciada en idiomas, traductora en formación e integrante de la Asociación de Ateos de Bogotá**. Le puede interesar: Especial: [Siete pecados de la iglesia católica en el conflicto socio político](https://archivo.contagioradio.com/especial-siete-pecados-de-la-iglesia-catolica-en-el-conflicto-sociopolitico/)]

Además salimos a las calles para preguntar a las personas si creían que la visita del sumo pontífice era importante o no y por qué lo consideraban. Le puede interesar: [Francisco pidió perdón por escándalos de la iglesia](https://archivo.contagioradio.com/papa-francisco-perdon/)

<iframe id="audio_20699544" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20699544_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="osd-sms-wrapper">

</div>
