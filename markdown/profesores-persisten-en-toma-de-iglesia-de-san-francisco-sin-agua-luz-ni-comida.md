Title: Profesores persisten en vigilia de Iglesia de San Francisco sin agua, luz ni comida
Date: 2017-06-07 11:06
Category: Educación, Nacional
Tags: educacion, fecode, Movilización, Paro de maestros
Slug: profesores-persisten-en-toma-de-iglesia-de-san-francisco-sin-agua-luz-ni-comida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-2-800x5501.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [07 jun 2017] 

Al menos **50 maestros y maestras amanecen en vigilia por la educación pública** en la Iglesia de San Francisco en el centro de Bogotá. Han manifestado que quieren contar con el apoyo de la Iglesia Católica y esperan que senadores lleguen hasta allí para proponer compromiso del Congreso sobre la situación de la educación del país.

### **Profesores no tienen agua, luz o comida en vigilia de San Francisco** 

Para David Cerero, miembro de la Asociación de Educadores, “la vigilia por la educación es un acto de recogimiento espiritual en el cual los curas franciscanos han impuesto sus reglas”. Sin embrago, Cerero afirmó que los educadores allí presentes **tienen necesidades fisiológicas que no han podido satisfacer** por lo que han pedido que las negociaciones se agilicen para que los maestros puedan terminar la vigilia. Le puede interesar: ["En fotos: "Así fue la movilización en la toma de Bogotá"](https://archivo.contagioradio.com/fotos-toma-de-bogota/)

En un comunicado emitido por el gremio de educadores, ellos y ellas denunciaron que “luego de completar 12 horas de vigilia en acuerdo con sacerdotes y la personería de Bogotá, no se nos ha permitido acceso a baños para realizar necesidades fisiológicas, no nos han **permitido el acceso a fuentes de electricidad para comunicarnos con nuestros familiares** y se nos han impedido el acceso de alimentos y agua”.

Cerero aseguró que la iglesia se mantiene acordonada por miembros de la Policía y que la situación alrededor del lugar es tranquila. Los maestros manifestaron que **“la vigilia tiene como propósito que la iglesia católica** y los parlamentarios promuevan una reforma al Sistema General de Participación, aumentando el presupuesto destinado a la educación”. Le puede interesar: ["Esperamos que "Estado invierta en educación los recursos que necesitan"](https://archivo.contagioradio.com/fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones/)

### **Negociaciones de FECODE y el gobierno se reducen a decir "no hay plata"**

La jornada de negociaciones de ayer entre Fecode y el Gobierno Nacional terminó sin acuerdo. Según un comunicado presentado por la Procuraduría General de la Nación, quien fue designada como negociadora, “una vez presentadas las propuestas por cada una de las partes, **estas no encontraron un punto de acercamiento** que permita llegar a un entendimiento”.

De igual forma, el presidente de Fecode, Carlos Rivas, afirmó que “Las negociaciones con el Ministerio de Educación se han estancado en el punto económico, debido a la actitud del Gobierno que se resume en una frase: **No hay plata”**. Además, afirmaron que han mantenido la vigilia dentro de los acuerdos hechos y le han exigido a la iglesia y a la personería condiciones básicas para poder desarrollar la vigilia.

Fecode aseguró que el paro, que completa 23 días, **no se levantará y negociaciones continuarán con la metodología de trabajo propuesta hasta ahora.** Los educadores esperan que se concreten los diálogos en los temas de Sistema General de Particiones, nivelación salarial, reconocimiento de primas extra legales y reconocimientos de bonificaciones.

<iframe id="audio_19135947" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19135947_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
