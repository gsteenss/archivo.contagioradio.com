Title: Impunidad, movilización social y Buenventura en "Cien Días" del CINEP
Date: 2017-05-31 14:24
Category: DDHH, Nacional
Tags: CINEP, DDHH, impunidad, Revista Cien Días
Slug: buenaventura-el-escenario-ideal-del-posacuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/20170531_123856-e1496258569601.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 may 2017] 

El día de hoy se realizó el **lanzamiento de la Revista "Cien Días vistos por CINEP/PPP"** y su edición "Veinte años de asombrosa impunidad". En un espacio de conversación, se discutieron temas alrededor de la presentación de tres artículos relacionados con la movilización social y la paz, Buenaventura como territorio clave para el posacuerdo y la impunidad como patrón común en asesinatos de líderes y lideresas, campesinos, defensores de Derechos Humanos.

<iframe src="http://co.ivoox.com/es/player_ek_19008365_2_1.html?data=kp6dkp2Xepahhpywj5WVaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncabmytDOjbXFttPVxdSYxsrQb6S9r6q9jdjTptPZjMrZjdHFstvVzs7S0NnTb8XZjNHOjdfJusqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Erika Parrado, miembro del Equipo de iniciativas de paz del CINEP, la presentación de los artículos **"busca incidir en términos políticos y académicos sobre la situación de asesinatos de líderes y lideresas y defensores de Derechos Humanos".** Así mismo, Parrado manifestó que "la iniciativa de la Revista Cien Días es involucrarse en las coyunturas críticas que se están presentando en el país". Le puede interesar: ["Cinep exige al Gobierno garantizar el derecho a la vida de los colombianos"](https://archivo.contagioradio.com/cinep-exige-a-gobierno-garantizar-la-vida-33167/)

**"Tres variaciones sobre la movilización social y la paz"**

Teniendo en cuenta las movilizaciones sociales de trabajadores públicos, maestros, los paros de Buenaventura y Chocó, el CINEP analizó las condiciones sobre la legitimidad y la criminalización de la protesta social y su relación con los acuerdos de paz entre el gobierno y las FARC.

<iframe src="http://co.ivoox.com/es/player_ek_19008818_2_1.html?data=kp6dkp2cdZmhhpywj5WXaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaLiwpC6w9eJh5SZoqnOjbfJt9XmxtXch5enb9Tjw9fSjcrQb83Vz9_Oz87JstXjjMnSjdHFb7PZ187g1saRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Ana María Restrepo, integrante del grupo de movimientos sociales del CINEP, "la historia de la movilización como forma de participación, **nos permite entender que hay conflictividades** que no han pasado en el conflicto, que se han articulado en el y que seguirán pasando". Esto quedó planteado y establecido en el punto 2.2 del Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera.

**"Buenaventura como escenario para el posacuerdo"**

Erika Parrado describió al municipio como "**una pieza clave para la economía nacional con expresiones de desigualdad, violencia, despojo y fracturas en los procesos comunitarios".** Según Parrado, Buenaventura "se ha pensado como uno de los puertos marítimos más importantes de Colombia y no se ha pensado en la importancia de las comunidades en términos de construcción de paz". Le puede interesar: ["Tras firma del Acuerdo se han agudizado actos violentos contra líderes sociales"](https://archivo.contagioradio.com/cinep-exige-a-gobierno-garantizar-la-vida-33167/)

Buenaventura se ha caracterizado por ser un espacio donde las violencias han convivido como parte de un conflicto armado con otros tipos de violencia que el CINEP denomina "silenciosas o ruidosas en nombre del desarrollo". Esto se complementa con lo que Parrado y su equipo de iniciativas de paz analizaron como el **"desconocimiento de las institucionalidades comunitarias que buscan crear iniciativas y plataformas sociales de paz".**

Así, los intereses económicos, las formas de violencia y también las construcciones sociales propias de los y las habitantes del puerto, hacen de esa región un espacio para la investigación y la definición de líneas de acción a nivel urbano y rural que permitan la implementación de lo que llaman la **"Paz territorial" en la que debe confluir las garantías de derechos y las posibilidades de inversión coherente con esas necesidades.**

**"Impunidad, ¿hasta cuándo y hasta dónde?"**

En el artículo sobre impunidad, el CINEP, hace énfasis en los millares de asesinatos de campesinos, miembros de comunidades negras, LGBTI, defensores de Derechos Humanos y líderes y lideresas. Luis Guerrero, director general del CINEP, establece en su trabajo que **"Bajo la inoperancia de la justicia, la presión de quienes la manipulan y la omisión de los indiferentes, se mantienen en la impunidad los asesinatos."**

De igual forma, establece que "en la medida en que se logre confrontar y contener los factores de violencia que hoy gravitan sobre la labor de los líderes y sus organizaciones, será posible consolidar el anhelo de los colombianos de alcanzar una paz estable y duradera."

### **En memoria de Mario Calderón y Elsa Alvarado** 

El lanzamiento de esta edición de la Revista "Cien Días vistos por CINEP" estuvo dedicada a la memoria de Mario Calderón, Elsa Alvarado y Carlos Alvarado, defensores de Derechos Humanos asesinados el 19 de mayo de 1997.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
