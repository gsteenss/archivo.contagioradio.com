Title: 15-M en España sigue fortaleciéndose en su 4to año de indignación
Date: 2015-05-15 17:47
Author: CtgAdm
Category: El mundo, Movilización
Tags: 15-M España, 15-M Madrid, 4 años del 15-M en España, Aniversario 15-M España, Candidaturas ciudadanas provienen del 15-M España
Slug: 15-m-en-espana-sigue-fortaleciendose-en-su-4to-ano-de-indignacion
Status: published

###### Foto:Munduberriak.org 

###### **Entrevista con [José Iwuit], miembro del 15-M Madrid:**  
<iframe src="http://www.ivoox.com/player_ek_4501048_2_1.html?data=lZqdk5WYfI6ZmKiak5aJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2Fd18rf1cbWrdCfkpqar5Cpt9HVhqigh6eVpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

El **15 de Mayo de 2011** una manifestación convocada por distintos colectivos de base protestó **contra el sistema electoral Español**, por repartir injustamente las votaciones ente los dos grandes partidos y dar pie al bipartidismo.

Esa misma noche **cientos de personas decidieron espontáneamente acampar en las plazas de los ayuntamientos** de las principales capitales, conformando el movimiento conocido como **"Indignados".**

Al calor de la crisis económica y de los **recortes sociales impuestos por la Troika, el FMI y el BM**, así como las altas tasas de paro y la represión contra la protesta social comenzó a organizarse un movimiento **asambleario, horizontal autogestionado y extraparlamentario.**

Poco a poco fueron conformándose **comisiones por barrios y pueblos así como grupos de trabajo** que más tarde derivarían en colectivos sociales de lucha y reivindicación e la **justicia social.**

De los colectivos que surgieron, el más importante ha sido la **PAH, Plataforma antidesahucios,** que ha conseguido parar gran parte de los desahucios de familias que quedaron sin empleo y no pudieron continuar sus hipotecas, perdiendo así su vivienda.

El movimiento a apoyado todas las **huelgas convocadas dándole un carácter social,** también ha defendido los **derechos de los inmigrantes, pensionados o afectados por los recortes en la seguridad social.** Analistas afirman que en el seno del movimiento hay **dos grandes corrientes** una la **extraparlamentaria y otra la parlamentaria.**

Según **José Iwuit, miembro de la Asamblea del 15-M Madrid, e**n las próximas elecciones se van a presentar **cientos de candidaturas ciudadanas pertenecientes a miembros del movimiento** o de sus distintos colectivos como la PAH. Por otro lado también en 2014 se conforma el partido Podemos, conformado por las bases del 15-M.

**Podemos actualmente es la tercera fuerza en intención de voto**, rompiendo así el tradicional bipartidismo Español.

Para el activista el mayor logro de el **sector extraparlamentario ha sido la conformación de Cooperativas Integrales** en distintas localidades del estado con el objetivo de **autogestionar sectores de la economía y lo social.**

El movimiento ha tenido un alcance internacional conformándose el **15-M como día internacional de los indignados.**
