Title: Los descerebrados
Date: 2016-03-24 13:51
Category: Javier Ruiz, Opinion
Tags: uribismo
Slug: los-descerebrados-del-uribismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/descerebrados-uribistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Google 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 24 Mar 2016 

[Como ya se sabe el peor enemigo del uribismo no son las FARC o el presidente Santos sino la paz. El uribismo actúa de manera sucia y tramposa para poner trabas a la paz. Además, el uribismo no la está pasando bien porque cada día que avanza se confirma que sus líderes son personas que han tenido actuaciones indebidas como, por ejemplo, su apoyo explícito a grupos paramilitares como es el caso del “apóstol” Santiago.]

Debido a esto, los uribistas han incrementado sus ataques a quienes cuestionen sus posturas guerreristas, a quienes cuestionen sus apoyos explícitos a grupos paramilitares siendo violentos y sobre todo con un lenguaje grotesco con el que tratan de amedrentar a su contendor con tal de meter miedo porque los argumentos nunca los han tenido.

No se me ocurre llamarlos de otra manera pero son unos “descerebrados” que a través de las redes sociales (Twitter sobre todo) no dudan en amenazar y en utilizar palabras que suelen usar los paramilitares en sus panfletos amenazantes o las que suele usar su “querido líder” cuando es cuestionado. Es común verlos decir o escribir “le voy a dar piso”, “lo voy a quebrar” o “si lo veo le doy en la jeta, marica”. Estos descerebrados creen que con tener un lenguaje de paramilitar van a callar a quienes con argumentos sólidos buscan un país diferente donde el conflicto, la sangre o la muerte sean superados para lograr un país mejor pero ellos creen que la guerra es la vía.

A la vez, uno se pregunta ¿quienes son estos descerebrados? Pues la mayoría de  estos descerebrados son gente común y corriente. Gente que se gana un salario mínimo, gente que no ha encontrado un trabajo estable o en su defecto tiene un trabajo precario, estudiantes universitarios, posiblemente su vecino, un familiar o un amigo, personas como usted o como yo. Puede ser ofensivo lo de “descerebrados” pero por desgracia son manipulados y no tienen autonomía porque creen en absurdas teorías conspirativas y en inventos absurdos de sus líderes como el “Castro-Chavismo”, o que todo es culpa del “comunismo internacional”, del “marxismo” o de “infernales ateos”. Vuelvo y repito, a la falta de argumentos solo queda meter miedo con insultos, teorías conspirativas de la Guerra Fría o amenazas de por medio.

Como dije antes, los “descerebrados” son gente normal manipulada o utilizada por los líderes del uribismo. Pero hay unos que se quieren hacer notar y “tomar liderazgo”. Con su irracionalidad y extremismo los han llevado a cometer idioteces de gran tamaño. Tenemos, como ejemplo, a un abogado que se exhibe con supuestas armas de fuego y que suele amenazar de muerte vía twitter pero a la hora de la verdad es un personaje que le huye al debate y pide escolta policial para su protección personal por temor a ser atacado.

El personaje anterior puede ser el “paradigma” de estos “descerebrados” que creen que con sus palabras matan gente como años atrás un estudiante universitario amenazó y pidió la muerte de campesinos y de los estudiantes de universidades públicas en las redes sociales sin medir las consecuencias de esto. Estos descerebrados nunca cogieron un arma o nunca supieron estar en un combate. Ellos no saben del conflicto interno, nunca fueron parte de las fuerzas militares, no han cogido un arma en su vida y así pretenden dar lecciones. Ellos solo saben repetir de memoria el relato uribista de la guerra.

[Este es un tema amplio que se seguirá profundizando por los debates que posiblemente surjan. A ellos toca pedirles que se “desarmen” y que la paz es el camino.]
