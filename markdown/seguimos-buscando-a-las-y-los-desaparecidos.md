Title: Seguimos buscando a las y los desaparecidos
Date: 2020-05-07 15:00
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: acuerdo cese al fuego farc y gobierno, cese al fuego unilateral FARC-EP, desaparecidos, desaparición, Desaparición forzada, FARC, FARC Diálogos de paz, FARC-EP, UBPD, VICTIMAS
Slug: seguimos-buscando-a-las-y-los-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/image-content-34075048-20190820222841_3440288_20190821134035.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### Por: John León UBPD - Componente FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Acuerdo de Paz, celebrado entre el Gobierno Nacional y la guerrilla de las **FARC-EP**, tiene como eje central el reconocimiento de las víctimas y se centra en la construcción colectiva de la Paz como principio fundamental como valor político supremo de la humanidad. En ese sentido y consecuentes los profundos retos humanitarios, consideramos de primer orden continuar con las labores que permitan dar respuestas en torno a las personas dadas por desaparecidas en el contexto y en razón del conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para adelantar dichos esfuerzos, el Gobierno Nacional y las **FARC-EP**, se comprometieron a proveer a la Unidad de Búsqueda de Personas Dadas por  
Desaparecidas[**UBPD**](https://www.ubpdbusquedadesaparecidos.co/), la información de la que dispongan para establecer lo  
acaecido con aquellas personas dadas por desaparecidas en el contexto y la  
razón del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De conformidad al comunicado conjunto numero 62 del 17 de octubre de 2015, suscrito por las partes en La Habana, así como lo contemplado en el Acuerdo Final, establecimos un equipo nacional compuesto por 82 hombres y mujeres en los diferentes territorios, que han articulado su proceso de reincorporación a las tareas humanitarias de búsqueda de personas dadas por desaparecidas, con el fin de continuar de manera más ágil la recopilación de información.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como firmantes del acuerdo hemos logrado la documentación de 346 casos  
con información relevante para la búsqueda humanitaria de personas dadas por desaparecidas, en las distintas regiones del país, que están ya en proceso de digitalización y sistematización de la **UBDP**. Así como también en el fortalecimiento de nuestro equipo en labores de documentación y  
recopilación de información que nos permitan aportar un informe necesario a las labores de búsqueda, teniendo en cuenta además el papel de  
las organizaciones y familiares para el cumplimiento efectivo del mandato de la **UBDP**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez venimos adelantando una serie de acuerdos y entendimientos que permita iniciar un proceso de diferenciación y construcción de mapas con sitios de interés en la búsqueda de personas desaparecidas que permita ir conociendo y construyendo el universo de personas desaparecidas y los posibles lugares para su búsqueda.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### [Le puede interesar: Comprender fenómeno de los desaparecidos en Colombia aporta a la memoria histórica del país](https://archivo.contagioradio.com/comprender-desaparecidos-capitulo-historia-colombia/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Consideramos importante que el Estado colombiano en cabeza del gobierno  
nacional fortalezca el trabajo y contribuya en agilizar el proceso de recolección de información para la búsqueda como expresión de un compromiso con el país, garantizando la presencia y funcionamiento real de la **UBPD** en todo el territorio nacional, asegurando que la sociedad en su conjunto comprenda su carácter humanitario y fortalecer este proceso, pues estamos convencidos que las bases de la paz se fortalecen con las acciones reparadoras y transformadoras, la verdad se erige como garantía de NO repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Ver mas: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
