Title: 403 mujeres han sido asesinadas por asumir roles políticos en Colombia
Date: 2018-03-08 20:00
Category: Mujer, Nacional
Tags: Asesinato de Mujeres, Día de la mujer, MOVICE, víctimas
Slug: 403-mujeres-asesinadas-asumir-roles-politicos-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/foto-mujeres-victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Mar 2018] 

En el día internacional de la conmemoración de la lucha y defensa por los derechos humanos de las mujeres, el Movimiento de Víctimas Crímenes de Estado (MOVICE), reveló que en Colombia han sido  asesinadas **403 mujeres que asumieron roles políticos, mientras que 14.955 mujeres sufrieron allanamientos por parte de la Fuerza Pública debido a su militancia.**

En el documento del MOVICE,  se evidencia que en lo corrido solamente del año 2017 más de 145 defensoras de derechos humanos fueron agredidas, 16 fueron asesinadas y  que “si bien un porcentaje de las víctimas directas son hombres, han sido muchas las mujeres” que han vivido el dolor de la guerra, como quedó registrado en la base de datos de Colombia Nunca Más, en donde en 30 años, se logró establecer que por lo menos **9.775 mujeres fueron víctimas de todo tipo de violencias**.

De esas acciones contra las mujeres, **5993 de denunciaron ser víctimas de violencia sexual perpetradas por estructuras paramilitares en convivencia con la Fuerza Pública,** de los cuales 206 casos han sido cometidos de forma directa por agentes del Estado. (Le puede interesar:[ "Las colombianas también para el 8M"](https://archivo.contagioradio.com/las-colombianas-tambien-paran-este-8m/))

### **Las desapariciones de las mujeres: una violencia silenciosa** 

La desaparición forzada también ha sido usada como estrategia violenta contra las mujeres. De acuerdo con el MOVICE en Colombia se encuentran desaparecidas 1079 mujeres, de las cuales, la mitad eran reportadas como no identificadas; **16 fueron ejecutadas extrajudicialmente y 1574 fueron torturadas**.

El primer caso de desaparición se presentó el 9 de septiembre de 1977 cuando miembros del Servicio de Inteligencia (SISPEC), conocidos como F2 detuvieron a Omaira Amaya, bacterióloga y militante del ELN. (Le puede interesar: "

### **Los dolores de la guerra** 

De igual forma, a este tipo de violencias se suman los desplazamientos forzados, los mecanismos de impunidad y revictimización como **la negligencia de los funcionarios públicos y de la Policía al no recibir las denuncias. También cabe resaltar que las mujeres muchas veces no cuentan con g**arantías para demostrar que las tierras de las que fueron sacadas ilegalmente, les pertenecen.

Frente a este panorama, el MOVICE, concluye, “la verdad integral y la satisfacción de los derechos de las víctimas de la violencia estatal son el único camino para la reconciliación y las garantías para la no repetición”.

###### Reciba toda la información de Contagio Radio en [[su correo]
