Title: Javier Córdoba Chaguendo, periodista de radio comunitaria fue asesinado en Tumaco
Date: 2019-10-20 12:52
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Periodistas, Tumaco
Slug: javier-cordoba-chaguendo-periodista-de-radio-comunitaria-fue-asesinado-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:FLIP 

La tarde del sábado 19 de Octubre se conoció del asesinato del periodista **Javier Córdoba Chaguedo**, quien se encontraba en la cabina de la emisora comunitaria Planeta Stereo, ubicada en el corregimiento de Llorente, del municipio de, Tumaco en Nariño.

Según las primeras versiones el periodista estaba en desarrollo de sus labores en la emisora, el viernes 18 de Octubre hacia las 9 p.m., cuando varios sujetos armados ingresaron al lugar y le dispararon en repetidas ocasiones hasta causarle la muerte.

La **Fundación para la Libertad de Prensa** aseguró a través de su cuenta de Twitter: "[La FLIP alerta sobre la total ausencia de Estado en esta población y el creciente aumento de amenazas contra periodistas en el departamento de Nariño. Exigimos acciones inmediatas de Fiscalía]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}y de Policía Nacional[ aclarar las circunstancias de esta asesinato".]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Del mismo modo se manifestó la delegación de la Unión Europea en Colombia "Lamentamos profundamente el asesinato de Javier Córdoba Chaguendo de Planeta Stereo y las amenazas a los periodistas de \#Nariño. El desarrollo de un país también depende de su derecho a estar informados. Por una prensa libre de amenazas \#DefendamosLaVida"

> La FLIP lamenta y rechaza el asesinato de Javier Córdoba Chaguendo, periodista que trabajaba en Llorente, Nariño. Fue asesinado el viernes 18 de octubre a las 9 pm. mientras hacía su programa musical en la emisora Planeta Stereo.
>
> — FLIP (@FLIP\_org) [October 19, 2019](https://twitter.com/FLIP_org/status/1185630467732705281?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Ya en Septiembre de 2019 se conoció un panfleto que se atribuyó a las Águilas Negras en el que se amenazaba a varios periodistas de la región, en la que también hacen presencia grupos del narcotráfico, paramilitares y disidencias de las FARC.

Noticia en desarrollo
