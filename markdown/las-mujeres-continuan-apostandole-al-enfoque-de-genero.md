Title: Un año retador para el enfoque de género en el acuerdo de paz
Date: 2017-11-24 15:13
Category: Mujer, Nacional
Tags: acuerdos de paz, enfoque de género, Implementación, mujeres
Slug: las-mujeres-continuan-apostandole-al-enfoque-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Victoria.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2017] 

Desde los diálogos de paz de La Habana, **las mujeres se ganaron un espacio fundamental en el que lograron exponer la necesidad de generar políticas públicas incluyentes y con un enfoque de género.** A un año de la implementación, Victoria Sandino integrante de la comisión de género afirmó que aún quedan muchos retos que afrontar para que el Acuerdo de paz, se implemente con la visión de género.

Para Sandino el balance de este primer año es positivo en términos de que se ha podido estrechar relaciones con diferentes organizaciones en defensa de los derechos de las mujeres y colectivos de mujeres, en todo el país, **que han permitido tener un mayor impacto a la hora de poner en marcha el enfoque de género** en la implementación de los Acuerdos de Paz.

“Seguimos en la interlocución para establecer las agendas comunes de las mujeres, desde la lucha por incluir y materializar el enfoque de género en la implementación” afirmó Sandino y expresó que este lastimosamente ha sido uno de los componentes que se ha querido sacrificar dentro de la puesta en marcha de los Acuerdos. **Sin embargo, afirmó que ha sido el apoyo de las mujeres el que ha impedido que esto suceda**.

“Hemos tejido una compañía de espiritualidad, de solidaridad, fortaleza y protección de las mujeres colombianas que en distintos momentos han padecido o han sido víctimas de la violencia en el conflicto armado” manifestó. (Le puede interesar: ["Aprendizajes y retos para la FARC tras un año del Acuerdo de Paz"](https://archivo.contagioradio.com/el-aprendizaje-y-los-restos-de-la-farc-tras-un-ano-de-la-implementacion/))

### **Los retos de las mujeres y la paz** 

Para el 2018, Sandino aseveró que el reto más importante será construcción de una agenda programática que incluya las necesidades de todas las mujeres del país, desde la ruralidad, la ciudad y las diferentes culturas. Así mismo señaló que hay un interés por apoyar a las mujeres que **hacen parte del Ejército de Liberación Nacional en el proceso de paz que adelantan en Quito**.

“Estamos comprometidas en lograr incidir y acompañar a las mujeres del ELN que están en Quito, para apoyar e intercambiar nuestras experiencias y que ellas puedan tomar lo que consideren para su beneficio” expresó Sandino.

<iframe id="audio_22334541" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22334541_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
