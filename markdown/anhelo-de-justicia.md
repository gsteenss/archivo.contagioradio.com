Title: Anhelo de justicia
Date: 2020-06-29 15:46
Author: AdminContagio
Category: Opinion, Rosa Canela
Tags: Ejercito Nacional, Indigenas embera, justicia
Slug: anhelo-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-29-at-3.37.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Contagio Radio {#foto-por-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: Abogada Rosa Canela

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### **21 de junio de 2020. Resguardo indígena Embera del Corregimiento de Santa Cecilia del municipio de Pueblo Rico.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

***…*** *el domingo por la tarde ella se fue a traer unas guayabas para hacer jugo y ella desapareció como a las cinco de la tarde y comenzamos a buscar a ella y nadie nos dio información de ella y desapareció toda la noche, hasta las diez de la mañana; nosotros fuimos a buscar a Guarato y hasta Aguita y allá tampoco nos daban información de nada. Después subimos a buscar donde los soldados y también nos dijeron que no habían visto a ella; después ya venimos para la casa, ya por la mañana, como a las diez de la mañana, fuimos a buscar otra vez y la encontramos ahí en el potrero llorando; allá juntico a la quebrada, la encontramos llorando ahí; llegamos y de ahí salimos pa´la casa y como llegó a la casa, entonces se puso a llorar y dijo que iba a decir la verdad y ella dijo que a ella la habían violado varias personas, los soldados; entonces nosotros llegamos ahí, allá donde los soldados pa´que le dijeran la verdad y si podía señalar, si podía reconocer y ella dijo que si; y como llegamos allá y ella reconoció, ella los señaló, de los nueve personas solamente pudo reconocer solamente tres personas. Ella tiene 11 años. Ella le contó a nosotros que unos soldados la violaron a ella y la cogieron a la fuerza y le taparon la boca y no le dejaron gritar. Entonces ella bregaba de venir de allá y después la cogieron otra vez y otros soldados la cogieron a la fuerza y la violaron allá y de ahí no la dejaron venir, ella se intentaba volar y no la dejaron venir y los soldados le dijeron a ella, si contaba esas cosas que los iban a echar del trabajo que no dijera nada, que no demandara, entonces ella dijo que no iba a decir nada. Eso nos dijo ella a nosotros. Queremos que paguen lo que le hicieron a mi hermana.  *

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Fueron 15 horas en las que siete soldados regulares del Pelotón Buitre II de la Octava Brigada del Ejército Nacional, sometieron a una niña de la comunidad Embera Chami, a la noche más oscura de su alma.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Descubiertos no por ninguna rigurosa investigación, ni menos aún por la destreza o talento de algún investigador de aquella ruralidad; definitivamente no, si este caso, no quedó como muchos otros en la impunidad, lo fue por la determinación del resguardo indígena Dokabu de Pueblo Rico, Risaralda, quienes una vez encontraron a la niña llorando, con su ropa desgarrada, cerca de una quebrada; llevarla a su casa y escuchar  su relato, no dudaron en aprovisionarse de su bastón de mando y organizados con la guardia indígena, desafiar la prepotente maquinaria militar para sacar de su guarnición a los responsables, quienes esa misma mañana, en medio de las tareas de búsqueda de la niña, les habían negado saber de su paradero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así fue, como ante la irrefutable evidencia, los nombres de los soldados Luis Fernando Mangareth Hernández, Deyson Andrés Zapata Isaza Zapata, Oscar Eduardo Gil Alzate, Juan David Guadi Ruiz, José Luis Holguín Pérez, Yair Steven González y Juan Camilo Morales Poveda, empezaron a hacer parte del histórico criminal de agentes del Estado.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego, quienes por el prurito de figurar, quisieron atribuir el esclarecimiento de lo ocurrido a una exhaustiva investigación penal, no pueden menos que ser desmentidos y objetados; no solo, por la vergonzosa perorata triunfalista, sino por el uso de un proscrito discurso patriarcal, por el que ni siquiera la formulación de imputación de cargos, como único acto que debía ser asumido por la Fiscalía, pues el caso les llegó con indiciados confesos, logró cumplir con el rigor penal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Patriarcal**, por hacer creer, que ahora resulta que es lo mismo un acceso carnal violento que uno abusivo y que de aquella trágica escena, el consentimiento aparece como una cuestión puramente *femenina*, en el que la niña, en lugar de haber quedado reducida por la fuerza, se decidió por no resistir. En otras palabras, la imputación penal a cargo de la Fiscalía, más o menos quiso decir que la niña de 11 años del resguardo indígena Dokabu, no solo tenía la responsabilidad de establecer límites a los actos de los soldados; sino que además su condición de niña indígena, no daba para considerarla víctima de una violación, sino más bien de un abuso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La prevaricadora formulación de imputación que la Fiscalía activó contra los soldados violadores, no es otra cosa que un preacuerdo camuflado, prohibido desde el artículo 199 del **Código de la Infancia y la Adolescencia**, no solo por haber desconocido el contexto de opresión y violencia al que fue sometida la fragilidad de la niña, en donde siete hombres armados y uniformados, la expusieron por más de 15 horas, sino porque nunca será lo mismo una condena por acceso carnal violento que una condena por acceso carnal abusivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Al final, la mal intencionada actuación de la Fiscalía en este caso, por el que su representante, ante tamaño yerro, ladinamente respondió malo por bogar; desoyó los gritos rotos y truncó la esperanza de quienes anhelamos una justicia afirmativa y reivindicadora; una justicia en el más estricto sentido de lo ético.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea mas de Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
