Title: Detienen periodista en valla de frontera entre Marruecos y España
Date: 2015-03-11 20:45
Author: CtgAdm
Category: El mundo, Movilización
Tags: 50 inmigrantes asaltan la valla de Melilla, Frontera entre Marruecos yMelilla, Masacre del Tarajal, Valla de la verguenza Melilla
Slug: 12-inmigrantes-siguen-aislados-en-la-valla-de-frontera-entre-marruecos-y-espana
Status: published

###### Foto:ABC.es 

Más de **50 inmigrantes intenta asaltar la valla** que separa Marruecos de España en la ciudad de Melilla, quedando **12 de ellos atrapados en medio de la frontera**. Solo 5 consiguieron acceder a España.

Aprovechando la niebla y la escasa visibilidad un grupo de inmigrantes de origen subsahariano ha asaltado la valla cerca del  Centro de Estancia Temporal de Inmigrantes (CETI), donde muchos de los residentes esperaban a sus compañeros.

Los **12 inmigrantes** que siguen suspendidos en la valla de paso de Farhana **se niegan a bajar** ante el fuerte **despliegue de fuerzas de la Guardia Civil española**, y solicitan la asistencia de organizaciones de derechos humanos. También ha sido **detenida una fotoperiodista** cuando transportaba a varios inmigrantes en su propio vehículo.

Todo esto sucede en una jornada en la que están **siendo juzgados miembros de la Guardia Civil** de Melilla y de Ceuta, ciudades coloniales españolas en territorio marroquí, por su **actuación en la masacre del Tarajal**, también en la frontera, donde **murieron 14 inmigrantes tras** disparos de las fuerzas de seguridad españolas cuando llegaban por mar a territorio europeo.

En el proceso de juicio las **declaraciones** los guardias civiles se limitan a afirmar que **dispararon porque vieron a su teniente hacerlo** y por eso pensaron que ellos también tenían que hacerlo.

**Estos hecho contrastan con la decisión de los partidos** de la **oposición** (excepto el conservador PP, actualmente en el gobierno con mayoría absoluta, y el ultraderechista Ciudadanos) han suscrito un **documento comprometiéndose a devolver la sanidad universal**, retornando los **derechos a los inmigrantes irregulares**.

En 2014 el ejecutivo conservador emitió un decreto ley por el que retiraba la universalidad de la salud, afectando principalmente al colectivo de inmigrantes "sin papeles". Con dicho documento se comprometen electoralmente todos los partidos de la oposición excepto el partido Ciudadanos.
