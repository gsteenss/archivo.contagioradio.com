Title: Ley de amnistía inicia su debate en el congreso
Date: 2016-12-19 13:01
Category: Nacional, Paz
Slug: ley-amnistia-inicia-debate-congreso
Status: published

###### [Foto: caracol] 

###### [19 Dic 2016] 

A partir de hoy, las comisiones primeras de Senado y Cámara debaten el proyecto de ley de amnistía como un primer elemento en la implementación de los acuerdos de paz con la guerrilla de las FARC EP. Según declaraciones de Alirio Uribe, esta ley podría estar lista a finales de Diciembre cuando se cumpla el trámite de los 4 debates, tanto  comisiones primeras como plenarios. Este proceso contará con el acompañamiento de los integrantes de Voces de Paz.

El proyecto de Amnistía incluye una serie de condiciones como que se debe aplicar de manera individual y gradualmente. Además solamente cobijará a las personas, integrantes de las FARC que hayan incurrido en delitos políticos o menores y no para quienes hay procesos por crímenes de Lesa Humanidad. **Según algunos cálculos el número de personas beneficiadas con esta ley podría ascender a 6000**.

### **Los delitos que pueden ser objeto de amnistía** 

El proyecto de ley, presentado por el gobierno a finales de la semana pasada, define el delito político como aquel en el que el Estado es considerado la víctima, y que haya sido cometido sin un ánimo personal sino en un conjunto claramente establecido y definido.

Los delitos que podrán ser objeto de amnistía son delitos políticos de rebelión, asonada, conspiración y sedición, usurpación, y retención ilegal de mando, junto con los delitos que le son conexos. A este respecto se refirió Iván Márquez y aseguró que esta es una medida que dará **confianza a los integrantes de las FARC que en este momento están comenzando su tránsito hacia las Zonas Veredales**.

Los que no podrán ser objeto de amnistía son genocidio, crímenes de guerra, toma de rehenes u otra privación grave de la libertad, tortura, ejecuciones extrajudiciales, desaparición forzada, acceso carnal violento y otras formas de violencia sexual, sustracción de menores, desplazamiento forzado, además del reclutamiento de menores.

### **Agentes del Estado** 

Respecto de las responsabilidades penales de los agentes del Estado el acuerdo afirma que serán objeto de una aplicación especial, equitativa y diferenciado para agentes del Estado, sin embargo tampoco cobija aquellos **delitos de Lesa Humanidad incluidos en el estatuto de Roma.**

Frente a lo afirmado por el Senador ponente Armando Benedetti, en el sentido de buscar restablecimiento de los derechos políticos para todos, incluso los parapolíticos, defensores de DDHH y juristas afirman que **se trata de incluir asuntos no incluidos en el acuerdo, para salvar responsabilidades que podrían aflorar en la aplicación de la Jurisdicción Especial de Paz**.

###### Reciba toda la información de Contagio Radio en [[su correo]
