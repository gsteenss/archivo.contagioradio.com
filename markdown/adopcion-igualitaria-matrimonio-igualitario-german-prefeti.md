Title: "Adopción y matrimonio igualitario no tienen reversa", Germán Rincón
Date: 2015-02-05 19:48
Author: CtgAdm
Category: LGBTI, Política
Tags: Adopción igualitaria, comunidad gay, LGBTI
Slug: adopcion-igualitaria-matrimonio-igualitario-german-prefeti
Status: published

##### Foto: Contagio Radio 

##### [Germán Rincón] 

La votación del proyecto que propone que las parejas del mismo sexo puedan adoptar, terminó con un empate 4 – 4, razón por la que **se nombró un conjuez que definirá si se da vía libre a la adopción igualitaria.**

El proyecto presentado por el magistrado Jorge Iván Palacio, que avalaba que las familias homoparentales pudieran adoptar, quedará en manos del **conjuez José Roberto Herrera,  de línea conservadora,** lo que genera pocas esperanzas para la comunidad gay de que se dé un fallo a favor de la posibilidad que las parejas del mismo sexo adopten.

Sin embargo, Germán Rincón, abogado y defensor de derechos de la comunidad LGBTI, afirmó que se trata de un “magistrado que a hecho carrera y se espera que siga la línea planteada por la Corte Constitucional”, que concedió a Ana y Verónica, la adopción conjunta de la hija biológica de una de ellas.

Para el abogado Rincón, **"la adopción y el matrimonio igualitario no tienen reversa"**, según él la decisión sobre adopción de parejas del mismo sexo debe tomarse a la luz del derecho y no de los prejuicios morales.
