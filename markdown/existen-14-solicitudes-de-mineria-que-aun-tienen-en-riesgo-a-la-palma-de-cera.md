Title: Existen 14 solicitudes de minería que aún tienen en riesgo a la palma de cera
Date: 2016-05-10 22:31
Category: Ambiente, Nacional
Tags: Mineria, palma de cera, Quindío, Salento
Slug: existen-14-solicitudes-de-mineria-que-aun-tienen-en-riesgo-a-la-palma-de-cera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Valle-de-Cocora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [10 May 2016]

Tras la negativa de los habitantes de Salento, Quindío, y de diversos sectores de la población civil frente a la posibilidad de que se realice actividad minera en el Valle del Cocora, este martes, Silvana Habib, directora de la Agencia Nacional de Minería, aseguró que no se otorgará el título minero para la explotación de oro en esa zona del país. Sin embargo, los habitantes esperan que ese anuncio se materialice con resoluciones y actos administrativos, teniendo en cuenta que aún existen 14 solicitudes de concesiones mineras en trámite.

“Cumpliendo el debido proceso no voy a otorgar el título, no tengo ningún elemento para otorgar el título, para eso es el debido proceso (…) una vez se haga la concertación con el acalde y la autoridad ambiental se le comunica a la empresa que su solicitud no es viable”, aseguró Habib.

La directora de la ANM, afirmó que los argumentos de la comunidad son suficientes para cerrar sobre ese título minero en Salento, pero la personera del municipio, Yessica Herrera, señala que además de ese título minero, existen otros que ponen en riesgo la palma de cera y además solo dos títulos aparecen a la fecha como terminados.

De acuerdo con la personera, la preocupación sigue vigente y así mismo continúa la movilización social y jurídica. Aunque el 98% de Salento tiene alguna declaratoria de protección ambiental, más del 50% del territorio estaría titulado o en trámite para otorgar licencias, por eso la población espera que renuncien todas las empresas, como ya sucedió con AngloGold Ashanti, o que la ANM revoque los títulos y suspenda los trámites.

La palma de cera no solo se encuentra en el Valle del Cocora, sino también en Toche, cercano a Cajamarca y Salento, un lugar que también se encuentra en riesgo por la actividad minera. Es por eso, que los habitantes del municipio continuarán movilizándose el próximo 19 y 20 de mayo cuando se realizará un encuentro para plantear estrategias por la vida y en defensa del territorio.

<iframe src="http://co.ivoox.com/es/player_ej_11490104_2_1.html?data=kpahm5WVdJWhhpywj5WbaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncbrZ1NjWxcaPjMbm08rfw5CRb7HZ09jc0MrWpYzHwtHS0NnTb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
