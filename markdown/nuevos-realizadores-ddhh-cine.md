Title: Nuevos realizadores a relatar los derechos de los migrantes
Date: 2017-07-05 16:10
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine, DDHH, migraciones
Slug: nuevos-realizadores-ddhh-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/nuevos-realizadores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Festival de Cine por los DDHH 

###### 05 Jun 2017 

En el marco  del **4to Festival Internacional de Cine por los Derechos Humanos** de Bogotá, se abre la convocatoria para que los nuevos realizadores, estudiantes, aficionados y productores, participen con producciones audiovisuales en las que aborden la temática central: **Migración y derechos de los migrantes en América Latina**.

Al ser la migración un fenómeno creciente con graves consecuencias y desafíos políticos, sociales y económicos para las sociedades y los gobiernos de América Latina es necesario registrar, a través de la mirada audiovisual, las dinámicas y características de migración de cada país y obtener un panorama regional de este fenómeno.

El ganador obtendrá **un viaje a Costa Rica con todos los gastos pagos**, con acceso exclusivo a los archivos de la Corte IDH y con la información allí suministrada, realizar un documental sobre esta institución que conmemora 40 años de trabajo. La convocatoria estará abierta hasta el 1 de agosto de 2017. (Le puede interesar: [Cortometrajes hechos por jóvenes se verán en Incinerante](https://archivo.contagioradio.com/cine-colombiano-incinerante/))

¿Cómo participar?

1\. Consultar las bases de la convocatoria y conocer los requisitos.  
2. Hacer un video de entre 1 y 3 minutos sobre la temática exigida.  
3. Subirlo a cualquier plataforma de video (YouTube, Vimeo, etc.).  
4. Inscribirse en este [formulario online](https://docs.google.com/a/contagioradio.com/forms/d/e/1FAIpQLSe6N0jpZqhZaYVsO8oCIKr-ejoRS7EAi28lkg-JyzEkDh04Hg/viewform).

La convocatoria "Nuevos Realizadores" es organizada por el Festival Internacional de Cine por los Derechos Humanos Bogotá con el patrocinio del Programa Estado de Derecho para Latinoamérica de la Fundación Konrad Adenauer y el apoyo de la Corte Interamericana de Derechos Humanos.
