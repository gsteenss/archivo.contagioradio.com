Title: Asesinan a Luz Herminia Olarte, lideresa social de Yarumal
Date: 2017-02-10 14:42
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, COEUROPA, Luz Herminia Olarte, Yarumal Antioquia
Slug: asesinan-a-luz-herminia-olarte-lideresa-social-de-yarumal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Asesinan-a-Luz-Herminia-Olarte-lideresa-social-de-Yarumal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [10 Feb 2017 ] 

El pasado 7 de febrero fue encontrado el cuerpo sin vida de la lideresa comunitaria y defensora de derechos humanos Luz Herminia Olarte, la Coordinación Colombia Europa nodo Antioquía, denunció los hechos a través de un comunicado y señaló que en lo corrido del año ya son 19 los líderes y lideresas asesinadas, de los cuales 5 tenían una ardua labor de defensa de derechos humanos en Antioquia y las instituciones estatales, no dicen nada.

Óscar Olarte integrante de la Coordinación, señaló que la lideresa era presidenta de la Junta de Acción Comunal de Yarumal y tenía un proceso con las comunidades campesinas del Llano de Chalí en Yarumal, relata que “fue vilmente asesinada” y aunque se había alertado a las autoridades de **amenazas previas contra la lideresa, “las autoridades no hicieron nada”,** en la misiva, la Coordinación exigió garantías al Gobierno y celeridad en las investigaciones para todos los asesinatos de líderes.

### Las autoridades sabían de las amenazas 

La organización, reveló que a pesar de recurrentes informes entregados al Sistema de Alertas Tempranas de la Defensoría sobre los asesinatos sistemáticos y el avance de grupos neo-paramilitares, **“no existen medidas efectivas del Estado para repeler, reducir, desarticular o someter a estos grupos”. **([Le puede interesar: 4 líderes afrocolombianos asesinados en menos de un mes](https://archivo.contagioradio.com/4-lideres-afrocolombianos-asesinados-en-menos-de-un-mes/))

Olarte manifestó que existe una preocupación por parte de las comunidades y organizaciones de derechos humanos que hacen presencia en la región, por el fortalecimiento de las estructuras paramilitares, las **“estrategias evidentemente sistemáticas”** de asesinatos, amenazas, hostigamientos, el silencio y la negligencia de las instituciones del Estado.

El defensor advirtió que tanto las comunidades como organizaciones han registrado que estos grupos paramilitares de **las Autodefensas Gaitanistas, se han movilizado por zonas donde hay presencia del Ejército, y “las Fuerzas Militares no han impedido su paso”.**

En 2016 un líder o lideresa fue asesinado cada mes en Antioquia, sin embargo en 2017 “tenemos un aumento de 500% en los asesinatos, van 2 meses y ya son 5 los líderes que han perdido la vida” puntualizó Olarte.

Por último, afirmó que saben del avance de estos grupos hacia el Urabá y “otras zonas que antes eran ocupadas por las FARC”, comentó que la comunidad de encuentra muy preocupada, **temen por la vida de los demás líderes y por la continuidad de los procesos campesinos en la región**, además resaltó que de acuerdo con lo pactado en La Habana, “el Gobierno no puede seguir evadiendo la responsabilidad de desmantelar el paramilitarismo”.

<iframe id="audio_16943813" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16943813_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
