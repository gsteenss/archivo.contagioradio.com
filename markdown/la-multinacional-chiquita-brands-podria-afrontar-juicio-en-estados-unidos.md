Title: La multinacional Chiquita Brands podría afrontar juicio en Estados Unidos
Date: 2016-12-06 16:59
Category: DDHH, Nacional
Tags: Chiquita Brands, grupos paramilitares
Slug: la-multinacional-chiquita-brands-podria-afrontar-juicio-en-estados-unidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/chiquita-brands.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [6 Dic 2016]

Un reciente fallo del Juzgado federal de la Florida en Estados Unidos, acepto que continúe en juicio en este país, una demanda en contra de la Multinacional Chiquita Brands interpuesta por las víctimas de grupos paramilitares por daños causados y pidiendo que sea la justicia estadounidense la que **investigue a los ejecutivos de la compañía  que permitieron que se financiara a estos grupos**.

Esto debido a que la Multinacional tenía intereses en que el proceso continuara en Colombia argumentando  que las víctimas tendrían en este país todas las garantías y que al igual que otros casos podrían presentarse ante la **Ley 975 de Justicia y Paz para recibir una indemnización por parte del Estado**.  Además agrego que en la actualidad **existen 1.700 casos más y esa demanda podría seguir el mismo rumbo.**

No obstante, el Juez no acepto estas afirmaciones y respaldo la Acción de Clase aseverando que “**los Estados Unidos tienen un fuerte interés en la vigilancia y disuasión de la conducta no ética e ilegales de las empresas estadounidenses en el apoyo a los organismos terroristas extranjeros”** y agrego que hasta el momento la multinacional no dio a conocer el estado en el que se encuentran los demás procesos.

Las víctimas llevaban aproximadamente diez años en este proceso expresando que en Colombia no existen las garantías para llevar un proceso de tal envergadura, anteriormente ya habían hecho una petición para que este caso fuese revisado por la Corte Suprema de Estados Unidos, pero esta declino, sin embargo este fallo abre la posibilidad de que el proceso continúe y se aclare por qué la multinacional Chiquita Brands financio grupos paramilitares en Colombia

Chiquita Brands habría cerrado operaciones en Colombia en el año 2004, sin embargo en el año 2007 la multinacional se declaró culpable por financiar grupos paramilitares en la región del Uraba, zona bananera del país, y fue penalizada con una multa por un valor de 25 millones de dólares. Le puede interesar: ["Empresas serían investigadas por financiación de la guerra en Tribunal Especial de Paz"](https://archivo.contagioradio.com/empresas-serian-investigadas-por-financiacion-de-la-guerra-en-el-tribunal-especial-de-paz/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>
