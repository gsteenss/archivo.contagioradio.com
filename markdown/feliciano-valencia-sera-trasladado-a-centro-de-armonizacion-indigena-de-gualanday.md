Title: Feliciano Valencia será trasladado a centro de armonización indígena de Gualanday
Date: 2015-11-05 11:08
Category: Movilización
Tags: Centro de Armonozación, Comunidades Indígenas en Colombia, Feliciano Valencia, Minga de recuperación de la madre tierra, Minga Indígena 2008, ONIC
Slug: feliciano-valencia-sera-trasladado-a-centro-de-armonizacion-indigena-de-gualanday
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/feliciano-valencia-734.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

<iframe src="http://www.ivoox.com/player_ek_9283559_2_1.html?data=mpellZqZfY6ZmKiakpaJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisbgysjWw9PTb7fVzcrbxc7Fb9TZ04qwlYqldYzo08bgzsbIpcXjjMaYxcrSuNPjjMnSjcbWsdDiyt%2Bah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Fernando Arias, ONIC] 

###### [5 Nov 2015] 

El tribunal superior de Popayan resolvió fallar a favor de la apelación presentada por la defensa del líder indígena para que se respetara la jurisdicción de las comunidades de esa región del país y se pueda [cumplir la condena de la jurisdicción ordinaria](https://archivo.contagioradio.com/movimiento-indigena-caucano-rechaza-detencion-de-feliciano-valencia/) en un Centro de Armonización, lagar destinado para que personas pertenecientes a esas comunidades cumplan las condenas.

Según Luis Fernando Arias, la posibilidad de que Valencia sea trasladado al centro de armonización es un punto muy importante en el camino hacia el reconocimiento de la jurisdicción indígena y el respeto por sus decisiones, sin embargo afirma también que seguirán buscando [demostrar la inocencia del líder indígena](https://archivo.contagioradio.com/indigenas-se-movilizan-en-popayan-para-exigir-libertad-inmediata-de-feliciano-valencia/) condenado a 30 años de prisión por secuestro simple.

La guardia indígena ya se está trasladando hacia la penitenciaría de Popayan para hacer efectiva la entrega y el traslado de Valencia que a partir de hoy quedaría bajo el resguardo exclusivo de las comunidades y de sus autoridades.

Por otra parte, hay un grupo, aún no determinado de indígenas recluidos en el mismo centro carcelario que iniciaron una huelga de hambre para exigir el mismo trato de Feliciano Valencia y en su condición de pertenecer a un resguardo pueda ser traslados a un centro de armonización en el que se garanticen sus derechos, muchas veces vulnerados en las cárceles.

La captura del líder social Feliciano Valencia, "**representa para las autoridades indígenas un atropello contra el movimiento social colombiano"** afirmaron varias autoridades en su momento. Valencia fue acusado de secuestro simple y condenado por el Tribunal Superior de Cauca a 16 años de prisión, por el ritual de castigo aplicado al cabo Jairo Danilo Chaparral, quien fue infiltrado en la minga indígena de La María, Cauca, en 2008.
