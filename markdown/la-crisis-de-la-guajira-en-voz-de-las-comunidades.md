Title: La crisis de la Guajira en voz de las comunidades
Date: 2016-04-21 12:46
Category: Hablemos alguito
Tags: Cerrejón, crisis en la Guajira
Slug: la-crisis-de-la-guajira-en-voz-de-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_2023.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Censat 

##### [21 Abr 2016] 

Comunidades negras e indígenas de La Guajira y organizaciones sociales aseguran que el fenómeno de El Niño es una “falacia” que ha servido como cortina de humo para tapar las irregularidades que han existido en torno a los permisos que se han otorgado a la empresa El Cerrejón, que sería la causa principal de la actual crisis humanitaria y ambiental que se vive en La Guajira.

Esa situación de hambre y sed, se agrava ante la inminente desviación del Arroyo Bruno, afluente principal del Río Ranchería y fuente de agua de la que dependen comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao. La desviación hace parte del proyecto de expansión del proyecto minero que ha sido denunciado por la población que ve en riesgo su permanencia territorial.

El saqueo de agua con la que se abastecen las familias, la deforestación de cerca de 12 mil hectáreas de bosque seco tropical, la desaparición de lagunas y arroyos, los residuos de la mina que se arrojan a las fuentes hídricas, los engaños a las familias, la extinción de tradiciones culturales, entre otras vulneraciones a los derechos humanos y del ambiente, las denuncian la organización Censat Agua Viva - Amigos de la Tierra Colombia, junto a los delegados comunitarios Yoe Arregocés, Rogelio Ustate y José Manuel Vergara ante los micrófonos de Contagio Radio.

<iframe src="http://co.ivoox.com/es/player_ej_11254292_2_1.html?data=kpafl5mWfZOhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncanVw9HSz9TXb6LgyNrW1tSPcYy8wtLP1MqPvYznxsmYx9OPsMKfqNrOzM7WpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
