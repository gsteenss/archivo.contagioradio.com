Title: Así afecta el recorte presupuestal al deporte colombiano
Date: 2017-08-10 13:31
Category: Economía, Nacional
Tags: deporte colombiano, deporte olímpico, deportistas, willington ortiz
Slug: recorte-al-deporte-es-ilogico-y-desacertado-willington-ortiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/colombianos-con-medalla-olimpicos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Ago 2017] 

Ante el anuncio del Gobierno Nacional, de recortar el presupuesto dirigido al deporte colombiano para el 2018, los deportistas de diferentes disciplinas han manifestado su rechazo. Consideran que **el recorte de 347 mil millones de pesos es desacertado e ilógico** en un momento donde el rendimiento deportivo ha aumentado en el país.

Para Willington Ortiz, ex futbolista colombiano, **“el anuncio del gobierno es ilógico, el deporte está creciendo** y los deportistas están ganando medallas de oro en presentaciones internacionales como los Juegos Olímipicos”. Ortiz recalcó que el deporte es un “vehículos para la paz” que necesita recursos e inversiones.

Si bien el presidente Juan Manuel Santos anunció que el recorte presupuestal se va a hacer en la construcción y remodelación de escenarios deportivos, Ortiz indicó que **esto afecta enormemente al desarrollo del deporte en el país**. (Le puede interesar: ["El mérito para los campeones"](https://archivo.contagioradio.com/el-merito-es-para-los-campeones/))

Vale la pena recordar, que los Juegos Nacionales de Ibagué de 2015 **no se realizaron porque los escenarios deportivos no se encontraban listos** producto del robo más de 140 mil millones de pesos destinados a construir un complejo deportivo de atletismo, un tejódromo, una pista de BMX, piscinas olímpicas y un patinodromo. Dos años después estos escenarios no se han recuperado.

El ex futbolista colombiano, fue enfático en manifestar que los recursos que se le van a quitar al deporte hacen parte de las **inversiones que se deben hacer para las preparaciones del ciclo olímpico de los deportistas** y que garantizan el buen rendimiento de ellos y ellas. “El dinero se necesita para que los deportistas puedan realizar una buena preparación teniendo en cuenta a sus entrenadores”.

**Deportistas olímpicos se van a ver afectados por el recorte en el presupuesto**

Tanto Comité Olímpico como Coldeportes, manifestaron que la reducción en el presupuesto, no afectaría a los deportistas olímpicos quienes van a seguir recibiendo el pago mensual que reciben normalmente. Sin embargo, Ortiz indicó que la falta de inversión de recurso **afecta directamente a los deportistas olímpicos** en la medida que no se puede afectuar el pago a los entrenadores. (Le puede interesar: ["López de Micay construye paz a través del deporte"](https://archivo.contagioradio.com/lopez-de-micay-construye-paz-a-traves-del-deporte/))

Según el ex fútbolista, **el apoyo económico que recibe el deporte se traduce en gastos de uniformes, viajes, concentraciones** y lo más importante, en el pago a los entrenadores. “Son los entrenadores los que tienen que hacer la preparación de un ciclo olímpico en donde no sirve que hagan contratos a 3 o 6 meses, se debe garantizar que ellos van a estar presentes en todo el ciclo de trabajo”.

Ante esta situación, Ortiz le recordó al gobierno que **“tiene el deber de patrocinar y de invertir en el deporte**. Cada vez que hay una buena presentación salen los políticos a sacar el pecho y ahora salen con esta noticia”. Igualmente manifestó que es necesario que los deportistas tengan representación en el Congreso de la República para que “puedan pelear por sus derechos”.

<iframe id="audio_20272828" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20272828_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
