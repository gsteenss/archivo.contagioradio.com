Title: ¿De verdad usted cree que Piedad Córdoba e Iván Cepeda son malas compañías?
Date: 2020-01-29 16:21
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: cristianismo., Opinión, piedad cordoba, Religión, Senador Ivan Cepeda
Slug: de-verdad-usted-cree-que-piedad-cordoba-e-ivan-cepeda-son-malas-companias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right"} -->

(Romanos 2,24) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bogotá, 20 de enero del 2020  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right"} -->

*"Me gusta tu Cristo... No me gustan tus cristianos.  Tus cristianos son muy diferentes a tu Cristo"*. -Mahatma Gandhi.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Ministro ordenado tres**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cristianos, cristianos, personas interesadas  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordial saludo,  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En días pasados, con un amigo en común te recordamos y el te llamó y me pasó el teléfono para saludarte. Me saludaste diciéndome: *¡Que gusto saludarlo¡ ¿Cómo está?, ¡por ahí lo he visto por un rio en una canoa, muy mal acompañado de Piedad Córdoba¡.* Cuando te decía que la conocía personalmente, que era muy buena persona, valiosa, muy humana, me interrumpiste: *“De todas maneras es comunista y todos los comunistas son lo mismo, cortados con la misma tijera*” e hiciste una lista: Piedad Córdoba, Iván Cepeda y otras personas del ámbito nacional e internacional.    

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Intenté explicarte que lo que llamabas “comunismo” cortado con la misma tijera, **era un amplio y diverso abanico** de tendencias políticas, corrientes de pensamientos, ideas filosóficas, movimientos sociales, propuestas políticas y personas, incluso con posiciones antagónicas entre ellos y te dije*:*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*No todos los que llamas “comunistas” lo son, hay muchas diferencias entre ellos; lo mismo que no todos los llamado cristianos o católicos son lo mismo. Al interior de las iglesias hay corrientes, visiones y maneras de actuar muy diferentes. Es como si a todos los cristianos nos identificaran con Alejandro Ordóñez"*.

<!-- /wp:quote -->

<!-- wp:paragraph -->

Inmediatamente me cortaste: *“!Él es un buen cristiano, un político cristiano!*”. Intente explicarte las razones por las cuales no compartía esa idea y de nuevo me cortaste: “!*Él no tiene nada de malo, es un buen político cristiano¡”.* Recuerda que hice un momento de silencio, cambié de tema preguntándote por tu familia y terminé la llamada.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Te conozco personalmente hace muchos años en el “mundo eclesial” (con lo bueno y lo malo que tenemos los seres humanos); conozco personalmente a la Dra. Piedad Córdoba y al Senador Iván Cepeda, desde hace muchos años trabando por la paz y los derechos humanos; no conozco personalmente al Dr. Alejandro Ordóñez pero he seguido sus actuaciones públicas y sus posiciones religiosas.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No estoy de acuerdo con la **descalificación personal** que haces de la doctora Piedad y del doctor Iván por sus ideas y su militancia política, legal y democrática, con el único argumento de “ser comunista” y **no coincidir** con tus ideas, sin conocerlos personalmente. Tienes derecho a no estar de acuerdo con sus ideas, con su manera de pensar. Que las ideas políticas contrarias lleven a descalificar a alguien como persona es inaceptable, más para líder religioso. Del otro lado, defiendes incondicionalmente al Dr. Ordoñez porque **coincide con tu visión religiosa y política,** fruto de una ideologizada que niegas, así piensan y actúan muchos amigos y conocidos de los dos. Reconozco que mi manera de pensar y actuar es fruto de una ideología. La ideología está presente en todos los seres humanos, consciente o inconscientemente.     
No le queda bien a un ministro ordenado rechazar y descalificarlas personas, considerándolas malas por tener ideas contrarias. Discutir, disentir, rechazar o aceptar las ideas políticas es normal pero con argumentos, con informaciones y criterios verificables, sin hacer juicios personales de valor, dejándose llevar por emociones y sentimientos desordenados, asumiendo los criterios del reinado de Dios (verdad, justicia, equidad, respeto...) anunciado por Jesús de Nazaret para hacer valoraciones. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Conocí a la Dra. Piedad Córdoba en abril del 2005 viajando de Quibdó a Bogotá, viajaba en actividades humanitarias y ella trabajaba por el Chocó. La comunicación fue más fluida en Colombianos y Colombianas por la Paz apoyando actividades nacionales e internacionales para la liberación de soldados y policías en manos de las **FARC** a quienes ni al Estado, ni a las fuerzas armadas y de policía parecía importarles que “se pudrieran” en las selvas (ya habían sido liberadas las personas más importantes). La Dra. Piedad movida por el sufrimiento de los ellos y sus familias y la necesidad de su liberación para abrir posibilidades de salidas políticas al conflicto armado en Colombia se jugó su capital político, muy reconocido en ese memento, por ellos.     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Viajamos muchas veces, de muchas formas y en diversos momentos a comunidades indígenas, campesinas y afrocolombianas en riesgo por la confrontación armada; a encuentros con familiares de policías y militares en manos de las FARC, abandonados a su suerte, para escuchar su dolor y conocer su drama; a diversos países para buscar solidaridad con liberación de policías y militares y apoyo para las víctimas de crímenes de Estado. En esos viajes la vi conmoverse con el drama, los sufrimientos y los dolores de las víctimas y realizar acciones concretas para paliar su dolor.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Conocí al Dr. Iván Cepeda, desde la creación del **MOVICE** (Movimiento de Victimas de Crímenes de Estado) y hemos compartido momentos duros, dolorosos y esperanzadores en el acompañamiento a las víctimas, en el trabajo por la paz, la justicia y la superación de la impunidad jurídica, social, religiosa y económica. En estos quince años he visto una persona honesta y coherente, de profundas convicciones políticas (no politiqueras)  y sociales; generoso y sin sectarismos. De gran calidez humana.      

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dos han sido víctimas del conflicto armado: a Iván le mataron el papá; a Piedad la secuestraron, le desaparecieron una hija; los dos vivieron tragedia humana, cultural, económica y política del exilio. A los dos los he visto trabajar por personas de partidos políticos contrarios a sus ideas y opciones, ayudando a salvar la vida de familiares de quienes mataron sus familiares y amigos, de quienes los amenazaron y los iban matar; los he escuchado hablar de sus victimarios con claridad, dignidad y sin venganza; los dos ha sido cacapaces de colocar el bien comun por encima de  los intereses personales. Con todo el daño sufrido no les he visto odio y fanatismo contra sus opositores o contradictores político e ideológicos, en cambio sí se lo he visto en muchos creyentes (cristianos  y cristianas del común, sacerdotes, pastores y líderes religiosos). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es de público conocimiento la intolerancia radical del Dr. Ordóñez hacia quienes piensan, son distintos y tienen creencias religiosas diferente a las suyas; la practica un cristianismo que podemos llamar “fariseo”, “letrado” y “saduceo”, es decir, más preocupado por la tradición, el legalismo, el puritanismo discriminatorio que por el mandamiento del amor, incluso a los enemigos y la práctica de la Justicia que predicada por nuestro Señor Jesucristo (Juan 15,9-17;  Mateo 5,6). Recuerda que los palabras más duras pronunciadas por Jesús fue contra los fariseos y los escribas o letrados que eran personas religiosas (Mateo 23,1-39). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muchas decisiones del Dr. Ordoñez fueron determinadas por cercanía o lejanía ideológica, por intereses politiqueros más que por la contundencia de las pruebas y el criterio de justicia; contradiciendo de hecho la Palabra de Dios:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “*No juzgará  por apariencia ni sentenciará de oídas, juzgará con justicia a los desvalidos y sentenciará con rectitud a los oprimidos”*
>
> <cite>(Isaías 11,3-4). </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El evangelio de San Lucas (18,1-8), Jesús cuenta la parábola de la viuda que va donde un juez, que ni temía a Dios ni respetado a los hombres, a que le hiciera justicia;  en principio no lo hizo, pero ante la insistencia de la viuda, le hizo justicia. A la campesina Ilda, el ejército le asesino su esposo en su propia casa (un líder campesino reconocido por toda la comunidad) en el año 90 dejándola viuda con 7 hijos menores; años después, presentó una demanda administrativa para que el Estado repara la familia y poder “sacar adelante” sus hijos. El expediente llegó despacho del Magistrado del Tribunal Administrativo de Santander Alejandro Ordóñez, quien en su sentencia no cuestionó la veracidad de los hechos pero le negó los derechos a la campesina y sus hijos porque para acreditar “su parentesco” presentaron la partida de matrimonio y las partidas de bautismo y, según el magistrado, esos documentos no eran aptos para probar “legalmente” el parentesco.    

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entiendes ahora, ¿Por qué considero al Dr. Ordoñez como un buen ejemplo del **actuar anticristiano** (contra el mensaje de Cristo) y a la Dra. Piedad  y al Dr. Iván buenas compañías?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [Columnas de A quien Correspond](https://archivo.contagioradio.com/author/a-quien-corresponde/)e

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe, 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P francoalberto9@gmail.com

<!-- /wp:paragraph -->
