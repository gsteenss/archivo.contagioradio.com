Title: Los cambios del congreso a las Circunscripciones de Paz
Date: 2017-11-10 17:34
Category: Nacional, Paz
Tags: acuerdos de paz, Circunscripciones de paz
Slug: circunscripciones-paz-un-paso-firme-en-la-implementacion-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Nov 2017]

Fueron aprobadas las circunscripciones de paz en la Cámara de Representantes, el reto ahora es que estas curules que se abren como escenario para ampliar la democracia del país, sean ocupadas **por las víctimas del conflicto armado y aquellas comunidades que han estado en el olvido estatal** y sin la influencia de los poderes tradicionales.

De acuerdo con Alirio Uribe representante por el Polo Democrático, señaló que se realizó un pacto en la Cámara de Representantes entre las fuerzas de la Unidad Nacional, para avanzar en la aprobación de las circunscripciones de paz y expresó que lo que sucede ahora es que **hay dos versiones de estas circunscripciones aprobadas una en Senado y otra en Cámara.**

Por un lado, la versión de la Cámara se dice que solamente se votará en zonas rurales de los 172 municipios que conforman las 16 circunscripciones de paz, y del otro lado se encuentra la de Senado que señala que se puede votar en las zonas urbanas y rurales y que cuando en algún municipio de los 152 existan más de 50 mil personas, en el censo electoral, ahí solo se votará en el área rural, en ese sentido Alirio Uribe manifestó que la versión que quedo de Senado es mucho más incluyente.

De igual forma en la propuesta de Senado podrían aplicar líderes sociales y víctimas del conflicto armado, mientras que en la de la Cámara solo pueden participar víctimas, “esto implicaría que por 8 años **tendríamos 16 víctimas con representación en el Congreso y que son de las regiones más afectadas por la violencia y excluidas de la vida política**” afirmo el representante.

El paso siguiente es que los equipos conciliadores tanto de Cámara como de Senado se reúnan para elegir una sola propuesta y luego que esta sea votada. (Le puede interesar: ["Los Acuerdos de Paz conjean en el Congreso de la República"](https://archivo.contagioradio.com/los-acuerdos-de-paz-cojean-en-el-congreso-de-la-republica/))

### **¿Hay dineros desde la Registraduría para financiar las circunscripciones de Paz?** 

En días pasado el Registrador había señalado en la Cámara de Representantes que no había dinero para poner en marcha las circunscripciones de paz, sin embargo, de acuerdo con el representante Alirio Uribe ayer el gobierno expreso que si hay presupuesto **para este punto y que el Registrador tendría que cumplir la ley y la constitución**.

En esa misma medida, ayer se aprobó una proposición en donde se obliga a que se instalen nuevos puestos de votación y por los tiempos es posible que se deba aplazar la inscripción de candidatos un mes. (Le puede interesar: ["Listos los 11 integrantes de la Comisión de la Verdad, la Convivencia y la No Repetición"](https://archivo.contagioradio.com/listos-comision-de-la-verdad/))

<iframe id="audio_22059326" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22059326_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
