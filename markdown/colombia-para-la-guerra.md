Title: Colombia PARA la guerra
Date: 2015-04-29 09:54
Author: CtgAdm
Category: Opinion, superandianda
Tags: colombia, Juanes, León Tolstoi, paz, Shakira, superandianda, uribe
Slug: colombia-para-la-guerra
Status: published

#### ‏Por [Superandianda ](https://archivo.contagioradio.com/superandianda/) 

Hablar de guerra y paz en Colombia, no es precisamente citar el concepto literario de la obra de León Tolstoi sino repetir el pobre y erróneo concepto que creo la ultraderecha para prolongar un conflicto que lleva más de 50 años en nuestra historia. La pobreza conceptual del colombiano promedio nace del nulo criterio político que posee y su incapacidad para fundar opiniones con contenidos exteriores diferentes a los que le ofrecen los medios de comunicación privados.

‏¿Por qué Colombia está dispuesta a mantener el conflicto armado a pesar de sus víctimas, abusos y divisiones? De manera absurda han logrado crear valoraciones sociales argumentadas en conceptos que nada tienen que ver con el verdadero significado de las palabras; es por eso que opinar diferente es terrorismo y vivir violentamente es cotidianidad. Existe un sistema diseñado que nos invita a sentirnos orgullosos de la guerra y a apoyarla, viene desde el esquema educativo hasta el informativo que tienen como único propósito formar sujetos incapaces de razonar distinto a lo que les muestran en la televisión o les enseñan en las aulas. Colombia PARA la guerra es el lenguaje que todos hablan: falsas concepciones que tienen como único objetivo tocar la “sensibilidad” de las personas creando odios y amores entre dos actores armados.

El diccionario PARA Colombia tiene palabras claves que en su momento no solo han servido para festejar muertes sino para promover campañas políticas, por ejemplo, héroe: los héroes de Colombia no tienen súper-poderes para salvar a las personas, a duras penas alcanzan el bachillerato, se originan en los estratos 1 ó 2 y su tarea es defender los intereses de la clase política dominante, clase que nunca estaría dispuesta a entregar a sus hijos para combatir en el monte porque los envían a estudiar al exterior para que vuelvan a seguir administrando el país; estos héroes son entregados por las familias más desfavorecidas para salvar la patria, y aparece otro concepto patria: Quizás es el amor a la tierra que canto Juanes o la campaña publicitaria de Colombia es pasión que justificaron en su momento la mano firme y el corazón grande del uribismo.

‏¿Defender la patria y morir como un héroe? Si ser héroe en Colombia es que los más pobres no tengan la posibilidad de ingresar directamente a la educación superior porque es obligatorio entregarse como carne de cañón para defender a un estado corrupto y sin memoria, mientras los hijos de los privilegiados se convierten en jóvenes emprendedores enriquecidos con zonas francas, es mejor ser un completo cobarde en este país. No vamos a negar lo conmovedor que es ver en escenario a Juanes cantándoles a los soldados discapacitados vestidos con la bandera colombiana con lágrimas en sus ojos, pero lamentablemente la guerra no es farándula ni entretenimiento, sus victimas no son trofeos y tampoco podemos llamar patria ni héroes a un sistema diseñado solo para unos cuantos y mantenido con el sufrimiento y la sangre de miles de ignorantes.

‏“Si todos hicieran la guerra por convicción no habría guerra” es cierto Tolstoi; lo difícil en Colombia es entender a la guerra y a la paz con un significado diferente al que nos publican los periodicos, el trabajo es lograr ver que unas muertes no son más justas que otras y que la patria no es la representación de Shakira en un mundial, ni los goles de James, ni las artesanías de los Uribe y mucho menos la dignidad es la prolongación de la historia violenta sin estar dispuestos a cambiar y a estudiar los conceptos fuera del sistema diseñado PARA Colombia, ese sistema que solo nos ha dejado miseria, ignorancia y desigualdad.

A propósito, queda abierta la lectura de GUERRA Y PAZ de León Tolstoi, gran expresión del sentido humano a la NO violencia.

#### [~~@~~Superandianda](https://twitter.com/Superandianda)
