Title: La cercana relación entre contaminación y Transmilenio
Date: 2018-09-14 12:36
Author: AdminContagio
Category: Ambiente
Tags: Bogotá, contaminación, Transmilenio
Slug: contaminacion-transmilenio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/csm_AgenciaNoticias-20170614-03_05_9e474c1549.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [14 Sept 2018] 

[En el 2012 **Bogotá recibió el premio Mundial de liderazgo climático y ciudad en Londres**, en la categoría de transporte Urbano. Después de 6 años, se abre una nueva licitación en la que se incorporará aproximadamente 1.400 buses nuevos al Sistema  y tecnologías de cero o bajas emisiones de material particulado.]

[Sin embargo, se sigue pensando en Buses con motor Diesel convencionales que arrojan 4,6 toneladas] [de CO2. En esta licitación, **no existe  una obligación que comprometa al sistema de transmilenio  para que la tecnología que se use en estos buses nuevos sea limpia**, como tampoco existe un incentivo real para que los proponentes incluyan estas tecnologías.]

[La contaminación del aire es el principal riesgo ambiental para la salud en áreas urbanas y el cuarto factor causal de muerte entre todos los riesgos para la salud. De acuerdo con la Organización Mundial de la Salud (OMS), **Transmilenio aporta el 10 % de las emisiones de CO2 del transporte**. Esto en cifras parece poco,  pero cuando se tiene en cuenta que, en promedio, son dos millones de viajes al día en el Sistema y que además, es el medio de transporte en la ciudad en el que los usuarios permanecen más tiempo evoca preocupación.]

[ El no tener controlado los niveles de contaminación provocados por las emisiones de los buses, causa una alerta de salud pública. Además, esta situación es una **clara vulneración de los derechos fundamentales a la salud y a la vida de las personas** que usan Transmilenio todos los días, que son las que están más expuestas, pues se mueren y se enferman a causa de la exposición personal a las emisiones de material particulado.  ]

[Estos argumentos son paradójicos ya que el sistema de Transmilenio fue creado con el propósito de remediar la problemática de la movilidad y el  ambiente de Bogotá en el 2000, y 18 años después **es uno de los emisores de dióxido de carbono más significantes y dañinos para salud de los capitalinos.**  ]

[infografia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/infografia.pdf)  
<iframe id="audio_28584837" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28584837_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
