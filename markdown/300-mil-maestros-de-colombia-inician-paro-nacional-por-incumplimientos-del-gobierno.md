Title: 300 mil maestros de Colombia inician paro nacional por incumplimientos del gobierno
Date: 2016-06-01 12:42
Category: Movilización, Paro Nacional
Tags: fecode, maestros, Paro Nacional
Slug: 300-mil-maestros-de-colombia-inician-paro-nacional-por-incumplimientos-del-gobierno
Status: published

###### Foto: Contagio Radio 

###### [1 Jun 2016]

Este miércoles cerca de **300 mil maestros de todo el país adelantarán un paro nacional de durante 24 horas,** convocado hace algunos días por la Federación Colombiana de Trabajadores de la Educación, Fecode, donde los profesores buscan exigir mejoras en la prestación del servicio de salud de los docentes y sus familias.

“Frente a esta realidad, llamamos al magisterio colombiano a movilizarnos este 1° de junio por un servicio de salud que se corresponda con el Pliego de Condiciones contratado, la definición de la nueva licitación sin más dilaciones y en los términos establecidos en la Ley 91 de 1989, el cumplimiento cabal de los acuerdos firmados y concitamos a la comunidad educativa, a los gobernadores, alcaldes, diputados y concejales, a hacer causa común en la lucha por un mejor y mayor financiamiento del Sistema General de Participación -SGP-, en tanto y por cuanto los recursos para educación, salud, agua potable y saneamiento básico, dependen de este sistema”, menciona el comunicado de  Fecode, mediante el cual se informa sobre el paro.

De acuerdo con Luis Alberto Grubert, presidente de Fecode, el 7 de mayo de 2015 se había pactado un acuerdo donde el gobierno se comprometía con el magisterio en la mejora del servicio de salud, sin embargo, **un año después denuncian que este servicio en lugar de regenerarse ha empeorado**  pese a que se conoce que de los 1,6 billones de pesos que el gobierno presupuesta para este tema, realmente solo se invierten 600 mil millones de pesos.

“**El gobierno se está ahorrando 600 mil millones de pesos, no hay necesidad de hacer un esfuerzo presupuestal, los recursos necesarios para el mejoramiento del servicio de salud ya están**”, explica Grubert, quien indica que en términos concretos, piden que se el servicio de salud sea oportuno, no existan barreras de acceso a medicamentos, procedimientos, y citas con especialistas.

Los maestros han tenido algunas reuniones con el viceministro de educación, Víctor Saavedra y se ha solicitado **en reiteradas veces una reunión con el presidente Juan Manuel Santos, sin embargo, no ha habido ninguna respuesta por parte del mandatario**. Por el momento, los profesores anuncian que están abiertos al diálogo y que solo se tratará de un paro de 24 horas.

[Fecode Mayo 29](https://es.scribd.com/doc/314503918/Fecode-Mayo-29 "View Fecode Mayo 29 on Scribd")

<iframe id="doc_11013" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/314503918/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe><iframe src="http://co.ivoox.com/es/player_ej_11745816_2_1.html?data=kpaklpqcdZehhpywj5WaaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYqdfZpsbm1ZCajauph7C4ppKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
