Title: Asesinan a Paula Rocero, Personera y defensora de DD.HH. en Samaniego, Nariño
Date: 2019-05-21 10:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de defensores de derechos humanos, nariño, Paula Andrea Rocero, Samaniego
Slug: asesinan-a-paula-rocero-personera-y-defensora-de-dd-hh-en-samaniego-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Samaniego.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PGN Col] 

El pasado 20 de mayo fue asesinada **Paula Andrea Rosero, abogada y personera del municipio de Samaniego, Nariño** tras ser víctima de un ataque con arma de fuego por hombres desconocidos que se desplazaban en moto por el sector urbano conocido como Romboide a tan solo un kilómetro del barrio San Juan dónde residía la funcionaria.

La abogada de 47 años se desplazaba en su auto cuando fue abordada por dos personas armadas en moto quienes dispararon en repetidas ocasiones contra su humanidad, los atacantes huyeron del lugar mientras Paula fue trasladada a un centro hospitalario al que llegó sin signos vitales.[(Lea también: Lideresa social Mayerlis Angarita sobrevive a atentado en Barranquilla) ](https://archivo.contagioradio.com/lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla/)

La personera, que velaba por la defensa de derechos humanos en Samaniego, ya había sido amenazada en 2016  luego de denunciar ante la Alcaldía un presunto manejo inadecuado de los recursos del centro asistencial Lorencita Villegas, en aquella ocasión se había realizado la solicitud de su protección.

La Defensoría del Pueblo  manifestó su solidaridad con la familia de la Personera y exigió que las autoridades correspondientes aclaren cuánto antes las circunstancias de su muerte mientras la Procuraduría General rechazó el suceso y señaló que con el fin de esclarecer quiénes fueron los responsables se ha desplazado una comisión  a Samaniego.

> .[@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) y [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) condenan crimen de la personera de Samaniego, Nariño. [\#Comunicado](https://twitter.com/hashtag/Comunicado?src=hash&ref_src=twsrc%5Etfw) conjunto [pic.twitter.com/bxd8puqQ8R](https://t.co/bxd8puqQ8R)
>
> — Defensoría delPueblo (@DefensoriaCol) [21 de mayo de 2019](https://twitter.com/DefensoriaCol/status/1130683036822048768?ref_src=twsrc%5Etfw)

> ? Con tristeza condenamos homicidio de personera de Samaniego, Nariño, Paula Andrea Rosero. Confiamos en que habrá pronta investigación, juzgamiento y sanción de los responsables. Nuestra solidaridad con su familia, amigos e instituciones en estos tristes momentos [pic.twitter.com/3IjJLZcKtz](https://t.co/3IjJLZcKtz)
>
> — ONU Derechos Humanos Colombia (@ONUHumanRights) [21 de mayo de 2019](https://twitter.com/ONUHumanRights/status/1130806022250086400?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
