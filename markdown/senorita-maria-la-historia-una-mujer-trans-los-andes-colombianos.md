Title: Señorita María, la historia de una mujer Trans en los andes colombianos
Date: 2017-02-24 14:00
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine Colombiano, Documental, lgtbi, Ruben Mendoza, Señorita María
Slug: senorita-maria-la-historia-una-mujer-trans-los-andes-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/señorita-maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Poster Oficial 

##### 24 Feb 2017 

En el marco del Festival Internacional de Cine de Cartagena de Indias - FICCI 57, el cineasta colombiano Rubén Mendoza estrenará su más reciente trabajo, Señorita María - La falda de la montaña, un documental que hará parte de dos de las competencias oficiales de la cita cinematográfica.

El quinto largometraje del director colombiano, acerca al espectador a la vida de María Luisa, una mujer campesina de 45 años a la que se le asignó al nacer un género distinto al que escogió. La historia que conquistó el lente del director, se desarrolla en Boavita, un pueblo conservador y católico detenido en el tiempo, incrustado en los andes boyacenses.

La existencia de la señorita María, ha estado marcada por su amor por los animales, su valentía, su fe y su carácter, que le han permitido sobrevivir en un contexto de exclusión y desprecio. No ha existido oscuridad capaz de derribarla ni de eclipsarla.

"El amor es un animal capaz de adaptarse a las más áridas montañas del odio. Esta película, es fundamentalmente un retrato. El retrato de una fuerza descomunal, femenina y desconocida para mí, para muchos”. Rubén Mendoza. Le puede interesar: [Pride: la unión hace la fuerza](https://archivo.contagioradio.com/pride-la-union-hace-la-fuerza/).

El director de producciones como La sociedad del semáforo, Tierra en la lengua y Memorias de Calavero, logra con esta producción, descentralizar la discusión sobre la comunidad LGBTI y llevarla a un escenario rural, estableciendo un inusual dialogo en el cine nacional, sin dejar de lado su habitual reflexión sobre la sociedad colombiana.

<iframe src="https://www.youtube.com/embed/lR0NIKn4y2Y" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El festival de cine más antiguo de Latinoamérica se llevará a cabo del 1 al 6 de marzo en la ciudad amurallada, en donde además del film Señorita María - la falda de la montaña, producido por Amanda Sarmiento, se exhibirán más de 160 películas en diferentes categorías que van desde la competencia oficial de ficción, nuevos creadores y cine colombiano.
