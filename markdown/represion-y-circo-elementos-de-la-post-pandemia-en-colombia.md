Title: Represión y circo: elementos de la post-pandemia en Colombia
Date: 2020-05-12 01:32
Author: AdminContagio
Category: Actualidad, Entrevistas
Tags: Economía, Wilson Arias
Slug: represion-y-circo-elementos-de-la-post-pandemia-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Represión-de-la-policía.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Represión-y-Circo.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Represión-y-circo-en-Bogotá.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Marovaan {#foto-marovaan .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador Wilson Arias se ha caracterizado por hacer un importante control político desde su curul a las decisiones del Gobierno en cuanto a temas económicos. Es común que Arias nos acompañe en los micrófonos de Contagio Radio cuando hablamos sobre reforma pensional, fiscal o gasto público y en esta ocasión, habló nuevamente sobre el manejo de los recursos que está dando el presidente Duque, porque han beneficiado a grandes capitales en detrimento de los más vulnerables y según el congresista: para financiar la represión.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Contagio Radio: En un momento tan difícil como el que estamos viviendo, en que estamos en estado de emergencia y el Presidente tiene facultades extraordinarias, ¿qué pensar de la labor del Congreso que aún no sesiona en forma y no hace su labor de control político y legislativo como debería?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Wilson Arias: En esto nos estamos poniendo de acuerdo porque el tema ha evolucionado. Les contaré una anécdota: yo estaba denunciando uno de los decretos que le daba más recursos al holding de Sarmiento Angulo, preparé mi intervención de fondo y no fue transmitida. Transmitieron a todo el mundo pero me interrumpieron cuando hablé de ese tema, y a mí no me parece gratuito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Puede ser que como dicen algunos amigos de la Policía, yo soy paranóico. Pero lo cierto es que me enteré por las redes que me cortaron y no pude reclamar, es como si Contagio Radio me quitara el chat mientras yo estoy hablando sobre un asunto del interés financiero, y sobre hechos que estaban afectando el sector financiero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eso significa que no podemos presentar mociones, ¿saben la importancia de presentar mociones de orden en un espacio en el que nos están haciendo 'jugaditas' todos los días? Es decir, si el señor Macías tiene la actitud de hacer la jugadita el día que está transmitiendo en vivo para quitarle la voz al Polo Democrático también en esa oportunidad, ¿se imaginan cómo ocurre actualmente por medios virtuales?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Colombia tiene una tradición presidencialista, esto es que en la estructura del poder tiene mucho peso el ejecutivo; y esa tradición con un gobierno autoritario ahora es investido con unas facultades extraodinarias en medio de un confinamiento también de las fuerzas políticas. Y a ese Presidente se le viene una época de hiperpresidencialismo, para asignar una categoría socio-políto, en medio de la cual es silenciada la oposición y confinada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y cuando un representante de la cámara osa ir para hacer su labor de manera semipresencial, el presidente de la corporación que antes era de la oposición y ahora del Gobierno, proscribe el ingreso de los representantes de la cámara. Y entre tanto, un representante del Centro Democrático acaba de decir que la democracia no puede ser obstáculo para la actuación del Gobierno contra la pandemia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Es un Gobierno que le está entregando el país al sector financiero, al ESMAD, y contratos de imagen.
>
> <cite>Wilson Arias</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El tema da para mucho, pero el contrato de la imagen y el contrado para represión con las armas del ESMAD es biopolítica. Son los cuerpos, objeto de un debate político porque nos confinan a quienes decimos representar, y no nos permiten ingresar al recinto de discusión. (Le puede interesar: ["En veinte años el ESMAD ha asesinado a 34 personas"](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: En el nuevo decreto tampoco se incluyen formas para reactivar el Congreso, ¿qué están pensando los congresistas de oposición para solucionar esto?

<!-- /wp:heading -->

<!-- wp:paragraph -->

WA: Desde toda la bancada de oposición, y bancadas independientes están jugando un papel importante porque estamos logrando que el país comience a interrogarse sobre este hecho tan particular. Entonces ya hay un nuevo momento que hemos propiciado, y propongo que analicemos desde nuevas categorías políticas porque el asunto tiende a despolitizarse.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto no puede ridiculizarse, porque la psicología explica que en momentos tan difíciles como estos la gente puede estar esperando un hombre providencial que los salve. **Para la izquiera, Duque puede ser un petardo, pero es un hombre que todos los días aparece en televisión diciendo que él es el salvador** y termina mucha gente de vocación democrática creyéndolo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: También hay controles que uno como ciudadano está esperando, como el de la Corte Constitucional que pareciera que no va al ritmo que se desearía ¿qué pensar de esta situación?

<!-- /wp:heading -->

<!-- wp:paragraph -->

WA: Estamos en momento de incertidumbre, y es bueno decir que yo también me pregunto sobre la labor de la Corte Constitucional. Porque es un momento de lucha política y lucha de ideas, luchas que no hemos perdido, porque recordemos que Duque no quería confinarse y fue la opinión pública la que lo obligó, así como los alcaldes con sus aciertos y defectos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Lo hago notar para decir que en el Congreso podríamos tener mayorías para una cierta política. Por eso no quieren que funcione.** Es que frente a la economía de este tipo de momentos es fácil ponerse de acuerdo en Keynes. Lo que propone es que frente a la crisis hay un recetario que más o menos todos admitimos, y en el congreso más o menos todos estamos de acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Por qué no se aplican esas propuestas? Primero, por el *momentum* excepcional y jurídico, pero tampoco nos ponemos de acuerdo porque Carrasquilla le hace caso al Fondo Monetario Internacional (FMI). Es que es un hombre antinacional, la receta monetarista está allí con todas sus letras: haga liquidez para el sector financiero, haga política expansiva pero para los bancos, irrigue la sociedad a través del sistema bancario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuando le decimos que busque plata a través de las reservas internacionales dice: no, porque el FMI no gusta de esa receta. Digo esto para decir que en esta complejidad no permiten que el Congreso se reuna, y no sé si es ingenuidad, pero en este momento tan especial uno piensa que si Duque no actúa correctamente que lo haga la Corte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: Sobre la compra que hizo el ESMAD para la represión de protestas, ¿es posible detener esa licitación?

<!-- /wp:heading -->

<!-- wp:paragraph -->

WA: A mí me da pena decirle al país que se están [gastando el dinero](https://twitter.com/wilsonariasc/status/1255156794927616003/photo/1) en el ESMAD, eso nos pregunta por el país que tenemos. Y el director General de la Policía dice que lo tenían previsto, pero yo sobre eso le creo: yo creo que el 21 de noviembre pensaron que este año sería de protestas y eso ya está estudiado en términos sociológicos sobre la cantidad de agitación en términos de protesta social venía para Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces dijeron: vamos a darle represión a este pueblo como sea, y aparece la pandemia. Y reconocen que no hay un plan social ni en pandemia ni post-pandemia. Pero en post-pandemia sí puede haber represión y circo, ya no pan y circo: ESMAD e imagen presidencial. Esa es la formula de la seguridad democrática: un capo carismático, eso fue Uribe, el capo carismático con un aparato de represión, eso es lo fundamental.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: ¿En razón de la pandemia se están planteado en el Congreso una agenda de derechos para responder a las demandas históricas en detrimento de esa represión?

<!-- /wp:heading -->

<!-- wp:paragraph -->

WA: En estos momentos hay un amplio márgen para la reforma a la salud, lo que ha revelado la pandemia es el fracaso del sistema sanitario. En épocas del capital ficticio y de financiarización de la economía, entonces también: abajo la universidad pública. Pero actualmente tengo que decir que volvió a ponerse en la agenda la clase obrera, trabajadora, y también los campesinos volvieron a existir porque son los que ponen el plato todos los días en la mesa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y una justicia alimentaria, energética y también social se pone también al órden del día; volvió a aparecer el Condor de los Andes y volvió a verse el Himalaya desde la India. **Lo que estoy diciendo es que esta es una oportunidad para soñar**, pero no para soñar de manera absurda, nos estamos disputando asuntos concretos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yo creo que salimos de aca para una reforma a la salud y una reforma educativa, es que el 21 de noviembre no se explica sin los jóvenes y las mujeres. Y todas las causas que nos llevaron a esa fecha siguen vigentes y se han exacerbado. (Le puede interesar: ["De la renta básica, a la garantía total de derechos"](https://archivo.contagioradio.com/de-la-renta-basica-a-la-garantia-total-de-derechos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
