Title: General (r) Henry Torres llamado a juicio por ejecuciones extrajudiciales
Date: 2016-08-12 17:49
Category: Judicial, Nacional
Tags: crímenes de estado, Ejecuciones Extrajudiciales, Ejército Nacional, falsos positivos
Slug: general-r-henry-torres-llamado-a-juicio-por-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [12 Ago 2016] 

La Corte Suprema de Justicia emitió el día de ayer, una resolución acusatoria contra el excomandante de la brigada 16 del Ejército Nacional, Henry William Torres Escalante, por su presunta **participación en la ejecución extrajudicial de Daniel Torres (padre), y Roque Julio Torres (hijo).** Hechos ocurridos en el año 2007, en municipio Aguazul del departamento del Casanare en el marco de la llamada Política de Seguridad Democrática.

Los antecedentes frente al caso del general Torres, iniciaron en el mes de marzo del presente año cuando la Fiscalía había ordenado su captura por “**hechos comprometedores en los casos de  'falsos positivos'”.** No obstante, la “determinación del ente acusador” logró ser aplazada gracias a la “petición oficial” en la que se esperaba la renuncia del general Henry William Torres, la cual nunca llegó.

[Esta vez las pruebas de la Fiscalía fueron suficientes para determinar que el oficial es responsables de al menos dos casos de ejecuciones extrajudiciales. S]eis soldados pertenecientes a la brigada XVI del ejército Nacional, de la cual estaba a cargo el general retirado Henry William Torres, fueron detenidos  por órdenes de la Unidad de Derechos humanos y Derechos Internacional Humanitario de la Fiscalía General de la Nación a causa de su presunta participación en los asesinatos de población civil, presentada en los informes oficiales como “muertes en combate”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
