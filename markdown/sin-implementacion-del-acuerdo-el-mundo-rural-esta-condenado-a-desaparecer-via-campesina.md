Title: Sin implementación del Acuerdo "el mundo rural está condenado a desaparecer" Vía Campesina
Date: 2017-11-21 12:25
Category: Nacional, Paz
Tags: acuerdos de paz, campesinos, comunidades, La Vía Campesina, Misión de Verificación, misión internacional
Slug: sin-implementacion-del-acuerdo-el-mundo-rural-esta-condenado-a-desaparecer-via-campesina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/campesino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

###### [21 Nov 2017] 

El día de hoy inició la II Misión Internacional de la Vía Campesina, quienes han acompañado la **verificación del punto sobre la Reforma Rural Integral del Acuerdo de Paz** firmado y acordado entre el Gobierno Nacional y las FARC. Durante una semana, visitarán comunidades del Meta, Caquetá, Cauca, Arauca y Tumacoy recogerán preocupaciones de campesinos, ex combatientes y habitantes de estos territorios.

El movimiento campesino internacional manifestó que a lo largo de la misión realizarán actividades de verificación para constatar la forma como se está realizando la implementación del Acuerdo de Paz en los territorios. En rueda de prensa indicaron que hay una preocupación por el **incumplimiento y la violación a los derechos humanos** en los territorios que pone en riesgo al campesinado colombiano.

### **Comunidad internacional deberá presionar por garantías al cumplimiento del Acuerdo de Paz** 

De acuerdo con Federico Pacheco, integrante del movimiento Vía Campesina en Europa, “a un año de la firma de los Acuerdos queremos comprobar de una manera directa en los territorios **hasta qué punto se han cumplido los compromisos firmados**”. Dijo que en el movimiento hay conciencia de que “no se está respetando el contenido del Acuerdo y tampoco se está poniendo en marcha las medidas legislativas para implementar lo pactado”. (Le puede interesar: ["La paz territorial no se siente en los territorios"Misión Internacional](https://archivo.contagioradio.com/la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional/))

Afirmó que los ex combatientes y los campesinos **no tienen garantías para la protección de su vida** y tampoco hay voluntad del Estado colombiano por liderar la defensa de los derechos de estas personas y no hay condiciones desde el Gobierno para generar alternativas efectivas para los campesinos. Ante esto, desarrollarán actividades para acercarse a las comunidades y recoger sus preocupaciones.

Pacheco indicó que el mayor reto de la comunidad internacional será cuando deban presentar el informe a la sociedad civil para **lograr la difusión efectiva de lo que está sucediendo en Colombia**. Manifestó que “se deben proponer soluciones y presionar a los organismos internacionales y a los Estados que se han comprometido con apoyar el cumplimiento del Acuerdo de Paz”.

### **Es necesario que congresistas se comprometan con los Acuerdos de Paz** 

En ocasiones pasadas, la Vía Campesina se reunió con algunos congresistas de Colombia para un intercambio de las diferentes preocupaciones alrededor de la implementación del Acuerdo de Paz. En esta segunda Misión, se reunirán para ver **los avances en materia de legislación** teniendo en cuenta que se acerca el final del mecanismo de Fast Track. (Le puede interesar:["Proceso de implementación afronta serias dificultades" Misión Internacional"](https://archivo.contagioradio.com/mision-internacional-verificacion-implementacion/))

El movimiento campesino espera que “los partidos políticos tengan un **compromiso prioritario con la paz** y con el cumplimiento de lo acordado por sobre otros intereses o estrategias que deben ser dejadas de lado”. Dijo que para esto es necesario que se unan fuerzas para que el proceso sea un éxito y además que sea posible hacerle frente “a los intereses de las multinacionales que están poniendo trabas en beneficio de sus propios intereses”.

Finalmente, resaltó que “sin un desarrollo rural y un cambio profundo en lo que es la estructura de la tierra y el apoyo a la agricultura campesina, **el mundo rural está condenado a desaparecer** o a seguir en la miseria”. Estableció que en Colombia, el Acuerdo de Paz es una herramienta para transformar esa realidad e invitó a la población urbana a “no dejarse engañar por el discurso de la extrema derecha que enfrenta a los pobres contra los pobres, los acuerdos no van a premiar a un sector en beneficio de otro”.

<iframe id="audio_22207523" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22207523_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
