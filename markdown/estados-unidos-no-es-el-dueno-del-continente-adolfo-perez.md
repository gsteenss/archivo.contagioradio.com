Title: “Estados Unidos no es el dueño del continente” Adolfo Pérez
Date: 2015-01-30 22:27
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: Adolfo Pérez Esquivel, Argentina, Bolivia, CELAC, Chile, Cuba, paz, Venezuela
Slug: estados-unidos-no-es-el-dueno-del-continente-adolfo-perez
Status: published

###### Foto: lanacion.com.ar 

##### [Adolfo Pérez Esquivel] <iframe src="http://www.ivoox.com/player_ek_4020859_2_1.html?data=lZWfkp2ZfY6ZmKiak5iJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYo8nTsMfjjLWSpZiJhZrmxt%2BYp9jVucrqxtGSlKiPlNPZzs7cjbPTpsbgjMnSjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

El Premio Nobel de Paz, Adolfo Pérez Esquivel, afirma que la reciente cumbre de la CELAC, demuestra que “Estados Unidos no es el dueño del continente”.  Pérez Esquivel agrega que “**La CELAC es uno de los organismos de integración más importantes**” y deberá ser el escenario para resolución de varios conflictos como el de **Bolivia y Chile por la salida al mar, la defensa de las Islas Malvinas por parte de Argentina y la ocupación militar extranjera en Haití.**

Uno de los puntos más relevantes en el panorama de Latinoamérica y el Caribe es el del **restablecimiento de las relaciones entre EEUU y Cuba**, en este aspecto Pérez señala que **el país del norte no podía seguir imponiendo sus condiciones sobre la Isla** que es un territorio soberano y libre, por lo tanto debe agilizarse y concretarse el desbloqueo económico.

Así mismo, la **salida de las bases militares de la OTAN o de EEUU** de los países del continente y el **cierre de “Guantánamo”** como centro de tortura deberán ser centro del debate al interior de la UNASUR y de CELAC, señala Esquivel, eso sin dejar de lado el potenciamiento de la integración económica a través del **Banco del Sur** anunciado recientemente.

Esquivel, concluye que la **situación de Venezuela y los intereses del capital transnacional** tienen al país suramericano en una situación difícil que ya se ha presentado en otras ocasiones, pero que, también como en el pasado, con las campañas contra Hugo Chávez, el presidente Maduro puede salir fortalecido **si recibe la solidaridad que en muchas ocasiones ha entregado al continente.**
