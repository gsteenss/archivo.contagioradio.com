Title: Salud de Dilan Cruz entra en estado “crítico irreversible”
Date: 2019-11-25 18:40
Author: CtgAdm
Category: Paro Nacional
Tags: Dilan Cruz, ESMAD, Paro Nacional
Slug: salud-de-dilan-cruz-entra-en-estado-critico-irreversible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Dilan-Cruz-publimetro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto Publimetro] 

###### [25 Nov 2019]

El más reciente comunicado del hospital San Ignacio, en el que se encuentra internado el joven Dilan Cruz desde el pasado sábado, cuando fue gravemente herido por parte del ESMAD que accionó un arma en su contra impactándolo en la cabeza, asegura que el joven se encuentra en estado crítico irreversible.

“Informamos que el paciente Dilan Cruz se agravó en las últimas horas y entró en un estado crítico irreversible estamos esperando evolución de curso final” por lo cual se entiende que su estado de salud no mejoraría y por el contrario tiende a empeorar. Esta situación podría generar una nueva ola de indignación y reprobación de la acción del Escuadrón Móvil Antidisturbios.

En desarrollo...
