Title: Así será la Jurisdicción Especial para la Paz
Date: 2015-12-15 12:59
Category: Judicial, Nacional
Tags: Conversaciones de paz de la habana, jurisdicción especial para la paz, MOVICE, Víctimas de colombia
Slug: jurisdiccion-especial-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imágen: contagio radio 

###### [15 Dic 2015]

Desde el pasado 23 de Septiembre se anunció el acuerdo pero solamente hasta la mañana de este martes se conoció el texto completo. Dentro de los cambios sustanciales al texto está el hecho de que los ex presidentes seguirán siendo juzgados por la Comisión de Acusaciones de la Cámara y que los delitos y violaciones a los DDHH por parte de las FFMM se remitirán a un tribunal especial creado por el Estado y con las mismas características de la JEP.

Las dos modificaciones ya son punto de críticas puesto que por una parte, la Comisión de Acusaciones de la Cámara no ha entregado ningún resultado en cuanto a investigaciones contra ex presidentes y por otra parte el tribunal especial para los integrantes de las FFMM podría ser una repetición de la Justicia Penal Militar.

1.  **Creación **

-   El 23 de septiembre de 2015, el Gobierno Nacional acordó crear una Jurisdicción Especial para la Paz que ejercerá funciones judiciales y hará parte del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).

<!-- -->

-   La JEP cumplirá con el deber del Estado colombiano de investigar, esclarecer, perseguir, juzgar y sancionar las graves violaciones a los derechos humanos y las graves infracciones al Derecho Internacional Humanitario (DIH) que tuvieron lugar en el contexto y en razón del conflicto armado.

1.  **Objetivos de la JEP**

-   Satisfacer el derecho de las víctimas a la justicia
-   Ofrecer verdad a la sociedad colombiana
-   Contribuir a la reparación de las víctimas
-   Contribuir a luchar contra la impunidad
-   Adoptar decisiones que otorguen plena seguridad jurídica a quienes participaron de manera directa o indirecta en el conflicto armado, respecto a hechos cometidos en el contexto y en razón de éste.
-   Contribuir al logro de una paz estable y duradera.

**III. Principios básicos **

-   **Centralidad de las víctimas:**Las víctimas tienen derecho a la verdad, la justicia, la reparación y la no repetición. En todas las actuaciones de la JEP se tomarán en cuenta como ejes centrales estos derechos y la gravedad del sufrimiento infligido.

<!-- -->

-   **Seguridad jurídica:**La JEP adoptará decisiones que otorguen plena seguridad jurídica a quienes participaron de manera directa o indirecta en el conflicto armado. Todas las decisiones de la JEP harán tránsito a cosa juzgada cuando estén en firme y se garantizará su inmutabilidad. El Tribunal para la Paz será el órgano de cierre de la JEP. Las decisiones de la JEP sólo podrán ser invalidadas o dejadas sin efecto por el Tribunal para la Paz.

<!-- -->

-   **Condicionalidad:**Para acceder a cualquier tratamiento especial previsto en la JEP es necesario reunir las condiciones que sobre verdad, reparación y no repetición se establezcan en el SIVJRNR. En las resoluciones y sentencias de la JEP se comprobarán los requisitos.

<!-- -->

-   **Derecho a la paz:**La paz es un derecho y el Estado está obligado a alcanzarla. La paz es condición necesaria para el ejercicio y disfrute de todos los demás derechos. Todos los operadores de la JEP deberán interpretar las normas pertinentes y tomar sus decisiones teniendo como principio orientador el derecho a la paz.

<!-- -->

-   **Integralidad:**La JEP hace parte del Sistema Integral, por ello deberá contribuir de manera efectiva, coherente y articulada con las demás medidas a la satisfacción de los derechos de las víctimas, la terminación el conflicto armado y el logro de una paz estable y duradera.

<!-- -->

-   **Inescindibilidad:**La aplicación de la JEP a todos quienes participaron de manera directa e indirecta en el conflicto armado es inescindible y por lo tanto ésta se aplicará de manera simultánea e integral.

<!-- -->

-   **Prevalencia:**La JEP prevalecerá sobre las actuaciones penales, disciplinarias o administrativas por conductas cometidas en el contexto y en razón del conflicto armado, al absorber la competencia exclusiva sobre dichas conductas. Lo anterior no obsta para que la justicia ordinaria continúe investigando, juzgando y sancionado los hechos y conductas que no sean competencia de la JEP.

<!-- -->

-   **Debido proceso:**Todas las actuaciones de la JEP respetarán los derechos fundamentales al debido proceso, defensa,  asistencia  de  abogado,  presunción  de  inocencia  y  la  independencia  e imparcialidad de los funcionarios judiciales. Las personas podrán ejercer su derecho a la defensa ante todos los órganos de la JEP y recurrir las resoluciones y sentencias de las salas y secciones. Todas las decisiones de la JEP serán debidamente motivadas y fundamentadas en pruebas confiables y admisibles ante tribunales de justicia.

<!-- -->

-   **Enfoque diferencial:**La JEP tendrá en cuenta las distintas consecuencias de las violaciones contra mujeres así  como  contra  los  grupos  más  vulnerables,  sujetos  de  especial  protección,  o especialmente afectados por el conflicto, entre ellos los pueblos indígenas, las comunidades afro-­descendientes, los campesinos, los más pobres, las personas en condición de discapacidad, las personas desplazadas y refugiadas, la población LGBTI y los adultos mayores.

<!-- -->

-   **Equidad de género:**Los órganos de las JEP serán conformados con criterios de equidad de género y respeto a la diversidad étnica y cultural. El funcionamiento de la JEP dará énfasis a las necesidades de las víctimas mujeres, niñas y niños.

<!-- -->

-   **Concentración en los casos más graves y representativos:**Todos los órganos de la JEP tendrán las más amplias facultades para organizar sus tareas, integrar comisiones de trabajo, fijar prioridades, acumular casos semejantes y definir la secuencia en que los abordará, así como adoptar criterios de selección y descongestión. Al ejercer estas facultades tendrán en cuenta la necesidad de evitar tanto que las conductas graves y representativas queden impunes, así como prevenir la congestión del Tribunal.

1.  **Competencia **

-   **Competencia temporal:**La JEP tendrá competencia respecto de conductas cometidas en el contexto y en razón del conflicto armado, cometidas con anterioridad a la firma del Acuerdo Final.

<!-- -->

-   **Competencia material:**El Tribunal para la Paz se ocupará de los delitos no amnistiables ni indultables, como los delitos de lesa humanidad, el genocidio, los graves crímenes de guerra, la toma de rehenes u otra privación grave de la libertad, la tortura, las ejecuciones extrajudiciales, la desaparición forzada, el acceso carnal violento y otras formas de violencia sexual, la sustracción de menores, el desplazamiento forzado, además del reclutamiento de menores conforme a lo establecido en el Estatuto de Roma, en especial de los casos más graves y representativos.

<!-- -->

-   **Competencia personal:**La JEP se aplicará, de forma diferenciada, otorgando un tratamiento equitativo, equilibrado, simultáneo y simétrico, a todos quienes habiendo participado de manera directa o indirecta en el conflicto armado, cometieron delitos en el contexto y en razón de éste, siempre que cumplan con las condiciones del SIVJRNR.

Respecto de los combatientes de los grupos armados al margen de la ley \[1\], la JEP solo se aplicará a los grupos que suscriban un acuerdo final de paz con el Gobierno y la participación en el SIVJRNR estará sujeta a la dejación de armas.

Respecto de los agentes del Estado, la JEP se aplicará a quienes hayan cometido delitos en el contexto y en razón del conflicto armado.

Los miembros de los grupos paramilitares que se desmovilizaron vienen rindiendo cuentas ante la justicia en los procesos de Justicia y Paz y en la justicia ordinaria, y en esa medida sus casos no serán competencia de la JEP. Sin embargo, el Gobierno se comprometió a tomar medidas para fortalecer el esclarecimiento del fenómeno en los procesos de Justicia y Paz y de la Ley 1424 de 2010. Serán de competencia de la JEP las conductas de financiación o colaboración con los grupos paramilitares, que no sean resultado de coacciones, respecto de aquellas personas que tuvieron una participación determinante en los crímenes más graves y representativos.

Los terceros que sin formar parte de las organizaciones o grupos armados hayan participado de manera indirecta en el conflicto armado y hayan cometido delitos en el contexto y en razón de éste, podrán acogerse a la JEP y recibir el tratamiento especial que las normas determinen. La JEP sólo podrá obligar a comparecer a aquellas personas frente a quienes existan bases suficientes para entender que la conducta existió y que la persona mencionada en efecto hubiera tenido una participación determinante en la comisión de los crímenes más graves y representativos.

Las personas que sin formar parte de las organizaciones o grupos armados hayan contribuido de manera directa o indirecta a la comisión de delitos en el marco del conflicto podrán acogerse a la JEP y recibir el tratamiento especial que las normas determinen.

1.  **Amnistías e indultos **

La concesión de indultos y amnistías se regirá por las siguientes reglas:

-   De conformidad con el DIH “A la cesación de las hostilidades, las autoridades en el poder procurarán conceder la amnistía más amplia posible”.

<!-- -->

-   La Constitución únicamente permite otorgar amnistías o indultos por el delito político de rebelión y otros delitos conexos con éste.

<!-- -->

-   Una Ley de Amnistía determinará de manera clara los delitos amnistiables e indultables y los criterios de conexidad. En la Ley de Amnistía se determinarán las conductas tipificadas en la legislación nacional que no serán amnistiables.

<!-- -->

-   La conexidad con el delito político comprenderá dos tipos de criterios:

**- Criterios de inclusión:**

  \* Delitos relacionados específicamente con el desarrollo de la rebelión cometidos con ocasión del conflicto armado.

  \* Delitos en los cuales el sujeto pasivo de la conducta es el Estado y su régimen constitucional vigente.

  \* Conductas dirigidas a facilitar, apoyar, financiar u ocultar el desarrollo de la rebelión.

**- Criterios de exclusión: **

  \* No serán objeto de amnistía ni indulto, ni de tratamientos equivalentes, los delitos de lesa humanidad, el genocidio, los graves crímenes de guerra, la toma de rehenes u otra privación grave de la libertad, la tortura, las ejecuciones extrajudiciales, la desaparición forzada, el acceso carnal violento y otras formas de violencia sexual, el desplazamiento forzado, además del reclutamiento de menores conforme a lo establecido en el Estatuto de Roma.

  \* Tampoco son amnistiables o indultables los delitos comunes que carecen de relación con la rebelión.

1.  **Procedimientos **

En la JEP se surtirán dos tipos de procedimientos: (i) Procedimiento en caso de reconocimiento de verdad y responsabilidad; y (ii) Procedimiento en caso de ausencia de reconocimiento de verdad y responsabilidad.

**VII. Órganos **

-   La JEP estará compuesta por cinco órganos y una Secretaría Ejecutiva:

- La Sala de Reconocimiento de Verdad y Responsabilidad y de Determinación de los Hechos y Conductas

- La Sala de Amnistía e Indulto

- La Sala de Definición de Situaciones Jurídicas

- La Unidad de Investigación y Acusación

- El Tribunal para la Paz, que estará compuesto, a su vez, por cinco secciones: (i) Sección de primera instancia en los casos de reconocimiento de responsabilidad; (ii) Sección de primera instancia en los casos de ausencia de reconocimiento de responsabilidad; (iii) Sección de apelación; (iv) Sección de revisión; y (v) Sección de estabilidad y eficacia

o La Secretaría Ejecutiva, que se encargará de la administración, gestión y ejecución de los recursos de la JEP bajo la orientación de la Presidencia de ésta

-   **Composición:**La JEP estará compuesta por Magistrados colombianos, aunque podrá contar con una minoría de magistrados extranjeros. Los Magistrados serán altamente calificados y deberá incluirse expertos en distintas ramas del derecho. Los magistrados serán elegidos mediante un proceso de selección que dé confianza a la sociedad colombiana.

1.  **Sala de Reconocimiento de Verdad y Responsabilidad y de Determinación de los Hechos y Conductas (SRVR) **

La SRVR recibirá toda la información y los reconocimientos de responsabilidad e identificará los casos más graves y representativos, sin que su competencia se limite a ellos. La SRVR tendrá las siguientes funciones:

-   Recibir informes de todas las investigaciones y sentencias de la justicia ordinaria por conductas cometidas en el contexto y en razón del conflicto armado, así como de las organizaciones de víctimas y de derechos humanos colombianas.

<!-- -->

-   Decidir si los hechos y conductas atribuidas a las distintas personas son competencia de la JEP por haber sido cometidos en el contexto y en razón del conflicto armado.

<!-- -->

-   Una vez recibida la información anterior, recibir los reconocimientos de verdad y responsabilidad. Estos podrán hacerse de manera individual o colectiva. En caso de reconocimientos colectivos, una vez las responsabilidades sean individualizadas, cada persona podrá aceptar responsabilidad o manifestar su desacuerdo con dicha individualización.

<!-- -->

-   Contrastar y cotejar los reconocimientos con la información recibida.

<!-- -->

-   Presentar una Resolución de Conclusiones ante el Tribunal con énfasis en la identificación de los casos más graves y representativos, la individualización de las responsabilidades, en particular de quienes tuvieron una participación determinante, los reconocimientos de verdad y responsabilidad, la calificación jurídica, y la identificación de las sanciones correspondientes.

<!-- -->

-   Remitir a la Unidad de Investigación y Acusación los casos en los que no haya reconocimiento de responsabilidad para que ésta, de existir mérito, inicie el procedimiento correspondiente ante el Tribunal.
-   Remitir a la Sala de Amnistía e Indulto el listado de recomendaciones de personas que deberán ser beneficiadas con dichas medidas.

<!-- -->

-   Remitir a la Sala de Definición de las Situaciones Jurídicas los listados de quienes no serán objeto de amnistía e indulto, ni serán incluidos en la Resolución de Conclusiones.

2.  **Sala de Amnistía e Indulto **

De conformidad con lo establecido en la Ley de Amnistía y desde el inicio de los procedimientos, esta Sala tendrá las siguientes funciones:

-   Otorgar amnistía o indulto en casos de personas condenadas o investigadas por delitos amnistiables e indultables, de oficio o a petición de parte y siempre conforme a lo establecido en la Ley de Amnistía.
-   Decidir sobre la procedencia de amnistías e indultos atendiendo las recomendaciones de la SRVR.
-   Interpretar la Ley de Amnistía e indulto y producir doctrina al respecto.
-   En el evento en que la petición verse sobre conductas no indultables o amnistiables, remitir el caso a la SRVR.

3.  **Sala de Definición de Situaciones Jurídicas **

Esta Sala definirá la situación jurídica de quienes no sean objeto de amnistía o indulto, ni hayan sido incluidos en la Resolución de Conclusiones de la SRVR. Para tal efecto esta Sala adoptará las resoluciones necesarias para definir la situación jurídica de estas personas, incluyendo aplicar mecanismos de cesación de procedimientos y de renuncia al ejercicio de la acción judicial.

4.  **Unidad de Investigación y Acusación **

Frente a los casos en los que no haya habido reconocimiento de responsabilidad, esta Unidad tendrá las siguientes funciones:

-   Investigar y de existir mérito, acusar ante el Tribunal para la Paz.
-   Decidir sobre las medidas de protección a víctimas y testigos y la adopción de medidas de aseguramiento y cautelares en caso de ser necesario.
-   Remitir casos a la Sala de Definición de Situaciones Jurídicas o a la Sala de Amnistía e Indulto, cuando considere que no es necesario investigar o acusar.
-   Deberá contar con un equipo de investigación técnico forense y con un equipo de investigación especial para casos de violencia sexual.

5.  **Tribunal para la Paz **

El Tribunal para la Paz será el órgano de cierre de la JEP. El Tribunal tendrá cinco secciones con las siguientes funciones:

**5.1. Sección de primera instancia en los casos de reconocimiento de responsabilidad **

-   Evaluar la correspondencia entre las conductas reconocidas, los responsables de las mismas, las descripciones jurídicas de las conductas y las sanciones propuestas a partir de la Resolución de Conclusiones de la SRVR.
-   Imponer las sanciones correspondientes y fijar las condiciones y modalidades de ejecución de las mismas.
-   Supervisar y certificar el cumplimiento efectivo de sus sentencias con el apoyo de los órganos y mecanismos de monitoreo y verificación del SIVJRNR que se designen para tal efecto, los cuales deberán presentar informes periódicos sobre el cumplimiento.

**5.2. Sección de primera instancia en los casos de ausencia de reconocimiento de responsabilidad **

-   Conocer de las acusaciones de la Unidad de Investigación y Acusación.
-   Someter a juicio contradictorio a quienes no reconozcan responsabilidad y hayan sido incluidos en la Resolución de Conclusiones.
-   Proferir sentencias, y en caso de que sean condenatorias, imponer las sanciones ordinarias o alternativas correspondientes.

**5.3. Sección de apelación **

-   Decidir sobre los recursos de apelación respecto de las decisiones de las Salas y Secciones de la JEP.
-   Decidir sobre los recursos de las víctimas por vulneración de derechos fundamentales, contra las sentencias de las secciones.

**5.4. Sección de revisión **

-   Decidir sobre las sanciones correspondientes de quienes ya hubieren sido condenados por la justicia ordinaria y determinar si se reúnen las condiciones y si ya hubo cumplimiento efectivo de las mismas.
-   Revisar, a petición del condenado y de manera excepcional, las sentencias ordinarias por conductas cometidas en el contexto y en razón del conflicto armado, cuando se alegue inexistencia del hecho o error manifiesto en la calificación jurídica.
-   Revisar las resoluciones o sentencias de la JEP cuando haya mérito para ello.
-   A solicitud de cualquier Sala o Sección, cuando existan dudas, determinar si las conductas relativas a la financiación han sido o no conexas con la rebelión.
-   Decidir sobre las solicitudes de comparecencia de una persona ante la JEP.
-   Resolver los conflictos de competencia entre los órganos de las JEP.

**5.5. Sección de estabilidad y eficacia **

Después de que el Tribunal para la Paz haya concluido sus funciones, se establecerá esta sección con el fin de:

-   Garantizar la estabilidad y eficacia de las resoluciones y sentencias adoptadas por la JEP.

<!-- -->

-   Resolver los casos que surjan con posterioridad a la terminación del cumplimiento de las funciones del Tribunal, por hechos cometidos con anterioridad a la firma del Acuerdo Final.

**VIII. Sanciones **

Las sanciones que imponga el Tribunal para la Paz tendrán como finalidad esencial satisfacer los derechos de las víctimas y consolidar la paz. Las sentencias del Tribunal enunciarán de manera precisa el contenido de la sanción, lugar de ejecución de la misma, así como las condiciones y efectos de éstas. Las sanciones serán de tres tipos:

-   **Sanciones propias: **Se impondrán a quienes reconozcan verdad y responsabilidad ante la SRVR. Tendrán una función restaurativa y reparadora del daño causado, y respecto a determinadas infracciones muy graves tendrán un mínimo de duración de 5 años y un máximo de 8 años. Comprenderán restricciones efectivas de libertades y derechos, tales como la libertad de residencia y movimiento. Para quienes no hayan tenido una participación determinante en los casos más graves y representativos la sanción será de 2 a 5 años.

**Restricción efectiva** significa que haya mecanismos idóneos de monitoreo y supervisión para garantizar el cumplimiento de buena fe de las restricciones ordenadas por el Tribunal, de tal modo que esté en condición de supervisar oportunamente el cumplimiento, y certificar si se cumplió. La JEP determinará las condiciones de restricción efectiva de libertad que sean necesarias para asegurar el cumplimiento de la sanción.

-   **Sanciones alternativas:**Se impondrán a quienes reconozcan verdad y responsabilidad ante la Sección de Primera Instancia, antes de proferir la sentencia. Tendrán una función esencialmente retributiva de pena privativa de la libertad de 5 a 8 años. Para quienes no hayan tenido una participación determinante en los casos más graves y representativos la sanción será de 2 a 5 años.

<!-- -->

-   **Sanciones ordinarias:**Se impondrán a quienes no hayan reconocido responsabilidad y sean condenados por parte del Tribunal. Cumplirán las funciones previstas en las normas penales. En todo caso la privación efectiva de libertad no será inferior a 15 años ni superior a 20 años en el caso de conductas muy graves.

Los lugares donde serán ejecutadas las sanciones estarán sujetos al monitoreo propio del sistema, así como a un régimen de seguridad y vigilancia. Se creará un órgano nacional o internacional que verificará el cumplimiento de las sanciones. En todo caso el Tribunal verificará el cumplimiento de las mismas.

1.  **Extradición **

No se podrá conceder la extradición ni tomar medidas de aseguramiento con fines de extradición respecto de hechos o conductas objeto de la JEP, cometidos durante el conflicto armado y con anterioridad a la firma del Acuerdo Final. Por otra parte, cualquier delito cometido con posterioridad a la firma del Acuerdo Final podrá ser objeto de extradición.

1.  **Participación en política**

La imposición de cualquier sanción por parte de la JEP no inhabilitará para la participación en política ni limitará el ejercicio de ningún derecho (activo o pasivo) de participación política, de conformidad con lo que sea acordado en el Acuerdo Final en desarrollo del Punto 3 -­ “Fin del Conflicto”.
