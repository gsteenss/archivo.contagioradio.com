Title: Concejal de Bogotá propone 40 curules en Congreso para integrantes de las FARC y el ELN
Date: 2015-11-11 16:40
Category: Nacional, Política
Tags: Antonio Sanguino, Concejo de Bogotá, ELN, FARC, Juan Manuel Santos, La Habana, participación política de las FARC, proceso de paz
Slug: concejal-de-bogota-propone-40-curules-en-congreso-para-integrantes-de-farc-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Concejal-Antonio-Sanguino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [antoniosanguino.com]

A través de una carta al presidente Juan Manuel Santos, el **concejal de la Alianza Verde, Antonio Sanguino,** propuso la creación de una circunscripción especial de paz, en el marco de los avances en los diálogos de paz que se desarrollan en la Habana, y la posibilidad de que se haga pública la mesa de conversaciones con el ELN.

Se trata de una iniciativa que busca la implementación de **40 curules para las guerrillas en el Congreso de la República, es decir, 15 en Senado y 25 en la Cámara de Representantes**.

Así mismo, solicitó la creación de **dos nuevos espacios en cada Asamblea Departamental, excluyendo San Andrés y dos curules en los concejos de los 200 municipios** que durante años han tenido presencia guerrillera.

El objetivo de Sanguino, según él, sería facilitar la integración a la vida civil y política de las guerrillas que han evidenciado diversas acciones de compromiso con las negociaciones que buscan dar fin al conflicto armado en Colombia.

De acuerdo con el concejal la duración de estas **circunscripciones debería ser de 12 años, es decir tres periodos.**

\_\_\_\_  
*La Primera Reconciliación es Política  
CARTA ABIERTA*  
Señor Presidente  
Juan Manuel Santos  
*"me parece absolutamente pertinente y necesaria la creación de una circunscripción especial de paz que facilite el tránsito a la vida civil y política de las guerrillas comprometidas en el actual proceso de paz una vez ocurra su desmovilización y sus integrantes hayan pasado por el marco de justicia transicional que se acuerde.*  
*Respaldando la solicitud de las FARC me parece que esta circunscripción podría crear 40 nuevas curules en el Congreso -15 en Senado y 25 en la Cámara de Representantes- y 2 nuevas curules en cada una de las Asambleas Departamentales exceptuando el Departamento de San Andrés y 2 curules en los concejos en los 200 municipios con presencia histórica de las guerrillas. La duración de esta circunscripción para asegurar una transición política exitosa debería ser de tres periodos constitucionales, es decir, 12 años. Estas curules serían entregadas por el Presidente de la República a las personas que así designen las FARC y el ELN".*  
*Cordialmente*  
Antonio Sanguino Páez  
Copresidente Alianza Verde  
Desmovilizado CRS  
Concejal de Bogotá.
