Title: Historia de dos cuarentenas
Date: 2020-04-13 15:00
Author: CtgAdm
Category: Opinion
Tags: Aislamiento, Reclamante de tierras, sucuestro, tortura
Slug: historia-de-dos-cuarentenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/secuestro_referencial.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Archivo virtual*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por: Juan Carlos Posada| Reclamante De Tierras de Cimitarra Santader**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy después de estar un par de semanas en cuarentena obligatoria, veo como personas cercanas y otros no tanto, comienzan a sufrir por múltiples dolencias físicas y mentales; experimentando angustia y desesperación por convivir con personas que aunque sean familiares, en ocasiones resultan ser desconocidos en su propia casa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mirando esto en perspectiva, no puedo evitar revivir lo que me sucedió entre el 14 al 20 de Mayo del 2014, cuando fui obligado a estar con limitación de movimiento por un [grupo armado ilegal](https://archivo.contagioradio.com/control-de-agc-se-consolida-en-cuarentena-en-bajo-cauca-antioqueno/); en aquella ocasión donde también me jugaba la vida estando a su disposición y a la de influencias externas a voluntad de sus estados de ánimo, movidos por la intención de que les entregara las tierras que yo había conseguido con trabajo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fueron 35 días de Secuestro donde cada hora aproximadamente me *echaban* un balde de agua fría para que no pudiera dormir, y donde para tenerme atento me golpeaban con una tabla en la planta de los pies. En una ocasión que todavía recuerdo con dolor trate de reaccionar contra uno de mis custodios y en castigo con una tabla que tenía en la punta una puntilla me la clavó en la cadera y luego en la rodilla derecha, esto y el tener que pedir permiso para poder ir al baño eran las actividades más degradantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jugaron con mi vida, con mi mente, tal vez como muchos sienten que ahora sucede**.En las noches inventaban **diferentes formas de entretenerse muchas de estas implicaban mi sufrimiento que extrañamente les causaba gozo a ellos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como la noche que cada uno apostaba que yo me pegaba un tiro con un revólver que tenía una sola bala y movían el tambor y yo debía disparar, después de llorar y hacerlo 6 veces se burlaron al mostrarme que con lo que estaba cargado el revólver era solo un casquillo ya detonado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así sentía lo frágil y insignificante que era mi existencia y cada vez que paraba el gatillo de ese revólver como simplemente iba a desaparecer de este mundo lleno de apegos no por cosas materiales si no por la vida de mis hijos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Existen varias similitudes con este coyuntural momento para nuestra sociedad; por ejemplo, el no poderte mover bajo la constante amenaza sobre tu vida y, en cómo **en cuestión de un día la vida te cambia por completo para siempre, como para mí ese 14 de Mayo**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy, estando escondido, con la necesidad de proteger mi vida, me veo obligado a estar lejos de personas que quiero como mis hijos, puesto que estar cerca de ellos los pondría en condición de vulnerabilidad o sufrirían cualquier daño colateral como consecuencia del riesgo que actualmente corro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vuelvo a observar a quienes mandan memes, chistes e incluso verdaderos comentarios desesperados, por tener que estar con sus esposas, hijos y demás seres queridos; y aunque no trato de hacer juicios al respecto, si quisiera llamarlos un poco a reflexionar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A muchos los despiertan esta [cuarentena](https://www.justiciaypazcolombia.com/pandemia-y-derechos-humanos-en-las-americas/)por su nombre y no de un golpe; pueden decidir si comer o no hacerlo y no se encuentran a merced de lo que les quieran dar**; cuando solo escuchas tu corazón y el cuerpo quiere colapsar, pero existe algo que te dice *“Adelante, todavía queda espacio para luchar”*, así no tengas idea quienes son los que te rodean incluso si te llevan a límites inhumanos o mortales; cuando no te dejan morir solo porque les eres útil por una firma que les representa salirse con la suya; en ese momento es fácil entender que quedarte en casa con tus seres queridos, lejos de ser un castigo es una enorme bendición, es un regalo que la vida te da.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igual que en ese momento, hoy tengo la convicción de que saldremos adelante, porque justo ahora quedarnos en casa define la vida, no solo la tuya sino la de muchos que te rodean; yo perdí evidentemente todo lo material, pero jamás pudieron arrebatarme el espíritu para luchar un poco más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuanto daría por estar en este momento recogiendo el desorden de mis hijos, tener a quien abrazar en las noches y poder decirle, *“tranquila, todo va a estar bien”*. **Usted en casa que lo tiene, aprovéchelo, agradézcalo; así sabrá al final que todo esto valió la pena.**  

<!-- /wp:paragraph -->
