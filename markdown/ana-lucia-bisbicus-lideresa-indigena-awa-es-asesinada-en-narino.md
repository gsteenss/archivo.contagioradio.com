Title: Ana Lucía Bisbicús, lideresa indígena Awá es asesinada en Nariño
Date: 2020-10-05 11:35
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Ana Lucía Bisbicús, Awá, Comunidad indígena Awá, nariño
Slug: ana-lucia-bisbicus-lideresa-indigena-awa-es-asesinada-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Ana-Lucia-Bisbicus.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este sábado **fue asesinada Ana Lucía Bisbicús García, integrante de la comunidad indígena Awá** del resguardo Pipalta Palvi Yaguapí ubicado en el municipio de Barbacoas, Nariño. (Le puede interesar: [Rodrigo Salazar, reconocido líder del Pueblo Awá fue asesinado en Llorente](https://archivo.contagioradio.com/rodrigo-salazar-reconocido-lider-del-pueblo-awa-fue-asesinado-en-llorente/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El crimen se perpetró el sábado 3 de octubre sobre las 8:30 de la noche, mientras **Ana Lucía asistía a un velorio, cuando fue retirada del lugar por miembros de un grupo armado ilegal, quienes la condujeron detrás de la iglesia y allí la asesinaron con varios disparos de arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ana Lucía tenía 50 años de edad y acompañaba el proceso organizativo de su comunidad y en general del pueblo indígena Awá. (Lea también: [En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, su familia venía siendo blanco de múltiples amenazas y ataques, al punto que **el pasado 6 de mayo había sido también asesinado su hijo Deiro Alexander Pérez  Bisbicús,** lo que consolida una doble victimización de la lideresa indígena y su familia y da cuenta de la sistematicidad con la que opera la violencia en ese territorio. (Le puede interesar: [Pueblo indígena Awá fue víctima de una nueva masacre](https://archivo.contagioradio.com/pueblo-indigena-awa-fue-victima-de-una-nueva-masacre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«*El conflicto armado nos esta exterminando física, cultural, organizativa y espiritualmente, la crisis humanitaria de la cual somos víctimas se incrementa con el pasar de los días, los asesinatos se van convirtiendo en una constante sin que los gobernantes regionales y nacionales tomen acciones concretas para salvaguardar nuestra existencia, omitiendo su responsabilidad constitucional de garantizar la vida y pervivencia del pueblo Awá, lo que ha hecho que los grupos armados ilegales decidan quién vive y quién muere en nuestro territorio ancestral*», expresó la **Unidad Indígena del Pueblo Awá -[UNIPA](https://twitter.com/awaunipa)-** después de documentar el hecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este nuevo crimen se suma al numeroso registro de asesinatos en contra de líderes y lideresas sociales, que según las cifras del Instituto de Estudios para el Desarrollo y la Paz -[Indepaz](http://www.indepaz.org.co/paz-al-liderazgo-social/)- asciende a **223 víctimas fatales, 82 de ellas pertenecientes a comunidades indígenas.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1312920215391436800","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1312920215391436800

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
