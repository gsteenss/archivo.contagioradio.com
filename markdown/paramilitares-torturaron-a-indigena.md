Title: Paramilitares torturaron a indígena en el Bajo Calima
Date: 2017-02-06 17:50
Category: DDHH, Nacional
Tags: Amenazas, Bajo Calima, indígenas, paramilitares, Torturas, víctimas
Slug: paramilitares-torturaron-a-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Nonam.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [6 Ene. 2017] 

Continúa el asedio paramilitar al Resguardo Humanitario y Biodiverso Santa Rosa de Guayacán en el Bajo Calima, luego que este sábado **el líder indígena José Cley Chamapurro de esta comunidad fuera víctima de una retención ilegal por parte de dos armados**, los cuales se presumen son paramilitares, este lunes se ha confirmado la presencia de cinco hombres vestidos de negro, con pasamontañas y armas largas ubicados en el trapiche Comunitario.

El caso de José Cley puso en alerta a su comunidad y organizaciones de derechos humanos, quien luego de **7 horas de estar retenido fue liberado con múltiples golpes en su cuerpo y según testimonios de la comunidad “amenazado de muerte”. **Le puede interesar: [Comunidad despide a Emilsen y Joe, líderes de Buenaventura](https://archivo.contagioradio.com/comunidad-despide-a-emilse-y-joe-34876/)

José Cley a su arribo al Resguardo fue atendido por su comunidad, quien le contó a la organización Comisión Intereclesial de Justicia y Paz que **“se encuentra en estado de shock y en silencio” hecho que ha impactado a los integrantes del lugar. **

Por estos hechos de tortura física y psicológica sufrida por el líder indígena y ante el miedo de cualquier represalia, **la familia de José Cley en la que figura su esposa y dos niñas de 8 y 5 años, tuvieron que desplazarse de manera forzada.**

Cabe recalcar que desde el pasado 17 de enero, la comunidad indígena de este Resguardo Humanitario ha visto varias veces a **hombres desconocidos y armados circulando en su territorio** y por el Rio Calima y San Juan que colinda con sus territorios. Le puede interesar: [Sigue asedio de paramilitares contra comunidad de Paz de San José de Apartadó](https://archivo.contagioradio.com/paramilitares-atacan-a-la-comunidad-de-paz-de-san-jose-de-apartado/)

José, igual que todos los integrantes del **Resguardo Humanitario y Biodiverso Santa Rosa de Guayacán están protegidos con medidas cautelares de la Comisión Interamericana de Derechos Humanos** desde el año 2011, resultado del desplazamiento forzado que fue causado por estructuras paramilitares.

Hasta el momento, asegura la comunidad, no ha habido respuesta de parte de las entidades estatales ante sus solicitudes y temen por la vida de los integrantes de su Resguardo. Le puede interesar: [Paramilitares asesinan a dos pobladores del Salaquí en el Bajo Atrato](https://archivo.contagioradio.com/parmilitares-choco-asesinato/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
