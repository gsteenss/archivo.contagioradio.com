Title: En Ferguson EEUU tanto la policía como el aparato de justicia son racistas
Date: 2015-03-04 18:16
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Justicia de EEUU dice que la policía de Ferguson es racista, Policía de Fergunson acusada de racista, Policía mata a mendigo en Los Angeles, Protesta contra la represión en Los Angeles acaba con dos detenidos
Slug: en-ferguson-eeuu-tanto-la-policia-como-el-aparato-de-justicia-son-racistas
Status: published

###### Foto:Europapress.es 

El Departamento de Justicia de **EE.UU realizó una investigación y concluyó que las autoridades policiales y las cortes judiciales de Ferguson tienen un patrón racista**.

El informe arroja cifras preocupantes con respecto a la **violación de derechos humanos** de la población **afroamericana**. Entre 2012 y 2014 los afroamericanos fueron detenidos el doble que los blancos con motivos referidos a la conducción de vehículos, a pesar de que las detenciones por tenencia de sustancias ilegales en vehículos particulares fueron más en población blanca.

El 68% de jueces suspenden los juicios de afroamericanos en comparación con otros sospechosos. Desde Abril hasta septiembre de 2014 el 95% de las personas retenidas mas de dos días en prisión eran afroamericanos. Por último el informe concluye que **el 88% del uso desmedido y excesivo de la fuerza fue contra negros**.

Organizaciones sociales y defensoras de DDHH han aplaudido el informe que se publicará este miércoles. **Amnistía Internacional** ha elogiado el informe en una nota de prensa publicada hoy, donde además **exhorta** al presidente de la república ha **crear un grupo de trabajo con el objetivo de monitorear toda la justicia nacional** en referencia al **racismo**.

El 9 de Agosto de 2014 **en Ferguson, Michael Brown**, joven de raza negra, fue **asesinado** a manos de un **policía blanco lo que desencadenó fuertes protestas y disturbios de la comunidad afroamericana**, hasta llegar al punto de que el presidente del estado declaró "el toque de queda".

El crimen que continua en la impunidad y responde a un móvil racista provocó la ira de miles de afroamericanos del país e hizo saltar las alarmas con respecto al racismo de carácter institucional y en concreto entre las fuerzas de seguridad y las instancias judiciales.

La presentación del informe transcurre en medio de la polémica provocada por un video donde se puede ver a **policías de la ciudad de Los Ángeles disparan y matan a un mendigo** en una simple pelea. Las reacciones no se han hecho esperar, organizaciones sociales han realizado una protesta contra la represión y el racismo que ha acabado con dos detenidos.

##### Video de la policía disparando y matando a un habitante de calle: 

\[embed\]https://www.youtube.com/watch?v=br3KrpzKH5k\[/embed\]
