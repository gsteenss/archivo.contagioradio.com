Title: Estos son las y los magistrados de la Jurisdicción Especial de Paz
Date: 2017-09-26 08:31
Category: Nacional, Paz
Tags: Comite de Escogencia, JEP, Magistrados JEP
Slug: estos-son-las-y-los-magistrados-de-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/jurisdiccion-especial-de-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  posta.com.mx] 

###### [26 Sept 2017 ] 

El Comité de Escogencia **dio a conocer los nombres de los magistrados seleccionados** que harán parte de la Jurisdicción Especial para la Paz. El 53% de los 51 magistrados escogidos son mujeres y hay una representación significativa de representantes de comunidades indígenas y afrocolombianos.

A continuación los nombre de los seleccionados.  
![Magistrados JEP 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Magistrados-JEP-1.jpeg){.alignnone .size-full .wp-image-47125 width="960" height="1280"}  
![Magistrados JEP 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Magistrados-JEP-2.jpeg){.alignnone .size-full .wp-image-47126 width="960" height="1280"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
