Title: Líderes de movilización afro al norte del Cauca son amenazados por paramilitares
Date: 2016-04-29 12:26
Category: DDHH, Nacional
Tags: afrocolombianos, Cauca, Movilización, paramilitares
Slug: lideres-de-movilizacion-afro-al-norte-del-cauca-son-amenazados-por-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-negras-norte-del-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Los 2 Orillas 

###### [29 Abr 2016] 

Tras la jornada de protestas que adelantan las comunidades negras del norte del Cauca, exigiendo el cumplimiento de acuerdos ya pactos con el gobierno nacional, y luego de la represión por parte del ESMAD a su movilización dejando una bebé de tres semanas herida, ahora **líderes de esta población denuncian que han sido amenazados por estructuras paramilitares acusándolos de “impedir el desarrollo”.**

El Equipo de Derechos Humanos del  Proceso de Comunidades Negras en Colombia PCN, la Asociación de Consejos Comunitarios del Norte del Cauca ACONC y  el Consejo Nacional de Paz Afrocolombiano CONPA, denunciaron que el pasado 27 de abril personas extrañas estuvieron preguntando en la vereda de Yolombó y en el casco urbano del municipio de Suarez, por las lideresas Alexa Leonor Mina y Mery Yein Mina, integrantes de la movilización de mujeres por el Cuidado de la Vida y el Territorio Ancestral.

Así mismo, denuncian que el 28 de abril, **la lideresa Francia Márquez, recibió en su celular un mensaje amenazante donde la declaran objetivo militar** junto a otros voceros de la comunidad negra del Norte del Cauca.

“Es muy preocupante que no habían pasado ni 12 horas de reunión con viceministros, cuando me envían esa **amenaza de grupos paramilitares**… En Suárez fueron a preguntar por dos compañeras unos hombres con el rostro tapado, con guantes y en una moto sin placa”, asegura Márquez, quien añade que pese a las solicitudes a la Unidad Nacional de Protección, esta no ha dado seguridad colectiva a las comunidades negras amenazadas por exigir que las multinacionales salgan de sus territorios.

Luego de una reunión con algunos funcionarios del gobierno nacional, se pactó revisar los acuerdos en la primera semana del mes de mayo, para después empezar a trabajar en una ruta de implementación de los mismos. Sin embargo, según cuenta la lideresa afrocolombiana, **los delegados del gobierno afirmaron que no todos los acuerdos se podrán cumplir,** por lo que Márquez cree que al no asumirse los compromisos, la Procuraduría debería entrar a investigar y sancionar a los ministros que no le cumplieron a las comunidades.

“Nosotros vamos a seguir exigiendo garantía de nuestros derechos para permanecer en nuestros territorios, no nos los estamos inventando, son derechos constitucionalmente e internacionalmente establecidos” sostiene Francia Márquez.

<iframe src="http://co.ivoox.com/es/player_ej_11355741_2_1.html?data=kpagl5qbeJKhhpywj5WbaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPQy8aPkYa3lIquk9fVucbujJKYroqnd4a1pcnS1MrXpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
