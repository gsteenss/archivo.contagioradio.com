Title: Cierre de cárcel la Tramacua de Valledupar sería un “acto de humanidad”
Date: 2016-01-20 15:47
Category: DDHH, Entrevistas
Tags: conversaciones de paz, FCSPP, prisioneros, tramacua de valledupar
Slug: cierre-de-carcel-la-tramacua-de-valledupar-seria-un-acto-de-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/tramacua1_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: derechodedelpueblo.blogspot 

<iframe src="http://www.ivoox.com/player_ek_10153499_2_1.html?data=kpWel5iYfZqhhpywj5WXaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTdxtffx5DIqYzXhqigh6aVtsTZzZDZw5C4tsLhwsjiw5DIqYzKwtHZx8nZtMLmjNjS1Iqnd4a1pcaY19OPaaamjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [20 Ene 2016]

Internos de la cárcel la Tramacua en Valledupar, denunciaron que no fueron consultados en el marco de la visita de verificación que se realizó para cumplir la [sentencia de la Corte Constitucional, que ordenaba la revisión de las condiciones de vida al interior del penal](https://archivo.contagioradio.com/?s=tramacua). Para los detenidos este fue **otro acto de agravio puesto que la situación es de extrema gravedad y son ellos quienes las enfrentan a diario.**

Según el comunicado público difundido a través de Contagio Radio, las personas detenidas en ese penal deberían haber tenido la oportunidad de hablar y plantear la situación que afrontan y de primera mano la **falta de agua potable, los tratos crueles y degradantes, la ausencia de programas de resocialización, el hacinamiento entre otras**.

Los detenidos hicieron un llamado a la sensatez al Tribunal Administrativo y reafirmaron que la situación es impostergable y que no se puede tomar la decisión de no cerrar la cárcel basándose en promesas y en trabajos a marcha forzada. “Se ha cumplido el plazo y en plata blanca no se cumplió lo ordenado” y agregan que “**de buenas intenciones está empedrado el camino del infierno**” en referencia a las promesas por parte de las directivas del penal.

Denunciaron también que el **director del establecimiento, el Capitán Luis Francisco Perdomo, realizó un “show mediático”** para mostrar avances en su gestión y afirmaron que en esa carrera el pasado 17 de Enero se detuvo **a 24 familiares, se les condujo a instalaciones judiciales y luego se le realizó un registro fotográfico a sus hogares**, violando el derecho a la intimidad y sometiendo a los familiares al “escarnio” público.

Para ellos el cierre de la cárcel y la posibilidad del cumplimiento de sus penas de una manera alternativa sería un acto de humanidad y de respeto mínimo a los Derechos Humanos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
