Title: Águilas Negras amenazan a mesas locales de víctimas y defensores de derechos humanos
Date: 2019-01-26 17:14
Author: AdminContagio
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas, Iván Cepeda, Panfleto
Slug: aguilas-negras-amenazan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/10444668_898617840176342_2306331343939005187_n-770x400.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Paramilitares....png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Aguilas-Negras.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Aguilas-Negras.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Aguilas-Negras.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [26 Ene 2019] 

Mediante un panfleto, el grupo autodenominado Águilas Negras amenazó a 30 organizaciones defensoras de derechos humanos, mesas locales de víctimas, y congresistas; afirmando que contra ellos y sus familias se haría un exterminio para evitar que la izquierda se tome el país. (Le puede interesar: ["El 84% del territorio colombiano  registra violencia contra líderes sociales"](https://archivo.contagioradio.com/autoria-del-45-de-asesinatos-contra-lideres-sociales-permanece-en-el-anonimato/))

En la amenaza, **se acusa a la Mesa Nacional de Víctimas, la Mesa Distrital de Víctimas y la organización Redepaz como instituciones que apoyan a la guerrilla y hacen contrainteligencia en las ciudades.** El escrito sigue el mismo patrón de amenazas que emplean otras estructuras como las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), con el que se acusa de guerrillero a  todo aquel que defiende los derechos humanos. (Le puede interesar:["Informe de Fiscalía sobre líderes sociales asesinados por el Estado es irrisorio"](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-lideres-sociales-asesinados-por-el-estado-es-irrisorio/))

### **Águilas Negras: Un nombre que se usa para amenazar líderes** 

En su último informe, el Instituto de Estudios Para el Desarrollo y la Paz (INDEPAZ) sostiene que las Águilas Negras "no aparecen en la geografía del país como un grupo armado que tenga estructuras permanentes y jefes conocidos: es un nombre o razón social utilizada por varios núcleos que tienen la experiencia de amenazar de muerte a organizaciones y a líderes o líderes sociales, y de crear terror con fines políticos de ataque, con lenguajes de ultraderecha".

Algunos políticos y analistas han señalado que las Águilas Negras es un nombre usado por agentes del Estado para amenazar, de forma oculta, a líderes y organizaciones que trabajan por los derechos humanos. (Le puede interesar:["18 grupos Narcoparamilitares operan en Colombia"](https://archivo.contagioradio.com/18-grupos-narcoparamilitares/))

\[caption id="attachment\_60636" align="alignnone" width="629"\]![Panfleto Águilas Negras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-26-at-8.17.28-AM-450x800.jpeg){.wp-image-60636 width="629" height="1118"} Panfleto Águilas Negras\[/caption\]

<div class="ff10">

</div>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
