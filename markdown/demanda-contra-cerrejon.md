Title: Exigen a Carbones del Cerrejón no desinformar sobre demanda en su contra
Date: 2019-08-26 18:58
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Cerrejón, comunidades de la Guajira, Contaminación ambiental por cuenta de la minería, Impacto ambiental, Iván Cepeda
Slug: demanda-contra-cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: laurismu] 

Comunidades afectadas por la explotación minera, organizaciones defensoras de derechos humanos y representantes políticos, se dieron cita este 26 de mayo para clarificar los puntos que presentaron el pasado 6 de agosto en la demanda, que ya fue admitida en el Consejo de Estado, solicitando la nulidad de la licencia ambiental que le permite a la empresa de Carbones de el Cerrejón operar en La Guajira, y así mismo desmentir las versiones que han surgido frente a las repercusiones ambientales, económicas y sociales que tendría esta decisión.

### **La demanda punto por punto** 

Una de las razones que llevó a los solicitantes a  presentar de manera pública los puntos demandados fue detener las diferentes hipótesis que se generaron alrededor de esta, desmintiendo los rumores sobre el cierre inmediato y total de la mina y así mismo el despido masivo de trabajadores.

“Pedimos a la empresa minera del Cerrejón que no desinforme a la opinión pública, que no genere un pánico económico con relación a esta acción que tiene toda la justificación y todos los argumentos para abrirse paso” expresó Ivan Cepeda, senador y demandante. (Le puede interesar: [Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/))

La demanda, que solicitó la suspensión de operaciones en las comunidades aledañas mientras se investiga sobre el cumplimiento de los requisitos ambientales que debe tener El Cerrejón, también hace alusión a las comunidades que viven en esta zona, y a **“la muerte de más de 4.900 niños en La Guajira como consecuencia de los actos mineros que hemos dejado avanzar año tras año en esta zona”**, afirma Armando Valbuena, miembro de la Organización Nacional Indígena de Colombia (ONIC).

La acción jurídica finalmente se refiere al derecho que tienen las comunidades a denunciar los daños ambientales, territoriales en los trabajadores y exige respetar la independencia de la justicia para que pueda actuar sin presiones ni interferencias y así el Consejo de Estado pueda hacer una correcta pronunciación sobre este caso ambiental.

### **Un alto al Cerrejón por impacto ambiental** 

Diego Cardona, coordinador general de CENSAT “Agua Viva”, se pronunció ante las cifras presentadas en la demanda sobre las afectaciones  a los derechos humanos en La Guajira señalando puntos como  la desnutrición que han sufrido 40.000 menores de edad, el desvíos de ríos que han causado  graves sequías  en un departamento que  como el señala "no es solamente desértico, cuentan con  parte de Sierra Nevada y  el único bosque de niebla en zona desértica en el mundo, este ecosistema en sí mismo es una fuente de vida". (Le puede interesar [El drama de la salud de las comunidades de La Guajira por la minería de El Cerrejón](https://archivo.contagioradio.com/comunidades-de-la-guajira-exige/))

Al mismo tiempo cuestionó los comentarios entorno al impacto económico por el cierre de El Cerrejón, que apuntan a que la clausura de este causaría crisis económica en todo el territorio, "si ha generado tantas ganancias a esa zona , ¿por qué la Guajira sigue siendo un  lugar con los índices de pobreza más altos de Colombia”, objetó. (Le puede interesar:[Con el Arroyo Bruno serían 27 las fuentes de agua que El Cerrejón ha secado en La Guajira](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/))

Los demandantes, quienes continúan a la espera sobre el proceso y las medidas que tomará el Consejo de Estado frente a este caso, concuerdan en su solicitud de detener los daños causados por la minería en La Guajira,  proteger la integridad de los residentes de la zona y  regular las normas ambientales que rigen al Cerrejón.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
