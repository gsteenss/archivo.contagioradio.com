Title: Se reabre caso por asesinato de sindicalista que trabajaba en Cocacola
Date: 2015-09-11 14:03
Category: DDHH, Nacional
Tags: abogado, Adolfo Charris, Adolfo Munera, asesinatos contra sindicalistas, AUC, carlos castaño, Comité de Solidaridad con los Presos Políticos, Fiscalía, Franklin Castañeda, Naciones Unidas, paramilitares, Sinaltrinal
Slug: se-reabre-caso-del-asesinato-de-sindicalista-afiliado-a-cocacola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/sincalista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [killercoke.org]

<iframe src="http://www.ivoox.com/player_ek_8321516_2_1.html?data=mZifk5qVeo6ZmKiakp2Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8af08rOxNfJb8TV1NSY0tTWb8LnxtjW0MbYs4zYxtGY1c7SqMrXwtHW1dnFb8XZjKjcxcbHs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Franklin Castañeda, Comité de Solidaridad con los Presos Políticos] 

###### [11 Sept 2015]

El pasado 9 de Septiembre se presentó una demanda ante El Comité de Derechos Humanos y el  Pacto Internacional de Derechos  Civiles y  Políticos de las Naciones Unidas con el fin de **reabrir el proceso de investigación por el asesinato del sindicalista Adolfo Munera,** quien trabajó durante años defendiendo los derechos de los trabajadores de Coca-Cola S.A  en la Costa Caribe colombiana.

La demanda fue presentada por la familia del líder sindicalista, el Sindicato Sinantrainal, el Comité de Solidaridad con los Presos Políticos y el Centro de Europa CETIM, con el objetivo de buscar la reparación, verdad y justicia frente al crimen contra Adolfo Múnera.

El sindicalista barranquillero, se destacó porque **en 1995 lideró una huelga  histórica logrando el cierre de las plantas de Coca-Cola en la costa caribe**, ocasionando una pérdida de 5000 mil millones de pesos  a la empresa.

Debido a ese tipo de acciones, Adolfo empezó a ser perseguido. **En 1997, su casa fue allanada y fue tildado públicamente de pertenecer a la insurgencia**. Tras esa situación, Munera decide enfrentar su proceso a la distancia durante dos años, ya que el fiscal no le permite presentar su versión libre de los hechos, sin embargo logra demostrar su inocencia. Durante ese tiempo su vida estuvo constantemente amenazada.

De acuerdo con Franklin Castañeda, abogado del Comité de Solidaridad con los Presos Políticos, **en 1998 un gerente de Coca-Cola se reúne con Carlos Castaño jefe de las AUC,** y desde ese momento hasta el 2003 nueve sindicalistas de Sinaltrainal afiliados a Coca-Cola fueron asesinados y decenas fueron amenazados.

Tras años de pedir protección a la Fiscalía, **Múnera fue asesinado el 31 de Agosto del 2002 a manos de Adolfo Charris Castillo quien le dispara ocasionándole la muerte. ** Charris ya había sido investigado por homicidios contra líderes sociales y sindicalistas.

“Lo que se quiere es investigar y juzgar a las personas que hicieron parte de este crimen” expresa Castañeda. Actualmente la Fiscalía  tiene a su cargo 80 investigaciones por amenazas, 41 por homicidio, una por tentativa de homicidio, una por desaparición forzada, una por tentativa de secuestro y una por secuestro simple contra sindicalistas o sus familiares. **Todas están en etapa preliminar.**
