Title: La Copa Presidente y el Tazón de la Inseguridad
Date: 2015-02-09 21:59
Author: CtgAdm
Category: Opinion
Tags: honduras, Partido Nacional de Honduras
Slug: la-copa-presidente-y-el-tazon-de-la-inseguridad
Status: published

#### **Gilberto Ríos Munguía** 

La reciente derrota del presidente Juan Orlando Hernández y del Partido Nacional de Honduras (Extrema Derecha), en la votación para darle rango constitucional a la Policía Militar del Orden Público (PMOP), ha generado desesperación en la élite de poder que respalda el continuismo del gobierno actual.

La PMOP con rango constitucional respondería directamente a las órdenes del poder ejecutivo, pero estas mismas se encargarían en todo el país del traslado y manipuleo de las urnas electorales en las próximas elecciones. La proyección maquiavélica y descarada de las pretensiones continuistas de Juan Orlando en la Presidencia de la República, hizo que cometiera un terrible error al no medir correctamente sus fuerzas en la votación legislativa y tampoco en calcular la consecuente   desmoralización de sus bases.

Repentinamente apareció en la nueva tanda de la campaña presidencial que ha mantenido el Poder Ejecutivo desde el primer día de gobierno, la Copa Presidente, un torneo de fútbol que patrocina la presidencia de la república para continuar exponiendo al perenne candidato. No obstante éste ha tenido también la precaución de no aparecerse por los estadios donde este fin de semana pasado comenzó el torneo, temiendo ser abucheado olímpicamente, como le ocurrió hace un par de semanas al alcalde de su mismo partido en el estadio de la segunda ciudad mas importante de país.

Mientras la Copa futbolística estaba en vísperas de su inauguración, fue asesinado violentamente el  presidente de uno de los clubes deportivos más conocidos de Honduras.  El fútbol de luto a pesar de la existencia hace más de cinco años de un impuesto adicional creado para prevenir el crimen, conocido como el Tazón de Seguridad, mismo que a la fecha habría recaudado más de 150 millones de dólares, sin que la ciudadanía haya tenido acceso jamás a rendimiento de cuentas de ese presupuesto.

El presidente Juan Orlando intenta volver con la PMOP haciendo gala de la pericia táctica de sus elementos en las antesalas de los partidos de su Copa, mientras la sociedad no se engaña con el circo, porque aún con todos los maquillajes, lo que acumula la patria es un enorme tazón de inseguridad.
