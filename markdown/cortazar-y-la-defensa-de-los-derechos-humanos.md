Title: Cortázar y la defensa de los Derechos Humanos
Date: 2016-08-26 13:38
Category: Cultura, Nacional
Tags: Derechos Humanos, Dictadura argentina, Julio Cortázar
Slug: cortazar-y-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Cortazar-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Julio Cortázar, nacido un 26 de agosto de 1914, es uno de los escritores latinoamericanos que alzó hasta su último aliento la bandera de la lucha por los Derechos Humanos; posición que tuvo incidencia en la producción de su obra literaria y su visión política del mundo.

A principio de los años 60, el triunfo de la **revolución cubana**, llevó a que Cortázar se cuestionara sobre su papel como actor político. En *La fascinación de las palabras*, el escritor asegura que estos sucesos le mostraron "de una manera cruel y que me dolió mucho, el gran vacío político que había en mí, mi inutilidad política…" lo que representó que estos temas se incorporaran a su trabajo literario.

**Activismo por Latinoamérica.**

De ahí en adelante, su interés por lo que ocurría en la política del continente durante los años en que se constituyeron las **dictaduras civico-militares**, lo llevaron a tomar decisiones personales como la de donar los derechos de autor de varias de sus obras en ayuda a los presos políticos de varios países incluyendo la Argentina.

En 1970, viaja a Chile en solidaridad con el gobierno de **Salvador Allende** y al año siguiente, junto a Simone de Beauvoir, Jean-Paul Sartre, se opuso a la persecución y arresto del autor **Heberto Padilla**, desilusionado con la actitud del proceso cubano, un sentimiento ambivalente que plasmaría en su poema "*Policrítica en la hora de los chacales*".

En 1974 recibió el premio Médicis étranger por su Libro de Manuel, donando el dinero recibido al **Frente Unificado de la resistencia chilena**. Durante ese mismo año, Cortázar participó en el Tribunal Russell II, que en Roma se reunión para examinar las violaciones a  los Derechos Humanos ocurridos en el marco de las políticas Latinoamericana; a partir de esta experiencia se creo el comic "*Fantomas contra los vampiros multinacionales*".

Durante ese mismo año, se unió a otros escritores como Borges, Bioy Casares y Octavio Paz, para exigir la **liberación del escritor uruguayo Juan Carlos Onetti**, detenido por las fuerzas militares bajo la acusación de pornógrafo, al deliberar como jurado en favor del cuento "*El guardaespaldas de Nelson Marra*", un proceso que lo llevaría a exiliarse en España y le traería serias implicaciones psicológicas.

Luego del triunfo de la **revolución sandinista** en Nicaragua, Cortázar llega clandestinamente a la localidad de Solentiname, en el primero de varios viajes que realiza a ese país, lo que le permitió seguir de cerca el proceso y la realidad local y regional, plasmadas en una serie de textos compilados en el libro "*Nicaragua, tan violentamente dulce*".

**La dictadura Argentina.**

Con la publicación en 1977 de sus cuentos: “*Segunda vez*” y “*Apocalipsis de Solentiname*”, Cortázar **provocó la cólera de la Junta militar argentina**, al denunciar la realidad histórico-política latinoamericana de ese momento. El primero describe, sin establecer responsables explícitos, la desaparición de personas en circunstancias misteriosas y el segundo refiere las situaciones de violencia padecidas por muchos intelectuales latinoamericanos durante este régimen.

Aunque su posición contra la dictadura en su país le llevó a optar en 1981 por la nacionalidad francesa, Cortázar no piensa en convertirse en un escritor abiertamente político, buscando mantener un contacto diferente con sus lectores: “si en otro tiempo la literatura representaba de algún modo unas vacaciones que el lector se concedía en su cotidianeidad real, hoy en día en América Latina es una manera directa de explorar lo que nos ocurre e interrogarnos sobre las causas por las cuales nos ocurre"; determinación que se ve reflejada en muchos de sus cuentos, como “*Reunión*” publicado en Todos los fuegos el fuego (1966), “*Sobremesa*” en Ultimo Round (1969), “*Graffiti*” publicado en Queremos tanto a Glenda (1981), “*Satarsa*” en Deshoras (1982).

Estas insinuaciones, llevaron a que el autor de "Rayuela", apareciera en **las listas negras de los militares,** con el legajo n.º 3178 con una ficha que contenía seis datos: apellido (Cortázar), nombre (Julio Florencio, el segundo escrito a mano alzada), nación (Arg. Francia), localidad, profesión (escritor) y antecedentes sociales o entidad: "Habeas", junto a la ficha de **otras 217.000 personas investigadas** por la Dirección de Inteligencia de la Policía de la Provincia de Buenos Aires.

3 años antes de su muerte en París, el **24 de marzo de 1981**, en un acto de repudio a la dictadura genocida argentina que tuvo lugar en un salón de Madrid, colmado de exiliados y familiares de desaparecidos, Cortázar dedicó un largo discurso a ellas, a la palabra Justicia, Democracia, Pueblo, Libertad, esencias del ser humano minuciosamente transformadas por el terrorismo de Estado y sus aparatos civiles.

Cortázar volvería por última vez a la Argentina en 1983, ya con la democracia restituida, recibiendo allí el abrazo de multitudes de admiradores y la indiferencia del recién instaurado gobierno de Alfonsín.

 
