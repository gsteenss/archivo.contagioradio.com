Title: Gobierno buscará ganar batalla por las objeciones a la JEP en el Senado
Date: 2019-04-09 20:26
Author: CtgAdm
Category: Paz, Política
Tags: Cámara de Representantes, Congreso, Gobierno, Objeciones
Slug: gobierno-batalla-objeciones-jep-senado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Iván-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Letra Directa] 

Con 110 votos a favor y 44 en contra este martes en horas de la noche la Cámara de Representantes rechazó las objeciones a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP); de esta forma la oposición logró al menos dos triunfos para la defensa de la paz: lograr un numeroso bloque de congresistas que apoyan la implementación de los acuerdos, y proteger la Ley Estatutaria de la JEP.

La discusión en plenaria de la Cámara de Representantes inició sobre  las 4 de la tarde, luego de decidir sobre los impedimentos y proposiciones presentadas, los representantes debatieron por cerca de 2 horas sobre la ponencia mayoritaria, cuya conclusión fue que debían negarse las objeciones. Pasadas las 7 de la noche la mayoría de la Corporación decidió que era momento de someter a votación la ponencia, que finalmente fue aprobada por 110 congresistas.

> Perdió [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) y perdieron todos los Representantes que siguen estancados en el pasado. [pic.twitter.com/73bOx2LbGG](https://t.co/73bOx2LbGG)
>
> — David Racero ?? (@DavidRacero) [9 de abril de 2019](https://twitter.com/DavidRacero/status/1115423886282248192?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
David Racero, representante a la cámara por Bogotá,  aseguró que durante el debate también estuvieron presentes los Ministros y el consejero para la política, Jaime Amin, intentando persuadir a los congresistas para que votaran negativamente la ponencia, por lo que concluyó que aún habrá mucha presión sobre el Congreso para que tome decisión sobre las objeciones y que pese a la importancia del paso dado ayer, falta la determinación que tome el Senado.

</p>
### **"Es una victoria y tenemos que celebrarlo": Racero** 

Racero indicó que tras conocer que la discusión sobre las objeciones se llevaría a cabo en el Congreso, la oposición puso en marcha una estrategia jurídica y política que el martes dio sus frutos, y logró fortalecer una gran coalición por la paz. Eso significó que partidos declarados como independientes (Liberales, Cambio Radical y de la U) votaran favorablemente la Ponencia en conjunto con los de oposición.

En ese sentido, según Racero el mensaje que dejó la oposición durante la plenaria y tras la decisión de la cámara tiene dos sentidos: tranquilidad para quienes apoyan la implementación del Acuerdo de Paz; y de unidad para quienes votaron por Duque y aun son reacios al proceso, porque en su criterio la oposición no quiere la impunidad, quiere la paz y la unidad, pero no sobre las objeciones. (Le puede interesar: ["Ganaremos quienes rechazamos las objeciones de Duque a la JEP: Angela María Robledo"](https://archivo.contagioradio.com/rechazamos-objeciones-angela-robledo/))

De acuerdo al Representante, además del mensaje de unidad en torno a la defensa por la paz, la segunda gran victoria tras la decisión de la Cámara sería la seguridad de que la Ley Estatutaria de la JEP quedaría como fue aprobada por la Corte Constitucional. Como explicó Racero, si el Senado decidiera rechazar también las objeciones, la Estatutaria tendría que pasar a sanción presidencial, y si el presidente Duque se negara a firmar la Ley, tendría que ser promulgada por el presidente del Senado, Ernesto Macias. (Le puede interesar: ["¿Qué hay detrás de las objeciones de Duque a la JEP?"](https://archivo.contagioradio.com/que-hay-detras-de-las-objeciones-de-duque-a-la-jep/))

En correspondencia con la comunicación de la Corte Constitucional sobre el tema, en caso de que ambas corporaciones aceptaran las objeciones e hicieran modificaciones a las mismas, el resultado sería objeto de revisión constitucional por parte de la Corte, y si no se lograra un acuerdo entre Senado y Cámara, de igual forma el alto tribunal tomaría decisiones. Teniendo en cuenta que la Corte fijó su posición sobre la Ley, y como lo menciona Racero, las objeciones no fueron por inconveniencias políticas, sociales o económicas sino jurídicas, es probable que la Ley fuera sancionada sin modificaciones.

### **La discusión en el Senado podría darse el jueves de esta semana, si Macias quiere** 

Antes de iniciar la discusión en Cámara de Representantes, Ernesto Macias dijo que habría vicios de fondo en el evento pues debería ser primero el Senado la corporación que asuma la discusión. Sobre esa postura, Racero sostuvo que se trató de un nuevo intento para dilatar la discusión sobre las objeciones, y argumentó que teniendo en cuenta que la Ley Estatutaria se enmarca en el proceso del 'Fast Track', el Presidente del Senado tiene que ponerla, "por obligación", con prelacion sobre los demás proyectos.

Si el procedimiento se siguiera como lo sugiere Racero, se podría realizar el debate en el Senado sobre las objeciones el jueves, teniendo en cuenta que dos senadores que integran la Comisión accidental de esta corporación creada para estudiar el tema radicaron la ponencia negativa como se hizo en la cámara. Sin embargo, Macias ha informado que esperan abrir la discusión solamente hasta después de semana santa.

<iframe id="audio_34270632" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34270632_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
