Title: Organizaciones Sociales exigen justicia para todos en implementación de JEP
Date: 2017-01-24 17:03
Category: Nacional, Paz
Tags: Implementación de los Acuerdos de paz, jurisdicción especial para la paz
Slug: organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [24 Ene 2017] 

Organizaciones sociales y defensoras de derechos humanos hicieron un llamado de atención al Congreso por discutir la incorporación constitucional y reglamentación del Acuerdo de La Habana sin contar con la participación de las víctimas o **pasando por alto puntos como las garantías de no repetición y la diferenciación de responsabilidades entre actores**. De igual modo, advirtieron que principios propios del proceso de paz, como la bilateralidad o autenticidad, que orientan el debate legislativo, fueron alterados otorgando beneficios a los agentes estatales.

### **Participación de las víctimas ** 

Durante la  sesión de la Comisión Primera que discutió el Acto Legislativo 02 que regula “el Sistema integral de justicia, verdad, reparación y garantías de no repetición” y la “Responsabilidad de agentes del Estado” **no se tuvo en cuenta a las víctimas o a las organizaciones que las representan**, en la agenda del primer día, pese a que las víctimas y su participación se estableció como un acto imprescindible para la construcción de una paz estable y duradera.

Acto que para las organizaciones defensoras de derechos humanos “**cierra la puerta para que las víctimas realicen aportes a los mecanismos de implementación** que puedan reforzar su protección y garantizar su participación dentro de los procedimientos del Sistema Integral y demás puntos acordados” Le puede interesar: ["Jurisdicción Especial para la Paz debe ser fiel a víctimas del conflicto armado: Iván Cepeda"](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-debe-ser-fiel-a-las-victimas-del-conflicto-armado-ivan-cepeda/)

### **Responsabilidad de Agentes Estatales** 

De igual forma las organizaciones alertan sobre un **conjunto de acciones que podrían estar modificando el tratamiento que se les daría a los agentes estatales**, ya que este se origina de una iniciativa del gobierno y no de lo pactado en los diálogos de La Habana, rompiendo el principio de bilateralidad y dejando de lado los aportes hechos por líderes, políticos, víctimas y la sociedad civil. Además en el caso de miembros de la Fuerza Pública se aplicará el Código Penal Colombiano y el Derecho Internacional Humanitario como ley especial, **descartando el marco de aplicación del derecho internacional de los derechos humanos**

A su vez, frente al tema de la responsabilidad por parte de agentes estatales las organizaciones indican que en la **redacción se desconoce la responsabilidad del superior o cadena de mando**, que podría tener implicaciones en la investigación y judicialización del conjunto de crímenes de lesa humanidad y crímenes de guerra. Igualmente evidencian que en caso de responsables no militares se omite la mención del tratamiento a recibir. Le puede interesar: ["Ley de Amnistía, un mecanismo para afrontar la impunidad del pasado"](https://archivo.contagioradio.com/ley-amnistia-mecanismo-afrontar-la-impunidad-del-pasado/)

En ese mismo orden las organizaciones defensoras de derechos humanos, evidencias que al elevar el rango a constitucional de las operaciones de la **Fuerza Pública podrían generarse implicaciones contrarias a los derechos de las víctimas** ya que podrían darse tergiversaciones y permitir que se invoquen figuras como el **“blanco legítimo”  o  “daño colateral”.**

Por otra parte, el principio de autenticidad se refiere a que la reglamentación debe estar acorde con lo pactado, hecho que se rompe con la introducción de medidas que no estaban en los acuerdos como la exclusión de acción y llamamiento en garantía para miembros de la Fuerza Pública, asimismo permite que agentes que hayan cometido delitos y estén en la cárcel puedan computar ese tiempo para su pensión, **el levantamiento de la prohibición de que personas condenadas sean reintegradas a la fuerza pública; la eliminación de los antecedentes penales, fiscales y disciplinarios.**

### **Amenazas a defensores de Derechos Humanos** 

Finalmente, las diferentes organizaciones alertaron sobre las graves amenazas que continúan enfrentando defensores de derechos humanos en todo el territorio “y que atenta contra los anhelos de paz del pueblo colombiano” por lo cual las organizaciones sociales **solicitan la pronta activación de la “unidad de investigación y desmantelamiento de las organizaciones criminales”** y las garantías para que las víctimas sean tenidas en cuenta en los diferentes debates de los actos legislativos de los Acuerdos de La Habana. Le puede interesar:["117 líderes fueron asesinados durante el 2016"](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

###### Reciba toda la información de Contagio Radio en [[su correo]
