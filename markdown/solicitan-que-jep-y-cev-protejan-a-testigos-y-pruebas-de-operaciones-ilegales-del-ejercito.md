Title: Solicitan que JEP y CEV protejan a testigos y pruebas de operaciones ilegales del ejército
Date: 2020-05-11 21:00
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: comision de la verdad, Ejército Nacional, interceptaciones ilegales, JEP
Slug: solicitan-que-jep-y-cev-protejan-a-testigos-y-pruebas-de-operaciones-ilegales-del-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Lideresas de Comunidades / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un grupo de más de 100 comunidades de todo el territorio nacional, emitieron una carta en la que piden que, tanto testigos, como pruebas de las operaciones de inteligencia militar sen protegidos por los organismos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, dado que dicha protección sería una garantía para que haya justicia y verdad en estos casos que han ocurrido en el país desde hace más de 20 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades, en su comunicación está solicitando el apoyo con el envío de cartas a la Jurisdicción Especial para la Paz y a la Comisión de la Verdad para que sean ellas las encargadas de hacer prevalecer el derecho a la verdad y a la justicia ante operaciones de inteligencia militar que también tienen como blanco a lideresas sociales como Luz Marina Cuchumbe y Jani Silva.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que desde finales de marzo, la [Comisión de Justicia Pa](https://twitter.com/Justiciaypazcol)z, organización de DD.HH. que apoya a varias comunidades y organizaciones en el territorio nacional, ha alertado que varios testigos señalan la existencia de un plan para atentar contra Jani Silva, promotora de procesos de sustitución voluntaria en Putumayo. [(Lea también: Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida)](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, otros testigos cuya identidad se reservada la organización de DDHH, expresaron que este no es el único caso de seguimientos y que otros líderes de procesos que acompaña la Comisión de Justicia y Paz, así como algunos de sus integrantes también son víctimas de espionaje. [(Lea también: Seguimientos contra periodistas son propios de regímenes totalitarios: FLIP)](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### JEP tiene las facultades para proteger testigos y pruebas de las operaciones ilegales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por tal motivo, comunidades y procesos sociales del país han solicitado a la Jurisdicción Especial de Paz, que tome todas las medidas necesarias para proteger a testigos y pruebas, amparada en su marco legal de acción, dado que las personas que denuncian estos hechos pueden ser blanco de operaciones militares para intentar acallarlos y abrir el paso a la impundad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "solicitar protección sobre testigos de excepción y sus familias, y las pruebas, evitando que ellos sean asesinados y las pruebas sean eliminadas."
>
> <cite>Carta de las comunidades</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo se solicitó a la Fiscalía en cabeza de Francisco Barbosa que **revele los nombres de todas las personas que se encuentran en las bases de datos ilegales de inteligencia militar** y copia de las órdenes expedidas a través de altos mandos militares por civiles con poder y con intereses empresariales.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Pedir la protección sobre testigos de excepción y sus familias, y las pruebas y que de este modo se puede evitar que las pruebas recolectadas sean eliminadas y que las personas involucradas permanezcan a salvo.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La petición de las comunidades se da en medio de la impunidad que cobija a las operaciones de inteligencia ilegal realizadas por el batallón Charry Solano, la Brigada XX y otras instancias que no han sido sancionadas ni esclarecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Prueba de ello se encuentra en el informe **«BINCI y Brigada XX: El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno(1977-1998)», r**ealizado por la Coordinación Colombia Europa, el Colectivo de Abogados José Álvear Restrepo y la Comisión Intereclesial de Justicia y Paz entregado a la JEP , el pasado 7 de febrero del 2020. [(Lea también: Los crímenes de la inteligencia militar llegaron a la JEP)](https://archivo.contagioradio.com/los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho documento recopila 35 casos de tortura, 51 casos de ejecuciones extrajudiciales y más de 70 casos de desaparición forzada, como un conjunto de prácticas de la inteligencia militar que tenían como finalidad perseguir a integrantes de movimientos sociales, partidos políticos de oposición, defensores de derechos humanos y periodistas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comisión de la Verdad solicitó acceso a las carpetas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, la Comisión de la Verdad se dirigió al ministro de Defensa, Carlos Holmes Trujillo, solicitándole los archivos de las interceptaciones que fueron dadas a conocer, enfatizando la importancia de tomar con seriedad las denuncias que se han hecho sobre las acciones de inteligencia militar en contra de líderes sociales. [(Le recomendamos leer: Las víctimas seguirán exigiendo que se cumpla lo pactado)](https://archivo.contagioradio.com/las-victimas-seguiran-exigiendo-que-se-cumpla-lo-pactado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha solicitud hecha al Ministerio de Defensa está amparada en el derecho de acceso a la información reservada que le otorga el Decreto 588 con el fin de "esclarecer la verdad histórica y de ética pública subyacente”, expresó el padre Francisco de Roux quien reiteró que esta es la ocasión para la legitimidad de la Fuerza Pública sufra las transformaciones necesarias para llegar al fondo del problema al interior de la institución. [(Le recomendamos leer: El Gobierno respeta la Comisión, pero no la respalda: Francisco de Roux)](https://archivo.contagioradio.com/el-gobierno-respeta-la-comision-pero-no-la-respalda-francisco-de-roux/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el gobierno y el Ministerio de Defensa han anunciado el retiro de algunos mandos militares, según diversos sectores sociales no existen acciones de fondo para impedir que este tipo de hechos se repitan, como ha ocurrido desde hace cerca de 20 años, cuando se realizaron las operaciones ilegales del DAS, por lo que se espera que la intervención de organismos como la JEP y la Comisión de la Verdad permitan se tenga acceso a la información y se garantice existan acciones para que estos hechos no permanezcan en la impunidad.[(Le puede interesar: Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DD.HH)](https://archivo.contagioradio.com/espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
