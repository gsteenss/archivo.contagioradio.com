Title: La reincorporación una oportunidad para emprender
Date: 2018-12-11 09:33
Author: AdminContagio
Category: Expreso Libertad
Tags: FARC, presos politicos, reincorporación
Slug: reincorporacion-oportunidad-emprender
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/agricultura_compostaje_modulo_iii-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo.uy 

###### 4 Dic 2018 

La **Cooperativa de Profesionales del Común ProdelComún** es el nuevo escenario constituido por 12 ex prisioneros y ex prisioneras políticas, en el que buscan generar espacios para la construcción de paz.

El primero, un **espacio de formación pedagógica** para las personas que se encuentran en el proceso de reincorporación; el segundo un **trabajo comunitario en las localidades de Usme y Kennedy**; y el tercero la apuesta por **recuperar la memoria gastronómica del campesinado** con la creación de un restaurante que servirá platos como el empedrado o el tinto paisa.

Escuche en este Expreso Libertad cómo se van a desarrollar cada una de estas iniciativas y la forma en que puede apoyarlas.

###### <iframe id="audio_30679961" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30679961_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
