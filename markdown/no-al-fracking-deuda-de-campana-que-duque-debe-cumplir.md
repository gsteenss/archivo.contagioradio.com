Title: No al fracking: Deuda de campaña que Duque debe cumplir
Date: 2020-08-10 20:45
Author: PracticasCR
Category: Ambiente, Nacional
Tags: Angélica Lozano, Fracking en Colombia, Gustavo Bolivar, Proyecto de Ley
Slug: no-al-fracking-deuda-de-campana-que-duque-debe-cumplir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/fdfdfdf.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Ecoportal

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 10 de agosto, cerca de 40 congresistas se unieron para decir No al Fracking. En la Secretaria General de la Cámara de Representantes del Congreso, radicaron el proyecto por el cual *“se prohíbe en el territorio nacional la exploración y/o explotación de los Yacimientos No Convencionales (YNC) de hidrocarburos”.* Además señalan que este tipo de actividades no aportan al país y por el contrario, generan grandes afectaciones ambientales y sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La iniciativa fue encabezada por la [Alianza Colombia Libre de Fracking](https://colombialibredefracking.wordpress.com/2020/08/09/organizaciones-ambientalistas-y-congresistas-radican-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia/) y apoyada por congresistas de diferentes bancadas como Angélica Lozano, senadora de la Alianza Verde; Jairo Cala, representante a la Cámara por el Partido Farc; Luciano Grisales, representante del Partido Liberal; Ciro Fernández, representante a la Cámara de Cambio Radical, entre otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los voceros y voceras de la Alianza Colombia Libre de Fracking expresan que las razones para decirle **no al fracking** es que s**e debe proteger el medio ambiente y la salud, cumplir con el Acuerdo de París del 2015**, así como “una forma de prevenir los conflictos sociambientales que a lo largo de los años se han venido presentando” en torno a estas actividades. (Le puede interesar: [Irreversible daño ambiental por metano en la atmósfera](https://archivo.contagioradio.com/irreversible-dano-ambiental-por-metano-en-la-atmosfera/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “No es un asunto menor \[...\] y nos motiva caminar hacia la transición energética, pero no creemos que sea suficiente con aumentar la producción de energías renovables sino se detienen la extracción y quema de combustibles fósiles”.  
>
> <cite>Tatiana Roa | Alianza Colombia Libre de Fracking.</cite>

<!-- /wp:quote -->

<!-- wp:heading -->

¿Razones para impulsar este proyecto de no al fracking ?
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La senadora Angélica Lozano dijo que esta no es la primera vez que se intenta presentar este tipo de proyectos ante el Congreso y reitera que es *“compromiso desde las ciudades en apoyo a los territorio, al ser este un asunto de todos y todas”*. (Le puede interesar leer: [Radican en la CIDH alerta contra avance del fracking](https://archivo.contagioradio.com/ante-cidh-se-radica-alerta-en-contra-del-avance-del-fracking/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado el representante a la Cámara de Cambio Radical, Ciro Fernandez siendo de Barrancabermeja -municipio que conoce de primera mano los impactos de las actividades de extracción-, señaló que esta intervención generan desplazamientos, muertes y grandes daños irreparables, recordando que para vivir *“necesitamos de agua y de ese líquido que se está agotando”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En conclusión los congresistas señalaron que se unen para defender los territorios, proteger el agua, el subsuelo, la biodiversidad, a las comunidades, generaciones futuras, y especialmente para lograr una vida digna. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, siendo esta una decisión que a todos y todas compete, el senador Bolívar dijo que se hace el llamado a los partidos Centro Democrático y Conservador a que se unan pues más allá de un interés económico, *"es en realidad es un proyecto común en defensa de la vida y el futuro que le queda a Colombia".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/2676104325963294","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/2676104325963294

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
