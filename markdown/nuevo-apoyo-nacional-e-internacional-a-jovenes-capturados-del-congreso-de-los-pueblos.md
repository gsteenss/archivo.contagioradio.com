Title: Nuevo apoyo nacional e internacional a jóvenes capturados del Congreso de los Pueblos
Date: 2015-07-15 10:24
Category: DDHH, Educación
Tags: atentados, camila vallejo, captura, Congreso, explosión, jovenes, porvenir, pueblos
Slug: nuevo-apoyo-nacional-e-internacional-a-jovenes-capturados-del-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-15-a-las-9.45.33-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Twitter 

#####  

<iframe src="http://www.ivoox.com/player_ek_4829822_2_1.html?data=lZ2fm52Wdo6ZmKiak5qJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPjyMbRw5CrsNDmysaYtc7QusKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gloria Silva, Abogada defensora de Paola Salgado] 

###### [15 jul 2015] 

\[caption id="attachment\_11269" align="alignright" width="263"\][![Camila Vallejo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-15-a-las-9.45.331-263x300.png){.wp-image-11269 .size-medium width="263" height="300"}](https://archivo.contagioradio.com/nuevo-apoyo-nacional-e-internacional-a-jovenes-capturados-del-congreso-de-los-pueblos/captura-de-pantalla-2015-07-15-a-las-9-45-33-2/) Camila Vallejo\[/caption\]

Se cumplen 8 días de la captura de 13 jóvenes en Bogotá, en lo que abogados y familiares han denominado un [Falso Positivo Judicial](https://archivo.contagioradio.com/capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales/). Desde entonces han sido diversas las manifestaciones de apoyo a los jóvenes y al Congreso de los Pueblos, organización de la que gran parte de ellos hacen parte.

Al cumplirse una semana desde su detención, siguen presentándose los mensajes de apoyo. La líder estudiantil chilena Camila Vallejo, actualmente diputada en ese país, por ejemplo, envió un mensaje de solidaridad a través de su cuenta en twitter, escribiendo "Desde Chile para Colombia también decimos \#LibertadSonInocentes. Por Paola Salgado Yo pido Justicia". A su vez, profesores de la Universidad Nacional de Colombia exigieron que se respete el debido proceso y no se criminalice a la institución de educación superior.

El día de hoy continuará la audiencia de imputación de cargos contra los jóvenes. Según explicó la abogada Gloria Silva, parte de la bancada de defensa, a los jóvenes no sólo no se les está procesando por las explosiones en Porvenir del 2 de julio, sino que se imputan cargos por hechos en lugares y tiempos disímiles. Así entonces, mientras a la mayoría de los jóvenes se les acusa por participar en una protesta estudiantil el pasado 20 de mayo, a otros no se les relaciona con esos hechos. "La confusión de imputaciones dificulta el ejercicio de la defensa", asegura la abogada.

"Esta captura masiva se dio por la necesidad de responder mediáticamente con acusaciones que son falsas, con sentencias anticipadas, y procesalmente con irregularidades que van a terminar convirtiendo este proceso en un show, y en una mezcla de imputaciones que van a crear confusión", puntualizó la abogada Silva.

A continuación reproducimos los comunicados de apoyo a los y las jóvenes procesadas, por parte de docentes de la Universidad Nacional:

### **Dirección del Departamento de Filosofía** 

Desde el pasado 8 de julio permanecen detenidas 13 personas, quienes presuntamente harían parte de una célula urbana de la guerrilla del ELN y serían responsables de los atentados perpetrados en Bogotá unos días antes. Una de las personas detenidas es Andrés Felipe Rodríguez Parra egresado del Departamento de Filosofía de la Universidad Nacional.

Presumiendo su inocencia,[ espero que - respetado el debido proceso, examinada y ponderada con justicia la legitimidad y fuerza de lo que se presente como evidencia en su contra - Andrés Felipe Rodríguez Parra recupere su libertad y que para él, sus familiares y amigos termine pronto este duro momento que actualmente están atravesando.]{.text_exposed_show}

<div class="text_exposed_show">

*Raúl Meléndez*  
*Director, Departamento de Filosofía*

<p style="text-align: justify;">

</div>

### **Pronunciamiento de profesores Facultad de Derecho y Ciencias Políticas** 

   
\[caption id="attachment\_11267" align="aligncenter" width="500"\][![Comunicado de Profesores UN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Comunicado-Profesores.jpg){.wp-image-11267 .size-full width="500" height="816"}](https://archivo.contagioradio.com/nuevo-apoyo-nacional-e-internacional-a-jovenes-capturados-del-congreso-de-los-pueblos/comunicado-profesores/) Comunicado de Profesores UN\[/caption\]

Siga el minuto a minuto del proceso [aquí](https://archivo.contagioradio.com/minuto-a-minuto-caso-de-jovenes-capturados-por-explosiones-en-bogota/).
