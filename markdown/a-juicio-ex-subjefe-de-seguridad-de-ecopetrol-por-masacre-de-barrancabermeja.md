Title: A juicio ex-subjefe de seguridad de Ecopetrol por masacre de Barrancabermeja
Date: 2017-02-28 16:20
Category: DDHH, Nacional
Tags: Barranca, Barrancabermeja, colectivo de abogados, masacre
Slug: a-juicio-ex-subjefe-de-seguridad-de-ecopetrol-por-masacre-de-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/victor-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [28 Feb. 2017 ] 

**José Eduardo González Sánchez**, subjefe de seguridad de Ecopetrol en 1999 fue llamado a juicio, por ser considerado **autor intelectual y promotor de la masacre del 28 de febrero de 1999 en Barrancabermeja**. Sánchez será juzgado por los delitos de homicidio múltiple, tentativa de homicidio agravado y desaparición forzada agravada.

Adicionalmente, Sánchez será llamado a juicio por la masacre del 16 de mayo de 1998 y por otros hechos que tienen que ver con **su pertenencia a las estructuras paramilitares del Bloque Central Bolívar.**

Eduardo Carreño, integrante del Colectivo de Abogados José Alvear Restrepo y abogado del caso, aseguró que pese a todo el andamiaje procesal “varios de los paramilitares confesaron que José Eduardo González Sánchez alias “Mauricio” o “Enrique” es un ex capitán del Ejército al servicio de Ecopetrol y fue **el encargado de coordinar este aparato criminal para garantizar que la Policía, el Ejército, la Armada no hicieran nada para proteger a la población”.**

Este llamado a juicio que será llevado a cabo en Barrancabermeja, fue hecho por la Fiscalía 29 especializada de Derechos Humanos.

En la actualidad, José Eduardo González Sánchez se encuentra en un sitio de reclusión en Medellín a la espera de ser juzgado por los crímenes cometidos con el Bloque Central Bolívar **“el sale de Ecopetrol y pasa a comandar este grupo paramilitar”** cuenta Carreño.

La defensa manifiesta que esperan que Sánchez confiese y dé cuenta de todas las personas de la estructura de la fuerza pública en Barrrancabermeja que participaron en la masacre “es decir, allí se menciona al coronel Rodríguez y al mayor Herrera del Batallón 45, Héroes de Majagual, pero no se dan las especificaciones de los nombres completos, ni su descripción física”

“La pregunta que hay que hacerse” dice Carreño “es **¿quién lo nombró a él saliendo de la inteligencia militar para que fuera miembro de la seguridad de Ecopetrol?** Es decir, ahí debe haber alguna responsabilidad en el nombramiento de este tipo de criminales" asevera el abogado. Le puede interesar: [Paramilitares habrían asesinado a 4 jóvenes en Barrancabermeja](https://archivo.contagioradio.com/paramilitares-habrian-asesinado-a-4-jovenes-en-barrancabermeja/)

**Por esta masacre, solo existe un caso en el que hay condenas contra el Estado colombiano** y por el cual se realizó un acto de reconocimiento de responsabilidad por parte del Ejército, sin embargo, los casos restantes se encuentran en trámite o en otros se ha negado los derechos de las víctimas en los procesos administrativos aduciendo que no hay responsabilidad directa de los miembros de la fuerza pública.

**La Masacre del 28 de febrero se realizó hace 18 años por 16 paramilitares al mando de Mario Jaimes, alias “El Panadero”**, pertenecientes a las autodenominadas “Autodefensas de Santander y Sur del Cesar", quienes ingresaron a varios barrios ubicados en la Comuna 5 al nororiente de Barrancabermeja. Allí asesinaron a 8 personas y dejaron heridas a dos más.

Así mismo, fueron desaparecidos de manera forzada otras dos personas quienes se presume fueron ejecutadas y sus cuerpos arrojados al río Lebrija, sin que hasta el momento se hayan encontrado sus restos. Le puede interesar: [Autodefensas Gaitanistas amenazan a líderes en Barrancabermeja](https://archivo.contagioradio.com/autodefensas-gaitanistas-amenazan-a-lideres-en-barrancabermeja/)

<iframe id="audio_17275937" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_17275937_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
