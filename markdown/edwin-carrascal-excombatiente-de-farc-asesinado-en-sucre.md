Title: Edwin Carrascal, excombatiente de Farc asesinado en Sucre
Date: 2020-03-14 22:35
Author: CtgAdm
Category: Actualidad, DDHH
Slug: edwin-carrascal-excombatiente-de-farc-asesinado-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/partido_farc_afp_12_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 10 de marzo a las 8:00 pm en la vereda San Antonio municipio de Coloso, Sucre fue asesinado el firmante de paz Edwin de Jesús Carrascal Barrios. ( Le puede interesar: <https://archivo.contagioradio.com/ex-prisioneras-politicas-estan-siendo-victimas-de-amenazas/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según testigos de la comunidad dos personas en moto se acercaron a la residencia de Carrascal, llamaron a su puerta y posteriormente le disparan. El excombatiente es trasladado al la Clinica Santa María de Sincelejo, pero por la gravedad de sus heridas, fallece.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según Yuri Camargo integrante del partido de las Fuerza Alternativa del Común quién denunció el hecho mediante su cuenta de twitter .

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/YuriCaFarc_ep/status/1237610512793210882","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/YuriCaFarc\_ep/status/1237610512793210882

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El caso queda a cargo de la Policía quién empieza un proceso de búsqueda para dar con los responsables del asesinato, sin embargo estos lograr huir sin dejar más rastro que dos armas de fuego a pocos metros del lugar del hecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el asesinato de Edwin Carrascal aumenta a 189 el número de firmantes de paz asesinados en los últimos meses, de las cuales 16 fueron registradas en el primer trimestre de 2020. (Le puede interesar: <https://www.justiciaypazcolombia.com/asesinatos-en-medio-de-la-presencia-militar-en-putumayo/>)

<!-- /wp:paragraph -->
