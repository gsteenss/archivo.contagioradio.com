Title: Y ganó el NO
Date: 2016-10-03 07:52
Category: Opinion, Tatianna
Tags: acuerdos de paz, paz, plebiscito por la paz
Slug: y-gano-el-no
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/plebiscito-no.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: TKM 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/tatianna-riveros-2/)- [@[tatiannarive]

##### 3 Oct 2016 

Tal vez un resultado inesperado para todos aquellos que por primera vez vimos una posible salida al conflicto armado; un resultado que también sorprendió a sus promotores, dueños de mentiras como los esferos borrables, la ley Roy Barreras, la imposición de la ideología de género y hasta el adoctrinamiento marxista. Sí, sorprendidos porque son dueños del voto irresponsable, un voto ciego que nos tiene hoy en la incertidumbre. Tan sorprendidos estaban que sólo tres horas después de los resultados el dueño de ese resultado salió a manosear frases queriendo dar un discurso pero no dijo nada, el dueño de la polarización del país invitó a la unidad al mejor estilo.

¿Qué habría pasado si esa pequeña diferencia fuera al revés? Aceptarían con la misma grandeza la derrota o en cambio habría ganado la maquinaria del gobierno a punta de mermelada. Sería acaso un fraude electoral anunciado desde el hackeo a la página de la Registraduría o en cambio habríamos entregado el país a las FARC. Habría ganado la compra de votos, el clientelismo o acaso se atribuirían también el porcentaje abstencionista.

Pues bien, sacar el 50% de la votación a favor con el 49% en contra y con una realidad del 63% de la abstención y dejar al País en un limbo político no es ganar nada, y diría exactamente lo mismo si la situación fuese al revés. Por mucho tiempo creímos que el problema de Colombia eran las FARC, hoy puedo decir con certeza que después de la corrupción el problema es la indiferencia.

¿Cómo le explicamos a esos pueblos en los que ganó el Sí que deben esperar mientras el gobierno, las FARC , y el Centro Democrático vuelven a negociar (en el hipotético de que pase)? Me quedo con la esperanza en el corazón de que le cumplí a Bojayá, Toribio, Miraflores, Mampuján, mi voto fue el primer voto que dí con alegría, con la plena seguridad de que no era por una persona sino para salvar la vida de miles y evitar el dolor que millones han sufrido la guerra.

¿Cómo le explicamos a los niños que no murió nadie pero que son inevitables las lágrimas?

¿Cómo entendemos que una tierra que parió a los falsos positivos y se los arrebataron votó por el NO?

Me pregunto si usted y los suyos sienten la misma satisfacción del deber cumplido y siguen con la esperanza intacta, o si por el contrario usted votó por los egos de unos y otros buscando "joder" al gobierno de turno, de ser esto último le regalo un abrazo porque usted regaló su voto y le falló a todos esos que dijeron Sí pensando en lo primero.

Me pregunto si le fallamos a esas poblaciones que desde el Domingo sienten de nuevo el temor, de mi parte les dejo mi voto y las ganas de seguir buscando el fin del conflicto y por qué no, la Paz.

Me quedo con el país que fue feliz un lunes, que vio aviones que no descargaban bombas pasar, que aplaudió a un Humberto y a un Sergio, me quedo con el país que fue partícipe del perdón de una guerrilla. Me quedo con las ganas de seguir, porque eso nadie me lo quita.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
