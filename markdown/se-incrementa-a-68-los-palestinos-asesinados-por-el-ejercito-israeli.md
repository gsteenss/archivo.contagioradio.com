Title: Se incrementa a 68 los palestinos asesinados por el ejército israelí
Date: 2015-10-30 13:17
Category: DDHH, El mundo
Tags: 14 menores de edad y una mujer embarazada fueron víctimas mortales, 68 palestinos muertos a manos de israelíes, Cisjordania, Franja de Gaza, Naciones Unidas, Presidente palestino Mahmud Abbas
Slug: se-incrementa-a-68-los-palestinos-asesinados-por-el-ejercito-israeli
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/palestina-222.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: vallecereza 

###### [30 oct 2015]

**La cifra de palestinos asesinados en octubre por el ejército israelí aumentó a 68 en los territorios de la Franja de Gaza y Cisjordania ocupada. **Mediante el balance de víctimas realizado por el Ministerio de Salud palestino, se afirmó que 51 palestinos pertenecían a diferentes partes de Cisjordania y los otros 17 a la Franja de Gaza.

Adicionalmente se estableció que entre las víctimas mortales, hay **14 son menores de edad y una mujer embarazada. Además, **el pasado jueves se reconoció  a un joven asesinado brutalmente por fuerzas militares  israelíes en la ciudad Al-Jallil en Cisjordania. Al parecer la policía afirmó que la muerte del joven fue ocasionada por intentar apuñalar a uno de los agentes que prestaban control militar, aparentemente el policía sufrió heridas leves en la cabeza, mientras que el palestino recibió varios impactos de bala ocasionándole la muerte.

Debido al aumento de violencia en contra de la población palestina durante octubre, el presidente palestino, Mahmud Abbás,** solicitó a las **Naciones Unidas un "**régimen de protección especial para el pueblo de los territorios ocupados, incluido Jerusalén Este, donde ahora  se vive la peor y más crítica situación desde 1948", expresó.**

Mientras tanto el Ministerio de Interior de Israel informó que el Gobierno legalizó de manera retroactiva aproximadamente 800 viviendas construidas en cuatro colonias de Cisjordania ocupada. Las viviendas están repartidas entre las colinas de Yakir (377 casas), Itamar (187) y Shiloh (94), en el norte de Cisjordania, y en Sansana (97), en el sur del territorio palestino.
