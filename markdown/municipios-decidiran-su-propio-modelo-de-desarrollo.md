Title: Municipios decidirán su propio modelo de desarrollo
Date: 2016-10-14 09:22
Category: Ambiente, Nacional
Tags: comunidades, consulta popular, Megamineria
Slug: municipios-decidiran-su-propio-modelo-de-desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/mineria1-e1476455180201.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Oct 2016] 

El pasado martes la Corte Constitucional declaró que los entes territoriales, en cabeza de alcaldes departamentales y municipales, tendrán competencia para regular el uso del suelo y garantizar la protección del medio ambiente, incluso podrían prohibir actividades extractivas.

Una acción de tutela interpuesta por una habitante del municipio de Pijao Quindío contra el Tribunal Administrativo, que declaró inconstitucional una consulta popular minera impulsada por la comunidad y liderada por el Alcalde Alberto Peña, fue crucial para la decisión de la Corte Constitucional.

Algunos Comités Ambientales señalan que la sentencia se convertiría en una herramienta importante, pues define que “los pueblos pueden optar por un desarrollo alternativo frente a un modelo de desarrollo nacional”. Esto permite que continúen las consultas populares en los territorios y que las comunidades decidan sobre el desarrollo minero del país. Le puede interesar: [5 grandes daños de la minería a gran escala](http://5%20grandes%20daños%20de%20la%20minería%20a%20gran%20escala)

Además la sala sexta de revisión de la Corte manifestó que el Gobierno Nacional ha construido políticas mineras sin contar con estudios técnicos, sociológicos y científicos que permitan evaluar los impactos ambientales que generan las actividades extractivas.

Frente a ello, la Corte ordenó la conformación de una mesa de trabajo interinstitucional para dar cumplimiento a los estudios que deben realizarse e identificar estos impactos en los ecosistemas colombianos. Se concedió un termino no prorrogable de 2 años contados a partir de la notificación de la sentencia. También se fijó que la entrega de informes de la mesa de trabajo deberá ser cada tres meses, para que en ejercicio de sus competencias la Procuraduría y la Contraloría adelanten el seguimiento a lo ordenado por la Corte. Le puede interesar: [Corte constitucional impide minería en ecosistemas estratégicos](https://archivo.contagioradio.com/corte-constitucional-impide-mineria-en-ecosistemas-estrategicos/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
