Title: Dineros del Fondo de Paz se gastaron en publicidad y perfilamientos para el gobierno Duque
Date: 2020-08-27 20:09
Author: AdminContagio
Category: Actualidad, Nacional
Tags: FLIP, Fundación para la Libertad de Prensa, Iván Duque, Perfilamientos, Publicidad
Slug: dineros-del-fondo-de-paz-se-gastaron-en-publicidad-y-perfilamientos-para-el-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Duque-perfilamientos-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Du-Brands-perfilamientos.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Fundación para la Libertad de Prensa -[FLIP](https://twitter.com/FLIP_org)-, por medio del portal «[Pauta Visible](https://pautavisible.org/)», publicó una investigación sobre **los recursos que el presidente Iván Duque ha destinado a publicidad oficial en sus dos primeros años. En estudio también aborda la elaboración de una serie de perfilamientos que recayeron sobre diversos líderes de opinión.** (Lea también: [Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la investigación **desde que Iván Duque asumió la Presidencia ha gastado más de 20 mil millones de pesos en contratos de publicidad, es decir, 27 millones diarios.** Con dichos recursos el Presidente pretendía, de acuerdo con la FLIP, mejorar su imagen en redes sociales, mitigar el impacto del Paro Nacional y multiplicar su presencia en los medios de comunicación durante la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los rubros de este gasto se destinaron así: **7.490 millones** para la producción y transmisión de contenidos audiovisuales; **7.390 millones** para estrategias digitales y de redes; **5.130 millones** para estrategias de comunicación generales y planes de medios; **118 millones** para la producción de material publicitario; y **56 millones** para la publicación de avisos en medios impresos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, **la investigación reveló que el 50% de los recursos provinieron del Fondo de Programas Especiales para la Paz (Fondo Paz).** Esta denuncia, se hizo pública hace algún tiempo y generó el pronunciamiento de la Contraloría la semana pasada, donde informó el hallazgo de un aparente hecho irregular en el gasto; y apuntó que los recursos de dicho Fondo no pueden ser utilizados  por el Gobierno  en publicidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo anterior, pese a que **una de las consignas de campaña, del hoy presidente Duque, se centraba en la austeridad en el gasto público y los constantes reproches que hacia al Gobierno Santos por considerarlo «*derrrochón*».** Frente a esto, cabe anotar que semanas después de asumir la Presidencia, Duque emitió la Directiva 09 en la que ordenaba a las entidades de la rama ejecutiva la austeridad en el gasto de publicidad, algo que a la luz del reporte de la FLIP, no cumplió a cabalidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Nuevos perfilamientos del Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph -->

La **FLIP reveló también que en desarrollo de esos contratos, se creó una base de datos con una lista de al menos 468 personas activas en la red social Twitter** quienes fueron perfilados por la **Presidencia de la República **para medir sus opiniones sobre el Gobierno. (Le puede interesar: [Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DDHH](https://archivo.contagioradio.com/espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los perfiles se encontraron periodistas, líderes de opinión, «influenciadores», abogados, políticos, entre otros, a los cuales clasificaban como *«negativo», «positivo», «neutro» o «N/A»* según la postura que tuvieran frente al Gobierno. Esto recuerda el episodio reciente en el que El Ejército Nacional llevó a cabo seguimientos y perfilamientos ilegales. Lea también: [Las violaciones a la libertad de prensa por parte del Ejército Nacional](https://archivo.contagioradio.com/las-violaciones-a-la-libertad-de-prensa-por-parte-del-ejercito-nacional/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1298968756253790208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1298968756253790208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1298971875209551872","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1298971875209551872

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1299034117519167488","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1299034117519167488

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fdbedout/status/1298967004385349632","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fdbedout/status/1298967004385349632

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Adicionalmente, llama la atención que según un documento que se dio a conocer, habría sido Hassan Nassar, actual Consejero de Comunicaciones de la Presidencia de la República, quien solicitó a la firma Du Brands,  empresa que maneja la imagen del presidente Duque, que tiene un contrato con el Gobierno y que hizo parte de la campaña por el NO en el plebiscito, la consolidación de la base de datos y los perfilamientos. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":89005,"width":580,"height":668,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Du-Brands-perfilamientos-888x1024.jpg){.wp-image-89005 width="580" height="668"}  

<figcaption>
Comunicación dirigida a Hassan Nassar

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
