Title: 500 pobladores se toman pacíficamente la alcaldía de Valdivia
Date: 2016-06-03 13:34
Category: Movilización, Paro Nacional
Tags: Alcaldía Valdivia, Hidroituango, Movimiento Ríos Vivos
Slug: 500-pobladores-se-toman-pacificamente-la-alcaldia-de-valdivia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Alcaldía-Valdivia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos ] 

###### [3 Junio 2016 ]

En vista de la desatención del gobierno nacional, departamental y local a la exigencia de las comunidades campesinas de que se suspendan los desalojos forzosos por cuenta del proyecto Hidroituango, desde las 7 de la mañana de este viernes **más de 500 pobladores se tomaron pacíficamente la alcaldía del municipio de Valdivia**.

Según Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, las comunidades permanecerán en la alcaldía hasta que las desalojen, como **las han desplazado de todas las playas del río Cauca**, afectándolas y vulnerando su derecho a la permanencia territorial y al acceso a los medios de subsistencia, por cuenta de las decisiones que desde EPM serían emitidas a las alcaldías municipales para el proyecto hidroeléctrico de Ituango.

De acuerdo con Zuleta, algunos de los alcaldes del bajo Cauca antioqueño han confesado que muchas veces **no leen los documentos que firman con Empresas Públicas de Medellín**, varios de los cuales han aprobado los desalojos de las comunidades.

"Sabemos que el municipio está rodeado de ESMAD, afuera de la alcaldía hay mucha policía, cuando ingresamos a la alcaldía nos recibieron con empujones, gritos e insultos", asegura Zuleta; sin embargo, permanecerán en la alcaldía exigiendo la suspensión de los desalojos y la instalación inmediata de una Comisión Nacional de Represas que analice y solucione los conflictos socioambientales que genera la construcción de megaproyectos hidroeléctricos.

<iframe src="http://co.ivoox.com/es/player_ej_11773121_2_1.html?data=kpakmZiVdpKhhpywj5WbaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzpCnb7vpzcrhw5CRb7OZpJiSo6nTt4zKytvc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
