Title: ¿Es la ideología de género el principal enemigo de la religión?
Date: 2019-12-10 15:01
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: Comunidad LGBTI, enfoque de género, LGBTI, Religión
Slug: ideologiadegeneroenemigodelareligion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Con la medida que midan a los otros, los medirán a ustedes**  
(Mateo 7,2)

Bogotá, 25 de noviembre del 2019

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales,*  
*mientras exigimos a los demás que no pequen, o los criticamos por pecadores.*

##### Estimado  
**Hermano en la fe**

Cristianos, cristianas, personas interesadas.

En nuestros diálogos fracasados, uno de los temas imposibles de abordar es el de **“genero”** y ante cualquier opinión diferente o matización a tu idea te ponía furioso y bloqueabas la conversación diciendo: “eso es ideología de género”, “es una estrategia internacional para acabar con la familia”, “la ideología de género quiere imponer la homosexualidad a nuestros hijos” o “no se puede ser cristiano y tener relaciones con la ideología de género”. Luego enviabas videos, mensajes de predicadores y de videntes que decían que la ideología de género es diabólica y peor que el mismo diablo, porque se vestía de derechos y respeto a la diferencia de la humanidad.

Con respeto, pero con sinceridad te pregunto:

#### **¿sabes lo que dices cuando hablas de ideología de** **género? ¿Lo puedes explicar en tus propias palabras?** 

Contra la “ideología de género” hablan pastores y pastoras de diversas iglesias y de diversos “rangos” (local, nacional e internacional), obispos, sacerdotes, religiosas, religiosos, cristianas y cristianos del barrio; lo hacen por diferentes medios (libros, revistas, radio, televisión y redes sociales y diálogos en la calle, en el bus, en la tienda…) y en diversas formas (liturgias, cultos, predicaciones, conferencias, mensajes religiosos por la redes sociales, diálogos y discusiones familiares, de amigos y en las iglesias). A varios les he pedido que me expliquen lo que entienden por ideología de género: unos se quedaban callados; otros, repetían frases sin explicarlas; unos pocos daban explicaciones bíblicas, teológicas, filosóficas o sociológicas, pero no admitían ningún cuestionamiento o argumentación diferente; otros recurrían a hechos reales y concretos (ataques a símbolos religiosos, actitudes grotescas frente a símbolos religiosos) de grupos que no representan todo el movimiento de mujeres y **LGTBI**.

Hay personas representativas de grupos de poder que se presentan como defensores de la familia, de la fe, de la tradición y de las buenas costumbres y que dicen estar en contra de la “ideología de género” pero muchas de sus actuaciones son contrarias a la ética y a los valores predicados por Jesús de Nazaret. Un ejemplo es el de políticos que atacan, calumnian y descalifican a quienes defienden o promueven los derechos de mujeres y de población LGTBI, o los derechos humanos y ambientales, mientras estaban implicados de diversas formas con el escándalo de la “comunidad del anillo” (personas de alto poder que utlizaban una red de prostitución homosexual dentro de la Escuela de Cadetes de policía General Santander, acusada de violaciones, abusos y chantajes y homicidios; la alférez Lina Zapata de tan solo 19 años, empezó a destapar esa olla podrida y fue asesinada).

Le puede interesar:[Una razón de la imposibilidad de hablar “decentemente” ](https://archivo.contagioradio.com/una-razon-de-la-imposibilidad-de-hablar-decentemente/)

Hay un sector del poder político y económico que ha manejado el país y lo ha conducido por el camino de la corrupción, la violación de derechos humanos, el narcotráfico y la guerra, convirtiéndolo en uno de los países mas inequitativos, violentos e injustos del mundo, manipulándolo con el uso de la ideología de género como un arma incuestionable contra quienes trabajan por la paz, la superación del conflicto armado y una política más justa e incluyente. Por el “miedo” a la ideología de género los creyentes han rechazado propuestas políticas decentes para concejos municipales, alcaldías, asambleas, gobernaciones, cámara de representantes, senado y hasta candidatos presidenciales y han elegido personas aliadas de la corrupción, el narcotráfico y la politiquería, responsables del empobrecimiento de las mayorías, del deterioro de la salud, de los daños ambientales y de la creación de una mentalidad que niega derechos a los otros y justifica su eliminación. ¿Se pueden considerar como cristianas estas actuaciones en nombre de Dios?

Dos hechos recientes, muestran el uso de la ideología de género en contra del mismo pueblo y de principios éticos que podemos considerar cristianos:

##### El primer hecho: 

El plebiscito en el cual muchos **creyentes dijeron NO a los acuerdos** para terminar el conflicto armado entre el gobierno y las **FARC** porque, según dijeron sus líderes, tenían “ideología de género”. Con razón se afirma que el NO ganó con los votos cristianos, lo que es una contradicción con el mandato de Jesús de Nazaret a sus seguidores: “perdonar a los enemigos” (Mateo 5, 43-47), “trabajar por la paz” (Mateo 5,9), “rechazar la violencia” (Matero 5,39) y a pesar de esto **lo hicieron convencidos que era lo que Dios quería y para salvar la religión**. Los Acuerdos tienen una perspectiva diferencial para que quienes sufrieron una mayor victimización por su condición sexual (mujer o población LGTBI) fueran atendidos según sus afectaciones y condición, y esto es de elemental justicia. Luego del plebiscito hablé, despacio y respetuosamente, con personas religiosas que habían votado por el NO y leímos juntos parte de los acuerdos, al final reconocieron que los desconocían, que votaron llevados por sus líderes. Incluso algunos pastores reconocieron que el **SI** estaba más de acuerdo con la Palabra de Dios que el **NO**, diálogos abiertos sobre el tema y se “disculparon”.

##### El segundo hecho fue: 

El referendo anticorrupción, las redes sociales fueron inundadas con mensajes diciendo que los cristianos no podían votarlo porque contenía “ideología de género”. El voto “cristiano” impidió que el referendo alcanzara el umbral necesario para hacer obligatorias las propuestas; beneficiando a los corruptos de siempre. Con la gran votación a favor de medidas anticorrupción, políticos de diversos partidos se comprometieron a impulsar leyes en el congreso que recogieran las propuestas del referendo; ya sabemos lo que pasó. A manera de ejemplo, el día antes del referendo visité una familia muy cercana, y allí estaba una vecina a quien le habían dicho en el trabajo que no podía votar el referendo, porque ella era una persona de fe y el referendo contenía ideología de género y por eso estaba en contra de su fe. Con la vecina leímos el texto de referendo, al final decidió votarlo.

#### **¿Te das cuenta del mensaje que están dejando estos hechos en la sociedad?** 

Atacar la corrupción es contrario del mensaje cristiano, los corruptos son buenos si están contra la ideología de género; defender la familia tradicional es apoyar la corrupción; es mejor una guerra cruel e interminable a un buen acuerdo, aunque sea imperfecto; los cristianos que hablamos de perdón y reconciliación hacemos lo contrario en la vida social, política y económica, es decir que *“predicamos y no aplicamos”.*

La otra cara de esta realidad es el dialogado con organizaciones feministas, con personas y organizaciones de población LGTBI, con defensores y defensoras de los derechos sexuales, en el que no he encontrado los “demonios” de los que me hablan. Con diferencias y posiciones contrarias, he encontrado padres y madres de familia preocupados por sus hijos, mujeres y hombres de diversas orientaciones sexuales queriendo dejar a las futuras generaciones un ambiente social y humano sin discriminaciones por razones sociales, raciales, sexuales, sin violencias e injusticias sociales y ambientales; pidiendo que niños y niñas no sean estigmatizados/as por una condición sexual que no eligieron, pidiendo que puedan expresarla y vivirla en el respeto mutuo, sin presiones y exigencias inhumanas, sin daños socio-religiosos.

Es un hecho que existen personas depravadas y con personalidades complejas en la población homosexual, como las hay en la población heterosexuales (hombres y mujeres que sienten atracción sexual por personas del otro sexo). Está bien hablar de la familia tradicional compuesta por papá, mamá e hijos, pero la inmensa mayoría de las personas que van a las iglesias no tienen una familia tradicional. Decir que una pareja heterosexual garantiza el bienestar humano y espiritual de los hijos está bien, pero la realidad nos muestra muy altos índices de violencia en las familias tradicionales, que más del 60% del abuso sexual de niños y niñas ocurre en el entorno familiar; que las personas homosexuales nacieron de parejas heterosexuales.

Por las repercusiones de esta realidad en lo social y lo religioso, en una próxima comunicación la seguiré profundizando, mientras tanto, considero que la actitud más cristiana, es asumir las palabras de Jesús: “No juzguen y no serán juzgados. Del mismo modo que ustedes juzguen, se les juzgará. La medida que usen para medir, la usarán con ustedes” (Mateo 7,1-2); “No condenen y no serán condenados. Perdonen y serán perdonados” (Lucas 6,37); porque ¿Qué sabemos del porqué una persona tiene una condición sexual diversa?, ¿Podemos juzgarla sin conocerla?, ¿Está alguien exento de que entre sus seres queridos haya una persona de “otra” condición sexual?, ¿Pudieron estas personas elegir su condición sexual? Y de otro lado, ¿quienes nos reconocemos heterosexuales elegimos nuestra condición?

Fraternalmente, su hermano en la fe,

P. Alberto Franco, CSsR, J&P  
francoalberto9@gmail.com

 
