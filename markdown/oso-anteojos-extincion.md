Title: Anteojos rotos
Date: 2017-03-31 09:00
Category: invitado, Opinion
Tags: Chingaza, colombia, Cundinamarca, especies en peligro, oso de anteojos
Slug: oso-anteojos-extincion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/IMAGEN-16639578-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El tiempo 

###### Por: Catalina Reyes-[ [@**SagenSiemirCata**]

###### 30 Mar 2017 

Al menos cinco osos de anteojos han sido asesinados en el territorio colombiano a lo largo del año. Estos simpáticos animales son el único sobreviviente de la subfamilia Tremarctinae y absoluto representante de los osos en Suramérica. La pérdida y fragmentación de sus hábitats, la caza y la poca información sobre su distribución hacen tambalear las vulnerables poblaciones de esta especie insignia de las imponentes cordilleras andinas. Los estamos acorralando, sin embargo, seguimos considerándolos nuestros enemigos.

Hace unos días, Colombia se ponía “los anteojos por la vida” en el marco del Día Internacional para la Protección de los Osos. Hoy, tantos anteojos resultan empañados ante el estupor que generan las crudas imágenes de un individuo abatido en el municipio de Fómeque, Cundinamarca, quien resulta ser una víctima más de las egoístas atribuciones humanas. Ante la infame persecución de los osos, el retumbar de miles de colombianas y colombianos quienes no concebimos la indiferencia como una opción, son aliciente para los innumerables esfuerzos de conservación que resultan vacuos sin el compromiso de comunidades e instituciones.

La imprescindible función ecológica del oso de anteojos ha sido ampliamente exaltada. Mientras muchas nos hemos proclamado guardianas del oso, persiste aún el desconocimiento sobre la importancia del gran dispersor de los bosques. Tal desinformación ha cobrado directamente la vida de decenas de osos en el país, bien sea por caza indiscriminada o por tráfico ilegal. Llama la atención la insolencia con la que muchos representantes de nuestra especie se relacionan con estas fichas clave del rompecabezas ecológico. De seguro en nuestro país, se nos ha hecho usual considerar a la víctima como el victimario.

Algunos comentarios sugieren la “imprudencia” de los osos al pasearse por los alrededores de tierras, que siendo parte de su nicho fundamental, ahora están ocupadas por actividades productivas tales como la ganadería y la agricultura. Esta primera actividad no sólo constituye la mayor causa del cambio climático, sino que en conjunto, sofocan cada vez más los fragmentos del territorio que han sido estratégicamente delimitados con el fin de conservar la megadiversidad del país. Los animales no-humanos no conocen de fronteras políticas o de jurisdicciones territoriales. Es ingenuo suponer que la fauna silvestre obedece estos límites, mucho más cuando la frontera agropecuaria se extiende sin pudor. La biodiversidad no se limita a las áreas protegidas: está en cada rincón de país intentando sobrevivir con los pocos y deteriorados recursos que les dejamos para aprovechar.

Muchos investigadores afirman que la percepción del conflicto socioambiental es mayor al conflicto real, no obstante cada día cobra más vidas. La presencia de los osos de anteojos pasa de ser un indicio de la salud de nuestros ecosistemas a encender las alarmas para evitar que la disputa termine con la desaparición de otro individuo, tal como sucede con el osezno avistado en el municipio de la Calera quien ya es acechado por cazadores. Confío en que no tengamos que presenciar la extinción del fastuoso oso sudamericano. Sin embargo, nunca está de más revisar periódicamente nuestros lentes; tal vez su desgaste nos está impidiendo reconocer a las víctimas históricamente más invisibilizadas y subordinadas: los animales no-humanos.
