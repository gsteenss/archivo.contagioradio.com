Title: Ejército nunca llegó a comunidad indígena confinada por paramilitares
Date: 2018-07-27 12:19
Category: DDHH, Nacional
Tags: AGC, Chocó, Comunidad Indígena, Derechos Humanos, ejercito
Slug: ejercito-nunca-llego-comunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-27-a-las-11.14.45-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Jul 2018] 

Las Comunidades Indígenas del resguardo Urada Jiguamiandó del municipio Carmen del Darién, Chocó, reportaron la salida del grupo neoparalimitar autodenominado  **Autodefensas Gaitanistas de Colombia (AGC), que desde hace 4 días** hacia presencia en la zona con un grupo de por lo menos 40 integrantes. (Le puede interesar: ["Indígenas de Chocó confinados por presencia paramilitar"](https://archivo.contagioradio.com/indigenas-confinados-paramilitar/))

En la mañana del 24 de julio, las comunidades de Ibudó y Padadó del mismo Municipio, presenciaron la salida de los hombres armados, indicando que se dirigieron rumbo a **Tamboral, lugar en el que se ha denunciado la existencia un campamento de las AGC en la zona.** Sin embargo, pese a las alertas, no han recibido respuesta alguna del Estado.

Adicionalmente, los indígenas aseguran que el 23 de julio aterrizó un Helicóptero con militares en el sector de Urada, ubicado a menos de dos horas caminando del Resguardo indígena; a pesar de ello, afirman que **ninguna unidad del ejército hizo presencia en el territorio. **(Le puede interesar: ["121 familias se encuentran confinadas por paramilitares: Pueblos Indígenas"](https://archivo.contagioradio.com/121-familias-se-encuentran-confinadas-por-paramilitares-pueblos-indigenas/))

Por esas razones, la comunidad indígena en Carmen del Darien sigue preocupada ante la situación de inseguridad que viven y la nula protección que reciben por parte del Estado colombiano, pese a contar con medidas cautelares para su protección. (Le puede interesar: ["Medidas cautelares a resguardo indígena Urada Jiguamiandó en Chocó"](https://archivo.contagioradio.com/medidas-cautelares-a-resguardo-indigena-urada-jiguamando-en-choco/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
