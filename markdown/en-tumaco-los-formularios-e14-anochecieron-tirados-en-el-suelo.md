Title: En Tumaco los formularios E14 anochecieron tirados en el suelo
Date: 2015-10-26 15:11
Category: Nacional, Política
Tags: Elecciones 2015, Elecciones en Tumaco 2015, Jornada electoral en Colombia 2015
Slug: en-tumaco-los-formularios-e14-anochecieron-tirados-en-el-suelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tumaco POPOLO 

<iframe src="http://www.ivoox.com/player_ek_9169955_2_1.html?data=mpajm56ZeY6ZmKiakpqJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bftdraw8jTb83j1JDT0dfRuc3V087c1ZCpdZWfwtPcxc3Jp8rZ09TbjdnNtsLY0NiYx9OPqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diego Ángulo] 

###### [26 Oct 2015 ]

[Las elecciones en Tumaco transcurrieron en medio de graves denuncias por delitos electorales, relacionados con los formularios E14, designados para el registro de los votos. Los **formatos en blanco** habrían sido encontrados por varios pobladores **regados en el suelo del casco urbano**, mientras que **a la Registraduría llegaron otros formularios ya diligenciados**.]

[Diego Ángulo, candidato a la alcaldía, asegura que se alarmaron al recibir notificaciones de los boletines que iba emitiendo la Registraduría, pues **al poco tiempo de cerrada la jornada electoral se recibieron los formularios E14 de 2 mesas de las 363 ubicadas en las veredas del municipio**, a 2 horas del casco urbano, mientras que los de las mesas allí presentes llegaron a la Registraduría hasta pasadas las 9 de la noche.]

[De acuerdo con Ángulo, para los pobladores de Tumaco **“se han robado las elecciones”**, un gran descontento por el que a las afueras del Coliseo, dónde se estaban contando los votos, se registraron manifestaciones que terminaron en fuertes disturbios. La comunidad exige que una **comisión de la Fiscalía** se haga presente en el municipio para verificar las irregularidades que se presentaron, mientras que **no ha habido ningún pronunciamiento oficial**.]
