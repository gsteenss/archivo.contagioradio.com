Title: Colombia inicia el año con aumento de violencia contra líderes sociales
Date: 2020-01-09 14:27
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinatos, campesinos, colombia, Huila, lideres sociales, Putumayo
Slug: colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/lideres-asesonados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

En los primeros días del 2020 en Colombia han sido asesinados cuatro líderes sociales, los hechos más recientes ocurrieron el 8 de enero; dos de ellos en Putumayo y uno en Huila; asimismo se conoció el intento de homicidio hacia el presiente de la vereda Buenas Esperanza.

Según la Red de Derechos Humanos del Putumayo, la primera víctima fue identificada como **Óscar Quintero Valencia**, en la vereda El Caño Sábalo, inspección de Santa Lucía a tres kilómetros de la vereda Buena Esperanza; el hecho se registró el 8 de diciembre hacia las 6:30 p.m. cuando cuatro hombres armados llegaron a la vivienda del señor Óscar Quintero Valencia *"a quien preguntaron con nombre propio para posteriormente asesinarlo en el lugar",* señaló la Red por medio de un comunicado.

Estos mismos hombres continuaron avanzaron por Santa Lucia hasta la vereda El Mango, donde asesinaron a **Gentil Hernández Jiménez.** (Le puede interesar: [“Desmantelamiento de organizaciones criminales debe empezar ya” Defendamos La Paz](https://archivo.contagioradio.com/desmantelamiento-de-organizaciones-criminales-debe-empezar-ya-defendamos-la-paz/))

Posteriormente a las 7:00 pm, los agresores llegaron a la vivienda del presidente de la vereda Buena Esperanza, **Arturo Tovar Collazos**, de 38 años,  quien tras ser avisado por su esposa que desconocidos lo buscaban "*con la excusa de necesitar una herramienta",* logró  huir por la parte trasera hacia una zona boscosa para ponerse a salvo, mientras lo sujetos se retiraban de la zona.

Acto seguido la esposa del líder llamó a la Fuerza Pública, solicitando acompañamiento y protección para el desplazamiento de Tovar de la vereda. (Le puede interesar: [Defensoría del Pueblo advierte sobre violaciones a DDHH contra comunidades de Putumayo](https://archivo.contagioradio.com/defensoria-del-pueblo-advierte-sobre-violaciones-a-ddhh-contra-comunidades-de-putumayo/))

### "Pedimos garantías a la vida de la población en Puerto Guzmán"

Por su parte la Red de DD.HH. de Putumayo, atribuyó los hechos al autodenominado Cartel de Sinaloa, quienes anunciaron en septiembre de 2019 *" llegar para quedarse* y *guerra a muerte"* contra el autodenominado Frente Carolina Ramírez de las FARC – EP. En hecho más recientes, el grupo armado se ha hecho visible con no solo estos asesinatos, desde su presencia se han registrado más de 10 ataques en la zona.

Así mismo la Red solicitó al Presidente Duque, al Defensor del Pueblo, a organismos internacionales, y a la Unidad Nacional de Protección, *"Garantizar el derecho a la vida e integridad personal de los líderes y las lideresas sociales de Puerto Guzmán, especialmente del compañero Arturo Tovar Collazos cuya vida y la de su familia se encuentra en grave riesgo y se encuentran en estos* *momentos en condición de desplazados por la violencia".*

### La violencia contra líderes está también en el Huila

Por otro lado, en el departamento de Huila, fue asesinada la líder socia**l Mireya Hernández Guevara** en Algeciras; Hernández era representante comunitaria de la Despensa Agrícola Del Huila y tesorera de la Junta de Acción Comunal del municipio.

Según vecinos y conocidos, el asesinato se dio en la noche del pasado 8 de enero en el Barrio 20 de julio, mientras Hernández se movilizaba en una moto en compañía de su esposo, Julio Rojas cuando fue abordada por hombres armados quienes le dispararon en reiteradas ocasiones.

Luego del ataque, los agresores se dieron a la fuga, mientras la líder era conducida al hospital de Algeciras, sin embargo, según el informe de los médicos, Mireya llegó sin vida al centro de atención. (Le puede interesar: [Asesinan a lideresa Gloria Ocampo en Putumayo](https://archivo.contagioradio.com/asesinan-a-lideresa-gloria-ocampo-en-putumayo-2/))

En Algeciras el asesinato de la reconocida líder, conmocionó a la población que no solamente repudió el hecho, también solicitó a la Policía de la zona hallar a los responsables de su muerte. (Le puede interesar: [Plan pistola en marcha contra líderes indígenas: ACIN](https://archivo.contagioradio.com/plan-pistola-en-marcha-contra-lideres-indigenas/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo](http://bit.ly/1nvAO4u)][[Contagio Radio](http://bit.ly/1ICYhVU).]
