Title: Plan Maestro de Aprovechamiento del Río Magdalena: cuando se hizo y por quienes
Date: 2015-03-31 12:35
Author: CtgAdm
Category: Ambiente y Sociedad, Opinion
Tags: CENSAT, Hidrochyna, Río Magdalena
Slug: plan-maestro-de-aprovechamiento-del-rio-magdalena-cuando-se-hizo-y-por-quienes
Status: published

###### Foto: Jheisson López 

#### **[Margarita Flórez](https://archivo.contagioradio.com/margarita-florez/) – [Directora de] [Ambiente y Sociedad](http://www.ambienteysociedad.org.co/es/inicio/)** 

En una nueva modalidad según la cual la planificación territorial, y ambiental en Colombia se hace bajo parámetros, por compañías y con fondos extranjeros de acuerdo al convenio interinstitucional firmado en el 2009 entre la Agencia Presidencial para la Cooperación[ ]{.Apple-converted-space}-APC, la compañía Hidrochyna (desplazó 6 grupos de técnicos para el estudio), y el Banco Nacional de Desarrollo de China, para adelantar el proyecto de “*Formulación Del Plan Maestro de Aprovechamiento del Río Magdalena*”.

Así como se lee y oye ni más ni menos que el plan sobre el denominado río de la patria. Y todo esto se supo gracias a la presión de los movimientos sociales liderados por Ríos Vivos – Colombia ([i](Colombians%20start%20‘Big%20Mobilisation’%20to%20save%20the%20country’s%20principal%20river.%20Protesters%20gather%20to%20oppose%20potentially%20devastating%20hydroelectric%20dam%20and%20waterway%20plans%20for%20the%20River%20Magdalena%20HYPERLINK%20%22http://www.theguardian.com/environment/andes-to-the-amazon/2015/mar/14/colombians-big-mobilisation-save-countrys-principal-river%22%20\t%20%22_blank%22%20http://www.theguardian.com/environment/andes-to-the-amazon/2015/mar/14/colombians-big-mobilisation-save-countrys-principal-river))([ii](https://defensaterritorios.wordpress.com/)), Universidad Surcolombiana([iii](http://millerdussan.blogia.com/)),  CENSAT ([iv](Censat.org)) - Agua Viva que lanzaron el 14 de marzo una gran expedición por el alto Magdalena para informar a los ciudadanos sobre lo que está pasando.

Qué comprende, y a donde se aplicará el Plan Maestro? En la jurisdicción de CORMAGDALENA, lo cual equivale a:

-   129 municipios a lo largo de la corriente principal del Río Magdalena (1,613 km), el Canal del Dique (114 km) y el curso bajo del Río Cauca (187 km)
-   13 departamentos, incluyendo Cauca, Huila, Tolima, Cundinamarca, Caldas, Boyacá, Antioquia, Santander, Cesar, Bolívar, Sucre, Magdalena, y Atlántico.
-   El área de planificación es de 69,400 km2, que representa el 26% de la Cuenca, con una población de 6,075,000 en 2010, alrededor del 17% de la Cuenca.

**Para qué es el Plan Maestro([v](Plan%20Maestro%20de%20Aprovechamiento%20del%20Río%20Magdalena,%20Powerchina;%20Hidrochina%20%20%20Corporation;Corpmagdalena;Agencia%20Presidencial%20para%20la%20Cooperación%20-%20APC;%20Departamento%20Prosperidad%20%20%20Social-DPS))?**

Para determinar la navegación, generación de energía hidroeléctrica y la protección del ambiente, control de inundaciones, control de anegación y riego, uso de la zona ribereña, regulación del curso del río, uso los recursos pesqueros, y recreación.[ ]{.Apple-converted-space}Es decir para planificar todo el curso del río, y de buena parte de la cuenca.

**Principios:**

a\) El contenido principal del Plan Maestro debe ser de acuerdo con el Convenio. *El alcance del trabajo debe tener como referencia las normas estándar de China aplicables al plan integral de las cuencas hidrográficas, con las consideraciones necesarias a las condiciones específicas en Colombia, y a su vez adaptadas a la información y datos disponibles y recolectados* \[negrillas **fuera de texto**\]

b\) Se debe cumplir con las leyes Colombianas, reglamentos y políticas. *Salvo que las normas técnicas sean necesarias en Colombia, se aplicaran las normas técnicas de China*.

e\) Se debe aprender de la experiencia en formulación e implementación de la planificación integral de cuencas fluviales que tiene China.

Los temas principales del Plan Maestro son la Protección ambiental, la navegación, generación de energía hidroeléctrica, uso de la tierra (control de inundaciones, drenaje y riego), aprovechamiento de la zona ribereña, mejoramiento del canal del río, aprovechamiento de recursos pesqueros, recreación, y gestión integrada.

Esto se traduce más o menos en:

***Represas***: centrales hidroeléctricas de Oporapa (220MW), Guarapo (140MW) El Manso (140MW) y Nariño (200MW). Como se sostiene que presentan condiciones favorables de construcción, áreas reducidas de inundación, menores impactos ambientales, buenos indicadores económicos, escala apropiada y capacidad regulatoria como reservorios, se debe promover los estudios previos para la viabilización de su construcción.

***Puertos***; mejoramiento y equipamiento de 6 puertos Barranquilla, Cartagena, Gamarra, Puerto Galán, Puerto Berrio y Puerto Salgar.

Este Plan estuvo fuera del conocimiento de la ciudadanía dado que según las autoridades el reporte original estaba en mandarín y en inglés y por lo tanto hasta no traducirse no estaba disponible (ahora existe una versión no autorizada). Pero además de esa falta de transparencia y de acceso a la información en materia ambiental lo que nos preguntamos frente a esta modalidad de planeación donde quedan las funciones del Ministerio del Medio Ambiente y Desarrollo Sostenibles como ente rector de la política ambiental, y en donde queda la voz de los millones de habitantes que pudieran ser impactados directamente, e indirectamente por las obras previstas, cuando se lee que el Plan ya está formulado y que se priorizarán los proyectos que se inscriban dentro de sus lineamientos.

La modalidad de financiación china para apalancar sus empresas cuando operan en extranjero ya ha sido probada en Ecuador (Refinería del Pacífico, sin que se hay desembolsado ya ha causado impactos sociales en la zona);la mina de Rioblanco en el norte del Perú, en fin es la no tan nueva modalidad imperante en la cooperación internacional pero que adquiere ribetes preocupantes cuando se continúa sin informar de manera previa, oportuna, y eficiente; y si la opinión de los habitantes se relega a audiencias de última hora.

Por ahora el movimiento ciudadano en defensa del río Magdalena merece el apoyo de todos los ambientalistas , para exigir que nos digan cómo se piensa compatibilizar el mandarín de las normas técnicas chinas con las reglamentaciones nacionales, las cuales se aplicarían sólo si estas son compatibles, según reza el documento.

Plan Maestro de Aprovechamiento del Río Magdalena, Powerchina; Hidrochina Corporation;Corpmagdalena;Agencia Presidencial para la Cooperación - APC; Departamento Prosperidad Social-DPS
