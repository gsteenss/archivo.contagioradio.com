Title: Bosque Bavaria es la compensación del daño ambiental causado por la empresa
Date: 2017-09-22 15:43
Category: Ambiente, Nacional
Tags: Bosque de Bavaria, Secretaría de Ambiente
Slug: bosque_bavaria_tala_arboles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/defensa-de-los-arboles-bavaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Cortesía habitantes de Kennedy] 

###### [ 22 Sep 2017] 

Los ciudadanos que se oponen a la destrucción del conocido Bosque de Bavaria en el occidente de la capital colombiana seguirán insistiendo en la defensa de este patrimonio natural a pesar de que un **juez de primera instancia dio vía libre a la tala de 350 árboles.**

La decisión del juez se toma tras la petición de Bavaria a la Secretaria de talar los eucaliptos. Los argumentos con lo que se avala esa acción son **la inestabilidad, la edad, y que no son una especie nativa que podría afectar el suelo de la Sabana de Bogotá.**

Sin embargo no se ha tenido en cuenta que, por lo menos en la práctica, el bosque existía como mecanismo de mitigación y restauración ambiental dado el alto impacto del funcionamiento de la empresa en el sector durante más de 40 años.** **(Le puede interesar: ["Siguen talando los árboles, y uno siente que le están desmembrando un brazo"](https://archivo.contagioradio.com/nadie_responde_por_tala_de_arboles_bavaria/))

### **Proyecto de vivienda en Bavaria** 

Manuel Samiento, concejal del Polo Democrático Alternativo, asegura que más bien esta situación se debe a un conflicto de intereses que tienen que ver con la posibilidad de llevar a cabo un proyecto de vivienda en esa zona, que ya había estructurado desde la administración distrital pasada el hoy secretario de planeación de Enrique Peñalosa, Andrés Ortiz.

Esos intereses se ven reflejados en esta situación que hoy tiene en vilo a la comunidad, que asegura, perdería uno de los pocos pulmones del suroccidente de la capital, sumado a que acabaría con el hogar de cientos de aves.

Por su parte, la perspectiva del **concejal del Polo Democrático es que "desde el punto de vista ambiental y urbanístico es un absurdo".** Primero porque se acabaría con una de las pocas zonas verdes que produce oxígeno para un sector altamente contaminado de Bogotá, y segundo porque la construcción de 12 mil viviendas en una zona altamente transitada y congestionada, empeoraría aún más la movilidad por avenidas como Las Américas y la Boyacá.

Aunque denuncian que el distrito no quiere escuchar a las ambientalistas y a la comunidad, el concejal indica que es necesario seguir movilizándose para que la alcaldía detenga el desarrollo de este proyecto, al que aún **no se  ha definido es la constructora que harían parte de este proyecto.**

<iframe id="audio_21041617" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21041617_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
