Title: 115 personas serán desalojas forsozamente por Hidroituango
Date: 2015-02-09 17:47
Author: CtgAdm
Category: Ambiente, DDHH, Economía
Tags: Ambiente, EPM, Hidroituango, Río Cauca
Slug: 115-personas-seran-desalojas-forsozamente-por-hidroituango
Status: published

##### [Foto: debatehidroituango.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4058300_2_1.html?data=lZWimpiUdI6ZmKiakpqJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZKpjNXS1NjTssLnjNjS1Iqnd4a1ktOYxsrXpc3jy8bgjcvTttTj28bax9PYqYzk0NeYqs7IttDd1dqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Humberto Pino] 

Durante el transcurso de este lunes, **alrededor de 115 personas serán desalojadas forzosamente de la playa la Arenera Cañón del Río Cauca**, por cuenta del proyecto hidroeléctrico Hidroituango en Antioquia, desarrollado por Empresas Públicas de Medellín, EPM.

El vocero de una de las comunidades víctimas de desalojo, Humberto Pino, denunció en los micrófonos de Contagio Radio que “desde diciembre se ha presentado un amparo policivo para desalojar la comunidad por hidroituango (…) **estamos esperando que hoy se realice el desalojo,  ya todo está listo, solo se espera la orden del día y la hora”.**

De acuerdo al vocero de la comunidad, las familias que van a  ser desalojadas, son personas que han sido víctimas de otros desalojos por  la construcción del  mismo proyecto hidroeléctrico, y además, muchas de ellas han sido **desplazadas por conflicto armado.**

Pese a que la comunidad está a la espera del desalojo forzado, Humberto Pino, señaló que se había llegado a unos acuerdos con instituciones gubernamentales, para que la empresa empezara  una verificación de la obra con las personas del sector,  **“estamos a la espera de que se cumpla esas garantías”,** aseguró el líder comunitario.

Cabe recordar, que **ya van más de 400 familias desplazadas forzadamente** de las playas del Río Cauca, razón por la cual, el movimiento Ríos Vivos inició la campaña, “Ni un desalojo más por represas en Colombia”, para que en Colombia no se realizasen más desalojos forzosos por proyectos mineros e hidroeléctricos. ([Ver nota relacionada](https://archivo.contagioradio.com/actualidad/van-mas-de-400-familias-desalojadas-forzosamente-por-hidroituango/))
