Title: El debate del siglo: ¿Qué piensa el Gobierno de la renuncia del fiscal Nestor Humberto Martínez?
Date: 2018-11-27 17:13
Author: AdminContagio
Category: Judicial, Nacional
Tags: Debate de Control Político, Fiscal, Odebrecht, Robledo
Slug: debate-del-siglo
Status: published

###### [Foto: @JoseJorgeLopezA] 

###### [27 Nov 2018] 

Tres debates de control político han tenido como personaje principal a **Nestor Humberto Martínez**, los dos primeros, buscando que el Fiscal se declarara impedido para investigar el caso de corrupción que involucra a Odebrecht; y el tercero, calificado como el debate del siglo, en el que se pedirá al Gobierno explicar su posición frente a una eventual renuncia.

El debate del siglo fue citado por 3 senadores de la oposición: **Angélica Lozano, Jorge Robledo y Gustavo Petro**; quienes dispondrán de cerca de 40 minutos para exponer sus ideas sobre por que debería renunciar el fiscal Nestor Humberto Martínez. Al encuentro está citada la ministra de interior Nancy Patricia Gutiérrez, como representante del Gobierno, quien deberá responder una pregunta: **¿Cuál es la posición del Gobierno sobre la renuncia del fiscal NHM?**

Aunque en esta ocasión Martínez no está obligado a presentarse ante el Senado, el debate será particular, puesto que es la primera vez que el Fiscal se confrontará a quienes han pedido su renuncia, **luego de que se hicieran públicos audios que prueban su conocimiento de la corrupción de la multinacional brasileña en el proyecto Ruta del Sol II**. (Le puede interesar: ["Coger al fiscal diciendo una verdad es una hazaña: Jorge Enrique Robledo"](https://archivo.contagioradio.com/coger-al-fiscal-diciendo-una-verdad-es-una-hazana-jorge-robledo/))

Ante la propuesta de tener un fiscal 'Ad Hoc' que investigue específicamente este caso, la oposición en general ha adoptado una posición negativa, y **muchos han pedido que NHM se aparte del ente acusador**, permitiendo que las investigaciones se desarrollen con un manto de 'legitimidad'. (Le puede interesar: ["¡Que renuncie el Fiscal"](https://archivo.contagioradio.com/que-renuncie-el-fiscal/))

Puede ver en este link la transmisión del debate: https://bit.ly/2zuNyB4

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
