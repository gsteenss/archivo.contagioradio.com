Title: Otra Mirada: Las caras de la crisis económica
Date: 2020-09-18 11:59
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: crisis económica, pandemia, sector agrícola
Slug: otra-mirada-las-caras-de-la-crisis-economica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Emboladores-parte-de-economía-informal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Ang Moreno

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La pandemia ha dirigido al país a una crisis económica en la que los realmente afectados han sido los sectores más vulnerables no solo de la ciudad sino támbién de las zonas rurales del país. Además, factores como la desigualdad social, el hambre, y los intereses del Gobierno Nacional se hacen más evidentes y latentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante este programa de Otra Mirada sobre las diferentes caras de la crisis económica en el país, participaron Carmenza Saldías, exsecretaría de Hacienda de Bogotá,  Jorge Cortés, primer vicepresidente de la Central Unitaria de Trabajadores de Colombia y Oscar Gutiérrez, director ejecutivo nacional de Dignidad Agropecuaria Colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Primeramente, los invitados comparten cuál es la situación de las zonas rurales a nivel económico desde que inició la pandemia, señalando que a pesar de que ya la pandemia profundizó e hizo más evidente la crisis económica, desde hace mucho tiempo ya estaba sucediendo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explican que mientras se pretendía hacer un prestamo a la aerolínea Avianca, es preocupante el presupuesto tan bajo que se va a dirigir a sectores como la salud, educación, cultura, investigactivo y agrícola. (Le puede interesar: [Sí hay recursos para Avianca pero no para sectores más vulnerables](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/)) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, Carmenza comenta que durante la crisis económica son las las mujeres otro de esos grupos que se han visto afectados y explica que seguramente desde el inicio se pudo haber hecho algo para evitar los rezagos que ya está dejándo la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, describen cuáles son las verdaderas caras de las crisis económica y señalan que el estallido social va a ser muy grave si el gobierno nacional no toma acciones inmediatas. (Si desea escuchar el programa del 16 de septiembre: [Otra Mirada: Informes de inteligencia y estigmatización vs movilización social](https://www.facebook.com/contagioradio/videos/323718368744179))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/782317212310237)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
