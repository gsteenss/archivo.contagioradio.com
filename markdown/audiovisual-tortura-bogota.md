Title: Una muestra de cine contra la tortura se verá en Bogotá
Date: 2017-06-22 16:34
Category: eventos
Tags: Cine, colombia, Cultura, tortura
Slug: audiovisual-tortura-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/elbailerojo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CCCT 

###### 22 Jun 2017 

En el marco de la conmemoración del Día Internacional en apoyo a las víctimas de tortura, se presentará en la Cinemateca Distrital de Bogotá una muestra  audiovisual que presenta desde diferentes perspectivas la incidencia que este flagelo ha tenido en la construcción social y política de Colombia y otros países del mundo.

Una invitación de la Coalición Colombiana contra la Tortura (CCCT),  que recoge 8 producciones, entre documentales y argumentales, que serán exhibidas en el sábado 24 y el martes 27 de junio en funciones de 3p.m., 5 p.m. y 7 p.m. (Le puede interesar: [La muestra internacional documental de Bogotá abre convocatorias](https://archivo.contagioradio.com/midboconvocatoria/))

<iframe src="https://www.youtube.com/embed/3HDC0a7nmSc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Los documentales "**Falsos Positivos**", "**Huellas que no callan**" y "**El baile rojo**: la memoria de los silenciados" de Colombia, "Granito: cómo atrapar a un dictador" de Guatemala, "La mirada del silencio" de Italia, "El acto de matar" coproducción entre Noruega, Dinamarca y el Reino Unido, desde Medio Oriente la nominada al Oscar como mejor película en habla no inglesa "**Incendies**" y la ganadora en la misma categoría "**La historia oficial**" de Argentina, son las producciones que componen la muestra.

![Programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DC7UX90XYAA3Vx1.jpg){.alignnone .wp-image-42701 .aligncenter width="459" height="634"}  
<iframe src="https://www.youtube.com/embed/BibM13-Jd8E" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
