Title: Personas transexuales podrán cambiar el género de su cédula
Date: 2015-06-16 16:49
Category: LGBTI, Nacional
Tags: Cambio de género, Closet Abierto, Derechos Humanos, Identidad de Género, Identidad sexual, LGBTI, Ministro de Justicia, Transexuales, Yesid Reyes
Slug: personas-transexuales-podran-cambiar-el-genero-de-su-cedula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Trans.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: sentiido.com 

##### [16 Jun 2015] 

Según el Decreto **1227**, las personas tarnsexuales acudirán a un notario para solicitar el cambio de sexo en sus documentos de identidad. En este trámite NO se podrá exigir pruebas distintas a la declaración juramentada del solicitante. En esa declaración basta indicar la voluntad de corregir el componente sexo. Nada más y nada menos.

El **Ministro de Justicia Yesid Reyes** se refirió favorablemente a este cambio: así entonces, este Gobierno realiza un avance histórico: ha decidido levantar esa barrera (a veces infranqueable) para que la población trans pueda tener una cédula de ciudadanía que refleje su identidad sexual.

Así las cosas, por primera vez,  personas **transexuales** logran realizar el cambio de género en su cédula de ciudadanía, por medio de un trámite notarial sin necesidad de someterse a una supuesta disforia de género para que fuese aprobado.

Antes, las personas **transexuales** debían certificar de alguna manera que tenían un trastorno de **disforia de género** o haberse sometido a un cambio de genitales por procedimientos quirúrgicos para al menos, comenzar el trámite, corriendo el riesgo de que se negara su solicitud.

Más allá de que se cambiara el género en su cédula, esto traía problemáticas en otros contextos, por ejemplo se negaba la posibilidad de acceder a un trabajo estable, por el hecho de que su género no coincidiera con su apariencia; la salud y la educación, eran otros de los ámbitos de vital importancia que afectaban a las personas trans, este avance cambiará considerablemente la situación.
