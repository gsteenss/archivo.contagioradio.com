Title: Es asesinado Guildon Solís, líder afro del Cauca
Date: 2020-12-06 07:11
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Cauca, líder afrocolombiano, Líder Social, NosEstanMatando
Slug: es-asesinado-guildon-solis-lider-afro-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 5 de diciembre del 2020 en horas de la mañana **fue asesinado el líder comunal Guildon Solís Ambuila, en el municipio de Buenos Aire en el departamento del Cauca**. Con él son 280 las y los líderes asesinado durante este año.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1335335584148185088?s=20","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1335335584148185088?s=20

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Guildon Solis Ambuila era un líder social de la comunidad Munchique y **durante las últimas semanas acompañaba a las [víctimas de la masacre](https://renacientes.net/blog/2020/09/21/continua-entocidio-en-colombia-masacre-en-la-vereda-munchique-municipio-de-buenos-aires-cauca/)cometida el pasado 20 de septiembre en la vereda Munchique**, municipio de Buenos Aires, en donde fueron masacradas 7 pobladores este territorio ancestral.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Previo a su asesinato , veinte días atrás el líder Guildon Solís **había recibió amenazas de muerte por parte de *"un comandante de la guerrilla y disidentes, quienes le advirtieron lo iban a asesinar"***, esto según información suministrada por el medio local, Meridiano Cauca. ([Líder juvenil y ambiental asesinado en Quibdó, Choco](https://archivo.contagioradio.com/lider-juvenil-y-ambiental-asesinado-en-quibdo-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron que ante la amenaza, líderes de este territorio habían solicitado atención a la Unidad Nacional de Protección, quienes indicaron que para ejecutar esta acción, el líder debía ser trasladado a Bogotá. Mudanza que no fue aceptado por Solís, ***" decidió permanecer en la zona, con el fin de luchar por los derechos de las víctimas de las masacres registradas anteriormente".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta nueva agresión en contra de los liderazgos social del Cauca, se concreta según Instituto de Estudios para el Desarrollo y la Paz -Indepaz-, como en asesinato **280 en todo el territorio nacional durante el 2020, de esto 90 casos corresponden al departamento del Cauca** en donde se registra un récord de agresiones, amenazas y asesinatos de líderes y lideresas sociales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
