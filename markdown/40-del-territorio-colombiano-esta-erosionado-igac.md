Title: 40% del territorio colombiano está erosionado: IGAC
Date: 2016-08-31 16:34
Category: Ambiente, Nacional
Tags: Amazonía, cambio climatico, deforestación
Slug: 40-del-territorio-colombiano-esta-erosionado-igac
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/deforestacion-e1472679143710.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Nacional 

###### [31 Ago 2016] 

**“El 40% del país ya padece de algún grado de erosión y el 15,6% de los suelos está afectado por la sobrecarga de ganado y cultivos”**, es una de las conclusiones más alarmantes que tiene la última publicación del Instituto Geográfico Agustín Codazzi, “Suelos y Tierras de Colombia”, donde se indica que entre 1967 y 2012 el país le dijo adiós a una cobertura boscosa tan grande como el departamento de Cundinamarca, es decir **2,4 millones de hectáreas.**

Juan Antonio Nieto Escalante, director General del Instituto Geográfico Agustín Codazzi, IGAC, señala que “La cobertura boscosa nacional muestra una tendencia regresiva en extensión de masa forestal, tendencia que se aceleró entre finales de los años 60 y la mitad de los 80. La desaparición de los bosques está ligada a la tala indiscriminada, a la minería ilegal, a la explotación desmesurada de los recursos naturales y al establecimiento de otros usos como los agrícolas y pecuarios, los cuales no son aptos para estas zonas”.

**Hace 45 años el 60% de todo el país era bosque,** y cubría los departamentos de Amazonas, Putumayo, Vaupés, Guaviare y Guainía, y gran parte del Chocó, Caquetá, Arauca y Nariño. Sin embargo, para 2014 la tasa de deforestación alcanzó el 16%, y en 2015, la Procuraduría General de la Nación aseguró en 2015 que el 61% de los municipios colombianos albergar alguna actividad minera, lo que ha profundizado la pérdida de bosques, por cuenta no solo de la minería sino también de las actividades agrícolas, pecuarias y la ganadería.

En 2012, los bosques se redujeron a 65.767.667 hectáreas (el 58% del país), representados en 60,7 millones de hectáreas de bosques primarios y secundarios y 5 millones de bosques fragmentados con pastos y cultivos.

**La mayoría de los suelos de la Amazonia son los más afectados por la deforestación,** pese a que los países que comprende la la selva amazónica conocen del alto potencial de esta zona como solución natural frente a la problemática del cambio climático.

“La mayor pérdida de bosque en Colombia se originó entre los años 60 y la mitad de los 80, con la reducción de más del 13% de la cobertura boscosa nacional. E**ntre 1985 y 2012 se registró una aparente disminución de la tendencia de destrucción**. Esta desaceleración de la tala de bosques en la última década está asociada posiblemente con aspectos socioeconómicos y variantes del orden público”, concluye Nieto Escalante.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
