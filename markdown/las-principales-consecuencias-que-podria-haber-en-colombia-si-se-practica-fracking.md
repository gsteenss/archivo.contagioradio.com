Title: Las principales posibles consecuencias del fracking
Date: 2017-08-25 12:26
Category: Ambiente, Voces de la Tierra
Tags: fracking, Ministerio de Ambiente
Slug: las-principales-consecuencias-que-podria-haber-en-colombia-si-se-practica-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/fracking-e1487780191287.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Al Jazeera 

###### [24 Ago 2017] 

En unas semanas se conocerá la hoja de ruta que ha definido el Gobierno colombiano para implementar el fracking, alegando que se **asegurando que ya se tienen lineamientos para "el desarrollo sostenible"** necesarios. No obstante, las comunidades ubicadas alrededor de los 22 bloques petroleros y en los que se podría explotar crudo mediante fracturamiento hidráulico temen por los impactos que en otros países han sido evidentes.

### Fuentes hídricas 

Las imágenes de Austalia donde se prende fuego a un río y este se enciende, o en Texas, Estados Unidos, donde se abre el grifo y al acercarse un poco de fuego enseguida no cae un chorro de agua sino de fuego, preocupan a los ambientalistas. Que se practique fracking en un lugar donde el **75% de los recursos hídricos se encuentran de manera subterráneas, podría aumentar el riesgo de que esas mismas imágenes se vuelvan realidad, pues las las fuentes hídricas podrían contaminarse de gas metano, y otras sustancias químicas que son usadas para fracturar la tierra y obtener petróleo.**

Aunque Colombia estudia la manera de reducir las tasas de material radioactivo en el agua que sale residual del fracking, no se ha logrado en ninguna parte del planeta disminuir esos índices hasta obtener los niveles aceptados por los entes de salud pública del mundo, para que se pueda demostrar que esa técnica se puede realizar de manera responsable, según explica  Camilo Prieto, vocero del Movimiento Ambientalista Colombiano.

Asimismo, preocupa le excesivo gasto de agua. De acuerdo con Prieto, el fracturamiento hidráulico requiere de entre **9 y 26 millones de litros de agua por pozo y año, pero** **solo puede reutilizar un 15% de esa agua. El** ambientalista afirmaba le año pasado que en **el país no existen estudios de geotectónica amplios.** Los análisis de hidrogeología son completamente limitados, lo que quiere decir, que no hay mapas geológicos certeros sobre la situación en la que se encuentra la zona donde se pensaría realizar el Fracking, y si llega a haber una fuente de agua subterránea donde se lleve a cabo esa actividad, sería muy posible que esta termine con **residuos tóxicos y sustancias radioactivas que pueden ocasionar cáncer, o el gas metano puede generar que el agua se vuelva inflamable. **[(Le puede interesar: Informe revela impactos del fracking en América Latina)](https://archivo.contagioradio.com/impactos-del-fracking-en-6-paises-de-latinoamerica/)

### Actividad sísmica 

Según el Servicio Geológico de Kansas **el fracking está directamente relacionado con el aumento en la actividad sísmica**de ese Estado. La entidad, vincula los terremotos ocurridos en la zona con la inyección de aguas residuales del fracking. Se denuncia que el **Estado registró más de 120 terremotos durante 2014 frente a ninguno en el 2012. Una situación que podría repetirse en Colombia.**

### Impactos sobre el Paisaje 

Una de las apuestas de las comunidades que precisamente han decidido y quieren cerrarle las puertas a **actividades mineras y petroleras como el fracking, tienen que ver con el ecoturismo, c**omo alternativa al desarrollo basado en el extrativismo, sin embargo el fracturamiento hidráulico sería una grave amenaza para esa propuesta. Según el Informe solicitado por el Parlamento Europeo y el Informe del Tyndall Centre de la Universidad de Manchester, esta forma de **explotar crudo requiere aplanar una superficie de más o menos una hectárea, allí habría espacio para 6 a 8 pozo**s, balsas de almacenamiento de líquidos de desecho y lodos, tanques y cisternas de almacenamiento del agua y de los productos químicos, equipo de perforación, camiones, etc; todo ello afecta el paisaje de la zona.

### Contaminación del Aire 

<div class="gmail-content gmail-clearfix">

<div class="gmail-field gmail-field-name-body gmail-field-type-text-with-summary gmail-field-label-hidden">

<div class="gmail-field-items">

<div class="gmail-field-item even">

De acuerdo con los científicos el proceso de fracking para extraer gas implica **inevitablemente fugas de gas natural, "que es 20 veces más potente que el dióxido de carbono como gas de efecto invernadero**", según la Asamblea contra la fractura hidráulica de Cantabria. Eso se ha demostrado con el caso de Fort Worth, una ciudad de 750.000 habitantes en Dallas, Estado Unidos, "según un estudio de la Southern Methodist University de 2008, la extracción de gas de pizarra generaban más esmog que todos los coches, camiones y aviones de la región de Dallas-Fort Worth, una conurbación de más de seis millones de habitantes", explica la Asamblea.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

</div>

</div>

</div>

</div>
