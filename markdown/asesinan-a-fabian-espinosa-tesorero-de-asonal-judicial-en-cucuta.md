Title: Asesinan a Fabian Espinosa, tesorero de Asonal Judicial en Cucuta
Date: 2015-06-01 17:23
Category: DDHH, Nacional
Tags: Asonal Judicial, Central Unitaria de Trabajadores de Colombia, Cucuta, Fabian Espinosa, MOVICE
Slug: asesinan-a-fabian-espinosa-tesorero-de-asonal-judicial-en-cucuta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Asonal-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cablenoticias 

<iframe src="http://www.ivoox.com/player_ek_4586252_2_1.html?data=lZqlmJeZdo6ZmKiak5eJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9OPpYy6wsfWw9OPidTkytPc1caJdqSf1crg0dfJttCfxcqYo9jTssLgjK%2Fixs7HrcLgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Nubia Mendoza integrante del MOVICE] 

###### [2Jun2015] 

El pasado fin de semana fue asesinado en la ciudad de Cúcuta, **Fabian Espinosa, tesorero de Asonal Judicial y defensor de Derechos Humanos**. El cuerpo sin vida fue encontrado en su lugar de residencia este lunes, a pesar de que las primeras indagaciones darían cuenta de que el asesinato se produjo durante el fin de semana.

[![fabian espinosa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/3827094934001_4269138034001_SINDICALISTA.jpg){.size-full .wp-image-9534 .alignleft width="312" height="206"}](https://archivo.contagioradio.com/asesinan-a-fabian-espinosa-tesorero-de-asonal-judicial-en-cucuta/3827094934001_4269138034001_sindicalista/)Desde el 2011 **Fabian venía denunciando amenazas de muerte en su contra**, y había solicitado su traslado al municipio de Los Patios en el 2013. Hace unas semanas, denunció ante el CTI que recibió llamadas intimidatorias en las que “le pedían que se quedara callado y dejara de hacer comentarios” según informó el diario El País.

Nubia Mendoza, integrante del MOVICE, afirma que las declaraciones del comandante de la policía de Cúcuta, en el sentido de que Fabian fue asesinado por ser integrante de la comunidad LGBTI son irresponsables puesto que la semana pasada el sindicalista advirtió su necesidad de protección por parte de las autoridades dadas las reiteradas amenazas en su contra.

Para los integrantes del **MOVICE**, organización cercana a Espinosa en esa zona del país, la situación de los defensores y defensoras de Derechos Humanos es preocupante, puesto que **en lo corrido del 2015 ya han sido asesinados tres defensores en el casco urbano de la ciudad de Cúcuta**.
