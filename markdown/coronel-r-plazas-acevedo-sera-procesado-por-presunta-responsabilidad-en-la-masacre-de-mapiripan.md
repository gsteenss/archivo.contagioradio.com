Title: Coronel (r) Plazas Acevedo será procesado por presunta responsabilidad en la Masacre de Mapiripán
Date: 2016-05-25 16:07
Category: Judicial, Nacional
Tags: Asesinato Jaime Garzón, Coronel Plazas Acevedo, Masacre Mapiripán, Paramilitarismo
Slug: coronel-r-plazas-acevedo-sera-procesado-por-presunta-responsabilidad-en-la-masacre-de-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Plazas-Acevedo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [25 Mayo 2016 ]

El coronel (r) Jorge Eliécer Plazas Acevedo ha sido llamado a juicio por la Fiscalía, según la investigación que cursa en su contra y el testimonio del excomandante paramilitar Raúl Hazbún, alias 'Pedro Bonito', el oficial **recibió dinero por omitir los controles para permitir la movilización de paramilitares** de las AUC que perpetraron la masacre de Mapiripán, entre el 15 y el 20 de julio de 1997.

Plazas Acevedo será **procesado por los delitos de homicidio agravado, secuestro extorsivo, terrorismo y concierto para delinquir**, al mismo tiempo que enfrenta un proceso penal por homicidio agravado como coautor del crimen que se cometió contra Jaime Garzón Forero, en Bogotá, el 13 de agosto de 1999.

El 31 de julio de 2014, el coronel retirado fue recapturado, tras **evadir durante 11 años a la justicia**, que en ese momento lo juzgaba por su responsabilidad en el secuestro y homicidio del empresario israelí, Benjamín Khoudari, por el que lo sentenció a 40 años de prisión.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

 
