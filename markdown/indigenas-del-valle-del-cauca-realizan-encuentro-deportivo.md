Title: Indígenas del Valle del Cauca realizan encuentro deportivo
Date: 2015-01-05 14:14
Author: CtgAdm
Category: Comunidad
Slug: indigenas-del-valle-del-cauca-realizan-encuentro-deportivo
Status: published

###### Fotografía: Comisión Intereclesial de Justicia y Paz 

###### [Descargar Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsRddHyD.mp3) 

En el Resguardo Santa Rosa de Guayacán, en Valle del Cauca, se lleva a cabo desde el 8 de agosto un encuentro deportivo-cultural entre diferentes comunidades de la región. Para los y las indígenas del Valle del Cauca les interesa mostrar que estas comunidades también juegan fútbol.
