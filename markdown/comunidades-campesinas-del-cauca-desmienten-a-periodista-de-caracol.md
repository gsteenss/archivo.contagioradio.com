Title: Comunidades campesinas del Cauca desmienten a periodista de Caracol
Date: 2016-06-08 17:10
Category: Paro Nacional
Tags: Canal Caracol, Cumbre Agraria, Minga Nacional, Paro Nacional
Slug: comunidades-campesinas-del-cauca-desmienten-a-periodista-de-caracol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/minga-cajibio-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: aconc] 

###### [8 Jun 2016] 

Las comunidades afirman que el periodista de Caracol siempre tuvo una **actitud provocadora, se rehusó a identificarse y desmintieron que haya sido detenido o insultado** como lo afirmó el periodista en la emisión del medio día. Los campesinos afirmaron que el reportero se negó a identificarse y por ello se le solicitó que se devolviera a la ciudad de Popayan y se le impidió el paso por el sitio del bloqueo.

Según lo transmitido en el noticiero de televisión de [Noticias Caracol](https://archivo.contagioradio.com/?s=caracol) el periodista Fredy Calvache habría sido retenido durante media hora por las comunidades que realizan el bloqueo de la vía panamericana a la altura del “Tunel” en el municipio de Cajibio en Cauca en el marco de la **[Minga Nacional](https://archivo.contagioradio.com/programas/paro-nacional/) que se desarrolla en todo el país.**

Según el comunicado los periodistas de Caracol y RCN que han realizado cubrimientos en protestas anterirores han servido para **desinformar al público** pero no para comunicar las exigencias de las comunidades campesinas.

Reproducimos el contenido del comunicado emitido por la comisión de DDHH de la Cumbre Agraria que hace presencia en el sitio de los hechos.

\[embed\]https://www.youtube.com/watch?v=TtpdAsXXU88\[/embed\]

##### ["[ANTE LA DESINFORMACIÓN POR LOS HECHOS EN LOS QUE SE VIO INVOLUCRADO EL PERIODISTA DE CANAL CARACOL FREDDY CALVACHE.](http://www.reddhfic.org/index.php?option=com_content&view=article&id=1761%3A2016-06-08-18-11-58&catid=181%3Aactualidad-2016&Itemid=413)] 

##### [[ANTE LA DESINFORMACIÓN ](https://youtu.be/TtpdAsXXU88)] 

##### Las comunidades campesinas, indígenas y afrodescendientes concentradas sobre el sitio conocido como entrada a El Túnel del Municipio de Cajibio en el Departamento del Cauca, aclaramos a la opinión pública nacional e internacional los hechos en los que se vio involucrado el periodista Freddy Calvache de la cadena Caracol. 

##### Aproximadamente a las 10:00 de la mañana del día 8 de junio del año 2016, al punto donde se mantiene el bloqueo permanente por parte de las comunidades movilizadas, llega un automóvil que no se identificó en el primer sitio de bloqueo. Al momento de ser requerido por los responsables de seguridad, el Señor Calvaché dejo sus pertenecías en el vehículo, rehusó a identificarse y empezó a tener actitudes provocadoras diciendo a los campesinos "Háganme lo que quieran" "Si quieren mátenme o amarrenme" "Solo la guerrilla amarra a la gente". 

##### Estos hechos de provocación sumados al recuerdo de la desinformación realizada por el Señor Calvache en el cubrimiento del Paro Nacional Agrario en el año 2013, en el cual señaló a los movilizados como responsables de los campesinos asesinados y heridos por la Fuerza Pública; produjeron indignación, a lo que se siguieron palabras en las que los movilizados le señalaban su carácter amarillista en el manejo de la información sobre protestas sociales en el departamento del Cauca. Posteriormente y sin haber sido requerido, ubico su teléfono celular en su entrepierna entre su ropa interior. 

##### Pese a las provocaciones, de manera pacífica los concentrados lo condujeron en dirección al primer punto de control del taponamiento, situación que en ningún momento tuvo como intención la retención del periodista, quien por sus propios medios empezó a caminar hacia la ciudad de Popayán. Ante estos hechos se activó una comisión de Derechos Humanos en la idea de verificar la situación y de garantizar el paso del Señor Calvache. La cual se dirigió al lugar de los hechos y avanzo a una distancia de 5 kilómetros sin lograr comunicarse con él. No obstante se han recibido informaciones que el señor Calvache se encuentra bien y en la ciudad de Popayán. 

##### Como de costumbre, la falta de ética periodística de este sujeto sale a relucir en un nuevo caso del famoso "Usted no sabe quién soy yo", al desinformar y engañar a la opinión pública hablando de una retención que jamás tuvo lugar. El respeto por el ejercicio periodístico y la libertad de prensa que han sido principios del el legítimo accionar de las comunidades movilizadas, puede ser verificado por todos los medios de comunicación que han llegado hasta este lugar para realizar el respectivo cubrimiento. 

##### A los medios de comunicación hacemos un fraternal y respetuoso llamado a informar desde la realidad de lo sucedido y no desde posturas apresuradas que pretenden distraer la atención de los verdaderos debates en relación a la necesidad de avanzar en confianzas y transitar hacia escenarios de negociación que pongan fin a los bloqueos. 

##### **COMUNIDADES CONCENTRADAS EN EL CORREGIMIENTO EL TÚNEL CAJIBÍO CAUCA** 

#####  8 DE JUNIO DE 2016" 
