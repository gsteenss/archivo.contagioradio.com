Title: Mientras Duque daba un discurso en el Congreso, 3 líderes fueron asesinados en Colombia
Date: 2019-07-21 18:29
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Arbey Ramón, Humberto Díaz, lideres sociales, Yamile Guerra
Slug: duque-lideres-asesinados-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este 20 de julio tres líderes fueron asesinados en diferentes regiones del país: **Yamile Guerra, en Floridablanca,** Santander; **Arbey Ramón, en Montañita,** Caquetá; y **Humberto Días, en Gigante,** Huila. Hechos que ocurrían mientras se instalaba la nueva legislatura, congresistas de la oposición invitaban a marchar por la vida de los y las líderes sociales, y el presidente Duque decía que durante su gobierno la tasa de homicidios contra estas personas había bajado.

Según las primeras informaciones**, Arbey Ramón era tesorero del Comité de Trabajo del caserío Miramar, en Montañita.** El Comité impulsaba la construcción de una carretera en la zona, y Ramón se disponía a encontrarse en una reunión para tratar el tema cuando fue asesinado en horas de la mañana. Por su parte, Yamile Guerra era una abogada y lideresa política en Floridablanca. (Le puede uinteresar: ["Lucero Jaramillo, activa promotora de sustitución de cultivos fue asesinada enCaquetá"](https://archivo.contagioradio.com/lucero-jaramillo-activa-promotora-de-sustitucion-de-cultivos-fue-asesinada-en-caqueta/))

De acuerdo a información oficia, la **Lideresa fue atacada sobre las dos de la tarde por hombres desconocidos, luego que ella les mostrase unos planos de terreno en su finca,** ubicada en el barrio Mirador de la Hacienda. Más tarde, en horas de la noche del sábado, **el líder Humberto Díaz Tierradentro fue víctima de una agresión por parte de sujetos armados que acabaron con su vida**; Díaz se desempeñaba como presidente de la vereda Guadalupe.

### **A líderes en Bollívar les dan 73 horas para abandonar el territorio**

En el Barrio Villa Aranjuez de Cartagena, Bolivar, las lideresas Rosa Marrugo y Clara Campo fueron amenazadas mediante un panfleto que les daba 73 horas para abandonar el territorio. El plazo para ambas presidentas de Juntas de Acción Comunal del sector se vencería en las próximas horas; razón por la que mostraron su preocupación, pues aunque las amenazas se han presentado hace cerca de un año, está sería la primera vez que se las intimida para que abandonen la zona.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
