Title: Maestros a paro este jueves en Bogotá
Date: 2019-05-29 14:29
Author: CtgAdm
Category: Educación, Movilización
Tags: educacion, Pliego, Profesores de Bogotá, secretaria de educacion
Slug: maestros-paro-jueves-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Educadores-irán-a-paro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Este jueves se espera que cerca de **30 mil maestros que hacen parte del sistema de educación oficial de Bogotá entren en paro durante 24 horas**, como forma de protesta al incumplimiento del pliego de peticiones presentado por los docentes el pasado 28 de febrero, en el que exigen soluciones ante las fallas en la implementación de la jornada única de colegios oficiales, la inclusión de cerca de 100 mil niños que están fuera del sistema educativo y la mejora de condiciones laborales para trabajadores de la educación.

Como explicó **William Agudelo, presidente de la Asociación Distrital de Educadores (ADE)**, el pliego fue radicado a finales de febrero y solo hasta el 11 de marzo se estableció una mesa de negociación; sin embargo, se trató de un "diálogo de sordos", porque la secretaria de educación Claudia Puentes, solo se presentó a la primera reunión de la mesa, y dejó delegados que no estaban interesados en resolver los problemas planteados por los profesores.

Según manifiesta Agudelo, algunos de esos problemas estarían relacionados con el Programa de Alimentación Escolar (PAE), que ha sido criticado por la calidad de los alimentos que reciben los estudiantes; la implementación de la jornada única que mantiene a los jóvenes en el colegio de 6 am a 3 pm, sin recibir una adecuada alimentación y la tercerización laboral, que vincula a los maestros con contratos que no garantizan su estabilidad laboral.

> Defensa Civil Colombiana Direccion Seccional Bogota DC Lideres Voluntarios apoyan emergencia por intoxicacion alinentaria Colegio Inem Localidad Ciudad Kennedy 35 Niños Valorados [pic.twitter.com/KZI8Bzpc0m](https://t.co/KZI8Bzpc0m)
>
> — EDERLEY TORRES (@ederleytorres) [17 de mayo de 2019](https://twitter.com/ederleytorres/status/1129445675815571457?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Adicionalmente, Agudelo se mostró en contra de la entrega de **colegios en concesión** (figura impulsada también por la anterior secretaria, y ahora ministra de educación, Maria Victoria Angulo); y manifestó la necesidad de ponerse de acuerdo sobre la cantidad de jóvenes excluidos de la educación oficial, que según las cuentas del distrito rondan los 65 mil estudiantes, pero para los educadores, podrían estar cerca de 100 mil.

En esa medida, el profesor señaló que la alcaldía de Enrique Peñalosa comete dos errores: el primero al cerrar el programa de jardín, trasladando su ejecución de la secretaria de educación a la secretaría de educación social; y al no realizar campañas para matricular más jóvenes a la educación distrital. (Le puede interesar: ["ADE denuncia represalias contra docentes que participaron en paro"](https://archivo.contagioradio.com/ade-se-reunira-con-secretaria-de-educacion/))

### **Maestros protestarán con clase a la calle** 

Por estas razones, y los 38 puntos no escuchados por Puentes que están incluidos en el pliego de peticiones, cerca de 30 mil maestros del distrito realizarán una jornada de 'clase a la calle' que se adelantará **el próximo jueves en horas de la mañana frente a la secretaría de educación, ubicada en la Calle 26, cerca a la Avenida 68**. (Le puede interesar: ["La Asociación Distrital de Educadores se toma la Secretaría de Educación"](https://archivo.contagioradio.com/la-asociacion-distrital-de-educadores-se-toma-la-secretaria-de-educacion/))

> [pic.twitter.com/RJwJ2RXXyl](https://t.co/RJwJ2RXXyl)
>
> — ADE (@adebogota) [28 de mayo de 2019](https://twitter.com/adebogota/status/1133398092110278656?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
