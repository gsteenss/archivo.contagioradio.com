Title: El primate más grande del mundo está punto de desaparecer
Date: 2016-04-04 18:18
Category: Animales, El mundo
Tags: especies en vías de extinción, Gorilas, República Democrática del congo
Slug: el-primate-mas-grande-del-mundo-esta-proximo-a-desaparecer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/foto-Brent-Stirton-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Brent Stirton 

###### [4 Abr 2016]

El primate más grande del mundo que habita en la República Democrática de El Congo, está a punto de desaparecer, según lo evidencia el más reciente informe de la Wildlife Conservation Society (WCS) y Fauna & Flora Internacional donde se concluye que **el 77% de la población de esta especie ha desaparecido.**

**La pérdida del hábitat por la deforestación, la caza ilegal alrededor de explotaciones mineras de coltán y los conflictos civiles** son las principales causas de la masiva pérdida de gorilas en el mundo, que pasó de un número de ejemplares **de 17.000 en 1995 a tener en la actualidad 3.800.**

Esta situación ha generado que se deba elevar el estado de amenaza del gorila de Grauer a ‘peligro crítico’. Pese a que estos animales están protegidos bajo medidas internacionales, su tamaño y carne son muy apetecidos para los cazadores ilegales.

Frente a esta situación desde Fauna y Flora Internacional se ha planteado que el plan a seguir para evitar la desaparición de esta especie, es **“formar, apoyar y equipar guardias ecológicos para combatir la caza furtiva de manera más eficaz;** construir redes de inteligencia, y apoyar a la estrecha supervisión diaria de las familias de gorilas para garantizar su protección; involucrar a los jefes tradicionales que detentan el poder tradicional en la región de educar a sus comunidades para detener la caza de estos simios".

El Parque Nacional de Kahuzi-Biega, La reserva Itombwe y la región de Tayna son los lugares claves para la protección de esta especie.
