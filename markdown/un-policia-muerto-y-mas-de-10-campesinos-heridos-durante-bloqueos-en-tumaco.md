Title: Un policía muerto y más de 10 campesinos heridos durante bloqueos en Tumaco
Date: 2017-04-03 17:24
Category: DDHH, Nacional
Tags: erradicación cultivos ilícitos, programas de sustitución de cultivos
Slug: un-policia-muerto-y-mas-de-10-campesinos-heridos-durante-bloqueos-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: TumacoPolo] 

###### [03 Abril 2017]

**Un policía muerto y más de 10 campesinos heridos es el saldo que deja la jornada de movilización de los campesinos en Tumaco**, que de igual modo denuncian que las protestas han sido infiltradas por sujetos que tienen otras intenciones y que serían estos lo que habrían provocado los desmanes y agresiones, además señalan que tampoco hay médicos o enfermeras que atiendan a las personas heridas.

Las marchas y bloqueos que adelantan los campesinos le exigían al gobierno el cumplimiento de los acuerdos pactados el pasado 4 de marzo, en donde se comprometía a **frenar la erradicación forzada de cultivos ilícitos**, en el transcurso del día se espera que ambas partes establezcan un escenario de diálogo que brinde garantías para los cultivadores de esta región. Le puede interesar: ["5 heridos dejan acciones del ESMAD en el corregimiento de Llorente Tumaco"](https://archivo.contagioradio.com/5-heridos-deja-accion-del-esmad-en-el-corregimiento-de-llorente-en-tumaco/)

De acuerdo con Iván Rosero, vicepresidente de la Junta de Acción Comunal de Llorente “**sí no hay un cumplimiento cabal en la ejecución de los acuerdos se presentan este tipo de situaciones**, tiene que haber una organización de parte del gobierno sobre cómo van a actuar para las comunidades campesinas que se acojan a los procesos de sustitución”. Le puede interesar: ["Plan de sustitución voluntaria no se está cumpliendo: COCCAM"](https://archivo.contagioradio.com/plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam/)

Los campesinos denuncian que Fuerzas Militares han ingresado a las fincas de pequeños productores de cultivos ilícitos, que ya hacen parte del programa de sustitución de cultivos, pasando por alto los pactos que se habían hecho con el Alto Comisionado para la Paz, Sergio Jaramillo, entre los que estaba **la realización de un censo de pequeños y medianos productores, actividades de pedagogía sobre la sustitución de cultivos ilícitos y luego una verificación** por parte del mecanismo tripartito del avance del programa.

<iframe id="audio_17932331" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17932331_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
