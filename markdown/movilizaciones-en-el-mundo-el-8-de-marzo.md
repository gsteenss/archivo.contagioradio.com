Title: El mundo gritó ¡No más violencia de género!
Date: 2017-03-27 11:30
Category: Libertades Sonoras, Mujer
Tags: 8 de marzo, feminicidios, Guatemala, Libertades Sonoras, mujeres
Slug: movilizaciones-en-el-mundo-el-8-de-marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [29 Mar. 2017]

En todo el mundo, durante la jornada del [**8 de Marzo miles de mujeres se unieron al "paro internacional de mujeres"**. Desde diversos lugares del mundo, las mujeres estuvieron rechazando la violencia de género, la falta de igualdad laboral, **los feminicidios,** la falta de justicia en los casos de feminicidios, entre otras importantes demandas. Le puede interesar: [Así fue el Paro de las Mujeres en el mundo](https://archivo.contagioradio.com/avanza-el-paro-de-las-mujeres-en-el-mundo/)]

Durante este segundo programa de Libertades Sonoras, tuvimos **un reporte de lo que aconteció no solamente en Colombia sino en varios lugares del país** con entrevistadas de organizaciones como el Comité de comunicaciones de Vivas nos Queremos desde Ecuador, quien dio cuenta de la jornada del 8 de marzo, en el que las mujeres se movilizaron. Le puede interesar: [Libertades Sonoras, un espacio para deconstruir estereotipos socioculturales](https://archivo.contagioradio.com/lanzamiento-programa-libertades-sonors/)

Estuvimos acompañadas de **Lorena Cabnal de la Red** [**de Sanadoras Ancestrales de Feminismo Comunitario,** quien nos compartió el dolor y la tristeza que rodearon los hechos en los que más de 40  niñas murieron a causa de un un incendio que ocurrió en un centro de menores de Guatemala, mientras protestaban por los abusos sexuales y físicos de los que eran víctimas. Le puede interesar: [Muerte de niñas en Guatemala "fue un crímen de Estado"](https://archivo.contagioradio.com/muerte-de-ninas-en-gutemala-fue-un-crimen-de-estado/)]

Así mismo, expuso la exigibilidad de justicia en la que están trabajando diversas organizaciones defensoras de DDHH y **han hecho responsable al Estado de Guatemala por esta masacre.** De igual modo, agradecieron la solidaridad mundial que se entretejió a raíz de este dramático suceso. Le puede interesar: [Aún no inician investigaciones en caso de 40 niñas de Guatemala](https://archivo.contagioradio.com/aun-no-inician-investigaciones-caso-40-ninas-guatemala/)

**Escuche aquí el programa completo**

<iframe id="audio_17808081" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17808081_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
