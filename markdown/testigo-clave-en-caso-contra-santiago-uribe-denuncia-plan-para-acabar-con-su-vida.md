Title: Testigo clave en caso contra Santiago Uribe, denuncia plan para acabar con su vida
Date: 2017-11-11 10:48
Category: DDHH, Nacional
Slug: testigo-clave-en-caso-contra-santiago-uribe-denuncia-plan-para-acabar-con-su-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/URIBE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Frente] 

###### [11 Nov 2017] 

El testigo de excepción Olwan de Jesús Agudelo Betancurt, quién fue **pieza clave en el proceso contra el empresario ganadero Santiago Uribe Vélez**, y además, fue integrante de la estructura de tipo paramilitar los “12 Apóstoles” denunció que habría un plan para acabar con su vida.

De acuerdo con Agudelo, que se encuentra en la Cárcel de Puertotriunfo, un poderoso empresario habría pagado para su ejecución al interior del centro carcelario. Coincidencialmente el detenido fue informado de un traslado del Patio 6 al Patio 5. **Patio en el que el testigo carece de garantías para su vida e integridad y posibilita la consumación del crimen**, sostuvo hace unos minutos. Al ser informado del traslado Agudelo exigió garantías para la protección de su vida y requirió ser trasladado al patio 4.

De igual forma, hace apenas una semana el abogado Daniel Prado, quien defiende a las víctimas de los “12 Apóstoles” denunció haber sido blanco de amenazas y hostigamientos, el último hecho se presentó cuando **Prado recibió un mensaje en su contestadora en el que le decían “H.P lo vamos a matar. **(Le puede interesar: ["Amenazan de muerte a abogado defensor de víctimas de los "12 Apóstoles"")](https://archivo.contagioradio.com/48740/)

**Noticia en desarrollo …**
