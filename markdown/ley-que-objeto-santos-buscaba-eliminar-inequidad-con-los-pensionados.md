Title: Santos incumplió promesa que hizo a los pensionados
Date: 2017-07-19 13:11
Category: DDHH, Política
Tags: aportes a salud, ley de aportes a salud, Pensionados, pensiones
Slug: ley-que-objeto-santos-buscaba-eliminar-inequidad-con-los-pensionados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/pensionados-e1500487692958.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Legal Corporation] 

###### [19 Jul 2017] 

El Gobierno de Juan Manuel Santos **dijo No a la ley que buscaba reducir los aportes a la salud del 12% al 4% de la población pensionada** del país. Ante esto, las y los pensionados manifestaron su preocupación y esperan que sea la Corte Constitucional la que avale la ley.

Para Victoria Escobar, vocera del Colectivo Feministas Pensionadas por los Derechos, “el gobierno se escuda **tras el argumento de que no tiene plata para financiar la reducción en los aportes**, pero el presidente del Senado dijo que la reforma tributaria se hizo para generar recursos para financiar impactos sociales como este”. Cabe resaltar que en la campaña reeleccionista, Juan Manuel Santos le había prometido a los pensionados del país que iba a reducir al 4% el aporte a la salud.

En efecto, uno de los argumentos que presentó el gobierno de Santos es que con la reducción del porcentaje a los aportes de salud de los pensionados, “se compromete la financiación del sistema de salud porque genera un impacto fiscal de 3.7 billones de pesos anuales”. Ante esto, Escobar manifestó que “**los aumentos a los salarios son por debajo del 12% que es el aporte que se hace a la salud** por lo que hay una angustia generalizada por si el dinero que tenemos alcanza para cada mes”. (Le puede interesar: ["Sistema de pensiones en Colombia favorece a los más ricos"](https://archivo.contagioradio.com/las-pensiones-en-colombia-benefician-a-las-entidades-privadas-desigualdad-en-el-pais-ambito-laboral-jubilacion/))

Adicionalmente, ella afirmó que “**el gobierno no nos está regalando nada, es un ahorro que hemos hechos a lo largo de muchos años de trabajar** para el país para que pudiéramos tener calidad de vida durante la vejez”. Por otro lado, Escobar recordó que el proceso de aprobación de la ley no termina con la objeción presidencial. “El proyecto va a regresar al Congreso para que sea sometido en plenaria y se discutan las objeciones y de ahí pasará a la Corte Constitucional quien dará la decisión final”.

### **Pensionados aportan 3 veces más a la salud que cualquier tabajador** 

La objeción a la ley que hizo el presidente Santos, significa para Juan Pablo Fernández, miembro de Justicia Tributaria en Colombia, **continuar con un sistema inequitativo que pone en desventaja a los pensionados.** Según él “hay una inequidad en el sentido de que los pensionados pagan el 12% únicamente en salud contrario al aporte del 4% que hace una persona en edad laboral”. (Le puede interesar: ["OCDE propone igualar edad de jubilación de hombres y mujeres"](https://archivo.contagioradio.com/ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres/))

Esto quiere decir que **un pensionado aporta una cantidad mayor al régimen de salud por los mismos beneficios que recibe cualquier trabajador** que hace un aporte inferior. De acuerdo con Fernández, “los pensionados aportan 3 veces más a la salud a pesar de que su salario es inferior al que tenían cuando trabajaban”. Bajo este panorama, los pensionados como Victoria Escobar, aún mantienen la esperanza de “poder terminar la vida con un ahorro digno”.

<iframe id="audio_19895289" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19895289_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
