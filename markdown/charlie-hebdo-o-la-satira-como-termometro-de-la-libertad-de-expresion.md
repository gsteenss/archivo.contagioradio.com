Title: Charlie Hebdo o la sátira como termómetro de la libertad de expresión
Date: 2015-01-30 18:06
Author: CtgAdm
Category: Hablemos alguito
Tags: atentados Paris, Charlie Hebdo, primavera árabe, racismo, xenofobia
Slug: charlie-hebdo-o-la-satira-como-termometro-de-la-libertad-de-expresion
Status: published

###### **Foto:ABC.es** 

#### **[Hablemos Alguito :]**  
<iframe src="http://www.ivoox.com/player_ek_4011750_2_1.html?data=lZWek5yZdI6ZmKiakpaJd6KklIqgo5iWcYarpJKfj4qbh46kjoqkpZKUcYarpJKwysbWsMrZjK3SxMnTb9CfzcaY1Yqnd4a1ktnW1MaPp9Dh0JDhx9fRaaSnhqegz8rYttCfxcqYzsaPsMrWxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Comenzamos nueva temporada de Hablemos Alguito, coincidiendo con la nueva página web y el nuevo estudio, estrenamos también nuevo formato para el programa.

Seguimos centrándonos en el análisis político nacional e internacional y nuestro programa mensual de colaboración con el Comité de solidaridad de presos políticos, al que este año añadiremos la voz a otras organizaciones sociales que trabajen el tema, con el objetivo de visibilizar la grave situación de violación de derechos humanos al interior de las cárceles de Colombia.

Después de los atentados de París contra la **revista satírica Charlie Hebdo, perpetrado por grupos islamistas radicales** , se han generado diversas opiniones encontradas al respecto de las causas que lo han producido.

También han salido ha flote antiguos **fantasmas de la vieja Europa como la xenofobia, el racismo, el clasismo** o la falta de integración social de diversos colectivos de la sociedad.

No es de extrañar que cada grupo político haya aprovechado para plasmar sus intenciones y objetivos sobre problemas enquistados en la propia sociedad.

Es por ello que con el programa de esta semana, queremos brindar a nuestros oyentes de **claves históricas, sociológicas y políticas** para que puedan entender y analizar la situación, creándose así, su propia opinión.

No nos olvidamos del papel de los medios de comunicación de masas y del papel que juegan a la hora de justificar acciones políticas, mediante la creación de opinión pública acorde a los intereses de los grupos empresariales que los financian.

Esta vez contamos en el diálogo **con Rafael Cid, analista político de distintos medios alternativos del estado español**, quién realiza una retrospectiva empezando por el papel de la sátira en las democracias occidentales, pasando por el pronunciamiento de la izquierda francesa e internacional ante lo acontecido y por supuesto investigando sobre la inmigración y los fenómenos sociales conocidos como xenofobia y racismo que están alimentando una creciente ultraderecha.
