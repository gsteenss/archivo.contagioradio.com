Title: Entregan alimentos descompuestos a guerrilleros de las FARC en zonas de preagrupamiento
Date: 2017-01-04 12:17
Category: DDHH, Nacional
Tags: FARC, ONU, zonas de concentración
Slug: entregan-alimentos-descompuestos-guerrilleros-las-farc-zonas-preagrupamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/farc-campamento-guerrilla-afp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [4 Ene 2017] 

[Las FARC denunció que los alimentos destinados para los guerrilleros en las zonas de preagrupamiento, están llegando en mal estado, e incluso algunos completamente descompuestos. Así lo evidencian las imágenes del ‘NC Noticias’, donde se muestran las **bolsas y empaques de los alimentos sucias, e incluso la sal vencida.**]

“Algunos alimentos están llegando regulares y otros ya en mal estado”, dice  Jairo Hernández, jefe de logística de las FARC-EP, quien agrega que es una situación preocupante porque pese a que se ha advertido **siguen llegando los alimentos podridos,** generando incluso, problemas de salud en algunos guerrilleros, que además no son atendidos debidamente.

[Así mismo, Rodrigo Londoño, jefe máximo de la guerrilla de las FARC, ha señalado que es necesario que la Cruz Roja Internacional y Médicos sin Fronteras, **realicen una inspección sanitaria de los alimentos que se encuentran en los campamentos** y los que están en las bodegas de la Agencia Nacional Logística.]

[Londoño también denunció que en  el punto de Buenos Aires, en el departamento del Cauca, los dormitorios, los baños, la cocina, la planta eléctrica y los ventiladores se vieron afectados por una inundación.]

[Por otra parte, frente a la polémica por dos funcionarios de la ONU que participaron en una fiesta de la guerrilla el pasado 31 de diciembre en La Guajira, **analistas han dicho que el video de esa situación se ha usado sin explicar el contexto,** como parte de la continuidad de una campaña contra el proceso de dejación de armas.        ]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
