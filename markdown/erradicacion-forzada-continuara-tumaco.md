Title: Erradicación forzada continuará en Tumaco por lentitud del Fast Track
Date: 2017-04-18 17:35
Category: DDHH, Nacional
Tags: COCCAM, erradicación cultivos ilícitos
Slug: erradicacion-forzada-continuara-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [18 Abr 2017] 

Los campesinos cocaleros de Tumaco se encuentran en asamblea permanente y decidirán si retoman los bloqueos de la vía al mar, **debido a que continúa la erradicación forzada en sus territorios, por la lentitud del Fast Track que no ha debatido el punto de sustitución** de cultivos ilícitos.

De acuerdo con Alejandra Torres, vocera de la Coordinadora Nacional de cultivadores de Coca Amapola y Marihuana, **“una cosa es lo que pasa dentro de los ministerios y las instituciones nacionales y otra la que está pasando en los territorios”** en donde pese a que se haya acordado entre el campesinado el gobierno finalizar con las erradicaciones en Tumaco, estas acciones continúan.

Para los campesinos una de **las principales causas de que no se suspendan las erradicaciones es la falta de articulación que existe entre los Ministerios de Post Acuerdo y de Defensa**, que aún no han establecido la diferencia entre los cultivos industriales, que entrarían a tratamiento penal diferencial y los que podrían suscribirse a los planes de sustitución de cultivos.

Situación que provoca que se incumplan de igual forma, los acuerdos sobre socialización de los planes de sustitución en las comunidades, que tendrían que haber iniciado desde el principio del mes de abril. Le puede interesar: ["Las comunidades dijimos no na la erradicación, pero si a la sustitución: Campesinos de Argelia Cauca"](https://archivo.contagioradio.com/dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca/)

Otra de las situaciones problemáticas es la retención de 12 miembros de la Policía en medio de la protesta, que posteriormente retornaron a sus funciones, **sin embargo la Fiscalía entiende esto como un delito y desconoce los acuerdos directos que realizaron los campesinos con algunas organizaciones. **

<iframe id="audio_18216870" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18216870_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
