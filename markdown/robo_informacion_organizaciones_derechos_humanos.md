Title: Hurtan información sensible a tres organizaciones defensoras de Derechos Humanos en Bogotá
Date: 2018-01-26 15:48
Category: DDHH, Nacional
Tags: agresiones contra defensores de derechos humanos, Bogotá, organizaciones de derechos humanos
Slug: robo_informacion_organizaciones_derechos_humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/hotel-inter-bogota-e1516999686779.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Booking.com] 

###### [26 Ene 2018] 

En el marco del IV Encuentro Nacional “Protesta Social y Paz” de la 'Campaña Defender la Libertad un Asunto de Tod@s', organizaciones defensoras de Derechos Humanos fueron víctimas de "un hurto de información de los computadores de las y los defensores de derechos humanos **Jesús Castañeda Hernández, de Fumpaz Bogotá y Daniela Buritica de la Fundación Comité de Solidaridad con los Presos Políticos (FCSPP); además fuer robada la memoria USB y microSD de Jeison Pava, integrante del Comité Permanente por la Defensa de los Derechos Humanos (CPDH).**

Según la denunciar interpuesta ante el gobierno colombiano, en cabeza del presidente Juan Manuel Santos, el hecho se cometió sobre **las 8:30 de la mañana cuatro personas, que llegaron muy temprano al Hotel Inter Bogotá, ubicado en la Carrera 3 No. 20 - 27,** donde se iba a realizar el evento, les solicitaron a los defensores bajar para hacer el registro, haciéndose pasar por trabajadores del hotel. Uno de los individuos se presentó con el nombre de Luis, quien además, les pidió dejar las maletas en el cuarto del salón donde se iba a realizar el encuentro, argumentando cuestiones de seguridad.

### **Robo de información estratégica** 

Precisamente ese fue el momento que aprovecharon los ladrones para saquear las maletas y llevarse la información que se encuentra en los dispositivos. Además, **la denuncia constata que no se trató de un simple robo, sino que tenía como fin llevarse la información,** ya que únicamente se llevaron los computadores y las memorias dejando incluso la grabadora de voz luego de extraer la memoria micro SD.

Luego de haber robado lo que buscaban, el relato de los hechos indica que las personas salieron del lugar y abordaron un vehículo, y las autoridades ya están investigando lo sucedido para dar con los responsables y recuperar la información.

"Estos hechos generan especial preocupación, pues **la información hurtada con intenciones desconocidas, no solo busca entorpecer el trabajo de los/as defensores/as, sino que compromete a un sin número de víctimas,** a quienes se les hace acompañamiento a nivel nacional y con quienes se adelantan procesos", dice la denuncia de las organizaciones.

"Al revisar las cámaras de seguridad del salón donde se realizaría el evento, se encontró que fueron alteradas con anterioridad para evitar la identificación de los sujetos", agregan las organizaciones.

Cabe destacar que este tipo de actos en contra de las organizaciones defensoras ha incrementado en los últimos dos años. En 2016, se presentaron un total de 279 agresiones contra defensores de DDHH y hubo 3 fueron víctimas de robos de información estratégica.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
