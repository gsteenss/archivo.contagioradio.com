Title: Solo el 24% de las mujeres rurales pueden tomar decisiones sobre sus tierras
Date: 2015-10-15 14:28
Category: Mujer, Nacional
Tags: 15 de octubre día Mundial de la Mujer rural, construcción de paz, dialogos de paz, Diálogos de paz en Colombia, Edilia Mendoza vocera de la Mesa de Incidencia Política de las Mujeres Rurales Colombianas, Mesa de Incidencia Política de la Mujeres Rurales, mujeres rurales
Slug: solo-el-24-de-las-mujeres-rurales-pueden-tomar-decisiones-sobre-sus-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/mujer-campesina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######                         www.ayudaenaccion.org 

<iframe src="http://www.ivoox.com/player_ek_9007335_2_1.html?data=mpWdmZiXeY6ZmKiak5WJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiIa3lIqupsaPjc%2Foxtfbw8jNs8%2FVzZDRx5DQpYzB1s%2FS1JC2udPVzZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Edilia Mendoza, vocera de la Mesa de Incidencia Política de las Mujeres Rurales Colombianas.] 

###### [15 oct 2015] 

En el marco de la **conmemoración del Día Mundial de la Mujer Rural,** la Séptima entrega del Censo Nacional Agropecuario continúa evidenciando la desigualdad entre géneros que aun existe en el campo, teniendo en cuenta que en Colombia, la adjudicación de tierras **desfavorece a las mujeres en el acceso a baldíos y a subsidios integrales de tierra, pues el 60% se asigna a los hombres y 40% a mujeres.**

Sin embargo, este último porcentaje se divide en dos aspectos, un 24% cuando son las campesinas solas las que toman las decisiones, y en un 16% de los casos las decisiones se toman entre hombres y mujeres. Cuando la mujer campesina está a cargo de sus tierras el acceso a las maquinarias, crédito y asistencia técnica es limitado.

Edilia Mendoza, vocera de la Mesa de Incidencia Política de las Mujeres Rurales Colombianas, asegura que la crisis ambiental y económica, sumado a la problemática del conflicto armado, pone en mayores riesgos a la población de mujeres campesinas, lo que las ha obligado a organizarse para exigir posibilidades reales de participación de las mujeres en el desarrollo de políticas públicas dirigidas a este sector.

En base a eso, sus exigencias se centran en la participación política frente a la Ley Agraria Alterna en articulación con La Cumbre Agraria. Sin embargo, de acuerdo a Edilia, para que estas reformas sean un hecho **“”.**

Para la vocera de las mujeres rurales, en la sociedad colombiana "**hay un temor frente a que las mujeres sean las que lideren el país",** sin tener en cuenta que son un sector trascendental para la construcción de paz.
