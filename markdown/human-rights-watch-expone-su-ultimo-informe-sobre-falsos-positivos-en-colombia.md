Title: Más de 180 batallones cometieron ejecuciones extrajudiciales entre 2002 y 2008
Date: 2015-06-25 07:29
Category: DDHH, Otra Mirada
Tags: colombia, Derechos Humanos, Faltos Positivos, Fiscalía General de la Nación, Human Rights Watch, Jose Miguel Vivianco
Slug: human-rights-watch-expone-su-ultimo-informe-sobre-falsos-positivos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Human.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4684243_2_1.html?data=lZullpeYd46ZmKiakpqJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDnhqigh6adb67dyNrSzpC6rdfVz8jcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[José Miguel Vivanco  ] 

###### [24 Jun 2015] 

En horas de la mañana, **Jose** **Miguel Vivanco**, director de **Human Rights Watch** expuso los puntos más importantes del último informe sobre **Falsos Positivos** en **Colombia**, titulado "El rol de los altos mandos en falsos positivos: Evidencias de responsabilidad de generales y coroneles del Ejército colombiano por ejecuciones de civiles".

En el informe se habla de evidencias que sugieren que numerosos generales y coroneles tenían conocimiento de varios casos de falsos positivos y que en algunas ocasiones fueron ellos mismos quienes ordenaron o en su defecto, hicieron posible que se cometieran los asesinatos.

Cerca de  **3.000 casos** son investigados actualmente por la **Fiscalía General de la Nación**, en los que tropas del ejército asesinaron a civiles, por la presión de incrementar el número de bajas en la guerra contra los guerrilleros, asegurando que habían sido muertes en combate.

De acuerdo a los análisis de **Human Rights Watch** los fiscales han identificado más de **180 batallones** que cometieron ejecuciones extrajudiciales entre 2002 y 2008, por lo que comandantes y unidades responsables de estos hechos podrían ser penalmente imputables por su capacidad de mando.

**Vivanco** expresó su indignación con el tema de los falsos positivos, "Quienes fueron comandantes de esas brigadas del ejército colombiano, deberían ser objeto de indagatorias, de investigación y deberían rendir cuentas ante las autoridades de la **Fiscalía** por la responsabilidad que tienen en esa serie de hechos atroces".

Grabaciones y testimonios de militares implicados en falsos positivos, obtenidos por Human Rights Watch acusan a varios generales y coroneles de conocer, planificar, ordenar o facilitar estos crímenes y como agravante algunos de estos oficiales se convirtieron posteriormente en mandos militares de mayor jerarquía en Colombia.
