Title: Convocan a Paro Nacional indefinido a partir del 23 de Octubre
Date: 2017-10-19 11:34
Category: Movilización, Nacional
Slug: convocan-a-paro-nacional-indefinido-a-partir-del-23-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/paro-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias] 

###### [19 Oct 2017] 

En la rueda de prensa en la que se anunció que, a partir del 23 de Octubre habrá Paro Nacional indefinido, se aseguró que **hay incumplimientos y lentitud sistemática en la implementación** jurídica y territorial de los avances en materia de derechos que contienen los acuerdos de la Habana, y se reiteró que no existen garantías de seguridad para el ejercicio de la política y la protesta social.

Organizaciones sociales entre las que se encuentran la ANZORC, FENSUAGRO, CCOCCAM, Marcha Patriótica, CONPI, CONAFRO y otras, convocaron en la mañana de este jueves a un paro nacional indefinido en el que reclamarán **implementación normativa y territorial del acuerdo con las FARC, cumplimiento de los acuerdos de las organizaciones con el gobierno y garantías de seguridad para el ejercicio de la política y la protesta social.**

Las organizaciones manifestaron que los acuerdos de paz **"están bajo amenaza constante"** y esto se evidencia con la lentitud con la que avanza el proceso de implementación de lo acordado en la Habana en los territorios. Además, indicaron que el Gobierno Nacional ha impuesto la erradicación forzada, incumpliendo los acuerdos pactados de sustitución voluntaria.

### **Campesino del país reclaman presencia integral del Estado en los territorios** 

De acuerdo con Juan Carlos Bautista, campesino y miembro de COCCAM en Tibú, Norte de Santander, **el paro no debería existir**, pero ante la falta de voluntad del Gobierno Nacional, para dialogar con los campesinos en el territorio, "es necesario que nos movilicemos y presionemos al Gobierno". (Le puede interesar: ["Implementación de los acuerdos no va por buen camino")](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-no-va-por-buen-camino/)

Los campesinos han visto como el Gobierno Nacional **se ha empeñado en abandonar a las comunidades** sin que exista una inversión integral en los territorios. Han reclamado que, conjunto con la sustitución voluntaria, debe haber procesos que les garantice el acceso a la salud, la educación y la movilidad.

Además, manifiestan que el Gobierno **ha dilatado el proceso de implementación en repetidas ocasiones**. "Hay un acuerdo que está firmado, que está quedando en el papel y en donde lo que se va a implementar debe incluir  las comunidades".

Bautista afirmó que las instituciones se han encargado de ir a los territorios, "les dice unas cosas a los campesinos pero **no actúan de una manera coordinada para que se implemente lo acordado**". Él reconoce que en Tibú se han hecho acuerdos de sustitución de cultivos entre el Gobierno y el campesinado pero, "se han quedado firmados en el papel".

Los campesinos, en diferentes oportunidades han manifestado su voluntad de **acogerse a la sustitución voluntaria de cultivos**, pero "el Gobierno no ha llegado a las asambleas comunitarias donde decidimos que vamos a hacer con los productos". (Le puede interesar:["Implementación de los acuerdos requiere movilización social: Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/))

Por esto, miles de campesinos harán parte de la movilización. Le han pedido al presidente de la República **"que se ponga la mano en el corazón** como un colombiano más para que reactive las mesas de diálogo y ponga atención a la situación que estamos viviendo los campesinos".

Además, la movilización pretende exigir justicia y celeridad en las investigaciones de los asesinatos sistemáticos de más de **120 líderes sociales en el país**. Por esto, las organizaciones sociales le han pedido al Gobierno Nacional que se respete y se den garantías para el ejercicio del derecho a la protesta y a la movilización, ya que no sienten que exista un ambiente seguro y de respeto por los derechos humanos de los colombianos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
