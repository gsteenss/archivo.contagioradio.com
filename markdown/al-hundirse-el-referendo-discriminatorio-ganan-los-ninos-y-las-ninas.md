Title: Al hundirse el referendo "discriminatorio" ganan los niños y las niñas
Date: 2017-05-11 12:06
Category: DDHH, Entrevistas
Tags: Adopcion, Cámara de Representantes, referendo
Slug: al-hundirse-el-referendo-discriminatorio-ganan-los-ninos-y-las-ninas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/524747_1-e1494522148584.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana.com] 

###### [11 Mayo 2017] 

En un debate de tira y afloje, los representantes de la Comisión Primera de la Cámara y miembros de la sociedad civil expresaron argumentos tanto en contra como a favor de la realización del referendo contra la adopción de niños y niñas por parejas homosexuales solteros y viudos. Para la representante Ángela María Robledo, "**el resultado de la votación es un ejemplo de tolerancia ante las adversidades".**

Quien gano en la votación de la Cámara de Representantes, fue la igualdad. **La combinación explosiva entre política y religión, destapó en el debate los verdaderos intereses que tenían los ponentes del proyecto.** Robledo afirmó que " los ponentes disfrazaron sus creencias homofóbicas con argumentos constitucionales". La defensa de las minorías y de los niños se impuso sobre la intención de querer imponer un modelo de familia heterosexual en una sociedad que es profundamente religiosa. Le puede interesar: ["Referendo "discriminatorio" reduce posibilidad de adopción de 3700 niños y niñas"](https://archivo.contagioradio.com/referendo-discriminatorio-dejaria-sin-posibilidad-de-ser-adoptados-a-3700-ninos-y-ninas/)

Dentro de las revelaciones de las verdaderas intenciones de los ponentes, en el recinto los participantes vieron la participación de Carlos Alonso Lucio como imprudente en la medida que es un persona que perdió sus derechos político. **Según Robledo, "la participación de Lucio busca solamente activarlo políticamente, reencaucharlo en la política".**

El recinto estuvo marcado por la alta presencia de defensores de derechos de la comunidad LGBTI y de los derechos de los niños. **Las madres cabeza de familia y las parejas del mismo sexo tuvieron la oportunidad de expresar sus argumentos** ante el deseo de poder adoptar niños y niñas. Estuvieron en contra ante los argumentos de Cambio Radical quienes aseguraron que las mujeres deben quedarse en casa cuidando a los niños y no trabajando.

Para muchos el referendo era discriminatorio y ante los argumentos religiosos Robledo afirmó que "no se puede invocar a Dios para invitar a discriminar. Se debe hacer para invocar al amor al prójimo". Le puede interesar: ["Debate sobre referendo no tiene futuro legal ni constitucional" Claudia López"](https://archivo.contagioradio.com/debate-sobre-referendo-de-adopcion-no-tiene-futuro-legal-ni-constitucional-claudia-lopez/)

Una vez terminado el debate, los ponentes expresaron su deseo de apelar la decisión bajo el amparo del derecho constitucional que poseen. Robledo espera que " lo hagan con argumentos serios y no se escuden en creencias religiosas". **Se espera que los 2,3 millones de firmantes y los ponentes del proyecto hagan un plantón el día de hoy.** Sin embargo para Robledo esta convocatoria y la apelación que anunció la senadora Vivian Morales, ponente del proyecto, son reflejos de que son "malos perdedores".

<iframe id="audio_18632409" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18632409_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
