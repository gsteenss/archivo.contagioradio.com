Title: Denuncian asesinato de Jorge Chantré Achipiz, secretario del cabildo Nasa Pueblo Nuevo
Date: 2017-05-04 17:19
Category: DDHH, Nacional
Tags: indigena, Nasa
Slug: asesinado-jorge-chantre-achipiz-secretario-del-cabildo-nasa-pueblo-nuevo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Jorge-Chantré-Achipiz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Archivo 

###### 04 de May 2017 

En la tarde de este jueves fue asesinado el indígena y dirigente **Jorge Chantré Achipiz, secretario del cabildo Nasa Pueblo Nuevo**, en el corregimiento de Mesetas, jurisdicción de Jamundí, Valle del Cauca, hasta el momento se desconoce el responsable de los hechos.

Según la información, Chantré, es sobrino de la dirigente indígena del Pueblo Nasa, Catalina María Achipiz,  quien se moviliza a esta hora hacia el lugar de los hechos.

En desarrollo...
