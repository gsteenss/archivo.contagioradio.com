Title: #Emparámate: La nueva campaña para conservar los páramos de Colombia
Date: 2016-08-31 12:10
Category: Ambiente, Voces de la Tierra
Tags: Ambiente, cambio climatico, páramos
Slug: emparamate-la-nueva-campana-para-conservar-los-paramos-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/PARAMO-e1472662287618.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: el campesino 

###### [31 Ago 2016] 

**Colombia posee cerca del 50% de los páramos del mundo,** que son esenciales para la regulación de los ciclos de agua y son la fuente de abastecimiento hídrico de 14 ciudades principales del país. Sin embargo, la mayoría de los colombianos desconocen las amenazas a las que se enfrentan los páramos y además no saben que de allí proviene el agua que usan diariamente.

Así lo concluyó un estudio realizado por un grupo de estudiantes de la Universidad Santo Tomás, que al ver los resultados se unió con la Asociación Ambiente para impulsar una campaña que procura visibilizar la importancia de estos ecosistemas y generar un mayor grado de apropiación de los colombianos frente a los páramos, que actualmente enfrentan fenómenos como **la erosión, el retroceso de los hielos, o la sequía de la vegetación necesaria para preservar el agua.**

La minería legal e ilegal, la ganadería extensiva, la explotación y exploración petrolera, la agricultura y el cambio climático, son las principales amenazas que enfrentan los páramos del país. Por ejemplo, entre los años 2002 y 2009, el gobierno entregó títulos mineros sin obligar a las empresas nacionales y multinacionales a cumplir mayores requisitos. Durante esos años **se pasó de 1,1 a 8,4 millones de hectáreas tituladas, entregándose 391 títulos mineros sobre páramos en cerca de 108.000 hectáreas.**

Según la organización de Mecanismos de Información de Páramos, en la actualidad las empresas con mayor cantidad de títulos mineros en zonas de páramo son[ Anglogold Ashanti Colombia S.A que tiene 41.849 hectáreas ](https://archivo.contagioradio.com/anla-y-ministerio-del-interior-adelantaban-consulta-previa-ilegal-en-parque-tayrona/)en el páramo de Santurbán, Los Nevados y Chili-Barragan; por su parte las empresas Acerías Paz del Río y Minas Paz del Río tienen entre las dos 112.510 hectáreas tituladas que se sobreponen en los páramos de Guantiva - La Rusa, Pisba, Rabanal y el Río Bogotá; además Greystar Rsources tiene 22.848 hectáreas en el páramo de Santurbán; para **un total de 177.207 hectáreaspara minería en zonas de páramo.**

Pese a este panorama las acciones para prevenir la destrucción de los páramos, tanto de las autoridades oficiales como de la ciudadanía, son mínimas, sin tenerse en cuenta la importancia de estos territorios para cuidar especies endémicas de fauna y flora, pues algunos estudios calculan que **en los páramos existen alrededor de 4,700 especies diferentes de plantas y 70 de mamíferos.** Por otra parte, son esenciales para la producción de agua, pues **solo en Bogotá más del 80% del agua que circula por los sistemas de acueducto proviene de los páramos Chingaza, Sumapaz y Guerrero.**

Es por ello que surge la campaña **\#Emparámate**, como una estrategia de la **Asociación Ambiente y Sociedad y la Universidad Santo Tomás, apoyada por más de 20 organizaciones** de la sociedad civil y medios de comunicación comprometidos con el medio ambiente que buscan generar conciencia acerca de la importancia de preservar la vida de los páramos.

\

<iframe src="https://co.ivoox.com/es/player_ej_12730306_2_1.html?data=kpeklZWXdJehhpywj5aZaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5ynca7V08zO1M7YpYy6zYqwlYqmd9PZ24qfpZClscPdxtPhx5Ddb9TjxM7SxsbIcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
