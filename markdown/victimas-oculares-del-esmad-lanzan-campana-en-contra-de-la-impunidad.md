Title: Víctimas oculares del ESMAD lanzan campaña en contra de la impunidad
Date: 2020-09-30 12:17
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #BrutalidadPolicial, #ESMAD, #Ojo, #Víctimas
Slug: victimas-oculares-del-esmad-lanzan-campana-en-contra-de-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/22218572054_533c6637d0_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 30 de septiembre distintas víctimas oculares (personas que han perdido los ojos luego de disparos de la Fuerza Pública a sus rostros) del ESMAD lanzarán una campaña de solidaridad que busca **"evidenciar, sensibilizar y mantener la memoria" de las y los jóvenes que sufrieron la brutalidad** y la fuerza desmedida del Escuadrón Móvil Antidisturbios.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Doce víctimas oculares por agresiones del ESMAD
-----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cristian Rodríguez, fue herido el 16 de diciembre, cuando se encontraba participando en una movilización que se dirigía hacia la Universidad Nacional de Colombia. El dictamen médico **fue un estallido ocular**, que le provocó la perdida total de su ojo izquierdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Junto a él otras doce personas perdieron sus ojos en el marco de las jornadas de protesta del 21N. De acuerdo con Rodríguez, "estas agresiones dolorosas y traumáticas, generalmente quedan en la impunidad. Configurándose como un nuevo hecho victimizante, puesto que ni el Estado, ni las instituciones dan cuenta o responden por lo ocurrido". (Le puede interesar:[Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar"](https://archivo.contagioradio.com/perdio-su-ojo-por-el-esmad-pero-no-la-fuerza-para-luchar-cristian-rodriguez/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este motivo, este conjunto de víctimas lanzan su campaña de solidaridad, con la que esperan evidenciar los daños irreparables que sufrieron, movilizar a la sociedad en torno a ejercicios de memoria de lo que i**mplica la brutalidad policial y el fin de la impunidad.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

El accionar del ESMAD
---------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez también afirma que esta campaña busca luchar contra la no repetición frente a las violaciones de derechos humanos por parte de la Fuerza Pública. En ese sentido**, rechazan el accionar de la Policía Nacional y el ESMAD en las jornadas recientes de protesta**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Los actos del Escuadrón Móvil Antidisturbios son devastadores y están aportando a la comisión de crímenes de lesa humanidad" Señala Cristian Rodríguez. Si usted quiere sumarse a esta campaña solo debe ingresar al siguiente link:[CAMPAÑA DE SOLIDARIDAD - Que tu derecho a protestar no te cueste un ojo o la vida](https://docs.google.com/forms/d/e/1FAIpQLSfRnC5JzDgX6bgMr1DNero2Zn34f2bfPZdmb8lYWajkIQixPA/viewform).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
