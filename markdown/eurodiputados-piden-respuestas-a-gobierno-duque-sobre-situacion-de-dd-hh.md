Title: Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH
Date: 2020-06-09 00:14
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Agresiones contra defensores de DDHH, Gobierno Duque
Slug: eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

[Foto: Presidencia - WikiCommons](Foto:%20Presidencia%20-%20WikiCommons)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un grupo de 28 eurodiputados envíó una carta al Gobierno de Iván Duque expresando su preocupación frente a las acciones de vigilancia y seguimientos ilegales efectuados por el Ejército, los asesinatos de líderes y lideresas sociales y excombatientes y el recrudecimiento de la violencia que se vive en los territorios donde aún no llega la implementación del Acuerdo de paz. Señalan que Iván Duque y su gabinete deben promover la protección de la población en tiempos de pandemia garantizando los DD.HH. de las personas y la construcción de paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Directrices del Ejército generan inquietud en Eurodiputados

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Los diputados señalan que han seguido de cerca la situación de derechos humanos y paz en el país por lo que se mostraron preocupados por las de acciones de vigilancia y seguimientos ilegales, realizados por el Ejército Nacional a al menos, 130 personas incluidas periodistas, investigadores, abogadas y [defensoras de DD.HH](https://twitter.com/Justiciaypazcol). jueces y juezas de las altas cortes, miembros de su Gobierno y políticos de la oposición. [(Lea también: Prensa bajo la mira de actividades ilegales del ejército en Colombia)](https://archivo.contagioradio.com/prensa-bajo-la-mira/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de estas revelaciones, también mencionan las directivas internas del Ejército que ponen en riesgo a la población civil de ser víctima de nuevas ejecuciones extrajudiciales, lo que genera en los diputados "serias inquietudes con respecto las políticas de defensa y doctrinas militares que existen dentro de sectores de la Fuerza Pública", pues afectan el orden constitucional y democrático del país. [(Le puede interesar: Prensa bajo la mira de actividades ilegales del ejército en Colombia)](https://archivo.contagioradio.com/prensa-bajo-la-mira/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IdoiaVR/status/1268900750975021056","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IdoiaVR/status/1268900750975021056

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Proteger la vida de defensores de DD.HH. y excombatientes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Basados en el informe del Programa Somos Defensores, que reveló como entre enero y marzo de 2020 fueron asesinadas 47 personas defensoras y en el incremento de violencia que se ha evidenciado en el Cauca, situaciones que han evidenciado cómo las medidas de confinamiento convierten a las comunidades y a sus líderes en blancos más fáciles para los grupos armados, los integrantes del Parlamento expresaron su preocupación con relación a las afirmaciones de las "organizaciones de derechos humanos respecto a que la respuesta del Estado ha sido insuficiente". [(Lea también: Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.)](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, los integrantes del Parlamento destacaron la importancia de tomar medidas para evitar que siga en aumento el ataque a excombatientes y sus familiares que ha ascendido a 200 desde la firma del Acuerdo de Paz y a redoblar los esfuerzos para proteger los territorios del **Chocó, Pacífico, Cauca, Bajo Cauca, Sur de Córdoba y el Catatumbo** donde el conflicto armado continúa generando "desplazamiento forzado, asesinatos selectivos, masacres, presuntas ejecuciones extrajudiciales y violencia sexual".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Es necesaria una salida dialogada al conflicto

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La carta también hace mención al cese al fuego anunciado por el Ejército de Liberación Nacional (ELN) y lo destacó como **"un paso importante para desescalar la violencia en el país y para avanzar en el escenario de construcción de paz";** razón por la que consideran necesario se tomen medidas para detener el conflicto en los territorios y se implementen las medidas recogidas en del Acuerdo de Paz para desmantelar los grupos sucesores del paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los diputados del Parlamento Europeo resaltaron que la implementación de los acuerdos de Paz, es una prioridad en su relación con Colombia por lo que se mostraron sorprendidos ante la solicitud de figuras del Gobierno de reubicar recursos para la paz y destinarlos a medidas contra la pandemia, por lo que exhortaron el Gobierno a prevenir la pandemia **"en el total respeto de los derechos humanos y de sus compromisos internacionales en materia de derechos humanos y de construcción de paz".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que hace tan solo unas semanas se dio a conocer que el gobierno Duque invirtió \$3.350 millones de pesos para manejar su estrategia digital, de redes y contenidos durante el 2020, tomando recursos de un fondo que tiene por objeto «la financiación de programas de paz encaminados a fomentar la reincorporación a la vida civil de grupos alzados en armas».[(Le puede interesar: Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país)](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
