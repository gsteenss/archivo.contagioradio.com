Title: En Carmen del Darién un voto costó entre 50 mil y 200 mil pesos
Date: 2015-10-26 15:53
Category: Nacional, Política
Tags: Compra de votos en Chocó, Conpaz, Elecciones Colombia 2015, Elecciones en Carmen del Darién, Paramilitares en Chocó, Presencia paramilitar Bajo Atrato, Raúl Palacio
Slug: en-carmen-del-darien-un-voto-costo-entre-50-mil-y-200-mil-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Compra-de-votos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 90 Minutos 

<iframe src="http://www.ivoox.com/player_ek_9169918_2_1.html?data=mpajm56VfI6ZmKiakp2Jd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfpMbfz8rSb8XZzZCxw9fNaaSnhqam0JDZsozq0NncjcjTt9WZpJiSpJiPqc%2Fo08qYl5WPscrgjN6YlJWUcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ramiro Álvarez, CONPAZ] 

###### [26 Oct 2015 ]

[La jornada electoral en el municipio del Carmen del Darién transcurrió en medio de fuertes denuncias sobre **compra de votos y presiones de tipo paramilitar** en puntos conocidos como Brisas y Bajirá, principalmente.]

[Ramiro Álvarez hasta el momento el único candidato de la Red CONPAZ que logró llegar al Consejo del municipio, asegura que **en el caserío de Brisas**, lugar de fuerte presencia paramilitar, entre el viernes y el sábado se **compararon votos** por una suma cercana a los **\$50 mil**, mientras que el día domingo el valor osciló entre **\$100 mil** y **\$200 mil**.]

[En otro punto conocido como Bajirá, se denunció que los **tarjetones y urnas** para la jornada de elecciones fueron **guardados en el mismo hotel en el que se hospedaba Erlin Ibarguen Moya**, candidato a la alcaldía del municipio por el partido Cambio Radical y estrechamente vinculado con el **arrendamiento de tierras a empresarios de la palma**. La dueña del Hotel denunció el hecho ante unidades policiales, quienes respondieron que no podían hacerse cargo porque era muy peligroso.   ]

[A este mismo punto, según Álvarez, le fue **impedido el paso a los testigos electorales del candidato de la Red CONPAZ Raúl Palacio**, porque según las autoridades no contaban con sus respectivas credenciales, las cuales tras largas dilaciones llegaron pasado el medio día.]

[Pese a que en las campañas electorales de los candidatos de CONPAZ, las comunidades mostraban alto nivel de respaldo, Álvarez afirma que es evidente que la **"gente se dejó presionar por el dinero", así como por las advertencias de los paramilitares** de que quien no votara por *x* o *y* candidato “ya sabía lo que le pasaba”.]

[Dada la **presión paramilitar** evidente en este municipio del Bajo Atrato chocoano son **pocas las garantías para quienes ganen curules en el Consejo**, concluye Álvarez, agregando que en puntos conocidos como Montaño, Vigía de Curvaradó y La Larga durante la jornada electoral **no hubo fuerza pública**, situación preocupante para sus pobladores.]
