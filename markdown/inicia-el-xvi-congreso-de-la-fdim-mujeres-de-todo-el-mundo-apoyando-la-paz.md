Title: Inicia el  XVI Congreso de la FDIM, mujeres de todo el mundo apoyando la Paz.
Date: 2016-09-16 10:09
Category: Mujer, Nacional
Tags: mujeres, Mujeres por la paz
Slug: inicia-el-xvi-congreso-de-la-fdim-mujeres-de-todo-el-mundo-apoyando-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14349008_10157457236040430_54454844_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 de Sept de 2016] 

Bajo el lema ***“Las mujeres unidad por la paz y la lucha contra el imperialismo”*** inició el XVI Congreso de la Federación Democrática Internacional de Mujeres (FDIM) en Bogotá, en el que más de 250 delegadas de todos los continentes se reunirán para compartir sus experiencias y tratar diferentes temas en torno a las mujeres, como: **la diversidad étnica y cultural, el impacto de los cambios climáticos, los Acuerdos de Paz en Colombia, entre otros**.

La conducción estará a cargo de la presidenta de la FDIM, la brasileña **Marcia Campos**, quien declaró que como parte del Congreso se desarrollará una **jornada de solidaridad con las víctimas de la guerra** y se emitirá una [declaración de apoyo al proceso de paz, ya que Colombia](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/)se ha convertido en imagen y esperanza para las demás naciones del mundo que apoyan la convivencia pacífica en el país, tras largos años de conflicto armado interno que han dejado millones de víctimas.

El congreso, que culminará el próximo domingo, ha extendido una invitación a todas las personas que quieran acompañar el acto simbólico de solidaridad de las **Mujeres por la Paz  Mundial**, que se llevará a cabo el día de mañana 16 de Septiembre a las 4:00 PM, en una Marcha que iniciará en la Torre Colpatria y finalizará en el monumento de la Pola, ubicado en el eje ambiental de la ciudad.

**La Federación** **Democrática** **Internacional de Mujeres** surge después de la Segunda Guerra Mundial, en Paris de 1945 y obtiene su estatus consultivo en 1972. Actualmente cuenta con 139 organizaciones naciones afiliadas a 124 países en África, Asia, Medio Oriente, Europa, América y el Caribe.

Algunas de las organizaciones anfitrionas del cónclave son: La Asociación de Nacional por la Paz y la Defensa de la Unión de las Mujeres Democráticas, La Unión de Mujeres Democráticas (filial de la FDIM) y el Departamento Femenino del Partido Comunista Colombiano.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]
