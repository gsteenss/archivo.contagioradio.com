Title: La ardua tarea que tendrán los integrantes de la Comisión de la Verdad
Date: 2017-07-25 13:41
Category: Paz, Política
Tags: Claudia Vacca, comision de la verdad, Comite de Escogencia
Slug: comienza-convocatoria-para-integrantes-de-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/sin-olvido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Olvido] 

###### [25 Jul. 2017] 

Como parte de lo estipulado en el Acuerdo de Paz se dio **inicio a las convocatorias para las personas que quieran ser parte del Sistema Integral de Verdad, Justicia, Reparación y No Repetición** (SIVJRNR), a través del Sistema de Justicia (JEP), la Comisión de la Verdad que ha sido considerada como la columna vertebral de este sistema, de la Unidad de Búsqueda de personas desaparecidas o de la Unidad de Desmantelamiento de las Organizaciones Criminales.

**Claudia Vacca, integrante del Comité de Escogencia** manifestó que para el caso de la Comisión de la Verdad "lo que se busca es establecer una comisión que evalúe, analice, escuche y haga visible los diferentes relatos de las víctimas del conflicto, de manera que con la verdad plena se inicie también un proceso de reconciliación, reparación y construcción y consolidación de la paz”.

### **Hay 3 convocatorias en marcha** 

A la fecha y hasta el 2 de agosto se pueden postular las personas que consideren pueden ser Magistrados o Magistradas de la Justicia Especial para la Paz, de la Unidad de Búsqueda de Personas Desaparecidas y la Dirección de la Unidad de Investigación y Acusación. Le puede interesar: [Así serán seleccionados las y los jueces para la Jurisdicción Especial de Paz](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/)

Según Vacca **se han registrado un alto volumen de personas inscritas a las convocatorias,** pero continuarán con todo el tema de pedagogía para que entre el 25 de julio y el 27 de agosto, todos los que estén interesados pueden ingresar a buscar los requisitos en la página web (<http://www.comitedeescogencia.com>) .

**Una vez esas inscripciones se adelantan, el comité hará una evaluación de los requisitos mínimos, la lista de aspirantes ** se publicará para las observaciones de toda la ciudadanía. "Esto va a permitir interactuar con la ciudadanía y hacer un juicio más integral sobre las calidades de cada uno de los aspirantes”.

### **Tres criterios para la elección ** 

El mandato del Acuerdo indica que **el Comité debe atender la composición demográfica de aquellos que fueron las principales víctimas** del conflicto durante estos más de 52 años, dentro de esa conformación están las mujeres y las minorías raciales,  teniendo en cuenta las diferencias en cómo los territorios han sufrido la guerra. Le puede interesar: [160 organizaciones y personas respaldan la constitucionalidad de la Comisión de la verdad](https://archivo.contagioradio.com/comisionverdadpaz/)

“El Acuerdo ha establecido que **estos 3 criterios, género, territorialidad y etnia sean considerados cuando el comité haga la selección.** Nuestro interés es que al revisar los requisitos mínimos podamos revisar esa composición y garantizar en adelante que cada organismo atienda a la representación territorial y racial”.

### **¿Cómo asegurar que haya una garantía de verdad desde la Comisión?** 

Lo que pretende el Comité de Escogencia es que, al permitir a la sociedad civil, a las organizaciones sociales y de defensa de los derechos humanos, académicos, entre otros, postularse o postular a terceros para ser parte de la Comisión de la Verdad estos convoquen personas que consideran puedan ser garantes de la búsqueda de la Verdad.

“Hay que considerar los elementos asociados a los diferentes relatos del conflicto armado que ha vivido el país y garanticen que se **represente la pluralidad en los candidatos que se vayan a someter a este proceso”**. Le puede interesar: [En Septiembre se conocerán los nombres de magistrados del Tribunal de Paz](https://archivo.contagioradio.com/en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz/)

Por lo cual solicitan a los postulados y postulantes que toda la información y los certificados que se adjunten en el proceso y que soportan la trayectoria sean subidos adecuadamente a la plataforma, para que el corto tiempo con el que cuenta el Comité sea optimizado y además para garantizar la selección de los mejores y más idóneos.

### **¿Qué sigue luego de esta convocatoria?** 

La única convocatoria que queda pendiente es la de los representantes de otros países en el Sistema de Justicia Especial para la Paz que se denominan Amicus Curiae, que son **extranjeros que podrán opinar y ayudar a analizar los casos que se presenten** a ese sistema de Justicia. [Lea también: Comisión de la verdad debe investigar la responsabilidad de las empresas en la guerra](https://archivo.contagioradio.com/comision-verdad-esclarecer-responsabilidad-empresarial/)

“La **convocatoria de los extranjeros en la JEP iniciará el próximo 28 de agosto** y esperamos la apertura de inscripciones entre el 19 de septiembre y el 3 de octubre, con lo que estaríamos cerrando la totalidad de las nominaciones que este comité tiene que hacer, para esperar que a finales de noviembre estemos cerrando todo el proceso”.

Vacca concluye asegurando que es muy importante que muchas personas puedan participar o ser postuladas “quienes pueden estar tranquilos y seguros de que e**ste Comité va a actuar con toda la independencia, con toda la neutralidad y la rigurosidad** para que se evalúen las condiciones y la calidad humana de su trayectoria y de sus hojas de vida sin ninguna presión externa”. Le puede interesar: [Víctimas proponen criterios para escoger integrantes del SIVJRNR](https://archivo.contagioradio.com/criterios-integrantes-del-sivjrnr/)

<iframe id="audio_19992164" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19992164_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
