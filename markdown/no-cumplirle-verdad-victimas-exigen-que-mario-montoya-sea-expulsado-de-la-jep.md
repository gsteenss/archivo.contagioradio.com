Title: Por no cumplirle a la verdad, víctimas exigen que Mario Montoya sea expulsado de la JEP
Date: 2020-12-04 10:14
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Ejecuciones Extrajudiciales, JEP, Mario Montoya, Verdad del conflicto armado
Slug: no-cumplirle-verdad-victimas-exigen-que-mario-montoya-sea-expulsado-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Mario-Montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Víctimas en plantón contra permanencia de Mario Montoya en la JEP / Asociación Minga

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Víctimas y organizaciones defensoras de DD.HH. radicaron una tutela ante la Sección de Revisión de la Jurisdicción Especial de la Paz (JEP) para que ordene a la Sala de Reconocimiento, la expulsión del comandante (r) del Ejército, Mario Montoya. **Las víctimas argumentan que su contribución con la verdad del conflicto en el caso 003 de ejecuciones extrajudiciales ha sido nula.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras Montoya negó en su versión voluntaria ante la JEP en febrero de este año tener algún conocimiento de esta práctica, en al menos doce versiones rendidas por subalternos, incluidos dos coroneles y un mayor, **los uniformados han expresado que el excomandante les presionaba para que los resultados en bajas fueran superiores.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones defensoras de derechos humanos ya habían presentando solicitudes para expulsar del mecanismo no solo al general (r) Mario Montoya, también al coronel (r) Publio Hernán Mejía, **por faltar a su compromiso con la verdad lo que conllevaría no solo su exclusión sino la pérdida de beneficios como libertad transitoria o penas más bajas de las que contempla la justicia ordinaria**. [(Lea también: "El Senado nos dio la espalda" expresaron las Madres de Soacha)](https://archivo.contagioradio.com/senado-nos-dio-la-espalda-expresaron-las-madres-de-soacha/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el abogado **Edwin Gil, de la Corporación Jurídica Yira Castro**, **pasaron 292 días desde que la Sala de reconocimiento recibió esta solicitud sin que hubiera una respuesta** lo que llevó a la petición de la tutela abogado, *"esperamos que el tribunal para la paz garantice los derechos de las víctimas tal como está establecido en los estándares internacionales de justicia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado resalta que en el proceso que se adelantaba contra Montoya en la justicia ordinaria estaba a portas de dar una imputación al general y no la realizó al este someterse a la JEP, *"**Montoya sigue burlándose de las víctimas, negando su responsabilidad a contar alguna verdad e incluso niega conocer de esas prácticas** cuando ya es de público conocimiento la práctica de ejecuciones extrajudiciales por miembros del Ejército Nacional".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

*“**La administración de justicia no puede permitir que quienes han desconocido sus deberes y compromisos, sigan gozando de beneficios** como rebajas de penas y no privación de la libertad"* expresaron las víctimas a través de un comunicado, exigiendo que Mario Montoya sea excluido de la jurisdicción e ingrese a un proceso de la justicia ordinaria o internacional *"que garantice la plena vigencia de nuestros derechos a la verdad, la justicia, la reparación integral y las garantías de no repetición”.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comandancia del general (r) Mario Montoya en la primera división expuesta en informe

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El Informes entregado a la [JEP](https://www.jep.gov.co/Paginas/Inicio.aspx) el pasado 24 de noviembre titulado ***“El deshonroso primer lugar"***, **resaltan cómo durante la comandancia de Mario Montoya en la Primera División del Ejército se registraron 218 ejecuciones extrajudiciales** entre diciembre de 2003 y abril de 2005. [(Lea también: Mural ¿quién dió la órden? le pertenece a la gente: MOVICE)](https://archivo.contagioradio.com/mural-quien-dio-la-orden-le-pertenece-a-la-gente-movice/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De los más de 316 informes de víctimas y organizaciones que ha recibido la jurisdicción, este documenta 153 casos que a**grupan a 218 víctimas de ejecuciones extrajudiciales cometidas por esta facción del Ejército.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que la P**rimera División del Ejército que hacía presencia en 98 municipios de los departamentos de Atlántico,** **Córdoba, La Guajira, Magdalena, Antioquia, sur del Bolívar, Cesar, y Chocó** es señalada de perpetrar el 62 de casos de ejecuciones extrajudiciales en Colombia. [Le puede interesar: Se instaló el mural «¿Quién dio la orden? 2.0»)](https://archivo.contagioradio.com/se-instalo-el-mural-quien-dio-la-orden-2-0/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
