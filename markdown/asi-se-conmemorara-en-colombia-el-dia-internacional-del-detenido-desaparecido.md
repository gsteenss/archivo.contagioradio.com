Title: Así se conmemorará en Colombia el Día Internacional del Detenido Desaparecido
Date: 2016-08-29 15:39
Category: DDHH, Nacional
Tags: Desaparición forzada en Colombia, Desapariciones forzadas
Slug: asi-se-conmemorara-en-colombia-el-dia-internacional-del-detenido-desaparecido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Desaparición-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País ] 

###### [29 Ago 2016] 

Con el fin de conmemorar el Día Internacional del Detenido Desaparecido, el Movimiento de Víctimas de Crímenes de Estado, junto a diversas organizaciones como la 'Corporación Claretiana Norman Pérez Bello', la 'Coordinación Colombia-Europa-Estados Unidos' y la 'Corporación Colectivo Sociojurídico Orlando Fals Borda', lideran un **conjunto de actividades académicas y culturales** **que se realizarán este martes** en distintas ciudades del país, con el fin de continuar exigiendo justicia ante la impunidad que impera en Colombia frente a la sanción de este delito.

### Bogotá 

En la ciudad la programación inicia a las 9:30 de la mañana con el performance [['Cuerpos Gramaticales'](https://archivo.contagioradio.com/a-13-anos-de-operacion-orion-victimas-siembran-justicia-y-verdad/)], la presentación de grupos musicales, las intervenciones de organizaciones e invitados especiales, el homenaje a [[Nydia Erika Bautista](https://archivo.contagioradio.com/nydia-erika-bautista/)] y un concierto que finalizará sobre las 5 de la tarde; todo ello en los predios dónde será construido el Museo Nacional de la Memoria.

En el Hotel Boutique City Center desde las 10:30 de la mañana y hasta las 3:30 de la tarde se llevarán a cabo actividades que incluyen una rueda de prensa, la presentación de la obra Souvenir y un **conversatorio sobre procesos de duelo y fotografía**.

### Buenaventura 

En el Bulevar del Centro a las 2 de la tarde, se realizará una jornada por la paz en la que habrá **teatro, música tradicional y urbana, poesía** y una eucaristía en memoria de las víctimas.

### Cali 

Desde las 8:30 de la mañana y hasta las 11, en las estaciones del MIO se hará un **recorrido por la memoria** y desde esta hora y hasta las 12 del mediodía en la Universidad Santiago de Cali se realizará un encuentro.

### Caldas 

En la Casa del Pueblo Rubén Castaño desde las 9 de la mañana y hasta las 12 del mediodía, se realizará un seminario taller para la **búsqueda, identificación y entrega digna de restos de quienes desaparecieron** en el contexto y en razón del conflicto armado; y de 2 a 6 de la tarde se llevará a cabo un plantón por la memoria y la dignidad frente al Palacio de Justicia.

### Medellín 

La conmemoración iniciará este martes con un plantón en la Plazoleta de la Alpujarra a las 9 de la mañana y se extenderá hasta el día miércoles, con un **foro sobre los derechos de las víctimas en el proceso de paz** desde las 9 de la mañana en el Museo Casa de la Memoria y un encuentro entre instituciones y organizaciones a las 3 de la tarde en la Universidad San Buenaventura.

### Villavicencio 

En el Cementerio Central desde las 8:30 de la mañana y hasta las 12 del mediodía se darán cita organizaciones de víctimas y familiares para conversar en torno al [[acuerdo de víctimas en el marco del proceso de paz](https://archivo.contagioradio.com/acuerdo-de-victimas-en-proceso-de-paz/)]; así como para realizar un acto de memoria en homenaje a las víctimas de desapariciones forzadas en los Llanos Orientales y sembrar un jardín de la memoria.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
