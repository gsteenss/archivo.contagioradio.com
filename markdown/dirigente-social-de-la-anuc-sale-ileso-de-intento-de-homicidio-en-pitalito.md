Title: Dirigente social de la ANUC sale ileso de intento de homicidio en Pitalito
Date: 2016-11-28 15:06
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, víctimas
Slug: dirigente-social-de-la-anuc-sale-ileso-de-intento-de-homicidio-en-pitalito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/informe_derechos_onu.-lasdororillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las Dos Orillas ] 

###### [28 Nov 2016] 

El pasado sábado 26 de noviembre, el defensor de derechos humanos y dirigente social de la plataforma ANUC**, Ruben Palomino fue herido mientras se desplazaba en su moto de camino a su casa en Pitalito**. Con este son 5 los hechos de violencia reportados durante el fin de semana en contra de líderes del movimiento social o defensores de derechos humanos.

Palomino, quien además es el vicepresidente de la seccional departamental del Huila de la ANUC, relató que él se encontraba camino hacía su casa, sobre la 1 de la tarde, cuando en el camino encontró piedras en el suelo, dice que en vez de bajarse de la moto en la que se transportaba aceleró y en ese momento **fue cuando un hombre se le acercó y le propino el disparo que afortunadamente no fue mortal.**

El dirigente señaló que es posible que este atentado se deba a un proceso de restitución de tierras que él se encuentra adelantando o por represarías frente a una movilización en contra de la represa del Quimbo que organizó la comunidad de Pitalito. Le puede interesar:["Estamos frente a un nuevo Genocidio: Carlos Lozano"](https://archivo.contagioradio.com/estamos-frente-a-un-nuevo-genocidio-carlos-lozano/)

“**Yo estoy con mi familia en este momento tomando la determinación de si abandonamos el departamento del Huila** o extremamos más la seguridad y continuamos acá con la lucha del territorio apoyando a todos los líderes que hay acá en Pitalito” y agregó que la ANUC se encuentras tomando medidas para mejorar la seguridad ya que sienten “que es difícil que la política o el ejercito los defienda” afirmó Ruben Palomino.

Frente a quienes están detrás de estos sucesos hasta el momento ningún grupo al margen de la ley se ha atribuído el atentado, sin embargo Palomino asegura haber sido víctima de persecución durante algunos días y que posiblemente sean estas personas las responsables. **En el caso puntual de la ANUC, son muchos los líderes de esta organización que han sido víctimas de persecución y asesinato,** entre ellos el anterior presidente de esta plataforma y varios miembros más.

Estas acciones sistemáticas de violencia que se vienen presentando incluso en medio de una ratificación de los acuerdos de paz, han sido entendidas por parte del movimiento social como un intento para continuar la guerra y acabar con las organizaciones sociales, así lo ha informado en su más reciente comunicado el Comité de Permanente por la Defensa de los Derechos Humanos. Le puede interesar: ["No cesan agresiones contra integrantes de Marcha Patriótica"](https://archivo.contagioradio.com/no-cesan-agresiones-contra-integrantes-marcha-patriotica/)

<iframe id="audio_14195230" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14195230_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
