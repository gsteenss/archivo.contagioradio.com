Title: Escalada de violencia es responsabilidad de la “extrema derecha”
Date: 2016-11-21 12:35
Category: DDHH, Entrevistas
Tags: Desmonte paramilitar, Extrema derecha
Slug: escalda-de-asesinatos-y-atentados-extrema-derecha
Status: published

###### [Foto: Contagio Radio] 

###### [21 Nov 2016]

Ivan Cepeda señala que la escalda de asesinatos y atentados que deja este fin de semana, en el que se presentaron 3 asesinatos y 2 atentados **tiene dos propósitos por parte de la extrema derecha**. Uno, llenar de miedo y de temor a personas y organizaciones que podrían aportar en el proceso de implementación de los acuerdos de paz y por ellos las agresiones se enfocan en líderes sociales.

Otro de los objetivos es más antiguo, según Cepeda se trata de crear un clima poco favorable a la construcción de la paz, es decir, “un ambiente de violencia generalizada” que favorece intereses de sectores políticos concretos. Le puede interesar: ["Nuevo acuerdo es definitivo y no contemplará alivios especiales: Iván Cepeda"](https://archivo.contagioradio.com/nuevo-acuerdo-es-definitivo-y-no-contempla-alivios-especiales-ivan-cepeda/)

### **Hay que hablar con los líderes de extrema derecha** 

El senador afirma que hay que hablar claramente, **que no solamente se debe enfrentar el paramilitarismo**, porque esa es la cadena más baja de una idea de extrema derecha y que es esa **extrema derecha** la que está atacando por todos los flancos. Esa extrema derecha es la que debe ser derrotada por la vía política.

Además señaló que no se puede seguir pidiendo lo que tradicionalmente se pide, es decir, no hay lugar a promesas de investigaciones exhaustivas o más protección, sino que hay que “llamarle a las cosas por su nombre” y sentarse a hablar con la extrema derecha, sus líderes y sus representantes para ver cuál es su apuesta política en este momento.

### **Hay que entender que Uribismo quiere “torpedear el acuerdo”** 

El Senador Ivan Cepeda afirmó que el uribismo no quiere que haya un acuerdo sino una “acta de sometimiento” y buscar que el acuerdo sea inviable. Según Cepeda los planteamientos que se hacen son inviables si se buscan aplicar. “la propuesta es prácticamente que se elimine el primer punto de la agenda”, igual en el punto de justicia. Además denunció que la actitud del centro democrático incita a la violencia contra quienes quieren la paz.

El congresista también recordó las palabras del senador Uribe en España que se refirió al genocidio de la Unión Patriótica como un “suicidio colectivo” y todo ese tipo de declaraciones lo que buscan es impedir un acuerdo de paz e impedir también una serie de reformas que se deben hacer para construir una paz estable y duradera.

<iframe id="audio_13865112" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13865112_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
