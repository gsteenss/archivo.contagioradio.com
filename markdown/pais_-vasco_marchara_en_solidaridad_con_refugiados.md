Title: País Vasco marchará en solidaridad con los refugiados
Date: 2017-04-27 19:05
Category: DDHH, El mundo
Tags: Bilbao, guernika, País Vsco, Refugiados
Slug: pais_-vasco_marchara_en_solidaridad_con_refugiados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/ongi-etorri-e1493337641188.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  PlataformaOEEBizkaia 

###### [27 Abr 2017] 

En el marco de la conmemoración de los 80 años del bombardeo de Guernika, un lugar emblemático de la construcción de paz, se desarrollará una jornada a favor de los refugiados. **Este 29 y 30 de abril desde la plataforma Ongi Etorri Errefuxiatuak** se expresará el rechazo a las políticas que se están llevando a cabo desde Europa con respecto a los refugiados, y reivindicarán al País Vasco como lugar de acogida.

Cientos de personas se movilizarán hacia Guernika, para decir "no a la guerra", "al racismo", "al capitalismo salvaje" y gritar "sí a los derechos humanos", "al refugio", "al feminismo", y a "una vida digna para todos y todas".

Son **más de 70 pueblos de Bizkaia los que han organizado esta gran marcha en solidaridad con los refugiados.** Una iniciativa que se realiza, al reconocerse que es necesario que la ciudadanía sea capaz "extender el mensaje en contra de aquellos otros mensajes racistas que llegan desde posiciones conservadoras, de extrema derecha".

La movilización llegará a Guernika sobre las 4:30 de la tarde del sábado. Hora en la que sonarán las sirenas al igual que lo hicieron hace 80 años durante el bombardeo por arte de aviones nazi. El último kilómetro de la movilización se recorrerá en silencio, y luego iniciará un acto en el que se **dará la voz a personas refugiadas, así como a una mujer superviviente del bombardeo de la villa.**

Testimonios de refugiados de **Siria, el Kurdistán iraní, Sahara, Colombia, Senegal y Perú, serán los protagonistas en la llegada de los ciudadanos** que rechazan el flagelo del deslazamiento forzado. Pero además se realizará otra serie de actividades culturales, y talleres, donde los principales mensajes de Ongi Etorri Gernika 2017 serán **“salimos pero para decir a quienes están huyendo que les abrimos las puertas”**, y “no queremos guerras, que somos un pueblo que busca paz”. [ (Le puede interesar: Guernika premia el acuerdo de paz de Colombia)](https://archivo.contagioradio.com/?p=39720)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
