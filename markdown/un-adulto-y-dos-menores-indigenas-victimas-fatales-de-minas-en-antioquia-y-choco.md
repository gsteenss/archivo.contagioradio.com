Title: Un adulto y dos menores indígenas, víctimas fatales de minas en Antioquia y Chocó
Date: 2020-03-12 15:58
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Antioquia, embera, indígenas
Slug: un-adulto-y-dos-menores-indigenas-victimas-fatales-de-minas-en-antioquia-y-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Minas-antipersonales-y-artefactos-explosivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CICR\_co {#foto-cicr_co .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 10 de marzo, la Organización Indígena de Antioquia (OEI) denunció la muerte de Lorena Domicó Bailarín de 12 años y su hermano, Epifanio Domicó Bailarín de 17 años por activación de una mina en el municipio de Frontino, Antioquia. Por su parte, las Autoridades Tradicionales Indígenas de Colombia Gobierno Mayor alertaron por la muerte de Wilson Sabugara Becheche de 44 años tras la explosión de una mina en Alto Baudó, Chocó.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lorena y Epifanio, hermanos Embera Eyábida afectados por** minas

<!-- /wp:heading -->

<!-- wp:paragraph -->

La OEI informó que los menores de 12 y 17 años pertenecían al pueblo Embera Eyábida, y fueron víctimas mortales de una mina en el resguardo Murrí Pantano del municipio de Frontino, Antioquia. La situación se presentó el pasado domingo en el sector conocido como La Cristalina sobre las 12 del medio día.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según explicó la Organización, el resguardo queda a dos días de camino del corregimiento La Blanquita, el más cercano. Por esta razón, los menores perdieron la vida mientras eran trasladados al centro de atención médica en Frontino. Con ambos hermanos, la OEI cuenta tres indígenas muertos por minas en los últimos cuatro meses en Frontino.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/OIA_COLOMBIA/status/1237567635014840320","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/OIA\_COLOMBIA/status/1237567635014840320

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Wilson Sabugara Becheche, integrante del pueblo Embera Dobida**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por medio de un comunicado, las Autoridades Tradicionales Indígenas de Colombia Gobierno Mayor, confirmó la muerte de Wilson Sabugara Becheche, víctima de una mina antipersonal mientras se desplazaba por el camino que conduce del río Alto Munguidó a la comunidad La Felicia, en Alto Baudó, Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Gobierno Mayor, la zona está minada por grupos al margen de la Ley, por lo que el riesgo se extiende a toda la comunidad cercana al lugar. Por esta razón, la Organización le reiteró al Estado la necesidad de hacer presencia institucional en la zona para garantizar los derechos de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fgobiernomayor1%2Fphotos%2Fa.168827850191245%2F744894645917893%2F%3Ftype%3D3&amp;width=500" width="500" height="501" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### **Mínas, una violación a los derechos constante en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la reciente presentación del balance humanitario por parte del Comité Internacional de la Cruz Roja ([CICR](https://www.icrc.org/es/document/colombia-preocupaciones-del-conflicto-armado-y-la-violencia)), el jefe de la delegación en Colombia Christoph Harnisch señaló la preocupación del organismo ante el aumento de víctimas de artefactos explosivos y minas antipersonal en el país durante 2019. En el informe se señala que hubo un aumento del 59% de los casos registrados por el Comité, pasando de 221 personas víctimas en 2018 a 352 en 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La alerta responde a que en 2017 se presentaron 57 casos. Harnisch sostuvo que todos los grupos armados hacen uso de este tipo de artefactos como una forma de control territorial, y que afecta mayoritariamente a comunidades en los departamentos de Norte de Santander, Arauca, Antioquia, Nariño, Bolívar, Cauca y Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [En 2019 se registraron 25.303 nuevos desplazados en Colombia: CICR](https://archivo.contagioradio.com/en-2019-se-registraron-25-303-nuevos-desplazados-en-colombia-cicr/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
