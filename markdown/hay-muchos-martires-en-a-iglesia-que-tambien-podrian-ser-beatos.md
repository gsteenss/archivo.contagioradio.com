Title: Hay muchos mártires en la iglesia que también podrían ser beatos
Date: 2017-09-08 16:24
Category: Nacional, Paz
Slug: hay-muchos-martires-en-a-iglesia-que-tambien-podrian-ser-beatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Beatificacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [08 Sept. 2017] 

A propósito de la beatificación de los sacerdotes Pedro María Ramírez y Jesús Emilio Jaramillo en Villavicencio por parte del Papa, **el padre jesuita Javier Giraldo calificó como desacertada la selección** puesto que, si bien pudieron haber realizado un trabajo destacado, en Colombia hay muchos otros mártires que bien pudieran merecer un reconocimiento que no fuera usado como hecho político.

“En Colombia hay una gran cantidad de mártires, al punto de que ha habido sacerdotes, incluso obispos, religiosas, laicos, **campesinos, obreros, estudiantes, católicos que han entregado su vida de una manera muy heroica** por sostener compromisos inspirados en el evangelio”. El Padre Giraldo asegura que la vida de esos mártires en Colombia y el testimonio de las comunidades con y por las que trabajaron, los acredita como tal y no han sido beatificados. Le puede interesar: [Animador regaño del Papa Francisco a obispos y cardenales](https://archivo.contagioradio.com/el-regano-del-papa-francisco-a-los-obispos-y-cardenales-en-colombia/)

### **Muertes de los dos sacerdotes beatificados se dio en medio del conflicto** 

La elección de los dos sacerdotes se da en un momento coyuntural del país en el que se trabaja por la paz, pero ambos mueren en una Colombia que se encontraba en un contexto de violencia partidista, por lo que tener esos dos escenarios podría ampliar la brecha y la polarización del país. Le puede interesar: [La petición de Gloria Gaitán al Papa por beatificación de sacerdote](https://archivo.contagioradio.com/gloria-gaitan-beatificacion-sacerdote/)

“A Monseñor Jesús Emilio Jaramillo, yo lo conocí personalmente y yo creo que, desde el punto de vista de su vida sacerdotal, episcopal él era austero, tenía una repugnancia muy radical por el dinero y fue estimado por su gente como alguien sensible y cercano, pero **su muerte se vio enredada en esa guerra nacional y regional (…)** se dice que él manejó mal los dineros de la diócesis o de su relación del obispo con los militares”.

Para el Padre Giraldo, cabe un manto de duda o “mancha” que puede que esta beatificación ha llevado a los medios y a la clase dirigente a utilizar esta muerte como un argumento para bloquear los diálogos que se tienen con la guerrilla del Ejército de Liberación Nacional – ELN -. Le puede interesar:[ Víctimas piden al Papa Francisco que interceda para que cesen asesinatos a líderes](https://archivo.contagioradio.com/victimas-piden-al-papa-francisco-que-interceda-para-que-cesen-asesinatos-a-lideres/)

“La muerte de Pedro Ramírez también queda envuelta en esa polémica entre liberales y conservadores y la Iglesia estuvo ahí. **Incluso hay denuncias de la hija de Jorge Eliécer Gaitán que dice que el Padre Ramírez Ramos desde el púlpito invitó a asesinar a Gaitán.** Acusación muy grave y que quizás explica la crueldad de su muerte”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
