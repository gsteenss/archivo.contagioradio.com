Title: Así fue encuentro de diálogo entre víctimas y Salvatore Mancuso
Date: 2020-09-01 16:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: CEV, comision de la verdad, comisionada patricia tobon, dialogos, Salvatore Mancuso, VICTIMAS
Slug: asi-fue-encuentro-de-dialogo-entre-victimas-y-salvatore-mancuso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/6-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/7-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/4-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/3-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-01-at-3.18.49-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"fontSize":"medium"} -->

\#VerdadesEnMovimento {#verdadesenmovimento .has-medium-font-size}
---------------------

<!-- /wp:heading -->

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray","fontSize":"small"} -->

###### Fotos por: Contagio Radio {#fotos-por-contagio-radio .has-text-align-right .has-cyan-bluish-gray-color .has-text-color .has-small-font-size}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un grupo de víctimas de crímenes de lesa humanidad y crímenes de guerra hombres y mujeres, afrocolombianos, indígenas, mestizos, habitantes rurales del Urabá antioqueño, Urabá córdobés, Norte del Chocó y Bajo Atrato en la ciudad de Apartadó, sostuvieron un diálogo virtual extrajudicial con varios solicitantes ante el Sistema Integral Transicional con responsables del sector empresarial y excombatientes responsables de su desplazamiento y despojo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese lunes, durante 45 minutos, las victimas sostuvieron una segunda comunicación esta vez por teléfono con Salvatore Mancuso en un acercamiento de valoración de la voluntad de enunciar verdades, reconocer responsabilidades y participar desde la verdad en la justicia y la reparación integral. El intercambio con Mancuso fue uno entre tres que se sostuvieron durante esa tarde con personas responsables de las victimizaciones contra la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El diálogo se dio en medio de un viaje de recolección de testimonios de las víctimas y de sus iniciativas de Garantías de No Repetición y Transiciones Territoriales a la Paz, que viene adelantando la[Comisión de la Verdad](https://comisiondelaverdad.co/), a través de la comisionada Patricia Tobón,  con el apoyo de organizaciones como la Comisión de Justicia y Paz, entre otras.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89188,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/7-1024x683.jpg){.wp-image-89188}  

<figcaption>
*[Danilo Ruela y Patricia Tobón, Comisionada de la Comisión para el Esclarecimiento de la Verdad]{.has-inline-color .has-cyan-bluish-gray-color}*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5,"textColor":"vivid-cyan-blue"} -->

##### [Le puede interesar: **Víctimas de Salvatore Mancuso piden su extradición a la Fiscalía**](https://archivo.contagioradio.com/victimas-de-salvatore-mancuso-piden-su-extradicion-a-la-fiscalia/) {#le-puede-interesar-víctimas-de-salvatore-mancuso-piden-su-extradición-a-la-fiscalía .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la conversación con Mancuso, la Comisionada de la Verdad, Patricia Tobón reitero la importancia de las verdades, la de ese encuentro y escuchó atentamente a las víctimas en el propósito de lograr que esta región, hoy sometida a nuevas dinámicas de violencia inenarrable que deben ser resueltas para lograr que la verdad posibilite transitar a una región realmente incluyente, justa en lo social y ambiental, como garantía de no repetición

<!-- /wp:paragraph -->

<!-- wp:image {"id":89186,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/6-1024x683.jpg){.wp-image-89186}  

<figcaption>
Oscar Carupia, Líder Embera. En el diálogo la comunidad embera preguntó a Mancuso por lo sucedido con el reconocido líder [kimy Pernía](https://www.justiciaypazcolombia.com/kimy-pernia/).

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La diciente respuesta de Mancuso, además de precisa y dolorosa, **indica que sectores empresariales están detrás de la orden de la desaparición y asesinato de Kimy**, verdad que será plena con la identificación del civil que ordenó este crimen de lesa humanidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra lideresa despojada exigió a Mancuso el listado con nombres de los empresarios despojadores y las formas de articulación para la comisión de diversos crímenes, gracias a los cuales se perpetúa y mantiene en esta región del país una imposición de un modelo de desarrollo excluyente, signado de sangre y de fuego.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89189,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/4-1024x683.jpg){.wp-image-89189}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Del mismo modo, las victimas reiteraron que las verdades de Mancuso deben ser protegidas, él, su vida y las pruebas- Agregaron que la cárcel no es justicia y que en cambio requieren un diálogo directo con los implicados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades van a continuar en sus diversos procesos organizativos formulando peguntas a este y otros responsables que han manifestado su disposición de enunciar verdad, reconocer las responsabilidades a la espera que  la Jurisdicción Especial de Paz tome decisiones prontas, pues tanto los solicitantes a ser comparecientes como las victimas, están siendo presionados y amenazados por las verdades que están reconstruyendo y reconociendo extrajudicialmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Son más de 2.500 víctimas acreditadas en el caso 004 y que han encontrado en la comisión de la verdad un espacio propicio para escuchar sus propuestas de sanciones propias para las garantías de no repetición y sus iniciativas de transición hacia una región en paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar del poder que se desarrolla en estos territorios por parte de estructuras armadas como las AGC y en algunas zonas donde existen disputas con el ELN, las víctimas en medio de este conflicto, siguen exigiendo verdad, justicia, reparación y una transición hacia la paz que pase por el dialogo con todos los actores del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como un mecanismo de seguimiento y respuesta a todas las preguntas, los diversos procesos comunitarios allí presentes recibieron un buzón en el cual depositarán sus preguntas que se harán llegar a Salvatore Mancuso, a los empresarios que han solicitado ser acogidos a la JEP y así mismo a excombatientes de diversas fuerzas responsables de sus victimizaciones.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89191,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-01-at-3.18.49-PM-1024x682.jpeg){.wp-image-89191}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Las victimas esperan que su voz sea escuchada y se tomen decisiones en derecho en la JEP, ajustadas a la realidad que han vivido y que siguen padeciendo en estos territorios, más allá de formulaciones legales que están impidiendo la verdad completa de terceros, agentes de Estado y empresarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte aseguraron que la presencia de la Comisionada Tobón en sus territorios abrió la esperanza que por fin se pueda lograr desde la CEV un tránsito en las verdades hacia la paz con justicia, sin odios ni venganzas. La Mejor Esquina de las Américas, lo será con verdades y cambios democráticos.

<!-- /wp:paragraph -->

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D1393939254336472%26extid%3DCpiyjPFCJMrqybZO&amp;show_text=false&amp;width=734&amp;height=411&amp;appId" width="734" height="411" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>
