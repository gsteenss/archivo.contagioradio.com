Title: “Crisis del petróleo, una oportunidad para Colombia” USO
Date: 2015-01-21 21:14
Author: CtgAdm
Category: Economía, Política
Slug: la-crisis-del-petroleo-es-una-oportunidad-para-los-trabajadores-y-para-colombia-uso
Status: published

###### Foto: USO 

Para disminuir el impacto de la crisis de petróleos, según la Unión Sindical Obrera, Colombia debe nacionalizar los campos de extracción de hidrocarburos, aumentar la participación del Estado en las empresas de levantamiento, y contratar de manera directa a los trabajadores de la industria. Estos son algunos de los argumentos expuestos en rueda de prensa el 20 de enero, la central sindical indicó que el Proyecto de Ley con estas iniciativas se presentaría en los próximos meses.

\[embed\]https://www.youtube.com/watch?v=oLk5PPyp444&feature=youtu.be\[/embed\]

El mundo está asistiendo a una crisis en la producción y precios del petróleo por cuenta del cálculo geopolítico, en que, por una parte se encuentran los países productores del mundo árabe quienes se unieron para hacer un “dumping” en contra de los EEUU; y por otro, países que subsisten de la producción petrolera, como Venezuela y Rusia que han apoyado a los EEUU para que se incremente la producción de crudo. La sobreoferta de petróleo ha llevado a la OPEP (Organización de Países Exportadores de Petróleo) a tomar decisiones en cuanto al precio del petróleo, que ha bajado desde el 2014.

Esta jugada económica ha generado que multinacionales petroleras en Colombia decidan reducir su producción, e incluso cerrar algunos campos en que los costos de levantamiento serían superiores a las ganancias. Pacific Rubiales ha manifestado que despedirá un número importante de los 310 trabajadores directos y los 2 mil tercerizados con los que cuenta, lo que se suma a las declaraciones de la Asociación Colombiana de Petroleos y CAMPETROL de poner en vilo a cerca de 25 mil trabajadores de la industria.

A estas medidas, se suma la solicitud por parte de petroleras como Occidental de Colombia, que insiste en la necesidad de disminuir la participación del Estado en las ganancias, esto es, un aumento en las exenciones tributarias. Habría que recordar que en los últimos 10 años, como consecuencia de la apertura a la inversión extranjera por parte del gobierno de Álvaro Uribe, la participación del Estado en las ganancias de la explotación petrolera ha sido del 30% y el 20%. Incluso, gracias al Decreto 1760, hay campos que entregan el 0% de sus ganancias al Estado. **Gracias a las 32 exenciones tributarias, Colombia ha dejado de devengar 608.54 billones de pesos**.

Para tener un punto de referencia, la Unión Sindical Obrera recuerda que según analistas, un pos-conflicto en Colombia, costaría cerca de 187  billones de pesos para los próximos 10 años, quiere decir, que con lo que recibiría el Estado por impuestos a multinacionales de hidrocarburos, tendría como financiarse el proceso de paz hasta 10 veces.

Según la USO, la respuesta del Estado ante la crisis será disminuir el número de inversiones y vender su participación en entidades como la Previsora de Seguros y Ecopetrol. Para la Unión Sindical Obrera, este modelo se asemeja al utilizado en la venta de Telecom, abriendo las puertas al capital privado hasta que la empresa quedó en manos de multinacionales.

“Nos están presentando la receta de la crisis: Desplome de precios del crudo, la producción y las reservas –señala Fredy Pulecio, Secretario de Asuntos de Usuarios y Beneficiarios de la USO-. Hubo vacas gordas que vieron solo las trasnacionales, y a hora vienen las vacas flacas para los trabadores y para el país, si es que no son vacas famélicas”.

Para esta central sindical, la crisis debería verse como una oportunidad, tanto para los trabajadores como para el Estado colombiano, de recuperar campos de explotación petrolera, como Campo Rubiales, tal como realizara el gobierno ruso de Bladimir Putin, que nacionalizó la industria del petróleo aprovechando la venta de acciones por parte de privados. De la misma manera, Ecopetrol debería destinar más recursos en la exploración; actualmente destina 530 millones de dólares, mientras destina 4.113 millones de dólares para producción.

Finalmente, la USO afirma que los costos de producción no disminuirán con el despido de trabajadores, como manifiesta Asociación Colombiana de Petróleos. El problema para la central sindical se encuentra en la tercerización laboral que se produce en Ecopetrol. Un trabajador tercerizado cuesta 3,5 veces más que un trabajador directo, generando un mayor impacto económico, y realizando actividades de menos calidad. Hasta hace 2 años las actividades de mantenimiento de las refinerías las realizaban trabajadores directos, pero actualmente, las hacen compañías multinacionales a grandes costos.

La USO recuerda –además- que la Ley 1429 prohíbe que las empresas tercericen actividades misionales; en el caso de Ecopetrol hay una nómina de 8 mil trabajadores directos y 37 mil tercerizados, de los cuales 24 mil realizan actividades misionales como el transporte, la refinación, el mantenimiento, la inspección y la gestoría.

¿Dónde está la crisis de Ecopetrol? –se preguntan los líderes sindicales-. “Si se van a reducir 3.500 millones de dólares producto de la crisis, esta debe resolverse donde se está presentando, y no atacando a los trabajadores, por la vía de reducir costos laborales”.

La USO presentará un Proyecto de Ley encaminado a modificar la renta petrolera, los precios de los combustibles, y recuperar campos (como el de Caño Limón, que fueron entregados por el gobierno Uribe casi a perpetuidad). Además, afirman que participarán de manera activa en la Segunda Asamblea Nacional por la Paz, y los foros regionales para hablar de la paz en Colombia.

"Nosotros, bajo la guerra, hemos creado y consolidado a Ecopetrol, y que ni crea el gobierno que hoy que estamos en proceso de paz, vamos a entregarle lo que ha sido la columna vertebrar de la economía nacional", concluyó Fredy Pulecio.
