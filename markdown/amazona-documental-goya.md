Title: Amazona en carrera por un Goya
Date: 2017-12-14 15:32
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, Goya
Slug: amazona-documental-goya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Amazona_01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Amazona pelicula 

###### 14 Dic 2017 

Por segundo año consecutivo una producción nacional es nominada en la categoría de Mejor Película Iberoamericana en la edición 32 de los Premios Goya, se trata del documental ***Amazona*** de la directora Clare Weiskopf y protagonizado por su madre Valerie Meikle.

Amazona, competirá en la categoría principal del evento con otras tres películas seleccionadas: ***Tempestad*** de Tatiana Huezo (México), ***Una mujer fantástica*** de Sebastián Lelio (Chile) y ***Zama*** de Lucrecia Martel (Argentina), según lo anunciado por la Academia de Cine español.

La cinta, codirigida con Nicolas van Hemelryck, ganó el premio de la audiencia en el Festival de cine de Cartagena y ha participado en certámenes internacionales como el de Documentales de Amsterdam, el Biografilm italiano, o el Docudays de Ucrania.

Las producciones ganadoras se darán a conocer durante la gala de los Goya 2018, que tendrá lugar el próximo 3 de febrero en el Hotel Marriott Auditorium de  Madrid.

 

 

<iframe src="https://www.youtube.com/embed/yybE19wOq9g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 

**Sinopsis:**

Después de la muerte trágica de su hija mayor, Valerie emprende un viaje a la selva colombiana buscando superar el duelo y encontrarse a sí misma, dejando atrás a sus otros hijos y a su familia. Clare, directora de este documental e hija de Valerie, tenía 11 años cuando esos hechos ocurrieron.

Ha pasado mucho tiempo y Clare, embarazada, decide confrontar con su madre las huellas de esa decisión. ¿Qué buscaba Val? ¿Pensó en las consecuencias que esa separación traería para los otros? El encuentro de madre e hija, no exento de tensiones y reproches, revela dos personalidades que chocan, pero, al mismo tiempo, un lazo que las une en su condición de mujeres que se preguntan por el sentido de ser madres, por la responsabilidad y las ansias de libertad, por el sentido o la trampa que se esconde detrás de la idea de la maternidad como sacrificio.

Un retrato franco e iluminador de un momento de dos mujeres, en el que todo el trayecto anterior de sus vidas cobra un nuevo sentido.

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
