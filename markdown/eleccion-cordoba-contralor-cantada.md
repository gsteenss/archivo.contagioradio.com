Title: Elección de Córdoba como Contralor estaba cantada: Navas Talero
Date: 2018-08-21 15:11
Category: Paz, Política
Tags: Centro Democrático, Contralor General de la República, Jorge Robledo, polo
Slug: eleccion-cordoba-contralor-cantada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-21-a-las-2.47.49-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SenadoGovCo] 

###### [21 Ago 2018] 

El Representante a la Cámara por Bogotá del Polo Democrático, **Germán Navas Talero,** manifestó que aunque el Centro Democrático fue derrotado por la opinión pública, desde la semana pasada se sabía que el nuevo Contralor no sería **Jose Felix Lafaurie,** sino **Carlos Felipe Córdoba**, quien lo seguía en la carrera por la Contraloría General de la República.

<iframe src="https://co.ivoox.com/es/player_ek_27979768_2_1.html?data=k5ymmZ6bepmhhpywj5WWaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncajZ09KSpZiJhZLijLPO2MbXb7XVzcrf0YqWh4zGxtXfx9jJstXVz9nSjcaPsMKfpIqwlYqldc7V08aYstTQs4y4joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para el representante, era evidente que no sería el candidato del Centro Democrático quien resultaría elegido porque el legislativo no lo quería. Sin embargo, las fuerzas políticas se reorganizaron en torno a **Córdoba, quien obtuvo 203 votos de 248 posibles** (75 en Senado y 128 en Cámara de Representantes). (Le puede interesar: ["MOE pide compromiso de transparencia en elección de Contralor"](https://archivo.contagioradio.com/moe-pide-compromiso-de-transparencia-en-eleccion-de-contralor/))

De esta forma Córdoba logró un triunfo amplio sobre su inmediato seguidor **Julio Cesar Cárdenas, apoyado por la bancada de oposición quien obtuvo 31 votos en total.** Por su parte, **Lafaurie consiguió tan solo 3 votos en Senado y 3 en Cámara,** en razón de las inhabilidades y dudas presentadas por los Congresistas del partido de Gobierno.

### **La elección de Contralor ha sido un proceso controversial** 

Según Pablo Bustos, presidente de la Red de Veedurías Ciudadanas, el proceso para elegir "a la cabeza de la lucha contra la corrupción" ha sido controversial desde el principio, pues **se "han cambiado las reglas del juego en la convocatoria para que el familiar de un congresista haga parte de la elección".** Adicionalmente, porque con el resultado de las votaciones, queda demostrado que los partidos políticos cerraron filas en torno a un candidato, lo que demostraría las fallas en el modelo de control fiscal del país.

Bustos, afirmó que este proceso revela que la "elección de contralor es totalmente política, donde los partidos hacen un manejo de lo público", afirmación que también han hecho Senadores como **Jorge Enrique Robledo**, del POLO, quien expuso en su cuenta de Twitter, el discurso de Córdoba en el que agradecía a sus "promotores: Germán Vargas Lleras, Aurelio Iragorri, César Gaviria Trujillo, Álvaro Uribe Vélez y Andrés Pastrana Arango".

<div class="permalink-inner permalink-tweet-container ThreadedConversation ThreadedConversation--permalinkTweetWithAncestors">

<div class="tweet permalink-tweet js-actionable-user js-actionable-tweet js-original-tweet my-tweet logged-in js-initial-focus" tabindex="0" data-associated-tweet-id="1031908371933347840" data-tweet-id="1031908371933347840" data-item-id="1031908371933347840" data-permalink-path="/Contagioradio1/status/1031908371933347840" data-conversation-id="1031907652903882757" data-can-be-self-threaded="true" data-is-reply-to="true" data-has-parent-tweet="true" data-tweet-nonce="1031908371933347840-185591c7-5507-46b6-ba13-88dadddad314" data-tweet-stat-initialized="true" data-screen-name="Contagioradio1" data-name="Contagio Radio" data-user-id="189165807" data-you-follow="false" data-follows-you="false" data-you-block="false" data-mentions="GNavasTalero" data-reply-to-users-json="[}]" data-disclosure-type="" data-tfb-view="/i/tfb/v1/quick_promote/1031908371933347840">

</div>

<div class="tweet permalink-tweet js-actionable-user js-actionable-tweet js-original-tweet my-tweet logged-in js-initial-focus" tabindex="0" data-associated-tweet-id="1031908371933347840" data-tweet-id="1031908371933347840" data-item-id="1031908371933347840" data-permalink-path="/Contagioradio1/status/1031908371933347840" data-conversation-id="1031907652903882757" data-can-be-self-threaded="true" data-is-reply-to="true" data-has-parent-tweet="true" data-tweet-nonce="1031908371933347840-185591c7-5507-46b6-ba13-88dadddad314" data-tweet-stat-initialized="true" data-screen-name="Contagioradio1" data-name="Contagio Radio" data-user-id="189165807" data-you-follow="false" data-follows-you="false" data-you-block="false" data-mentions="GNavasTalero" data-reply-to-users-json="[}]" data-disclosure-type="" data-tfb-view="/i/tfb/v1/quick_promote/1031908371933347840">

<div class="js-tweet-text-container">

\[caption id="attachment\_55847" align="aligncenter" width="564"\][![Contralor General de la República](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-21-a-las-11.43.59-a.m.-564x669.png){.wp-image-55847 .size-medium width="564" height="669"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-21-a-las-11.43.59-a.m..png) Twitter del Senador por el POLO, Jorge Enrique Robledo\[/caption\]

</div>

</div>

</div>

El presidente de la Red de Veedurías concluyo invitando a los ciudadanos a ser parte del control a la Contraloría, puesto que hay grandes retos que deberá asumir frente a la corrupción y; porque el conjunto político que lo ha elegido presenta fragilidades en esa lucha y podría tener intereses particulares en una entidad que tiene a su cargo más de **4 mil empleados, y cerca de 425 mil millones** de pesos al año de los Colombianos.

<iframe src="https://co.ivoox.com/es/player_ek_27979829_2_1.html?data=k5ymmZ6cdpqhhpywj5aYaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncbHVw9HcjafZt9Xj1IqfpZDUtsbnysnS0NnJb7PZxZDRx5C6qcbY1teSpZiJhaXV1JCwy9rIpcXVz8bgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div class="permalink-inner permalink-tweet-container ThreadedConversation ThreadedConversation--permalinkTweetWithAncestors">

<div class="permalink-inner permalink-tweet-container ThreadedConversation ThreadedConversation--permalinkTweetWithAncestors">

###### [Reciba toda la información de Contagio Radio en][su correo](http://bit.ly/1nvAO4u)[o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][Contagio Radio](http://bit.ly/1ICYhVU). 

</div>

</div>
