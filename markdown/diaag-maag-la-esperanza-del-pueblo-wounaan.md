Title: Diaag Maag: La esperanza del pueblo Wounaan
Date: 2019-09-24 15:37
Author: CtgAdm
Category: DDHH, Especiales Contagio Radio
Tags: Chocó, Desplazamiento, Pichimá, Valle del Cauca
Slug: diaag-maag-la-esperanza-del-pueblo-wounaan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ewuanda-pichima-quebrada.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/pichima-quebrada.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/pichima-2.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WOUNNAN.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/pichima.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### reportajes

DIAAG MAAG, LA ESPERANZA DEL PUEBLO WOUNAAN
-------------------------------------------

Los Wounnan se exponen a ser revictimizados por un mundo desequilibrado espiritualmente, que obligó a Ewandan a bajar de sus montañas para atraparlo en un frío albergue...

Por: Sandra Gutiérrez Mogollón

https://youtu.be/uhdaR5\_o4O4

###### 26 Agosto 2019

Las casas solo existen cuando las habitan las personas, dijo Gloría mientras recorría su vivienda y repasaba uno a uno los rincones de lo que hasta hace dos meses era su morada. Con angustia, fue recolectando los daños que el abandono le provocó a su hogar, que son los mismos que el desplazamiento forzado le provocan a su cuerpo y a su alma.

Gloria es una indígena Wounaan, integrante del Resguardo de Pichimá Quebrada, en el Litoral del Bajo San Juan, y hace parte de las 25 familias que el pasado 2 de julio tuvieron que salir despavoridas de sus hogares para protegerse de las balas y la muerte, que de improvisto se apoderaron de sus tierras, luego de que dos grupos armados iniciaran una confrontación en el lugar.

Horas después de esos terroríficos hechos, 119 indígenas se montaron en sus canoas de madera y bajaron por el río San Juan hasta llegar a Docordó, en donde tuvieron que refugiarse en tres albergues en ruinas. Tres lugares con pocas paredes, escasa luz, ausencia de tranquilidad y nada en común con sus hogares.

El destierro de Ewandan

“**Para que un indígena se vaya a la cama, es porque tiene que estar muy enfermo**” expresó Aidé, docente del Resguardo, que tiene la labor fundamental de hacer que su cultura perdure en las generaciones venideras a través de la enseñanza de la tradición. Aidé señaló que la causa de las dolencias de su comunidad tiene que ver con estar en tierra ajena, una en la que simplemente no pueden ser.

“Nosotros allá tenemos un sitio sagrado en donde hay agua bendita y vamos para pedirle a Ewandan, nuestro Dios, que nos proteja, allá llevamos a nuestros niños, y ahora ese sitio está abandonado”, explicó Aidé, porque en Docordó, Ewandan ha sido víctima de una especie de confinamiento, sin ríos puros para fortalecer su ritualidad, espacios propicios para celebrar su existencia o momentos especiales para conmemorar la fe.

https://youtu.be/yvOt5SAcLj4

Asimismo, desde el primer momento en que los Wounaan llegaron a los albergues sus cuerpos empezaron a resentirse. Los dolores de estómago invadieron a las personas sin importar edad o género, sus pieles se llenaron de brotes y uno a uno fueron rindiéndose al dolor, y aunque sus médicos ancestrales también se encuentran en los refugios, estos se están maniatados en un lugar en donde no pueden invocar a sus espíritus ni recurrir a la tierra para conseguir los remedios.

En cuanto a la acción oportuna del Estado, Aidé manifestó que desde el primer momento del desplazamiento los Wounaan sintieron el rechazo y el desprecio de las autoridades, que solo los han determinado cuando ha existido denuncia y presencia por parte de organizaciones defensoras de derechos humanos.

Por lo demás, las atenciones médicas han sido tan mediocres como en el resto de Colombia con médicos que recetan acetaminofén para todas las dolencias, situación que ya ha provocado la muerte de un niño, el estado crítico de otro más y de dos adultos mayores. Las almas de los Wounaan están peligro.

Pichimá Dosig: El hogar de los espíritus

El nombre del Resguardo en el dialecto Wounaan es Pichimá Dosig, que traduce Pichimá Quebrada y es porque justamente para llegar a este recóndito lugar, hay que embarcarse durante más de dos horas por el Río San Juan, pasar por dos desembocaduras en el mar Pacífico, una jungla de manglares y luego entrar por la quebrada Pichimá para llegar a la comunidad.

Ese camino fue el que recorrieron habitantes del Resguardo y organizaciones defensoras de derechos humanos, el pasado 24 de agosto, para constatar la falta de garantías de seguridad por parte del Estado que les imposibilita regresar.

Una vez pisaron tierra, las ansias por entrar a sus casas era evidente. Con pasos apresurados Gloria se retiró a su vivienda, ubicada a un costado de la cancha de Fútbol, para ratificar sus temores. La Fuerza Pública ingresó a su hogar y dañó dos butacas, en la cocina unas abejas decidieron construir su panal, mientras que las hierbas se subieron por las vigas de madera.

Y si las casas se sentían sin alma, la comunidad estaba sin vida, Dionel un Tonguero o médico espiritual, explicó que uno de los daños más grandes tras el enfrentamiento, fueron los sonido de las armas y las explosiones que espantaron a los espíritus guardianes y ahora se sentía un desequilibrio en el Resguardo, que solo podía repararse con un trabajo muy fuerte. Razón por la cual, si el retorno se hace efectivo, primero tendrá que llegar un grupo de Jaivanás o guías espirituales, para traer la armonía al territorio.

Diaag Maag: Retornar

Este es el segundo desplazamiento forzado del Resguardo Pichimá Quebrada. El primero lo padecieron en el año 2016, cuando el Ejército realizó un bombardeo cerca a sus hogares. En esa ocasión también llegaron a Docordó, y fue la desesperación la que hizo que regresaran a su territorio sin ninguna garantía por parte del Estado, hecho que implicó poner en riesgo la vida de cada uno de los integrantes de esta comunidad.

Por eso, en esta oportunidad, los Wounaan decidieron organizarse y hacer un plan que reúna todas las condiciones que necesitan para poder regresar, porque según Dionel, Diaag Maag significa retornar, e implica mucho más que volver.

Diaag Maag para Gloria es alegría, una que la atraviesa de pies a cabeza, porque si logra volver a su casa podrá vestirse solamente con su paruma o falda, sus senos ya no estarán atrapados en camisas que encierren su libertad, ni su cuerpo sufrirá los moralismos occidentales. Significa que su hija dejará de tener pesadilla o de pedirle a Ewandan que proteja su casa y significa vivir en paz.

Para Aidé, Diaag Maag traduce recuperar su misión ancestral de continuar en la enseñanza de su cultura, regresar a dar clases y una vez terminé la jornada, bañarse con las niñas y niños en el río de agua pura. Traduce tardes en su casa tejiendo artesanías de Güérregue y dejar de tener miedo.

https://youtu.be/Tjam6pwN\_64

Mientras Dionel es mucho más concreto Diaag Maag, es volver a la familia y el hogar; y para que esto sea una realidad, el Resguardo está exigiendo a las autoridades correspondientes que vayan al territorio y certifiquen que no hay peligro, es decir que no hay presencia de grupos armados o de artefactos explosivos, que no hay siembra de minas antipersona y sobretodo que el lugar es apto para ser habitado, y lo piden pronto porque cada día que pasan en Docordó, sus descendientes se exponen a ser revictimizados por un mundo desequilibrado espiritualmente desde hace mucho, que obligó a Ewandan a bajar de sus montañas para atraparlo en un frío albergue.

[![Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ESMAD-pedagógica-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/)

#### [Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/)

Según varios relatos de estudiantes de la Universidad Pedagógica, el artefacto que explotó al interior del campus habría…  
[![Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/palacio-de-justicia-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/)

#### [Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/)

Jesús Armando Árias Cabrales, comandante de la Brigada XIII responsable de liderar la retoma del Palacio de Justicia…  
[![Comunidad Huitoto atribuye al Ejercito Nacional el asesinato de Victor Chanit](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comunidad-Huitoto-Victor-Chanit-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/comunidad-huitoto-llora-asesinato-de-victor-chanit/)

#### [Comunidad Huitoto atribuye al Ejercito Nacional el asesinato de Victor Chanit](https://archivo.contagioradio.com/comunidad-huitoto-llora-asesinato-de-victor-chanit/)

El 22 de septiembre , fue encontrado sin vida el mayor y guía espiritual Víctor  Manuel  Chanit  Aguilar,…  
[![Siga en vivo el lanzamiento del libro 'El Aprendiz del Embrujo'](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Aprendiz-del-Embrujo-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/siga-en-vivo-el-lanzamiento-del-libro-el-aprendiz-del-embrujo/)

#### [Siga en vivo el lanzamiento del libro 'El Aprendiz del Embrujo'](https://archivo.contagioradio.com/siga-en-vivo-el-lanzamiento-del-libro-el-aprendiz-del-embrujo/)

El Aprendiz del Embrujo realiza un balance del primer año del presidente Iván Duque y sus políticas denunciadas…  
[![Fuerza Pública y exparamilitares relatan cómo concertaban desapariciones forzadas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Desapariciones-HH-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/fuerza-publica-exparamilitares-concertaban-desapariciones-comision-verdad/)

#### [Fuerza Pública y exparamilitares relatan cómo concertaban desapariciones forzadas](https://archivo.contagioradio.com/fuerza-publica-exparamilitares-concertaban-desapariciones-comision-verdad/)

Excombatientes de FARC, AUC e integrante del Gaula narraron a la Comisión de la Verdad cómo efectuaban desapariciones…  
[![Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ESMAD-pedagógica-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/)

#### [Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/)

Según varios relatos de estudiantes de la Universidad Pedagógica, el artefacto que explotó al interior del campus habría…  
[![Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/palacio-de-justicia-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/)

#### [Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/)

Jesús Armando Árias Cabrales, comandante de la Brigada XIII responsable de liderar la retoma del Palacio de Justicia…  
[![Comunidad Huitoto atribuye al Ejercito Nacional el asesinato de Victor Chanit](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comunidad-Huitoto-Victor-Chanit-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/comunidad-huitoto-llora-asesinato-de-victor-chanit/)

#### [Comunidad Huitoto atribuye al Ejercito Nacional el asesinato de Victor Chanit](https://archivo.contagioradio.com/comunidad-huitoto-llora-asesinato-de-victor-chanit/)

El 22 de septiembre , fue encontrado sin vida el mayor y guía espiritual Víctor  Manuel  Chanit  Aguilar,…  
[![Siga en vivo el lanzamiento del libro 'El Aprendiz del Embrujo'](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Aprendiz-del-Embrujo-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/siga-en-vivo-el-lanzamiento-del-libro-el-aprendiz-del-embrujo/)

#### [Siga en vivo el lanzamiento del libro 'El Aprendiz del Embrujo'](https://archivo.contagioradio.com/siga-en-vivo-el-lanzamiento-del-libro-el-aprendiz-del-embrujo/)

El Aprendiz del Embrujo realiza un balance del primer año del presidente Iván Duque y sus políticas denunciadas…  
[![Fuerza Pública y exparamilitares relatan cómo concertaban desapariciones forzadas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Desapariciones-HH-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/fuerza-publica-exparamilitares-concertaban-desapariciones-comision-verdad/)

#### [Fuerza Pública y exparamilitares relatan cómo concertaban desapariciones forzadas](https://archivo.contagioradio.com/fuerza-publica-exparamilitares-concertaban-desapariciones-comision-verdad/)

Excombatientes de FARC, AUC e integrante del Gaula narraron a la Comisión de la Verdad cómo efectuaban desapariciones…
