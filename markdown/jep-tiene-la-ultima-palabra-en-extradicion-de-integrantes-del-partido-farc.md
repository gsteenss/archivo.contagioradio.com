Title: JEP tiene la última palabra en extradición de integrantes del Partido FARC
Date: 2018-05-30 13:48
Category: Nacional, Paz
Tags: Aprobación de la JEP, extradicion, JEP
Slug: jep-tiene-la-ultima-palabra-en-extradicion-de-integrantes-del-partido-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/03-02-2015justice_gavel-e1527700435872.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UNODC] 

###### [30 May 2018] 

Luego de que las comisiones primeras conjuntas, tanto del Senado como de la Cámara, aprobaran los artículos que reglamentan el código de procedimiento para la **Jurisdicción Especial para la Paz,** algunos analistas reiteraron que, en lo que tiene que ver con la extradición, la JEP debe resolver las fechas en que fue cometido el delito y mientras esto ocurre, es necesario suspender el trámite.

Dentro del debate, recordaron que la JEP tiene como funciones, en lo relacionado con la extradición, decretar pruebas para determinar **la fecha del presunto delito** para saber si ocurrió antes o después de la firma del Acuerdo de Paz. Adicionalmente, debe contar con el material que requiera para emitir conceptos y no puede suspender ningún proceso de extradición.

De acuerdo con Alfredo Beltrán, jurista y ex magistrado de la Corte Constitucional, el **Acto legislativo 01 de 2017** indica que la JEP debe resolver las fechas en que fue cometido el acto delictivo para saber si fue antes o después de la firma del Acuerdo de Paz y así determinar su jurisdicción.

Indicó que para que se pueda establecer esto, “debe tener pruebas sobre la fecha en la que se hubiere cometido el hecho”. Esto hace parte de la garantía de **ser juzgados por la JEP** y que debe dar el Estado colombiano cuando los hechos delictivos cometidos en el exterior ocurrieran antes de una fecha determinada. (Le puede interesar:["Fiscalía tiene 10 días para entregar pruebas a la JEP en caso Santrich"](https://archivo.contagioradio.com/santrich-pruebas-extradicion-jep/))

Por esto, Beltrán argumentó que lo que el Congreso ratificó, **“no es nada distinto a lo que establece el Acto Legislativo 01 de 2017”**. Con esto en mente explicó que la JEP nunca ha asumido la posición de determinar si suspende o no los procesos de extradición a integrantes de las FARC.

Según el analista, lo que ha hecho la Jurisdicción Especial de Paz es aclarar que, mientras se resuelve el caso **con fundamento en las pruebas,**  "tiene que suspenderse el trámite por fuerza de las circunstancias”.  Esto quiere decir que “la JEP no se ha sobrepasado en sus funciones y, por el contrario, está obrando bajo los supuestos con los cuales fue creada”.

###### <iframe id="audio_26264379" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26264379_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
