Title: “Si no hay acuerdo ni pensemos que el paro se levanta” Paro de profesores
Date: 2017-06-16 11:37
Category: Educación, Nacional
Tags: ade, fecode, Paro de profesores
Slug: paro-de-profesores-sigue
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-2-800x5501.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: USO] 

###### [16 Jun 2017] 

Henry Sánchez, asesor de FECODE aseguró que no son muchas las expectativas frente al encuentro de ese sindicato con el Ministerio de Hacienda, dado que cuando han estado presentes los representantes de esa entidad la respuesta siempre ha sido que “no hay plata”. **Sánchez señala que si esa sigue siendo la postura, es muy probable que continúe el paro de profesores**.

Aunque no se ha establecido si es el Ministro de Hacienda el que participa en la reunión de hoy, FECODE asegura que la claridad que hay que hacerle al país es que el paro de profesores no es por el salario de los maestros sino por la financiación a mediano y largo plazo de la educación Pública. Sobre ese punto Sánchez señaló que son **80 billones de pesos los que ha perdido la educación**, sin contar con lo que ha perdido la educación superior.

### **Exigencias de los maestros contemplarían una reforma constitucional** 

Para Sánchez, la clave en el argumento de Sánchez es que haya argumentos claros en torno a la fórmula sobre la proyección “juiciosa” de la financiación de la educación pública que llegue a convertirse en ley, es decir que se debería hacer una reforma constitucional y por eso afirma que lo que exige el paro del magisterio es de gran alcance, respecto a los "pañitos de agua tibia" que propone el gobierno.

En torno a la duración del Paro que este 16 de Junio completa 36 días, Sánchez, señaló que aunque en **Colombia hay una costumbre de hacer paro de 2 o 3 días en los que se logran algunas “moronas”** y pone el ejemplo del aumento del salario, por ello es necesario que se entienda que los maestros consideran esta movilización como “histórica” por el mismo alcance de las exigencias que están haciendo.

**Gobierno no ha cumplido las metas en educación planteadas en el plan de desarrollo**

La idea es que no se le permita al gobierno que se desmejore la educación pública y se facilite el camino de la privatización. El ejemplo más fuerte es el del sistema de salud, que desde 2001 ha sido una catástrofe para los colombianos. En ese sentido se resalta la necesidad de ampliar la planta docente y destinar recursos suficientes para la infraestructura.

Sanchez afirma que el Paro de Maestros no está exigiendo cosas que estén por fuera de la norma sino que son exigencias que tienen que ver con el plan de desarrollo y los planteamientos que hizo. “Miremos el cumplimiento de las metas en educación”, según él no se han resuelto los problemas que el plan pretendía atacar, además se aplazan las metas que se habían planteado. [Lea también: Hay prouestas pero el gobierno no tiene voluntad de negociar](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

### **Es al gobierno al que hay que reclamarle por el paro de maestros** 

Por otra parte también aseguro que como se ha dicho anteriormente, si no se cumplen las exigencias y si no se logran los avances que se han planteado se seguirán endureciendo las protestas. En ese sentido afirma que entiende los malestares de la ciudadanía y en ese sentido no hay que reclamarle a los maestros y maestras sino reclamar al gobierno por no cumplir y no garantizar el derecho de todos y todas. [Lea también: FECODE rechaza violencia del ESMAD contra profesores en Nariño](https://archivo.contagioradio.com/fecode-rechaza-violencia-del-esmad-contra-profesores-en-narino/)

Además aseguró que en el paro de profesores los maestros están asumiendo una carga presupuestal muy fuerte, es decir, el sacrificio es de ellos y ellas, e insistió en que la ciudadanía bogotana debería exigir al alcalde Peñalosa que se siente a negociar con la Asociación Distrital de Educadores lo que tiene que ver concretamente con la situación de la educación en la ciudad.

<iframe id="audio_19305869" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19305869_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
