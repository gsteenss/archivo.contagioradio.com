Title: Universidades se unen por la protección animal
Date: 2015-09-29 11:00
Category: Animales, eventos, Voces de la Tierra
Tags: Pontificia Universidad Javeriana, protección animal, Unión Animalista, Universidad de la Salle, Universidad de los Andes, Universidad del Rosario
Slug: universidades-se-por-la-proteccion-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/perrooo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: bugui.org 

###### [29 Sept 2015] 

Del 28 de septiembre al 4 de octubre  la Unión Animalista Universitaria realizará la primera Semana Interuniversitaria por la Protección Animal.

Las actividades se ejecutarán  en diferentes universidades como La Universidad del Rosario, Universidad de los Andes, Universidad de la Salle, Pontificia Universidad Javeriana, con el fin de promover temas como la **consulta antitaurina; la tenencia responsable de animales de compañía; la adopción y esterilización  de caninos y felinos, entre otros.**

Más información en [Semana Interuniversitaria por la Protección Animal.](https://www.facebook.com/events/1089847311043526/)

Lunes Septiembre 28  
Tema: Foro: Empatía, moralidad y justicia en animales no humanos.  
Hora: 5 PM  
Lugar: Auditorio por confirmar, Universidad del Rosario (Sede Centro)

Fecha: Martes Septiembre 29  
Tema: Cineforo (película por confirmar)  
Hora: 5 PM (apertura de puertas 4:30 PM)  
Lugar: Salón W 101, Universidad de los Andes.

Fecha: Miércoles Septiembre  30  
Tema: Conservatorio sobre zoofilia.  
Hora: 11 AM  
Lugar: Auditorio por confirmar, Universidad de la Salle (Sede norte)

Fecha: Jueves Octubre 1  
Tema: Taller "Vínculo humano-animal"  
Hora: 6 PM  
Lugar: Auditorio Marino Troncoso, Pontificia Universidad Javeriana.

Fecha: Domingo Octubre 4: Marcha Mundial por los Derechos de los Animales.  
Hora: 9 AM  
Lugar: Plaza de Todos la Santa maría.
