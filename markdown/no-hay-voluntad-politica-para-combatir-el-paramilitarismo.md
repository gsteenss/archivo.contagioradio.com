Title: "No hay voluntad política para desmontar el paramilitarismo"
Date: 2017-01-25 14:32
Category: Nacional, Paz
Tags: Jurisdicción Especial de Paz, Paramilitarismo, Unidad de investigación paramilitarismo
Slug: no-hay-voluntad-politica-para-combatir-el-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/paramilitares-tolima-AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo] 

###### [25 Ene 2017] 

Organizaciones sociales y defensoras de derechos humanos expresaron su preocupación frente a la falta de acción por parte del gobierno, en compromisos como la **conformación y activación de la Unidad Investigativa y de Desmantelamiento de las Estructuras Paramilitares**, que daría luces sobre quiénes se encuentran detrás de los asesinatos a líderes, defensores y defensoras de derechos humanos en el país.

Esta denuncia se hizo en medio de una Audiencia en la que participó  la abogada Soraya Gutiérrez, integrante del Colectivo de Abogados José Alvear Restrepo, quién señaló que dicha Unidad  **“todavía no se ha creado,  ha sufrido modificaciones y  que el Fiscal General de la Nación no ha tenido voluntad política, aún, de dar los recursos y ponerla  a funcionar”**. Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP2](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

Pese a que diferentes **informes de derechos humanos como el de[Indepaz](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/) o Somos defensores, han señalado que la presencia del paramilitarismo persiste en el país** y que es necesario su desmote para alcanzar una paz estable y duradera, el Ministro de Defensa Juan Carlos Villegas expresó  que “**En Colombia no hay paramilitarismo.** Decir que en Colombia hay paramilitarismo significaría otorgar un reconocimiento político a unos bandidos dedicados a la delincuencia común u organizada”

No obstante, el Ministro del Interior Juan Fernando Cristo reconoció la presencia del grupo paramilitar “Clan del Golfo” en el Urabá, estructura que estaría ocupando los lugares en los que antes estaban ubicadas las FARC-EP y señaló que “el Gobierno Nacional actuará con toda la contundencia contra este grupo criminal”. Al mismo tiempo, pidió aumentar las medidas de protección a líderes y defensores de derechos humanos en las diferentes regiones del país.

**En los primeros 25 días del año han asesinado a 15 líderes y defensores de derechos humanos, mientras que las comunidades continúan denunciando la presencia** de grupos paramilitares cerca de las zonas veredales en donde se reagrupan los miembros de las FARC-EP, sin que exista un accionar por parte de la Fuerza Pública.

<iframe id="audio_16652386" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16652386_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
