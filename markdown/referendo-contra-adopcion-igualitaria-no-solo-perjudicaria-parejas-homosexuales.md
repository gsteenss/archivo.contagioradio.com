Title: Referendo contra adopción igualitaria no solo perjudicaría a parejas homosexuales
Date: 2016-03-28 11:11
Category: LGBTI, Nacional
Tags: Adopción igualitaria, referendo, Vivian Morales
Slug: referendo-contra-adopcion-igualitaria-no-solo-perjudicaria-parejas-homosexuales
Status: published

###### Foto: Colombia Gay Parade 

###### 28 Mar 2016 

Luego de que el pasado 4 de noviembre **la Corte Constitucional fallara a favor de la adopción igualitaria en Colombia**, este miércoles 29 de marzo, 32 Congresistas y otros ciudadanos de confesiones religiosas entregarán las **2 millones 135 mil firmas** que lograron recolectar  para un **referendo contra** esta medida, pero que además, estaría incluyendo que personas solteras tampoco puedan adoptar.

La planilla que contienen las firmas, en principio indica que la propuesta es que no puedan adoptar las parejas del mismo sexo, pero sólo unas letras más adelante **asegura que tampoco podrán hacerlo las personas solteras**.

La propuesta se ampara en el supuesto de que “no existen estudios científicos concluyentes y mucho en menos en Colombia que permitan afirmar que no hay afectaciones negativas en el desarrollo integral de un menor” y agrega que “ante la **duda razonablemente fundada**… es obligación del Estado y la sociedad **proteger al niño de potenciales daños**”, por lo que “dentro de los distintos tipos de familia, la más adecuada para garantizar su desarrollo es la familia conformada por un hombre y una mujer”.

Es así como el referendo constitucional promovido para prohibir que parejas homosexuales adopten, **afectaría también a aquellas personas solteras que deseen hacerlo**, pese a que esto último no sea anunciado por los promotores de la iniciativa, quienes deberán alcanzar un mínimo de 1.8 billones de firmas para obtener el aval de la Registraduría.

Por su parte, el abogado y defensor de los derechos de la comunidad LGBTI, Germán Rincón Perfetti, ha asegurado que “**los derechos humanos no pueden ser objeto de consulta ciudadana**” y agrega que personas como la senadora Vivian Morales, que es abogada, saben que no se puede llamar a este tipo de mecanismos para tomar esa decisión.

Aquí el texto que contiene la planilla.

[![PLANILLA 2 001](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/PLANILLA-2-001.png){.aligncenter .size-full .wp-image-21943 width="1024" height="593"}](https://archivo.contagioradio.com/referendo-contra-adopcion-igualitaria-no-solo-perjudicaria-parejas-homosexuales/planilla-2-001-2/)[![adopcion](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/adopcion-.png){.aligncenter .size-full .wp-image-21945 width="1085" height="729"}](https://archivo.contagioradio.com/referendo-contra-adopcion-igualitaria-no-solo-perjudicaria-parejas-homosexuales/adopcion/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
