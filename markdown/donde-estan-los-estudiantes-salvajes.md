Title: ¿Dónde están los estudiantes salvajes?
Date: 2017-09-15 11:07
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: campañas políticas, estudiantes, politica
Slug: donde-estan-los-estudiantes-salvajes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/IMG-20170915-WA0004-01.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**

###### 15 Sep 2017

Colombia es un Estado fallido en términos administrativos y políticos. Podría asegurar que la peor tragedia administrativa de Colombia no es su corrupción. Quienes afirman eso, puede que tengan que lidiar aún con la falsa consciencia producida por los medios masivos de información en donde se muestran “escandalizados” con todo lo que pasa, pero en la práctica, sus dueños y financiadores son amigos, juegan golf, se acompañan en las bodas y almuerzan en el club con los mismos políticos y empresarios responsables de la barbarie.

La falsa consciencia viene  empaquetada en slogans de campaña que no son propios de un reconocimiento del problema político, sino más bien de un marketing político que avanza con fuerza sometiendo a los sujetos al error garrafal de individualizar los problemas del país, concluyendo de la manera más hueca que los pobres son los culpables de ser pobres,  que los paros son los responsables de la crisis, burlándose de los campesinos por no querer ser empresarios, señalando a la crítica “por no dejar avanzar”, creyendo que la naturaleza no tiene derechos, o incluso culpando al sujeto que no sale a votar, de  los robos que hace un politiquero… ¡¡claro!! con tal de ponernos a todos contra todos, con tal responsabilizar a todos, incluso al amigo, incluso al vecino… menos al que en realidad hace el daño.

La falsa consciencia tiene atrapado al individuo en un eterno presente lleno de mercancía a su disposición para sublimar la chiquitez de su deseo de libertad, para sublimar lo efímero de la concepción de la felicidad, porque al final, en el colegio, en el instituto, en la universidad lo inundaron de presente y lo obligaron sin la fuerza a darle la espalda al pasado, a ignorar la multidimensionalidad, cercenando de paso la posibilidad de construir utopía y configurando en últimas, personas que no piensan más allá de su nariz.

Grandes esfuerzos discursivos e intervenciones se llevan a cabo en muchas universidades, escuelas y colegios para explicar, en el marco académico, que la corrupción es tan solo un síntoma. Por supuesto en esos mismos escenarios, masiva y lastimosamente, hay profesores comentando a sus estudiantes que no expondrán a un autor por estar “pasado de moda” o que animan a los estudiantes a que generen ideas nuevas sin haber profundizado problemas teóricamente, es decir sin saber dónde están parados.

Así como muchos se dieron cuenta de que la guerra no era el problema del fracaso del modelo político colombiano. Con más fuerza se consolida la idea de que la corrupción tampoco es el problema; ¿acaso no nos damos cuenta de que “escándalo” tras “escándalo” de corrupción, igual no pasa absolutamente nada? ¿Por qué?

Sencillamente porque la corrupción no es el problema, es el síntoma de una enfermedad agresiva producto de un virus que ha entronizado al “yo” produciendo individualismo hueco y degradante. Un virus que ha vendido la “liberación” impulsando placeres efímeros para que los sujetos continúen fabricando los barrotes de su propia celda en la que la única libertad que tienen es la de comprar y fortalecer así  al modelo sin cuestionarlo… sí… eso es un virus, el virus del neoliberalismo.

Éste virus está instalado en nuestra época como organigrama del modelo productivo que repercute en lo ideológico, se instaura en lo psicológico y ahora hasta en lo neurológico, es decir ¡¡un biopoder en todo el sentido de la palabra!! se encuentra inserto en las venas más íntimas de nuestra sociedad, no se nota… no se siente… se diluye en los escenarios sociales de cuerpos domesticados porque les escondieron el filtro de la capacidad crítica.

¿Dónde emerge la capacidad crítica? No hay de otra, la historia lo demuestra, la única salida es una educación que subvierta la comprensión de la problemática y aquellas prácticas de estudiantes que pronto prontísimo serán los “nuevos” profesionales, mucho más “flexibles académicamente” es decir adecuados a hacer lo que les toque y no lo que soñaron, profesionales que hoy componen como ya lo advierte Pepe Mujica: “el nuevo proletariado” es decir personas tituladas, muy productivas pero sin consciencia; ese “nuevo proletariado” en formación está domesticado por el consumismo, bien sea de ideas tales como “*libertá” “sea su propio jefe”, “haga lo que quiera”* o bien de mercancías innecesarias que, al comprarlas, sin darse cuenta los vuelven más presos, más dóciles.

¿Dónde están los estudiantes salvajes? ¿dónde están los estudiantes que tenían la capacidad de enfrentarse al sistema con su única arma, es decir, el ímpetu revolucionario propio de la juventud? Y que no vengan a decir que el ímpetu revolucionario de la juventud es un cuento trasnochado, porque quienes dicen eso, ni siquiera se dan cuenta de que esa idea es prestada y que su libertad de expresión, precisamente al ser oficio de una falsa libertad en ocasiones no les alcanza para dar una opinión política seria.

¿Dónde están los estudiantes salvajes que desconfiaban de las identidades si éstas en realidad no los liberaban? ¿dónde están los estudiantes salvajes que desconfiaban del profesor, de la academia misma si éstos en esencia no eran más que herramientas al servicio de la mercantilización del sujeto y de la educación?

¿Dónde están los estudiantes salvajes que aunque sea pintaban una pared con el único propósito de decir **¡¡existimos y no estamos de acuerdo con su barbarie!!**? ¿se dejaron comer cuento de la estética impuesta con el auspicio de las leyes que siempre les han traicionado? ¿dónde están esos estudiantes salvajes que enfrentaban al Esmad con rosas, besos, petos, fuego y carnavales? ¿los han domesticado a todos? si así fue,  ¿acaso se dieron cuenta en qué momento ocurrió?

Que no vengan a dar terapia los frustrados, argumentando que salir de la universidad, cambiar de trabajo o tener hijos, son premisas de la domesticación tan infame en la que muchos se envolvieron… eso es cobardía. Por supuesto, si hay algo de salvajismo en este artículo, debe ser que hay algo de coherencia con lo mencionado, por tanto, no sería ofensa que lo llamaran “salvaje” si dicho concepto está alejado de su acepción colonialista, total, aquí se está preguntando por dónde están los estudiantes salvajes.

**¿Dónde están los estudiantes salvajes que pueden detener esta barbarie?**

¿Convencidos por los más domesticados de que lamiendo las botas del consumo “a su manera” son más libres?  ¿Convencidos por algunos profesores de que *“esta mierda no la cambia nadie”* cuando ni esos mismos profesores han podido cambiar lo mezquinos que siguen siendo aún después de dar la clase?  ¿Será que andan convencidos de que el carácter legal o ilegal del producto que consumen los hace menos esclavos de un sistema que le interesa movilizar mercancías sea como sea?

Dónde están los estudiantes salvajes, si este todo llamado Colombia los necesita tanto y tanto.

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
