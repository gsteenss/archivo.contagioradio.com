Title: El antes y después de la 73ª Asamblea Mundial de la Salud
Date: 2020-06-03 10:17
Author: Mision Salud
Category: Mision Salud, Opinion
Tags: Salud
Slug: asamblea-mundial-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/salud-8.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Por **[Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por Angela Acosta\***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**\* Directora de Misión Salud**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*“La salud como derecho económico, social, cultural, tiene como sustrato la igualdad. Por lo mismo, aun aplicando el principio del cumplimiento progresivo de los DESC, esto no significa la aplicación de estándares heterogéneos en la población, como tampoco la paralización de las mejoras argumentando la falta de recursos públicos.”*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Salud Universal en el Siglo XXI: 40 años de Alma-Ata -Comisión Alto Nivel OPS 2019*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Antes de la Asamblea Mundial de la Salud 73: la Atención Primaria en Salud (APS)**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito de los cuarenta años de la Conferencia de Alma Ata (1978) sobre Atención Primaria en Salud (APS) (organizada por la Organización Mundial de la Salud y Unicef) en la que se acordaron los conceptos y componentes esenciales de la APS, escenarios como el desarrollado desde la Organización Panamericana de la Salud, en el cual una Comisión de Alto Nivel conformada por exmandatarios, líderes comunitarios y académicos, recuperaron en 2018 lo que dicha Conferencia estableció como APS.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el día de hoy dichos componentes esenciales están en proceso de incorporación a los sistemas de salud como parte de la consecución social del derecho a la salud, la universalidad en el acceso y la prelación de lo colectivo sobre lo individual (*Comisión Alto Nivel OPS 2019[**\[i\]**](#_edn1)).*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La actual emergencia sanitaria evidencia, hasta el momento, que la mejor medida de contención de la expansión y de los desenlaces de la enfermedad es la cuarentena con seguimiento y tratamiento domiciliario, ya lo decía la Declaración de Alma Ata en su momento: reforzar la vigilancia epidemiológica, proteger colectivos de riesgo y poblaciones vulnerables, fortalecer los centros de atención para promoción y prevención de medidas oportunas (no la financiación de servicios, equipos e intervenciones en salud que segmentan la posibilidad de todos).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**¿Qué paso en la 73ª Asamblea Mundial de la Salud?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los pasados 18 y 19 de mayo se llevó a cabo la 73ª Asamblea Mundial de la Salud y, si bien la agenda solo desarrolló los puntos de la plenaria inicial, no es poco relevante que el punto“Atención Primaria en Salud”, que era el primero previsto en la Agenda del Comité A, y primer punto del Pilar 1 de la Asamblea, incluso antes del punto de cobertura universal (enfoque que por el término “universal” muchos consideran sinónimo del acceso universal, hoy masivamente adoptado como suele pasar con mucha otra terminología sanitaria) Aun cuando varios países manifestaron la importancia de la APS en su actual quehacer en la plenaria, ni la Resolución WHA73.1 “Respuesta a la COVID-19”, ni las declaraciones mismas fueron suficientes para priorizar coyunturalmente asuntos que no obedecen a las condiciones de financiamiento y segmentación de la cobertura de los sistemas de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Prueba de esto es que la [Resolución aprobada](https://apps.who.int/gb/ebwha/pdf_files/WHA73/A73_R1-sp.pdf) solo hace un ***“llamamiento”*** a que los países adopten algunas medidas para apoyar el acceso a saneamiento y la higiene, la prevención y el control de las infecciones, velando por que se preste la debida atención a la promoción de medidas de higiene personal. Igualmente, los países logran hacer un llamado a las desfavorables desigualdades, tan mencionadas en el documento “Salud Universal en el Siglo XXI: 40 años de Alma-Ata” (*Comisión Alto Nivel OPS 2019).  *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Sin embargo, tanto en la Asamblea de 2019 como en la plenaria 2020 lo que se prioriza como estructura principal de los sistemas de salud es la cobertura,* la limitante excluyente de las coberturas y priorizaciones de los sistemas de salud es que terminan enfocando pocos esfuerzos a servicios de promoción, prevención general a la población y prevalece la demanda específica de servicios. El financiamiento termina ocupándose de robustecer infraestructuras y de financiar intervenciones en salud las cuales pueden no ser las más pertinentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera la Resolución WHA73.1 queda huérfana en su preocupación *“por la morbilidad y la mortalidad causadas por la pandemia de COVID-19, sus efectos negativos en la salud física y mental, el bienestar social, las repercusiones negativas en las economías y las sociedades y la consiguiente exacerbación de las desigualdades dentro de los países y entre ellos”. *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los sistemas de salud en los países que buscan universalizar el acceso sin discriminar condiciones particulares son probablemente más tendientes a la igualdad que otros que lo segmentan, por ejemplo, según las posibilidades de aportes financieros de la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**¿Qué vendrá?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la actual emergencia sanitaria la mayoría de las personas contagiadas (entre el 80-90%) no han requerido uso de servicios hospitalarios y su seguimiento es domiciliario. La educación sanitaria, el reconocimiento individual y colectivo se vuelven fundamentales en la superación de este momento, es decir rescatar los elementos que reconocemos como APS.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En su momento cobrarán igual importancia las funciones de promoción y prevención, y esperemos que el reconocimiento de la 73ª Asamblea *“la inmunización extensiva contra la COVID-19 como bien de salud pública mundial”*, favorezca la superación de la emergencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En específico los medicamentos no son las intervenciones más inmediatas para el tratamiento de la COVID- 19, vendrá seguramente su momento. Al respecto, hoy más que nunca requerimos consenso social frente a la esencialidad de estos Durante la Asamblea Mundial de la Salud de 2019 veíamos con preocupación cómo la respuesta a alertas globales sobre no disponibilidad de medicamentos esenciales (como los antibióticos benzatínicos) apuntaban al incentivo al modelo actual de innovación, tal como lo establecen los Objetivos de Desarrollo Sostenible. ¿Teniendo ya alternativas terapéuticas desarrolladas y probadas por más de 60 décadas, valdrá la pena seguir insistiendo en este tipo de respuestas?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[**Angela Acosta**](https://twitter.com/AngelaAcostaSan)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Directora General - Misión Salud

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:paragraph -->

[\[i\]](#_ednref1) <https://iris.paho.org/handle/10665.2/50742>

<!-- /wp:paragraph -->
