Title: FLIP alerta sobre información que estaría ocultando EPM y el SIATA en Hidroituango
Date: 2018-06-02 14:06
Category: DDHH, Nacional
Tags: EPM, Hidroituango, SIATA
Slug: flip-alerta-sobre-informacion-que-estaria-ocultando-epm-y-el-siata-en-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DcMNMHwWkAECdSq-e1525375369701.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [01 Jun 2018]

A través de un comunicado de prensa la Fundación para la Libertad de Prensa, FLIP, denunció que la Empresa Pública de Medellín y el Sistema de Alerta Temprana de Medellón y el Valle de Aburrá, SIATA, estarían incumpliendo con su deber de transparencia, que les "**exige brindar información oportuna, veraz y completa frente a los riesgos de Hidroituango"**.

De acuerdo al comunicado de la FLIP, en los últimos días han tenido conocimiento sobre denuncias hechas por periodistas y ciudadanos en relación al manejo de la información sobre Hidroituango. En ese sentido para la Fundación, el deber que tienen las entidades correspondientes frente a este hecho, con respecto a la información, "**se refuerza en circunstancias de emergencia".**

### **Las denuncias que ha recibido la FLIP**

La primera denuncia que hace la FLIP con respecto a la información que no se ha dado referente a la crisis de Hidroituango, tiene que ver con el sensor **302 que mide el nivel del agua  del río Cauca en la represa y que maneja el SIATA**.

Según las denuncias que han llegado a la FLIP, este sensor fue deshabilitado para consulta al público, a partir de las 10 de la mañana del 20 de mayo. Tiempo después el SIATA respondió que la información si había sido deshabilitada argumentando que " los sensores instalados por SIATA continúan funcionando, la información está siendo entregada al PMU (Puesto de Mando Unificado), para tener una adecuada e integral interpretación de la información. Se continuará informando a toda la ciudadanía en caso de ser necesario”. Horas más tarde SIATA señaló que la información **se desenlazó de su sitio web para evitar “interpretaciones aisladas que pueden ser erróneas”**.

Esta misma situación se presentó el 22 de mayo, en esa ocasión el SIATA señaló que había inconvenientes con la telecomunicaciones. Para la FLIP ocultar esa información es "contrario a las obligaciones que tiene el SIATA" y recordó que por ley (1712 de 2014), esta en la obligación **de actuar de buena fe y facilitar el acceso a la información "en los términos más amplios posibles"**.

### **EPM quita documentos de su página web** 

Otra de las denuncias tiene que ver con EPM, que habría eliminado documentos que antes eran públicos en su página web, para ocultar información relacionada con el proyecto Hidorituango. Para la FLIP, los cuestionamientos que generan estas acciones en los ciudadanos, **levantan dudas sobre la transparencia de EPM y "sobre la eventual responsabilidad disciplinaria o penal por ocultar documentos públicos".**

La FLIP logró establecer comunicación con EPM en donde les respondieron que el retiro de esta información se dio luego de que se considerará confusa para las personas que la consultaban, sin embargo, la FLIP le recordó a la empresa que por ley también debe tener un procedimiento cuando vaya a retirar información de su página web. (Le puede interesar: ["EPM sigue brindando información confusa: Ríos Vívos"](https://archivo.contagioradio.com/epm-sigue-brindando-informacion-confusa-raiz-los-derrumbes-hidroituango-rios-vivos/))

### **Las amenazas a trabajadores de Hidroituango para que no den información a la prensa** 

La FLIP también tuvo conocimiento de denuncias en donde EPM habría amenazado a sus trabajadores para que no revelen información a los periodistas que cubren esta situación. Hecho que genera preocupaciones para la Fundación, debido a que los trabajadores están legitimados para brindar información sobre los riesgos a los que estén expuestas las personas.

Así mismo expresó que "Cualquier tipo de represalia en contra de ellos iría en contra de los estándares internacionales s**obre libertad de información y que protegen a aquellos alertadores que informan sobre irregularidades** que ocurran al interior de las organizaciones para las que ellos trabajan".

De otro lado, John Maya, Vicepresidente de Negocios de EPM, aclaró que en Hidroituango hay más de once mil trabajadores y que no todos son directamente contratados por EPM sino por sus contratistas o asesores. Según los funcionarios Maya y Alzate, desde EPM no se ha dado ninguna instrucción en este sentido.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
