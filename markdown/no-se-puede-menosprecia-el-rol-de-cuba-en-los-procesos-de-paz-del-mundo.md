Title: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo
Date: 2019-10-07 13:48
Author: CtgAdm
Category: Nacional, Política
Tags: acuerdos de paz, Cuba, países garantes
Slug: no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CUba.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

A través de una carta dirigida al presidente Iván Duque, un grupo conformado por más de 60 académicos e intelectuales advierten sobre el error que significaría romper relaciones diplomáticas con Cuba, esto con relación a las recientes declaraciones del alto Gobierno, que de forma ambigua giran en torno una posible ruptura entre ambas naciones.

Ante los frecuentes cuestionamientos del mandatario colombiano e integrantes de su gabinete, quienes insisten en la solicitud de etxradición desde La Habana a los negociadores del ELN - lo que **violaría el protocolo establecido en caso de rompimiento de la mesa de diálogo** - los firmantes alertan sobre el camino que está tomando el gobierno de cara a su relación con el país garante y el agravamiento en sus diferencias, recordando que de esta misma forma fue que se produjo la ruptura diplomática entre Colombia y Venezuela.

**"Lo único que se ha logrado con esta política inútil ha sido un escalonamiento de las tensiones bilaterales. ¿Vamos a repetir con Cuba este camino equivocado?**", cuestiona la carta que menciona la importancia histórica de mantener una cercanía entre Estados.

### Ante Cuba, Colombia debe propiciar una política exterior cooperativa y no confrontacional 

De igual forma, la carta destaca la utilidad de la diplomacia para ambas naciones que comparten una agenda y en particular el importante rol que ha cumplido Cuba como país garante en las negociaciones con las FARC. Resaltan además la importancia del país insular como puente necesario para impedir un agravamiento en la tensión que existe entre Colombia y Venezuela. [(Lea también: Cuba y Noruega reiteran su apoyo al Proceso de Paz)](https://archivo.contagioradio.com/cuba-y-noruega-reiteran-su-apoyo-al-proceso-de-paz/)

Con estos argumentos, quienes firman la carta, solicitan al Gobierno actuar con mayor serenidad y dejar abiertas las puertas para apelar al diálogo con un país que es considerado **"uno de los ejes más sólidos de la tradición de nuestra política exterior"**. [(Le puede interesar: Violar protocolo establecido con ELN sería un irrespeto a la comunidad internacional)](https://archivo.contagioradio.com/protocolo-eln/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
