Title: Rusia y Grecia ultiman acuerdo comercial
Date: 2015-04-08 14:53
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Banco Central Europeo, Grecia, Rusia, Troika, Tsiripas habla de las sanciones de la UE sobre Rusia, Tsiripas visita Putin en Rusia, Unión europea, Visita de presidente Griego a Rusia
Slug: rusia-y-grecia-ultiman-acuerdo-comercial
Status: published

###### Foto:Rtve.es 

Tras la tensión existente entre Grecia y la UE debido a la auditoría de la deuda contraída por los 3 rescates financieros y las sanciones de la UE contra **Rusia** por su papel en el conflicto armado en Ucrania. Hoy se **han reunido en Moscú Vladimir Putin y Álex Tsiripas** para dialogar sobre la **problemática de las sanciones a Rusia** y sobre **inversiones en infraestructuras** por parte de ambos países.

Según **Putin** "*… Grecia no ha pedido ayuda financiera a Rusia. Hemos hablado de realizar distintas inversiones en el terreno de las infraestructuras, transporte, energía,* etc...". Y es que este ha sido el principal tema de las conversaciones entre los mandatarios.

Grecia endeudada, con altos niveles de desempleo, y con su economía basada principalmente en el turismo necesita explorar otros mercados en los que **realizar inversiones para poder pagar la deuda** y mejorar la situación financiera del país.

Por otro lado **Rusia ahogada por las sanciones de la UE** y, con serias **dificultades para distribuir el gas** que antes hacía a través de los gasoductos en **Ucrania**, **necesita otros países** y territorios para poder hacerlo.

Para el país heleno, igual que para España o Italia, las **sanciones contra Rusia han conllevado a una réplica por parte de este país cancelando todas las exportaciones**, principalmente de productos lácteos, verduras y frutas provocando un **grave daño sobre la economía griega** y de todos los países de la UE que exportaban a Rusia.

De esta reunión ya se ha materializado un **acuerdo** entre ambos países, **Grecia se conectará al gasoducto rusoturco y a través de este, Rusia podrá vender energía a la UE**.

El anuncio del acuerdo económico ha **provocado que las bolsas europeas se encogieran**, como por ejemplo el IBEX 35, siendo compañías como el BBVA, Repsol o Hiberdrola, las que más han descendido.

Dicha reunión sucede en medio del clima de tensión entre Grecia y la UE, por la comisión de investigación que ha creado el país heleno para poder auditar la deuda e investigar si es totalmente legítima.

Por último **Grecia** ya ha hecho oficial lo que **Alemania** le **debe por la ocupación nazi**, es decir **278.700 millones de euros en concepto de reparación**. Deuda que Alemania niega al gobierno encabezado por Syriza.
