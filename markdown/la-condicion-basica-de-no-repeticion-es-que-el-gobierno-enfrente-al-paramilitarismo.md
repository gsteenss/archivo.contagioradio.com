Title: “La condición básica de no repetición es que el Gobierno enfrente al paramilitarismo”
Date: 2015-09-25 12:05
Category: Entrevistas, Paz
Tags: Acuerdo de justicia farc y gobierno, acuerdo de justicia firmado en la habana, Alfredo Molano, Diálogos de La Habana, dialogos de paz, impunidad, Justicia Penal Militar, justicia y reparación integral, Neoparamilitarismo, verdad, víctimas
Slug: la-condicion-basica-de-no-repeticion-es-que-el-gobierno-enfrente-al-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Molano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

<iframe src="http://www.ivoox.com/player_ek_8612977_2_1.html?data=mZuelJ6be46ZmKiak5iJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nkMKfxNTbxs7HrYa3lIqvldOPpoa3lIquk9jNp8KfxcqY0NSPtsbkxtnWxc6Jh5SZo5jbjcrXb9LpxpDSzpCrs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alfredo Molano] 

###### [25 Sept 2015] 

[**“La condición básica de no repetición es que el Gobierno enfrente al paramilitarismo”**, manifestó el sociólogo e investigador Alfredo Molano, tras la firma del acuerdo del sistema integral de justicia entre el gobierno colombiano y las FARC.]

[“El acuerdo significa un muy difícil retroceso” sin embargo, la pervivencia del paramilitarismo pone en duda la materialidad de la paz. Según Molano, **falta voluntad política por parte del gobierno colombiano para esclarecer los vínculos de las estructuras paramilitares con militares, políticos y empresarios**.  ]

[De acuerdo con Molano, lo central en este sistema de justicia transicional es la verdad de las dos partes, pues “Las FARC no hicieron solas la guerra”. **Las dos partes en negociación deben ser juzgadas, así como los sectores que están a su alrededor**, agrega. La verdad es el elemento que permite resolver el problema de la cárcel, implica además una sanción social para la satisfacción de las víctimas.  ]

[Las **declaraciones de sectores uribistas frente a la publicación del comunicado encarnan intereses electorales** y las de militares encarnan la intención de seguir siendo juzgados por la justicia penal militar, la que es una muestra de impunidad, concluye Molano. ]
