Title: Abren incidente de desacato contra Duque por operaciones militares de EEUU
Date: 2020-10-16 14:29
Author: AdminContagio
Category: Actualidad, Política
Tags: Brigadas de Estados Unidos, Carlos Holmes Trujillo, Iván Duque
Slug: incidente-desacato-ivan-duque-brigadas-estadounidenses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Por-crimenes-de-lesa-humanidad-denunciaran-a-Ivan-Duque-y-Carls-Holmes-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Duque / Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Tribunal Administrativo de Cundinamarca abrió un incidente de desacato en contra del presidente Iván Duque **por que habría desconocido el fallo que ordenaba desde el pasado 2 de julio suspender las operaciones en el país de la Brigada de Asistencia Fuerza de Seguridad de los Estados Unidos** (SFAB, por sus siglas en inglés) cuya presencia no fue consultada con el Congreso de la República.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión que emitió el Tribunal Administrativo de Cundinamarca se notificó formalmente al Gobierno el 2 de julio como respuesta a la tutela interpuesta por varios congresistas en la que advertían que el presidente Duque había pasado por encima del Senado al permitir que una misión del ejército de EE.UU llegara al país sin tener el aval del Congreso. [(Lea también: Episodio de las Brigadas militares estadounidenses es ejemplo de la política exterior colombiana)](https://archivo.contagioradio.com/episodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Teniendo en cuenta que se concedieron tres días hábiles al presidente para cumplir el fallo este tenía como plazo hasta el 6 de julio para acatar el fallo. Por lo contrario en medio de una rueda de prensa, el ministro Carlos Holmes Trujillo expresó que las tropas de EE.UU. han permanecido activas desde el pasado 20 de julio, **violando la orden judicial del Tribunal Administrativo de Cundinamarca que suspendía las actividades de los 53 soldados que componen esta brigada.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gobierno Duque invita a saltarse las instituciones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

No es la primera vez que el presidente Duque no responde a las exigencias de altos tribunales. Ante las reiteradas peticiones de más de 120 comunidades al Gobierno, exigiendo garantías en los territorios y en relación con una petición de cese al fuego y el amparo de sus derechos fundamentales, **el Tribunal Administrativo de Cundinamarca también había resuelto que en un plazo de cinco días el presidente debía responder de fondo a dichas comunidades,** de no responder a esta decisión el caso iba a ser remitido a la Corte Constitucional para ser revisado. [(Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El incidente se da en la misma semana en que la [Procuraduría General](https://twitter.com/PGN_COL)notificó al ministro de Defensa, Carlos Holmes Trujillo para que en un plazo de cinco días entregue toda la información con relación a las acciones y presencia que han realizado las tropas extranjeras en Colombia, esto ante los derechos de petición hechos por los senadores Iván Cepeda y Antonio Sanguino y que hasta el momento no han recibido respuesta desde la cartera de Defensa. [(Le puede interesar: Holmes Trujillo a responder por Brigadas de EE.UU. en Colombia)](http://(Le%20puede%20interesar:%20Holmes%20Trujillo%20a%20responder%20por%20Brigadas%20de%20EE.UU.%20en%20Colombia))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
