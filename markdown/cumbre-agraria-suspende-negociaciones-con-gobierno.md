Title: Cumbre Agraria suspende negociaciones con gobierno
Date: 2016-10-21 13:27
Category: Movilización, Mujer
Tags: Acuerdos Cumbre, Cumbre Agraria, Minga Nacional
Slug: cumbre-agraria-suspende-negociaciones-con-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cumbre-Agraria.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 de Oct 2016] 

La **Cumbre Agraria Étnica y Popular anunció que se suspenden las negociaciones del pliego único de 8 puntos que se discutían en la Mesa Única Nacional,**  debido a las dilaciones y la falta de voluntad por parte del gobierno. Uno de los problemas es que se financió el arranque de los proyectos pero no su continuidad**.**

### **Las razones de voceros de Cumbre Agraria** 

La última reunión de Cumbre Agraria y gobierno tenía el carácter de revisar el cumplimiento de los acuerdos y tomar decisiones frente a cómo financiarlos,  sin embargo de acuerdo con José Santos, vocero del PCN**, los viceministros  se dedicaron a dar informes de gestión**. Le puede interesar:"[Cumbre Agraria] [y Gobierno definen calendario de acuerdo](https://archivo.contagioradio.com/cumbre-agraria-y-gobierno-definen-calendario-de-acuerdos/)s"

Según Santos, desde el año 2014 los Ministerios de Hacienda y Agricultura **no han asignado los recursos comprometidos para la financiación de los proyectos** productivos por 250.000 millones anuales, pasados 2 años no se han desembolsado en su totalidad  los que tienen vigencia fiscal del 2014 y 2015 por "falta de flujo de caja".

“**Se están dejando los proyectos a medias** cuando en muchas regiones ya existe producción, ejemplo de esto es Buenaventura que en sus consejos comunitarios decidieron sembrar yuca y arroz y tienen la comida almacenada porque no han podido comprar las trilladoras ni las secadoras” afirmó el vocero de Cumbre Agraria.

### **Las alternativas planteadas por la Cumbre** 

 Una posible solución sería que **la Contraloría  y Procuraduría investigue qué pasó con los dineros,** a su vez las comunidades ya se encuentran buscando alternativas para salvar las producciones. De igual forma la vocería anunció que se mantendrá en asamblea permanente a la espera de un llamado de diálogo por parte del Gobierno.

Para Santos "el presidente está conversando con los promotores del No, pero **no genera condiciones para que la gente que apoyamos el sí, tengan posibilidades de construir paz en los territorios** y culminar los proyectos que Cumbre Agraria ha firmado con el gobierno" eso es abonar el terreno de la Paz territrorial, concluye el vocero.

De igual forma los voceros alertaron la **difícil situación por la que están atravesando los defensores de derechos humanos en Colombia**, en lo corrido del año han asesinado a 21 defensores en el país, actos que van en contravía de  una "paz completa". Le puede interesar: ["Cumbre solicitó medidas cautelares de la CIDH"](https://archivo.contagioradio.com/cumbre-agraria-solicito-medidas-cautelares-de-la-cidh/)

<iframe id="audio_13421640" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13421640_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
