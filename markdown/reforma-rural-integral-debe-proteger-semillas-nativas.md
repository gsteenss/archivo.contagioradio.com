Title: "Reforma Rural Integral debe proteger semillas nativas"
Date: 2018-02-15 08:38
Category: Nacional, Paz
Tags: comunidades, reforma rural integral, semillas criollas, semillas nativas
Slug: reforma-rural-integral-debe-proteger-semillas-nativas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/semillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La República] 

###### [14 Feb 2018] 

El 15 de febrero de 2018 se llevará a cabo el Foro “Agro biodiversidad y Derecho Humano a la Alimentación en el contexto de la Reforma Rural Integral”. Esto con el fin de fomentar un diálogo con entidades del Gobierno Nacional para conservar la **agrobiodiversidad de las semillas nativas** y criollas de, por ejemplo, cacao, maíz y arroz, como un insumo para la implementación de la Reforma Rural Integral.

De acuerdo con Mauricio García, coordinador de la campaña Semillas de Identidad de la Fundación Swiss Aid, se quiere plantear la necesidad de valorar las semillas nativas y criollas como un **sustento para la seguridad alimentaria**. Dijo que desde el Estado se envían mensajes de que “las semillas nativas son de mala calidad, que no son productivas y que no sirven para el desarrollo”.

### **Comunidades dependen de las semillas nativas** 

En el foro, las comunidades y diferentes organizaciones mostrarán que estas semillas “son la posibilidad de lograr soberanía y seguridad alimentaria, que pueden responder al cambio climático y que son la posibilidad de generar una posibilidad alternativa de **garantizar alimentación adecuada** a las comunidades y las ciudades en general”.

Las organizaciones han venido realizando propuestas productivas y organizativas donde se establece que “las semillas nativas y criollas son un eje muy importante para el trabajo de las comunidades campesinas”. Por esto, han afirmado que las acciones del Estado “sólo legitiman a las **corporaciones gubernamentales** como productoras válidas de material vegetal de uso agrícola y favorece a las multinacionales”. (Le puede interesar:["Sin implementación del Acuerdo el mundo rural está condenado a desaparecer: Vía Campesina"](https://archivo.contagioradio.com/sin-implementacion-del-acuerdo-el-mundo-rural-esta-condenado-a-desaparecer-via-campesina/))

### **Reforma Rural Integral debe proteger las semillas nativas** 

García recordó que, en el Acuerdo de Paz y específicamente en la Reforma Rural Integral, “el Estado se comprometió a **proteger las semillas nativas y criollas**”. Además, “se comprometió a proteger los sistemas productivos en los cuales ellas se están produciendo” y a vigilar los cultivos transgénicos que “afectan las semillas nativas”.

Para esto, se ha buscado que la Reforma Rural Integral contemple propuestas del Gobierno que protejan las semillas que, “**son patrimonio de las comunidades** que las han creado y ellas las únicas dueñas”. Afirmó que, en la actualidad, la normatividad afecta la venta de estas semillas pues “sólo pueden ser comercializadas si son certificadas”.

Finalmente, García indicó que el foro, que se llevará a cabo en el Auditorio Félix Restrepo de la Universidad Javierana de Bogotá, buscará realizar un contexto de la Reforma Rural Integral al igual que se desarrollarán paneles sobre **agrobiodiversidad** y soberanía alimentaria. Allí, se compartirán experiencias sobre las redes de semillas en diferentes territorios y la importancia para las comunidades.

<iframe id="audio_23787912" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23787912_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
