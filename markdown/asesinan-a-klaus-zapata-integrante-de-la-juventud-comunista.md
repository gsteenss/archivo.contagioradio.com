Title: Asesinan a Klaus Zapata, integrante de la Juventud Comunista en Soacha
Date: 2016-03-07 12:30
Category: DDHH, Nacional
Tags: Juventud comunista, Klaus Zapata, Paramilitarismo, soacha
Slug: asesinan-a-klaus-zapata-integrante-de-la-juventud-comunista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Klaus.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Perfil Facebook 

###### [<iframe src="http://co.ivoox.com/es/player_ek_10706271_2_1.html?data=kpWkkpuWe5Khhpywj5WbaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncaLnxtjW0MbSb8KfrNHO19iPnsLkwtnOh5enb8ri1crU1MbSuMafxcqYzsaPjtbqxtPh18mPh9Dh1pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

###### [Heiner Gaitán, JUCO] 

###### [7 Mar 2016] 

Sobre las **7:00 de la noche de este domingo fue asesinado el líder comunitario Klaus Zapata, miembro de la Juventud Comunista  y de la Red Juvenil de Soacha,** que fue víctima de un sujeto quien le propinó dos disparos una vez había finalizado un partido de fútbol en el barrio Ciudad Latina del municipio de Soacha.

Klaus era estudiante de noveno semestre de Comunicación Social en la Fundación Universitaria Minuto de Dios, además era el responsable de la línea comunicativa de la organización Juventud Comunista en el municipio de Soacha y acompañaba la realización de diferentes talleres audiovisuales con organizaciones como la Red Juvenil de Soacha y Ojo Al Sancocho, en la localidad de Ciudad Bolívar y el municipio de Soacha.

“En principio no creemos que el asesinato esté relacionado con la actividad minera en Soacha, sino más bien porque en el barrio donde vivía Klaus, en la comuna 1, hay presencia de grupos paramilitares", dice Heiner Gaitán, vocero de Juventud Comunista, JUCO, en Soacha, quien agrega que se trata de una zona donde **operan grupos paramilitares ligados al microtráfico, como los Tierreros y los provenientes de Buenaventura.** Un situación que generó la creación de Escuela Itinerante de Derechos Humanos "Jóvenes construyendo un nuevo territorio", de la que hizo parte Klaus. "No descartamos que estas organizaciones estén detrás de este asesinato", señala el vocero de la JUCO.

"**Klaus es semilla, vamos a despedirlo como se lo merece, por él vamos a ganarle esta partida a la guerra y a la muerte",** expresa Gaitán, quien afirma que este fatídico evento no frenará las luchas que abanderaba su compañero.

A su vez, desde la Juventud Comunista en Soacha se realiza un llamado a las personas que se encontraban en el lugar de los hechos para que se acerquen a las autoridades  y realicen la declaración frente a lo ocurrido con el fin de que la Fiscalía actúe y no quede en la impunidad la muerte de este líder social, ya que en el momento, **la persona que disparó huyó del lugar.**

La familia y la organización del líder comunitario han informado que el velorio se realizará en los próximos días y estará acompañado por **una movilización de despedida denominada "Mil claveles blancos, mil aplausos por Klaus"** donde se hará un llamado a erradicar la cultura violenta que se llevó a este líder político y mandar un mensaje contundente a los perpetradores del hecho, como lo sostiene Heiner Gaitán.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
