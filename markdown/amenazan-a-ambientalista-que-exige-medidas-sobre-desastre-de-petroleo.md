Title: Amenazan a ambientalista que exige medidas sobre desastre de petróleo en la Lizama
Date: 2018-06-01 13:44
Category: Ambiente, DDHH
Tags: Lizama, Pozo la Lizama, Río Magdalena
Slug: amenazan-a-ambientalista-que-exige-medidas-sobre-desastre-de-petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/La-Lizama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Costa Noticias] 

###### [01 Jun 2018] 

Después de que se diera el desastre de la Lizama, producto del derrame de petróleo del pozo 158 que pertenece a Ecopetrol, hace tres meses, los daños y afectaciones, tanto a la comunidad como a la naturaleza, continúan sin ser atendidos por las autoridades. Las comunidades denunciaron que hay líderes ambientalistas amenazados y organizaciones defensoras de los derechos ambientales afirman** que la recuperación de los territorios tardará entre 7 a 10 años.**

Oscar Sampayo, integrante de la Alianza contra el Fracking, denunció que el líder ambientalista que ha sido víctima de hostigamientos y amenazas es Leonardo Suarez, coordinador del comité de concertación de la Vizcaína, en Santander. Los hechos se presentaron el pasado 26 de mayo**, cuando el líder fue abordado por un hombre que lo amenazó un un arma blanca, manifestándole que lo iba a matar**.

Suarez estaba denunciando, junto con el comité de concertación de la Vizcaína el accionar ilegal de grupos al margen de la ley en el territorio que han llegado después del derrame de petróleo a intentar establecer un control sobre la población. El comité es el escenario de encuentro entre las comunidades y Ecopetrol en la región del Santander y según Sampayo, **“la inestabilidad social que produjo esta emergencia, ya venía desde antes, pero esta situación es preocupante”**.

### **Los daños al ambiente tardarán entre 7 a 10 años en repararse** 

Frente a los daños ambientales Sampayo afirmó que continúan en la misma incertidumbre desde que se conoció el desastre natural, debido a que Ecopetrol solo ha dado a conocer informes de prensa y en ellos no dice mucho. Además, los campesinos han denunciado que, sobre la Quebrada la Lizama, Caño Muerto y el Río Sogamoso, **aún existen manchas de crudo que siguen impactado las laderas por donde recorres estos afluentes**. Manchas que van a dar al río Magdalena.

Sobre la fauna y flora de los ecosistemas de la región, una de las especies más afectadas han sido los manatíes que habitan el río Sogamoso, debido a las aguas contaminadas de hidrocarburo, de igual forma el jaguar en vía de extinción también se encuentra a la deriva, esto debido a que según Sampayo, no se hizo un estudio sobre el corredor de este animal, **ni un inventario de cuantas especies había para saber si se afectó o no su ecosistema**. (Le puede interersar:["Animales, los más afectados con el derrame de petróleo en Barrancabermeja"](https://archivo.contagioradio.com/animales-los-mas-afectados-con-el-derrame-de-petroleo-en-barrancabermeja/))

“Acá hay un folclorismo total en cuanto a la protección de la fauna y la flora” afirmó Sampayo y agregó que la restauración de los ecosistemas y del ambiente puede tardar entre **7 a 10 años, debido a la contaminación y a la falta de precaución por parte de Ecopetro**l.

<iframe id="audio_26306755" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26306755_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
