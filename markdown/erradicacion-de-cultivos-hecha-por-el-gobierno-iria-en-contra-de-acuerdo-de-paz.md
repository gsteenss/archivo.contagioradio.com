Title: Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz
Date: 2016-11-28 16:14
Category: Ambiente, Nacional
Tags: Cultivos de uso ilícito, Erradicación Forzada
Slug: erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/ejercito-nacional2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ejercito Nacional] 

###### [28 Nov 2016] 

En un comunicado de prensa el Comando de los Puntos de Preagrupamiento Temporal de las FARC-EP de los municipios de Uribe – Meta, sentó su posición frente a la erradicación de cultivos ilícitos que se vienen presentando en algunas zonas del país, que **no hacen parte de plan de restitución de tierras establecido en los Acuerdos de Paz de La Habana y que según ellos “constituye un engaño a las comunidades”.**

En el documento señalan que en departamentos como el Meta, funcionarios del gobiernos han estados socializando una propuesta de erradicación de cultivos de hoja de coca que no es el correspondiente a lo acordado en el punto 4 de solución al problema de drogas ilícitas.

Esta propuesta ofrece subsidios a los campesinos de **\$750.000 pesos por finca cocalera** y se entregaría cada dos meses y una ayuda para financiar huertas caseras. Oferta que para las FARC-EP  **“no satisface las urgentes necesidades de las familias cocaleras y  está encaminada en negar lo ya acordado en materia de drogas ilícitas,”**.

Razón por la cual, las FARC-EP indica que las comunidades “**deben rechazar la oferta y unirse a exigir la implementación** del Programa Nacional de Sustitución de Cultivos de Uso Ilícito”.Además afirmaron que si alguna comunidad se encuentra interesada en conocer más a fondo la implementación de este punto del Acuerdo de paz, la guerrilla se compromete a realizar pedagogía sobre este punto.

Sin embargo el Meta no es el único departamento en el campesinos vienen denunciado esta labor de erradicación de cultivos, hecha por las Fuerzas Militares, por fuera de lo acordado en la Habana. En departamentos como el Putumayo y Norte de Santander campesinos han señalado que se están realizando erradicaciones y que a su vez se amedranta a la personas que no esté de acuerdo con esta situación**. **Le puede interesar:["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

Finalmente en el texto la guerrilla de las FARC añade que la implementación del Programa Nacional de Sustitución de Cultivos  de uso Ilícito (PENIS) debe impulsar planes de desarrollo alternativo para la sustitución de cultivos,  debe ser construido conjuntamente con las comunidades y comprender la formalización de la propiedad, adecuación de tierras, riego, asistencia técnica, créditos, comercialización  e infraestructura industrial para agregarle valor a la producción y, entre otras inversiones públicas,  salud, educación, vivienda y servicios básicos. Le puede interesar:["Erradicaciones forzadas son otra forma de matar al campesinado"](https://archivo.contagioradio.com/erradicaciones-forzadasotra-manera-de-matar-al-campesinado/)

###### Reciba toda la información de Contagio Radio en [[su correo]
