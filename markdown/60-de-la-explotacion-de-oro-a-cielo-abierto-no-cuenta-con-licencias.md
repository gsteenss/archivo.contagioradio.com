Title: 60% de la explotación de oro a cielo abierto no cuenta con licencias
Date: 2016-06-29 15:42
Category: Ambiente, Nacional
Tags: cultivos ilícitos, minería a cielo abierto, minería de aluvión
Slug: 60-de-la-explotacion-de-oro-a-cielo-abierto-no-cuenta-con-licencias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Minería-de-aluvión.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Bagre ] 

###### [29 Junio 2016] 

De acuerdo con la más reciente investigación de los Ministerios de Justicia y del Derecho, Minas y Energía, y Medio Ambiente junto con la Oficina de las Naciones Unidas contra la Droga y el Delito, **el 60% de la explotación de oro a cielo abierto se realiza sin autorización, el 79% se concentra en Antioquia y Chocó**, y en Caquetá, Nariño y Putumayo más del 80 % de las zonas en las que se practica esta minería, se localizan en territorios afectados por cultivos ilícitos, mientras que en Bolívar ocurre en el 45 % y en Chocó, Valle y Antioquia entre el 30 y el 35%.

La investigación alerta sobre el impacto ambiental y social de este tipo de minería. **El 46% de la actividad se concentra en Consejos Comunitarios de Comunidades Negras**, otro tanto en resguardos indígenas, especialmente en los Emberá- Katío de Chocó y Antioquía. Así mismo, cinco Parques Nacionales Naturales han resultado afectados junto a nueve de sus zonas cercanas. Una de las principales conclusiones es que la explotación de oro a cielo abierto es uno de los principales motores de pérdida de cobertura vegetal de alto valor ambiental. **Para 2014 se perdieron 24.450 hectáreas, 77% de ellas en Chocó**.

<iframe id="doc_43268" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/317074134/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-GFGj5yM6qxxZSRjHseSQ&amp;show_recommendations=true&amp;show_upsell=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.75"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
