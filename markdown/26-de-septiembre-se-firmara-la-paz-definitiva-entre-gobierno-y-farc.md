Title: 26 de Septiembre se firmará la paz definitiva entre gobierno y FARC
Date: 2016-09-02 18:31
Category: Nacional, Paz
Tags: acuerdo final, FARC, Gobierno, paz
Slug: 26-de-septiembre-se-firmara-la-paz-definitiva-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paz-e1472858979891.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [2 Sep 2016]

El presidente Santos anunció que el próximo 26 de Septiembre se realizará la firma protocolaria del **acuerdo final de paz entre el gobierno y las FARC, también anunció que se realizará en la ciudad de Cartagena.** El anuncio lo hizo en medio de su discurso ante empresarios vinculados a Confecamaras *“Tal vez es el anuncio más importante que he hecho en mi vida, la paz se va a firmar el **26 de septiembre** en Cartagena”* afirmó.

Esta firma define el inicio de la movilización de los integrantes de las FARC hacia las zonas veredales transitorias de acuerdo al cronograma definido y publicado en el comunicado conjunto 76 del **23 de Junio en el que se da cuenta del dispositivo de seguridad** y de verificación para la dejación de armas por parte de esa guerrilla…

*A partir del día D+1 la Fuerza Pública reorganizará el dispositivo de las tropas para facilitar el desplazamiento de las estructuras de las FARC-EP a dichas Zonas y para el cumplimiento del Acuerdo sobre el Cese al fuego y Dejación de las Armas.*

*Por su parte a partir del día D+5, las distintas misiones, comisiones y Unidades Tácticas de Combate (UTC) de los frentes de las FARC-EP se desplazarán hacia dichas Zonas previamente acordadas, siguiendo las rutas de desplazamiento establecidas de común acuerdo entre el Gobierno Nacional y las FARC-EP.*

Adicionalmente **a partir del 26 de Septiembre se contarán las 3 fases en que se realizará el proceso de dejación de armas** por parte de las FARC, que será monitoreada por la ONU y la CELAC.

“*Con la Firma del Acuerdo Final inicia el proceso de Dejación de las Armas de las FARC-EP, que incluirá el transporte del armamento a las zonas, la destrucción del armamento inestable, y la recolección y almacenamiento en contenedores del armamento individual de manera secuencial y en tres fases así: 1 Fase: D+90, el 30%; 2 Fase: D+120, el 30%; y 3 Fase: D+150, 40% restante, según la hoja de ruta (cronograma de eventos) acordada por el Gobierno Nacional y las FARC-EP que guía el proceso del Fin del Conflicto luego de la firma del Acuerdo Final.”*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
