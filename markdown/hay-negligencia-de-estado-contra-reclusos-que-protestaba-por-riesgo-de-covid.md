Title: Negligencia de Estado contra reclusos que protestaba por riesgo de COVID-19
Date: 2020-03-22 15:33
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel, Covid-19, estado
Slug: hay-negligencia-de-estado-contra-reclusos-que-protestaba-por-riesgo-de-covid
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/modelo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos rechazaron las afirmaciones de la Ministra de Justicia Margarita Cabello, en donde aseguraba que los amotinamientos de la Cárcel La Modelo fueron producto de un intento masivo de fuga. Para las organizaciones **"hay negligencia de Estado contra reclusos** que protestaba por riesgo de COVID-19, provocando la muerte de estas 23 personas y más de 83 heridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización **Lazos de Dignidad** manifiesta que el accionar de la Fuerza Pública, durante la noche del 21 de marzo, podría constituirse como un crimen de Estado y una masacre en contra de la población reclusa. Asimismo aseguraron que las afirmaciones de Cabello "minimizan y justifican" una situación que es producto de "una sistemática violación a derechos humanos en las prisiones".

<!-- /wp:paragraph -->

<!-- wp:heading -->

Hay negligencia del Estado contra reclusos
------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con la Comisión de Justicia y Paz, la negligencia del Estado frente a la población carcelaria ha sido histórica. Sin embargo frente a la situación presentada la noche del sábado 21 de marzo se deben formular tres cuestionamientos hacia el Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Primero **la falta de actuación para desarrollar un plan de contingencia ante la llegada del Covid-19** al territorio nacional, en contraposición a la vulnerabilidad de las personas privadas de la libertad. (Le puede interesar: ["Ninguna Cárcel en Colombia está lista para afrontar Covid 19"](https://archivo.contagioradio.com/ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Al ser ampliamente conocido que el sistema carcelario no posee las condiciones para garantizar los derechos de las personas por la negación constante de derechos, y al no adoptar medidas concretas para proteger del contagio, **es predecible que las personas protestaran con la finalidad de exigir medidas para salvaguardar sus vidas,** pues la propagación es inminente" señalan.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Estado debe reconocer su responsabilidad
-------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Segundo, afirman que tanto las muertes de las 23 personas, como los más de 83 heridos pudieron haberse evitado si se hubiesen acatado los llamados del movimiento carcelario, realizados desde el pasado 16 de marzo, y estableciendo mesas de diálogo que permitieran no solo calmar a la población reclusa, sino comentar los protocolos que se estuviesen tomando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, aseguran que **las afirmaciones de Cabello desconocen el estado de abandono e inhumanidad de las cárceles**, que se refleja en la sentencia T 153 de 1998, con la que se declara el estado de cosas inconstitucionales en el sistema penitenciario y pretendiendo ocultar la evidente **responsabilidad del Estado en Esta situación.** Le puede interesar: ["Avance del Coronavirus Colombia"](https://coronaviruscolombia.gov.co/Covid19/index.html))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, también iría en contravia del reciente pronunciamiento de Naciones Unidas, que señalaba que en el marco del control de la pandemia no se podía ni violar ni restringir derechos humanos, acciones que estarían dándose en los inmediato con la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Estado sigue sin dar respuestas
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tercero la Comisión de Justicia y Paz manifiesta que no hay un plan de prevención concreto que tengan en cuenta el hacinamiento y violación a derechos humanos, frente a la población que sigue reclusa, y así evitar una situación similar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, "a pesar de que se dio la declaración del estado de emergencia, el 17 de marzo, y de la cuarentena por 19 días, **no hay ninguna acción por parte del INPEC y el Ministerio de Justicia en adoptar medidas sanitarias** de prevenciones que impida el contagio en esos centro de reclusión."

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado afirman que es urgente que el INPEC de cuenta de una lista oficial con nombre y cédula de las personas asesinadas y heridas, para dar el parte a las familias y que se establezca una mesa de diálogo con los internos, debido a que ellos siguen denunciando que los amotinamientos continúan y que la cifra de personas asesinadas y heridas, es mucho más alta. (Le puede interesar: ["Amotinamiento dejó 23 muertos en cárcel La Modelo")](https://archivo.contagioradio.com/amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Comité de Solidaridad con los Presos Políticos, que hace presencia en el lugar de los hechos afirma que fuera de la cárcel La Modelo también se presenta una difícil situación con las familias de los prisioneros que piden información urgente sobre el estado de los reclusos y aseveran que la situación "aún no ha sido controlada" al interior del centro penitenciario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, también se suman al rechazo de estos hechos y exigen acciones inmediatas como la declaratoria de emergencia, que ha sido respaldada por la Procuraduría y la Defensoría del Pueblo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
