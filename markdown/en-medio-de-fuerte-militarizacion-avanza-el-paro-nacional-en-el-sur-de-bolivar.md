Title: En medio de fuerte militarización avanza el paro nacional en el Sur de Bolívar
Date: 2016-05-30 13:03
Category: Movilización, Paro Nacional
Tags: paro nacional sur de bolívar
Slug: en-medio-de-fuerte-militarizacion-avanza-el-paro-nacional-en-el-sur-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Marcha-Sur-de-Bolívar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cristian y su Blog ] 

###### [30 Mayo 2016 ]

Desde el pasado jueves en medio de una fuerte militarización, tanto de las zonas urbanas como rurales del Magdalena Medio, por lo menos 20 mil pobladores de esta región, así como del Sur de Bolívar y Norte de Santander, se movilizan para concentrarse en San Martín y exigir al gobierno nacional cumplir con la [[agenda de la Cumbre Agraria de 2014](https://archivo.contagioradio.com/vuelve-el-paro-nacional-el-30-de-mayo-tras-dos-anos-de-incumplimientos-del-gobierno/)], y que brinde soluciones concretas a las problemáticas mineroenergéticas de larga data en la región.

De acuerdo con Teófilo Acuña, de la 'Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar' CISBCSC, desde el viernes están llegando las comunidades al municipio de San Martín, pese a que las fuerzas militares han querido obstaculizar la movilización, militarizando toda la región del Magdalena Medio, incluidas la Hacienda Bella Cruz, en la que también está el ESMAD.

"Nos preocupa la situación porque evidencia que la intención del Gobierno es no permitir que las comunidades exijan el cumplimiento de los pactos que se han hecho", permitiendo incluso que se continúen otorgando las concesiones de explotación minera, desconociendo sus impactos ambientales y sociales, asegura el líder.

"No se ha cumplido con la restitución en las Hacienda Bella Cruz, Las Pavas, y Terraplén", los campesinos las han querido recuperar para lograr vivir dignamente, agrega Acuña, e insiste en que las comunidades se movilizan para denunciar los daños que ha causado la construcción de la ruta del sol, entre ellos, la canalización de fuentes hídricas, y la reactivación del paramilitarismo en esta región del país.

<iframe src="http://co.ivoox.com/es/player_ej_11716083_2_1.html?data=kpakk5uUfJShhpywj5WcaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbXZ0MvWztSPhcTphqigh6eVpYyhjKjcz87XrYa3lIqvldOPrc%2FoxtfZ0cjZp8qZpJiSpJjSb7Tp05Cv0dHNusLmjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
