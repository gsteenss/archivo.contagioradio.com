Title: Islamismo, fundamentalismo y terrorismo
Date: 2015-05-28 06:00
Category: Cesar, Opinion
Tags: Al qaeda, Boko Haram, colombia, El Islam, Estado Islámico, Siria
Slug: islamismo-fundamentalismo-y-terrorismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/irak-Estado-Islamico-de-Irak-y-el-Levante.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reuters 

#### [**Por [Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)**] 

###### [Mayo 28 de 2015] 

Las fotos y los vídeos que hemos visto en los últimos días en los medios de comunicación acerca del terrorismo, sobrepasan cualquier dimensión que pudiéramos tener sobre tal fenómeno. Aquí en Europa, a diferencia de nuestro continente, se le da una amplia cobertura interpretativa y mediática al conjunto de acciones del Estado Islámico y su mal llamado “Califato”.  Lo que se ha hecho en Palmira, Siria,  es otro botón de muestra (¡como si se necesitaran más!)  del carácter terrorista  de una organización que asesina y reprime a poblaciones enteras en nombre del Islam, y que destruye el patrimonio cultural de antiguas civilizaciones. Guardadas las proporciones,  es como si una organización  latinoamericana, cualquiera y armada o no, destruyera las pirámides aztecas, el Parque San Agustín y otros sitios arqueológicos de Colombia, y el Cuzco en el Perú.  Pero esto hay que explicarlo a los niños, jóvenes, adultos e incluso a los sectores ilustrados, que hoy evidencian altos niveles de incomprensión y desconocimiento. Por ejemplo, ciertos académicos e intelectuales colombianos se refieren al Estado Islámico como ejemplo de una  izquierda “anti imperialista” en proceso de “reconversion cultural” .

El Islam nada tiene que ver con el terrorismo, ni sus fieles son por antonomasia agentes, cómplices o autores materiales de la barbarie terrorista. Todo lo contrario. Quienes conocen de religiones han explicado hasta la saciedad que el Islam no proyecta la violencia;  y los que la practican son ajenos a los extremismos. Otra cosa es la interpretación sectaria y político-ideológica que del Islam hacen los gobiernos respectivos, lo que los lleva a impedir la libertad religiosa y a reprimir a las poblaciones ; como sea, a este respecto no hay diferencia entre gobiernos islámicos y gobiernos católicos. Y algo bien distinto también es la práctica fundamentalista de organizaciones como Boko Haram y Al-Qaeda.

El fundamentalismo es enemigo de la democracia; excluye por principio a quienes se constituyen en el “otro” ; margina, elimina y controla a los pueblos. Es lo que está ocurriendo hoy en todas las zonas y regiones en las cuales se impone, bien por debilidad de los gobiernos (Siria), ya por el odio que engendra la represión gubernamental en las regiones (contra kurdos en Irak y Siria) ; también por el derrumbe del Estado (Libia), o por lo que ha significado las  intervenciones militares europea y norteamericana (Irak). Las acciones de los grupos fundamentalistas se disocian de la realidad y la suplantan; como no logran ganar el apoyo de manera política lo hacen por la vía armada y utilizando el miedo en su favor. En breve, pretenden tomar atajos en una lucha contra el mal (contra el impío dirían los católicos).

Del fundamentalismo al terrorismo no hay sino un paso. Uno solo y hacia el abismo. Habida cuenta del contexto socio-político-religioso en África, Asia y el Medio Oriente (petróleo, drogas, tráfico de personas, esclavitud, armas y millones de dólares), la crisis de los Estados, la precariedad del orden geo político (Estados que aspiran a ser potencias regionales), el Estado Islámico y su Califato, Boko Haram y Al-Qaeda  ya cruzaron los limites, incluido los de la lógica racional (claro,  si alguna vez la ha habido). No hay vuelta atrás.

Con ello, han colocado a los pueblos y naciones en el centro de la guerra que estamos soportando desde ahora. La responsabilidad esencial radica en las potencias imperialistas y en los gobiernos fundamentalistas. Pero el paso al abismo terrorista lo ha dado el Estado Islámico. Las decapitaciones, la represión a las mujeres, la opresión cultural y religiosa, el oscurantismo, nada de esto hace parte de una “reconversión cultural”. Esta ficción sólo existe en la visión campista de quienes no pueden comprender la realidad.
