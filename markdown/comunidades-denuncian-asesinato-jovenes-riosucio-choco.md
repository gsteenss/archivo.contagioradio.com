Title: Comunidades denuncian asesinato de jóvenes en Riosucio, Chocó
Date: 2019-01-16 09:20
Author: AdminContagio
Category: DDHH, Nacional
Tags: asesinato, Chocó, Derechos Humanos, Riosucio
Slug: comunidades-denuncian-asesinato-jovenes-riosucio-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/jovenes-asesinados-choco-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Suministrada 

###### 16 ene 2019 

Según la organización Tierra y Vida de Riosucio - Choco, el pasado 13 de Enero, dos desconocidos entraron a la casa de Pedro Moreno, de 20 años, en el barrio de El Centro y le dispararon cinco veces, ocasionandole la muerte. Esa misma noche, en un caso separado, Yuber Córdoba, de 20 años, fue ultimado con arma de fuego mientras se encontraba por el puerto del barrio Escolar.

Las comunidades de Riosucio, en Chocó, junto a las organizaciones sociales, pidieron al Estado colombiano mayor protección dadas las constantes acciones de grupos armados ilegales operando en la región del Bajo Atrato, esto tras el asesinato de los jóvenes y las constantes amenazas por la presencia permanente de estructuras armadas ilegales o paramilitares.

Yeison Farid Mosquera, director de Tierra y Vida, afirmó que los dos jóvenes eran universitarios y que habían reportado en su contra amenazas de las Autodefensas Gaitanistas de Colombia (AGC), a quienes la organización social señaló como responsable de estos crímenes.

Mosquera manifestó que los jóvenes de Riosucio viven bajo la constante amenaza de estos grupos paramilitares que han tomado control del territorio y que han asesinado a por lo menos seis jóvenes en Riosucio. A pesar de que las comunidades ha denunciado estas crímenes, Mosquera afirma que la Fuerza Pública no ha tomado las medidas suficientes para garantizar su seguridad.

Por esta razón, Tierra y Vida también señaló a la Policía y el Ejército Nacional como responsables de estos crímenes, "por ser tan negligente frente a las constante amenazas de nuestros lideres y personas por parte de grupos armados que delinquen en nuestro territorio."

### Líderes piden "reunión urgente" con el Gobierno Nacional 

Mosquera afirmó que en el municipio se han registrado violaciones de los derechos humanos como amenazas de las mal llamada "limpieza social" de jóvenes, desplazamiento forzado y el asesinato de líderes sociales. Según la Defensoría del Pueblo, Riosucio es uno de los municipios en el país donde más se han registrado asesinatos de líderes sociales en el 2018, con nueve casos.

Ante la inseguridad, los líderes sociales han pedido una reunión con el Ministerio de Defensa y el presidente Iván Duque para reportar y formular mecanismos para combatir la situación "precaria y fuera de control" que viven los riosuceños a mano de grupos paramilitares.
