Title: Gobierno propone ampliar requisitos para realización de consultas populares
Date: 2017-08-23 12:09
Category: Ambiente, Nacional
Tags: consultas populares, Gobierno Nacional, Proyecto de Ley
Slug: gobierno_amplia_propone_requisitos_consultas_populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: revistaccesos] 

###### [23 Ago 2017] 

Además de los requisitos existentes para que se puedan llevar a cabo consultas populares en Colombia, el gobierno ha presentado un borrador de un proyecto de Ley por medio del cual buscaría que, antes de la votación para que pueda ser prohibido un proyecto minero-energético, **sea necesario la activación de un cabildo abierto.**

Se trata de una de las formas que ha encontrado el gobierno para poner freno al boom de consultas populares en todo el país. Se han desarrollado 7, en las que más del 96% de los votantes han decidido impedir la puesta en marcha o la continuación de proyectos petroleros y mineros, y además aún hay **44 iniciativas de consultas mediante las cuales los pobladores buscan defender sus territorios**.

El gobierno ha propuesto los cabildos abiertos como un complemento obligatorio en el que se expongan los distintos puntos de vista para que se decida sobre el futuro de la vocación de los municipios. Una propuesta que podría demorar el procedimiento para que se logre concretar una consulta popular, que además debe someterse antes a los análisis por parte de la alcaldía de turno, los concejos municipales, y un tribunal que debe dar el concepto de constitucionalidad a la pregunta propuesta.

La iniciativa del gobierno Santos ha sido presentada a la Comisión de Seguimiento, Impulso y Verificación del Acuerdo de Paz (Cesivi), integrada por representantes de las instituciones y las FARC. **Un proyecto de Ley estatutaria que debe estar acorde con lo pactado en La Habana y que por el momento las FARC no ve con buenos ojos.**

Según el gobierno, lo que se pretendería es que haya mayor información antes de salir a votar. Es así como se abriría la convocatoria a un cabildo amplio, donde habría argumentos a favor y en contra, a través de información que sea verificable, para luego acudir al mecanismo de participación popular.

[PROYECTO DE LEY PARTICIPACIÓN CIUDADANA Versión Final](https://www.scribd.com/document/357042701/PROYECTO-DE-LEY-PARTICIPACIO-N-CIUDADANA-Versio-n-Final#from_embed "View PROYECTO DE LEY PARTICIPACIÓN CIUDADANA Versión Final on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" src="https://www.scribd.com/embeds/357042701/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-5kU7OV4aCIDwV3MYwuRz&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_55970" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
