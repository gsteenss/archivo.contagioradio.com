Title: Reunión con Consejo Superior de la UPTC definirá continuidad del paro de sus estudiantes
Date: 2018-11-08 16:11
Author: AdminContagio
Category: Movilización, Nacional
Tags: Paro Estudiantil, Universidad pública
Slug: reunion-con-consejo-superior-de-la-uptc-definira-continuidad-del-paro-de-sus-estudiantes
Status: published

###### [Foto: Archivo] 

###### [7  Nov 2018] 

Luego de 28 días de movilización estudiantil en la **Universidad Pedagógica y Tecnológica de Colombia (UPTC),** directivas de la Institución emitieron un comunicado en el que invitaban a levantar el paro, y retomar la jornada académica este miércoles. No obstante, estudiantes del centro educativo manifestaron que no hay condiciones para continuar en clases, y pidieron que se realice la reunión en la que expondrían su pliego de peticiones ante la administración.

Según el estudiante Camilo Tavera, el Consejo Superior Universitario (CSU) determinó el pasado 6 de noviembre el fin de la anormalidad académica, y el inició de clases este miércoles; sin embargo, la movilización estudiantil ha señalado que el paro se mantendrá, porque sus peticiones aún no han sido atendidas por parte de las directivas de la Institución.

Tavera afirmó que pese al anuncio, los estudiantes seguirán su agenda de actividades que incluye un encuentro con el CSU este jueves, en el que expondrán su pliego de peticiones que abarca el aumento de la planta docente de la institución, la revisión del presupuesto de la Universidad y su distribución en todas las sedes de la misma, así como mejoras al bienestar estudiantil.

El estudiante expresó que en cada una de las sedes de **Duitama, Tunja, Sogamoso, Chiquinquirá e incluso en la extensión de Casanare un promedio de 200  a 300 estudiantes continuarán en el lugar** “en tanto la administración no dé resultados, los estudiantes no vamos a salir del campus universitario, no vamos a cesar la toma del campamento mientras no se dé una solución al pliego local y nacional de exigencias”.

### **Así atacan la protesta** 

Tavera también denunció que en el marco del campamento que se realiza en la sede de Tunja, en la noche del 6 de noviembre, efectivos de la policía intentaron ingresar a las instalaciones de la Institución escalando por las paredes de la misma. Ante estos hechos, pidieron garantías ante las directivas de la UPTC, como de la Fuerza Pública para que se respete la movilización pacífica que realizan.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F249348795737717&amp;width=500" width="500" height="642" frameborder="0" scrolling="no"></iframe>  
Sumándose a dichas denuncias, **los colectivos estudiantiles de la Universidad de Pamplona también manifestaron su rechazo a las amenazas de las que han sido víctimas** por parte de un grupo denominado como "anticapuchos" y responsabilizan a la administración de la universidad por cualquier ataque en su contra mientras protestan de forma pacífica.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F249349849070945&amp;width=500" width="500" height="593" frameborder="0" scrolling="no"></iframe><iframe id="audio_29939221" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29939221_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]

 
