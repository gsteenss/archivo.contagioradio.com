Title: Asesinan al líder Jhon Jairo Guzmán en Tarazá, Antioquia
Date: 2020-10-20 10:16
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Antioquia, bajo cauca antioqueño, Jhon Jairo Guzmán, Tarazá
Slug: asesinan-al-lider-jhon-jairo-guzman-en-taraza-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Jhon-Jairo-Guzman.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Jhon-Jairo-Guzman-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este fin de semana se reportó **el asesinato del líder social Jhon Jairo Guzmán en el corregimiento Barro Blanco, de Tarazá, Antioquia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades que hicieron el levantamiento del cuerpo señalaron que los restos del líder presentaban **«*varios impactos de arma de fuego y heridas con arma blanca*».** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jhon Jairo Guzmán se desempeñaba como **vicepresidente de la Junta de Acción Comunal** de la vereda El Tesorito de ese municipio ubicado en el Bajo Cauca antioqueño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el presente año se han reportado varios asesinatos de líderes en esa zona; como el ocurrido el pasado mes de junio en el que un grupo paramilitar asesinó al líder campesino **Edier Lopera** dejando su cuerpo expuesto durante cerca de una semana, sin permitir que sus allegados o las propias autoridades lo recogieran, para generar terror y zozobra en la comunidad. (Le puede interesar: [Paramilitares impiden el levantamiento del cuerpo de Edier Lopera tras 8 días de su asesinato](https://archivo.contagioradio.com/paramilitares-impiden-el-levantamiento-del-cuerpo-de-edier-lopera-tras-8-dias-de-su-asesinato/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el pasado mes de agosto fue asesinada **Sandra Banda Meneses**, de 48 años, quien presidía la Junta de Acción Comunal del barrio El Paraíso, del corregimiento La Caucana, del mismo municipio de Tarazá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En la zona operan grupos armados al margen de la ley como las autodenominadas Autodefensas Gaitanistas de Colombia -AGC-, el grupo armado Los Caparrapos y disidencias de las FARC-EP**, los cuales se disputan el oro y las rutas del narcotráfico, dejando a la población civil en el medio. Le puede interesar: [Control de AGC se consolida en cuarentena en Bajo Cauca Antioqueño](https://archivo.contagioradio.com/control-de-agc-se-consolida-en-cuarentena-en-bajo-cauca-antioqueno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a la fuerte presencia del Ejército y la Fuerza Pública en la zona, ya que **el Bajo Cauca es la zona más militarizada del departamento de Antioquia con por lo menos 5.000 hombres de la Fuerza de Tarea Conjunta Aquiles y 2.000 más de la operación Agamenón 2** —según voceros de [CCEEU](https://coeuropa.org.co/158/) (Nodo Antioquia) — los hechos victimizantes en contra de la población civil siguen ocurriendo. (Lea también: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según registros del Instituto de Estudios para el Desarrollo y la Paz –[Indepaz](http://www.indepaz.org.co/lideres/)-; en Antioquia han sido asesinados al menos 23 líderes sociales en lo que va del año 2020, lo que se constituye en casi el 10% de los asesinatos perpetrados en todo el territorio nacional, siendo la región del Bajo Cauca antioqueño una de las más afectadas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1317554817506680832","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1317554817506680832

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
