Title: Ya viene, ¿preparados?
Date: 2020-03-19 11:10
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, Coronavirus, Covid-19, pandemia, Pánico, pensamiento crítico
Slug: ya-viene-preparados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/corona.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#596168"} -->

###### **Foto tomada del Espectador** 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aspectos objetivos rodean toda la situación por la que atraviesa el mundo en estos instantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay un virus de alta propagación, que recibió un trato por los Chinos supuestamente “exagerado”, pero tiempo después, Italia, España, Francia y muchísimos países más, descubrieron que los chinos no estaban tan locos al tomar esas medidas que se veían extremas desde una Europa relajada, y que aún se ven con desconfianza desde una Colombia que ha demostrado que puede pasar de la ausencia total de análisis, al pánico extremo...

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### y ya sabemos que con pánico, ricos y pobres, estudiados y no estudiados, se comportan exactamente igual.   

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aspecto objetivo es que según me indicaba la mañana del 16 de marzo de 2020 un colombiano parte de un equipo de investigación radicado en la Universidad de Arkansas en los Estados Unidos y que se encuentra trabajando con datos compilados bajo el sistema **WHO, CC, NHC, DXY y el Geographic Information System**, es que el aislamiento sí funciona y que determinantemente son los adultos mayores quienes más sufren; del mismo modo aseveraba que es bastante serio lo que ocurre, y que probablemente luego de los 14 días de incubación, cuando el virus se muestre en todo su esplendor en un país como Colombia, los casos serán miles. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Pero, es un virus que nos matará a todos?

<!-- /wp:heading -->

<!-- wp:paragraph -->

le pregunté al experto, pero el indicó que no se trata de la letalidad en sí misma, sino del índice de propagación y de la capacidad con la que cuentan los sistemas de salud de los diferentes países para hacer frente a esta situación. Los chinos levantan hospitales en 10 días. En Colombia, no quiero especular… solo sé que se caen los puentes y obras que deberían durar construyéndose 3 años, se terminan en 10 años. (*94 con NQS*)  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aspecto objetivo es que en Colombia el expresidente Uribe endosó el sistema de salud a los privados y desde hace casi 20 años los fracasos de las EPS, los robos al dinero pagado con el esfuerzo de los colombianos terminaron invertidos en condominios en Miami, en zonas francas, en centros comerciales, restaurantes, en cierre de importantes clínicas y en general en una precarización del sistema de salud. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En este marco hay que hacer análisis crítico. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Colombia al no estar preparada para el aumento exponencial de infectados por el virus, pondrá en riesgo a mas gente de ese porcentaje que vivirá con más fuerza los efectos de la enfermedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Colombia posee índices muy altos de informalidad laboral. Eso significa que a muchas personas no les van a pagar los días de la cuarentena. Que muchos negocios sobreviven con el día a día, y ese coletazo económico que está golpeando a todos los países también golpeará con fuerza a Colombia. El sector turismo ya lo está viviendo de una manera muy grave.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es probable que esta crisis también sirva como pilotaje para redefinir la universidad en Colombia y la educación en general; es decir, puede que sea el impulso definitivo a la virtualización del sistema, ya que todo está enfocado hacia la educación para el trabajo, para “insertarse en el mercado laboral” y sería un momento perfecto para decir, menos profesores, menos universidades que desestabilicen con teorías y profundizaciones, a ciudadanos de los que solo se necesita su acoplamiento al mercado laboral. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo, el presidente Duque atravesaba un nuevo escándalo, pero esta vez algo demasiado turbio, una investigación por compra de votos, es decir, el robo de la presidencia de la república. Esta crisis sin duda ha sido un salvavidas imprevisto para el uribismo, porque finalmente la atención está posada sobre el virus. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Pensar una pandemia como resultado de un proyecto político no es algo descabellado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No son teorías de “la conspiración” ni mucho menos. No hace muchos años, el comunismo decayó como fuente de miedo social en occidente. Sabemos que para 2001, el terrorismo reemplazó a ese comunismo que se disolvió con la **URSS**. No obstante, hoy, el terrorismo ya no es fuente de miedo social, no solo con la derrota del Estado Islámico en Siria, sino por la transformación del miedo en comedia, como por ejemplo sucedió con los videos del “árabe” dejando maletas y saliendo a correr en distintas localidades norteamericanas.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ni el comunismo, ni el terrorismo son fuentes de miedo social hoy. Y los poderes hegemónicos requieren recordarnos por qué son necesarios. El virus global puede ser un nuevo elemento que bajan de los anaqueles y les sirve a muchos regímenes políticos, quienes sacarán provecho mediante sus guerras psicológicas. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Con el terrorismo vendieron seguridad. Con el virus venderán esperanza.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sí, así de etéreo es esto, pero a la vez de contundente. Incluso los intelectuales más críticos, tomarán la esperanza como el foco, al igual que lo hacen Trump o Duque cuando se dirigen a sus pueblos envueltos en la figura del padre que proveerá algo más importante que la seguridad…la esperanza. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, aún es prematuro para hacer sentencias definitivas. Nos aproximamos los columnistas, los estudiosos de la política y de la ciencia médica al momento en que podamos concatenar conclusiones. Esta crisis le sirvió a Trump por sus elecciones, le servirá a China con su tema en Hong Kong, le sirvió a Duque y su robo de elecciones, le servirá a Maduro porque el mundo ha dejado de presionarlo, le sirvió a Cuba y a su misión médica, en fin… es variopinto el panorama, pero de regreso a Colombia las cosas podrían pasar de claro a oscuro. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El problema de la sociedad colombiana es ha perdido gran parte de su capacidad crítica.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La noche del 22 de noviembre de 2019 incluso muchos docentes universitarios participaron en las turbas que buscaban a los vándalos que aparecían y desaparecían en medio de una ciudad bajo toque de queda…

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿se puede imaginar que se propague ese pánico que ya ha comenzado con respecto al virus? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya están peleando por cloro en los supermercados, están apedreando casas de personas infectadas, y así seguirá subiendo la temperatura de la histeria colectiva, de la que es muy difícil salvarse pues el miedo empuja a las personas a actuar impulsivamente, lo que resultará en que muchos colombianos actuarán sobre el instinto de conservación y demás tendencias básicas de nuestra humanidad. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No es un escenario distópico ni pesimista.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y lamento ser aguafiestas con los coros de la esperanza pero uno de los mecanismos que es utilizado como recurso de la acción psicológica para desvirtuar y no permitir que la sociedad logre una conciencia verdadera de sí misma, es el que cuando se afirma el amor propio, la esperanza y otros sentimientos afines, se compensan los temores y las frustraciones diversas del sujeto, haciendo que inconscientemente, este sujeto en medio de su pánico y al que le fue dada la esperanza por parte del gobernante, no logre ver en ese gobernante un agente perjudicial y por tanto queda a su merced cuando la crisis pase… si, es allí el tema, cuando esta crisis pase. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No hay recomendaciones desde un punto de vista crítico más allá de entender que todos los aspectos racionales que se mezclan a la hora de visualizar esta problemática, pueden quedar diluidos durante un momento de pánico extremo… ya en Colombia están algunas tiendas y supermercados sin mercancía, ya se están conociendo vía redes sociales, casos tan absurdos como reales; esperemos de corazón y razón que logremos hacer frente a estos dos virus letales: **el miedo y el covid-19.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver [otras columnas de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)  

<!-- /wp:paragraph -->
