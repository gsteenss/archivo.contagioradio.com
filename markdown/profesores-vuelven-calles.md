Title: Por deuda histórica profesores vuelven a las calles en febrero
Date: 2019-02-01 18:11
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, fecode, Gobierno, paro
Slug: profesores-vuelven-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/fecode-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FECODE] 

###### [1 Feb 2019] 

**Este 14 de febrero**, los profesores agrupados en la Federación Colombiana de Trabajadores de la Educación (FECODE) se tomarán las principales capitales del país, como preambulo a la presentación del pliego de peticiones al Gobierno. En esta ocasión, los educadores pedirán que se modifique el Plan Nacional de Desarrollo (PND), por considerarlo inconveniente para el servicio de educación público del país.

Según explica el Miguel Angel Pardo, integrante de la junta directiva de FECODE, **el Gobierno ha decidido en su PND completar los recursos que no logró recaudar vía Ley de Financiación** recortando el gasto social. Dicha reducción incluiría la disminución al presupuesto destinado para que los municipios atiendan la educación básica, así como el sistema de salud de los maestros.

Esta decisión sería abiertamente contraria a los pactos alcanzados el semestre anterior, en que el gobierno Duque se comprometió a otorgar recursos a los municipios para atender esta necesidad; **de forma tal que se pagara la deuda histórica calculada por el gremio profesoral en 73 billones de pesos**, producto de reformas constitucionales que redujeron la participación nacional en los presupuestos locales.

Adicionalmente, los maestros reclamarán por la vida de los líderes sociales, puesto que como lo relata Pardo, el derecho a la vida es el pilar para acceder al derecho por la educación; y "desde 2016 han sido asesinados más de 300 líderes, **al punto que tenemos una taza de compañeros asesinados cada 30 horas"**. (Le puede interesar: ["FECODE está dispuesto a dar espacio a estudiantes en reunión con el Gobierno"](https://archivo.contagioradio.com/fecode-reunion-gobierno/))

### **El Gobierno Duque no cumple pactos** 

El 14 de febrero los maestros radicarán el pliego de peticiones, que incluirá la propuesta de una reforma constitucional que garantice recursos para la educación durante los próximos 10 años; y exigirán el cumplimiento de los compromisos pactados el año pasado "porque el Gobierno se ufana de cumplir los compromisos con otros países cuando se trata de Tratados de Libre Comercio, pero no es igual con temas sociales o de la agenda de paz". (Le puede interesar: ["Desconociendo protocolo, Gobierno cierra puertas a un futuro proceso de paz"](https://archivo.contagioradio.com/protocolo-procesos-de-paz/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
