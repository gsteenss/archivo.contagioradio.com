Title: Comunidades del Río Guayabero denuncian agresiones de la Fuerza Pública en operativos de erradicación forzada
Date: 2020-11-29 10:33
Author: PracticasCR
Category: Actualidad
Tags: Erradicación Forzada, PNIS, Río Guayabero
Slug: comunidades-campesinas-del-rio-guayabero-denuncian-agresiones-de-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Comunidades-del-Rio-Guayabero-denuncian-agresiones-en-operativos-de-erradicacion-forzada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Representantes de las comunidades campesinas de la región del Río Guayabero en el departamento del Meta, se trasladaron a Bogotá para hacer un llamado a las entidades estatales de atender las problemáticas que aquejan a ese territorio, las cuales tienen según la propia voz de las comunidades, sumida a la región en “*una crisis humanitaria y una crisis de uso y tenencia de la tierra*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades denuncian el abandono histórico de ese territorio por parte del Estado colombiano, la falta de acceso a servicios básicos como salud, educación, vías,  entre otros, lo cual, tiene a la región y a las comunidades sumidas en una extrema precariedad, razón por la que se han visto obligadas a desarrollar el cultivo de hoja de coca para alcanzar su mínima subsistencia. (Le puede interesar: [Espacio Humanitario Campesino del Guayabero, una alternativa para defender la vida](https://archivo.contagioradio.com/espacio-humanitario-campesino-guayabero-la-respuesta-agresiones-fuerza-publica-meta/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, los campesino han expresado su disposición para sustituir voluntariamente estos cultivos a la espera que el Estado les brinde medidas compensatorias para lograr su subsistencia económica, es decir que desarrolle el Programa Nacional de Sustitución de Cultivos de uso Ilícito -[PNIS](https://www.etnoterritorios.org/CentroDocumentacion.shtml?apc=x-xx-1-&x=1261#:~:text=Soluci%C3%B3n%20al%20problema%20de%20Drogas,de%20pobreza%2C%20la%20falta%20de)-; compromiso que, según los campesinos no ha asumido cabalmente y en contraste se ha enfocado en la vía militar a través de la erradicación forzada, la cual ha traído múltiples vulneraciones de derechos humanos y agresiones en contra de las comunidades por parte de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ejército estigmatiza comunidades del Guayabero

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los campesinos señalaron como responsables de estas agresiones a la **Fuerza de Tarea Conjunta OMEGA del Ejército Nacional,** **la cual opera en ese territorio a través del Plan Artemisa;** e incluso denunciaron el trato criminal que les dan las Fuerzas Militares, frente a esto uno de los voceros señaló que el General Raúl Hernando Flórez los relaciona constantemente con la  guerrilla por el simple hecho de alzar su voz en contra de la erradicación forzada. (Le puede interesar: [Operativo de erradicación deja a un menor herido en Zona de Reserva Campesina Perla Amazónica](https://archivo.contagioradio.com/operativo-de-erradicacion-deja-a-un-menor-herido-en-zona-de-reserva-campesina-perla-amazonica/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “La Fuerza Pública nos criminaliza e incluso nos han disparado hiriendo a al menos seis de nuestros compañeros campesinos”
>
> <cite>Fernando Montes Osorio corresponsal de Voces del Guayabero</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, como balance de la gira de incidencia que realizaron los delegados de las comunidades del Río Guayabero en Bogotá, señalaron que ni el Ministerio de Defensa, ni la dirección del Ejército habían accedido a conversar con ellos para exponer las vulneraciones de las que son víctimas. Asimismo, señalaron los compromisos que adquirieron los entes de control como la Procuraduría y la Defensoría del Pueblo en el acompañamiento a las comunidades especialmente en los operativos de erradicación. (Le puede interesar: [Fuerza Pública entró disparando, hurtando y persiguiendo a la gente: COCOCAUCA](https://archivo.contagioradio.com/fuerza-publica-entro-disparando-hurtando-y-persiguiendo-a-la-gente-cococauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, los campesinos señalaron que el acompañamiento que han tenido hasta el momento por parte de estas entidades ha sido bastante limitado y que esperan un mayor grado de compromiso tanto por parte de la Procuraduría en su labor disciplinaria para sancionar los excesos y abusos de la Fuerza Pública, como de la Defensoría del Pueblo la cual no ha mostrado disposición para garantizar los derechos de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “La Defensoría Regional del Meta ha estado totalmente ausente, incluso justificando la acción de las Fuerzas Militares, por eso hemos pedido también a la Procuraduría que adelante investigaciones en contra de esta dependencia de la Defensoría del Pueblo”
>
> <cite>John Edilson Castañeda, vocero de las comunidades campesinas del Río Guayabero, Meta</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
