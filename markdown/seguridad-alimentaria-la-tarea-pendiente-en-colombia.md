Title: Seguridad Alimentaria, la tarea pendiente en Colombia 
Date: 2016-07-25 08:22
Category: Ambiente y Sociedad, Opinion
Tags: seguridad alimentaria
Slug: seguridad-alimentaria-la-tarea-pendiente-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/boyaca13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Gabriel Galindo 

#### [****\*Louis **Epstein - Ambiente y Sociedad**] 

###### 25 Jul 2016 

Según el Programa de Alimentación Mundial, el 43% de los colombianos no tienen seguridad alimentaria. En otras palabras, casi la mitad de la población “carece de acceso diario a suficiente comida nutritiva y asequible”. En un país de más de 47 millones de personas, eso significa que casi 20 millones tiene que sufrir por hambre o malnutrición. Para un Estado cuya constitución dice, en el artículo 44, que “la alimentación equilibrada” hace parte de los derechos fundamentales de los niños, y que ha ratificado el Convenio Internacional Sobre Derechos Económicos, Sociales, y Culturales –el cual, en su artículo 11, garantiza el derecho universal a alimentación adecuada–, este alto porcentaje de deficiencia alimentaria constituye un claro fracaso.

El gobierno colombiano no ha logrado cumplir con sus obligaciones legales. De hecho, la Corte Constitucional de Colombia admitió la inconstitucionalidad del estado de los programas de alimentación del gobierno en el 2004, porque no proveyeron un nivel básico de alimentación a las personas desplazadas (en Acción de tutela instaurada por Abel Antonio Jaramillo y otros contra la Red de Solidaridad Social y otros\*) y ordenó hacer disponibles fondos adicionales para programas de alimentación.

Teniendo en cuenta la seriedad de esta situación, uno pensaría que la administración Santos usaría su liderazgo nacional para ayudar a alimentar a todos quienes lo necesitan. Sin embargo, debido a falta de acción a nivel nacional, han sido alcaldes locales y gobiernos regionales los responsables de brindar acceso a alimentos a los residentes de sus localidades y regiones. Predeciblemente, esta combinación de políticas de seguridad alimentaria sin dirección general ha estado plagada de problemas. En Bogotá, por ejemplo, cada nuevo alcalde ha cambiado las prioridades de la ciudad en relación de la política alimentaria. Esto ha llevado a que los programas más importantes, como el *Plan Maestro de Abastecimiento y Seguridad Alimentaria de Bogotá, *que nació durante la administración del exalcalde Gustavo Petro, sean completamente diferentes de las políticas del alcalde actual, Enrique Peñalosa.

Tantos cambios en las prioridades y en los fondos destinados para programas de seguridad alimentaria han causado confusión y desigual nivel de desarrollo en diferentes barrios y regiones. En la localidad de Ciudad Bolívar, una parte integral del plan de seguridad alimentaria fue la construcción de una plataforma logística llamada Lucero Tesoro donde se pudiera organizar, procesar y distribuir alimentos frescos. Pero los \$17.211 millones que fueron invertidos en esta nueva obra de infraestructura, que necesitaba el barrio, terminaron siendo desperdiciados por la mala administración y la falta de participación de la comunidad.

Oficiales del gobierno de diferentes alcaldes gradualmente redujeron los cuatro centros planeados, como Lucero Tesoro, a solamente uno. Tampoco apoyaron a Afrijosum, una asociación de productores de fríjol a la que el gobierno encargó la gestión de Lucero Tesoro, al no darle suficientes fondos o líneas de crédito para pagar a los campesinos a los que les compraban sus productos a través de este nuevo sistema creado por el gobierno. Como resultado, muchos campesinos de la región, que habían confiado en que Afrijosum iba a pagar sus deudas, perdieron todo cuando la organización se quedó sin dinero.  La falta de gestión centralizada y apoyo del gobierno hizo que una iniciativa de alimentación con mucha promesa y con mucho esfuerzo de parte de una organización de productores, que habría reducido los precios de la comida en un barrio muy necesitado, fallara completamente.

En vez de programas del gobierno aislados y desconectados de la comunidad, como Lucero Tesoro, las iniciativas de seguridad alimentaria deberían seguir el modelo de organizaciones campesinas, como APAVE, Asociación de Productores Agropecuarios de Vergara. Este grupo de 900 pequeños productores de Cundinamarca y otras regiones cercanas ha logrado dar una voz a los campesinos en cuestiones de desarrollo económico con gobiernos regionales y municipales. Esto resultó en la creación de mercados donde comida sana y fresca se vendía a precios justos para los productores y consumidores: “Mesas Provinciales”, donde se resuelven cuestiones agropecuarias de una manera democrática. APAVE, a pesar de las amenazas violentas y el desplazamiento por conflictos rurales, es un buen ejemplo de lo que puede lograrse cuando los diferentes niveles del gobierno apoyan a los proyectos creados por los campesinos mismos. En el futuro, organizaciones como APAVE pueden ser utilizadas para conectar campesinos a programas del gobierno de manera que ellos puedan participar, pero en sus propios términos.

Una nueva serie de iniciativas del gobierno, apoyadas por la comunidad y con el aprendizaje de los éxitos y las fallas del pasado, podrían mejorar la seguridad alimentaria y el acceso ofreciendo asistencia técnica de ingenieros y técnicos agropecuarios, líneas de crédito apoyadas por el gobierno para los campesinos y nuevos fondos para la infraestructura básica. Ojalá la burocracia de Colombia pueda aprender de sus errores y aprovechar el talento de los líderes locales que ya están trabajando para modernizar y mejorar la alimentación de los colombianos.

"Colombia | WFP | United Nations World Food Programme - Fighting Hunger Worldwide." Colombia | WFP | United Nations World Food Programme - Fighting Hunger Worldwide. World Food Programme, n.d. Web. 18 July 2016.

\* Corte Constitucional Sentencia T-025/04

"Entrevista Con Director De APAVE." Personal interview. July 2016

**\*Pasante en Ambiente y Sociedad/***Brown University*

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 

------------------------------------------------------------------------
