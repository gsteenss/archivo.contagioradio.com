Title: La verdad de Mancuso que podría llegar a la JEP
Date: 2019-06-21 16:03
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Congreso, JEP, Justicia y Paz, Mancuso
Slug: la-verdad-de-mancuso-que-podria-llegar-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Mancuso.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @LaLibertadCo  
] 

Desde Estados Unidos, el ex jefe paramilitar **Salvatore Mancuso manifestó su disposición de acogerse a la Jurisdicción Especial para la Paz (JEP), y manifestó que la Ley de Justicia y Paz no había reparado víctimas, no ha servido para condenar a máximos responsables de violaciones a los DD.HH. e incluso, funcionarios judiciales quieren ponerle trabas a su ejecución**. La versión de Mancuso ocurrió en el Congreso de la República, lugar en el que se desarrollaba la audiencia "Militares y terceros responsables: retos de la JEP", citado por la representante a la cámara Maria José Pizarro.

La participación de Mancuso en el debate llamó la atención, pues por segunda vez que se escucharía su voz en este recinto pero en condiciones diferentes. En esta ocasión, como lo afirmó **Alberto Yepes, director de la Coordinación Colombia, Europa, Estados Unidos (COEUROPA)**, lo sucedido ayer es importante porque "evidencia una voluntad de sectores importantes que estuvieron comprometidos en la guerra, involucrados en violaciones a los derechos humanos, para someterse a una instancia para contar verdades".

En ese sentido, el Director recalcó que es importante escuchar lo que Mancuso tiene por decir, y aceptar su disposición para someterse a la JEP. Algunos de los hechos mencionados por el ex jefe paramiltiar están relacionados con el pasado, y los crímenes cometidos en el marco del Conflicto, mientras otros son críticas al presente, y cómo se ha desarrollado la ley de Justicia y Paz.

### **Ley de... ¿Justicia y Paz?  
** 

Para iniciar su intervención, Mancuso afirmó que entendía que la guerra no era el camino, pidió perdón a las víctimas y exaltó la labor de quienes "a pesar de todos los obstáculos", lograron un acuerdo de paz con las FARC. Más adelante, **criticó la Ley 975 de 2005, o Ley de Justicia y Paz,** señalando que "debido a las falsas distinciones conceptuales y múltiples inconsistencias, vacíos e indefiniciones y por la parcialidad del proceso" **no se había cumplido con los estándares de justicia transicional.**

Hecho que se evidencia en que pasados 14 años de su promulgación, no se han proferido condenas "para grandes responsables de violaciones a los derechos humanos y menos aún" se ha generado reparación a sus víctimas. Incluso, su propio caso es ejemplo de ello, **pues de los 80 mil hechos por los cuales debería responder en virtud de su línea de mando, solo ha obtenido condena por 2 mil de ellos.**

### **Tomaría 100 años satisfacer los derechos de las víctimas con la Ley de Justicia y Paz**

El poco avance en procesos judiciales hace que incluso personas que pagaron 8 años de condena y 4 años de prueba no han recibido sentencia a pesar de haberla solicitado; y ante la falta de acción, la justicia ordinaria ha abierto nuevos procesos que retoman lo dicho por los mismos acusados en el marco de la Ley 975 para acusarlos. Así las cosas, Mancuso lamentó que se cumpliera su pronostico: **que tardarían más de 100 años en cumplir con lo acordado dada la forma en la que se planteó la norma.**

A ello se suma la denuncia sobre la actualidad del proceso, según la cual "algunos funcionarios de la policía judicial y Fiscalía están utilizando la justicia como forma de retaliación y presión indebida", esto, para **poner en entredicho las declaraciones aportadas por Mancuso ante los tribunales.** (Le puede interesar: [" La macabra alianza entre paramilitares y empresas bananeras"](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/))

Por último, Mancuso criticó el alcance de la Ley de Justicia y Paz, afirmando que esta limitada "a una pequeña parte del universo de victimarios del conflicto y una sola fracción de las víctimas y la verdad" lo que le impide hacer frente a todos los hechos de la guerra y que se evada la responsabilidad del Estado, militares y terceros responsables". (Le puede interesar: ["Se destapan nuevos nexos del uribismo con el paramilitarismo"](https://archivo.contagioradio.com/se-destapan-nuevos-nexos-del-uribismo-con-el-paramilitarismo/))

### **Por el contrario, la JEP puede ser todo aquello que no fue la Ley de Justicia y Paz** 

El ex paramilitar calificó la ley de **Justicia y Paz como una "parodia jurídico política"**; en sentido contrario, manifestó que la JEP es la mayor apuesta jurídica a un complejo proceso de paz, y es el momento para que la Jurisdicción demuestre que los errores de esta Ley se pueden corregir. Por su parte, Yepes dijo que la voz de Mancuso debería ser escuchada y la Corte Suprema de Justicia podría actuar sobre las denuncias contra la Fiscalía y funcionarios judiciales para castigar el sabotaje a la Ley 975 de 2005.

También resaltó el reconocimiento que hizo Mancuso a la responsabilidad de mando, porque ello significaría que estaría dispuesto a responder por misiones que ordenó, y permitió que se cometieran; ante lo que espera que no se vea interferido por quienes se podrían ver afectados por la verdad, y **que la JEP acepte su sometimiento, como forma de trabajar por esclarecer plenamente sobre lo ocurrido en el marco del Conflicto.**

<iframe id="audio_37414725" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37414725_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
