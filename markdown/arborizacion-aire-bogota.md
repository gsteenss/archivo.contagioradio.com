Title: Arborización y contaminación del aire en Bogotá
Date: 2019-03-07 13:30
Category: Voces de la Tierra
Tags: "Soma Mnemosine" teatro la Candelaria, aire, Árboles, Bogotá, Peñalosa
Slug: arborizacion-aire-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/fuego-detras-paloquemao1-e1551983121421.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Casas] 

###### [7 Mar 2019] 

Iniciando su último año en la alcaldía de Bogotá, Enrique Peñalosa ha sido critico por el aumento en la tala de árboles de la ciudad; el Distrito ha defendido está medida por el riesgo de árboles viejos que se caen, parques que requieren más espacio para canchas o para construir urbanizaciones (como en el Bosque Bavaria). Por su parte, los ambientalistas recuerdan el valor imprescindible de la vida vegetal en la ciudad.

En este programa se exponen las causas y consecuencias de la contaminación del aire en Bogotá; de igual forma se advierten los riesgos para la salud física y mental de quienes habitan la Capital, y se explica la relación entre la calidad del aire y el proceso de arborización, es decir, la cantidad de zonas verdes que se cuidan para que sigan cumpliendo con su función y las que se crean, para que aumentar las ya constituidas.

<iframe id="audio_33168162" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33168162_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  

###### [Encuentre más información ambiental en][[ ]Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)[y escúchelo los jueves a partir de las 11 a.m. en] [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
