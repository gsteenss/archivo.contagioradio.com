Title: Kurdos desalojan al estado islámico de la ciudad de Kobane
Date: 2015-01-26 23:12
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Estado Islámico, Kobane, Kurdistán, Siria
Slug: kurdos-desalojan-al-estado-islamico-de-la-ciudad-de-kobane
Status: published

Después de 4 meses de duros enfrentamientos entre las Unidades de Protección del pueblo y el grupo armado Estado Islámico, la ciudad de **Kobane ha sido liberada** por sus propios moradores dejando así una derrota importante para los islamistas radicales.

Kobane es un enclave importante para el EI, además de estar rodeada, por el norte es fronteriza con Turquía, de la cual no ha recibido ningún tipo de apoyo, pero tampoco de la comunidad internacional, solo escasamente del **Ejercito Libre de Siria**.

Por ello constituye un ejemplo de organización y de resistencia ante la embestida de grupos armados islamistas de carácter radical financiados, o no, por EEUU y que actualmente están azotando la región.

Han sido Kurdos quienes han liberado la ciudad, que reclama un **estado propio** y que actualmente esta **diseminado entre los estados de Irak, Turquía y Siria**, sufriendo las consecuencias de las distintas guerras civiles y de la represión por parte de los estados donde actualmente conviven.

Por último el peso y la importancia de las **mujeres en la resistencia ha sido clave**, prueba de ello son las numerosas fotos de milicianas combatiendo y organizando la resistencia.
