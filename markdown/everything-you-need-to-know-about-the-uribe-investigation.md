Title: Everything you need to know about the Uribe investigation
Date: 2019-10-08 11:08
Author: CtgAdm
Category: English
Tags: Alvaro Uribe, Metro Bloc, Supreme Court of Justice
Slug: everything-you-need-to-know-about-the-uribe-investigation
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/34146277812_d52c5f93f3_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Flickr Centro Democrático] 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Former president and current senator Álvaro Uribe testified this week before the Supreme Court to defend himself from allegations of witness tampering. Human rights activists said that this investigation offers credibility to the justice system and could lead to other cases against the former president. ]

[This investigation started in 2011 when Pablo Hernán Sierra García (known by his alias “Alberto Guerrero”), a former paramilitary commander,  requested a meeting with Senator Iván Cepeda, during which he told the congressman that former president Uribe and his brother, Santiago Uribe, had created the Metro Bloc of the so-called Peasant Self-Defense Forces of Córdoba and Urabá (ACCU). This led Cepeda to accuse Uribe of having links to paramilitary groups that same year.]

[Following the accusation, Juan Guillermo Monsalve, another former paramilitary commander, also requested a meeting with Cepeda to affirm Sierra’s testimony. Monsalve added that Santiago Uribe committed irregularities during the 2002 presidential elections, which his brother won. ]

[In 2012, Uribe’s lawyers accused Cepeda of abuse of public office for interviewing the two witnesses. In 2013, new witnesses were added to the case and the following year, Uribe claimed that Cepeda had bribed witnesses to falsely accuse the Uribe brothers.]

[After a legal saga of almost four years, the Supreme Court ruled that the visits Senator Cepeda made to the aforementioned witnesses because he acted within his competence as a senator and did not offer anything — including money — in exchange for the testimonies. In a twist of events, the high court ordered an investigation into whether Uribe had tampered with witness testimonies. ]

[In this new case, the Court has more than 100 hours of evidence, including wiretapped phone calls that reveal Uribe offering favors to witnesses to incriminate Cepeda. This past August, the Court called on Uribe and other key witnesses to testify.]

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/La-Cronología-del-caso-contra-Uribe-830x1024.jpg){.aligncenter .size-large .wp-image-74667 width="830" height="1024"}

### **What does the call to testify mean for those who suffered through the Uribe administation?**

[Soraya Gutiérrez, president of the José Alvear Restrepo Lawyer Association (CAJAR), said that the collective has suffered from the actions of what she called Uribe’s authoritarian administration because it “used all its state intelligence to persecute human rights defenders, and we suffered the consequences of his ‘Democratic Security’ policy.” For Gutiérrez, this case means that the justice system is finally coming after high-level officials that have violated the law. ]

[The lawyer lamented that Uribe is not being investigated for more serious crimes, but assured that the Supreme Court’s decision to call on Uribe to testify is important because the former president has “used his power to divert investigation and to bribe people.” At the time, the CAJAR president stressed that this event could lead to other legal processes within the Supreme Court.]

### **Three investigations that could follow Uribe’s testimony**

[Alirio Uribe Muñoz, Senator Cepeda’s legal defense, told Contagio Radio that this case could lead to more serious allegations: foremost, that the Uribe brothers created the Metro Bloc. Meanwhile, the Superior Court of Antioquia has an ongoing investigation open against Santiago Uribe for allegedly creating “The 12 Apostles,” a paramilitary group that is said to have had its start in a ranch formerly owned by the Uribe brothers.]

[Cepeda has also denounced before the Supreme Court that Uribe is implicated in the massacres of El Aro and La Granja as well as the assassination of activist José María Valle. All these crimes occurred in Antioquia while Uribe was governor of the province. The Superior Tribunal of Medellín called on the Attorney General's Office to investigate Uribe for his alleged participation and knowledge of the massacre in El Aro. ]

### **Is Uribe trying to evade the Supreme Court?**

[After the Court announced it would move forward with this case, Uribe said that his family was pleading with him to withdraw from politics. At another moment, the senator appeared to follow through with this wish and decided to abandon his congressional seat, but finally desisted. According to analysts, this move to leave Congress could allow him to escape the Supreme Court's jurisdiction.]

[Gutiérrez contradicted this take and said that the Court would not lose jurisdiction in this case because when Uribe allegedly committed the crimes he enjoyed “constitutional protections” that would require the high court to revise the case. Still, she clarified that this issue would provoke a legal debate. The CAJAR president said that she expects the Court maintains its jurisdiction over the case and hopes that the public will support the judicial power’s independence and decision.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
