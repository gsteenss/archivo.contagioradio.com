Title: FARC pide reunión con fiscal de la Corte Penal Internacional ante situación de la JEP
Date: 2017-11-19 20:20
Category: Nacional, Paz
Tags: Corte Penal Internacional, FARC, Timoleon Jimenez
Slug: farc_corte_penal_internacional_jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Corte-Penal-Internacional-e1472760697575.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reporteros asociados] 

###### [19 Nov 2017] 

Por medio de una carta dirigida a la Fiscal de la Corte Penal Internacional, Fatou Bensouda, el partido, Fuerza Alternativa Revolucionaria del Común, FARC, le solicitó una reunión ante lo que ha sido la implementación de la paz, pero puntualmente lo que tiene que ver con e **fallo de la Corte Constitucional sobre la Justicia Especial para la Paz.**

En el texto, la FARC señalan que dicho fallo de la Corte "**abre las puertas a la impunidad, en una clara burla a los derechos de las víctimas"**, ya que la sentencia excluyó de la obligatoriedad de someterse a la JEP, a agentes estatales no militares y a terceros responsables de graves crímenes.

De acuerdo con la carta, esa situación implica que se deja el juzgamiento de esos actores a la justicia ordinaria que, durante los más de cincuenta años de conflicto armado en el país, "jugó el papel de instrumento de guerra a favor del Estado, aplicándose con todo su rigor apenas a los contradictores del mismo".

Por otro lado, resaltan que por parte parte de la FARC "tal y como recién acaba de certificarlo el Señor Secretario Adjunto para los Asuntos Políticos de las Naciones Unidas, Jeffrey Feltman, **se ha producido un cumplimiento estricto de todos y cada uno de los compromisos pactados",** pero que por parte del gobierno ha sido todo lo contrario.

Aquí el texto completo de la carta

<div class="itemIntroText">

*Respetada Señora Fiscal:*

*Reciba Usted desde Colombia, el entusiasta saludo del nuevo partido político FUERZA ALTERNATIVA REVOLUCIONARIA DEL COMÚN, nacido del desarrollo del Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera, firmado el 24 de noviembre del año anterior entre el gobierno de nuestro país y la guerrilla de las FARC-EP.*

</div>

<div class="itemFullText">

*Dicho Acuerdo puso fin al más largo conflicto armado de nuestro continente, mediante una serie de fórmulas que fueron pactadas, como usted conoce, tras cinco años continuos de deliberaciones en la ciudad de La Habana, Cuba. A partir de su firma se puso en movimiento el engranaje de su implementación constitucional, legal y práctica.*

*Por nuestra parte, tal y como recién acaba de certificarlo el Señor Secretario Adjunto para los Asuntos Políticos de las Naciones Unidas, Jeffrey Feltman, se ha producido un cumplimiento estricto de todos y cada uno de los compromisos pactados, sintiéndonos así en el completo derecho a exigir del Estado colombiano un  comportamiento semejante.*

*Muchísimas cosas podríamos exponer frente al desconsolador estado de la implementación del Acuerdo Final, pero no es ese el propósito de la presente misiva. Nuestra alarma real, la que nos mueve a dirigirnos a usted, estriba en la reciente sentencia C-17 dictada por la Corte Constitucional de nuestro país, que declaró exequible el Acto Legislativo 01 del presente año.*

*Tal Acto Legislativo incorporó a la Constitución Nacional el denominado Sistema Integral de Verdad, Justicia, Reparación y No Repetición, uno de cuyos más importantes capítulos lo constituye el dedicado a la Jurisdicción Especial para la Paz, JEP, cuyo objetivo central no es otro que satisfacer el derecho a la justicia de las víctimas del conflicto.*

*La Corte Constitucional de nuestro país, en la sentencia referida, se pronunció acerca de la exequibilidad de dicho Acto Legislativo. Es de anotar que el fallo de la Corte fue informado a la opinión pública mediante un comunicado oficial, que resume el texto de la sentencia, pero que esta misma aún no se ha dado a conocer y habrá que esperar algún tiempo para ello.*

*En el mencionado comunicado la Corte Constitucional colombiana, pese a declarar ajustado a la Constitución el conjunto del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, declara simultáneamente inconstitucionales varias disposiciones del capítulo de la Jurisdicción Especial para la Paz, creando una serie de situaciones realmente preocupantes.*

*Es a ese respecto que solicitamos de la manera más respetuosa a Usted, la realización de una entrevista con delegados de nuestra organización política y sus asesores jurídicos. En nuestro parecer, conforme con la jurisprudencia internacional sobre la materia, el fallo de la Corte colombiana abre las puertas a la impunidad, en una clara burla a los derechos de las víctimas.*

*La jurisdicción Especial de Paz fue concebida como un mecanismo excepcional de justicia transitoria, cuyo objetivo no sólo era poner fin al conflicto, sino sobre todo asegurar que llegara a su fin la impunidad reinante en Colombia en materia de crímenes de Estado y graves violaciones a la ley internacional por parte de terceros en el conflicto.*

*En el Acuerdo Final quedó consignada por parte nuestra,  la voluntad de comparecer ante esta jurisdicción, ofrecer verdad y suministrarla completamente, asumir la responsabilidad en lo que nos corresponda y cumplir con  las sanciones a que haya lugar por obra de nuestros hechos. Así reafirmamos nuestro compromiso con los derechos de las víctimas del conflicto.*

*La sentencia de la Corte excluyó de la obligatoriedad de someterse a la JEP a agentes estatales no militares y a terceros responsables de graves crímenes, dejando su juzgamiento a la justicia ordinaria, justicia que en más de cincuenta años de conflicto jugó el papel de instrumento de guerra a favor del Estado, aplicándose con todo su rigor apenas a los contradictores del mismo.*

*Por tal motivo nuestro partido ha decidido radicar el Acuerdo Final firmado el 24 de noviembre en el Teatro Colón de Bogotá entre el Presidente Juan Manuel Santos y Timoléon Jiménez, comandante de las FARC-EP, en la Secretaría Técnica de la Corte Penal Internacional, a objeto de que sea ampliamente conocido y valorado.*

*De ese modo aspiramos a conseguir que la Corte Penal Internacional sopese, cómo el fallo de la Corte Constitucional sobre la JEP y los cambios de última hora agregados en el Congreso de la República,  destrozan elementos claves destacados por expertos internacionales por su sujeción a la legislación internacional sobre los crímenes de guerra y de lesa humanidad.*

*De la Señora Fiscal, respetuosamente,*

*RODRIGO LONDOÑO ECHEVERRY*

</div>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
