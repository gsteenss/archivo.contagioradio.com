Title: Un Tango por Mocoa
Date: 2017-04-12 10:59
Category: Cultura, Nacional
Tags: Ayudas para Mocoa, Mocoa
Slug: un-tango-por-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-11-at-7.24.05-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [11 Abr 2017] 

Eran las siete de la noche y Cristina Ulcue se encontraba en la sala de Alma de Tango, un lugar envuelto entre la bohemia y el romanticismo, que por una noche sería el refugio de un **amor más grande que la pasión por el bandoneón, sería el escenario para que la solidaridad y la voluntad se reunieran y bailaran por Mocoa**.

Cristina viajó 623km, salió de su ciudad y aunque recalca que ella no fue damnificada de la avalancha ocurrida el 1 de abril, hace parte del pueblo Nasa, comunidad que ha logrado establecer que **800 familias pertenecientes a su resguardo indígena lo perdieron todo en la madrugada de ese sábado**.

Llego a la helada capital, por primera vez y por tan solo una noche, para ser recibida por los mejores bailarines de Tango del país, que días atrás y en apoyo a la iniciativa de Javier Sánchez, bailarín profesional, decidieron juntarse para **brindar un espectáculo de talla internacional y recolectar de esta forma donaciones para los habitantes de Mocoa.**

Faltaban cinco para las ocho y una fila se iba formando a las afueras de Alma de Tango, las mujeres con sus zapatos de tacón y faldas ajustadas, los hombres de pantalón de paño y zapatos lustrados, se preparaban para dejar en la pista una vez más, el alma.

Cristina observaba, en silencio, las parejas que giraban con las manecillas del reloj y que con elegancia y el drama, se desvanecían entre pasos largos y abrazos. Pronto llegarían las palabras de Javier, la bienvenida a una noche inolvidable. Le puede interesar: ["El Tango se une esta noche por Mocoa"](https://archivo.contagioradio.com/el-tango-se-une-esta-noche-por-mocoa/)

“Mucha gente en Colombia piensa que consignar algo individual es muy poco y terminan no haciéndolo, cualquier cosa que podamos aportar suma, **nosotros juntamos el medio del tango, somos el medio hoy para canalizar las ayudas**".

En una noche más de 120 parejas, profesionales, apasionados, aprendices lograron recolectar **120 kilos de donaciones en alimentos no perecederos y \$4.322.000 pesos, que serían entregados a Cristina para que los llevará hasta Mocoa**.

![Contagio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-11-at-7.22.30-PM.jpeg){.alignnone .size-full .wp-image-39097 width="584" height="584"}

El turno para hablar de Cristina llegó, la razón de recorrido de 623km, ya era un hecho “Desde Mocoa, desde el pueblo Nasa, estamos agradecidos, queremos contarles que está pasando con nuestro pueblo, **tenemos madres cabeza de hogar que quedaron solas, niños huérfanos, padres que perdieron sus hijos**”.

“Yo sé que no es fácil, me duele por los niños, me duele por las familias que han quedado solas y han perdido sus viviendas, **el gobierno Nacional no se ha hecho presente en nuestra comunidad,** las ayudas que hemos conseguido son de organizaciones, a ustedes gracias”.

Pese a las noticias que los asistentes habían escuchado y visto en diferentes medios de información, **la realidad que exponía Cristina era estremecedora,** su voz quebrantada reflejaba madurez, dignidad y agradecimiento.

La noche se revistió de voluntad, seguían las muestras artísticas de las y los campeones de Tango, con lentejuelas y coquetería, los artistas iban dejando sus mejores pasos, **se entregaban a la humildad y dignidad de Cristina e iban cerrando la velada.**

![Tngo por Mocoa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-11-at-7.25.49-PM.jpeg){.alignnone .size-full .wp-image-39114 width="1040" height="584"}

Las luces se atenuaron e indicaron el final del show, poco a poco los aficionados fueron retomando sus lugares en la pista, se abrazaron para despedir a las últimas milongas, adiós a las nuevas parejas, el violín, melancólico marcaba una sensación diferente esta noche, **le hacía el juego a la esperanza que demostró que existe y prevalece, desde los artistas, desde la vida.**

Alma de Tango, La Milonga del Sueñño Tango Esencias, El Desbande, Tupungatina, Barra de Tango Universidad del Rosario, Milonga Tinta Roja y Textiles Moda, **se quedan desde esa noche en el alma Nasa del Putumayo.**

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
