Title: Así son las audiencias preparatorias de participación en diálogos ELN y gobierno
Date: 2017-10-27 13:14
Category: Nacional, Paz
Tags: Apoyo al proceso de paz, ELN, Quito
Slug: asi-seran-las-audiencias-preparatorias-de-participacion-en-dialogos-eln-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mesa-social-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [27 Oct 2017] 

La próxima semana iniciarán las audiencias públicas de participación de organizaciones con los equipos de diálogo de la mesa en Quito para preparar la participación que recogerá la postura de la sociedad. Las organizaciones deberán responder a 2 preguntas y tendrán **15 minutos para intervenir con sus respuestas y propuestas para este reto del proceso de paz entre el ELN y el gobierno Nacional**.

La pregunta es ¿Según sus experiencias y conocimientos, cuáles considera usted que deberían ser los mecanismos y formas de participación de la sociedad en el proceso de conversación, y en particular la participación de su sector? Y la segunda parte consiste en proponer experiencias que la organización considere importante y aporte esa postura a la mesa de diálogos. (Le puede interesar:["Listo escenario para la participación social en conversaciones de Quito"](https://archivo.contagioradio.com/listo-escenario-para-la-partcipacion-social-en-conversaciones-de-quito/))

### **30,31 y 1 de noviembre** 

La distribución de las organizaciones que participaran quedo establecida de la siguiente forma **el 30,31 y 1 de noviembre van estar 40 organizaciones representadas por 46 personas**. Estos días participaran población LGTBI, organizaciones campesinas de Arauca, organizaciones campesinas nacionales y regionales, un sector étnico en el que estarán representadas organizaciones indígenas del nivel regional o nacional.

De igual forma participarán gremios de la capital, la institucionalidad, el sector de jóvenes y experiencias del Magdalena y Santander, de igual forma estará el sector minero energético incluidos Ecopetrol y otras empresas petroleras o afines.

### **7, 8 y 9 de noviembre** 

La segunda ronda que se realizará el **7, 8 y 9 de noviembre participarán mujeres**, organizaciones multilaterales, organizaciones de la región del Pacífico, representantes de personas con capacidades especiales, plataformas de paz, sindicatos, la región del sur occidente y organizaciones de víctimas y derechos humanos. (Le puede interesar: ["Listo mecanismo de monitoreo y verificación del cese bilateral entre el ELN-Gobierno"](https://archivo.contagioradio.com/cese-al-fuego-entre-el-eln-y-el-gobierno-ha-reducido-intensidad-del-conflicto/))

### **14 y 16 de Noviembre** 

Las delegaciones se encontrarán con académicos y ambientalistas, el 15 de noviembre con la región Caribe y el 16 con los sectores y organizaciones urbanas, a su vez se desarrollará una **audiencia virtual con colombianos en el exilio y se estaría planteando la participación de personas desde las cárceles del país**.

De igual forma, 20 organizaciones sociales y 4 plataformas de paz, acordaron 8 puntos comunes para enfatizar. El primero es que las recomendaciones que se hagan desde la sociedad deben ser vinculantes, que la convocatoria de participación debe gestarse a nivel local, departamental, regional y nacional; **que exista una coordinación entre sectores y una retroalimentación de lo expuesto**.

Además, señalan la importancia de que haya una articulación con las FARC y la implementación de los acuerdos, que se tengan en cuenta los avances y acumulados sociales, que la participación sea paritaria y que haya garantías de seguridad para líderes en el país.

###### Reciba toda la información de Contagio Radio en [[su correo]
