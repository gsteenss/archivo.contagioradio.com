Title: Refrendación de Acuerdos de paz quedará en manos del Congreso
Date: 2016-11-02 13:27
Category: Nacional, Política
Tags: #AcuerdosYA, Apoyo al proceso de paz
Slug: plebiscito-demoraria-aun-mas-implementacion-acuerdos-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/congreso-e1478108708937.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia ] 

###### [2 de Nov 2016] 

El presidente Santos afirmó la posible aprobación de los acuerdos de paz vía Congreso y descartando un segundo plebiscito, hecho que de acuerdo con el ex Magistrado Alfredo Beltrán es completamente viable y constitucional, debido al cargo que tiene el máximo mandatario de mantener y restablecer el orden público en el territorio Nacional, **mientras que un segundo plebiscito podría ser mucho más lento. **

El ex magistrado además afirmó que la facilidad de este mecanismo es que “**muchos de los puntos pactados en la Habana no requieren que se expidan más leyes sino que se cumplan leyes actuales como es el caso del Agro**”. Mientras que las desventajas de hacer otro plebiscito son las demoras en el tiempo.

Beltrán es claro al decir que desde la constitución el presidente no tenía la imperiosa necesidad de realizar el plebiscito, motivo por el cual tanto el presidente como el Congreso de la República “mantienen a plenitud las atribuciones y competencias constitucionales y **pueden tomar la decisión política sobre los acuerdos de paz**”.

Frente a los riesgos de esta decisión Beltrán afirma que es el presidente quién “**debe analizar las circunstancias y en conjunto con el congreso llevar a cabo la política de paz**”. La metodología para la aprobación de los acuerdos se daría de las misma forma como un ley ordinaría, pasaría a debates por cámara y posteriormente senado.

**Esta no sería la primera vez en Colombia que se usa este mecanismo para intentar terminar conflicto armados con diferentes guerrillas**. El presidente Barco, después de haber pactado con las FARC-EP lo usó con la ley 77, al igual que el ex presidente Betancur con la ley de admistía que otorgó durante su gobierno. Le puede interesar: ["Alcaldes y gobernadores respaldan el carácter humanitarios de los acuerdos de paz"](https://archivo.contagioradio.com/alcaldes-y-gobernadores-respaldan-el-caracter-humanitario-de-los-acuerdos-de-paz/)

<iframe id="audio_13586391" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13586391_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
