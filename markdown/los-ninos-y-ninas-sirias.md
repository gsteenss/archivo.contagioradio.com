Title: Los niños y niñas sirias
Date: 2016-03-28 11:49
Category: Eleuterio, Opinion
Tags: Conflicto en Medio oriente, Europa, Siria
Slug: los-ninos-y-ninas-sirias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Tammam-Azzam.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tammam Azzam 

#### **Por [Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 28 Mar 2016 

[Marzo de 2011, estamos en plena ebullición de las revueltas árabes en el norte de África. Los aires de revolución traspasan fronteras y geografías y se adentran en otras regiones del mundo árabe. En la ciudad de Daraa, Siria,  un grupo de niños de 11 años se impregnan de esa brisa que llega desde el Mediterráneo y deciden actuar. Una acción simbólica, una reivindicación pacífica, un acto de libre expresión, una pequeña gamberrada dirían algunos. Esperan a que oscurezca para llevarla a acabo. A la mañana siguiente el muro de su escuela aparece pintado con el siguiente deseo:]*[el pueblo quiere la caída del régimen.]*

[Poco después los niños desaparecieron. Durante varios días las familias sufrieron con la espera y finalmente fueron a denunciar ante la policía. El jefe de seguridad, Aatef Najib,] [primo de Bashar al Assad, presidente heredero y omnipotente de Siria, les respondió: “]*[olvidaos de vuestros hijos, si queréis nuevos, traernos a vuestras mujeres.”]*[ La soberbia  amenaza no se cumplió y tiempo después los pequeños aparecieron. Habían sido brutalmente torturados. Ante tal barbarie la gente comenzó a salir a las calles a protestar, una vez más había sido la acción de los más jóvenes la que había impulsado al resto de la sociedad a tomar la iniciativa.  La gente marchó en barias ciudades exigiendo la dimisión de los responsables y reformas en el régimen. Hubo cien muertos en la primera semana pero las protestas se expandieron…]

[La compañera Elisa Marvena, responsable del colectivo Solidaridad con la Revolución Siria en la ciudad de Barcelona, nos hace un relato de aquellos días. “]*[Las protestas pacíficas se prolongaron durante siete meses pero ya desde el primer momento había francotiradores  que disparaban a la gente en la calle. A medida que la contestación del régimen va siendo cada vez más violenta se pasa de una mera revuelta, de peticiones de reformas y cambios, a una revolución porque lo que se pretende entonces es derrocar el sistema.”]*[El ejército y la policía no aflojan. Entre ellos se establecieron dos líneas de actuación, si la primera duda o no dispara a los ciudadanos que se manifiestan, la segunda debía disparar a sus propios compañeros. A partir de entonces surgen las primeras organizaciones civiles de defensa armada para proteger a los manifestantes y las primeras deserciones en el ejército. De la unión de estas dos circunstancias se formará en junio de 2012 el Ejército Sirio Libre]**,**[ al que muchos jóvenes se van a unir. “]*[Se dan casos de gente de veinte y pocos años, universitarios que jamás en su vida se preocuparon por la política que con todo su corazón y la ilusión por liberar a su país, cogen el fusil y se unen al nuevo ejército.”]*

Después los acontecimientos se precipitan. La revuelta desemboca en una cruenta guerra donde hay más de 300 mil muertos y más de cuatro millones de exiliados que han abandonado el país, un país devastado a día de hoy. También han surgido actores terribles como el ISIS o Daesh neofascistas de corte islámico que expanden su terror más allá de las fronteras sirias llegando al corazón de Europa. Una Europa que responde bloqueando sus fronteras para frenar la llegada de los que huyen de éste y otros horrores, entre los que se encuentran muchos niños y niñas como los que pintaron las paredes de su escuela pidiendo libertad para su pueblo.
