Title: Reclutamiento forzado: realidad inocultable en el Medellín metropolitano
Date: 2016-02-29 09:28
Category: Fernando Q, Opinion
Tags: Medellin, Paramilitarismo, reclutamiento forzado
Slug: reclutamiento-forzado-realidad-inocultable-en-el-medellin-metropolitano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/paramitarismo-medellin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimundo (archivo) 

#### **[Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano)** 

###### 29 feb 2016 

Nuevamente se abre el debate sobre el tema del reclutamiento forzado de niños, niñas y adolescentes en la ciudad de Medellín y su Área Metropolitana, problemática que ha demostrado la falta de Estado local, departamental y nacional.

[Varios estudios han surgido que arrojan cifras sobre la situación: el más reciente de la Universidad de Antioquia planteó que en los últimos cuatro años las bandas de la ciudad reclutaron, léase bien, a 1.745 niños. ¡1.745 niños! Todos durante la Administración que convirtió a Medellín en la ciudad más innovadora del mundo; un castillo de naipes -reducción de homicidios como triunfo institucional sobre el crimen, que aumentó otras manifestaciones de criminalidad- al que apenas empiezan a caérsele las barajas, especialmente sus ases.]

[Durante los últimos cuatro años, el crimen organizado se instaló y fortaleció bajo el amparo de un acuerdo criminal -pacto del fusil- que ayudó a consolidar las rentas ilegales y los ejércitos de menores reclutados a la fuerza; las denuncias eran constantes pero la administración desoía mientras los premios llegaban continuamente.   ]

[En julio de 2014, Análisis Urbano publicó el artículo “][[Los desterrados de San Gabriel: crónica de un desahucio en los límites de la Comuna 13]](http://analisisurbano.com/?s=los+desterrados+de+San+Gabriel)[”, en el que se denunció el reclutamiento forzado con casos concretos y que muchas familias se vieron obligadas a pagar \$5.000 pesos diarios para que sus hijos no fueran participes de la guerra urbana; el que se negaba, o moría o debía irse del barrio: Jota y Daniel se negaron a reclutarse y a irse del lugar en el que crecieron. Hoy están muertos.]

[Después de las muertes de Jota y a Daniel, la orden criminal de reclutar a todos los jóvenes seguía vigente, así que 22 familias se desplazaron de San Gabriel por miedo a perder otro de sus hijos; el desplazamiento masivo fue escándalo nacional pero nadie se centró en sus causas originales: el reclutamiento forzado. Para el estado fue sólo una cifra más que dejaba su abandono.]

[Dos años después de este suceso, y de otros casos que fueron ocultados por la  que parecía Secretaría (o Vicealcaldía que para el caso también aplica) de Cosmética Oficial y no de Gobierno (o Gobernabilidad), vuelve a ser noticia que gran cantidad de familias son obligadas a pagar semanalmente la vacuna para que sus hijos no sean reclutados: 10 mil pesos, e incluso 50 mil mensuales, para que los jóvenes no corran la misma suerte de Jota, Daniel, y muchos otros que murieron por la misma causa bajo el anonimato.]

[La verdad es que las bandas ven en los niños, niñas y adolescentes una mano de obra barata, dócil y arrojada, que puede servir para diferentes actividades criminales: campaneros, cobradores de vacuna, transportadores de armas y drogas, labores de inteligencia, y participantes del aparato militar criminal ya sea como vigilantes de trincheras y fronteras y, obviamente, como combatientes, sicarios, torturadores y  desmembradores de personas.]

[La Personería de Medellín presentó su informe anual de Derechos Humanos el pasado 23 de febrero, y con él sólo refuerzo mi hipótesis: esta era una ciudad de papel y mediática, asentada sobre una alfombra (reducción de homicidios) que cubría la violencia, la verdadera criminalidad y las violaciones de Derechos Humanos; quienes hemos tenido alfombras sabemos que cuando no queremos barrer la mugre sólo debemos elevar un poco el tapete y con él tapar la basura.]

[Redujeron los homicidios, pero la desaparición forzada salió a las calles y hoy 968 personas continúan desaparecidas, 147 son niños, niñas y adolescentes; estos son los resultados de un pacto criminal que tuvo la anuencia estatal.]

[Ahora, pregunto: ¿Quién nos va a responder por Jota, Daniel y los 147 menores desparecidos? ¿Dónde está el Alcalde, el comandante de la Meval y los asesores que ocultaron la realidad?]

[Esta es una de las mayores vergüenzas que tiene Medellín: durante cuatro años despilfarraron más de 400 mil millones de pesos en seguridad y la infancia y la adolescencia nunca estuvieron protegidas. Si aquí no hay corrupción, omisión, complicidad y crimen… Juro que no sé qué hay.]
