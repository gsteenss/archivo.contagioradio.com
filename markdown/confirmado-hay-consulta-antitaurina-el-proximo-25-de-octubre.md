Title: Confirmado: Hay Consulta Antitaurina el próximo 25 de octubre
Date: 2015-09-18 16:21
Category: Animales, Voces de la Tierra
Tags: Alcaldía de Bogotá, Consulta Antitaurina, corridas de toros en Bogotá, crueldad animal, elecciones 25 de octubre
Slug: confirmado-hay-consulta-antitaurina-el-proximo-25-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bailaton-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Tras la reunión **entre el registrador Distrital, Jaime Hernando Suárez, y la Secretaria General del distrito, Martha Lucía Zamora**, donde se analizaron los costos y las posibilidades de que la consulta popular antitaurina se realice con las elecciones regionales, se concluyó que la Alcaldía será quien asuma el valor de este mecanismo de participación popular.

Luego de conocerse la noticia, los defensores de los animales celebran la decisión con la "bailatón" que se desarrolla desde las 2 de la tarde frente a Registraduría.
