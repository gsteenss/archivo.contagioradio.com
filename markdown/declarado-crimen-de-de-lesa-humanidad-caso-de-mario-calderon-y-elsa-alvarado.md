Title: Declarado crimen de lesa humanidad caso de Mario Calderón y Elsa Alvarado
Date: 2017-05-12 08:16
Category: Judicial, Uncategorized
Tags: CINEP
Slug: declarado-crimen-de-de-lesa-humanidad-caso-de-mario-calderon-y-elsa-alvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/27e98eccacc65c98966c4c9f956811d3_1463713816-e1494594662808.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [12 Mayo 2017] 

La Fiscalía General de la Nación **declaró crimen de lesa humanidad el asesinato de los investigadores y defensores de derechos humanos Mario Calderón y Elsa Alvarado.** La decisión se da a tan solo una semana de cumplirse 20 años de este crimen, cometido por grupos paramilitares y con responsabilidad del Estado colombiano.

A través de un comunicado la Comisión Colombiana de Juristas, organización que lleva el caso, se conoció esta decisión en la que la Fiscalía declara: **“los delitos fueron cometidos dentro de un plan sistemático y generalizado contra la población civil”.** Por lo tanto, reitera y ratifica la calificación hecha en la resolución de situación jurídica del actualmente investigado Coronel en retiro Jorge Eliécer Plazas Acevedo. Le puede interesar: "Defensores de derechos humanos "contra las cuerdas

La determinación del ente fiscal también declara el asesinato del padre de la defensora Carlos Alvarado como crimen de lesa humanidad.

**El hecho sucedió el 19 de mayo de 1997** cuando paramilitares fuertemente armados, se hicieron pasar por agentes del Cuerpo Técnico de Investigación de la Fiscalía, irrumpieron en el apartamento de los defensores.

En desarrollo..
