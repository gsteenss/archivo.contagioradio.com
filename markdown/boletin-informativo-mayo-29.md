Title: Boletín Informativo Mayo 29
Date: 2015-05-29 17:36
Category: datos
Tags: Actualidad Contagio Radio, con la presencia de 300 policías y el ESMAD, contra los indígenas del Norte del Cauca, conversaciones entre Grecia y Turquía, Corinto de la localidad de Suba, EEUU ha sacado a Cuba de la lista de países terroristas, Noticias del día en Contagio Radio, SMAD
Slug: boletin-informativo-mayo-29
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4571244_2_1.html?data=lZqkk5eYeI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk56ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Continúa la **represión por parte del Escuadrón Antidisturbios ESMAD,** contra los indígenas del Norte del Cauca. De acuerdo con **Héctor Dicue, exconsejero de la Asociación Indígena del Norte del Cauca**, **ACIN**, el pasado 21 de mayo, nuevamente **la fuerza pública realizó un intento de desalojo contra las comunidades** que se encuentra en el proceso de **Liberación de la Madre Tierra en Corinto**.

-**El líder Grecochipriota Nikos Anastasiadis, y el Turcochipriota Mustafá Akinci,** iniciaron este jueves una ronda de **conversaciones entre Grecia y Turquía** con el objetivo de comenzar un proceso de paz en Chipre y f**inalizar con el último muro de separación que existe en Europa.**

-En el **barrio Corinto de la localidad de Suba**, con la presencia de **300 policías y el ESMAD**, y por medio de gases lacrimógenos, piedras, tanquetas y chorros de agua, **la Casa Cultural 18 de Diciembre** se ha encontrado bajo amenaza de desalojo; asegura **Antonio Torres**, habitante de la vivienda.

-**EEUU ha sacado a Cuba de la lista de países terroristas**, en la que estaba desde 1982, por la estigmatización por parte de los gobiernos sucesivos de Estados Unidos. El gesto supone un paso más para el **restablecimiento total de las relaciones diplomáticas**.

-Los países garantes del proceso de paz entre el **Gobierno colombiano y las FARC-EP**, Cuba y Noruega, se han pronunciado **solicitando se aceleren las conversaciones entorno a un cese bilateral del fuego**. Ante este pronunciamiento, algunos sectores sociales han expresado que los garantes se están sobrepasando en su papel. Habla **Carlos Medina Gallego del Centro de pensamiento de la Universidad Nacional de Colombia.**

-Al menos **1.500 personas han muerto en la India por la ola de calor extremo** que ha sacudido el país con **temperaturas entre 47 grados hasta los 52 grados centígrados.** Las pésimas condiciones laborales, el sistema sanitario y la extrema pobreza **han llevado al país al colapso y a la catástrofe humanitaria**.

 
