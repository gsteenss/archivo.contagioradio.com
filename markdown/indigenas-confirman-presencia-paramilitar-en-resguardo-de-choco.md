Title: Indígenas confirman presencia paramilitar en resguardo de Chocó
Date: 2016-09-01 14:59
Category: DDHH, Nacional
Tags: Bajo Atrato, paramilitarismo colombia, Paramilitarismo en Chocó
Slug: indigenas-confirman-presencia-paramilitar-en-resguardo-de-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [1 Sep 2016] 

Entre el 20 y el 21 de agosto, integrantes del 'Resguardo Humanitario y Ambiental Uradá Jiguamiandó So Bia Drua', junto a representantes de organizaciones defensoras de los derechos humanos nacionales e internacionales, realizaron una visita de verificación en la que **confirmaron la presencia de por lo menos 100 paramilitares en el Resguardo**, y su asentamiento en los puntos conocidos como 'La Bracharia', 'Loma de Canelón', 'Quebrada El Choro y la cabecera del río Uradá, en Carmen del Darién, Chocó.

De acuerdo con esta verificación, los paramilitares se vienen asentando desde hace por lo menos 13 días, se movilizan tanto de civil como de camuflado y siempre portan armas; todo ello **a escasos metros de la subestación de policía de Pavarandó y de la base militar ubicada en Llano Rico** y a la que pertenece un Sargento de apellido Vega quién aseguró que sobre la región hay presencia de minería ilegal y que la [[presencia paramilitar](https://archivo.contagioradio.com/alto-riesgo-de-desplazamiento-en-choco-por-incremento-de-operaciones-paramilitares/)] podría deberse al cobro de vacunas.

Los pobladores también comunicaron a la comisión de verificación, la retención arbitraria de la que el pasado 10 de agosto fueron víctimas dos jóvenes indígenas pertenecientes al resguardo, a quienes **los paramilitares obligaron a servir de guías durante cuatro días**, en los que además los interrogaron sobre la presencia de la guerrilla.

Frente a esta situación de peligro, los líderes y habitantes del Resguardo exigen a los paramilitares la retirada de su territorio "porque somos pueblo de paz y que no aceptamos ningún actor armado en nuestra jurisdicción". En ese sentido, demandan la intervención inmediata de las autoridades competentes que investiguen los hechos y **activen medidas de protección adecuadas para las comunidades indígenas** de Ibudó; Padadó; Jaibía, Coredocito; Alto Guayabal; Nuevo Cañaveral y Bidoquera, Ancadía, que se encuentran en peligro.

<iframe id="doc_67977" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322771109/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CQGUSMbWH5M7XIehiRm7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ]
