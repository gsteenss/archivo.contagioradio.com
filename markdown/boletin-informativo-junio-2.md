Title: Boletín Informativo Junio 2
Date: 2015-06-02 17:30
Category: datos
Tags: alirio uribe, Asesinato de Fabian Espinosa sindicalista, Día del campesinado, Mesa Regional de Organizaciones sociales del departamento del Putumayo, Noticias del día en Contagio Radio, Nuge Buglé contra Barro Blanco, XII Reunión de la Conferencia de las Partes de la Convención de Ramsar
Slug: boletin-informativo-junio-2
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4585932_2_1.html?data=lZqll56Xdo6ZmKiakpqJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZeRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Fabián Espinosa**, sindicalista asesinado el pasado fin de semana **había solicitado medidas urgentes de protección por las múltiples amenazas en su contra**, por ello la familia y demás integrantes del MOVICE y de los sindicatos de los que hacía parte rechazan las **versiones entregadas por la policía en la que se afirma que su crímen se cometió por ser integrante de la comunidad LGBTI** y lo ubican como un delito común. **Nubia Mendoza integrante del MOVICE**.

-Este martes, **la Mesa Regional de Organizaciones Sociales del departamento del Putumayo**, decidió **suspender los diálogos con el gobierno nacional**, debido a los últimos hechos presentados donde se evidencia la **incoherencia del gobierno, al querer continuar en la guerra y así mismo, con las fumigaciones aéreas con glifosato**, habla **Yury Quintero, integrante de la Mesa de Organizaciones regionales del Putumayo.**

-En el marco de la celebración del **día del campesinado** que se llevará a cabo el 5 de Junio del presente año, el **Comité de Interlocución Campesino y Comunal “CICC”, y la Alianza Campesina y Comunal “ALCAMPO”**, convocaron a un conversatorio sobre el **“Aporte del proceso Mercados Campesinos al Abastecimiento Alimentario en Bogotá”** con el fin de afianzar las relaciones interinstitucionales entre la población campesina y la urbana.

-**La comunidad indígena Nuge Buglé**, se encuentra en un **proceso de resistencia en contra del proyecto hidroeléctrico Barro Blanco** de la empresa hondureña GENISA, que afectaría social y ambientalmente a la comunidad. **Manolo Sárate**, quien hace parte de la **Secretaría de alianza Estratégica Nacional de movimientos sociales de Panamá.**

-Esta semana se lleva a cabo la **XII Reunión de la Conferencia de las Partes de la Convención de Ramsar**, donde se llamó la atención a los 168 países que hacen parte de este evento internacional, debido a que **en los últimos 100 años el planeta ha perdido un 64 % de sus humedales, y además la población de especies de estos ecosistemas se ha reducido un 66 %**.

-Frente a **las cifras entregadas** recientemente **por el partido Centro Democrático**, queda claro que **la guerra es una barbarie que hay que terminar de una vez por todas**, pero también queda claro que **el partido que lidera el expresidente Uribe siempre ha hecho política en función del miedo**. **Alirio Uribe, representante a la Cámara e integrante del Frente Amplio por la Paz**.
