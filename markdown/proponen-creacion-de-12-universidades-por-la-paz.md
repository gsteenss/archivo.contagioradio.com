Title: Proponen creación de 12 universidades por la Paz
Date: 2015-10-06 16:37
Category: Educación, Nacional
Tags: 12 universidades, cacarica, Catedra universidades por la paz, Comisión Intereclesial de Justicia y Paz, Conpaz, Curvarado, etnoeducacion
Slug: proponen-creacion-de-12-universidades-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: onic.org.co 

<iframe src="http://www.ivoox.com/player_ek_8826040_2_1.html?data=mZ2fmJWYdI6ZmKiak5WJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh4a3lIquk9nJqNPVjMbPy8rWuMKfxcqY19PNusbm1M7Rw8nJt4zk0NeYzsaPtMLujpC30c3FssKfrdSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Johana López, Justicia y Paz Colombia] 

###### [6 oct 2015] 

El día de ayer se **inauguró la “Cátedra abierta de universidades por la paz”**, un proyecto desarrollado desde la Comisión de Justicia y Paz, que busca **crear 12 universidades**, generando desde los territorios y las comunidades una cátedra y un conocimiento al rededor de la paz mediante el diálogo de saberes.

Johana López, integrante del equipo de Justicia y paz, indica que en esta cátedra **participan 70 líderes y lideresas de distintas partes del país**, a los que pertenecen distintos procesos urbanos, campesinos, madres indígenas y afrodescendientes, López afirma que “en este proceso los líderes y lideresas ganan elementos teóricos y prácticos, para la construcción de las realidades en sus territorios”.

López indica que esta cátedra gira alrededor de **tres ejes** el primero un **eje ambiente**, el territorio y  paz; el segundo **las identidades y género** y el tercero **la participación en el tema de los derechos**. Estos temas serán dado por docentes con títulos universitarios y otros que “tienen una práctica formal o informal de la educación propia de estas comunidades”.

La integrante de Justicia y paz, afirma que **esta cátedra servirá para que las comunidades se cuestionen** “¿por qué la guerra?, ¿por qué la pobreza?, ¿por qué las condiciones de desigualdad?, ¿por qué un conflicto armado de tantos años?” y hace un llamado para que las entidades del gobierno, diferentes organizaciones y parte del sector privado ayuden en la construcción de estas 12 universidades “para la construcción de una paz realmente duradera”.
