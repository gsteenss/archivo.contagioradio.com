Title: FARC suspenden cese al fuego unilateral tras muerte de 26 guerrilleros
Date: 2015-05-22 10:12
Author: CtgAdm
Category: Nacional
Tags: Cese unilateral de fuego, Conversaciones de paz de la habana, FARC-EP, Frente Amplio por la PAz, Fuerzas Armadas Colombianas, Suspensión del cese al fuego unilateral FARC-EP
Slug: comunicado-de-las-farc-sobre-suspension-de-cese-al-fuego-unilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ivan-marquez-farc-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div class="itemIntroText">

###### Foto:Pulzo.com 

A través de un comunicado público, la guerrilla de las FARC afirma que suspende el cese unilateral que mantuvo durante 5 meses, desde el pasado 20 de Diciembre y que según las organizaciones veedoras como el Frente Amplio por la Paz, redujo las operaciones de guerra en más del 90% en todo el territorio nacional.

El comunicado emitido por las FARC-EP, respecto a la suspensión del cese unilateral al fuego, afirma que el cese es insostenible debido a las operaciones conjuntas de las FFMM y el bombardeo del Ejército Colombiano en Guapi que ha dejado 26 guerrilleros muertos. Según diversos analistas políticos, esto supondría un nuevo pulso militar y la escalada del conflicto armado.

#### **Comunicado:** 

"*No estaba en nuestra perspectiva la suspensión de la determinación del cese al fuego unilateral e indefinido proclamado el 20 de diciembre de 2014 como un gesto humanitario y de desescalamiento del conflicto, pero la incoherencia del gobierno Santos lo ha logrado, luego de 5 meses de ofensivas terrestres y aéreas contra nuestras estructuras en todo el país.*

</div>

<div class="itemFullText">

*Deploramos el ataque conjunto de la Fuerza Aérea, el ejército y la policía ejecutado en la madrugada del jueves, contra un campamento del 29 Frente de las FARC en Guapi (Cauca), en el que, según fuentes oficiales, resultaron asesinados 26 guerrilleros.*

*Nos duelen por igual las muertes de guerrilleros y soldados, hijos de un mismo pueblo y procedentes de familias pobres. Debemos parar este desangre.*

*Contra nuestra voluntad tenemos que proseguir el diálogo en medio de la confrontación. Aunque Santos anuncia que mantendrá la ofensiva, insistiremos en la necesidad de acordar cuanto antes, para la salud del proceso de paz y evitar nuevas victimizaciones, el cese bilateral de fuegos que con tanta insistencia han reclamado las mayorías nacionales.*

*Agradecemos la labor de seguimiento y verificación del cese unilateral que durante cinco meses realizaron el Frente Amplio por la Paz y el movimiento social y político de Colombia.*

***Secretariado del Estado Mayor Central de las FARC-EP"***

</div>
