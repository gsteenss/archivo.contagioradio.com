Title: Van 1071 colombianos indocumentados deportados de Venezuela según OCHA
Date: 2015-08-26 12:11
Category: El mundo, Entrevistas, Política
Tags: Cucuta, democracia, Frontera entre Colombia y Venezuela, gobierno de Nicolás Maduro, San Antonio del Táchira, Venezuela
Slug: van1071-colombianos-indocumentados-deportados-de-venezuela-segun-ocha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/COLOMBIA-VENEZUELA-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.elnuevoherald.com] 

<iframe src="http://www.ivoox.com/player_ek_7617947_2_1.html?data=mJuemZ6Ye46ZmKialZuJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmsLijJadmZaPp9Dg0NLPy8bSs9SfytPR0cjZscbi1cbR0diPqMbk0Nfhw8nTt4zYxpDjx9PJvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alejandro Silva,Radio del Sur] 

###### [26 Ago 2015] 

**Según cifras de la Oficina de las Naciones Unidas para la Coordinación de Asuntos Humanitarios, OCHA, 1071 colombianos han sido deportados por indocumentación**, entre ellos 241 menores de edad, más de 2000 han retornado a Colombia de forma espontánea. A pesar de la difícil situación de las personas deportadas y las denuncias sobre maltratos, las autoridades del vecino país afirman que se está cumpliendo con la entrega digna de las personas a las autoridades colombianas.

[![Cucuta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Cucuta.png){.aligncenter .size-full .wp-image-12829 width="531" height="678"}](https://archivo.contagioradio.com/van1071-colombianos-indocumentados-deportados-de-venezuela-segun-ocha/cucuta/)

[Alejandro Silva, periodista de Radio del Sur, asegura que la medida de cierre fronterizo fue decretada tras el ataque que sufrieron 3 militares de la Guardia Venezolana, como una estrategia de seguridad por parte del gobierno venezolano para combatir el contrabando en el marco de la guerra económica latente actualmente en este país. Vea también [Continúan medidas de combate al contrabando y el paramilitarismo en la frontera](https://archivo.contagioradio.com/gobierno-venezolano-continua-medidas-de-combate-al-paramilitarismo/)]

[No obstante la pretensión nunca ha sido atacar directamente a los colombianos residentes en Venezuela, antes bien, la intensión es emprender **acciones concretas que pongan fin a las estructuras paramilitares y de contrabando** que, en complicidad con la Guardia Venezolana, transportan víveres y gasolina, a través del puente internacional y de los caminos de trocha, para ser revendidos en territorio colombiano.]

[El periodista agrega que las casas que han sido destruidas están comprometidas en actividades ilegales con drogas, juegos de azar, alcohol, estas casas no son todas de las personas que han sido deportadas. ]

[Se espera que la reunión programada para el día de hoy en Cartagena, entre las cancillerías de Colombia y Venezuela junto con gobernaciones de Apure, Arauca y Suria, de luces concretas sobre cómo proceder para **transformar y reactivar las relaciones en las fronteras colombo-venezolanas**.]

[La posición del gobierno venezolano es no abrir la frontera hasta que se consolide un mecanismo real que ponga fin al contrabando y el paramilitarismo de las zonas fronterizas, entretanto, se compromete a adelantar **investigaciones sobre las responsabilidades de las instituciones estatales, como la Guardia, en el cobro de vacunas para el paso de productos por la frontera**.]
