Title: Homenaje a las más de 6.000 víctimas de la UP "Nunca más otro genocidio"
Date: 2019-10-07 17:33
Author: CtgAdm
Category: Memoria, Movilización
Tags: Familiares de víctimas genocidio UP, marcha, UP
Slug: up-nunca-mas-otro-genocidio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/UP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CAHUCOPANA] 

El próximo 10 de octubre está convocado un homenaje a las 6.000 víctimas del Genocidio contra la Unión Patriótica (UP). Sobrevivientes y familiares se reunirán para marchar bajo el lema “nunca más otro genocidio”. Además de este acto, está previsto que durante los días 11 y 12 se realicen conversatorios entorno a la memoria. (Le puede interesar: "[Luego de 25 años Corte IDH fallará sobre genocidio de la UP](https://archivo.contagioradio.com/luego-25-anos-corte-idh-fallara-genocidio-la-union-patriotica/)")

El genocidio de los miembros del partido político Unión Patriótica a manos de actores estatales y paramilitares fue una de las mayores tragedias del conflicto armado colombiano. Aún no se conoce el paradero de más de 500 víctimas. Por ello, la Corporación para la Defensa y Promoción de Derechos Humanos (REINICIAR) busca realizar jornadas contra el olvido así como acompañamiento de acciones jurídicas y políticas.

### Programación 

En esta edición, que supone el XIV Encuentro Nacional, se realizará una marcha que está prevista que se inicie a las 10:00 a.m. del jueves y que culmine en la Plaza de Bolívar. Para el 11 y 12 de octubre se han organizado varios paneles para reflexionar sobre las violaciones de estos Derechos Humanos así como analizar el tratamiento que se ha dado al exterminio de la Unión Patriótica. Los coloquios tendrán lugar en el Centro de Convenciones Gonzalo Jiménez de Quesada.

**Viernes, 11 de octubre**

10:45 a.m. "Los retos del caso UP ante la Corte Interamericana de Derechos Humanos"

2:30 p.m. "La Reparación integral de las Víctimas del Genocidio contra la UP ante el Sistema Interamericano"

**Sábado, 12 de octubre: "El reencuentro**

8:30 a.m. Informe de la Corporación Reiniciar a las Coordinaciones

10:30 a.m. El Reencuentro de la Esperanza. Evento realizado por las 26 Coordinaciones departamentales de víctimas y familiares del Genocidio contra la UP.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
