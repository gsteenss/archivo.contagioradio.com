Title: CIDH evidencia grave crisis de Derechos humanos en México
Date: 2015-10-05 15:21
Category: El mundo, Judicial
Tags: 43 desaparecidos Ayotzinapa, CIDH, Derechos Humanos, Enrique Peña Nieto, Informe Preliminar CIDH, normalistas méxico, Radio derechos Humanos, Tortura México, Violación a derechos humanos en México
Slug: cidh-evidencia-grave-crisis-de-derechos-humanos-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/México-violencia-desaparecidos-EFE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa.com 

<iframe src="http://www.ivoox.com/player_ek_8804633_2_1.html?data=mZ2dlpuXd46ZmKiak5iJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh6q4qZDS2M7Iqc%2FXysaYydfFusafxNfW1c7Xb8XZjKnS1MrHrNDnjM3iz8bSs9SfxtOYr4qnd4a1mt2ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Héctor Cerezo, Comité Cerezo de DDHH] 

###### [5 oct 2015] 

La **Comisión Interamericana de Derechos Humanos** (CIDH) que llegó a México el pasado 2 de octubre constató en su informe final que el país atraviesa una “**grave crisis de derechos humanos**”, en donde la desaparición forzada, la impunidad y la tortura no son "hechos aislados".

La comisión encabezada por la presidenta Rose-Maire Belle Antoine, hizo un llamado a que el Estado **esclarezca de forma urgente lo ocurrido con los 43 desaparecidos** de Ayotzinapa, además de “reorientar la investigación a fin de seguir las líneas que surgen del informe del GIEI, que difieren de la hipótesis bajo las cuales se han realizado las investigaciones de la PGR”.

Héctor Cerezo, miembro del comité Cerezo de Derechos Humanos, indica sobre este informe que “se debe tener una conciencia fundamental sobre este*”,  *dado a la situación crítica en materia de derechos humanos que presenta *“*prácticas sistemáticas de violación, ocultamiento e impunidad”.

El informe que fue realizado con el **apoyo de organizaciones de derechos humanos, quienes presentaron sus denuncias**, muchas de ellas fuera de los registros oficiales, también plantea un “ataque generalizado contra defensores y defensoras de derechos humanos" , que además coloca en debate que “no existe un marco jurídico para que las personas puedan continuar sus procesos”.

Es preocupante la actitud del gobierno que “en lugar de tomar las observaciones y generar políticas, lo primero que hace es descalificar el informe, diciendo que efectivamente lo que vio la CIDH son hechos aislados", indica Cerezo.

Este informe preliminar de la CIDH presenta **retos para las organizaciones de DD.HH** al documentar los casos de desapariciones forzadas, casos de impunidad, de tortura y de violencia en México ya que “muchas de las organizaciones que presentaron denuncias en los estados de la República no cuentan con seguridad para seguir haciendo su trabajo".

El Estado mexicano indicó que continuará con el sistema interamericano de derechos humanos, pero pidió que "el análisis de la CIDH se realice con la objetividad y seriedad que requiere", afirmaron que esperan el informe definitivo de la comisión de la CIDH, en el primer semestre del 2016.
