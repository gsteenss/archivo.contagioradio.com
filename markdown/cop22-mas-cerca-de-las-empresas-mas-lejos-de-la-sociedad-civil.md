Title: "COP22: más cerca de las empresas, más lejos de la sociedad civil"
Date: 2016-11-16 13:55
Category: Ambiente, Voces de la Tierra
Tags: Acuerdos COP 21, COP 22, gases efecto invernadero
Slug: cop22-mas-cerca-de-las-empresas-mas-lejos-de-la-sociedad-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/COP22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Msecnd.net] 

###### [16 Nov 2016] 

Distintas organizaciones ecologistas del mundo señalan que la COP22 es una “feria empresarial de lavado verde" y denuncian un retroceso en las **sanciones a grandes empresas quienes generan la mayor cantidad de Gases Efecto Invernadero,** además dejan de lado las propuestas **que apuntan a un verdadero cambio en el modelo de desarrollo.**

Al referirse al “lavado verde”, Ecologistas en Acción afirma que la misión de las grandes empresas extractivas es** “vender tecnología verde"** a gobiernos y multinacionales, que cada día afectan más a poblaciones vulnerables del mundo, **negándoles la posibilidad a una vida digna en entornos saludables.**

El lema oficial de la cumbre “pasar de las palabras a la acción" contrasta con una política que apunta al estancamiento del Acuerdo de París, es decir, **demorar acciones contundentes lo suficiente para que sea imposible aplicar las recomendaciones científicas** y limitar el calentamiento global a 1,5ºC.

### **Multinacionales las beneficiadas** 

Defensores ambientales reiteran que “la presencia de multinacionales en estas cumbres supone un constante **bloqueo a iniciativas ciudadanas y ecologistas, así como la configuración de la percepción social del cambio climático”. **Le puede interesar: [Ecologistas en acción cuestiona acuerdo de parís en el día de la tierra.](https://archivo.contagioradio.com/dia-tierra-acuerdo-paris/)

Fue evidente en el desarrollo de la COP22 que hubo una sobre-representación de las **industrias fósiles, energéticas, nucleares, petroleras y constructoras, cuyo número de representantes  triplicó al de participantes de la sociedad civil.**

Ecologistas en Acción asegura que históricamente estas cumbres han servido como “una eficaz coartada para aquellos agentes que, lejos de cambiar sus acciones socio-climáticas y reconocer sus errores históricos, **siguen presentándose como salvadoras de una situación de la que ellos mismos son responsables”.**

La organización ambientalista **exige la retirada del estatus de observador a todas las empresas petroleras acreditadas dentro de las COP**, “como ya lo hizo en 2007 la Organización Mundial de la Salud con las tabacaleras.

"Creemos que la visión lucrativa del cambio climático perpetúa un modelo socioeconómico obsoleto que **provoca atentados sociales y ambientales muchas veces irreversibles”.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
