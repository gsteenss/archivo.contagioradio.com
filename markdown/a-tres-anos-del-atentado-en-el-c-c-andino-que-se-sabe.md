Title: A tres años del atentado en el C.C Andino ¿qué se sabe?
Date: 2020-06-16 20:04
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: Atentado andino, Atentados en Bogotá, Derechos Humanos, Fiscalía General de la Nación, montaje judicial
Slug: a-tres-anos-del-atentado-en-el-c-c-andino-que-se-sabe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/tmb1_734144_20170617234410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: La Gaceta Argentina {#foto-de-la-gaceta-argentina .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este **17 de junio** se cumplen tres años del atentado en el C.C Andino, hecho que continúa plagado de incertidumbre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diez de las once personas capturadas por este acto afirman ser víctimas de un montaje judicial y recientemente salieron de la cárcel por vencimiento de términos, sin que la Fiscalía haya demostrado su participación en esas acciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En este **Expreso Libertad**, hablamos con la abogada Luisa Niño, defensora de derechos humanos y representante de dos de las personas señaladas por la Fiscalía, quien relata cómo se ha tejido un conjunto de arbitrariedades en estos tres años de proceso, que no solo han vulnerado los derechos humanos de las personas, sino que evidencian una ausencia total de justicia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el programa también está la voz de César Barrera, padre de César, víctima de este montaje judicial, que salió el pasado viernes 5 de junio de la cárcel. Para él, estos tres años han significados múltiples afectaciones a su familia que van desde amenazas en contra de sus familiares, hasta el exilio por el temor de otros montajes en contra de sus allegados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Caso Andino, un proceso cargado de injusticias y dilaciones](https://archivo.contagioradio.com/caso-andino-un-proceso-cargado-de-injusticias-y-dilaciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tanto Niño como César, aseguran que están a la espera de que finalmente las personas sindicadas por la Fiscalía salgan de este proceso como inocentes, y que tanto los entes acusadores cómo el país, obtengan verdad en torno a los verdaderos responsables de este atentado.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/351864155783011/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/351864155783011/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Vea mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
