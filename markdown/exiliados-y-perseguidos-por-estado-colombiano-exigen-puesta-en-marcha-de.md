Title: Exiliados y perseguidos por Estado Colombiano exigen comisiones de verdad sobre paramilitarismo
Date: 2016-11-15 22:13
Category: DDHH, yoreporto
Tags: #AcuerdosYA, exiliados colombianos, paramiliatares
Slug: exiliados-y-perseguidos-por-estado-colombiano-exigen-puesta-en-marcha-de
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Jorge-en-La-Habana-e1479261770556.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Constituyentes Exiliados políticos perseguidos por el Estado Colombiano] 

###### [[16 Nov 2016]  
[***Eliécer***]

[La organización de “Constituyentes Exiliados políticos perseguidos por el Estado Colombiano” por intermedio de Jorge[Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Florián, en entrevista desde Bilbao, España, para **Contagio Radio**, exigieron la inmediata **puesta en funcionamiento y participación de las comisiones de la verdad y esclarecimiento del fenómeno paramilitar en Colombia** para exponer casos emblemáticos del exilio político y denunció ,que por el auge del [paramilitarismo]{.m_314135268022236672m_-502582965875610294hiddenspellerror} aún no hay garantías de seguridad por parte del gobierno de este país suramericano para retornar a Colombia, a pesar de los acuerdos firmados entre el presidente  Juan Manuel Santos y la insurgencia de las [FARC-EP.]{.m_314135268022236672m_-502582965875610294hiddenspellerror}]{lang="ES-CO"}

[Además, la organización anunció que el próximo 26 de noviembre **los exiliados políticos se reunirán en Madrid, España con el objetivo de preparar lo que será la participación de los mismos en la implementación de los acuerdos** reajustados próximos a salir de la mesa de negociaciones en La Habana, Cuba, entre el gobierno y las Farc-Ep, así como su posible interlocución como víctimas del Estado en la mesa de diálogos entre el gobierno y el [ELN]{.m_314135268022236672m_-502582965875610294hiddenspellerror} en Quito, Ecuador.]{lang="ES-CO"}

### [  
]

 [Jorge [Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Florián, hijo del profesor universitario Jorge [Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Romero, secuestrado por paramilitares en la ciudad de Barranquilla, al norte de Colombia el 28 de agosto del 2001 y posteriormente asesinado, es uno de los voceros de “Constituyentes exiliados políticos perseguidos por el Estado Colombiano” plataforma conformada a mediados de noviembre de 2014 en Bilbao, Estado Español y que reúne más de **150 organizaciones de exiliados en Europa y América Latina.** ]{lang="ES-CO"}

[Por otro lado, Freytter estuvo recientemente en La Habana, Cuba junto con una treintena de víctimas, dirigentes políticos y sociales que respaldaron el sí en el pasado plebiscito, Freytter dijo que “el balance del encuentro con la delegación de las [FARC-EP]{.m_314135268022236672m_-502582965875610294hiddenspellerror} fue muy positiva ya que se analizó la forma como llegar a esa masa de población que se abstuvo de participar en el plebiscito y además pedir una reunión urgente de los comités del Sí, las víctimas y los exiliados políticos residentes en el exterior con el presidente Juan Manuel Santos antes que este vaya a recoger el premio nobel de paz en [Oslo]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Noruega”.]{lang="ES-CO"}

### [  
]

[  
[Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Florián expresó que: “fue muy importante la presencia internacional de Constituyentes exiliados perseguidos por el Estado Colombiano, ya que pu**dimos explicar y entregar nuestro dossier y propuestas a la mesa de diálogo** y las delegaciones presentes, garantías de retorno digno, nuestro papel en la implementación de los acuerdos, así como ratificar las denuncias sobre la [revictimización]{.m_314135268022236672m_-502582965875610294hiddenspellerror} a los exiliados en el exterior con la Operación Europa”.]{lang="ES-CO"}

[Igualmente con referencia al retorno de los exiliados, perseguidos políticos por el Estado colombiano el dirigente dijo que “es un tema a discutir próximamente ya que **se requiere seguridad, verdad, justicia, reparación y garantías de no repetición** ya que como sujetos políticos y sociales participamos en la construcción de la paz y la solidaridad con el movimiento popular en Colombia.”]{lang="ES-CO"}

### **[No hay garantías para el retorno, los paramilitares siguen matando en Colombia]{lang="ES-CO"}**

[  
Preguntado sobre si en estos momentos existen garantías para el retorno de los exiliados políticos perseguidos por el Estado colombiano, Freytter respondió enfáticamente que “**en estos momentos no existe una verdadera garantía por parte del Estado colombiano** quien no reconoce el exilio político como víctimas del terrorismo de Estado y además, está claro que las **estructuras paramilitares se encuentran vigentes operativa, económica y militarmente** en las cinco regiones de nuestro país y es un tema que el gobierno no quiere discutir más cuando se sigue [revictimizando]{.m_314135268022236672m_-502582965875610294hiddenspellerror} a la población, desplazando, persiguiendo, asesinando a los líderes y a los que quieran regresar al país, ante lo cual estamos en la idea de elaborar un plan estratégico instando a las [Farc-ep]{.m_314135268022236672m_-502582965875610294hiddenspellerror} y al gobierno colombiano pero también a la mesa de conversaciones que se abre con el [ELN]{.m_314135268022236672m_-502582965875610294hiddenspellerror} para que el exilio político tenga una protección no solo nacional a su regreso sino internacional que debe traducirse en que los exiliados que voluntariamente deseen regresar al país no pierdan su estatus de exiliado o refugiado durante un tiempo determinado ya que si son nuevamente perseguidos por el Estado y el [paramilitarismo]{.m_314135268022236672m_-502582965875610294hiddenspellerror} o no logran reintegrarse a la sociedad que dejaron hace mucho tiempo puedan regresar a los países de acogida”.]{lang="ES-CO"}

### [  
]

### 

[Jorge [Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} Florián insistió en hacer claridad en torno al debate de las víctimas [“]{.m_314135268022236672m_-502582965875610294hiddengrammarerror}ya que el gobierno colombiano [insiste ]{.m_314135268022236672m_-502582965875610294hiddengrammarerror} en [instrumentalizar]{.m_314135268022236672m_-502582965875610294hiddenspellerror} y globalizar todas las víctimas y hacemos énfasis que nosotros somos víctimas perseguidas por el terrorismo de Estado y su engendro el [paramilitarismo]{.m_314135268022236672m_-502582965875610294hiddenspellerror} y ejemplo de ello en el exterior está la **“Operación Europa” en el marco de la seguridad nacional en los gobiernos de Álvaro Uribe Vélez, que buscó estigmatizar, perseguir, [descategorizar]{.m_314135268022236672m_-502582965875610294hiddenspellerror} el exilio político colombiano** el cual no puedo ser asesinado con el objetivo de ser aislado de la esfera política dando como resultado procesos judiciales, falsos positivos, [hackeo]{.m_314135268022236672m_-502582965875610294hiddenspellerror} de cuentas de correos, chuzadas telefónicas etc., aquí en Europa y otros continentes".]{lang="ES-CO"}

[A su ves Jorge agrego "en eso nos diferenciamos ya que los medios de comunicación nacional e internacional, usurpados por el gobierno, focalizan las llamadas víctimas de la insurgencia pero no focalizan las víctimas del terrorismo de Estado y sobre todo el reconocimiento del exilio político por lo que sería bueno que los medios hicieran un seguimiento a las víctimas del Estado en el exilio y su [revictimización. ]{.m_314135268022236672m_-502582965875610294hiddenspellerror}]{lang="ES-CO"}**[Somos víctimas del establecimiento colombiano y ese Estado se tiene que comprometer a reparar y decir la verdad de lo que ha pasado con el exilio colombiano".]{lang="ES-CO"}**

### [  
]

[  
Frente al papel que deben jugar los exiliados políticos, Freytter afirmó "consideró importante el rol que jugaran los constituyentes exiliados perseguidos políticos del Estado colombiano en la implementación de los acuerdos ya que **hay personas interesantes con gran conocimiento de la política colombiana y sus regiones donde es lo clave para el futuro."**]{lang="ES-CO"}

[  
Así mismo reclamó para Constituyentes exiliados políticos perseguidos por el Estado colombiano, **un asiento en la comisión de la verdad y otro en la comisión de esclarecimiento del fenómeno del** [**paramilitarismo**,]{.m_314135268022236672m_-502582965875610294hiddenspellerror} donde es menester exponer una serie de casos emblemáticos del exilio político para que sean estudiados y reconocidos por las comisiones que deben estar en marcha ya, que deben constituirse ya porque quedó plasmado en los acuerdos de Cartagena y "donde el gobierno colombiano no ha puesto los instrumentos necesarios para constituir dichas comisiones que son sumamente importantes y donde los exiliados vamos a aportar una agenda que es de máximo interés para nosotros las victimas que somos el centro de los acuerdos" señaló.]{lang="ES-CO"}

[  
]{lang="ES-CO"}**[26 de Noviembre encuentro de Constituyentes exiliados políticos perseguidos por el Estado colombiano.]{lang="ES-CO"}**

**[  
]{lang="ES-CO"}**[Finalmente Jorge [Freytter]{.m_314135268022236672m_-502582965875610294hiddenspellerror} confirmó que el próximo sábado 26 de noviembre en Madrid, España, los Constituyentes exiliados políticos perseguidos por el Estado Colombiano que residen en Europa, se estarán reuniendo con carácter extraordinario con el objetivo de definir estrategias y organizar su participación en la implementación de los acuerdos pedir una reunión con la comisión de paz y el gobierno colombiano.]{lang="ES-CO"}
