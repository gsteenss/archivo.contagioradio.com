Title: Responsabilidad divida en hechos de Uribe, Meta
Date: 2016-07-11 16:06
Category: Nacional, Paz
Tags: cese bilateral, Meta, Proceso de paz en Colombia, uribe
Slug: responsabilidad-divida-en-hechos-de-uribe-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ivan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: doblellave.com] 

###### [11 Julio 2016 ] 

A través de un comunicado el jefe de la Delegación de Paz Humberto de la Calle explicó que los hechos sucedidos el 8 de Julio en el municipio de Uribe, Meta fueron una **falta de coordinación por parte de la guerrilla**, mientras que las FARC exigen que se **aclaren los acontecimientos y la presencia de la patrulla militar en la zona**.

De la Calle afirma que los sucesos fueron "**un error por parte de las FARC**, al fijar unas coordenadas equivocadas, distintas al lugar en donde debían operar las garantías de seguridad en la Uribe", acción que generó que se diera el combate. Aún no se conoce un comunicado oficial por parte del Ministerio de Defensa que explique bajo qué orden se dio el ataque. Por otro lado, Iván Márquez aseguro en su cuenta de Twitter que "**No fue un error porque el ataque se produjo con la certeza que las Farc se encontraban en el sitio con una bandera blanca como señalización"**

De acuerdo con Jaime Caycedo, integrante del Partido Comunista y de la Unión Patriótica, - "este es un incidente lamentable que pone a prueba lo que se ha venido acordado en la Habana, sin embargo creo yo que no es correcto la posición del doctor de la Calle que descarga toda la responsabilidad en las FARC, pueden haber imprecisiones en las coordenadas, pero eso no explica **por qué se da el incidente en donde son tropas del Ejercito Nacional las que actúan en contra de unos guerrilleros** desarmados que están desarrollando un programa previamente convenido con el gobierno".

De otro lado, Caycedo afirmó que es necesario que la **fuerza pública se repliegue para que se de el cumplimiento de los acuerdos aprobados bilateralmente**, "el ejercito se esta aprovechando de la situación del cese al fuego para coptar nuevo espacios y territorios,estas acciones contradicen el espíritu y letras de los[acuerdos establecidos de un cese al fueg](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/)o que implica que hay necesidad de que paren los operativos de guerra por parte y parte de los actores".

<iframe src="http://co.ivoox.com/es/player_ej_12189640_2_1.html?data=kpeemp6aeJGhhpywj5WcaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncavVytLSjajFvcTZxdSSlKiPmbGhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
