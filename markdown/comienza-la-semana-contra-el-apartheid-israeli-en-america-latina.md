Title: Comienza la Semana Contra el Apartheid Israelí en América Latina
Date: 2020-04-16 15:36
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: América Latina, Apartheid, Apartheid Israel, Semana contra el apartheid
Slug: comienza-la-semana-contra-el-apartheid-israeli-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image-2.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image-3.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/06341351_xl.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: HispanTv {#foto-de-hispantv .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: Saaid Jamis Tovar

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hoy inicia nuevamente la Semana Contra el Apartheid Israelí. Este es un esfuerzo mancomunado de diferentes colectivos de **BDS** en Latinoamérica, que ante la crisis generada por la pandemia del Covid-19 y el deficiente sistema de salud neoliberal de los Estados, unificaron esfuerzos para realizar estos dos conversatorios virtuales enfocados  en la militarización, debido a las políticas que ejercieron diversos gobiernos y a los lazos comerciales de Israel en esta materia, siendo uno de los principales socios armamentistas en Latinoamérica pese a las violaciones a los derechos humanos. Por esta razón el **Movimiento BDS** impulsa a nivel mundial un embargo militar a Israel.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83178,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image-2.png){.wp-image-83178}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Los conversatorios programados para el día de hoy y el próximo 23 de abril, abordan las temáticas de **“*¿Cómo sobrevivir al confinamiento? Lecciones desde Gaza”* y *“Militarismo, lugares de encierro y control poblacional en Palestina y América Latina.*** Estos conversatorios serán transmitidos en el perfil de [Facebook Movimiento BDS – Boicot, Desinversiones y Sanciones](https://www.facebook.com/bdscastellano) y tendrán participación de invitadas palestinas y de América Latina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primer conversatorio se contará con la participación de **Jaldía Abubakra**, es nacida en Gaza y actualmente participa en Izquierda Unida, BDS Madrid, Unadikum y Alkarama movimiento de mujeres palestinas. Adicionalmente se contará con la participación de **Nada Hussien** coordinadora del PACBI (Campaña Palestina de Boicot Académico y Cultural de Israel) en Palestina y el Mundo Árabe.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83179,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image-3.png){.wp-image-83179}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Para el día 23 de abril se contará con las invitadas **Budour Hassan** (Palestina), **Rosa Herrera** (Argentina), **Sofia Alvarado** (Chile), **Aracely Cortés-Galán** (México).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Isabel Rikkers** de integrante de **BDS Colombia**, manifestó que “Es una oportunidad para que las personas se acerquen a la causa palestina, desde los ámbitos que ya habitan y las capacidades que tienen. Si eres artista, la Semana Contra el Apartheid es una oportunidad para aportar a la causa palestina. Si eres estudiante también puedes aportar desde la academia. No es para hablar solamente de que está sucediendo en Palestina, sino para conmemorar que compartimos luchas muy urgentes y justas”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/bdscastellano/videos/159471522051455/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/bdscastellano/videos/159471522051455/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Le puede interesar:[Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
