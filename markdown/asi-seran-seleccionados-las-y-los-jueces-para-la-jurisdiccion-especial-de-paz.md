Title: Así serán seleccionados las y los jueces para la Jurisdicción Especial de Paz
Date: 2016-08-12 07:53
Category: Nacional, Paz
Tags: FARC, Gobierno, jurisdicción especial para la paz, proceso de paz
Slug: asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/dialogos_de_paz_-_afp_0-e1471006222803.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [12 Ago 2016] 

Esta mañana las delegaciones de paz del gobierno y las FARC, anunciaron cómo será el mecanismo de selección de los magistrados de la Jurisdicción Especial para La Paz. En ese sentido el **Papa Francisco, el Secretario General de las Naciones Unidas, la Sala Penal de la Corte Suprema de Justicia, la delegación en Colombia del Centro Internacional de Justicia Transicional (ICTJ), la Comisión Permanente del Sistema Universitario del Estado** serán quienes elijan a los jueces que compondrán la JEP, teniendo en cuenta las más altas calidades éticas, y reconocidas trayectorias profesionales.

### Aquí el comunicado completo 

Comunicado conjunto \#88, La Habana, Cuba, 12 de agosto de 2016

El Gobierno Nacional y las FARC-EP informamos que en el marco de lo establecido en el acuerdo de la Jurisdicción Especial para la Paz hemos llegado a un acuerdo sobre criterios y el mecanismo de selección de los magistrados de la Jurisdicción Especial para la Paz así como del Director de la Unidad de Investigación y Acusación:

### Mecanismo de Selección: 

Comité de Escogencia: El Gobierno Nacional y las FARC-EP hemos acordado proponer a las siguientes instituciones y personas, que cada una de ellas, si decidieran aceptar esta solicitud, escojan una persona para conformar el mecanismo de selección mencionado en el numeral 68 del acuerdo de creación de la Jurisdicción Especial para la Paz de 15 de diciembre de 2015:

Su Santidad el Papa Francisco  
El Secretario General de las Naciones Unidas  
La Sala Penal de la Corte Suprema de Justicia  
La delegación en Colombia del Centro Internacional de Justicia Transicional (ICTJ).  
La Comisión Permanente del Sistema Universitario del Estado.  
Los integrantes del comité deberán ser personas de altas calidades éticas, y reconocidas trayectorias profesionales.

Los integrantes del comité seleccionarán a los Magistrados por mayoría de 4/5, con un sistema de votación que promueva el consenso.

El comité de escogencia gozará de toda la autonomía e independencia para que pueda cumplir sus funciones de manera imparcial. Las partes acordarán un reglamento de composición y funcionamiento del comité de escogencia, que además regulará los mecanismos de postulación y elección, mecanismos que contarán con la debida publicidad y garantías de transparencia de manera que facilite el seguimiento y veeduría por parte de la sociedad y que permita recibir comentarios y opiniones de personas y organizaciones sobre los candidatos.

### Criterios de selección: 

En el acuerdo sobre el punto 5 “Víctimas” de la Agenda del Acuerdo General, numerales 65 y 66 de la JEP, fueron acordados los siguientes criterios para la selección de los Magistrados del Tribunal y de las Salas:

Todos los Magistrados “deberán estar altamente calificados y deberá incluirse expertos en distintas ramas del Derecho, con énfasis en conocimiento del DIH, Derechos Humanos o resolución de conflictos”.

El Tribunal y cada Sala “deberá ser conformado con criterios de equidad de género y respeto a la diversidad étnica y cultural, y será elegido mediante un proceso de selección que de confianza a la sociedad colombiana y a los distintos sectores que la conforman”.

“Para ser elegido Magistrado del Tribunal para la Paz deberán reunirse los mismos requisitos que para ser magistrado de la Corte Constitucional, de la Corte Suprema o del Consejo de Estado de Colombia, salvo la nacionalidad colombiana para los extranjeros. En ningún caso se aplicará un sistema de carrera”.

“Para ser elegido Magistrado de Sala deberán reunirse los mismos requisitos que para ser magistrado de Tribunal Superior de distrito judicial. En ningún caso se aplicará un sistema de carrera”.

En el proceso de selección se tendrán en cuenta los estándares internacionales de independencia judicial y las altas calidades morales de los candidatos, así como el dominio del idioma español.

De acuerdo con lo establecido en el acuerdo de la Jurisdicción Especial para la Paz, el comité de escogencia deberá elegir siguiendo los criterios establecidos en la JEP:

20 magistrados colombianos y 4 extranjeros para el Tribunal para la Paz  
18 magistrados con nacionalidad colombiana y 6 magistrados extranjeros para las 3 Salas de Justicia  
Una lista adicional de 19 nacionales y 5 extranjeros para el Tribunal para la Paz y las Salas de Justicia en caso de que se requiera aumentar el número de magistrados o sustituir a alguno de sus miembros.

#### El Presidente o Presidenta 

El Director o Directora de la Unidad de Investigación y Acusación que tendrá plena autonomía para seleccionar a los profesionales que harán parte de la misma

Los procesos de escongencia deberán concluir lo más pronto posible después de la entrada en vigor del Acuerdo Final. En todo caso se priorizará la elección de los magistrados o magistradas de la Sala de Amnistía y de la Sala de Definición de las Situaciones Jurídicas.

### ¿Cómo funcionará la jurisdicción Especial para la Paz?. 

> [\#HablemosDePaz](https://twitter.com/hashtag/HablemosDePaz?src=hash) ¿Y cómo funcionará la jurisdicción Especial para la Paz?. [@ensanro](https://twitter.com/ensanro) responde [pic.twitter.com/02UgsrMjXn](https://t.co/02UgsrMjXn)
>
> — Contagio Radio (@Contagioradio1) [12 de agosto de 2016](https://twitter.com/Contagioradio1/status/764093087098347520)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
