Title: El diálogo: un primer paso para construir la verdad en Colombia
Date: 2019-05-29 17:36
Author: CtgAdm
Category: Nacional, Paz
Tags: AUC, comision de la verdad, comunidades indígenas, Fuerzas militares, Gobierno, lideres sociales
Slug: el-dialogo-un-primer-paso-para-construir-la-verdad-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: @ComisionVerdadC] 

Con diferentes perspectivas, diplomáticos líderes sociales e indígenas, antiguos integrantes de actores armados, integrantes del Gobierno, miembros retirados del Ejército, víctimas y empresarios se congregaron alrededor de un diálogo que tuvo como objetivo destacar la importancia de narrar la verdad como parte del proceso de reparación suscrito en el Acuerdo de Paz.

El padre Francisco de Roux, presidente de la Comisión de la Verdad, hizo un llamado a dejar las ideologías de lado, y lenguaje políticamente correcto y dejar de acudir a argumentos de autoridad cuando se trata de hablar sobre esclarecer lo que sucedió en el marco del conflicto armado, **"estamos atrapados en el miedo, debemos ponernos por encima de eso y estar a disposición de las víctimas"**,  declaró.

**Rodrigo Londoño, máximo dirigente del partido FARC**, confesó que no fue fácil la decisión de aceptar la invitación al foro pero admitió que muchas veces, el miedo que existe a contar la verdad está al mirar hacia atrás y revisar los actos de violencia que cometió el grupo guerrillero cuyos resultados "eran contrarios a lo que buscaban".

**Luis Eduardo Cifuentes antiguo integrante de las Autodefensas Unidas de Colombia (AUC)**, compartió con los asistentes su experiencia después de dejar las armas, sin embargo alertó que después de su desmovilizacion, "ni las víctimas han sido reparadas, ni los reinsertados han recibido una oportunidad, recoger armas es lo fácil, pero se trata de reintegrar a las personas a la sociedad".

**Aída Quilcué, líderesa indígena del Norte del Cauca** se refirió a la importancia del control territorial, y  destacó, al igual que lo hizo el fotógrafo **Jesús Abad Colorado** la importancia de la multiculturalidad en el país, ". Nos comprometemos a caminar en esa diversidad hacia la paz que necesitamos todos y todas las colombianas" afirmó Quilcué.

### Una verdad sin lenguajes ambiguos 

Por su parte el general (r) **Rafael Colón** insistió en la importancia de **"contar la verdad sin adornos"** y en la que no solo se incluyan los testimonios de los actores armados sino que también invite alcaldes y gobernadores del territorio colombiano para dialogar sobre los hechos de violencia que sucedieron durante el conflicto armado.

“Las verdades hay que contarlas como son. Hemos cargado con una cruz tan pesada que no hemos podido soltar: los falsos positivos. Esa cruz debemos haberla botado desde hace mucho tiempo contando la verdad sin tapujos", señaló Colón.

La declaración del general fue respaldada por **Jorge Ballén, presidente del grupo empresarial Panaca** quien insistió en que el sector privado ha sido ajeno al Acuerdo final, por lo que debe comenzar a aportar, apostándole a la reintegración de aquellas personas que dejaron las armas.

### Las preguntas que surgen ante la actitud del Gobierno 

Algunos de los panelistas, incluidos el padre de Roux, le plantearon algunas preguntas al comisionado de Paz Miguel Ceballos sobre la completa implementación de los acuerdos, en particular sobre el avance en la restitución de tierras y un sentir de la población, que percibe en general un retorno al miedo y a la violencia.

El integrante del gobierno, respondió a las inquietudes y declaró que no se puede continuar dividiendo el país entre el sí y el no, apelando a una transición y superar el tema de la polarización.

“Sentimos que no hay una política de Estado a favor de la paz” declaró **Clemencia Carabalí,** lideresa social e integrante del Proceso de Comunidades Negras quien invitó a los presentes a sumar a la construcción de paz generando verdaderas acciones de cambio. [(Lea también: Comisión de la Verdad expresó su respaldo a líderes sociales del Cauca)](https://archivo.contagioradio.com/comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca/)

Al concluir el evento, las partes reconocieron la necesidad de tener confianza en el otro para avanzar en la construcción de verdad y en la importancia de poner por encima de todo la vida humana y  lo necesario que escuchar las diferentes voces que desde distintas orillas buscan cumplir con la firma del acuerdo.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
