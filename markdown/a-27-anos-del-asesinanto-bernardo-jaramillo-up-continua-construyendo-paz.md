Title: A 27 años del asesinato de Bernardo Jaramillo, UP continúa construyendo paz
Date: 2017-03-22 12:24
Category: DDHH, Nacional, Sin Olvido
Tags: Bernardo Jaramillo Ossa, Unión Patriótica
Slug: a-27-anos-del-asesinanto-bernardo-jaramillo-up-continua-construyendo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Unión-Patriótica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [22 Mar 2017] 

Luego de 27 años del asesinato del líder político, Bernardo Jaramillo, integrante de la Unión Patriótica, la Fiscalía General ordenó compulsar copias para investigar a 15 miembros de DAS que conformaban su esquema de seguridad. De acuerdo con Gabriel Becerra quién hace parte de la Unión Patriótica **este es tan solo un paso en la búsqueda de la verdad sobre el genocidio de la plataforma política.**

Gabriel Becerra, señaló que la decisión, tardía, por parte de la Fiscalía, “**es la expresión de la impunidad total frente al hecho del magnicidio de Bernardo Jaramillo y frente al genocidio de la Unión patriótica**” y producto del accionar en conjunto de diferentes sectores del país como los militares, los sectores económicos y de la clase política que han impedido que la justicia opere.

Jaramillo hace parte de los más de 3000 integrantes de la Unión Patriótica que fueron asesinados, sin embargo, **hasta el momento, no hay ninguna investigación o captura en materia intelectual, frente a quienes ordenaron el crimen,** ni quiénes son los responsables del genocidio de este partido político. Le puede interesar:["Presidente debió pedir perdón por víctimas de crímenes de Estado: Jaime Caicedo"](https://archivo.contagioradio.com/presidente-debio-haber-pedido-perdon-ante-el-pueblo-colombiano-por-victimas-de-crimenes-de-estado-jaime-caicedo/)

la Unión Patriótica recobró su personería Jurídica hace 3 años, no obstante, Becerra expresó que hace falta un proceso de reparación tanto política como a las víctimas que permita retomar la fuerza que tenía el partido y una transformación institucional que permita una apertura democrática “**el país de hoy continúa con los mismos elementos de carácter estructural de hace 30 años**”.

Frente al Acuerdo de paz y las posibilidades de generar un escenario con mayor representación que de paso a la participación de la Unión Patriótica, Becerra indicó que la firma del acuerdo ya significa un gran paso “**nadie dijo que iniciar esta nueva etapa historia va a ser fácil**, pero estamos asistiendo a un hecho importante producto de la lucha de muchos sectores revolucionarios”. Le puede interesar:["Así se perdieron los votos de la Unión Patriótica"](https://archivo.contagioradio.com/union-patriotica-ha-recuperado-mas-de-4-mil-votos-y-aspira-a-curul-en-concejo-de-bogota/)

“**Necesitamos la configuración de una nueva institucionalidad para el 2018, el Acuerdo de paz no solo debe expresarse en el Estatuto de la Oposición**, sino que se exprese en otras reglas de juego frente a financiación, medios de información que permitan la participación de otras fuerzas políticas” agrego Becerra.

<iframe id="audio_17710247" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17710247_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
