Title: Dictamen de Medicina Legal confirma que Miguel Ángel Barbosa fue asesinado
Date: 2016-07-08 12:53
Category: DDHH, Nacional
Tags: agresiones esmad, Miguel Ángel Barbosa, Movimiento estudiantil Colombia, Universidad Distrital
Slug: dictamen-de-medicina-legal-confirmaria-que-miguel-angel-barbosa-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-minga-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [8 jul 2016] 

Este viernes se conoció el dictamen de Medicina Legal que confirma que el estudiante de la Universidad Distrital murió tras una **falla neurológica causada por un golpe en la cabeza con objeto contundente**. El informe fue remitido a la Fiscalía que conformó un grupo especial de investigación para establecer las causas que rodearon el homicidio del joven.

Miguel Ángel Barbosa falleció el 3 de junio de este año, luego del estado de coma en el que lo había inducido el cuerpo médico del Hospital del Tunal, tras ser **agredido con una granada aturdidora que habrían lanzado miembros del ESMAD**, durante la arremetida a la [[toma cultural de la Sede Tecnológica](https://archivo.contagioradio.com/fallecio-estudiante-udistrital-agredido-por-el-esmad/)] del pasado 21 de abril.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
