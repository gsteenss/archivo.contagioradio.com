Title: Ahora resulta que los dan por desaparecidos
Date: 2015-11-06 13:46
Category: Nicole, Opinion
Tags: acuerdos de paz, Armando Arias Cabrales, FARC, Jesús Orlando Ospitia Garzón, Palacio de Justicia
Slug: ahora-resulta-que-los-dan-por-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/image_content_24532619_20151020102942.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elcolombiano.com 

**Por [Nicole Jullian](https://archivo.contagioradio.com/nicole-jullian/)  **

###### [6 Nov 2015] 

Hoy se cumplen 30 años sin que se sepa a cabalidad lo ocurrido con los desaparecidos y las desaparecidas del palacio de justicia. Son 30 años de olvido político, 30 años de silencio estratégico y 30 años de negacionismo institucional. Respecto a este punto no hay ningún indicio de que la cosa vaya para mejor, aun así los más optimistas digan que estamos ad portas de la paz. Digo esto pues según lo acordado en La Habana el pasado 18 de octubre en la sesión número 62 respecto a la búsqueda de personas desaparecidas, ahora resulta que en Colombia a las personas las dan por desaparecidas.

Gobierno y FARC llegaron en esa sesión a unos acuerdos (que se pondrían en marcha antes de la firma de los acuerdos) respecto a la búsqueda de personas desaparecidas dizque “con el fin de aliviar el sufrimiento de las familias de las personas dadas por desaparecidas y de esta manera contribuir a la satisfacción de sus derechos”. El acuerdo buscaría por una parte “poner en marcha unas primeras medidas inmediatas humanitarias de búsqueda, ubicación, identificación y entrega digna de restos de personas dadas por desaparecidas en el contexto y en razón del conflicto armado interno” y por otra parte crear “una Unidad especial para la búsqueda de personas dadas por desaparecidas en el contexto y en razón del conflicto armado.”

Pero perdón, un momento: ¿cómo así que ahora debemos aceptar que se hable de personas dadas por desaparecidas en el contexto y en razón del conflicto armado interno? ¿Cómo es posible que después de la ardua lucha que significó combatir un aspecto esencial del negacionismo institucional, situación que permitió la incorporación de la desaparición forzada como delito en el código penal colombiano, de una día para otro las FARC doblegan su mano y aceptan semejante sinsentido? ¿Cómo poder creer que con este sinsentido se podrá aliviar el sufrimiento de los familiares de las personas víctimas de la desaparición forzada? El carácter pasivo con que el Gobierno y las FARC caracterizan al perpetuador o a los perpetuadores del delito de desaparición forzada, en tanto ahora se trata de personas que fueron dadas por desaparecidas, es la cosa más inadmisible que a mi juicio ha sido hasta ahora acordada por la mesa de negociaciones de La Habana.

En noviembre del 2014 tuve la oportunidad de entrevistar al Director de Fiscalías Nacionales, Jesús Orlando Ospitia Garzón, precisamente sobre el preocupante subregistro en torno a las cifras de desaparición forzada. Ospitia, al tratar de buscar razones que justificaran el caos en torno al manejo de las cifras de desaparición forzada, fue muy enfático al decirme: “lo que pasa es que usted no conoce la realidad del campo en Colombia, ahí la gente se emborracha, se desaparece y luego de unos días vuelve a aparecer”.

La apreciación de Ospitia respecto a que la gente en Colombia se desaparece, por más aberrante que ésta sea, no me sorprendió en lo más mínimo. Por el contrario, sí me sorprende de sobremanera que en el último acuerdo al que llegaron el Gobierno y las FARC ambas partes no hayan sido capaz de nombrar las cosas por su nombre. La connotación de que a las personas ahora resulta que las dan por desaparecidas, como una acto antojadizo de un alguien desconocido que se le ocurre de repente dar por desaparecida a una persona, ridiculiza el destino de cada una de estas personas y los hechos de violencia que llevaron a que a estas personas se les haya despojado de su derecho a existir.

En primer lugar, el delito está tipificado en el código penal como desaparición forzada.

En segundo lugar los mecanismos internacionales de protección de los derechos humanos han sido enfáticos al insistir que en esta política estratégica de ocultamiento y exterminio del que piensa distinto la participación del Estado (por acción u omisión) es innegable. Para los efectos de La Convención Internacional para la protección de todas las personas contra las desapariciones forzadas de las Naciones Unidas se debe entender por desaparición forzada "el arresto, la detención, el secuestro o cualquier otra forma de privación de libertad que sean obra de agentes del Estado o por personas o grupos de personas que actúan con la autorización, el apoyo o la aquiescencia del Estado, seguida de la negativa a reconocer dicha privación de libertad o del ocultamiento de la suerte o el paradero de la persona desaparecida, sustrayéndola a la protección de la ley. Cabe recordar que el gobierno de Colombia ratificó esta Convención.

La Fiscal Ángela Buitrago, quien fue la que adelantó la investigación y acusó por desaparición forzada al coronel (r) Alfonso Plazas Vega y a los generales (r) Jesús Armando Arias Cabrales e Iván Ramírez en relación a los hechos de violencia del 6 y 7 de noviembre de 1985, indicó en un seminario reciente sobre el rol de los archivos en la construcción de paz que el hecho de que aparezcan los cuerpos no soluciona en nada el problema, lo esencial es saber por qué murieron.

 El pasado ya pasó. El futuro, por el contrario, es incierto e indeterminado. De todos modos con el pasado aún podemos hacer algo: cambiarle el sentido, cambiarle el sentido para que exista la posibilidad de un nuevo futuro. La construcción de una paz honesta necesita de una dirigencia política renovada que esté en condiciones de querer cambiarle el sentido negacionista al pasado de Colombia. Para ello esta nueva dirigencia necesitará de un horizonte de país más amplio, de una lectura del pasado más profunda. Lo acordado entre las FARC y el Gobierno respecto a la búsqueda de personas desaparecidas demuestra elestrecho horizonte de los negociadores, demuestra que la lectura que se ha hecho sobre la desaparición forzada es liviana, denigrante e insensata. En ese sentido, me sumo a la opinión de William Ospina cuando dice que no cree “que un mero pacto entre élites guerreras, siendo tan necesario y tan útil, vaya a garantizarnos una paz verdadera.”

Hasta que la dignidad se vuelva costumbre!

[ ]

** **
