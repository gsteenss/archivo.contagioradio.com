Title: "ELN y Gobierno deben cumplir su palabra" Carlos Velandia
Date: 2016-10-28 17:38
Category: Entrevistas, Política
Tags: ELN, Mesa de diálogos Quito
Slug: eln-y-gobierno-deben-cumplir-su-palabra-carlos-velandia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [28 de Oct 2016] 

Apropósito de la no instalación de la mesa de diálogos y de su suspensión hasta nuevo aviso, el analista Carlos Velandia, quien a su vez es gestor de paz para el proceso de conversaciones entre el ELN y el Gobierno Nacional aseguró que es **“un muy mal precedente el aplazamiento de la mesa por incumplimientos que a su manera de ver no tienen una explicación al menos sólida para la opinión pública”**.

Para el gestor de paz esta situación se podría enderezar si el **ELN y el Gobierno aclaran qué ocurrió para que hechos como los sucedidos no vuelvan a pasar antes de sentarse a la mesa**, a su vez indicó que no puede ser el tiempo una tala, ni dar pie a la improvisación para que ambas partes logren llegar fortalecidas al instalación de la mesa.

Frente a los motivos de la cancelación de la instalación de la mesa, Juan Camilo Restrepo afirmó en un comunicado de prensa que es los **compromiso establecidos por los equipos del gobierno y el ELN** en la última ronda en Caracas, fueron precisos para ambas partes. Por lo que siempre se dejó claro que era necesaria la liberación efectiva del ex congresista Odín Sánchez para dar inicio a esta fase pública” y mencionó que sobre el medio día de ayer el Comité Internacional de la **Cruz Roja informó que ya se había dado inicio al proceso de liberación de Odín Sánchez.**

Mientras que Pablo Beltrán, miembro del ELN afirmó que en la cuenta de Twitter de la guerrilla que **“no estaban de acuerdo con la medida tomada por el gobierno”**. Estos actos significan para Carlos Velandia un corto circuito frente a como manejan la información ambas partes **“más allá que las voluntades son los hechos los que hablan y los que le dan solidez a las voluntades políticas y decisiones que se tomen"**.

Sin embargo, desde los resultados del pasado dos de octubre con el plebiscito, han surgido diferentes iniciativas ciudadanas que le están haciendo un llamado al gobierno y las insurgencias para que finalice el conflicto armado en el país, presión que para Velandia se transforma en una necesidad y prioridad para que se solventen los inconvenientes lo más pronto posible. Le puede interesar:["Organizaciones sociales piden cese bilateral para instalar mesa en Quito"](https://archivo.contagioradio.com/organizaciones-sociales-piden-cese-bilateral-instalar-mesa-quito/)

“**La palabra hay que hacerla valer, porque es a través de ella que se va a resolver el conflicto** y si tiene poco valor pues no tiene mucho sentido iniciar un proceso donde se arranca con una mala entrada a la mesa” afirmó el gestor de paz.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
