Title: 5.000 madres comunitarias realizan plantón en las sedes del ICBF
Date: 2015-06-12 16:00
Category: Movilización, Nacional
Tags: Aportes pensionales, Derechos Humanos, gobierno colombiano, ICBF, Madres Comunitarias, Olinda García
Slug: 5-000-madres-comunitarias-realizan-planton-en-las-sedes-del-icbf
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/madres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: noticiasunolaredindependiente.com 

##### <iframe src="http://www.ivoox.com/player_ek_4633252_2_1.html?data=lZuglZeZdo6ZmKiakpmJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9Pdz8nOjazFtsSZpJiSo6nFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Orinda García, Presidenta Sintracihobi.] 

###### [12 Jun 2015] 

Durante la mañana de este viernes, cerca de **5.000 madres comunitarias** se movilizaron frente a las sedes del **Instituto Colombiano de Bienestar Familiar** a nivel nacional, para exigir se reconozcan sus derechos, por medio del pago de las** prestaciones sociales y aportes pensionales **adquiridos tras 28 años de servicio a la institución.

Más del **50%** de las madres comunitarias del **ICBF** están a punto de pensionarse, la problemática está, en que el instituto **no les reconocerá sus aportes pensionales porque muchas de ellas no han cotizado la totalidad de semanas legales** que se requieren ante el estado para gozar de este derecho.

**Olinda García,** Presidenta del sindicato de trabajadoras al cuidado de la infancia y adolescencia del sistema nacional del **ICBF**, asegura que el sindicato instaurará una demanda frente al tema laboral y pensional, en vista de que el **ICBF** pretende sacarlas de allí por su edad y por las enfermedades terminales que muchas de ellas padecen.

Según la presidenta García “no se han cotizado las semanas suficientes porque no se crearon **políticas públicas** **claras** para que estuvieran afiliadas a un sistema de pensiones, la responsabilidad fue del gobierno que no estableció estas políticas cuando creó estos programas”.
