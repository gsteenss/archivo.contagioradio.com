Title: New Army directive could obstruct military members' contribution to truth
Date: 2019-08-22 13:15
Author: CtgAdm
Category: English
Tags: Army, extrajudicial killings, military, Special Peace Jurisdiction, Truth Commission
Slug: new-army-directive-could-obstruct-military-members-contribution-to-truth
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Ejército-Nicacio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Photo: @COMANDANTE\_EJC] 

 

The Spanish newspaper El País revealed Wednesday that new directives from the army commander General Nicacio Martínez are instructing military members to present a planned narrative of the armed conflict to the Special Peace Jurisdiction (JEP, by its Spanish initials) and the Truth Commission.

 

The document, titled Plan 002811 and dated March 13, 2019, has created concern over the influence it may have on the individual testimonies of military members who appear before these bodies of the transitional justice system. Some also worry that the instructions could outright deny or manipulate the facts behind alleged abuses committed by military members.

 

On its behalf, the Army said that the directive aims to “guide the position of the institution in spaces of truth and historical memory,” but for Alberto Yepes, of the Colombia-Europe-United States Coordination, the document attempts to “impose a version of the truth in an official manner” and to undermine the obligations of military members before the JEP and the Truth Commission.

 

The Commission, which began its three-year mandate late last year, relies on information from the Armed Forces that will be cross-examined and included in their final report that will be presented in 2021. Meanwhile, the war tribunal has taken on the cases of 2,000 military members who participated in the armed conflict — 99% of which are linked to extrajudicial killings or forced disappearances.

### **“Army would impose an official denial”**

 

The directive consists of three main guidelines. The first includes “counter-arguments” that “will serve as a guide for the contributions that members of the National Army may potentially make to the Truth Commission.”

 

The second guideline highlights the “emblematic cases” of human rights violations that the FARC guerrillas committed against the Army. The third concept underlines that the Army was also a victim of the armed conflict and plans to annex a list of good acts the Army took part in throughout the regions.

 

“The National Army must build and rebuild its truth about its origins, causes, development and impact in the armed conflict so that it may be possible to define identity traits in relation to its actions,” read one of the documents.

 

Yepes warned that the directive obstructs individuals from acknowledging the truth, which could be replaced by a “predesigned truth.” By laying out these guidelines, it also seems that there exists a “profound fear of the truth,” Yepes added.

 

The Ministry of Defense defended its directive amid the controversy, claiming that the previous military leadership designed the instructions in November 2018 after the former army commander General Alberto Mejía spoke with the president of the Truth Commission Father Francisco de Roux.

 

The Colombian Army has been at the center of various controversies this year over some of its decisions, including an order signed by General Martínez that calls for an increase in combat kills and most recently, reports of corruption within the institution.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
