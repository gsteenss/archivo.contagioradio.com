Title: Comunidades expresan respaldo a propuestas de Defendamos La Paz
Date: 2019-06-22 10:54
Author: CtgAdm
Category: Comunidad, Nacional
Tags: acuerdo de paz, Defendamos la Paz, Ejercito de Liberación Nacional
Slug: comunidades-expresan-respaldo-a-propuestas-de-defendamos-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Twitter:@IvanCepedaCast ] 

En una carta abierta, comunidades afectadas por el conflicto armado respaldaron las 13 propuestas que presentó la plataforma Defendamos La Paz para avanzar la implementación del Acuerdo de Paz, entre ellos reabrir **el diálogo con el Ejército de Liberación Nacional** (ELN) y **dar fin a los asesinatos de líderes y lideresas sociales**.

Según el documento, el recrudecimiento del conflicto armado en el país se ha visto en los asesinatos de líderes, pero también en un **incremento en casos de desplazamientos forzosos, intimidaciones, abusos sexuales y reclutamientos forzosos**. Las comunidades amenazadas, según ellos, se encuentran obligados a guardar silencio por miedo de ser asesinados. (Le puede interesar: "[Plataforma Defendamos la Paz recogerá firmas para curules de víctimas](https://archivo.contagioradio.com/defendamos-la-paz-2/)")

"**No podemos denunciar o nos matan**. En comunidades rurales se han instalado personas en cada comunidad al estilo de las famosas CONVIVIR o de informantes pagos. Carecemos de garantías para libre movimiento, libre expresión, el derecho a la asociación", lee la carta.

También, lamentaron la decisión del Gobierno de retomar la política de aspersión aérea con glifosato, asegurando que los efectos para las vidas humanas y los ecosistemas será "irreparable". A pesar de las preocupaciones por los efectos para la salud de los campesinos y ambiente, el Gobierno indicó que esta medida controversial se instalará el próximo mes. (Le puede interesar: "[El regreso del glifosato, una amenaza para campesinos en zonas cocaleras](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)")

Finalmente, expresaron un fuerte respaldo al trabajo que ha ejercido la  Jurisdicción Especial para la Paz (JEP) en búsqueda de la verdad, la justicia, la reparación y las garantías de no repetición con las víctimas del país. Los autores de la carta resaltaron que "implementación del Acuerdo para la terminación del conflicto armado entre el Gobierno Nacional y las FARC-EP es fundamental para la democracia".

En ese sentido, anunciaron que **participarán en la recolección de 1 millón de firmas que propone la plataforma Defendamos La Paz para instaurar las 16 curules de las víctimas en el Congreso**. Además, proponen aportar a esta iniciativa con su memoria, música, bailes y alegría "para seguir defendiendo la nueva democracia de la paz".

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
