Title: El laberinto de la pandemia COVID-19
Date: 2020-12-11 00:49
Author: Mision Salud
Category: Columnistas, Mision Salud, Nacional, Opinion
Tags: Actualidad, colombia, Covid-19, pandemia, vacuna, Vacunas en Colombia
Slug: el-laberinto-de-la-pandemia-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/fgfd.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/fff.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ilustracion-2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/iii.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/uuu.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/3LT52STGSNBRFLH2GYYN3OU4T4.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### Por [Jennifer Bueno](https://www.linkedin.com/in/jenniferbuenor/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde septiembre de 2019 el mundo entró en el laberinto del virus SARS-CoV-2, que, dada su contagiosidad en el marco de la globalización, sumergió al planeta en una pandemia sin precedentes. Hacemos el recorrido hasta hoy desde la perspectiva de la sociedad civil de las diversas capas que este laberinto tiene y compartimos nuestras reflexiones frente a cada una.

<!-- /wp:paragraph -->

<!-- wp:image {"id":94052,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/fgfd-1024x683.jpg){.wp-image-94052}  

<figcaption>
Photo by [Susan Yin](https://unsplash.com/@syinq?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->

####  La pandemia COVID-19

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Septiembre de 2019 la Junta de Monitoreo de Preparación Global (*GPMB* por sus siglas en inglés) de la Organización Mundial de la Salud (OMS) publicó el informe anual [“Un mundo en riesgo” (*A Word at risk)*](https://apps.who.int/gpmb/assets/annual_report/GPMB_annualreport_2019.pdf), en el que alertaba nuevamente, sobre el riesgo agudo de enfrentar una epidemia (regional) o pandemia (global). Según el informe esta epidemia/pandemia seria devastadora, no solo porque causaría la pérdida de miles de vidas humanas sino porque se generaría un caos social y una grave crisis a todas las economías del mundo, siendo las personas más pobres quienes más sufrirían en ese escenario.

<!-- /wp:paragraph -->

<!-- wp:image {"id":94056,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/fff.png){.wp-image-94056}  

<figcaption>
“El mundo en riesgo. El mundo se encuentra en riesgo agudo de vivir una devastadora enfermedad regional o global, epidémica o pandémica, que no solo causaría pérdida de vidas, sino que trastornaría las economías y crearía un caos social”. Traducción libre de Misión Salud a partir de referencia.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En dicho informe también se llamaba la atención sobre el quiebre de confianza pública que estaban enfrentando gobiernos, científicos, medios de comunicación, profesionales de la salud y sistemas de salud de la mayoría de los países del mundo, y cómo esta pérdida de legitimidad amenazaba las capacidades de estos actores para funcionar de manera efectiva en un escenario de emergencia. Una situación que se vería exacerbada por la desinformación amplia y rápidamente difundida por las redes sociales, que obstaculizaría el control de enfermedades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un año después el mundo entero enfrentó la crisis advertida. El virus SARS-CoV-2 se diseminó por prácticamente todos los países del mundo y en 4 meses se convirtió en la pandemia COVID-19, colapsando las capacidades de atención en salud y llevando a la toma de medidas drásticas y costosas como las cuarentenas y confinamientos obligatorios. Durante los primeros meses el colapso de los servicios de salud se vio agravado por la falta de conocimiento sobre la enfermedad y por el inevitable proceso de aprendizaje sobre las causas, la evolución, los síntomas, los posibles tratamientos, etc. La humanidad entró en una carrera de experimentación en busca del protocolo ideal para abordar a los pacientes infectados sintomáticos, otro para aquellos asintomáticos, y otro para prevenir la propagación del virus entre las personas sanas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Impacto financiero y social de las medidas de control

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como ya lo había advertido el informe del GPMB, el impacto económico y social de la pandemia ha sido devastador para todas las economías del mundo. Según el Banco Mundial la recesión generada por esta pandemia será la peor desde la Segunda Guerra Mundial (2) y para América Latina y el Caribe superará consecuencias más graves de las causadas por la crisis financiera global del 2008 y la crisis de la deuda latinoamericana en 1980 (3). El escenario más optimista según la CEPAL proyecta una recuperación a 10 años para los países de América Latina (4).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como consecuencia de la pandemia de COVID-19, entre 88 y 115 millones de personas pasarán a la pobreza extrema en 2020 (5). Según datos del Banco Interamericano de Desarrollo, en América Latina y el Caribe se perdieron al menos 24,83 millones de trabajos entre febrero y noviembre de 2020 (12,5% del empleo total de la región) (6). En Colombia el panorama es igual de grave, según el Departamento Administrativo Nacional de Estadística (DANE), para el mes de septiembre de 2020 la tasa de desempleo nacional fue de 15,8% (7).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un panorama tan preocupante como este ha causado que la esperanza de todo el planeta se fije en lograr una cura milagrosa, una fórmula absoluta que permita volver a la *normalidad*. Los ojos del mundo están puestos en la carrera de desarrollo de tratamientos de la COVID-19 efectivos y eficaces y de una vacuna contra el SARS-CoV-2.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### La vacuna ¿una solución milagrosa?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si bien se han hecho diversos llamados para tener en cuenta los desarrollos terapéuticos para tratar la COVID-19 con medicamentos sin patentes y de precios asequibles (8,9) los esfuerzos de los gobiernos y las instancias multilaterales se han concentrado en acelerar los diversos procesos de desarrollo de posibles vacunas, y en realizar las negociaciones con los laboratorios que los están llevando a cabo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la objetividad es una buena aliada en este escenario, debemos ser realistas y hacernos a la idea de que una vacuna no va a resolver la crisis social y económica que estamos viviendo. Cuando ya se cuente con vacunas en fase de producción y prontas a la comercialización será importante recordar que la historia de su desarrollo ha registrado serios desafíos de tipo técnico-científico y geopolítico para la distribución y aplicación en la población, que afectan la puesta en marcha, o no, de futuros programas de vacunación (10,11). 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Camino técnico

<!-- /wp:heading -->

<!-- wp:paragraph -->

El proceso de desarrollo a nivel técnico-científico ha estado rodeado de varias controversias. La principal de ellas es el tiempo en el que se están realizando dichos desarrollos y las implicaciones que este afán pueda tener en términos de seguridad, efectividad y eficacia en las vacunas desarrolladas (12). Tradicionalmente el desarrollo de una nueva molécula/vacuna le ha tomado a la ciencia de 10 a 16 años. Durante ese tiempo deben realizarse diversas pruebas y ensayos clínicos que permitan llevar al mercado un medicamento o vacuna que sea seguro y eficaz. Si bien es cierto que las tecnologías han permitido que la ciencia sea más rápida y eficiente en sus procesos de investigación y desarrollo, es imposible no alertarse frente a que un proceso que, en promedio, toma 12 años, se vea reducido a 2 años, o incluso a menos de 1.

<!-- /wp:paragraph -->

<!-- wp:image {"id":94057,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ilustracion-2.png){.wp-image-94057}  

<figcaption>
Ilustración 2. Comparación de tiempos de desarrollo de vacunas proceso estándar vs.  
Proceso COVID-19 (12)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En este momento (diciembre de 2020) de las más de 200 vacunas en desarrollo, hay 8 vacunas que ya se encuentran en la fase 3 de investigación ([ver reportes](https://mision-salud.org/AlianzaLAC_AaM/?page_id=163)), según la página del mecanismo GAVI-COVAX (13). Estar en fase 3 quiere decir que la vacuna en estudio está siendo comparada con el tratamiento alternativo (en este caso placebo) en grandes grupos de personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Será la primera vacuna en terminar los estudios fase 3 la mejor opción? Somos escépticos, no solo porque el afán no es un buen consejero, sino porque las condiciones de almacenamiento y transporte de las vacunas anunciadas por algunos de los que van más adelantados en el desarrollo son prácticamente imposibles de cumplir para países como el nuestro. Por ejemplo, el transporte de envases en vidrio sometidos a cadenas de frío por debajo de los 0° (-70° C) (14) es un desafío logístico, especialmente para países tropicales y del sur global cuyas temperaturas desafían la posibilidad de acceder a vacunas frías (14,15). Por eso consideramos prudentes las decisiones de los gobiernos de España y Francia que recientemente anunciaron que la vacunación **será voluntaria** e iniciará con los grupos de riesgo, esto es, profesionales de la salud y personas mayores de 60 años.

<!-- /wp:paragraph -->

<!-- wp:image {"id":94058,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/iii-1024x512.jpg){.wp-image-94058}  

<figcaption>
Ilustración 3. Tomado de GAVI-COVAX (12)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->

#### Camino político

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este proceso técnico-científico ha venido trazándose en medio del camino político, porque dado el modelo de mercado rigente en el mundo, acceder a una vacuna o a un medicamento es un complejo problema político en el que se miden fuerzas económicas e intereses globales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El desarrollo de las vacunas está concentrado en los países desarrollados o en el norte global (ver ilustraciones 3 y 4). China, Estados Unidos, la Unión Europea y Rusia, concentran las iniciativas de desarrollo más adelantadas y son quienes apuntan a estar de primeros en la línea de suministro para el planeta entero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El posicionamiento frente a la opinión pública de cada una de estas opciones ha estado enmarcado en una bruma mediática que va más allá del tema COVID-19. Pronunciamientos de defensores y detractores de una u otra opción a diario difunden mensajes que más allá de aclarar el panorama generan más confusión, y esto al final va a redundar en la adherencia a los planes de vacunación que se fijen en cada país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los países de América Latina y el Caribe han avanzado de manera individual en la firma de acuerdos directos con laboratorios (casi todos con las opciones de Estados Unidos y la Unión Europea), pocos países como Venezuela y Brasil han optado por explorar las opciones de Rusia, China o Cuba (16–18), y en la participación en algunos de los mecanismos multilaterales que están en construcción en este momento (COVAX o C-TAP). Es innegable la apuesta de los países de medianos ingresos para tratar de involucrarse en todas las estrategias posibles dada su baja capacidad financiera para competir con los países ricos en la compra/reserva de las vacunas, y dados sus ingresos superiores a los países más pobres que los sacan de las opciones de donación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cualquiera de las vacunas desarrolladas en el norte global los países del sur tendremos que incurrir no solo en la compra sino también en los gastos de importación y/o adecuación, transporte, etc. Este será el escenario general para todos los países de la región, excepto para Cuba y Brasil, quienes se encuentra en proceso de desarrollo de varias vacunas (en fase I y estudios preclínicos, respectivamente) (19).

<!-- /wp:paragraph -->

<!-- wp:image {"id":94059,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/uuu-1024x517.png){.wp-image-94059}  

<figcaption>
Ilustración 4. Vacunas en desarrollo en fases de investigación preclínica, fase 1, 2 y 3. Tomado de COVAX-GAVI a partir de WHO (13,20)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Esta limitación de productores frente a la demanda global ha desatado lo que varios líderes de opinión han denominado “nacionalismo de las vacunas” (20), un fenómeno en el que cada país toma las medidas internas necesarias para asegurar el suministro de vacunas a sus ciudadanos sin tener en cuenta las necesidades de otras naciones. Bajo esta dinámica nacionalista según The Economist, los países más ricos han acaparado cerca de la mitad del suministro proyectado de vacunas COVID-19 y los países con ingresos más bajos, como Colombia, tendrán que esperar años para lograr vacunar a toda su población (22). Desde la mirada epidemiológica, este escenario puede ser moderado en tanto ya se está construyendo una inmunidad de rebaño, que permite que no sea necesaria la vacunación de toda la población, esta inmunidad amplia los plazos y le da tiempo al país para tomar decisiones con el menor riesgo posible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como lo mencionamos párrafos atrás, el mundo no es del todo ciego a las posibles diferencias y desventajas que tienen los países de menores ingresos a la hora de acceder a tecnologías en salud, tampoco esta es una dificultad nueva, sino que ha sido documentada desde hace más de 20 años (23). Para tratar de equilibrar la balanza en el caso de esta vacuna, han surgido varias propuestas y alternativas que describiremos brevemente a continuación.

<!-- /wp:paragraph -->

<!-- wp:table -->

<figure class="wp-block-table">
  ------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  E                   INICIATIVA                                                                                                                                                                                                                      PROS                                                                                                                                                                                                                                                                                     CONTRAS
  **EN DESARROLLO**   Banco de acceso a tecnologías COVID- (C-TAP) *(COVID-19 Technology Access Pool)*                                                                                                                                                Busca compartir datos, conocimiento, propiedad intelectual y facilitar el acceso equitativo a productos de salud que permitirán salvar vidas contra la COVID-19. (24)                                                                                                                    No hay avances o compromisos concretos.  
  **ACTIVO**          [COVAX-GAVI](https://www.mision-salud.org/2020/09/blog-el-tiempo-que-es-la-iniciativa-gavi-covax-y-cuales-son-sus-principales-controversias/)                                                                                   No es una marca de vacuna ni un laboratorio. Es un mecanismo global de riesgo compartido para la adquisición conjunta y la distribución de eventuales vacunas COVID-19 (25). Codirigida por GAVI, tiene un portafolio conformado por varios de los proyectos de vacunas en desarrollo.   Serias fallas de transparencia de negociaciones con laboratorios productores.Tendencia a priorizar interés de laboratorios productores por encima del interés general.Participación de filantropía capitalista (Fundación Bill & Melinda Gates).Solo se han incluido en el portafolio vacunas producidas por potencias hegemónicas.  
  **EN ESTUDIO**      [India - Sudáfrica](https://blogs.eltiempo.com/medicamentos-sin-barreras/2020/11/06/covid-19-y-propiedad-intelectual-que-solicitud-hicieron-india-y-sudafrica-ante-la-omc/) suspensión medidas de propiedad intelectual ADPIC   Petición a Organización Mundial del Comercio. Propusieron una exención temporal de algunas de las [obligaciones de los ADPIC](https://www.mision-salud.org/2015/08/licencias-obligatorias-un-vistazo-general-para-entender/).                                                            Una iniciativa que tiene resistencia de alto nivel.Se están dando los debates al interior de la OMC en tiempos no adecuados para un marco de emergencia global.
  ------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:heading {"level":4} -->

#### Escenario regional y local

<!-- /wp:heading -->

<!-- wp:paragraph -->

América Latina y el Caribe, de momento, tiene tres países con capacidad e infraestructura para producir las vacunas que requiere la región: Brasil, México y Argentina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cada uno de estos países ha optado, entre otras estrategias, por acuerdos directos entre laboratorios farmacéuticos privados y público-privados, para realizar la adecuación y distribución en la región de vacunas desarrolladas en Europa o China. En el caso de la alianza México-Argentina se trata de un acuerdo con el laboratorio multinacional AstraZeneca, financiado por el millonario Carlos Slim (26) a través del cual se espera que estos países puedan aprovechar su capacidad productora para adecuar la vacuna y distribuirla en la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una dirección diferente avanzan Cuba con en el desarrollo de su vacuna “Soberana”, y Venezuela que realizó un acuerdo con Rusia para garantizar el suministro a su población (27,28). El resto de los países de la región estamos a la espera de que los más ricos y los que tienen las capacidades industriales de desarrollo y producción decidan la logística de venta y suministro y nos den el turno. La estrategia de Colombia fue anunciada por primera vez en la rendición de cuentas del 30 de junio de 2020 (29). En ella el Ministro de Salud informó de manera general que la estrategia contemplaría 3 acciones centrales (30) y dichas estrategias, con nuevas adicionales, han ido avanzando como se muestra en el cuadro siguiente.

<!-- /wp:paragraph -->

<!-- wp:table -->

<figure class="wp-block-table">
  ---------------------- --------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Fecha de anuncio**   **Acción**                                                                                                                                          **Estado** **(4 meses después)**
  Junio 30               Respaldo de Colombia a Resolución WHA 73.1 de la Asamblea Mundial de la Salud 2020                                                                  En efecto Colombia respaldó la resolución WHA 73.1 de la AMS 2020, sin embargo, esta resolución ha quedado en la historia como una acción diplomática más, pues si bien mecanismos como los propuestos por Costa Rica, India y Sudáfrica van en vía de realización del compromiso adquirido en la resolución, estos han enfrentado la oposición por parte de las grandes potencias económicas (tanto en la OMS como en la OMC). 
  Junio 30               Compra centralizada a través del Fondo de la Organización Panamericana de la Salud (OPS) y simultáneamente participación en mecanismo GAVI-COVAX.   Se ha anunciado que será a través del Fondo Rotatorio de la OPS que se realizará la compra al mecanismo COVAX (30) y que las vacunas adquiridas por este mecanismo estarán disponibles en el país en el segundo semestre de 2021.
  Junio 30               Participación de la población colombiana en ensayos clínicos fase III de las vacunas que se encuentran en desarrollo.                               Participación en estudio Solidarity de la OMS.
  Julio 28               Negociaciones directas y/o confidenciales con laboratorios productores.                                                                             Colombia ha firmado acuerdo de confidencialidad para tener acceso a vacunas el primer semestre de 2021 con Pfizer, AstraZeneca, Janssen, Sinopharm, CaSino y el Serum Institut de India (31).
  ---------------------- --------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<figcaption>
Cuadro 1. Estrategias de Colombia para adquirir una posible vacuna COVID-19  
(Elaboración propia a partir de (32,33)

</figcaption>
</figure>
<!-- /wp:table -->

<!-- wp:paragraph -->

Desde el inicio de esta crisis nuestra Directora, Dra. Angela Acosta, ha venido alertando sobre las controversias de la estrategia nacional para acceder a la vacuna y ha hecho un llamado constante a equilibrar esfuerzos, pues al enfocarse en la vacuna se están dejando de lado estrategias como los programas de prevención y promoción y atención primaria que al ser potenciados podrían tener un impacto inmediato en la propagación del virus (34), así como el acompañamiento y respaldo a las iniciativas de investigación local sobre tratamientos posibles a los casos positivos (p.e. ensayo clínico de ivermectina en Valle del Cauca) (9,35).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia se espera que esta semana sean publicadas las directrices del esquema de vacunación construido por el Ministerio. La obligatoriedad de la vacunación, el consentimiento informado, entre otras especificaciones serán objeto de rigurosa veeduría ciudadana puesto que ha sido muy limitado el acceso a la información gubernamental sobre los acuerdos y decisiones. La participación ciudadana ha sido restringida y solo se conocen los datos publicados en medios oficiales y las alocuciones del Presidente, del Ministro de Salud y otros voceros del gobierno. Desde Misión Salud hemos elevado la solicitud al Presidente para que se abran a la brevedad espacios de transparencia y veeduría a través de los cuales las y los colombianos podamos acompañar de manera directa el camino de las decisiones que se están tomando (36).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las decisiones que se están tomando de manera discrecional van a afectar directamente la salud de todas las familias colombianas, por lo que consideramos la transparencia sobre la información de los acuerdos, decisiones, vacunas, esquemas y estrategias de vacunación que deben ser sometidas a absoluta transparencia. Si esto estuviera aconteciendo no se habría convertido en una polémica el hecho de que en Colombia se les eximirá de responsabilidad sobre efectos adversos a los laboratorios productores de la vacuna y que quien la asumirá será el Estado colombiano. El reporte de la GPMB ya anunciaba los problemas que generaría la falta de confianza en los gobiernos y los sistemas de salud del mundo. En Colombia, que no es la excepción, debería tenerse en cuenta que la sociedad no tiene plena confianza en las acciones del Gobierno, justificada por años y años de abandono social y corrupción. El gobierno actual no puede cerrarse a actuar de manera discrecional y sin transparencia y amplia participación pues estas acciones se transforman al final en un *palo en la rueda* de cualquier acción que requiera pleno compromiso ciudadano. Esta apertura implica un desafío a las capacidades administrativas del gobierno, ya reducidas y colapsadas por la crisis, sin embargo, es la que garantizará un compromiso social amplio y voluntario.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Referencias**

<!-- /wp:heading -->

<!-- wp:list {"ordered":true,"type":"1"} -->

1.  Global Preparedness Monitoring Board. A world at risk. Annual Report on global preparedness for helath emergencies \[Internet\]. 2019 sep \[citado el 25 de noviembre de 2020\]. Report No.: 2019. Disponible en: <https://apps.who.int/gpmb/assets/annual_report/GPMB_annualreport_2019.pdf>  
2.  Banco Mundial. La COVID-19 (coronavirus) hunde a la economía mundial en la peor recesión desde la Segunda Guerra Mundial \[Internet\]. World Bank. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://www.bancomundial.org/es/news/press-release/2020/06/08/covid-19-to-plunge-global-economy-into-worst-recession-since-world-war-ii>  
3.  Banco Mundial. Perspectivas económicas mundiales: América Latina y el Caribe \[Internet\]. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <http://pubdocs.worldbank.org/en/657071588788309322/Global-Economic-Prospects-June-2020-Regional-Overview-LAC-SP.pdf>  
4.  CEPAL. Construir un nuevo futuro: una recuperación transformadora con igualdad y sostenibilidad \[Internet\]. 2020. 243 p. (Periodos de sesiones CEPAL). Disponible en: https://repositorio.cepal.org/bitstream/handle/11362/46227/1/S2000699\_es.pdfhttps://repositorio.cepal.org/bitstream/handle/11362/46227/1/S2000699\_es.pdf
5.  Lakner C, Yonzan N, Gerszon Mahler D, Castañeda Aguilar A, Wu H, Fleury M. Últimas estimaciones del impacto de la COVID-19 en la pobreza mundial: el efecto de los nuevos datos \[Internet\]. Blog de datos. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://blogs.worldbank.org/es/datos/ultimas-estimaciones-del-impacto-del-coronavirus-en-la-pobreza-mundial>  
6.  Banco Interamericano de Desarrollo. Observatorio Laboral Covid-19 | BID \[Internet\]. Observatorio Laboral COVID-19. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://observatoriolaboral.iadb.org/>  
7.  DANE. Empleo y desempleo \[Internet\]. Mercado laboral Colombia. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://www.dane.gov.co/index.php/estadisticas-por-tema/mercado-laboral/empleo-y-desempleo>  
8.  Machado M. Acelerador-ACT, COVAX y ampliación de plazos - \[Internet\]. Misión Salud. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://www.mision-salud.org/2020/10/acelerador-act-covax-y-ampliacion-de-plazos/>  
9.  Pharmabaires. IVERMECTINA: OTRO ESTUDIO CONFIRMA SU EFECTIVIDAD EN TRATAMIENTOS COVID 19 \[Internet\]. Pharmabaires. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <http://www.pharmabaires.com/salud-e-interes-publico/1775-ivermectina-otro-estudio-confirma-su-efectividad-en-tratamientos-covid-19.html>  
10. Gallagher J. Covid: Vaccine will “not return life to normal in spring”. BBC News \[Internet\]. el 1 de octubre de 2020 \[citado el 25 de noviembre de 2020\]; Disponible en: <https://www.bbc.com/news/health-54371559>  
11. The DELVE Initiative. SARS-CoV-2 Vaccine Development & Implementation; Scenarios, Options, Key Decisions \[Internet\]. Royal Society DELVE Initiative; 2020 oct \[citado el 25 de noviembre de 2020\]. (DELVE report). Report No.: 6. Disponible en: <https://rs-delve.github.io/reports/2020/10/01/covid19-vaccination-report.html>  
12. Machado M. El riesgo de aprobar vacunas COVID-19 de manera acelerada - \[Internet\]. Misión Salud. 2020 \[citado el 25 de noviembre de 2020\]. Disponible en: <https://www.mision-salud.org/2020/09/el-riesgo-de-aprobar-vacunas-covid-19-de-manera-acelerada/>  
13. GAVI. COVID-19 vaccine tracker \[Internet\]. GAVI | COVID-19. 2020 \[citado el 26 de noviembre de 2020\]. Disponible en: <https://www.gavi.org/tag/covid19>  
14. Gelles D. La difícil tarea de transportar una vacuna que debe estar a 80 grados Celsius bajo cero. The New York Times \[Internet\]. el 21 de septiembre de 2020 \[citado el 26 de noviembre de 2020\]; Disponible en: <https://www.nytimes.com/es/2020/09/21/espanol/negocios/transporte-vacunas-logistica.html>  
15. Redacción. Qué es una “vacuna caliente” y cómo su desarrollo puede ayudar a los países más pobres del mundo a combatir el coronavirus. BBC News Mundo \[Internet\]. el 9 de noviembre de 2020 \[citado el 26 de noviembre de 2020\]; Disponible en: <https://www.bbc.com/mundo/noticias-54843070>  
16. Cordero A. Rusia suministrará a Latinoamérica 100 millones de dosis de la vacuna contra el virus. France 24 \[Internet\]. el 10 de septiembre de 2020 \[citado el 30 de noviembre de 2020\]; Disponible en: <https://www.france24.com/es/20200910-francia-confinamiento-coronavirus-covid19-israel>  
17. Misión Salud. Esperemos que las próximas decisiones de la Comisión asesora para adquirir vacuna \#COVID19 del @MinSaludCol tenga en cuenta las opciones disponibles para \#AmericaLatina. \[Internet\]. Twitter. 2020 \[citado el 30 de noviembre de 2020\]. Disponible en: <https://twitter.com/MisionSaludCo>   
18. Vargas V. América Latina se suma a la búsqueda global de vacunas contra el COVID-19 \[Internet\]. Gente Saludable. 2020 \[citado el 30 de noviembre de 2020\]. Disponible en: <https://blogs.iadb.org/salud/es/america-latina-vacunas-covid-19/>  
19. Marsh S. Cuba lidera carrera por vacuna contra coronavirus en América Latina. SWI swissinfo.ch | Enfoque \[Internet\]. el 12 de noviembre de 2020 \[citado el 30 de noviembre de 2020\]; Disponible en: <https://www.swissinfo.ch/spa/enfoque-cuba-lidera-carrera-por-una-vacuna-contra-coronavirus-en-am%C3%A9rica-latina/46157910>  
20. World Health Organization. Draft landscape of COVID-19 candidate vaccines \[Internet\]. 2020 \[citado el 26 de noviembre de 2020\]. Disponible en: <https://www.who.int/publications/m/item/draft-landscape-of-covid-19-candidate-vaccines>  
21. GCIP Global Congress IP. \[ES\] Acceso a alternativas de tratamiento y vacunas para COVID – ¿qué hacemos con las patentes \[Internet\]. 2020 \[citado el 27 de noviembre de 2020\]. (Semana de la Propiedad Intelectual, interés público y COVID-19). Disponible en: <https://www.youtube.com/watch?v=c6-GxsgPs7o&list=PLoq-zQntXFbR3poSShK9FjBrrIBziYBUr&index=9>  
22. The economist. Rich countries grab half of projected covid-19 vaccine supply. The Economist \[Internet\]. el 12 de noviembre de 2020 \[citado el 27 de noviembre de 2020\]; Disponible en: <https://www.economist.com/graphic-detail/2020/11/12/rich-countries-grab-half-of-projected-covid-19-vaccine-supply>  
23. Holguín Zamorano G. La guerra contra los medicamentos genéricos. Un crimen silencioso. Aguilar; 2014. 321 p. 
24. Presidencia de Costa Rica. Costa Rica entrega al mundo avances tecnológicos contra el COVID-19 \[Internet\]. 2020 \[citado el 26 de noviembre de 2020\]. Disponible en: <https://www.presidencia.go.cr/comunicados/2020/09/costa-rica-entrega-al-mundo-avances-tecnologicos-contra-el-covid-19/>  
25. GAVI. COVAX Facility \[Internet\]. GAVI - COVAX. 2020 \[citado el 26 de noviembre de 2020\]. Disponible en: <https://www.gavi.org/covax-facility>  
26. Fundación Carlos Slim. AstraZeneca anuncia acuerdo con la Fundación Carlos Slim para suministrar la vacuna COVID-19 a América Latina \[Internet\]. Fundación Carlos Slim. 2020 \[citado el 30 de noviembre de 2020\]. Disponible en: <https://fundacioncarlosslim.org/astrazeneca-anuncia-acuerdo-con-la-fundacion-carlos-slim-para-suministrar-la-vacuna-covid-19-a-america-latina/>   
27. Prensa Nacional, Melean E. Venezuela ratifica acuerdos para garantizar más de 10 millones de vacunas Sputnik-V • Ministerio del Poder Popular para Relaciones Exteriores \[Internet\]. Ministerio del Poder Popular para Relaciones Exteriores. 2020 \[citado el 1 de diciembre de 2020\]. Disponible en: <http://mppre.gob.ve/2020/11/15/venezuela-ratifica-acuerdos-garantizar-10-millones-vacunas-sputnik-v/>  
28. Schlenker O. Llegó la vacuna rusa a Venezuela: ¿y ahora qué? | DW | 09.10.2020. DWCOM \[Internet\]. el 9 de octubre de 2020 \[citado el 1 de diciembre de 2020\]; Disponible en: <https://www.dw.com/es/lleg%C3%B3-la-vacuna-rusa-a-venezuela-y-ahora-qu%C3%A9/a-55223581>  
29. Ministerio de Salud y Protección Social. Audiencia pública de rendición de cuentas del Ministerio de Salud y Protección Social \[Internet\]. 2020 \[citado el 24 de octubre de 2020\]. Disponible en: <https://youtu.be/RTbOmXBXQ7w?t=4543>  
30. Bueno J. El plan de Colombia para garantizar el acceso a vacuna COVID-19 \[Internet\]. Misión Salud. 2020 \[citado el 1 de diciembre de 2020\]. Disponible en: <https://www.mision-salud.org/2020/07/el-plan-de-colombia-para-garantizar-el-acceso-a-una-posible-vacuna-contra-el-sars-cov-2/>  
31. Leal D. ¿Qué tan cerca está la llegada a Colombia de la vacuna contra el Covid-19? Radio Nacional de Colombia \[Internet\]. el 24 de noviembre de 2020 \[citado el 1 de diciembre de 2020\]; Disponible en: <https://www.radionacional.co/noticia/coronavirus/que-cerca-la-llegada-a-colombia-de-la-vacuna-contra-covid-19>  
32. Ministerio de Salud y Protección Social. Minsalud explica proceso para adquisición de vacuna contra covid-19 \[Internet\]. Ministerio de Salud y Protección Social. 2020 \[citado el 1 de diciembre de 2020\]. Disponible en: <https://www.minsalud.gov.co/Paginas/Minsalud-explica-proceso-para-adquisicion-de-vacuna-contra-covid-19.aspx>  
33. Colprensa. Colombia busca adquirir vacuna para Covid y aplicarla en primer semestre de 2021 \[Internet\]. www.vanguardia.com. 2020 \[citado el 1 de diciembre de 2020\]. Disponible en: <https://www.vanguardia.com/colombia/colombia-busca-adquirir-vacuna-para-covid-y-aplicarla-en-primer-semestre-de-2021-FK3095052>  
34. ASINFAR. 3er LIVE-CLASH ASINFAR / La vacuna... ¿Estamos a tiempo? \[Internet\]. 2020 \[citado el 1 de diciembre de 2020\]. (Live clash ASINFAR). Disponible en: <https://www.youtube.com/watch?v=tQD2hRJ8ZQM>  
35. Casa Editorial El Tiempo. Así es el estudio pionero para saber si fármaco sirve contra el covid. El Tiempo \[Internet\]. el 22 de julio de 2020 \[citado el 1 de diciembre de 2020\]; Disponible en: <https://www.eltiempo.com/colombia/cali/cientificos-explican-como-es-el-ensayo-clinico-de-la-ivermectina-para-controlar-el-covid-19-520646>
36. Misión Salud. Compromiso intersectorial por la transparencia, transferencia de tecnología y propiedad intelectual y acceso equitativo de una posible vacuna covid-19 para países de bajos y medianos ingresos \[Internet\]. 2020 \[citado el 1 de diciembre de 2020\]. Disponible en: <https://www.mision-salud.org/wp-content/uploads/2020/11/20201130_Carta-abierta-al-Presidente-de-la-Republica-transparencia.pdf>

<!-- /wp:list -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### **Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Conozca aquí a [Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
