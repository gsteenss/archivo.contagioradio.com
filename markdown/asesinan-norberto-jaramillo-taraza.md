Title: Colombia pierde a un líder "innato": Asesinan a Norberto Jaramillo en Tarazá
Date: 2018-09-05 17:32
Author: AdminContagio
Category: DDHH, Nacional
Tags: Antioquia, Líder Social Asesinado, Sustitución de cultivos
Slug: asesinan-norberto-jaramillo-taraza
Status: published

###### [Foto: Contagio Radio] 

###### [5 Sept 2018] 

Otro líder social fue asesinado en Tarazá, Antioquia, se trata de **Norberto Jaramillo, presidente de la Junta de Acción Comunal (JAC) de la vereda La Envidia,** quien se encontraba impulsando la sustitución de cultivos de uso ilícito. Con Jaramillo ya son 25 líderes sociales asesinados en ese departamento y más de 4.000 mil personas han sido desplazado del territorio.

Según información de **Oscar Zapata, integrante del nodo de la Coordinación Colombia-Europa-Estados Unidos (COEUROPA)** en Antioquia, los hechos se habrían presentado el pasado 3 de septiembre, cuando varias personas armadas llegaron a la vivienda del líder social y le dispararon en diversas ocasiones. Zapata afirmó que Jaramillo se caracterizaba por ser un líder innato, que había participado en varios procesos de movilización con la Asociación Campesina del bajo Cauca, y en sus últimos días de vida, se dedicó a impulsar el programa de sustitución.

A pesar de que Jaramillo no había denunciado amenazas contra su vida, Zapata afirmó que en Tarazá los líderes  del proceso de sustitución están siendo extorsionados por el grupo **Los Caparrapos, mientras las autodenominadas Autodefensas Gaitanistas de Colombia (AGC)**, los amenazan si pagan esas extorsiones. (Le puede interesar: ["110 líderes sociales han renunciado a su labor en Antioquia por amenazas"](https://archivo.contagioradio.com/110-lideres-sociales-han-renunciado/))

Por otra parte, el Sistema de Alertas Tempranas (SAT) de la Defensoría del Pueblo ya había llamado la atención sobre 11 desplazamientos masivos en la zona, lo que ha significado la huida del territorio de **4.200 personas.** Situación que para Zapata se resumen en que "los campesinos están en medio de una disputa de poder de esas estructuras (paramilitares), y el Estado no hace nada".

### **¿El Estado colombiano tiene control sobre el bajo Cauca Antioqueño?** 

De hecho, el integrante de COEUROPA denunció que **un día después del homicidio de Jaramillo, su cuerpo aún no había sido levantado por las autoridades judiciales,** razón por la cual, líderes intentaron llegar hasta el lugar de los hechos pero fueron interceptados por hombres que, al parecer, pertenecían a los Caparrapos. Aunque el cuerpo del líder finalmente fue trasladado por miembros del ejército, en el lugar no hubo presencia judicial, hecho que para Zapata revela "que el control en la zona lo tienen los paramilitares y no el Estado".

Adicionalmente, Zapata sostuvo que **la expansión de los grupos paramilitares "no sería realizable sin una posible ayuda de autoridades civiles o militares",** razón por la cual pidió **"voluntad real y no sólo creación de mesas"** al presidente Iván Duque, al Ministro de Defensa Guillermo Botero y al gobernador de Antioquia Luis Pérez, para priorizar el tema del asesinato de líderes sociales en sus agendas. (Le puede interesar: ["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
