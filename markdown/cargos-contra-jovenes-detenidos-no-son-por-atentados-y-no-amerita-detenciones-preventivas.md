Title: "Cargos contra jóvenes detenidos no ameritan detenciones preventivas"
Date: 2015-07-21 15:52
Category: DDHH, Judicial
Tags: congreso de los pueblos, detencion, explosiones, falsos, joven, jovenes, judiciales, porvenir, positivos
Slug: cargos-contra-jovenes-detenidos-no-son-por-atentados-y-no-amerita-detenciones-preventivas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Foto-Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Congreso de los Pueblos 

#####  

<iframe src="http://www.ivoox.com/player_ek_5068481_2_1.html?data=lpWjmpmcdY6ZmKiakp2Jd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmyNTgjcjTstXmwpDXh6iXaaOn18rbx9iPqMboxtPWxtTXb8%2FjjMbax9fNuMKfxcrhx9PHrdDixtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Garzón, Abogado defensor] 

######  

###### [21 jul 2015] 

Se cumplen 9 días de audiencias, y casi 2 semanas desde la detención de 15 jóvenes acusados ante los medios de información de ser los responsables de las explosiones en las sedes de Porvenir del 2 de julio, y mientras, con el paso de los días, fue quedando claro que ninguna de las imputaciones tenía relación con dichas explosiones, fue aumentando la duda sobre el manejo que la judicatura estaba dando a los procesados.

Una vez imputados los cargos, frente a los cuales los 13 jóvenes se declararon inocentes, la Fiscalía ha procedido a solicitar medida de aseguramiento en establecimientos penitenciarios. El proceso en el cual el ente acusador deberá comentar y justificar la solicitud para cada uno y cada una de las acusadas, podría tomar una semana más.

Para los abogados de la bancada de la defensa, el artículo 366 del Código de Procedimiento Penal, bajo el cual se analiza el caso de los 10 jóvenes imputados de "fabricación de explosivos"  y "violencia contra servidor público", es claro que no es necesaria la detención domiciliaria; sin embargo, teniendo en cuenta el contexto mediático en el cual se ha desarrollado este proceso y la manera en que la judicatura ha tratado el caso, las esperanzas con escasas.

El proceso, según explica el abogado Manuel Garzón, es una muestra de la incongruencia del Estado en lo relacionado con su política criminal, en tanto los 10 jóvenes procesados por su presunta participación en una protesta en la Universidad Nacional, podrían cumplir penas de hasta 40 años de prisión, mucho mayores a las penas asignadas a delitos denominados mayores, como "rebelión" o "terrorismo".

Entre tanto, las 13 personas capturadas se han fortalecido anímicamente, según afirman los abogados, como consecuencia de la solidaridad manifestada por familiares, amigos y organizaciones sociales.
