Title: Ejército continúa la erradicación forzada en Putumayo
Date: 2016-11-15 15:27
Category: Nacional, Paz
Tags: Cultivos de coca, Erradicación Forzada, hoja de coca
Slug: ejercito-continua-erradicacion-forzada-de-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Siglo] 

###### [15 Nov  2016] 

Hacia las 9:30 am del Domingo 13 de Noviembre, militares adscritos a la Brigada 27 de Selva a cargo del Sargento Palmas, ingresaron a predios del asentamiento Nasa el Píldoro, ubicado en el Resguardo de Alpes Orientales, Municipio de Puerto Caicedo, y realizaron una erradicación forzada de cultivos de coca en la finca de **Jesús Camayo, líder comunitario quien la cultiva y trabaja como elemento cultural, medicinal y espiritual dentro de la cosmovisión del Pueblo Nasa.**

La comunidad denuncia que dichas acciones militares violan sus derechos culturales al desconocer la cosmogonía del Pueblo Nasa, el **derecho fundamental a la consulta previa reconocida por la Corte Constitucional** en la sentencia 383/03 y los **diálogos entre el Gobierno Nacional y la Mesa de Organizaciones Sociales** del Putumayo donde el Ministro Luis Gilberto Murillo se comprometió el pasado 4 de Septiembre a frenar las erradicaciones manuales hasta que se acuerde el plan de sustitución.

Afirman también que estos abusos van en de los avances alcanzados entre el Gobierno Nacional y la Guerrilla de las FARC-EP para lograr **mejorar las condiciones de vida en el campo, alcanzar una paz estable y duradera** a través de la creación de planes de sustitución concertados, acordado en el Punto \#4: **Solución al Problema de Drogas Ilícitas.**

Habitantes del territorio manifiestan estar preocupados por este tipo de actuaciones que **irrespetan las distintas culturas presentes en el territorio amazónico y generan una mayor conflictividad social.** Le puede interesar: [Erradicaciones forzadas de coca no contribuyen a la Paz.](https://archivo.contagioradio.com/campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
