Title: Universidades colombianas se pronuncian frente a la paz
Date: 2016-09-21 11:10
Category: Nacional, Paz
Tags: crisis educación Colombia, educación superior colombia, Universidad Pública Colombiana
Slug: universidades-colombianas-se-pronuncian-frente-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Universidades-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [21 Sept 2016 ] 

Durante una semana se reunieron los delegados de 20 universidades y 2 Institutos de Educación Superior para reflexionar en torno a la universidad, la región y la construcción de paz. En el encuentro intercambiaron sus puntos de vista, saberes y modos de acción implementados para hacer frente a la guerra y los que pondrán en marcha para lograr la **consolidación de una paz con justicia social e inclusiva, en la que el Estado salde la deuda que tiene con la educación pública**.

Para los académicos, es fundamental que el Estado colombiano en cabeza del Ministerio de Educación, atienda las demandas del sector, como eje central para la construcción de paz en las regiones, teniendo en cuenta la **función de las universidades como formadoras, acompañantes y promotoras de la paz y la reconciliación**. Justamente una de las primeras exigencias es que el MEN asuma la responsabilidad de convocar la [[reforma estructural de la Ley 30 de 1993](https://archivo.contagioradio.com/se-agudiza-la-crisis-en-las-universidades-publicas-colombianas/)], en la que participe directamente la comunidad universitaria.

Desde las universidades se exige a MinEducación revisar y reorientar el sistema de bolsas concursables, que se han convertido en generadoras de competencia en el sector; así mismo se demanda **que el cuerpo docente ocasional sea formalizado**, que se revise la obligada expansión de programas en las universidades que no cuentan con garantías de infraestructura y financiación. Por otra parte se llama la atención del Ministerio para que intervenga y logre que del presupuesto nacional haya una [[mayor partida presupuestal que beneficie el sector](https://archivo.contagioradio.com/durante-los-ultimos-52-anos-colombia-ha-invertido-179-000-millones-de-dolares-en-la-guerra/)].[ ]

"Quizá estos sean apenas los puntos básicos para empezar y mantener un diálogo abierto, franco y respetuoso entre la academia de las regiones y el Estado, [[para que la construcción de paz sea estable y duradera](https://archivo.contagioradio.com/se-lanza-la-iniciativa-la-paz-si-es-con-educacion/)] (...) con una dimensión territorial y cultural" se asegura desde la comunidad universitaria que insiste en que **"las tareas del país de la guerra, no pueden ser las mismas del país de la paz"**, por lo que se hace necesario [[aprobar con el sí en el plebiscito los acuerdos](https://archivo.contagioradio.com/el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz/)] pactados en La Habana.

En el marco del apoyo que las universidades han extendido al proceso de paz, en varias de ellas se llevan a cabo foros relacionados con el acuerdo final sobre el **conflicto armado, la construcción de paz y los retos de la academia**. Este jueves en la UPTC de Tunja desde las 8 y hasta las 12, funcionarios y directivos asistirán a un conversatorio del que harán parte Carlos Medina Gallego y Manuel Restrepo Domínguez.

Este viernes en Chiquinquirá se realizará el Encuentro Nacional de Escritores, en el que se conversará sobre la paz y los derechos humanos. El Sábado en Boyacá se hará el encuentro Regional de Jóvenes. Para el próximo 4 de octubre se planea la realización de un conversatorio sobre **la universidad, la investigación y la construcción de paz** en la UPTC de Tunja, con la participación de Víctor Currea de Lugo y Manuel Restrepo Domínguez.

###### [Reciba toda la información de Contagio Radio [[en su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
