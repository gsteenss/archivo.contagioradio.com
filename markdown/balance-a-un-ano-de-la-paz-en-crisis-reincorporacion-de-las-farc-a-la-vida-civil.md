Title: Balance a un año de la paz: Cuatro aspectos de la crisis en la reincorporación de FARC
Date: 2017-11-23 17:42
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, Gobierno Nacional, reincorporación
Slug: balance-a-un-ano-de-la-paz-en-crisis-reincorporacion-de-las-farc-a-la-vida-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/reincorporación2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Nov 2017] 

La crisis de la reincorporación la retrata la Fundación Paz y Reconciliación en el segundo capítulo del informe “Como va la paz”. Afirman que en la historia de Colombia “nunca se ha desarrollado una experiencia de reincorporación **que haya sido exitosa en su totalidad**, el fracaso de estos procesos ha perpetuado episodios de violencia que siguen marcando la historia del país”.

Las razones que la Fundación expresa para argumentar el riesgo que vive el proceso de la reincorporación son **la inseguridad física, la inseguridad jurídica, la inseguridad económica y la inseguridad social**. Esto, representa un problema mayor debido a que una reincorporación exitosa es la mayor garantía para contrarrestar la reincidencia de estos grupos armados.

Ahora, estos problemas de la reincorporación se deben a que **no hay voluntad por parte de las instituciones del Estado** para llevar a cabo las acciones pertinentes. De acuerdo con la Fundación, “los bajos niveles de articulación interinstitucional limitan la acción de las entidades (…) y no se ha logrado que alguna entidad asuma una coordinación eficaz, con capacidad de ordenar, con espíritu gerencial y mando político”. (Le puede interesar: ["Balance a un año de la paz: FARC, la guerrilla que mayor cantidad de armas ha dejado"](https://archivo.contagioradio.com/farc-ha-sido-la-guerrilla-que-mayor-cantidad-de-armas-ha-dejado/))

### **Hay falta de recursos para una implementación de calidad** 

Adicional a la falta de voluntad política, **no hay suficientes recursos** para que se aplique una reincorporación adecuada y se garantice la seguridad económica de las personas que están haciendo el tránsito a la vida civil. “Por ejemplo, el Gobierno comenzó la entrega de los beneficios económicos como la renta básica que representa el 90% de salario mínimo, pero se denuncia que el sistema de bancarización no es eficiente”.

**Tampoco hay una estrategia de implementación de los proyectos productivos** de mediana y larga escala debido a que el Gobierno está teniendo en cuenta una reincorporación individual que se asemeja al modelo de reintegración de los paramilitares y las FARC han manifestado que es fundamental desarrollar un proceso de reincorporación colectivo “que mantenga la cohesión de la organización”. (Le puede interesar: ["Jean Arnault expresó su preocupación por el proceso de reincorporación en Colombia"](https://archivo.contagioradio.com/49353/))

Otra gran preocupación es que los Espacios Territoriales de Capacitación y Reincorporación, “**no son aptos para el desarrollo de proyectos productivos** de mediana o larga escala, porque a duras penas son funcionales para pequeños proyectos productivos de subsistencia”. Sumado a esto, estos territorios no han sido adecuados al 100% y esto es uno de los motivos por los cuales aumenta la deserción.

### **Incumplimientos generales del acuerdo afectan la reincorporación efectiva** 

En repetidas ocasiones, diferentes organizaciones han manifestado que el incumplimiento general del acuerdo de paz por parte del Gobierno Nacional, ha hecho que aumenten las **inseguridades para la efectiva reincorporación**. Ante esto, el informe indica que hay un ambiente hostil en el Congreso de la República “por parte de algunos partidos políticos para pasar los proyectos de ley que implementan la Jurisdicción Especial de Paz, la Reforma Rural, las Circunscripciones Especiales de Paz, entre otras”.

Finalmente, de este capítulo de la crisis de la reincorporación, **se vinculan diferentes fenómenos que ponen en riesgo la transición** de los excombatientes hacia la vida civil y esto dificulta el éxito que puede llegar a tener el proceso de paz. Ante esto, la Fundación Paz y Reconciliación en conjunto con otras organizaciones sociales, hacen un llamado para que se establezcan las garantías necesarias para la seguridad física, jurídica, económica y social de los excombatientes de las FARC.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
