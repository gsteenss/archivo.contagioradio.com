Title: Antonio José García
Date: 2019-05-21 12:39
Author: CtgAdm
Slug: antonio-jose-garcia
Status: published

[![El atrato y la resilencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CACARICA-FESTIVAL-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-atrato-y-la-resilencia/)  

#### [El atrato y la resilencia](https://archivo.contagioradio.com/el-atrato-y-la-resilencia/)

Posted by [Antonio José García Fernández](https://archivo.contagioradio.com/author/antonio-jose-garcia/)Allí, queda en plena evidencia que el esfuerzo colectivo está enfocado a la consolidación de la reconciliación, a la construcción de la memoria, a la búsqueda de la verdad  
[Leer opinión](https://archivo.contagioradio.com/el-atrato-y-la-resilencia/)
