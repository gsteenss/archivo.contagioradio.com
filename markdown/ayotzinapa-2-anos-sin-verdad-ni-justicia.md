Title: Ayotzinapa, 2 años sin verdad ni justicia
Date: 2016-09-26 17:44
Category: El mundo, Movilización
Slug: ayotzinapa-2-anos-sin-verdad-ni-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Accion-Global-por-Ayotzinapa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Acción Global por Ayotzinapa 

##### [26 Sep 2016] 

A 2 años de la desaparición de los 43 estudiantes de la Normal Superior Rural "Raúl Isidro Burgos" de Ayotzinapa, los familiares de los jovenes permanecen con la misma incertidumbre del primer día, a pesar que según el último informe público sobre el caso, se han presentado 130 detenciones, 422 resoluciones judiciales, 850 declaraciones, 1.651 actuaciones periciales y un expediente de 240 tomos y un cuarto de millón de folios.

A pesar de las múltiples evidencias que involucran la participación de agentes del estado Mexicano en los crímenes cometidos el 26 de septiembre de 2014,  la falta de respuestas por parte del gobierno Peña Nieto y la complicidad de las instituciones encargadas de investigar y garantizar el cumplimiento de la justicia, han permitido que a la fecha el caso continue en total impunidad.

La postura promulgada desde la oficialidad es que los estudiantes fueron asesinados por narcos de Iguala,  e incinerados en un basurero en medio del monte, una tesis que ha sido controvertida por el Grupo Interdisciplinario de de Expertos Independientes (GIEI) compuesto por 5 especialistas designados por la CIDH, quienes tras analizar pruebas e investigar por cuenta propia concluyeron que el gobierno no contaba con evidencias concretas para sustentar sus argumentos.

El GIEI, tampoco coincidió en la causa de la masacre.  La explicación del Gobierno fue que la policía detiene, por orden del alcalde corrupto, a los jovenes para los entregarlos a narcos quienes confundiéndolos con narcos rivales, optan por asesinarlos y quemarlos en una pira de neumáticos y madera, arrojando sus cenizas a un río. Los expertos negaron que los estudiantes fuesen quemados en el basurero y resaltó que el batallón militar de la zona vio la persecución y detención de los estudiantes.

Los especialistas solicitaron entrevistarse con los soldados del batallón, petición que nunca fue concedida, para finalmente irse de México acusando al Gobierno de obstruir el caso. “Dentro del aparato del Estado hay fuerzas que no quieren que se investigue la verdad. Son fuerzas estructurales”, afirmo el español Carlos Beristáin, integrante del grupo.

**Violencia en incremento.**

Adicionalmente, la violencia en Iguala en el transcurso de los últimos dos años no ha disminuido, de acuerdo con el portal "Animal Político"  los asesinatos en el municipio ubicado al norte de Guerrero, han repuntado hasta en un 45% tras la desaparición de los estudiantes. Citando los registros del Sistema Nacional de Seguridad Pública, en 2015, el primer año que Iguala fue vigilada por fuerzas estatales y federales, fueron asesinadas 105 personas, lo que representa un incremento de 45% en el número de homicidios, con respecto al año anterior; las muertes por arma de fuego, por ejemplo, se incrementaron un 136%.

En lo que respecta al presente año la tendencia continua. durante el periodo enero-agosto fueron asesinadas en Iguala 96 personas, el número de homicidios más alto para este periodo en los últimos seis años y aunque en la estadística aún falta registrar los últimos cuatro meses del año, en este 2016 Iguala ha sufrido más asesinatos con arma de fuego, comparado con años anteriores, al sumarse 58 víctimas hasta agosto pasado. Además, en el periodo 2015-2016, medio centenar de personas han sido baleadas, aunque salvaron la vida, lo cual contrasta con los 28 casos que se registraron en el periodo 2013-2014.
