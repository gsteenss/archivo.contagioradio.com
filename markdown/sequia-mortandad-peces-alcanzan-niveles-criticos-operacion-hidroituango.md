Title: Sequía y mortandad de peces alcanzan niveles críticos por operación de Hidroituango
Date: 2019-01-21 14:16
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Empresas Públicas de Medellín, Hidroituango, Movimiento Ríos Vivos, Río Cauca
Slug: sequia-mortandad-peces-alcanzan-niveles-criticos-operacion-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 21 ene 2019 

[Tras el cierre de unas de las compuertas en el megaproyecto Hidroituango, las comunidades y organizaciones sociales de los municipios de los alrededores, en Antioquia, han denunciado registros de anormalidades que han afectado a las poblaciones en río abajo de la obra hidroeléctrica **como la sequía y la mortalidad de peces, además de los niveles críticos de agua en el río Cauca.**]

Según Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, se registraron incidentes de sequía y mortandad de peces en municipios río abajo de Hidroituango después que se completara la operación para cerrar la compuerta 2 en la casa de máquinas el 16 de enero. Hechos que contradicen las declaraciones de Empresas Públicas de Medellín (EPM). Seis horas después del procedimiento, el gerente de EPM Jorge Londoño de la Cuesta declaró que las comunidades podrían estar tranquilas dado que los caudales del río habían regresado a niveles normales para esta época del año.

En respuesta, Zuleta confirmó que se ha reportado un interminable número de peces muertos, así como el desasbesticimiento de agua en áreas cerca del proyecto, como el Cañon del Río Cauca, además de zonas lejanas, en los municipios Caucasia y Ciénaga. Zuleta agregó que EPM, junto a la Defensa Civil, el Ejército y la Policía, les prohibió el acceso al río a los pescadores durante y después de la operación, hechos que harían parte de un esfuerzo de EPM para obstaculizar la toma de evidencias de los efectos negativos de la operación.

Zuleta manifestó que estos efectos socioambientales, sumados a la grieta que se ha reportado en el proyecto hidroélectrico, no son "normal", como había declarado el gerente de EPM, y que en cambio, **las comunidades siguen viviendo bajo riesgos graves de Hidroituango.**

### **Movimiento Ríos Vivos Antioquia: "Nos dejaron completamente solos"** 

En agosto, la Contraloría General de la Nación publicó un informe que señalo a EPM y la Autoridad Nacional de Licencias Ambientales (ANLA) por la crisis de Hidroituango. Sin embargo, Zuleta informó que el Movimiento Ríos Vivos Antioquia no ha conocido de un seguimiento de la Contraloría al informe; tampoco, se ha pronunciado la Procuraduría General ni la Fiscalía General frente a este informe.

[(Le puede interesar: EPM "perdió el control sobre Hidroituango": Contraloría)](https://archivo.contagioradio.com/epm-perdio-el-control-sobre-hidroituango-contraloria/)

"No solo sentimos que el Gobierno nos ha dejado solos con este monstruo encima, con este riesgo permanente que todos los días crece, sino que también todo el Estado colombiano," dijo Zuleta.

Además, Zuleta agregó que los desplazados por la represa, quienes hoy se encuentran albergados en las oficinas de EPM en Sabanalarga  y en el coliseo de Ituango desde hace más de seis meses, no han recibido atención de la administración municipal de Ituango, el alcade de Ituango ni el alcalde de Sabanalarga para encontrar nuevas viviendas y medios de sustento.

Ante estos hechos, el Movimiento Ríos Vivos Antioquia ha decidido consultar a expertos independientes para determinar los "riesgos reales" a que las comunidades están sometidos y determinar albergues más seguros para las poblaciones desplazadas.

<iframe id="audio_31650496" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31650496_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
