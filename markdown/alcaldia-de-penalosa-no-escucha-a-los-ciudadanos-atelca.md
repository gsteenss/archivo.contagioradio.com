Title: No claudicará la defensa de la ETB: SIndicatos
Date: 2017-03-01 12:53
Category: DDHH, Nacional
Tags: ETB, revocatoria Peñalosa
Slug: alcaldia-de-penalosa-no-escucha-a-los-ciudadanos-atelca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [1 Mar 2017] 

Sindicatos afirman que el Cabildo Abierto que se realizó ayer, sirvió para **ratificar el proceso de revocatoria hacía el alcalde Enrique Peñalosa y para continuar defendiendo a la ETB como empresa pública de Bogotá**. El próximo encuentro para finalizar el Cabildo se realizará el 7 de marzo.

Los trabajadores expresaron que era una ofensa  que el presidente de la ETB, Jorge Castellanos, mencionara que los empleados son **“sumamente costosos y privilegiados”,** cuando en realidad esos **beneficios no representan ni el 10%** de los ingresos de la ETB y que a ellos se les descuenta el 4% de lo devengado para aportar a esos beneficios, afirmo Alejandra Wilches, presidenta de ATELCA.

Además la actitud que demostró Peñalosa durante el encuentro fue “sobrada” pasando por alto el carácter participativo de este escenario al dar como un hecho la venta de la ETB, afirmación que según Wilches **“demuestra la clase de Alcaldía que quiere desarrollar Peñalosa sin escuchar a los ciudadanos”**. Le puede interesar: ["Ya se han recolectado 200 mil firmas para revocar a Pañalosa"](https://archivo.contagioradio.com/ya-se-han-recolectado-200-mil-firmas-para-la-revocatoria-de-penalosa/)

Las personas asistentes al  cabildo también denunciaron que **funcionarios y administrativos de la Alcaldía fueron citados desde las 7 de la mañana, dos horas antes del inicio del espacio**, para ocupar el auditorio e imposibilitar la entrada de la ciudadanía. Le puede interesar: ["Listos formatos de recolección de firmas para revocar a Peñalosa"](https://archivo.contagioradio.com/listos-formatos-de-recoleccion-de-firmas-para-revocatoria-de-penalosa/)

Actualmente ATELCA lleva tres procesos de demanda frente a la decisión de venta de la ETB, el primero es **contra el Alcalde y 35 de los concejales que votaron para la venta, por presunto prevaricato**, hay otra denuncia por presunto pánico económico y por administración desleal.

De igual modo, sindicatos como Sintrateléfonos, la Central Unitaria de Trabajadores y ATELCA, anunciaron una próxima **movilización y la instauración de más mecanismos jurídicos para evitar la venta de la ETB.**

<iframe id="audio_17300433" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17300433_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
