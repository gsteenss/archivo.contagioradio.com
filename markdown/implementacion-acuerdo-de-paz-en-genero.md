Title: ¿Cómo va la implementación del acuerdo de paz en temas de género?
Date: 2018-07-24 17:53
Category: Mujer, Paz
Tags: acuerdo de paz, cerac, CINEP, enfoque de género
Slug: implementacion-acuerdo-de-paz-en-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Jul 2018] 

En el marco del "Seminario Avances y Retos en la Implementación del Acuerdo Final", Consuelo Corredor, como vocera de la Secretaría Técnica del Componente de Verificación Internacional (STCVI) presentó el informe semestral de la implementación del enfoque de género en los acuerdos de paz, en el que se evidencia que **menos del 50% de la reglamentación jurídica tiene incorporado el enfoque de género.**

En dicha presentación Corredor, quien es Coordinadora del equipo del Centro de Investigación y Educación Popular/Programa Por la Paz (CINEP/PPP), y Jorge Restrepo, Director del Centro de Recursos para el Análisis de Conflictos (CERAC), reconocieron la dificultad para evaluar el acuerdo de paz y dejaron ver su preocupación sobre algunos puntos claves que, podrían afectar el desarrollo del posacuerdo.

Para Corredor, la Ley de Procedimiento de la JEP, que incluyó 3 modificaciones entre las que se encuentra la eliminación del enfoque de género, es una situación preocupante, pues **desconoce las afectaciones diferenciales y específicas que han sufrido las mujeres, así como los miembros de la población LGBTI en el conflicto colombiano.** (Le puede interesar:["Aún se puede defender el espÍritu de la JEP"](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/))

Según la coordinadora del CINEP, los enfoques territoriales, étnicos y de género son una de las grandes innovaciones de este acuerdo de paz; y es una de las razones por las que **"Colombia está en las agendas internacionales, como un caso a seguir y de análisis de construcción de paz, y ya no para analizar la violencia"**. Por esta razón, en el informe de la STCVI reconoce que el enfoque de género "manifiesta la construcción social en torno a los discursos sobre las diferencias normativas entre hombres y mujeres, implicando una relación de poder entre ambos".

### **La incorporación jurídica del enfoque de género es del 48%** 

Según el Informe de la implementación del enfoque de género en los Acuerdos de Paz, de 35 decretos expedidos gracias a las facultades otorgadas por el Acto Legislativo 1 de 2016, 17 incorporan disposiciones que corresponden con el enfoque de género. De otra parte, de los 5 Actos Legislativos y 6 Leyes aprobadas vía 'Fast Track', 5 contienen elementos correspondientes al enfoque de género.

**Estos datos revelan que del total de 46 Decretos, Actos Legislativos y Leyes, 22 incluyen el enfoque diferencial y de género,** lo que equivale a un 48 %. El informe añade que "de los 7 Proyectos de Ley radicados en el segundo periodo de la cuarta y última legislatura, 6 incorporan disposiciones correspondientes a la inclusión del enfoque de género".

Estas cifras señalan que aunque se ha avanzado en el proceso de implementación, los hechos constatan las dificultades a las cuales se enfrenta el acuerdo de paz y, en especial la inclusión del enfoque de género, que seguirá siendo un terreno en disputa de diversos sectores que se niegan a reconocer las afectaciones diferenciadas que sufrieron mujeres y miembros de la comunidad LGBT en medio del conflicto armado colombiano.

[Final Primer Informe Implementacion Enfoque de Género STCVI Junio 2018](https://www.scribd.com/document/384614974/Final-Primer-Informe-Implementacion-Enfoque-de-Genero-STCVI-Junio-2018#from_embed "View Final Primer Informe Implementacion Enfoque de Género STCVI Junio 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_95796" class="scribd_iframe_embed" title="Final Primer Informe Implementacion Enfoque de Género STCVI Junio 2018" src="https://www.scribd.com/embeds/384614974/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-eX89ICHra7tO4uXhVtPp&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
