Title: Se creó Mesa Nacional de Tierras para Afectados por Represas
Date: 2015-08-03 12:02
Category: DDHH, Nacional
Tags: Defensoría del Pueblo, Derechos Humanos, Desplazamiento, despojo de tierras, ESMAD, INCODER, Movimiento Ríos Vivos, procuraduria
Slug: se-creo-mesa-nacional-de-tierras-para-afectados-por-represas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Rios-Vivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Asoquimbo 

##### <iframe src="http://www.ivoox.com/player_ek_5912707_2_1.html?data=lp6elJyUe46ZmKiakp2Jd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9PZwsnOjbLJt8Kfr8bQy9TSpc2fxcqYts7JttPV1JDdw9fFb6Laxsjhw8nTt4zk0NeYtMrUto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Isabel Cristina Zuelta, Movimiento Ríos Vivos] 

###### [3 Agosto 2015]

Tras la gira del Movimiento Ríos Vivos en Bogotá donde se realizó una reunión con funcionarios del INCODER, se creó la Mesa Nacional de Tierras para afectados y afectadas por represas, teniendo en cuenta que **las 125 represas que hay en todo el país han dejado menores posibilidades a los campesinos de acceder a la tierra.**

**Del 27 al 31 de julio** una delegación del Movimiento Ríos Vivos conformada por representantes de los afectados por las represas de El Quimbo, Ituango, Sogamoso, Urrá y Salvajina, realizaron una gira en Bogotá con el objetivo de dar a conocer las problemáticas que se viven en las regiones cuando se intervienen los ríos para construir represas, para luego llegar a sus regiones con avances en la protección de sus derechos y soluciones al despojo de tierras.

Es por eso, que el **Movimiento se reunió con el INCODER y presentó sus argumentos y denuncias frente al despojo de tierras** que afecta a las comunidades cuando se desarrolla un mega proyecto hidroeléctrico, causando problemáticas sociales, económicas y de violaciones a los derechos humanos, ya que las comunidades basan sus actividades productivas en torno a  los ríos.

Tras escuchar los planteamientos de Ríos Vivos, se concluyó crear la **Mesa Nacional con el fin de encontrar un mecanismo o marco jurídico** a través del cual las comunidades que han perdido su actividad productiva por la construcción de represas puedan acceder a tierras y de esta manera restablecer sus derechos al trabajo y la vida digna.

**El INCODER quedó como la institución estatal encargada** de verificar y llevar a cabo este proceso en compañía de otras entidades del gobierno necesarias para garantizar a los afectados el derecho a la tierra, es por eso que para la apertura de la mesa el próximo 25 de agosto se convocó a los Ministerios de Agricultura, Hacienda, Ambiente e Interior.

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, afirma que en términos generales la gira en Bogotá fue muy importante para fortalecer el proceso interno del colectivo, sin embargo, resalta que los organismos de control como **la Procuraduría y la Defensoría del Pueblo no quisieron hablar con los representantes de Ríos Vivos**. Por su parte la Autoridad Nacional de Licencias Ambientales, **ANLA, tampoco quiso reunirse con el movimiento** diciendo que “no le interesaba dialogar con Ríos Vivos sobre el tema de violaciones a derechos humanos”, explica Zuleta.
