Title: Empate técnico en referendo reeleccionista en Bolivia
Date: 2016-02-22 15:03
Category: El mundo, Política
Tags: Bolivia, Estado Plurinacional, Evo Morales
Slug: empate-tecnico-en-referendo-reeleccionista-en-bolivia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/referendo_bolivia_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: noticiaaldia] 

###### [22 Feb 2016]

Hasta el momento y sin resultados oficiales por parte del organismo electoral de Bolivia acerca de los resultados del referendo que se realizó este fin de semana en ese país, se ha conocido, por sondeos a boca de urna, que el NO ganaría por un margen muy estrecho, incluso sectores del gobierno **hablan de un empate técnico, debido a que en los conteos actuales no se está teniendo en cuenta el voto rural ni el de los bolivianos en el extranjero.**

Por su parte Katu Arkonada, integrante de la Red de intelectuales en defensa de la humanidad, ha afirmado que las voces que dan por ganador al NO, son todas voces de oposición que han estado integradas en lo que llamó **“la campaña más sucia” realizada en los últimos años en Bolivia**, por la serie de ataques mal intencionados dirigidos desde la oposición hacia el gobierno de Evo Morales. Por lo tanto hasta no tener el consolidado de un reconteo no se puede cantar victoria.

Sin embargo, luego de conocerse los resultados finales hay dos escenarios posibles. Uno en que el presidente Morales pueda presentarse de nuevo a la reelección a finales de 2019, en este caso el gobierno tendría que dedicarse a gobernar y a seguir construyendo los cambios que han hecho de Bolivia un ejemplo a seguir en lo que tiene que ver con la redistribución de la riqueza.

El otro escenario, si gana el NO, en el que el gobierno tendría, por una parte, evaluar los errores que se cometieron en la campaña y el mismo ejercicio de gobierno, pero por otra parte, prepararse para los ataques que provendrían desde el mismo sector de oposición, pero también asegurar **un relevo urgente del proceso del partido MAS y que asuma los avances que se han tenido hasta el momento como una bandera de campaña.**

Para Katu Arkonada, este es uno de los principales retos de los gobiernos “progresistas” de América Latina, por tanto **no se han logrado consolidar procesos sociales capaces de renovar sin traumatismos y con el mismo respaldo y apoyo popular a los nuevos liderazgos.**
