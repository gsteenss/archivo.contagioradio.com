Title: Paramilitares obligan a comunidad a asistir a reunión en Cacarica, Chocó
Date: 2017-06-06 18:02
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, cacarica
Slug: paramilitares-obligan-a-comunidad-a-asistir-a-reunion-en-cacarica-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [06 Jun 2017] 

Son dos las denuncias de actuaciones a plena luz del día, de paramilitares integrantes de las denominadas Autodefensas Gaitanistas de Colombia en el territorio colectivo del Cacarica. Por una parte, el pasado Domingo ingresaron a la comunidad del Limón y **convocaron a una reunión obligatoria con la comunidad de la Ecoaldea de Paz, Nueva Esperanza, antes Zona Humanitaria.**

Por otra parte, se denuncia que desde el pasado lunes por lo menos 6 paramilitares, vestidos de civil y portando armas cortas, se establecieron en la Zona Humanitaria Nueva Vida, **sin que la exigencia de la comunidad para que se retiren haya sido efectiva**. Le puede interesar: ["Con lista en mano paramilitares amenazan a pobladores de Cacarica"](https://archivo.contagioradio.com/paramilitares-amenazan-pobladores-cacarica-36208/)

Los dos hechos se presentan en medio de la presencia militar a menos de 2 horas de los lugares humanitarios y a pesar de que las denuncias con ubicación concreta, dan cuenta de la presencia paramilitar, y el control territorial que están ejerciendo los armados en ese territorio colectivo de comunidades negras. **No se presenta ningún control por parte del Fuerza Pública**.

La denuncia realizada por la organización de Derechos Humanos, Comisión de Justicia y Paz, también señala que desde la llegada de los paramilitares al territorios, **hace 5 meses, se han estado presentado operaciones de control y de coptación social** como el reclutamiento de niños y jóvenes, la organización de fiestas comunitarias como el dia de la madre y la entrega de obsequios a las familias.

Todo ello facilitado por al ausencia histórica del Estado y la falta de control militar efectivo a pesar de la evidente presencia de paramilitares y la fuerte militarización de la Zona. Le puede interesar:["Cerca de 650 paramilitares ejercen control territorial en Cacarica"](https://archivo.contagioradio.com/650-paramilitares-ejercen-control-en-cacarica-35568/)

En la denuncia realizada también se expresa que los hombres auto identificados como integrantes de las AGC, **afirmaron que con ellos llevarían el progreso a las comunidades y los mantendrían bajo su protección**.

A esta situación se suman las dificultades para la comunicación y la ausencia de respuestas a las medidas de la Unidad Nacional de Protección, que impiden que las comunidades activen las alertas tempranas. Le puede interesar: ["Comunidades de Cacarica conmemoran 20 años de la operación Génesis"](https://archivo.contagioradio.com/20-anos-renaciendo/)

###### Reciba toda la información de Contagio Radio en [[su correo]
