Title: Así participaron las mujeres en el Paro Cívico de Buenaventura
Date: 2017-06-16 11:19
Category: Libertades Sonoras, Mujer
Tags: buenaventura, mujeres, Paro de Buenaventura
Slug: las-mujeres-en-el-paro-civico-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Mujeres-Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [14 Jun. 2017] 

Las mujeres han tenido siempre un papel protagónico en los procesos de reivindicación de derechos de las comunidades y Buenaventura en el departamento del Valle del Cauca (Colombia), no ha sido la excepción. **En el último paro cívico que duro 22 días, las mujeres estuvieron siempre al frente,** ocupando espacios en las mesas de diálogo con el Gobierno  Nacional y de los procesos que se desarrollaron en las calles de ese distrito.

**En \#LibertadesSonoras** quisimos saber cuál fue el papel que tuvo la mujer en este paro de viva voz de **Danny Ramírez, secretaría técnica de la Mesa por la Ley 1257 de 2008, feminista, defensora de DD.HH de Buenaventura** y activista. Le puede interesar: [La violencia contra las mujeres es una pandemia: mujeres en el Senado](https://archivo.contagioradio.com/desde-el-senado-piden-renuncia-del-concejal-ramon-cardona/)

Además, realizamos todo un recorrido por lo que sucedió durante esos 22 días, las actividades, las reivindicaciones de las mujeres y cómo va a seguir el movimiento feminista luchando por el acceso a sus derechos.

<iframe id="audio_19305501" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19305501_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
