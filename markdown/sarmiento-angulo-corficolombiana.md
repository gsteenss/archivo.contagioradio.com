Title: ¿Sarmiento Angulo desconocía los millonarios sobornos pagados por Corficolombiana?
Date: 2019-01-24 15:34
Author: AdminContagio
Category: Nacional, Política
Tags: audiencia, corrupción, Odebrecht, Sarmiento Angulo
Slug: sarmiento-angulo-corficolombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/593071_1-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [24 Ene 2019] 

Este jueves se reanudó la audiencia sobre el caso **Odebrecht en Colombia**, y tras dos sesiones de interrogatorios a testigos, se han logrado importantes revelaciones sobre el primer soborno que pagó la multinacional brasileña en conjunto con **Corficolombiana** para obtener el contrato de la Ruta del Sol II. Pese a que se han aclarado algunos asuntos, quedan varios cuestionamientos en la opinión pública como **el papel de Luis Carlos Sarmiento Angulo y Luis Carlos Sarmiento Gutiérrez (hijo), así como del fiscal Nestor Humberto Martínez en esta trama de corrupción.**

**Las audiencias son el inicio del juicio contra el expresidente de Corficolombiana José Elías Melo**; y en la primera de las sesiones, **Luiz Antonio Bueno Junior**,  ex-directivo de Odebrecht para Colombia, afirmó que luego de llegar al país tuvo acercamientos con el Grupo Aval, y a través de Andrés Uriel Gallego (ministro de transporte en 2009) y su viceministro, **Gabriel García Morales**, empezaron a sostener múltiples reuniones en el apartamento de Juan Manuel Barraza, en donde se habría configurado el primer soborno.

Con este dinero que fue pagado al ex-viceministro Gabriel García Morales, se aseguraba que contrato de la Ruta del Sol II fuera adjudicado a Odebrecht/Corficolombiana y no a los demás oferentes. (Le puede interesar:["Escándalo Odebrecht salpica a familia Sarmiento y al Banco de Bogotá"](https://archivo.contagioradio.com/audiencias-de-caso-odebrecht-salpican-al-sarmiento-gutierrez-y-banco-de-bogota/))

Posteriormente, Luiz Bueno sostuvo que Odebrecht desembolsó los **6,5 millones** **de dólares** y le dijo a Elías Melo que se cruzarían cuentas de los dineros a partir de los contratos de las vías, frente a lo que Melo respondió que él tendría que escalar toda la operación para programar los pagos con Luis C. Sarmiento Gutiérrez. En ese momento no fue claro si efectivamente Sarmiento se reunió con Melo para tratar el tema, pero Melo sí manifestó que el pago del soborno se hizo a García Morales, y **el dinero se movió en Colombia gracias al Banco de Bogotá**.

### **El miércoles hablaron García Morales y Enrique Ghisays** 

Durante la audiencia del pasado miércoles comparecieron el exministro Gabriel García Morales, quien también está preso por co-hecho y celebración indebida de contratos, y **Enrique Ghisays**, quien está detenido por lavado de activos y enriquecimiento ilítico. García Morales dijo que conoció a Elias Melo en Colombia en una reunión realizada en la Casa de Nariño con Marcelo Odebrecht, en la que también está **Juan Manuel Barraza** y Luiz Bueno.

Adicionalmente, García reconoce que él no ayudo a crear los pliegos de la Ruta del Sol II, pero sí ayudo a favorecer las adjudicaciones en favor de Odebrecht/Corficolombiana, y sostuvo que **la multinacional brasileña tuvo injerencia en esos pliegos**. El ex-viceministro explicó que **Victoria Guarín, quien era cónyuge de Daniel García (alto funcionario del Grupo Aval)**, era el puente de Odebrecht con la corporación internacional consultora del gobierno Uribe encargada de la estructuración de los pliegos para la Obra.

Versión que concuerda con una columna de Daniel Coronell de 2017, en la que denunciaba que los pliegos de la Ruta del Sol estaban hechos a la medida de Odebrecht/Corficolombiana. García Morales también afirmó que solicitó a Corficolombiana un concepto jurídico al abogado **Hugo Palacios** para amparar la decisión del comité evaluador de la Ruta del Sol, que por decisión unánime adjudicó el contrato al consorcio Colombo-brasileño. Sin embargo, **el concepto jurídico fue pedido el 14 de diciembre y al día siguiente estaba listo, lo que significa que estaba previamente preparado**.

Por su parte, Enrique Ghisays fue la persona recomendada ante Odebrecht para mover el dinero del Soborno (\$ 6,5 USD) desde Andorra hasta Panama, aunque hasta el momento no se tiene certeza del destino de ese dinero. Además, durante la audiencia quedó claro que Odebrecht giró el dinero, pero era Corficolombiana la encargada de hacer ese pago, razón por la que **durante la construcción de la Ruta del Sol II se firmaron contratos adyacentes con los que la filial del Grupo Aval pagó a Odebrecht "el préstamo"**.

### **El papel de los medios, ¿el cuarto poder?** 

Durante su indagación, García Morales aseguro que lo contactaron periodistas de otros medios off the record (sin revelar la fuente) para favorecer el consorcio: lo que hacían dichos periodistas era ir en contra de OHL, empresa que también competía para obtener los pliegos. García expuso que también se reunió en el apartamento de un funcionario público del Gobierno (sin aclarar qué funcionario) con **José Fernando Hoyos, quien era periodista de Semana, y llevaba un dossier de argumentos contra OHL para evitar que ganara el contrato**.

En ese momento, la idea era apoyar el licitante que finalmente obtuvo la adjudicación de la obra mientras se desprestigiaba a OHL. Como lo resalta **Maria Fernanda Carrascal,** **una de las líderes del colectivo \#RenuncieFiscal**, esa influencia sobre los medios logró su cometido y aún hoy, sigue siendo necesario investigar qué periodistas y funcionarios estuvieron influyendo la opinión pública en favor del consorcio constructor de la Ruta del Sol II. (Le puede interesar: ["Con cacerolas sigue el movimiento por la renuncia del Fiscal Néstor Martínez"](https://archivo.contagioradio.com/prepare-la-cacerola-para-exigir-este-17-de-enero-la-renuncia-del-fiscal-nestor-martinez/))

Investigación que tiene relevancia si se tiene en cuenta que, como lo señala Carrascal, se está tratando de inculpar únicamente a Elias Melo mientras se evita hablar de Luis Carlos Sarmiento padre e hijo, resultando en una pregunta relevante: **¿es posible que un empleado como Melo pueda sacar 6,5 millones de dólares a las espaldas de sus jefes Luis Carlos Sarmiento Angulo y Luis Carlos Sarmiento Gutiérrez?** (Le puede interesar: ["Las razones para pedir nulidad en elección de Néstor Martínez"](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
