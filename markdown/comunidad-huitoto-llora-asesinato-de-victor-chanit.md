Title: Comunidad Huitoto atribuye al Ejercito Nacional el asesinato de Victor Chanit
Date: 2019-09-25 13:01
Author: CtgAdm
Category: DDHH
Tags: asesinato, denuncia pública, Líder Indígena, Resguardo Indígena
Slug: comunidad-huitoto-llora-asesinato-de-victor-chanit
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/AGUAS-NEGRAS-2.pdf" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comunidad-Huitoto-Victor-Chanit.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivoparticular] 

 

Por medio de una [Denuncia Pública la Organización Nacional de los Pueblos Indígenas de la Amazonía Colombiana ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/AGUAS-NEGRAS-2.pdf) (OPIAC), denunciaron la desaparición forzada y posterior asesinato de Victor Manuel Chanit Aguila, y hacen responsable al Ejercito Nacional de estas acciones cometidas en el resguardo Indígena de Bajo Aguas Negras Caqueta de la Jurisdicción del municipio de Solano, Caqueta. (Le puede interesar: [Comunidades indígenas denuncian incremento de la violencia en Tierradentro, Cauca](https://archivo.contagioradio.com/cabildos-asesinatos-tierradentro-cauca/))

En la denuncia cuentan de manera enumerada como fue la llegada de hombres armados al resguardo, el 20 de septiembre y como en medio de la salida de estos del territorio y la acumulación de personas se llevaron a Chanit, en contra de su voluntad, “sin justificación alguna”, describen también como se inició un proceso de búsqueda por parte de la comunidad, y como el  22 de septiembre ,  lo encontraron sin vida en una platanera cercana.

Según Oscar Daza Coordinador de Derechos Humanos de  la OPIAC, en esta denuncia los habitantes del Resguardo Indígena manifiestan que el potencial responsable es el ejército, " se evidenciaron rastros de botas militares cerca a la casa de Mayor Victor Manuel Chanit, además en el lugar donde se encontró el cuerpo, a la distancia había un grupo de militares, que en medio de burlas veían como eran recogidos los restos, como diciendo ahí está , ahí se lo dejamos”.

Así mismo reconocen en el comunicado la preocupación generada por estos hechos, que han dejado un ambiente de luto e intranquilidad en la comunidad, y exigen al Estado colombiano, la Gobernación del Caquetá, la Defensoría del Pueblo y a la Fiscalia General de la Nación, responsabilidad ante acciones que afecten la integridad de las comunidades, cumplimiento de los artículos que los reconocen como pueblo indígena y el desarrollo de acciones legales hacia los gestores de este crimen.

 

> *“el pueblo MURUI MUNINA ( HUITOTO) reconocido por la Corte Constitucional como un pueblo indígena en riesgo de extinción física y cultural, por lo cual se deben fortalecer los esfuerzos institucionales para su protección, exigimos respeto por nuestros pueblos y el cumplimiento de los artículos que declaran respectivamente que “El Estado reconoce y protege la diversidad”- Denuncia Pública emitida por los* Pueblos Indígenas de la Amazonía Colombiana

### **Quién fue Victor Chanit  **

Victor Chanit tenía 63 años, era dirigente indígena y se desempeñó como gobernador de 2005 a 2010. Por más de 20 años fue líder de su territorio asumiendo responsabilidades en el consejo de ancianos para hablar sobre temas como el estado de su comunidad y en los últimos meses se dedicaba al manejo espiritual y a la agricultura. (Le puede interesar: [&\#171;No nos dejen solos&\#187;, la petición de líderes sociales del Chocó ante amenazas)](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/)

Según Garay su perdida deja un vacío cultural, espiritual y social dentro de su comunidad; al igual que un dolor en sus ocho hijos y esposa, “nosotros nos sentimos solos, él era el que nos guiaba y motivaba cada día a enfrentar las diferentes labores y sentarnos en nuestra maloka a meditar sobre ello”, afirman los integrantes de su comunidad. (Le puede interesar:[Asesinato de Gobernador y líder indígena enluta al Pueblo Awá](https://archivo.contagioradio.com/awa-luto-asesinato-de-lideres/))

Según Carlos Garay Coordinador de Derechos Humanos de ASCAINCA luego de esta situación **el Resguardo indígena se ha desplazado hacia Las Aguas, en el departamento del Putumayo, porque temen regresar a su territorio** **mientras exista presencia de fuerza pública**, "nadie quiere volver por eso  no se ha podido realizar el ritual en la maloka de despedida del cuerpo".

En manos de la Fiscalía está la investigación que permita determinar quienes fueron los gestores de este crimen;al mismo tiempo Robinson López representante de la OPIAC ante la Coordinadora de las Organizaciones Indígenas de la Cuenca Amazónica (COICA), presentó en las últimas horas en la Conferencia Anual de la Alianza del Clima, una lectura de la denuncia pública, presentando ante estos entes internacionales el caso del asesinato de  Víctor Chanit.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
