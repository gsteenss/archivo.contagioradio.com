Title: El despojo de tierras en el Bajo Atrato sigue vigente
Date: 2020-05-21 17:37
Author: CtgAdm
Category: DDHH, Nacional
Tags: #BajoAtrato, #DerechosHumanos, #Despojo, #Paramilitares, #Uraba
Slug: el-despojo-de-tierras-en-el-bajo-atrato-sigue-vigente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/informe-platano083116_153.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La región del Bajo Atrato ha sido históricamente una de las más golpeadas por el despojo de tierras. Fenómeno que en este territorio se encuentra acompañado por la alianza estratégica entre **grupos paramilitares, sectores empresariales y avales de la institucionalidad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta entrevista la abogada I**sabel Velásquez**, especialista en temas de tierras e integrante de la Comisión de Justicia y Paz, narra los hechos de despojo en el Bajo Atrato, acción que inicia con la arremetida violenta de la estructura paramilitar del Bloque Elmer Cárdenas en la década de los 90 y bajo el auspicio de la Brigada XVII de Carepa, al mando del entonces general Rito Alejo del Río. (Le puede interesar: "[Ordenan investigar penalmente a empresas bananeras del Urabá antioqueño](https://www.justiciaypazcolombia.com/ordenan-investigar-penalmente-a-empresas-bananeras-del-uraba-antioqueno/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A partir de esa arremetida de violencia y plan de despojo se han generado daños al tejido social **y graves afectaciones a los ecosistemas de selva en el Bajo Atrato**. (Le puede interesar: ["En Caño Manso la dignidad enfrenta el despojo"](https://archivo.contagioradio.com/en-cano-manso-la-dignidad-enfrenta-al-despojo/))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F914291682344229%2F&amp;show_text=false&amp;width=734&amp;appId=1237871699583688&amp;height=413" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->
