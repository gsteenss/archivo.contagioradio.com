Title: Campesinos e indígenas están en riesgo permanente: Misión internacional Vía Campesina
Date: 2017-11-27 13:24
Category: Nacional, Paz
Tags: acuerdos de paz, misión internacional, verificación de los acuerdos de paz, Vía Campesina
Slug: campesino-e-indigenas-estan-en-riesgo-permanente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/victimas_desaparecidos_derechos_humanos-e1511807474298.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Zona Cero] 

###### [27 Nov 2017] 

Habiendo culminado la segunda Misión Internacional de verificación de la implementación de los Acuerdos de Paz, que realizó la Vía Campesina en diferentes territorios del país, los integrantes de la misión pudieron constatar las preocupaciones que tiene en general la comunidad internacional; **hay retrasos que ponen en riesgo a las comunidades.**

De acuerdo con Fausto Torres, integrante de la Vía Campesina de Nicaragua, pudieron evidenciar **el riesgo en el que viven las comunidades campesinas y los indígenas** teniendo en cuenta que no se ha avanzado en lo que tiene que ver con la implementación de la Reforma Rural Integral que se pactó en la Habana. Dijo que “hay inseguridad ciudadana y amenazas en donde los campesinos demandan atención”.

Adicionalmente, las comunidades pudieron manifestarle a la misión las diferentes preocupaciones en torno a temas vitales como **la falta de agua potable en los territorios, las carencias en la atención en salud y las amenazas a los líderes sociales**. Afirmó que a los ex combatientes de las FARC no se le ha otorgado las garantías de seguridad que requieren por lo que, al igual que los campesinos, continúan en condiciones precarias. (Le puede interesar: ["Sin implementación del Acuerdo "el mundo rural está condenado a desaparecer": Vía Campesina"](https://archivo.contagioradio.com/sin-implementacion-del-acuerdo-el-mundo-rural-esta-condenado-a-desaparecer-via-campesina/))

Para la misión internacional, fue extraño conocer de primera mano que **“los campesinos se sentían más seguros cuando las FARC andaban por el territorio**”. Torres confirmó que esto indica que “la Policía y el ejército tienen que hacer una labor muy fuerte para garantizar la seguridad ciudadana”.

### **Tumaco continúa bajo la incertidumbre** 

En la visita a Tumaco en Nariño, la misión internacional constató que **aún hay mucha incertidumbre** a raíz de los hechos sucedidos en el mes de octubre donde fueron asesinados 6 campesinos que buscaban impedir la erradicación forzada de los cultivos de hoja de coca. “La gente no quería salir ni de día ni de noche por la preocupación, el Gobierno debe crear una comisión para estar pendiente de las comunidades, que exigen respuestas y atención”, indicó el integrante de la Misión.

Frente a este panorama, aseguró que el Gobierno debe generar acciones donde se le garantice a las poblaciones **la satisfacción de sus derechos básicos** y donde la sustitución se haga de manera gradual “en el marco de un proceso que busque que el productor, que va a dejar los cultivos ilícitos, tenga la posibilidad de cultivar un producto que le ayude con sus necesidades básicas”. (Le puede interesar: ["La paz territorial no se siente en los territorios"](https://archivo.contagioradio.com/la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional/))

### **Recomendaciones de la misión internacional** 

Esta misión, tras haber realizado las observaciones en terreno, realizarán un **observatorio permanente con la Vía Campesina en Colombia** “en donde queremos que haya una mayor presencia de las autoridades del Gobierno en los territorios”. Además, buscarán que se resuelvan las necesidades prioritarias como la falta de agua y las garantías de seguridad para las poblaciones.

Finalmente, Torres indicó que **harán un acompañamiento al movimiento campesino colombiano**. Sugirió que es necesario que se fortalezcan las organizaciones campesinas para que se pueda crear un comité de veeduría por comunidad y así intentar que las instituciones que están encargadas de implementar los acuerdos de paz, cumplan con esto.

<iframe id="audio_22332388" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332388_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
