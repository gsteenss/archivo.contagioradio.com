Title: Nuevo Código de Policía es "totalmente arbitrario"
Date: 2016-05-05 12:50
Category: DDHH, Nacional
Tags: alirio uribe, código de policía, ESMAD, posconflicto
Slug: nuevo-codigo-de-policia-es-totalmente-arbitrario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/aptopix-colombia-unrest.jpeg-0675f-copia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: APTOPIX 

###### [5 May 2016]

En plenaria del Senado fueron aprobados los más de 300 artículos del nuevo Código de Policía y Convivencia, que ahora pasará a ser estudiado por la Cámara de Representantes, donde algunos congresistas como Alirio Uribe, esperan presentar varias modificaciones pues algunos sectores califican este nuevo Código como **represivo, arbitrario y dictatorial.**

Este proyecto de Ley impulsado por el gobierno nacional y presentado por el senador Germán Varón del partido Cambio Radical, plantea normas de convivencia que resultan necesarias, pero tiene otras que, de acuerdo con el represente Alirio Uribe, no son coherentes con un escenario de posconflicto.

**Dar facultades a la policía para disolver una protesta social, allanamientos sin orden judicial, detenciones arbitrarias, y el uso del espacio público,** entre otras disposiciones, son algunos de los puntos más polémicos de este nuevo Código.

**“A punta de pata y bolillo se piensan defender el espacio público sin tener en cuenta que el 51% del empleo en Bogotá es informal.** El espacio público no es del alcalde es de todos los ciudadanos”, dice el congresista frente a la posibilidad que da este nuevo Código para que se pueda desalojar de cualquier forma a quienes viven de las ventas ambulantes.

Uribe Muñoz, asegura que está de acuerdo con cambiar el Código de Policía, "pues el actual es un decreto de Estado de sitio”, sin embargo, ve con preocupación esta propuesta, pues es “totalmente arbitraria” teniendo en cuenta que la policía no está en capacidad de hacerlo cumplir como se quisiera, y en cambio se volvería un arma para violar los derechos humanos de la ciudadanía. “**A punta de este Código no puede garantizarse una sana convivencia”, expresa.**

Para el estudio del proyecto de Ley en la Cámara de representantes, **Alirio Uribe, pedirá que se elimine el ESMAD,** “que causa disturbios y violaciones a los derechos humanos”. Además espera que sea un proyecto que de hunda para poder analizar mejor la creación de un nuevo Código de Policía coherente con la paz.

<iframe src="http://co.ivoox.com/es/player_ej_11422896_2_1.html?data=kpahlJecfZehhpywj5WdaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpCajbfJtNPZ1Mrb1sbSuMafwpDZw5CnaaSnhqaez8bWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
