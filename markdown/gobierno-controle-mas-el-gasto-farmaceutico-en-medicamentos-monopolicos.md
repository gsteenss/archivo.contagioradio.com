Title: Gobierno: controle más el gasto farmacéutico en medicamentos monopólicos
Date: 2017-03-02 11:04
Category: Mision Salud, Opinion
Tags: Medicamentos, sistema de salud
Slug: gobierno-controle-mas-el-gasto-farmaceutico-en-medicamentos-monopolicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/medicamentos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Reuters 

#### **Por: [Germán Holguín - Misión Salud ]** 

###### 2 Mar de 2017

[La entrada en vigencia de la Ley Estatutaria de Salud el mes pasado es un acontecimiento esperanzador y preocupante.]

Esperanzador, porque esta ley reconoce que la salud es un derecho fundamental autónomo, extiende la cobertura del sistema de salud a todos los servicios requeridos por el paciente, con excepción de aquellos que sean excluidos expresamente, y otorga a los médicos autonomía para prescribir los servicios que requiera el paciente según su diagnóstico. Así se pretende garantizar el goce efectivo de la salud, el mayor bien de todo ser humano.

Preocupante, debido a que la aplicación de esta ley incrementará el desequilibrio financiero que hoy padece el sistema de salud, ya que presumiblemente implicará costos adicionales de \$2 billones anuales.

En este horizonte, para que la Ley Estatutaria  no produzca un efecto bumerán es indispensable controlar al máximo el gasto farmacéutico, en la seguridad de que así los recursos del sistema son suficientes. De lo contrario se producirá la ruina del sistema.

Entre las venas rotas del gasto farmacéutico que es necesario cauterizar, hay dos imprescindibles: i) La prescripción irresponsable de medicamentos y ii) Los precios exorbitantes de los medicamentos monopólicos.  (Le puede interesar: [Cerca de 700 mil personas mueren en la región por falta de acceso de medicamentos](https://archivo.contagioradio.com/700-mil-personas-mueren-en-la-region-por-falta-de-acceso-de-medicamentos/))

En relación con la primera, de lo que se trata es de equilibrar la autonomía médica con rigurosos  códigos de autorregulación,  para abolir la prescripción bajo estímulos económicos. En Estados Unidos uno de cada cuatro médicos recibe dinero de las grandes farmacéuticas a cambio de recetar sus medicamentos costosos en vez de versiones genéricas de igual eficacia terapéutica pero más económicas. En Colombia no hay estadísticas sobre el particular pero se presume que la situación no es menos delicada. En respuesta a ello, La ley Estatutaria prohíbe a los actores privados el otorgamiento de dádivas a profesionales de la salud.

Es obvio que sin control a esta prohibición y sin autorregulación médica, la autonomía disparará aún más el consumo irracional de medicamentos de alto costo.

En cuanto a la segunda de las venas rotas, para nadie es un secreto que los medicamentos monopólicos registran hoy unos precios “astronómicos”. Ningún otro calificativo cabe a tratamientos para el cáncer, la artritis o la diabetes, por ejemplo,  cuyo precio oscila entre 50.000 y 250.000 dólares/paciente/año. Precio que no puede asumir ningún sistema de salud sin poner en riesgo su estabilidad.

Para contrarrestar este escándalo, el gobierno tiene el deber de aplicar de manera sistemática todas las medidas a su alcance para regular el precio de los medicamentos monopólicos, como son:

-   [Establecimiento de topes máximos para el recobro de medicamentos.]
-   [Control de precios a partir de la referenciación internacional (Circular 03/2013).]

[Estas dos medidas ahorraron al presupuesto nacional \$1,6 billones entre 2011 y 2014. No obstante, la segunda no se aplica desde mayo de 2014.]

-   [Control de precios para medicamentos declarados de interés público (Circular 03/2016). Sólo se ha aplicado al Glivec® (2016) como resultado de una gestión adelantada por Misión Salud, Ifarma y CIMUN.   ]
-   [Control de precios con base en la evaluación del aporte terapéutico de los nuevos medicamentos.]
-   [Negociaciones centralizadas de precios. Únicamente Colombia Compra Eficiente ha utilizado este instrumento para la compra de medicamentos para el tratamiento de la hemofilia y la enfermedad renal crónica;]
-   [Ejercicio pleno del derecho-obligación de otorgar licencias obligatorias para proteger la salud pública de los efectos nocivos de las patentes farmacéuticas y allanar el camino a los medicamentos genéricos con precios asequibles. El gobierno nunca ha ejercido este derecho.  ]

[El derecho de acceso a medicamentos necesarios para la salud y la vida está por encima de los intereses comerciales.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
