Title: Continúan las capturas contra integrantes de Congreso de los Pueblos
Date: 2018-06-08 13:16
Category: DDHH, Nacional
Tags: Andrés Gil, congreso de los pueblos, presos politicos
Slug: continuan-las-capturas-contra-integrantes-de-congresos-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/congrso-de-los-pueblos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Jun 2018] 

El pasado 6 de junio, Congreso de los Pueblos denunció la captura ilegal de su secretario técnico Julian Andrés Gil, cuando salía de la sede de esta plataforma. De acuerdo con la organización, tanto los cargos que se le imputan como **el procedimiento en si mismo, presentan irregularidades que buscan criminalizar la protesta social.**

De acuerdo con Fabian Laverde integrante de Congreso de los Pueblos, Gil denunció que la detención se dio de formas completamente ilegales, "sobre las 2 de la tarde, en la sede nacional de Congreso de los Pueblos, se presentan varias personas de civil, sin ninguna identificación, acompañadas por dos policías uniformados", posteriormente rodean a Gil que se encontraba en su motocicleta. Otras personas que se encontraban en el lugar le exigieron a la policía y a quienes se encontraban allí que se identificaran. **Minutos después las personas se colocaron las chaquetas de la SIJIN.**

De igual forma, Laverde expresó que el traslado de Gil también tuvo irregularidades, ya que el vehículo en el que se realizó  no portaba ninguna identificación de la SIJIN o la Policía. Sumado a ello, la audiencia de legalización se hará en el municipio de Viani, Cundinamarca, cuando la captura se realizó en Bogotá**. Debido a que el juez que ejecutará el proceso se encuentra en este lugar. **

Previamente a esta situación, Gil, también denunció que había sido víctima de extraños seguimientos por parte de personas vestidas de civil, que además habrían hecho un registro fotográfico de sus movimientos.

### **Los delitos contra Andrés Gil** 

El procedimiento fue efectuado por la SIJIN y la Fiscalía, que le imputó cargos por rebelión, porte de armas y municiones y el delito de receptación. Para Laverde, los delitos que se imputaran a Gil buscan criminalizar la labor de defensa de derechos humanos y la protesta social, **"consideramos que esto hace parte de una redada, de una campaña en contra del movimiento social, de los liderazgos sociales"**.

Frente al porte de armas, Laverde aclaró que durante la captura no se decomisó ningún arma en flagrancia y se desconoce cual es el delito al que se refiere la Fiscalía con la receptación. (Le puede interesar: "Atentan contra la vida de la defensora de DDHH María Ruth Sanabria.

Meses atrás se había presentado la captura de otros 30 integrantes de esta organización en el territorio de Tumaco. Razón por la cual, Marilen Serna vocera de Congreso de los pueblos expresó que están siendo víctimas de una persecución política desde el Estado. (Le puede interesar: ["Por persecución judicial se presentan líderes del Congreso de los Pueblos ante la Fiscalía"](https://archivo.contagioradio.com/persecucion-judicial-congreso-de-los-pueblos/))

<iframe id="audio_26434774" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26434774_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
