Title: Paramilitares arremeten de nuevo contra reclamantes de Las Guacamayas
Date: 2017-02-15 14:40
Category: DDHH, Nacional
Tags: Asesinatos de líderes comunitarios, Guacamayas Turbo, Presencia Paramilitar
Slug: paramilitares-arremeten-nuevo-reclamantes-las-guacamayas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Reclamantes-Guacamayas-1-1068x601.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Feb 2017] 

Las comunidades que habitan en la vereda Las Guacamayas en zona rural de Turbo Antioquia, denunciaron que el en la madrugada del miércoles, 2 hombres desconocidos llegaron a la casa de la familia Payares y amenazaron de muerte a la **líder Luzmila Payares, quien logró escaparse y no se tiene información concreta de su paradero.**

Julio León, líder comunitario manifestó que la situación es insostenible y que “han sido muy repetitivas las amenazas contra la gente del Concejo Comunitario”, afirmó que no es un hecho nuevo, pues el Estado sabe que desde el 2007 los campesinos habían tomado la iniciativa de regresar a sus tierras, **“sin recibir acompañamiento o garantías por parte de las instituciones”**. ([Le puede interesar: Familias confinadas en sus fincas por ataques paramilitares en](https://archivo.contagioradio.com/familias-confinadas-en-sus-fincas-por-ataques-paramilitares-en-guacamayas/)Guacamayas)

### Las comunidades ya habían denunciado 

León advierte que desde septiembre del año pasado, cuando las comunidades estuvieron sitiadas en la vereda por presencia paramilitar, han realizado **distintas denuncias ante instituciones estatales “y no nos dicen nada, mire lo que pasó con el compañero Porfirio, lo mataron y ya habíamos denunciado”**. ([Le puede interesar: "Asesinato de reclamante de tierras “pudo haberse evitado”](https://archivo.contagioradio.com/asesinato-de-reclamante-de-tierras-pudo-haberse-evitado/))

Advirtió que tras las amenazas, hostigamientos y asesinatos hay fuertes intereses de empresarios y grupos al margen de la ley, **interesados en que no se restituyan las tierras “porque son ellos los que están empoderados  y atacan a los campesinos”.**

Señaló que las denuncias y los casos no pueden seguir quedando en la impunidad, “no existen investigaciones que hayan dado con los responsables, intelectuales o materiales de lo que viene pasando en esta región”.

Por último, indicó que las Fuerzas Militares presentes en la zona “saben lo que esta pasando y tampoco hacen nada”, enunció que es **urgente la presencia de las autoridades competentes y que la Unidad Nacional de Protección “atienda nuestro llamado y proteja nuestros líderes”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
