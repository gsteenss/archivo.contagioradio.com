Title: El regreso a Hiroshima narrado por Kaneto Shindo
Date: 2015-08-06 14:14
Author: AdminContagio
Category: 24 Cuadros, Cultura
Slug: el-regreso-a-hiroshima-narrado-por-kaneto-shindo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/9_PFM16_RVB_0162.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [6, Ago, 2015] 

Tan solo 7 años después de los ataques nucleares sobre las ciudades japonesas de Hiroshima y Nagasaki, el director Kaneto Shindó, llevó a la pantalla "Gembaku no ko" (Los niños de Hiroshima), un film basado en una colección de cuentos y poemas escritos por jóvenes supervivientes de la Bomba atómica.

La cinta, ambientada en el año 1949, es el relato del regreso de Takako Ishikawa (Nobuko Otowa), una joven profesora a su lugar de nacimiento, destruído por la bomba atómica 4 años antes. Buscando entre las ruinas a sus amigos, compañeros y honrando la memoria de sus padres y su hermana menor asesinados en el ataque.

Con la producción de Kindai Eiga Kyokai / Mingei, la música de Akira Ifukube, y la dirección de fotografía de Takeo Ito, "Los niños de Hiroshima" fue presentada en Cannes como nominada al gran premio del Festival en 1952 y obtendría en 1955 el premio UN de la Academia Británica.

Shindó, nacido en Hiroshima en 1912, fue guionista, productor y director artístico, sus trabajos posteriores a Gembaku no ko, tuvieron un espíritu claramente antibelicista, buscando educar a los jóvenes educaba a los jóvenes mostrándolos la devastación que producían las armas atómicas.

Aunque su nombre no es tan popular como los de Akira Kurosawa, Mizoguchi o Shohei Imamura, la obra de Shindo merece un reconocimiento dentro de la Historia del Cine Universal; por la influencia del estilo humanista del maestro Kenji Mizoguchi, para quien escribió dos guiones.

<iframe src="https://www.youtube.com/embed/ruawAX6-tyQ" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Reparto : Nobuko Otowa, Osamu Takizawa, Niwa Saito, Tsuneko Yamanaka, Shinya Ofuji, Takashi Ito, Chikako Hosokawa, Masao Shimizu, Yuriko Hanabusa, Tanie Kitabayashi, Tsutomu Shimomoto, Eijirô Tôno, Taiji Tonoyama, Jûkichi Uno.
