Title: 2020 una oportunidad para unificar y concentrar las exigencias sociales: ONIC
Date: 2020-01-21 15:50
Author: CtgAdm
Category: Movilización, Nacional
Tags: Movilización, ONIC, Paro Nacional
Slug: 2020-oportunidad-unificar-exigencias-sociales-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EK-bjDpXYAYONNi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ONIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de la reactivación del Paro Nacional este 21 de enero que simboliza el regreso a las calles de la ciudadanía el consejero mayor de la **Organización Nacional Indígena de Colombia (ONIC), Luis Fernando Árias** se refirió a la agenda de la movilización para este 2020 y la trascendencia y aportes que los pueblos originarios tendrán a lo largo del año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el dirigente, esta nueva jornada es una oportunidad para seguir rechazando las políticas de un Gobierno que "ha tenido una actitud desafiante, con quienes nos hemos movilizado" y que ha demostrado que no tiene la voluntad política de sentarse a negociar, "lo que se viene es un pulso de fuerzas entre el establecimiento en cabeza del Gobierno y quienes nos movilizamos permanentemente".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El consejero fue enfático en que para que exista una mayor cohesión en la movilización, es necesario escoger puntos claves que sirvan de insignia para el movimiento que pasó de exigir 13 puntos a 104 en su pliego de peticiones. [(Le puede interesar: La defensa de la vida, un punto central del Paro Nacional)](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se refirió en especial a casos ejemplares como la movilización indígena en Ecuador, donde, su accionar junto al de la ciudadanía lograron dar marcha atrás y que derogar el paquetazo del presidente Lenin Moreno que buscaba eliminar el subsidio a los combustibles. Un ejemplo que afirma, deben seguir los movimiento sociales en Colombia [(Lea también: Movimiento indígena logra derogar el decreto 883 en Ecuador)](https://archivo.contagioradio.com/movimiento-indigena-logra-derrogar-paquetazo-en-ecuador/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Ha faltado mayor coordinación interna y una vocería unificada, este ha sido un proceso diverso y plural con distintas expresiones, es necesario poder organizar, tenemos que definir punto específicos, como pasó en Ecuador". Afirma que el error está en pretender alcanzar una agenda tan amplia, "deberíamos coger uno o dos asuntos de las peticiones y hasta que no echemos eso para atrás la movilización se mantiene".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido señaló que estos puntos esenciales para nutrir la movilización deberían centrarse en **la prohibición del Fracking, la fumigación con glifosato que hace parte del cumplimiento del Acuerdo de Paz, lo que es transversal a la protección de las vidas de comunidades, indígenas, líderes sociales, defensores y defensoras de DD.HH,** "son cosas elementales, que nos convienen a todos, son elementos sencillos pero que tienen una implicación de carácter nacional enorme". [(Le recomendamos leer: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo)](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En contexto, las cifras según la ONIC

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Es necesario resaltar que según la ONIC, del 7 de agosto de 2018 al 5 de noviembre de 2019, se han dado **un total de 18.956 agresiones a la defensa de los derechos humanos, un total de 134 homicidios, [49 afectaciones al territorio](https://www.onic.org.co/comunicados-regionales/3543-presencia-de-actores-armados-en-resguardos-ponen-en-alto-riesgo-a-comunidades-indigenas-de-antioquia) y 504 amenazas**, que denotan la necesidad de exigir la protección de la vida de los pueblos originarios y en general de los territorios donde han sido asesinados 23 líderes sociales en los primeros 20 días del año.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Movimiento indígena, clave para la movilización en el 2020

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a la fortaleza que ha demostrado la comunidad indígena en el continente y que ha sido crucial para el progreso de las manifestaciones, Árias expresó que con la presencia de este sector, se continuará realizando una presencia simbólica e inspiradora para la movilización. [(Le recomendamos leer: Los tres puntos de organizaciones y procesos ambientales en el paro nacional)](https://archivo.contagioradio.com/los-tres-puntos-de-organizaciones-y-procesos-ambientales-en-el-paro-nacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Evidencia del poder de convocatoria indígena que estuvo al frente de este proceso en la Minga Nacional en marzo y abril del 2019, sumado a la coyuntura actual y "al proceso de levantamiento de los procesos populares" es necesario trabajar por la unidad, "es un proceso que irá en ascenso, y tendremos una capacidad igual o mayor a la que tuvimos el año pasado, que se irá consolidando no solo a nivel Bogotá sino a nivel nacional", auguró el coordinador de la ONIC.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46857317" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46857317_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
