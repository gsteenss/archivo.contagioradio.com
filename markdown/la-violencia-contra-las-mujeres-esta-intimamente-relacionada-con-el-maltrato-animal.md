Title: A un paso de ser aprobada en el Congreso ley 172 que protege los animales
Date: 2015-11-25 13:32
Category: Animales, Nacional
Tags: Juan Carlos Losada, Juan Manuel Galán, Maltrato animal, Natalia Parra, Proyecto de Ley 172
Slug: la-violencia-contra-las-mujeres-esta-intimamente-relacionada-con-el-maltrato-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/animalista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9506009_2_1.html?data=mpqdmJWUfY6ZmKiakpeJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksLowtHWw5C0pdPmwoqfpZDFp9Xd187g1saJdqSf0cbQy8vNt9XVjN6Yw9PNscLgytjhw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, Plaforma ALTO] 

Este miércoles, la plenaria del Senado del Congreso de la República realizará el cuarto y último debate del proyecto de Ley 172, con ponencia del senador Juan Manuel Galán, con el que se daría paso a la penalización del maltrato animal, **“dándole un nuevo estatus a los considerados como seres sintientes”,** afirma el **representante a la cámara Juan Carlos Losada, autor del proyecto.** De pasar en esta debate continuará  su paso a la sanción presidencial y a  revisión de la corte constitucional.

Coincidencialmente este mismo día se conmemora en todo el mundo el Día de  NO Violencia contra las Mujeres, en ese marco, de acuerdo con Natalia Parra, directora de la Plataforma ALTO y secretaria técnica de la bancada animalista, “la violencia contra las mujeres está íntimamente relacionada con las violencias hacia los animales”, lo que a su vez  significa que “ambas luchas no están aislada.

Con esta Ley las conductas como **el maltrato animal, la zoofilia, los tratos crueles y denigrantes serían castigadas.** Además, se reformaría el Estatuto Nacional de Protección de Animales con el objetivo de actualizar el valor de las multas y facilitar la competencia de las autoridades de policía. Para su cumplimiento se le otorgaría a la Policía Nacional la facultad de realizar aprehensiones preventivas de animales que están siendo objeto de maltrato.

En ese sentido, cabe recodar las palabras de Randall Lockwood, psicólogo y vicepresidente Humane Society of the USA, quien dice “**No todo individuo que haya maltratado a animales acabará siendo un asesino en serie, pero casi todos los asesinos en serie cometieron actos de crueldad con animales"*.***

*Cabe recodar que  días atrás, miles de colombianas y colombianas han mostrado su apoyo al proyecto de Ley, con* \#**APOYOLEYANIMAL172**, solicitando a los senadores su voto a favor de esta Ley.

La animalista, también asegura que penalizar el maltrato animal, es un paso importante hacia la paz que se espera firmar para marzo de 2016, “**Es hora de que este país evolucione en ambos sentidos (los derechos de las mujeres y el respeto hacia los animales)”**, concluye Natalia Parra.
