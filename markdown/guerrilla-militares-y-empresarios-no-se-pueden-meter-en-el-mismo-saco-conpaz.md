Title: “Guerrilla, militares y empresarios no se pueden meter en el mismo saco” CONPAZ
Date: 2015-02-16 17:29
Author: CtgAdm
Category: Otra Mirada, Paz
Tags: Cesar Gaviria, Conpaz, Conversaciones de paz en Colombia, ELN, FARC, Juan Manuel Santos, Perdon y olvido
Slug: guerrilla-militares-y-empresarios-no-se-pueden-meter-en-el-mismo-saco-conpaz
Status: published

###### Foto: runrun.es 

Según la organización de Comunidades Construyendo Paz, CONPAZ, la **responsabilidad en la guerra no puede recaer en la misma proporción en las guerrillas, en los militares y en los empresarios**, puesto que cada uno tiene razones de ser diferentes. Además hay una propuesta muy clara en torno a que las propias comunidades deben ser las que definan la manera de aplicar los castigos a los responsables de la violación o la victimización.

El castigo debe estar acorde con el daño causado, puntualiza una de las integrantes de CONPAZ, que **“la cárcel no arregla a nadie”** por ello, hacen énfasis en la propuesta de que sean **las propias víctimas las que apliquen los castigos**, esa sería una forma propia de justicia transicional.

Las comunidades CONPAZ, también consideran que el perdón y el olvido no son compatibles con sus propuestas de verdad, justicia y reparación integral, puesto que la memoria es la garantía de que no se repitan las situaciones victimizantes y violatorias de los Derechos Humanos.

Otro de los aspectos de evaluación que hacen, de cara a la propuesta del expresidente Cesar Gaviria, tiene que ver con los empresarios, afirman que **no se pueden establecer bases para que los empresarios sean acogidos dentro de la justicia transicional** sin que se conozcan todos los elementos de su participación e instigación en los crímenes cometidos por ellos a través de los grupos paramilitares. **No es lo mismo un empresario financiador del desplazamiento forzado que una guerrilla**, o no son lo mismo las violaciones a los DDHH de cometidas por los militares que las acciones de las guerrillas.

En el mismo marco de las propuestas CONPAZ afirma que si se piensa en un nuevo orden en los territorios, **no se podrían permitir inversiones de empresas ligadas a violaciones a los Derechos Humanos**, pero para ello es imprescindible el reconocimiento de responsabilidad a través de la verdad y la reparación integral de los daños causados.

Así las cosas, desde los sectores de las víctimas y de las organizaciones de DDHH es de rechazo total a una propuesta que, según analistas terminaría siendo juzgada con el paso del tiempo, tal y como ha sucedido en otros procesos de “perdón y olvido” como en Argentina o Brasil con la reciente actuación de la comisión de la verdad que ha desprendido en una serie de investigaciones contra políticos y militares.
