Title: Profesores de la Universidad Autónoma de Colombia llevan 3 meses sin salario
Date: 2019-05-30 13:21
Author: CtgAdm
Category: Educación, Entrevistas
Tags: maestros, paro, profesores, Universidad Autónoma de Colombia
Slug: profesores-universidad-autonoma-meses-sin-salario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Educación-univerdidad-Autónoma-de-Colombia-huelga.-blu_radio_paro_uniautonoma_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Leonardo Agudelo  
] 

Para los profesores y trabajadores de la **Fundación Universidad Autónoma de Colombia (FUAC)** la crisis tocó fondo, según denuncian, la administración de la Institución no paga seguridad social desde diciembre de 2018 y les adeuda salarios desde marzo. Por esta razón, los profesores de la Universidad decretaron el pasado lunes 20 de mayo la hora cero para el paro de actividades, para lograr el cumplimiento de las obligaciones por parte de las directivas de la Institución.

Según explicó el profesor de la Universidad, Osvaldo Morantes, las irregularidades en los pagos se vienen presentando desde hace cerca de dos años; pero la situación se agravó **a finales de 2018, cuando cesó el pago de seguridad social para los trabajadores de la Universidad.** A ello se sumó en el presente año el incumplimiento en el pago de salarios para docentes desde febrero, y para trabajadores desde marzo.

Morantes señaló que antes de optar por la huelga acudieron a instancias legales, pidiendo al Ministerio de Trabajo (MT) y de Educación Nacional (MEN) intervenir, para encontrar soluciones a la situación y evitar afectaciones a los más de 4 mil estudiantes de la FUAC. **El Ministerio de Trabajo hizo una solicitud de información a las directivas de la Institución, sin embargo, no fue atendida.** (Le puede intersar: ["Maestros a paro este jueves en Bogotá"](https://archivo.contagioradio.com/maestros-paro-jueves-bogota/))

### **La situación será expuesta en el congreso el próximo miércoles 5 de junio**

Ante el silencio de las directivas, y del MEN, desde el pasado lunes 20 de mayo los trabajadores de la Universidad decretaron la hora cero del paro. En el marco de esta jornada de huelga, este jueves, cerca de las instalaciones de la FUAP (calle 12 con carrera 4) se desarrollarán distintas actividades, entre las que se encuentran la visita a los profesores por parte de la senadora Aída Avella.

Adicionalmente, **el próximo miércoles 5 de junio en la Comisión Sexta del senado se desarrollara un debate de control político** citado por congresistas de la bancada alternativa, en el cual están citadas Maria Víctoria Angulo (ministra de educación) y Alicia Arango (ministra de trabajo), para pedirles que tomen medidas sobre el manejo que se está dando a la Autónoma. (Le puede intersar: ["Con panfletos amenazan a líderes estudiantiles en Universidad de Antioquia"](https://archivo.contagioradio.com/con-panfletos-amenazan-a-lideres-estudiantiles-en-universidad-de-antioquia/))

[Comunicado de Huelga Universidad Autónoma de Colombia](https://www.scribd.com/document/411955087/Comunicado-de-Huelga-Universidad-Autonoma-de-Colombia#from_embed "View Comunicado de Huelga Universidad Autónoma de Colombia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_14713" class="scribd_iframe_embed" title="Comunicado de Huelga Universidad Autónoma de Colombia" src="https://es.scribd.com/embeds/411955087/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-YAxklWuOZDbP7OACA7wh&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
