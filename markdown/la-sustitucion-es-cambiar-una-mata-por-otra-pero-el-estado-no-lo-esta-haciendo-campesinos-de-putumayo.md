Title: "La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo 
Date: 2019-06-11 18:22
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Abuso de fuerza ESMAD, Erradicación Forzosa, PNIS, Puerto Asís, Putumayo
Slug: la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/hojasdecoca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Desde el 28 de mayo, efectivos del ESMAD hacen presencia en la vereda **La Cumbre, en Puerto Asís, Putumayo**, erradicando a la fuerza los cultivos de coca de los campesinos que se acogieron al **Programa Nacional Integral de Sustitución (PNIS)** y quienes aún esperan la entrega de los proyectos productivos prometidos por el Gobierno. Ante este incumplimiento, desde este 11 de junio, la comunidad optó por una movilización pacífica exigiendo la suspensión de la erradicación forzosa y el cumplimiento del acuerdo firmado por el campesinado.

"Sustituir es cambiar una mata por otra mata, pero el Estado no lo están haciendo, simplemente están erradicando y dejando a los campesinos a la deriva" expresó **Marco Rivadeneira, presidente de la Asociación de Campesinos de Puerto Asís** quien agregó que no hay presencia del Gobierno en el territorio.

"Nosotros hemos manifestado nuestra posición, pedimos que se hagan diálogos de concertación vereda por vereda para que el campesino sea quien decida, él es el que conoce el territorio y sabe lo qué se puede cosechar" indicó Rivadeneira exaltando que la economía requiere de un tiempo para ser cambiada,  teniendo en cuenta que la población campesina del Putumayo lleva 40 años cultivando coca.  [(Lea también: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo)](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

Pese a que se realizó una verificación por parte de la Defensoría del Pueblo y Naciones Unidas, los enfrentamientos entre la comunidad y el ESMAD continúan en este municipio, mientras esperan a que el Gobierno acuda a dialogar, "los campesinos cumplieron y el Gobierno no lo ha hecho", sentenció el líder campesino.  [(Le puede interesar: Reportan asesinato del campesino Alneiro Guarín en Puerto Asís, Putumayo)](https://archivo.contagioradio.com/reportan-asesinato-del-campesino-alneiro-guarin-en-puerto-asis-putumayo/)

<iframe id="audio_37018618" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37018618_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
