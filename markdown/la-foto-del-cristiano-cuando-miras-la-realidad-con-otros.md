Title: "La foto del Cristiano": Cuando miras la realidad con otros 
Date: 2015-07-16 07:00
Category: Opinion, Ricardo
Tags: Israel, Kibutz Nir Yitzhack, Mezquita Al-Aqsa, Palestina
Slug: la-foto-del-cristiano-cuando-miras-la-realidad-con-otros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Jerusalen-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)  -  **@ferr\_es] 

###### 16 Jul 2015

Tomé esta fotografía en Jerusalén el 4 de junio de 1998. En la Ciudad Vieja, para llegar al Barrio Judío, debía pasar el control militar israelí. Justo donde está el “Check Point” se ve esta imagen tan icónica. Me sorprendió mucho que mis amigos hebreos pasaran de largo, sin usar sus cámaras...

¡Clic! Como estaba muy ufano con mi foto, se la mostré a mi amigo Jaim Shaní, del Kibutz Nir Yitzhack y su respuesta burlona me sorprendió: ***Ricardo, simplemente hiciste la foto del cristiano****. Ni al  judío ni al musulmán les gusta esa imagen.  Solo a un cristiano, como vos, se le ocurre hacer  esa toma.*

Para mí la composición era perfecta: al fondo, el Domo de la roca \[1\], la Mezquita Al-Aqsa \[2\] y en primer plano, el Muro de los lamentos o Kótel \[3\]. El Muro hace límite entre el Barrio Judío y el Barrio Musulmán \[4\], es decir, todo un resumen de historia y geopolítica.

“Ricardo, el problema es que los judíos solo quieren ver el Kótel,  y los musulmanes solo quieren ver su mezquita y el Domo de la roca”.

Un periodista israelí, me explicó la dureza de las **visiones excluyentes** y la raíz cultural del conflicto en el medio oriente: “Veo lo mío y no veo al otro, que no existe o no debería existir. Y mientras pensemos así, no habrá paz”. “Los judíos y los musulmanes convivieron pacíficamente en Toledo”, insistía el colega.

En Israel – Palestina, escuché versiones del conflicto, muy centradas en argumentos culturales y religiosos. Los líderes de Israel reclaman el derecho a vivir en paz pero olvidan la expulsión violenta de millones de palestinos, la supuesta “imposibilidad” de su retorno, el asedio y asesinato de los que se quedaron, el robo de tierras, el absoluto control del agua del Jordán \[5\] y la omisión criminal de la ONU frente a Jerusalén \[6\].

**Los negocios de la paz.** Por esos días había algo de paz entre Israel y su vecinos, como resultado de los acuerdos firmados en Camp David \[7\], entre Anwar Al-Sadat y Menachem Bejín en septiembre de 1978 \[7\]. Coincidía también que en 1998 el mundo cristiano estaba a pocos meses de celebrar 2000 años del  nacimiento de Jesús y había mucha actividad turística. Los hoteles y las aerolíneas estaban haciendo un gran negocio.

Aprendí entonces que los conflictos nunca se quedan en el plano local y pasan fronteras. Por tanto, las mediaciones internacionales, si son de buena fe, son bienvenidas. Sobre los líderes, aprendí que no deben ser eternos y que deben ceder su legado al ciudadano común. Solamente cuando el vecino más humilde pueda disfrutar de su casa, familia, trabajo y vejez tranquila, podremos hablar de un nuevo tiempo. Entonces veremos fotos, periódicos y postales viejas y las mostraremos a los niños: “**Esta fotografía es del tiempo en que había guerra. Los de este lado, no querían ver** **a** **los del otro lado**”...

###### [\[1\] Cúpula de la roca:] 

###### [     <https://es.wikipedia.org/wiki/C%C3%BApula_de_la_Roca>] 

###### [\[2\] Al-Aqsa: La Lejana.  المسجد الاقصى, *Al-Masyid Al-Aqsa*] 

###### [     <https://es.wikipedia.org/wiki/Mezquita_de_Al-Aqsa>] 

###### [\[3\] Muro de los lamentos: **הַכֹּתֶל הַמַעֲרָבִי**, Hakótel Hama'araví*,* o Muralla Occidental.] 

###### [     <https://es.wikipedia.org/wiki/Muro_de_las_Lamentaciones>] 

###### [\[4\] Cuatro sectores o barrios dentro de la Ciudad Vieja de Jerusalén: Judío, Cristiano, Musulmán y Armenio.] 

###### [\[5\] Río Jordán: Nace en Líbano, va por Siria (Región del Golán ocupado por Israel), Jordania, Israel y Palestina. Es decir, que afecta a cinco países. Israel  controla el agua del río Jordán y la ONU omite intervenir.    ] 

###### [\[6\] La ONU omite hacer cumplir el acuerdo fundacional del Estado de Israel en 1948, que definía un estatuto espacial para Jerusalén: Una ciudad neutral y bajo mandato de una fuerza internacional que garantice el libre acceso de judíos, cristianos y musulmanes.] 

###### [\[7\] Acuerdos de paz de Camp David:] 

###### [     <http://www.historiasiglo20.org/TEXT/campdavid1978.htm>] 

######  
