Title: Colombia ¿un país libre de discriminación?
Date: 2015-06-02 15:52
Category: closet, LGBTI
Tags: Bogotá, Camila Acosta, Cartagena, Cinthya de la Espriella, Cundinamarca, Derechos Humanos, Discriminación, Homosexualidad, LGBTI, Medellin, Viviana Romero
Slug: colombia-un-pais-libre-de-discriminacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/IMG-20150602-WA0027.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_4585422_2_1.html?data=lZqll5mWdo6ZmKiakpaJd6Kpkoqgo5eZcYarpJKfj4qbh46kjoqkpZKUcYarpJK91NTLtsLhwpCfl5DIqYzBwt7cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Programa 25 de Mayo  
] 

En Closet Abierto, se ha hecho un recorrido a nivel internacional para resaltar el respeto o, por el contrario, la vulneración de los derechos de la comunidad LGBTI. Ahora se abre un espacio a nivel nacional para hablar acerca de las costumbres y culturas colombianas, con el fin de demostrar la forma en la que esto puede influir para rechazar la homosexualidad.

Cinthya de la Espriella de Cartagena, Viviana Romero de Vergara, Cundinamarca y Camila Acosta de Medellín hablan acerca de la cultura de sus lugares nativos, haciendo énfasis en el machismo arraigado al que están acostumbrados, sus formas de vestir, comportamientos, apariencias, e incluso hasta las clases sociales y creencias religiosas, factores que son claves en la aceptación de la homosexualidad.

Se realiza además una comparación de sus lugares natales versus la ciudad en la que viven actualmente que en este caso es Bogotá. Las tres invitadas coinciden en que es un lugar en el que definitivamente existe discriminación, pero hay mucha más libertad de expresión, a diferencia de lugares pequeños en los que la cultura domina a tal punto de no permitirles sentirse independientes y autónomos para decidir sobre su orientación sexual.
