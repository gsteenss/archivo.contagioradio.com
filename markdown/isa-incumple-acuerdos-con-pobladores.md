Title: ISA incumple acuerdos con pobladores de San Jorge, Córdoba
Date: 2018-07-27 16:36
Category: DDHH, Nacional
Tags: Campamento Humanitario, Cerromatoso, Comunidad de Paz de San José de Apartadó, desempleo, San Jorge
Slug: isa-incumple-acuerdos-con-pobladores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-27-a-las-1.55.58-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @jperezdelgado] 

###### [27 Jul 2018] 

Ante los altos indices de desempleo regional, y la baja porción de mano de obra local contratada por la empresa de infraestructura eléctrica Intercolombia, filial de ISA, habitantes del Alto San Jorge, en Córdoba, han decidido establecer un Campamento Humanitario, reclamando que se vincule laboralmente a un mayor número de pobladores de la región.

Según Erika Benavides, representante del Campamento Humanitario, aunque la mano de obra local está calificada y la empresa **tiene más de 500 empleados, menos del 20% de ellos son vecinos del sector**, hecho que afecta la situación de ocupación en la región.

La protesta, se lleva a cabo por el incumplimiento de dos acuerdos previos en los que la empresa se comprometió a vincular un mayor porcentaje de la población en su zona de influencia. Adicionalmente, las personas que están contratadas no cuentan con garantías de seguridad laboral, hecho que conlleva riesgos para la salud de los trabajadores.

Benavides señala que pese a los incumplimientos y los atropellos que han vivido por parte tanto de la empresa como de la policía, que ha utilizado gas pimienta en su contra, la protesta se mantiene con la intención de diálogo y de forma pacífica, evitando alterar la prestación del servicio de luz del departamento.

De acuerdo con la vocera del Campamento Humanitario, en el lugar se encuentran más de 300 personas oriundas de la provincia de San Jorge entre las que se encuentran indígenas, afros y trabajadores; sin embargo, Intercolombia contrata a ciudadanos chinos y venezolanos, que no cuentan como mano de obra calificada.

<iframe id="audio_27272377" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27272377_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
