Title: Esperamos que "Estado invierta en educación los recursos que se necesitan"
Date: 2017-06-06 12:27
Category: Educación, Entrevistas
Tags: educacion, fecode, maestros, Paro de maestros
Slug: fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/18985304_1338919136229206_2011021108_n-copia-e1496769126958.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 jun 2017]

Ante la falta de acuerdos con el Ministerio de Educación, **Fecode aceptó la mediación del Procurador General Fernando Carrillo,** como facilitador en las negociaciones con el Gobierno Nacional. Maestros y maestras de todo el país se movilizan hoy en Bogotá para exigirle una propuesta seria que garantice la financiación de la educación en el país.

Hace dos días, el gremio de maestros del sector público Fecode, rechazó la propuesta de la Ministra de Educación Yaneth Giha, **de designar un mediador con base en el Decreto 160 de 2014** referente a procesos de negociación y solución de controversias. Para Fecode, “esta figura decretada sería fría y decidiría por encima de las partes”. Le puede interesar:["Ya se inició la movilización de docentes para la toma de Bogotá"](https://archivo.contagioradio.com/ya-se-inicio-la-movilizacion-de-docentes-para-la-toma-de-bogota/)

Según Diego Echeverry, integrante de la Asociación Distrital de Institutores de Antioquia (ADIDA), “con la mediación del Procurador y la voluntad del gobierno, esperamos que el Estado invierta en la educación los recursos que se necesitan”. Además, Echeverry afirmó que **50 congresistas propusieron la adición de 500 mil millones de pesos** para responder y girar a los entes territoriales el dinero que se necesita para la alimentación escolar, el transporte y la implementación de la jornada única.

### **Hay que garantizar el derecho a la educación de los niños y niñas** 

El gobierno y los docentes comparten la preocupación de que **8 millones de niños y niñas de todo el país continúen sin recibir clases.** Para Echeverry, “la preocupación que tenemos por los niños y niñas es grande y estamos interesados en que el conflicto se resuelva lo antes posible”.

Docentes han manifestado que las movilizaciones se han hecho teniendo en cuenta que la primera infancia ha sido la más perjudicada con el detrimento de la educación. Echeverry afirmó que **“el paro de docentes, más allá de reclamar una nivelación salarial, busca que las condiciones con las que se están atendiendo a los niños mejoren porque es realmente lamentable”**. Le puede interesar: ["Maestros y maestras ¿un paro de tradición?"](https://archivo.contagioradio.com/maestros-y-maestras-un-paro-de-tradicion/)

### **Programa académico de 2017 sería modificado** 

Los docentes aseguraron que habrá que reprogramar el calendario académico para garantizar que los niños recuperen las clases que han perdido. En repetidas ocasiones, los padres de familia y los estudiantes han manifestado su solidaridad con los maestros y maestras del país.

Con la gran Toma a Bogotá que se realiza hoy, **los educadores y educadoras de Colombia le hacen un llamado al gobierno para que establezca una propuesta real** al sistema general de participación y así reducir el déficit de la educación. Han dicho que no se ha podido llegar a un acuerdo que aumente el salario teniendo en cuenta los sobre costos de la reforma tributaria. Le puede interesar: ["Gobierno está "repartiendo la pobreza" pero no aumenta presupuesto: Fecode"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/)

De igual forma, Fecode anunció que hoy se realizará una sesión ordinaria de la Junta Directiva Nacional de la Federación a las 5:30 pm para establecer un balance sobre las movilizaciones del día.

<iframe id="audio_19109245" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19109245_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
