Title: Debate en El Ecléctico "La Reserva Van der Hammen"
Date: 2016-08-08 12:08
Category: El Eclectico
Tags: Alcaldía Enrique Peñalosa, Reserva Forestal Thomas Van der Hammen
Slug: debate-en-el-eclectico-la-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

A este debate asistieron **Sabina Rodríguez van der Hammen**, abogada y nieta del científico holandés Thomas van der Hammen, exasesora legal de la Fundación Gaia Amazonas y actual integrante de la Unidad de Trabajo Legislativo de la representante a la cámara por la Alianza Verde Ángela Robledo; **Santiago Silva Schlesinger**, antropólogo de la Universidad de los Andes, exdirector y editor del portal web Huérfanos Políticos y actual columnista de El Ecléctico; **Iván Matallana**, estudiante de derecho integrante de la Dirección de Veeduría Ciudadana y en la campaña de Enrique Peñalosa; y **Nicolás Ramírez**, estudiante de economía de la Javeriana y director del periódico EJE de esa facultad, actual columnista de El Ecléctico.

Los panelistas discutieron la posibilidad de que la reserva Thomas van der Hammen sea urbanizada, así como las recientes declaraciones en público del alcalde Enrique Peñalosa. Algunos defendieron la necesidad de conservar la reserva en su integridad mientras otros propusieron un modelo de urbanización controlada y ambientalmente sostenible.

También evaluaron la propuesta del alcalde Peñalosa de construir vías para descongestionar el norte de Bogotá que atravesaran la reserva Van der Hammen y los posibles beneficios y daños que podría causarle al medio ambiente. Los panelistas hicieron amplias referencias a la necesidad de controlar el crecimiento de la ciudad y las posturas se dividieron respecto a si la reserva debía ser conservada como medio de evitar la expansión desmedida de Bogotá, o si por el contrario, debía urbanizarse como medio para controlar el crecimiento ilegal hacia el norte.

<iframe src="http://co.ivoox.com/es/player_ej_12475990_2_1.html?data=kpehmZqdfZGhhpywj5WVaZS1kZaSlaaVd46ZmKialJKJe6ShkZKSmaiRdI6ZmKiah5eWlsbnxtfjw5C6pc-fxcrfja3Fsc7Zz4qflJCoqcPV1cqYx9OPic2fxsjZh6iXaaKtxNnWxdSJdpOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
