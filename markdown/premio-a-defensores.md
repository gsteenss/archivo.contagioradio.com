Title: Defensores de DDHH a cargo de la elección de los defensores del año
Date: 2017-09-15 17:25
Category: DDHH
Tags: defensores de derechos humanos, Diakonia, Premio a la defensa de los derechos humanos
Slug: premio-a-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/premio-defensores-derechos-humanos-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Sept 2017] 

Los defensores ganadores de la sexta versión del premio Nacional a la Defensa de los Derechos Humanos en Colombia **serán elegidos por jurados destacados en el campo de los derechos humanos.** La premiación será el martes 19 de septiembre a las 8:30 am en el auditorio Luis Carlos Galán  de la Universidad Javeriana.

Grajales indicó que quienes realizan el proceso de selección de los ganadores **“son personas que están involucrados directamente con la defensa** y promoción de los derechos humanos”. Ellos y ellas han tenido la tarea de seleccionar 4 ganadores entre muchos que podrían serlo y que también han merecido reconocimiento.

Dentro de los jurados se encuentra **Martin Almada de Paraguay**, quien es un reconocido defensor de los derechos humanos. La presidenta del Centro Nicaraguense de Derechos Humanos, Vilma Núñez de Escorcia también es jurado al igual que el estadounidense Mark Chernick quien es director del Centro de Estudios de América Latina.

Entre los jurados colombianos se encuentran la historiadora y docente **Marta Nubia Bello**, monseñor Héctor Fabio Henao quien se desempeña como delegado de la iglesia en el Consejo Nacional de Paz e Ignacio Gómez, fundador de la Fundación para la Libertad de Prensa.  (Le puede interesar: ["Estos son los finalistas del Premio Nacional a la Defensa de los derechos humanos"](https://archivo.contagioradio.com/estos-son-los-finalistas-al-premio-nacional-a-la-defensa-de-los-ddhh/))

### **Defensores participarán en 3 conversatorios de derechos humanos** 

Adicional al premio, **los 16 finalistas de más de 70 nominaciones, estarán realizando tres conservatorios** el día lunes 18 de septiembre a las 4:00 pm en el auditorio Félix Restrepo de la misma Universidad. Allí, tocaran los temas de protección a los defensores de derechos humanos en la transición hacia la paz, la reconciliación, memoria y luchas sociales en el posconflicto y las empresas, medio ambiente y los derechos humanos en el posconflicto.

Según Cesar Grajales, director de Diakonia en Colombia, es importante que la ciudadanía participe en los conversatorios para por ejemplo, **“escuchar que piensan sobre el ejercicio de la memoria** las personas que más han sufrido el conflicto armado”.

Dijo que defensores como **Leiner Palacios, los indígenas del Cauca o las mujeres Wayuu** van a poder dar su punto de vista sobre problemáticas que los han afectado directamente y que han trabajado por defender los derechos de las poblaciones más vulnerables y olvidadas del país. (Le puede interesar:["Denuncian 335 agresiones a defensores de derechos humanos en 2017 "Agúzate"](https://archivo.contagioradio.com/somos-defensores-agresiones-en-2017-aguzate/))

Grajales recordó que la ceremonia de entrega del premio va a ser transmitida a través de [www.premiodefensorescolombia.org](http://www.premiodefensorescolombia.org/). A la premiación asistirán autoridades del gobierno, delegaciones diplomáticas, organismos de las Naciones Unidas y los grandes protagonistas que serán las y los defensores de derechos humanos.

<iframe id="audio_20907612" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20907612_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
