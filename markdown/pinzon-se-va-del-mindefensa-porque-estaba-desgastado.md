Title: "Pinzón se va del MinDefensa porque estaba desgastado" Ramiro Bejarano
Date: 2015-05-20 14:05
Author: CtgAdm
Category: Nacional, Política
Tags: colombia, defensa, guerra, Juan Carlos Pinzón, luis carlos villegas, ministerio, paz, proceso de paz, ramiro bejarano, Santos
Slug: pinzon-se-va-del-mindefensa-porque-estaba-desgastado
Status: published

###### Foto: Radio Macondo 

 <iframe src="http://www.ivoox.com/player_ek_4523652_2_1.html?data=lZqflZuZdo6ZmKiakpmJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMri24qwlYqmd8%2Bfz9SYw9XTttXV04qwlYqldYziwsnOjcaPsMKf0cbnjcrSb7jV1M3W0MzYs8%2BfxNTa0ZDScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ramiro Bejarano, analista político] 

###### [20 may 2015]

Aunque algunos medios de información han señalado el cambio del gabinete de Defensa como un viro de la política del presidente Juan Manuel Santos para preparar sus ministerios para un eventual post conflicto, analistas políticos como Ramiro Bejarano no son tan optimistas.

El columnista señala 3 puntos por los cuales, el nombramiento de Luis Carlos Villegas como ministro de defensa, en reemplazo de Juan Carlos Pinzón, nombrado embajador en Washington, son negativos para el país.

En primer lugar, indica que este nombramiento reitera el talante del gobierno Santos. "Él tiene un circulo estrechísimo de amigos y cercanos con los que el quiere gobernar, y eso le ha impreso un sello a su administración, que no interpreta las regiones, que no consulta las regiones, porque los ministros y altos funcionarios son de determinados barrios de Bogota". Para Bejarano, esto es negativo en cuanto no se abren escenarios políticos a personas con responsabilidad política, que representen y abarquen a un mayor número de colombianos, "y no a un exiguo número de colombianos amigos del presidente Santos".

En segundo lugar, la salida de Pinzón del Ministerio de Defensa se habría dado porque "estaba completamente desgastado". Su aporte en Washington, asegura será tan insuficiente como lo fue para la paz de Colombia desde el MinDefensa.

En tercer lugar, el columnista afirma que el paso de los empresarios por las responsabilidades públicas no siempre ha sido lo más exitoso. El historial de Luis Carlos Villegas por la ANDI, ECOPETROL, y la Federación Nacional de Cafeteros, no auguran un camino exitoso en el MinDefensa, como demostró la experiencia de Sabas Pretelt. "Con las enormes dificultades que hay en la milicia, con mejores ojos veían a Pinzón que era de los suyos", concluye el analista.
