Title: Comunidades negras exigen garantías para líderes tras asesinato de Genaro Garcia
Date: 2015-08-24 16:40
Category: Comunidad, Movilización, Nacional, Paz
Tags: afros, Cauca, comunidades negras, FARC, Genaro García, Proceso Comunidades Negras, proceso de paz
Slug: comunidades-negras-exigen-garantias-para-lideres-tras-asesinato-de-genaro-garcia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tumacoadentro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lasillavacía.com 

<iframe src="http://www.ivoox.com/player_ek_7529724_2_1.html?data=mJqfm5yWeI6ZmKiak5aJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfz8rU1MbXb8bsyszS0JDLpdPVz9mSpZiJhaXV1JDdw9fFb82ZpJiSo6nIqdPZ1JDh1MbXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Santos, vocero PCN] 

###### [Ago 24 2014] 

José Santos vocero Nacional del Proceso de Comunidades Negras (PCN) se pronunció sobre el comunicado de las FARC en donde reconocen públicamente que la Columna Móvil Daniel Aldana fué responsable del asesinato del líder comunitario Genaro García, y además denunció amenazas contra otros líderes comunitarios.

Santos indicó que este anuncio no es sorpresa “*les damos las gracias por la valentía que tienen de reconocer el hecho públicamente*”, pero también resaltó que “*es lamentable que las FARC estén generando este tipo de acciones en contra de los líderes sociales, luchadores populares, de la gente que defiende y cuida el territorio*”.

El vocero del PCN muestra preocupación ya que en la comunidad pidieron una misión de verificación a distintas organizaciones, frente a una posible contradicción entre las FARC y la columna Móvil Daniel Aldana, debido a que esta columna y el Octavo Frente de las FARC han amenazando a distintos líderes comunitarios.

“*Pedimos a las FARC EP que pudieran controlar a los mandos que están en esas zonas para que no vayan a cometer otro hecho lamentable como ocurrió con Genaro*”, insistió José Santos.

Sin embargo el llamado del Vocero de las Comunidades Negras no fue, únicamente para los miembros de las FARC, sino también para el gobierno y la fuerza pública “*Que detengan la incursión de minería ilegal en todos los territorios*”, ya que esta está ligada a la **presencia de grupos paramilitares, por ende las comunidades exigen “*el desmonte de todos los grupos paramilitares*”.**

Según el vocero del PCN hay varias personas amenazadas que deben ser protegidas de manera urgente para que no se repitan situaciones como la del asesinato de Genaro García. Las personas amenazadas son Abraham Sinisterra: Concejo comunitario del Río san Francisco en el municipio de Guapi, Rosa Simontaño: Representante Legal del concejo comunitario Alto Guapi, Jairo Contreras: Concejo comunitario las nueve esperanzas, del Hoyo (Patillá), Sara Liliana Quiñónez: Presidenta de la Junta comunitaria del Alto Mira.

El vocero del Proceso de Comunidades negras indicó que del **31 de agosto, al 3 de septiembre las comunidades negras del suroccidente van a realizar una caravana que llevará por nombre “Genaro García”** y se dirigirá hasta Tumaco en apoyo a los consejos comunitarios en defensa de su autonomía y territorios bajo la consigna “Porque el territorio no se vende se ama y se defiende”
