Title: Así viven el conflicto armado las comunidades del río San Juan en "tiempos de paz"
Date: 2017-07-18 18:33
Category: Comunidad, Nacional
Tags: Enfrentamientos, Río San Juan
Slug: asi-viven-el-conflicto-armado-las-comunidades-del-rio-san-juan-en-tiempos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/enfrentamiento-río-san-juan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Fotograma video 

18 de Julio 2017

#### **Por Bernardino Dura  - Comunicador CONPAZ** 

El día lunes 17 de Julio aproximadamente a la 9 a.m,  la infantería de Marina se enfrentó a un grupo armado ilegal, a pocos metros de la comunidad  de San José y Taparalito en el río San Juan, Chocó, el enfrentamiento duro cerca de 20 minutos.

Luego de este hecho la comunidad que reúne a más de 900 indígenas Wounaan se encuentra  confinada y no ha podido salir a realizar sus actividades cotidianas de pesca y recolecta de alimentos en sus fincas, ante el temor de nuevos enfrentamientos en el territorio.

La comunidad  de San José y Taparalito, solicita a las instituciones del Estado y a las organizaciones defensoras de los derechos humanos colaboración para garantizar el buen vivir  de las comunidades y generar ayudas de emergencia humanitaria.

Todos la población indígena wounaan repudiamos este hecho sucedido en su territorio donde hace media década  del conflicto armado tienen azotado a la población civil, han desplazado y amenazado los líderes.

Hoy los líderes de las comunidades de San José y Taparalito fueron a hacer la respectiva denuncia ante la Alcaldía del caso sucedido a la población.

El gobierno tanto que ha hablado de paz pero en el campo no sea cesado el conflicto armado y lo que queremos es paz,  tranquilidad y armonía para poder vivir feliz.

 Por  Bernardino Dura Comunicador popular CONPAZ

<iframe src="https://www.youtube.com/embed/c8WAbvrw3xs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

------------------------------------------------------------------------

###### **Como parte del trabajo de formación de Contagio Radio con comunidades en toda Colombia, un grupo de comunicadores realiza sus reportajes o noticias sobre los que sucede en sus comunidades .** 
