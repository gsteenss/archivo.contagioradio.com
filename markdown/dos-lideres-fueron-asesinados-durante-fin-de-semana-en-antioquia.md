Title: Dos líderes sociales fueron asesinados en Antioquia
Date: 2018-08-06 15:33
Category: DDHH, Nacional
Tags: Antioquia, grupos armados, lideres sociales
Slug: dos-lideres-fueron-asesinados-durante-fin-de-semana-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Ago 2018] 

Dos líderes sociales fueron asesinados durante este fin de semana en el departamento de Antioquia, se trata de Alfredo Ruíz, quien respaldaba la iniciativa de sustitución de cultivos de uso ilícito en el bajo Cauca antioqueño y Hernán Chavarría, líder barequero del oriente de este departamento.

### **Alfredo Ruíz ** 

De acuerdo con Oscar Zapata, integrante del Nodo Antioquia de la Coordinación Colombia Europa Estados Unidos, los hechos se presentaron el pasado **3 de agosto, cuando a altas horas de la noche legaron varios hombres armados a la vivienda de Ruíz**, ubicada en La Caucana, en el municipio de Taraza.

Alfredo Ruíz hacía parte del programa de sustitución de cultivos de uso ilícito y recientemente había renunciado a su liderazgo en la Junta de Acción Comunal, que para Zapata tendría que ver con presiones por parte de estructuras armadas paramilitares que hacen presencia en el territorio y **que estarían extorsionando a los campesinos, hecho al que Ruíz se habría opuesto**.

Además, Zapata denunció que las familias que se han vinculado al programa de sustitución de cultivos de uso ilícito estarían siendo víctimas de un grupo armado conocido como los "Caparrapos", que les estarían exigiendo un **pago por 200 mil pesos de los dineros que las personas obtienen como parte de la financiación del programa**, de lo contrario serían asesinados.

De igual forma, han llegado denuncias de campesinos que afirman que el grupo armado conocido como las Autodefensas Gaitanistas de Colombia ha señalado que las personas que realicen esos pagos a los "Caparrapos" **también serán asesinadas**.

A estos hechos se suman la fata de procedimiento legal por parte de la Fiscalía y Medicina Legal, ya que según los campesinos, fueron ellos quienes tuvieron que trasladar el cadáver de Ruíz hasta el municipio de Tarazá debido al control territorial de los grupos armados en esa zona.

### **Hernán Chavarría** 

Hernán Darío Chavarría era miembro fundador de la Asociación de Barequeros del norte Antioqueño, nació en el municipio de La Granja, razón por la cual conocía el dolor del conflicto armado y era víctima del proyecto de Hidroituango.

Su asesinato se perpetró a altas horas de la noche, cuando se dirigía a casa de uno familiares, en el trayecto personas le propinan varios golpes que acaban con su vida.  (Le puede interesar: "[17 líderes sociales han sido asesinados este año en Antioquia"](https://archivo.contagioradio.com/17-lideres-sociales-han-sido-asesinados-este-ano-en-antioquia/))

En lo corrido de este primer semestre han sido **asesinados 21 líderes sociales en Antioquia y este departamento es el segundo lugar de Colombia** en el que más se atenta contra la vida de líderes sociales y defensores de derechos humano , la mayoría de ellos en la zona conocida como bajo Cauca.

<iframe id="audio_27619600" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27619600_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
