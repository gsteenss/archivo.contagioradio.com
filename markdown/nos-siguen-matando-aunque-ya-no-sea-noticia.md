Title: Nos siguen matando aunque ya no sea noticia
Date: 2017-04-20 08:42
Category: Carolina, Opinion
Tags: feminicidio, mujeres, Violencia contra la mujer
Slug: nos-siguen-matando-aunque-ya-no-sea-noticia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [lunita lu](https://www.flickr.com/photos/wildchild/ "Ir a la galería de lunita lu") 

#### **[Por: Carolina Garzón Díaz]** 

###### 20 Abr 2017 

*[No más crímenes contra defensoras y lideresas sociales. No más asesinatos de mujeres en las calles, en las casas, en los parques o en los centros comerciales.]*

[Hace unos días un hombre en Bogotá asesinó a Claudia Rodríguez, su ex pareja. Pocos días antes una mujer que trabaja en la casa de la lideresa social Marylen Serna, en Popayán, fue secuestrada, torturada, violada y amenazada para sacar información de la líder del Congreso de los Pueblos.]

[Dos casos atroces de violencia encarnizada contra los cuerpos de las mujeres. Ocurrieron hace menos de 15 días pero la indignación parece bajar y se pierde en cientos de noticias. El primer caso ya no es tema en los medios de comunicación y el segundo jamás lo fue.]

[Cada uno de estos hechos ocurrió a mujeres con diferentes opciones de vida, pero hay que llamar la atención sobre la vulnerabilidad en la que todas estaban. Ya existían denuncias, en el caso de Claudia se había trasladado de ciudad y en el caso de la mujer cercana a Marylen Serna, la lideresa ya ha advertido sobre las amenazas en su contra y las posibles afectaciones en quienes la rodean. Parece que fueron en vano y aquí tienen responsabilidad las autoridades y el Estado.]

[Los hechos son estremecedores, pero tristemente no son nuevos. Como en otras ocasiones hemos escuchado, hombres se llevan a mujeres en camionetas o vehículos que nadie puede identificar, así se llevaron a Nydia Érika Bautista en 1987. Hombres atacan a mujeres para privarlas de su libertad, así como secuestraron y violaron a Nohora Cecilia Velásquez en 2003. Usan la violencia sexual para amedrentar, amenazar, humillar y destruir, así como lo intentaron con la violación a Jineth Bedoya en una cárcel de Bogotá en el 2000. Otra mujer que fue raptada, tortura y abusada fue la pequeña Yuliana Samboní, con tan solo siete años de edad. Su caso nos conmovió, pero no vemos acciones para evitar hechos similares.]

[Como el de ellas hay miles de casos en el país, muchos con nombres propios y otros en el anonimato. La cifra es dolorosa: sólo en los primeros tres meses de 2017, del 1 enero al 5 de abril, fueron asesinadas 204 mujeres, 24 de ellas a mano de sus parejas o exparejas. Y el índice de impunidad en casos de feminicidios es bastante alto: desde el 2013 hasta2017 la Fiscalía inició 345 procesos pero únicamente hay 53 condenas. Pero esto ya no es noticia.]

[Dentro de las mujeres asesinadas recientemente (aunque también históricamente) no olvidemos a aquellas defensoras de derechos humanos, gestoras culturales y lideresas sociales y ambientales a quienes han sido víctimas de homicidio por su trabajo en pro de la vida, el ambiente y la comunidad. De las 193 agresiones individuales contra defensores y defensoras de DDHH reportadas por el Programa Somos Defensores entre enero y marzo de este año, el 24% ocurrieron contra mujeres. Para ser más exacta: en los primeros tres meses de 2017 hubo 46 agresiones, de ellas cuatro asesinatos, contra defensoras de DDHH y lideresas sociales.]

[En Colombia es necesario articular las secretarías de la mujer con las de educación y salud para trabajar en el reconocimiento de los derechos de las mujeres, rechazar la violencia y prevenir nuevos casos. Se requiere articulación del más alto nivel para que las investigaciones por feminicidio avancen. Se necesita presión y veeduría de los medios de comunicación, de las organizaciones y de las instituciones. A nivel internacional la relatoría sobre derechos de las mujeres de la Comisión Interamericana de Derechos Humanos (CIDH) debería pronunciarse y los casos que no encuentren justicia en Colombia podrían ser de conocimiento del Comité para la eliminación de la discriminación contra la mujer (CEDAW).]

[Con violencia y muerte nos han infundido miedo a decenas de mujeres durante décadas. Hoy, en el 2017, a las mujeres son siguen secuestrando, asesinando, amenazando, desapareciendo y violando. Tenemos que insistir para detener la violencia contra las mujeres, día a día, sin pausa, con vehemencia, aunque ya no sea noticia. Un lucha tan persistente como la “Operación Siriri” que nos ha enseñado por más de 30 años otra mujer valiente, Fabiola Lalinde. ¡No más violencia contra las mujeres!]

#### [**Leer más columnas de opinión de  Carolina Garzón**](https://archivo.contagioradio.com/carolina-garzon-diaz/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
