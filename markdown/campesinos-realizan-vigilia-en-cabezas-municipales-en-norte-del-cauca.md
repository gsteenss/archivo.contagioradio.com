Title: Ante amenazas campesinos vigilan cabeceras municipales en Norte del Cauca
Date: 2016-11-10 11:17
Category: yoreporto
Tags: Norte del Cauca
Slug: campesinos-realizan-vigilia-en-cabezas-municipales-en-norte-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-en-norte-de-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por: Agencia de Comunicaciones Prensa Alternativa Cauca - Yo Reporto** 

###### 10 Nov 2016 

[Ante la difícil situación en materia de derechos humanos que se vive en el Norte del Cauca, la guardia campesina de la Asociación Pro Constitución de las Zonas de Reserva Campesina de Caloto, Corinto y Miranda, comenzaron a ejercer vigilancia propia como instrumento de protección a la ciudadanía. ]

[Las comunidades campesinas decidieron en sus organizaciones generar un mecanismo de protección para hacerle frente a las violaciones a los derechos humanos que han sufrido a lo largo de este año. En algunas esquinas de calles acordadas, haciendo rondas en diferentes manzanas o realizando circuitos en motos, brindan seguridad a los pobladores y ayudan a preservar la vida de los pobladores.]

[Desde el momento que se dispusieron para este nuevo trabajo, han contado con el acompañamiento permanente de la Red de Derechos Humanos Francisco Isaías Cifuentes -Red FIC-, organización defensora de los derechos humanos en el suroccidente colombiano, que constantemente elabora denuncias e informes especiales en relación a las infracciones que afectan a la población civil.]

[Deivin Hurtado, coordinador departamental de la RED FIC, expresa que "ante las reiteradas violaciones a los derechos humanos, las comunidades campesinas se han visto en la necesidad de comenzar a organizarse en materia de seguridad, puesto que hay una serie de acciones que ponen en riesgo a las comunidades organizadas como sucedió con un miembro de la asociación campesina de Caloto asesinado este año o los tres de Corinto. De la misma manera, los campesinos ven que el Estado colombiano no cumple con su función de velar por la vida e integridad de los colombianos."]

[Estas legítimas y necesarias prácticas de organizarse en torno a la seguridad no quita que el escenario que se viene de implementación de los acuerdos logrados en La Habana, deban cesar de manera definitiva acciones que atentan contra la integridad de las poblaciones, para que estas zonas afectadas por el conflicto social y armado, materialicen un verdadero enfoque territorial donde la defensa de la vida y el buen vivir sean objetivos primordiales del Estado colombiano. ]
