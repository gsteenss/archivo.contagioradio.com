Title: Comisión de la Verdad debe esclarecer responsabilidad empresarial
Date: 2017-02-23 17:12
Category: Nacional, Paz
Slug: comision-verdad-esclarecer-responsabilidad-empresarial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Empresas-en-el-conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Feb 2017] 

Esclarecer los hechos, participación y responsabilidades del sector privado en el conflicto armado y el escalamiento de la violencia, la creación de una reforma institucional en los territorios, que convoque a las empresas en la reconstrucción de las regiones azotadas por la guerra, su participación en conflictos comunitarios y sus responsabilidades por la violación de derechos ambientales, son algunas sugerencias hechas por organismos internacionales, **para que la Comisión de la Verdad y el Sistema Integral de Verdad, Justicia, Reparación y No Repetición, cumplan y se beneficie a las víctimas.**

El Centro Regional de Empresas y Emprendimientos Responsables –CREER–, el Institute for Human Rights and Business –IHRB- y el Center for International Law and Policy, plantearon que el Sistema Integral de Verdad, Justicia, Reparación y No Repetición -SIVJRNR- de los Acuerdos de La Habana, no es claro en cuánto a la cuestión de las empresas privadas **“que patrocinaron fuerzas de seguridad del Estado o grupos paramilitares”**, o el papel de algunas industrias **“que contribuyeron a la opresión y la escalada de la violencia”.**

Durante el simposio “Construcción colectiva de la verdad y la reconciliación: abordando el rol de las empresas”, los expertos determinaron que hasta el momento tanto la práctica, como los estudios sobre **“la relación entre la responsabilidad empresarial y la Justicia Transicional han sido mínimos”.** ([Le puede interesar: La macabra alianza entre paramilitares y empresas Bananeras](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/))

### Empresas no pueden evadir su responsabilidad en el Conflicto 

Señalaron que “existe una escasa comprensión del papel del sector privado en la reconstrucción territorial, institucional y la gobernanza”, dentro de un proceso de justicia transicional, que se oriente a **“la superación de la desconfianza que se ha generado durante los años de permanencia del conflicto”**. Puntualizaron que históricamente “las empresas han estado usualmente presentes y a veces muy activas en las zonas de conflicto”, por lo cual no pueden evadir sus responsabilidades en un Proceso de Paz.

Resaltaron que en la historia de Colombia existe un patrón de participación empresarial en el conflicto, que “a pesar de algunos litigios emblemáticos pero aislados” contra empresas pertenecientes a los sectores extractivo, agrícola y demás, las empresas siguen estando **“implicadas en el acaparamiento masivo de tierras, el desplazamiento forzado interno, el asesinato, la desaparición de líderes”** y otras graves violaciones de derechos humanos que “perjudican directamente tanto a los individuos como al tejido social de las comunidades locales”.

### Justicia Transicional para la participación y la reparación 

Explicaron que la Justicia Transicional, ofrece entonces una oportunidad para que se aborden los **“conflictos sociales subyacentes entre las empresas y las comunidades”**, al mismo tiempo que sienta “las bases para prevenir, mitigar y hacer frente a futuros conflictos armados”.

Dentro del SIJVRNR, las Comisiones de Verdad juegan un papel preponderante, para el caso colombiano, hasta el momento esta Comisión, no ha sido orientada a involucrar a las empresas “para que participen en audiencias públicas, en las tomas de testimonios y otros ejercicios de construcción de la verdad”.

Los expertos, aseguran que este escenario abre la posibilidad a la impunidad y “a frustrar las necesidades de justicia de las víctimas”, pues las acusaciones, en el marco de procesos judiciales, **“rara vez buscan responsabilizar directamente a las empresas por el papel que jugaron en episodios violentos”.** ([Le puede interesar: Empresas bananeras responsables de crímenes de lesa humanidad](https://archivo.contagioradio.com/empresas-bananeras-responsables-de-crimenes-de-lesa-humanidad/))

Llamaron especial atención sobre el hecho de que dentro de lo pactado en La Habana, no se refieren explícitamente a las empresas sino a "terceros", y **“las implicaciones negativas para las víctimas” que puede tener esta imprecisión de los Acuerdos.**

Por último, mencionaron la importancia de incluir en el SJVRNR, los Principios Rectores de Empresas y los Derechos Humanos de las Naciones Unidas –PRNU–, los cuales establecen que el Estado debe asegurar que las empresas se responsabilicen de los daños, **“de manera que las víctimas tengan recursos adecuados y eficaces”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
