Title: En juicio contra Feliciano Valencia hubo dos irregularidades que anularían la condena
Date: 2016-05-23 14:42
Category: Movilización, Nacional
Tags: ACIN, Cauca, Corte Suprema de Justicia, Feliciano Valencia
Slug: en-juicio-contra-feliciano-valencia-hubo-dos-irregularidades-que-anularian-la-condena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/felciano-valencia5-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: youtube] 

###### [23 May 2016]

La sala de casación penal de la Corte Suprema de Justicia decidiría hoy si el recurso presentado por la defensa del líder indígena Feliciano Valencia para dejarlo en libertad tendría el suficiente sustento probatorio. El argumentos de la defensa se basan en **por lo menos dos irregularidades que se cometieron durante el proceso de juicio** y en los elementos que sustentaron la condena.

El abogado Germán Pabón, que representa los intereses de Valencia explicó que aunque la audiencia de hoy no dejará libre a su defendido de manera inmediata, si es crucial que se evidencien las irregularidades que se denuncian, puesto que **en adelante la Corte decidirá si da validez a los argumentos** y esto resultaría en la libertad de Valencia en el corto plazo.

Según Pabón, en el proceso se presentaron dos [situaciones que violan la jurisprudencia ordinaria y que atentan contra la jurisprudencia indígena:](https://archivo.contagioradio.com/sectores-sociales-e-indigenas-rechazan-detencion-del-lider-indigena-feliciano-valencia/)

1.  En el proceso se incurrió en **irregularidad sustancial** porque se infringieron los artículos 29 y 246 de la Constitución en que se establece que las comunidades indígenas pueden ejercer jurisdicción dentro de sus territorios

El abogado explica que la **detención del cabo Jairo Danilo Chaparral** se realizó por la guardia indígena en uso de sus funciones jurisdiccionales, por lo tanto, si hubiese delito serían las autoridades indígenas las encargadas de acusar y establecer los castigos correspondientes.

2.  **No se puede acusar a Feliciano Valencia por coautoría de secuestro simple, como lo indica el artículo 29 de la ley 559 de 2000**. Aunque la detención del cabo la realizó la propia guardia indígena durante el juicio oral se demostró que no medió una orden directa o complicidad por parte del líder indígena. La guardia cumplió las funciones de control que le establecieron las autoridades tradicionales y se retuvo al cabo en medio del resguardo indígena de la María.

Según el abogado la Corte tendría que actuar en derecho y dar validez tanto a los procedimientos regulares establecidos en la ley y hacer valer la propia constitución que establece **la independencia de la jurisdicción indígena.** Lo que viene será el estudio por parte de la sala de casación de la Corte y el pronunciamiento posterior a favor o en contra del recurso interpuesto.

En la tarde de este lunes se realizará un plantón en que las organizaciones y personas manifestarán su e[xigencia de libertad para Feliciano Valencia](https://archivo.contagioradio.com/movimiento-indigena-caucano-rechaza-detencion-de-feliciano-valencia/). La cita es a partir de las 3 de la tarde.

<iframe src="http://co.ivoox.com/es/player_ej_11635580_2_1.html?data=kpajlZqZfJGhhpywj5WcaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5yncajZ09KSpZiJhZLijLXOxIqnd4a2lNOYj6bGs8jVxdSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
