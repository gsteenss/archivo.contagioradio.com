Title: "Decisión de C. Constitucional sobre consumo de sustancias en espacios públicos fue sensata"
Date: 2019-06-08 22:13
Author: CtgAdm
Category: Judicial, Política
Tags: consumo, Corte Constitucional, Drogas, Sustancias psicoactivas
Slug: decision-constitucional-consumo-de-sustancias-sensata
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Fotos-editadas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado jueves 6 de junio la Corte Constitucional derogó la norma del Código de Policía que prohibía el consumo de alcohol, así como la dosis mínima de sustancias psicoactivas en espacios públicos. Para algunos, la medida significa un retroceso en las normas de convivencia, y una forma de 'propiciar' el consumo; para otros, una decisión admirable, que asimila el derecho al libre desarrollo de la personalidad y abre el camino para debatir sobre el uso de estas sustancias.

> La Corte Constitucional declaró inexequibles las expresiones 'alcohólicas, psicoactivas o' contenidas en el Artículo 33 (lit. c, num. 2); y las expresiones 'bebidas alcohólicas' y 'psicoactivas o' del Artículo 140 (num. 7) del Código de Policía (Ley 1801 de 2016). 1/3 [pic.twitter.com/XwBKmc0OlV](https://t.co/XwBKmc0OlV)
>
> — Corte Constitucional (@CConstitucional) [6 de junio de 2019](https://twitter.com/CConstitucional/status/1136780512511614976?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Esta era una decisión que en cualquier momento llegaría**

**Julian Quintero**, director de [Échele Cabeza](http://www.echelecabeza.com/que-es-echele-cabeza), resaltó el valor de la Corte al tomar esta decisión en un contexto de 'populismo punitivo', en el que se celebra la prohibición y el castigo hacia los consumidores en lugar de la prevención y educación en el uso de estas sustancias. Adicionalmente, destacó que era una prohibición que violaba derechos fundamentales, razón por la que tarde o temprano se quedaría sin piso jurídico.

Quintero señaló que la puesta en marcha de tal prohibición, y la corrección por parte de la Corte, llevarán a algunos problemas, por ejemplo: ¿Se devolverá el dinero a quienes se les destruyó o confiscó la dosis mínima o las bebidas alcohólicas por consumirlas en espacio público? ¿Qué pasará con quienes tuvieron que pagar multas, al ser sancionados por esta situación?

Otra dificultad (señalada desde algunos sectores de la opinión pública) es el choque generado entre el derecho al consumo y el derecho de los niños al ambiente sano, por ejemplo en los parques. Sin embargo, el Director de Échele cabeza desestimó este argumento, y afirmó que era una **estrategia usada para juzgar moralmente a los consumidores, acudiendo a los derechos de los niños**.

En esa medida, Quintero sostuvo que es tan importante proteger el derecho a consumir y como el de los niños; razón por la que consideró importante pensar en una **regulación de los comportamientos en los espacios públicos, "para que todos puedan convivir y no estar prohibiendo,** porque eso siempre es lo más sencillo, pero también lo más mediocre". (Le puede interesar: ["Gobierno Duque está en absoluta ignorancia sobre la política de drogas"](https://archivo.contagioradio.com/gobierno-duque-ignorancia-drogas/))

### **La política de drogas en Colombia opera bajo picos emocionales**

Tras la entrada en vigencia del Decreto 1844 de 2018 que permitía a las autoridades incautar la dosis mínima, Échele Cabeza realizó un sondeo sobre los efectos de esta decisión en el que participaron casi 2 mil personas; según Quintero, **ninguno de los encuestados "se había dejado persuadir por el Decreto".** Los efectos del Decreto se reflejaron en el aumento de sustancias decomisadas, así como del costo de esas sustancias pero no en la reducción del consumo; para el Experto, ello es muestra de los picos emocionales con los que se manejan este tipo de políticas.

Otro argumento normalmente utilizado por quienes apuntan al prohibicionismo tiene que ver con el aumento en el consumo de sustancias psicoactivas; pero el Director de Échele cabeza rechazó también esta premisa; en primer lugar, porque **el aumento en el consumo significa el fracaso de las prohibiciones.** En segundo lugar, porque los consumos han aumentado a nivel global y el caso de Colombia es particular: "[el gramo de Cocaína y heroína, en relación costo-pureza es el más barato del mundo; las drogas de síntesis son un 15-20% más baratas; y Colombia es el país donde llegan las drogas más rápido a domicilio".]

Bajo estas condiciones, Quintero expresó que **es extraordinario que los consumos en el país no sean mucho mayores de lo que son.** Al tiempo, es un contexto que invita a hacer un cambio en la forma como se aborda el problema. (Le puede interesar: ["Decreto de Duque para prohibir dosis mínima es una persecución a consumidores de drogas"](https://archivo.contagioradio.com/decreto-de-duque-para-prohibir-dosis-minima-es-una-persecucion-a-consumidores-de-drogas/))

### **"No es prohibiendo sino previniendo"**

El Directivo de Échele Cabeza explicó que las cifras de consumo suenan alarmantes, y una persona que fue educada bajo el dogma de la guerra, cree que la solución es atacar a las sustancias y los consumidores; "a ello se suma una narrativa de 30-40 años que ha hecho pensar a las personas que las drogas son el problema, cuando el problema son las políticas". Por estas razones, Quintero aseguró que la decisión de la Corte **fue un llamado de sensatez**, al tiempo que una invitación a **dejar de lado la prohibición, y empezar a trabajar en la prevención y la educación.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
