Title: Los crímenes de la inteligencia militar llegaron a la JEP
Date: 2020-02-07 14:35
Author: AdminContagio
Category: DDHH, Judicial
Tags: Doctrina del Enemigo Interno, Inteligencia Militar
Slug: los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/fuerzas-militares.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 7 de febrero, organizaciones defensoras de derechos humanos presentarán ante la Jurisdicción Especial para la Paz, el informe "BINCI y Brigada XX: El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno(1977-1998)", que revela las conductas criminales cometidas por integrantes de esas instituciones castrenses, a partir de la Doctrina del enemigo interno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento, elaborado por la Comisión de Justicia y Paz, el Colectivo de Abogados José Alvear Restrepo y la Coordinación Colombia Europa Estados Unidos, recopila 35 casos de tortura, 51 casos de ejecuciones extrajudiciales y más de 70 casos de desaparición forzada, como un conjunto de prácticas de la inteligencia militar que tenían como finalidad acabar con el "enemigo interno", que se traducía en una persecución a integrantes de movimientos sociales, partidos políticos de oposición, defensores de derechos humanos y periodistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Así funcionó la inteligencia militar en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre las violaciones a derechos humanos puestas en marcha por el Estado, bajo motivaciones políticas, el informe señala que existió un patrón que contó con hechos como**  desaparición forzada, tortura y ejecuciones extrajudiciales.** (Le puede interesar : ["Informe relata el rol de la inteligencia militar en los crímenes de Estado"](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esos actos eran antecedidos por operativos de control y vigilancia a las personas para indagar sobre su vida personal, sus familias o personas que frecuentaba, para ello regularmente se usaba la infiltración e interceptación a grupos de personas. Posteriormente se pasaba al secuestro y/o privación ilegal de la libertad por parte de integrantes de las unidades de inteligencia del ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego, las víctimas eran conducidas a instalaciones de las Fuerzas Militares, o lugares clandestinos, usualmente usando vehículos particulares que estaban registrados bajo estas unidades militares, en donde eran torturadas, con la finalidad de obtener información.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a la responsabilidad en la cadena de mando de estas prácticas, el documento expone, que por lo menos en cuatro casos se compromete la responsabilidad de varios superiores jerárquicos del Batallón de Inteligencia y Contrainteligencia Charry Solano y de la Brigada XX, entre los que se encuentran: el Mayor General (R) Iván Ramírez Quintero, el Brigadier General (R) Álvaro Hernán Velandia Hurtado, el General (R) Harold Bedoya Pizarro, el Brigadier General (R) Jaime Ruiz Barrera y el General (R) Miguel Vega Uribe.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El cierre del BINCI y la Brigada XX  

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Batallón de Inteligencia y Contrainteligencia Brigadier General Ricardo Charry Solano operó a nivel nacional desde 1962 hasta 1985, a partir de ese momento la Brigada XX asumió sus labores y funcionó hasta que fue disuelta en 1998.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ambos grupos, según el informe, contaron con los medios humanos, técnicos y tecnológicos de la inteligencia adscrita al Ejército para llevar a cabo estas violaciones de DDHH, y creó todo un aparato organizado del Estado que «tiene **diferentes conexiones con instituciones gubernamentales o sectores civiles,** para desarrollar operaciones sicológicas, de hostigamientos, desaparición o ejecución».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a los delitos cometidos por el BINCI,  en el 2017, en fallo de primera instancia, el juzgado sexto especializado de Bogotá, condenó a 11 años de prisión a tres integrantes de sus integrantes por la desaparición forzada de un militante del M-19 que logró sobrevivir a diferentes torturas y un intento de asesinato.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, respecto a las acciones cometidas por la Brigada XX, en 1998, la embajada de Estados Unidos, junto a organizaciones defensoras de derechos humanos denunciaron los crímenes realizados por esta estructura que culminaron con el cierre de la misma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para las organizaciones es importante resaltar que las prácticas efectuadas por ambas instituciones castrenses son sistemáticas y consideradas como delitos de lesa humanidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La inteligencia militar en Colombia no compagina con los DD.HH

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con las organizaciones que presentan el documento a la JEP, se espera que el informe "incentive la denuncia por parte de las víctimas que sobrevivieron a las torturas o de los familiares y allegados de quienes ya no están, para que el país pueda dimensionar el accionar de este órgano de inteligencia".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la entrega del documento, las organizaciones también esperan que se inicie un plan, en conjunto con la Unidad de Búsqueda, para encontrar a las personas que aparecen relacionadas en el informe como desaparecidas; y solicitan medidas cautelares con relación a la protección y conservación de los archivos de inteligencia de la Brigada de Institutos Militares (BIM), a la cual estuvieron adscritos el BINCI y la Brigada XX, con el fin de proteger y garantizar el acceso a la información.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe, además se entrega en un momento complejo para las Fuerzas Armadas, debido a que recientemente, se denunció el uso ilegal de la inteligencia militar, al realizar interceptaciones a periodistas, políticos y defensores de derechos humanos, situación que recurrente desde 1985 en Colombia y que se junta a otros escándalos como el de las "chuzadas del D.A.S", Andromeda, entre otros. <https://www.justiciaypazcolombia.com/en-residencia-del-defensor-daniel-prado-se-halla-dron/>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Razón por la cuál las organizaciones aspiran a que el documento también sirva para hacer un análisis sobre las violaciones a derechos humanos que ha cometido la inteligencia militar, incluso después del cierre de estas estructuras. (Le puede interesar: ["Las peores prácticas de la inteligencia militar nunca se fueron")](https://archivo.contagioradio.com/las-peores-practicas-de-inteligencia-militar-nunca-se-fueron/)

<!-- /wp:paragraph -->
