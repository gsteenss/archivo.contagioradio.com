Title: Víctimas de Estado deben participar en debates de implementación de acuerdos de paz
Date: 2017-02-02 12:58
Category: Nacional, Paz
Tags: acuerdos de paz, colombia, Implementación, víctimas de estado
Slug: victimas-estado-colombia-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 2 Feb 2017 

Parlamentarios presentaron este jueves **una proposición para que las víctimas de crímenes de Estado puedan participar en los debates de implementación** de los acuerdos de paz alcanzados en la Habana.

La proposición busca que se adicione la participación de las víctimas de Estado, al parágrafo del artículo 1 del proyecto de Ley N° 02 de 2016 del Senado y el 04-2016 de la Cámara, por el cual se reglamenta el Acto legislativo 01 de 2016, que establece los instrumentos jurídicos para facilitar y asegurar la implementación y el desarrollo del acuerdo de paz. Le puede interesar: ["Jurisdicción especial de paz debe ser fiel a las víctimas del conflicto armado": Iván Cepeda](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-debe-ser-fiel-a-las-victimas-del-conflicto-armado-ivan-cepeda/)

De acuerdo con la propuesta, el parágrafo en mención, **debería incluir la invitación al Presidente de la Mesa Nacional de Participación Efectiva de las Víctimas y un delegado del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE)**, a todas las sesiones en las que se discutan proyectos relacionados con los derechos de las víctimas y que sean tramitados mediante el Procedimiento Legislativo Especial para la Paz.

Entre los firmantes, figuran los representantes a la Cámara Angela María Robledo, Inti Asprilla, Alirio Uribe Muñoz y el Senador Iván Cepeda Castro. A través de redes sociales las organizaciones piden al congreso **se de un trato Simétrico para el conjunto de víctimas que garantice su participación**.

\[caption id="attachment\_35653" align="aligncenter" width="439"\]![PETICION VICTIMAS DE ESTADO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/PETICION-VICTIMAS-DE-ESTADO.jpeg){.wp-image-35653 width="439" height="585"} Petición de los congresitas\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
