Title: Vender ISAGEN: El peor negocio del mundo (1er parte)
Date: 2015-05-14 06:00
Author: CtgAdm
Category: Opinion, Ricardo
Tags: centrales hidroeléctricas, colombia, EPM, ISAGEN, Three Mile Island
Slug: vender-isagen-el-peor-negocio-del-mundo-1er-parte
Status: published

#### [Ricardo Ferrer Espinosa ](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) 

**¿Cambiaría usted su corazón por un par de zapatos?** Juan Manuel Santos reincide con el mismo tipo de negocio que firmó en los TLC, donde Colombia entrega la gallina de los huevos de oro y a cambio recibimos un bello patito de goma.

“Planeación” le dice que es buen negocio vender las centrales hidroeléctricas para construir las carreteras ¿De qué nos sirve tener buenas vías si perdemos el motor?

El corazón por un par de zapatos. En este negocio no nos darán otro corazón de repuesto, pero nuestro cadáver lucirá calzado nuevo, que será la envidia de los vecinos cuando vean en penumbras nuestro funeral.[   ]{.Apple-converted-space}

La producción de energía por medio de centrales hidroeléctricas es 52 veces más barata que cualquier otro sistema (1). Los insumos básicos son la fuerza de gravedad y el agua.

Al Producir energía por otros medios, necesitamos carbón, gas y petróleo: recursos caros, cada vez más escasos y no renovables. Además, generan desechos contaminantes. Recordemos los accidentes nucleares de Three Mile Island, USA (1979), Chernobyl, Ucrania (1986),[ ]{.Apple-converted-space}Fukushima, Japón (2011). Es energía cara y peligrosa para la humanidad. Los efectos residuales son abortos, mutaciones monstruosas, cáncer y suelos inútiles. La energía nuclear es la pesadilla perfecta.

No es casualidad que hoy, cuando las potencias guerrean por hidrocarburos para producir energía, se codicie **ISAGEN.** Los depredadores manejarán las hidroeléctricas de Colombia, un país raro cuyo sistema eléctrico utiliza una mínima parte de combustibles fósiles.

Tenemos montañas con buenas lluvias, perfectas para construir represas de "aprovechamiento múltiple": agua potable, energía. Evitamos inundaciones y guardamos agua para las épocas de sequía. Las represas generan empleo con la recreación, el turismo y la hotelería.

En Colombia las centrales hidroeléctricas están cerca a las ciudades. No pasa lo mismo en China, Argentina y Brasil, cuyas redes de distribución son más lejanas. Justo ahora, cuando las hidroeléctricas de Colombia valen su peso en oro, el gobierno las quiere cambiar por un plato de lentejas. Se supone que el presidente debe velar por el interés nacional. Por el contrario, privatiza y cede soberanía a las multinacionales.

Los políticos y empresarios llevan años desmantelando la red de Servicios Públicos Domiciliarios. Endeudan al país y agobian a la población con tarifas abusivas. Privatizan electricidad, alcantarillado, aseo, gas (y demás combustibles), transportes y teléfonos. Las mismas políticas afectan la educación, la salud, y la justicia.

Las comunicaciones (correos, telefonía, radio, Internet, televisión) se ha rediseñado en favor de las multinacionales mientras que el usuario paga tarifas altas por un servicio deficiente. La promesa de brindar mejor servicio a menor precio nunca se ha cumplido. Entregar la energía eléctrica de Colombia será otro desfalco.

Negocio donde las multinacionales ganan y Colombia pierde. El Estado maltrata a la población, cuando construye represas: En vez de indemnización, la[ ]{.Apple-converted-space}población afectada por las obras, enfrenta guerra sucia, desapariciones y asesinato de los líderes cívicos. Viene el desplazamiento y culmina con el despojo de las tierras en donde se hacen los embalses. Esa es la memoria de la tenebrosa "Democracia" colombiana. (2). ¿Cuántas vidas sacrificadas por Hidroituango? (3). Las ganancias se privatizan y nuestra gente pone los muertos. Dicen que Dios le dio a Colombia los recursos más fabulosos y los gobernantes más corruptos. Y con ellos nunca habrá paz.

###### (1) El valor de las nubes / Debate sobre EPM. Roque Berto Londoño Montoya. 1996. 

###### (2) Conflicto, discursos y reconfiguración regional El oriente antioqueño: de la Violencia de los cincuenta al Laboratorio de Paz\* Por Clara Inés García\*\* Iner 2007. Investigadora del Grupo de Estudios del Territorio perteneciente al Instituto de Estudios Regionales (INER) de la Universidad de Antioquia. 

###### (3)[ ]

###### (4)[ ]
