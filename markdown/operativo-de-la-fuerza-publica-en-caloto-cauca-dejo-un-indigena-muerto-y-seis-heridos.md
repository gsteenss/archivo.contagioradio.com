Title: Operativo de la Fuerza Pública en Caloto, Cauca dejó un indígena muerto y seis heridos
Date: 2018-03-27 15:47
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Caloto, Cauca, Fuerza Pública, indígenas, Indigenas Nasa, operativos militares
Slug: operativo-de-la-fuerza-publica-en-caloto-cauca-dejo-un-indigena-muerto-y-seis-heridos
Status: published

###### [Foto: Pulzo.com] 

###### [27 Mar 2018] 

Indígenas Nasa denunciaron que el pasado 25 de marzo entre las 8:00 pm y las 9:00 pm,  en el corregimiento del Palo, municipio de Caloto, Cauca, integrantes de la Fuerza Pública realizaron un operativo en búsqueda de un "objetivo militar", dejando un indígena muerto y seis personas heridas. El hecho sucedió cuando ** campesinos y nasas **departían en una discoteca.

De acuerdo con la denuncia que realizó la Asociación de Cabildos Indígenas del Norte del Cauca, quienes resultaron heridos fueron **remitidos al hospital de Caloto** y otros centros médicos de segundo y tercer nivel. Indicaron que los afectados presentan “heridas por impactos de proyectil en extremidades, así como heridas abiertas de compromiso”. El indígena que falleció fue José Wilson Escue del Resguardo de Huellas Caloto.

### **Hubo un uso desmedido de la fuerza por parte de las Autoridades** 

De acuerdo con Edwin Lectano, integrante de esa organización, “un comando de Fuerza Pública integrado por la Policía Nacional y el Escuadrón Móvil Antidisturbios y apoyado por las Fuerzas Militares, hicieron un **uso desmedido de la fuerza"** en la medida que “hubo utilización de armamento de largo alcance en poco espacio donde hay presencia de civiles”.

Afirmó que, en el momento del operativo, **departían cerca de 1.500 personas, y pese a ello** la Fuerza Pública utilizó “una gran cantidad de gases y bombas aturdidoras". Allí, al ser un espacio de comercio, se encontraban también niños y mujeres por lo que condenaron el uso desmedido y la ejecución de estas medidas en zonas que son transitadas de manera frecuente por civiles. (Le puede interesar: ["Campesinos e indígenas están en riesgo permanente: Misión Internacional Vía Campesina"](https://archivo.contagioradio.com/campesino-e-indigenas-estan-en-riesgo-permanente/))

### **Indígenas condenaron la forma de actuar de la Fuerza Pública** 

Los indígenas manifestaron que “por información de prensa sabemos que la Fuerza Pública **tenía un objetivo militar** importante para ellos y sería un disidente de las FARC o de un grupo armado al margen de la ley”. Sin embargo, Lectano argumentó que no están cuestionando el objetivo del operativo, sino que están “condenando la forma en la que se están planeando este tipo de acciones que deja como resultado un asesinato”.

Recordó que **no es la primera vez** que la Fuerza Pública incurre en este tipo de acciones “de fuerza desmedida con resultado de civiles heridos”. El pasado 27 de febrero en el mismo sector, “un indígena resultó herido por acciones militares”. Por esto, han pedido que las autoridades apliquen el principio de la proporcionalidad y que “revisen que sus acciones no dejen daños” contra la población civil.

Finalmente, los indígenas les exigieron a los organismos de control que “revisen este tipo de acciones", ya que creen "que las acciones militares **deben ser calculadas** de forma tal que no generen daños colaterales”. Además, les han pedido a los actores ilegales que no ingresen en sus territorios debido a que esto genera enfrentamientos, y es la comunidad la que queda en medio del fuego.

<iframe id="audio_24859516" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24859516_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
