Title: Palestina iza su bandera en sede de la ONU
Date: 2015-09-30 14:50
Category: DDHH, El mundo, Otra Mirada
Tags: Asamblea General de la ONU, Ban Ki Moon, Bandera Palestina, Franja de Gaza, Mahmud Abbas, ONU, Palestina
Slug: palestina-iza-su-bandera-en-sede-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bandera_palestina_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radio.uchile.cl 

###### [30 sep 2015] 

Después del discurso en la ONU del presidente de Palestina **Mahmud Abbas** en el que denunció la injusticia histórica que se comente sobre su patria, Ban Ki Moon anunció que se izaría **por primera vez la bandera Palestina**, con el fin de que  "*este acto impulse a la llamada comunidad internacional a buscar alternativas para la solución de dos Estados, Israel y Palestina, que puedan convivir mano a mano*".

El acto contrastó con la continuidad de la política de ocupación en Jerusalen, la represión a las manifestaciones de la religión musulmana en la plazoleta de las Mezquitas y la realización de nuevos bombardeos en la noche del martes sobre extensos terrenos habitados en la Franja de Gaza.

El observador palestino ante la ONU, Riyad Mansour, indicó  que "*izar la bandera afuera de la sede principal de la instancia, no finalizará la ocupación israelí, pero es un signo del apoyo internacional hacia el viaje palestino rumbo a la materialización de un Estado*". El pasado 10 de septiembre **se acogiera esta medida** con 138 votos a favor, nueve en contra y 41 abstenciones.

Los países que se abstuvieron de votar fueron Estados Unidos, Israel, Canadá, República Checa, Panamá, Palau, Micronesia, Nauru, y las islas Marshall, fueron las naciones que se opusieron a la medida, por considerar que **se trata de un reconocimiento de la soberanía de los palestinos sobre el territorio ocupado por Israel desde 1967**.

A continuación el dsicurso completo de Mahmud Abbas ante la ONU

\[embed\]https://www.youtube.com/watch?v=rz-sSXNYAuY\[/embed\]
