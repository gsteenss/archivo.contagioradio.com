Title: La memoria de Héctor Abad Gómez, en "Carta a una sombra"
Date: 2015-07-02 18:01
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Carlos Gaviria Díaz, Carta a una sombra, Cecilia Faciolince, Cine, Daniela Abad, El Olvido que seremos, Héctor Abad Gómez
Slug: la-memoria-de-hector-abad-gomez-en-carta-a-una-sombra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/LUksyH6X3HRHTJ_XQdcM_aq8hcWQj45SpdxpKFQaGPoMHcjIE9h8CA_H34tYOqz0HEoAQGzLjeHcOBzCxU1JW83c1SSJ4MfzxWUy-ywtPab81zffWwrMkD_kT_KK2LjP8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Después de recibir el Premio del Público y del Jurado en la más reciente edición del Festival Internacional de Cine de Cartagena, **“*Carta a una sombra*”**, documental inspirado en el libro “*El Olvido que seremos*” del escritor y periodista antioqueño **Héctor Abad Faciolince**, se proyecta desde hoy en salas de Cine Colombia en Bogotá. Una mirada real y optimista a la vida del médico **Héctor Abad Gómez**, en el que la memoria se manifiesta a través de los sentimientos de una familia que resisten al olvido, la violencia y el rencor.

El 25 de Agosto de 1987, el doctor Héctor Abad Gómez, quien dedicó su vida al trabajo por la salud pública y la defensa de los derechos humanos, cayó baleado en la ciudad de Medellín, en un acto de violencia irracional, de esos que se repite con demasiada frecuencia en Colombia. Poco menos de 10 años después del fatídico suceso, Héctor Abad Faciolince, su único hijo varón, recrearía literariamente la vida y muerte de su padre en **“El Olvido que seremos”** una obra biográfica que toma prestados recursos de la novela en un ejercicio de memoria personal, familiar y social.

Cuando su abuelo fue asesinado, **Daniela Abad**, hija del escritor, apenas contaba con un año de vida. Para la joven cineasta formada profesionalmente en Barcelona, co- dirigir el documental fue una manera de conocerlo, escucharlo y sentirlo vivo, además de ser una oportunidad para dejar un regalo "mucho más vivo" a su familia y un testimonio audiovisual para todos los colombianos.

\[caption id="attachment\_10785" align="aligncenter" width="1600"\]![Daniela Abad, Directora](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/94W3mxD_YFa-iQci9zQ552iMUxesvx9Hc1pLUkcxSMkHZh820DMzSAV_txxHNSZDtfbcMvHSEQQT5KYqD3bps42OaPCummmgGpDIsFlwiGT6IJmW2KSQ8JYUnBOREhw1U.jpg){.wp-image-10785 .size-full width="1600" height="1073"} Cuando Asesinaron a su abuelo, Daniela Abad tenía un año de vida.\[/caption\]

Reconoce que la idea de realizar el documental no fue suya, pero sintió la necesidad de “proteger a su familia” eso tan íntimo que vive en cada fotografía, carta hablada (grabaciones enviadas por el médico durante sus múltiples viajes) “la voz tiene un efecto muy raro es tener a la otra persona muy cerca”, cada registro periodístico y todos los recuerdos de su paso por el mundo que ayudan a enriquecer su retrato.

La primera piedra de la producción fue puesta por un grupo de holandeses, equipo al que se integraría como cuota nacional, por sugerencia Abad Faciolince, el director Miguel Salazar (Robatierra, 2009, La toma, 2011) y más adelante Daniela, como estudiante de cine en el trasescena. Finalmente la relación con los holandeses se rompió por cuestiones relacionadas con el idioma y el entendimiento de la cultura colombiana entre otras, dejando la dirección en manos de los cineastas colombianos.

\[caption id="attachment\_10786" align="aligncenter" width="800"\][![Rodaje Carta a una sombra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/OVN3ioBp2wHNwlmkWGYoAiUXNUEAU1D3PflQNAMXePY.jpg){.wp-image-10786 .size-full width="800" height="600"}](https://archivo.contagioradio.com/la-memoria-de-hector-abad-gomez-en-carta-a-una-sombra/ovn3iobp2whnwlmkwgyoaiuxnueau1d3pflqnamxepy/) Héctor Abad Faciolince, a caballo durante el rodaje del Documental\[/caption\]

En palabras de Miguel Salazar “Carta a una sombra es una historia sencilla de un hombre que se dedicó a trabajar por los demás, a tratar de hacer una mejor sociedad, un hombre bueno, que lo mataron justamente por ser bueno que es un poco la paradoja de Colombia”.  
El documental refleja además la polarización y la intolerancia que se vive en el país en relación con el activismo social y militancia ideológica y política “luchar algunas veces por los demás puede ser llamado comunismo, esa polarización que ha estado tan presente en Colombia nos ha llevado a que el conflicto nunca termine” asegura Salazar.

A las voces de el escritor, de su madre Cecilia Faciolince y sus hermanas, se unen los recuerdos del recientemente fallecido **ex magistrado Carlos Gaviria Díaz**, factor que para Daniela le da un cierto valor añadido como documento histórico y de memoria " al registrar posiblemente algunas de sus últimas entrevistas en el documental".

<iframe src="https://www.youtube.com/embed/ScKuXDU4jBA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En tiempos en que se habla de paz y reconciliación , “Es importante que los colombianos conozcan a mi abuelo, no solo por él sino por los valores que rescataba", resalta la directora y agrega que aunque no buscaron este tiempo en particular para que saliera la película "es un momento optimista y mi abuelo era una persona optimista, que creía en el ser humano a pesar de todo".
