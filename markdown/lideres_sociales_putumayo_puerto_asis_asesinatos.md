Title: Guerra contra los líderes de sustitución se está tomando al Putumayo
Date: 2017-12-21 17:31
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Puerto Asís, Puerto Vega Teteye, Putumayo
Slug: lideres_sociales_putumayo_puerto_asis_asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/putumayo-teteye-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Miputumayo.com.co] 

###### [21 Dic 2017] 

[Solo en el mes de diciembre, en Putumayo han sido asesinados tres lideres sociales. El más reciente de esos hechos sucedió el pasado 18 de diciembre sobre las 10 de la noche, en la vereda Puerto Colombia en el municipio de Puerto Asís. Esta vez el crimen ha causado mayor conmoción, pues al lado de Pablo Oviedo, como se llamaba este lider comunitario, **murió su hija Karen, de apenas 8 años de edad, por cuenta de las mismas balas con las que impactaron a su padre.**]

De acuerdo con el relato de Andrea Alvis, integrante de la Red de Derechos Humanos de Putumayo, Pablo Oviedo había salido a atender a las fueras de su casa a unas personas que preguntaron por él. Enseguida hombres desconocidos le empezaron a disparar en repetidas ocasiones, generando que una de esas balas impactara en el cuerpo de la pequeña Karen que ni siquiera alcanzó a ser trasladada a un centro médico, mientras que su padre, falleció horas más tarde en el Hospital de San Francisco de Asís.

### **Las cifras de la violencia en Putumayo** 

Las cifras de la Red Nacional de Información (RNI) de la Unidad para las Víctimas, evidencian que el temor de las comunidades no es vano. En** Puerto Guzmán ya se cuentan un total de son 76 amenazas, en Puerto Leguízamo 43 y en Puerto Asís 33, la mayoría de estas son **contra la vida de dirigentes comunales y líderes de las comunidades que impulsan procesos de sustitución.

Asimismo, la Unidad de Víctimas tiene en sus registros, por lo menos cinco asesinatos de líderes sociales y excombatientes de las FARC entre octubre y diciembre. En este último mes ya se cuentan tres personas muertas, además de 1.181 desplazamientos forzados en lo que va del 2017.

### **Asesinatos sin discriminación alguna** 

Pablo, de 45 años, era un dirigente comunal de la vereda Puerto Colombia en el Corredor Puerto Vega Teteyé, donde vivía hace más 30 años. De acuerdo con Alvis, esa zona donde vivía este líder, se ha vuelto blanco de amenazas para quienes empujan a las familias a adherirse a los planes de sustitución de cultivos de uso ilícito, sumado a que otros **grupos armados buscan aprovechar los incumplimientos del gobierno para cooptar los territorios y tener el dominio del narcotráfico.**

La familia de Oviedo, ya había sido víctima del conflicto armado, pues uno de sus familiares había muerto en circunstancias violentas en medio de la guerra. Ante tal hecho, la Red de DDHH manifestó su preocupación debido a las situaciones de revicitmización a las que se están sometiendo las familias de esta región del país.

Según Alvis, no es la primera vez que a los violentos no les importa cegar la vida de quienes estén acompañando a los líderes, como es el caso de los niños y niñas. Precisamente, hace unos días, en el municipio de San Miguel, atentaron contra un presidente de junta y su hijo menor de edad, quienes resultaron heridos. "Son prácticas sucesoras del paramilitarismo. Esto está sucediendo indiscriminadamente, sin importar las vida de los niños", expresa la integrante de la Red de Derechos Humanos de Putumayo.

### **¿Qué están haciendo las autoridades?** 

Aunque se han creado una serie de mecanismos por parte del gobierno para hacer frente  a este tipo de situaciones, Andrea Alvis, señala que parece que fueran una serie elementos que solo se anunciaron para "hacer escándalo mediático".

El problema también radica en que el Ministerio del Interior ,que es responsable de la Ley de víctimas y el cumplimiento del acuerdo, no está reconociendo ni garantizando la participación de las organizaciones sociales y comunitarias del departamento, y en cambio estas han sido víctimas de estigmatizaciones por parte de la propia Fuerza Pública.

"Quienes nos hemos desgastado somos las organizaciones sociales con esta 'reunionitis', sin ningún tipo de respuesta contundente. Lo único que ha sucedido es la continuación de la estigmatización", dice la defensora de DDHH, y explica que **hace unos días fueron capturadas seis personas, que podrían ser víctimas de un montaje judicial, por la forma como fueron detenidas.**

Toda una situación que ha generado temor en la comunidad, pero también los lideres sociales que en muchos casos han decidido renunciar a su labor por la estigmatización y los asesinatos, pero además se están presentando desplazamientos de los pobladores que temen por su vida.

<iframe id="audio_22788499" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22788499_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
