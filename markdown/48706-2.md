Title: Estos serán los retos políticos de la FARC para el 2018
Date: 2017-11-02 13:04
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, Victoria Sandino
Slug: 48706-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Nov 2017] 

Grandes retos tendrá que asumir la FARC como partido político de cara a las próximas elecciones del 2018. Por un lado, está la apatía de la ciudadanía hacia los partidos políticos, el incumplimiento de los acuerdos de paz por parte del gobierno, la falta de trámite de proyectos como la Reforma Política y la JEP, y por el otro la falta de garantías que se evidencia en el asesinato de integrantes de este partido, líderes sociales y defensores de derechos humanos. **Así lo afirmó Victoria Sandino, quien representará a este partido en las elecciones a senado**.

### **Cuál será la política de las FARC** 

Para Sandino el desafío más grande se encuentra en materializar lo pactado en La Habana, a través de la implementación de los acuerdos de paz en todo el territorio colombiano, que debe ser incluyente tanto con las poblaciones campesinas y étnicas, como a las poblaciones urbanas. **De igual forma afirmó que para que este hecho sea una realidad debe darse una apertura democrática que permita una participación efectiva de la ciudadanía.**

“La política está plagada de corrupción, de muchos vicios y en esa medida lo que queremos es que la política sea la actividad cotidiana y permanente de toda la ciudadanía y eso **significa empezar a tratar los problemas que tiene la gente en sus comunidades**, en sus barrios, en su trabajo, en todas las esferas de la vida” afirmó Sandino.

Otro de esos desafíos, tanto para Victoria Sandino como para Sandra Ramírez, únicas delegadas de la FARC a las elecciones, es continuar en la defensa de la implementación de los Acuerdo con enfoque de género, para Sandino esta es una labor que al igual que la participación ciudadana, deberá realizarse desde la cotidianidad de las mujeres. (Le puede interesar:["La apuesta de las FARC es la "apertura política""](https://archivo.contagioradio.com/las-farc-iran-a-la-politica/))

### **La izquierda democrática hacia el 2018** 

Con el anuncio de las aspiraciones presidenciales de Rodrigo Londoño continúa abriéndose el espectro de candidatos desde la izquierda para las elecciones 2018, sin embargo, de acuerdo con Sandino, se está discutiendo **las posibles coaliciones que podrían darse con aquellos** que han manifestado su voluntad de seguir con la implementación de los acuerdos de paz.

“Seguiremos trabajando, por la gran coalición que creemos necesaria, por la gran convergencia de los sectores democráticos, no solamente por la campaña sino por lograr una presidencia democrática que le apueste a la implementación de los acuerdos y de afianzar la paz” expresó Sandino. (Le puede interesar: ["Atentan contra dos integrantes de las FARC en Ricaurte, Nariño"](https://archivo.contagioradio.com/atentan-contra-dos-integrantes-de-las-farc-en-ricaurte-narino/))

<iframe id="audio_21846911" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21846911_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
