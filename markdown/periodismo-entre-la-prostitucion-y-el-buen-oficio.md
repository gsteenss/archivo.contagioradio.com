Title: Periodismo: entre la prostitución y el buen oficio
Date: 2015-02-10 12:40
Author: CtgAdm
Category: Carolina, Opinion
Tags: carolina garzón, libertad de prensa en colombia, periodismo en colombia
Slug: periodismo-entre-la-prostitucion-y-el-buen-oficio
Status: published

#### Por [**[Carolina Garzón ]**](https://archivo.contagioradio.com/carolina-garzon-diaz/) 

El 9 de febrero en Colombia se celebra el Día del Periodista, una celebración que nace con la aparición del primer periódico “Papel Periódico de la Ciudad de Santafé de Bogotá”, dirigido por el cubano Manuel del Socorro Rodríguez. Desde aquel 9 de febrero de 1971 la fecha se ha convertido en un homenaje a los periodistas y una excusa para reflexionar sobre la situación de los periodistas en Colombia y los problemas de nuestro oficio.

**[El periodismo amordazado]**

Ser periodista en Colombia es una profesión peligrosa, según el reciente informe de la Fundación para la Libertad de Prensa -FLIP- “Durante el 2014 la FLIP registró 131 agresiones directas contra la prensa en Colombia.” Y a estas agresiones contra los periodistas se suma la impunidad “De esto dan cuenta los 3 casos de asesinatos de periodistas que prescribieron sin mayores avances en su investigación judicial y la falta de un fallo de segunda instancia en el proceso contra Ferney Tapasco por el crimen de Orlando Sierra, entre otros.”

Desde 1977 hasta 2015 en Colombia han sido asesinados 142 periodistas, quienes fueron silenciados por su labor y especialmente por denunciar la corrupción, la ilegalidad, los crímenes y las alianzas entre los políticos y empresarios con grupos paramilitares y narcotraficantes. A esta censura a los periodistas se añade la persecución, el ejemplo más claro es la Central de Inteligencia Militar “Andrómeda” dónde se interceptaban correos electrónicos de periodistas nacionales y extranjeros.

**[La prostitución del oficio]**

Sin embargo, el buen periodismo en los medios masivos no ha disminuido únicamente porque ha sido asesinado, amenazado o violentado. El buen periodismo ha muerto porque se ha vendido al mejor postor. Así lo advierte la FLIP cuando dice: “la pauta publicitaria sigue siendo un tema preocupante para la libertad de expresión.” Y efectivamente lo es. Los grupos económicos más poderosos del país son dueños de los medios de comunicación, y las empresas de gran capital gastan millonadas en pauta publicitaria. Lamentablemente de esa manera las empresas han logrado comprar las conciencias y la ética de los medios de comunicación y los periodistas. Ahora priman los publirreportajes, los elogios y la defensa de quienes pautan e, incluso, pagan comisiones a ciertos periodistas. Sobre esto recomiendo un artículo de Daniel Pardo titulado “Pacific ES Colombia” dónde devela esa prostitución del periodismo en medios como la WRadio, Confidencialcolombia.com y el portal Kien&Ke. Para reafirmar este punto, el propio portal Kien&Ke despidió al periodista Daniel Pardo después de publicar este artículo.

**[La esperanza del buen periodismo]**

La necesidad de la información diversa, amplia, objetiva, desde la base y con un enfoque crítico generó en Colombia el surgimiento de medios alternativos, comunitarios y rurales que han construido con tenacidad una visión más democrática de los medios de comunicación y del mismo periodismo. Creo que es a ellos y ellas a quienes se debe dedicar este día. El 9 de febrero, Día nacional del periodista, debe ser un reconocimiento a los comunicadores populares, los periodistas barriales, a las redes de comunicación rurales y los medios digitales y virtuales que han hecho de la información una herramienta para la construcción de la sociedad, la lectura crítica del país y la creación de un agenda mediática que contenga las voces de los otros, de quienes no son escuchados o quieren ser callados. Esa es la esencia misma de este oficio para el que, en palabras de Kapuscinski, los cínicos no sirven.

###### [Carolina Garzón Díaz ([@E\_vinna] 
