Title: Desmitificando la Marihuana en Colombia
Date: 2019-05-24 16:39
Author: CtgAdm
Category: Programas
Tags: colombia, Drogas, Hablemos Alguito, Marihuana, política antidrogas
Slug: desmitificando-la-marihuana-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-24-at-4.35.58-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Planoinformativo 

El próximo 20 de Julio, se presentará en el Congreso de la República, un proyecto de ley que busca la regularización de la marihuana para su uso recreativo. Propuesta que surge desde una alianza de congresistas de diferentes tendencias políticas, que tiene como intención generar una política pública que maneje de forma distinta el tema sobre drogas en Colombia, pasando de una medida punitiva a una de salud pública.

En este programa de Hablemos Alguito, los invitados [sociólogo especialista en atención  integral y prevención del consumo de sustancias psicoactivas y Coordinador de la estrategia "Pedagogía de las drogas" de la división de salud de  Universidad Nacional de Colombia, Gloria Miranda, Historiadora y Maestria Cum Laude en Construcción de Paz de la Universidad de los Andes y Yamid González, politólogo de la Universidad Nacional y autocultivador debatieron sobre la posibilidad de esa regularización de la Marihuana para uso recreativo. ]

Los participantes aseguraron que en Colombia aún falta mucho por hacer en el tema de pedagogía sobre el consumo de drogas y puntualmente, sobre el uso de la marihuana en sus múltiples usos. Razón por la cual, enfatizaron en la necesidad de analizar más a fondo la relación entre narcotráfico y consumidores, desde todas las aristas que tiene este debate, para superar la criminalización a quienes consumen y la falta de acciones eficaces contra las grandes cadenas de narcotráfico.

https://youtu.be/RSmdACPA9lI

Reciba toda la información de Contagio Radio en [su correo](https://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://bit.ly/1ICYhVU)
