Title: Gira del Movimiento Ríos Vivos denuncia problemáticas en torno a proyectos hidroeléctricos
Date: 2015-07-29 15:57
Category: Ambiente, DDHH
Tags: Cauca, Censat agua Viva, Defensa y Minas y Energía, El Instituto Colombiano de Desarrollo Rural, Hidroeléctricas, Hidroituango, hidrosogamoso, la Agencia Nacional de Licencias Ambientales, la Defensoría del Pueblo y la Embajada de Noruega, la Unidad de Víctimas y Restitución, la Unidad Nacional de Protección, mega priyectos, Ministerio del Interior, Movimiento Ríos Vivos, Quimbo, represas, Tatiana Roa
Slug: movimiento-rios-vivos-llego-a-bogota-para-visibilizar-problematicas-en-torno-a-proyectos-hidroelectricos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-título1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: No al Quimbo 

<iframe src="http://www.ivoox.com/player_ek_5528517_2_1.html?data=lpqfmpqVe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs4zGhqigh6aos9Sft87j0diPsM3ZyIqwlYqmd4zVjKfcydTYaaSnhqaejdXFtsKf187gy8fNsMruwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tatiana Roa, Censat Agua Viva] 

**Del 27 al 31 de julio el Movimiento Ríos Vivos** y las organizaciones que hacen parte de ese colectivo, realizan una **gira en Bogotá** con el objetivo de dar a conocer las problemáticas que se viven en las regiones cuando se intervienen los ríos para construir represas.

A su regreso a las regiones, el movimiento espera llegar con avances reales en la protección de **sus derechos y soluciones al despojo de tierras** al que han sido sometidos como consecuencia de la construcción de las hidroeléctricas.

Es por eso, que en el marco de esa gira, este miércoles se convocó en el Congreso de la República, un foro denominado, “**Extractivismo y experiencias de resistencia desde los territorios”,** donde participan diversas organizaciones que defienden los ríos de Colombia, y que han sido víctimas de diferentes violaciones a sus derechos humanos, por proteger sus comunidades y el ambiente.

Por ejemplo, la construcción de las represas **La Salvajina en el Cauca, Urrá en Córdoba, Hidrosogamoso en el departamento de Santander, El Quimbo en el Huila e Hidroituango en Antioquia,** están dejando a su paso el desplazamiento de comunidades que se han ido quedando sin tierras donde llevar adelante su proyecto de vida, pero también la desaparición forzada de un líder, **el asesinato de cinco en Santader, y 51 a nivel nacional,** las amenazas contra 31 líderes y lideresas de Ríos Vivos, la judicialización infundada de más de 25, la tortura de dos e intento de secuestro de dos más, así como el **desplazamiento de 100.000 personas** a nivel nacional por causa de las represas, como lo señala un comunicado de la organización.

En Bogotá, los líderes y lideresas se reunirán con algunos funcionarios  del Ministerio del Interior, Defensa y Minas y Energía, la Unidad Nacional de Protección, la Unidad de Víctimas y Restitución, el Instituto Colombiano de Desarrollo Rural, la Agencia Nacional de Licencias Ambientales, la Defensoría del Pueblo y la Embajada de Noruega, para encontrar soluciones a las problemáticas que viven sus comunidades por cuenta de los mega proyectos hidroeléctricos.
