Title: Cuestionan postura de Human Right Watch sobre la JEP
Date: 2017-07-17 14:43
Category: Nacional, Paz
Tags: Acuerdos de paz de la Habana, Cadena de Mando
Slug: cuestionan-postura-de-human-right-watch-sobre-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/jose_miguel_vivanco-hrw_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zonacero] 

###### [17 Jul 2017] 

Diego Martínez, asesor jurídico de los diálogos de paz de La Habana cuestionó la afirmación de José Miguel Vivanco, director de la organización Human Rigth Watch, sobre **la excesiva participación política de integrantes de esta guerrilla hace parte de una postura política regional en la que se intenta frenar el paso de alternativas democráticas**.

La afirmación de Vivanco hace parte de una serie de recomendaciones que la organización Human Right Watch le re hizo a la Corte Constitucional en la que señala “una disposición excesivamente amplia sobre la participación política para miembros de las FARC y una definición injustificadamente rígida de responsabilidad de mando para miembros de las Fuerzas Armadas que podría impedir que altos mandos de la fuerza pública rindan cuentas por los crímenes cometidos por sus subalternos”.

Para Martínez la restricción que sugiere la organización nunca se ha realizado en ningún proceso de paz en el mundo **“en la jurisprudencia internacional no existe ningún antecedente cercano a ese tipo**” aseguró Martínez y agregó “hay una posición muy política por parte de esta ONG”. (Le puede interesar: ["Lísto el censo socioeconómico de las FARC-EP"](https://archivo.contagioradio.com/43216/))

### **Las tensiones en la definición de cadena de mando de las Fuerzas Militares** 

Referente a la definición que existe de la cadena de mando en la Jurisdicción Especial de Paz, Martínez expresó que hay que esperar a la consideración que haga la Corte Constitucional “**de lo que se trata es que la Corte avale los principios de la JEP que definitivamente hablan del respeto al DIH y el respeto al Derecho Internacional de los derechos humanos**”.

Frente a cómo blindar los Acuerdos de Paz, para que no se genere un marco de impunidad, Martínez manifestó que ahí se deberían accionar los límites del derecho Internacional y un ejemplo de ello fue cuando las Fuerzas militares manifestaron su interés por excluir el principio de interpretación de cadena de mano y **fue la Corte Penal Internacional la que hizo un llamado para que se no se eviten las investigaciones correspondientes o de lo contrario será esta instancia la que las asuma.**

“Hoy están en la mira los altos funcionarios del Estado por intentar excluirse de un marco de competencia judicial, están en la mira los terceros financiadores que también pretenden excluirse de marco de justicia y lo que dicen las organizaciones internacionales es que nadie puede excluirse” aseveró Martínez. (Le puede interesar:["Si estado no juzga a empresarios y mandos militares que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/))

En este mismo sentido y referente a la posibilidad de que se puedan juzgar parapolíticos en la JEP, el asesor jurídico de las FARC, aseguró que “en el caso de los parapolíticos que no hayan sido sancionados por la justicia colombiana, deben entrar en la JEP”. De esta manera los **políticos que financien aún estructuras paramilitares podrán ser investigados**, contrario esto a lo asegurado por la CSJ en torno a que ellos no deberían estar en la JEP.

### **La ley de Amnistía y la falta de su ejecución** 

Desde hace tres semanas más de 1.500 prisioneros políticos se mantienen en huelga de hambre debido a los incumplimientos del Estado al otorgar las amnistías, para Martínez esta situación tiene dos orígenes. Uno tiene que ver con las **inequívocas interpretaciones de los jueces de ejecución de pena a la hora de aplicar la amnistía**.

En segundo lugar, son las serias falencias en la rama judicial “recordemos las recientes investigaciones de corrupción al jefe de anticorrupción o los casos del Meta que evidencian una rama judicial corrupta y que se opone a los acuerdos y ahí hay una serie de deficiencias” señaló Martínez. (Le puede interesar:["No hay errores en la Ley de Amnistía, lo que hay es negligencia del Estado: Santrich"](https://archivo.contagioradio.com/no-hay-errores-en-la-ley-de-amnistia-lo-que-hay-es-negligencia-del-estado-santrich/))

<iframe id="audio_19843574" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19843574_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
