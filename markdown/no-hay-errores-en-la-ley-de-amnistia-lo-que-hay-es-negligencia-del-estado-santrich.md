Title: No hay errores en la ley de Amnistía, lo que hay es negligencia del Estado: Santrich
Date: 2017-07-06 12:27
Category: Entrevistas, Paz
Tags: acuerdos de paz, Huelga de Hambre FARC-EP
Slug: no-hay-errores-en-la-ley-de-amnistia-lo-que-hay-es-negligencia-del-estado-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SANTRICH.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Jul 2017] 

De acuerdo con Jesús Santrich, integrante del secretariado de las FARC, desde que inicio la huelga de hambre de los más de **1.500 prisioneros políticos que hay, tan solo se han liberado 22** que ya fueron trasladados a la zona veredal de Icononzo y tampoco hay algún tipo de respuesta de parte del Estado.

Santrich aseguró que la toma de esta decisión para ejercer presión no debe ser entendida como un favor que se le está pidiendo al Gobierno, sino **como exigencia al cumplimiento de los Acuerdos de Paz de La Habana**, además reafirmó que no es cierto que las amnistías sean negadas por fallas en el mecanismo jurídico.

“En el caso de la Ley de Amnistía yo creo que la falla protuberante que tiene de fondo, es **haber tenido la buena fe en que los jueces de ejecución de pena iban a querer cumplir** con esa ley y en la confianza que se tuvo de buena fe con el Estado” señaló Santrich.  (Le puede interesar: ["Amenazan con aislamiento a presos de las FARC en huelga de hambre"](https://archivo.contagioradio.com/amenazan-con-aislamiento-a-presos-de-las-farc-en-huelga-de-hambre/))

Frente a los rumores sobre su estado de salud, el integrante del secretariado expresó que se encuentra bien y con voluntad de seguir adelante en la huelga, sin embargo, manifestó que pese a que no se tiene aún la cifra de los **prisioneros políticos en las cárceles que afrontan situaciones difíciles de salud, crece la preocupación por sus condiciones.** (Le puede interesar:["Más de 1.400 presos políticos de las FARC continúan en huelga de hambre"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre/))

De igual forma reitero que la huelga de hambre se levantará hasta el día en que estén en libertad todos los presos políticos que son beneficiarios de la ley de Amnistía y manifestó que estos incumplimientos del gobierno, que se suman a otros como la falta de construcción de las zonas veredales, debe ser un llamado a la sociedad para que se volqué en la defensa de los acuerdos y **se evite que se conviertan en el mayor “falso positivo” contra la paz.**

<iframe id="audio_19662887" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19662887_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
