Title: Ganadores y ganadoras del Premio Nacional a la defensa DD.HH. 2019
Date: 2019-09-04 16:41
Author: CtgAdm
Category: DDHH, eventos
Tags: acciones de cambio, Lideres, lideresas, Premio a la defensa de los derechos humanos, Universidad Javeriana
Slug: ganadores-del-premio-nacional-a-la-defensa-de-derechos-humanos-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-1.19.05-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.25-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.16-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.21-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.15-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

Se llevó a cabo **la octava  versión del Premio Nacional a la Defensa de los Derechos Humanos Diakonia** que otorga un reconocimiento público en 4 categorías que buscan visibilizar el aporte de los y las defensoras de derechos humanos a la construcción de democracia y paz en Colombia.

Los ganadores de este año son  reconocidos  por la trascendencia de sus acciones y  trabajo en zonas como  Cauca,   Catatumbo, Simitarra, Arauca, entre otras regiones envueltas en el conflicto armado, también es un reconocimiento a la persistencia y valentia en la defensa de causas sociales en medio de las situación de violencia, persecución, y asesinato  a los lideres.

### **Categoría 1: «Defensor o Defensora del Año** 

En la categoría de Defensor o Defensora del año, el premio fue otorgado Clemencia Carabalí Rodallega presidenta de la Asociación de Mujeres Afrodescendientes del Norte de Cauca (ASOM), quien ha trabajado más de 30 años por la defensa de los territorios ancestrales de las comunidades negras y los derechos de las mujeres, promoviendo la autonomía económica  femenina y el desarrollo de la fuerza tradicional del pueblo negro.

![Ganadores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.25-PM-1024x573.jpeg){.aligncenter .size-large .wp-image-73106 width="1024" height="573"}

###### [Foto:Contagio Radio] 

### **Categoría 2A: «Experiencia o Proceso Colectivo del Año (Proceso Social Comunitario)»** 

La Asociación Campesina del Valle del Río de Cimitarra  , fue la que se llevo el reconocimiento a la Experiencia o Proceso Colectivo del Año. Esta asociación trabaja en la promoción y defensa de los derecho humanos, además desarrollan trabajos sociales, comunitarios y políticos, en esta región del Magdalena Medio donde los intereses de las grandes empresas y los megas proyectos manejan la agenda económica.

###### [Foto:Contagio Radio]![Ganadores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.16-PM-1024x813.jpeg)

**Categoría 2B: «Experiencia o Proceso Colectivo del Año (Modalidad de ONG)»**

En esta categoría, el galardón se lo llevó la Consultoría para los Derechos Humanos y el Desplazamiento (CODHES), esta organización investiga las razones, contextos y responsables de las graves violaciones a los derechos humanos en Colombia, aparte esta alerta a brindar respuestas y oportunidades  la crisis humanitaria cuando se dan crimines de que vulneren los derechos.

###### ![Ganadores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.21-PM-1024x640.jpeg)

###### [Foto:Contagio Radio] 

### **Categoría 3: Reconocimiento a «Toda una Vida»** 

Ricardo Esquivia Ballestas, fue la galardonada, un reconocimiento a su labor como defensor de derechos humanos por mas de 40 años, ha sido participe en la reconstrucción de lo ocurrido durante el conflicto, el proyecto que mas lo destaca son los "diálogos improbables" un espacio creado para unir voces de actores del conflicto que jamás se contrariaran en un contexto cotidiano.

![Ganadores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-04-at-3.48.15-PM-1024x577.jpeg){.aligncenter .size-large .wp-image-73109 width="1024" height="577"}

###### [Foto:Contagio Radio] 

**Homenaje a Maria Mercedes Méndez **

De igual forma este año se hizo un homenaje especial a la política y  activista  ** María Mercedes Méndez de Garcia, defensora  de derechos humanos asesinada el 3 de junio de 1992 en Caño Silbao, El Castillo-Meta. **Fue la primera alcaldesa elegida por votación popular en El Castillo, su homenaje fue un recordatorio hacia todas las víctimas de los últimos meses, candidatos y candidatas  que han perdido su vida en el marco de estas elecciones regionales en el país.

Recordando su vida y su trabajo , sus cuatro hijas hicieron un llamado al gobierno exigiendo la  no  impunidad y hacen un llamado al estado colombiano para garantizar el derecho a la verdad y la justicia, "mas que esperanza necesitamos acción, una vez que empieza a hacerlo encontramos esperanza en todas partes"

El Programa Colombia de Diakonia en conjunto con la Iglesia Sueca, ha otorgado este premio desde el año 2012 **como un mecanismo de respaldo al trabajo que realizan los defensores de derechos humanos y del territorio en el país**.

Como en años pasados, el reconocimiento a los hombres, mujeres y organizaciones que contribuyen a la democracia y la paz se hará como parte de un mecanismo de protección ante la alarmante situación de riesgo en la que viven estas personas en Colombia. (Le puede interesar: [No vamos a salir de nuestro territorio, no vamos a dejar nuestra casa: líderes sociales del Cauca](https://archivo.contagioradio.com/no-vamos-salir-nuestro-territorio-no-vamos-a-dejar-nuestra-casa-lideres-sociales-cauca/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
