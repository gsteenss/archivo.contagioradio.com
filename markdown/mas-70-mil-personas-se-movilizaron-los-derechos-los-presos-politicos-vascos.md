Title: Más de 70 mil personas se movilizaron por los derechos de los presos políticos vascos
Date: 2017-01-16 11:38
Category: DDHH, El mundo
Tags: Bilbao, ETA, País Vasco, presos politicos
Slug: mas-70-mil-personas-se-movilizaron-los-derechos-los-presos-politicos-vascos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/PRESOS-ETA-e1484584464238.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Confidencial 

###### [16 Ene 2016]

Sin importar las fuertes lluvias, sobre las 5:40 de la tarde del sábado, cerca de 70 mil personas se congregaron en La Casilla, Bilbao, para marchar por los presos políticos del País Vasco. Esta vez con el lema *Giza eskubideen, konponbidearen eta bakearen alde. Euskal presoak, Euskal Herrira,* **Derechos Humanos, resolución y paz,** aseguraron que esperan esta sea la última movilización.

Las calles de la ciudad gritaron para exigir el fin de la dispersión de los presos de ETA y un cambio en la política penitenciaria del Gobierno español. “Es una jornada importante para **denunciar el tratamiento de los presos enfermos, es una lucha necesaria y de solidaridad,** en contra de las acusaciones falsas, seguimiento, detenciones arbitrarias y montajes policiales”, expresó Elena Ortega, madre del preso político madrileño Alfon, quien agregó que la sociedad debe denunciar esta situación, y el Estado debe garantizar los derechos humanos de los presos, teniendo en cuenta que se trata de un Estado democrático.

A Bilbao llegaron caravanas de diferentes lugares de España para apoyar la movilización y las actividades que se realizaron durante todo el día que buscaban denunciar ante el mundo la situación en la que se encuentran los presos vascos, según anunciaron los voceros de la **Plataforma Sare,** que convocó la manifestación.

Oscar Reina, vocero del sindicato nacional Andaluz de trabajadores, fue una de esas tantas personas que llegó desde otros lugares del territorio español, para acompañar la movilización. “Nos ha traído creer en otro mundo más justo. Lo que ocurre es un crimen de lesa humanidad, la dispersión a la que condenan a los presos vascos va en contra de la declaración de los Derecho Humanos. Hoy demostramos que no estamos de acuerdo con esa política bárbara del **Estado español, que criminaliza a quienes pedimos libertad, independencia y soberanía**”.

La movilización incluso fue apoyada por personas como **Rosa Rodero, viuda del sargento de la Ertzaintza (la policía) asesinado por ETA,** quien aseguró a medios locales que lucha para que a los presos se les cumplan sus derechos, aunque no respalde una amnistía general. “Lo único que queremos es llegar a la paz”, afirmó.

Hoy 273 integrantes y simpatizantes de ETA cumplen condena en 40 cárceles de toda España, mientras otros 78 se encuentran en prisiones de Francia. Justamente es por eso, que la manifestación exigió el fin de la dispersión pues los familiares de los reclusos denuncian que deben viajar horas cada fin de semana para visitarlos.

La marcha que avanzó por la calle Autonomía, y que luego llegó al Ayuntamiento de Bilbao, finalizó con el discurso de Nerea Alias y Andoni Aizpuru en representación de Sare. Allí denunciaron que el **Estado mantiene en prisión “a presos que desde hace tiempo cumplieron la condena impuesta por los tribunales”** porque no se les aplica la acumulación de penas, “a pesar de que la normativa europea obliga a España a hacerlo” por lo que aseguraron que se está aplicando una política penitenciaria de “venganza”. Sin embargo, afirmaron que el “2017 es un año decisivo” para la resolución de ese conflicto.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
