Title: Yeison Franco Sánchez, ¿otro falso positivo judicial?
Date: 2020-01-22 15:45
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: falso positivo judicial, GOES, Juzgados, policia
Slug: yeison-franco-sanchez-otro-falso-positivo-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Captura-de-Yeison-Franco-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado martes 21 de enero agentes de Policía llegaron a la residencia familiar de **Yeison Franco Sánchez**, estudiante de Trabajo Social de la Universidad Colegio Mayor de Cundinamarca para realizar un allanamiento. Los uniformados buscaban pruebas contra el joven por el delito de concierto para delinquir sin embargo, personas cercanas a él afirman que se trataría de un falso positivo judicial.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El allanamiento y captura, procedimientos irregulares**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según relató **Laura Escobar, compañera sentimental de Yeison,** ambos estaban juntos sobre el medio día cuando recibieron una llamada de la mamá de Yeison contándole sobre el allanamiento en la casa. La mamá también les anunció que lo buscaban por el delito de concierto para delinquir, razón por la que el joven se alistó para presentarse ante las autoridades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a las personas que estuvieron durante el allanamiento, integrantes del Grupo de Operativos Especiales de Seguridad (GOES) de la Policía irrumpieron en la propiedad y **obligaron a las personas que estaban presentes a tirar los celulares al un lado,** impidiendo su comunicación con otras personas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/gd_inmuebles/status/1219681550670925824","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/gd\_inmuebles/status/1219681550670925824

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Posteriormente, permitieron la comunicación e hicieron el allanamiento que duró cerca de tres horas, llevándose elementos como periódicos y un computador. No obstante, Escobar sostuvo que **no les permitieron ver el acta de aquello que se llevaron de la casa.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras Yeison Franco estaba dispuesto para comparecer ante las autoridades, efectivos de la Policía los interceptaron y les ordenaron que se detuvieran. Un uniformado que se identificó como el subintendente Fernández, leyó sus derechos y dijo que lo trasladarían a la Unidad de Reacción Inmediata (URI) de Puente Aranda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Después de buscarlo en la URI y en otros dos sitios donde supuestamente sería dirigido el estudiante, en conjunto con organizaciones defensoras de derechos humanos se puso una alerta, tomando en cuenta que **Yeison fue detenido-desaparecido hasta las 11 de la noche**, momento en el que lograron constatar su ingreso a la estación de Los Mártires.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **En este momento está en audiencia de legalización de captura**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Escobar** señaló que mientras era capturado, a Yeison le dijeron que le imputarían el delito de concierto para delinquir por su participación en las marchas y estar en ciertos colectivos; sin embargo **sus allegados afirman que su vida cotidiana se limita al estudio y el trabajo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este momento se adelanta su audiencia de legalización de captura ante un juez de control de garantías en los juzgados de Paloquemao, en Bogotá. (Le puede interesar:["En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras"](https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualización: El juez legalizó su captura, y Yeison continuará en reclusión a la espera de nueva audiencia que tome definiciones sobre el caso.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
