Title: Iván Cepeda anuncia que recusará al Fiscal Barbosa en proceso contra Álvaro Uribe
Date: 2020-08-31 14:50
Author: CtgAdm
Category: Nacional
Slug: ivan-cepeda-recusara-a-fiscal-barbosa-en-proceso-contra-alvaro-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Cepeda-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras conocerse que la Corte Suprema de Justicia remitirá a la Fiscalía el proceso contra Álvaro Uribe por los delitos de fraude procesal y soborno, el senador Iván Cepeda anunció que recusará al fiscal Francisco Barbosa pues este no podría asumir la investigación. (Le puede interesar: [No queremos entrar en un júbilo ingenuo: Iván Cepeda](https://archivo.contagioradio.com/no-queremos-entrar-en-un-jubilo-ingenuo-ivan-cepeda/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una rueda de prensa ofrecida este lunes, el Senador del [Polo Democrático](https://twitter.com/PoloDemocratico) anunció que, aunque no han recibido la notificación por parte de la Corte Suprema de Justicia, aceptan y respetan esta decisión del alto tribunal y por consiguiente acudirán a esa instancia para continuar en la búsqueda de la justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El anuncio se hace unas horas después de que se conociera, extraoficialmente, que la Sala de Instrucción de la Corte Suprema de Justicia habría resuelto que perdió la competencia tras la renuncia de Uribe al senado de la república y que ya no tendría fuero para seguir acudiendo ante esa instancia. (Lea también: [Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado del senador Cepeda, Reinaldo Villalba aseguró que lo más seguro es que el actual Fiscal, no esté habilitado para asumir el proceso dada la cercanía de Barbosa con el actual Presidente de Colombia, por tal motivo, recusarán al fiscal y quien tendría que nombrar un fiscal ad hoc sería la propia Corte Suprema de Justicia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Proceso de Álvaro Uribe en la Fiscalía podría ser una garantía de impunidad**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador Cepeda aseguró que, contrario a lo que hace la defensa de Uribe, esa parte del proceso si acata las decisiones de la justicia y los tribunales. Sin embargo, son varios los puntos de análisis que aseguran que muy posiblemente el caso del exsenador en la Fiscalía tenga menos avances que en la Corte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace un par de  semanas, Reinaldo Villalba, abogado de Iván Cepeda en este caso, había señalado que Uribe buscaba huir de la competencia de la Corte porque «*finalmente la Fiscalía General de la Nación está en manos de un ‘super amigo’ del Presidente de la República*»; que allí no iba  a haber una garantía de independencia e imparcialidad; y que existía «*una desconfianza plena sobre lo que se pueda resolver*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar además, que Daniel Hernández, fiscal delegado para el caso del abogado Diego Cadena, el cual versa sobre los mismos hechos y presuntos delitos —que viene siendo tramitado en la Fiscalía, dado que Cadena no tiene fuero— en la audiencia de imputación contra este, sugirió directamente al Juez que supuestamente Álvaro Uribe no conocía sobre los ofrecimientos de su abogado a los testigos y que había sido engañado por Cadena; hecho que desde ya, deja entrever cuál podría ser la postura del ente acusador frente a la responsabilidad del expresidente. (Lea también: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
