Title: “Presidente Santos le hizo conejo a los pensionados”: Alirio Uribe Muñoz
Date: 2018-06-22 14:33
Author: ambiente y sociedad
Category: Economía, Nacional
Tags: Alirio Uribe Muñoz, Centro Democrático, OCDE, Pensión
Slug: presidente-santos-les-hizo-conejo-a-los-pensionados-alirio-uribe-munoz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Querido_Dinero-MILLENNIANS-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Querido Dinero] 

###### [22 Jun 2018] 

El representante a la cámara por Bogotá del Polo Democrático Alternativo (PDA), Alirio Uribe Muñoz, lamentó que la Corte Constitucional tumbara el Proyecto de Ley 170 de 2016, mediante el cual se buscaba reducir del 12% al 4% el aporte de los pensionados al Régimen Contributivo. Iniciativa que se hundió **pese al apoyo que habían manifestado tanto el presidente Juan Manuel Santos, como el ministro de hacienda, en vísperas de las elecciones presidenciales del 2014.** (Le puede interesar: ["Santos incumplió promesa que hizo a los pensionado"](https://archivo.contagioradio.com/ley-que-objeto-santos-buscaba-eliminar-inequidad-con-los-pensionados/))

Aunque fue la sala plena del alto tribunal la encargada de negar el proyecto de ley, Alirio Uribe indicó que se trata en realidad de un hecho contra evidente, puesto que el argumento de la Corte fue que éste no contaba con el aval del gobierno, que en los últimos debates de la ley se opuso afirmando que dicho proyecto, significaba un déficit en el sistema de salud.

Sin embargo, el Representante Uribe Muñoz sostuvo que **había un análisis financiero que avalaba el proyecto**, a lo que añadió que “el Congreso hizo todo lo que tenía que hacer: expidió la ley y rechazo las objeciones presidenciales, pero será el nuevo congreso el que tendrá que promover otra ley”.

### **El nuevo gobierno y las pensiones** 

De cara a la posibilidad de que haya un nuevo proyecto de ley en favor de los pensionados, el representante es escéptico, pues hay factores que condicionan la intención de satisfacer las demandas de los beneficiarios del sistema de pensiones: El pasado gobierno de Álvaro Uribe, y la entrada de Colombia a la Organización para la Cooperación y el Desarrollo Económico (OCDE).

Siendo el Senador Álvaro Uribe, líder de la colectividad de la cual hace parte el  presidente electo Iván Duque, es posible suponer que el próximo gobierno del Centro Democrático (CD) no será favorable en términos de pensiones. Esto, en razón de que, como lo señaló Alirio Uribe, **fue el líder del CD quien mediante el acto legislativo 01 del 2005 elimino 1 de las 14 mesadas que recibían los pensionados, y adicionalmente restringió otros beneficios para los cotizantes.**

Y de otra parte están las recomendaciones de la OCDE, que según Alirio son bastante claras: Aumentar edades de pensión, igualar las edades entre hombres y mujeres, incrementar las cotizaciones, aceptar que la pensión sea inferior al salario mínimo y acabar con el sistema público de pensiones, que según el representante a la cámara, “a día de hoy paga cerca del 85% de las pensiones en Colombia”. (Le puede interesar: ["OCDE propone igualar edad de jubilación de hombres y mujeres"](https://archivo.contagioradio.com/ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres/))

Por estas razones, Alirio Uribe concluyo que **el movimiento de pensionados tiene que mantenerse, y seguir organizado** para seguir buscando las reformas que le convienen al grueso de los afiliados al sistema y no solo a unos pocos.

<iframe id="audio_26682776" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26682776_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
