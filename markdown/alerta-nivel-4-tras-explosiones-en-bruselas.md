Title: Alerta nivel 4 tras explosiones en Bruselas
Date: 2016-03-22 12:41
Category: El mundo, Política
Tags: atentado bruselas, belgica, Estado Islámico
Slug: alerta-nivel-4-tras-explosiones-en-bruselas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Bruselas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario La Guaira ] 

<iframe src="http://co.ivoox.com/es/player_ek_10897298_2_1.html?data=kpWlm5yWfZmhhpywj5WaaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaLgxtfhw5DSrdfZzZChjdnWpdSfxt3dztTXrdDixtiYx9OPhtPp1MrZw9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ioanna Gimnopoulou, 'Solidaridad Socialista'] 

###### [22 Mar 2016 ] 

De acuerdo con los reportes más recientes, las explosiones en el aeropuerto de Brucelas y en la estación de metro de Maelbeek, han provocado la **muerte de 34 personas y herido a otras 215 de quienes hasta el momento no se conoce su identidad**, según afirma Ioanna Gimnopoulou, integrante de la ONG 'Solidaridad Socialista'.

"Estamos consternados pero no sorprendidos", asevera Gimnopoulou, en referencia a la situación de **constante alarma que se vive en la capital belga tras los ataques terroristas ocurridos en París**, un ambiente de inseguridad en el que ha aumentado la presencia de militares en las calles de Bruselas.

Pese a que distintas cadenas informativas han llegado a asegurar que el Estado Islámico se atribuye la comisión de estos atentados, según Gimnopoulou **hasta el momento no hay un pronunciamiento oficial frente a la autoría** material o intelectual de las explosiones.

Por cuenta del ambiente de pánico que se vive en Bruselas las autoridades han decretado 3 días de luto y alerta nivel 4, **han ordenado a los ciudadanos no hacer uso del transporte público, ni permanecer por mucho tiempo en las estaciones ferroviarias** y han solicitado la donación de sangre.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

   
   
 
