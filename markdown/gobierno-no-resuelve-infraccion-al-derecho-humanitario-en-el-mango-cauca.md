Title: Gobierno no resuelve infracción al derecho humanitario en El Mango, Cauca
Date: 2015-07-03 11:15
Category: DDHH, Nacional
Tags: Argelia, Cauca, Cese al fuego bilateral, cese al fuego unilateral, derecho humanitario, el mango, FARC
Slug: gobierno-no-resuelve-infraccion-al-derecho-humanitario-en-el-mango-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/El-Mango-policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lizeth Montero 

<iframe src="http://www.ivoox.com/player_ek_4718827_2_1.html?data=lZyemp2We46ZmKiakpiJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPstCf08rg18rQusafytPT1MbHp8qZpJiSpJjSb8LgjMnS1MrHrNCfydraw9PNuMLmytSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Habitante de El Mango, Cauca] 

###### [3 de julio 2015]

En horas de la madrugada la guerrilla de las **FARC - EP atacó a las unidades policiales que se ubicaron en el punto conocido como El Cedro,** corregimiento de El Mango, ubicado al lado de la población civil en el departamento del Cauca.

De acuerdo con pobladores del municipio de Argelia, las unidades policiales respondieron  al hostigamiento que se prolongó por  varios minutos. A menos de una hora del hostigamiento se produjo un sobrevuelo del avión fantasma. Antes del amanecer ingresaron nuevas tanquetas con más unidades regulares de la policía.

La continuidad de la **presencia policial en un lugar donde se encuentran las bases de una entidad educativa sigue siendo una infracción al derecho humanitario**. Aunque los habitantes lograron sacar a la policía del centro del poblado como una expresión de desobediencia civil y pacífica, aún  el gobierno no resuelve el problema de fondo.

Los habitantes de El Mango continúan exigiendo el acatamiento a las normas humanitarias por parte de la policía sobre los civiles y sus bienes, pues siguen sufriendo zozobra y temor frente a lo que pueda ocurrir con sus vidas.

En su resistencia los habitantes **han exigido a las partes acordar  el Cese Bilateral al fuego,** medida que resolvería la crisis humanitaria en diversas regiones de Colombia, luego de la terminación del cese unilateral por parte de las FARC EP, por los bombardeos de las Fuerzas Militares, ordenadas por el presidente Santos en la que cayeron algunos de sus negociadores que se encontraban en un proceso pedagógico en sus filas.
