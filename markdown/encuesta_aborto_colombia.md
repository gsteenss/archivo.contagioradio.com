Title: Las nuevas visiones de la sociedad colombiana frente al aborto
Date: 2017-09-28 16:47
Category: Mujer, Nacional
Tags: aborto, embarazo
Slug: encuesta_aborto_colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto1-e1506634623556.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: vaconfirmamendoza.com.ar 

###### [28 Sep 2017] 

**El 65% de la sociedad colombiana está de acuerdo con el aborto en las tres causales que ha establecido la Corte Constitucional.** Así lo concluye una encuesta que ha decidido realizar la Mesa por la Vida y la Salud de las Mujeres con el objetivo de conocer la percepción de las personas frente al aborto en Colombia, en el marco del Día de Acción Global por un Aborto Legal, Seguro y Gratuito.

"Hay una valoración positiva tras la encuesta. La sociedad respalda decisiones libres de las mujeres, pero también se evidencia que hay retos frente a la comprensión de la autonomía de las decisiones de las mujeres", señala Juliana Martínez, coordinadora de la organización que llevó a cabo la encuesta.

Para Martínez, es necesario visibilizar que **la penalización del aborto constituye un tipo de violencia hacia las mujeres,** y en esa línea, no solo se debe trabajar en que en los países de América Latina despenalicen la interrupción voluntaria del embarazo, sino que también sea la sociedad la que deje de criminalizar a quienes deciden tomar esta vía.

### Sobre los derechos sexuales de las mujeres 

Otro de los resultados importantes de la encuesta que se realizó en las 32 ciudades capitales del país, es que **el 74% de las personas consideran que el Estado debe garantizar la protección de derechos sexuales de las mujeres, **en esa medida, "Entre los hallazgos del estudio se destaca que el 80% de las personas encuestadas respondieron que consideran muy importante que su candidato/a presidencial defienda los derechos de las mujeres y más del 70% afirmaron que es muy importante que defienda específicamente los derechos sexuales y reproductivos", dice la organización encuestadora. [(Le puede interesar: ABC de la despenalización del aborto en Colombia)](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)

Asimismo **la mitad de las personas consultadas no están de acuerdo con que las mujeres vayan a la cárcel por tomar esta decisión; frente al 25% que consideran que las mujeres sí deben ir a la cárcel. **A su vez, el 62% de las y los participantes de la encuesta reconocieron que el aborto es una decisión sólo de la mujer. Lo que da cuenta de que poco a poco en Colombia se entiende que la paz y el progreso también están directamente conectados con el hecho de que las mujeres decidan sobre su cuerpo.

"A pesar de que los avances han sido tímidos, resaltamos que las percepciones de la población colombiana están cambiando, y que la IVE se reconoce como un derecho fundamental de las mujeres, tal como lo señaló la Corte Constitucional en la Sentencia C-355 de 2006", dice la Mesa.

### Las propuestas y el contexo 

De acuerdo con datos de la **Mesa por la Vida y la Salud de las Mujeres,** en América Latina y el Caribe cada día se realizan 55 mil abortos inseguros en el mundo de los cuales, el 95% se dan en países en vías de desarrollo. Esa situación es la causa de 1 de cada 3 muertes maternas en la región.

En Colombia se realizan** cerca de 400.000 abortos al año, de los cuales solo 3400 se hacen de manera segura.** De allí que estén naciendo nuevas iniciativas de redes de mujeres que buscan acompañar a quienes optan por interrumpir sus embarazos, es así como este 28 de septiembre fue presentada 'Parceras'. "Un llamado a tejer solidaridades entre mujeres, a generar ‘parcería’ en casos concretos. Abortar no es tan chévere si se está sola, entonces mucho mejor que sea acompañada”, sostiene la abogada Sofía Piñeros, integrante de esa propuesta.[(Le puede interesar: Las resistencias en América Latina desde el aborto)](https://archivo.contagioradio.com/resistencias_america_latina_aborto/)

Con esta iniciativa feminista se espera llegar a todo tipo de mujeres que se encuentren en medio de esta situación, presionadas por su entorno. "**Somos la respuesta a un Estado que, a través de leyes, nos dice que prefiere vernos presas y muertas antes que libres, sanas y tomando decisiones sobre nuestros cuerpos”**, señala la red.

<iframe id="audio_21159750" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21159750_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
