Title: Sedes regionales de la UIS exigen matrícula cero para sus estudiantes
Date: 2020-09-03 18:57
Author: AdminContagio
Category: Actualidad, Educación
Tags: Matrícula cero, Santander, uis
Slug: sedes-regionales-de-la-uis-exigen-matricula-cero-para-sus-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-03-at-5.07.52-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-03-at-5.51.50-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: UIS / Archivo Personal

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la Gobernación de Santander anunció en el mes de mayo que pagaría la matrícula de estudiantes de la [Universidad Industrial de Santander](https://twitter.com/UIS) (UIS[)](https://twitter.com/UIS) de los estratos 1 y 2 beneficiando a cerca de 6.500 estudiantes aportando un valor cercano a los \$3.200 millones, sus sedes regionales aún no han recolectado los recursos necesarios para que más de 1.000 estudiantes puedan acceder a la educación superior.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la actualidad, la sede metropolitana cuenta con el 83% de sus estudiantes de pregrado reciben apoyo para el pago de su matrícula del segundo semestre del 2020, el 31% reciben un apoyo parcial mientras el 52% reciben el apoyo del 100& de la totalidad de la matrícula. Un triunfo del movimiento estudiantil que durante el mes de julio por medio de un plantón exigió a los directivos cumplir con el compromiso adquirido con los estudiantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, según explica María Aguilera, representante estudiantil de la UIS las sedes regionales en **Barrancabermeja, Barbosa, Málaga y Socorro** aún no cuentan con una matrícula cero universal por lo que se está pidiendo la ayuda de las Alcaldías de cada municipio. [Matrícula Cero: la clave para garantizar la educación pública superior](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":89333,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-03-at-5.07.52-PM.jpeg){.wp-image-89333}  

<figcaption>
Foto: Archivo personal

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde la semana pasada desde las rectorías de las cuatro sedes surgieron algunas cartas para las Secretarías de Educación y Alcaldías solicitando su apoyo, algunas han manifestado que no existen rubros para subsidiar la matrícula de estudiantes en las universidades públicas del área metropolitana y del departamento. [(Lea también: Sí hay recursos para Avianca pero no para sectores más vulnerables)](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Daniela Cáceres, representante estudiantil** de la Sede Barrancabermeja relata que ante la ausencia de recursos para pagar la matrícula, mucho estudiantes están pensando en posponer o cancelar su semestre. **"Lo que estamos analizando del presuuesto es que se requieren más o menos 5.000 millones de pesos para hacer un cubrimiento total".** [(Lea también: Retos para una educación digna en tiempos de pandemia)](https://archivo.contagioradio.com/retos-para-una-educacion-digna-en-tiempos-de-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Se busca una ayuda para el estudiantado sin desfinanciar a la universidad y que está sigaa siendo púbica, las Alcaldías tienen un presupuesto de libre inversión, sabemos que aunque este puede disminuir puede ser posible que destine este dinero para las sedes", agrega Cáceres. Con los recursos se podría otorgar educación a los cerca de **562 en Socorro sin contar los estudiantes de nivel introductorio, 344 estudiantes de Barbosa, cerca de 410 de la sede de Barrancabermeja y 521 en Málaga.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
