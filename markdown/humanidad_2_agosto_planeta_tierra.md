Title: La humanidad entró en deuda con el planeta tierra desde el 2 de agosto
Date: 2017-08-02 18:54
Category: Ambiente, El mundo, Nacional
Tags: cambio climatico, contaminación
Slug: humanidad_2_agosto_planeta_tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/planeta-tierra-e1492875332583.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ChristianChan/iStock 

###### [2 Ago 2017] 

Este 2 de agosto la humanidad consumió todos los recursos naturales que le debían haber alcanzado para todo el año, es decir que las personas empiezan a vivir en deuda con el planeta, y por tanto, este miércoles ha sido declarado como el **Día de la sobrecapacidad de la Tierra.** Así lo establece Global Footprint Network, una organización que cada año mide los recursos naturales que  podrían consumir anualmente lo seres humanos.

Este año, como viene siendo costumbre, las personas han gastado con bastante antelación los recursos de los que disponía anualmente. ¿La razón? Global Footprint Network y la WWF señalan que **la humanidad está consumiendo de tal manera que necesita 1,7 planetas, aunque solo exista uno.**

De acuerdo con dicha organización, las emisiones de gases de efecto invernadero constituyen el 60% de la huella ecológica de la humanidad, y como cada día las personas están usando más carros, cada vez aumenta más la población, y se consumen más cárnicos, la fecha de sobrecapacidad de la tierra es cada vez más prematura.

### ¿Cómo se calcula? 

Se combina la biocapacidad de la tierra para regenerar sus recursos naturales, con la huella ecológica anual. De esta manera es como se logra saber qué tanto se podía gastar, y cuál es el déficit. Producto de esa operación se ha logrado establecer que **para 1971 el presupuesto ecológico de la tierra se gastó el 21 de diciembre, para 2001 esa fecha ya se había cumplido el 23 de septiembre, y en este 2017 ese día se ha desplazado casi dos meses.**

“Tristemente el día del exceso terrestre se anuncia cada vez más cerca de los primeros meses del año”, manifiesta Luis Germán Naranjo, director de conservación de la WWF, quien invita a “intentar ser más racional con los alimentos, la energía, el agua y los desechos que consumimos”.

Incluso, cabe resaltar que un reciente estudio de la Universidad de Columbia Británica, publicado en la revista Environmental Research Letters explica que si se quiere salvar el planeta lo realmente útil sería "rebajar de forma sustancial la huella de carbono de cada individuo”, y por consiguiente sería necesario **"comer una dieta basada en vegetales (ser vegetariano), evitar los vuelos en avión, vivir sin carro y tener familias más pequeñas"**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
