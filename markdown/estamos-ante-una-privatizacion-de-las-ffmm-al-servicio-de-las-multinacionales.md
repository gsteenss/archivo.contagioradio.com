Title: Fuerzas militares colombianas al servicio de las empresas extractivistas
Date: 2015-11-04 17:55
Category: DDHH, Nacional
Tags: Congreso de la República, Contraloría, Drummond, Ecopetrol, Ejecuciones Extrajudiciales, El Cerrejón, El Consorcio Minero Unido – Drummond, Endesa, Fiscalía, Frondino Gold Mines, Hidroituango, Iván Cepeda, Mineros S.A, ministerio de defensa, Pacific rubiales, paramilitarismo en Colombia, Polo Democrático Alternativo, posconflicto, proceso de paz, procuraduria, proyectos minero-energéticos en Colombia, Vetra
Slug: estamos-ante-una-privatizacion-de-las-ffmm-al-servicio-de-las-multinacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [connuestraamerica.blogspot.com]

###### [3 Nov 2015]

Impulsado por el senador del **Polo Democrático Alternativo, Iván Cepeda**  se llevó a cabo el debate de control político sobre convenios entre empresas del sector minero-energético y fuerza pública, encaminado a responder la pregunta, **¿Cuál es el papel de la fuerza pública en el posconflicto armado?**

**“Estamos ante una privatización de las fuerzas militares al servicio de las multinacionales**”, asegura el senador Cepeda, quien añade “las fuerzas militares terminan siendo una especie  de servicio de seguridad de las empresas para resolver conflictos socio-ambientales o para reprimir a los sindicalistas (…) **si va a haber un acuerdo de paz esos convenios no tienen razón de existir, a no ser de que sea solo para reprimir a las comunidades”, dice.**

En las últimas dos décadas en Colombia se ha vivido una profundización del modelo extractivista que ha generado la privatización de la fuerza pública, violación de los DDHH y pérdida de soberanía nacional.

De acuerdo con la investigación  realizada por el  equipo del senador del Polo Democrático, se han creado 20 Batallones Especiales Energéticos y Viales (BAEEV), que deben proteger infraestructura energética, minera, vial, e hidrocarburos. Así mismo, se ha implementado 9 Centros de Operaciones Especiales para la Protección de la Infraestructura Crítica y Económica del Estado (COPEI) y además, existen al menos 1.229 convenios de cooperación entre las FFMM y diversas multinacionales por un **valor de 2,57 billones de pesos**. Según la respuesta de Ministerio de Defensa,  se habla de la destinación de más de **68.255 personas que tienen como tarea cuidar al sector minero energético, de infraestructura y vial.**

El Consorcio Minero Unido – Drummond, Mineros S.A, Frondino Gold Mines, Endesa, Pacific Rubiales y El Cerrejón son las seis empresas que tienen su propio batallón financiados con impuestos pagados por los colombianos y colombianas.

**Hidroituango:**

Con la construcción de la hidroeléctrica Hidroituango por parte de Empresas Públicas de Mededellín, se ha afectado a cientos de campesinos, barequeros y pescaderos, muchas de esas familias víctimas del conflicto armado y que han sido despojados de sus tierras por cuenta de la realización de este megaproyecto, que cuenta con dos  convenio con la Policía Nacional  por un valor de 2.839.000.0000 de pesos. Además, ha pactado 6 convenios por 35.361.927.005 con el Ejército Nacional.

**Putumayo:**

En esta región del país, el 100% de la Zona de Reserva Campesina [e]stá concesionada (en etapa de exploración o producción) para hidrocarburos, a las empresas Ecopetrol, Vetra, Emerald, Gran Tierra, Amerisur, Consorcio Colombia Energy y Pacific, lo que ha implicado la realización de total de 40 convenios entre estas empresas y la fuerza pública, por un valor total de \$ 73.566.252.583,  afectando a la Zona de Reserva Campesina la Perla Amazónica y a los territorios del pueblo Nasa.

**Centro del Cesar:**

En esta zona del país hacen presencia las empresas Drummond, Colombian Natural Resourses y PRODECO,  que cuentan con un total de 23 convenios con las fuerzas del Estado por un valor total de 10,760.000.000 de pesos.

Cabe recodar que empresas como Drummond y Prodeco están siendo investigadas por tener vínculos con estructuras paramilitares. Según el estudio “el lado oscuro del carbón” basándo en testimonios de excomandantes paramilitares, contratistas y exempleados, la organización holandesa PAX, asegura que tanto Drummond como Prodeco financiaron a los paramilitares entre 1996 y 2006.

**Meta- Pacific Rubiales:**

Las empresa Ecopetrol cuenta con un total de 8 convenios por un valor de  12.677.000.000 de pesos y la multinacional  PACIFIC y METAPETROLEUM (subsidiaria de Pacific Rubiales) tiene 24 contratos por 79.142.000.000 pesos. Desde la fuerza pública se ha realizado seguimientos y amenazas en contra del líder social Héctor Sánchez, quien demandó a Pacific Rubiales por la contaminación de una fuente de agua en su predio.

**Casanare:**

Ecopetrol, BP, Emerald, Equion, Pacific, Petrobras, Petrominerales, entre otras, cuentan con un total de 40 convenios por un valor total de \$ 209.488.539.666. En esta región, se destacan casos sobre ejecuciones extrajudiciales. De acuerdo con información de la Fiscalía, actualmente se investigan un total de 146 casos, que ocurrieron entre 1999 y 2007, en los cuales los presuntos responsables fueron miembros de la Brigada 16 del Ejército Nacional cuyos militares protegían las actividades de las empresas ya nombradas.

\_\_\_

Las cifras anteriores fueron solicitadas al Ministerio de Defensa por el equipo del senador Iván Cepeda, quien afirma que se gastaron un año para conseguir la información, y desde el Ministerio se violaron todos los términos con el fin de no dar la información exacta, lo que para el senador significa que “algo huele muy mal y no se ha querido mostrar”, teniendo en cuenta que en la investigación realizada en Meta, Putumayo, Casanare, Antioquia y Cesar, varias de las unidades de la fuerza pública han terminado involucradas en  ejecuciones extrajudiciales, tortura, violaciones  sexuales, hechos de represión y autoritarismo.

Frente a ese panorama, el congresista solicitó a la Contraloría la realización de un control directo a la ejecución de los convenios, y así mismo la investigación será remitida  a la Fiscalía y a la Procuraduría para que se adelanten las investigaciones pertinentes, especialmente aquellas por ejecuciones extrajudiciales.
