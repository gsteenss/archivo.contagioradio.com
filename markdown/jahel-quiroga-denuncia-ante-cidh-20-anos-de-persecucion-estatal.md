Title: Jahel Quiroga denuncia ante CIDH 20 años de persecución estatal
Date: 2016-04-05 16:54
Category: DDHH, Nacional
Tags: CIDH, Jahel Quiroga, UP
Slug: jahel-quiroga-denuncia-ante-cidh-20-anos-de-persecucion-estatal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Jahel-Quiroga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Arco Iris ] 

###### [5 Abril 2015 ]

Las **amenazas, hostigamientos, estigmatizaciones, investigaciones judiciales injustificadas, interceptaciones** y acopios ilegítimos de información de inteligencia, por parte de agentes del Estado colombiano contra la defensora de derechos humanos Jahel Quiroga Carrillo, fueron puestos en consideración ante la CIDH en este 157 periodo de sesiones.

De acuerdo con la defensora, la constante persecución, intimidación y estigmatización que ha tenido que enfrentar desde 1993 , la ha llevado a tener "una vida familiar clandestina", en la que **no ha podido estar cerca de sus hijos, ni salir del país, por el temor a ser detenida** por cuenta del señalamiento como vocera de las FARC, que pesa en su contra.

Según afirma la abogada defensora, estas violaciones a los derechos humanos responden a un **patrón sistemático del que el Estado colombiano es responsable por comisión** y por desatender la situación de vulneración y peligro en la que se encuentra Quiroga, en razón de su activismo social desde la década los ochenta, como integrante de la UP.

Las pruebas presentadas ante la CIDH dieron cuenta de las investigaciones que cursan en contra de la defensora por su supuesta vinculación con la guerrilla de las FARC, basada en **información interceptada por agentes del extinto DAS**.

Escuche la audiencia completa:

https://www.youtube.com/watch?v=-EhAD0VD31w  
 

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
