Title: Significado de la rebaja en las salvaguardias sociales y ambientales del Banco Mundial
Date: 2015-02-09 22:17
Author: CtgAdm
Category: Ambiente y Sociedad, Opinion
Tags: Banco Mundial, Consenso de Washington, fondo monetario
Slug: significado-de-la-rebaja-en-las-salvaguardias-sociales-y-ambientales-del-banco-mundial
Status: published

###### Por [Margarita Florez](https://archivo.contagioradio.com/margarita-florez/) - Directora de Ambiente y Sociedad 

El Banco Mundial – BM fue visto por los movimientos sociales como la entidad que promovió junto con el Fondo Monetario - FMI el ajuste estructural a través de las reformas del estado que comenzaron a ejecutarse en la década del 80, decretadas por el denominado Consenso de Washington. Para realizar dichas las reformas se acudió a otorgar mayores créditos, y compromisos que aumentaron la deuda (causa del Consenso), y el desmonte del naciente estado de derechos humanos sociales y culturales. Este tipo de créditos y los destinados a la inversión se otorgaron a los países sin ninguna consideración social ni ambiental.

Sólo cuando los mismos movimientos sociales y las ONG ambientalistas comenzaron a presionar al BM para incorporar estándares socio ambientales, esto se logró de manera parcial, y fue así como se adoptaron las Políticas Operacionales (OPs) para los llamados préstamos de inversión. La primera de diez políticas fue la de Evaluación Ambiental (1999), la de hábitats naturales (2001); Bosques (2002); Seguridad de Represas (2001); “Reasentamiento involuntario” de (2001), y la de Pueblos Indígenas (2005). Quedaron por fuera de su cobertura, los préstamos para ajuste estructural, préstamos de desarrollo de Políticas (DPL) es decir los que van al corazón del sistema económico y político.

A pesar de este alcance parcial, algunas ONG y organizaciones indígenas, consideraron a estas políticas operacionales como algún asidero para reclamar por los efectos sociales y ambientales de proyectos financiados por las entidades del Grupo del Banco Mundial. El incumplimiento de dichas políticas ha conducido al BM a ser cuestionado frente al Panel de Inspección que es el órgano que lo vigila.

Este sistema incompleto, pero lo único existente es lo que en los dos últimos años ha sido objeto de un desmonte con el pretexto de modernizarlas. Algunos de estos  cambios han sido catalogados como positivos en la medida que extienden su enunciado a sectores sociales como la población LBGTI; niñez; sectores vulnerables; género. Es decir igual que las normas nacionales incorporan sujetos y derechos pero sin fijarles el alcance, y como en el caso de los trabajadores su preocupación solo es respecto de los trabajadores en los proyectos, no la falta de garantáis para ejecutar el trabajo o para tener derecho al trabajo.

La lógica es trasladar mayores funciones y obligaciones a los estados pero sin fijarles alcance y profundidad. Las nuevas salvaguardias sacan muchos aspectos fuera de su comprensión. Por ejemplo ahora se menciona que la Evaluación Social y Ambiental se realice dentro de un “plazo razonable”, no un plazo determinado como está ahora, y hará parte de un documento que no se expresa si será público, entre el BM y el estado prestatario.

Menciona el nuevo documento de salvaguardias, nociones progresistas como el consentimiento, libre, previo e informado aspecto con el cual todo el mundo está de acuerdo pero deja su acatamiento, y ejercicio sometido a la voluntad del estado. Y esto básicamente es el problema: ya no serán las salvaguardias requisitos previos a los proyectos, ni obligatorios para los prestatarios sino que es el estado nacional el que determina, si en últimas se cumplen o no. Esta situación soberana sería plausible si operáramos dentro de unas democracias con suficiente representatividad; si no se nos siguieran planteando reformas más profundas para que el estado sea más funcional a las inversiones, mejor dicho si fuéramos ciudadano (a) de estados de derechos con amplias protecciones, y para el medio ambiente.

Las organizaciones sociales tendrán en octubre del 2014 en Lima frente a la próxima Asamblea de Gobernadores del Banco Mundial, una oportunidad para exigir mayor protección para sus intereses.
