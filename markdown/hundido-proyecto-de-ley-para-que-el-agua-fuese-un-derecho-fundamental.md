Title: Hundido proyecto de Ley para que el agua fuese un derecho fundamental
Date: 2016-12-07 15:31
Category: Ambiente, Otra Mirada
Tags: #SomosAguayVida, Alianza Verde
Slug: hundido-proyecto-de-ley-para-que-el-agua-fuese-un-derecho-fundamental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Agua-e1458146372649.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Taringa] 

###### [7 Dic 2016] 

Se hundió proyecto de ley que pretendía que el agua se considerará como derecho Fundamental en la Constitución. La iniciativa que pasaría a séptimo debate y que contaba con aprobación total, hasta ese momento, **no fue tramitada a tiempo y por lo tanto no pasó por el debate en el Congreso. **

El proyecto buscaba que el gobierno protegiera las fuentes naturales de agua, es decir ríos, lagunas e incluso aguas subterráneas, de la contaminación de la que actualmente son víctimas. Esta iniciativa ya había sido aprobada sin ningún voto en contra en 6 debates previos y solo le faltaban dos para ser vinculada como derecho fundamental. Le puede interesar: ["Congresistas buscan declarar el agua como derecho fundamental"](https://archivo.contagioradio.com/proyecto-de-ley-busca-declarar-el-agua-como-un-derecho-fundamental/)

Sin embargo, de acuerdo con el senador por la Alianza Verde y ponente del proyecto, Jorge Prieto, el trámite para que se realizará el debate estaba a cargo del presidente de la comisión primera de la Cámara de Representantes, Telésforo Pedraza,  perteneciente al partido Conservador, quien de acuerdo con el senador, **no habría pasado el tramite imposibilitando que se diera el debate y hundiendo el proyecto.**

“Pese a que le insistimos por todos los medios el Representante no quiso pasar el proyecto por ningún motivo, **yo entiendo que está siguiendo instrucciones del gobierno nacional, porque desde un principio no estuvo de acuerdo con este proyecto”** afirmó Prieto.

No obstante otros senadores y representantes que apoyaban esta iniciativa anunciaron que **volverá a ser presentada en el próximo periodo de debates en el Congreso**, e intentaran movilizar a la ciudadanía en torno a la defensa del agua. Este proyecto llevaba dos años elaborándose  y obteniendo respaldo de diferentes organizaciones ambientales y sociales. Le puede interesar: ["Gobierno es indiferente ante desviación de Arroyo Bruno"](https://archivo.contagioradio.com/gobierno-indiferente-arroyo-bruno/)

De igual forma Prieto aseguro que no existirá una paz completa si no se garantizan mecanismos jurídicos para que las comunidades tengan derecho al agua “Debe respetarse el medio ambiente y evitar que se sequen los yacimientos de agua, **proteger el agua es proteger la vida**, si este recurso escasea los campesinos van a proteger con su vida si es posible el recurso que les ha permitido existir”.

<iframe id="audio_14719707" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_14719707_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
