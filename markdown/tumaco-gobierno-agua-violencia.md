Title: Tumaco: sin gobierno, sin agua y con mucha violencia
Date: 2019-04-17 14:35
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: coca, Lider social, nariño, Tumaco
Slug: tumaco-gobierno-agua-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/chart.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/get_img.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/ejecuciones-extrajudiciales-_page-0001-670x1024.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Tumaco_from_Air.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia Commons] 

Este miércoles los habitantes de Tumaco, Nariño, completaron 25 días sin suministro de agua, 3 años sin tener un alcalde elegido popularmente y décadas reclamando al Gobierno Nacional que pose su atención sobre el Puerto. Según Leder Rodríguez, líder y habitante del Puerto, el Municipio está en una situación de ingobernabilidad porque no hay autoridad que se haga cargo de los problemas del territorio.

De acuerdo al testimonio de Rodríguez, solo hasta hace media hora habría llegado a ciertos sectores de Tumaco el suministro de agua, un servicio que hace más de 4 años presenta deficiencias por cuenta de fallas; que según la empresa Aguas de Tumaco, se debe a fallas en la planta que redistribuye el líquido por el sistema de tuberías del Municipio. Sin embargo, como lo recuerda el líder, la situación se presenta hace años y desde el "Tumacazo" de 1988 (protesta ciudadana que pedía mejorar la calidad de los servicios) nada ha cambiado.

Por otra parte, la infraestructura que provee el servicio de alumbrado al Municipio es frágil, al punto que incluso tormentas de lluvia pueden afectar el funcionamiento del sistema eléctrico. A las dos situaciones se suma la corrupción de los dirigentes locales, que terminan acusados por el uso de los pocos recursos que se destinan a la región para solventar estos problemas. (Le puede interesar: ["Comunidades del pacífico preparadas para revelar la verdad oculta tras el conflicto"](https://archivo.contagioradio.com/comunidades-de-tumaco-preparadas-para-revelar-la-verdad-oculta-tras-el-conflicto-2/))

### **Sin Alcalde elegido, no hay quien tome determinaciones sobre Tumaco** 

Recientemente fue capturado por corrupción Víctor Gallo, quien fue alcalde en el periodo de 2011 a 2016 y hasta diciembre de 2018 ejerció como gerente de Aguas de Tumaco. Durante su alcaldía, Gallo firmó un contrato para la construcción de más de 300 viviendas que repararían a las víctimas de la avalancha del Río Mira ocurrida en 2009; no obstante según datos de Planeación Nacional y la Secretaría de Planeación de Tumaco, un 68% de los más de 7 mil millones de pesos destinados para la obra están perdidos.

El encargado de seleccionar los predios para la construcción de las viviendas fue el excongresista y exalcalde de Tumaco Neftalí Correa, quien fue destituido de su cargo como Representante a la Cámara, porque mientras se desempañaba como máxima autoridad del Municipio incurrió en irregularidades al contratar el servicio de internet para 36 instituciones educativas.

Despúes de la salida de Gallo de la Alcaldía vino la elección de María Emilsen Angulo, quien ejercería como autoridad civil del Municipio entre 2016 y 2019, pero su designación fue anulada por el Consejo de Estado al presentarse una inhabilidad que la impedía para presentarse a elecciones. El impedimento consiste en la existencia de una unión marital entre Angulo y Jairo Guagua Castillo, quien ejercía como subgerente del Hospital San Andrés de Tumaco.

En abril de 2007, luego de unas elecciones atípicas, salió electo Julio César Rivera como reemplazo de Angulo, pero en diciembre de 2018 fue capturado por los cargos de peculado, contrato sin cumplir requisitos legales y falsedad en documento público. Los delitos se habrían cometido durante la alcaldía de Gallo, mientras Rivera era secretario de planeación, y participó en la construcción de las 327 casas para los damnificados por la avalancha de 2009.

En la actualidad, Jhon Jairo Preciado ejerce como alcalde encargado de Tumaco; y aunque las cifras oficiales de la Policía destacan un descenso de la violencia en la zona, algunas personas señalan que se debe a un pacto de no agresión que se acordó entre las bandas que operan en el territorio. (Le puede interesar:["El puerto del Pacífico con mayor número de líderes asesinados desde 2016"](https://archivo.contagioradio.com/cauca-narino-2/))

> Tumaco: al parecer los grupos armados en la ciudad han llegado a un acuerdo para reducir los enfrentamientos. Lo hacen para poder sacar coca con mayor facilidad. Me alegra que los tumaqueños tengan un respiro pero un pacto entre bandidos no es la paz sustentable que Tumaco merece [pic.twitter.com/aOrtrTKv2n](https://t.co/aOrtrTKv2n)
>
> — José Miguel Vivanco (@JMVivancoHRW) [9 de enero de 2019](https://twitter.com/JMVivancoHRW/status/1083018328644505602?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Coca, grupos armados y pobreza son el pan de cada día en Tumaco** 

Según las cifras más recientes del Observatorio de Drogas de Colombia, en el departamento de Nariño se registraron 45.734 hectáreas cultivadas con Coca en 2017, un 27% del total reconocidas a nivel nacional. De esas hectáreas, Tumaco concentra 19.516, siendo el Municipio con mayor presencia de estos cultivos en el país. (Le puede interesar: ["¡A la fuerza!, así sera la erradicación de cultivos de Coca en Colombia"](https://archivo.contagioradio.com/fuerza-sera-erradicacion-coca/))

\[caption id="attachment\_65065" align="aligncenter" width="606"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/chart-300x200.png){.wp-image-65065 width="606" height="404"} Nariño concentra el 27% de hectáreas sembradas con cultivos de uso ilícito\[/caption\]

Adicionalmente, según datos de la Defensoría del Pueblo, Nariño y Cauca son los departamentos más peligrosos para desarrollar labores en torno a la defensa de los derechos humanos, pues presentan la mayor cantidad de asesinatos de líderes sociales. La mayoría de estos crímenes, contra personas que impulsaban el Programa Nacional Integral de Sustitución (PNIS) y la restitución de tierras. (Le puede interesar: ["Asesinan a Argemiro López, líder social y promotor de sustitución en Nariño"](https://archivo.contagioradio.com/asesinan-a-argemiro-lopez-lider-social-y-promotor-de-sustitucion-de-cultivos-en-tumaco/))

\[caption id="attachment\_65071" align="aligncenter" width="523"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/get_img-208x300.jpg){.wp-image-65071 width="523" height="754"} Cauca y Nariño registran la amyor cantidad de líderes asesinados en el país\[/caption\]

En el Municipio hacen presencia además de las Fuerzas Armadas, las Guerrillas Unidas del Pacífico, la Gente del Orden, el Frente Estiven Prado, el Frente Oliver Sinisterra y el ELN, entre otros. La confrontación de algunos de estos actores causó en días recientes un desplazamiento masivo de 729 personas, pertenecientes a 3 comunidades, de acuerdo con los datos registrados por la Oficina de Naciones Unidas para la Coordinación de Asuntos Humanitarios, con sede en Colombia.

> \[EHP\] Flash Update N°1 - Desplazamientos masivos en Tumaco (Nariño) - <https://t.co/ErWKxf8CGm> [\#ConLosDesplazados](https://twitter.com/hashtag/ConLosDesplazados?src=hash&ref_src=twsrc%5Etfw) [\#PR20](https://twitter.com/hashtag/PR20?src=hash&ref_src=twsrc%5Etfw) [@GerardGomez59](https://twitter.com/GerardGomez59?ref_src=twsrc%5Etfw) [pic.twitter.com/91xKUDyDde](https://t.co/91xKUDyDde)
>
> — Ocha Colombia (@ochacolombia) [12 de abril de 2019](https://twitter.com/ochacolombia/status/1116824673084424193?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
A ello se suman la denuncia de la Asociación de Juntas de Acción Comunal de los ríos Mira, Nulpe y Mataje del pasado 26 de marzo, sobre presuntas ejecuciones extrajudiciales, en la que afirmaron que dos campesinos integrantes del Consejo Comunitario de Alto Mira y Frontera fueron arrestados por hombres del Ejército sin razón, y sacados del territorio en helicópteros hacía un lugar aún desconocido. (Le puede interesar: ["En cementerio del Pacífico  podrían haber más de 150 personas sin identificar"](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/))

</p>
\[caption id="attachment\_65080" align="aligncenter" width="710"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/ejecuciones-extrajudiciales-_page-0001-670x1024-196x300.jpg){.wp-image-65080 width="710" height="1087"} Dos campesinos son sacados del territorio por el Ejército\[/caption\]

Por estas razones, los habitantes del Puerto emprendieron acciones de protesta el lunes y martes, cerrando la vía Panamericana y esperando la llegada a la zona del Presidente, para que, como lo señala Leder Rodríguez, tome cartas sobre el asunto y defina una autoridad para Tumaco. Decisión que debería darse pronto, en lugar de esperar a tener una situación como la vivida en 2017, cuando una masacre llamó la atención del Gobierno central sobre el puerto.

<iframe id="audio_34606411" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34606411_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
