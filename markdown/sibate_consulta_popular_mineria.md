Title: Sibaté prepara consulta popular contra la minería
Date: 2017-07-05 19:44
Category: Ambiente, Nacional
Tags: consulta popular, Sibate
Slug: sibate_consulta_popular_mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: revistaccesos 

###### [5 Jul 2017] 

En el municipio de Sibaté, la comunidad trabaja para realizar una consulta popular con el objetivo de frenar la minería que se realiza desde hace más de 30 años. **Desde agosto empezará la recolección de firmas por parte de la comité promotor de la consulta.**

De acuerdo con Camilo Rojas, integrante de la comunidad de Sibaté, desde la instalación de la Empresa Eternit se han presentado problemas ambientales en la zona, además de los las situaciones ya conocidas por el asbesto. Sin embargo, la megaminería a cielo abierto para la explotación de arena ha sido el mayor problema tanto ambiental como para la salud de la comunidad.

Rojas señala que la empresa **minera Comind S.A., filial de la italiana Minerali Industriali, tiene una licencia minera por más de 30 años.** Y aunque apenas llevan 8, los 35.000 habitantes de Sibaté ya sufren graves afectaciones por esa actividad, entre ellas, la contaminación de las fuentes hídricas y los daños a la salud.

### Consecuencias 

Desde el 2015 las comunidades afectadas por por los daños ambientales, han empezado a movilizarse en contra de la minería tras empezar a sentir los efectos de dicha actividad. Según denuncian, los títulos mineros de la empresa comprenden la vereda El Peñón y por tanto una zona de subpáramo, que además de tener un gran valor ambiental, provee de agua al **municipio  de Sibaté, pero además a Fusagasugá y un sector de Silvania.**

La preocupación de los  pobladores radica en que la concesión de explotación minera se ubica en la misma montaña donde nace el agua que surte los acueductos de los municipios. Además esa zona de la cordillera oriental también hace parte de la cuenca del páramo de Sumapaz, el más grande del mundo.

Los pobladores señalan que los químicos que se usan para extraer la arena están contaminando la fuentes de agua del municipio, entre ellas **la quebrada Barro Blanco**. Asimismo aseguran que algunas personas que viven sobre la vía por la que pasan las volquetas de la empresa sufren enfermedades pulmonares por el polvo que produce el paso de esos vehículos.

### Riesgo eminente 

La movilización de las comunidades logró que la CAR obligara a la empresa a actualizar su plan de manejo ambiental. No obstante, la multinacional no ha cumplido, y sin embargo las entidades ambientales continúan permitiendo la actividad minera pese a los daños ambientales.

Camilo Rojas, además denuncia que se busca dar más licencias de explotación, ya que descubrieron un yacimiento de arena. El líder de la comunidad agrega que **en esa zona existen otros 12 títulos mineros en proceso de explotación y hay 22 solicitudes más en curso. **

<iframe id="audio_19648530" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19648530_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
