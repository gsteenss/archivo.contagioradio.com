Title: Los últimos días de Rousseff como presidenta de Brasil
Date: 2016-08-26 17:41
Category: El mundo, Otra Mirada
Tags: Brasil, Dilma Rousseff, Impeachment
Slug: los-ultimos-dias-de-rousseff-como-presidenta-de-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/2135770.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Nación 

###### 28 Agos 2016

Luego de un primer día de fuertes debates, denuncias y protestas sociales, este viernes continua la etapa final dentro del proceso de impeachment que cursa en contra de la mandataria **Dilma Rousseff**, separada de su cargo desde el pasado 12 de mayo por el delito de **responsabilidad fiscal**.

En la Cámara de Senadores presidida por **Ricardo Lewandowski**, inició la sesión en la que representantes del Partido de los Trabajadores PT, elegido democraticamente para ocupar la presidencia, presentaron denuncias por lo que consideran ha sido u**n proceso plagado de irregularidades**, exigiendo su anulación inmediata.

Las reclamaciones del PT, se relacionan con el tiempo período de tiempo que se pretende abaracr, bajo el plazo establecido en la Constitución de 180 días para adelantar el impeachment; para el caso Rousseff ha sido llevada a juicio en poco más de 90 días de iniciado el proceso y con la calidad moral que tienen los senadores para votar contra la mandataria, cuando 24 de los 81 son investigados por delitos de corrupción. Argumentos que fueron respondidos por los partidarios del interino Michel Temer con insultos y ataques personales.

En el cierre de la sesión del **jueves 25**, fue presentada la declaración de los testigos de la parte acusadora: Júlio de Oliveira, fiscal y el auditor Antonio Carlos Costa D'Ávila Carvalho Júnior, funcionarios del Tribunal de Cuentas de la Unión, quienes respondieron a las preguntas vinculadas a los asuntos fiscales del gobierno Rousseff.

La tarde noche de este **viernes 26 **se presentarán a rendir declaración testigosde la defensa entre los que se encuentran el exministro de Economía Nelson Barbosa, el profesor de Derecho de la Universidad Estadual de Río de Janeiro Ricardo Lodi Ribeiro y el economista y exprofesor y asesor económico de Rousseff, Luiz Gonzaga Belluzo, también la exsecretaria del presupuesto Esther Dweck, el profesor de Derecho Geraldo Prado y el exviceministro de Educación Luiz Costa. La lista de testigos deberá responder un total de más de cien preguntas, por lo cuál el segundo día podría extender su sesión hasta el día **sábado 27** e incluso hasta el **domingo 28**.

El próximo **lunes 29**, la presidenta en persona presentará su defensa desde las 9 de la mañana hora local, con un tiempo de 30 minutos para presentar sus argumentos, tiempo que puede ser extendido si se considera necesario por parte de Lewandoski, quién al terminar la intervención de Rousseff, procederá a interrogarla junto con los senadores y abogados de la parte acusatoria.

Al terminar la sesión de preguntas, ambas partes tendrán una hora y media para discutir, acto seguido deberá iniciar el debate entre los senadores, disponiendo cada uno de 10 minutos para expresar su posición, etapa pueda durar alrededor de 20 horas. Finalizado esto cada abogado de ambas partes dispondrán de hora y media para dirigirse al plenario antes de la votación.

[Para la **decisión definitiva** los 81 senadores decidirán a través del voto electrónico y la presidenta es culpable o inocente, de esta manera: s]i dos tercios (54 senadores) vota a favor de condenarla, Dilma Rousseff pierde la presidencia y queda inhabilitada para ejercer cualquier cargo público durante 8 años, si los votos no llegan a esa cifra, el proceso sería archivado, Rousseff quedaría absuelta y volvería a su cargo como presidenta de Brasil.
