Title: Dura crítica a RCN y Caracol por no cubrir agresión a paro de maestros
Date: 2017-06-10 14:20
Category: DDHH, Nacional
Tags: CARACOL, Docentes, maestros, Marchas, protestas pacíficas, RCN, Venezuela
Slug: 42081-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/RCN-CARACOL-e1443363760425.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Jun. 2017] 

**“Triste y decepcionante el cubrimiento de RCN y Caracol a las marchas pacíficas de los educadores** en la Capital de la Republica en la tarde de este viernes” así lo aseguraron en una dura carta dada a conocer este sábado.

Comunicación en la que también aseguran, rechazan el silencio de los y las colombianas quienes en sus redes sociales condenan las situaciones que acontecen en Venezuela, pero por el contrario **guardan silencio por el accionar desmedido del ESMAD contras las protestas pacíficas de los docentes. **Le puede interesar: [Tres profesores asesinados en desarollo del Paro, Johana Alarcón la última](https://archivo.contagioradio.com/42004/)

Además, agregaron que es lamentable como se refieren a las situaciones acaecidas en Venezuela como “represión” de “dictadura” mientras que **para RCN y Caracol lo que sucedió con los docentes este viernes en el que las tanquetas del ESMAD** los violentaron con chorros no significó lo mismo. Le puede interesar: [ESMAD ataca el paro de maestros en Bogotá](https://archivo.contagioradio.com/esmad-ataca-con-agua-y-gases-lacrimogenos-el-paro-de-maestros-en-bogota/)

Agradecen en su misiva la difusión con la que contaron por parte de medios alternativos como NC Noticias a quién definen como el medio de comunicación de las FARC y a las redes sociales por divulgar la situación sufrida por **los profesores que “iban completamente desarmados, no portaban ni lanzaron piedras, bombas molotov,** ni metralla a través de morteros artesanales. No rociaron gasolina ni incendiaron a nadie, pese a lo cual no fueron llamados "resistencia heroica".

**“Se han caído sus máscaras y gracias a ellos mismos entendemos que su "información" es sesgada** y que actúan más que como medios de información, como instrumentos poderosos de propaganda para atacar”, concluyen. Le puede interesar: [Estas serán las próximas actividades del paro de profesores](https://archivo.contagioradio.com/estas-seran-las-proximas-actividades-del-paro-de-profesores/)

Compartimos con ustedes la carta completa

[Carta en la que se critica a RCN y CARACOL por no cubrir agresión a paro de maestros](https://www.scribd.com/document/350916155/Carta-en-la-que-se-critica-a-RCN-y-CARACOL-por-no-cubrir-agresion-a-paro-de-maestros#from_embed "View Carta en la que se critica a RCN y CARACOL por no cubrir agresión a paro de maestros on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_74546" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350916155/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-p8RVdoDh6KlaeQ1Zusj7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
