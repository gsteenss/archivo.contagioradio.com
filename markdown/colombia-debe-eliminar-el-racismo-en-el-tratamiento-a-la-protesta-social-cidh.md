Title: Colombia debe eliminar el racismo en el tratamiento a la protesta social: CIDH
Date: 2017-06-13 17:30
Category: Nacional
Tags: comunidades afro, ESMAD
Slug: colombia-debe-eliminar-el-racismo-en-el-tratamiento-a-la-protesta-social-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [12 Jun 2017] 

A través de un comunicado la Comisión Interamericana de Derechos Humanos, CIDH, expresó su preocupación por la información en torno a la actuación de la fuerza pública, y el Escuadrón Móvil Antidisturbios, ESMAD, durante el paro cívico de Buenaventura y **llamó a Colombia a que se neutralicen los estereotipos raciales y se atiendan las necesidades de las comunidades afrodescendientes**.

La CIDH manifestó que el 2 de Junio recibieron denuncias que ponen de manifiesto el uso excesivo de la fuerza que incluyó, por ejemplo, el uso de tanques entre civiles y el uso desmedido de gases lacrimógenos muy cercanos a viviendas con niños y mujeres embarazadas “**se utilizaron gases lacrimógenos contra manifestantes pacíficos, incluidos niños, personas mayores y personas con discapacidad**.” Le puede interesar: ["ESMAD impide labores de defensores de DD.HH y periodistas en Buenaventura"](https://archivo.contagioradio.com/41556/)

En el comunicado la CIDH expresa que la protesta de las comunidades se desarrolló en exigencia de derechos básicos “**Las movilizaciones fueron en reclamo de acceso a fuentes laborales, vivienda, agua potable, educación y salud, entre otros derechos económicos, sociales y culturales**” por lo que se entendería que con la actuación del ESMAD no se respetó el derecho a la protesta.

En ese sentido la CIDH recordó que todos los **Estados “deben adoptar medidas a fin de garantizar que todos los sectores de la sociedad puedan ejercer su derecho a participar** pacíficamente en protestas, sin temor a sufrir actos de violencia”, recordando, de esta manera, que el Estado colombiano firmó la Carta Interamericana y está obligado por sus leyes a cumplirla.

### **Eliminar el racismo al atender las exigencias de las comunidades negras** 

En ese marco la Comisión Interamericana reafirmó que **“La conducta de las fuerzas de seguridad debe respetar en todo momento las normas internacionales** de derechos humanos y cumplir con los principios de legalidad, necesidad y proporcionalidad”. Le puede interesar: ["Hasta los niños han sido victimas del ESMAD en Buenaventura"](https://archivo.contagioradio.com/se-recrudece-la-represion-a-paro-civico-de-buenaventura/)

Por otra parte, la CIDH instó al Estado colombiano a destinar fondos, recursos humanos y de todo tipo para que se eliminen los estereotipos raciales tanto en el Estado mismo como en la atención a las exigencias de las comunidades afrodescendientes "**mejorar las condiciones de vida de las personas de ascendencia** africana con respecto a la atención médica, la vivienda, la educación y el trabajo."

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
