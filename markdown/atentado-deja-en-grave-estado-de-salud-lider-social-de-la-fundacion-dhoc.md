Title: Así fue el atentado contra integrante de la Fundación DHOC
Date: 2017-02-21 12:28
Category: DDHH, Entrevistas
Tags: Arauca, Atentado, Fundación DHOC
Slug: atentado-deja-en-grave-estado-de-salud-lider-social-de-la-fundacion-dhoc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/DHOC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: REMA] 

###### [21 Feb. 2017] 

En la madrugada de este martes **Francisco Javier Gómez**, abogado, defensor de derechos humanos e integrante de la Fundación DHOC en Arauca, fue **víctima de un atentado con arma blanca por parte de dos hombres que ingresaron a su casa, y atentaron contra su humanidad mientras dormía.** Francisco se encuentra en grave estado de salud en el Hospital San Vicente de esa ciudad.

Según Juan Torres, vocero de Marcha Patriótica en Arauca y quien reside en la misma casa con Francisco, **no es muy claro hasta el momento cómo pudieron ingresar los dos sujetos a la casa**, pero relata que por ser a altas horas de la madrugada todo estaba en silencio y ambos se encontraban durmiendo.

“A mí me despiertan unos gritos del compañero Francisco, me despierto como aturdido, cuando abro la puerta de mi cuarto **veo que dos sujetos salen del cuarto de él**, uno pequeño y otro flaco, alto. **Salen corriendo, bajan las escaleras y detrás sale Francisco pidiéndome auxilio, gritándome ‘Juan me mataron, no me deje morir’**” relato Torres. Le puede interesar: [117 líderes fueron asesinados durante 2016](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

Después de este difícil momento, Torres asegura que llevó a Francisco al Hospital San Vicente de Arauca **“cuando ingresó a la sala de cirugía tuvo un infarto”**. Hasta el momento se conoce que el defensor de derechos humanos tiene afectados varios órganos de su cuerpo y se encuentra en cuidados intensivos.

Francisco Gómez y Juan Torres han sido objeto de seguimientos por parte de sujetos que no han sido identificados y pese a que **ambos líderes interpusieron las respectivas denuncias no han recibido la atención que se requiere.**

“Hemos puesto la denuncia a la Fiscalía, solicitamos a la Unidad Nacional de Protección medidas, pero **lo que nos contestaron hace 10 días es que los hechos no revestían gravedad y que por lo tanto lo que iban a hacer es informarle al cuadrante de la Policía**” relató Torres. Le puede interesar: [Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

Para Torres, sería importante poder contar con medidas de protección a la sede donde está ubicada la Fundación DHOC, ubicada a dos cuadras de donde sucedieron los hechos contra la vida de Francisco. Así mismo, **aseguran que requieren esquema de protección.**

En la actualidad se espera que comiencen las respectivas investigaciones para dar con los responsables, pero Torres recalcó que no permitirán que este hecho se catalogue como un robo, puesto que “en el primer piso de la casa hay objetos de valor, no se llevaron nada y en el cuarto del compañero Francisco no esculcaron nada, todo está en orden”. Le puede interesar: [“Colombia no necesita más víctimas; Necesita líderes comunitarios” Demócratas de EEUU](https://archivo.contagioradio.com/colombia-no-necesita-mas-victimas-necesita-lideres-comunitarios-democratas-de-eeuu/)

**¿Quién es Francisco Gómez?**

Francisco **es un reconocido abogado defensor de los reclamantes de tierras del despojo que hizo la empresa Occidental de Colombia hace 21 años** a unos campesinos en el municipio de Arauquita. Le puede interesar: [Defensores de derechos humanos "Contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

Además de realizar la defensa jurídica, Francisco realiza acompañamiento constante a estas comunidades para estar al tanto de cualquier situación que acontezca.

Por esta labor, dice Torres, **se podrían estar afectando intereses de las compañías petroleras que se encuentran en la zona**, que cuentan con 16 bloques petroleros concesionados que están en proceso de exploración.

<iframe id="audio_17137277" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17137277_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
