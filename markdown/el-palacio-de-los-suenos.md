Title: El Palacio de los Sueños
Date: 2019-12-05 10:00
Author: Camilo de las Casas
Category: Opinion
Tags: Ciudadanos, ESMAD, Paro Nacional
Slug: el-palacio-de-los-suenos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/andres-zea-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto de: Andres Zea/Contagio Radio] 

#### **Un Estado policiaco se revela nuevamente ante nuestros ojos.** 

Las fracturas del establecimiento ante un acuerdo de paz, timidamente reformista en lo social ha permitido desnudar, lo que ya habíamos escrito antes, un orden de desigualdad protuberante, que se alimentó en medio del espejismo de unos enemigos internos, llamados terroristas.

El despelleje se inició luego del plebiscito, que mostró una ciudadanía nueva hastiada del horror de la guerra. Se volvió expresión electoral con Petro, más allá de él y de su autismo, y ha ido mostrando mayores alcances desde el pasado ***21 de noviembre***. Hoy se refleja el fruto de la dejación de armas de las **FARC** que debe articularse a lo rural.

Esa ciudadanía es más que el añorado hito del Paro Nacional del *14 de septiembre de 1977*, y también más que su contrarelato, el establecimiento que representa Duque.

##### **La expresión ciudadana se expresó más allá de las reivindicaciones de los propios sindicatos.** 

vea mas:[Especial PARO NACIONAL](https://archivo.contagioradio.com/paro-nacional/)

Los ciudadanos manifestantes son un sector popular organizado, un sector de sectores de clases medias, un nuevo sector empresarial, rompiendo el patriarcalismo y verticalismo. Ha sido una expresión transgeneracional, defraudada, insatisfecha, proyectiva. Mujeres, jóvenes, acompañados de padres y abuelos. Tradiciones en ruptura ante la posibilidad que a su heredad se le niegue un futuro distinto. *Razones por las cuales los cambios propuestos son posibles.*

Estos ciudanos están más allá de las migajas de pan para hoy. Están por realidades posibles que deshagan privilegios del sector bancario, el sector extractivista, entre otros. Están con el propósito que la vida y las libertades sean respetadas con la protección real de los líderes sociales. Se suman a que los sectores rurales sean respetados en sus derechos, y al desmonte del **ESMAD**, y de todas las formas de violencia política y criminalidad. En el alma del Paro se expresa el futuro ambiental, la sensibilidad frente la protección de la vida en toda su extensión seres humanos, aguas, bosques, animales.

Y del otro lado, la expresión gremial empresarial es la misma de siempre. Dialogar sin que se cambie la agenda económica. Hay que seguir la ruta, eso es lo que dicen sin sonroja alguna. Esto es en otras palabras, las metas definidas por organismos internacionales y ellos con sus intereses.

#### **En palabras de Duque: legalidad para la injusticia, para la desigual.** 

Entre tanto, los ciudadanos transformantes descalifican el vandalismo, incluso, las expresiones visuales en las pintas en vidrios y paredes, limpiando las paredes ellos mismos, y logran demostrar la infiltración y saboteo policial y de sectores políticos. Estos nuevos ciudadanos transformantes lejos están de odiar, expresan su repulsa, su indignación se acercan a desarmar al **ESMAD** sin la violencia y sin odio, y esto significa asumir la rabia de otra manera.

Insistimos, del otro lado de sectores del establecimiento usan el fantasma de un estado de excepción que se ha empezado a aplicar sutilmente. Los caminantes son atacados porque sí o porque no. Las cuentas de correo bloqueadas o interceptadas. Los militares a la calle. El raqueteo de celulares de niños jóvenes para ver sus contenidos. Y, claro, la guerra psicológica con toques de queda con los medios empresariales, con excepciones, descalificando a los ciudadanos, justificando la represión, evitando abrir espacios para que sean escuchados y facilitar el diálogo. La fiscalía en lo suyo, en lo heredado de *Néstor Humberto,* usando indebidamente las plataformas de interceptación, tal cual, como las diversas inteligencias de Estado han sido descubiertas recientemente por *Daniel Coronell* y *María Jimena Duzán.*

Así como en la obra de Ismail Kadaré en el País de los Sueños, todos los ciudadanos están siendo mirados en sus sueños en organismos de seguridad. El estado policiaco está alimentándose en medio de las formas democráticas de la legalidad.

**Es sin duda el juego del terror del Estado que quiere paraliza.**

le puede interesar: [Volviendo al pasado ](https://archivo.contagioradio.com/volviendo-al-pasado/)
