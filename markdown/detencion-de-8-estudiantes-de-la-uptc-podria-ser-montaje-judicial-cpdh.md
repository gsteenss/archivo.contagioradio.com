Title: Detención de 8 estudiantes de la UPTC podría ser montaje judicial: CPDH
Date: 2015-09-24 13:48
Category: Educación, Nacional
Tags: cpdh, Disturbios en UPTC 17 de septiembre, Estudiantes capturados en Tunja, Falsos Positivos Judiciales, montajes judiciales, Radio de derechos humanos, Tunja, UPTC
Slug: detencion-de-8-estudiantes-de-la-uptc-podria-ser-montaje-judicial-cpdh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/UPTC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: m.periodicoeldiario.com] 

###### [24 Sept 2015] 

[Este miércoles **8 estudiantes de la Universidad Pedagógica y Tecnológica de Tunja en Boyacá, fueron capturados por civiles acompañados de agentes policiales**, quienes les indicaron que los acusaban por fabricación, tráfico y porte de armas de uso privativo de las Fuerzas Armadas, fabricación de explosivos y concierto para delinquir.]

[Javier Rojas, director del Comité Permanente por los Derechos Humanos, CPDH, seccional Boyacá, afirma que las detenciones se dieron entre las 8 de la mañana y las 5 de la tarde, tras la aparición de **panfletos en los que se anunciaban capturas por los disturbios presentados en la UPTC el pasado 17 de septiembre** que provocó la suspensión de las clases.]

[De acuerdo con la información que se conoce hasta el momento estos jóvenes son capturados por seguimientos que les venían haciendo desde hace 2 años por su participación en distintas movilizaciones. Sin embargo, **teniendo en cuenta que los estudiantes no pasan de tercer semestre, se trata de montajes judiciales** en el marco de la represión a la protesta social, asegura Rojas.  ]

[Los abogados aún **no conocen las pruebas en las que se basan las órdenes de captura** emitidas por una juez de la Fiscalía Tercera Especializada, esperan conocerlas en la audiencia que se llevará a cabo este miércoles, con la preocupación de que sean los directivos de la universidad  quienes hayan denunciado a estos jóvenes, señala Rojas.]

[Mientras que las directivas de la universidad no se han pronunciado oficialmente, **estudiantes de diversas carreras adelantan movilizaciones y plantones como el de ayer al medio día que fue reprimido por unidades del ESMAD**, por lo que decidieron entrar en asamblea permanente. ]

[Rojas asegura que tienen información de posibles nuevas capturas y el conocimiento de que **estos hechos responden al mismo modus operandi de las detenciones de los 13 jóvenes ocurridas en Bogotá**, sin embargo, esperan las determinaciones de la audiencia de imputación de cargos. ]
