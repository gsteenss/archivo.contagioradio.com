Title: Disparos indiscriminados del Ejercito causan la muerte a niño campesino del Sumapaz
Date: 2015-02-27 21:41
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, ejercito, Sumapaz
Slug: disparos-indiscriminados-del-ejercito-causan-la-muerte-a-nino-campesino-del-sumapaz
Status: published

###### Foto: Radio Santafe 

 

La tarde del jueves 26 de febrero, la familia Gamba Rojas se encontraba rodeando su ganado en la vereda de Santa Rosa, Municipio de Nazareth, Sumapaz, cuando miembros del Ejercito de la Fuerza de Tarea del Sumapaz, de la Base de Santa Rosa, abrieron fuego de manera indiscriminada.

Flor Alba Rojas intentó resguardar a sus dos hijos, pero ante el pánico que le generaron las acciones del Ejercito, Edwin Steven de 12 años de edad, sufrió un paro cardiaco fulminante que acabó con su vida.

La Brigada Tercera del Ejercito emitió un comunicado el viernes 27 de febrero, en que afirma que los hechos son materia de investigación, y presentan "un saludo de condolencias a la familia del menor".

Mientras el alcalde local de sumapaz, William Santoyo, justificó las acciones del ejercito afirmando que "son acciones arbitrarias pero que corresponden al miedo que tiene el Ejercito de las acciones terroristas en la región", la Organización Comunal del Sumapaz rechazó los hechos, afirmando que este no es el único caso en que niños y jóvenes de la región sufren impactos de bala sin que se conozca castigo para los responsables, señalando que es el Ejercito colombiano quien "sigue sembrando terror en el territorio".

\[caption id="attachment\_5433" align="aligncenter" width="786"\][![Sumapaz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/Comunicado.jpg){.size-full .wp-image-5433 width="786" height="984"}](https://archivo.contagioradio.com/ddhh/disparos-indiscriminados-del-ejercito-causan-la-muerte-a-nino-campesino-del-sumapaz/attachment/comunicado/) Sumapaz\[/caption\]
