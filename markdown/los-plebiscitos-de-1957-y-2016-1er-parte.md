Title: Los plebiscitos de 1957 y 2016 ( 1er parte)
Date: 2016-09-04 06:00
Category: Cesar, Opinion
Tags: plebiscito por la paz, proceso de paz
Slug: los-plebiscitos-de-1957-y-2016-1er-parte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/plebiscito-1957.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La voz del pueblo 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

[Mucha agua ha corrido bajo los puentes desde 1957, año del plebiscito que dio origen a un nuevo modelo de control estatal excluyente, autoritario y elitista: el]*[Frente Nacional]*[. Corrían los tiempos del gobierno militar de Gustavo Rojas Pinilla, represivo como el que más; el “Jefe Supremo” quiso imponerse individualmente, y a las Fuerzas Armadas, sobre las elites políticas y económicas del liberalismo y el conservatismo quienes querían ejercer el poder directamente, sin el molesto intermediario. Luego de una intensa presión eclesial y gremial para su retirada, el paro de la ANDI y las otras agremiaciones tuvo éxito. Rojas Pinilla dejó como sucesor en el gobierno a una Junta Militar de cinco miembros y marchó al exilio.]

[La Junta, controlada completamente por la dirigencia liberal-conservadora, fue la encargada de convocar al plebiscito para el 1° de diciembre de 1957. El antecedente inmediato de éste fueron las reuniones políticas en España entre Laureano Gómez y Alberto Lleras Camargo en Benidorm (julio 1956) - que acordó el sistema de coalición bipartidista -, y Sitges (julio 1957) - que aprobó el plebiscito como procedimiento para su concreción -.]

[Con motivo de la generalizada Violencia entre liberales y conservadores,]**de la protesta contra el texto a ser sometido a aprobación**[, de la pugna conservadora entre laureanistas, alzatistas y “trecejunistas”, del]*[miedo utilizado como arma política]*[ y del llamado “rojismo” en las filas militares antes del 1° de diciembre hubo tres conspiraciones cívico militares contra la Junta, y el 2 de mayo de 1958, dos días antes de las elecciones presidenciales el rojismo intentó un golpe de Estado bajo el mando del coronel Hernando Forero Gómez; décadas después la hija de Rojas Pinilla reconoció públicamente que su padre había organizado la acción. Lo interesante es que los responsables del golpe no fueron sancionados dado que la figura de la “impunidad pactada” se impuso por Razón de Estado.]

[Algunos puntos del texto plebiscitario merecen resaltarse. Partiendo de Dios como fuente suprema de toda autoridad se estableció: 1) las mujeres tendrían los mismos derechos que los hombres; 2) el pacto liberal-conservador se extendería hasta 1968; 3) el gabinete ministerial seria bipartidista y las Fuerzas Armadas podrían participar en él; 4) los funcionarios que pertenecieran a la carrera administrativa no podrían participar en las actividades políticas de los partidos; 5) el nuevo gobierno destinaría el 10% del presupuesto nacional para la educación pública; 6) los magistrados en la Corte Suprema de Justicia serían vitalicios; y 7) en adelante las reformas constitucionales serían realizadas únicamente por el Congreso.]

***Por el Sí votaron 4.169.294 colombianos; en contra 206.864 y en blanco 20.738.***[ Los comunistas  se perdieron políticamente: recurrieron al blanco puesto que, decían, como el texto tenía aspectos negativos pero también tenía elementos positivos, “sin votarlo negativamente” el ciudadano dejaría constancia de su inconformidad … También propusieron una fórmula independiente que sería considerada por la legislación como voto en blanco; en breve ésta señalaba: 1) anulación de las medidas represivas de Rojas, cese del Estado de Sitio, libertades democráticas y derechos políticos desde los 18 años; 2)  elecciones libres; 3) representación de todos los partidos en el Congreso y creación de un régimen parlamentario; 4) responsabilidad de los militares por delitos comunes cometidos; 5) 30% del presupuesto para Educación y disminución del rubro de guerra; 6) reforma agraria; 7) salario mínimo; 8) recuperación de las riquezas nacionales; y 9) política exterior independiente y de paz.]

***Para decirlo de una vez el plebiscito “se abrió paso como recurso cesarista***[” .]

[La impunidad pactada entre las elites liberal-conservadoras fue el sello original del plebiscito de 1957 y del Frente Nacional. El 7 de agosto de 1958 Alberto Lleras Camargo se posesionaba como primer presidente del nuevo régimen.]
