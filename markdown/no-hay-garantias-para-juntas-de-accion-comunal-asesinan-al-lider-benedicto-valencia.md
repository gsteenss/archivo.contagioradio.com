Title: No hay garantías para Juntas de Acción Comunal, asesinan al líder Benedicto Valencia
Date: 2019-05-18 17:22
Author: CtgAdm
Category: Comunidad, Nacional
Tags: asesinato de líderes sociales, Caquetá, juntas de acción comunal
Slug: no-hay-garantias-para-juntas-de-accion-comunal-asesinan-al-lider-benedicto-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Benedicto-Valencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Florencia\_Caque] 

Benedicto Valencia, presidente de la Junta de Acción Comunal (JAC) de Miraflores, Puerto Rico en Caquetá  desapareció el pasado miércoles 15 de mayo cuando un grupo de hombres armados se lo llevó a la fuerza. Solo se supo que había pasado con él cuando fue hallado sin vida en una vereda de San Vicente del Caguán con impacto de bala en la espalda y la cabeza. Su homicidio hace más evidente la crisis que viven los afiliados a las JAC en Colombia

**Jaime Gutierrez Ospina, promotor de DD.HH. de la Confederación Nacional de Juntas de Acción Comunal** indicó que los responsables habrían sido hombres de las disidencias de las Farc. Benedicto nunca haba sido amenazado pero si resaltó que por ser víctima del conflicto, estaba en proceso de restitución de tierras.

"Benedicto fue un excelente líder comunal, reelegido como presidente por su compromiso con la comunidad",  comenzó como afiliado de la JAC de Miraflores para luego ser secretario y finalmente presidente, adelantó proyectos con las vías comunitarias, el transporte escolar de los niños de la zona y la defensa de los derechos del territorio. [(Lea también: Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales)](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/)

### Los actores armados que amenazan a las Juntas de Acción Comunal 

El caso de este departamento es particular pues aquellas zonas que fueron dejadas por las Farc tras la firma del acuerdo de paz, han comenzado a ser retomadas por grupos que ven en el Caqueta un territorios óptimo para el cultivo de coca, el tráfico de armas y  un corredor estratégico para moverse hacia Meta, Guaviare, Nariño, Cauca y Putumayo.

"Nos preocupa presuntamente hay un grupo de disidencias que están al servicio de derecha, conocen los líderes y los están amenazando" expresó Ospina quien también indicó que son cada vez más frecuentes los movimientos políticos que amenazan a los integrantes de JAC por no pertenecer a su partido y un incremente en la aparición de grupos narcotraficantes procedentes de México.

### Hay un maridaje entre las fuerzas del Estado y estas bandas criminales". 

El promotor de DD.HH. señaló que estas denuncias las han puesto en conocimientos de las autoridades locales sin embargo no han sido escuchados  y solo cuando los líderes comunales son asesinados es que se abre un proceso que no avanza en la mayoría de ocasiones,  "uno queda aterrado cómo en menos de 24 horas saben quiénes son los actores para algunos casos de estrato 10, pero nosotros somos de estrato 0,1 y 2 y no nos tienen en cuenta" inquiere.

Ospina también denunció el atropello del Ejército, **"Es curioso, donde hay presencia de la guerrilla, los comuneros son estigmatizados de ser aliados de la guerrilla,  pero si nos vamos donde la presencia es de bandas como Los Caparrapos o el Clan del Golfo, allí si no estigmatizan al líder comunal, hay un maridaje entre las fuerzas del Estado y estas bandas criminales".**

### Soluciones alternas y crisis de la Unidad Nacional de Protección 

Ospina también se refirió a la falta de recursos de la Unidad Nacional de Protección y al desinterés del Ministerio de Hacienda por fortalecer a la entidad que debe proteger a las personas amenazadas, "no quiero entrar en defensa de UNP no tiene los recursos para brindar las garantías de protección" quien contrasta esta carencia con el aumento en el asesinato de integrantes de JAC en el país, pasando de 58 en el 2016 a 78 en el 2017 y finalmente a 91 en el 2018.

Con este panorama, las JAC de todo el país que suman más de 65.000 están planteando una movilización nacional en los territorios para evidenciar la grave situación que viven las zonas rurales.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
