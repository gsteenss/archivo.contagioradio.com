Title: Asesinado Jimmy Medina, líder social en San José del Guaviare
Date: 2017-10-05 12:40
Category: DDHH, Nacional
Tags: Asesinato a líderes, asesinato de defensor de derechos humanos, Defensor de DDHH, San José del Guaviare
Slug: fue-asesinado-lider-social-en-san-jose-del-guaviare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DLX1hxxX0AAyp6i.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RemaACPP] 

###### [05 Oct 2017 ] 

Organizaciones sociales denunciaron y rechazaron el **asesinato del líder social y campesino Jimmy Humberto Medina Trujillo** en la vereda Puerto Nuevo del corregimiento del Capricho en San José del Guaviare. El líder fue asesinado el 3 de octubre, tenía 32 años y era vicepresidente de la Junta de Acción Comunal de esa vereda.

De acuerdo con la información de La Mesa de Interlocución y Acuerdo Agropecuario (MIA), y la Red de Medios REMA, los hechos ocurrieron cuando el defensor de derechos humanos estaba **“coordinando labores comunitarias** para el mejoramiento de la vía que une Puerto Viejo y Cachicamo”. (Le puede interesar: ["185 defensores de derechos humanos han sido asesinados en el último año"](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/))

Medina se encontraba desplazándose al sitio al medio día cuando fue interceptado “por un número indeterminado de individuos" Sobre la 1:00 pm, y a 200 metros de donde fue interceptado, **fue encontrado su cuerpo sin vida**, tenía ocho impactos de bala y señales de tortura.

Diferentes organizaciones y asociaciones campesinas de la región, manifestaron que Medina era un líder **“honesto, comprometido, transparente y reconocido por su comunidad”**. Le exigieron a las autoridades el esclarecimientos de los hechos en los que fue asesinado el defensor “para evitar que este caso quede en la impunidad”. (Le puede interesar: ["Asesinatos de líderes sociales son sistemáticas: Somos Defensores"](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/))

Finalmente, indicaron que “estos hechos **no empañan nuestra decisión de seguir defendiendo los derechos** y los intereses de las comunidades de la región del Río Guayabero y de los departamentos de Guaviare y sur del Meta en este momento donde la sociedad colombiana hace esfuerzos por abrir y construir sendas de paz y reconciliación”.

\[caption id="attachment\_47594" align="alignnone" width="522"\]![Denuncia de asesinato de Jimmy Medina](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Captura-de-pantalla-2017-10-05-a-las-12.38.42-p.m..png){.size-full .wp-image-47594 width="522" height="578"} Comunicado tomado de Rema Noticias\[/caption\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
