Title: Oposición venezolana violaría Constitución si destituye a Maduro
Date: 2016-10-24 13:14
Category: El mundo, Política
Tags: Golpe de Estado, Venezuela
Slug: oposicion-de-venezuela-violaria-constitucion-si-destituye-a-maduro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/sputnik-Mundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sputnik Mundo] 

###### [24 de Oct 2016]

Después del anuncio del golpe parlamentario en Venezuela, que **pretende hacerle un juicio político al presidente Maduro,** se conocieron otras **medidas que implementaría la Asamblea Nacional que no serían legales y pasarían por alto la Constitución**. El día de hoy se llevará a cabo una movilización de venezolanos exigiendo que se garantice la democracia al interior del país.

Frente a la posibilidad de que Maduro salga de la presidencia de Venezuela, Hernán Vargas, miembro de ALBA Movimientos, indica que **es necesario que se tenga constitucionalmente mayorías absolutas en la Asamblea Nacional** y en este momento las coaliciones de los sectores opositores aún no logran el número de diputados necesarios, que sería alrededor de 120, motivo por el cual no se podría llevar a cabo este procedimiento.

A su vez, la Asamblea Nacional anunció que tomará medidas como destituir y denunciar a los rectores y jueces del Consejo Nacional Electoral y el Tribunal Supremo de Justicia, ante organismos internacionales, acciones que de acuerdo con Vargas, **no son legales, debido a que la Asamblea no tiene facultades constitucionales para hacer efectivas estas decisiones.**

Se prevé que hoy se lleve a cabo la movilización del Oficialismo, en donde **venezolanos saldrán a exigir que se mantenga la democracia en el país**, mientras que los opositores organizan “La toma  a Venezuela” que se hará el 26 de octubre. Para Vargas estas movilizaciones **hacen parte de los pulsos internos del país, sin embargo considera que cada vez más las marchas de los opositores  pierden fuerza** debido a la falta de una hoja de ruta que trascienda las movilizaciones. Le puede interesar:["Jornada intensa de movilización en Venezuela de sectores Oficiales y de Oposición"](https://archivo.contagioradio.com/jornada-intensa-de-movilizacion-en-venezuela-de-sectores-oficiales-y-de-oposicion/)

"Las recientes encuestas vienen diciendo que **la mayoría de las personas no creen que esta situación se resuelva con un gobierno de derecha**, esa es la realidad de las mayorías" afirmó Hernán Vargas.

<iframe id="audio_13456728" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13456728_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
