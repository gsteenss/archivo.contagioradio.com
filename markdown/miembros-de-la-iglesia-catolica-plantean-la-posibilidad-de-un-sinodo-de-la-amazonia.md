Title: Miembros de la iglesia católica plantean la posibilidad de un Sínodo de la Amazonía
Date: 2017-09-11 17:08
Category: Entrevistas, Paz
Tags: Papa, Papa Francisco, Vaticano, Visita Papa Francisco
Slug: miembros-de-la-iglesia-catolica-plantean-la-posibilidad-de-un-sinodo-de-la-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Sinodo-Francisco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minuto Uno] 

###### [11 Sept. 2017] 

Fueron varios los desafíos y las ausencias que planteó la visita del Papa Francisco a Colombia, la construcción de la paz de manera integral, la reconciliación, la justicia también para las víctimas de los crímenes de Estado y la necesidad de implementar los acuerdos con voluntad política real, fueron algunos de ellos. Sin embargo, para la propia Iglesia ese desafío podría convertirse en realidad en un Sínodo de la Amazonía.

Para el **Padre Henry Ramírez, misionero claretiano, la posibilidad de este conclave sería una de las primeras señales de que el mensaje del Sumo Pontífice cayó en tierra fértil.** En su intervención frente a los obispos en el palacio arzobispal de Bogotá, Francisco hizo un llamado especial a no abandonar la iglesia en la Amazonía y esta propuesta sería un primer síntoma de las reacciones que deja la visita del Papa.

“Se empezó a hablar de la posibilidad de realizar un sínodo de la Amazonía que sería algo muy novedoso porque hasta ahora los sínodos han desarrollado temas como la familia, el clero, la vida religiosa pero un sínodo específicamente para una región y un tema ambiental sería novedoso. Esperamos que esa idea no sea simplemente un diálogo de comedor, sino que se lleve adelante” afirma el sacerdote.

Además “algunas diócesis y pastorales sociales regionales” están empezando a plantear una campaña en torno a la reparación y la reconciliación. Le puede interesar: [Animador regaño del Papa Francisco a obispos y cardenales](https://archivo.contagioradio.com/el-regano-del-papa-francisco-a-los-obispos-y-cardenales-en-colombia/)

### **Los desafíos planteados por Francisco** 

Para el Padre Ramírez los desafíos en general son claros, **Francisco fue contundente en afirmar que la paz solamente se construye si es con todos y todas** y si se garantizan los derechos de las víctimas a la verdad, la justicia y la reparación integral, además esa construcción no sería fruto de las leyes sino de una demostración amplia de voluntad política.

En cuanto esos derechos el sacerdote hace referencia a que la reconciliación tampoco puede tener como base el olvido, requiere un reconocimiento de las responsabilidades de todos los actores de la guerra para que el perdón y esa reconciliación sean efectivos. Además, **Francisco también hizo énfasis en la necesidad de que la paz la construya el pueblo** y que se garantice una participación amplia de todos los sectores de la sociedad.

### **¿Qué es un sínodo?** 

El Sínodo es una reunión eclesiástica dirigida por la jerarquía de la Iglesia Católica, a ella asisten Obispos de todo el mundo que han sido previamente seleccionados. Desde 1965, año de su promulgación se discuten temas como la fe, la moral y la disciplina al interior dicha institución. Le puede interesar: [Histórico discurso del Papa Francisco ante la ONU](https://archivo.contagioradio.com/historico-discurso-del-papa-francisco-ante-la-onu/)

De ahí la novedad en la posibilidad que se realice un Sínodo en el que se aborden temas como la tala indiscriminada de árboles, la exclusión de los pueblos indígenas, la minería a cielo abierto y otros temas ambientales que afectan a los pueblos de la Amazonía para este caso, pero en general del mundo entero.

<iframe id="audio_20809673" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20809673_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
