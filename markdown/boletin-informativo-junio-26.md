Title: Boletín informativo junio 26
Date: 2015-06-26 17:12
Category: datos
Tags: 9 meses desaparición estudiantes Ayotzinapa, Día Internacional de Orgullo Gay, Encuentro de Unidad Juvenil, Reunión Gobierno y comunidad de El mango Cauca
Slug: boletin-informativo-junio-26
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4693411_2_1.html?data=lZumlZmVdY6ZmKiakpuJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZeacYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Hoy viernes 26 de junio inicio el **Encuentro de Unidad Juvenil**, escenario en el que convergirán **múltiples organizaciones** con la intención de **discutir** su papel como jóvenes en la construcción de una Bogotá y un país en paz y con justicia social. **Jeisson Quintero** miembro de la **Coordinadora de Organizaciones TJuntas.**

-Este **26 de junio** se cumplen **9 meses** desde que **43 estudiantes** de la Normal Rural de **Ayotzinapa**, en el **Estado de Guerrero en México**, fueran **desaparecidos** por el grupo paramilitar "**Guerreros Unidos**" en presunta colaboración con la policía y el ejercito mexicano. Para la **Procuraduría General** de la República el **caso está cerrado**, sin embargo, las víctmas aseguran que no ha habido acceso al **Derecho a la verdad**. Habla **Héctor Cerezo**, del **Comité de Derechos humanos Cerezo**.

-Este jueves en la tarde, se llevó a cabo una **reunión** entre **delegados del gobierno nacional**, con los **voceros del Mango, Cauca**, que tenía “la esperanza de escuchar **algún tipo de solución**” frente a la situación que vive la comunidad, quienes denuncian ser **trinchera de la policía**. Sin embargo, “el gobierno es tajante y **no** hubo posibilidad de negociar, como lo afirma **Orlando**, habitante y vocero de la población de El Mango.

-El próximo **28 de Junio** se celebra el **Día Internacional de Orgullo Gay** que gira entorno a toda la comunidad **LGBTI**, conmemorando los disturbios del Stonewall en Nueva York, Estados Unidos de 1969, que dan inicio al **movimiento de liberación homosexual**. En Bogotá se realizará la **XX** marcha por la ciudadanía plena, bajo el lema: “**Colombia libre de prejuicios”**. **Ricardo Montenegro**, de asuntos **LGBTI** de la Secretaria de Integración Social
