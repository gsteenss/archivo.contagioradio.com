Title: Boletín Informativo Agosto 3
Date: 2015-08-03 17:30
Category: datos
Tags: Asesinato fotoperiodìsta Rubèn Espinosa, Mesa nacional de tierras afectados por represas, MOVICE Capitulo Antioquia cumple 10 años, Movimiento Ríos Vivos, Onic demandarà a Sèptimo Dìa
Slug: boletin-informativo-agosto-3
Status: published

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Boletin-de-agosto-3.mp3"\]\[/audio\]

*[Noticias del Día: ]*

-Según Luis Fernando Arias, Consejero Mayor de la Organización Nacional Indígena de Colombia, ONIC, las tres emisiones del programa periodístico Séptimo día, afectan la imagen de las comunidades indígenas en Colombia y generan graves riesgos sobre la vida de las personas señaladas de corrupción y de nexos con las guerrillas.

-Organizaciones defensoras de derechos humanos y de libertad de expresión en México, han manifestado rechazo ante el asesinato del foto-periodista Rubén Espinosa, quien fue encontrado muerto el pasado primero de agosto junto con cuatro mujeres. Entidades como la Comisión Nacional de Derechos Humanos exigieron a la fiscalía seguir "la línea de investigación relacionada con la labor periodística del fotorreportero", el asesinato se da luego de conocerse las amenazas que existían en contra de la vida del comunicador.

-Tras la gira del Movimiento Ríos Vivos en Bogotá donde se realizó una reunión con funcionarios del INCODER, se creó la Mesa Nacional de Tierras para afectados y afectadas por represas, teniendo en cuenta que las 125 represas que hay en todo el país han dejado menores posibilidades a los campesinos de acceder a la tierra. Habla Isabel Cristina Zuleta del Movimiento Ríos Vivos.

-El Movimiento de victimas de crímenes de estado concluyó su asamblea para celebrar los 10 años de existencia del MOVICE capítulo Antioquia. Habla Ingrid Vergara integrante del MOVICE.
