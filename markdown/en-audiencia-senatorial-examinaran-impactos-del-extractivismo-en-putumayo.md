Title: En audiencia senatorial examinarán impactos del extractivismo en Putumayo
Date: 2019-04-11 21:27
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: extracción de recursos naturales, Puerto Asís, Putumayo
Slug: en-audiencia-senatorial-examinaran-impactos-del-extractivismo-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Extracción-Petrolera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: noticialaminuto] 

Este 13 de abril, comunidades del Putumayo llevarán a cabo la **Audiencia Pública Senatorial Ambiental, Por la Defensa del Agua y el Territorio: Putumayo somos Amazonía,** asamblea a la que fueron invitados los congresistas Alberto Castilla, Pablo Catatumbo, Feliciano Valencia y Antonio Sanguino con el fin de reivindicar al Departamento como territorio Amazónico y regular la actividad extractivista de las multinacionales que explotan el territorio.

**Marco Rivadeneira, presidente de la Asociación Campesina de Puerto** **Asís**, afirma que el propósito de esta audiencia pública es denunciar el impacto que está generando la actividad extractivista, frenar el otorgamiento de nuevas licencias y títulos en el territorio y defender el derecho a la consulta Popular y Previa además de realizar un seguimiento a las licencias ambientales otorgadas por el Ministerio de Ambiente, que han permitido la extracción de **minerales como oro, cobre, platino, zinc, molibdeno y plata.**

La expansión mineropetrolera ha tenido en el Putumayo un crecimiento muy acelerado, conformando extensos bloques petroleros en 12 de los 13 municipios del departamento, tan solo en Puerto Asís **"está programada la excavación de 200 pozos hasta el año 2020"** denuncia Rivadeneira quien hace énfasis en que el Putumayo debe ser reconocido como territorio Amazónico no apto para la extracción. [(Lea también: "Colombia debe analizar el costo-beneficio de la extracción petrolera": Manuel Rodríguez) ](https://archivo.contagioradio.com/extraccion_petrolera_colombia_manuel_rodriguez_becerra/)

### **Somos **Amazonía 

[El líder explica a su vez, que no hay un impacto positivo en la región a raíz de las extracciones, "no hay acueductos, no hay hospitales, no hay vías, no hay proyectos productivos, no hay nada, se sigue sacando el petroleo pero en realidad la gente está en la miseria y la pobreza", haciendo referencia a poblaciones como Orito, municipio donde ha perdurado la extracción a lo largo de medio siglo dejando a su paso afectaciones ambientales y sociales.]

El Presidente de la Asociación señala que las multinacionales presentes en el Putumayo **(Amerisur, AngloGold Ashanti, Vetra y Ecopetrol)**, no asumen su responsabilidad frente a la crisis ambiental, "piensan, que por ser multinacionales van a pasar por encima de las decisiones de las comunidades, quienes hemos manifestado que se hagan las consultas previas, que también nos reconozcan nuestros derechos" exige Rivadeneira.

<iframe id="audio_34406032" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34406032_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
