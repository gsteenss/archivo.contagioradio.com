Title: La marcha mundial por el clima en un entorno natural en Bogotá
Date: 2015-11-27 12:04
Category: Ambiente, Nacional
Tags: “clima ambicioso”, 28 y 29 de noviembre Biciusuarios de Bogotá se reunirán para rodar en bicicleta desde de Bogotá hasta “Campo Base” de camping ubicado en el municipio de Suesca, Ecologistas en Acción, la Marcha Mundial por el Clima, La marcha mundial por el clima en Bogotá se realizará bajo un entorno natural.
Slug: la-marcha-mundial-por-el-clima-en-un-entorno-natural-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/bici.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:andaluciainformación 

###### [27 nov 2015]

Este 29 de noviembre se realizó** la Marcha Mundial** por el Clima, esta movilización para llamar la atención de los gobernantes para que actúen a favor del cambio climático.

Cabe recordar que la Conferencia de las partes COP21 se iniciara el próximo 30 de noviembre, donde los gobiernos demostraran a la sociedad que esta movilización surge para que se tomen decisiones eficaces ante el calentamiento global.

En el marco de esta marcha las principales ciudades del mundo realizarán mas de 2300 eventos y movilizaciones ,  adicionalmente esta iniciativa tiene como objetivo exigir a los líderes mundiales a que se vinculen evitando las consecuencias del cambio climático.

Sin embargo mas de 400 organizaciones entre ellas Ecologistas en Acción quieren demostrar que el cambio climático esta afectando a todas las personas, regiones, ecosistemas y economías.

**Colombia también marcha por el clima**

Biciusuarios de Bogotá se reunirán para rodar en bicicleta desde de Bogotá hasta “Campo Base” de camping ubicado en el municipio de Suesca a 60 km de la capital ya que el recorrido es un entorno natural con el fin de concientizar el cuidado del medio ambiente.
