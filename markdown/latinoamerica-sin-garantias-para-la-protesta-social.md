Title: Latinoamérica sin garantías para la protesta social
Date: 2017-03-04 14:25
Category: DDHH, Otra Mirada
Tags: abusos Fuerza Pública, Criminalización de la protesta
Slug: latinoamerica-sin-garantias-para-la-protesta-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [04 Mar 2017] 

Pese a que la protesta es un derecho fundamental para exigir la garantía de los demás derechos, en Latinoamérica los gobiernos siguen **respondiendo de forma violenta a estas manifestaciones, criminalizando a militantes y activistas, aprobando políticas que buscan limitar o restringir este derecho y continúan sin referirse a la impunidad de las violaciones a los derechos humanos.**

De acuerdo con el informe, “Los Estados Latinoaméricanos frente a la protesta social”, realizado por diferentes organizaciones en el continente, el análisis y seguimiento de manifestaciones en 8 países (Colombia, Argentina, Perú, Uruguay, Brasil, Chile, México y Venezuela), evidenció que el **uso de la fuerza y la criminalización de la protesta son las herramientas predominantes en la resolución de conflictos, sin subsanar las problemáticas de fondo.**

### **Leyes para restringir o limitar la protesta** 

De acuerdo con el informe, en Latinoamérica se han multiplicado las leyes, proyectos de ley, reglamentos e interpretaciones judiciales  que pretenden regular, reprimir o limitar el derecho a la protesta, con características como **imponer más restricciones burocráticas, imponer y aumentar las sanciones penales a conductas en protestas y exigir la obligatoriedad del aviso previo para hacer la movilización.**

Colombia ha hecho uso de estas medidas a través de las modificaciones al **nuevo Código de Policía, reglamenta el aviso previo a la movilización**, hora de inicio, ruta por donde transitará y responsables de la protesta, además tipifico como delito en el 2011 la obstaculización de calles. Le puede interesar: "[Ya son 24 las demandas al Código de Policía"](https://archivo.contagioradio.com/ya-son-24-las-demandas-al-codigo-de-policia/)

En otros países como México, las protestas **no pueden ser espontáneas** y deben tener previo aviso, en Perú hay algunos espacios en donde es prohibido que se realicen manifestaciones o plantones cerca de edificios públicos o en zonas centrales. En U**ruguay está prohibido protestar o tener reuniones de más de 50 personas,** en frente del Palacio de Gobierno.

De igual forma, hay países que han tomado medidas mucho más radicales autorizando el uso de la Fuerza Armada para controlar las protestas o movilizaciones. En el caso de **Brasil, la constitución de este país autoriza el uso de las Fuerzas Armadas en la garantía del orden público, pero no aclara las situaciones en que esta debería ser utilizada.**

El informe señala que en los países analizados, la intervención de las **Fuerzas Armadas se da en su gran mayoría en las movilizaciones o protestas rurales, las periferias o asentamientos informales**, aumentando el riesgo de la violación a los derechos humanos por las pocas garantías que ya hay en estos territorios. Le puede interesar: ["6 campesinos heridos en manifestaciones contra erradicación forzada"](https://archivo.contagioradio.com/campesinos-exigen-presencia-del-gobierno-en-tumaco/)

### **Latinoamérica y la criminalización de la Protesta** 

Otra de las medidas que han usado algunos países para controlar las protestas es la **creación de leyes antiterroristas, que al ser tan amplias e indefinidas permiten cualquier tipo de imputación de cargos** y dan paso a la criminalización de los conflictos, Brasil imputo cargos por terrorismo contra 4 integrantes del Movimiento Sin Tierras, en el año 2015.

En Chile la ley antiterrorista no se adecua a los estándares de derechos humanos, por lo cual la Corte Interamericana de Derechos Humanos, **señaló que esta ley vulnera la garantía  al debido derecho procesal y se rige a partir de prejuicios raciales y estereotipos para acusar a las personas.**

En Brasil, tras las multitudinarias movilizaciones del 2013, se aumentaron las penas por incendios, daños al patrimonio público y agresiones físicas cuando las víctimas sean policías, sin embargo durante la aprobación de este proyecto de ley, **no se tuvieron en cuenta las evidencias del funcionamiento arbitrario e ilegal del poder Judicial**. Le puede interesar:["Protestas contra golpe de estado en Brasil, paralizan vías en 7 estados"](https://archivo.contagioradio.com/protestas-contra-golpe-de-estado-en-brasil-paralizan-vias-en-7-estados/)

De igual modo, hay otras herramientas que los países han venido desarrollando para la criminalización de la protesta, uno de ellos tiene que ver con la interceptación y vigilancia de las comunicaciones de  organizaciones y movimientos sociales, que se pueden registrar antes de la movilización o durante la misma, **otorgándole permisos a la fuerza pública para grabar a las personas que asisten a las movilizaciones.**

En Colombia, este tipo de grabaciones, por parte de agentes del Escuadrón Móvil Antidisturbios **ya es permitido y puede usarse como prueba a la hora de imputar cargos.**

### **Y …¿Quién responde por  la impunidad?** 

Todas las leyes promulgadas para controlar las movilizaciones o reprimirlas también han generado una amplitud para las facultades de las Fuerzas Policiales que a su vez, dan paso a un **marco de impunidad y violaciones a los derechos humanos de quienes protestan, sin que exista una respuesta estatal y judicial** de control a los excesos de fuerza de estas fuerzas policiales.

El informe indicó que hay dos problemáticas en las que los órganos de control demuestran serias deficiencias en Latinoamérica: **la demora en las investigaciones a militares o policías y el avance insuficiente de la atribución de responsabilidades penales.** Le puede interesar:[ "Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

A su vez el documento exponen 3 masacres en donde se ha demostrado la actuación y responsabilidad de la Fuerza Pública, sin que existan algún avance en la investigación o imputación de cargos a policías o agentes estatales, mientras que del otro lado se ha investigado y juzgado a civiles, las masacres son: **Masacre de Bagua, Perú, en el año 2009, Parque Indoamericano, Argentina, en el año 2010 y la Masacre de Curuguaty, Paraguay, en el año 2012.**

Para el caso de Colombia, organizaciones defensoras de derechos humanos, han demostrado que **hay un 99% de impunidad en crímenes cometidos por agentes estatales**, al igual que retrasos y demoras en las investigaciones, sin que existan responsables por los daños, lesiones o muertes de civiles en protestas.

### **La protesta es un derecho** 

Finalmente el texto afirmó que al ser la protesta un derecho fundamental los gobiernos latinoamericanos deben trabajar en la **creación de políticas públicas que permiten solucionar la problemática de fondo**, al igual que conformar esquemas de seguridad en la protesta que garanticen su ejercicio y no por el contrario la represión.

Otro hecho en el que el Estado debe brindar garantías es en el **avance de las investigaciones e imputación de responsabilidades a los abusos de fuerza** ya denunciado por las comunidades, e incluso replantear el ejercicio de la  reacción violenta y represiva al interior de las mismas fuerzas públicas.

[Encuentre aquí el informe](http://www.cels.org.ar/protestasocial_AL/)

###### Reciba toda la información de Contagio Radio en [[su correo]
