Title: "La Paz en Jaque" una denuncia sobre el aumento del pie de Fuerza en Colombia
Date: 2018-01-10 11:59
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Fuerza Pública
Slug: la-paz-en-jaque-una-denuncia-sobre-el-aumento-del-pie-de-fuerza-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ejercito-Popular-de-Liberación-e1527526839809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE agencia] 

###### [10 Ene 2018] 

“La Paz en jaque” es el más reciente informe sobre la situación de los derechos humanos y violencia sindical en Colombia, que evidenció, que pese a la firma de los Acuerdos de Paz en La Habana, al interior del país “se sigue usando la violencia institucional para el sostenimiento del actual sistema económico”, ejemplo de ello sería el incremento en el gasto militar de** 8,2 por ciento**, destinando un total de  ****32,4 billones** pesos, mientras el asedio hacia lo líderes sociales, sindicalistas y defensores de derechos humanos continúa avanzando**.

### **La militarización en tiempos de paz** 

Una de las situaciones que se denuncia en el informe tiene que ver con el aumento de la Fuerza Pública en el país y de las arbitrariedades que continúan cometiéndose, una de ellas tiene que ver con la permanencia de bases estadounidenses en territorio colombiano, pese a que la Corte Suprema de justicia lo consideró ilegal. **Las bases están ubicadas en Larandia, Tres Esquinas, Puerto Leguizamón, Leticia y Florencia**.

De igual forma se expuso la militarización de diferentes regiones del país como el Catatumbo, en donde hacen presencia el Batallón de Infantería No.15 General Francisco de Paula Santander, **la Brigada móvil No 23, el Batallón Energético y Vial No.10, que hacen parte de la Brigada 30 y la Fuerza de Tarea Vulcano**.

De acuerdo con las comunidades y el informe, este aumento en el pie de Fuerza corresponde con favorecer el avance de proyectos económicos. Además expresa que la población civil ha sido víctima de diferentes formas como **ejecuciones extrajudiciales, detenciones arbitrarias, hostigamientos permanentes, desplazamientos forzados, empadronamientos y estigmatización hacia las comunidades**.

### **El movimiento social en la mira de la Fuerza Pública** 

Otro de los temas a tratar en el informe **es la persecución, criminalización y judicialización hacia el movimiento social, por parte de la Fuerza Pública**, en las diferentes movilizaciones, paros o protestas que transcurrieron el año pasado.

El informe denunció el trato militar con el que es tratada la protesta social, el uso de armas no convencionales que son usadas de forma indiscriminada contra la población, como es el caso de los gases lacrimógenos, la tortura contra personas detenidas y la inteligencia militar.

Uno de los casos que se reportó fue la detención ilegal del defensor de derechos humanos Rommel Durán y la comunicadora María Montiel, durante las manifestaciones en Ayacucho, César, en el mes de abril. Ambos fueron golpeados por integrantes del ESMAD. (Le puede interesar: ["Ayacucho y la Mata, César completan 37 días de protesta"](https://archivo.contagioradio.com/comunidades-de-ayacucho-y-la-mata-en-cesar-completan-37-dias-de-paro/))

### **La avanzada paramilitar** 

El documento manifestó que la principal fuente de financiación de los grupos paramilitares es el narcotráfico, la ganadería extensiva, el contrabando de gasolina y de la palma conocida como corozo. Así mismo, el texto estableció **diferentes tipos de intimidaciones hacia la comunidad o los defensores de derechos humanos**, líderes y sindicalistas, entre los que se encuentran los panfletos, los seguimientos, las amenazas y las acciones contra la vida de las personas.

De igual forma, se logró identificar que los grupos paramilitares se presentan con bajo diferentes nombres en el país, en el caso del Norte de Santander, **se han denominado como Clan del Golfo, Villacareros, Escorpiones, Águilas Negras y Los Rastrojos o Urabeños**.

Además, comunidades como las del Catatumbo y Tibú han denunciado el aumento de incursiones paramilitares en sus territorios con anuncios de recuperar el control en estos lugares. (Le puede interesar:["Paramilitares atentaron contra integrantes de la comunidad de Paz de San José de Apartadó"](https://archivo.contagioradio.com/atentado-comunidad-san-jose-apartado/))

### **La protección del Ambiente un riesgo para el modelo actual** 

En el informe también se habla de los múltiples proyectos minero energéticos que se están llevando a cabo en el país, específicamente en los departamentos de Santander y Norte de Santander, y de los obstáculos que se han puesto, **por parte de las instituciones para frenar la defensa del ambiente**.

Tal es el caso de la suspensión de las consultas populares de El Peñón y Carmen del Chucuri, en donde por falta de recursos no se pudo llevar a cabo esta elección. De igual forma, el informe denuncia las agresiones y hostigamientos a la líder ambientalista Nini Johanna Cárdenas, en Carmen de Chucurí, **el último incidente contra su vida se presentó el pasado 10 de diciembre de 2018**, cuando fue perseguida por un hombre en la plaza de este municipio. (Le puede interesar: ["Amenazada líder social en Carmen del Chucurí"](https://archivo.contagioradio.com/amenazada-lider-social-ambientalista-en-carmen-del-chucuri-en-santander/))

Este informe fue realizado por el Equipo Jurídico Pueblos, Casa Aguayá, el Sindicato de trabajadores del Sistiema Agroalimentario, SINALTRAINAL, ASOFAMIPREC, SINALTRUNAL, ASOFAMINTERCUC y colectivos de presos sociales de diferentes regiones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
