Title: Asesinan a Jorge Eliecer Roa Patiño, ex alcalde de Miraflores en Guaviare
Date: 2018-08-14 18:11
Category: DDHH, Nacional
Slug: asesinan-jorge-patino-alcalde-en-guaviare
Status: published

###### [Foto: Rashide Frias] 

###### [14 Ago 2018] 

En horas de la tarde del lunes 13 de agosto, fue asesinado Jorge Eliecer Roa Patiño, ex alcalde de Miraflores en Guaviare (2001-2003), líder campesino, comerciante y transportador.  Según la Corporación Reiniciar, su administración es muy recordada por obras en sectores como educación, salud, cultura y deporte; y por el buen trato que sostenía con funcionarios y habitantes de la zona.

De acuerdo con el comunicado emitido por Reiniciar, durante la alcaldía de Roa, tanto él, como los Concejales de la Unión Patriótica (UP) del Municipio tuvieron que trasladarse a la Capital del Guaviare para asegurar su vida, puesto que vivían en constante amenaza de paramilitares y bajo "el silencio cómplice de la Fuerza Pública". (Le puede interesar: ["Genocidio de la UP irá a la Corte Interamericana de DDHH"](https://archivo.contagioradio.com/genocidio-de-la-up-ira-a-la-corte-interamericana-de-ddhh/))

El caso de persecución vivido por el ex alcalde de Miraflores hace parte de los documentados por Reiniciar, que a su vez fueron objeto de una demanda ante la Comisión Interamericana de Derechos Humanos , por la que el Estado Colombiano fue encontrado responsable del exterminio de la UP, "realizado mediante una persecución política que compromete a sus propios agentes y su complicidad con grupos paramilitares".

[Comunicado de la Corporación Reiniciar sobre el asesinato Jorge Eliécer Roa Patiño](https://www.scribd.com/document/386274189/Comunicado-de-la-Corporacion-Reiniciar-sobre-el-asesinato-Jorge-Elie-cer-Roa-Patin-o#from_embed "View Comunicado de la Corporación Reiniciar sobre el asesinato Jorge Eliécer Roa Patiño on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_95352" class="scribd_iframe_embed" title="Comunicado de la Corporación Reiniciar sobre el asesinato Jorge Eliécer Roa Patiño" src="https://www.scribd.com/embeds/386274189/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-zhR3Ytu31E7wLGPTrccz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
