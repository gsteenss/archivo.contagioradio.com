Title: 21 de noviembre: por la dignidad  
Date: 2019-11-06 12:32
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: 21 de noviembre, gobierno colombiano, marcha, Marchas
Slug: 21-de-noviembre-por-la-dignidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/76660539_1230226887163716_8907561781614870528_o.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/audio111.3gpp" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/johan-mendoza.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **[Foto por: Andres Zea/Contagio Radio]** 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/johan-mendoza.mp3"\]\[/audio\]

#### Si el mundo cambia o no cambia con una marcha, es la pregunta que el fatalista quiere poner, como piedra en el zapato, al ímpetu que exhala la dignidad golpeada. 

Si la existencia parece un castigo y una culpa, en muchas ocasiones también es acción. Por ahora, no es momento de responder a esa pregunta; los necios que insisten en ver lo inútil de una marcha, siempre siembran una necedad con mucho espíritu: es decir una maldición.

#### *Cosas graves, difíciles y trágicas ocurren en Colombia.* 

Millones de personas convivimos en medio de un sincretismo perverso entre diversión, barbarie, presión, civilismo, descanso e insomnio. Un cóctel que ha dejado tendidos en la moqueta depresiva a muchos colombianos.

Cierto es que desde un ataúd de cristal nos mira la vida acribillada pero no destruida. Cierto es que sentimos una fatiga terminal, esa fatiga que tiene el pueblo colombiano frente a la ola de asesinatos, y sobre todo frente a las reacciones del gobierno, de sus escuderos más rabiosos y de sus escuderos más terriblemente tibios.

*¿Por qué fingir una buena conciencia cuando en realidad no se siente en lo más mínimo lo que está pasando en Colombia? ¿por qué no aceptan algunos personajes que les importa nada la muerte de nuestros hermanos colombianos y con eso nos permitirían no odiarlos, sino amarlos en todo el esplendor de nuestras enemistades? ¿por qué engañan si no sienten nada?*

En Colombia los días han estado turbulentos y las noches calladas. Balas ruidosas y palabras silenciosas han traído tempestades, pues por todo el país, luego de almorzar y bailar, la gente ha tenido un espacio para consumarse en la determinación. ***No queremos este gobierno, no así**.* Y no es nada personal, es el todo que hoy somos como sociedad, lo que convoca esta tempestad.

**Marchar sí sirve. Toda Latinoamérica lo sigue demostrando.** Porque cuando se marcha, las agendas públicas se transforman, a las agendas mediáticas les queda muy berraco taparlo todo y por eso sirve, porque las demandas se escuchan, porque nos damos cuenta de que no éramos los únicos quienes las pensábamos.

No importa que no estemos maduros para hacer reales nuestros sueños maduros, pero hay que saber que la compañía de los pies sobre las calles rescata a las almas que claman dignidad desde las soledades más injustas.

El plomo narco-paramilitar en asocio con sectores de la institucionalidad, con la permisividad del gobierno y con las suaves palabras de los tibios contra los responsables, se han metido en la sangre de nuestra sociedad. Ese plomo pesa, *¡carajo si pesa!* pero no tanto como las ideas que nos negamos a abandonar, no por necios, sino porque las ideas que emergen cuando observamos la barbarie que acontece hoy en Colombia, son guiadas por la musa de todas nuestras luchas: *la dignidad.  *

Claramente el 21 de noviembre es solo una fecha, una gran fecha porque más que definir si será violenta o pacífica, sería justo asegurar que será masiva, pues al final, millones de pies sobre las calles, dicen más que de la paz y de la guerra juntas.

El 21 de noviembre no ha llegado la hora de nuestra última lucha; una marcha es una marcha, calienta los cuerpos y las almas, provoca el rechazo de quien no logra verse entre la multitud por miedo a perder sus privilegios, pero no es nuestra última lucha porque la barbarie está enquistada. **El 21 de noviembre es solo el primero de mil pasos. **

Hoy estamos aferrados a las lágrimas como Elmiro se aferró a Dorilla, tantos asesinatos hacen mella, y no es para menos. Pero también estamos soñando con que algún día pasemos del vasallaje a la finalidad de toda nuestra lucha. Como dijo el sabio, si en algún momento permitimos que se convirtiera al lobo en un perro, y a los humanos en el mejor animal doméstico, entonces es necesario que el lobo vuelva al bosque y los humanos a luchar por ser humanos.

El 21 de noviembre no perseguimos una bandera. Somos los indignados contra la barbarie que quiere arruinar la vida misma, la educación, la salud, las pensiones, entre otras cosas. Los que caminamos por las calles y carreteras, sabemos que nos amamos aun en este invierno de las muertes desapercibidas. Por eso escribimos, nos organizamos en el día en y en la noche, para no dejar tan solas a las dignidades ni a su madre poderosa y justa llamada dignidad.

#### El 21 de noviembre es el día en que nos amaremos entre vivos y muertos 

por eso pisaremos las calles, negando la actitud de convencer, sin antes emitir un mensaje claro con nuestros cuerpos callados, cansados de tanta barbarie, pero no vencidos.  Que no se confundan; el silencio jamás será callar. La lucha no solo es marchar. La violencia por sí sola y el pacifismo por sí solo se parecen en algo… avanzan con vendas en los ojos por caminos donde hay espinas, pero pocas rosas.

El pacifismo solitario, ese que no está dispuesto a arriesgar el cuerpo, es la doctrina escrita para animales domésticos. La violencia solitaria es la doctrina del más necio individualismo... si tan solo fuéramos muy salvajes y solidarios como para organizarnos, entonces la violencia y el pacifismo, se convertirían en el elogio de nuestra capacidad, y ambas harían el amor en un pedestal llamado: humanidad.

El 21 de noviembre, quienes tienen el poder, no solo económico sino moral de este país, afilarán sus lenguas, sus notas periodísticas, sus discursos judicializantes y sus pendejadas. *¿Por qué nos odian salvajes? ¿por qué nos aman domésticos? ¿De qué sirve alimentar la farsa que sustenta las tesis de quienes tienen tan jodida a Colombia?* este gobierno, merece caer.

Y bueno, las gentes que se presentan tibias, de medias tintas siempre corroen a todo el conjunto. Maquillan discursos para hacer olvidar cosas tan grandes como la dignidad. Tibios se dibujarán inmaculados, pero con color rojo este 21 de noviembre, seguirán produciendo náuseas con sus doctrinas de la resignación y mucho asco seguirán dando sus cátedras donde enseñan a querer las cosas medias.

#### El 21 de noviembre saldremos con determinación a solicitar dignamente la renuncia del presidente de la república y una convocatoria inmediata a elecciones. 

Mientras eso no ocurra, nuestra patria será el estado de lucha permanente. Lucharemos en las calles, en los salones, en las conversaciones más superficiales y en las mas profundas, durante las comidas y hasta en nuestros sueños. Lucharemos en los días y en las noches. Nuestra pacha mama será la dignidad, porque sin ella nada fluye hacia el ser humano.

El 21 de noviembre nos detestarán silenciosos, nos detestarán salvajes; pero están preocupados porque ya supimos que las palabras más racionales a veces hacen trizas las cosas. *El 21 de noviembre estamos obligados a sentir*. El 21 de noviembre no estamos obligados a justificar o demostrar una protesta nacional. No.  Que se bajen de esa nube los que les gusta investigarlo todo, explicarlo todo; recuerden que cavar y cavar les termina dañando las manos que también sirven para acariciar.

Con tristeza escribo que antes y después del 21 de noviembre, existe un martirio reservado que contienen las víctimas que aún hoy no son víctimas. Por eso marchemos por la dignidad. Luego de que dimita el gobierno, haremos una gran constituyente donde la razón y los pueblos se mezclarán en la más famosa de todas nuestras concordias.

Pero por ahora el 21 de noviembre no es una jornada de carnaval, no es una jornada de celebración, pues todo tiene su espacio, esta marcha, para que lo sepan bien los omnicontentos, es de indignación. No quiere violencia. No pretende eso, no opacarán con noticias falsas un sentimiento que recorre todo el país. Estamos cansados de la forma como se conduce todo el Colombia. Por eso, es probable que la violencia la comience el gobierno cuando se vea acorralado por tanta, pero tanta gente, que tendrá la esperanza de poner su cuerpo en las calles del país.

Sí, la barbarie aumenta no solo en el Cauca; pero por eso, no marchamos para celebra*r ¿por qué nos niegan la rabia si nosotros ya nos saciamos de llorar? ¿quién inventa la testaruda mentira de que estar indignado es odiar?* No mientan *¿por qué nos roban los deseos de anhelar con rabia la dignidad?*  … seguro estoy de que en el funeral de un familiar del youtuber de 40, sería de muy mal gusto llegar a bailar reggaetón en medio de sus lágrimas ¿entonces? *¿por qué unas intimidades se respetan y otras son vituperadas o resignificadas sin pedirnos permiso? ¿a quiénes le conviene convertir una tragedia colectiva en celebración?* ¿A quiénes colombianos, a quiénes?

le puede interesar:([La contra violencia ](https://archivo.contagioradio.com/la-contra-violencia-en-ecuador/))

El 21 se acerca, es tan solo una fecha**, pero una fecha que quiere vivir en la memoria de este país**, como aquella ocasión en que tuvo lugar la protesta más grande de nuestra historia; aquella ocasión en que, de la mano de la dignidad, solicitamos la renuncia de un gobierno que incendió el país… eso fue durante un noviembre del año 19… ¿lo recuerdas?

 
