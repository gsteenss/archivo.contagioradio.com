Title: Con acuerdo municipal, Concejo de Yopal prohíbe fracking en su territorio
Date: 2019-05-12 12:53
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: acuerdo municipal, Concejo de Yopal, fracking
Slug: con-acuerdo-municipal-concejo-de-yopal-prohibe-fracking-en-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El pasado 10 de mayo, el Concejo de Yopal aprobó un proyecto de acuerdo que prohíbe la explotación de petróleo a través del fracking en este municipio de Casanare. Este sería **el primer acuerdo municipal en Colombia que impide la implementación de esta técnica no convencional**.

Según el concejal Juan Vicente Nieves, co-ponente del proyecto, el Acuerdo busca proteger el ambiente, en particular las cuencas hídricas, que en esta región del país han sufrido de afectaciones ambientales a causa de la explotación tradicional de petróleo. Nieves sostiene que los impactos de explotar con técnicas no convencionales podrían ser incluso más graves que los que han conocido "en carne propia" en los últimos 40 años.

"Es mucho más destructivo de las prácticas que se vienen haciendo. **Hay estudios que demuestran como contaminan y destruyen el agua y el subsuelo.** Hay posibilidades de que minerales radioactivos que hay en el subsuelo, pueda salir a la atmósfera. Eso es un atentado al medio ambiente", afirmó el Concejal.

La explotación de Yacimientos No Convencionales todavía no tiene luz verde para ser implementado en el país, sin embargo, el Gobierno ha demostrado su interés en proceder con esta técnica al incluir dos artículos en el Plan Nacional de Desarrollo que hablan sobre la viabilidad del fracking, y una hoja de ruta para proceder con su implementación. (Le puede interesar: "[El ambiente, otra víctima del Plan Nacional de Desarrollo](https://archivo.contagioradio.com/el-ambiente-otra-victima-del-plan-nacional-de-desarrollo/)")

Al respeto, el concejal Nieves sostiene que la mayoría de **los habitantes de Yopal están en total desacuerdo con la posición del Gobierno y que el mensaje del municipio es que el Gobierno está equivocado**. El Concejal también resaltó que la población había intentado adelantar siete consultas populares para frenar todo tipo de explotación petrolera en este municipio, pero fueron rechazadas por el Tribunal Administrativo de Casanare.

"Como nos bloquearon esa consulta popular, ahora recogiendo ese sentimiento de la población, adelantamos este proyecto de acuerdo, esperando que lo nos respete. Vamos a ver que sucede porque es el primer proyecto de acuerdo de que se tiene conocimiento en el país que prohíbe el fracking", dijo el Concejal. (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")

Con los dos fallos de la Corte Constitucional que le dejaron sin piso legal a las consultas populares para restringir actividades extractivistas, los acuerdos municipales se han convertido en uno de los principales mecanismos democráticos que utilizan los municipios para decidir sobre el uso del suelo en sus territorios. (Le puede interesar: "[Con acuerdo municipal, Urrao le dice 'no' a la minería](https://archivo.contagioradio.com/mineria_urrao/)")

Si bien algunos acuerdos anti-mineros tambíen han sido tumbados por tribunales administrativos en diferentes instancias, el Consejo de Estado decidió el año pasado que los entes territoriales tienen la competencia constitucional para proteger el medio ambiente en su región. Por tal razón, el Concejal espera que "en todo el país se replique esto que estamos haciendo para ver qué va hacer el Gobierno con esa nueva práctica del fracking".

<iframe id="audio_35759698" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35759698_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
