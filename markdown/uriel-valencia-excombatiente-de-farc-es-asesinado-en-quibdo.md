Title: Uriel Valencia, excombatiente de Farc es asesinado en Quibdó
Date: 2020-10-04 00:39
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Asesinatos, FARC, firmantes de paz
Slug: uriel-valencia-excombatiente-de-farc-es-asesinado-en-quibdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Presentación-de-Publicidad-Amarillo-y-Azul-8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 3 de octubre sobre las 8:00 pm se conoció el **asesinato del excombatiente de FARC, Uriel Valencia Ochoa** en la ciudad de Quibdó, departamento del Chocó.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1312567978941181954","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1312567978941181954

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Uriel Valencia tenía 46 años de edad, inicio su proceso de reincorporación en el [ETCR](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)de Vidrí , Héroes de Murri en Virgia del Fuerte**, Antioquia, Espacio que fue *"suspendido unilateralmente por el Gobierno Nacional de manera inconsultada"* y generando alta incertidumbre y miedo en los firmantes de paz que allí habitaban.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Pese a que se desconocen las razones de su asesinato el partido FARC recuerda que finalizando el mes de septiembre se registraron diferentes grafitis y panfletos** en 60 municipios del país en donde se ratificaba la presencia de las Autodefensas Gaitanistas de Colombia, en lugares como Chocó, Antioquia, Bolívar, Casanare, Caldas, Cesar, Córdoba, Cundinamarca, Tolima y San Andrés.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de **Uriel Valencia se convierte según** el **reporte de [Indepaz](http://www.indepaz.org.co/wp-content/uploads/2020/07/3.-Informe-Especial-Asesinato-lideres-sociales-Nov2016-Jul2020-Indepaz-2.pdf), en el 231 de excombatientes asesinados desde la firma del Acuerdo de Paz** en 2016; hechos que se siguen presentando pese a las diferentes acciones nacionales como internacionales que develan las vulnerabilidades que enfrentan los y las excombatientes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
