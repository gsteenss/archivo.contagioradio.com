Title: Comunidad indígena Wounaan desmiente información de la Unidad de Víctimas
Date: 2016-08-05 15:01
Category: Comunidad, Nacional
Tags: Ayuda humanitaria, Chocó, indígenas
Slug: comunidad-indigena-wounaan-desmiente-informacion-de-la-unidad-de-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/PICHIMA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bernardino Dura 

###### 5 Ago 2016 

La población desplazada de **Pichimá Quebrada** rechaza la información presentada el día 24 de Junio de 2016 por la Unidad para la Atención y Reparación Integral de Víctimas de Chocó (Uariv) en el informe sobre la situación de vulnerabilidad de los indígenas Wounaan.

[De acuerdo a la población desplazada, en cabeza del gobernador Álvaro Chamapuro Peña, hay inconsistencias en el informe de la Subdirección de Prevención y Atención de Emergencias del Municipio del Litoral del San Juan, especialmente en lo que se refiere a temas de seguridad.]

[![pichima carta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/pichima-carta.png){.aligncenter .size-full .wp-image-27454 width="665" height="591"}](https://archivo.contagioradio.com/comunidad-indigena-wounaan-desmiente-informacion-de-la-unidad-de-victimas/pichima-carta/)

En el informe se aclara que solamente en dos ocasiones se ha prestado ayuda humanitaria en la comunidad, sin embargo, también se afirma que miembros de la comunidad indígena pueden recoger alimentos con calma en Pichima Quebrada, lo cual no es cierto. Algunas familias realizan actividades cotidianas para sustento de la familia como pesca, cargas de arenas o balastros, pero en la cabecera municipal, no en su comunidad de origen, como dice el documento.

[Gracias a las familias de Unión Balsalito, que han donado algunos productos como bananos, papachinas y primitiva, es que consiguen estos productos de pan coger que se muestran en las fotografías. Además, la mayor parte de la población desplazada está pagando arriendo (62 familias repartidas en 29 casas) y 32 familias en el albergue ofrecido por el Municipio, que no cumple con condiciones mínimas de salubridad.]

[Hasta el momento, ningún miembro de la etnia Wounnan presente en Litoral del San Juan ha recibido documentos por parte de la fuerza armada donde se garantice la seguridad en el territorio.]

[Desde el 11 de abril del presente año, 94 familias, compuestas por 466 personas de la etnia Wounnan, se encuentran desplazadas en el municipio de Litoral del San Juan a causa de los continuos enfrentamientos y bombardeos del ejército a los grupos subversivos que operan en la zona. Al contrario de lo que dice el informe, la situación no ha mejorado. La población de Pichima Quebrada siente que no se tiene en cuenta la difícil situación en la que se encuentran a causa de este desplazamiento forzado y que este informe no muestra la realidad.]

**Bernardino Dura Ismare del resguardo de Pichimá**

**Comunicador popular CONPAZ**
