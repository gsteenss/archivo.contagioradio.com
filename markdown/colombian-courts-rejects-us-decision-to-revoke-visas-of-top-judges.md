Title: Colombian courts reject US decision to revoke visas of top judges
Date: 2019-05-29 15:45
Author: CtgAdm
Category: English
Tags: high courts, JEP, Press Conference
Slug: colombian-courts-rejects-us-decision-to-revoke-visas-of-top-judges
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Presidentas-altas-Cortes.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

On May 14 **the** **Constitutional Court, the State Council and the Special Peace Jurisdiction (JEP)** held a press conference to address the "pressures" of the United States these high tribunals have been subjected to, referring to the U.S. government's decision to revoke the visas of two magistrates of the Constitucional Court, Antonio José Lizarazo y Diana Fajardo.

These institutions reiterated their commitment to exercise their "**jurisdictional functions with autonomy and independence of any interference from any source**." Gloria Stella Ortiz, president of the Constitutional Court, said that as a member of the judicial branch, her duty is the correct and effective administration of justice...without benefits, predispositions nor prejudices."

"The country can be sure that those who exercise the magistracy will act in accordance with our convictions, with the firmness to preserve the institucionality and with the dedication that we have shown before society to administer an imparcial, objective and transparent justice," the magistrate said.

Magistrate Ortiz added that President Iván Duque was informed of the top judges' visa status. She also said in her final remarks that the Supreme Court of Justice and the Higher Council of the Judiciary did not attend the press conference because on the one hand, their functional are "more logistical" than the administration of justice and on the other, the Supreme Court had already talked to the press on the subject.

### **Cesar Gaviria: Duque and the USA creating pressures on the courts** 

In a letter published on Tuesday, the former president and director of the Liberal Party Cesar Gaviria expressed concern over "**the wrongful pressure exerted on the high courts and the JEP by both the presidential administration and the United States."**

Gaviria claimed the act amounted to a violation of the country's sovereignty because it subjects the implementation of the peace accord to "the judgment of government officials in Washington."

The ex-president expressed that he is willing to support the government's achievements, but this would not be possible if the president "does no defend himself or is not interested in defending himself," speaking in reference to the decision of the United States to revoke the visas and to pressure Colombia to resume fumigations with glyphosate.

The colective Defend the Peace also asked for greater protections and special vigilence from the United Nations for the magistrates of the courts for the "attacks and pressions these have victimized recently."
