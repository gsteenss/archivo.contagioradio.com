Title: Mesa de conversaciones con el ELN se anunciaría en Caracas
Date: 2016-10-10 15:02
Category: Paz
Tags: conversaciones de paz con el ELN, ELN, Juan Manuel Santos, Mesa Social para la paz.
Slug: conversaciones-eln-30554
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/dialogos-Eln-y-gobierno-e1468532020926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [10 Oct 2016]

El inicio de las **conversaciones de paz con la guerrilla del ELN** se realizaría esta tarde a las 8 p.m Caracas (7 hora de Colombia). La cancillería de Venezuela envió una acreditación a periodistas citándolos a la casa amarilla. Según se conoció el anuncio tiene que ver con el proceso de paz en Colombia. En ese mismo sentido se pronunció Monseñor Dario Monsalve.

Además el CICR emitió un comunicado en el que dan cuenta de la liberación de un civil en manos del [ELN](https://archivo.contagioradio.com/ad-portas-de-la-fase-publica-de-conversaciones-eln-gobierno/). El hecho se produjo en zona rural del municipio de Fortul en el departamento de Arauca y según el comunicado se constituye en la tercera persona liberada en las últimas 2 semanas. **Las liberaciones se han producido el pasado 6 de Octubre y el 29 de Septiembre.**

Para Victor de Currea Lugo, [todas estas acciones, incluidos los anuncios de prensa](https://archivo.contagioradio.com/eln-decreta-cese-de-operaciones-ofensivas-para-votacion-del-plebiscito/) de los últimos días pueden representar una noticia positiva y favorable a la construcción de paz. El analista señala que una noticia en ese sentido puede aportar a la **premisa de que la paz se construye con 2 mesas y un solo proceso**, teniendo en cuenta la coyuntura actual en que se está recomponiendo el acuerdo final con las FARC.

Además, De Currea afirmó que habría 3 objetivos en la movilización social. Uno para **respaldar los acuerdos de paz entre el gobierno y las FARC**, que también debe orientarse  denunciar la campaña de “patrañas” impulsada por los promotores del NO. Un segundo elemento es la conformación de la [Mesa Social por la Paz](https://archivo.contagioradio.com/piden-intervencion-de-papa-y-onu-para-iniciar-fase-publica-con-eln/) y un tercero en que se abra la posibilidad de discutir temas que no están en la agenda con las FARC para que estén en la agenda con el ELN.

Las conversaciones de paz con el ELN se anunciaron de manera pública el pasado 31 de Marzo con la presencia de Antonio García y Plablo Bletrán por parte de la guerrilla y Frank Pearl por parte del gobierno nacional. Se espera que la delegación del gobierno se mantenga mientras que por parte del **ELN se ha escuchado que Antonio García sería reemplazado por Gustavo Aníbal Giraldo Quinchía "pablito"** jefe militar de esa organización.

<iframe id="audio_13260397" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13260397_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
