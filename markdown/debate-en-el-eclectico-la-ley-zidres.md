Title: Debate en el Ecléctico "La ley Zidres"
Date: 2016-08-10 17:05
Category: El Eclectico
Tags: Campesinos colombianos, Debate Zidres, Ley Zidres, proceso de paz
Slug: debate-en-el-eclectico-la-ley-zidres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesino-e1460152084815.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Quinto Poder 

En esta ocasión asistieron **Andrés Pachón**, abogado y magíster en Derecho Público, profesor de derecho público económico en la U. Santo Tomás y columnista en El Ecléctico; **Andrés Díaz**, negociador internacional, politólogo e internacionalista y asesor del Senador Iván Name; **Andrés Fuerte**, economista y candidato a magíster en Investigación Social Interdisciplinaria, miembro de la coalición contra la Ley ZIDRES y demandante de la Ley 1776 de 2016; y **Juan Camilo Castellanos**, abogado y magíster en derecho administrativo, ex codirector del Partido Liberal, y actualmente edil de La Candelaria por ese partido.

En el debate se discutieron las repercusiones que podría traer la ley Zidres. Algunos panelistas señalaron que favorece la concentración de la tierra mientras que otros afirmaron que lo que la ley hace es aumentar la productividad agrícola del país. Además, los asistentes se refirieron a la posibilidad de que esta ley se convierta en un obstáculo para el desarrollo de los acuerdos de paz entre el Gobierno y las Farc, particularmente respecto a lo concerniente al fondo de tierras que se planeó en La Habana.

Por último, el debate giró en torno al modelo de asociatividad que propone la ley entre los campesinos y las grandes empresas del sector agrícola. Las posiciones se enfrentaron respecto a si esa forma de asociar al pequeño productor con el grande es favorable para la productividad agrícola y las condiciones de los campesinos o si, por el contrario, lo que hace es cargar a los campesinos con todos los riesgos y entregar los beneficios a los grandes productores.
