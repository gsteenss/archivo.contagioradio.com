Title: Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»
Date: 2020-10-06 12:02
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Gómez Hurtado, Álvaro Leyva, Movimiento Nacional por la Verdad, Rodrigo Londoño
Slug: rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Rodrigo-Londono-dice-si-a-la-verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Hoy martes se dio a conocer la respuesta de Rodrigo Londoño, líder del Partido Político FARC, a la carta del exministro Álvaro Leyva en la que hacía un llamado para la conformación de un «M*ovimiento Nacional por la Verdad*». (Lea también: [FARC- EP reconoce ante la JEP la verdad sobre 6 homicidios, entre ellos el de Álvaro Gómez Hurtado](https://archivo.contagioradio.com/farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Leyva escribió la misiva luego de enterarse de la revelación que realizaron miembros del antiguo secretariado de las FARC-EP reconociendo su responsabilidad en los homicidios de Álvaro Gómez Hurtado, Hernando Pizarro León-Gómez, José Fedor Rey, Jesús Antonio Bejarano, Fernando Landazábal Reyes y Pablo Emilio Guarín.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=VJUC7Eom6xE","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=VJUC7Eom6xE

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph -->

Rodrigo Londoño expresó que «*efectivamente, fuimos las FARC-EP las únicas responsables del execrable hecho de haber privado de su vida al doctor Álvaro Gómez Hurtado. Y la Jurisdicción Especial para la Paz, así como la Comisión de la Verdad, recibirán de nosotros los elementos que pueden acreditarlo*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El excomandante de las FARC-EP, expresó que en ese grupo armado, Alvaro Gömez Hurtado era considerado como «*el senador que había incendiado el Congreso de la República, clamando por el exterminio de las llamadas repúblicas independientes. No resultaba difícil alimentar ideas y sentimientos negativos hacia él*» y agregó que ese era «*el grave peligro del sectarismo en política; se llega a pensar que el atentado personal lo soluciona todo*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Hoy los antiguos guerrilleros de las FARC vemos cuán equivocados estuvimos, cuánto contribuimos al infierno en que se convirtió nuestra querida Colombia»
>
> <cite>Extracto carta de Rodrigo Londoño, excomandante de las FARC-EP</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicionalmente, Londoño señaló que luego del crimen «*el Establecimiento se encarnizó en mutuos reproches y sindicaciones que alcanzaron niveles sorprendentes*» y agregó que «*investigar la verdad pasó a segundo plano, ante la urgente necesidad de demostrar que el culpable era el que se quería*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso, afirmó frente a las dudas que ha suscitado en algunos sectores la confesión del crimen, que era momento de dejar descansar en paz de Álvaro Gómez Hurtado, dejar de victimizarlo con el fin de saciar aspiraciones políticas y agregó que las FARC-EP eran las únicas responsables del hecho y había que dejar de buscar culpables donde no los había. (Le puede interesar: [El trabajo de la JEP para llegar a la verdad histórica en Colombia](https://archivo.contagioradio.com/el-trabajo-de-la-jep-para-llegar-a-la-verdad-historica-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente al llamado de Leyva, Rodrigo Londoño expresó que uno de los componentes más importantes del Acuerdo de Paz es la verdad «*por dolorosa y terrible que sea*» y que con la confesión de ese y otros crímenes, los excombatientes de FARC se unían «*a su clamor por la verdad*», pues «*sólo así cerraremos la página horrorosa de la guerra*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Exparamilitares también expresaron su compromiso con la verdad

<!-- /wp:heading -->

<!-- wp:paragraph -->

La semana pasada en sendas cartas dirigidas a Álvaro Leyva también había expresado su disposición de contribuir con la verdad y las víctimas del conflicto armado en Colombia, para lo cual solicitaron también su acceso de la Jurisdicción Especial  para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-, para así poder declarar en calidad de comparecientes en los procesos. (Lea también: [Exparamilitares dicen sí a la verdad](https://archivo.contagioradio.com/exparamilitares-dicen-si-a-la-verdad-para-reparar-a-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El compromiso de exjefes guerrilleros y exparamilitares, daría cuenta que un «M*ovimiento Nacional por la Verdad*» como el que plantea Álvaro Leyva es posible, lo cual contribuiría con la construcción de verdad para las víctimas. (Le puede interesar: ["Jorge 40", la llave que abre otras puertas a la verdad](https://archivo.contagioradio.com/jorge-40-la-llave-que-abre-otras-puertas-a-la-verdad/))

<!-- /wp:paragraph -->
