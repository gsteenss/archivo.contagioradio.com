Title: En Colombia preparan jornada de rechazo a visita de Primer Ministro israelí
Date: 2017-09-07 12:09
Category: Onda Palestina
Tags: Apartheid Israel, Bejamin Netanyahu, Palestina
Slug: rechazo-visita-netanyahu-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/21270955_2016989875213813_2506902705489953356_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BDS Colombia 

###### 9 Sep 2017

**El próximo 13 de septiembre el primer ministro Israelí Benjamín Netanyahu realizará una visita oficial a Colombia**, en una gira que incluye además a México y Argentina; un viaje ante el cual **la sociedad civil colombiana prepara una jornada de rechazo**.

Algunas de las organizaciones que participarán son el **Congreso de los Pueblos, El Partido Comunista Colombiano, La Marcha Patriótica, El Partido Socialista de los trabajadores y grupos antimilitaristas como la Colectiva la Tulpa**, acompañados por organizaciones estudiantiles, sindicales, y personas del común.

Del 11 al 13 se planea llegar al país Austral, y el 13 se dirige al país norteamericano. En medio del tránsito de un país a otro el ministro hará parada de pocas horas en nuestro país **para consolidar la relación que desde hace años construyó con el gobierno Santos-Uribe** .

Según las declaraciones del ministerio de relaciones exteriores israelí esta gira se realiza para **mejorar lazos comerciales con Israel y abrir un diálogo con la comunidad judía de América Latina** en materia de tecnología, agricultura, seguridad y medicina.

Esta visita demuestra que **América Latina se ha convertido en una región sumamente estratégica para Israel**, en parte porque uno de sus mercados seguros, el europeo, ha venido cerrándose por las campañas de boicot y las restricciones que ha impuesto la Comunidad Europea a los productos de las colonias ilegales.

En medio de la visita **se espera que se fortalezcan acuerdos comerciales, pero teniendo en cuenta el perfil exportador Israelí** esto podría significar una intensificación en la importación militar. Sin embargo, el viaje se da en medio de una de las más agudas crisis que ha tenido el dirigente en su país.

En esta emisión de Onda Palestina recopilaremos una serie de historiales judiciales del primer ministro israelí, Bejamín Netanyahu, quien, a propósito de su visita a Latinoamérica, está involucrado en múltiples líos con la Corte Penal Internacional, la entrevista a un activista argentino con motivo de la llegada de Netanyahu a sus tierras, la historia de cómo una poetisa terminó siendo una figura fuerte y significativa frente a la ocupación israelí, y finalmente, tendremos un debate sobre las implicaciones que ha tenido el primer ministro israelí desde su llegada a ese puesto en el año 2009.

<iframe id="audio_20747132" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20747132_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio.
