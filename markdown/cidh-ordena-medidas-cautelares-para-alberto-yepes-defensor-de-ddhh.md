Title: CIDH responde ante vulnerabilidad de defensores de DDHH
Date: 2017-03-03 17:28
Category: DDHH, Nacional
Tags: Alberto Yepes, CCEEU, CIDH, Derechos Humanos
Slug: cidh-ordena-medidas-cautelares-para-alberto-yepes-defensor-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cidh-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [03 Mar. 2017] 

Ante las reiteradas amenazas recibidas por el defensor de derechos humanos, **Alberto Yepes** Coordinador del Observatorio de Derechos Humanos de la Coordinación Colombia – Europa – Estados Unidos (CCEEU), la **CIDH le otorgó medidas cautelares como mecanismo para proteger su vida** y garantizar la continuación de su trabajo en pro de las víctimas de Colombia.

**La Comisión Interamericana de Derechos Humanos (CIDH) solicitó al Estado colombiano que adopte las medidas necesarias** para proteger a Alberto Yepes “ante la serie de amenazas, ataques y actos de violencia a los que se ha visto sometido en los años recientes”. Le puede interesar: [CIDH registra 14 asesinatos de defensores de DDHH en Latinoamérica en 2017](https://archivo.contagioradio.com/cidh-registra-14-asesinatos-defensores-ddhh-latinoamerica/)

**Los hechos a los que hace referencia la CIDH son ingresos y robos a su casa**, que en algunos casos se hicieron mientras su hija se encontraba allí, razón por la que la CIDH hizo extensiva la protección a ella.

Según la CIDH, estos hechos se habrían presentado **“como una retaliación debido a las actividades que ejerce el señor Yepes Palacio como coordinador de la CCEEU** que ha venido presentando informes sobre el contexto de violencia en Colombia y específicamente sobre ejecuciones extrajudiciales en donde se vincula a miembros del crimen organizado y de las Fuerzas Armadas”. Le puede interesar: [Estado no mostró voluntad política en la CIDH frente a desaparición forzada: CCJ](https://archivo.contagioradio.com/estado-no-muestra-voluntad-politica-en-el-tema-de-desaparicion-forzada/)

Adicionalmente, **los integrantes de la CCEEU han sido objeto de robos de sus computadores**, dando como resultado la pérdida de información sensible con la que trabaja esta organización. Sucesos que coincidieron con el momento en que presentarían informes sobre casos de ejecuciones extrajudiciales y desapariciones forzadas.

**Esta medida internacional fue solicitada por varias organizaciones sociales** como la Corporación Jurídica Libertad (CJL), la Comisión Intereclesial de Justicia y Paz, el Colectivo de Abogados José Alvear Restrepo (Cajar), y el Observatorio de Derechos Humanos de la red hicieron la solicitud dada la falta de garantías por parte del Estado colombiano para el trabajo de Yepes.

**Esta no es la primera vez que un defensor de derechos humanos debe solicitar medidas cautelares ante la CIDH** debido a la inacción del Estado en cuanto a su protección o reparación de daños causados. Le puede interesar: [CIDH cobija a 9 mil mujeres wayúu con medidas cautelares](https://archivo.contagioradio.com/cidh-cobija-a-9-mil-madres-gestantes-y-lactantes-con-medidas-cautelares/)

Algunos ejemplos son los familiares de las víctimas del Palacio de Justicia, las víctimas de la operación Génesis, las madres gestantes y lactantes de La Guajra, la Fundación Nidya Erika Bautista, así como a 8 defensores de DD.HH. Le puede interesar: [Condena de CIDH a Colombia es una sentencia con rostro de mujer](https://archivo.contagioradio.com/condena-con-rostro-de-mujer/)

Aunque las medidas de la CIDH llaman la atención del Estado, no significan medidas físicas inmediatas pero si se convierten en herramienta para exigir el respeto por sus derechos y escenarios de interlocucion frente a un Estado que muchas veces no escucha los gritos de auxilio de cientos de líderes y defensores de DD.HH.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]{.s1}

<div class="ssba ssba-wrap">

</div>
