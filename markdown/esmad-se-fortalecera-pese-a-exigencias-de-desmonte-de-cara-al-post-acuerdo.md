Title: ESMAD se fortalecerá de cara al post acuerdo
Date: 2016-06-28 15:19
Category: DDHH, Nacional
Tags: Conversaciones de paz de la habana, ESMAD, FFMM, Paro Nacional
Slug: esmad-se-fortalecera-pese-a-exigencias-de-desmonte-de-cara-al-post-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Kienyke] 

###### [28 Jun 2016]

Gran indignación han causado las palabras del presidente Santos justo cuando diversas organizaciones sociales, juristas, analistas y congresistas insisten en la necesidad de [reformular el papel de las Fuerzas Militares](https://archivo.contagioradio.com/para-esclarecer-la-verdad-del-paramilitarismo-deben-darse-reformas-en-las-ffmm/) y de policía de cara al post acuerdo con la guerrilla de las FARC, el presidente Juan Manuel Santos anunció que el [ESMAD](https://archivo.contagioradio.com/?s=esmad) se fortalecerá porque “vamos a ver (…) más movilizaciones y protestas sociales”

El Escuadrón Móvil Antidisturbios ha sido señalado como uno de los [cuerpos de policía más violatorios del los derechos humanos](https://archivo.contagioradio.com/por-agresiones-del-esmad-al-paro-nacional-33-eurodiputados-envian-carta-al-presidente-santos/), justamente porque sus prácticas obedecen a un tratamiento de guerra contra la protesta social y porque se le endilgan decenas de muertes en medio de hechos de represión contra la protesta social, por ejemplo, la muerte de 3 comuneros indígenas en el marco del Paro Agrario de este año o el **[estudiante de la Universidad Distrital Miguel Angel Barbosa.](https://archivo.contagioradio.com/fallecio-estudiante-udistrital-agredido-por-el-esmad/)**

El representante a la Cámara Alirio Uribe ha afirmado que el ESMAD es un cuerpo de policía que debería desmontarse justamente por las acusaciones que pesan en su contra y porque por **la naturaleza de ese cuerpo de policía es de atacar la protesta social**, en ese marco podría seguir siendo fuente de más agresiones, heridas y asesinatos de personas.

Adicionalmente al anuncio del presidente Santos, está por firmarse el nuevo Código de Policía, que será demandado por organizaciones de DDHH, que da atribuciones propias de una dictadura y no de una etapa de post acuerdo con la guerrilla más antigua del continente.

En el discurso del presidente Santos, durante la ceremonia de ascenso del general Nieto, recalca que precisamente de cara a la firma de un acuerdo final es que se necesitan unas fuerzas militares y de policía más fortalecidas “*se requieren personas de temple, personas fuertes, personas con nervios de acero, pero con el corazón ardiente por el amor a su institución y a su Patria para cumplir bien ese indispensable deber de preservar el orden público en todas las zonas y territorios del país.*” para [enfrentar las protestas sociales que se avecinan luego de esa firma](https://archivo.contagioradio.com/page/2/?s=esmad).

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
