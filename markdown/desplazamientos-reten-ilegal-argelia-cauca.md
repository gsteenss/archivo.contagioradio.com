Title: Al menos 80 personas desplazadas permanecen en retén ilegal en Argelia, Cauca
Date: 2020-04-25 12:22
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Argelia, Cauca
Slug: desplazamientos-reten-ilegal-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Desplazamiento_Apartado-e1553374753387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: desplazamientos Cauca/ CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el [Proceso de Comunidades Negras](https://twitter.com/renacientes) (PCN), son cerca de 80 personas pertenecientes a las comunidades de La Nayita, Zabaleta y Agua Clarita del Consejo Comunitario AfroRenacer del Micay quienes se encuentran sin poder movilizarse en un retén ilegal impuesto por grupos paramilitares, pese a la alta presencia del Ejército en una zona con altos niveles de militarización

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Días atrás la Comisión de Justicia y Paz denunció que disidencias de FARC permanecían en las veredas mientras personas de la comunidad habían advertido que ante las múltiples violencias en la región la única opción que quedaba, han afirmado que una de las posibilidades que les queda era el desplazamiento forzado. [Lea también: En Cauca las comunidades somos el peon de los armados](https://archivo.contagioradio.com/en-cauca-las-comunidades-somos-el-peon-de-los-grupos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Habitantes de la región denuncian que existe una intención de acabar con el proceso organizativo de las comunidades negras que han velado por la implementación del Acuerdo y la sustitución voluntaria de cultivos de uso ilícito en sus territorios. [(Lea también: Asesinan a tres personas en Micay, Cauca)](https://archivo.contagioradio.com/micay-asesinato-tres-personas/?fbclid=IwAR0xe_pBhvJdpr02nfRGZzyWUnH_CsXJ6UMaSszlLUhILdYGKqC0lirXMu0)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Escala la violencia en diversos municipios y veredas del Cauca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Estos hechos se suman a los ocurridos el pasado 22 de abril en la vereda Agua Clara, zona limítrofe entre los municipios de Argelia, López y El Tambo, donde fueron asesinados tres hombres pertenecientes al Consejo Afro Renacer el Micay en medio de una reunión que organizó la comunidad para establecer medidas de protección pues en días anteriores, integrantes de las disidencias de las FARC habrían amenazado de muerte a estas tres personas”. [(Lea también: Tres diferencias entre disidencias de FARC recorriendo las guerras en Colombia)](https://archivo.contagioradio.com/tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En horas de la noche de ese mismo día, Hugo Jesús Girlado, defensor de DD.HH e integrante de Marcha Patriótica también fue asesinado en Buenos Aires, Cauca, mientras que la guardia cimarrona del PCN fue hostigada por hombres armados en el corregimiento La Balsa en la vereda Las Lomitas, una hora antes el Ejército había pasado por el mismo lugar. Según Indepaz, de los 84 asesinatos contra defensores de DD.HH. en Colombia, 26 han ocurrido en el departamento del Cauca.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
