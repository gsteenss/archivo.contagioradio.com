Title: Matrícula Cero: la clave para garantizar la educación pública superior
Date: 2020-07-30 20:51
Author: AdminContagio
Category: Actualidad, Educación
Tags: Educación Superior, Matrícula cero, Universidad Nacional, Universidad Pedagógica
Slug: matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Matricula-Cero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"fontSize":"small"} -->

Foto: Matrícula Cero exigen los estudiantes de la Universidad Surcolombiana @LeonFredyM

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras instituciones como la Universidad de Nariño que logró un aporte de 1.800 millones de parte de la gobernación del departamento, o la Universidad del Tolima y sus más de 13.000 estudiantes han consolidado la matrícula cero con un monto inicial de 6.000 millones de pesos, estudiantes de diferentes instituciones de educación pública superior continúan a la espera de los entes territoriales para poder acceder a un estudio digno para el segundo semestre del 2020 en medio de la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Universidad Nacional y a propósito de la emergencia sanitaria por el coronavirus, estudiantes decidieron desarollar una "encadenatón" en las las instalaciones académicas para exigir la matrícula cero, Daniela Viuche Conde, una de las estudiantes que permanece en los campamentos instalados en la sede de la ciudad de Bogotá, explica que pese a la ausencia de medidas de bioseguridad por parte de la institución se mantienen formas de cuidado para mantener la actividad de la mejor forma posible.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque se planteó iniciar un diálogo con la universidad no se ha tenido una respuesta definitiva. La estudiante advierte además sobre la progresiva reducción de participación de los aportes de la nación en presupuestos para la educación superior lo que ha llevado "al casi autofinanciamiento de la Universidad Nacional"; agrega, que tras revisar el análisis de sensibilidad financiera del 2020 se conoció un déficit de 32.881 millones de pesos, que sumado al faltante acumulado que se ha generado año tras año requeriría un total de 77.466. millones para cerrar el 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explica el profesor **Pedro Hernández Castillo, presidente Nacional de la [Asociación Sindical de Profesores Universitarios](https://twitter.com/ASPU_Colombia)y negociador de la Mesa de Diálogo para la Construcción de Políticas en Educación Superior**, aunque con la movilización estudiantil del 2018 se logró que el Gobierno destinara una cifra de 7 billones de pesos, recursos que en parte se destinaron durante el 2019 a las universidades públicas en su presupuesto general por más de 500.000 millones de pesos y para el 2020 esta cifra se extendió a los 600.000 millones de pesos, son necesarias más medidas para cubrir los 500.000 millones de pesos que requería cubrir la totalidad de la matrícula cero.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Explica, que en diálogos con el Gobierno se le sugirió que dentro del dinero existente en el Fondo de Mitigación de Emergencia (FOME) se destinaran nuevos recursos para las universidad públicas y sus matriculas, sin embargo la respuesta a la solicitud llegó con el decreto 662 que otorgó dinero para prestamos a universidades públicas y privadas por el valor de un billón quinientos mil millones de pesos, que si bien cubre el valor de las nóminas no garantiza recursos para que estudiantes en pregrado y posgrado se financien.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La estudiante de la Universidad Nacional señala que hoy, es la misma falta de garantías y voluntad política de reconocer a la educación como un derecho fundamental y no como un bien que se vende en el mercado la que ha impedido que se le dé prioridad y que en lugar de eso, favorezca a sectores económicos y empresariales en lugar de los sectores más vulnerables.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte **Fabían Pineda de la Universidad Pedagógica** explica que sus exigencias como estudiantes incluyen la matrícula cero, una política de bienestar integral y una condonación de matrículas previas como la del primer semestre del 2020, para alcanzar dicho objetivo explican que serian necesarios 9.366 millones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El Gobierno tiene que sacar recursos para que no haya una alta deserción" advierte el profesor destacando la importancia de los entes territoriales para ayudar a financiar este recurso. En ciudades como Bogotá, señala que la respuesta podría estar en acudir a un porcentaje del Producto Interno Bruto que desde la capital general cerca de de 105.000 millones de dólares y que podrían aportar al acceso a la educación, mientras en otros territorios sería ideal la creación de un plan regional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La deserción está a la vuelta de la esquina

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde hace siete días, cinco estudiantes de la Universidad Surcolombiana se mantienen en huelga de hambre por la Matricula Cero para estudiantes de Neiva, Pitalito, Garzón y La Plata, al respecto, **Humberto Perdomo, representante de los estudiantes ante el Consejo superior de la institución** y parte de la huelga señala que pese al agotamiento físico aún no se ha tenido una respuesta concreta por parte de la gobernación del Huila.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Son dos exigencias en particular las que se hacen desde el estudiantado, lograr una matrícula que garantice los recursos para el segundo semestre del 2020 y el primero del 2021 uno y que desde la gobernación se comprometan a una transferencia anual o la creación de un fondo para la Universidad que logre la reducción de la matrícula. [(Le recomendamos leer: Menos del 10% de quienes presentan el ICFES se gradúan de la Universidad)](https://archivo.contagioradio.com/menos-icfes-graduan-universidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Somos 12.357 personas estudiantes en las cuatro sedes del departamento, se requieren un poco más de 13.000 millones de pesos, por eso se ha planteado a diferentes entidades la consecución de estos recursos, la Gobernación del Tolima ha planteado la suma de 2.200 millones de pesos por conceptos de regalías, sin embargo es una cifra insuficiente", explica el representante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Humberto, se calcula que cerca de un 40% de la población estudiantil al no acceder a la matricula podría desertar durante el próximo semestre, lo que equivale a alrededor de 5.000 estudiantes que no estarían en condiciones de asumir el costo de la matrícula, [(Le puede interesar: Sin «matricula cero» estarían en riesgo un 40% de estudiantes de la Surcolombiana)](https://archivo.contagioradio.com/estudiantes-de-la-universidad-surcolombiana-exigen-matricula-cero/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En términos globales, según el profesor Hernández, de los 600.000 estudiante matriculados en educación superior en instituciones públicas, público, el 94% de los estudiantes corresponde a los estratos 1,2 y 3, es decir más de 500.000 estudiantes cuyas familias han sido las más afectadas ante la contingencia sanitaria y cuyos hogares, que representan cerca del 60% de la población dependen del mercado informal, afectado en particular durante la cuarentena, "no es solo la matrícula cero también se trata del sustento de un estudiante, pero eso no significa que en en el sector privado la situación no sea igual de difícil", agrega.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El reto de la virtualidad en la educación superior

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a la virtualidad como un medio de apoyo a la educación, el profesor Hernández señala que esta exige un tiempo y recursos de los que carecen parte de los y las estudiantes, y que según el docente podrían ser cerca del 30% de la población quienes no cuentan con los medios para comunicarse virtualmente. Según la Coalición Colombiana por el Derecho a la Educación cerca del 62.6% de las sedes urbanas educativas están sin internet y 20% de los estudiantes y profesores no tienen electricidad, especialmente en zonas rurales. [Lea también: Sin luz ni internet no puede haber educación virtual de calidad)](https://archivo.contagioradio.com/a-colombia-llego-la-educacion-virtual-mientras-el-20-de-los-estudiantes-no-tenia-ni-luz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relata el profesor, desde abril se manifesto al Gobierno la necesidad de subsidiar la conectividad de los estudiantes de estratos 1 y 2, algo que tendría un costo cercano a los 200.000 millones de pesos, y aunque desde la cartera de Hacienda se han añadido \$28,6 billones al presupuesto general para enfrentar la crisis, "ni se garantizó la renta básica, pero tampoco se destinaron a subsidar a los estudiantes que no tenían facilidad de conexión".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No solamente acá estamos discutiendo que se pague el valor de la matricula, a partir de la pandemia es difícil tener la economía necesaria y materiales, para seguir estudiando, la virtualidad es un reto pero aún estamos cortos, falta profundizar, no lo vemos como la herramienta principal para impartir educación", explica Daniela Viuche quien a su vez destaca el aumento en el desempleo de jóvenes que llegó a un 26,6%, incrementando en un 8% con relación al 2019. [(Lea también: La educación superior también requiere medidas para enfrentar el COVID 19)](https://archivo.contagioradio.com/la-educacion-superior-tambien-requiere-medidas-para-enfrentar-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
