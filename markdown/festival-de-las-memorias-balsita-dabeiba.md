Title: La IV edición Festival de las Memorias tendrá lugar en La Balsita, Dabeiba
Date: 2019-11-22 16:31
Author: CtgAdm
Category: DDHH, Nacional
Tags: comision de la verdad, Festival, Festival de las Memorias
Slug: festival-de-las-memorias-balsita-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Festival-de-las-Memorias-Dadeiba.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/dabeiba-festival-de-las-memorias.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/festivales-4.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Desde el próximo 28 de noviembre hasta el 1 de diciembre tendrá lugar en la Comunidad de Vida y Trabajo La Balsita, municipio de Dabeiba, Antioquia, **la cuarta edición del Festival por las Memorias**, un encuentro organizado por la Comisión Intereclesial de Justicia y Paz, entre comunidades afectadas y responsables  del conflicto armado que busca tejer una memoria restaurativa.

El evento se realiza al conmemorarse 22 años del desplazamiento que vivieron las familias campesinas de las veredas de La Balsita, La Argelia, Tocunal y Antasales por grupos paramilitares. Alrededor de 400 personas fueron forzadas a abandonar sus territorios y hoy han logrado consolidarse como un espacio de paz en la zona humanitaria El Paraíso.

En esta oportunidad, el foco principal girará en torno a la desaparición forzada y desplazamiento que sufrieron los habitantes. Asimismo, se realizará  la conmemoración de las víctimas de la masacre de La Balsita y El Aro. Para ello, se recorrerán los lugares significativos de memoria como el Cañón de la Llorona, que fue escenario de constantes enfrentamientos entre el Ejército y la guerrilla de las FARC-EP y en el que murieron civiles, actualmente es un  “Bosque de Paz”.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/festivales-4.png){.aligncenter .wp-image-77064 width="592" height="592"}

El encuentro también será un espacio para que  las víctimas den testimonio de la violencia que sufrieron directamente en el conflicto armado: escuchar sus voces, sus historias, y de esta manera, contribuir en materia de verdad, justicia y garantías de no repetición.

Además, este festival dará inicio a la presentación de los informes que serán entregados a la Comisión de la Verdad, el próximo 2 de diciembre, que recogen investigaciones sobre las dinámicas de despojo desarrolladas en la región del Urabá y el Bajo Atrato (Le puede interesar: "[Informes sobre despojo en el Urabá y Bajo Atrato llegarán a la Comisión de la Verdad](https://archivo.contagioradio.com/informes-sobre-despojo-en-el-uraba-y-bajo-atrato-llegaran-a-la-comision-de-la-verdad/)")

**Anteriores ediciones del Festival de las Memorias**

Esta cuarta edición completa el ciclo de Festivales de las Memorias de este 2019. El primero de ellos se realizó en Cacarica, en motivo de la operación Génesis que desplazó a las comunidades negras; el segundo tuvo como lugar de encuentro el Resguardo Ambiental Humanitario So Bia Drua, Jiguamiandó y finalmente el tercer Festival se hizo en Caño Manso-Camelias, Curbaradó (Le puede interesar: "[Del tres al seis de octubre se realizará el tercer Festival de las Memorias en Caño Manso, Curbaradó](https://archivo.contagioradio.com/octubre-realizara-festival-de-las-memorias-en-cano-curvarado/)").

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
