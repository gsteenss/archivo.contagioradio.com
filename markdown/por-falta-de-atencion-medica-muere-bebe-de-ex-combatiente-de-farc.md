Title: Por falta de atención médica muere bebé de integrante de FARC
Date: 2018-01-12 14:07
Category: DDHH, Nacional
Tags: espacios de reincorporación, ex combatientes FARC, FARC, mujeres FARC, salud FARC
Slug: por-falta-de-atencion-medica-muere-bebe-de-ex-combatiente-de-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Farc-21-960x500.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [12 Ene 2018] 

A través de una denuncia pública, los integrantes del Espacio Territorial de Capacitación y Reincorporación Urias Rendón en el Meta, informaron del **fallecimiento del bebé en gestación** de una de las ex combatientes de las FARC que se encuentra en proceso de reincorporación. Este hecho, que ocurrió el 10 de enero, se presenta en medio de las fallas del servicio de salud para esta población.

El comunicado indica que debido a la falta de presencia médica en el Espacio Territorial **“una pareja Fariana perdió la esperanza de poder conformar una familia”**. Explican que María Darcy Laso Rodríguez tenía 5 meses de gestación y sufrió un sangrado que no fue atendido por la falta de servicio médico.

De acuerdo con el diario El Espectador, la mujer pudo salir del Espacio de Reincorporación con la ayuda de los **pobladores de la vereda Playa Rica**. Sin embargo, el bebé falleció en el camino al centro médico de San Vicente del Caguán que queda a dos horas de esta zona.

### **Por negligencia médica en 2017 ya había fallecido un bebé de una ex combatiente** 

Este caso no es el primero que se presenta dentro de la población de ex combatientes de las FARC. En abril del año pasado Rodrigo Londoño, integrante del partido político FARC, informó que por falta de atención médica **falleció en Bogotá un bebé prematuro** de una ex combatiente que se encontraba en prisión.

En su momento Rocío Cuellar, madre del menor, indicó que el bebé **había nacido con algunas complicaciones** por lo que requería de atención médica especializada oportuna. Sin embargo no se presentó el servicio y el niño falleció. (Le puede interesar: ["Por negligencia médica fallece un bebé de guerrillera de las FARC"](https://archivo.contagioradio.com/crisis-de-salud-infraestructura-y-seguridad-en-zona-veredal-gallo/))

En ambos casos los ex combatientes que se encuentran en proceso de reincorporación indicaron que, “los bebés de las guerrilleras **son los hijos e hijas de la paz**, que mueran por negligencia del Estado, es matar la esperanza que nace con un nuevo país”.

### **Espacios de Reincorporación carecen de atención en salud** 

En repetidas ocasiones los integrantes de las zonas de reincorporación han denunciado las condiciones de salud en las que han tenido que vivir. Indican que **no se ha cumplido lo pactado** en los acuerdos de paz. Las mujeres que se encuentran en proceso de gestación, no tienen las garantías suficientes para garantizar la vida de ellas y de los bebés.

En otros espacios de reincorporación e incluso en aquellas zonas donde los ex combatientes han salido debido a las precarias condiciones de infraestructura y **amenazas de grupos paramilitares**, diferentes comités y organizaciones sociales han expresado su preocupación por el estado de salud de estas personas.(Le puede interesar: ["Crisis de salud, infraestructura y seguridad en Zona Veredal el Gallo"](https://archivo.contagioradio.com/crisis-de-salud-infraestructura-y-seguridad-en-zona-veredal-gallo/))

En estos espacios se han dado múltiples casos de paludismo simultáneos y enfermedades que se producen por la falta de tuberías, baños y zonas adecuadas para evitar que se produzcan estos brotes. Cuando esto sucede, **no hay equipos médicos** para atender estos casos.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
