Title: Es necesaria una comisión independiente que diga qué pasa al interior del Ejército
Date: 2019-07-30 21:14
Author: CtgAdm
Category: Nacional, Política
Tags: Ejecuciones Extrajudiciales, Ejército Nacional
Slug: es-necesaria-una-comision-independiente-que-diga-que-pasa-al-interior-del-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ejercito-colombia-696x391.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

La Comisión de Excelencia nombrada por el Gobierno para investigar la existencia de directrices que fomentara la páctica de ejecuciones extrajudiciales al interior del Ejército para obtener resultados, descartó la existencia de este  método, pese a ello, la Comisión admitió basarse únicamente en estudios oficiales, lo que no representaría la totalidad de testimonios y pruebas que evidencian la continuidad de esta práctica por parte de algunos sectores de las fuerzas armadas en el territorio nacional.

Para Alberto Yepes, coordinador del observatorio de DDHH y DIH de la Coordinación Colombia-Europa-EE.UU. (COEUROPA), esta comisión no es imparcial frente a sus conclusiones, pues fue conformada por funcionarios afectos al partido del Gobierno, "son personas que no tienen carácter de independencia o experticia para que fuera denominada de 'excelencia', apunta.  [(Le puede interesar: Nueve comandantes del Ejército estarían implicados en falsos positivos: Human Rights Watch)](https://archivo.contagioradio.com/nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch/)

Yepes argumenta que se trata de un acto de encubrimiento y negacionismo que en lugar de acudir a estudio de campo, se basó en informes oficiales, pasando por alto hechos que han sido ampliamente denunciados, en particular, cree que **el caso del asesinato del excombatiente Dímar Torres, es un hecho que bastaría para desmentir que no se presentan casos de ejecuciones extrajudiciales.**

Para el coordinador, es necesaria una comisión independiente que evalúe al Ejército y que esté integrada por verdaderos expertos del Derecho Internacional Humanitario y no personas "adscritas al partido del Gobierno y que de entrada no entren a blanquear una actitud que ha generado interrogantes y cuestionamientos" en la comunidad internacional y nacional.

Para Yepes, llama la atención que desde un tiempo para acá, la Fuerza Pública dejó de presentar las bajas durante sus operaciones, "se están ocultando en los reportes , si revisan, no aparece en los últimos meses ni una sola baja, sino que se habla de 'afectaciones' un término que para el coordinador resultado ambiguo y que puede relacionarse a neutralizaciones, una palabra que engloba capturas, desmovilizaciones y bajas.

### "Es necesaria una depuración severa al interior del Ejército"

Sobre los relevos que anunció el ministro de Defensa, Guillermo Botero en respuestas a los hechos de corrupción al interior de la Cúpula del Ejército, Yepes indica que estos "no son ajenos a la responsabilidad del propio Gobierno y que pese a que se habló de medidas implacables contra quienes participaron de estos hechos de corrupción al interior de la Fuerza Pública, las sanciones no se han visto reflejadas en los verdaderos responsables.

"Se han concentrado en algunos casos de corrupción pero no se hace referencia a los abusos de violación derechos humanos" advierte Yepes al referirse a las denuncias contra la fuerza pública que llegan de regiones como el Catatumbo donde se habla de seguimientos, empadronamientos y detenciones arbitrarias. [(Lea también: Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos)](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/)

### "Pareciera que la guerra es la situación más conveniente"

Contrario a ello, para el coordinador, lo que se ha presentado bajo el nuevo Gobierno "es una intención de poner en el mando del Ejército a oficiales que tienen la intención de reinstalar la guerra en muchas regiones",  y  permitir a su vez el crecimiento de actores ilegales como una manera de justificar la continuidad de la guerra.

Este accionar, según Yepes podría leerse como una estrategia para hacer trizas la paz y "hacer inoperante la implementación de los acuerdos" pues no solamente estamos siendo testigos de "un inmovilismo de la paz sino un retorno a situaciones de guerra en donde la voluntad del Estado, juega un papel crucial en volver a tiempos pasados en el que la guerra fue muy beneficiosa", concluye.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39185031" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39185031_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
