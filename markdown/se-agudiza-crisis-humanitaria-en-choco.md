Title: Se agudiza crisis humanitaria en el Chocó
Date: 2018-09-19 10:15
Author: AdminContagio
Category: DDHH, Nacional
Tags: Chocó, Crisis humanitaria, Derechos Humanos, grupos armados
Slug: se-agudiza-crisis-humanitaria-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/DSC06937-e1521301398818.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [18 sept 2018] 

Un reciente informe realizado por diversas organizaciones defensoras de derechos humanos, señaló que la crisis humanitaria en el Chocó continúa sin que existan medidas concretas por parte del Estado para contrarrestarla. De acuerdo con el documento, el control de estructuras armadas, **el aumento en la violencia, la desnutrición infantil y la degradación ambiental, son algunas de las situaciones más preocupantes.**

### **La violencia nunca se fue del Chocó** 

En el documento, las organizaciones expresaron que "la presencia de los grupos armados en el territorio sigue siendo evidente", al igual que el control por parte de estructuras como ELN y el Clan del Golfo. En esa medida, señalaron que se esta presentando **reclutamiento a la población civil y de menores de edad, el retorno del cultivo de minas antiperson**a y se esta convocando a reuniones a las que deben asistir los habitantes debido a diferentes mecanismos de presión que son usados por las estructuras.

Mientras que el control territorial se estaría generando con la **imposición de horarios para la movilidad** de la población y la construcción de las llamadas "**fronteras invisibles.** (Le puede interesar: Hay un plan para asesinar líderes en Chocó: [Comisión de Justicia y Paz](https://archivo.contagioradio.com/plan-para-asesinar-lideres-en-choco/))

De igual forma, afirmaron que preocupa la financiación de estos grupos con economías ilegales, a través de herramientas como la **extorsión o el robo de animales**. Estos hechos, según las organizaciones, han provocado varios episodios de **desplazamientos forzados masivos, confinamientos de las comunidades y asesinatos de líderes sociales,** que no parecen tener un fin, debido a la falta de medidas de parte del Estado.

Frente al accionar de la Fuerza Pública, el informe aseguró que **son insuficientes las labores adelantas por las autoridades**, además aseveraron que se creó una red de informantes conformada por población civil, que ha aumentado el riesgo para los habitantes. Asimismo, afirmaron que existen **integrantes de la Fuerza Pública que tienen relaciones con estructuras ilegales,** dificultado el desmonte de las mismas.

### **La vulneración de los derechos humanos en el Chocó** 

En referencia a la problemática de desnutrición que vive el departamento del Chocó, las organizaciones indicaron que **los subsidios alimentarios provenientes del gobierno no alcanzan para atender a la población**, hecho que se agraba, de acuerdo con el texto, al no tener una política gubernamental que favorezca la producción agrícola y al **incumplimiento de los acuerdos de paz y planes de sustitución**, afectando principalmente a los menores de edad.

Adicionalmente, pese a los diversos fallos de la Corte Constitucional, abogando por el derecho a la salud, las organizaciones informaron que **no existe ningún avance en la solución a la crisis de la salud en el departamento**, como tampoco en la creación de un sistema que garantice la atención diferenciada a las comunidades indígenas y afros, que en su mayoría habitan el territorio, ni la infraestructura necesaria para brindar el servicio.

El ecosistema y la naturaleza del Chocó, según el informe, también estaría afrontando grandes riesgos, debido a que la **minería ilegal, la tala indiscriminada, los cultivos ilícitos y el mal manejo de residuos por parte de la ciudadanía,** han acabado con la fauna y flora del departamento que "hoy pone en riesgo la riqueza biodiversa".

### ** El pueblo no se rinde carajo** 

El informe señaló que pese a que en el departamento se ha realizado varios acuerdo con el Estado y se han realizado paros cívicos, el más reciente el año pasado, **no existe ningún tipo de voluntad por parte del gobierno** por cumplirlos. Sin embargo, le están exigiendo a las autoridades que i**mplemente el capítulo étnico de los Acuerdos de paz,** cumpla de manera inmediata los acuerdos frente a la crisis de la salud del departamento y garantice la vida digna para los habitantes.

Además, señalaron la importancia de mantener un escenario de diálogo con la guerrilla del ELN, y expresaron que "**no es suficiente el desarme y la reincorporación de los grupos armados ilegales,** si no se trata la graves situación humanitaria del Chocó".

[INFORME-CRISIS-HUMANITARIA-EN-EL-CHOCÓ-FINAL](https://www.scribd.com/document/388983969/INFORME-CRISIS-HUMANITARIA-EN-EL-CHOCO-FINAL#from_embed "View INFORME-CRISIS-HUMANITARIA-EN-EL-CHOCÓ-FINAL on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_27876" class="scribd_iframe_embed" title="INFORME-CRISIS-HUMANITARIA-EN-EL-CHOCÓ-FINAL" src="https://www.scribd.com/embeds/388983969/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-k02UKO1et4MLvlBIsNR4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
