Title: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz
Date: 2020-09-10 19:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: acuerdo de paz, Pacto por la Vida y la Paz
Slug: organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Pacto-por-la-Vida-y-la-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Ante la ola de violencia, las masacres, los asesinatos a líderes sociales y excombatientes, los desplazamientos forzados, y el abuso de la Fuerza Pública tanto en territorios rurales como urbanos; todo lo cual se ha agudizado aún más en el escenario de pandemia** y tiene al país sumido en una crisis humanitaria; este jueves distintas organizaciones sociales del Pacifico y el occidente colombiano, hicieron un llamado al país para suscribir un **Pacto por la Vida y la Paz.** (Lea también: [No cesa la barbarie: 55 masacres en 2020](https://archivo.contagioradio.com/no-cesa-la-barbarie-55-masacres-en-2020/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Junto a representantes del Gobierno, diversos sectores sociales del **Chocó, Valle del Cauca, Cauca, Nariño** **y Putumayo** se unieron para hacer un llamado a la sociedad colombiana a suscribir este Pacto por la Vida y la Paz, el cual ha sido liderado por **140 organizaciones sociales, étnicas, y eclesiásticas  de estos departamentos.** A esta convocatoria se unieron también, artistas como Goyo y Tostao de Choquibtown, César López, la actriz Carolina Guerra y los escritores Ricardo Silva Romero y Alfredo Ramos Salcedo. (Lea también: [Instalan Campamento humanitario por la vida ante la crisis humanitaria en Colombia](https://archivo.contagioradio.com/instalan-campamento-humanitario-por-la-vida-ante-la-crisis-humanitaria-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las razones  para convocar un Pacto por la Vida y la Paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según las organizaciones convocantes, la firma de este pacto ha sido motivada, entre otros aspectos, por el **incumplimiento del Acuerdo de Paz alcanzado con las FARC** **y la negación al diálogo con el ELN** por parte del Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También afirmaron que, su presencia en los territorios les ha permitido constatar que **“*****hay una política gubernamental de obstrucción y de débil implementación integral del Acuerdo *****d*****e Paz con las FARC”*****.** También, aseguraron que pese a un alivio inicial que se vivió en los primeros meses después de la firma del Acuerdo, en los últimos dos años y medio la violencia se ha recrudecido en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente,  señalaron que *“****hay un clamor no escuchado”* de comunidades y organizaciones en el país** **para que se “*complete la paz*** *mediante procesos de diálogos que permitan una solución política al interminable conflicto armado”.* (Le puede interesar: [Otra Mirada: Comunidades exigen respuestas de paz a Duque](https://archivo.contagioradio.com/otra-mirada-comunidades-exigen-respuestas-de-paz-a-duque/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, enfatizaron en un *“incremento de la pobreza, exclusión social, reordenamiento y despojo a territorios, la corrupción, la desinstitucionalización de la democracia, el recorte de derechos, ****el incumplimiento gubernamental de acuerdos sociales y disminución de garantías para la movilización social”.* Todas estas, razones que motivan y justifican la suscripción de un Pacto para encontrar una** salida negociada al conflicto con todos los grupos armados y para que el país se una alrededor del diálogo y el respeto por la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las intervenciones el delegado de la Co**misión de Justicia y Paz, Danilo Rueda, expresó que el Pacto por la Vida y la Paz que plantea un Acuerdo Humanitario Global ha tenido un avance con la decisión del Tribunal de Cundinamarca** de amparar el derecho a la salud y a la paz dando respuesta de fondo a comunidades étnico territoriales y campesinos que a través de una tutela exigieron un cese de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma la tutela que fue presentada por distintas comunidades en donde exigían al **presidente Ivan Duque acuerdos humanitarios en los territorios que garantizaran paz territorial y el derecho a la vida de los y las lideres sociales, por medio de la solicitud de** *"paz en los territorios a travez del cese armado y el un alto a la erradicación forzada"* "; tutela que que rebasó el tiempo de cumplimiento de términos sin representar cambios considerables.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Algunos de los mandatos del Pacto:

<!-- /wp:heading -->

<!-- wp:paragraph -->

**A.** Exigir e impulsar la implementación integral, con participación social vinculante del *“Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz estable y duradera”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**B.** Exigir a todos los actores armados, Fuerza Pública y no estatales, realizar un cese al fuego humanitario de carácter multilateral o unilateral simultáneo. El propósito es garantizar alivios humanitarios a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**C.** Suscribir un Acuerdo Humanitario Global para acatar, respetar y poner en práctica los principios básicos del Derecho Internacional Humanitario, que detengan el reclutamiento de menores, la no ocupación de lugares de uso comunitario, el respeto del principio de distinción, la no restricción a las comunidades al acceso a alimentos, movilidad y medicamentos, entre otras acciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**D.** Suscribir acuerdos humanitarios territoriales, como punto de partida para avanzar en un futuro Acuerdo de Paz nacional completo y definitivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**E.** Realizar misiones humanitarias a distintas zonas del Pacífico, Suroccidente y otras regiones del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**F.** Ratificar la imperiosa necesidad de superar el conflicto armado, reafirmar la prioridad del dialogo, el respeto a la vida a la movilización ciudadana y la autonomía de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**G.** Reiterar al Gobierno que valore la solución negociada al conflicto armado y retome así, el dialogo con el ELN y los acercamientos con otros grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**H.** Establecer seguimiento y control a las Fuerzas Armadas para que actúen con transparencia en el marco de la legalidad, del respeto a los Derechos Humanos y al Derecho Internacional Humanitario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lea el Pacto por la Vida y la Paz completo [aquí](https://es.scribd.com/document/475038466/PACTO-POR-LA-PAZ-Y-POR-LA-VIDA#from_embed).

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/640386903566258","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/640386903566258

</div>

<figcaption>
Pacto por la Vida y la Paz

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
