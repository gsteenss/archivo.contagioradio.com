Title: Empresa del periódico El Tiempo contaminó con tinta humedal Jaboque
Date: 2016-04-08 17:28
Category: Ambiente, Nacional
Tags: Bogotá, El Tiempo, Enrique Peñalosa, Humedal Jaboque, Secretaría de Ambiente
Slug: empresa-de-el-tiempo-contamino-humedal-jaboque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Jaboque-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Humedales Bogotá 

###### [8 Abr 2016] 

\

El pasado jueves 31 de marzo, la comunidad vecina al humedal Jaboque en la localidad de Engativá, notó que las agua del humedal tenía una mancha azul que se estaba esparciendo. Enseguida informaron a las autoridades, y el viernes la Secretaría de Ambiente notó que **la mancha provenía de la empresa Printer Colombiana S.A.S, perteneciente a El Tiempo Casa Editorial.**

Los daños causados al humedal son irreversibles según informa Daniel Bernal de la Fundación Humedales Bogotá, quien describe que **las aguas contaminadas probablemente ya llegaron a río Bogotá; la vegetación acuática se contaminó; e incluso, aunque algunas aves alcanzaron a escapar hacia otros sectores del humedal, algunas tinguas se visibilizan con marcas de tinta azul; además el olor químico es bastante fuerte.**

Según conoce Bernal, la Secretaría de Ambiente anunció que impondrá una sanción a la empresa por el daño causado, que de acuerdo con las versiones de Printer Colombiana, se habría tratado de un accidente. Se conoce que tras las intervenciones que se le realizaron al humedal en la administración pasada de Enrique Peñalosa, este fue canalizado y las aguas lluvias caen al humedal, de manera que por esos tubos la tinta se habría filtrado.

Desde Humedales Bogotá, se espera que más allá de una sanción monetaria, **exista un espacio pedagógico para que las empresas que generan este tipo de daños al ambiente**, aprendan a reconocer los humedales y riquezas naturales de la ciudad y así mismo se tenga mayor conciencia sobre los daños irreversibles que se pueden ocasionar.

“La empresa no tiene sellos en sus puertas y parece todo en funcionamiento normal, operarios en las afueras y gente entrando a su trabajo. Advierto qué no tengo claro que tipo de sellamiento o sanción hubo contra Printer así que no puedo catalogar esto de anormal o ilegal. **El humedal continúa con manchas azules de tinta y olores químicos en algunas zonas”,** anunció Daniel Bernal, quien señala que pese a que el periódico EL Tiempo publicó que 50 hombres trabajan en la limpieza del humedal, él pudo contar cerca de 15 personas en esas labores, el pasado miércoles 6 de abril.

Por el momento, la comunidad espera que las labores de limpieza continúen hasta que se logre retirar la mayor cantidad de tinta posible. Por su parte, **El Tiempo dijo que “El plan de  contingencia se mantendrá hasta que se hayan superado todos los inconvenientes** causados, se cuenta con la supervisión y vigilancia de la Secretaría Distrital de Ambiente”.

<iframe src="http://co.ivoox.com/es/player_ej_11098676_2_1.html?data=kpadm52ae5ehhpywj5aXaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncaXVz87SzpCmqdPiwtGYj5Cquc%2FYwsjWh6iXaaOnz5C119LJqMLgxtiYpNTLs9WZpJiSo5aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
