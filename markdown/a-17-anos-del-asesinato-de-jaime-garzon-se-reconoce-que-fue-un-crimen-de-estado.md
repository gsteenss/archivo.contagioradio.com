Title: A 17 años del asesinato de Jaime Garzón se reconoce que fue un crimen de Estado
Date: 2016-09-30 14:21
Category: DDHH, Nacional
Tags: Asesinato Jaime Garzón, crímenes de estado, Jaime Garzon
Slug: a-17-anos-del-asesinato-de-jaime-garzon-se-reconoce-que-fue-un-crimen-de-estado
Status: published

###### Foto: Archivo Familia Garzón 

###### 30 de Sept 2016 

El pasado **28 de septiembre la Fiscalía emitió la resolución No. 048, en la que se declara el asesinato del periodista Jaime Garzón Forero como un crimen de lesa humanidad**, cometido en un contexto sistemático de violencia, al igual que el del abogado y defensor de Derechos Humanos Eduardo Umaña .

Estos dos casos no fueron los únicos ocurridos en ese contexto, entre 1997 y 1999 fueron asesinados **Mario Calderón y Elsa Alvarado investigadores del CINEP, quienes documentaban los alcances del paramilitarismo en el Urabá antioqueño y Jesús María Valle abogado defensor de Derechos Humanos**, últimado en su propio despacho en Ituango, luego de denunciar la participación de Fuerzas Militares en las masacres del Aro y La Granja.

Se calcula que en el país fueron cometidos 1.293 homicidios de defensores de derechos humanos entre 1998 y 2014**,** razones por las cuales hoy la Fiscalía puede decir que [el caso de Garzón es de lesa humanidad.](https://archivo.contagioradio.com/consejo-de-estado-condeno-a-la-nacion-por-el-crimen-de-jaime-garzon/) Otro de los insumos para lograr esta declaración, fueron las **denuncias de amenazas y seguimientos de agentes paramilitares y estatales consignadas en los registros del ente investigador durante ese periodo de tiempo.**

“De lo precedente se infiere que el modus operandi común a los homicidios de defensores de derechos humanos consistió en que miembros de la fuerza pública, escudados en su lucha contrainsurgente, declararon objetivo legítimo a defensores (...), con ocasión de su labor o por tener una postura crítica frente a sectores oficiales, información transmitida al máximo jefe de las AUC, Carlos Castaño Gil, quien ordenó a la banda sicarial la ‘Terraza‘ ejecutar el homicidio” señaló el organismo.

Fueron también clave en este proceso las d**eclaraciones del exjefe paramilitar Diego Murillo Bejarano, alias Don Berna presentadas ante la fiscalía los días 10, 11 y 12 de agosto del año 2015**, en las que aceptó la responsabilidad de grupos de paramilitares en estos cinco asesinatos, involucrando con su testimonio [altos mandos oficiales de las Fuerzas Militares.](https://archivo.contagioradio.com/los-generales-y-militares-involucrados-en-asesinato-de-jaime-garzon/)

###### Reciba toda la información de Contagio Radio en [[su correo]
