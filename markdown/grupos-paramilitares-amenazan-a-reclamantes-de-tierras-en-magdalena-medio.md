Title: Grupos paramilitares amenazan a reclamantes de tierras en Magdalena Medio
Date: 2016-04-05 13:21
Category: DDHH, Nacional
Tags: Magdalena Medio, Paramilitarismo, Restitución de tierras
Slug: grupos-paramilitares-amenazan-a-reclamantes-de-tierras-en-magdalena-medio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Despojo-de-tierras-e1518786329996.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Protectionline 

###### [5 Abr 2016] 

Crecen los temores en los habitantes del Magdalena Medio por la persistencia de grupos paramilitares que se oponen a la restitución de tierras. Según campesinos de la región, se han desarrollado **reuniones donde participan estas estructuras junto a empresarios que se han apropiado de los territorios** y que buscarían mecanismos para impedir que las tierras sean devueltas a sus legítimos propietarios.

En Chibolo, Plato y Sabanas de San Ángel, los campesinos se encuentran en medio de la zozobra  por la presencia de grupos neoparamilitares conocidos como ‘**Águilas Negras’, ‘Ejército Antirestitución’ entre otros, que han hecho llegar amenazas a quienes buscan que se les restituyan 10 mil hectáreas de tierras,** además intimidan a la población  diciéndoles que “a partir de las 6 de la tarde todo se recoge”, como lo denuncia una de las víctimas, quien señala que no hay presencia de Policía o Ejército presente en la región.

De acuerdo con la Corporación Jurídica Yira Castro, acompañante de los campesinos de esa región, aproximadamente en los últimos tres meses esas amenazas se han venido acrecentándose justo desde que se conoció que **César Augusto Castro Pacheco , alias ‘Tuto Castro’ ex jefe paramilitar, hace pocos meses permanece en retención domiciliaria**. A partir de esa situación, la Corporación indica que “la zona se volvió a prender, creando nuevos mecanismos, y utilizando estas nuevas figuras como asociaciones, para generar temor”.

Según la Corporación Yira Castro en el grupo de opositores presuntamente, se encuentran: Augusto Castro Pacheco alias "Tuto Castro", Saúl Severini, la familia Cortina Calanche, un sujeto conocido como el Mono Bedoya, Gerardo Orozco (firmante del pacto de Chibolo) así como ganaderos de la Región del sector del Difícil y San Ángel, ellos harían parte de las AUC y habrían firmaron el pacto de Chibolo.  Así mismo,  estarían enviando comunicaciones a varios ganaderos  solicitándoles apoyo económico "generoso" con el fin de crear un proceso contra la restitución.

**“No se sabe que nos pueda pasar, somos campesinos gente trabajadora, estamos nerviosos y en medio de la guerra”**, dice uno de los campesinos víctimas de estas intimidaciones, y agrega “Se comenta que grandes ganaderos que están en contra de la restitución apoyan esos grupos”.

### En contexto 

En Magdalena Medio grupos paramilitares al mando de  Rodrigo Tovar Pupo, alias ‘Jorge 40’ despojó a partir de mecanismos de violencia a habitantes de 17 veredas. Entre 1997 y 2003, ‘El Tuto Castro’ desplazó  a los campesinos de la región, cuyas víctimas regresaron hace poco a sus territorios con el objetivo de recuperar las tierras que les fueron arrebatadas.

Esta zona del país ha sido uno de los lugares más azotados por la presencia paramilitar en el marco del conflicto armado. De acuerdo con información de Acción Social en los 12 municipios del Magdalena **199.746 familias fueron desplazadas entre 2000 y 2009, y en ese mismo tiempo fueron asesinadas 9.642 personas.**

<iframe src="http://co.ivoox.com/es/player_ej_11054789_2_1.html?data=kpadl5mbfJqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncajm1tXc1ZDUpdPVzs7Zy9nFtsbnjMbax9PFvsLijMaY1MrHsMLhwtPhx9iPqMaf1c7S1NfFt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
