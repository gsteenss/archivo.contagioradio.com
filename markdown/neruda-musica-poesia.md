Title: Los versos de Neruda convertidos en canción
Date: 2017-07-12 15:29
Category: Viaje Literario
Tags: Música, Neruda, poesia
Slug: neruda-musica-poesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Neruda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 12 Jun 2017 

La obra poética de [Pablo Neruda](https://archivo.contagioradio.com/si-tu-me-olvidas-pablo-neruda/) se encuentra entre las más representativas de las letras latinoamericanas. Sus versos han adquirido inmortalidad trascendiendo a su fallecimiento, ocurrido en 1973, gracias a las múltiples publicaciones que de su trabajo han circulado por todo el mundo.

La pluma maravillosa del poeta nacido en Parral el 12 de junio de 1904, ha servido para inspirar a escritores, pintores, cineastas y músicos, quienes han transformado sus palabras en imágenes, [películas](https://archivo.contagioradio.com/neruda-una-vision-de-pablo-larrain/) y canciones cómplices de miles de historias de amor, luchas, encuentros y desencuentros.

En 2004, año en que se conmemoraron los cien años del natalicio de Neruda, uno de los muchos homenajes que tuvieron lugar fue la presentación de "**Neruda en el corazón**" un disco compuesto por 18 temas producidos a partir de poemas del escritor. Una producción que lleva el mismo título del poema escrito por Rafel Alberti en 1973.

Artistas de la talla de **Ana Belén**, **Pablo Milanes, Joaquín Sabina, Pedro Guerra, Jorge Drexler, Adriana Varela y Miguel Ríos** entre otros, pusieron sus voces y talento para reproducir textos como el famoso "Poema 15" "Me gusta cuando callas...", o el poema XX "Puedo escribir los versos mas tristes..." en memorables canciones.

Con la producción y distribución de la discográfica BMG de México, el disco incluye el registro fílmico en DVD de los artistas interpretando los temas en vivo, intercalados con la lectura de algunos poemas, un trabajo que, 13 años después de su lanzamiento, vale la pena escuchar y compartir. (Le puede interesar: [El día que Gabo entrevisto a Neruda](https://archivo.contagioradio.com/el-dia-que-gabo-entrevisto-a-neruda/))

<iframe src="https://www.youtube.com/embed/EEBr2FBiR4Q" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
