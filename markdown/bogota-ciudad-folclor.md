Title: En agosto, Bogotá se convierte en "Ciudad de Folclor"
Date: 2017-08-05 14:00
Category: eventos
Tags: Bogotá, Cultura, danza
Slug: bogota-ciudad-folclor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/bogota_ciudad_folclor_supercades_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Idartes 

##### 05 Ago 2017

Si es amante de la danza, el folclor y los espectáculos culturales, prepárese, pues **del 6 al 31 de agosto llega la 5ta versión del Festival "Bogotá ciudad de Folclor"**. Un evento  que contará con la participación de setecientos artistas en escena, 8 escenarios y 10 obras. Manifestaciones dancísticas, provenientes de Colombia y de otras naciones latinoamericanas, que ponen en alto sus festividades populares más representativas.

El Festival inicia en el teatro Cafam de Bellas Artes con el espectáculo **"De fiesta, memoria de tradiciones"**, evento que enseñara las danzas tradicionales de las fiestas y celebraciones de Colombia, Brasil, Ecuador, Perú, México, Bolivia, Costa Rica y Venezuela. Le puede interesar: [Ágora: un Festival para echar cuentos](https://archivo.contagioradio.com/cuentos-agora-festival/).

La programación continua el** 11 de agosto**, a las 6 p.m. en el Teatro Villa Mayor, con la gala **"Lo Mejor de la Danza Folclórica Colombiana 2017"**. **El 15** a las 7 p.m. en el Teatro Jorge Eliécer Gaitán se rendirá **homenaje a los maestros Ligia de Granados y Donaldo Lozano**, fallecido recientemente, por cuenta del Ballet Folklórico Colombiano y el grupo de Danzas del Litoral Pacífico de la Universidad Pedagógica Nacional.

El **viernes 18** el evento se trasporta al Teatro al Aire Libre La Media Torta con la **Gala de Folclor Colombiano de Bienestares Universitarios SUE**. Desde las 2 p.m. se presentarán en tarima los grupos de la Universidad Pedagógica Nacional, La Universidad Nacional de Colombia, La Universidad Militar Nueva Granada, El Sena, La Universidad Distrital Francisco José de Caldas, La Escuela Tecnológica Instituto Técnico Central y el Colegio Mayor de Cundinamarca.

La agenda es muy variada y la mayoría de los eventos son de entrada gratuita hasta completar aforo. Para consultar los horarios y fechas en las que se realizaran los espectáculos del Festival Bogotá Ciudad de Folclor, puede ingresar a la página de IDARTES y programarse para ser parte de este festival.
