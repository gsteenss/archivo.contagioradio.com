Title: Se creará Instancia con Pueblos Étnicos para implementación de Acuerdos de Paz
Date: 2017-03-16 16:01
Category: Nacional, Paz
Tags: implementación acuerdos de paz, Pueblos étnicos
Slug: se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_171.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Mar 2017] 

La Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo final, dio luz verde para que se inicie la conformación de la Alta Instancia con Pueblos Étnicos, como parte de lo pactado en La Habana, que tendrá como funciones dar insumos y recomendaciones a la CSIVI con el fin de que **contribuir a garantizar la incorporación de un enfoque transversal étnico, de género, mujer, familia y generación implementación y seguimiento del Acuerdo Final**

De igual forma, otras de las tareas importantes que tendrá esta organización serán realizar **seguimiento a la implementación de los acuerdos**, en lo que corresponde a sus comunidades, mantener canales de comunicación entre los pueblos étnicos y la CSIVI y **apoyar la difusión, visibilizarían y socialización del Acuerdo Final**. Le puede interesar:["Se creará grupo de Expertos en Tierras para implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/se-creara-grupo-de-expertos-en-tierras-para-implementacion-de-acuerdos-de-paz/)

Además, al ser un órgano consultor también podrá estar al tanto de la reparación de los crímenes cometidos contra comunidades étnicas en el país, que han acabado a través de prácticas violentas ,como el desplazamiento y las masacres, **perpetradas por grupos armados con las costumbres culturales.  **Le puede interesar: ["Comisión Étnica pide participación en Tribunal de Paz"](https://archivo.contagioradio.com/comision-etnica-pide-participacion-en-tribunal-para-la-paz/)

Esta Instancia estará conformado por 8 delegados, entre hombres y mujeres, de las organizaciones representativas de los pueblos étnicos. La selección de estas personas la harán las organizaciones que participaron en la construcción del capítulo étnico en La Habana y **tendrá un periodo de funcionamiento por dos años**. Transcurrido este tiempo se realizará el cambio de delegados a partir de un mecanismo que proponga la CSIVI.

###### Reciba toda la información de Contagio Radio en [[su correo]
