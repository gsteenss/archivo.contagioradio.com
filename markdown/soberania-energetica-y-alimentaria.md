Title: Soberanía energética y alimentaria
Date: 2018-09-24 16:38
Author: AdminContagio
Category: Voces de la Tierra
Tags: Hidroituango, soberanía alimentaria, Soberanía Energética
Slug: soberania-energetica-y-alimentaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/4693ba70fe7ccf8c5461d5f42e16af89_M-e1473409702922-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: scambieuropei.info 

###### 13 Sep 2018 

Estamos pasando de la energía producida por los combustibles fósiles y el carbón a la era de las energías renovables no convencionales como la solar, la eólica y la producida por las olas entre muchas otras; adicionalmente, es necesaria una transición alimentaría que permita hacer de este planeta un lugar sostenible para las comunidades sin impactar de manera negativa al ambiente.

Para abordar la transición energética, agro-ecológica y alternativas frente al desastre de las represas, en este Voces de la Tierra, nos acompañaron **María Camila Macías**, de la Coordinación Nacional de Ríos Vivos Colombia e integrante de ASOQUIMBO en el Departamento del Huila, y **Adriana Cortés** de la organización Wateman, médica de la Universidad Javeriana con Maestría en salud pública de la Universidad de Sidney, Australia.

<iframe id="audio_28825347" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28825347_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
