Title: Gobierno prolonga los ETCR, ¿y luego qué?
Date: 2019-08-15 19:49
Author: CtgAdm
Category: Nacional, Política
Tags: etcr, excombatientes, FARC, Proyectos Productivos
Slug: gobierno-etcr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/ETCR-Heiler-Mosquera.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @][PachoTolozaF] 

Este jueves 15 de agosto finaliza la figura jurídica de los Espacios Territoriales de Capacitación y Reincorporación (ETCR) en los que aún viven cerca de 3.200 excombatientes. Líderes políticos del partido FARC, así como otras personalidades, habían pedido al Gobierno extender la permanencia de estos espacios, como una forma de garantizar un adecuado proceso de reincorporación que, **aunque parece ser uno de los temas que quiere apoyar el Gobierno Duque, no ha tenido el soporte esperado.**

### **¿Qué pasará con los ETCR?**

Según información de la **Agencia para la Reincorporación y la Normalización (ARN),** de los **13.200 excombatientes  que están en proceso de reincorporación, 3.246 residen en los 24 ETCR** que aún existen en el territorio nacional. Para estas personas, el Gobierno prolongará la existencia de los espacios por lo menos un año, según la información que ha trascendido a los medios de información corporativos.

Asimismo, el Gobierno ha anunciado que destinaría 16 mil millones de pesos para la compra de predios donde están los espacios, de tal forma que se integren a sus regiones como parte de corregimientos y veredas. Sin embargo, **tendrán que ser reubicados dos ETCR en Guaviare y Putumayo**, que por diferentes razones no pueden mantenerse en el espacio físico que les fue asignado tras la firma del Acuerdo de Paz. (Le puede interesar: ["Crece la expectativa ante reubicación de 11 Espacios Territoriales"](https://archivo.contagioradio.com/crece-la-expectativa-ante-reubicacion-de-11-espacios-territoriales-de-capacitacion-y-reincorporacion/))

### **Los ETCR se pueden prologar un año más, pero eso no resuelve las necesidades de los excombatientes**

**La senadora del partido Farc Victoria Sandino** declaró que había entendido que la figura de ETCR no se prolongaría un año, "habían dicho que hasta diciembre, pero la situación sigue siendo la misma". Sandino explicó que lo que necesitan los excombatientes es **"garantía de tierra para los proyectos productivos, y apoyo para los proyectos productivos en sí mismos"**. (Le puede interesar:["Sabemos que hay desafíos par ala paz, los hemos visto y escuchado: Consejo de Seguridad de la ONU"](https://archivo.contagioradio.com/sabemos-que-hay-desafios-para-la-paz-los-hemos-visto-y-escuchado-consejo-de-seguridad-de-la-onu/))

En ese sentido, señaló que este gobierno y el pasado utilizaron una política dañina "como es el tema de dispersión de nuestra gente". Según cifras de la misma ARN, **8.720 excombatientes residen fuera de los ETCR y 1.052 más están pendientes por ser ubicados;** lo que puede significar un riesgo económico y para su integridad física, pues quedan limitados a las opciones que se pueden brindar a sí mismos en términos de seguridad y sostenimiento.

> HILO sobre algunas preguntas básicas de la reincorporación de excombatientes de las FARC.  
> Infografía [@ideaspaz](https://twitter.com/ideaspaz?ref_src=twsrc%5Etfw)  
> 1. ¿Dónde están actualmente los excombatientes?  
> - 67% residen fuera de los ETCR.  
> - Sobre el 8% se desconoce ubicación. [pic.twitter.com/8GLp6bkbXx](https://t.co/8GLp6bkbXx)
>
> — Juan Carlos Garzón (@JCGarzonVergara) [August 15, 2019](https://twitter.com/JCGarzonVergara/status/1162016988807520256?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"No es que nosotros queramos estar en un asistencialismo permanente"**

En cuanto al tema de paz, el presidente Duque ha sido criticado por intentar reducir el Acuerdo al desarme, desmovilización y reintegración (DDR) de las FARC, en el marco del enfoque de legalidad de su programa de gobierno. No obstante, Sandino criticó que incluso la 'R' no está garantizada, pues los mismos funcionarios de la ARN les dijeron a excombatientes que se podían ir tranquilos de los ETCR, porque los proyectos productivos individuales obtendrían financiación.

La realidad es que **hasta el momento se han apoyado 29 proyectos, beneficiando a 1.506 excombatientes** (con corte al 2 de julio de este año).  (Le puede interesar:["Gobierno pone ‘pico y placa’ a visitas internacionales en los Espacios Territoriales de Capacitación y Reincorporación"](https://archivo.contagioradio.com/gobierno-pico-placa-visitas-etcr/))

Por esta razón, concluyó que la preocupación real no es "que se acabe la figura de los espacios, es que no hay cómo vivir"; y aclaró que "no es que nosotros queramos estar en un asistencialismo permanente", necesitan ver garantizados sus derechos fundamentales, pero también que se asegure el acceso a tierras para que quienes habitan los espacios puedan trabajarla, y financiaciar sus proyectos. (Le puede interesar: ["Asesinan a excombatientes que esperaban proyectos productivos en Nariño y Cauca"](https://archivo.contagioradio.com/asesinan-a-excombatientes-que-aguardaban-proyectos-productivos-en-narino-y-cauca/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
