Title: Violento desalojo de 170 familias por parte del ESMAD
Date: 2017-01-12 13:50
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, Bello, Desalojo
Slug: violento-desalojo-de-170-familias-por-parte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/bello.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contra Corriente] 

###### [12 Enero 2017] 

RECTIFICACIÓN

Según las primeras versiones, durante el desalojo, producido en el barrio Nueva Jerusalen mientras se desarrollaba un proceso de desalojo, se habría provocado las muertes de un niño de siete años, una mujer de la tercera edad y el aborto de tres mujeres, pero no se ha logrado corroborar la información debido al número de personas que habitan este lugar, la extensión del territorio y al accionar intermitente en que se dio el operativo por parte del ESMAD.

En estos momentos abogados de organizaciones están intentando corroborar la información y se esta a la espera de un comunicado por parte de la Secretaria de Salud, que podría superar los 52.

El menor de edad habría fallecido por efecto de la explosión de una bomba de aturdimiento que cayó al interior de su hogar. De igual forma, miembros de la comunidad han denunciado heridas graves por impactos de balas de goma utilizadas por el ESMAD, que dejarían un saldo de 52 personas lesionadas, entre ellas un joven con lesión en uno de sus ojos.

\[caption id="attachment\_34639" align="alignleft" width="355"\]![Contra Corriente](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/esmad.jpg){.wp-image-34639 width="355" height="474"} Afectado por balas de goma del ESMAD\[/caption\]

Los hechos ocurrieron mientra se desarrollaba un operativo de desalojo a 170 familias, autorizado por un juez de Bello, mientras la comunidad salía en una movilización pacífica a exigir su derecho a la vivienda, hecho que produjo una reacción desmedida en fuerza por parte del Escuadrón Móvil Anti Disturbios.

Por su parte, el Coronel Juan Carlos Rodríguez, subcomandante de la Policía Metropolitana del Valle de Aburrá, negó la muerte de personas durante el desalojo.

Noticia en desarrollo...
