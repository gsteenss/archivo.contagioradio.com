Title: Convocan audiencia pública por la vigencia del paramilitarismo en Colombia
Date: 2016-04-12 11:37
Category: DDHH, Nacional
Tags: Congreso de la República, Derechos Humanos, Paramilitarismo, proceso de paz
Slug: convocan-audiencia-publica-por-la-vigencia-del-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El pilón 

###### [11 Abr 2016] 

En el marco de las últimas actuaciones paramilitares, como el paro armado de las Autodefensas Gatinaistas, organizaciones sociales convocan una audiencia pública este 15 de Abril en el Congreso de la República, para** visibilizar la situación actual del paramilitarismo, aclarar cómo el Estado está o no respondiendo a éste y proponer recomendaciones a mesa de conversaciones de La Habana.**

Este año, se han reportado 28 asesinatos contra defensores de derechos humanos, lo que implica una acción sistemática cometida por [grupos paramilitares](https://archivo.contagioradio.com/?s=paramilitares). Sin embargo, el gobierno se ha limitado a decir que no se trata de algo sistemático, y atribuye esas acciones violentas a las BACRIM, que no serían un fenómeno similar la paramilitarismo según lo planteado por el consejero presidencial para los Derechos Humanos, Guillermo Rivera.

Es por eso que en el Congreso se evidenciará con cifras y hechos puntuales la vigencia de este fenómeno, con la audiencia **“Persistencia del Paramilitarismo: Sin garantías no hay paz sostenible”**, que se llevará a cabo en el Salón Boyacá a partir de la 9 de la mañana.

Según la Defensoría del Pueblo, las BACRIM comenzaron a ser identificadas desde el año 2006, como grupos u organizaciones armadas que surgieron producto de las deficiencias en la implementación de la política de Desmovilización Desarme y Reinserción  de las AUC. La mayoría de las organizaciones y de víctimas coinciden en afirmar que se trata de paramilitares y que la [desmovilización no fue efectiva porque se conservaron las estructuras criminales y de poder](https://archivo.contagioradio.com/grupos-paramilitares-amenazan-a-reclamantes-de-tierras-en-magdalena-medio/) en las regiones en que operan esos grupos.

Los principales grupos que se han identificado pos- AUC son: Autodefensas Gaitanistas de Colombia, Urabeños, Águilas Negras, los Rastrojos, Bloque Meta, Bloque Libertadores de Vichada. Además, los grupos autodenominados como: [Autodefensas Gaitanistas](https://archivo.contagioradio.com/segundo-paro-armado-deja-en-entredicho-lucha-contra-el-paramilitarismo/),  Urabeños, Rastrojos y otros grupos regionales han sido identificados en 55 informes de riesgo, en los escenarios de riesgo reportados.

De acuerdo con la Comisión Interamericana de Derechos Humanos, CIDH **“la violencia derivada de la falta de una desarticulación efectiva y completa de las [estructuras armadas de grupos paramilitares, continúa impactando severamente los derechos de las y los habitantes de Colombia”](https://archivo.contagioradio.com/amenazas-paramilitares-se-habrian-comenzado-a-cumplir-en-putumayo/).** Asimismo, según la Defensoría, en 27 de los 32 departamentos de Colombia hay presencia de grupos paramilitares que actualmente actúa en contra de los avances en el proceso de paz que se adelanta en La Habana, y ahora, los diálogos públicos iniciados con la guerrilla del ELN.

En ese contexto, e**n una reunión de organizaciones sociales, congresistas y defensores de Derechos Humanos, el Estado propuso constituir un grupo mixto** en exista representación del Estado, la Policía, Fiscal y Defensoría del Pueblo, junto a cinco voceros de las organizaciones y plataformas de derechos humanos, con el fin de evaluar cada una de las agresiones que se han presentado contra líderes sociales y discutir sobre las medidas más eficaces para dar garantías a quienes apoyan el proceso de paz con las guerrillas.

Dicho espacio, que ha sido la única salida al problema, propuesta por el gobierno se ha presentado como un consenso sin serlo, por lo que varios voceros de organizaciones están exigiendo que se convierta en una mesa transitoria que brinde soluciones urgentes ante la arremetida paramilitar.

###### Siga la audiencia en en vivo a través de: [![Captura de pantalla 2016-04-13 a las 9.55.17 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Captura-de-pantalla-2016-04-13-a-las-9.55.17-a.m..png)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
