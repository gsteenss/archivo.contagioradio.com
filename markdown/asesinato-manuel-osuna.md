Title: Denuncian asesinato del campesino Manuel Osuna Tapias en San José de Uré
Date: 2019-07-07 12:36
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, asesinato, campesino, cordoba
Slug: asesinato-manuel-osuna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

La Fundación Social Cordoberxia denunció el asesinato de Manuel Osuna Tapias este sábado en horas de la mañana cerca del corregimiento de Versalles, en San José de Uré (Córdoba). La Fundación llamó la atención sobre el homicidio del campesino de 67 años, porque se encontró su cabeza cortada y su casa quemada; **presuntamente, a manos del Bloque Virgilio Peralta Arenas, o también llamados "Los Caparros".**

**Manuel Osuna Tapias, era integrante de la Asociación de Campesinos del Sur de Córdoba (ASCSUCOR); y junto a él, ya se cuentan 8 integrantes de esta Organización asesinados en lo corrido del año.** Así mismo, resulta relevante la cifra de 15 beneficiados por el Plan Nacional Integral de Sustitución (PNIS) asesinados este año en la misma región. (Le puede interesar: ["Asesinan al líder y beneficiario del Plan de Sustitución, Manuel Gregorio González en Córdoba"](https://archivo.contagioradio.com/asesinan-al-lider-social-manuel-gregorio-gonzalez-en-cordoba/))

Según han advertido organizaciones como Somos Defensores, está sería una forma de intimidación, usada por los actores armados para causar miedo entre la población mediante asesinatos con sevicia. En San José de Uré, y en el sur de Córdoba Los Caparrapos; las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), llamadas por el Gobierno Clan del Golfo; el Nuevo Frente 18 y el Ejército Nacional están en disputa por lograr el control del territorio.

De acuerdo a Cordoberxia, el mismo sábado pero en horas de la madrugada se presentaron combates entre las autodenominadas AGC y el Ejército en el corregimiento Viera Abajo, en San José de Uré, presentándose una persona muerta y tres capturadas. (Le puede interesar: ["Jóvenes que se acogieron al plan de sustitución fueron asesinados en San José de Uré"](https://archivo.contagioradio.com/jovenes-que-se-acogieron-al-plan-de-sustitucion-fueron-asesinados-en-san-jose-de-ure/))

> La omision del Estado sigue desangrando al pais. En la vereda el Cerro del mpio San José de Uré, Córdoba, fue vilmente asesinado el campesino UBADEL OSUNA de 67 años de edad.
>
> — Fundación Sumapaz (@FunSumapaz) [6 de julio de 2019](https://twitter.com/FunSumapaz/status/1147653620600623104?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Noticia en desarrollo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
