Title: CONPAZ culmina su primera Asamblea Nacional
Date: 2015-02-27 18:13
Author: CtgAdm
Category: Movilización, Paz
Tags: cese al fuego unilateral FARC-EP, comision de la verdad, Conpaz, FARC, proceso de paz, víctimas
Slug: propuestas-de-conpaz-en-su-primera-asamblea-nacional
Status: published

##### Foto: Contagio Radio 

Organizaciones y comunidades Construyendo Paz en los Territorios, CONPAZ, realizó la primera Asamblea Nacional con la participación de más de **119 procesos rurales de 11 departamentos de Colombia, con el objetivo de analizar los avances en “el proceso de paz entre el gobierno que preside Juan Manuel Santos** y la guerrilla de las FARC EP, los avances en la exploración para la mesa con ELN, y la situación con el EPL, así como, las propuestas del Movimiento Social por la Paz con Justicia Socio Ambiental”, según comunicado de la Comisión Interclesial de Justicia y Paz.

La asamblea se desarrolló en el marco de la celebración de los 15 años de la red CONPAZ y la conmemoración de los **18 años de la** **Operación Génesis de la brigada 17**, ocurrida del 24 al 28 de febrero de 1997, con un año más del asesinato de Marino López, Gilberto Daza, y de Demetrio López.

**El desescalamiento del conflicto, la justicia transicional, la comisión de la verdad y género,** fueron los temas puntuales sobre los que la red construyó sus propuestas para las conversaciones de paz que se llevan a cabo en la Habana.

**“Partimos de una constatación, que para nosotros las víctimas no ha existido justicia penal,** es más, el aparato judicial nos ha perseguido y hasta encarcelados injustamente, y ha contribuido en muchas ocasiones a la injusticia socio ambiental”, informaron en el comunicado.

Estas propuestas y conclusiones partieron de la diferencia de responsabilidades históricas, éticas y políticas del Estado, empresarios y políticos e iglesias y las guerrillas; asumiendo de que se deben conocer y reconocer las responsabilidades frente a las víctimas, pero también, a la dignidad de todos y todas, incluyendo a los combatientes. [(Ver comunicado completo)](https://comunidadesconpaz.wordpress.com/2015/02/27/declaracion-asamblea-nacional-de-red-de-comunidades-construyendo-paz-en-los-territorios-conpaz/)
