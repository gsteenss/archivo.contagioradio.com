Title: Asciende a 10 mil el número de prisioneros políticos en Colombia
Date: 2015-02-10 21:50
Author: CtgAdm
Category: Judicial, Política
Slug: 10-mil-prisioneros-politicos-en-colombia
Status: published

###### Foto: Razón 

<iframe src="http://www.ivoox.com/player_ek_4065431_2_1.html?data=lZWjl5mXdY6ZmKiak5WJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLXysrbxsqPpYylkZDay9GPqc2fz4qwlYqmhc7Z09SYxsqPtNPd1M7c0MrWs9Sf2pDd0dGJh5SZoqnhy8jTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jhon León, Corporación Solidaridad Jurídica] 

En el año 2010 las organizaciones acompañantes y los propios prisioneros políticos entregaron un documento en el que se estimaba la existencia de 7.500 prisioneros por motivos políticos. Según un informe de la Fundación para la Solidaridad y la Defensa, para el **2015 este número habría crecido a los 10 mil**.

Sin embargo, realizar un censo sobre la población carcelaria en Colombia, identificando particularmente a prisioneros y prisioneras políticas es una tarea ardua -según las organizaciones de Derechos Humanos- en razón del **desconocimiento y la negación que hay por parte del Estado colombiano** de la existencia de este grupo poblacional en los centros penitenciarios.

En términos porcentuales, la mayor parte de las personas judicializadas, procesadas y condenadas por el delito de Rebelión corresponden a **líderes y lideresas sociales, principalmente activistas agrarios**, estudiantes, y miembros del movimiento social y popular en general que están en las cárceles por discernir del régimen político colombiano.

Le sigue el grupo de personas que habitaba en zonas de conflicto, y que fueron, o son judicializadas por "colaborar con estructuras armadas", ser presuntos financiadores, logísticos, entre otros; y finalmente el grupo lo completan alrededor de 3.500 prisioneras y prisioneros de guerra, que hacen parte de estructuras armadas insurgentes, como las guerrillas de las **FARC, el ELN y el EPL**.

Esta particular composición es resultado de la **Sentencia C-456 de 1996 de la Corte Constitucional, que desnaturalizó y desapareció** en la práctica el **delito político**, al punto de que sea imposible en Colombia argumentar ante los tribunales que las conductas de los imputados corresponden lógicas ideológicas.

Para el Movimiento Nacional Carcelario y las organizaciones de Derechos Humanos acompañantes, la realidad colombiana de las prisiones refleja la ausencia de una serie de garantías reales para el **ejercicio de la oposición política**, y la desigualdad en términos de oportunidades para la gran mayoría de hombres y mujeres que se encuentran en centros penitenciarios del país, motivo por el cual, este tema debería hacer parte de la **agenda para la paz tanto en la Mesa de Diálogos de La Habana como de la sociedad colombiana en general.**

La **Corporación Solidaridad Jurídica** señala que las experiencias de paz a nivel internacional han demostrado que para que conflictos armados internos llegaran a buen termino, fue necesario pasar por procesos de amnistía que dieran libertad a las y los prisioneros políticos.

El Movimiento Nacional Carcelario y las organizaciones de Derechos Humanos que acompañan a los prisioneros políticos en Colombia buscan darse cita en el **II Encuentro Nacional e Internacional de la Coalición Larga Vida a las Mariposas**, **del 20 al 22 de marzo en el Centro de Memoria Histórica**, para desarrollar escenarios de construcción de opinión, propuestas de coordinación y acción que aporten a la construcción de la paz con justicia social desde y para la población carcelaria en Colombia.
