Title: Segundo encuentro por la Verdad: un reconocimiento a familias de personas desaparecidas
Date: 2019-08-23 18:28
Author: CtgAdm
Category: DDHH, Nacional
Tags: comision de la verdad, Desaparición forzada, MOVICE, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: encuentro-verdad-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/desaparecidos-dia-e1504117350695.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Movice Antioquia] 

Del 26 al 28 de agosto se realizará en Pasto, Nariño el **Segundo Encuentro por la Verdad**, organizado por la **Comisión de la Verdad y la Unidad de Búsqueda de Personas dadas por desaparecidas (UBPD)** el que tendrá como foco el **"reconocimiento a la persistencia de las mujeres y familiares que buscan personas desaparecidas”**, rindiendo un tributo a 40 organizaciones de familiares dedicadas a la búsqueda de sus seres queridos.

Algunas de estas organizaciones acumulan más de 40 años realizando esta labor con la esperanza de saber qué pasó y dónde están sus seres queridos, la alianza entre las dos entidades se da tras un  proceso de reconocimiento que inició en cinco ciudades del país y que culminará con un acto solemne el miércoles 28 de agosto,  en el Teatro Javeriano de Pasto.

Los días previos a esta ceremonia, se realizarán conversatorios alrededor de experiencias internacionales y nacionales de búsqueda además de eventos culturales para dignificar la labor de mujeres, familiares, organizaciones, pueblos y comunidades.

Para los organizadores de este Segundo Encuentro  por la Verdad, **“Quienes buscan a sus seres queridos no temen llegar a lugares inhóspitos; exigen respuestas sin importar poner en riesgo su integridad, y jamás desisten”.** (Le puede interesar:[Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/))

> [\#ReconocemosSuBúsqueda](https://twitter.com/hashtag/ReconocemosSuB%C3%BAsqueda?src=hash&ref_src=twsrc%5Etfw) | Encuentro por la Verdad en [\#reconocimiento](https://twitter.com/hashtag/reconocimiento?src=hash&ref_src=twsrc%5Etfw) a la persistencia de mujeres y familiares que buscan personas desaparecidas.
>
> 27 y 28 de agosto | [\#Pasto](https://twitter.com/hashtag/Pasto?src=hash&ref_src=twsrc%5Etfw), [\#Nariño](https://twitter.com/hashtag/Nari%C3%B1o?src=hash&ref_src=twsrc%5Etfw) [@ubpdbusqueda](https://twitter.com/ubpdbusqueda?ref_src=twsrc%5Etfw) y [@ComisionVerdadC](https://twitter.com/ComisionVerdadC?ref_src=twsrc%5Etfw) <https://t.co/9HbtmUqD9Y>
>
> — Unidad de Búsqueda UBPD (@ubpdbusqueda) [August 23, 2019](https://twitter.com/ubpdbusqueda/status/1164703255793999875?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### ¿Cuántos personas desaparecidas registrados hay en Colombia?

Según el Observatorio de Memoria y Conflicto del Centro Nacional de Memoria Histórica, consultado por la UBPD para realizar su trabajo, **las personas víctimas de desaparición forzosa son 83.036 mientras que por secuestro se conocen 38.357 casos**, de los cuales  solo 333 fueron registrados como personas desaparecidas.

Por otro lado, los desaparecidos por reclutamiento de menores son de 17.895, de los cuales solo 154 están reportados como personas desaparecidas. Sin embargo, esta cifra puede compararse con la cantidad de habitantes en ciudades como Rionegro, Antioquia o Ipiales, Nariño, un total que supera las 120.000 personas.(Le puede interesar:[Tres conclusiones del Diálogo para la No Repetición sobre asesinato de líderes sociales](https://archivo.contagioradio.com/tres-conclusiones-del-1er-dialogo-para-la-no-repeticion-sobre-el-asesinato-de-lideres-sociales/))

Este Segundo Encuentro por la Verdad destaca el trabajo de las familias de los desaparecidos, teniendo en cuenta que es un universo desconocido, regido por leyes y procesos que han hecho invisible la situación de miles de víctimas, sumado a esto el miedo que tienen muchos causado por amenazas u otras formas de revictimización, que al final de cuentas dificultan el proceso de denuncia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
