Title: 'Águilas Negras' amenazan a organizaciones y lideres sociales del Cauca
Date: 2016-03-29 16:16
Category: DDHH, Nacional
Tags: Aguilas Negras, Cauca, Paramilitarismo
Slug: panfletos-amenazantes-aguilas-negras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Na 24 

###### 29 Mar 2016 

**En el departamento del Cauca, circula en las calles y correos una serie de panfletos con autoría del grupo paramilitar 'Águilas Negras',** a través de los cuales se amenaza de muerte a defensores de derechos humanos, periodistas, profesores, estudiantes, empleados públicos, campesinos y organizaciones sociales a quienes señalan de guerrilleros, declarándolos objetivo militar al igual que sus colaboradores y familiares.

En estos panfletos, se amenaza a la actual secretaria de gobierno departamental del Cauca, Alejandra Miller, integrantes de la junta patriótica departamental, como a Enrique Senteno, vocero de Marcha Patriótica de Cauca y organizaciones sociales en las que se encuentra ASOINCA (Asociación de Institutores y Trabajadores de la Educación del Cauca), ASPU (Asociación Sindical de Profesores Universitarios), CIMA (Comité de Integración del Macizo Colombiano), ACIN (Asociación de Cabildos Indígenas del Norte del Cauca) y la Ruta Pacifica de las Mujeres, **a quienes se les da una semana para salir de sus territorios, de no ser así, serían asesinados.**

Cristian Delgado, coordinador de Derechos Humanos del movimiento político y social Marcha Patriótica,  señala que semanas atrás se viene denunciando ante las autoridades la presencia paramilitar que existe en el departamento, que en los últimos días ha desatado amenazas, señalamientos y ejecuciones extrajudiciales, como el asesinato del gobernador **William Alarcón, gobernador del resguardo de Río Blanco y el Maricela Tombe, a quien le dispararon en el municipio de El Tambo. Así mismo, en Corinto se registró una masacre en la que grupos paramilitares habrían acabado con la vida de tres personas, con armas de uso privativo de las fuerzas militares.**

Según Delgado, la circulación de estos panfletos amenazantes se da a nivel nacional, **en departamentos como Meta, Antioquia, Putumayo y Santander**, donde se distribuye en espacios públicos, puerta a puerta y vía correo electrónico.

Las organizaciones sociales y defensores de DDHH, solicitan al gobierno colombiano que se reconozca el fenómeno del paramilitarismo junto a sus modos de accionar, con el objetivo de que se combata estas estructuras y se brinden garantías para los líderes sociales.

<iframe src="http://co.ivoox.com/es/player_ej_10972446_2_1.html?data=kpWmmZeYeJehhpywj5WaaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncYammIqwlYqcdcjpytHO1ZCyqcjmwtiSlJyPpc7Zz8bnw9OPpYzj08zO0M7epcTd0NPS1ZDdb83dxcrfx9iPt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[![Panfleto amenazante](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/12939110_10153854141235020_265828809_n.jpg){.aligncenter .size-full .wp-image-21996 width="502" height="856"}](https://archivo.contagioradio.com/panfletos-amenazantes-aguilas-negras/12939110_10153854141235020_265828809_n/)

 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
