Title: Tres diferencias entre disidencias de FARC recorriendo las guerras en Colombia
Date: 2020-04-23 21:28
Author: CtgAdm
Category: Actualidad, Otra Mirada
Tags: Cartel de Sinaloa, Cauca
Slug: tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: disidencias/ Brujula Internacional

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La reciente ola de violencia y asesinatos que se ha presentado en el Cauca, sin ser el único departamento que ha revivido la guerra por el control territorial y el dominio de los cultivos de coca, dejan entrever no solo la lucha entre las disidencias de las FARC, el Cartel de Sinalóa, los grupos paramilitares y el ELN, sino las fricciones entre las estructuras derivadas de la antigua guerrilla como **los frentes 'Dagoberto Ramos', 'Jaime Martínez' y 'Segunda Marquetalia'**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta división ha llevado a cuestionar cuál es el rumbo que han tomado aquellos que no se acogieron al Acuerdo de Paz y cómo se han configurado en los territorios en los que hacen presencia, sino también los intereses que hacen direccionan sus actos en contraste con los que se llaman disidencias en el Cauca. [(Lea también: Asesinan a tres personas en Micay, Cauca)](https://archivo.contagioradio.com/micay-asesinato-tres-personas/?fbclid=IwAR0xe_pBhvJdpr02nfRGZzyWUnH_CsXJ6UMaSszlLUhILdYGKqC0lirXMu0)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para 2017, la ONG International Crisis Group calculaba que unos **1.000 hombres y mujeres seguían en armas**, lo que equivale a un 9% de la antigua guerrilla, mientras que para 2019, según un informe de las Fuerzas Militares registraba 2300 integrantes y 1400 redes de apoyo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Alberto Yepes de la[Coordinación Colombia Europa Estados Unidos (COEUROPA)](https://twitter.com/coeuropa), las disidencias podrían clasificarse por ahora en tres denominaciones, de un lado las denominadas como Nueva Marquetalia, un grupo que estaría al mando de figuras como **Iván Márquez, Hernán Darío Velásquez 'El Paisa', Jesús Santrich y Henry Castellanos Garzón alias "Romaña".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho grupo se apartó de la línea política adoptada por el partido FARC y volvió a la lucha armada "para reemprender un proyecto insurgente con motivaciones políticas", al respecto, Yepes señala que de este grupo no se ha conocido mayor actividad de guerra. [(Le puede interesar: Gobierno también es responsable del regreso a las armas de Iván Márquez)](https://archivo.contagioradio.com/gobierno-tambien-es-responsable-del-regreso-a-las-armas-de-ivan-marquez/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La segunda disidencia sería la del Frente Primero y Séptimo de las FARC vinculada a Gentil Duarte que recoge excombatientes que no se acogieron al acuerdo de paz en Guaviare, Vichada, Guainía, Putumayo y parte del Meta. Este actor tendría una de sus principales actividades en el narcotráfico, cultivos de uso ilícito y las rutas hacia Brasil, sin embargo aún no es fácil de identificar un proyecto político, pese a encarnar un discurso revolucionario.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las disidencias paramilitarizadas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente Yepes alerta sobre una tercera disidencia que actuaría en departamentos como Cauca, Nariño y Putumayo y que abiertamente ha sido vinculada al tráfico de narcóticos y a carteles mexicanos como el de Sinaloa, sometiendo a las comunidades, oponiéndose a proyectos de sustitución voluntaria y en general la implementación del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Incluso antes de la firma del Acuerdo de Paz, se planteaba que algunos grupos tenían un manejo importante de rutas del narcotráfico y que su vocación era rentística, **"el asunto es que con un control militar del territorio tan milimétrico, necesariamente deben entrar en acuerdo con sectores de la Fuerza Pública que controlan las rutas, puertos y faciliten esta actividad"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Muchos de estos grupos además de combatir a las tropas del ELN, y perseguir a las organizaciones sociales, se han convertido en los principales enemigos de los líderes sociales del país, por lo que Yepes concluye que su naturaleza, sus nexos y prácticas políticas los articula a una estrategia paramilitar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Más de 13.000 excombatientes se acogieron a la paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Pese a la fundación del Partido FARC y a la entrega de armas de más de 13.000 excombatientes** que se encuentran en proceso de reincorporación, las disidencias son las que continúan llevando la guerra a las regiones y que permiten que subsista un estigma alrededor de quienes sí se acogieron al acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es una confusión que se genera de manera intencional y que resulta rentable para la estrategia de quienes están detrás del exterminio de los líderes sociales" Asegura Yepes, para el tampoco existe nada más conveniente para los verdaderos responsables que reportar que gran parte de estos asesinatos están siendo perpetrados por las disidencias de las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta situación conlleva varios retos, en especial para el partido político FARC que agrega el defensor, debe darle un cambio a su denominación sin dejar de lado sus ideales de lucha **"en un momento en que el nombre de FARC termina utilizándose para exculpar al Estado y a quienes están detrás de este exterminio",** un acto que aclararía que no son las mismas disidencias que están en los territorios y que saltaron a la lucha política institucional. [(Lea también; La paz no la detiene nadie, afirman excombatientes de FARC)](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otra parte, para Yepes, **la responsabilidad también recae en la Nueva Marquetalia de Iván Márquez, quienes, si deberían demostrar que tienen un proyecto político de transformación social, por ello tienen el reto de demostrar que no tienen que ver con las estructuras** que están asesinando a líderes sociales y que rompieron con el narcotráfico y la extorsión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Si callan sobre los asesinatos de las bases sociales y los exterminios de los movimientos en estas regiones, desdibujan la posibilidad de que sean identificados como una insurgencia que persigue transformaciones en el campo económico y social", lo que abriría las puertas y que sean considerados en un futuro como actores de una nueva negociación el conflicto armado junto a otros grupos como el ELN o el EPL. [(Le recomendamos leer: "El Acuerdo es mucho más grande que unas personas que se apartan")](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
