Title: A los trabajadores de la Universidad Nacional no les han aumentado el salario
Date: 2015-04-13 15:15
Author: CtgAdm
Category: Movilización, Nacional
Tags: estudiante, Ignacio Mantilla, juan carlos arango, mane, paro, salario, sintraunal, sintraunicol, Universidad Nacional
Slug: a-los-trabajadores-de-la-universidad-nacional-no-les-han-aumentado-el-salario
Status: published

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_4347448_2_1.html?data=lZihmZmYfI6ZmKiakpyJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbX1dTfjcnJb83VjLrby9vJttTdxcbRjbPFp8rjz8bZjcbZscbi1cbfh6iXaaK4wpDgw9HFtsrj1JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Carlos Arango, Vocero Comité Pro Mejora Salarial] 

El lunes 13 de abril los y las trabajadoras de la Universidad Nacional de Colombia entraron a paro indefinido en sus sedes de Bogotá, Palmira y Medellín, para exigirle al rector Ignacio Mantilla el aumento de los salarios de manera equitativa.

Según denuncian los trabajadores, en la mesa de negociaciones establecida en septiembre del 2013, el rector se comprometió a hacer un incremento salarial a los trabajadores con menos ingresos y a volver regresar al concurso administrativo aquellos cargos que vienen siendo de libre nombramiento del rector.

19 meses después, ninguno de estos acuerdos se habría cumplido, y los incrementos salariales significativos sólo se habrían dado a los asesores de las directivas, nombradas por ellos mismos; mientras se dejó por fuera a 60 trabajadores del colegio IPARM, 75 trabajadores de mantenimiento, y se le dieron aumentos "pírricos" a trabajadores y trabajadoras de carrera administrativa, señala Juan Carlos Arango, vocero del Comité Pro-Mejora Salarial de la Universidad Nacional.

"Este no es un conflicto diferente, es el mismo conflicto del año 2013", indica Arango. los trabajadores no estarían exigiendo un nuevo aumento salaria, como afirma el rector Mantilla, sino la nivelación acordada que acabe con la inequidad entre trabajadores de concurso y de libre nombramiento. Este se sumaría a los problemas de administración que se le endilgan a la rectoría de la Universidad, como la falta de presupuesto para la contratación docente que se vivió a principio de semestre académico 2015.

El vocero de los trabajadores insiste en que la actitud de este gremio es de diálogo, pero que consideran difícil llegar a acuerdos por la actitud hostil que tiene el rector de la universidad para con sus trabajadores.
