Title: Venta de ETB  será demandada por sindicatos
Date: 2016-05-24 13:11
Category: Judicial, Nacional
Tags: Concejo de Bogotá, ETB, privatizacion etb, Venta ETB
Slug: venta-de-etb-sera-demandada-por-sindicatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Venta-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo ] 

###### [24 Mayo 2016 ]

La venta de la ETB será frenada por los sindicatos, a través de la movilización ciudadana y acciones jurídicas como la demanda que se interpondrá contra Jorge Castellanos, el actual presidente de la compañía, por las decisiones administrativas que ha tomado con el fin de denigrar la imagen de la empresa y motivar a sus usuarios para que se suscriban a otras empresas, cómo asegura William Sierra, presidente de Sintratelefonos.

Sierra denuncia que Jorge Castellanos, puso un marcha un **paro administrativo que frenó las instalaciones de servicios en varias de las residencias**, impidió que los carros transportarán los materiales que permanecen guardados en las bodegas; así mismo, varios de los funcionarios están motivando a los usuarios para que se suscriban a Claro, cuando llaman a pedir asesoría.

De acuerdo con Sierra, se quiere confundir a la comunidad jugando con sus necesidades, diciendo que con el dinero de la venta se construirán 30 colegios, pero lo que no se dice es que 15 de ellos se entregarán en concesión, sabiendo que **en las centrales de la ETB que están desocupadas por el cambio de tecnología se pueden construir estos colegios**. En la central de Bella Vista se edificó una biblioteca, pública en principio, pero que terminó en manos de la Universidad del Bosque.

Sierra insiste en que con 11 votos a favor y 4 en contra, el Concejo aprobó en primer debate la venta de la ETB, sin considerar los argumentos técnicos y financieros presentados por algunos concejales y líderes sindicales que demuestran que la enajenación de la empresa implica **consecuencias negativas no sólo para las finanzas del distrito, sino para los bolsillos de los ciudadanos. **

Los concejales que fueron nombrados para defender a la comunidad, están **defendiendo los intereses de las multinacionales**, asegura Sierra e insiste en que la movilización contra la venta de la empresa se mantiene, al punto de convocar un **gran paro cívico del que participarán todos los sindicatos y sectores sociales**. Así mismo se plantea hacer un cabildo abierto en el Congreso y poner en marcha las acciones jurídicas que en el 98 frenaron la intención del actual alcalde de [[vender la empresa](https://archivo.contagioradio.com/?s=etb+)].

<iframe src="http://co.ivoox.com/es/player_ej_11650654_2_1.html?data=kpajl5WaeZWhhpywj5aZaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncbjdzdHWw9KPl8rZ09fOh5enb7S9r7m%2Fo7mpkKa6sLO8tZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
