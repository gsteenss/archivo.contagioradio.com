Title: Grupos paramilitares ahora se financian con la trata de personas
Date: 2016-07-29 23:39
Category: DDHH, Nacional
Tags: explotación sexual, paramilitares, posconflicto, Trata de personas
Slug: grupos-paramilitares-ahora-se-financian-con-la-trata-de-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/trata-de-personas-e1469852928477.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Infobaires 

###### [29 Jul 2016] 

Este sábado se conmemora el Día Mundial Contra la Trata de Personas, un flagelo que **ocupa el segundo lugar de negocios ilegales más lucrativos en el mundo,** y que en Colombia se presenta en prácticamente todas las regiones del país, debido a la falta de oportunidades laborales, la pobreza, el conflicto armado y la impunidad en la que continua esta grave vulneración de derechos humanos relacionada con la esclavitud y la explotación.

La trata de personas incluso se ha vuelto un negocio más rentable que el narcotráfico, lo que generó que tras la implementación de la Ley de Justicia y Paz ó Ley 975 de 2005, con la que se esperaba la desmovilización de las estructuras paramilitares, ahora los grupos emergentes de estas bandas **se estén financiando de la trata de migrantes y víctimas del conflicto armado, como lo asegura Claudia Quintero, vocera de la Corporación Anne Frank, quien explica que este tipo de delito ocurre bajo la tolerancia de las instituciones.**

**Medellín, Bogotá, Valle, Ipiales, incluso en la región fronteriza del Amazonas se evidencian casos de trata de personas,** usualmente con fines de explotación sexual, "No hay sitio de Colombia en el que no haya casos de trata de personas", afirma Quintero, y agrega que  las alcaldías y gobernaciones deben implementar la normativa existente en contra de la trata de personas, pues **actualmente son pocas las capturas y muchos menos son las condenas, por ejemplo en 2013 no hubo ni una sola condena por este delito.**

Por otra parte, existe también un problema frente a lo naturalizada que se encuentra esta situación entre las y los colombianos, "**somos un país que naturaliza hasta la explotación sexual infantil"**, expresa la vocera de la Corporación Anne Frank. De acuerdo con ella los niños, niñas, y adolescentes son el especial punto de atención para estas mafias, y ahora el internet es una de las principales herramientas para captar víctimas, pues **existen al menos dos mil portales pornográficos donde son explotados niños y niñas colombianos.**

En ese sentido, desde la Corporación se viene trabajando hace años para combatir este delito, y señalan que las leyes no deben quedarse en el papel si no ponerlas en práctica. Además debe empezar a tipificarse las nuevas modalidades de trata de personas como lo es el acoso sexual a través de internet, más conocido como Grooming.

Aunque desde la Corporación Anne Frank no se niegan algunos esfuerzos importantes por parte del gobierno para frenar este flagelo, esto no ha sido suficiente y debe ponerse mayor atención a este delito cuyas víctimas han sido invisibilizadas e incluso revitimizadas, **pese a que se trata de una problemática en la que se debe poner especial atención para la etapa del posconflicto.**

Para mayor información sobre el trabajo de la Corporación Anne Frank visite [www.corporacionannefrank.org](http://www.corporacionannefrank.org/)

<iframe src="http://co.ivoox.com/es/player_ej_12395074_2_1.html?data=kpegm5qUe5Whhpywj5eaaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaTgwtrRy8aPldbdz9nS1NSJdqSfpNTf0tTWpcTdhqigh6eXsoy1z9PSjavWpc_fjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
