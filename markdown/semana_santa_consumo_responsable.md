Title: Semana Santa: una época para pensarse el consumo responsable
Date: 2018-03-28 13:08
Category: Ambiente, Animales, Nacional
Tags: animales en peligro de extinción, Semana Santa, tortuga icotea
Slug: semana_santa_consumo_responsable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: petenenfotos/minube] 

###### [28 Mar 2018] 

En Semana Santa, el tráfico de fauna silvestre, el consumo peces y tortugas y el uso de la palma de cera, hace que la semana destinada a la reflexión y la oración por los demás, se convierta en una semana de sufrimiento y explotación desenfrenada de la fauna y flora del país.

**"El llamado es a respetar la creación de Dios en la que muchos creen"**, expresa Catalina Reyes, integrante de la Plataforma ALTO, quien señala que, por ejemplo, respecto a la  palma de cera, de la cual existen 11 especies en el mundo, las 7 especies que hay en Colombia están en vía de extinción. Por esto, indicó que "el país tiene la responsabilidad con el mundo de no acabar esas especies, la invitación es a no incentivar la compra y venta de palma".

Y es que para esta época las noticias respecto al tráfico y consumo de especies de fauna y flora en vías de extinción aumenta. A inicios de esta semana, las autoridades reportaron que **habían sido incautados 31 bultos de palma de vino y 8 kilos de carne de tortuga icotea.**

### **El viacrucis de la tortuga icotea ** 

Esta última especie es una de las principales víctimas de las celebraciones religiosas de estos días. Se trata de un animal que habita en la Costa Atlántica colombiana, **y fue declarada por la Unión Internacional para la Conservación de la Naturaleza como una especie amenazada.**

Pese a ese panorama, siguen siendo víctimas de los traficantes de animales silvestres, quienes las venden para ser consumidas, lo que conlleva a una muerte cruel y violenta ya que les extrae la poca carne que tienen. Por ejemplo, una tortuga adulta de esa especie, pesa aproximadamente **1.500 gramos, y sólo tiene 20 por ciento de carne en su cuerpo.**

Para, Luz Stella Ordoñez, Sociedad Protectora de Animales de Barranquilla se trata de **“un acto cruel e ilegal y es un delito que castiga** a quien la consuma, trafique y tenga en cautiverio estos animales silvestres”. Ella, califica de “aberrante” la forma como los traficantes matan a la tortuga, con el objetivo de que las personas consuman unos cuantos gramos de carne.

Además de esta tortuga, Catalina Reyes también llama la atención sobre el **consumo de pez mota, la trucha del Magdalena, el atún rojo, las aletas de tiburón, e**species mayormente amenazadas para esta época particularmente en la Costa y Buenaventura.

Los animalistas y ambientalistas recomiendan que quienes estén de vacaciones deben entender que hacer parte de la cadena de tráfico de especies, constituye un delito tipificado en la Ley 599 del 2000 del Código Penal. Así mismo, se puede denunciar estas prácticas con la Policía Ambiental o con la Sociedad Protectora de Animales de Barranquilla.

<iframe id="audio_24927802" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24927802_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
