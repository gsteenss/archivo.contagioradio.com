Title: Noche herida: lucha y berraquera de una abuela desplazada
Date: 2017-03-28 09:45
Author: AdminContagio
Category: 24 Cuadros
Tags: campesinos, colombia, Desplazamiento, Documental
Slug: noche-herida-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/16903454_1432115793465982_9143104658191961603_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Noche Herida 

###### 23 Mar 2017 

El próximo 30 de marzo se estrenará en salas de cine colombianas, el documental  "Noche herida", una película de Nicolás Rincón Gillé, y coproducida por Medio de Contención Producciones, que narra la historia de Blanca y sus nietos, quienes se enfrentan a las condiciones de la marginalidad urbana y del desarraigo en que viven en el barrio Verbenal de Bogotá, capital de Colombia.

La producción corresponde a la parte final de la trilogía "Campo Hablado" de Rincón Gillé, precedida por "En lo escondido" (2007), premio Jori Ivens en el Festival de Cine Real (2007) y "Los Abrazos del Río" (2010), premio Globo de Oro en El Festival de los 3 Continentes. Le puede interesar: [Las mujeres ecuatorianas y el derecho a decidir sobre su sexualidad](https://archivo.contagioradio.com/mujeresviolenciaabortoecuador/)

“No siempre el cine documental consta de entrevistas, voces en off e imágenes de archivo. Buscamos mostrar una historia emotiva, cotidiana, familiar, con navidad, serenatas y momentos de tensión. Al mismo tiempo se ilustran temáticas como el desplazamiento, la importancia de la educación y los conflictos sociales urbanos”, asegura Manuel Ruíz, productor del documental.

El documental, que tendrá pantalla hasta el 3 de abril, ha sido reconocido como Mejor película colombiana en el Festival Internacional de Cartagena de Indias FICCI 2016, con la Mención del jurado de Cinéma du Réel 2016, el premio Corazón de bronce en International Film Festival The Heart of Slavonia 2015 y el Primer Premio MARFICI 2015. además de la Mención del jurado RISC, Marsella  y el Premio documental SCAM, ambos en 2015.

<iframe src="https://www.youtube.com/embed/bF2cisKAN2Q" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Sinopsis: Huyendo del campo, Blanca vive con tres de sus nietos en la frontera de Bogotá. En plena adolescencia, Didier, el mayor, decide abandonarla. A la distancia Blanca trata de protegerlo invocando a las benditas almas mientras refuerza su atención sobre los dos más jóvenes, Camilo y John, por miedo a que se también se pierdan. Es la lucha de una abuela por el futuro de los suyos; una historia milenaria en un contexto contemporáneo de exclusión.
