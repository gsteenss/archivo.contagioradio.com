Title: Oposición buscará evitar construcciones en Embalse San Rafael
Date: 2019-05-08 14:25
Author: CtgAdm
Category: Judicial, Política
Tags: Alcaldía, Bogotá, Enrique Peñalosa, Juez, San Rafael
Slug: oposicion-buscara-evitar-construcciones-en-parque-san-rafael
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Embalse-de-San-Rafael.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Bogotá] 

En días recientes, el juez sexto administrativo del circuito de Bogotá determinó la suspensión de la licitación iniciada por el Distrito para construir un parque en el Embalse San Rafael, en la vía que conecta a Bogotá con el municipio de La Calera. La decisión judicial determinó que era posible determinar una amenaza al ecosistema y la biodiversidad de la zona, dándole la razón a Concejales de oposición que también rechazaron el Proyecto, y esperan poder detenerlo definitivamente.

En el Parque Ecológico San Rafael la Alcaldía abrió un proceso de licitación en enero de este año para la construcción de 11 kilómetros de vías peatonales y para bicicletas, miradores, zona de picnic y de restaurantes; adicionalmente contempla la construcción de un cable aéreo que conecte la ciudad desde la Calle 134 con 7 ma hasta el Embalse. Obras con un costo calculado de 543 mil millones. (Le puede interesar: ["¿Cuáles serían las afectaciones causadas por el Sendero Las Mariposas?"](https://archivo.contagioradio.com/cuales-serian-las-afectaciones-del-sendero-las-mariposas/))

> Enrique Peñalosa detuvo el cable de San Cristóbal, también embolató el de Cerro Norte y los demás, pero quiere atravesar uno turístico para llevarle clientes al “Disneyland” que quiere hacer en el embalse. ?
>
> Medio billón de pesos para otro capricho. [pic.twitter.com/Gqs7nqGIpj](https://t.co/Gqs7nqGIpj)
>
> — Carlos Carrillo ??‍? (@CarlosCarrilloA) [7 de mayo de 2019](https://twitter.com/CarlosCarrilloA/status/1125737185238573057?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **¿Qué se busca con la acción popular?**

Como explicó la concejal Xinia Navarro, la suspensión de la licitación fue posible gracias a una acción popular contra el Distrito impulsada por el Sindicato de Trabajadores y Empleados de Servicios Públicos de Bogotá (SINTRAEMSDES), y apoyada por la bancada del Polo en el Concejo de la Ciudad. En la decisión, el juez determinó que se violó el principio de precaución y se pusieron en riesgo los derechos ambientales de las personas que habitan cerca al proyecto así como su estructura ecológica principal. (Le puede interesar: "[Peñalosa no atiende recomendaciones académicas sobre calidad de aire en Bogotá"](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/))

En ese sentido, para la Concejal, la medida fue acorde al riesgo sobre este espacio, en el que se pretenden construir zonas duras y que podría afectar al embalse que surte de agua a los habitantes de la Capital. Sin embargo, la Concejal indicó que las medidas judiciales continuarán; pues la Alcaldía no se ha retractado aún de la construcción de este Proyecto, y tomando en cuenta la importancia ambiental del Parque (uno de los pulmones verdes más cercanos a Bogotá), los Proyectos que allí se desarrollen deben tener enfoque de turismo sustentable.

<iframe id="audio_35569054" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35569054_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
