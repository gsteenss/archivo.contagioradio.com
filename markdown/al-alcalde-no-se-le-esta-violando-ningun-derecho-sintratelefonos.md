Title: “Al alcalde no se le está violando ningún derecho”: Sintrateléfonos
Date: 2017-06-08 13:14
Category: Nacional, Política
Tags: CNE, Peñalosa, Revocatoria a Peñalosa
Slug: al-alcalde-no-se-le-esta-violando-ningun-derecho-sintratelefonos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/laestampida10-e1496942265519.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Con la Oreja Roja] 

###### [08 Jun 2017] 

La Sala Penal del Tribunal Superior de Bogotá rechazó la acción de tutela presentada por el alcalde Enrique Peñalosa contra del Consejo Nacional Electoral (CNE) por ser inprocedente. Con esta decisión el **proceso de revocatoria contra el mandatario de la capital colombiana sigue en marcha.**

El alcalde de Bogotá consideró que, con el proceso de revocatoria, se le estaban violando sus derechos fundamentales y electorales. Sin embargo, la decisión del Tribunal hizo énfasis en que los **mecanismos de participación ciudadana son derechos fundamentales** adscritos a las democracias participativas. (Le puede interesar: ["CNE debe respetar constitución: Comité de Revocatoria a Peñalosa"](https://archivo.contagioradio.com/la-revocatoria-a-penalosa-continua/))

Para Gustavo Merchán, miembro de Sintratelefonos, organización que ha apoyado la revocatoria del alcalde Enrique Peñalosa, “el fallo del Juez se hizo en derecho porque **al alcalde no se le está violando ningún derecho.** La iniciativa de revocatoria se hizo amparados en la constitución política”.

Si bien algunos sectores de la capital han dicho que un año no es tiempo suficiente para evaluar los alcances de la alcaldía, las organizaciones que apoyan la revocatoria aseguran que **“un año es más que suficiente para que un gobernante demuestre su desarrollo en el cargo y sus capacidades para asumir el mismo”.** En Bogotá y según Merchán, “Peñalosa le ha mentido a la ciudadanía y por eso un millón de personas han apoyado el proceso de revocatoria”. (Le puede interesar: ["Enrique Peñalosa: el alcalde del conflicto"](https://archivo.contagioradio.com/enriquepenalosarevocatoria/))

**Insatisfacción ciudadana supera el 90%**

Para Merchán, “Peñalosa no ha cumplido con su plan de gobierno y la insatisfacción ciudadana supera el 90%. Esto nos dio la posibilidad de iniciar un proceso de revocatoria del mandato”. Según el Comité de Revocatoria, **"Peñalosa nunca dijo en su plan de gobierno que iba a vender la ETB,** tampoco dijo que iba a vender las acciones de la empresa de acueducto y la de energía ni que iba a acabar con los auxilios de los menos favorecidos".

Merchán afirmo que el mandatario bogotano “ha cerrado 51 comedores comunitarios y en varias oportunidades le ha mentido a la ciudadanía sobre la veracidad de sus estudios”. A esto se suma el hecho, que relata Merchán, sobre **la politización del Concejo de Bogotá** "quien aceptó la venta de la ETB sin que estuviera dentro del plan de gobierno". (Le puede interesar: ["Con movilizaciones y acciones legales defenderán revocatoria contra Peñalosa"](https://archivo.contagioradio.com/mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa/))

Las organizaciones que defienden la revocatoria, aseguran que los mecanismos de participación deben ser respetados puesto que son mecanismo de control político y **“es el pueblo quien decide si un gobernante termina o no su mandato”.**

<iframe id="audio_19158357" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19158357_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
