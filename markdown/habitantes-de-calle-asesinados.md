Title: Más de 4 mil habitantes de calle han sido asesinados en 10 años
Date: 2018-12-13 17:52
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asesinatos, Bogotá, habitante de calle, Temblores
Slug: habitantes-de-calle-asesinados
Status: published

###### [Foto: Publimetro] 

###### [13 Dic 2018] 

Ser habitante de calle en Antioquia, Valle del Cauca, y especialmente Bogotá, significa estar sometido a golpes, maltrato e indiferencia; sin contar que **para más de 4 mil de ellos, se ha traducido en la muerte**. Así lo revela el informe "Los N.N." (Nunca nadie) de la ONG Temblores, con el que buscan llamar la atención sobre la grave situación de derechos humanos que padece esta población.

Los Nunca Nadie, pretende mostrar la realidad de quienes viven en esta condición de ciudadanía pero son tratados como si no lo fueran. Un informe que se construyó a partir de la recolección de datos de la Fiscalía, la Policía Nacional y del Instituto de Medicina Legal, correspondiente al periodo entre **2007 y 2017**. Informaciones cruzadas con testimonios y entrevistas semi-estructuradas, con las que buscaron contrarrestar el sub-registro de los datos oficiales, así como trabas institucionales para acceder a los mismos.

### **Al menos un habitante de calle** [[**fue**]]** asesinado cada día durante los últimos 10 años** 

Los investigadores encontraron que en muchas de las visitas de campo y entrevistas realizadas en Bogotá, se evidenció que **los habitantes de calle sufren doble proceso de desplazamiento**: el primero, causado por efectos del conflicto armado; y el segundo, al llegar a Bogotá y no poder establecerse en ningún sitio; sumado a esto, viven diariamente con el temor a la muerte.

Los datos obtenidos de la Policía y la Fiscalía, muestran que **a lo largo de los 10 años documentados, fueron asesinados 4.176 habitantes de calle**, la mayoría de ellos hombres (92%). Un 80% de casos ocurrieron en las calles, con mayor frecuencia los fines de semana, y en altas horas de la noche o en la madrugada; hechos que para los investigadores reflejan que fueron asesinados donde suelen dormir, en horas en que no hay volumen de tránsito en las calles.

Según el reporte de Medicina Legal, **el 54% de los homicidios se realizó con arma de fuego, el 34% con arma blanca y el restante con otras armas**; situación que para la ONG representa una alerta, puesto que las armas de fuego están, en su mayoría, en poder de la Fuerza Pública. De forma paralela a esta cifra, llama la atención que en el periodo de estudio, mientras en Colombia se redujeron los homicidios en un 30,7%, el mismo flagelo contra habitantes de calle se incrementó en un 138%.

### **Bogotá, Antioquia y Valle del Cauca, territorios peligrosos para los habitantes de calle** 

Los lugares en los cuales se presenta la mayor cantidad de casos de violencia física son Antioquia (599), Valle del Cauca (411) y Bogotá (7.868); de igual forma, **son estos espacios en los que se registran el mayor número de homicidios: 495, 740 y 1.175, respectivamente**. (Le puede interesar: ["En diciembre se celebrará la vida de los h. de calle"](https://archivo.contagioradio.com/habitantes-de-calle/))

**Según Medicina Legal, se han registrado 11.031 casos de violencia física**; mientras que la Policía registra solo 4.083 casos. La diferencia, explican los investigadores, puede relacionarse con que las víctimas de maltrato acuden a Medicina Legal para valorar sus heridas, pero debido a las posibilidades de re-victimización, y la desconfianza en los órganos de justicia, desisten de realizar una denuncia.

### **Bogotá mejor para todos (menos para los habitantes de calle)** 

Según el Censo del Departamento Administrativo Nacional de Estadística (DANE) de 2017, **en Bogotá hay 9.538 habitantes de calle**, población que representa cerca del 1% total de la ciudad, todos ellos con los mismos derechos ante la Ley; no obstante, como lo evidencian las cifras del informe, es el lugar más peligroso del país para vivir en esta condición.

De acuerdo a las entrevistas y testimonios recogidos por los investigadores, **Los Martires, La Candelaria, Santa fé, San Cristóbal y Usaquén son las localidades donde se sienten más vulnerados por la policía**; muchos de los relatos recogidos tienen que ver con golpizas por parte de los uniformados, uso abusivo de las tonfas (bolillos) así como de las esposas.

Al maltrato físico se suma la persecución de la que son víctimas, muestra de ello fue la intervención al Bronx en mayo de 2016; cuando bajo el argumento de acabar con el peligro para menores de edad, la Alcaldía de Bogotá decidió “intervenir” el lugar conocido como la L. Dicha acción provocó el desplazamiento forzado de quienes dormían en la zona, incluso con denuncias de traslados a lugares fuera de Bogotá en camiones, y la fragmentación de las estructuras expendedoras de drogas hacia los alrededores del lugar.

### **Se necesitan un enfoque de derechos humanos para tratar a esta población** 

Entre las conclusiones del informe, los investigadores señalan la evidente persecución policial contra los habitantes de calle; **el aumento de homicidios, muchos de ellos premeditados dadas las circunstancias en las que ocurren**; y la impunidad para investigar, juzgar y sentenciar a quienes cometen estos delitos. (Le puede interesar: ["Fin del Bronx no acaba con la violencia contra h. de calle"](https://archivo.contagioradio.com/fin-del-bronx-no-acaba-con-la-violencia-contra-habitantes-de-calle/))

Por esta razón, Temblores pide a las instituciones del Estado, la Fuerza Pública, los entes investigadores y quienes tienen que velar por la garantía de los derechos constitucionales, **que se trate a esta población con el enfoque de derechos humanos en los procedimientos**. Esto significa garantizar su acceso a la justicia, la salud, la educación, la libre movilidad, así como a una vida digna y libre de violencias.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
