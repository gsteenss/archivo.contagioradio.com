Title: 4 líderes afrocolombianos asesinados en menos de un mes
Date: 2017-01-19 15:10
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Emilsen Manyoma, Joe Javier Rodallega, Juan de la Cruz Mosquera, Moisés Mosquera, paramilitares en buenaventura, Red CONPAZ
Slug: 4-lideres-afrocolombianos-asesinados-en-menos-de-un-mes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radiomacondo] 

###### [19 Ene 2017] 

El Comité por la Defensa de los Derechos de las Víctimas de Bojayá, denunció mediante un comunicado la impunidad frente al asesinato de 4 líderes afrocolombianos, a manos de grupos paramilitares, **por su trabajo en la defensa de los derechos humanos, la vida y el territorio.**

En el comunicado, el Comité manifiesta indignación frente a los asesinatos de Emilsen Manyoma, lideresa del Bajo Calima e integrante de la red CONPAZ, su esposo  Joe Javier Rodallega,[Moisés Mosquera y su padre Juan de la Cruz Mosquera,](https://archivo.contagioradio.com/parmilitares-choco-asesinato/) líderes de la comunidad de la cuenca del Río Salaquí, **todos defensores y denunciantes de la presencia e intereses de grupos paramilitares en la región**. Le puede interesar: [Asesinan a lideresa de CONPAZ y a su compañero en Buenaventura.](https://archivo.contagioradio.com/asesinan-a-lideresa-de-conpaz-y-a-su-companero-en-buenaventura/)

También expresan que existe una preocupación por todas las comunidades del río Atrato “que siguen expuestas a las actuaciones de estos grupos paramilitares, por lo cual exhortamos al Presidente de la Republica y a todas las autoridades del Gobierno Nacional **que asuman labores efectivas de prevención de violaciones de los Derechos Humanos”.**

Por último, en la misiva el Comité exige al Estado colombiano que **“demuestre su auténtica voluntad de Paz, pues estos hechos se suman a las decenas de líderes sociales que sistemáticamente fueron asesinados durante el año 2016** (…) esperamos que las autoridades competentes apliquen justicia y detengan esta oleada de crímenes contra los defensores de Derechos Humanos, líderes comunitarios y trabajadores por la paz”.

###### Reciba toda la información de Contagio Radio en [[su correo]
