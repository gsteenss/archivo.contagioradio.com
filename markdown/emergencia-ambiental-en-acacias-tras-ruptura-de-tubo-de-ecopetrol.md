Title: Ruptura de tubo de Ecopetrol generó emergencia ambiental en Acacias, Meta
Date: 2019-02-01 14:17
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Acacías, Cormacarena, Ecopetrol, Meta
Slug: emergencia-ambiental-en-acacias-tras-ruptura-de-tubo-de-ecopetrol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-02-at-9.18.11-AM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Suministrada] 

###### [1 Feb 2019] 

Las comunidades de Acacias, Meta denunciaron una emergencia ambiental que se dio tras  la ruptura de un tubo de vertimiento de la empresa petrolera Ecopetrol en la vereda La Unión, donde **la fuga contaminó al menos de 900 metros de humedales**.

Según un comunicado de Ecopetrol, la empresa activó un plan de contingencia después que se diera a conocer la ruptura de una línea de flujo de 36 pulgadas, que transportaba "agua de vertimiento tratada y libre de hidrocarburos" de la Estación Acacías al Río Guayuriba en la mañana del 27 de enero. De acuerdo con información proveniente de Ecopetrol, fue suspendida la operación del tubo para detener la fuga y se instaló un nuevo caño.

Al respecto, Alirio Virguez, representante legal de la Cooperativa Coopesca, denunció la demora de Ecopetrol para reaccionar a la emergencia ambiental dado que la empresa petrolera dejó trascurrir **más de nueve horas después de que el dueño del predio notificó a la empresa de la fuga** para comenzar la reparación de la tubería.

Además, Virguez sostuvo que al visitar el prado afectado, integrantes de las comunidades determinaron que los tubos de residuos sí contienen petroleo crudo, al contrario de las declaraciones de Ecopetrol. Virguez expresó preocupaciones sobre los efectos ambientales que podría generar esta fuga dado que el agua de residuos quemó el pasto y fue absorbido por el suelo.

![WhatsApp Image 2019-02-01 at 12.48.17 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-01-at-12.48.17-PM-800x501.jpeg){.alignnone .size-medium .wp-image-60942 width="800" height="501"}

### **La respuesta de las autoridades ambientales** 

Virguez afirmó que esta no es la primera vez que estalla el mismo tubo de vertimiento de Ecopetrol. El año pasado, también se reportó una fuga de este tubo en otra vereda de Acacías. Además, manifestó las preocupaciones de las comunidades sobre la posible contaminación del Río Guayuriba.

Según los pescadores, se ha documentado casos de mortandad de peces y de contaminación de peces con crudo, que Virhuez afirmó sabían tanto a petroleo que no se podrían comer. Sin embargo, el líder social manifestó que faltan estudios académicos para determinar el efecto verdadero de las actividades de Ecopetrol en el río.

Estos hechos fueron denunciados a la Corporación para el Desarrollo Sostenible del Área de Manejo Especial de La Macarena (Cormacarena), sin embargo, no recibieron respuesta de la autoridad ambiental. "Uno llega con las denuncias a Cormacarena y de ahí no pasa nada," afirmó Virguez.

Ante estos últimos hechos, el dueño de la finca afectada estaría realizando un denuncia formal ante Cormacarena, la Fiscalía General de la Nación y la Procuraduría General de la Nación.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
