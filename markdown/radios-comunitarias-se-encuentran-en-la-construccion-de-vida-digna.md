Title: Radios comunitarias se encuentran en la construcción de vida digna
Date: 2015-09-10 14:38
Category: Movilización, Nacional
Tags: comunicación alternativa, comunicación popular, Ley antimonopolio, medios populares, Radio comunitaria
Slug: radios-comunitarias-se-encuentran-en-la-construccion-de-vida-digna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/radio-comunitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Culturasurvival.org 

<iframe src="http://www.ivoox.com/player_ek_8300619_2_1.html?data=mZidkpuVfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlsLYytTgjcjTsdbiytnO1M7Ft4znxpDS0MjZqc%2Fo08bbjcrSb83VjMjc0NjYttbXxM6SpZiJhpTijMmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Willie Oquendo] 

###### [10 sep 2015] 

[Se realizó en Bogotá, en el barrio el Dorado de la localidad de Santa Fé, el encuentro de radios comunitarias que tuvo como objetivo **intercambiar experiencias de Medellín, Neiva y Bogotá** frente a la transformación y reconfiguraciones de los tejidos sociales, desde la radio comunitaria.]

Willie Oquendo organizador del evento indicó que son 12 experiencias las que participaron de este escenario con las cuales se buscaba “**potenciar esos trabajos como posibilidad, no solo para escuchar otras versiones y opciones, sino para organizarse entorno a medios de comunicación distintos**”.

Con este evento también busca que estas experiencias se articulen en una agenda que permita “**hacer una Colombia distinta, un barrio distinto, apostándole para que los jóvenes encuentren otra posibilidad de seguir soñando este país**” dice Oquendo.
