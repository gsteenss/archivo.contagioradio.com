Title: Sin argumentos sólidos legalizan captura de 13 líderes sociales
Date: 2015-07-10 12:43
Category: DDHH, Entrevistas
Tags: Colombia Informa, congreso de los pueblos, David Uribe, ELN, Explosiones en Bogotá, Jóvenes capturados
Slug: sin-argumentos-solidos-legalizan-captura-de-13-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11692702_1151561871525593_4408124755510560296_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_4762253_2_1.html?data=lZyjlJeZd46ZmKialJ6Jd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjxMrg0ZDHs8%2Fo08aYk5iProa3lIqvldvJssbnjIqylIqcdIatpM3OjdjNqNCfzoqwlYqlddSf0trSjdrSb87jz9nOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Uribe, abogado de la Comisión de Justicia y Paz] 

###### [10 Jul 2015] 

En una maratónica jornada de audiencias y pese a los argumentos expresados y argumentados por la defensa el **juzgado 72 de garantías declaró legales los allanamientos realizados el pasado martes 7 de Julio en la ciudad de Bogotá.** Hacia las 3 de la madrugada de hoy, y **después de vencidos los términos**, también se legalizó la captura de 13 personas, acusadas de participar en las explosiones de la semana pasada en Bogotá.

Según uno de los abogados de los jóvenes detenidos, **los allanamientos tenían 24 horas para ser legalizados, y las capturas 36 horas** que se cuentan a partir del momento mismo de la captura. La defensa de los jóvenes argumentó que **no hubo elementos suficientes para realizar tanto los allanamientos como las capturas**. Un allanamiento se debe hacer cuando hay elementos suficientes y para buscar material que se sabe que existe y no para ver que se encuentra, señala el defensor.

Por otra parte y de acuerdo a los elementos presentados por la fiscalía, y a los cuales no se puede hacer referencia directa por la reserva de la audiencia, **ninguno de los argumentos tiene que ver con los hechos que en principio se presentaron en los medios de información**, es decir, como responsables de las explosiones de la semana pasada en la ciudad de Bogotá. Vea también [Fiscalia no tendría pruebas de responsabilidad de jóvenes capturados en Bogotá.](https://archivo.contagioradio.com/fiscalia-no-tendria-pruebas-de-responsabilidad-de-jovenes-capturados-en-explosiones-en-bogota/)

El abogado señala que ese proceso no es un montaje sino una manipulación mediática, además **ni el presidente, ni la fiscalía, ni la policía pueden salir a los medios a decir lo que dijeron puesto que se viola el derecho a la presunción de inocencia** y se ejerce una **presión muy fuerte al juzgado** que debió dejar en libertad a los jóvenes por vencimiento de términos en la noche de ayer. Pese a los argumentos de la defensa el juzgado no argumentó las razones por las que se violaron los términos perentorios.

Hacia las 2 de la tarde de este viernes se reanudará el juicio con la audiencia de imputación de cargos y la petición de la medida de aseguramiento. Según el abogado no hay muchas herramientas de la defensa más que información acerca de los delitos que se imputan a los jóvenes que serían **rebelión, terrorismo y concierto para delinquir**. [Vea también Estos son los perfiles de las personas capturadas por explosiones en Bogotá](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/)

Adicionalmente, el abogado resaltó que las diversas manifestaciones de solidad frente a los juzgados se ha escuchado y ha sido muy reconfortante, tanto para los jóvenes como para los defensores y defensoras que han asumido el proceso.
