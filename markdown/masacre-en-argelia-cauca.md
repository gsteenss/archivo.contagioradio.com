Title: Conmoción en Argelia, Cauca por asesinato de siete personas
Date: 2018-07-03 17:37
Category: DDHH, Nacional
Tags: Argelia, Cauca, ELN, masacre
Slug: masacre-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/masacre-argelia-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Twitter 

###### 03 Jul 2018 

En la madrugada de este martes fueron encontrados siete hombres sin vida en el corregimiento **Deziderio Zapata del municipio de Argelia**, departamento del Cauca, los cuerpos habrían sido dejados en este lugar por dos camionetas.

La masacre fue conocida por pobladores de la vereda que encontraron al grupo de hombres tirados en el suelo con múltiples impactos de bala, según la versión del **Gobernador de Cauca Óscar Rodrigo Campo**, los hombres habrían sido asesinados en cercanías del municipio Tambo y luego trasladados a ese lugar.

Una de las hipótesis planteada por el mismo gobernador, es que los hombres se habrían encontrado con miembros del frente Jose María Becerra del ELN. De igual forma el mandatario aseguró que esta por establecerse si pertenecían a grupos disidentes de las FARC o su presencia estaba relacionada con actividades de narcotráfico.

Las autoridades y pobladores desconocen el nombre de las víctimas y del grupo que habría ejecutado está acción. Cabe recordar que hace una semana los [pobladores de Argelia denunciaron que el autodenominado grupo Comando Popular de Limpieza](https://archivo.contagioradio.com/panfleto-limpieza-social-en-cauca/) (CPL) a través de un panfleto amenazó con realizar una limpieza social en el municipio de Argelia, Cauca.

Del mismo modo, en el mes de febrero tres personas fueron asesinadas y nueve heridas luego que un grupo armado con capuchas y armas largas incursionaron en en el caserío del corregimiento y dispararon contra la población que se encontraba participando de las fiestas de carnaval en la plaza principal. [Incursión de tipo paramilitar deja tres muertos y nueve heridos en Argelia, Cauca](https://archivo.contagioradio.com/incursion_armada_paramilitar_argelia_cauca/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

------------------------------------------------------------------------
