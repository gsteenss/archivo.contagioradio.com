Title: Cerca de 900 organizaciones sociales y de DDHH llaman a pactar un cese bilateral
Date: 2015-05-29 17:12
Category: Otra Mirada, Paz
Tags: cese unilateral de las FARC, Conversaciones de paz de la habana, Cuba, FARC, Frente Amplio por la PAz, Juan Manuel Santos, Noruega
Slug: cerca-de-900-organizaciones-sociales-y-de-ddhh-llaman-a-pactar-un-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/DDHH-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

En un comunicado publicado este viernes, 840 organizaciones sociales y plataformas de Derechos Humanos, respaldaron las conversaciones de paz que se desarrollan en La Habana Cuba, entre el gobierno de Colombia y la guerrilla de las FARC-EP. En el comunicado, se insiste en la necesidad de que se **pacte un cese bilateral del fuego, como mecanismo para impulsar el proceso de paz y des escalar el conflicto.**

Para las organizaciones sociales, el gobierno nacional y en especial el Presidente Santos debe ser fiel al mandato de los colombianos que lo respaldaron en las elecciones “**Santos fue elegido para la paz, no para la guerra”** afirman los voceros, quienes reclaman **la lealtad por parte del presidente** que debería generar las condiciones reales para pactar el cese bilateral.

El llamado a la guerrilla de las FARC-EP es perseverar en la mesa de conversaciones ***“en contra del deseo de sectores de poder que pretenden bombardear el proceso***”. También afirman que al pueblo colombiano se le agota la paciencia para que el gobierno de muestras reales de querer conseguir la paz.

Las organizaciones hicieron un llamado a la sociedad en general “*a pronunciarse en favor del des escalonamiento de la confrontación y del cese bilateral al fuego*” y así mismo respaldaron el reciente comunicado de los países garantes, **Cuba y Noruega** que piden acelerar las conversaciones para pactar el cese al fuego y resaltaron que la labor de los países garantes es precisamente **garantizar la permanencia de las conversaciones de paz.**

A continuación el comunicado completo...

[Llamado de 870 Organizaciones a Proseguir Las Conversaciones de Paz y a Pactar El Cese Bilateral Al Fuego 2...](https://es.scribd.com/doc/267066124/Llamado-de-870-Organizaciones-a-Proseguir-Las-Conversaciones-de-Paz-y-a-Pactar-El-Cese-Bilateral-Al-Fuego-29052015 "View Llamado de 870 Organizaciones a Proseguir Las Conversaciones de Paz y a Pactar El Cese Bilateral Al Fuego 29052015 on Scribd")

<iframe id="doc_54395" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/267066124/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
