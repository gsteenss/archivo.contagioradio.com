Title: En Fotos: Así se vivió la firma del acuerdo de paz
Date: 2016-11-24 16:41
Category: Galerias
Slug: asi-se-vivio-la-firma-del-acuerdo-final
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 24 Nov 2016 

Hombres y mujeres acompañaron con banderas blancas, sombrillas y bombas de colores, la firma del acuerdo de paz entre la guerrilla de las Farc y el Gobierno Nacional. La cita fue en la Plaza de Bolívar a tan solo media cuadra del Teatro Colón, lugar donde se  selló con la firma del presidente Juan Manuel Santos y el líder máximo de la guerrilla Rodrigo Londoño, el acuerdo final para la terminación del conflicto.

Le puede interesar: [Estos son los retos de implementación del acuerdo de paz ](https://archivo.contagioradio.com/estos-son-los-retos-de-implementacion-del-acuerdo-de-paz/)

\

###### Fotos: Gabriel Galindo/Contagio Radio 
