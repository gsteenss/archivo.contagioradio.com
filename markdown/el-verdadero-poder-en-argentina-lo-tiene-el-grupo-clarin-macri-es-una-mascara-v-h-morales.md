Title: "El verdadero poder en Argentina lo tiene el grupo Clarín, Macri es una máscara": V.H. Morales
Date: 2016-01-12 15:30
Category: El mundo, Otra Mirada
Tags: Argentina, ley de medios Argentina, Libertad de prensa Argentina, Macri
Slug: el-verdadero-poder-en-argentina-lo-tiene-el-grupo-clarin-macri-es-una-mascara-v-h-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/VHm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: urgente24.com 

##### 12 Ene 2016 

El locutor, periodista y escritor uruguayo Víctor Hugo Morales, despedido el día lunes de la radio Continental de Argentina, manifestó a través de su cuenta en la red social twitter su malestar por el no reconocimiento en la injerencia del presidente Mauricio Macri, sobre la decisión que lo aleja de la radio en la que trabaja desde el año 1987.

La reacción del comunicador se dio tras las respuestas dadas ante la prensa por el mandatario argentino quien aseguro que “desde su gobierno no opinan de periodistas”, pero luego el mismo Macri afirmó que Morales se había convertido en un 'fanático kirchnerista', discurso que en criterio del periodista fue construido por Hector Magnetto, director ejecutivo del grupo Clarín, con quién sostuvo un pleito legal en 2014.

> En conferencia [@mauriciomacri](https://twitter.com/mauriciomacri) dijo q desde su gobierno no opinan de periodistas y luego dijo q yo me convertí en un "fanático kirchnerista"
>
> — Víctor Hugo Morales (@vh590) [enero 12, 2016](https://twitter.com/vh590/status/686908333601779712)

> Eso q dice [@mauriciomacri](https://twitter.com/mauriciomacri) es lo que construyó Magnetto sobre mí. Es natural que Macri lo repita. [\#MacriEsMagnetto](https://twitter.com/hashtag/MacriEsMagnetto?src=hash).
>
> — Víctor Hugo Morales (@vh590) [enero 12, 2016](https://twitter.com/vh590/status/686909164908589056)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
En su intervención el presidente agregó que lamentaba la situación por la “larguísima relación” que a su criterio sostenía con Morales y sus declaraciones que para el mandatario son “Falsas” en las que califica su salida del medio de comunicación como una muestra de la censura impuesta por el gobierno y de lo que sucede en una “situación agobiante, asfixiante para la democracia y la libertad de expresión”. (Leer [Los ataques de Macri contra la libertad de prensa en Argentina](https://archivo.contagioradio.com/los-ataques-de-macri-contra-la-libertad-de-prensa-en-argenina/)). Al despido del comunicador se han sumado en las últimas horas los de sus compañeros de estación Matías Canillan, Julian Capasso y Fabiana Segovia, decisiones que han despertado toda suerte de manifestaciones de apoyo, por parte colegas de medios de comunicación y organizaciones sociales y de derechos humanos utilizando la etiqueta \#VHMCensurado y convocando a una marcha hacia la Plaza de Mayo a las 18 hora local.

</p>
> Comunicado de prensa: Nuestro abrazo y nuestra solidaridad con [@vh590](https://twitter.com/vh590) <https://t.co/ThIMfLb3MK> [\#VHMCensurado](https://twitter.com/hashtag/VHMCensurado?src=hash) — Abuelas Plaza Mayo (@abuelasdifusion) [enero 11, 2016](https://twitter.com/abuelasdifusion/status/686583996218798080)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
En entrevista con el canal Telesur, Morales aseguró que el verdadero poder en Argentina lo tiene el grupo Clarín y que Mauricio Macri “es nada más que una máscara”, y lo que vendrá será un periodismo alineado a los intereses del gobierno que ha estado encargado de ajustar "todo lo que a su parecer no está bien en la nación”.

<iframe src="https://www.youtube.com/embed/7_JhQhJ_Hoo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Victor Hugo Morales, considerado por muchos el “relator deportivo por excelencia” de habla hispana, fue despedido el lunes a las 8:30 a.m. antes de iniciar su espacio “La mañana”, decisión que la emisora atribuyó a “reiterados incumplimientos contractuales” y que para el comunicador obedece a la necesidad del medio de mantener la pauta comercial gubernamental.
