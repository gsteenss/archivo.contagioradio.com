Title: Fondo de Tierras contaría con 7 millones de hactáreas de baldíos
Date: 2017-05-25 16:21
Category: Paz, Política
Tags: acuerdo de paz, CSIVI, Fondo de Tierras para la Paz
Slug: fondo-de-tierras-para-la-paz-contara-con-7-millones-de-hectareas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 May 2017] 

A través de un comunicado de prensa la Comisión de Verificación e Implementación de los Acuerdos de Paz (CSIVI) informó que logró llegar a un nuevo acuerdo con el gobierno Nacional para la presentación del decreto por el cual se crea el **Fondo de Tierras y se facilita el acceso y la formalización de la tierra**.

De acuerdo con la vocera de Voces de Paz, Imelda Daza, uno de los puntos más álgidos, que aún no se ha saldado son las tierras baldías que se destinarían para este fondo, sin embargo, uno de los puntos que se acordó es que las **tierras que hagan parte de este fondo estarán cobijadas bajo la figura de Zonas de Reserva Campesina**.

“En esencia lo que se procura es que los terrenos baldíos que existen en el país, sea factible adjudicarlos a familias campesinas para una explotación bajo la modalidad de Zonas de Reserva Campesina, se habla de **7 millones de hectáreas**” afirmó Daza. Le puede interesar: ["Unidad de Restitución de Tierras mete un mico a la ley 1448"](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/)

Además, el gobierno Nacional se comprometió a que a incorporar en el Proyecto de Ley de Tierras, que se va a tramitar vía Fast Track, **el acceso a derechos de uso para campesino y campesinas colombianos**. Le puede interesar: ["Informe sobre acumulación de Baldíos se quedaría sin piso si se aprueba Ley de Tierras"](https://archivo.contagioradio.com/123-mil-hectareas-de-tierras-habrian-sido-adquiridas-irregularmente-contraloria/)

De esta forma continua el trámite del proyecto y su expedición para que sea firmado por el presidente Santos y habilite la posibilidad de iniciar la materialización del Acuerdo de Reforma Rural Integral. De igual forma los indígenas pertenecientes a la ONIC expresaron que retomaran las conversaciones en la mesa permanente de concertación **para continuar con la consulta previa de este proyecto de ley en sus comunidades**.

### **¿Qué es el Fondo de Tierras?** 

El fondo de tierras es una de las partes principales contempladas en el acuerdo del fin del conflicto entre el gobierno y las FARC-EP, en el punto sobre Desarrollo Rural Integral y busca que los campesinos del país tengan acceso a la tierra y se de cumplimiento a la ley 160 de 1994 que prevee la entrega de tierras baldías a los campesinos que las ocupan.

Del fondo, según el acuerdo, harán parte las tierras producto de procesos judiciales de extinción de dominio, las tierras baldías recuperadas a favor de la Nación, las tierras provenientes de la actualización y delimitación de las zonas de reserva forestal, las tierras inexplotadas que serán extinguidas, las tierras adquiridas y expropiadas por motivos de utilidad pública, y las tierras donadas.

El Fondo de tierras también cuenta con unos instrumentos administrativos que son la adjudicación de baldíos de la Nación, la recuperación de tierras, ordenamiento de zonas de reservas forestales, adjudicación de tierras del Fondo Nacional Agrario provenientes de la Dirección Nacional de Estupefacientes y la compra directa.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
