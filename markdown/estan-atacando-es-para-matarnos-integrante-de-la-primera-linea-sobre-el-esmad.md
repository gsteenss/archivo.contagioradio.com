Title: “Están atacando es para matarnos”: integrante de la Primera Línea sobre el ESMAD
Date: 2019-12-11 16:19
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Escudos Azules, ESMAD, Paro Nacional, policia, Primera Línea, UNAL, Universidad Nacional de Colombia
Slug: estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/79535730_10157155146552832_4608684003791208448_o.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/20191211-025_h264_640x360_1Mbps_with_watermark.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Luis Carlos Ayala] 

[El martes 10 de diciembre, en el marco del Paro Nacional, se presentaron fuertes abusos por parte del Escuadrón Móvil Antidisturbios (ESMAD), en las inmediaciones de la Universidad Nacional, sede Bogotá.]

[En medio de una manifestación pacífica en la entrada de la Calle 45 con Carrera 30 de la UNAL, varios agentes del Escuadrón incitaron la violencia al comenzar un ataque con armas aturdidoras y gases lacrimógenos.]

[La Primera Línea, estudiantes que decidieron tomar medidas de precaución frente a la violencia por parte de la fuerza pública, fueron agredidos sin consideración en medio de un enfrentamiento que se postergó hasta las horas de la noche. La agresión con aturdidoras y gases lacrimógenos, junto al nulo respeto por la protesta social, hicieron parte del día.][(Lea también: En veinte años el ESMAD ha asesinado a 34 personas)](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/)

[Para Nao, integrante de los escudos azules, conocidos como la Primera Línea, el desarrollo de la manifestación se desarrolló pacíficamente hasta que llegó el ESMAD.  “Se había realizado una convocatoria para una marcha. No fue la suficiente gente, así que decidimos hacer bloqueos intermitentes en la Calle 26 y la en la Carrera 30. Se quería hacer un poco de ruido y dar visibilización al paro, no generar violencia”. ]

[Sin embargo, los escudos azules y las arengas pacíficas no fueron suficientes para contener la brutalidad del ESMAD, que terminó por dispersar el bloqueo intermitente en la Carrera 30 y obligar a los manifestantes a retirarse. “No respondimos a ninguna de las invitaciones del ESMAD —añade Nao—, pero en un momento se intentaron subir al puente peatonal para tomar posición desde arriba y la gente se los impidió. La vía se bloqueó por un momento y eso dio razón para que nos agredieran con gases, aturdidoras y bolillo”.]

### **El accionar del ESMAD se vuelve cada vez más violento** 

[Uno de los integrantes de la Primera Línea fue agredido por varios miembros del ESMAD, que lo tumbaron al suelo y le propinaron patadas y golpes con bolillo. “No pensamos que el enfrentamiento con el ESMAD fuera tan violento, al declararnos desde el principio como un grupo pacífico”.]

[Dentro de la arremetida por parte del Escuadrón, una persona fue detenida y varias tuvieron heridas abiertas en las piernas por culpa de las aturdidoras. “Lo liberaron después gracias a la intervención de los defensores de derechos humanos” afirmó Nao.][(Lea también: Nace en Colombia la Primera Línea, defensa pacífica contra la represión del ESMAD)](https://archivo.contagioradio.com/nace-en-colombia-la-primera-linea-defensa-pacifica-contra-la-represion-del-esmad/)

[Por otro lado, nunca se dio una respuesta violenta por parte de las personas que hacían parte de la manifestación y los miembros del ESMAD hasta que aparecieron los «capuchos» e inició el tropel. “Cuando se armó el tropel nosotros nos fuimos. No defendemos tropeles, defendemos marchas”.]

\[video width="640" height="360" mp4="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/20191211-025\_h264\_640x360\_1Mbps\_with\_watermark.mp4"\]\[/video\]

### **Lo que les espera a los escudos azules de la Primera Línea** 

[Como mencionó Nao, no esperaban una respuesta tan violenta por parte del ESMAD debido a su carácter pacífico. Sin embargo, después de este hecho han decidido calmar los ánimos, recolectar todas las pruebas, tanto fotográficas como en video, para realizar las consiguientes denuncias. ]

[“Vamos a reunirnos pronto para saber qué tipo de acciones tomar y tratar de recolectar las pruebas para colocar denuncias a la institución como tal. De igual forma, invito a todas las personas que tengan material de los ataques a que lo suban a redes e inunden de contenido los espacios virtuales”. ]

### **Preocupan las nuevas acciones por parte del ESMAD** 

[En medio del Paro Nacional, y con nulas intenciones por parte del gobierno de Iván Duque de retirar el ataque del ESMAD, o considerar desmontarlo, procedimientos como el que se usaron en la noche del 10 de diciembre en la UNAL resultan sumamente preocupantes.]

[En medio de las capturas, una mujer fue arrestada por los agentes del Escuadrón y subida a un carro particular para transportarla. Este tipo de procedimientos está prohibido y puede considerarse «secuestro» por parte de la fuerza pública. El automóvil, de placas HCI264, está registrado en el RUNT a nombre de la Policía Nacional. ]

https://twitter.com/i/status/1204769576447287296

[Gracias a la atención oportuna de un grupo de ciudadanos, que siguieron en otro carro particular al vehículo donde estaba la mujer, se logró su liberación unas cuadras más adelante de donde había sido raptada. Según lo que contó la mujer, había sido transportada allí para “garantizar su protección”. ]

### **Una situación paralela ocurrió en el ParkWay** 

[Mientras se desarrollaba la manifestación y los enfrentamientos con el ESMAD en la UNAL, unas cuadras más al oriente, en el barrio La Soledad, un plantón de personas que se manifestaban pacíficamente fue arremetido con violencia por parte del Escuadrón.]

[Viviana Castiblanco, integrante de la Red de Derechos Humanos de Bogotá, explicó que habían cerca de 40 personas en el ParkWay reunidos con música, sin actos de violencia, bloqueando una de las vías. Primero llegaron unos agentes de Policía con los cuales “se negoció y se despejó un carril para que los carros transitaran”.][(Le puede interesar: Las razones para pedir el desmonte del ESMAD a 20 años de su creación)](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/)

[Una hora después, llegaron cerca de 35 agentes del ESMAD junto a fuerza disponible de la policía. Capturaron a un joven que se encontraba “bailando en mitad de la calle y lo montaron a un vehículo oficial. De igual forma, una mujer y otros participantes, entre ellos un defensor de derechos humanos, fueron retenidos sin estar realizando ninguna acción violenta o ilegal”. ]

[“No los llevaron al CAI más cercano —comenta Viviana— ya que nosotros estuvimos preguntándole a los policías del lugar. Luego me escribe mi compañero de DDHH y me dice que están en el CAI de Teusaquillo. Allí liberan a mi compañero pero no hay razón de la chica que cogieron ni del muchacho que se llevaron de primeras. Segun ellos, los habían soltado antes”. ]

[En las jornadas de manifestación del día 10 de diciembre, se presentaron “17 personas golpeadas y afectadas entre los puntos de la UNAL y el Centro de Memoria, debido a golpes leves y afectaciones por disparo de gases”. Luego se pudo confirmar, como explicó Viviana, “la liberación tanto de la chica como del muchacho”. ]

[Uno de los puntos más importantes, tras estos hechos, es la formulación de planes de contingencia en casos de brutalidad policial. Según Viviana, se han repartido “flyers y volantes con información a la hora de retenciones o capturas ilegales. También invitamos a compartir estos dos números de celular, 313 392 1916 y 315 895 6080, ambos números de emergencia de derechos humanos”.]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45379712" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45379712_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_45380242" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_45380242_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
