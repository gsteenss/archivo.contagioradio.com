Title: Gobierno intenta frenar el trámite de Consultas Populares con argumento del presupuesto
Date: 2017-10-20 12:06
Category: Ambiente, Nacional
Tags: consultas populares, Registraduría Nacional
Slug: gobierno-intenta-frenar-el-tramite-de-las-consultas-populares-con-el-argumento-del-presupuesto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Accesos] 

###### [20 Oct 2017]

Ambientalistas están denunciando que la suspensión de las Consultas populares en el país, debido a la falta de recursos, **es una estrategia más por parte del gobierno para frenar este mecanismo.** De acuerdo con el abogado Rodrigo Negrete, este hecho no solo atentaría contra la Constitución si no que devela los intereses que tiene el gobierno por defender un modelo económico a base de la minería sin importar el impacto social y ambiental de la actividad.

Desde que la Corte Constitucional avaló la posibilidad de que los municipios, los concejos o los habitantes puedan decidir sobre sus propias formas de desarrollo se han realizado 9 consultas populares, en ese sentido lo que destacan los ambientalistas **es que se han salvado los recursos hídricos y la estabilidad social y ambiental de esos mismos territorios**.

Para Rodrigo Negrete, abogado y ambientalista, la decisión de la Registraduría de suspender las consultas y la revocatoria, es una estrategia para frenar un derecho que está en la ley y en la constitución. Además **han desarrollado estrategias como presionar a los alcaldes y gobernadores para que no se de trámite a los procedimientos** y  los alcaldes no tienen los recursos de ley para gestionar los dineros.

Según el abogado no existe ninguna ley que señale que deben ser los municipios o departamentos los que deban financiar las consultas populares en sus territorios y que debe ser la Registraduría la que financie a las comunidades. “**poner a los alcaldes a buscar financiación es un mandato que no está en ninguna ley** y no podría hacerlo porque no están facultados para ello”.

A su vez, el abogado manifestó que cuando se fija una fecha para realizar una consulta popular es porque el Registrador ya ha consultado a las entidades correspondientes para garantizar que la misma se lleve a cabo, **por ende cancelar la consulta en Granda sería “un ataque del gobierno”**. (Le puede interesar: ["Registraduría pide suspender consulta popular en Granada, Meta"](https://archivo.contagioradio.com/por-falta-de-presupuesto-se-suspende-consulta-popular-en-granada-meta/))

**¿Cuál será el destino de las Consultas populares?**

Negrete afirmó que podrían interponer acciones de tutela contra la Registraduría y el Ministerio de Hacienda, por la violación a los derechos fundamentales de las comunidades e ir a instancias como la Procuraduría y agregó que el Comité de Derechos Humanos, Económicos, Sociales y Culturales emitió un informe en donde insta al gobierno de Colombia a respetar y acatar los mandatos de las consultas populares, **razón por la cual esta situación también podría ser llevada a tribunales internacionales.** (Le puede interesar: ["Comité de la ONU exige respeto a las Consultas Populares"](https://archivo.contagioradio.com/comite-de-la-onu-aboga-por-el-respeto-a-las-consultas-populares/))

De igual forma la Diocesis de Granada, a través de un comunicado de prensa señaló que han acompañado el proceso de la comunidad que "lucha por la defensa y salvaguarda de la tierra y el territorio" por tal razón expresó su preocupación por la vulneración que se esta generando por parte de las instituciones de orden nacional, específicamente al ejercicio pleno de los derechos políticos.

[Comunicado Diocesis de Granada Consulta Popular](https://www.scribd.com/document/362151412/Comunicado-Diocesis-de-Granada-Consulta-Popular#from_embed "View Comunicado Diocesis de Granada Consulta Popular on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Comunicado Diocesis de Granada Consulta Popular" src="https://www.scribd.com/embeds/362151412/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LTknIlIwUlQ8JjyIClNS&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_89917" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
