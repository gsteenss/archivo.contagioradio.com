Title: Dos personas fueron torturadas por paramilitares en Blanquicet, Turbo
Date: 2017-10-02 12:09
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, Autodefensas Gaitanistas, Blanquicet, paramilitares, Paramilitarismo, Turbo
Slug: paramilitares-torturan-a-dos-personas-en-blanquicet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica ] 

###### [2 Oct 2017] 

La comunidad del corregimiento de Blanquicet en el municipio de Turbo, Antioquia, denunció que los paramilitares de las **Autodefensas Gaitanistas ingresaron a ese territorio** el 1 de octubre, allí torturan a dos habitantes.

De acuerdo con un líder de la comunidad, desde el 28 de septiembre los paramilitares de las AGC **han venido aumentando sus acciones de intimidación contra varios líderes**. Afirmó que ese día hubo presencia de la Policía y lograron sacarlos del territorio. (Le puede interesar: ["Paramilitares amenazan a organización de familiares de las FARC"](https://archivo.contagioradio.com/familiares-farc-amenazas/))

Sin embargo, el 1 de octubre a las 3:00 pm, los líderes sociales se encontraban en una reunión en la vereda Villa Eugenia, “cuando se terminó la reunión las personas se fueron a su casa y **sin medir distancia los paramilitares llegaron a la casa de dos líderes**”.

El líder aseguró que las personas agredidas fueron Juan Viloria y Fidencio Calle. “**A ellos los cogieron a garrote y a patadas**, a Fidencio lo torturaron y le machacaron las uñas”. Ambas personas fueron abandonadas y, de acuerdo con la información, a las 2:00 am la Policía logró sacarlos de la casa para ser trasladados al hospital.

### **Comunidad pide presencia estatal urgente** 

Debido a lo que sucedió, a las constantes amenazas que sufren los líderes de esta comunidad y a la **fuerte presencia del grupo paramilitar**, la comunidad le ha pedido al Estado “que haga presencia permanente por medio de Policía y Ejército”. (Le puede interesar: ["Paramilitares imponen controles a transporte público en Chocó"](https://archivo.contagioradio.com/44498/))

También han dicho que en múltiples ocasiones **han denunciado ante la Unidad Nacional de Protección la grave situación** que viven los líderes, pero “lo que han hecho es retirar los esquemas de seguridad”. Dicen que varios reclamantes de tierras han sido agredidos y que los empresarios les han quitado el territorio y “nadie se atreve a reclamar”.

Finalmente, el líder recordó la grave situación que viven las y los campesinos de Blanquicet, **“donde están los campesinos no hay presencia del Estado, ahí mandan los paramilitares”.**

<iframe id="audio_21217738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21217738_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
