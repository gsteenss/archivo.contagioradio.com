Title: Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país
Date: 2020-05-05 19:55
Author: CtgAdm
Category: Actualidad, Paz
Tags: Recursos para la Paz
Slug: mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia/ Ivan Duque

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

  
El pasado 30 de abril, la Presidencia suscribió un contrato por \$3.350 millones de pesos con la firma Du Brands SAS, para manejar la estrategia digital, de redes y contenidos del Gobierno durante el 2020, sin embargo, los recursos de donde se pagaría a la compañía - una de las que financió el No durante el plebiscito por la paz de 2016 - provendrían de un fondo que tiene por objeto "la financiación de programas de paz encaminados a fomentar la reincorporación a la vida civil de grupos alzados en armas".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho contrato especifica que se debe definir e implementar "la estrategia de imagen y posicionamiento online del presidente", además pide entregar informes sobre tendencias en redes, riesgo reputacional u oportunidades de participación. Según información recolectada por otros medios, con Du Brands SAS **se han celebrado cuatro contratos en la Presidencia que suman más de \$8.000 mil millones.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JUANCAELBROKY/status/1257537161756381184","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JUANCAELBROKY/status/1257537161756381184

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

**Juan Fernado Cristo,** exministro de Interior e integrante de **[Defendamos la Paz,](https://twitter.com/DefendamosPaz)** señala que esta contratación hecha con la firma Du Brands SAS, una de las 16 organizaciones financiadoras de la campaña del No en el plebiscito del 2016, tiene dos formas de leerse, ambas iguales de preocupantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En primer lugar, al ver que se gastan cerca de  \$3.350 millones de pesos con el fin de manejar redes sociales del presidente Duque, Cristo señala que se trata de recursos exagerados en momentos en los que el dinero se requiere para proteger la salud y el empleo de las y los colombianos. [(Le puede interesar: Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana)](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha cifra, **sumada a los cerca de los 9.600 millones de pesos invertidos por la Policía Nacional en la adquisición de 23 carros blindados demuestran una "absoluta insensibilidad frente a la difícil situación que están atravesando los colombianos"** como consecuencia de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado señala que el hecho de elegir a una firma que financió con 350 millones de peso a la campaña del No durante el Plebiscito, y que ahora a esa misma compañía se le pague y se beneficie con dineros de la paz resulta ofensivo. [(Lea también: Víctimas del conflicto llevan más de tres años esperando sus curules)](https://archivo.contagioradio.com/victimas-del-conflicto-llevan-mas-de-tres-anos-esperando-sus-curules/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Dónde está la austeridad que demanda el Gobierno?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cristo señala que en este momento se habla de la necesidad de concentrar los recursos para atender a las poblaciones vulnerables, sin mencionar el apoyo que podría brindarse a los reincorporados y sus proyectos productivos, que para octubre de 2019, d**e 13.000 proyectos presentados, solo 500 habían sido aprobados, 35 de carácter colectivo y solo 19 con dinero ya desembolsado**. [(Lea también: Gobierno apoya los proyectos individuales pero no los colectivos en los ETCR)](https://archivo.contagioradio.com/gobierno-apoya-los-proyectos-individuales-pero-no-los-colectivos-en-los-etcr/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Hassan Nassar, actual jefe de comunicaciones de la Casa de Nariño explicó que se trata de la estrategia de comunicaciones de todo el Gobierno, en cabeza de Iván Duque y que son dineros que provienen del Ministerio de Hacienda, para Cristo se trata de una evidencia más **de cómo el Gobierno no tiene una "conexión" con la realidad que vive el país.** [(Le puede interesar: Promover la paz en los territorios: el desafío de Defendamos la Paz para el 2020)](https://archivo.contagioradio.com/promover-la-paz-en-los-territorios-el-desafio-de-defendamos-la-paz-para-el-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Concluye que estos contratos, sumados a las peticiones de sectores como el Centro Democrático, de tomar dinero de la paz para destinarlo a ayudas humanitarias por el Covid-19, reiteran que "desde el 7 de agosto de 2018 el Gobierno busca entorpecer la implementación del Acuerdo de Paz" y que pese a sus intentos, la comunidad internacional ha ejercido la presión suficiente para evitar que así suceda.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Decisiones de Duque también afectan a los reincorporados

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Es sabido que el fondo creado para la financiación de la reincorporación no va por buen camino desde que se inició el gobierno Duque, incluso desde el gobierno Santos. Son cerca de 13 mil personas las que necesitarían los mayores respaldos posibles para poder hacer efectivo y eficaz su proceso de reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de los ejemplos del mal empleo de estos recursos en el bajo porcentaje de aprobación y desembolso de los proyectos productivos solicitados por grupos de reincorporados. Hay que recordar que el acuerdo de paz no solamente establece el tema de la renta básica, que está en 790.000 pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se espera que el gobierno apruebe la destinación acordada con un monto de ocho millones de pesos para proyectos productivos. Según los últimos reportes de la ARN solamente se han aprobado 52, lo cual no quiere decir que se hayan desembolsado todos los recursos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Adicionalmente los proyectos productivos aprobados y con desembolso, o con recursos de cooperación internacional estaban en fase de implementación, producción o comercialización cuando se decretó el estado de emergencia y ahora están paralizados.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Con este panorama, el gobierno, si realmente estuviera comprometido con la paz, debería pensar en un plan de mitigación al que le vendrían muy bien los 3500 millones de pesos que se vana a gastar en publicidad.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

**Se trata de proyectos turísticos, de ganadería, de producción agrícola o incluso de comercialización de productos manufactureros que están completamente parados**. Con este panorama, el gobierno, si realmente estuviera comprometido con la paz, debería pensar en un plan de mitigación al que le vendrían muy bien los 3.500 millones de pesos que se vana a gastar en publicidad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
