Title: Comunidades del Huila exigen justicia y dicen no al represamiento del río Guarapas
Date: 2015-10-13 14:33
Category: Movilización, Nacional
Tags: Comisión Intereclesial de Justicia y Paz, Conpaz, Familiares de víctimas genocidio UP, Genocidio UP, Huila, impunidad, justicia, Masacre de campesinos en Palestina, Palestina, Represa El Quimbo
Slug: comunidades-del-huila-exigen-justicia-y-dicen-no-al-represamiento-del-rio-guarapas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Unión-Patriótica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [aporrea]

<iframe src="http://www.ivoox.com/player_ek_8963571_2_1.html?data=mZ6jlZqbdY6ZmKiakpyJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfxcrZja3Zrc3VjMrly8zJsoze1tjhy8jNpYztjMnWxcrSb8%2FjjMbZjdfJtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Enrique Chimonja] 

###### [13 Oct 2015]

[El pasado fin de semana en Palestina, Huila, familiares de Martin Humberto Astaiza, Jesús Galindo, Yesid Tunjo y Jaime Loaiza, **campesinos asesinados en el marco del genocidio al partido político de  la UP**, junto con organizaciones como la Comisión Intereclesial de Justicia y Paz, CONPAZ y la Corporación Reiniciar conmemoraron los **30 años de esta masacre que aún sigue en la impunidad**.]

[Enrique Chimonja, hijo de una de las víctimas, aseguró que cerca de **150 campesinos de las 42 veredas** de este municipio participaron del evento de conmemoración, que incluyó una bicicletada desde Pitalito, como muestra de solidaridad con las familias de las víctimas para la **exigencia de justicia por un crimen que continúa impune**.]

[Así mismo los participantes del evento instalaron en el parque principal una placa, específicamente en el lugar en el que ocurrieron los asesinatos, como reclamación **“Para que este tipo de crímenes no vuelvan a suceder en ningún territorio del país”**, afirma Chimonja.]

[Las comunidades indígenas del municipio se han solidarizado con las reivindicaciones de estos familiares pues en palabras de Chimonja, “Han entendido que en esta coyuntura de los Diálogos **es necesario exigir justicia ante la impunidad**”. Quienes además han denunciado los crímenes ambientales producto de la instalación de la represa del Quimbo.]

[Problemáticas que se agudizan con las decisiones del Gobierno de continuar con la **privatización de las fuentes hídricas para la producción de energía eléctrica** a través de la **instalación de 2 nuevas represas en el río Guarapas, que surte de agua a diversas comunidades de la región.**]

 
