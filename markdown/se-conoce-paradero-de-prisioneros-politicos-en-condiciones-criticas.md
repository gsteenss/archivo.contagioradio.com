Title: Prisioneros políticos en condiciones críticas luego de traslados
Date: 2020-03-27 14:04
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel, picaleña, prisioneros
Slug: se-conoce-paradero-de-prisioneros-politicos-en-condiciones-criticas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Después de varios días se logró establecer el paradero de prisioneros políticos, sin embargo se pudo constatar que están en condiciones críticas. Sus familias señalan que pese a qué ya saben que se encuentran en la cárcel de Picaleña, en Ibagué, las condiciones de reclusión son deplorables y preocupantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con un comunicado del colectivo Libres e Inocentes, Lizeth Rodríguez, Lina Jiménez y Alejandra Méndez, acusadas del atentado en el Centro Comercial Andino y quienes hasta el pasado viernes se encontraban recluidas en la cárcel del Buen Pastor, fueron trasladadas sin previo aviso y sin permitirles recoger sus pertenencias, y ahora estarían en la cárcel de Coiba, en Ibagué, junto a otras cien reclusas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que habría aumentado el riesgo de Contagio del COVID 19, según la organización, debido a que en la cárcel del Buen Pastor, en Bogotá, se había denunciado un posible caso de contagio. (Le pude interesar: ["Se desconoce paradero de prisioneros políticos luego de traslados arbitrarios"](https://archivo.contagioradio.com/se-desconoce-paradero-de-prisioneros-politicos-luego-de-traslados-arbitrarios/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, las tres prisioneras políticas se encuentran actualmente en un calabozo, en donde si bien se está aplicando el aislamiento para las familias, las medidas frente al INPEC y administrativos siguen siendo nulas. Razón por la cual la alerta sobre la llegada de la pandemia a los centros de reclusión es máxima.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Prisioneros políticos de FARC en condiciones críticas
-----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el caso de los prisioneros políticos Moises Quintero, Oscar Rodríguez y José Ángel Parra, integrantes de FARC, los llevaron al centro penitenciario de Picaleña en Ibague. Sin embargo, la situación de Parra, quien padece de leucemia, podría tener complicaciones en una reclusión que no cuenta con las condiciones mínimas de salubridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumado a ello el Comité de Solidaridad con Prisioneros Políticos manifiesta que Parra se ha destacado por su labor en la defensa de derechos humanos del movimiento carcelario y que a pesar de estar acogido al Acuerdo de Paz, su proceso jurídico continúa en vilo. (Le pude interesar:["El fin no ha sido fugarnos – Mujeres reclusas en El Buen Pastor Bogotá"](https://www.justiciaypazcolombia.com/el-fin-no-ha-sido-fugarnos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo la Brigada Jurídica Eduardo Umañana denuncia que el prisionero Diego Fernando Álvarez, que se encontraba en la cárcel La Modelo, no ha sido ubicado por sus familiares, tras los hechos del 21 de marzo. De igual forma están exigiendo a la Defensoría del Pueblo que se emita una lista con los nombres de las personas heridas, los hospitales en los que estarían y las personas asesinadas para que las familias puedan tener iformación inmediata del lugar en el que se encuentran las personas.

<!-- /wp:paragraph -->
