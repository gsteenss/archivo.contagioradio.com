Title: En Tolima, Quindío y Caquetá el agua se defiende en carnaval
Date: 2016-06-02 17:37
Category: Ambiente, Nacional
Tags: Caquetá, Marcha carnaval, Quindío, Tolima
Slug: tolima-quindio-y-caqueta-realizan-un-carnaval-en-defensa-del-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-por-el-agua-e1464906866438.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gente de cabecera 

###### [2 Jun 2016] 

Este viernes 3 de junio, las poblaciones del departamento de Caquetá, Quindío y Tolima, marchan en medio de un carnaval por la defensa del agua frente a la amenaza extractivista en zonas ambientalmente estratégicas, como lo son estos departamentos.

Desde las 9 de la mañana, **en Florencia las personas se concentrarán en la Normal Nacional y UniAmazonía, en Armenia a las 2 de la tarde el punto de encuentro será en el Parque Fundadores, y en Ibagué a las 2:30 de la tarde los manifestantes se encontrará  en el SENA.** Sin embargo, las personas de otras zonas del país interesadas en defender el agua y demás riquezas naturales de Colombia, podrán evidenciar su apoyo a través de las redes sociales usando el  hashtag \#CarnavalPorElAgua.

El objetivo es demostrar el rechazo a la intervención extractivista, tanto petrolera como minera en estos territorios, que supondría daños ambientales irreversibles, entre ellos, la pérdida de las fuentes hídricas.

### Caquetá 

Los municipios de Doncello, Puerto Rico, Paujil y San Vicente, hacen parte del departamento del Caquetá y **representan el 18.67% de la región Amazónica** colombiana, un área que requiere altos estándares de protección ambiental por la cantidad de agua que allí se produce, sin embargo que se encuentra amenazada por 43 bloques petroleros.

Según la Agencia Nacional de Hidrocarburos, ANH, en Caquetá se planea la puesta en marcha de estos bloques estarías dispuestos para las empresas,** Meta Petroleum, Pacific Stratus, Canacol, Emerald Energy, Monterico, Hupecol, C&C Energy, Optima Range y Ecopetrol,** son las empresas interesadas en realizar actividad petrolera en este departamento.

[![Caqueta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Caqueta.jpeg){.aligncenter .wp-image-24856 width="538" height="333"}](https://archivo.contagioradio.com/tolima-quindio-y-caqueta-realizan-un-carnaval-en-defensa-del-agua/caqueta/)

###  

### Quindío 

Tras la negativa de los habitantes de Salento, Quindío, y de diversos sectores de la población civil frente a la posibilidad de que se realice actividad minera en el Valle del Cocora, la Agencia Nacional de Minería, aseguró que no se otorgará el título minero para la explotación de oro en esa zona del país. Sin embargo, los habitantes esperan que ese anuncio se materialice con resoluciones y actos administrativos, teniendo en cuenta que aún existen 14 solicitudes de concesiones mineras en trámite, sin tener en cuenta que el **98% de Salento tiene alguna declaratoria de protección ambiental, y en contraste con ello, más del 50% de ese territorio estaría titulado o en trámite para otorgar licencias.**

El riesgo es inminente, pues no la palma de cera, árbol nacional de Colombia estaría en riesgo, sino también especies animales como **el Loro orejiamarillo, que actualmente es una de las especies de loros más amenazadas del mundo** debido principalmente a la destrucción de su hábitat y la tala de la palma de cera donde construye sus nidos y de la cual se alimenta.

[![Armenia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Armenia.jpeg){.aligncenter .wp-image-24858 width="415" height="415"}](https://archivo.contagioradio.com/tolima-quindio-y-caqueta-realizan-un-carnaval-en-defensa-del-agua/armenia/)

### Tolima 

Esta es la octava marcha carnaval con expresiones culturales, creativas pacificas y de amor en defensa del territorio y el agua, en el marco de lo que ha significado la lucha contra la minería.  **Ibagué tiene el 53 % de su territorio en títulos mineros concedidos y solicitados, es decir, 18 veces todo su casco urbano**. La amenaza minera se ubica principalmente sobre las cuencas de los ríos Coello, Combeima, Toche, Totare y Cocora. Los tolimenses salen a marchar en defensa del agua pues la cuenca del río combeima abastece de agua a la ciudad de Ibagué donde hay títulos mineros de Anglogold Ashanti Colombia, entre otros.

[![Tolima](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Tolima.jpg){.aligncenter .wp-image-24857 width="417" height="608"}](https://archivo.contagioradio.com/tolima-quindio-y-caqueta-realizan-un-carnaval-en-defensa-del-agua/tolima-2/)

https://www.youtube.com/watch?v=hX-gcK6evrE&feature=youtu.be

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
