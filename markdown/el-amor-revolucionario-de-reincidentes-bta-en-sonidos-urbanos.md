Title: El amor revolucionario de Reincidentes Bta en Sonidos urbanos
Date: 2016-06-06 12:17
Category: Sonidos Urbanos
Tags: Hip hop bogotano, Rap, Reincidentes, Reincidentes Bta
Slug: el-amor-revolucionario-de-reincidentes-bta-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reincidentes-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reincidentes Bta 

###### 31 May 2016 

**Reincidentes Bta**, agrupación bogotana de hip hop que nace en el año 2007 nos acompaño en la segunda temporada de Sonidos Urbanos. Sus integrantes: Manuel, Daniel y Jaison nos hablaron del nacimiento de la banda, de sus influencias musicales y de esas historias que han marcado su camino en la escena hip hop.

La mezcla con el hardcore y el reggae le ha permitido a la banda llegar a más oídos y dar su mensaje de inconformismo y autonomía desde el amor y la fuerza. Muestra de esto es su canción “Amor Revolucionario”, una canción que evoca los sentimientos y la valentía femenina.

En el inicio de esta nueva temporada de Sonidos Urbanos, Reincidentes Bta presentó "A parar para avanzar”canción que le canta a lucha en las calles y que podría convertirse en el himno del paro nacional que se vive en el país.

<iframe src="http://co.ivoox.com/es/player_ej_11827293_2_1.html?data=kpallJyWfZShhpywj5WVaZS1kpaSlaaWeY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiatMrNssTdxcrb1srXb6PowpDS0JC3s8_dxdTgjdrWpsLi0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
