Title: 5 Indígenas del Norte del Cauca han sido asesinados en últimos días
Date: 2016-07-22 15:57
Category: Nacional
Tags: ACIN, Cauca, indígenas, Paramilitarismo
Slug: 5-indigenas-del-norte-del-cauca-han-sido-asesinados-en-ultimos-dias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ACIN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [nasaacin.org]

###### [22 Jul 2016] 

Durante la última semana, cinco indígenas han sido asesinados al norte del Cauca. Entre las víctimas se encuentra, **Beatriz Nohemi Morano quien tenía 9 meses de gestación, pero ni ella ni el bebé sobrevivieron al ataque**. Un hecho que se une a varias amenazas hacia la comunidad indígena por parte de grupos paramilitares de la zona.

En total en el primer semestre del 2016, **ya son 17 los asesinatos de líderes indígenas en esta zona del país.** De acuerdo con la ACIN, los hechos en los que asesinaron a las últimas dos personas, se dieron cuando los indígenas llegaban a la ciudad y fueron interceptados en la vía para ser asesinados con arma de fuego.

“Esta serie de eventos desafortunados señalan una situación preocupante y de riesgo para las comunidades indígenas en el norte del Cauca, toda vez que **se presentan en un contexto en el cual han circulado panfletos amenazantes suscritos por grupos paramilitares,** hechos que desarmonizan el territorio y se configuran como muestra de que la violencia y la guerra están lejos de desaparecer en la región”, señala el comunicado de la ACIN.

Por su parte,  Edwin Capaz, Coordinador defensa a la vida de la ACIN, asegura que **las denuncias han sido reiterativas pero no las han atendido las autoridades competentes**, lo que ha generado la impunidad y con ello, que los hechos continúen sucediendo.

Es por ello, que las comunidades indígenas del Norte del Cauca fortalecerán la guardia indígena, pero además **exigen que la Fiscalía General de la Nación investigue y sancione a las personas responsables** de estos hechos que tienen alarmada a la población.

<iframe src="http://co.ivoox.com/es/player_ej_12309511_2_1.html?data=kpegkp6ZdZKhhpywj5aaaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncabY2M7bjajFtMLuhpewjajTs9PYytPOxtTWb8XZx8rb1caPpYzgwpDjy8nFb8XZjNHOjaanja-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
