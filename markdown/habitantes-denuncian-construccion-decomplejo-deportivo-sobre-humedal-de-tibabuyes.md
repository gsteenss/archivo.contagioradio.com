Title: Habitantes denuncian construcción de complejo deportivo sobre Humedal de Tibabuyes
Date: 2018-10-05 15:44
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Alcaldía Distrital, Bogotá, Humedal de Tibabuyes, Peñalosa
Slug: habitantes-denuncian-construccion-decomplejo-deportivo-sobre-humedal-de-tibabuyes
Status: published

###### [Foto: Tiguatech] 

###### [05 Oct 2018] 

Los habitantes de las localidades de Suba y Engativá, que viven cerca al Humedal de Tibabuyes, denunciaron que recientemente conocieron las iniciativas que adelanta la Alcaldía Distrital para construir un **complejo deportivo y corredores ambientales en la última zona verde que queda de este hábitat.**

El Humedal Tibabuyes o Juan Amarillo, es el más grande en Bogotá con 222 hectáreas, y según Julieta Caballero, ambientalista y habitante de este lugar, se pretendería construir un gran parque metropolitano que estaría a cargo del IDRD, y que contaría con canchas sintéticas edificadas justamente **sobre la zona de amortiguamiento del humedal y la única reserva verde con fauna que aún tiene**.

Frente a este hecho Caballero afirmó que esa construcción es ilógica debido a que este humedal es uno de los pocos que aún existe en la capital y que alberga fauna y flora nativa del territorio, además de ser una parada para especies migratorias, " los humedales son los espacios más vulnerables del mundo, **hay un llamado a conservarlos porque son indispensables y estratégicos para la supervivencia de los humanos y los animales**" afirmó.

Asimismo Caballero señaló que el IDRD esta desconociendo su propia misión, debido a que acabarían con un hábitat que ya brida beneficios a las personas "los humedales y los parques nos brindan bienestar, tienen una cobertura vegetal que permite la conversión de aire en una magnitud que ninguna maquina humana aún lo hace". (Le puede interesar: ["Bogotá ha perdido el 90% de sus humedales en 40 años"](https://archivo.contagioradio.com/bogota-ha-perdido-el-90-de-sus-humedales-en-40-anos/))

### **Si acaban el Juan Amarillo, Bogotá acabará con una parte de su hábitat** 

La ambientalista recordó que los humedales son ecosistemas muy frágiles y que los animales que viven allí no distinguen de límites o territorios, razón por la cual, si es intervenido, no se dejaría espacio para ellos, además este lugar ya es un corredor de fauna que conecta con otros **humedales como el de la Conejera, el Jaboque y la Reserva Thomas Van Der Hammen.**

El  próximo 13 de octubre el IDRD estaría convocando a la última reunión con la comunidad para dar a conocer este proyecto, en el Colegio Rodolfo Llinás, posteriormente se daría el proceso de la licitación y la construcción empezaría en el mes de diciembre. Frente a los corredores ambientales, el Acueducto publicó el pasado 4 de octubre en su página web, la convocatoria de la licitación de este plan.

<iframe id="audio_29123819" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29123819_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
