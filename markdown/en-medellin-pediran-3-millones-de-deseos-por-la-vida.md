Title: En Medellín pedirán 3 millones de Deseos por la Vida
Date: 2017-08-11 13:29
Category: DDHH, Nacional
Tags: Antioquia, asesinatos en medellin, deseos por la vida, Medellin
Slug: en-medellin-pediran-3-millones-de-deseos-por-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKQhF00XoAAnJ4h.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/no-mataras-e1502476846580.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twiter @JUANBROSADO] 

###### [11 Ago 2017] 

Como símbolo de protesta contra los homicidios constantes en Medellín Antioquia, **un grupo de ciudadanos tiñó de rojo las aguas de diferentes fuentes de la ciudad** el pasado 30 de julio. La alcaldía de esta ciudad rechazó la iniciativa y realizó el cobro por 3 millones de pesos para cambiar el agua de las fuentes.

Ante esto, **los ciudadanos realizan la campaña Deseos Por la Vida** donde buscan recoger el dinero y seguir enviando un mensaje de defensa de la vida. (Le puede interesar: ["En Antioquia se han presentado 111 agresiones contra defensores de DDHH"](https://archivo.contagioradio.com/en-antioquia-se-han-presentando-111agresiones-contra-defensores-de-dd-hh/))

Pese a que, según el Sistema para la Seguridad y Convivencia de Medellín, las cifras de homicidios se han reducido en comparación con años anteriores, en lo que va del 2017 se han reportado 301 homicidios relacionados con ajustes de cuentas, intolerancia, riñas y violencia intrafamiliar. Igualmente, **sólo en el mes de julio ocurrieron 52 asesinatos**.

Teniendo presente esta situación, **los ciudadanos decidieron enviar un mensaje que evite que las muertes violentas se sigan presentando** o que por lo menos no se "naturalicen". Fue por esto que, usando analina vegetal roja, pintaron las aguas del Parque Bolívar, el parque San Antonio, el Parque de los Pies Descalzos y las fuentes de agua del teatro Pablo Tobón.

Según Mayra Duque, vocera del movimiento ciudadano, **“la Alcaldía rechazó tajantemente la acción que habíamos realizado** y en un principio dijeron que el costo de la recuperación de las fuentes iba a ser de 50 millones de pesos, en realidad esto costó 3,070.133 pesos”. (Le puede interesar: ["Las estructuras criminales no han sido desmanteladas en Medellín"](https://archivo.contagioradio.com/las-estructuras-criminales-no-han-sido-desmanteladas-en-medellin/))

Para devolverle esta plata a la ciudad, ellos y ellas decidieron crear la campaña Deseos por la Vida **a través de la plataforma Little Big Money** donde cualquier persona puede donar desde 25 mil pesos.

Duque manifestó que esta donación “más que pagar por la recuperación de las fuentes busca incrementar el mensaje hacia la sociedad donde **decimos que no se puede permitir que el homicidio sea algo normal**”. Igualmente, le hacen un llamado al Alcalde, Federico Gutiérrez, para “revisar el plan de seguridad y convivencia que no está funcionando”.

Luego de que recojan el dinero, ellos y ellas **lo cambiarán por monedas para que la ciudadanía se acerque a una fuente de agua y “pida un deseo por la vida”**. Cuando esto suceda, recogerán las monedas para que sean contadas por un notario y finalmente las entregarán a la alcaldía. Duque informó que, si logran recoger más dinero del estipulado, lo guardarán para crear un fondo ciudadano que financie futuras acciones por la vida. (Le puede interesar: ["Asesinada líder de Asokinchas en Medellín"](https://archivo.contagioradio.com/asesinada-lideresa-asokinchas-medellin/))

Finalmente, Duque manifestó que la iniciativa de recoger el dinero “es una excusa para seguir diciéndole a la ciudadanía y a los defensores sociales la importancia de defender la vida, sentimos que hay que unirnos para proteger la vida.” Para donar a la iniciativa puede hacerlo a través de **http://littlebigmoney.org/es/deseosporlavida\#backers**

<iframe id="audio_20288952" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20288952_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
