Title: Un día para conmemorar a las víctimas de crímenes de Estado y exigir que no se repita
Date: 2017-03-06 16:58
Category: DDHH, Nacional
Tags: crímenes de estado, Día por la Dignidad de las Víctimas de Crímenes de Estado, MOVICE
Slug: un-dia-para-conmemorar-a-las-victimas-de-crimenes-de-estado-y-exigir-que-no-se-repita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-06-at-10.53.54-AM1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [6 Mar 2017] 

Familiares de las víctimas de crímenes de estado y el MOVICE, manifestaron que las exigencias para la conmemoración de este 6 de Marzo, es el **desmonte del paramilitarismo, garantías de seguridad para líderes y lideresas comunitarias, la implementación** efectiva de los Acuerdos y celeridad en el inicio de conversaciones con el ELN.

Distintas organizaciones defensoras de derechos humanos denunciaron que es evidente que **"el paramilitarismo y sus estructuras no han sido realmente desmontadas, por el contrario, continúan actuando".** De acuerdo con el más reciente informe de la Defensoría del Pueblo, al menos 120 líderes, lideresas y defensores de derechos humanos, han sido asesinados entre el 1 de enero de 2016 y el 20 de febrero de 2017.

Advierten que el Estado se ha mantenido negligente frente a las denuncias sobre la presencia de paramilitares en los territorios, amenazas, hostigamientos, atentados y homicidios, que **"se han convertido en una constante en el día a día de las comunidades de ciertas zonas del país".**

Por todo ello, en 10 ciudades del país se realizarán marchas, plantones y muestras artísticas  para conmemorar a las víctimas y **exigir al Estado que no haya más impunidad, que se de cumplimiento a lo pacatado en La Habana y hayan garantías de no repetición** ante las graves violaciones a los derechos humanos y al Derecho Internacional Humanitario, como requisito inexorable para una Paz completa, estable y duradera.

En Bogotá, la cita es en la Plazoleta Eduardo Umaña Mendoza en la Kra. 7ma con Cll 21, a apartir de las 4:00pm y hasta las 7:00pm, donde habrá una galería de la memoria, proyecciones de piezas audiovisuales a cargo del colectivo Bicio-Visual y otras muestras artísticas para apoyar a los familiares de las víctimas.

###### Reciba toda la información de Contagio Radio en [[su correo]
