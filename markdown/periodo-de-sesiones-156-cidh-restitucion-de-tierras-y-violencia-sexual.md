Title: Hay 177 amenazas contra líderes reclamantes de tierras en Colombia
Date: 2015-10-22 12:04
Category: DDHH, Nacional
Tags: CIDH, Comisión Intereclesial de Justicia y Paz, diálogos en la Habana, Ley de justicia y paz, Ley de Victimas y Restitución de Tierras, Paramilitarismo, Periodo 156 de sesiones CIDH
Slug: periodo-de-sesiones-156-cidh-restitucion-de-tierras-y-violencia-sexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [22 Oct 2015 ] 

[Durante la mañana de este miércoles, en las audiencias públicas que adelanta la Corte Interamericana de Derechos Humanos, CIDH **se evidenció las contradicciones que hay entre los datos que plantea el gobierno frente a la restitución de tierras, y lo que evidencian las organizaciones de derechos humanos** que acompañan a la víctimas en estos procesos.]

[Desde la **Unidad de Restitución de Tierras, se señaló que el 84% de los solicitantes han sido restituidos.** Pero cabe recordar, que en abril del 2015, se realizó en el Congreso de la República un debate de control político frente al tema, allí se concluyó que para esa fecha únicamente se había restituido 4.000 hectáreas, lo que significaba que para cumplir la meta de seis millones de hectáreas restituidas, como **lo planteó el presidente Santos, la Unidad de Restitución de Tierras tardaría 543 años**, ya que se había cumplido con el 0,34%.]

[Por otro lado, de acuerdo al Estado, existen los medios jurídicos, instrumentos y recursos, para la restitución de tierras y la reparación de las victimas. Sin embargo,  Danilo Rueda, defensor de derechos humanos de la Comisión de Justicia y Paz, y peticionario ante la CIDH en la audiencia asegura que los mecanismos del gobierno no han sido suficientes, teniendo en cuenta que un **porcentaje importante de los reclamantes de tierra no ha podido regresar por problemas de seguridad.**]

[Rueda también se refirió a cifras que contrastan la realidad frente a la situación de los defensores de derechos humanos en procesos de restitución de tierras. Allí se reportó que hay un total de **177 amenazas** a líderes y defensores, **104 campañas de desprestigio**, **3 atentados**, 17 procesos de **persecución judicial**, 3 casos de **torturas**, **2 secuestros** a menores de edad, 1 caso de **abuso sexual** y 4 **salidas temporales del país**.]

[Pese a la documentación y denuncia de los casos, el Estado colombiano no ha mostrado voluntad en la investigación y sanción a los sectores empresariales e incluso de la iglesia que han tenido responsabilidad directa tanto en la comisión de estos delitos como en la práctica sistemática de despojo, al ser los principales beneficiarios, según indicó el defensor de derechos humanos.]

[En la actualidad,  en las tierras que fueron despojadas a comunidades indígenas, campesinas y afrodescendientes, están **presentes miembros de las estructuras paramilitares que expropiaron estos territorios**, articulándose con sectores políticos y empresariales, como lo demuestran los casos de 40 comerciantes asociados a la multinacional Chiquita Brands, que financiaron con cerca de 40 millones de dólares a grupos paramilitares y de quienes la **Fiscalía tiene pleno conocimiento**, según explica Rueda.]

[La exigencia entonces es que el Estado colombiano **desmonte las estructuras paramilitares vigentes actualmente**, investigue y **sancione a los políticos y empresarios** beneficiarios directos del despojo, y derogue el decreto de microfocalización para que las víctimas tengan igualdad ante la ley, concluye el integrante de la Comisión de Justicia y Paz. ]

https://www.youtube.com/watch?v=kroGQvfkJx4

[ ]
