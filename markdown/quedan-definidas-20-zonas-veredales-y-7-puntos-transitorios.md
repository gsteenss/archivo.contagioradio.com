Title: Quedan definidas 20 Zonas Veredales y 7 Puntos Transitorios
Date: 2016-09-16 12:27
Category: Paz
Tags: Conversacioines de paz en Colombia, Conversaciones de paz de la habana, Diálogos de paz Colombia, proceso de paz Colombia
Slug: quedan-definidas-20-zonas-veredales-y-7-puntos-transitorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Delegados-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Delegación Paz FARC-EP ] 

###### [16 Sept 2016] 

Este jueves se reunieron los miembros del secretariado de las FARC-EP, con los delegados de la Misión de la ONU en Colombia, el Alto Comisionado para la Paz y el Comandante del Comando Estratégico de Transición, en los llanos del Yarí, con el objetivo de concertar los protocolos para la Décima Conferencia de las FARC-EP; la logística de instalación de las Zonas Veredales de Normalización y los Puntos Transitorios de Normalización, así como la **firma del Acuerdo Final y el despliegue del Mecanismo de Monitoreo y Verificación**.

Los delegados acordaron que el próximo 25 de septiembre se reunirán en Cartagena para finalizar la preparación logística del [[Mecanismo de Monitoreo y Verificación](https://archivo.contagioradio.com/arranca-la-verificacion-tripartita-al-cese-bilateral-de-fuego-en-colombia/)], que se desplegará a sus sedes locales entre el 6 y el 10 octubre. En el encuentro también se resolvió que se implementarán **20 Zonas Veredales Transitorias de Normalización y 7 Puntos Transitorios de Normalización**, y que en la misma ciudad, el 26 de septiembre se llevará a cabo la firma del Acuerdo Final, para que el 27 de septiembre se instale la Comisión de Implementación, [[Seguimiento y Verificación](https://archivo.contagioradio.com/onu-y-celac-verificaran-cese-al-fuego-y-dejacion-de-armas/)], que desde este día comenzará su labor con el acompañamiento de las Naciones Unidas.

[![zonas-y-puntos-veredales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Zonas-y-Puntos-veredales-e1474045673382.jpg){.aligncenter .wp-image-29383 .size-full width="929" height="515"}](https://archivo.contagioradio.com/quedan-definidas-20-zonas-veredales-y-7-puntos-transitorios/zonas-y-puntos-veredales/)

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

###  
