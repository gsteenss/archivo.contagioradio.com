Title: Estas son las razones para la revocatoria de Enrique Peñalosa
Date: 2017-01-10 16:10
Category: Nacional, Política
Tags: ETB, revocatoria Peñalosa, Unidos revocaremos a Peñalosa
Slug: estas-las-razones-la-revocatoria-enrique-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/revocatoria_Peñalosa-e1504722871558.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [10 Enero 2017 ] 

Comunidades Muiscas de Suba, vendedores ambulantes, sindicalistas, ambientalistas organizaciones comunitarias y ciudadanos independientes, **se han unido a la iniciativa para la revocatoria de Enrique Peñalosa, sin importar las diferencias políticas,** desmintiendo las afirmaciones de algunos medios sobre el partidismo de los comités inscritos ante la Registraduría.

[Carlos Carrillo, integrante del comité ‘Unidos revocamos a Peñalosa’, aseguró que ]**[“las instituciones no han permitido a la ciudadanía ejercer el control político y es ese el mayor motivo para la ciudadanía". ]**

Los tres comités inscritos ante la Registraduría Distrital, [han manifestado que en la alcaldía Peñalosa ]**640 vendedores informales han sido desalojados, los habitantes del Bronx no han sido atendidos ni reubicados de manera oportuna, los problemas de movilidad se han agudizado** y se ha puesto en riesgo importantes reservas ambientales de la ciudad.

Carrillo señaló que “durante un año de mandato, Peñalosa ha hecho lo que ha querido con la complicidad de entes de control y medios de comunicación (…) **hay un montón de cosas que han llegado demasiado lejos con Enrique Peñalosa por eso la salida es la revocatoria".**

Sectores de la sociedad que disienten de la iniciativa de revocatoria han expuesto que el proceso que costaría unos \$90.000 millones es innecesario. Algunos integrantes de los comités han dicho que **"ese valor es mínimo frente a lo que le costaría a Bogotá que Peñalosa continuará en la alcaldía". **

De igual forma, distintas organizaciones y ciudadanos han manifestado que "Enrique Peñalosa no dijo en su programa de gobierno que **pensaba privatizar las empresas públicas de Bogotá o vender el 20% de ISAGEN, tampoco dijo nada acerca de los intereses en el tema de movilidad y la Reserva van der Hammen”**, puntualizó Carrillo.

### **¿Cuáles son los pasos a seguir para la Revocatoria?** 

A partir del 9 de Enero la Registraduría Distrital **tendrá 15 días para emitir el formato de recolección de las firmas que respaldarán el pedido de revocatoria. **Le puede interesar: [“Unidos revocaremos a Peñalosa” inscrito ante la registraduría.](https://archivo.contagioradio.com/unidos-revocaremos-a-penalosa-inscrito-ante-la-registraduria/)

Por último, Carrillo indicó que el próximo 12 de Enero, los comités darán una rueda de prensa para aclarar los motivos de la revocatoria y anunciaran los puntos de recolección de firmas, que **deberán ser auditadas por la Registraduría en menos de 2 meses después de presentadas y de ser avaladas darían paso a un nuevo escenario electoral en 2017.**

<iframe id="audio_16077303" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16077303_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
