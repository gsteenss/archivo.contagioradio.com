Title: La abstención continúa "decidiendo" el futuro de Colombia
Date: 2016-10-03 12:23
Category: Nacional, Política
Tags: abstención, colombia, democracia, paz, Plebiscito
Slug: el-abstencion-continua-decidiendo-el-futuro-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/elecciones_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gomera Noticias 

###### 3 Oct 2016 

**Con un 62,6% en las urnas la abstención ganó este domingo el plebiscito por la paz**. De un total de 34.899.945 de personas habilitadas para ejercer el derecho al voto, sólo 13.066.047 votaron según el informe de la Registraduría Nacional.

De acuerdo a la investigación realizada en 2013 por el Centro de Estudios en Democracia y Asuntos Electorales (CEDAE) de la Universidad Sergio Arboleda, en Colombia **los niveles de participación en las elecciones nacionales y sub-nacionales han sido históricamente bajas**, en contraste con los altos niveles de abstención ciudadana que oscilan entre un 50 y 60%.

Los periodos analizados de participación y abstención ciudadana (de **1.978 a 2010**), tanto en lo que respecta las elecciones presidenciales, como las elecciones legislativas para cámara y senado evidencian los **altos índices de abstencionismo en contraste con el potencial de votos**.

En el análisis realizado para las elecciones presidenciales, los ciudadanos que no ejercieron su derecho al voto fueron entre un **40,98%**, equivalente a 8.547.694 en la segunda vuelta presidencial de 1998 entre los candidatos Andrés Pastrana y Horacio Serpa, a un porcentaje máximo de abstencionismo del **66,05%** (es decir un total de 11.325.266 personas) en la primera vuelta de 1994 entre Ernesto Samper y el mismo Pastrana.

Los periodos posteriores al 94 dejan en evidencia que la abstención en Colombia no es algo nuevo. En 1998 el porcentaje de ciudadanos que no ejercieron su derecho al voto fue del **48,44%** (primera vuelta) y **40,98%** (segunda vuelta); en 2002 **53,53%**; en 2006 **54,95%**; en 2010 **50,10%** (primera vuelta) y **55,65%** (segunda vuelta) y del **59,93%** en 2014.

El 2 de octubre, Colombia volvió a tener una de las cifras más altas de abstención con un 62,6%, motivo por el cual debe volverse la mirada sobre los factores que incidieron en el no ejercicio de uno de los componentes esenciales de la democracia, como también, deberá ser analizado el papel que desempeñaron las diferentes fuerzas políticas en la cohesión o fragmentación de la sociedad.
