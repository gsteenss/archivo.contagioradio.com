Title: "Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá
Date: 2017-12-14 15:10
Category: DDHH, Nacional, Otra Mirada
Tags: amenazas a líderes sociales, asesinato de líderes sociales, Bajo Atrato, Empresarios, lideres sociales, Urabá Antioqueño
Slug: hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Dic 2017] 

Veinticinco líderes y lideresas sociales del Bajo Atrato Chocoano y el Urabá Antioqueño **amenazados de muerte** y familiares de líderes que han sido asesinados, se reunieron en Bogotá para exigir garantías y poder retornar a sus territorios. Aseguran que detrás de los asesinatos, hay intereses de empresarios que han ocupado sus tierras y que a finales de los 90, los despojaron y desplazaron de sus tierras.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, los líderes sociales “están amenazados de muerte **por una estrategia determinada** por empresarios vinculados a la criminalidad en la región, que han sido señalados en diferentes instancias judiciales y que han sido denunciados ante diferentes instancias”. Recordaron que en menos de 10 días, fueron asesinados dos líderes de la región, Mario Castaño y Hernán Bedoya.

### **Detrás de los asesinatos y las amenazas, hay intereses políticos y económicos** 

Estos líderes llevan más de 20 años luchando por retornar y proteger su territorio. En repetidas ocasiones han manifestado que debido a las denuncias que han realizado han atentado contra su integridad, **“nos han quitado la vida”,** afirmaron. Ellos y ellas han dicho que son los empresarios palmeros, ganaderos, plataneros, mineros y bananeros los que se apropiaron del territorio. Asimismo, manifiestan una complicidad de la fuerza pública, instituciones gubernamentales y representantes de empresas como la Asociación Agropecuaria Campesina AGROMAR S.A, con la que se enfrentó toda su vida Mario Castaño(Le puede interesar: ["La navidad sin Mario Castaño"](https://archivo.contagioradio.com/la-navidad-sin-mario-castano/))

Los líderes asesinados habían denunciado las actuaciones de mala fe de empresarios y representantes de empresas como **Baldoyno Mosquera Palacios, Javier Restrepo y Juan Guillermo González,** de quienes los integrantes de las comunidades han recibido amenazas directas de muerte. **“¿Hasta cuándo nos van a escuchar?”,** se preguntan las personas.

En ese mismo sentido indican que **desde 1997 ha existido una estrategia para tomar los territorios** que les pertenece a las comunidades. Indicaron que “el Gobierno Nacional se ha encargado de tergiversar la realidad, dicen que son ajustes de cuentas entre empresarios contra la comunidad, pero ¿qué cuentas tenemos nosotros con los empresarios? Ninguna”. Ante los medios de comunicación reiteraron que, en esa región, hay poderes políticos y económicos “muy poderosos que están acabando con la vida de quienes reclaman los derechos”. (Le puede interesar: ["Asesinan a Mario Castaño, líder reclamante de tierras en Chocó"](https://archivo.contagioradio.com/asesinan-a-mario-castano-lider-reclamante-de-tierras-en-el-choco/))

### **Paramilitares y Fuerza Pública protegen a los empresarios** 

Ante el anuncio de las Autodefensas Gaitanistas de Colombia sobre el cese al fuego unilateral, los líderes del Bajo Atrato y Urabá, indicaron que “es una medida que llena de esperanza", pero que los autores intelectuales de los asesinatos son estructuras armadas contratas directamente por empresarios, "**que están pagando para que nos maten"**. (Le puede interesar: ["Asesinado líder reclamante de tierras Hernán Bedoya en Chocó"](https://archivo.contagioradio.com/asesinado-reclamante-de-tierras-hernan-bedoya/))

Además, han denunciado que pese a las directrices dadas desde los altos mandos de las AGC, los integrantes de bajo mando de esa estructura operan como sicarios en el territorio. "Por cualquier 300.000** o 500.000 pesos matan a cualquiera**”, expresan.

Por otra parte, les sorprende que aún con la presencia de la Fuerza Pública continúen sucediendo estos asesinatos. En esa línea, aseveraron que el Batallón de selva No. 54, que se encuentra en Llano Rico, acompaña al empresario cuando llega a la comunidad, "el ejército lo cuida cuando se dirige a las tierras que le ha quitado a la comunidad. Los militares lo acompañan a hacer todos los procedimientos ilegales”.

### **"Partidos políticos deben abstenerse de recibir dinero de empresas involucradas en asesinatos a líderes"** 

Teniendo de presente el inicio de la carrera electoral, los líderes y lideresas fueron enfáticos en manifestar que los políticos, **“tienen que dejarnos a los campesinos en nuestras tierras tranquilos”**. Afirmaron que la sociedad “no puede seguir eligiendo a los mismos de siempre” y que los partidos políticos debería abstenerse de recibir recursos para sus campaña de las empresas que están comprometidas en el asesinato de líderes. (Le puede interesar: ["El Chocó continúa sin saber qué es la paz"](https://archivo.contagioradio.com/el-choco-continua-sin-saber-que-es-la-paz/))

Diferentes organizaciones sociales han instado a los partidos políticos a que actúen de manera transparente para que, en el futuro, “no queden atrapados y dirigidos por los intereses de esas empresas”. Finalmente, los líderes exigieron una reunión con Guillermo Rivera, Ministro del Interior y con el vicepresidente Óscar Naranjo, para que se de una respuesta clara sobre la situación de las tierras y la protección de las comunidades, y **“no tengamos que volver a unas exequias nunca más”.**

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
