Title: Comandante del ejército argentino durante la dictadura es condenado a cadena perpetua
Date: 2016-08-25 13:56
Category: El mundo, Otra Mirada
Tags: Abuelas plaza de mayo, Dictadura argentina, militares Argentina, Videla
Slug: ex-militares-de-la-dictadura-argentina-condenados-a-cadena-perpetua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Megacausa-La-Perla_658x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Día a día 

##### [25 Agos 2016]

El Tribunal oral federal número 1 de Córdoba, dictó condena de prisión perpetua en contra de **Luciano Benjamín Menéndez**, ex titular del Tercer cuerpo del Ejército con jurisdicción en 9 provincias del noroeste argentino, por delitos de lesa humanidad cometidos durante la última d[ictadura cívico- militar](https://archivo.contagioradio.com/ordenan-captura-sobre-hebe-de-bonafini-titular-de-madres-de-plaza-de-mayo/) que padeció ese país.

La sentencia por delitos como la privación ilegítima de la libertad, tormentos, homicidios y otros crímenes que atentan contra la humanidad, representa **la máxima pena establecida por el sistema judicial** de ese país y se suma a otras 15 que se han impuesto desde que empezó el proceso en 2012, por las que hoy Menéndez se encuentra bajo detención domiciliaria.

Las causas dan cuenta de sucesos ocurridos entre 1975 y 1979, entre el último año del Gobierno de Isabel Perón y los primeros de la dictadura militar de Jorge Videla, que tuvieron lugar en centros clandestinos de detención, tortura y exterminio de Córdoba, conocidos como **"La Perla" y "La Ribera"**, de los que se registran 417 víctimas por las cuales se han sido imputadas 52 personas.

Lo establecido este jueves con la decisión del tribunal de enjuiciamiento, presidio por Jaime Díaz Gavier, le impone al ex militar una "**[inhabilitación absoluta perpetua, accesorias legales y costas]**", sin embargo [el mismo despacho lo absolvió en 23 casos de tormentos agravados](https://archivo.contagioradio.com/el-verdadero-poder-en-argentina-lo-tiene-el-grupo-clarin-macri-es-una-mascara-v-h-morales/), un hecho de homicidio, seis hechos de allanamiento ilegal y 9 de robos.

Dentro de la misma diligencia, **[f]ueron condenados 27 uniformados más, ente los que se cuentan el capitán retirado Héctor Vergez, quien era jefe del grupo paramilitar "Comando Libertadores de América"**, y el mayor en retiro Ernesto Barreiro, entre otros, mientras que Antonio Filiz; Broccos; Juan Carlos Cerutti; Vélez y Melfi fueron absueltos.

Mientras se presentan oficialmente los fundamentos del juicio, programada para el mes de octubre, varias **organizaciones sociales y de Derechos Humanos preparan movilizaciones** en espera de ser leídas las sentencias definitivas.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
