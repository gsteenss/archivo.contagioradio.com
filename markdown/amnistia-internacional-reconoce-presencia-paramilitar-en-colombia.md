Title: Cambios en los Acuerdos de Paz no garantizan derechos de las víctimas: Amnistía
Date: 2017-02-22 13:46
Category: DDHH, Nacional
Tags: Amnistía Internacional, Informe Anual, Ley Zidres, Líderes asesinados
Slug: amnistia-internacional-reconoce-presencia-paramilitar-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/victimas-amnistia-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Feb. 2017] 

El más reciente **informe de Amnistía Internacional** señala que las privaciones arbitrarias de la vida, las desapariciones forzadas, la tortura y la impunidad siguen siendo parte de la realidad en Colombia. Así como los abusos cometidos por “grupos armados ilegales” y las denuncias de connivencia entre estos grupos y agentes estatales.

Este informe de 477 páginas, centró su atención en el capítulo de Colombia en el **proceso de paz, conflicto armado interno, fuerzas de seguridad, abusos cometidos por grupos armados** (grupos guerrilleros y paramilitares), de la **impunidad, la situación de los defensores de DDHH, la violencia contra niñas y mujeres**, así como del escrutinio internacional. Le puede interesar: [Jurisdicción Especial debe beneficiar a las víctimas](https://archivo.contagioradio.com/jurisdiccion-especial-debe-beneficiar-a-las-victimas/)

Aunque la organización reconoce que haber logrado un acuerdo de paz con la guerrilla de las FARC e instalar una mesa de negociación con el ELN permitió avanzar en la disminución de la violencia en Colombia, también **expresó su preocupación por el aumento de asesinatos de defensores y defensoras de DDHH en el país.**

En esta línea, Amnistía dejo ver su preocupación por los cambios que se hicieron al Acuerdo de Paz, que según la ONG **no refuerzan de manera significativa los derechos de las víctimas. **Le puede interesar: [Congreso archivó posibilidad de que víctimas estén en centro de Acuerdo Paz](https://archivo.contagioradio.com/congreso-archivo-posibilidad-de-que-victimas-sean-centro-de-acuerdo-paz/)

Así mismo, esta ONG ve con preocupación el **aumento de las cifras de asesinatos a líderes** comunitarios, activistas ambientales y a personas que hacían campaña por la paz y la justicia.

En el informe reconocen la presencia de grupos paramilitares en el territorio colombiano e incluso manifiestan que **“los paramilitares siguen delinquiendo pese a su supuesta desmovilización hace 10 años”**, lo que se ha traducido en amenazas de muerte, homicidios y numerosas violaciones a los derechos humanos.

El documento también llama la atención en los casi **nulos avances en materia de condenas a los más de 30 mil paramilitares que se acogieron a la Ley de Justicia y Paz** de 2005, y dice que “la mayor parte de los paramilitares no se sometieron al proceso de Justicia y Paz, y recibieron amnistías de facto”.

Amnistía Internacional agrega además, que si bien se conocieron de abusos por parte de las FARC y el ELN, representados en amenazas contra líderes sociales y periodistas, **los casos que fueron atribuidos a las FARC disminuyeron con el avance del proceso de paz. **Le puede interesar: [Víctimas de Estado deben participar en debates de implementación de acuerdos de paz](https://archivo.contagioradio.com/victimas-estado-colombia-paz/)

De otra parte, afirman que mientras los defensores de derechos humanos continúan exigiendo el respeto a sus territorios, leyes como la de Restitución de Tierras avanza con lentitud y el Congreso aprueba la Ley  1776, que crea grandes proyectos agroindustriales denominados Zonas de Interés de Desarrollo Rural, Económico y Social (ZIDRES).

En materia de **derechos de las mujeres y las niñas, el informe de Amnistía da cuenta de la continuidad de la violencia contra ellas,** pese a los intentos del Gobierno y el Estado por avanzar en contra de estos hechos los avances son pocos y las investigaciones no presentan avances.

Según cifras de la Unidad de Víctimas **desde 1985 hasta el 1 de Diciembre han existido más de 17.500 víctimas de delitos contra la integridad sexual** relacionados con el conflicto. Le puede interesar: [Participación Política, el reto de organizaciones de Mujeres y Disidentes del Género](https://archivo.contagioradio.com/participacion-politica-reto-organizaciones-mujeres-disidentes-del-genero/)

Amnistía Internacional documenta la situación de los derechos humanos en 159 países y territorios, en donde dice que para millones de personas el **2016 fue un año de miseria y miedo implacables sembrados por los abusos contra los derechos humanos** que gobiernos y grupos armados han cometido de múltiples maneras.

[Informe Amnistía Internacional 2016/2017](https://www.scribd.com/document/340040110/Informe-Amnistia-Internacional-2016-2017#from_embed "View Informe Amnistía Internacional 2016/2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_83805" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/340040110/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-L2cRNWx3vyNOUtSdrWrX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6522053506869125"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
