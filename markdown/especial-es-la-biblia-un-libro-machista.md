Title: Especial: ¿Es la Biblia un libro machista?
Date: 2017-01-05 16:30
Category: Mujer, Otra Mirada
Tags: Biblia, Fe, Machismo, mujeres
Slug: especial-es-la-biblia-un-libro-machista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/BibliaMujer.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Guatemala Seculiar] 

###### [3 Enero 2017] 

**A través del tiempo la iglesia ha sido cuestionada por sus diversas formas de actuar ante diferentes situaciones acontecidas en el mundo.** Dentro de dichos cuestionamientos también se ha hablado del papel de la mujer y la importancia que puede o no dársele dentro de una historia de fe.

La biblia, un libro histórico y base para la iglesia, considerado por el Concilio Vaticano II “la palabra de Dios, escrita en lenguaje humano” también ha tenido su papel protagónico.

**Esencialmente cuando se pregunta si la Biblia es un libro machista las personas contestan que sin duda lo es. Sin embargo, en algunos casos padres y obispas (estas últimas no reconocidas por la iglesia romana) han asegurado que todo depende de cómo se lea la Biblia.**

Pero no puede desconocerse que la biblia es comprendida en algunos casos con una mirada fundamentalista, desde la cual no se relaciona con la época de la historia, la cultura y el lenguaje de su tiempo.

Sin embargo lo que ha ido sucediendo a través del tiempo es que la biblia se ha estado mirando de una nueva manera, con mirada de mujer, relecturas ayudadas desde la hermenéutica para reconocer este texto como referente para mujeres del mundo.

Escuche aquí el especial realizado desde Contagio Radio en la que tres conocedores hablan del tema.  
<iframe id="audio_15715956" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15715956_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
