Title: Ejército captura a guerrilleros que se movilizaban hacia zona de concentración
Date: 2016-08-29 12:49
Category: Judicial, Nacional
Tags: Diálogos de La Habana, Diálogos de paz Colombia, proceso de paz
Slug: ejercito-captura-a-guerrilleros-que-se-movilizaban-hacia-zona-de-concentracion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Guerrilleros-Bloque-Magdalena-Medio-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias Uno] 

###### [29 Ago 2016 ] 

De acuerdo con el portal web 'Análisis Urbano', en la madrugada del pasado sábado fueron **capturados dos miembros del Frente 36 de las FARC-EP y tres campesinos** quienes se desplazaban hacia la zona de normalización ubicada en la vereda Carrizal. La captura fue realizada por integrantes de la Brigada XI del Ejército, en una finca del sector conocido como La corona del corregimiento de Puerto Claver, en el Bagre, Antioquia.

Según se conoce, la operación que fue coordinada con la Policía pretendía la captura de miembros del ELN, sin embargo los capturados fueron Víctor Manuel Gutiérrez y Uberley Madrid, guerrilleros del Bloque Magdalena Medio de las FARC-EP, a quienes les fueron incautadas dos pistolas, y **tres campesinos, que ya están en libertad, pero que el Ejército quería hacer pasar como integrantes de la guerrilla del ELN**.** **

Fernando Quijano, director del Portal, asegura que los dos miembros de las FARC-EP que fueron capturados en el marco de una labor de avanzada para el traslado de la tropa hacia la zona de normalización, serán **judicializados en la ciudad de Medellín por porte ilegal de armas**; situación que preocupa teniendo en cuenta que tanto el Gobierno como las FARC-EP han declarado el cese al fuego y de hostilidades bilateral y definitivo.

De acuerdo con el analista son varios los interrogantes que se despliegan de esta situación que perturba el clima positivo que se viene generando en la región para la implementación de los acuerdos de paz, razón por la que **se debe exigir al Ejército Nacional no violar los protocolos** **para el traslado de miembros de las FARC-EP** a los [[sitios que se han acordado para la concentración](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)] y dejación de las armas.  
<iframe src="http://co.ivoox.com/es/player_ej_12704530_2_1.html?data=kpekkpmZd5Ghhpywj5WbaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncafZ09PO0MnTb7Lpys_O0NSJdqSfotOSpZiJhZLgytjW1ZC5tsPVz9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
