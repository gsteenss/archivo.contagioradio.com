Title: En Colombia 10 especies de aves migratorias están en peligro de extinción
Date: 2015-05-11 16:44
Author: CtgAdm
Category: Animales, Nacional
Tags: animales en vías de extinción, aves migratorias, Canadá, colombia, Día Internacional de las Aves Migratorias, Estados Unidos, Humedales, Ministerio de Ambiente, ONU, Unión Internacional para la Conservación de la Naturaleza
Slug: en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/P1010020.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [laschivasdelllano.com.co]

###### [11 de May 2015] 

De acuerdo al Ministerio de Ambiente y Desarrollo Sostenible, en **Colombia existen** **275 especies de aves migratorias, de las cuales 10 están en peligro de extinción** clasificadas por la Unión Internacional para la Conservación de la Naturaleza, UICN.

**El deterioro y desaparición de los hábitats naturales, el cambio climático, la rápida urbanización, la contaminación y el uso insostenible de los espacios naturales** son algunas de las causas que han generado que estas 10 especies de aves migratorias se encuentren en amenaza, de acuerdo a la ONU.

Según las Naciones Unidas, en el mundo "algunas especies podrían extinguirse dentro de una década, mientras que en otros casos se producirían **pérdidas de población del hasta el 9%**”.

De las 10.000 especies de aves en la tierra, **Colombia posee 1.905 de las cuales, 90 únicamente son del país**, además, Bogotá es la capital del país con mayor diversidad de aves con **200 especies**, gracias a la cantidad de humedales y páramos que hay, que son los ecosistemas más usados por estos animales, por lo que 32,8% de las aves acuáticas son especies migratorias que tienen un área total de ocupación de humedal para el interior del país es de 202.525 Km2.

El país cuenta con la ubicación perfecta para que las 275 especies aves pasen por Colombia si van a ingresar a Suramérica. De ellas, **154 llegan de Estados Unidos y Canadá.**

La región del Caribe, los Andes, la Reserva de la Biósfera RAMSAR Ciénaga Grande de Santa Marta, la Isla Salamanca, la Sabana Grande y el Complejo de Humedales costeros de la Guajira, son algunos de los lugares por donde pasan las aves migratorias acuáticas y playeras.

De acuerdo a este panorama, la celebración  del Día Internacional de las Aves Migratorias se realizó este fin de semana en el humedal Santa María del Lago teniendo en cuenta que es crucial conservar y preservar estos hábitats que acogen a las aves durante el año.
