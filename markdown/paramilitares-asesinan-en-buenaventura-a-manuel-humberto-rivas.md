Title: Paramilitares asesinan en Buenaventura a Manuel Humberto Rivas
Date: 2015-10-02 13:45
Category: DDHH, Nacional
Tags: Ataques en Buenaventura, Aumento paramilitarismo, Comisión de Justicia y Paz, Derechos Humanos, Emisora derechos humanos, paramilitares, Paramilitares asesinan a Manuel Humberto RIvas Murillo
Slug: paramilitares-asesinan-en-buenaventura-a-manuel-humberto-rivas
Status: published

###### Foto: contagioradio.com 

###### [2 oct 2015]

El domingo 27 de septiembre a las 8:30 am en Buenaventura, el jefe paramilitar alias “Pepe” junto a alias “Pitufo” y dos **paramilitares** más, quienes **controlan esta zona**, **asesinaron a Manuel Humberto Rivas Murillo** en su casa, en el sector de “la Ruñidera”.

El señor Humberto era padre de cuatro hijos y trabajador de la madera, quien en la madrugada de ese mismo día **salió en defensa de un amigo suyo cuando paramilitares del sector llegaron e intentaron golpearlo.** Alias “Pepe” antes de dispararle le reclamó por lo ocurrido en la madrugada y le **ordenó en presencia de 30 personas que se arrodillara**, ante lo cual Rivas se negó.

Estos hechos se suman a las denuncias que han hecho distintas organizaciones como la Comisión de Justicia y Paz en las que indica que las **violaciones a derechos humanos persisten en Buenaventura** y este año han aumentado. **Ver también:** [Alertan aumento de paramilitarismo en Buenaventura ](https://archivo.contagioradio.com/alertan-aumento-de-la-desaparicion-forzada-y-paramilitarismo-en-buenaventura/)

Este hecho se suma al [asesinato del comerciante Wilder Ubeimar](https://archivo.contagioradio.com/paramilitares-asesinan-a-comerciante-que-se-nego-a-pagar-extorsion-en-buenventura/), quién se negó a pagar una extorsión; la **persecución y desplazamiento** de la familia Aaragón en Buenaventura que resultó con el asesinato de Christian Aragón Valenzuela y Ángel Mina.

Además estas acciones se suman distintas denuncias de ocupación paramilitar que ha realizado la comunidad en sus territorios y frente a lo que la fuerza pública y el gobierno no han tomado medidas para enfrentar esta situación.

Ver también: [DD.HH denuncian ataques en su contra en Buenaventura](https://archivo.contagioradio.com/defensores-de-derechos-humanos-denuncian-ataques-por-paramilitares-en-buenaventura/)
