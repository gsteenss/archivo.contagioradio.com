Title: Arranca último foro sobre el fin del conflicto e implementación de acuerdos
Date: 2016-02-08 12:11
Category: Paz
Tags: Foro Fin Conflicto
Slug: arranca-ultimo-foro-sobre-el-fin-del-conflicto-e-implementacion-de-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Foro-Fin-Conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PNUD Colombia ] 

<iframe src="http://www.ivoox.com/player_ek_10359663_2_1.html?data=kpWgl56aepShhpywj5WWaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncaLm08bbxcaPaaSnhqeuztnNsdCfx9Tf0ZDXs8PmxpDSzpDKrc%2BfxcrZjcjTssfgysjh0ZDJb8rh0dHSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alejo Vargas, UN] 

###### [8 Feb 2016 ]

De acuerdo con la solicitud de la Mesa de Conversaciones entre el Gobierno nacional y las FARC-EP desde este lunes y hasta el miércoles en el Hotel Tequendama de Bogotá, se lleva a cabo el ‘Foro Nacional Fin del Conflicto y Refrendación, Implementación y Verificación’, con la coordinación del Centro de Pensamiento y Seguimiento a los Diálogos de Paz de la Universidad Nacional y de las Naciones Unidas en Colombia, y la participación de 7**00 representantes de todos los sectores de la sociedad colombiana**.

Alejo Vargas, director del Centro de Pensamiento, asegura que la diversidad de los sectores que fueron convocados al Foro “refleja la realidad del país”, tanto organizaciones de víctimas como movimientos sociales y el sector privado se darán cita para socializar sus propuestas sobre la finalización del conflicto y la implementación de los acuerdos, que se convertirán en insumo para las discusiones de la mesa de conversaciones en La Habana. Teniendo en cuenta que “**la inmensa mayoría de los acuerdos han sido aprobados con base en las propuestas de la sociedad civil**”, como asevera Vargas.

Por su parte Juan Martínez, integrante de  la iniciativa Comunidades Construyendo Paz en los Territorios, afirma que desde CONPAZ hay una gran expectativa por este foro en el que propondrán que sean las comunidades más afectadas por el conflicto quienes participen activamente en la implementación de los acuerdos, por lo que mantienen abierta la posibilidad de que en algunas de sus regiones se emplacen los **territorios en los que los miembros de las FARC-EP pagarían las penas alternativas que se han contemplado**.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
