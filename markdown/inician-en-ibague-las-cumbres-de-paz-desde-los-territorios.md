Title: Inician en Ibagué las Cumbres de Paz desde los territorios
Date: 2016-01-29 15:02
Category: DDHH, Nacional
Tags: Cumbre Agraria Campesina Étnica y Popular, Cumbres por la paz, Zidres
Slug: inician-en-ibague-las-cumbres-de-paz-desde-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/CZ5xGmOWwAEixq7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Noticias AEP 

<iframe src="http://www.ivoox.com/player_ek_10251016_2_1.html?data=kpWfl5aUdZehhpywj5WZaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncariysjWw9OPqc%2BfqsfOydrJb83V1JCw19LGtsbnjMnSjbXFvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [César Jeréz, ANZORC] 

###### [29 Ene 2016] 

Iniciaron hoy en Ibague las [Cumbres de Paz](https://archivo.contagioradio.com/inician-las-cumbres-de-paz-por-el-territorio-colombiano/), iniciativa de la Cumbre Agraria, Campesina, Ëtnica y Popular con financiación del gobierno nacional en cumplimiento de los compromisos adquiridos en la negociación resultante de los paros campesinos del Catatumbo en los años 2013 y 2014.

César Jerez, representante de la Asociación de Zonas de Reserva Campesina ANZORC, manifiesta que además de hacer una revisión  al proceso de negociación con el gobierno, el objetivo central de las cumbres será revisar "el anclaje territorial de esa negociación y de lo que serán los eventuales acuerdos finales con las FARC  y el inicio del proceso con el ELN".

En la instalación de la Cumbre, que tuvo lugar en el céntrico parque Murillo Toro, el ambiente fue de total proactividad demostrando las "ganas de aportar al proceso de paz y al fortalecimeinto de la Cumbre Agraria" en el país como señala Jeréz; un clima favorecido por el respaldo del alcalde de la ciudad Guillermo Alfonso Jaramillo a la actividad.

Cerca de 400 integrantes de organizaciones provenientes de diferentes latitudes  de la geografía nacional, harán parte de las mesas de trabajo en estos dos primeros días de realización, a los que siguen otras cumbres que recorrerán varias regiones del país hasta llegar a una gran cumbre Nacional, que tendrá lugar en la ciudad de Bogotá.

Una de los ejes del trabajo durante el desarrollo de la cumbre será la movilización, planificada desde las manifestaciones del año anterior, frente a los incumplimientos justificados en  "la actitud reiterativa del gobierno de dejar pasar el tiempo de tramite mientras aplica un paquete legislativo que es altamente regresivo" tanto para los acuerdo alcanzados como para lo que se negocia en la Habana como señala Jeréz.

El vocero de la organización manifestó además la preocupación surigida por la determinación del gobierno Santos de [instalar las Zidres](https://archivo.contagioradio.com/zidres-legalizan-el-acaparamiento-de-tierras-en-casanare/), con las que se legaliza el despojo y el acaparamiento de tierras, situación que va en contra vía de las acciones que deben emprenderse para alcanzar la paz.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
