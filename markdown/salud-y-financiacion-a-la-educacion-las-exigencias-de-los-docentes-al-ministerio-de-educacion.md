Title: El vía crucis de los acuerdos entre maestros y Ministerio de Educación
Date: 2018-02-21 14:52
Category: Educación, Movilización, Nacional
Tags: educacion, fecode, maestros, Ministerio de Educación
Slug: salud-y-financiacion-a-la-educacion-las-exigencias-de-los-docentes-al-ministerio-de-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jorge-robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jorge Robledo] 

###### [21 Feb 2018] 

Maestros desmintieron la afirmación de la Ministra de Educación Yanhet Gihab, que señaló que la protesta de este miércoles era injustificada y afirmaron que las condiciones laborales y de salud para los docentes siguen siendo pésimas y **que todavía no existen formas concretas de superar la des financiación a la educación del país**.

### **La Salud de los docentes en crisis desde hace más de dos años** 

Una de las exigencias del paro anterior de que duró 33 días, tenía que ver con hacer un cambio en el modelo de licitación de los prestadores del servicio de salud a los maestros y maestras del país. Al finalizar el año pasado, las empresas prestadoras de servicio ya habían sido aprobadas a través de la licitación.

Sin embargo, de acuerdo con Over Dorado, integrante de FECODE, los docentes tienen que afrontar peores situaciones con estas nuevas prestadoras del servicio de salud que antes, **“Medicina general, medicina especializada, camas, lo que tiene que ver con atención médica, ha venido fallando en su generalidad**”. (Le puede interesar: ["Gobierno tiene hasta diciembre para cumplir los acuerdos a docentes"](https://archivo.contagioradio.com/gobierno-tiene-hasta-diciembre-para-cumplir-acuerdos-fecode/))

En el caso específico de Antioquia, los docentes denunciaron que no tienen salud ni para ellos ni para sus familias debido a que no tienen sedes principales o alternas, con las condiciones suficientes para hacerlo. De acuerdo con los docentes, **la pésima atención médica ya cobró la vida de dos docentes en este departamento**.

### **La deuda con la Educación crece** 

En este punto Over Dorado afirmó que ha existido un avance en la mesa de diálogo instalada con el Ministerio de Educación. El desacuerdo en este tema radica en cómo y de dónde gestar el dinero para la financiación de la Educación, esto debido a que el año pasado el Ministerio de Educación afirmaba que había una incapacidad financiera que impedía llegar a un acuerdo con los docentes, **y ahora dice que el problema se encuentra en la ineficiencia del sector**.

Situación que para Over Dorado significa que, en vez de generar mayor presupuesto para la financiación de la Educación, se mirara de que otros rubros se puede sacar dinero para darle a la educación.

“El gobierno hace una adición presupuestal del presupuesto general de la nación para el sector educativo, coge, suple y tapa el hueco, pero ahora como el sistema esta desfinanciado la deuda para el año 2018 es mayor, **el déficit es de 1.2 millones de pesos**” manifestó Dorado. (Le puede interesar: ["Gobierno esta repartiendo la pobreza pero no aumenta presupuesto: FECODE"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/))

### **Las 48 horas de paro** 

Los maestros de FECODE expresaron que el paro de 48 horas tiene como finalidad denunciar públicamente las condiciones y la situación que tienen que vivir los profesores en Colombia y llamar la atención de las instituciones gubernamentales que realmente presenten propuestas certeras sobre las fuentes de financiación para la Educación.

El próximo 23 de febrero ya hay una reunión programada entre una delegación del Ministerio de Educación y los docentes para tratar el tema de **sistema general de participaciones**. Sin embargo Over Dorado afirmó que debe existir una voluntad concreta por dialogar o de lo contrario los esfuerzos serán nulos.

<iframe id="audio_23956391" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23956391_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
