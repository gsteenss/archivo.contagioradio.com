Title: De la responsabilidad moral, política e intelectual en la Comisión Histórica: La carta de Renan Vega a Eduardo Pizarro
Date: 2015-03-03 18:43
Author: CtgAdm
Category: Otra Mirada, Paz
Tags: Comisión de Memoria Histórica del Conflicto y sus víctimas, Conversaciones de paz de la habana, eduardo pizarro, paz, renan vega, víctimas
Slug: la-responsabilidad-moral-politica-e-intelectual-de-la-comision-historica-la-carta-de-renan-vega-a-eduardo-pizarro
Status: published

###### Foto: Contagio Radio 

Renán Vega, integrante de la 'Comisión histórica del conflicto y sus víctimas' que entregó su informe hace unas semanas, manifiesta que Eduardo Pizarro no incluyó varias de sus notas en la relatoría presentada. Según Vega, una vez conocido el borrador de las relatorías hechas por Moncayo y Pizarro, entregó 16 páginas con observaciones de las cuales este último no incluyó ninguna.

Más de un año estuvo la delegación de Paz de las FARC proponiendo el establecimiento de una Comisión de Histórica del Conflicto, que permitiera contextualizar las discusiones que se daban en la mesa de diálogos con el gobierno de Juan Manuel Santos. El jefe negociador, Humberto de la Calle, se negó sistemáticamente. y sólo hasta que quedó claro que la Comisión Histórica propuesta por las FARC sería diferente a la Comisión de la Verdad, que sí tendría la posibilidad de establecer responsabilidades sobre el conflicto colombiano, el gobierno accedió.

Probablemente, no se imaginó el gobierno, como tampoco lo hicieron los investigadores convocados a hacer parte de la Comisión Histórica del Conflicto y sus Víctimas, lo que generaría en la opinión pública la publicación de los resultados de las 12 investigaciones adelantadas por el grupo de expertos. No pasó una semana desde que el Informe viera la luz, antes de que el Ministro de Defensa, Juan Carlos Pinzón, manifestara ante los medios que "aquí no nos pueden salir ahora con verdades alternativas sobre el conflicto"; o que el ex-presidente, Cesar Gaviria propusiera un modelo de justicia transicional en que se vieran cobijados empresarios y ganaderos que financiaron a grupos paramilitares, puestos en evidencia por la Comisión Histórica.

Sin ser un documento definitivo, sin duda el Informe es una síntesis fundamental para el proceso histórico que vive Colombia. Por eso no deja de molestarle al profesor Renan Vega que uno de los relatores, Eduardo Pizarro, "burlara los acuerdos establecidos" para la realización del Informe. Afirma que su relatoría no es "una lectura múltiple y pluralista de la historia", como sentencia el título, y que, por el contrario, carece de responsabilidad moral, política e intelectual.

A continuación, reproducimos completo el documento del profesor vega, y que mantiene abierto el debate sobre el papel de la Historia y de los científicos sociales, en la consecución de la paz para Colombia.

#####  

A propósito de la democracia y el pluralismo  a la colombiana 
-------------------------------------------------------------

#### Así no, señor Eduardo Pizarro 

 

##### “A veces, quedarse callado equivale a mentir, porque el silencio puede ser interpretado como aquiescencia”. 

<div id="TextoNoticia">

##### Miguel de Unamuno, Universidad de Salamanca, 12 de octubre de 1936. 

Después de leer la “relatoría” presentada por Eduardo Pizarro en la Mesa de Diálogos de La Habana he constatado con indignación que es prácticamente igual al borrador que fue entregado el 18 de enero a los comisionados. Esto no tendría ningún problema si quienes leímos esa versión hubiéramos estado completamente de acuerdo y no hubiéramos enviado notas y sugerencias, para ser incorporadas a la versión final, como le corresponde a un relator. Seguramente algunos comisionados no hicieron comentarios al texto, pero ese no fue mi caso, porque el domingo 25 de enero envié un documento de 16 páginas con comentarios a los dos borradores de “relatoría”. Mientras que mis sugerencias fueron aceptadas e incorporadas en el documento de Moncayo, ninguna de ellas fue incluida en el documento de Pizarro. (Ver texto de mi autoría: [Observaciones sobre relatorias.pdf]{}).

Eduardo Pizarro desconoció mis apreciaciones, que suponían hacerle modificaciones a su escrito y solicitarle con argumentos que incorporara los resultados de mi indagación sobre el rol que ha desempeñado Estados Unidos en el conflicto colombiano. Pizarro desconoció las pocas reglas de juego que habíamos fijado por unanimidad los miembros de la Comisión Histórica del Conflicto Armado y sus Víctimas (CHCAV), en las reuniones del 27 de agosto y del 30 de septiembre en la Sede Episcopal, cuando se estableció un calendario de trabajo. En ese cronograma se acordó lo siguiente: entrega de los informes individuales el 21 de noviembre; luego, los relatores presentarían su propuesta de relatoría el 5 diciembre; después, los relatores devolverían el texto a los comisionados que tendrían un plazo de siete días para comunicar sus opiniones por escrito; y, por último, entre el 12 y el 15 de diciembre se harían las correcciones finales a las relatorías y se efectuaría una reunión de cierre para intercambiar comentarios sobre los textos definitivos.

La suspensión de las conversaciones de La Habana por parte del gobierno colombiano, así como problemas de tiempo de los relatores, hicieron que se modificara este calendario interno de trabajo, aunque se mantuvo la fecha de entrega del informe individual, para el 21 de noviembre. No obstante, esto no significó que se cambiaran las reglas de juego que habíamos establecido. Las propuestas de relatorías llegaron a mi correo electrónico el 18 de enero, acompañadas de una nota en la cual se informaba que había plazo hasta el lunes 26 de enero para que cada miembro de la CHCAV hiciera sus respectivos comentarios. En su mensaje Víctor Manuel Moncayo dice de manera textual: “Apreciados colegas: hemos convenido con Eduardo Pizarro remitirles el *proyecto de borrador* que cada uno ha preparado. \[…\] La idea es recibir sus comentarios y observaciones a más tardar el 26 de enero de 2015. *Posteriormente hemos programado una reunión de todos los integrantes de la CHCV el 31 de enero a las [8.am](http://8.am/) en la sede de la conferencia episcopal, para intercambiar ideas sobre las relatorías y las observaciones que se hayan formulado”.* (Correo electrónico, enero 18 de 2015, énfasis mío).

En forma cumplida remití mis comentarios a los dos relatores el domingo 25 de enero y las primeras reacciones me hicieron pensar que serían tenidos en cuenta, pues Eduardo Pizarro en un correo electrónico del 26 de enero habla de “los comentarios que me envía Renán Vega, que voy a estudiar con responsabilidad \[…\]”. Esto me hizo suponer en forma cándida, que eso se iba a hacer, y, en consecuencia, se modificaría el borrador de “relatoría” y se incluirían mis sugerencias, o por lo menos una parte de ellas. No sabemos si las estudió o no, porque al final primó la irresponsabilidad intelectual.

Como era previsible, el sábado 31 de enero Eduardo Pizarro no asistió a la reunión final de la CHCAV y por lo tanto no existió la posibilidad física de discutir su propuesta de relatoría. Porque esa era la finalidad: no dar la cara para no debatir y dejar el borrador tal cual, como efectivamente sucedió.

En esas condiciones, como hecho cumplido, entregó a la Mesa de Conversaciones de Paz de la Habana el mismo texto de su propuesta inicial, desconociendo en forma arrogante mis críticas y aportes.

Para mí, éste es un hecho inaceptable, nada democrático, escasamente transparente, sin la menor muestra de pluralismo y de una muy cuestionable actitud ética, no solo respecto a la Comisión y uno de sus miembros, sino de irrespeto ante la sociedad colombiana, por burlar los acuerdos establecidos y silenciar voluntariamente el mensaje de uno de los comisionados. Para qué hablar tanto de *democracia* y pluralismo, si cuando se necesitó ponerlos en práctica, para incorporar conceptos que no comparte el relator, sencillamente se desconocieron, como si nunca hubieran existido. No sobra recordar que la democracia supone admitir los juicios de quienes piensan distinto y no solo de quienes están de acuerdo. Por eso, resulta tragicómico que al texto de Pizarro se le titule “Una lectura múltiple y pluralista (sic) de la historia”, cuando en realidad no tiene nada de pluralista.

Por todas estas razones, manifiesto mi más enérgico rechazo a la actitud de Eduardo Pizarro por su falta de seriedad, ponderación, equilibrio y rigor en la labor que le encomendó la Mesa de Diálogos de La Habana. Su comportamiento es poco respetuoso del trabajo intelectual, y termina siendo una forma disimulada de censura.

Esto se evidencia en algunas de las declaraciones de prensa del señor Pizarro con posterioridad a la entrega del Informe, entre los cuales podemos citar la siguiente: “las Farc pensaron que el relato histórico de esta comisión iba a favorecer su mirada, de una guerrilla víctima del terrorismo de Estado, que los había obligado a empuñar las armas para resistir y que, por tanto, su levantamiento era legítimo. Pero las Farc fueron sorprendidas porque algunos de los ensayistas controvirtieron ese relato histórico”. (Citado en Hernán González Rodríguez, “Causas y orígenes del conflicto”, *El Espectador,* 26 de febrero de 2015, disponible en <http://www.elespectador.com/opinion/causas-y-origenes-del-conflicto-columna-546504>).

[]{#_GoBack}Esta afirmación poco ponderada genera la impresión que la visión dominante sobre la historia contemporánea de Colombia es la de la insurgencia y no la del Estado y las clases dominantes y por eso se presenta como un éxito que algunos de los ensayistas reprodujeran la versión oficial, que niega el terrorismo de Estado. Pero lo que oculta conscientemente el señor Pizarro en su falsa relatoría es que mi postura –en contravía de las versiones dominantes de académicos y violentologos ligados al Estado y a las clases dominantes– se centra en analizar la contrainsurgencia y el Terrorismo de Estado predominante en Colombia, como elemento sustancial del comportamiento del bloque de poder contrainsurgente. En este sentido, el Estado, las clases dominantes y sus violentologos fueron sorprendidos con otra visión sobre los orígenes, causas y factores que explican el conflicto armado en Colombia, en la que precisamente el Estado colombiano, sus Fuerzas Armados y los Estados Unidos no salen bien parados. Entre otras cosas, eso explica el silenciamiento por los medios periodísticos y sus columnistas de cabecera de esa visión crítica y alternativa a las miradas convencionales sobre la historia reciente de Colombia.

Finalmente, en la actitud de Eduardo Pizarro de desconocer los aportes que yo hice a su propuesta de “relatoría” encuentro que se manifiestan dos elementos propios de la antidemocracia colombiana, como son *la arrogancia de los poderosos* y *el desconocimiento de la palabra empeñada.* Arrogancia de los poderosos porque aparte de que este personaje hacia ostentación continua de su carácter de Embajador ante los Países Bajos (Holanda), a la larga actuó de la misma forma que lo hacen todos aquellos que tienen una pequeña cuota de poder (un micropoder) en Colombia y que consiste en no escuchar a los que carecen de poder y pisotear sus apreciaciones, como si no tuvieran el más mínimo valor. Desconocimiento de la palabra empeñada, puesto que igual que el Presidente de la República cuando le conviene echa por la borda los pactos existentes, convierte su palabra en papel mojado y suspende en forma inconsulta y unilateral los diálogos de La Habana, lo mismo hace su subalterno Eduardo Pizarro al no respetar los acuerdos establecidos.

Resultan insoportables los medios antidemocráticos y poco pluralistas del señor Pizarro, quien, con una gran dosis de cinismo, en los medios de comunicación del poder (RCN, Caracol, *El Tiempo,* *Semana*) figura como el campeón de la democracia y el pluralismo, así como el portaestandarte de una supuesta responsabilidad moral, política e intelectual. ¿Será que tenemos que aceptar, de la misma forma que lo hace el gobierno -cuándo a su acomodo y en forma arbitraria deja de cumplir su palabra y suspende las conversaciones de paz- que uno de los relatores no respete la palabra empeñada? ¿Eso es lo que nos espera en el futuro, un absoluto irrespeto de los acuerdos firmados?

Renán Vega Cantor

Miembro de la Comisión Histórica del Conflicto y sus Víctimas

</div>
