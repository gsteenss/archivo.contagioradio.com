Title: "Delimitación de páramos debe ser concertada con las comunidades": Ambientalistas
Date: 2018-01-24 14:56
Category: Ambiente, Nacional
Tags: delimitación de páramos, titulos mineros
Slug: delimitacion-de-paramos-debe-ser-concertada-con-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/almorzadero_paramo7.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Eliécer Sánchez] 

###### [23 Ene 2018] 

Durante el fin de semana fue noticia, la firma de Juan Manuel Santos, que dio el aval para la delimitación de siete páramos del país: el de Perijá, en Cesar; Almorzadero, entre Santander y Norte de Santander; Citará, en los límites de Antioquia y Chocó; los nevados de Sotará y Guanacas-Puracé-Coconucos, en Cauca y Huila y, el de Doña Juana-Chimayoy en Cauca, Nariño y Putumayo.

“Con estas nuevas delimitaciones alcanzamos **2.064.091 hectáreas y seguimos cumpliendo con el compromiso de nuestra meta**. Esto hace parte de las acciones en materia de conservación y que son complementarias a nuestra meta de dejar más de 30 millones de hectáreas de áreas protegidas al final de este gobierno”, dijo el Presidente Santos.

Con bombos y platillos se anunció que el gobierno ha cumplido con la delimitación de 30 de los 37 ecosistemas que tiene **Colombia, teniendo en cuenta que el país posee el 50%** de los páramos del mundo. Sin embargo, organizaciones ambientalistas alertan sobre la necesidad de que se tenga en cuenta la participación de las comunidades en esas delimitaciones, pues este es uno de los aspectos que no se tienen en cuenta, y por lo cuál se han derivado todos los problemas relacionados con la fallida delimitación del Páramo de Santurbán. (Le puede interesar: [¡Santubán no está en venta!)](https://archivo.contagioradio.com/en-santander-recalcaron-que-santurban-no-esta-en-venta/)

**La participación de las comunidades**

La delimitación permite que se cree un blindaje sobre esa área, e implica que se desarrolle un plan de ordenamiento y manejo dentro de la zona. Sin embargo, de acuerdo con Margarita Flores, directora de la Asociación Ambiente y Sociedad, el problema histórico con las delimitaciones es que **no se ha tenido en cuenta la participación de las comunidades que durante décadas han hecho presencia en esos territorios**, ya sea porque encontraron oportunidades económicas a partir de la agricultura, o porque son víctimas de desplazamiento, entre otras razones.

"Creo que tener un nivel de protección es bueno para el ecosistema y da seguridad a las personas que allí viven para que mediante reconversiones de sus actividades pudieran desarrollar labores compatibles con el ecosistema y su conservación", explica Flores.

En esa misma línea, Juana Hofman, directora de Justicia Ambiental, también ve como prioridad la participación de los pobladores. No obstante, llama la atención sobre la necesidad de tener en cuenta que la sentencia de la Corte Constitucional sobre esa materia. Allí se dicta que se debe involucrar a todos los actores, es decir, no solo a las comunidades sino también a las empresas que han tenido títulos mineros, l**o cual abre una puerta a que no se garantice el blindaje del ecosistema, y en cambio, prevalezcan intereses económicos.**

"¿Cuál es el sentido de la participación? no significa entrar a concertar lo científico, de lo cual ya tenemos certeza. De lo que se trata es de reconocer aquello que allí ha estado en este tiempo", dice Hofman.

**El problema de la entrega de títulos mineros**

Lo importante con la delimitación es que se crea el acto administrativo en el cual se dice qué, dónde, y cómo son las actividades compatibles con dichos territorios. En ese sentido, sobre lo que sí se ha manifestado expresamente la Corte Constitucional, es sobre la prohibición de actividades mineras en zonas de páramo.

El problema es que, cuando en el año 2016 el alto tribunal tomó esa decisión, la Agencia Nacional de Minería informó que estaban vigentes 473 títulos mineros en páramos, 284 en fase de explotación, 136 en construcción y montaje y 53 en exploración. Esto, teniendo en cuenta que en los ocho años de gobierno de Álvaro Uribe fueron entregados **391 títulos mineros sobre páramos en cerca de 108.000 hectáreas, pero además esta política se intensificó en con la locomotora minero-energética con Juan Manuel Santos.**

Al respecto, Margarita Flores califica esa desenfrenada forma de entregar títulos mineros, como "un pecado original", pues se dieron "sin ningún criterio ambiental". En esa línea, propone cambiar le régimen del Código de Minas pues es "obsoleto", y por tanto, "se deben desconocer los títulos mineros si existen razones ambientales y sociales", expresa. (Le puede interesar: [Hay más de 400 títulos mineros en páramos)](https://archivo.contagioradio.com/400-tituloa-mineros-seran-retirados-por-fallo-de-la-corte-constitucional/)

### **¿Pago por servicios ambientales?** 

Teniendo en cuenta que" delimitar a los páramos, no es sacar a la gente", se debe pensar en un mecanismo para compensar a la gente que en sus manos tiene la protección del ambiente. Es así, como la directora de Ambiente y Sociedad propone el pago por servicios ambientales. "Si hay fondos para compra de predios, se debería invertir en proyectos de manejo sostenible de páramos**. Si hay plata para servicios ambientales, deben ser recursos para la gente que ayuda a conservar el ecosistema".**

<iframe id="audio_23345507" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23345507_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
