Title: Zonas Veredales se convertirán en espacios de estudio y salones de clase
Date: 2017-08-04 13:05
Category: Nacional, Paz
Tags: acuerdos de paz, comunidades, educacion, ex combartientes, FARC, Zonas Veredales
Slug: zonas-veredales-se-convertiran-en-zonas-de-estudio-y-salones-de-clase
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/bibliotecas-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 Ago 2017]

El Ministerio de Educación, integrantes de la FARC-EP, la Universidad Abierta y a Distancia y el Consejo Noruego para los Refugiados, presentaron la iniciativa “Arando la educación” que busca que **4500 ex combatientes de las FARC y 4000 habitantes de comunidades aledañas a las Zonas Veredales puedan completar sus estudios** de formación básica y secundaria.

El programa contempla que **118 tutores se trasladarán a 26 Zonas Veredales y Puntos transitorios de Normalización** “para iniciar la fase de implementación de los ciclos educativos”. El programa iniciará el 7 de agosto y está enfocado a que los estudiantes sean mujeres y hombres entre los 18 y 28 años. (Le puede interesar: ["Sale primer grupo de menores de Zonas Veredales transitorias"](https://archivo.contagioradio.com/sale-primer-grupo-de-menores-de-zonas-veredales-transitorias/))

Para Jairo Quintero, miembro del Consejo de Reincorporación de las FARC, el proceso de construcción del programa **tuvo dificultades, pero se logró desarrollar en 3 ejes.** Él explico que el primer eje fue realizar una caracterización de la población de ex combatientes para “establecer el margen de analfabetismo y de acuerdo a eso se elaboró el programa”.

El segundo eje fue “echar a andar el proceso de alfabetización” y por último construyeron un modelo de educación para la paz que incluye un **lenguaje que “se ajusta a las condiciones actuales de las diferentes Zonas Veredales del país”**. Quintero manifestó que este proceso debe ser diferenciado y debe contribuir a saldar la deuda del Estado con las comunidades más vulnerables. (Le puede interesar: ["Nueva ola de solidaridad con las Zonas Veredales transitorias"](https://archivo.contagioradio.com/nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias/))

Los ponentes manifestaron que este programa educativo será un **primer paso para lograr que se avance en la cadena formativa** de los excombatientes de las FARC y así ellos y ellas puedan desarrollar actividades afines a sus habilidades y capacidades. Ante esto, Quintero informó que “se va a iniciar un trabajo en cada Zona Veredal que tenga en cuenta las necesidades y problemáticas específicas de cada territorio para crear programas especiales y dirigidos para cada una de las Zonas”.

De igual forma, el Vice Ministro de Educación, Pablo Jaramillo, manifestó que el **programa educativo se desarrollará en 6 ciclos flexibles** para que los estudiantes puedan completar los estudios básicos de primaria y secundaria. Jaramillo afirmó que el programa ya está en funcionamiento en Chocó, Nariño y Cauca. (Le puede interesar: ["Zonas Veredales contarán con bibliotecas y ludotecas para la paz"](https://archivo.contagioradio.com/zonas-verdales-contaran-con-biblioteca-y-ludoteca-para-la-paz/))

### **Dificultades que tuvo la creación del programa** 

Según Jairo Quintero, el proceso de elaboración del programa **“tuvo 4 meses de tropiezos”**. Para ellos, existe una preocupación en cuanto que el Ministerio de Educación, “no tiene políticas para el tema de reincorporación. El Ministerio tiene programas y propuestas para desarrollar procesos de educación para adultos pero no para el procesos de formación y educación de ex combatientes que, es una población con unas particularidades y condiciones distintas”. (Le puede interesar: ["Hay un 80% de incumplimientos por parte de gobierno en Zonas Veredales: FARC"](https://archivo.contagioradio.com/gobierno-ha-incumplido-en-un-80/))

De igual forma, manifestó que las Zonas Veredales aún no cuentan con las condiciones de infraestructura y de comunicación necesarias para el desarrollo de estos procesos. Sin embargo, rescataron que la formación se haga de forma presencial de la mano de tutores y esperan que los ex combatientes y habitantes de las regiones aledañas a las Zonas Veredales, **puedan tener acceso a una educación de calidad que fortalezca los procesos de reincorporación.**

<iframe id="audio_20171661" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20171661_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
