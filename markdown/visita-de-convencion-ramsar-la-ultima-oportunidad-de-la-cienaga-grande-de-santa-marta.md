Title: Visita de Convención Ramsar: La última oportunidad de la Ciénaga Grande de Santa Marta
Date: 2016-08-22 16:15
Category: Ambiente, Nacional
Tags: Ambiente, Ciénaga grande Santa Marta, Humedales, Ramsar
Slug: visita-de-convencion-ramsar-la-ultima-oportunidad-de-la-cienaga-grande-de-santa-marta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cienaga-Grande-de-Santa-Marta-e1461004237548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Unal 

###### [22 Ago] [2016] 

Expertos de la Convención de Ramsar, tratado internacional para la protección de los humedales, realizarán una visita del **22 al 26 de agosto a Colombia para evaluar la situación de la Ciénaga Grande de Santa Marta**, y plantear las recomendaciones necesarias para salvar el complejo de humedales más importante del país, que **en las últimas semanas se vivió una mortandad de cerca de 200 toneladas de peces.**

La misión de asesoramiento de expertos es un mecanismo que tiene la convención de humedales cuando los países inscritos solicitan la visita para recibir las sugerencias pertinentes cuando uno de estos ecosistemas se encuentra en peligro. En Colombia, esta solicitud se vio motivada por un esfuerzo desde hace más de 2 años por parte de la Asociación Interamericana para la Defensa del Ambiente (AIDA), junto a la Universidad del Norte y la Universidad de la Florida quienes alertaron sobre la grave situación de la Ciénaga Grande de Santa Marta.

Motivado por esa preocupación internacional, el gobierno colombiano ha respondido y han solicitado la misión de asesoramiento que empieza este 22 de agosto con reuniones institucionales en Bogotá, para luego desplazarse hacia el humedal y ampliar la información sobre la situación de la manos de los actores que hacen parte de ella, tales como los pescadores, palmeros, el gobernador e incluso el Ministro de Ambiente, Luis Gilberto Murillo.

“La situación es de calamidad, la ciénaga está en un colapso ecológico, es un paciente que está en cuidados intensivos”, expresa la profesora de la Universidad de Magdalena, Sandra Vilardy, quien asegura que además de ser una crisis ambiental, **se trata también de una calamidad humanitaria, pues a partir del deterioro ecosistémico se evidencia la pobreza y condiciones de vida de los pobladores**.

Vilardy explica que hay que hacer una revisión profunda de la gestión de la Ciénaga. En ese sentido es primordial que sea incluida en la lista de Ramsar de los humedales más amenazados del mundo, y se empiece una gestión asociada a la restauración de flujos hídricos y del bosque de manglar de manera activa, para lo cual sería necesario recursos financieros importantes, teniendo en cuenta que el grave daño ambiental que se dejó de lado durante años.

Aunque se trata de una convención vinculante pero voluntaria, la profesora de la Universidad del Magdalena, afirma creer en la “buena voluntad” por parte del gobierno nacional, ya que **nunca antes un Ministro de Ambiente, había demostrado tal compromiso con la Ciénaga.**

<iframe src="http://co.ivoox.com/es/player_ej_12628762_2_1.html?data=kpejlJ2bepOhhpywj5aWaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbTVz8nfw5C6rc3V08nmh5enb7biytvS1NjNqMLYjMnSjbLFq8XVzcrbw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
