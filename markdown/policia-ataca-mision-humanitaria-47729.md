Title: Policía ataca a misión humanitaria en el lugar de la masacre de Tumaco
Date: 2017-10-08 18:04
Category: Nacional
Tags: Misión Humanitaria, Naciones Unidas, Tumaco
Slug: policia-ataca-mision-humanitaria-47729
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/mision-humanitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagio radio] 

###### [08 Oct 2017] 

Misión humanitaria que se dirigía hacia el lugar donde sucedió la masacre en Tumaco fue atacada a disparos por integrantes de la fuerza pública. Fueron víctimas de amenazas por parte del ESMAD, policías encapuchados grabaron los rostros de los y las participantes, y evidenciaron que árboles en los que se notan los impactos de bala en el sitio de la masacre están siendo talados.

Además la policía está ocupando un bien civil y ha impedido que la familia propietaria llegue puesto que allí se dio la conocida Masacre de Tumaco.

El hecho sucedió hacia las 2 p.m de este domingo, cuando la misión en compañía de periodistas, entre ellos una integrante de Contagio Radio avanzaban al lugar, en videos grabados es ese momento se escuchan disparos y los gritos de los integrantes de la misión humanitaria que a viva voz dicen los nombres de las organizaciones que allí estaban.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154869736675812%2F&amp;width=800&amp;show_text=false&amp;appId=894195857389402&amp;height=337" width="800" height="337" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A pesar del llamado de la misión a no disparar las detonaciones continuaron y las personas que allí estaban tuvieron que correr y atravesar  la Quebrada La Honda, para llegar al finalmente al caserío El Tandil, según el comunicado de la organización Comisión Intereclesial de Justicia y Paz.

La misión había notificado con anterioridad su visita al lugar, igualmente los funcionarios y defensores de derechos humanos contaban con prendas que identificaban las organizaciones de las que hacían parte, entre ellos estaban las insignias de Naciones Unidas.

Según el reporte de nuestra periodista,  la policía hizo los disparos desde una distancia menor a 15 metros hacia el lugar donde se encontraba la misión y no hacia al aire como lo han manifestado otras versiones. [Lea también Informe forense desmiente versión oficial sobre masacre de Tumaco](https://archivo.contagioradio.com/informe-forense-contradice-version-del-gobierno-en-masacre-de-tumaco/)

El comunicado de la Policía sobre los hechos manifiesta que los uniformados lanzaron dos granadas de aturdimiento porque integrantes de la misión entraron a la fuerza a una parte de la base militar que allí se encontraba, el informe no señala el uso de disparos, aunque en los videos tomados por Contagio Radio se escuchan las detonaciones. En comunicación con las ONG´s estas han manifestado como injustificable el documento policial, que según ellas  justifica la extralimitación de las funciones de los uniformados.

La visita que se realizaba el día de hoy tenia por objetivo verificar y dialogar con las comunidades sobre los hechos ocurrido el pasado 5 de octubre que dejaron un saldo de seis personas muertas, según el informe de la Medicina Legal. [Lea también 9 personas muertas y 18 heridas en Tumaco](https://archivo.contagioradio.com/9-personas-erradicacion-forzada-tumaco/)

La  misión está conformada por integrantes de CPDH, Justapaz, CONPAZ, Corporación Yira Castro, Minga, Somos Defensores, Diócesis de Tumaco, y Comisión de Justicia y Paz, junto con delegados de la Personería Municipal, la Oficina del Alto Comisionada de Derechos Humanos y la MAPP OEA y periodistas de la Revista Semana, El Espectador.

Este uno de los momentos del ataque policial a defensores y periodistas, hace unas tres horas en lugar de Masacre Tumaco. [pic.twitter.com/EOWF7W93D8](https://t.co/EOWF7W93D8)

> — Justicia y Paz (@Justiciaypazcol) [8 de octubre de 2017](https://twitter.com/Justiciaypazcol/status/917148337169731584?ref_src=twsrc%5Etfw)

### **ESMAD amenazó a misión humanitaria** 

Según el informe de las organizaciones no solamente fueron víctimas de disparos, sino que también, minutos antes de emprender la caminata hacia el predio en que se dieron los hechos el grupo de unas 15 personas y no 200 como señala una de las versiones, fue interceptado por un agente del ESMAD que los amenazó, advirtiendo que si continuaban les podía pasar algo.

Adicionalemnte ya acercándose al sitio la misión se percató de que habían hombres encapuchados, con prendas de uso policial y junto a policías que si tenían el rostro descubierto. Al ser preguntados por los efectivos policiales encapuchados no hubo respuesta.

### **Más detalles del operativo policial** 

 A pesar de la identificación de los integrantes de la Misión Humanitaria la policía gritaba "para afuera" impidiendo así la realización de la labor de los defensores de DDHH y periodistas en la misión. Según uno de los integrantes de la misión que alcanzó a ver a los uniformados uno de ellos sacó un arma y disparó, luego de ello fueron varios lo disparos que habrían hecho contra los civiles y no al aire.

En el comunicado de la policía también se afirma que los integrantes de la misión humanitaria intentaron entrar por la fuerza a una base, lo que no es cierto dado que la llamada "base" incluye la casa de una familia y es justamente el sitio en el que se presentó la masacre y en la que pueden haber más evidencias de los hechos.

De hecho, el propietario del inmueble pensaba ingresar a su casa a sacar ropa aprovechando la presencia de las autoridades civiles y los defensores de DDHH, sin embargo la reacción policial lo impidió. Este hecho constituye una violación al derecho internacional humanitario dado que no se permite que personal policial o militar ocupe un bien civil.

###### Reciba toda la información de Contagio Radio en [[su correo]

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

