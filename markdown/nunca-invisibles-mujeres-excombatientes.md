Title: Nunca invisibles, la apuesta política de mujeres excombatientes
Date: 2018-11-01 15:56
Author: AdminContagio
Category: Expreso Libertad
Tags: FARC-EP, mujeres
Slug: nunca-invisibles-mujeres-excombatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/descarga-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Partido FARC 

###### 23 oct 2018 

**Nunca Invisibles** es la apuesta política de mujeres ex prisioneras políticas y excombatientes del movimiento político FARC, que busca generar **procesos de memoria frente a la historia de las mujeres**, sus hazañas, retos y logros durante los más de 50 años del conflicto armado en el país.

En este Expreso Libertad, escucharemos los testimonios de **4 mujeres que desde sus roles al interior de FARC le apuestan a la construcción de memoria feminista en Colombia**.

<iframe id="audio_29779345" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29779345_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
