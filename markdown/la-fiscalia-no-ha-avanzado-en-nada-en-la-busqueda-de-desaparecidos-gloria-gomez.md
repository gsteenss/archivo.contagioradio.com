Title: 23.441 personas han desaparecido de manera forzada en Colombia
Date: 2016-05-25 12:55
Category: DDHH, Nacional
Tags: Desaparición forzada, Fiscalía General de la Nación, víctimas
Slug: la-fiscalia-no-ha-avanzado-en-nada-en-la-busqueda-de-desaparecidos-gloria-gomez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/la_escombrera_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ZonaCero 

###### [25 May 2016] 

A partir del **23 de mayo hasta el 31 del mismo mes**, se conmemora la Semana Internacional de los Detenidos Desaparecidos "Por la Paz y la Dignidad entrega de los Desaparecidos Ya" que contará con diferentes actividades en varías ciudades del país. Según los últimos datos de Medicina Legal hay 23.441 personas desaparecidas de manera forzada en Colombia.

Esta conmemoración tiene como fin **visibilizar y reconstruir memoria de cada una de las personas víctimas de la desaparición** y se ha convertido en una de las fechas históricas del movimiento de familiares y sobrevivientes a nivel global, sin embargo en el país los avances en este tema son nulos.

De acuerdo con Gloria Gómez, presidenta de ASFADDES (Asociación de Familiares de Detenidos y Desaparecidos), en cuanto a la búsqueda y ubicación de las personas desaparecidas **con vida no se ha logrado nada**, puesto que se han entregado son los  cuerpos sin vida en situaciones que aún no tienen motivos de esclarecimiento. [A su vez señala que los organismo de investigación no asumen con prontitud la búsqueda](https://archivo.contagioradio.com/casos-de-desaparicion-forzada-en-colombia-cuentan-con-99-9-de-impunidad/)y el hallazgo de las personas y tampoco se establece presuntos responsables, dejando las desapariciones en la impunidad.

Frente al acuerdo especial para la búsqueda de personas desaparecidas que se firmó este año, Gloria Gómez indicó que en el caso de las acciones de confianza no se han presentando progresos, debido a que la [Fiscalía sigue realizando entregas masivas, no realiza una entrega digna de los cuerpos y no ha garantizado un debido proceso,](https://archivo.contagioradio.com/inoperancia-del-estado-no-ha-permitido-justicia-y-reparacion-para-las-victimas-de-desaparicion-forzada/) ejemplo de ello es el caso de Cimitarra en donde las excavaciones se realizaron con retroexcabadora, hecho que **no protegió los cuerpos encontrados y que dejó sin garantías la cadena de custodia.** Acciones como esta entorpecen los procesos y evidencia que la Fiscalía va en contra de los protocoles y acuerdos.

Por otro lado afirma que esta lucha ha servido para visibilizar el fenómeno de la desaparición forzada en el país como un **drama que viven miles de familias Colombianas** y que esta conmemoración servirá para reflexionar en torno a cómo lograr materializar los acuerdos realizados.

Las actividades se harán en ciudades como Bucaramanga, Barrancabermeja y Bogotá en donde se llevarán acabo galerías de la memoria y y foros sobre la dignidad y el drama de las víctimas de desaparición en Colombia.

<iframe src="http://co.ivoox.com/es/player_ej_11663826_2_1.html?data=kpajmJicdpehhpywj5aVaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncajg0NfWw5Crs87Z25Cajaa3iqK4parAj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
