Title: 'Gasimba', 'bacán', 'camello' hacen parte del nuevo Diccionario de Colombianismos
Date: 2016-12-20 12:18
Category: Cultura, Otra Mirada
Tags: Colombiansmos, Diccionario, Español, Instituto Caro y Cuervo
Slug: gasimba-bacan-camello-hacen-parte-del-nuevo-diccionario-de-colombianismos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Colombianismos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Caro y Cuervo] 

###### [20 Dic. 2016] 

**En el mundo hay cerca de 560 millones de personas que hablan español** según el Instituto Cervantes, incluidos aquellos quienes tienen esta lengua como nativa, los que son estudiantes de español como lengua extranjera y quienes la han aprendido y tienen un uso limitado de esta.

**Colombia con 48 millones de hispanoparlantes ocupa el tercer lugar de países con más hablantes de este idioma**. Sin embargo, cada lugar cuenta con sus particularidades dentro del uso de la lengua.

El Instituto Caro y Cuervo de Colombia, dio a conocer recientemente **el nuevo 'Diccionario de Colombianismos' en los que las personas podrán encontrar diversidad de palabras y expresiones** que se usan en la actualidad, pero que en la mayoría de los casos hacen parte de la memoria que a través de cientos de generaciones ha sido heredada.

**Este nuevo diccionario contiene cerca de 10 mil expresiones y se ha asegurado que estará listo para finales de 2017.** Mientras tanto el Caro y Cuervo ha dado a conocer algunas palabras con su significado, pero además ha hecho toda una campaña de expectativa.

Para la directora del Caro y Cuervo lo que hay que destacar de **este diccionario es que las personas que lo  lean pueden recordar a sus abuelos, a sus amigos u otras épocas a través de diversas palabras que pueden ser encontradas allí.**

Compartimos algunas palabras que están incluidas en el nuevo diccionario:

-   **Amigovio, via.** s./adj. inf. juv. Persona con la que se tiene una relación afectiva menos formal que el noviazgo.
-   **Viringo, ga** (tb. beringo, biringo, veringo). adj. Referido a una persona, desnuda.
-   **Bacán:** Hombre bueno, simpático, amable. Forma de tratamiento usada entre amigos y compañeros. (Ñero, parce, parcero).
-   **Camello:** Trabajo, empleo. Tarea dura y poco agradable.
-   **Gasimba:** Bebida embotellada o enlatada (gaseosa).
-   **En bombas:** Rápidamente, al instante.
-   **Aceite (medir el):** Comprobar la capacidad de otro, sometiéndolo a una prueba o situación difícil para determinar su desempeño.
-   **Botaratas:** Referido a una persona generosa, que le agrada gastar dinero.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
