Title: Bucaramanga se movilizará en defensa del páramo de Santurbán
Date: 2017-06-22 19:39
Category: Ambiente, Voces de la Tierra
Tags: Bucaramanga, santurbán
Slug: bucaramanga_movilizacion_santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/PARAMO-DE-SANTURBAN-e1461876510933.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ola política 

###### [22 Jun 2017] 

Bucaramanga se prepara para marchar en contra los  proyectos mineros sobre el Páramo de Santurbán. Así lo estableció la población mediante una **asamblea ambientalista en la que se decidió que el 6 de octubre se hará dicha movilización.**

La marcha se organiza en el marco de la nueva **amenaza minera por parte de la Sociedad Minera de Santander, Minesa.** Empresa que está apunto de iniciar ante Autoridad Nacional de Licencias Ambientales, ANLA, el trámite para obtener la licencia ambiental que le daría vía libre al proyecto de explotación ‘Soto Norte’, muy cerca al Páramo de Santurbán. **Un proyecto que busca explotar durante 23 años nueve millones de onzas en concentrados de pirita y cobre.**

Además esta semana se conoció sobre los convenios que firmó la Universidad Industrial de Santander, UIS, con la empresa árabe Minesa, que despiertan las sospechas de la comunidad estudiantil. Ante dicha amenaza, el Comité de Santurbán invita a conformar una **Gran Alianza Ciudadana por el Agua de la región, con énfasis en la protección del páramo.**

Entre las actividades acordadas se estableció la realización de diversos espacios debate, foros y conversatorios. Además habrá un **debate de control político que se realizará en la Asamblea departamental el 28 de junio a partir de las 8:00** de la mañana donde también se hará el lanzamiento de la Gran Alianza Ciudadana por el Agua el próximo 13 de julio.

Cabe recordar que “la unidad de toda la sociedad, condujo en el pasado al hundimiento del proyecto de GreyStar y hoy se está convocando a la misma unidad para derrotar el presupuesto ilimitado de Minesa, mediante la movilización pacífica, la presión ciudadana y un debate amplio e informado en todos los espacios”, dice el Comité de Santurbán en un comunicado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
