Title: Senadores pretenden aplicar "operación tortuga" a jurisdicción de paz
Date: 2017-03-10 12:51
Category: Paz
Tags: Fast Track, Jurisdicción Especial de Paz, Senado, voces de paz
Slug: fast-track-operacion-tortuga-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:honduradio] 

###### [10 Mar 2017]

Parece que a la JEP y al SIVJRR se le estuviera aplicando una "operación tortuga" lo que según Voces de Paz e Iván Cepeda, es evidente luego del “bochornoso” debate en el que se hiceron evidentes los insultos, el ausentismo, la indisciplina y los vícios en torno a los impedimentos entre otras dilaciones.

Tanto “Voces de Paz” como el Iván Cepeda, señalaron que es necesario aumentar la veeduría social a las discusiones del Fast Track, **puesto que durante la campaña electoral será aún más difícil avanzar.** [(Lea también: Todo sobre la JEP)](https://archivo.contagioradio.com/page/2/?s=jurisdicci%C3%B3n+especial+de+paz)**  
**

### **La veeduría social será fundamental en el próximo debate de la JEP** 

<iframe src="https://co.ivoox.com/es/player_ek_17476696_2_1.html?data=kpyhmZuafZehhpywj5aYaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjIqfpZC0s83jjKnSz9THtoa3lIquk9nNp9Chhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ivan Cepeda, congresista del Polo Democrático, señaló que se espera que el lunes 13 hagan presencia en el auditorio de la plenaria del senado la mayor cantidad de personas posibles para presionar a los demás congresistas y lograr un avance efectivo en la discusión. ([Lea también: Cambios en JEP abrirían la puerta a la Corte Penal Internacional](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/))

Igualmente también habría que presionar para que se respeten los términos del acuerdo en torno a **garantizar el derecho a la verdad** que se vería afectado por la insistencia en eliminar las responsabilidades de los militares en la cadena de mando y la posibilidad de que los terceros en la guerra no comparezcan ante esos tribunales.

Para Cepeda una de las principales dificultades que se afrontan en este momento es el clientelismo que gira alrededor de las discusiones, los intereses particulares y de partidos, no solamente de cara a la campaña sino para lo que queda del gobierno y en el presupuesto de la paz que se debe comenzar a ejecutar. ([Le puede interesar: Algunos buscan impunidad en JEP](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-2/))

### **Fast Track en "Operación tortuga"** 

<iframe src="https://co.ivoox.com/es/player_ek_17476746_2_1.html?data=kpyhmZubeJehhpywj5acaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncavVytfcjarXuNPVxcaSlKiPmtDXxtiYxsqPtMLujoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte Jairo Estrada, señala que es evidente que esta discusión en torno a la JEP tiene fuertes implicaciones para sectores políticos y económicos, por ello es que se estaría aplicando una “operación tortuga” para presionar cambios sustanciales en el proyecto original como los señalados por Cepeda. ([Lea también: Ultra derecha y empresarios le temen a la JEP](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/))

Frente a la actitud de algunos congresistas en la llamada "operación tortuga" Estrada señala que serán los hechos y los resultados los que permitan evaluar la actitud y las posiciones de los partidos frente al trámite de la JEP, pero también de la Comisión de la Verdad y de la Unidad de Búsqueda de personas desaparecidas, **tres elementos que hacen parte del Sistema Integral de Verdad, Justicia, Reparación y No Repetición.**

Estrada, representante de Voces de Paz en el senado, resalta que seguramente el martes estaría aprobado este proyecto, además viene el trámite de las **10 circunscripciones especiales lo cual, por lo menos, va a garantizar la presencia de caras nuevas** en el legislativo, y seguramente un cambio de ritmo y los temas en las discusiones de unos próximos periodos.

Lo cierto es que los proyectos de implementación de los acuerdos de paz con las FARC están avanzando, a paso lento, pero avanzando y depende de todos y todas acompañar esa implementación que serán la posibilidad de cambios en un futuro próximo, señalaron tanto Cepeda como Estrada.

###### Reciba toda la información de Contagio Radio en [[su correo]
