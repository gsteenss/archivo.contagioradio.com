Title: Gobierno está con las multinacionales al frenar consultas populares: R García
Date: 2017-10-25 16:41
Category: Ambiente, Nacional
Tags: consultas populares, Registraduría Nacional
Slug: gobierno-esta-las-multinacionales-al-frenar-consultas-populares-r-garcia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2Orillas] 

###### [25 Oct 2017] 

Organizaciones Ambientalistas **rechazaron las diferentes iniciativas impulsadas en conjunto entre el alto gobierno y algunos senadores**, que pretenden limitar el mecanismo de la consulta popular, entre las estrategias esta el proyecto que ya fue presentado ante la CSIVI, , condicionando el uso de este mecanismo a la realización de cabildos abiertos, convocados por autoridades municipales y una iniciativa del senador Roy Barreras.

Uno de esos senadores es Roy Barreras, que ha impulsado un proyecto de ley para que se realicen audiencias públicas informativas que establezcan un diálogo en las comunidades, debido a que según el las consultas se estarían realizando sin pleno conocimiento por parte de los habitantes de los efectos de la misma. (Le puede interesar: ["Aprobada consulta popular en El Castillo, Meta"](https://archivo.contagioradio.com/aprobada-consulta-popular-en-el-castillo-meta/))

Renzo García, integrante del Comité Ambiental del Tolima, señaló que esta afirmación no es cierta y que **mientras las comunidades están saliendo a defender su cultura tradicional, la vocación agrícola del territorio y el ambiente, el gobierno está defendiendo el interés de las multinacionales**, “las comunidades que han venido realizando estos procesos cuentan con la suficiente información para poder decirles no a estos proyectos”.

Además, de acuerdo con Renzo, este proyecto tampoco es claro con lo que pretende, le da prioridad a los primeros mandatarios y desconoce de tajo la posibilidad de que las comunidades puedan convocar el mecanismo de la consulta popular, **“robándose” la posibilidad de participar por parte de las personas en una democracia.**

### **El presupuesto para las consultas populares** 

Frente al caso de Granada y la suspensión de la consulta popular por falta de dinero, García señaló que son “estratagemas” para bloquear la posibilidad de usar la consulta popular “es inaceptable que el Ministerio de Hacienda, por ejemplo, **diga que no tiene claro quién debe financiar las consultas, cuando el artículo 120 de la Constitución, dice que es responsabilidad de la Registraduría**”.

Además, García manifestó que no es coherente que se diga que no hay dineros para las consultas populares, pero que si lo haya para realizar las consultas **de partidos políticos como los 80 mil millones que cuesta la consulta interna del partido Liberal**.

Otra de las denuncias que están haciendo las comunidades es que luego de la suspensión de las consultas, ninguna entidad responde sobre nuevas fechas o el estado del proceso para llevarlas a cabo. (Le puede interesar:["La consulta sigue viva: Habitantes de Granada"](https://archivo.contagioradio.com/consulta_popular_granada_meta_sigue_viva/))

### **¿Cómo continuar haciendo consultas populares en el país?** 

Frente a las posibilidades de como continuar con los procesos de consulta previas que ya están en marcha, García señaló que activaran todos los mecanismos que hay en la constitución, **de igual forma expresó que se estaría pensando potenciar luchas desde la política en defensa del territorio** y continuar en una movilización ciudadana más fuerte.

Sobre qué hacer con la falta de presupuesto, Renzo García, afirmó que se está pensando en optimizar recursos de forma tal que las consultas se realicen el mismo día que otros procesos electorales. (Le puede interesar: ["Organizaciones Nacionales e Internacionales piden a la Corte que no limite la consulta popular"](https://archivo.contagioradio.com/organizaciones-nacionales-e-internacionales-piden-a-la-corte-constitucional-que-no-limite-consultas-populares/))

<iframe id="audio_21691012" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21691012_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
