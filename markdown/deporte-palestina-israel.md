Title: Pese al bloqueo a jugadores por parte de Israel, Gaza gana la copa Palestina de Fútbol
Date: 2017-08-16 12:49
Category: Onda Palestina
Tags: BDS, Gaza, Palestina
Slug: deporte-palestina-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/gaza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:playgroundmag.net 

###### 16 Ago 2017 

A pesar de que Israel prohibió a nueve jugadores de un club de fútbol palestino salir de la Franja de Gaza a disputar algunos partidos en la Cisjordania ocupada, el equipo gazatí de los **Rafah Youngsters** salió victorioso y se coronó como campeón del torneo local.

La victoria se dió luego de que ganaran en casa 2 a 0 contra el equipo de Hebrón, el **Ahly al-Khalil**, y lograran una igualdad en el partido de visitante.

**Abdussalam Haniyye**, miembro del Consejo Supremo de Juventud y Deportes de Palestina, **ha pedido a la FIFA**, el organismo internacional de fútbol, **que tome medidas para poner fin a esta prohibición**.

Esta medida debe tomarse en contexto: hace exactamente un año pasó algo muy similar, la final de **la Copa Palestina se retrasó después de que seis jugadores de un equipo de Gaza también fueran bloqueados por Israel a la hora de viajar.** En ese caso los jugadores finalmente pudieron trasladarse a Cisjordania gracias a que la FIFA interpuso una denuncia ante el gobierno israelí.

Por esta y otras razones el movimiento Boicot Desinversiones y Sanciones (BDS), que tiene su base en Palestina, ha llamado a los deportistas del mundo a **rechazar cualquier invitación que se les haga para participar en eventos deportivos en Israel** o para hacer parte de eventos patrocinados por este gobierno, hasta que dicho Estado respete plenamente los derechos de los deportistas, y en general de la sociedad civil palestina.

<iframe id="audio_20369633" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20369633_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio.
