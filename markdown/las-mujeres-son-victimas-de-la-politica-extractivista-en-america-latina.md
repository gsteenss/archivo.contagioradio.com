Title: Las mujeres son víctimas directas del extractivismo en América Latina
Date: 2015-08-17 10:35
Category: DDHH, Mujer, Nacional
Tags: bancada de mujeres en congreso, Censat agua Viva, Gran Minería en Colombia, La Gran Minería Envenena, Marcha de mujeres negras del Cauca, Mineria, mujeres
Slug: las-mujeres-son-victimas-de-la-politica-extractivista-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/mujer_victima_mineria_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: redlatinoamericanademujeres 

<iframe src="http://www.ivoox.com/player_ek_6738336_2_1.html?data=l5ygmpiXeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkMLnjNLizMrWqdSf1NTbjduJh5SZoqnQ1s7RpdSfxc7fx8jYpdSfxcrZjcrcuNPVxNnW2M7XsdCfxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tatiana Roa, coordinadora general de CENSAT Agua Viva] 

###### [14 Ago 2015]

Desde el martes 18 de agosto y hasta el viernes 20 tendrá lugar el “**Encuentro Nacional de Mujeres defensoras de la vida frente al extractivismo**”, en el municipio de Fusagasugá. El evento contará con invitadas nacionales e internacionales, indígenas, campesinas, afrodescendientes y pobladoras urbanas, que lideran propuestas de acción frente al modelo extractivista imperante actualmente en América Latina.

Tatiana Roa, coordinadora general de CENSAT Agua Viva, comenta que la intención del evento es **continuar tejiendo espacios de resistencia frente a los impactos negativos que trae consigo la presencia de multinacionales en Colombia**, así como en los demás países latinoamericanos y que perjudican tanto los territorios, como los modos de vida y los cuerpos de las mujeres rurales.

Según Roa las afectaciones directas a las mujeres tienen que ver con la **profundización del modelo patriarcal de dominación**, el detrimento de la agricultura, la contaminación de fuentes hídricas, la proliferación de enfermedades y la agudización de la violencia sexual, entre otras.

Ante la falta de espacios de análisis en el país sobre las implicaciones del extractivismo en la vida y los cuerpos de las mujeres, este encuentro pretende consolidar una **hoja de ruta que les permita accionar frente a la distribución inequitativa de los bienes comunes, el despojo y la privatización de la vida.**
