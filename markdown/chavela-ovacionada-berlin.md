Title: Chavela, ovacionada en Berlín
Date: 2017-02-18 14:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Berlinale, Chavela Vargas, Documental, estreno
Slug: chavela-ovacionada-berlin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/19438723-peru-chavela-vargas-file-600_84132.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 17 Feb 2017 

El documental biográfico sobre la cantante mexicana Chavela Vargas, fue estrenado en la sección Panorama, la segunda más importante del Festival de Cine de Berlín, Berlinale. El proyecto dirigido por Catherine Gund y Daresha Kyi, tuvo una gran acogida por parte del jurado y el público asistente a la 67 edición del evento.

A través de varias entrevistas grabadas en 1991 por Gund, imágenes de archivo y entrevistas a personajes  cercanos a la artista como Pedro Almodóvar o algunas de sus amantes, “Chavela” recorre en 90 minutos la vida de la famosa cantante de origen costarricense que, con su desgarradora voz, conquistó cientos de escenarios alrededor del mundo y se convirtió en un icono de libertad y rebeldía al desafiar los roles asignados a las mujeres de su época.

Las directoras, también aprovecharon la premier para hablar sobre la difícil situación que atraviesa México, con las nuevas políticas migratorias de Donald Trump y su plan de construir un muro fronterizo.

A cinco años de la muerte de la intérprete de "la Llorona", el documental  hace parte de las numerosas apuestas biográficas incluidas en la más reciente edición del festival, como “El joven Karl Marx” centrada en el gran pensador y revolucionario alemán o “Django” basada en la figura del jazz Django Reinhardt, entre otras.

<iframe src="https://player.vimeo.com/video/202123182" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[CHAVELA trailer](https://vimeo.com/202123182) from [Aubin Pictures](https://vimeo.com/aubinpictures) on [Vimeo](https://vimeo.com).
