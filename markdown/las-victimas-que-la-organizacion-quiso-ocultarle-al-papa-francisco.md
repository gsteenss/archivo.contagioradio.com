Title: Las víctimas que la organización quiso ocultarle al Papa Francisco
Date: 2017-09-11 17:01
Category: DDHH, Nacional
Tags: víctimas de crímenes de Estado, Visita del Papa Francisco
Slug: las-victimas-que-la-organizacion-quiso-ocultarle-al-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [11 Sept 2017] 

Aunque fueron demasiadas las expectativas en torno a la visita del papa Francisco y su encuentro con las víctimas en Villavicencio, fueron muchas las que se quedaron sin establecer contacto con el Pontífice. Por ejemplo, **brillaron por su ausencia las víctimas de crímenes de Estado, los familiares de las víctimas de ejecuciones extrajudiciales, las víctimas de desaparición forzada y las del Escuadrón Móvil Anti Disturbios entre otras.**

Según Luz Marina Cuchúmbe, madre de Hortensia Cuchúmbe víctima de ejecución extrajudicial, las víctimas del Estado no se sintieron representadas en quienes tuvieron la oportunidad de exponer ante toda Colombia su caso **“las víctimas de Estado no se les dio la oportunidad de decir que estaba pasando, yo no me sentí recogida**, a mi me hubiera gustado que nos dieran oportunidad, la verdad no se le puede ocultar a nadie ni siquiera a Dios” afirmó Cuchúmbe.

Amplios grupos de víctimas se quedaron sin hablarle al país en público o en privado yuademás, las cartas que llevaban para Francisco, ni siquiera pudieron ser entregadas directamente, sino que se presume llegó a Francisco a través de un obispo que las recibió.

**Víctimas de ejecuciones extrajudiciales**

La hija de Luz Marina fue asesinada en Inzá, Cauca, por las Fuerzas Militares, como ella hay en Colombia más de **3.000 familias** que están a la espera de la justicia para que se condene a los responsables de estos crímenes. (Le puede interesar[: "CPI solicita que 29 generales y coroneles sean juzgados por ejecuciones extrajudiciales"](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/))

En ese sentido las víctimas de las ejecuciones extrajudiciales también estaban a la espera de poder hablar con el Papa Francisco para expresarle el anhelo que tenían para que intercediera ante el Estado Colombiano en el cumplimiento de los Acuerdos de paz pactados con las FARC- EP, **en el que se abre un capítulo para el cumplimiento de la justicia en crímenes cometidos por integrantes de la Fuerza Pública** y que ha tenido modificaciones que podrían generar la impunidad total.

### **Víctimas de desaparición** 

De acuerdo con la Asociación de Familiares de Detenidos Desaparecidos, en Colombia el 84% de las víctimas de este delito son responsabilidad del Estado. De acuerdo con los últimos datos de Medicina Legal, en el país hay más de **23.441 personas desaparecidas de manera forzada, mientras que en el marco del conflicto armado los registros son de 45.944** personas.

Este es uno de esas situaciones que las víctimas del Estado manifestaron que fue invisibilizada durante la visita del Papa Francisco y que tampoco lograron compartir con el sumo pontífice. (Le puede interesar: ["El Estado Colombiano es responsable del 84% de las desapariciones forzadas"](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/))

### **Millones de pasos por la paz** 

La caravana de Millones de pasos por la Paz, también buscaba entregarle una carta al Papa Francisco, con la intensión de pedirle que intercediera por que que no haya más asesinatos a líderes sociales y defensores de derechos humanos y que con la visita del Papa a **Colombia se de paso a una reconciliación, al respeto por la vida y la participación social para alcanzar la paz**.

En la caravana había presencia de diferentes organizaciones sociales, étnicas y populares que finalmente no pudieron encontrarse con el Papa, pero que a través de pancartas intentaron hacerle llegar este mensaje al sumo Pontifice.

<iframe id="audio_20808421" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20808421_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
