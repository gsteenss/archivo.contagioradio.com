Title: Isagén tiene 6 meses para limpiar el Río Sogamoso
Date: 2015-04-10 14:52
Author: CtgAdm
Category: Ambiente, Nacional
Tags: hidrosogamoso, ISAGEN, Movimiento Ríos Vivos, Movimiento Social en Defensa del Río Sogamoso, represas, Rio Sogamoso, Santander, Tribunal Administrativo de Santander
Slug: isagen-tiene-6-meses-para-limpiar-el-rio-sogamoso
Status: published

##### Foto: [boletinesdeprensacompromiso.blogspot.com]

**El Tribunal Administrativo de Santander ordenó a ISAGEN la limpieza del embalse y la remoción de la cobertura vegetal**, gracias a la acción popular interpuesta por Claudia Patricia Ortíz Gerena, del Movimiento Social en Defensa del Río Sogamoso y afectada por la construcción del proyecto hidroeléctrico de la empresa ISAGEN.

Ortiz, había instaurado el pasado mes de marzo una “solicitud de medidas cautelares” en la cual se advierte la existencia de un peligro de inundación que traería graves consecuencias para las comunidades ribereñas del río Sogamoso, debido “**al desequilibrio ecológico por las obras de construcción de la Represa de Hidrosogamoso**, para hacer efectiva la protección de los derechos colectivos a gozar de un ambiente sano, a la existencia del equilibrio ecológico, el manejo y aprovechamiento racional de los recursos naturales para garantizar su desarrollo sostenible, su conservación, restauración o sustitución y el derecho a la seguridad y prevención de desastres previsibles técnicamente”, señala la acción.

El 27 de marzo, la respuesta del **Tribunal Administrativo de Santander** fue positiva, y decretó medidas cautelares ordenando a ISAGEN, “la respectiva limpieza, remoción de la cobertura vegetal y todos aquellos materiales blandos, orgánicos y objetables del río Sogamoso, así mismo, establecer todas aquellas medidas ambientales preventivas, lo anterior deberá efectuarse dentro de **un plazo no mayor a seis (6) meses,** advirtiendo que dicha remoción deberá efectuarse cada vez que la misma sea observada, según lo expresado en la parte motiva de este proveído.”

Por medio de este fallo, se reconoce la responsabilidad de **ISAGEN**, al incumplir con lo estipulado en la licencia ambiental. Lo que ha generado que la comunidad se haya visto gravemente afectada al tener que aguantarse **malos olores, la perdida de la calidad del agua, nauseas, vómitos, dolores de cabeza, infecciones en la piel, caída del cabello,**  entre otras enfermedades causadas por la variación constante de los niveles del río. [(Hidrosogamoso huele a podrido)](https://archivo.contagioradio.com/hidrosogamoso-huele-a-podrido/)

De acuerdo a las explicaciones de la comunidad, la apertura de las compuertas de la represa provoca **la variación de los niveles del río y el pudrimiento de material orgánico,** generando gas metano y ácido sulfhídrico, cuyos efectos son altamente nocivos para la salud de la población que constantemente inhala esos gases.

Aproximadamente **el 15% de la población infantil se está viendo afectada**, ya que como lo asegura Claudia Ortiz,  el río Sogamoso es ahora “un río donde los niños ya no se pueden bañar por que salen enfermos, mucho menos pueden tomar de esa agua”.
