Title: Continúa asesinato de líderes en el país, aparece muerto el campesino Albert Martínez
Date: 2017-11-15 12:12
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Cauca, marcha patriotica
Slug: asesinan_campesino_albert_martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/lideres-asesonados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Nov 2017] 

El pasado miércoles el campesino, Albert Martínez Solarte de 41 años de edad había sido reportado como desaparecido, luego de que unos sujetos lo sacaran de su finca localizada en la vereda Chinchal Carmelo, pero este martes, **apareció muerto con varios impactos de bala en su cuerpo en zona rural del municipio de Cajibío** en Cauca.

El primer reporte de las autoridades indica que el cadáver fue encontrado por un pescador en La Salvajina, en Suárez. Según se ha conocido, **Martínez hacía parte del movimiento político y social, Marcha Patriótica,** y había llegado a la zona a trabajar hace algunos meses.

La Red de Derechos Humanos del Suroccidente Colombiano confirmó que el agricultor asesinado era del municipio de Rosario, en Nariño. Por su parte, el inspector de Policía  del municipio de Piendamó, Cauca, practicó el levantamiento del cadáver, que luego fue  trasladado a Popayán donde le realizaron la necropsia logrando determinar su identidad.

De acuerdo con cifras de la Defensoría del Pueblo, con la muerte de Albert Martínez y Luz Jenny Montaño, ocurridas en esta semana, **ya suman 202 asesinatos de líderes sociales desde enero de 2016 hasta la fecha. **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

###### 
