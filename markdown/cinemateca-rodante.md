Title: Cinemateca rodante: una experiencia de desarrollo, exhibición y conservación audiovisual
Date: 2015-04-18 19:22
Author: CtgAdm
Category: 24 Cuadros, Educación
Tags: 24 cuadros, Biblioteca Especializada en Medios Audiovisuales “BECMA”, Cine, Cinemateca al parque, Cinemateca Distrital, Cinemateca rodante, ciudad bolivar, Giovanna Segovia, Instituto Distrital de las Artes, Rafael Uribe Uribe, Suba, Tunjuelito, Usaquén
Slug: cinemateca-rodante
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/postal_fase22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: idartes 

Las inscripciones para las diferentes convocatorias estarán abiertas hasta el próximo 5 de Mayo.

La oportunidad para que las y los Bogotanos interesados en capacitarse en temas relacionados con la creación, exhibición y conservación audiovisual llega con la cuarta temporada de la **Cinemateca Rodante** una estrategia de intervención integral y territorial de la Gerencia de Artes Audiovisuales del **Instituto Distrital de las Artes**, Idartes.

Hasta el próximo 5 de Mayo, los habitantes de **Teusaquillo, Puente Aranda, Santa fé, Martires, Usme y San Cristóba**l,podrán inscribirse y tomar talleres de formación integral para la realización de cortometrajes de ficción, con módulos que incluyen producción, dirección, edición, fotografía, sonido, arte. Al finalizar los talleres, con los conocimiento adquiridos y una idea aportada por los participantes, se realizara un largometraje al que tendrá su estreno en **“Cinemateca al parque”** cita audiovisual promovida por la entidad.

Para las localidades de **Rafael Uribe Uribe** y **Tunjuelito**, se brindará un taller especializado en escritura de guión para cortometraje de ficción, para **Suba y Usaquén** un taller de producción ejecutiva para cortometrajes, rutas de financiación en creación y distribución de las producciones que crearán durante el proceso de formación.

En la localidad de Kennedy un taller de fotografía y sonido para video mientras que para Fontibón estará centrado en la realización de video clips. En **Ciudad Bolívar** se impartirá un taller enfocado a la producción y puesta en escena.

En cuanto a la formación para la creación y fortalecimiento de Cine clubes dirigido a ciudadanos, colectivos y Casas de la Cultura, los talleres se brindaran en las localidades de Teusaquillo, San Cristobal, Puente Aranda, Santa fé, Martires y Candelaria.

Otra de las apuesta de la estrategia es la **“Videoteca local”** que busca recolectar la producción audiovisual que se realiza en las localidades, para ser preservada en la **Biblioteca Especializada en Medios Audiovisuales “BECMA”** de la Cinemateca Distrital, publicada, circulada y exhibida en diferentes espacios alternativos de la ciudad como **Cine Clubes y Salas Asociadas.**

Las personas interesadas en participar de las diferentes convocatorias no deben necesariamente tener algún antecedente asociado con el audiovisual, tal como expresa **Giovanna Segovia**, Asesora Misional de la Cinemateca Rodante a Contagio radio “nos interesa que la carta de motivación que escriba la gente sea muy convincente y que en realidad quieran hacer parte de este proceso“ agregando que tanto jóvenes como adultos y adultos mayores estan invitados a vincularse activamente con el taller de su interés, hasta completar un total de 40 cupos por cada uno.

Toda la información, contenidos e inscripciones de las convocatorias estan disponibles en el sitio de la Cinemateca en [www.idartes.gov.co](http://www.idartes.gov.co) o en [www.cinematecadistrital.gov.co](http://www.cinematecadistrital.gov.co)
