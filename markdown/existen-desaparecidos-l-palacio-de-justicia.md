Title: ¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia
Date: 2019-09-06 18:55
Author: CtgAdm
Category: DDHH, Memoria
Tags: Corte IDH, desaparición, Fiscalía, Palacio de Justicia, víctimas
Slug: existen-desaparecidos-l-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MG_0547.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MG_0537.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MG_0534.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-06-at-5.36.48-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagioRadio] 

Este 6 de septiembre, **la Corte Interamericana de Derechos Humanos (Corte IDH) analizó el avance de las investigaciones** y el incumplimiento por parte del Estado para con los familiares de los desaparecidos del Palacio de Justicia. Una audiencia donde las víctimas solicitaron se investigue, identifique y sancione a los responsables y se realice una entrega digna de sus seres queridos  y donde la Fiscalía finalmente tuvo que reconocer que sí existió desaparición forzada aquel noviembre de 1985.  
**  
**Actores estatales, familiares de los desaparecidos, abogados, representantes de la iglesia, organizaciones internacionales y la institucionalidad colombiana hicieron parte de esta audiencia, presentado pruebas y alegatos  acerca de los niveles de cumplimiento de las sentencias de la corte por parte del Estado colombiano.(Le puede interesar:[Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia](https://archivo.contagioradio.com/fiscalia-desaparecidos-palacio-de-justicia/))

### **¿Qué solicitaron las familias de las víctimas del Palacio de Justicia  ?** 

Para los familiares de los desaparecidos, uno de los puntos más importantes fue la determinación de los culpables, como lo señaló **Alejandra Rodríguez**, vocera de las víctimas, “nosotros sabemos y está en el proceso que los militares son los responsables, ahora falta que el Estado lo reconozca y los judicialice como debe ser y lo ordena la Corte”.

Además, señalaron su incomodidad ante la falta de respuestas y seguimiento a sus casos, “en definitiva el Estado no está preparado y no sabe cómo resolver crímenes de lesa humanidad cuando son cometidos por parte de agentes estatales”, esto con respecto al incumplimiento que dio la corte en el 2014 sobre este caso.

Adicionalmente realizaron un llamado a la opinión pública a verificar los datos, a investigar los casos y la respuesta de sus fuentes antes de hacer  visible algún señalamiento, “gracias a mal manejo de la información ahora no gritamos ¿dónde están los desaparecidos?, sino ¡existen los desaparecidos!, señala Rodríguez; esto en vista a las entrevistas dadas por la Fiscalía en días anteriores.(Le puede interesar:[Tejiendo, familiares conmemoran 33 años del Palacio de Justicia](https://archivo.contagioradio.com/a-33-anos-del-palacio-de-justicia-familiares-invitan-a-tejer-por-la-memoria/))

![Palacio de Ju](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MG_0537-1024x683.jpg){.aligncenter .size-large .wp-image-73253 width="1024" height="683"}

### **En el marco jurídico ¿qué se espera?** 

Eduardo Carreño Wilches, representante de víctimas por el colectivo de abogados “José Alvear Restrepo” (Cajar), señaló que los puntos tratados durante la audiencia se centraron en el reconocimiento de cadáveres, la ausencia de investigadores, culpabilidad de los actores, y al cumplimiento al 100% de las indemnizaciones.

El colectivo enfocó parte de su alegato en el reconocimiento por parte del Estado de la culpabilidad en la desaparición y muerte de funcionarios y visitantes del Palacio, según Carreño, “el alto mando es responsable de la totalidad de los hechos, por saber lo que iba a pasar con el M-19 y no hacer nada para prepararse y proteger a las personas”.

De igual forma, se abordaron temas como investigar  a quienes entregaron los cuerpos, determinar que no solo fueron desapariciones sino desapariciones forzadas e indemnizar a las víctimas, un proceso que ha avanzado de forma lenta, tal como señaló Carreño.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-06-at-5.36.48-PM-1-1024x768.jpeg){.aligncenter .size-large .wp-image-73257 width="1024" height="768"}

**El papel de agentes internacionales **

La participación de representantes internacionales, es útil para tener bases y resultados sobre las acciones que ha tomado la Corte IDH, ejemplos como Perú, donde señalaron la eliminación de beneficios que tenía el expresidente Fujimori y su regreso a prisión, o como en Guatemala, donde se logró que la Fiscalía tomara medias de organización y planeación para avanzar en las investigaciones. (Le puede interesar:[Estado responderá por 14 condenas en su contra ante la Corte Interamericana de DD.HH.](https://archivo.contagioradio.com/estado-condenas-corte-interamericana/))

Según Viviana Krsticevic. directora Ejecutiva del Centro por la Justicia y el Derecho Internacional (CEJIL), el resultado de esta audiencia fue negativo, “el Estado claramente está en completo desacato con las decisiones de la Corte Interamericana”, lo que ha derivado en  hacia las víctimas, lentitud en los procesos y falta de garantías como se ha podido evidencias en varias ocasiones.

El CEJIL representa alrededor de 3.000 víctimas de violaciones de derechos humanos en distintos países de las Américas, generando alianzas con abogados y defensores de derechos humanos en caso de lesa humanidad, como lo es el del Palacio de Justicia.

> [“Pese a los gastos y recursos que tiene el Estado colombiano y la fortaleza de su institucionalidad, no se han tomado medidas diligentes y serias para investigar con exhaustividad el destino de los desaparecidos, ni para hacer justicia a  los crímenes de lesa humanidad y las graves violaciones  a los derechos humanos que fueron establecidas en la Corte”]

Las víctimas fueron escuchadas y ahora tendrán que esperar una sentencia de la Corte que evalué los niveles de cumplimiento de las órdenes dadas en sus sentencias originales y aclare algunos conceptos de derecho internacional, esperan también que se instale una veeduría internacional con estrategias claras, así mismo cambiar a los actuales delegados de la Fiscalía.

La Corte IDH, publicará una nueva resolución frente a este caso y tendrá en cuenta medidas que garanticen y protejan la salud de las víctimas de los desaparecidos e indicará al Estado medidas estructurales y puntuales para hacer efectiva la justicia y encontrar la verdad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
