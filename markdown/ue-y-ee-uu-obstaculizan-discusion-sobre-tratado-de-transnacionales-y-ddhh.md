Title: UE y EE.UU obstaculizan discusión sobre tratado de transnacionales y DDHH
Date: 2015-07-08 16:37
Category: DDHH, El mundo
Tags: Banco Mundial, Campaña Global para Desmantelar el Poder de las Trasnacionales, Censat agua Viva, Derechos Humanos, Fondo Monetario Internacional, Mónica Vargas, Naciones Unidas, ONU
Slug: ue-y-ee-uu-obstaculizan-discusion-sobre-tratado-de-transnacionales-y-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/MF_Espinosa_Day2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [panorama.ridh.org]

<iframe src="http://www.ivoox.com/player_ek_4739652_2_1.html?data=lZygm5uZdo6ZmKiakp6Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfsLPCjdjJb8Xd1Mji1sqPt9DW08qY1tfFuMLY0JDT1MrSuMafwpDB1MbXssLXytTbw9HJt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mónica Vargas, Campaña Global para Desmantelar el Poder de las Trasnacionales] 

Durante esta semana **la Unión Europea y Estados Unidos, han intentado obstaculizar la discusión que se lleva a cabo en la ONU sobre transnacionales y derechos humanos**. Por medio de la ausencia de voceros gubernamentales, y amenazas diplomáticas hacia otros países, se ha procurado bloquear la construcción del instrumento vinculante que pretende que las empresas respeten los derechos de las comunidades.

Las acciones de estas naciones están “demostrando una acción absolutamente antidemocrática”, expresa Mónica Vargas, integrante de la Campaña Global para Desmantelar el Poder de las Trasnacionales.

El viernes el Consejo de Derechos Humanos de las Naciones Unidas tomará una **decisión frente a la adopción de una resolución para establecer un instrumento legal obligatorio**, a través del cual se exija a las transnacionales cumplir con normas en relación a los **derechos humanos.**

Pese a las acciones de los países que están en contra de ese tratado, se están realizando las negociaciones del grupo intergubernamental para construir ese instrumento vinculante,  y se ha entregado **una propuesta de ocho puntos concretos consensuados entre las 190 organizaciones sociales** que hacen parte de esta iniciativa.

Por Colombia, **Danilo Urrea integrante de Censat agua Viva,** ha aportado sobre la valorización del territorio y los derechos de la naturaleza. Así  mismo se ha aportado en contenidos frente a las obligaciones o responsabilidades que por ejemplo, deberían tener el  **Fondo Monetario Internacional el Banco Mundial** que “son responsables y tienen **un papel clave en la arquitectura de la impunidad”**, señala Vargas.
