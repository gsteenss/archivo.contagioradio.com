Title: 30 años sin Nydia Erika Bautista
Date: 2017-08-30 09:10
Category: Carolina, Opinion
Tags: desaparecidos colombia, Nydia Erika
Slug: 30-anos-sin-nydia-erika-bautista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/nidia-erika.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/)\*** 

###### 30 Ago 2017 

Hace 30 años el mundo se volvió un lugar peor. Así como se vuelve peor cada día para la familia de una persona que es detenida y desaparecida.

[El mundo perdió a **Nydia Erika Bautista**, un hijo perdió a su madre, una hermana a su cómplice y unos padres a su hija. Hace 30 años todos, incluso quienes nacerían después, perdimos la oportunidad de conocer a Nydia, de escuchar sus ideas, de compartir una historia, de contarnos un chiste o de sentarnos​ a pensar un mejor país, una mejor Colombia.]

[Por eso nos hemos negado a olvidarla, a ella, y a los más de 60.000 desaparecidos que nos han arrebatado en cinco décadas. Cada 30 de agosto, Día internacional del detenido desaparecido y aniversario de la desaparición forzada de Nydia Erika Bautista, nos preguntamos ¿Quién se los llevó? ¿Por qué? ¿Quiénes fueron los responsables? Y con el tiempo nos damos cuenta que esas respuestas van apareciendo, pero la pregunta más importante sigue sin respuesta ¿Dónde están los desaparecidos?]

[Evocar a Nydia Erika, a 30 años de su desaparición forzada, es recordar que ese 30 de agosto de 1987, el mismo día de la primera comunión de su hijo, fue secuestrada por militares adscritos a la  XIII Brigada del Ejército-Batallón Charry Solano. Luego, fue llevada a una finca en la zona de Quebradablanca, en el municipio de Guayatebal, mantenida en cautiverio, torturada y violada durante varios días. Después fue asesinada y su cuerpo fue abandonado como N.N. Fue desaparecida y 30 años después los responsables no han respondido ante la justicia. Su caso pertenece a esa escandalosa cifra de 99% de impunidad en los casos de desaparición forzada en Colombia.]

[Pero evocar a Nydia también es ver cómo se volvió mariposa, cómo se convirtió en semilla y cómo ha permanecido presente en quienes luchan contra el flagelo de la desaparición forzada y en los familiares que no descansan en la búsqueda de los desaparecidos (o de las víctimas). Vive su fuerza, también, en aquellos que, aun encontrando a sus seres amados, vivos o muertos, deciden seguir el camino de la solidaridad y el trabajo por quienes aún esperan una noticia del paradero de su ser querido.]

[Hoy, más que nunca, se enciende una esperanza en Colombia para que miles de personas desaparecidas sean encontradas. Con la firma del Acuerdo de Paz y la implementación del mismo se crea la]*[Unidad de Búsqueda de personas dadas por desaparecidas en el marco y en razón del conflicto armado]*[(UBPD) la cual funcionará durante 20 años y tiene por mandato “Dirigir, coordinar y contribuir a la implementación de acciones humanitarias de búsqueda e identificación de personas dadas por desaparecidas que se encuentren con vida, y en los casos de fallecimiento, cuando sea posible, la localización y entrega digna de restos”.]

[Por ello, este 30 de agosto, este 30 aniversario, nos hace un llamado a seguir rodeando a los familiares y amigos de las víctimas de desaparición forzada. Nos invita a respaldar a las organizaciones, fundaciones y asociaciones que trabajan por encontrarlas, por acompañar a quienes siguen buscando y se empeñan en que no se repita. Este 30 es, sin duda, una invitación a comprometernos y a exigirle al Estado que responda cada uno de los interrogantes de los familiares. Este 30 de agosto, este 30 aniversario, es necesario que nos unamos al reclamo de verdad y de justicia. Desde este 30 la invitación es a sumar luchas, hasta encontrarlos.]

#### [**Ver más columnas de Carolina Garzón Díaz**](https://archivo.contagioradio.com/carolina-garzon-diaz/)

###### [\* Comunicadora social- Periodista. Defensora de derechos humanos. Estudiante de la Maestría en Dirección de Comunicación de la Universidad de Montevideo (Uruguay). Twitter: @E\_Vinna] 

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
