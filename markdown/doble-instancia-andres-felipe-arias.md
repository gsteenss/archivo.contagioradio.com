Title: La doble instancia retroactiva no salvará a Andrés Felipe Arias
Date: 2019-07-29 21:30
Author: CtgAdm
Category: Judicial, Nacional
Tags: agro ingreso seguro, Andres Felipe Arias, Doble Instancia, Gustavo Gallón
Slug: doble-instancia-andres-felipe-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Andres-Felipe-Arias-e1564453367903.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Usuario de Twitter] 

El abogado y director de la Comisión Colombiana de Juristas (CCJ) Gustavo Gallón afirmó que **las posibilidades de que una doble instancia retroactiva cambie la sentencia contra Andrés Felipe Arias son muy pocas**. La conclusión la extrae luego de analizar el recurso interpuesto por Arias ante el Comité de Derechos Humanos de las Naciones Unidas, que fue el encargado de pedir legislación sobre la doble instancia. (Le puede interesar: ["La estrategia del uribismo que busca lavar la imagen de Andrés F. Arias"](https://archivo.contagioradio.com/uribismo-andres-felipe-arias/))

Gallón recordó que en la queja que el Exministro presentó ante el Comité señalaba 15 argumentos contra su sentencia entre los que mencionaba una supuesta persecución por parte de la fiscalía que presidía Vivian Morales, que no se le había permitido mostrar pruebas a su favor y que no había tenido derecho a la segunda instancia. De estos argumentos, el Comité reconoció que debía tener acceso a la doble instancia, pese a que la Constitución de Colombia establece que la garantía en los procesos judiciales para aforados es que son investigados por la Corte Suprema de Justicia.

Por lo tanto, Gallón dijo que sentía pena, por quienes crean que Arias es inocente. Al tiempo, afirmó que dado el resultado del Comité se puede aventurar que una doble instancia creada por la Ley que cursa en el Congreso para este propósito, **probablemente fallaría en su contra y lo encontraría igualmente culpable** de peculado por apropiación.

### **Andrés Felipe Arias: ¿De la segunda instancia a la Casa de Nariño?** 

Ante la matriz de comunicaciones que ha utilizado el uribismo, refiriendo que Andrés Felipe Arias "no se robó un solo peso", Gallón recordó las razones por las que Arias fue condenado, "lo que la gente puede saber es que él repartió plata a personas ricas, dueñas de terrenos (...) que estaban favoreciendo su campaña presidencial". El director de la CCJ señaló que incluso hubo irregularidades dentro de la irregularidad, pues se sabe que un predio fue dividido en diferentes fracciones, y se entregó dineros a familiares 'dueños' de esos lotes.

Según información que ha trascendido, algunos de esos dineros fueron devueltos; por lo tanto, se sabe que el Exministro de agricultura actuó mal, de otra forma, no se habrían regresado esos dineros.  Para concluir, el Abogado se refirió a la idea de que Arias sea el candidato del Centro Democrático para las elecciones presidenciales de 2022 al señalar que **"así de mal está el uribismo que no tiene un mejor candidato que presentar un delincuente que robó dineros de esa manera"**.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### <iframe id="audio_39185042" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39185042_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
