Title: "Declaraciones de Peñalosa sobre la RTVDH son de la peor mala fe": Manuel Rodríguez
Date: 2016-07-25 16:30
Category: Ambiente, Entrevistas
Tags: ALO, Enrique Peñalosa, Manuel Rodríguez, Reserva Thomas Van der Hammen
Slug: declaraciones-de-penalosa-sobre-la-rtvdh-son-de-la-peor-mala-fe-manuel-rodriguez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Manuel-b.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Universidad de Los Andes 

###### [25 Jul 2016] 

“La Reserva Thomas Ven Der Hammen que no tiene árboles, está impidiendo que se desembotelle la ciudad al norte”, fue en lo que volvió a insistir el Alcalde Enrique Peñalosa, quien aseguró que solicitó a la CAR el permiso para pasar sobre la reserva tres avenidas. Ante estas aseveraciones, Manuel Rodríguez Becerra, exministro de Ambiente expresa que **la declaración del alcalde “es de la peor mala fe”**, pues es de su conocimiento el potencial ecológico de esta zona.

**“Él sabe muy bien cuáles son los argumentos científicos a favor de la reserva y el potencial ecológico para la restauración de bosques y otros ecosistemas…** decir que no hay árboles, eso no es serio. No le queda bien al alcalde confundir a la opinión, no es democrático, no le quedan bien esos argumentos tan simplistas y absolutamente faltos a la verdad”.

De acuerdo con Rodríguez, **la Reserva es de enorme importancia para Bogotá, pues se trata de la última área con la que se podría generar algún tipo de restauración ecológica** teniendo en cuenta que “en la sabana hay una destrucción ambiental enorme”, expresa el exministro, quien añade que lo que se debe hacer, **es promover un debate público, respetando la existencia de esa reserva**, y solicitar a la CAR alternativas para comunicar el norte de Bogotá con la sabana.

En 1999, en su primera administración Enrique Peñalosa había solicitado el mismo permiso al Ministerio de Ambiente, asegurando que si no urbanizaba el norte de Bogotá "iba  a haber una tragedia", y aunque no se le autorizó toda la zona, **se le concedieron finalmente 2.500 hectáreas y hoy en día no hay una sola hectárea urbanizada,** explica Manuel Rodríguez, quien dice no entender por qué la insistencia en desconocer el valor ecológico de la reserva y querer urbanizar esas 1.395 hectáreas.

El experto en ambiente señala que **la responsabilidad de un dirigente público o un alcalde es promover la protección del ambiente**, y con estas declaraciones, Peñalosa se está dando a conocer como “uno de los únicos alcaldes contemporáneos que tiene una visión muy corta sobre lo que puede ser la conservación ambiental”.

La ampliación de la Avenida Boyacá, la ampliación de la Avenida Ciudad de Cali y la construcción de la Avenida Longitudinal de Occidente (ALO), son los proyectos que planea poner en marcha el alcalde, sin embargo, el director de la CAR, Néstor Franco, dijo en la mañana del lunes que **no han recibido algún tipo de petición para construir vías sobre la reserva.**

Por ahora, para el exministro esta **discusión va a acabar en los tribunales,** y lo importante será continuar generando espacios pedagógicos para que la ciudadanía comprenda la importancia de la conservación ambiental de esta zona de la sabana de Bogotá.

<iframe src="http://co.ivoox.com/es/player_ej_12333930_2_1.html?data=kpeglZidd5Ghhpywj5ebaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpC2s8Xmhqigh6aoq9bZ24qfpZClscPdxtPhw9HNt9XVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
