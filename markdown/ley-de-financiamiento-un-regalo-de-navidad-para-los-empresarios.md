Title: Ley de Financiamiento, un regalo de navidad para los empresarios
Date: 2018-12-20 17:36
Author: AdminContagio
Category: Nacional, Política
Tags: Ley de Financiamiento
Slug: ley-de-financiamiento-un-regalo-de-navidad-para-los-empresarios
Status: published

###### Foto: @angelamrobledo 

###### 20 Dic 2018 

[La Ley de Financiamiento, impulsada por el Ministerio de Hacienda, fue aprobada esta semana en Senado y Cámara tras dos meses de reformas y polémicas, medida que más allá de recaudar los 7.1 billones en impuestos, otorgó exenciones a las grandes empresas mientras se golpea a la clase media y baja.]

Según la representante de la Colombia Humana, Ángela María Robledo, dicha ley es “uno de los regalazos de Duque para las grandes corporaciones" las cuales pasarán de pagar un impuesto de renta del 33% al 30%, mientras que para otros sectores, como el de los bienes de capital de importación, desaparecerá el IVA.  
[  
Respecto al  impuesto plurifásico sobre las gaseosas y bebidas azucaradas, que ahora tendrán que pagar en toda la cadena de producción y de consumo, Robledo aclaró que se decidió finalmente dejar exento de dicho gravamen a las tiendas de barrios las cuales verían afectadas en gran medidas sus ganancias de ser incluidas en tal proceso.   
  
Por el contrario, dentro de la Ley de Financiamiento se incluyó una sobretasa de 4 % en 2019 y de 3 % de 2020 a 2022 para el sector financiero, el cual es el que tiene más exenciones de impuestos pero paradójicamente es el que más produce en el país, un argumento más para Robledo quien afirma que se trata de una reforma que “no es equitativa, ni transparente ni eficiente” tal como lo demanda la Constitución.]

[**Problemas de fondo de la reforma**  
  
Según estudios realizados por la Comisión Económica para América Latina y el Caribe (CEPAL), es probable que en el país se realice una nueva reforma tributaria dentro de dos años pues en el marco de América Latina, Colombia es uno de los países que tiene más bajos aportes a impuestos, “tributar en Colombia para las grandes empresas no significa una disminución en su concentración de riqueza” agrega Robledo.  
  
[[Como han advertido diferentes sectores políticos, la reforma tendría además problemas de trámite que podrían llevar a que la Corte Constitucional tumbe la iniciativa. Políticos como Germán Vargas Lleras también han anunciado que acudirán a está instancia, sin embargo Robledo aseguró que de hacerlo la bancada alternativa  tendrá unas razones muy diferentes a las de Lleras quien seguramente objetará por el impuesto para el sector financiero.]]]

<iframe id="audio_30940444" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30940444_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
