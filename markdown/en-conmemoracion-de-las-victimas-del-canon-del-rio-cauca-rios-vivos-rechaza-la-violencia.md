Title: En conmemoración de las víctimas del Cañon del Río Cauca Rios Vivos rechaza la violencia
Date: 2017-09-21 08:13
Category: DDHH, Nacional
Tags: Hidroituango, Rios Vivos
Slug: en-conmemoracion-de-las-victimas-del-canon-del-rio-cauca-rios-vivos-rechaza-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/742f4714-50e6-494d-b433-9e2851f6ffe6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ríos Vivos] 

###### [20 Setp 2017] 

El Movimiento Ríos Vivos denuncio que mientras se encontraba realizando una actividad de conmemoración de las víctimas del cañón del río Cauca, llamada **“Siembra de cuerpos gramaticales”**, fue asesinado una persona por sicarios, en el Valle de Toledo, situación que pone de manifiesto la falta de garantías para las comunidades.

Los hechos se presentaron en horas de la tarde, en ese momento los asistentes al evento regresaban en una movilización pacífica al casco urbano del Valle de Toledo. De acuerdo con un comunicado de prensa del Movimiento Ríos Vivos, “este episodio genera preocupación pues a pesar de haber anunciado el evento masivo con suficientemente tiempo, solicitando seguridad por el riesgo que de manera sostenida hemos tenido que sufrir como opositores a  la construcción de hidroituango, **la Policia Nacional  se enfocó  en dar seguimiento a nuestras acciones pero no en nuestra seguridad**”.

En ese sentido la organización expresó que rechaza cualquier acto de violencia e hizo un llamado a las instituciones competentes por esclarecer los hechos sucedidos. (Le puede interesar: ["Líneas de transmisión de Hidroituango podrían podría afectar la salud: comunidades"](https://archivo.contagioradio.com/comunidades-afectadas-lineas-transmision-hidroituango-denuncian-irregularidades/))

### **La jornada de conmemoración de las víctimas del Río Cauca** 

La actividad que desarrollaba la organización era una jornada que pretendía rendir homenaje a las víctimas que fueron desaparecidas, asesinadas, torturadas y tiradas desde El Puente Pescadero, desde ese momento e**ste sitio se convirtió en escenario para la memoria**, sin embargo, actualmente estaría en riesgo por la inundación de Hidroituango que lo dejaría bajo el agua.

Durante el evento los asistentes realizaron diferentes acciones como lanzar flores al río y traer a la memoria a Nelson giraldo, Robinson Mazo, Ovidio Zabala y demás víctimas y **afectados estuvieron en la mente y corazones de los cuerpos sembrados y acompañantes.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
