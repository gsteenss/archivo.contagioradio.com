Title: Más de 80 reclusos de cárcel de Palogordo están en huelga de hambre
Date: 2020-04-09 11:49
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel
Slug: mas-de-250-reclusos-de-carcel-de-palogordo-estan-en-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carcel-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Más de 80 reclusos de la cárcel de Palogordo en Girón, Santander, permanecen en huelga de hambre para exigir al Estado colombiano **acciones urgentes** frente a la protección de sus vidas de cara a la pandemia del Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con uno de los reclusos, esta protesta inició el pasado martes 7 de abril. Asimismo afirma que hasta el momento se han sumado dos patios del centro penitenciario, **es decir aproximadamente 250 personas, de las cuales se mantienen en la protesta alrededor de 88 de ellos.** (Le puede interesar: ["Inicia protesta pacífica de la población reclusa en Colombia"](https://archivo.contagioradio.com/inicia-protesta-pacifica-de-la-poblacion-reclusa-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Hasta el momento no hemos tenido ningún tipo de respuesta por parte de la dirección del penal" afirma el recluso. Además agrega que el pliego de exigencias ya esta en conocimiento del Ministerio Público, Defensoría del Pueblo y el director del INPEC. Sin embargo, tampoco han recibido respuestas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El pliego de exigencias de los reclusos de Palogordo
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los reclusos del centro penal tienen un pliego de 7 puntos, entre los que se encuentra la puesta en marcha del decreto 1144 de 2020 de emergencia carcelaria en donde se ordenaban medidas para afrontar el contagio del virus COVID 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, hasta el momento, en esa cárcel no se ha dado el acuartelamiento en primer grado de la guardia del INPEC. Tampoco se han garantizado implementos de aseo como jabón, antibacterial, entre otros. Menos aún **existe un protocolo de aislamiento o plan de contingencia en caso de contagio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, los prisioneros están exigiendo que se cumpla con el derecho a la comunicación con sus familiares. En un principio las directivas lo habrían resuelto con visitas virtuales. Sin embargo, hasta el momento solo ha tenido a**cceso el 3% de la población reclusa** de esa cárcel, debido a la falta de infraestructura que permita la conectividad tanto en la carcel como para los familiares, pues muchos de ellos son campesinos y no tienen acceso a computadores o correos electrónicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Acá hay familias que no tienen como acceder a un computador y si lo tienen no pueden acceder a una red 4G que soporte la conexión" señala el prisionero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, exponen que deben adelantarse **los acuerdos con empresas telefónicas que garanticen esta comunicación** que "no puede ser restringida bajo ninguna circunstancia". (Le puede interesar: [«Todos tenemos derechos, ellos también»](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien-2/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, solicitan que se construya una mesa de seguimiento de derechos humanos. Misma que podría contar con la participación de las autoridades del Ministerio público, comunidad internacional, el Comité de Solidaridad con los presos políticos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente piden que **no se tome ningún tipo de represalias por parte del INPEC ni la directiva** del centro penitenciario contra los reclusos que están participando de esta huelga, a la espera de medidas que protejan sus vidas.

<!-- /wp:paragraph -->
