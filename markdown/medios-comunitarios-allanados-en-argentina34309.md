Title: Medios alternativos: víctimas de allanamientos en Argentina
Date: 2017-01-05 09:53
Category: DDHH, El mundo
Tags: Allanamientos, Argentina, Macri, medios de comunicación
Slug: medios-comunitarios-allanados-en-argentina34309
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/protesta-argentina-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Barricada TV] 

###### [5 Enero 2017] 

Al completar un año del Gobierno de Mauricio Macri, **varios medios de comunicación continuan denunciando los ataque sistemáticos de los que han sido víctimas.** El cierre de varios medios, los saqueos, los despidos y la disminución en las pautas son ahora parte del diario vivir de los periodistas en Argentina. **El hecho más reciente se presentó en las instalaciones de Resumen Lationamericano en la capital de dicho país.**

Conforme lo informaron integrantes de Resumen Lationamericano, **el pasado 1 de enero en las oficinas de dicho medio de comunicación se registró un allanamiento en el que se saquearon varios elementos** importantes para la redacción como equipos de vídeo, discos externos, tarjetas de memoria y material de archivo. Le puede interesar: [Ante la CIDH se expondrán violaciones a la libertad de expresión en Argentina](https://archivo.contagioradio.com/ante-la-cidh-se-expondran-violaciones-a-la-libertad-de-expresion-en-argentina/)

En el comunicado entregado por el medio aseguraron que "los que nos conocen y siguen en nuestra actividad (este 2017 cumplimos 24 años de trabajo contrainformativo ininterrumpido en prensa gráfica, TV, radio) saben que nos han dado un golpe durísimo pero también podrán adivinar que no vamos a retroceder. Buscaremos reequiparnos como podamos, pero RESUMEN LATINOAMERICANO no dejará de informar en estos tiempos difíciles y de discurso único (sic)".

De igual modo, en el contexto de dicha denuncia se recordaron **los hechos sufridos el pasado 12 de Diciembre por parte de la redacción de la revista Anfibio y Cosecha Roja también en la capital de Argentina**, en donde la forma de actuar fue muy similar a la sufrida por Resumen Lationamericano.

En la denuncia entregada por Resumen Lationamericano, aseguran que estos actos tienen como denominador común "la hostilidad promovida por el gobierno de Mauricio Macri contra opositores o quienes puedan cuestionar sus políticas de ajuste y represión, siendo víctimas de ello militantes sociales, sindicales, y también medios populares de comunicación". Le puede interesar: [Medidas de Macri perjudican a los argentinos más pobres](https://archivo.contagioradio.com/medidas-de-macri-convulsionan-a-los-argentinos-mas-pobres/)

Según un informe entregado en el año 2016 por la Comisión Investigadora de Atentados a Periodistas – Federación Latinoamericana de Periodistas (CIAP-FELAP), el año pasado se contabilizaron 39 periodistas, fotógrafos y comunicadores populares asesinados principalmente en México, Guatemala y Honduras, donde los gobiernos se caracterizan por sus políticas de ajuste y de represión al pueblo.

Leer aquí comunicado de Resumen Latinoamericano: <http://bit.ly/2iEpHFj>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
