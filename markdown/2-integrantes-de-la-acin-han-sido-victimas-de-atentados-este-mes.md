Title: Dos integrantes de la ACIN han sido víctimas de atentados este mes
Date: 2018-02-22 16:48
Category: DDHH, Nacional
Tags: ACIN, indígenas, Norte del Cauca
Slug: 2-integrantes-de-la-acin-han-sido-victimas-de-atentados-este-mes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nubia Acosta] 

###### [22 Feb 2018] 

La Asociación de Cabildos Indígenas del Norte del Cauca (ACIN) expresó su preocupación y encendió las alarmas frente a los **actos de violencia de los que han sido víctimas dos de sus integrantes en menos de una semana**, en el departamento del Cauca. Durante el mes de Febrero dos de sus líderes han sido víctimas de atentados y persisten las amenazas además de la incapacidad del gobierno para garantizar seguridad y vida digna.

### **Los atentados a líderes indígenas** 

El primer atentado se registró el pasado 18 de febrero contra **Ricardo Guamal, líder comunal y comunero del Resguardo indígena de Jambaló,** fue víctima de 3 impactos con arma de fuego. Frente a este hecho Edwin Lectano, responsable de derechos humanos de la ACIN, aseguró que se está investigando si el acto de violencia fue producto de un atraco o de un intento de asesinato. Guamal se encuentra hospitalizado y bajo vigilancia médica debido a la gravedad de las heridas.

El segundo acto de violencia se perpetró un día después, el 19 de febrero cuando **Enrique Fernández, sobreviviente y vocero de las víctimas de la masacre del Naya**, se encontraba en su casa con su familia, cuando se percató que le habían dejado un artefacto explosivo en la puerta de su casa. Afortunadamente, el artefacto no explotó y pudieron salir de la vivienda con su familia.

Fernández, recientemente, había denunciado que estaba siendo víctima de llamadas amenazantes de personas que luego se identificaron como miembros de las Autodefensas Gaitanistas de Colombia. (Le puede interesar: ["Así fue el acto de perdón del Ejército por asesinato de líder indígena Eleazar Tequia"](https://archivo.contagioradio.com/perdon_ejercito_asesinato_lider_indigena/))

### **Líderes indígenas del Cauca en alerta roja** 

Para Edwin Lectano, las situaciones vividas esta semana reflejan la inseguridad tanto para los líderes sociales como para las comunidades. A estos hechos se suma el anuncio por parte del gobierno nacional **de suspender los planes de sustitución de tierras y continuar con la erradicación forzada.**

“Nos preocupa la situación de nuestras familias indígenas y campesinas que están en el sector que tienen el anhelo de salir adelante y que quieren que se adelanten estos procesos de sustitución voluntaria, pero que no ven garantías, **una de ellas es el grado de incumplimiento del Estado Colombiano en relación al acuerdo de paz”** afirmó Lectano.

A su vez, el control y el aumento de la presencia de grupos armados como el ELN, grupos disidentes de la FARC y paramilitares **ha aumentado el riesgo y la violencia para las comunidades en el territorio**, sin que haya mayores acciones por parte de la Fuerza Pública. (Le puede interesar: ["Arhuacos ganan batalla contra empresa de Hidrocarburos Azabache"](https://archivo.contagioradio.com/arhuacos-ganan-batalla-contra-empresa-de-hidrocarburos-azabache/))

En ese sentido la ACIN le está exigiendo al gobierno nacional que cumpla los acuerdos en materia de sustitución de cultivos ilícitos y garantías de seguridad, sin que la única solución sea el aumento de pie de Fuerza en el territorio, sino más bien, se piense en la inversión social. Así mismo convocaron a las demás autoridades como la Defensoría del Pueblo y organizaciones defensoras de derechos humanos a que estén alertas sobre su situación.

<iframe id="audio_23980621" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23980621_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
