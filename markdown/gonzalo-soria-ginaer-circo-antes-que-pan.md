Title: Gonzalo Soria Ginaer - Circo antes que pan
Date: 2014-12-17 14:45
Author: CtgAdm
Category: Opinion
Slug: gonzalo-soria-ginaer-circo-antes-que-pan
Status: published

Sucedió en un pequeño pueblo Mediterráneo de cuyo nombre, mejor no acordarme…

El nuevo alcalde elegido por los vecinos del municipio, era un hombre de ciencia, uno de esos con vocación pragmática, metódico, alejado de populismos e ideales políticos exacerbados. “Un poco bicho raro”, comentó algún vecino. A pesar de no ser oriundo del lugar, llevaba casi dos décadas viviendo allí y en su casa en propiedad cercana a la iglesia, habían nacido sus dos hijos. Durante su estancia como nuevo farmacéutico, ganó fama de hombre cabal, discreto y aburrido.

Cuando decidió presentarse como candidato a la alcaldía, fueron muchos los que vieron con buenos ojos que la sensatez se sentara en la silla dirigente en esos tiempos locos de despilfarro y consecuente penuria. Un poco de orden y cordura “¡y mano dura si hace falta!”, apuntó alguien. Recién investido como nuevo alcalde nuestro hombre se dirigió en primer lugar a revisar las cuentas del ayuntamiento, bien sabido es que economía es lo primero. “Las cuentas claras y el chocolate espeso” le gustaba bromear sin gracia alguna.

Como no podía ser de otro modo, de claras las cuentas tenían bien poco si es que algo tenían; los presupuestos no cuadraban por ningún lado y las facturas eran cosa de fábula y poco tenían que ver con la realidad económica que pretendían reflejar. “En fin, era de esperar”, trató de animarse el señor alcalde. Concienzudamente dedicó largas noches a tratar de recontar los desfalcos, desentramar fraudes y clarificar las deudas. Cuando hubo concluido hizo públicas las cuentas: “Faltan 500 mil pesetas”, denunció, lo que vienen a ser unos 3 mil euros de ahora.

Más que por la falta de dinero en las arcas públicas, la gente se asombró de la vocación y la honestidad con las que el nuevo alcalde había emprendido su mandato y fue aplaudido y saludado cordialmente por las calles como nunca antes lo había sido. Visto que había ganado cierta admiración entre sus paisanos y entendiendo también que la falta de cultura les hacía ver el fraude como algo habitual, decidió que debía de dar algunos pasos para cambiar las mentalidades en el pueblo.

Los sucesos se produjeron a finales del verano, en vísperas de las fiestas patronales. Visiblemente alterados, un numeroso grupo de vecinos y vecinas, niños, también abuelos, se dirigieron hacia casa del alcalde formando lo que suele llamarse una marabunta. Rodearon la fachada lanzando gritos y toda clase de insultos y amenzas; hubo quien se animó con alguna piedra contra la ventana. Adentro, incrédulo y visiblemente alterado, el alcalde llamaba por teléfono a las fuerzas del orden, mientras la madre aterrorizada, alejaba a sus pequeños de las ventanas. La gente del pueblo estaba indignada con su alcalde, había prohibido las corridas de vaquillas en las fiestas. “Bien está eso de que no robe pero ¿quién se creé que es para decirnos cómo divertirnos?”

Aquella madrugada la familia del alcalde abandonó el pueblo. Durante la semana de fiestas hubo vaquillas como de costumbre.
