Title: 10 grupos paramilitares hacen presencia en Tumaco denuncian las FARC
Date: 2017-04-22 11:45
Category: Nacional, Paz
Slug: paramilitares-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/edison-romaña-e1520530757955.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [22 Abr 2017]

Luego del asesinato de Luis Alberto Ortiz Cabezas, integrante de las FARC y beneficiario de amnistía en el marco del proceso de paz con esa guerrilla, las FARC han denunciado, constantemente, la presencia de grupos paramilitares en Tumaco y en el pacífico colombiano, según ellos son **por lo menos 10 estructuras que intentan ejercer el control de la región.**

Según Romaña, ellos tienen claro que hay una complejidad de estructuras paramilitares que operan en Tumaco, Llorente, el Rosario y “eso no es un secreto de todo el pueblo nariñense ni de la fuerza pública o las demás unidades del Estado” y resalta que, aunque Nariño y Tumaco son zonas piloto para el desmantelamiento del paramilitarismo “no se ha hecho hasta el momento nada”. **Solamente en este año han asesinado 140 personas en Tumaco, denunció.** ([Le puede interesar: Precariedad, corrupción e inseguridad en Zonas Veredales](https://archivo.contagioradio.com/precariedad-corrupcion-e-inseguridad-flagelan-las-zonas-veredales/))

Edison Romaña, comandante de la Zona Veredal Transitoria de Normalización, Ariel Aldana en Tumaco, afirmó que hay mucha tristeza en los integrantes de las FARC EP que habitan allí porque Ortíz, conocido como “Pepe” era muy querido por sus compañeros. **Por ello, exigieron, una vez más, que se apliquen todas las garantías de seguridad integrales** acordadas con el gobierno.

El comandante de la ZVTN también resaltó que “a pesar del temor, también hay nostalgia porque era un guerrillero muy querido”, según explicó se ha hablado del tema, pero también del **compromiso que tienen los integrantes de la guerrilla con la implementación del acuerdo y la transición a un partido político** que se conformaría en Agosto. ([Lea también Asesinado integrante de las FARC en Tumaco](https://archivo.contagioradio.com/asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia/))

En cuanto a las garantías de seguridad, para Romaña no se trata de ponerle “guardaespaldas” a los integrantes de las FARC que en desarrollo de la libertad otorgada por la amnistía, sino que es necesario aplicar, no solamente las garantías de protección, sino también las garantías para el ejercicio de la política y la dejación de armas.

**Construcción de Zona Veredal avanza en 40%**

 Respecto de las afirmaciones del gerente de las Zonas Veredales, la zona Ariel Aldana era una de las más problemáticas, sin embargo se citó una reunión que permitió superar los impases y seguir avanzando, aunque lento se está haciendo. “Consideramos que en dos o tres meses de pronto, si siguen con el ritmo que van, hacen entrega de este campamento”.

<iframe id="audio_18310396" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18310396_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
