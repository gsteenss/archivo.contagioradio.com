Title: Nuestra estrategia será movilizarnos con alegría: UNEES
Date: 2018-11-27 12:08
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, estudiantes, Movilización, Paro Nacional
Slug: movilizarnos-unees
Status: published

###### [Foto: Contagio Radio] 

###### [27 Nov 2018] 

**Desde las 8 de la mañana de este martes se desarrolla una nueva reunión de la mesa de negociación para superar la crisis que atraviesa la educación superior.** En esta ocasión, los estudiantes esperan que el Gobierno llegue con un ánimo conciliador que permita acordar una inyección de recursos a la base presupuestal de las Instituciones de Educación Superior (IES), para culminar el presente semestre académico y asegurar la estabilidad financiera de los próximos años.

**Andrés Gómez, vocero de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, aseguró que la expectativa frente a esta reunión no es distinta a lo que vienen pidiendo estudiantes y profesores: “que el Gobierno Nacional tenga disposición para dar más recursos para la educación superior", es decir, que se atiendan las exigencias de los estudiantes, y se supere la crisis que atraviesa la educación superior.

A los métodos de presión ejercidos por las directivas de diferentes IES, y que han denunciado los estudiantes, se suman las diferentes informaciones circuladas por los medios empresariales de información en las que se afirma que los semestres serán cancelados, o suspendidos. Especulaciones que involucran a instituciones como la Universidad Nacional, sede Bogotá, o la Universidad Pedagógica Nacional.

> Universidad Pedagógica Nacional descarta cancelar o suspender semestre académico<https://t.co/hIOnOMDk6h> [pic.twitter.com/fWYjJ2aQGr](https://t.co/fWYjJ2aQGr)
>
> — UPedagógicaNacional (@comunidadUPN) [27 de noviembre de 2018](https://twitter.com/comunidadUPN/status/1067439900444803073?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Para Gómez, **dichas informaciones responden a una matriz mediática de noticias falsas, impulsada por el Gobierno, que busca presionar las bases del movimiento estudiantil**; razón por la que se anuncia una negociación difícil. Pese a ello, el estudiante declaró que la cancelación de semestres es una decisión puramente política, que no puede excusarse desde argumentos jurídicos o financieros.

### **Movilizaciones y paro estudiantil continúan** 

El estudiante declaró que en el país hay dos grandes consensos: **La defensa de la educación superior y el apoyo al movimiento universitario; y el rechazo a la Ley de Financiamiento.** Por lo tanto, todos los sectores sociales están dialogando para construir un paro cívico nacional, que en el caso de los estudiantes, se extendería a diciembre si es necesario. (Le puede interesar: ["Caravanas de estudiantes avanzan hacia gran movilización por la educación superior"](https://archivo.contagioradio.com/caravanas-de-estudiantes-avanzan-hacia-gran-movilizacion-por-la-educacion/))

Gómez anunció que la estrategia de los estudiantes será la movilización con alegría, y la siguiente gran muestra de ello se llevará a cabo este miércoles, jornada en la cual, campesinos, trabajadores, profesores, indígenas, transportadores y estudiantes realicen tomas de las capitales del país, desde tempranas horas de la mañana. (Le puede interesar: ["Así va la mesa de negociación entre estudiantes y Gobierno"](https://archivo.contagioradio.com/negociacion-estudiantes-gobierno/))

[![WhatsApp Image 2018-11-27 at 12.23.54 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-27-at-12.23.54-PM-670x800.jpeg){.size-medium .wp-image-58721 .aligncenter width="670" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-27-at-12.23.54-PM.jpeg)

[![WhatsApp Image 2018-11-27 at 12.24.01 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-27-at-12.24.01-PM-616x800.jpeg){.size-medium .wp-image-58722 .aligncenter width="616" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-27-at-12.24.01-PM.jpeg)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
