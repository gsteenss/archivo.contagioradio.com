Title: Peñalosa no da respuesta a futuro laboral de más de 2000 trabajadores de Aguas de Bogotá
Date: 2018-02-02 14:42
Category: Movilización
Tags: Aguas de Bogotá, Bogotá, protesta de trabajadores, trabajadores de aseo
Slug: penalosa-no-da-respuesta-a-futuro-laboral-de-mas-de-2000-trabajadores-de-aguas-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Aguas_Bogotá_Protesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [02 Feb 2018] 

Tres días completa la movilización de los trabajadores de la empresa Aguas de Bogotá, quienes protestan por el inminente despido de **3.700 personas** luego de que el Distrito realizara una licitación para recoger la basura en la capital que no incluyó a esa empresa. Ellos y ellas han denunciado los ataques del ESMAD contra las personas que se encuentran protestando.

**¿Qué está pasando con los trabajadores de Aguas de Bogotá?**

De acuerdo con Miguel Rivera, trabajador de Aguas Bogotá, “desde el 19 de diciembre cuando se presentaron los pliegos de licitación y **Aguas de Bogotá quedó por fuera**, las organizaciones sindicales decidimos reunirnos para defender a los 3.700 trabajadores que estamos esperando un acuerdo de formalización laboral”.

Desde entonces realizaron una mesa de negociación en conjunto con la Personería, la Procuraduría y demás entes de control. Allí, las autoridades “**no garantizaron la estabilidad laboral** de las personas y ellos estaban contando con ubicar a la gente en otros puestos del Distrito, pero como entramos en ley de garantía, no nos pueden contratar”. (Le puede interesar: ["Alcalde Peñalosa es irresponsable y no respeta la Corte Constitucional"](https://archivo.contagioradio.com/alcalde-penalosa-es-irresponsable-y-no-respeta-a-la-corte-constitucional-recicladores/))

Adicional a esto se han reunido con las empresas privadas que ahora van a prestar el servicio “y sólo nos garantizaron trabajo para **1.200 personas”**, por lo que 2.500 personas quedaron sin opción laboral. Rivera enfatizó en que estas personas se encuentran en vulnerabilidad extrema en la medida en que son una población “cerca de pensionarse, con discapacidades adquiridas en la empresa, madres cabeza de hogar y gestantes”.

### **ESMAD ha atacado a los manifestantes** 

Los trabajadores han denunciado que la alcaldía de Bogotá ha tenido una actitud represiva debido a que el **ESMAD ha tratado de eliminar la protesta** utilizando gases lacrimógenos, bombas aturdidoras y balas de goma. Ante las denuncias de actos vandálicos por parte de los trabajadores, ellos dicen tener pruebas de que no son ciertas y los daños fueron provocados por la Policía.

Sin embargo, Elsa Peña, trabajadora de la empresa y quien hace parte de la mesa que evalúa la situación de los trabajadores, argumenta que “por lo menos la mitad de las organizaciones sindicales que hacemos parte de Aguas de Bogotá, no participamos de los **actos vandálicos** que se llevaron el día de ayer”.

### **Derechos fueron vulnerados a los trabajadores** 

En lo que concuerdan todos los sindicatos es en que a los trabajadores **les vulneraron los derechos** al no tener en cuenta a la empresa para realizar la licitación. Desde la mesa de trabajado le han hecho un llamado a los entes del Distrito para que se busquen condiciones que garanticen el traslado a diferentes puestos de trabajo “con condiciones dignas”. (Le puede interesar: ["Distrito ampliaría hasta 2070 la existencia del relleno sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

Ellos y ellas han pedido que se le den proyectos a la empresa Aguas de Bogotá, que entre otras cosas es filial del Acueducto, “para que la gente pueda permanecer allí”. En la mesa, han concertado el traslado de 1.200 personas en las demás empresas que licitaron “pero las condiciones económicas **son mucho menores** que las que tenemos en Aguas de Bogotá”.

Finalmente, los trabajadores han insistido en que la protesta se debe llevar de manera pacífica y que el Distrito debe **respetar sus derechos laborales**. Insisten en que las convocatorias de los demás operadores no son suficientes y excluyen a las personas que tengan más de 50 años y también a la población femenina.

<iframe id="audio_23519557" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23519557_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
