Title: Denuncian montaje judicial en contra de Angie Solano, docente y activista social
Date: 2020-09-29 18:05
Author: CtgAdm
Category: Expreso Libertad
Tags: #AngieSolano, #Fiscalía, #GeneralSantander, #Malegría, #Montajesjudiciales
Slug: denuncian-montaje-judicial-en-contra-de-angie-solano-docente-y-activista-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/angie.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 2 de julio fue capturada en Manizales, Angie Solano docente y activista social, señalada por la Fiscalía de haber participado en el atentado en contra de la Escuela Gral Santander. Sin embargo, de acuerdo con su defensa, no hay material probatorio frente a este hecho. Razón por la cual afirman que se estaría gestando un **montaje judicial**.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La captura de Angie Solano
--------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre las dudas que hay sobre el proceso se encuentran el momento de su captura, el accionar de altos funcionarios del país y el material probatorio en su contra. Un conjunto de inconsistencias que vulneran sus derechos al debido proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Solano, fue capturada cuando llegó a visitar a su familia en Manizales debido a que residía en Argentina, en donde desarrollando sus estudios de maestría. El proceso fue adelantado junto a tres allanamiento a las viviendas de sus familiares, pese a que la mujer ya no vivía allí.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_57222687" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_57222687_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

No obstante, la orden de captura había sido emitida desde un mes antes de su arribo al país. Hecho que para Santiago Salinas, abogado defensor de Solano, evidencia una intención **"mediática"** del arresto, porque pudo realizarse en el aeropuerto, durante los puestos de control. (Le puede interesar:["Julián Gil: A dos años del montaje judicial del líder social"](https://archivo.contagioradio.com/julian-gil-a-dos-anos-del-montaje-judicial-del-lider-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumada a esta situación, Salinas afirma que la captura se produce en medio de una presión política, "[veíamos en los medios al presidente de la República y Ministro de Defensa,](https://archivo.contagioradio.com/particulas-de-libertad-una-radionovela-sobre-los-montajes-judiciales-en-colombia/) actuando como jueces en el marco del proceso, es decir emitiendo **una condena pública**".

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las pruebas en los medios vs pruebas en la audiencia
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado expresa en que mucha de la información revelada por medios de información **no está contenida en el expediente**. Como también, manifiesta que ha sido por estos que se enteraron de acciones como seguimientos a Solano por parte de la Fiscalía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo asegura que de las 304 páginas del informe presentado por la Fiscalía, **solo en 27 se relaciona a Angie con el atentado** y están dispersas. Situación que para el defensor evidencia una ausencia de garantías en este proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"No estamos seguros si lo que se está debatiendo son aquellos contenidos **presentado en los medios masivos**, o si lo que se está debatiendo es en efecto los elementos puestos de manifiesto en el marco del proceso" señala Salinas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En torno a las imágenes que, según la Fiscalía, habrían identificado a Angie como la persona que acompaña al vehículo del atentado, en una moto; aún el ente **no ha podido comprobar que** quien **aparece en las imágenes es Solano.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Quién es Angie Solano
---------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Fabio Solano, padre de la docente y estudiante, afirma que su hija se ha destacado por su pasión a la educación; "desde muy pequeña ha sido muy juiciosa y siempre quiso ser maestra" y agrega que su **único delito ha sido estudiar.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además señala que la razón para que Angie realizaba sus estudios en el exterior son los altos costos de la educación superior en Colombia. Actualmente Solano se encuentra cursando la maestría en historia en la Universidad de San Martín de Buenos Aires.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, compañeros, familiares y docentes crearon la campaña Malegría y Libertad. Apuesta que busca defender la inocencia de Angie Solano y exigir su libertad en el marco de lo que ellas y ellos denuncian como un montaje judicial. (Le puede interesar: ["Campaña Malegría y Libertad"](https://www.facebook.com/Campa%C3%B1a-Malegr%C3%ADa-y-Libertad-100555571736419))

<!-- /wp:paragraph -->
