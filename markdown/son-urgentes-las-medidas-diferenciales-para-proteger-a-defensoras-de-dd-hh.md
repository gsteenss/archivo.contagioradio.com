Title: Son urgentes las medidas diferenciales para proteger a defensoras de DD.HH.
Date: 2019-07-04 12:49
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: Amenazas contra defensoras de derechos, amenazas contra líderes sociales, Defensoría del Pueblo, violencia sexual
Slug: son-urgentes-las-medidas-diferenciales-para-proteger-a-defensoras-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Defensores-Asesinados-e1477508845527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

En el marco de la Mesa de Protección a la Vida de las Mujeres en Cali, el **defensor Nacional del Pueblo, Carlos Negret advirtió** que al menos **983 líderes sociales han sido intimidados en el país y que de esta población el 50% son mujeres,** un dato que da cuenta de lo necesarias que son las medidas de protección diferenciales para las defensoras de derechos humanos y que hasta el momento no han sido aplicadas.

**La directora de la Corporación Humanas, Adriana Benjumea** expresó su preocupación y señaló que es imposible desconocer cómo, después de la firma del Acuerdo de la Habana, "a los líderes y lideresas les está costando la vida trabajar por la paz", sin embargo destacó que esta es la primera vez en que las alertas tempranas registran la situación de las defensoras, pese a que las mujeres siempre han ejercido labores de liderazgo en los territorios y siempre han sido amenazadas.

Según el defensor del pueblo, entre marzo de 2018 y mayo de 2019 **cerca de 480 lideresas o defensoras de derechos humanos fueron agredidas, una estadística que incluye 447 amenazas, 20 homicidios y 13 atentados;** un alarmante dato que reitera a la sociedad, la importancia de no ignorar los panfletos amenazantes que circulan en los territorios.

**"Hay panfletos que eternamente las han nombrado, han sido amendrentadas incluso con particularidades, amenazas de tipo sexual  e intimidaciones que incluyen a sus hijos y sus hijas"**, por lo que según Benjumea, ahora al conocerse estas alertas, no es posible que el Gobierno sea indiferente ante esta situacón pues, "la ruta ya está dada, el sistema de alertas tempranas únicamente es un aviso previo,  pero la alerta no soluciona si no hay una acción de protección", cuestionó.

### ¿Cómo proteger a las mujeres defensoras de derechos? 

La directora de Humanas indicó que el Acuerdo de Paz incluye más de 100 medidas de enfoque de género por lo que lo primero que hay que hacer es asegurar la implementación de dichas garantías, además resaltó que de cara a un proceso electoral, las políticas del Gobierno "deben agudizar sus acciones y recoger las observaciones que han hecho las plaaformas de mujeres para prevenir los riesgos sobre los que han alertado".

Finalmente agregó que la Fiscalía tiene una gran responsabilidad  de establecer un enfoque diferencial en sus investigaciones, al revisar quienes son los que están detrás de los panfletos amenazantes y por qué en estos son incluidas no solo ellas sino también sus familias. [(Lea también: Informe revela aumento en violencias contra las lideresas en Colombia)](https://archivo.contagioradio.com/62024-2/)

### El importante mensaje de la Comisión de la Verdad a las mujeres 

Respecto al primero de los Encuentros por la Verdad, centrado en el reconocimiento de las mujeres y personas LGBTI víctimas de las violencias sexuales en el conflicto armado celebrado el pasado 26 de junio por la  Comisión de la Verdad, Adriana Benjumea señaló que la participación de más de 650 personas, en su mayoría mujeres y personas LGBTI, "levantaron la voz por otras mujeres que no pudieron estar" por un tipo de violencia que no se ha podido prevenir y que sigue siendo indiferente para la sociedad colombiana.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38021886" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38021886_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
