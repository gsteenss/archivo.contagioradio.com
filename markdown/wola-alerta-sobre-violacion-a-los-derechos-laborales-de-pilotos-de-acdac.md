Title: WOLA alerta sobre violación a los derechos laborales de pilotos de ACDAC
Date: 2018-03-02 12:32
Category: DDHH, Nacional
Tags: ACDAC, Avianca, Huelga de pilotos, pilotos de avianca, WOLA
Slug: wola-alerta-sobre-violacion-a-los-derechos-laborales-de-pilotos-de-acdac
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKmPaWSWAAoR06c-e1506535314979.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CUT] 

###### [02 Mar 2018] 

Desde que empezaron los procesos disciplinarios de la aerolínea Avianca contra los pilotos agremiados en la Asociación Colombiana de Aviadores Comerciales (ACDAC) el 25 de febrero, han sido despedidos **74 trabajadores**. Ante esta problemática, la Oficina en Washington para América Latina (WOLA) le hizo un llamado al Estado Colombiano para que proteja los derechos laborales de los pilotos.

La organización no sólo alertó sobre la situación laboral, sino que también llamó la atención de la **decisión de la Corte Suprema** de declarar ilegal la huelga que duró 54 días a finales del año pasado en donde los trabajadores pedían mejores condiciones laborales. Frente a esto, afirmó que la decisión “va en contra de fallos anteriores que resolvían casos de negociación colectiva similares”.

### **Corte Suprema violó el derecho constitucional a la huelga** 

Enfatizó en que, el argumento de que el transporte aéreo es un servicio público esencial, no puede servir de excusa para **negar el derecho constitucional a la huelga** que tienen los trabajadores en Colombia. Dijo que la Corte ignoró las recomendaciones hechas por la Organización Internacional del Trabajo y la convención 087 del mismo que establece la libre asociación y la protección de este derecho.

Además, recalcó que la decisión del tribunal de arbitramento propuesto por el Ministerio de Trabajo, “no le permitía a Avianca **despedir a los pilotos que participaron en la huelga**”. En esta medida, “Avianca, inmediatamente, desafió las decisiones del árbitro” y los procesos disciplinarios actuales van en contra vía de lo establecido. (Le puede interesar:["Fallo sobre ilegalidad de huelga de pilotos golpea la libertad sindical"](https://archivo.contagioradio.com/fallo-sobre-ilegalidad-de-huelga-de-pilotos-golpea-la-libertad-sindical/))

### **Avianca no ha garantizado audiencias justas** 

Frente a estos procesos disciplinarios, WOLA enfatizó en que “están siendo llevados a cabo bajo **serias violaciones al debido proceso**”. Esto teniendo en cuenta que a los pilotos de ACDAC no se les ha garantizado una audiencia justa o la presentación de una defensa legal. “De acuerdo con ACDAC, Avianca ha procedido a despedir a los pilotos con actos de intimidación con el propósito de aniquilar la unión”.

Por su parte, la Central Unitaria de Trabajo (CUT), que es una de las centrales de unión sindical más grande del país, **ha rechazado la decisión de la aerolínea** de despedir a sus pilotos y anunció que interpondrá una queja formal ante la Organización Internacional del Trabajo.

Finalmente, WOLA hizo un llamado al Ministro de Trabajo para que garantice la **protección de los pilotos** que participaron en la huelga y además pidió que los procesos disciplinarios se realicen con las plenas garantías de un juicio justo y afirmó que “los pilotos no pueden enfrentar represalias de Avianca y accionistas".

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
