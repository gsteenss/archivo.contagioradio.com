Title: Pérdida de curul de Ángela María Robledo es un golpe a la oposición
Date: 2019-04-25 16:27
Author: CtgAdm
Category: Nacional, Política
Slug: perdida-de-curul-de-angela-maria-robledo-es-un-golpe-a-la-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-76.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

Como un fuerte golpe a la oposición ha sido catalogada la reciente decisión del Consejo de Estado que en su Sección Quinta optó por retirar la curul de la representante a la Cámara Ángela María Robledo, que ocupaba ese cargo gracias al Estatuto de la oposición, aprobado en el marco de los acuerdos de paz.

Con tres votos a favor y uno en contra, el Consejo determinó que la representante había incurrido en doble militancia política, **al no renunciar de su cargo como congresista doce meses antes de presentarse como candidata a la vicepresidencia** por el movimiento Colombia Humana.

Por su parte, la representante dijo que si bien respeta la decisión de la institución, también lo considera "un golpe" para la democracia, a la oposición y a los **ocho millones de colombianos** que votaron por la Colombia Humana en la segunda vuelta. (Le puede interesar: "[Colombia Humana presentará candidatos por firmas tras decisión del CNE](https://archivo.contagioradio.com/cne-niega-por-segunda-vez-personeria-juridica-a-la-colombia-humana/)")

> No me quitan la curul a mí, sino a ocho millones de colombianos/as que por Constitución y el Estatuto de la Oposición tienen el derecho a ella.
>
> Estoy serena pero con profundo dolor. Respeto el fallo del Consejo de Estado aunque no lo comparta. [pic.twitter.com/66XKuyXIbP](https://t.co/66XKuyXIbP)
>
> — Ángela María Robledo (@angelamrobledo) [25 de abril de 2019](https://twitter.com/angelamrobledo/status/1121512760020434945?ref_src=twsrc%5Etfw)

> — Gustavo Petro (@petrogustavo) [25 de abril de 2019](https://twitter.com/petrogustavo/status/1121479373377478657?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Decisión de anular la elección de [@angelamrobledo](https://twitter.com/angelamrobledo?ref_src=twsrc%5Etfw) viola gravemente los derechos de la oposición, y desconoce la voluntad de ocho millones de electores que votaron por ella y por [@petrogustavo](https://twitter.com/petrogustavo?ref_src=twsrc%5Etfw).
>
> — Iván Cepeda Castro (@IvanCepedaCast) [25 de abril de 2019](https://twitter.com/IvanCepedaCast/status/1121482584385576963?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> He acompañado a [@angelamrobledo](https://twitter.com/angelamrobledo?ref_src=twsrc%5Etfw) porque creo en su integridad, coherencia y valentía
>
> Su voz en la [@CamaraColombia](https://twitter.com/CamaraColombia?ref_src=twsrc%5Etfw) es la voz de 8 millones de personas y mas que hoy son silenciadas. Se violan con este fallos lo derechos de la Oposición estrenando estatuto.
>
> Mi afecto y solidaridad <https://t.co/ANmS2jqFqd>
>
> — María José Pizarro Rodríguez (@PizarroMariaJo) [25 de abril de 2019](https://twitter.com/PizarroMariaJo/status/1121479425873281024?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Yo me pregunto por qué dejaron a Martha Lucía Ramírez en la Vicepresidencia y sacan a Angela Maria del Congreso.  
> Los dos casos son iguales.
>
> Para mí es una persecución a la oposicion política... Qué democracia!!!!
>
> — Aída Avella E (@AidaAvellaE) [25 de abril de 2019](https://twitter.com/AidaAvellaE/status/1121478615496036352?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Este es el Estado colombiano, que quiere por todos los medios acabar con quienes hacemos oposición a su nefasto y regresivo proyecto. [\#AngelaMaríaEstamosContigo](https://twitter.com/hashtag/AngelaMar%C3%ADaEstamosContigo?src=hash&ref_src=twsrc%5Etfw) te seguiremos acompañando por que sabemos la valiosa que eres para este país. [pic.twitter.com/iGu8yzpOT1](https://t.co/iGu8yzpOT1)
>
> > He acompañado a [@angelamrobledo](https://twitter.com/angelamrobledo?ref_src=twsrc%5Etfw) porque creo en su integridad, coherencia y valentía
> >
> > Su voz en la [@CamaraColombia](https://twitter.com/CamaraColombia?ref_src=twsrc%5Etfw) es la voz de 8 millones de personas y mas que hoy son silenciadas. Se violan con este fallos lo derechos de la Oposición estrenando estatuto.
> >
> > Mi afecto y solidaridad <https://t.co/ANmS2jqFqd>
> >
> > — María José Pizarro Rodríguez (@PizarroMariaJo) [25 de abril de 2019](https://twitter.com/PizarroMariaJo/status/1121479425873281024?ref_src=twsrc%5Etfw)
>
> <p>
> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
> </p>
> — Victoria Sandino (@SandinoVictoria) [25 de abril de 2019](https://twitter.com/SandinoVictoria/status/1121489441686728704?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
