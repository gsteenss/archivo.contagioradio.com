Title: No avanza investigación por interceptaciones ilegales del ejército a 4 meses  de la denuncia
Date: 2020-09-10 14:55
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Ejército Nacional, FLIP, Perfilamientos ilegales
Slug: no-avanza-investigacion-por-interceptaciones-ilegales-del-ejercito-a-4-meses-de-la-denuncia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ejercito-chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Ejército Nacional - El País

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque han pasado cuatro meses desde que se revelaron las operaciones de espionaje ilegales del Ejército en contra de políticos, periodistas nacionales y extranjeros, así como organizaciones de abogados y no gubernamentales en Colombia; la [Fundación para la Libertad de Prensa (FLIP)](https://twitter.com/FLIP_org) expresó su preocupación pues siguen sin proporcionarse garantías para la libertad de prensa ni para la protección a las fuentes ante los pocos avances que se han evidenciado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que el pasado 20 de mayo, la Procuraduría formuló pliego de cargos a dos generales en retiro, cinco coroneles, tres mayores, un teniente y dos suboficiales pertenecientes a unidades de inteligencia, aún no se ha fijado una fecha para una audiencia de juicio disciplinario. [(Lea también: Las violaciones a la libertad de prensa por parte del Ejército Nacional)](https://archivo.contagioradio.com/las-violaciones-a-la-libertad-de-prensa-por-parte-del-ejercito-nacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma durante el mismo mes, la Fiscalía anunció que desde el mes de enero se había comenzado una investigación por los delitos de violación ilícita de comunicaciones, uso ilícito de equipos trasmisores, entre otros delitos; caso que incluso **llamaría a interrogatorio el general (r) Nicacio Martínez, meses después, no se ha conocido más acerca de dicha audiencia.** [(Le recomendamos leer: Prensa bajo la mira de actividades ilegales del ejército en Colombia)](https://archivo.contagioradio.com/prensa-bajo-la-mira/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Fiscalía no da respuestas sobre caso que vincula al Ejército

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La Fiscalía también anunció que se investigaría y se buscaría información sobre los equipos incautados en el allanamiento ordenado por la Sala de Instrucción de la Corte Suprema de Justicia en diciembre de 2019 y que fueron usados para dichos perfilamientos, sin embargo **tampoco se conocen vinculaciones formales al proceso**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque la FLIP ha ejercido presión a través de derechos de petición para conocer los nombres de los periodistas víctimas de seguimientos, la Fiscalía no ha brindado mayor información al respecto, al contrario sostienen que no se trataría de 130 personas perfiladas, como informó Semana sino que sería un número inferior a 20.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a hechos que la FLIP había advertido **podrían ser una de las agresiones más graves registradas en los últimos años, debido a su masividad y su extensa cobertura,** resaltan que la carencia de medidas ha impedido "esclarecer de manera pronta la verdad sobre las vigilancias del Estado" y así prevenir que vuelvan a ocurrir.[(Le puede interesar: Seguimientos contra periodistas son propios de regímenes totalitarios: FLIP)](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidad internacional expresó su preocupación

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Es de resaltar que el pasado 19 de agosto, cuatro **Relatores Especiales de Naciones Unidas y la Relatoría de Libertad de Expresión de la Comisión Interamericana de Derechos Humanos** manifestaron al Gobierno su preocupación sobre dichas actividades de vigilancia y la elaboración de perfiles por parte del Ejército,

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, integrantes del Congreso de EE.UU. no solo han pedido explicaciones al Gobierno sobre la situación que ocurre al interior del Ejército, congresista como Jim McGovern han pedido suspender las ayudas militares y revisar lo que se está haciendo con la ayuda militar de ese país en Colombia. [(“Estados Unidos debería suspender la ayuda militar a Colombia" Congresista Jim McGovern)](https://archivo.contagioradio.com/jim-mcgovern-suspender-ayuda-militar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque los relatores advirtieron que dichos perfilamientos, "someten a los periodistas a un riesgo elevado e interfieren con el desarrollo del ejercicio periodístico". Pese a ello, el Gobierno no ha respondido de manera completa los interrogantes que ha generado el actuar de las Fuerzas Militares [(Lea también: Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.)](https://archivo.contagioradio.com/espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante ello, la FLIP señala que es necesario "adelantar con celeridad y sancionar ejemplarmente a los responsables" en medio de las investigaciones o de lo contrario existe la posibilidad de que los hechos permanezcan en la impunidad. [(Le puede interesar: Medios independientes son la otra víctima de la Fuerza Pública en la movilización)](https://archivo.contagioradio.com/medios-independientes-son-la-otra-victima-de-la-fuerza-publica-en-la-movilizacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
