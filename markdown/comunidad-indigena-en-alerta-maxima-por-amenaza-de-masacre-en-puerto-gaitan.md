Title: Comunidad indígena en alerta máxima por amenaza de masacre en Puerto Gaitán
Date: 2017-08-28 17:32
Category: DDHH, Nacional
Tags: desplazados, Puerto Gatián
Slug: comunidad-indigena-en-alerta-maxima-por-amenaza-de-masacre-en-puerto-gaitan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/el-espectador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [28 Ago 2018]

Indígenas pertenecientes a la comunidad Kubeo-Sikuani, en la inspección del porvenir, del municipio de Puerto Gaitán en Meta, estarían **en riesgo de ser masacrados y desplazados, por parte de una estructura armada que estaría operando en el territorio**, así lo denuncio la Corporación Claretiana Norman Pérez.

De acuerdo a la denuncia, sobre la 8 de la mañana del 28 de agosto, cuando un hombre llego a la vivienda del gobernador indígena Mauro Chipaje para manifestarle que “en horas de la noche de hoy, llegarían **hombres armados en motos y carros para desplazar a toda la comunidad de los predios**” donde viven actualmente y que él venía a informarles para evitar una masacre. El sujeto sería el cuidador de “Matarredonda”, una porción de tierras invadida por integrantes de la familia Sierra,

**Las amenazas a habitantes del Porvenir no son nuevas**

Desde el año 2010, la comunidad campesina e indígena de El Porvenir ha recibido amenazas por parte de hombres que se identifican como integrantes de las **Águilas Negras, El Clan Úsuga, Las Autodefensas Giatanistas de Colombia, La Empresa**, entre otros, amedrentando a la comunidad y evitando que las personas permanezcan en su tierra.

Entre el año 2014 al 2015, **5 personas, pertenecientes a la comunidad fueron asesinadas, mientras que líderes de las veredas Matarratón y El Porvenir** fueron amenazados, generando desplazamiento de los habitantes de sus propiedades. (Le puede interesar:["Paramilitares amenazan a familiares de El Porvenir, en Puerto Gaitan"](https://archivo.contagioradio.com/paramilitares-amenazan-familias-porvenir-puerto-gaitan-meta/))

Por su parte, los indígenas Kubeo-Sikuani se asentaron en El Porvenir, desde el año 2015, retornando luego de vivir la violencia, sin embargo, desde que llegaron, han denunciado **ser víctimas sistematicas de atentados y amenazas que buscan provocar la salida definitiva del pueblo indígena del territorio**.

Además, han manifestado que personas externas a la comunidad llegaron a ocupar el territorio y posteriormente fueron víctimas de atentados con armas de fuego y tuvieron que refugiarse en casas de campesinos para salvaguardarse. De igual forma, aseguraron que todos estos hechos **son de conocimiento público de las autoridades sin que se haya tomado medidas para cuidar la vida de los indígenas. **(Le puede interesar: ["Corte Constitucional ordena adjudicación a comunidades en Puerto Gaitán Meta"](https://archivo.contagioradio.com/corte-constitucional-ordena-adjudicacion-tierras-comunidades-puerto-gaitan/))

Frente a esta situación los indígenas expresaron que se encuentran en alerta máxima y exigieron que diferentes autoridades como la Policía Nacional y el Ejercito, garanticen los derechos a la vida, la tierra y a no ser desplazados de sus propiedades; a la Fiscalía General y a la Procuraduría, les solicitaron que se adelanten las investigaciones correspondientes sobre quiénes estarían detrás de estas amenazas y atentados; y las autoridades locales que se acate la sentencia de la Corte Constitucional en donde se ratifica a la comunidad indígena como tenedora de la tierra.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
