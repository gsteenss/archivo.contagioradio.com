Title: Renace Bancada Animalista en el Congreso de la República
Date: 2015-04-23 12:26
Author: CtgAdm
Category: Animales, Nacional, Voces de la Tierra
Tags: 087, Alianza Verde, Angélica Lozano, Animales, Animalistas, Antonio Navarro, Claudia López, Congreso, Juan Carlos Lozada, Maltrato animal, Partido Liberal, Pertido Conservador, Plataforma Alto, Polo Democrático, Príncipe
Slug: renace-bancada-animalista-en-el-congreso-de-la-republica
Status: published

##### Foto: Plataforma ALTO 

<iframe src="http://www.ivoox.com/player_ek_4397216_2_1.html?data=lZimmZeVeo6ZmKiakpuJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhsLixMbRw5DFssrhwtHW1dnFb9TZjM7a0trQt8KfxcrgxsqPqc2fpNTbydfJt9CfxcqYtMrUaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Catalina Tenjo, Plataforma Alto] 

Senadores y representantes de diferentes partidos políticos, asistieron a un desayuno organizado  por movimientos animalistas, el pasado 22 de abril en el Congreso de la República. El objetivo era retomar la bancada animalista, para **impulsar los compromisos por la defensa de los animales en Colombia**, específicamente para apoyar el **Proyecto de Ley  087** del representante a la cámara Juan Carlos Lozada, por el cual se penalizaría el maltrato animal.

Catalina Tenjo, de la Plataforma Alto e integrante de la mesa de trabajo del programa Onda Animalista de Contagio Radio, expresó que fue importante evidenciar que pese a las diferencias políticas, se contó con la participación de diversos partidos como, el Liberal, Polo Democrático Alternativo, Alianza Verde, Conservador y Centro democrático, lo que demuestra que **“el animalismo es una variedad, nos une la defensa y protección de los animales”.**

Congresistas como Angélica Lozano, Claudia López y Antonio Navarro no pudieron asistir pero aseguraron todo su apoyo a la causa animalista.

Además se contó con la participación de **Marina Isaza, la mamá del perro llamado Príncipe**, que fue víctima de Juan Sebastián Toro, quien en medio de una discusión con sus dueños decidió dispararle. Marina, **entregó 28.313 firmas** recolectadas a través de Chage.org donde se muestra el **apoyo de la ciudadanía al Proyecto de Ley** para la protección de los animales no humanos.
