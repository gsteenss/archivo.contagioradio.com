Title: Roban información relacionada con labor de líder Temistocles Machado en Buenaventura
Date: 2018-02-08 15:18
Category: DDHH, Nacional
Tags: buenaventura, Derechos Humanos, lideres sociales, Paro cívico
Slug: roban-informacion-relacionada-con-labor-de-lider-temistocles-machado-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticiero 90 Minutos] 

###### [08 Feb 2018] 

El Comité Inter Organizacional por la Defensa de los Territorios en Buenaventura denunció que uno de sus líderes sociales fue víctima de un robo de información relacionada con los casos sobre **despojos de tierras de esa zona del país.** Con este hecho, sumado al asesinato del líder Temístocles Machado el 27 de enero, desde el Comité afirman que se trata de una estrategia para desarticular las labores de dicha organización que ha trabajado en defensa de los derechos étnicos y colectivos.

De acuerdo con la denuncia, el día 30 de enero el ex sacerdote Adriel José Ruiz Galván, defensor de derechos humanos y líder social del barrio Lleras ubicado en Buenaventura, se percató de que **la puerta de su vivienda estaba abierta** “pero no violentada, entonces verifica que el computador no está y al revisar su mochila observa que estaba vaciada y que su billetera con todos sus documentos tampoco se encuentra, entonces revisa todo el apartamento y ve que otras cosas de valor como cámara filmadora y electrodomésticos están allí”.

En su computador “contiene la información de los casos de **derechos humanos que adelanta la fundación FUNDESCODES**, y el comité”. El defensor, “había hecho declaraciones públicas, tanto por los medios de comunicación como en medio de lasn exequias que se llevaron a cabo en el mismo lugar en donde fue asesinado el líder Temístocles Machado”. (Le puede interesar: ["La lucha por la que habrían asesinado a Temístocles Machado"](https://archivo.contagioradio.com/temistocles_machado_lider_buenaventura_paro_asesinado/))

Tras los hechos, el señor Ruiz interpuso la denuncia por hurto el 5 de febrero “ante la instancia de policía judicial que tiene a cargo la investigación del asesinato de **Temistocles Machado**”. Esto, teniendo en cuenta que “hay una relación del robo con el asesinato de Machado, dado que ambos son miembros del comité inter-organizacional y del proceso paro cívico”, dice el comunicado de la denuncia.

### **Exigen garantías para la defensa de los derechos humanos en Buenaventura** 

Ante estos hechos, el Comité le manifestó al Gobierno Nacional el riesgo en el que se encuentra **el Comité Inter-organizacional** por la defensa de los territorios ganados al mar, que es una base articulada al paro cívico de Buenaventura. En esa medida le solicitan a las diferentes organizaciones sociales nacionales e internacionales que “sumen esfuerzos” para que se prevengan “hechos que atenten contra la integridad física y psicológica de Adriel Ruiz, Fundescodes, el comité inter organizacional y el comité de paro cívico de Buenaventura”.

Además, le exigieron al Gobierno que cumpla con lo estipulado el pasado 6 de junio de 2017 cuando se establecieron una serie de acuerdos que buscan **garantizar las condiciones dignas de las comunidades de Buenaventura** que se han visto afectadas por la dinámica de la expansión portuaria. Finalmente manifestaron que es necesario que se establezcan “medidas urgentes con carácter extraordinario para prevenir cualquier hecho que vulnere los derechos de las comunidades, líderes sociales y defensores de derechos humanos”.

###### Reciba toda la información de Contagio Radio en [[su correo]
