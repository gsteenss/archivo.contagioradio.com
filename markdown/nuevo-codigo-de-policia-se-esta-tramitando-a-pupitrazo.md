Title: Nuevo Código de Policía se está tramitando "a pupitrazo"
Date: 2016-06-07 16:25
Category: DDHH, Nacional
Tags: código de policía, ESMAD, Minga Nacional
Slug: nuevo-codigo-de-policia-se-esta-tramitando-a-pupitrazo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/código-de-policía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: bogota.gov.co 

###### [7 Jun 2016] 

A “pupitrazo” se está tramitando en el Congreso de la República el nuevo Código de Policía, sin que los congresistas lo analice y discutan, es lo que denuncia el representante del Polo Democrático, Alirio Uribe, quien hace poco propuso que se desmontara el ESMAD.

“Ni los representantes conocen que fue lo que aprobaron el pasado jueves, no se conoce la ponencia final, se votaron 173 artículos en bloque sin ninguna discusión”, asegura el congresista quien reconoce que el Código de Policía debe cambiarse, pero no, sin  que el legislativo lo discuta artículo por artículo, sobre todo teniendo en cuenta el escenario de posconflicto que se avecina.

**Dar facultades a la policía para disolver una protesta social, allanamientos sin orden judicial, detenciones arbitrarias, y el uso del espacio público,** entre otras disposiciones, son algunos de los puntos más polémicos de este nuevo Código.

“El congreso siempre le hace el juego al gobierno, entiendo que el presidente estaba llamando a las bancadas para sacar adelante el código de policía”, señala el representante.

Frente a la propuesta del desmonte del Escuadrón Antidisturbios, Uribe Muñoz indica que el Paro nacional es la prueba fehaciente de que ese aparato debe ser eliminado, sin embargo afirma que el ambiente en el Congreso no está dispuesto para que esa iniciativa sea aprobada.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Alirio-Uribe.mp3"\]\[/audio\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
