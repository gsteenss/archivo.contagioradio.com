Title: Se estrena en Colombia "El silencio de los fusiles "
Date: 2017-07-20 10:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombia, proceso de paz
Slug: silencio-fusiles-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/El-Silencio-de-los-Fusiles-unicas-funciones-20-21-22-y-23-de-julio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El silencio de los fusiles 

###### Jul 20 2017 

El documental con el que por primera vez se dio apertura a la más reciente edición del Festival de Cine de Cartagena FICCI 2017, se estrena en salas de Colombia.  "El silencio de los fusiles" es una producción de la periodista Natalia Orozco quien durante cuatro años siguio detalladamente el proceso de paz entre el gobierno y la guerrilla de las FARC EP .

La producción que se presentará en únicas funciones los días 20 al 23 de Julio en 12 ciudades del país, busca dar una mirada elocuente al proceso de conversaciones y a los hombres y mujeres que hicieron parte del mismo en uno y otro lado de la mesa, pasando del escepticismo y la desconfianza a la firma de los históricos acuerdos. (Le puede interesar: [Daupará muestra de cine y video indígena abres sus convocatorias](https://archivo.contagioradio.com/daupara-cine-indigena/))

<iframe src="https://www.youtube.com/embed/Vdyy4sL19y8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
