Title: Más de 2 mil colombianos fueron extraditados a los EEUU durante el gobierno de Alvaro Uribe
Date: 2015-07-23 12:37
Category: DDHH, Nacional
Tags: alvaro, carcel, carcelario, EEUU, Estados Unidos, extradicion, marcha, movimiento, planton, politicos, presos, prisioneras, prisioneros, uribe
Slug: mas-de-2-mil-colombianos-fueron-extraditados-a-los-eeuu-durante-el-gobierno-de-alvaro-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Extradicion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Miguel Ramos 

<iframe src="http://www.ivoox.com/player_ek_5214177_2_1.html?data=lpeelpabe46ZmKiakp6Jd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIquk9iPqMafk5Day9GPp9Dg0NLPy8bSs9Sfx9rS1NTSb8bs1dfOxs7YpcXj1JDOjdHTt4y5prqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Liria Manrique, Abogada] 

######  

###### [23 jul 2015] 

Prisioneros y prisioneras en Colombia, familiares y organizaciones de abogados que realizan trabajo de acompañamiento a la población carcelaria iniciaron este jueves 23 de julio una campaña contra la extradición. Según afirma el equipo de abogados que les asesora jurídicamente, la falta de regulación en la legislación colombiana frente a la figura de la extradición permite que se irrespete el debido proceso a las personas procesadas, y se viole la soberanía colombiana.

Actualmente los procesos de extradición se tratan en ámbitos administrativos, en los cuales se busca mantener buenas relaciones internacionales, cumpliendo con tratados de cooperación, sin que se evalúen jurídicamente a profundidad los abusos que se presentan en diversos casos.

Los prisioneros y prisioneras colombianos que pagan penas en otros países son expuestos al desarraigo total, al negárseles los vínculos familiares, e imponérseles lenguajes que desconocen y normatividades que no manejar.

El mayor numero de extradiciones en Colombia se presenta hacia los EEUU, con un registro de más de 2 mil colombianos y colombianas enviadas durante el gobierno de Alvaro Uribe Vélez, según afirma la abogada Liria Manrique. Le siguen países como Italia, Francia y México.

Las organizaciones participes de la Campaña Contra la Extradición trabajan de manera conjunta con el representante Victor Correa, en la presentación de un proyecto de ley que legisle la extradición, asegurando garantías al debido proceso para las personas recluidas. Esperan que la Campaña haga la suficiente presión en el Congreso para que se trate el tema en el siguiente periodo legislativo.

\
