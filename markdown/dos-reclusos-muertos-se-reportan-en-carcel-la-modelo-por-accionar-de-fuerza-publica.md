Title: Más de dos reclusos muertos denuncia movimiento carcelario de La Modelo por accionar de Fuerza Pública
Date: 2020-03-21 23:56
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #FuerzaPública, Asesinatos, carcel, ESMAD, INPEC
Slug: dos-reclusos-muertos-se-reportan-en-carcel-la-modelo-por-accionar-de-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-21-at-10.07.40-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras los amotinamientos de la población carcelaria del centro penitenciario La Modelo, los internos denuncian un accionar desmedido y violento por parte de la Fuerza Pública que hasta el momento ha dejado **dos personas asesinadas y decenas de heridos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con los reclusos, los amotinamientos se realizaron como protesta al gobierno del presidente Iván Duque que no ha tomado medidas urgentes entorno a la protección de los prisioneros **frente a la crisis del virus Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los prisioneros afirman que luego de que iniciaran los actos de protesta, el INPEC inició acciones violentas en contra de la población reclusa con armas de fuego, que hasta el momento dejan un saldo de dos personas asesinadas y decenas de heridos**, producto de los impactos de bala.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Defensoría del Pueblo hace presencia en este momento en la cárcel con la intención de establecer un canal de comunicación; sin embargo no se está prestando atención médica a quienes se encuentran en graves condiciones de salud. (Le puede interesar: ["Ninguna Cárcel en Colombia está lista para afrontar Covid 19"](https://archivo.contagioradio.com/ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

El cacerolazo por la vida de la población reclusa
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 21 de marzo, el movimiento carcelario colombiano convocó a un cacerolazo por la defensa de la vida de la población reclusa, tras la falta de medidas concretas que garanticen la salud de las personas que se encuentran en los centros penitenciarios frente al contacio del virus Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la cárcel para mujeres el Buen Pastor, mujeres adelantaron una protesta con cacerolas y pancartas en donde le pidieron al presidente que garantice las condiciones para el aislamiento de las personas, que debe incluir un acuartelamiento de la guardia del INPEC y garantías a los derechos a la salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la cárcel de Cómbita, Boyacá, los internos iniciaron un diálogo con el director del centro penitenciaron con el que esperan que se tomen medidas frente a la entrada y salida del personal administrativo, contratistas y guardias, para reducir las posibilidades de contagio del Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El la cárcel La Picota, en Bogotá, los internos adelantaron un cacerolazo sumándose a los llamados del movimiento carcelario y reclamando el cumplimiento de la sentencia T 153 sobre el estado de cosas inconstitucionales.

<!-- /wp:paragraph -->
