Title: Trabajadores de INCAUCA serían responsables del asesinato de comunero indígena
Date: 2017-03-23 13:12
Category: DDHH, Nacional
Tags: Cauca, CRIC, Hacienda Miraflores, Norte del Cauca
Slug: incauca-responsables-del-asesinato-de-comunero-indigena-del-cric
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/indigenas_buenaventura_contagioradio-e1490306482847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ORIVAC  
] 

###### [23 Mar 2017]

Según la denuncia del Consejo Regional Indígena del Cauca, CRIC, hacia la 1 y 30 de la tarde de este 22 de Marzo, Javier Oteca, comunero indígena de que participa en la Minga de Liberación de la Madre Tierra en la hacienda “Miraflores” en Corinto, Cauca, **habría sido asesinado por trabajadores de seguridad del ingenio azucarero INCAUCA.**

El gobernador Jorge Ticue señaló que aunque no se ha establecido el autor directo de los **disparos que cegaron la vida de Javier Oteca**, la reacción de la guardia indígena pudo realizar la retención de dos de los trabajadores que se encontraban en el sitio de los disparos en ese momento, además la policía habría c**apturado también a dos integrantes de la seguridad de INCAUCA** que habrían disparado contra los comuneros.

Luego se pudo establecer que las comunidades indígenas dejaron en libertad a los trabajadores capturados para que las autridades judiciales desarrollen los trabajos de investigación pertinentes.

Según el gobernador indígena, el proceso de liberación de la Madre tierra se realiza en una porción muy pequeña de la hacienda que cuenta con más de 600 hectáreas, todas las cuales están siendo explotadas por los ingenios mencionados. [Consulte Aquí el comunicado del CRIC](http://www.cric-colombia.org/portal/comunicado-frente-al-asesinato-del-liberador-de-la-madre-tierra-javier-oteca-en-corinto-cauca/)

Además este tipo de enfrentamiento se podrían evitar si el gobierno cumpliera con la asignación de **tierras prometida a los indígenas como parte del proceso de reparación por la masacre del Nilo**.

Por estos hechos las comunidades indígenas exigieron a la Fiscalía General de la Nación, Procuraduría y  la Defensoría del Pueblo que se realicen las investigaciones de manera diligente y se sancione a los responsables para que este tipo de hechos no se repitan, además pidieron la solidaridad de las organizaciones internacionales para que se pronuncien a favor del derecho a la justicia y a la tierra.

### **Sobre el proceso de liberación de la Madre Tierra en Cauca** 

Este proceso de **liberación de la Madre Tierra** se adelanta desde finales del mes de Febrero de 2015 en 3 puntos de la región del **Norte del Cauca** tan solo en las primeras semanas, es decir, para el mes de marzo de ese mismo año, ya se habían producido por lo menos 152 indígenas heridos por la acción del ESMAD. ([Lea tambien: Asi va proceso de liberación de la Madre tierra](https://archivo.contagioradio.com/152-indigenas-heridos-y-silencio-del-gobierno-a-un-mes-de-liberacion-de-la-madre-tierra-en-el-cauca/))

Las tierras reclamadas por los indígenas en los municipios de Corinto, Miranda y Santander de Quilichao, asciende a 6500 hc que el gobierno tiene alquiladas a los ingenios del Cauca, sobre los cuales pesan los intereses del propio ministro de agricultura que no se ha pronunciado sobre la reclamación de los indígenas. ([Lea tambien: Indigenas del Cacua se suman a Liberación de la Madre Tierra](https://archivo.contagioradio.com/mas-de-100-indigenas-heridos-dejan-dos-semanas-de-liberacion-de-la-madre-tierra-en-el-cauca/))

### **La versión de la empresa** 

Según lo que la empresa ha manifestado a RCN la muerte del comunero se dió luego de que los indígenas atacaran a un grupo de trabajadores que estaban desarrollando labores de mantenimiento.

"Momentos después detonó lo que habría sido otra papa bomba, dentro del grupo atacante, y al parecer uno de los integrantes del grupo invasor murió como efecto de la explosión" señala la nota publicada en dicho medio.

Reproducimos el contenido completo del comunicado del CRIC

###### *"Hacia la 1:40 pm del 22 de marzo del presente año, fue asesinado JAVIER OTECA en uno de los puntos de liberación de la Madre Tierra en Corinto, perteneciente al territorio ancestral de Santa Elena. Este hecho ocurrió en la hacienda Miraflores y fue perpetrado por personal que labora en los ingenios azucareros de la misma, a escasos metros donde se encuentra el ejército y la policía nacional.[]{#more-17225}*

###### *En todas las mingas y trabajos comunitarios desde que inició este proceso de liberación de la Madre Tierra, se han presentado agresiones por parte del personal de las haciendas a los liberadores. Como también ataque de la fuerza pública, quienes en muchas ocasiones han agredido a los liberadores, han destruido sus cultivos, sus ranchos, les han disparado indiscriminadamente y ahora los asesinan.*

###### *Javier Oteca fue uno de los comuneros qué más persistió en el proceso de liberación de la Madre Tierra, desde el 2014 cuando se inició el proceso de liberación por faltas de tierras en la parte rural. Y por el incumplimiento del Gobierno Nacional,  quien no ha querido dar solución al problema de tierras. Desde ese tiempo Javier era un hombre que motivaba y se preocupaba por el grupo que lideraba. Pero hoy en la realización del este ejercicio fue asesinado y ahora descansa en el seno de la madre tierra.*

###### ***Por tal razón exigimos: ***

###### *A la Fiscalía General de la Nación, Procuraduría y  Defensoría del Pueblo, para que investiguen este asesinato de manera diligente, determinen quienes son los responsables materiales e intelectuales de este hecho y los sancione enérgicamente, con el fin de que este hecho no quede en la impunidad como han pasado con muchos casos del movimiento indígena.** ***

###### ***A los Organismos Nacionales e Internacionales defensoras de Derechos Humanos: ***

###### *Ante este hecho lamentable las autoridades del Consejo Regional Indígena de Cauca- CRIC hace una llamado a las Defensoría del Pueblo y a Alto Comisionado para los Derechos Humanos, la OEA y demás organismos nacionales e internacionales defensores de derechos humanos, para que se pronuncien sobre estos hechos de violencia que se está presentando con los comuneros indígenas del Norte del Cauca en ejercicio de liberación de la Madre Tierra.*

###### *Es indignante el asesinato de un compañero indígena quien por defender los derechos colectivos del pueblo nasa, tenga que sufrir las consecuencias que enlutan al movimiento indígena. Por tal motivo desde el Consejo Regional Indígena Cauca rechazamos el asesinato del compañero Javier Oteca y manifiestan las más sentidas condolencias para los familiares y su organización."  
*

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
