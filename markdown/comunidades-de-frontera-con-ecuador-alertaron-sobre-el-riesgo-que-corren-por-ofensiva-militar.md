Title: Comunidades de frontera con Ecuador alertaron sobre el riesgo que corren por ofensiva militar
Date: 2018-04-16 16:30
Category: DDHH, Política
Tags: acciones militares, alias guacho, colombia, ecuador, frontera con ecuador, periodistas asesinados
Slug: comunidades-de-frontera-con-ecuador-alertaron-sobre-el-riesgo-que-corren-por-ofensiva-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/alta-mira-y-frontera-e1523908421385.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cero Setenta] 

###### [16 Abr 2018] 

Teniendo en cuenta el asesinato de los trabajadores del diario El Comercio de Ecuador en la frontera con Ecuador a manos de las disidencias de las FARC, el Consejo Comunitario de Alto Mira y Frontera **alertó sobre el alto riesgo en el que se encuentran las comunidades** tras los anuncios de ofensiva militar en zona rural de Tumaco, Nariño.

La alerta la realizaron a los **organismos defensores de derechos humanos**, a las entidades del Estado y a la ciudadanía en general. Afirmaron desde el Consejo Comunitario que el riesgo aumenta para las poblaciones que se encuentran donde las autoridades han ubicado a alias “Guacho” quien habría secuestrado y asesinado a los tres hombres.

### **Juan Manuel Santos confirmó que asesinatos ocurrieron en Colombia** 

En medio del mal manejo de la información de lo ocurrido, el mandatario colombiano confirmó que los cuerpos de **Javier Ortega, Paul Rivas y Efraín Sagarra** se encuentran en territorio colombiano a pesar de que antes había dicho que los homicidios ocurrieron en Ecuador.

Ante esto, señaló a alias “Guacho” como **objetivo militar** “de alto valor” por lo que le solicitó al Ministro de Defensa, Luis Carlos Villegas y a la Canciller María Ángela Holguín que coordinen las acciones militares con sus pares en Ecuador teniendo como objetivo militar a la disidencia militar. (Le puede interesar:"[Los testimonios que comprometen a la Fuerza Pública en masacre de Tumaco](https://archivo.contagioradio.com/los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco/)")

### **Organizaciones de prensa condenaron el asesinato de los periodistas** 

Sumado a lo anterior, organizaciones de prensa de Colombia y de Ecuador condenaron el asesinato de los periodistas e indicaron que hubo un **manejo irresponsable** de la información de los hechos por parte de ambos gobiernos. Por esto, argumentó que los gobiernos tuvieron falta de diligencia para proteger la vida de los reporteros y no hubo acciones para facilitar la liberación de los mismos.

La Fundación para la Libertad de Prensa recordó que la Comisión Interamericana de Derechos Humanos otorgó **medidas cautelares** a favor de los tres periodistas el 12 de abril de 2018. Por esto, los Estados tenían que haber salvaguardado la vida de los periodistas, haber garantizado que ellos desarrollaran su actividad periodística e informar sobre las acciones adoptadas para investigar los hechos, medida que ahora deben garantizar los Estados.

###### Reciba toda la información de Contagio Radio en [[su correo]
