Title: Denuncian a FFMM por violar DDHH en Córdoba, Bolívar y Norte de Santander
Date: 2015-02-18 22:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: bolivar, cordoba, DDHH, ejercito, tibu, violacion, Violaciones a los DDHH en Colombia
Slug: denuncian-a-ffmm-por-violar-ddhh-en-cordoba-bolivar-y-norte-de-santander
Status: published

###### Foto: Tiempo 

Nuevos escándalos envuelven a las Fuerzas Militares por ataques en contra de la población civil en diferentes departamentos del país.

En **Tibú, Norte de Santander**, el joven Elezeimer Niño Contreras recibió un disparo en la cabeza en medio de un reten militar del Batallón de Caballería mecanizada número 5. Según lo denuncia Ascamcat, organización campesina de la región, el joven de 21 años de edad se trasladaba en la parte de atrás de una camioneta cuando recibió el impacto que le causó la muerte instantaneamente. El conductor de la misma, Alberto Villamizar, resultó herido como consecuencia de los disparos propinados por miembros del Batallón.

En **Montecristo, departamento de Bolívar**, un soldado del Batallón Nariño agredió verbal y físicamente al joven Jiovanny Garrido, asegurando que "le importaba un culo embalarse con él y pagarlo". Posteriormente, cuando Jiovanny Garrido intentó interponer una denuncia por lesiones personales, el Sargento Mosquera, responsable del Batallón, intentó empadronar al campesino.

Por su parte, la **Asociación de Campesinos del Sur de Córdoba** denunció que soldados de la brigada móvil número 16, pertenecientes a la Séptima División del Ejercito, están ejecutando planes para agredir física y moralmente a las campesinas y campesinos de la región, estableciendo toques de queda ilegales, atacando psicológicamente a las personas al apuntarles con sus fusiles e infringirles terror, maltratándolas verbalmente y violando el derecho al trato digno de mayores y personas en condición de discapacidad, e infringiendo agresiones físicas -como golpes y patadas- en contra de las personas más jóvenes.

Las organizaciones denuncian que el Ejercito colombiano abusa de su poder infringiendo terror en las diferentes regiones del país, y exigen que las **Fuerzas Militares** y de Policía dejen de atentar contra la población colombiana. Si la paz pasa por los territorios, como anunció el Presidente Juan Manuel Santos, las medidas para que cese la violación de Derechos Humanos por parte del Estado debería verse reflejado en estos escenarios.
