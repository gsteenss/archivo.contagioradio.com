Title: Cierre del mes de Agosto fue fatídico para líderes sociales en Colombia
Date: 2020-09-01 12:05
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Bolívar, Cauca
Slug: cierre-del-mes-de-agosto-fue-fatidico-para-lideres-sociales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diseno-sin-titulo-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Líderes sociales CNA / @LuisGPerezCasas -

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia que se ha agudizado en las más recientes semanas en medio del accionar paramilitar, la omisión del Estado y una disputa territorial entre diferentes grupos armados una vez más llegó hasta las zonas rurales del país donde fueron asesinados cuatro líderes sociales de los departamentos de Meta, Cauca y Bolívar en menos de 24 horas. **Según el Instituto de Estudios para el Desarrollo y la Paz (Indepaz) el registro des líderes y defensores de DD.HH. asesinados en 2020 asciende a 203 personas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el corregimiento de Sinaí en Argelia al sur de Cauca, hombres armados, que se movilizaban en un vehículo, asesinaron a **Jhon Montero, campesino que fungía como vicepresidente de la Asociación de Padres de Familia de la Institución Educativa de Sinaí.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De acuerdo con la información entregada desde esa población, los hechos ocurrieron el pasado 29 de agosto cerca de las 4:00 pm cuando Jhon Montero transitaba por la vereda Desiderio Zapata en la vía que conduce del corregimiento El Mango cuando fue abordado por sujetos arados que se movilizaban en un automóvil.. [(Lea también: En menos de 48 horas se registran 8 asesinatos a líderes sociales)](https://archivo.contagioradio.com/en-menos-de-24-horas-se-registran-ocho-asesinatos-a-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ataques contra líderes continúan al Sur de Bolívar

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma el mismo día, se conoció el asesinato de **Fernando "Panadero" Gaviria Garcia, integrante también del Coordinador Nacional Agrario y de Omaira Alcaraz 'Cachi', fiscal de la Junta de Acción Cumunal (JAC) de la vereda Alto San Juan** en el municipio de San Pablo al sur de Bolívar donde según Indepaz, la comunidad denunció en múltiples ocasiones el incursionar de grupos paramilitares. [(Le recomendamos leer: Asesinan a Edwin Acosta, líder social y minero de Tiquisio, Bolívar)](https://archivo.contagioradio.com/asesinan-a-edwin-acosta-lider-social-y-minero-de-tiquisio-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según [Congreso de lo Pueblos](https://twitter.com/C_Pueblos/status/1300094356553109506/photo/1), en horas de la noche llegaron dos hombres encapuchados hasta la vivienda de Omaira, la amenazaron a ella y a su familia, se desplazaron posteriormente a otro sector de la vereda donde se encontraba Fernando quien desempeñaba sus funciones como tendero de una tienda; al ingresar al establecimiento le ordenaron desplazarse hasta la casa de Omaira, Fernando se resistió y le dispararon en siete ocasiones. Después de lo sucedido se dirigieron a la casa de Omaira donde fue asesinada la lideresa. [(Lea también: Caravana denuncia nueva crisis humanitaria en el sur de Bolívar)](https://archivo.contagioradio.com/caravana-denuncia-nueva-crisis-humanitaria-en-el-sur-de-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Meta también ve el resurgir de la violencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, en el departamento de Meta se registró el asesinato de **Julio Cesar Sogamoso, integrante de lo Asociación Campesina de pequeños y medianos productores del Río Cafre -AGROCAFRE)**. Según organizaciones sociales del municipio de Puerto Rico, hombres armados habrían atacado a Julio Cesar quien se desempeñaba como presidente de la Junta de Acción Comunal de la vereda Barranco Colorado, municipio de Puerto Rico.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
