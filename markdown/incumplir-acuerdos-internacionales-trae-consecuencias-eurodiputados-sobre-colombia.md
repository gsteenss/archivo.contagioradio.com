Title: "Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia
Date: 2020-06-10 18:36
Author: CtgAdm
Category: Actualidad, Otra Mirada, Programas
Tags: acuerdos internacionales, colombia, Derechos Humanos
Slug: incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Junio_2020-Carta-DDHH-Covid-19-Colombia-ESP.docx-1.pdf" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Junio_2020-Carta-DDHH-Covid-19-Colombia-ESP.docx-1-1.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Laprensa.hn*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia en medio de la pademia han crecido las preocupaciones en torno al tema de política exterior, esto debido a acciones que permiten pensar que **los [organismos internacionales](https://archivo.contagioradio.com/onu-declara-que-colombia-violo-el-derecho-a-la-vida-del-sindicalista-adolfo-munera/) para este Gobierno no cobran la importancia que deberían tener**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ejemplo de ellos son acciones como negar la entrada al relator de la ONU, l**a postura frente a [Cuba](https://www.justiciaypazcolombia.com/la-labor-de-cuba-ha-sido-para-la-paz-no-para-la-guerra/) en su papel de garante en el proceso de paz entre FARC y ELN** , así como lo que ocurre con las relaciones con el Gobierno de Venezuela. (Le puede interesar: [Al Gobierno no le gusta que se hable de impunidad: Plataformas de DD.HH](https://archivo.contagioradio.com/al-gobierno-no-le-gusta-que-se-hable-de-impunidad-plataformas-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Idoia Villanueva, eurodiputada integrante del partido político español Podemos**, señaló que Colombia vive un momento internacional convulso ***"en donde hay diferentes líderes y gobiernos que intentan aprovechar el estado de crisis para retroceder en términos de Derechos Humanos"***, algo que para la parlamentaria no solo ocurre en Colombia sino en varios países.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Podemos, como grupo político ha hecho presencia de manera latente desde el 2016 en Colombia, extendiendo su apoyo a la negociación del proceso de paz, y el recorrido que ha hecho el país en caminado a la paz; **por eso hoy sus integrantes ven con preocupación los hechos de violencia y vulneración a los Derechos Humanos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actos que según la diputada van de la mano con las políticas de guerra que hoy desarrolla Estados Unidos las cuales "**dan prioridad a la economía antes que a las mismas comunidades y a los derechos en su propio país**". (Le puede interesar: [Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este destacó que no es posible pensar Gobiernos que trabajen solos, *"en un mundo globalizado donde las relaciones internacionales cada vez son mas cercanas, y en donde el ojo de **la comunidad internacional no va dejar dar un paso atrás en términos de la democracia la justicia social en derechos y libertades**"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"Este es un conflicto que no puede ganar ni el Gobierno ni las guerrillas, porqué que al final quien pierde al incumplir los acuerdos internacionales** **son las** ***comunidades"***

<!-- /wp:quote -->

<!-- wp:paragraph -->

Allí pone como ejemplo la situación de la mesa de diálogo con ELN , considerando que es un paso fundamental en camino a la construcción de una paz completa en el país, y en donde el Gobierno colombiano no puede desconocer lo firmado en el Acuerdo de La Habana y acuerdos internacionales; ***"la única solución ahora es el diálogo sin importar cueste lo que cueste para el Gobierno, porqué la comunidad merece el anuncio del cese al fuego bilateral".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cómo actuar ante un Gobierno que parece dejar de lado los acuerdos internacionales?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para la parlamentaria este tipo de comportamientos deben reforzar la incidencia de la comunidad internacional, *"**implicarnos en países que están sufriendo retrocesos en manera de derechos y libertades como Colombia es prioridad y lo hemos dejado claro en una carta** enviada hace algunos días al Presidente".* ([Lea carta completa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Junio_2020-Carta-DDHH-Covid-19-Colombia-ESP.docx-1-1.pdf))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una carta que presenta una alerta del Parlamento Europeo hacia el Gobierno y los reiterados incumplimientos al Acuerdo de Paz, *"los gobiernos pueden decir que no les importa lo que dicen las comunidades internacionales, pero l**a falta a los compromisos por la paz, trae siempre consecuencias"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y ante las consecuencias que pueden ocurrir la diputada señaló, "**los acuerdos están para cumplirlos**", y destacó que la Unión Europea ha destinado un fondo para la paz con más de 120 millones de euros para la ejecución de proyectos en torno a la reincorporación y el desarrollo rural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto fue de gran alarma para el Parlamento saber que el Gobierno de Duque pretendía **reubicar el presupuesto de la paz para la lucha contra el Covid**, ***"las medidas de protección contra la pandemia se deben llevar a cabo con el respeto de los derechos humanos y la construcción de paz"***, destacando que esto es algo que el Gobierno esta dejando de lado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Que consecuencias pueden existir ?

<!-- /wp:heading -->

<!-- wp:paragraph -->

"*Somos conscientes que este Gobierno no estaba a favor, ni de lado del Acuerdo de Paz, ni mucho menos de los pactos internacionales*", destacó **Villanueva**, añadiendo las consecuencias que este tipo de posturas pueden traer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de ellas son las sanciones por parte de los organismos internacionales ante los incumplimientos, *"****las decisiones políticas siempre afecten a la población civil y es algo que hay que tener siempre en cuenta*, *pero las ordenes del Gobierno también al final terminar siendo condenadas*** *dentro y fuera del país".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto último haciendo alusión a las más de 130 intervenciones y [perfilamientos](https://archivo.contagioradio.com/prensa-bajo-la-mira/)por parte del Gobierno a organizaciones defensoras de Derechos Humanos, periodistas, comunidad internacional, opositores jueces, y defensores, **acción clara según la diputada al incumplimiento de los acuerdos internacionales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalmente hizo un llamado al Gobierno nacional para que confirme *"cuáles son sus planes para cumplir y avanzar en los acuerdos"*, y agregó que ante la carta enviada hace algunos días, no han recibido ningún tipo de respuestas, *"estamos muy pendientes a una respuesta y estamos haciendo seguimiento continuo a los hechos que vulneren los derechos humanos en Colombia, **queremos que sepan que no están solos y que tienen nuestro respaldo".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(*Le puede interesar diálogo completo sobre "Espionaje del Estado en tiempos de pandemia"*)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F1726277247496424%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" style="border:none;overflow:hidden" scrolling="no" allowtransparency="true" allow="encrypted-media" allowfullscreen="true" width="734" height="413" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
