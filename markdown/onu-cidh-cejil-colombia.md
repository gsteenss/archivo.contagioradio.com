Title: ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL
Date: 2020-06-16 21:41
Author: CtgAdm
Category: Actualidad, DDHH
Tags: CEJIL, colombia
Slug: onu-cidh-cejil-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/ConsejoDDHH-ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: ONU

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Viviana Krsticevic Directora Ejecutiva del [Centro por la Justicia y el Derecho Internacional (CEJIL)](https://www.cejil.org/es), organización enfocada en la defensa de los derechos de poblaciones vulnerables ante omisiones o vulneraciones por parte de los estados en América Latina, habló en entrevista para Contagio Radio señalando la importancia de analizar desde dónde se originan las situaciones vulneratorias de derechos en Colombia y países de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Krsticevic afirmó que **el autoritarismo con el que se opera desde algunos gobiernos lleva a que cualquier individuo que cuestione el actuar institucional o defienda los derechos de poblaciones vulnerables o movimientos sociales, sea visto como «enemigo del Estado y de las Fuerzas de Seguridad».**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto lo relacionó con los perfilamientos ilegales que se realizaron desde el Ejército Nacional: «Si se mira quién estuvo perfilado en las acciones de espionaje militar en Colombia, es un amplio rango de personas de distintos sectores ideológicos y profesionales, que están unidas en parte, por un trabajo de cuestionamiento frente al poder y la acción institucional. Eso las pone del lado de los peligrosos». ([Le puede interesar: Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.)](https://archivo.contagioradio.com/espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El rol de los organismos internacionales es vital para proteger los DDHH en tiempos de Pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Krsticevic afirmó que el Sistema Interamericano de Derechos Humanos y la Organización de Naciones Unidas (ONU) han sido enfáticos sobre los límites al poder estatal, la defensa de derechos y la libertad de expresión; y adicionalmente, que existe una articulación normativa e institucional entre los mecanismos de protección de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También señaló que **se está a la espera de algunos pronunciamientos que se avecinan por parte de estos organismos, los cuales pueden servir de guía para la implementación de nuevas políticas, prácticas y en general para la toma de decisiones judiciales que amparen los derechos de la ciudadanía.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe recordar que casos como los de los senadores Gustavo Petro (por su destitución como Alcalde de Bogotá por parte de la Procuraduría) e Iván Cepeda & Roy Barreras (denunciantes por la supuesta destinación de recursos entregados por EE.UU. para el espionaje de políticos, periodistas, magistrados, etc.) fueron sometidos a la jurisdicción del Sistema Interamericano y se encuentran a la espera de una decisión definitoria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Redes y acciones ciudadanas son cruciales en el momento actual afirma CEJIL

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Directora Ejecutiva del CEJIL se mostró optimista frente a la situación actual de lucha por los DD.HH., manifestó que el surgimiento de las nuevas tecnologías y en particular de las redes sociales, ha permitido que se visibilicen cosas que «antes quedaban ocultas por las prioridades de la prensa». Igualmente, afirmó que esa visibilización ha propiciado que se generen cambios en los juicios de apreciación y la consideración de lo que es «injusto» y «antidemocrático».  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Krsticevic señaló que desde muchas organizaciones y sectores ciudadanos se está tratando de darle dimensión a la problemática de vulneración de DD.HH. y a no minimizar ninguna práctica intromisoria por inofensiva que parezca, pues manifestó que estas prácticas se constituyen en el «preludio» de un gran entramado de corrupción y silenciamiento por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
