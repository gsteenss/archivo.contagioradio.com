Title: Una exposición busca dignificar la memoria de los líderes de la UP
Date: 2017-08-09 09:35
Category: eventos
Tags: Documental, Festival de cine ddhh, memoria, UP
Slug: memoria-up-exposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo familia Santamaría 

###### 09 Ago 2017 

En el marco del 4to Festival Internacional de Cine por los Derechos Humanos Bogotá que inicia el próximo 11 de agosto, se inaugura en Bogotá la exposición **"Álbumes de memoria y narraciones visuales. Galería Unión Patriótica"** una obra de la artista Luisa Santamaría hija de Gabriel Jaime Santamaría, fundador y presidente de ese movimiento político.

La exposición y documental, tiene como objetivo **dignificar, reconstruir y valorar el papel que tuvieron para la democracia los líderes y militantes de la Unión Patriótica (UP)** y exaltar la resistencia contra el olvido por parte de los familiares de las víctimas del genocidio político. (Le puede interesar:[La apuesta de la Unión patriótica para la unidad de cara a las elecciones 2018](https://archivo.contagioradio.com/sectores-del-movimiento-social-se-uniran-para-elecciones-2018-aida-abella/))

“Este es un trabajo de memoria para reconocer a **Gabriel Jaime Santamaría y demás dirigentes asesinados,** quienes resaltaron y lucharon por la igualdad de los derechos, la solidaridad y el apoyo al campesinado. Con esta construcción colectiva de álbumes **rindo un tributo a la palabra que me enseñaron mis padres, la unión**", afirma la artista Luisa Santamaría.

Adicionalmente, durante la inauguración se realizará el lanzamiento del libro **"Las ideas socialistas en Colombia" de Jorge Eliécer Gaitán,** presentado por **Gloria Gaitán y Pablo Catatumbo**. El evento tendrá lugar el jueves **10 de agosto a las 6:30 de la tarde** en el Hall central de la Universidad Jorge Tadeo Lozano Cra. 4. n.º 22-61 con entrada libre. La exposición estará abierta al público **hasta el 31 de agosto**.
