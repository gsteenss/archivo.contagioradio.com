Title: Gobierno también es responsable del regreso a las armas de Iván Márquez
Date: 2019-08-29 15:24
Author: CtgAdm
Category: Nacional, Paz
Tags: FARC, Iván Márquez, Jesús Santrich
Slug: gobierno-tambien-es-responsable-del-regreso-a-las-armas-de-ivan-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Iván-Márquez.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube/Jacobo Alape] 

Al conocerse el anuncio hecho por el ex jefe negociador de La Habana, Iván Márquez, de regresar a las armas junto a otros líderes de la organización, son muchas los interrogantes que surgen, entre ellos las consecuencias que su accionar tendría sobre la implementación del acuerdo,  los territorios y en la misma organización política del partido FARC.

Para **el director de Indepaz, Camilo González Posso**, se trata de una situación lamentable y una estrategia equivocada por parte de Iván Márquez y su grupo, "hacer política de armas en medio de una lucha por la implementación no tiene mucho futuro", evalúa el experto argumentando que se está pavimentando el camino de su propio fracaso al generar pertubaciones al verdadero propósito transformador del Acuerdo de Paz. [(También puede leer: "El Acuerdo es mucho más grande que unas personas que se apartan")](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/)

Para Posso afirmaciones de Márquez, como "el fracaso de la paz" son una lectura errada de la coyuntura nacional al no reconocer que hay una transformación de la política en Colombia, destacando que en medio del lento proceder de la implementación, **"el acuerdo ha tenido medidas institucionales importantes y ha modificado la situación de guerra"** y además  ha abierto espacios para la movilización y  el derecho a la protesta. [(Lea también: Información entregada a Unidad de Búsqueda por FARC esclarecería paradero de 276 personas)](https://archivo.contagioradio.com/informacion-entregada-a-unidad-de-busqueda-por-farc-esclareceria-paradero-de-276-personas/)

### "Por donde se le tome, Márquez están cayendo en su propia trampa" 

El anuncio de que no van a secuestrar es lo mínimo que se podía esperar de esta declaración, lo aislaría de la forma más visible si recurren a estos procedimientos pero dicen que se van a financiar de las rentas que le van a cobrar al narcotráfico y a actividades ilegales, eso de nuevo es un pacto con el diablo "yo le cuido su ruta, yo le cuido su cultivo y de eso me financio para la guerra, es una trampa mortal para ellos mismos y para el país"

González Posso indica que al depender de dinero del narcotráfico, Márquez y su círculo cercano requerirán hacer alianzas que los terminarán por  sumir en una "trampa" de la que el experto considera, lograron salir para poder hacer política y a la que hora están volviendo a caer, **"ese hueco del narcotráfico es la imposiblidad de acción rebelde, es quedar atrapados en los círculos del negocio de la cocaína"**, sentenció.

### Responsabilidad del Gobierno 

Aunque  en su comunicado Márquez señala que operarán de forma diferente evitando atacar a "policías y soldados respetuosos de los intereses generales", Posso advierte que esta sería una falacia pues será inminente que se presenten confrontaciones con el Ejército, que tras este anuncio probablemente recibirá nuevas ordenes para afrontar esta situación. ([Le puede interesar: ¿Se agota la esperanza de paz de Iván Marquez y 'El Paisa'?)](https://archivo.contagioradio.com/esperanza-paz-de-ivan-marquez/)

A propósito del Gobierno, el experto señala que este "tiene su responsabilidad y por supuesto este es el ambiente propicio para que determinadas estrategias de retorno a la guerra se impongan", sin embargo agrega que pese a que desde el momento inicial de los acuerdos comenzaron a verse las dificultades "no se puede establecer una relación de causalidad inmediata"

Posso concluye que el país está ante **"un juego peligroso"**, porque más allá del discurso compartido, se está mandando un mensaje a los excombatientes, a los jóvenes y a la sociedad en general de forma explícita que busca regresar a la guerra.   [(Lea también: La desaparición de Santrich no puede ser un impedimento para buscar la paz)](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/)

**Síguenos en Facebook:**  
<iframe id="audio_40609708" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40609708_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
