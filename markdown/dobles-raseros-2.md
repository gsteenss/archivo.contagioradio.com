Title: Dobles raseros
Date: 2017-05-22 13:53
Category: Javier Ruiz, Opinion
Tags: Medios de información, senadora Viviane Morales, Venezuela
Slug: dobles-raseros-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/dobla-moral-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Medios 

#### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 22 May 2017 

Colombia es un país donde el doble rasero, el doble discurso o la doble moral son un mal endémico que ha perjudicado la opinión, la percepción y la información de lo que está aconteciendo.

Al parecer nuestros políticos y nuestros líderes recurren al doble rasero sin darles un poco de vergüenza, creyendo que eso está bien y que no van a ser reprochados por eso. Se equivocan al creer que toda la gente no se da cuenta de su doble rasero al tratar determinados temas o de realizar incomodas comparaciones para lograr sus cometidos. Lo malo de esto es que ha tenido una fuerte incidencia en la opinión pública fabricando consensos, la gente es manipulada y se dejan convencer de esos dobles raseros creyendo que así son bien pensantes, más democráticos, más indignados o más defensores de la democracia.

El doble rasero les parece perfecto para dar lecciones cuando acá no aplican esas mismas lecciones que se pretender dar. Es como se dice popularmente “se predica pero no se aplica” y muchos han caído en ese error tan garrafal y cuando son confrontados responden de mala manera o responden con insultos porque su doble rasero ha quedado al descubierto y sus argumentos quedarán inválidos.

En estos últimos meses hemos sido testigos de dobles raseros de distinta índole: El doble rasero de Viviane Morales y su esposo Carlos Alonso Lucio que por imponer su creencia cristiana hicieron hasta lo imposible para que fuera aprobado un referendo para que parejas del mismo sexo y personas solteras no pudieran adoptar niños porque supuestamente los niños estaban en grave desprotección de sus derechos. Morales y Lucio, por el contrario, callan cuando los niños sufren vejámenes por personas o familias heterosexuales “la familia ideal” según estos políticos religiosos. Nunca vimos a Viviane Morales o a quienes están obsesionados por la farsa de la “ideología de género” defender a estos niños o al menos una palabra de apoyo frente a esto. Los niños son un cálculo electoral y se nota el doble rasero.

Ni que decir cuando nos acordamos de Venezuela, el demonio predilecto de la ultraderecha y de las personas que posan de progresistas, cuando pretenden dar lecciones de democracia al país vecino porque supuestamente no hay democracia, no hay libertad, no hay comida o no hay medicinas pero callan cuando en Colombia no hay democracia o no hay libertades.

Criticamos al país vecino por una supuesta dictadura pero acá el Estado colombiano y los colombianos realizan prácticas dictatoriales como lo hizo el empresario musical y manager Julio Correal censurando al cantante de metal venezolano Paul Gillman por su simpatía al chavismo y a Nicolas Maduro siendo retirado de Rock al Parque que es un festival donde muchos cantantes o grupos de rock han expresado con libertad sus posiciones políticas y esto sirvió para mostrar la verdadera cara de este dizque gestor cultural, su cara intolerante y dictatorial.

Nos quejamos de la supuesta represión de la policía venezolana pero callan y hasta ven con buenos ojos la represión del ESMAD a la gente pobre y débil del país. Ven con buenos ojos la represión a los vendedores ambulantes y músicos que se rebuscan la vida en las calles, ven con buenos ojos la represión hacía estudiantes universitarios que protestan por sus derechos siendo acusados de vándalos y terroristas.

Ven con buenos ojos que sean reprimidos las personas de Buenaventura y de Quibdó que están en paro por el abandono estatal y por el desgobierno. De ahí el doble rasero de los medios de comunicación que nos quieren imponer su agenda desinformativa hablando más de los males de Venezuela que de los males propios porque le dedican más tiempo a lo que acontece allá que a las crisis internas.

En Venezuela si hay autoritarismo, si hay crisis de salud, si hay abuso de autoridad, si hay crisis de alimentos y acá en Colombia estos males son más notorios y peores pero eso no da “rating” y tienen que dar la apariencia de que acá no pasa nada reforzando el mito de que Colombia es “la democracia más solida del continente”. Y los periodistas que están en Buenaventura o en Quibdo no se ponen máscaras de gas y chaleco antibalas como si lo hacen en Caracas.

A partir de ese mito de que somos “la democracia más solida del continente” los políticos, sobre todo los de ultraderecha, desvían la atención diciendo que “no debemos ser como Venezuela” mientras aplauden y justifican la precaria democracia colombiana como se pudo ver en los intentos de sabotear la revocatoria contra el alcalde Enrique Peñalosa por su desgobierno e ineptitud diciendo que “meterle mucho pueblo a estos procesos no es democrático” o que esto generaba “crisis de gobernabilidad”. Excusas para evitar que la democracia colombiana se desarrolle para defender intereses particulares.

Ya que se mencionó a Peñalosa, que dirán los opinadores que son antiuribistas y apoyan a Peñalosa de la reunión que tuvo con Uribe en el Palacio de Lievano. En especial Daniel Samper Ospina quien apoya a Peñalosa y es atacado por Uribe, quien apoya a Peñalosa, acusándolo de “bandidito”. Creo que eso no va a tapar su incoherencia y su doble rasero al “humorista político”. Ni que decir el silencio atronador de la derecha por la corrupción del golpista Temer en Brasil o el doble Rasero del mediocre director general de la OEA Luis Almagro que solo ve a Venezuela pero calla sobre lo que acontece en Brasil, en México o en la misma Colombia.

Y muchos de ustedes que posan de progresistas caen en la trampa de los dobles raseros y de pretender dar lecciones cuando acá estamos peor que en países vecinos. No miremos la paja en el ojo ajeno y dejemos el doble rasero a ver si mejoramos un poco como país.

#### [**Leer más columnas de Javier Ruiz**](https://archivo.contagioradio.com/javier-ruiz/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
