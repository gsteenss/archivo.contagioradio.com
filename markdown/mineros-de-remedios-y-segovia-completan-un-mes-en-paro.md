Title: Mineros de Remedios y Segovia completan un mes en paro
Date: 2017-08-15 14:44
Category: Movilización, Nacional
Tags: Mineros Remedios y Segovia, Movilización
Slug: mineros-de-remedios-y-segovia-completan-un-mes-en-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Remedios.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [15 Ago 2017] 

Los mineros de Remedios y Segovia completan un mes en paro, sin que se establezca una mesa de interlocución con el gobierno y la empresa Grand Colombia Gold, para legalizar el trabajo que durante décadas se ha desarrollado como minería artesanal. **Hasta el momento hay reporte de 15 personas heridas y 3 personas detenidas**.

El día de ayer habitantes de la comunidad denunciaron que el ESMAD arremetió contra manifestantes, mientras el resto de los pobladores se encontraban en una iglesia cercana, sin tener en cuenta las afectaciones que esto provocaría, de acuerdo con Jaime Gallego, este hecho**, enfado a las personas y produjo enfrentamientos entre la Fuerza Pública y los manifestantes hasta las 10 de la noche**.

De igual forma, expresó que uno de los barrios tuvo que ser desalojado luego de que uniformados del ESMAD se ubicaran en este lugar para lanzar gases lacrimógenos. Otra de las denuncias que hace la comunidad es el aumento en los precios de los alimentos, debido a que tanto el comercio como los conductores se han unido al paro, **situación que ha sido aprovechada por otras personas para traer productos y venderlos más caro.**

Gallego, agregó que hasta el momento la Defensoría del Pueblo solo ha hecho presencia en tres ocasiones y que ni las alcaldías **locales ni la gobernación de Antioquia, se han pronunciado** frente a estos hechos y las denuncias que se han realizado sobre violación de derechos humanos. (Le puede interesar:["Continúa paro minero en Remedios y Segovia pese a la presión"](https://archivo.contagioradio.com/continua-paro-de-mineros-en-remedios-y-segovia-pese-a-la-represion/))

Hoy una delegación de mineros hará una presentación en la Cámara de Representantes en donde espera que diferentes ministerios atiendan el llamado que hacen para lograr un acuerdo en Remedios y Segovia.  Sin embargo, la comunidad ha manifestado **que continuarán con la movilización pacífica hasta que no sean atendidas sus demandas**. (Le puede interesar: ["Segovia y Remedios siguen esperando respuestas a crisis laboral y social"](https://archivo.contagioradio.com/segovia-y-remedios-siguen-esperando-respuestas-a-crisis-laboral-y-social/))

**Grand Colombia Gold**, es una de las multinacionales que más extrae oro en Colombia, en el año 2016 extrajeron cerca de 115 mil onzas de oro, igualmente es la empresa que demandó al estado colombiano por no desalojar a los mineros de Marmato, la cifra de la demanda es de **US\$700 millones de dólares**.

<iframe id="audio_20348140" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20348140_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
