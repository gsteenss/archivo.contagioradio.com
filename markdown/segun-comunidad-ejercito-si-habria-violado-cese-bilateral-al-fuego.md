Title: Según comunidad, Ejército sí habría violado cese bilateral al fuego
Date: 2016-11-18 13:07
Category: Nacional, Paz
Tags: Asesinados, Asesinatos, Cese al fuego, FARC, Gobierno, guerrilla, Guerrilleros, paz
Slug: segun-comunidad-ejercito-si-habria-violado-cese-bilateral-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cdn1.uvnimg.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univision] 

###### [18 Nov. 2016] 

Luego de los hechos ocurridos el pasado 16 de noviembre, que dejaron como resultado el **asesinato de dos guerrilleros de las FARC-EP y un tercer capturado, por parte de miembros de la V Brigada del Ejército Nacional que operan al sur de Bolívar,** se conformó una comisión de verificación alterna para recolectar información de los hechos que han estado aconteciendo en esta zona.

Dicha comisión estuvo compuesta por miembros de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó (AHERAMIGUA), en conjunto con las comunidades de la vereda El Golfo, del municipio de Santa Rosa del Sur de Bolívar.

En el informe, la comisión presenta una serie de antecedentes que han estado sucediendo en este municipio y que han sido denunciados respectivamente. Por los cuales, no ha habido ningún tipo de investigación ni sanción a las personas implicadas. Pero también da cuenta de lo acontecido con el asesinato de los dos guerrilleros de las Farc.

**¿Qué sucedió con Joaco y Mónica, los guerrilleros de las Farc?**

El informe asegura que **“aproximadamente a la 1:30 p.m.  “Joaco” se encontraba hablando por celular y de manera inesperada cayó al piso, el disparo que recibió fue levemente percibido, en ese momento “Mónica” se inclina a ver que le sucedió y  también recibe un disparo quedando en el suelo”.**

Así mismo, se manifiesta que las personas allí presentes se dan cuenta que **los disparos fueron efectuados por francotiradores** “Los miembros del Ejército estaban aproximadamente entre 30 y 40 metros de donde se encontraban los guerrilleros, y tras los disparos lanzan dos ráfagas de disparos al aire. **El tercer insurgente va llegando y es capturado por los del Ejército, quienes le ordenan tirarse boca abajo, con las manos sobre la nuca”.**

Según data el informe, **los pobladores, luego del atentado contra la humanidad de los dos guerrilleros, fueron víctimas de insultos y malos tratos por militares** que ingresan a las casa que están en la zona donde fallecieron los dos integrantes de las Farc.

Dentro de los reiterados insultos que reciben los pobladores, el Ejército les dice que son colaboradores y cómplices de la guerrilla.

Cerca de las 12:30 p.m., según cuentan los pobladores entrevistados, se llevó a cabo el procedimiento de levantamiento de los cadáveres.

Los habitantes también señalan que algunos soldados habían sido vistos días atrás, vestidos de civil,  comprando cosas en el lugar de los hechos  y viendo televisión en la casa a la que ingresaron el día de los hechos.

Finalmente, en el informe, la comisión integrada por miembros de (AHERAMIGUA) presentan una serie de conclusiones y recomendaciones a la misión tripartita, que espera sirvan como insumo para el trabajo que esta ya viene adelantando.

[Informe Preliminar Comisión Alterna compuesta por miembros de la Asociación de Hermandades Agroecológicas y...](https://www.scribd.com/document/331566114/Informe-Preliminar-Comision-Alterna-compuesta-por-miembros-de-la-Asociacion-de-Hermandades-Agroecologicas-y-Mineras-de-Guamoco-AHERAMIGUA#from_embed "View Informe Preliminar Comisión Alterna compuesta por miembros de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó (AHERAMIGUA) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_24369" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331566114/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LIk9Tik5Pujo0xjKLLkv&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
