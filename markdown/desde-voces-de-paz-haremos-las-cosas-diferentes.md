Title: Desde “Voces de Paz” haremos las cosas diferentes: Judith Maldonado
Date: 2016-12-15 09:46
Category: Entrevistas, Paz
Tags: Cámara, Congreso, FARC, Movimiento ciudadano, voces de paz
Slug: desde-voces-de-paz-haremos-las-cosas-diferentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Judith-Maldonado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 15 Dic. 2016 

Este jueves 15 de Diciembre, se realizó la inscripción de “Voces de Paz”, movimiento que estará a cargo de la veeduría en la implementación de los acuerdos de paz tanto en Senado como en Cámara. Los representantes de este movimiento tendrán voz pero no voto en el Congreso y será el preámbulo para la creación de un partido político.

Judith Maldonado, Abogada defensora de Derechos Humanos y Ex-candidata popular a la Gobernación de Norte de Santander será una de las integrantes de dicho grupo manifiesta que **“el fin de dicho movimiento es la defensa del Acuerdo de Paz, lo requerimos con urgencia".**

**Así como ella, serán Jairo Rivera, Pablo Cruz, Imelda Daza, Francisco Tolosa y Jairo Estrada quienes trabajen juntos en pro de dicha veeduría** pero además, según Maldonado “nos han asignado una gran tarea sin manual de instrucciones” y agregó “todos sabíamos que teníamos que formar una agrupación de ciudadanos y ciudadanas y al revisar las organizaciones sociales se realizó la selección de los integrantes”.

Cabe destacar que estas personas no se convierten en voceros de las FARC, sino de la paz, por ello su papel estará enfocado en vigilar la normativización y defender el espíritu de los acuerdos **“el rol que nos han encargado es ser los voceros y defensores de los acuerdos de paz, de los trámites legislativos que va a hacer el Congreso de cara a la implementación” puntualizó Maldonado.**

A futuro este movimiento pretende continuar fortaleciéndose “yo espero que haya una inmensa mayoría que a partir de hoy pueda decir yo integro la agrupación política “Voces de Paz” teniendo en cuenta que **nuestro fin es diferente a cualquier otro partido político (…) porque lo que queremos es defender la paz y facilitar camino para otros procesos de paz” concluyó Maldonado. **Le puede interesar: [“Voces de paz” será el movimiento ciudadano con voz en el Congreso](https://archivo.contagioradio.com/voces-de-paz-sera-el-movimiento-ciudadano-con-voz-y-voto-en-congreso/)

<iframe id="audio_15036083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15036083_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
