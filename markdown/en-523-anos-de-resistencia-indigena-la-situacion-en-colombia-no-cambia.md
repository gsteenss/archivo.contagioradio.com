Title: En 523 años de resistencia indígena la situación en Colombia sigue igual
Date: 2015-10-14 10:46
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Desalojos por parte del Cerrejón, El Cerrejón en La Guajira, Extracción de carbón en La Guajira, Luis Fernando Arias, ONIC, Palma aceitera en Guaviare, resistencia indígena, Violación de derechos a pueblos indígenas en Colombia
Slug: en-523-anos-de-resistencia-indigena-la-situacion-en-colombia-no-cambia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Guardía-Indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [onic] 

<iframe src="http://www.ivoox.com/player_ek_8980678_2_1.html?data=mZ6lkpubfI6ZmKiakp6Jd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BflpegjcaJh5SZo5bc1ZDIqYzmxtjW1dnJssTdwpDW0MmJh5SZoqnUx9PFb83VjNjW1trFp8qZpJiSpJjSb8bijJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Fernando Arias, ONIC] 

###### [13 Oct 2015]

[El pasado 12 de octubre se conmemoraron **523 años de resistencia ante el exterminio y la imposición cultural** de la que han venido siendo víctimas los pueblos indígenas del continente americano, desde la invasión territorial por parte de españoles, portugueses e ingleses a finales del siglo XV.  ]

[Luis Fernando Arias, Consejero Mayor de la ONIC, afirma que esta fecha conmemora la **invasión de territorios** americanos que no ha cesado y que **continúa vulnerando los derechos** sociales, económicos, culturales y ambientales de los pueblos indígenas en Colombia. Por lo que **“Debemos liberar a nuestros estados del yugo transnacional, neoliberal y capitalista que los ha subyugado”**.]

[Arias, cita 2 de los cientos de casos que denotan la violación de derechos a los pueblos indígenas colombianos. El primero de ellos, el de los Wayuu en La Guajira, afectados por los 30 años de explotación de la mina de carbón más grande a cielo abierto, El Cerrejón. El segundo, relacionado con las 3 comunidades indígenas en Guaviare que sobreviven comiendo “sopa de mango biche” a causa del monocultivo de palma aceitera en este departamento.]

[Vea también: [[Al menos 1 niño muere a diario por desnutrición y deshidratación en La Guajira](https://archivo.contagioradio.com/al-menos-1-nino-muere-a-diario-por-desnutricion-y-deshidratacion-en-la-guajira/)]]

[La actividad extractiva en La Guajira por parte del Cerrejón ha **desplazado comunidades, vulnerado derechos laborales, causado impactos ambientales irreversibles y generado afectaciones en la salud de los pobladores**. En palabras de Arias una “crisis humanitaria alarmante” ante la que ni el Gobierno ni la empresa han asumido costos para la atención y solución de estas problemáticas.]

[Pese a las denuncias interpuestas **“El Estado ha actuado sordo, ciego y mudo”** asegura Arias, por lo que los indígenas Wayuu han recurrido a instancias alternativas como los Tribunales Populares.]

[Vea también: [[El Cerrejón en la mira del Tribunal Popular contra las transnacionales en La Guajira](https://archivo.contagioradio.com/se-realizara-tribunal-popular-contra-las-transnacionales-en-la-guajira/)]]

[El Consejero Mayor refiere que **en Guaviare 3 comunidades indígenas mueren de hambre**, pues la agroindustria de la palma aceitera les deja para comer sólo mangos. A lo anterior se suma la **“orfandad estatal”** en la que estas comunidades están sumidas y que no ha posibilitado la superación de las **afectaciones** que el **conflicto armado**, los **procesos de colonización del narcotráfico** y la **implantación de monocultivos** les ha provocado.]

[Pese a las Sentencias proferidas por la Corte Constitucional y los pronunciamientos de entes internacionales **el Estado colombiano no ha actuado de forma responsable**, por lo que el movimiento indígena solicita a la comunidad nacional e internacional unirse a las demandas de los pueblos indígenas pues **“no se puede ser cómplice del genocidio”** y es necesario articular la "capacidad política y jurídica para **exigir retiro de empresas como el Cerrejón del territorio nacional**", concluye Arias.]
