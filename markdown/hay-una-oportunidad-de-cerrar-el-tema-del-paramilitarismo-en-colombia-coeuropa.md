Title: Hay una oportunidad de cerrar el tema del paramilitarismo en Colombia: COEUROPA
Date: 2016-07-08 16:17
Category: DDHH, Nacional
Tags: COEUROPA, conersaciones de paz de la Habana, Derechos Humanos, ELN, FARC, Paramilitarismo
Slug: hay-una-oportunidad-de-cerrar-el-tema-del-paramilitarismo-en-colombia-coeuropa
Status: published

##### 8 jul 2016

La Coordinación Colombia Europa Estados Unidos, COEUROPA, que agrupa más de 400 organizaciones de Derechos Humanos en el país, se reúne desde este miércoles y jueves para discutir los retos y las apuestas de las organizaciones. El paramilitarismo es uno de los asuntos a tocar, **pues hay una oportunidad de cerrar ese tema en Colombia y construir desde ahí una nueva lógica de defensa de los DDHH** y de construcción de la paz luego del fin del conflicto armado.

Durante estos dos días, las organizaciones que hacen parte de esa plataforma han discutido en torno al **proceso de paz de la Habana, y reiteraron su respaldo, así como la importancia de que se abra la mesa pública con el ELN** para que haya una “paz completa”, asegura Diana Sánchez, una de las [voceras de la plataforma](https://archivo.contagioradio.com/?s=coeuropa), que, además, insiste en la necesidad de entender los acuerdos una vez se firmen, para hacer pedagogía con la mayoría de la población Colombiana.

Sánchez insistió en la necesidad de hacer pedagogía en las regiones en torno a los acuerdos, pero también de hacer presencia porque “la situación en las regiones sigue complicada” y sigue requiriendo de [atención, tanto a nivel nacional, como internacional](https://archivo.contagioradio.com/revision-de-doctrina-militar-debe-hacerse-de-cara-al-pais-coeuropa/). La vocera reiteró que el tema central del trabajo de las organizaciones seguirá siendo el de los DDHH, **porque se trabaja en la lógica de [alcanzar paz con dignidad](https://archivo.contagioradio.com/el-avance-de-la-paz-es-cuestion-de-garantias-coeuropa/).**

El trabajo de esta asamblea ha rondado sobre los momentos y los acuerdos de paz tanto con las FARC como con el ELN, la situación interna de las organizaciones y la cooperación internacional, y se espera que al concluir emita una **declaración política en la que reiteren sus compromisos como organizaciones hacia la construcción de la paz** con garantías de respeto por los derechos de todos y todas.

<iframe src="http://co.ivoox.com/es/player_ej_12164645_2_1.html?data=kpeemJmaeJahhpywj5WbaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaXdwtPOjbiJh5SZopbbxc3JvoampJCw0ZCpudPj0caap9jYpcXj1JDC0M7Is9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
