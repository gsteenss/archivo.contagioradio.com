Title: Ayotzinapa o el terrorismo de estado en México
Date: 2014-12-17 16:41
Author: CtgAdm
Category: Hablemos alguito
Slug: ayotzinapa-o-el-terrorismo-de-estado-en-mexico
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fseoisjE.mp3)  
¡Vivos se los llevaron vivos los queremos! son las palabras que más se pronuncian desde hace dos meses en México. El 26 de Septiembre de 2014 policías municipales del municipio de Iguala del estado Guerrero de México atacaron a estudiantes de la Escuela normal de Ayotzinapa, causando 6 muertos, 43 desapariciones forzadas y un sinfín de heridos. Hoy dos meses después aún no han aparecido los estudiantes, pero si han continuado las desapariciones forzadas, en Cocula se ha sabido que en Julio fueron desaparecidos 30 estudiantes más. Así como la aparición de numerosas fosas comunes en el propio estado y de 11 cadáveres calcinados y decapitados, en Chilapa también en el propio estado.  
Para entender lo sucedido entrevistamos a Carlos, estudiante de la Escuela Normal de Ayotzinapa, a Bitulfo Rosales abogado defensor de Derechos Humanos y a Alejandro Cerezo del Comité Cerezo (organización social de DDHH).Todos ellos apuntan hacia la unión entre policía narcos y paramilitares de carácter fascista. Es por ello lo sucedido en la Normal, ya que es una escuela de tradición de izquierdas y donde se han educado luchadores sociales de todo tipo, además de ser una de las escuelas más combativas del propio estado. También nos interesamos por las fuertes protestas y la represión que el estado ha cargado contra los manifestantes, muchos de ellos detenidos en total indefensión. En todo este clima de protestas e indignación es necesario analizar las palabras del presidente Peña Nieto, de su partido político y de los demás de cara a enfrentar el conflicto.
