Title: Respeto de DD.HH sería condición para presupuesto de EE.UU hacia Colombia
Date: 2017-05-24 17:36
Category: Nacional, Paz
Tags: Donald Trump, proceso de paz
Slug: respeto-de-dd-hh-seria-condicion-para-presupuesto-de-ee-uu-hacia-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/1b_trump-e1495663920632.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [24 May 2017] 

El anunció de la **reducción en un 35% que harían Donald Trump, en su plan de presupuesto a Colombia, no es tan preocupante** frente a las medidas que el presidente ha tomado con referencia a los apoyos que Estados Unidos le brinda a otros países, como lo son el caso concreto de México y Venezuela, así lo afirmó Lisa Haugaard, directora de Latin American Working Group.

De acuerdo con Haugaard, Trump planea realizar recortes mucho más serios incluso al interior de su país, como a los programas para personas de escasos recursos y clase media en Estados Unidos, con la finalidad de aumentar la inversión en guerra de este país. Además, agregó que lo más importante es que en **el presupuesto que se presentó se mantenga el apoyo al proceso de paz de Colombia**.

No obstante, el senador Marco Rubio señaló, a través de su red social en Twitter que no respalda el proceso de paz en Colombia, sino que esta a favor de continuar la asistencia a este país con condiciones, Haugaard indicó frente a este hecho, que en el **senado existe un apoyo al proceso de paz y que esta tan solo es una voz** al interior del recinto.

El Congreso de los Estados Unidos tendrá que aprobar los presupuestos y se espera que se modifiquen debido a que diferentes senadores, tanto **Republicanos como Demócratas han expresado no estar a favor de los recortes**. Le puede interesar: ["Llegó la hora de la verdad para el Acuerdo de Paz en el Congreso"](https://archivo.contagioradio.com/llego-la-hora-de-la-verdad-para-el-acuerdo-de-paz-en-el-congreso/)

Otro de los temas que orienta la agenda Estados Unidos – Colombia, es la lucha contra el narcotrafico y aunque el gobierno de Trump, es partidario de una guerra antinarcóticos tradicional se espera que los congresistas **se apeguen al texto del Acuerdo entre el Gobierno y las FARC-EP** para que se priorice la política de sustitución de cultivos de uso ilícito. Le puede interesar: ["Corte Constitucional congela Acuerdo de Paz con las FARC: Enrique Santiago"](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Frente a este tema Haugaard afirmó que “la administración Trump está volviendo a la guerra contra la droga” con penas muy altas para personas que son adictos a las drogas o vendedores de pequeñas cantidades, expresando su preocupación por el aumento de la producción de coca en Colombia. Sin embargo, considera que por el momento las **intenciones de Trump no se encuentran en aumentar o generar un presupuesto para combatir las drogas**.

###### Reciba toda la información de Contagio Radio en [[su correo]
