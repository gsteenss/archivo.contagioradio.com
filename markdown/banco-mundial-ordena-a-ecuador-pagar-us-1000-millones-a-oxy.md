Title: Banco Mundial ordena a Ecuador pagar US 1000 millones a OXY
Date: 2015-11-03 18:24
Category: Ambiente, Economía, El mundo
Tags: Banco mundial sanciona a Ecuador, Explotación petrolera amazonía ecuatoriana, Oxy explotación en Ecuador, Rafael Correa
Slug: banco-mundial-ordena-a-ecuador-pagar-us-1000-millones-a-oxy
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/oxy_ecuador_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: vistazo.com] 

##### [3 Nov 2015]

De acuerdo con la determinación del tribunal de arbitraje del Banco Mundial, el gobierno ecuatoriano tendría que cancelar un total de 1.061 millones de dólares como indemnización por la anulación del contrato que la multinacional petrolera tenía para explotaciones en una zona de la amazonía ecuatoriana.

La sentencia del Centro de Arreglo de Disputas Relativas a Inversiones (CIADI), adscrito al Banco Mundial, fue proferida tras la demanda entablada por Occidental (Oxy) en el año 2006, cuando el gobierno ecuatoriano declaró la caducidad del contrato por haber cedido en Agosto de 2000 y sin avisar al Estado, el 40% del mismo a la compañía AEC, filial de la canadiense Encana.

La compañia argumentaba que la decisión del país andino violaba el Tratado Bilateral de Inversiones (TBI) con Estados Unidos, al asumir las operaciones del campo tras su salida de la zona, en una especie de "confiscación de activos", ante lo cual el presidente Rafael Correa, manifestó en su cuenta de Twitter que "Tratan la caducidad por incumplimientos legales, como confiscación. ¡Otro atentado a nuestra soberanía!".

"Llegó laudo OXY. Logramos nulidad de 40 % de laudo original, es decir 700 millones menos, pero nos ordenan pagar cerca de mil" millones,  afirmó Correa.

Aunque la CIADI aun no hace público el contenido de la sentencia, el mandatario aseguró en la red social que se mantienen las negociaciones para llegar a un acuerdo con la compañía, la misma que inicialmente pretendía recibir 3.300 y luego 1.700  millones de dólares, por el contrato que les permitiría extraer por lo menos 100.000 barriles de crudo al día.

La polémica sentencia equivalente al 3,3 % del presupuesto estatal para 2016, agudiza la situación económica del país golpeada por la caída de los precios del crudo y el alza del dólar, adoptada como moneda oficial.

 
