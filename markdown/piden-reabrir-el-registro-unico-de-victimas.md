Title: Piden reabrir el Registro Único de Víctimas
Date: 2017-04-13 10:40
Category: DDHH, Nacional
Slug: piden-reabrir-el-registro-unico-de-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Unidad-de-víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidad de Víctimas] 

###### [12 Abr 2017]

El Registro Único de Víctimas del conflicto que ha logrado documentar que en Colombia se han producido 8.100.180 de personas victimizadas de las cuales 6.366.598 son sujetos de atención y 1.733.582 personas son víctimas directas. Sin embargo desde 2010 el registro se cerró. En el caso de los c**rímenes de los paramilitares y otras situaciones podrían sumarse 1 millón de personas a 2017**.

Según Alfonso Castillo, presidente de la Asociación Nacional de Ayudas Solidarias, ANDAS, es necesario que se reabra dicho registro, ya que la victimización en razón del conflicto armado persiste. “Si se reabre ese registro se estima que podrían ingresar un millón de personas” que hasta el momento no están reconocidas.

La exigencia se hace en medio del reciente anuncio de un decreto en que el Estado asumiría una **prestación vitalicia para las personas víctimas del conflicto que han perdido el 50% o más de su capacidad laboral**, según el anuncio las personas que demuestren esa situación y que estén registradas en el RUV podrán acceder a este auxilio que tendría un monto de un salario mínimo legal vigente, es decir, cerca de 713 mil pesos mensuales de manera vitalicia.

Para Castillo este anuncio es muy positivo pero debe garantizarse que llegue a hacerse realidad, puesto que tendría que enfrentarse a las regulaciones y cambios que hagan los congresistas y a la regla fiscal que también ha afectado a otros proyectos que buscan favorecer a las víctimas, incluso ha frenado sentencias que **ordenan al Estado indemnizar de manera suficiente y eficiente a las personas afectadas** por el propio Estado o por el conflicto.

Además el decreto señala que hay unas barreras de acceso, una de ellas que se logre demostrar el derecho a ser beneficiarios, o que no haya sido favorecido con alguna indemnización administrativa y otra serie de situaciones que **se esperaría no implicaran mayor dificultad, sobre todo cuando se está hablando de las víctimas** que son el centro del acuerdo entre el gobierno y las FARC.

<iframe id="audio_18121096" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18121096_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
