Title: Hay crisis en la implementación del acuerdo de paz
Date: 2017-09-26 13:01
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, Gobierno Nacional, implemntación de los acuerdos de paz, reincorporación
Slug: hay-crisis-en-la-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Laprensa.hn] 

###### [26 Sept 2017 ] 

Luego de un año de la firma de los acuerdos de fin del conflicto con las FARC, diversos analistas y sectores sociales han señalado que **es preocupante el estado de incumplimiento del acuerdo de paz, en todos los niveles.** Por una parte no se ha cumplido a los integrantes de las FARC en el proceso de reincorporación y en el congreso pareciera estarse negociando un nuevo acuerdo.

Las FARC denunciaron, el 24 de septiembre de 2017, la muerte del integrante Roberto Sepúlveda en la cárcel donde se encontraba detenido hace 9 años. De acuerdo con Jesús Santrich, Sepúlveda **tenía orden de libertad y no le habían dado salida.** Dijo que “debido a la negligencia institucional y la inasistencia médica murió”.

El integrante del partido político manifestó que, en los territorios que antes eran Zonas Veredales, **hay incertidumbre por la falta de garantías de seguridad** inmediata y a futuro de las personas que se encuentran en proceso de reincorporación. (Le puede interesar: ["Implementación de los acuerdos de paz no va por buen camino"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-no-va-por-buen-camino/))

Por ejemplo una denuncia es que en el territorio del Gallo en Sucre, había antes 120 personas de las cuales **60 se han retirado**. “El Gallo es un matadero donde la gente espera a que le llegue su suerte porque no ha avanzado el proceso de reincorporación como quedó pactado en la Habana” asegura Santrich.

Por otro lado, en varias ocasiones **han denunciado la incursión de paramilitares** en cercanía al Espacio Territorial de Reincorporación de Remedios en Antioquia. Allí, de acuerdo con las denuncias, el 25 de septiembre, los paramilitares habrían secuestrado a tres integrantes de las FARC, una de las cuales habría sido asesinada, aunque la información está por confirmar.

### **FARC argumenta mala fe del gobierno para implementar los acuerdos de paz** 

Para Santrich **hay varias señales de que se estaría tejiendo una "telaraña jurídica"** para impedir la participación en política de las FARC. Una de las líneas de esa telaraña son los señalamientos y cuestionamientos de la fiscalía al listado de bienes. "El Fiscal no aceptó la lista de bienes que entregamos y este proceso no ha terminado" . (Le puede interesar: ["Implementación de los acuerdos requiere movilización social": Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/))

Igualmente, desde el gobierno se ha cuestionado el proceso de construcción del listado de integrantes de las FARC **cuando este no ha terminado**. Santrich afirmó que es un proceso conjunto en que se evalúa y se verifica por parte de ellos. Dijo que no se puede señalar las exclusiones como intentos de las FARC de acreditar personas que no son de la organización.

Dijo también que, el Congreso de la República, **está modificando el acuerdo de paz** en la medida que ha querido, por ejemplo, reestructurar las Circunscripciones Especiales de Paz. Santrich afirmó que "los tiempo electorales están reducidos, no hay elegibilidad política y el tiempo del fast track se está acabando".

**Segunda Misión de la ONU en Colombia es una esperanza para el cumplimiento de los acuerdos**

Ante el trabajo de la primera Misión de la ONU, que se encargó de verificar el proceso de dejación de armas por parte de las FARC, Santrich indicó que fue **una gestión transparente y exitosa** que benefició al proceso.

Por esto manifestó que la segunda Misión es un aliento para que la comunidad internacional **verifique y haga seguimiento al cumplimiento** de los acuerdos que tienen que ver con la reincorporación a la vida civil de los integrantes de las FARC. (Le puede interesar: ["Del fast track al slow track")](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/)

Hizo además un llamado para que la comunidad internacional y la ONU **ayuden a que los acuerdos de la Habana se cumplan.** Para él, el gobierno ha incumplido en múltiples ocasiones “la palabra pactada, demostrando la mala fe que existe para continuar con la implementación”.

<iframe id="audio_21104328" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21104328_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
