Title: “La paz es un derecho contra-mayoritario” FARC-EP
Date: 2016-10-03 12:51
Category: Nacional, Paz
Tags: Corte Constitucional, FARC, Juan Manuel Santos, plebiscito por la paz
Slug: la-paz-es-un-derecho-contra-mayoritario-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/timo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NNC] 

###### [03 Oct 2016] 

A través de un video publicado por la agencia de noticias Nueva Colombia, Rodrigo Londoño aseguró que las FARC permanecerán fieles a lo acordado y que todos los frentes de esa guerrilla seguirán asegurando el cese bilateral y definitivo. Además llamaron a todos los sectores sociales a movilizarse en favor de la paz y contra el odio, y recordó que **la paz es un derecho “contramayoritario” el acuerdo ya fue consignado en Suiza lo que le confiere un carácter un efecto irrevocable** y da la viabilidad jurídica.

Londoño también hizo alusión las recientes sentencias de la Corte Constitucional afirmó que la consulta plebiscitaria no tiene efecto jurídico sino político y que el efecto jurídico del acuerdo lo da su propia firma como acuerdo especial. “**La paz llegó para quedarse**” y resaltó que los enemigos de la paz no se podrán imponer sobre el sentimiento mayoritario.

\[embed\]https://www.youtube.com/watch?v=4ykUmDgwNMk\[/embed\]  
Además, las FARC publicaron una editorial en la analizan los resultados del plebiscito y que reproducimos a continuación.

##### *"Según los resultados electorales el No ha ganado el plebiscito de este dos de octubre* 

##### *Ante este resultado adverso a la refrendación electoral se conforma el escenario de “la vía difícil” para la implementación de los acuerdos de la Habana, pues la “vía fácil del plebiscito” que ilusoriamente había soñado el Presidente Santos y que a última hora impuso en la mesa de la Habana al momento de la firma final, ha sufrido un determinante revés electoral y político.* 

##### *No nos interesa hacer un análisis de lo que significa para la “gobernanza” de Santos este resultado electoral. Le corresponde a sus consejeros y ministros hacer el balance de lo ocurrido y tomar las decisiones que correspondan para mantener y continuar el proceso de paz firmado con las FARC-EP, además del anunciado para con el ELN, con el fin de llevar a “toda” la sociedad colombiana hacia la paz, que fue su promesa electoral con la que fue elegido como presidente y ha sido el principal objetivo de la “gobernanza” Santista.* 

##### *Uribe Vélez, su adversario de fracción de clase oligárquica logró conformar una coalición religiosa-política apoya da por UN sector militarista de las FFMM, el mismo que hizo el vuelo rasante durante el discurso de Timoleón, coalición a la que se le adhirió el Procurador Ordoñez y varias Iglesias incluido el obispado católico, y quienes exhibieron toda su fuerza en la masiva manifestación del 10 de agosto pasado, como un adelanto de lo que se acaba de presentar en el plebiscito acabado de votar.* 

##### *Dos cosas son irrenunciables:* 

##### *1 El acuerdo logrado en la Habana después de más de 5 cinco años de diálogos y dificultades, el que finalmente se logró firmar con la aprobación de Toda la Comunidad Internacional, que lo ha legitimado en si totalidad como un acuerdo internacional alcanzado entre un Estado legítimo, dirigido por el Presidente Santos legítimamente elegido en un proceso electoral legítimo y para un periodo constitucional: No Puede ser tocado, ni trasformado, ni adulterado por ninguna de las Partes que lo firmaron, so pena de quedar deslegitimado inmedatamente. ES FACIL: NO SE PUEDE ALTERAR PORQUE SE INVALIDA automáticamente.* 

##### *2- La legitimación alcanzada por las Farc-EP ante el Pueblo colombiano, es decir, por los millones de colombianos con sus familias que votaron por el Sí a la paz de Colombia, que entendieron todos los abundantes gestos de paz de las Farc-EP, su conferencia guerrillera con sus conclusiones, y asumieron los perdones por los hechos de guerra sucedidos en las décadas de conflicto, TAMPOCO SE PUEDE PERDER, REGRESANDO A LA GUERRA.* 

##### *Estas dos legitimaciones, son las que Uribe Vélez y su patota político religiosa y militarista quieren reversar a como dé lugar; empujando nuevamente a la guerrillerada, que ha mostrado honestamente su voluntad de dejar las armas para a que se regresen al combate armado; lo cual se debe evitar al máximo. Es decir, salvo una provocación insoportable que realicen las fuerzas en mención, se debe seguir manteniendo la voluntad expresada por el comandante Timoleón Jiménez de PERSISTIR EN LA PALABRA COMO ARMA DE PAZ PARA COLOMBIA.* 

##### *Y, por último, dada la fractura tan marcada de la sociedad colombiana puesta en evidencia por la votación Plebiscitaria; PERSISTIR TERCAMENTE en la vieja consigna de las dos Insurgencias de CONVOCAR DE MANERA URGENTE una Asamblea Nacional CONSTITUYENTE AMPLIA Y DEMOCRATICA, que incluya al ELN y al EPL y selle definitivamente el proceso de paz en Colombia.* 

##### *Ya lo había advertido con bastante anticipación uno de nuestros columnistas: Santos cayó en su propia trampa y ahora para salvar la paz de todo el país y de todos los colombianos, y si realmente su compromiso es con la paz que tanto invocó, deberá tomar con cabeza fría este revés electoral y convocar lo más pronto posible a una CONSTITUYENTE que tanto evitó. No es el momento de renuncias para que suba a la presidencia Vargas Lleras, taimado militarista adversario de los acuerdos de Paz con las dos insurgencias. Es el momento de profundizar el proceso CONSTITUYENTE que el Plebiscito puso al orden del día en Colombia.* 

##### *Las paradojas de le Historia son así: Desconcertantes.* 
