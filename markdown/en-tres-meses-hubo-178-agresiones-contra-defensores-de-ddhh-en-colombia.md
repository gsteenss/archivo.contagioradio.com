Title: En tres meses hubo 178 agresiones contra defensores de DDHH en Colombia
Date: 2015-10-28 16:57
Category: DDHH, Nacional
Tags: defensores de derechos humanos, Informe trimestre defensores derechos humanos, proceso de paz, Radio derechos Humanos, Somos defensores, Unidad Nacional de protección, UNP
Slug: en-tres-meses-hubo-178-agresiones-contra-defensores-de-ddhh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/LOSNADIES1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9197436_2_1.html?data=mpammZmXeo6ZmKialJWJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bf1dfS1ZDRqdTZ1JDV18fTb5KrmZDOydfJt8rjz8rgjcjTstXmwpDRx8vJstTj08rgjcnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Guevara, SIADDHH] 

###### [28 oct 2015]

Agresiones contra defensores y defensoras de derechos humanos de julio a septiembre de este 2015 van en aumento en comparación con el mismo periodo de 2014. El SIADDHH, recogió** 178 casos de defensores y defensoras de derechos humanos** agredidos de diferentes formas. El **39% fueron en contra de hombres y el 61% contra mujeres** y en 118 casos la responsabilidad recae sobre grupos paramilitares.

Carlos Guevara, coordinador de comunicaciones e incidencias, indica que este incremento “no es algo casual” y se debe a que en el mes de septiembre se **amenazaron a más de 80 mujeres defensoras**  que están **vinculadas al “proceso de paz** y al trabajo de proceso de paz”, lo cual “*tiene una vinculación directa frente al papel que está jugando la mujer y los movimientos de mujeres frente al tema del proceso de paz y sobretodo el aterrizaje del proceso de paz en lo territorial*”.

Carlos Guevara, indica que este informe presenta “*continuidad de un escenario de un incremento de agresiones contra defensores a partir del año 2014*”, los cuales siguen siendo “objetivo militar”. **En el último trimestre, han sido amenazados 133 defensores**, hay registrados 17 asesinatos, 3 víctimas de atentados, 15 detenciones arbitrarias, 3  víctimas de proceso de judicialización y 6 víctimas de hurto de información sensible con la que trabajaron y hay un desaparecido.

Frente a los responsables el informe evidencia que el paramilitarismo persiste al cometer 118 de las agresiones, la Fuerza Pública 15, la Fiscalía 8, la guerrilla del ELN 1 y actores desconocidos en 36 hechos. Lo que resulta grave es que el gobierno se niega a reconocer la existencia del paramilitarismo al afirmar que los grupos señalados de amenazar "no existen" como en el caso de las Aguilas Negras.

Guevara indica que pese a que el programa ha identificado a los responsables “el gobierno nacional se encuentra bastante desinteresado en investigar, encontrar y judicializar a los perpetradores de las agresiones, muestra de ello, sería esa misma negación de la existencia de los grupos paramilitares.

El informe presenta 8 hechos de agresiones por parte de la Fiscalía, en los que  ha sido copartícipe de "detenciones arbitrarias contra defensores de derechos humanos" y se evidencia "persecución política", cuestión que puede ir en aumento en los próximos meses, lo cuál preocupa ya que la Fiscalía es muy rápida en las investigaciones de acusaciones contra los defensores, pero cuándo los delitos están dirigidos contra la vida de ellos, estas no avanzan.

** Ver :** [Informe Somos Defensores](https://es.scribd.com/doc/287534559/Informe-Somos-Defensores-Trimestral "View Informe Somos Defensores Trimestral on Scribd")  
<iframe id="doc_40056" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/287534559/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
