Title: Abiertos los archivos de la Internacional Comunista
Date: 2015-03-13 16:06
Author: CtgAdm
Category: Cesar, Opinion
Tags: archivo internacional comunista, Trotsky, Unión Soviética, URSS
Slug: abiertos-los-archivos-de-la-internacional-comunista
Status: published

###### Imagen: sp6.fotolog 

#### **Por [[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)]** 

Conocer el pasado para no repetir sus errores en[ ]{.Apple-converted-space}el[ ]{.Apple-converted-space}ahora es un adagio común. En el terreno de la investigación científica lo traducimos como producción de la verdad histórica desde el presente (Walter Benjamin). La escritura de la historia requiere el acceso al “documento” - lo escrito, lo oral, la Memoria, la Víctima, el ícono, la foto -; pero el Príncipe, el poder, el Estado, siempre intenta impedirlo. Siendo la historia la de los vencedores, la verdad oficial, única, ha sido la norma incluso en países cuyas economías no se rigen, o no lo hicieron, por la ley del valor.

Los años 20 del siglo pasado fueron los del imperialismo, la revolución y sus fracasos. Lenin, Trotsky y los bolcheviques se hicieron con el poder en el imperio zarista y fundaron la Unión Soviética (URSS). Para consolidar el triunfo llamaron a la revolución mundial, especialmente en Alemania; la creación de la Tercera Internacional o Internacional Comunista (Komintern) en 1919, en[ ]{.Apple-converted-space}Moscú,[ ]{.Apple-converted-space}respondía a tales fines.

La democracia directa y sus asociadas, la representativa y la participativa, forjaron los caminos de ese partido mundial, la Komintern. Entre 1919 y 1922 se realizaron 4 congresos que dejaron una herencia programática sobre aspectos distintos alusivos al poder, a la política de alianzas (Frente Único), a la cuestión de las nacionalidades, a la revolución colonial, entre otros.

Sin embargo, a partir de 1924 la economía y la política en la Unión Soviética cambiaron de orientación. Emergió el estalinismo y la burocracia desplazó a la democracia imponiendo su dictadura en el Estado, en la clase trabajadora, en los órganos dirigentes y en el partido; así se consiguió también el control sobre la Tercera Internacional. Los años siguientes fueron los de las purgas internas y la expulsión de la Oposición de Izquierda de la Komintern. Y la historia se tornó en oficial, en dogma.

Guerra, imperialismo y revolución se mezclaron en los años 30; la Internacional[ ]{.Apple-converted-space}pasó a organizar una serie de derrotas.[ ]{.Apple-converted-space}Francia y Alemania se convirtieron en la cuna del Frente Popular - alianza estratégica entre los partidos comunistas y la burguesía al mando del Estado – y la guerra civil en España (1936-1939) puso al descubierto lo que significaba la intervención militar condicionada del estalinismo soviético: por ejemplo, el pacto de no-agresión entre Stalin y Hitler en 1938 facilitó parcialmente el triunfo de Francisco Franco.

[Todo esto se presentó como éxito. Pero la historia no duerme; ahora los archivos podrán conducirnos a la verdad histórica - objetiva, parcial y relativa -. Luego de años de trabajo, y más de un millón de dólares de financiación, el]{.s1}Archivo Estatal de Historia Social y Política de Rusia (AEHSPR), con la colaboración de sus pares en Europa, ha abierto al público[ ]{.Apple-converted-space}los archivos de la Komintern. Desde el 5 de marzo de 2015 los investigadores podrán acceder digitalmente en forma gratuita a[ ]{.Apple-converted-space}miles de documentos. Hay que anotar, sin embargo, que aún hay otros miles de documentos que no han sido desclasificados y que esperan que el[ ]{.Apple-converted-space}nuevo Príncipe lo autorice. El AEHSPR ha anunciado también que se podrán consultar los documentos relativos a las Brigadas Internacionalistas que combatieron en la guerra de España. El vínculo es [http://sovdoc.rusarchives.ru]{.s1}

La Historia y la Memoria continúan siendo campos de batalla.
