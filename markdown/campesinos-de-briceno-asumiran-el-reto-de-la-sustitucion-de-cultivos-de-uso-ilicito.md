Title: Campesinos de Briceño asumirán el reto de la sustitución de cultivos de uso ilícito
Date: 2016-06-15 17:41
Category: Nacional, Paz
Tags: Briceño, Conversaciones de paz de la habana, desminado humanitario, ELN, FARC, Gobierno Nacional, Sustitución de cultivos de uso ilícito
Slug: campesinos-de-briceno-asumiran-el-reto-de-la-sustitucion-de-cultivos-de-uso-ilicito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/briceño-sustitución-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ocha colombia] 

###### [15 Jun 2016]

Los campesinos de Briceño afirman que el anuncio de ser municipio piloto de sustitución de [cultivos de uso ilícito](https://archivo.contagioradio.com/por-que-deberian-revisarse-las-penas-de-mujeres-por-delitos-de-drogas/) es una buena noticia porque puede impulsar la economía campesina, pero también exigieron mayor aporte por parte del Estado puesto que la **proliferación de este tipo de cultivos se da porque los demás productos no tienen los canales de comercialización adecuados**, es decir, vías de transporte e infraestructura favorable.

El pasado viernes se anunció que el municipio de Briceño en Antioquia será el escenario del plan piloto de sustitución de cultivos de uso ilícito acordado entre el gobierno nacional y la guerrilla de las FARC en el marco de las conversaciones de paz que se desarrollan en la Habana. Fabio Muñoz, **campesino habitante de la vereda “el orejón” una de las primeras beneficiadas asegura** que en la región se sienten muy optimistas por dicho anuncio.

Para Muñoz, el problema de la gran cantidad de cultivos de uso ilícito ha sido cuestión de supervivencia y que muchos de los campesinos se vieron en [la obligación de sembrar coca dadas las condiciones económicas del municipio](https://archivo.contagioradio.com/comunidades-piden-atencion-de-santos-para-que-escuche-propuestas-sobre-politica-antidrogas/) y de la región. En ese sentido el contenido del acuerdo es para una **atención integral de este tipo de problemáticas** porque incluye acciones en varios sentidos.

Según las delegaciones de paz una de las primeras tareas que se cumplirían en el marco de este plan tiene que ver con la regularización de la propiedad de la tierra y otros asuntos relacionados que están contenidos en el **acuerdo marco de Desarrollo Rural Integral**. Además se adecuarían las condiciones de infraestructura y de comercialización que favorecieran el impulso de otro tipo de economías.

Otro de los aspectos relevantes del acuerdo es que la nueva lógica de intervención dejaría de perseguir al productor como delincuente para comenzar a trabajar el tema **[desde la perspectiva del derecho](https://archivo.contagioradio.com/fumigaciones-terrestres-con-glifosato-una-decision-paradojica-y-negligente/)** a producir en la tierra y el derecho de dar un sustento digno a las familias campesinas. En las declaraciones de Iván Márquez este punto aparece resaltado *“No más castigo judicial a la gente humilde por delito de pobreza.”*

### **Briceño: piloto de acuerdo de desminado y descontaminación de artefactos explosivos** 

Para Fabio Muñoz, tanto este acuerdo como el anterior en torno al **desminado y descontaminación** han significado varios [retos para los pobladores](https://archivo.contagioradio.com/se-reanuda-desminado-humanitario-en-el-orejon/), por una parte lograr un trabajo coordinado con los actores en la guerra y por otra superar los prejuicios entre unos y otros para poder conseguir el objetivo de limpiar el territorio y aunque ese proceso siga sin culminar del todo se evalúa como un importante avance.

Otro de los grandes problemas ha sido la persistencia del **[paramilitarismo](https://archivo.contagioradio.com/acciones-paramilitares-en-briseno-antioquia-tienen-en-riesgo-de-desplazamiento-a-400-personas/)** que es otra de las tareas pendientes del Estado colombiano y un compromiso que tendrá que asumir como garantía de la aplicación de los acuerdos de paz.

<iframe src="http://co.ivoox.com/es/player_ej_11923325_2_1.html?data=kpamlJiXdpahhpywj5aUaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjbLZaaSnhqee0d-PcYy3wtLdx9jNstChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
