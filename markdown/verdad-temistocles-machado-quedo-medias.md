Title: Verdad en el asesinato de Temístocles Machado quedó a medias
Date: 2019-04-03 13:31
Author: CtgAdm
Category: Comunidad, Líderes sociales
Tags: buenaventura, entrevista, lider, Temístocles Machado, Voz
Slug: verdad-temistocles-machado-quedo-medias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Temistocles-Machado-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: postalesparalamemoria.com] 

###### [3 Abr 2019] 

Más de un año después del asesinato de Temístocles Machado, líder de tierras en Buenaventura, Valle del Cauca, la comunidad ha seguido sus pasos y se ha mantenido en la defensa del territorio. El de Don Temis, como era conocido, es uno de los pocos casos en que la justicia ha dado resultados, en un periodo de tiempo relativamente corto. (Le puede interesar:["La lucha por la que habría sido asesinado Don Temis"](https://archivo.contagioradio.com/temistocles_machado_lider_buenaventura_paro_asesinado/))

**El asesinato de Don Temis ocurrió el 27 de enero de 2018 en Buenaventura**, y por los delitos de homicidio agravado, concierto para delinquir y porte ilegal de armas fue encontrado culpable Jorge Luis Jaramillo, autor material de hecho, quien fue condenado a 17 años de prisión y deberá pedir perdón a la familia del Líder. (Le puede interesar: ["Familiares de Don Temis continuarán con su legado"](https://archivo.contagioradio.com/familiares-de-temistocles-machado-lider-social-asesinado-continuaran-con-su-legado/))

**María Helena Cortés, líder de tierras que trabajó junto a Machado defendiendo a los pobladores del barrio Isla del Sol en el puerto**, afirmó que la decisión judicial deja un sabor agridulce para la comunidad. En primer lugar, porque en este caso la Fiscalía procedió con eficiencia, generando una condena rápida (situación que no ocurre en la mayoría de casos), y dio tratamiento al hecho como un asesinato por motivos políticos.

Sin embargo, Cortés lamentó que se redujera la pena de Jaramillo en un 50% solo por haber confesado el hecho, y peor aún, que **el condenado omitiera en su declaración los nombres de los autores intelectuales,** lo que significa que “las personas que están ejerciendo liderazgo en sus comunidades están en riesgo”, pues la persona que pagó para asesinar a Don Temis sigue en la clandestinidad, y podría seguir ordenando la muerte de otros líderes.

En ese sentido, la condena quedó a medias porque lo importante es que el asesinato no quede impune; como lo explicó la Líder, no solo por la tranquilidad de la Comuna 6, o Buenaventura sino de todo el país y la comunidad internacional, que ve un gran riesgo en la “inoperancia de la justicia cuando se trata del asesinato de líderes sociales”. (Le puede interesar:["Roban información relacionada con la labor de Don Temis en Buenaventura"](https://archivo.contagioradio.com/roban-informacion-relacionada-con-labor-de-lider-temistocles-machado-en-buenaventura/))

### **Colombia tiene una deuda con la reconciliación** 

Jaramillo también deberá disculparse con la familia de Machado, situación frente a la que Cortés se mostró escéptica, porque “más que una orden, la disculpa debe nacer desde el victimario”, y añadió que en términos de reconciliación, Colombia tienen una deuda a lo largo de estos 50 años de conflicto, porque no se trata de tener “perdones a medias y verdades a medias” sobre lo ocurrido, sino sinceridad completa en cada acto.

En el caso particular de Don Temis, la Activista en el Puerto, consideró que hace falta un proceso para ambas partes: la familia del Líder y Jaramillo para poder pensar que la reconciliación y perdón serán sinceros, y no serán tomados como un “cliché, para decir: cumplimos”. Adicionalmente, porque ese perdón debería ser también hacia la Comunidad, porque su asesinato se perpetró en el parqueadero Los Nativos, de propiedad del líder ubicado en su barrio, lo que significó que, así como lo habían matado a él, a cualquiera lo podrían haber asesinado.

### **Los problemas contra los que se enfrentó Don Temis persisten en Buenaventura** 

Maria Elena Cortés, enfatizó que los intereses contra los que trabajó Don Temis siguen estando presentes en el Puerto: La posibilidad de seguir en su tierra, la lucha por tener un hospital de primer nivel en el Distrito Especial y que se retribuya en algo la riqueza que entra por Buenaventura. Como lo recordó la Líder, de la zona salen unas 3.500 tractomulas diarias, es uno de los principales puertos de Latinoamérica, y es el lugar en el que se abastecen buques de combustibles, agua y remesas.

Adicionalmente, Buenaventura aporta en impuestos 5 billones anuales al país (se calcula que circula más dinero por el Puerto que en Bogotá, Cali o Medellín), razón por la que aún **no se entiende que sus 420 mil habitantes no tengan garantizado el acceso a servicios fundamentales como al agua.**

En ese sentido, Don Temis “presentaba identidad, dignidad y resistencia; además era un visionario: él tenía 90 mil folios que fueron enviados al Centro Nacional de Memoria Histórica, y más de la mitad de esos folios era una construcción jurídica”, lo que representa finalmente la historia de las tierras que defendía. (Le puede interesar: ["La ruptura de confianza entre las víctimas y el Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/victimas-memoria-historica/))

### **“Don Temis se multiplicó, y vive en la voz de muchas personas”** 

**El efecto que buscan quienes asesinan a los defensores de derechos humanos en las comunidades es amedrentarlas para que cesen en el reclamo de sus derechos**, esta situación ha sido ampliamente explicada por organizaciones como Somos Defensores o la Asociación Minga.

Sin embargo, en el caso de Don Temis, la gente no abandonó su comunidad y entendió que no se podían seguir repitiendo los modelos de violencia. Como expuso Cortés, **“después de un año de habernos golpeado el asesinato, Don Temis se multiplicó y vive en la voz de muchas personas”**. (Le puede interesar: ["Amenazan a dos líderes sociales del paro cívico de Buenaventura"](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/))

Temistocles Machado fue un líder en todo el sentido de la palabra, articuló, gestionó y trabajó con la gente, eso explica que la tristeza de su muerte se haya convertido en fortaleza, y como relató Cortés, la comunidad siguió trabajando en el archivo de la Comuna 6, realizaron un mural en honor al líder y hoy, “contando todo esto que se hace, el miedo se va”, al punto que hasta los niños del Barrio se preguntan por qué hay miedo, si hay tanto amor en la Comunidad.

<iframe id="audio_34096812" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096812_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
