Title: Formulario de contacto 1
Date: 2018-12-18 15:44
Author: AdminContagio
Slug: formulario-de-contacto-1
Status: published

<label> Tu nombre (requerido)  
\[text\* your-name\] </label>

<label> Tu correo electrónico (requerido)  
\[email\* your-email\] </label>

<label> Asunto  
\[text your-subject\] </label>

<label> Tu mensaje  
\[textarea your-message\] </label>

\[submit "Enviar"\]  
1  
Contagio Radio "\[your-subject\]"  
Contagio Radio <wordpress@contagioradio.com>  
contagioradio@contagioradio.com  
De: \[your-name\] &lt;\[your-email\]&gt;  
Asunto: \[your-subject\]

Cuerpo del mensaje:  
\[your-message\]

--  
Este mensaje se ha enviado desde un formulario de contacto en Contagio Radio (https://www.contagioradio.com)  
Reply-To: \[your-email\]

Contagio Radio "\[your-subject\]"  
Contagio Radio <wordpress@contagioradio.com>  
\[your-email\]  
Cuerpo del mensaje:  
\[your-message\]

--  
Este mensaje se ha enviado desde un formulario de contacto en Contagio Radio (http://contagioradio.com)  
Reply-To: diana@decemedialab.com

Gracias por tu mensaje. Ha sido enviado.  
Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.  
Uno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.  
Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.  
Debes aceptar los términos y condiciones antes de enviar tu mensaje.  
El campo es obligatorio.  
El campo es demasiado largo.  
El campo es demasiado corto.  
El formato de fecha es incorrecto.  
La fecha es anterior a la más temprana permitida.  
La fecha es posterior a la más tardía permitida.  
Hubo un error desconocido subiendo el archivo.  
No tienes permisos para subir archivos de este tipo.  
El archivo es demasiado grande.  
Se ha producido un error subiendo la imagen  
El formato de número no es válido.  
El número es menor que el mínimo permitido.  
El número es mayor que el máximo permitido.  
La respuesta al cuestionario no es correcta.  
El código introducido es incorrecto.  
La dirección de correo electrónico que has introducido no es válida.  
La URL no es válida.  
El número de teléfono no es válido.
