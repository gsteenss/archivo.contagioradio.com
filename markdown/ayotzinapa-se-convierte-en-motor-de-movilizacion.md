Title: Ayotzinapa se convierte en motor de movilización
Date: 2015-01-27 17:48
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Ayotzinapa, Enrique Peña Nieto, mexico
Slug: ayotzinapa-se-convierte-en-motor-de-movilizacion
Status: published

###### Foto: proceso.com.mx 

La inconformidad de los mejicanos y mejicanas ha ido **creciendo y ha encontrado en el caso Ayotzinapa un motor de indignación** y de reivindicación de múltiples causas, incluyendo la exigencia de la renuncia de Enrique Peña Nieto y una nueva constitución.

La Octava Jornada Global por Ayotzinapa, realizada el pasado 26 de Enero, la reivindicación de justicia por la desaparición de los 43 estudiantes de la Normal Rural Isidro Burgos de Ayotzinapa el 26 de Septiembre de 2014, se vio alimentada por otras **víctimas de crímenes del Estado**, incluso, se exige que no se realicen **elecciones en el Estado de Guerrero**, puesto que votar por uno u otro candidato, es **votar por los propios victimarios** como explica Jhonatan, coordinador de Revolución 3.0.

Adicionalmente se estableció que los próximos **31 de enero y 5 de febrero** se realizarán una serie de reuniones tendientes  organizar y planificar próximas acciones en una **agenda de trabajo que recoja la indignación y la canalice**, no solamente a través de la movilización sino de otras acciones.

Esta octava jornada global, se caracterizó porque no se vivieron las escenas de represión de otras acciones, que han dejado varias personas detenidas.
