Title: "Las armas son las que deberían estar confinadas, no los combatientes": Delegación de Paz FARC
Date: 2016-03-29 17:31
Category: Entrevistas, Paz
Tags: delegación FARC, proceso de paz
Slug: delegacion-de-paz-farc-desmiente-que-haya-crisis-en-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Benkos-Bioho-farc-e1459283629913.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

###### [29 Mar 2016 ] 

A propósito de las afirmaciones hechas en diferentes medios de información sobre una crisis en el proceso de paz,  Contagio Radio habló con Benkos Biohó, integrante de la Delegación de Paz de las FARC y miembro la Subcomisión para la Finalización del Conflicto, quien asegura que no hay crisis en el proceso de paz, sino un serie de diferencias que están en discusión.

Es decir, el 23 de marzo no se firmó ningún acuerdo o pre acuerdo dado que el **tiempo fue insuficiente para evacuar algunos temas**, y aunque se ha avanzado, el integrante de la delegación de paz de las FARC, se abstiene de dar una fecha específica más allá de asegurar que "el próximo ciclo será muy nutrido" y que **"el 2016 es el año de la paz"**.

<div class="yj6qo ajU">

<div id=":md" class="ajR" tabindex="0" data-tooltip="Mostrar contenido acortado">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

</div>

</div>

### [Territorios Especiales para la concentración de miembros de las FARC, [TERREPAZ](https://archivo.contagioradio.com/terrepaz-podria-ser-la-solucion-integral-para-el-conflicto-en-colombia/)] 

Frente a la consolidación de las zonas en las que estarían emplazados los ex combatientes, Biohó asegura que, tanto la extensión como el número, son aspectos que se están acordando en la Mesa y parte de la discusión tiene que ver con la caracterización e implementación de tres tipos de espacios.

El primero denominado 'zonas campamentarias' en las que estarían los miembros de las FARC y en las que la seguridad correría por su cuenta; el segundo, referido como **'zonas de transición o amortiguamiento’ en las que actuaría la Comisión de Verificación** y un tercero en el que estarían las fuerzas designadas por el Estado y la comunidad internacional.

La polémica radica en la concepción de esas zonas. Mientras que la delegación del Gobierno ha asegurado que las zonas deben ser reducidas y ocupadas únicamente por miembros de las FARC. Por su parte Biohó afirma que esta guerrilla considera que **se deben confinar las armas y no a los combatientes**, "las armas deben estar confinadas y reguladas en un lugar específico y con un mecanismo de verificación y monitoreo de los acuerdos, **pretender el aislamiento de nuestra organización de las organizaciones de base con las que hemos convivido históricamente es algo que no tiene lógica** cuando se nos está planteando la posibilidad de acceder al espacio político electoral".

Razón por la que se están analizando los mecanismos que les "permitiría interactuar con las comunidades, **ampliar el radio de acción con la sociedad colombiana, que es lo que al final va a permitir que se sume ese conjunto** (...) al proceso de construcción de paz", teniendo en cuenta que, como asevera Biohó, el pago de penas alternativas establecidas en el marco de la jurisdicción especial, implica que se concentren en sitios específicos sin que ello delimite su capacidad de interlocución con la sociedad civil. Parte de estos análisis han incluido el estudio de la[ [propuesta presentada por CONPAZ](https://archivo.contagioradio.com/conpaz-pide-acelerar-negociaciones-de-paz-con-eln-y-farc/)] a la Mesa de Negociaciones y frente a la que todavía no se tiene un pronunciamiento oficial.

### [Paramilitarismo] 

Uno de los puntos de discusión más álgidos en la Mesa de Conversaciones ha sido el de la dejación de armas, al respecto Biohó insiste en que este paso implica contar con garantías de desmonte del paramilitarismo porque amenaza la seguridad de quienes se reinserten a la vida civil. El paramilitarismo es un "**fenómeno multidimensional que (...) debe ser combatido de manera frontal con una política de Estado"** y que evite que las élites sigan "aferrada al uso de la violencia para permanecer en las estructuras de poder y control de la nación".

Por tanto insiste en que debe adoptarse una política real y efectiva de combate frontal a las prácticas paramilitares en todos su niveles, no solamente los relacionados con los actores armados que son utilizados por los sectores políticos y económicos del país para controlar, "un compromiso histórico sí se quiere avanzar como nación" y "**un proceso que tendrá que ser paulatino" dado lo enraizado de que está el fenómeno en la sociedad**.

### [Dejación de armas] 

Para que exista una efectiva dejación de las armas, las FARC han planteado al Gobierno nacional, que debe haber **garantías no solo para el grupo guerrillero, sino para el conjunto de la sociedad colombiana**, de manera que cesen las estigmatizaciones, asesinatos, desapariciones, torturas y detenciones de los colombianos que expresan una posición política y una visión de país distinta.

Según la propuesta que explica Biohó, la **dejación de armas se haría de forma gradual y alternada con el cumplimiento de los acuerdos.** Una primera fase en la que las armas de uso colectivo se dejarían custodiadas por la veeduría internacional y una segunda etapa en la que las armas de uso individual se dejarían en esos mismos lugares.

De esta manera **se garantizará que no exista "proselitismo armado"**, pues los delegados por las FARC para interactuar con la población "en ningún momento portarían ningún tipo de armas", explica Biohó, quien agrega que "en su totalidad las armas estarían confinadas en unas áreas específicas" de acuerdo con los puntos de transición que se definan entre el gobierno y las FARC. El delegado concluye que **poner una fecha límite a la dejación de las armas, como lo ha hecho el presidente Santos, "genera una presión indebida"**.

### [Pedagogía para la paz] 

La guerrilla de las FARC asume como una "necesidad imperiosa" el que puedan realizar pedagogía con la población civil y las guarniciones militares, tal y como lo ha venido haciendo el Gobierno nacional, pues para ellos, **interactuar con las comunidades, es ampliar ese radio de comunicación que al final permitirá que la sociedad colombiana en su conjunto se sume a la construcción de la paz**.

Para la delegación "no tiene sentido" que los ex combatientes "sean confinados" en zonas de concentración pues este aislamiento impediría que las comunidades en las que han "convivido históricamente" despejen sus dudas frente al proceso y puedan cooperar en la implementación de los acuerdos, necesidad que han trasmitido a la Mesa de Conversaciones, pues para ellos **es lógico que el fin del conflicto debe generar espacios y garantías para el debate político** sobre la visión de país que tienen las FARC, para que los colombianos puedan tomar una decisión.

### [Responsabilidad de Estados Unidos] 

En relación con la injerencia de Estados Unidos en lo que se ha mencionado que parte de la protección de los ex combatientes en los territorios correría por cuenta suya, Biohó asegura que "ya hay una resolución de la ONU", que estudia la seguridad y que "lo lógico de estos procesos es la separación de fuerzas y la implementación de mecanismos que permitan el control territorial"; sin embargo, agrega que la responsabilidad de EEUU tiene que ver con su aporte monetario a la guerra y que las acciones que el país asuma llevar a cabo tienen que ser un **"compromiso moral" de un Estado que ha influido directamente en el conflicto armado** **y que debe abonar "el camino de la reconciliación nacional".**

<iframe src="http://co.ivoox.com/es/player_ej_10980387_2_1.html?data=kpWmmpWXfJihhpywj5iVaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncYamk7HO1ZDFts7V1JDg0dOPsMLnjNbix5DIqcPZ04qwlYqliMLijMrg1sbWb8Tjz8vW0MbIpdSZk6iY0NSPsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
