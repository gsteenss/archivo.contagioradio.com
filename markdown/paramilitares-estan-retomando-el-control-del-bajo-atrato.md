Title: Paramilitares están retomando el control del Bajo Atrato
Date: 2019-02-26 15:55
Category: DDHH, Nacional
Tags: AGC, Bajo Atrato, Chocó, paramilitares
Slug: paramilitares-estan-retomando-el-control-del-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Grupos-armados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereseclesial de Justicia y Paz] 

###### [25 Feb 2019] 

Las zonas humanitarias de Pueblo Nuevo, Nueva Esperanza y el resguardo humanitario de Alto Guayabal, que adoptaron esa figura para que no ingrese ningún actor armado a su territorio, ubicadas en la región del Bajo Atrato, llevan más de una semana denunciando el control paramilitar del que están siendo víctimas. Dicha situación ha provocado el confinamiento de las comunidades, temor en las personas y el desplazamiento forzado de familias, sin que el Estado tome medidas urgentes frente a estos hechos.

Asimismo, desde hace 10 días la Comisión Intereclesial de Justicia y Paz viene informando el aumento de confrontaciones armadas entre actores legales e ilegales en los territorios de Jiguamiandó y Curvaradó, situación que ya causó la muerte de un niño por desnutrición, y tiene en riesgo a todas las zonas humanitarias con territorios en el Bajo Atrato.

### **Zona Humanitaria Pueblo Nuevo** 

En las últimas horas, la Zona Humanitaria de Pueblo Nuevo, ubicada en el territorio colectivo de Jiguamiandó, alertó sobre la presencia de cerca **de 60 paramilitares, integrantes de las Autodefensas Gaitanistas de Colombia (AGC**), que fueron vistos saliendo del área de la Zona Humanitaria en dirección al caserío el Hobo, ubicado a 40 minutos de distancia, apróximadamente.

De acuerdo con la denuncia, los hombres que vestían camuflados, armas largas y brazaletes que los identificaban como miembros de las AGC, llegaron hacia las 6:10 pm al territorio del Hobo y se situaron a 10 minutos de camino de la comunidad. Otro grupo de paramilitares de las AGC, se ubicaron por el sector de Zapayal, a menos de 10 minutos de la Zona Humanitaria de Jiguamiandó.

Desde el pasado 23 de febrero, los habitantes de Pueblo Nuevo advirtieron la incursión paramilitar de aproximadamente **30 hombres de las AGC, hacia las 6 de la tarde.**  Los hombres obligaron a la comunidad a reunirse y durante 45 minutos estuvieron en el territorio, preguntándole a las personas por un líder social. Sin embargo, la población respondió **que cada uno de ellos es un líder**. Posteriormente los paramilitares amenazaron que sí no decían quien era el líder los sacaban del territorio. (Le puede interesar: ["Paramilitares de las AGC intentan asesinar a líder en zona humanitaria de Jiguamiandó, Chocó"](https://archivo.contagioradio.com/neoparamilitares-intentan-asesinar/))

En seguida, y luego de la renuencia de la comunidad a responder, los paramilitares afirmaron que "debían acostumbrarse a la presencia de los armados en el territorio porque iban a sacar al ELN de la zona". De igual forma, la Comisión Intereclesial de Justicia y Paz, señaló que "**algunos de estos paramilitares manifestaron que tenían el apoyo de los militares y que iban a conocer las denuncias".**

### **Zona Humanitaria Nueva Esperanza ** 

El sábado 23 de febrero, la Comisión denunció que 8 hombres ingresaron en cuatro motos platino RX y Honda, a la Zona Humanitaria de Nueva Esperanza, buscando a los hermanos Fabio y Asprilla Álvarado. Las personas, integrantes de las AGC, iban vestidas de civil, con los rostros cubiertos con cascos y dos de ellas llevaban pasamontañas.

La organización señaló que los hombres portaban armas largas, ametralladoras y armas cortas. Una vez ingresaron, la comunidad activo sus mecanismos de protección internos y de inmediato defensores de la Comisión Intereclesial de Justicia y Paz y observadores internacionales, abordaron a los armados.

Uno de ellos reaccionó al ver a las defensores empuñando su arma apuntando al cielo, luego se retiraron de la Zona Humanitaria. Al salir, los paramilitares expresaron: **“vámonos llegaron estos hp gringos”**. (Le puede interesar: ["Alerta por fuertes operaciones paramilitares en territorios de Curvaradó y Jiguaminadó"](https://archivo.contagioradio.com/operaciones-neoparamilitares-curvarado-jiguamiando/))

### **Resguardo Humanitario Alto Guayabal** 

Desde el pasado 24 de febrero, **la comunidad indígena Embera del Alto Guayabal se encuentra confinada** por la presencia de paramilitares de las AGC en el territorio, que han impedido la libre movilidad.

Este hecho sumado a los constantes enfrentamientos entre actores armados, ha generado que actividades como la pesca y los trabajos de campo sean limitados. Además, el ELN también habría advertido a los habitantes sobre no salir de sus viviendas por los combates.

Asimismo, la falta de alimentos ha provocado por lo menos 40 casos de desnutrición al interior de la comunidad indígena, **que ya ocasionó la muerte de un menor de edad, de 8 meses de nacido. **

### **En Contexto: ¿Qué pasa en el Bajo Atrato?**

Como lo explica el director del Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ) Camilo Gonzalez Posso, el Chocó y Bajo Atrato son zonas que tienen más del 90% del territorio en propiedad colectiva y con el que se cruzan zonas ambientalmente protegidas. Precisamente, de sus riquezas naturales se deriva el interés por estos territorios y por desarrollar negocios como la explotación de madera, con unas ganancias calculadas "en 1.500 millones de dolares al año", o la extracción de oro **"que son más de 4 mil millones de dolares al año entre el Chocó y el Bajo Cauca".**

Para el analista, las rentas producto de la explotación de estos recursos naturales, el control territorial para realizar su extracción y la pugna por el control de las rutas y zonas de producción de narcotráfico, explican el aumento de actividad por parte de grupos armados ilegales en el territorio.

Posso afirmó que la respuesta institucional es muy débil. De hecho, recordó que durante reuniones que han sostenido con los pobladores de la zona, estos se han quejado por la inoperancia de las autoridades militares y civiles.

### **¿Aumento de pie de fuerza sirve para algo?** 

El instituto INDEPAZ registró un aumento de la presencia de la Fuerza Pública en la zona y una contención a la expansión de los diferentes grupos; no obstante, dada la geografía del territorio, las comunidades han señalado la acción como poco eficaz, y siguen denunciando la expansión del área de influencia de los diferentes grupos.

Pero el problema no radica, únicamente, en la cantidad de uniformados en la zona o las dificultades de acceso al terreno, pues tal como lo ha denunciado la Comisión de Justicia y Paz, **es probable que los agentes de la Fuerza Pública conozcan los movimientos de las AGC que se han dado recientemente.**

A ello añade Posso que, "incluso temas como la gran minería con dragas sigue allí, con todo y sus impactos ambientales", lo que denota una incapacidad institucional del Estado para controlar lo que ocurre en el territorio, y garantizar la vida de las comunidades que diariamente se ven asediadas por las acciones de grupos narcoparamilitares, integrantes del ELN o, disidencias que no se acogieron al proceso de paz.

<iframe id="audio_32863160" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32863160_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
