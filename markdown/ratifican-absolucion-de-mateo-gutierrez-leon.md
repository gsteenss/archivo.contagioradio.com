Title: Ratifican absolución de Mateo Gutiérrez León
Date: 2020-01-15 13:51
Author: CtgAdm
Category: Judicial, Nacional
Tags: falso positivo judicial, Jóvenes del Andino, Mateo Gutierrez
Slug: ratifican-absolucion-de-mateo-gutierrez-leon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de 20 meses en prisión y una absolución en primera instancia el pasado 7 de Noviembre de 2018, el Tribunal Superior de Bogotá ratificó, la decisión de absolver a Mateo Gutierrez León por los delitos de concierto para delinquir, terrorismo, porte ilegal de armas y hurto agravado y calificado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso del joven estudiante de sociología de la Universidad Nacional fue uno de los más sonados dadas las circunstancias en las que se produjo su captura y el momento de conmoción que vivía el país. En diversos medios de comunicación se señaló al joven de ser responsable de una serie de explosiones en Bogotá en sedes de entidades bancarias y de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en el momento del juicio la fiscalía decidió presentar pruebas solamente por la explosión de una bomba panfletaria en el centro de Bogotá que no dejó víctimas. La acusación se basaba en el testimonio de una persona que dijo reconocer a Gutiérrez como una de las personas que lo amarró para producir la explosión. Lea también: [Jóvenes del Andino víctimas del sistema de justicia](https://archivo.contagioradio.com/libertad-implicados-atentado-andino/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Justicia dejaría en claro que caso Mateo Gutiérrez es un Falso Positivo Judicial
--------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de varios meses de juicio también se intentó vincular al joven de 21 años con el llamado "MRP" sin que el ente acusador haya logrado vincularlo, por ello la procuraduría se apartó de la hipótesis del fiscal y pidió la absolución de León que se dio luego de la decisión del Juez Especializado. Lea tambien: [Así fue la captura de Mateo Gutiérrez](https://archivo.contagioradio.com/pruebas-de-la-fiscalia-no-demuestran-la-culpabilidad-de-mateo-gutierrez/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La apelación, resuelta este miercoles por el Tribunal Superior de Bogotá podría pasar a manos de la Corte Suprema de Justicia si la Fiscalía decide presentar un recurso de Casación, sin embargo hasta el momento, las decisiones judiciales están comprobando que el caso de Mateo es un Falso Positivo Judicial. Ver también [Jucio contra Mateo Gutierrez carece de pruebas: defensa](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
