Title: Asesinato de Ariel López, es el octavo contra la comunidad LGBTI de Barranquilla entre 2018 y 2019
Date: 2019-08-02 17:57
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Asesinatos contra población LGBT, Atlántico, Barranquilla, Caribe Afirmativo
Slug: asesinato-de-ariel-lopez-es-el-octavo-contra-la-comunidad-lgbti-de-barranquilla-entre-2018-y-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Áriel-López.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Emisoras ABC] 

En horas de la tarde del pasado 1 de agosto fue asesinado **el docente Ariel López** quien hacía parte de los procesos de incidencia de la organización Caribe Afirmativo junto a la comunidad LGBTI en el marco del posconflicto en Soledad, Atlántico. Con su asesinato, son ocho los casos que han ocurrido bajo la misma circunstancia en el área metropolitana de Barranquilla, cuatro de las víctimas eran profesores.

Según relata Wilson Castañeda, director de Caribe Afirmativo, el docente de 43 años no había denunciado ninguna amenaza en su contra. Pese a vivir con su esposo, Ariel fue atacado mientras se encontraba solo en su casa y herido en numerosas ocasiones con arma blanca, no hay señales de forcejeo ni robo en el lugar del crimen.

Castañeda compara el panorama actual con el que se conoció cerca del 2010 cuando homicidios con características similares se presentaron en el Eje Cafetero donde la Fiscalía determinó que se trataba de una serie de ataques sistemáticos en contra de integrantes de organizaciones LGBTI.

### Casos como el de Ariel ya han sido replicados en el pasado

Advierte que no se puede asegurar que se trata de un crimen por prejuicio o por la orientación sexual de Áriel, hace la salvedad que la Fiscalía debería tener esa hipótesis sobre la mesa, **"los sitios de encuentro de las víctimas han sido filtrados por actores ilegales, motivados por el prejuicio, usan su orientación sexual para hacerla más vulnerable"**, explica .

Para el director de Caribe Afirmativo, es clave generar campañas en los gobiernos locales, pues es la ausencia de estrategias de cultura ciudadana la que lleva a que en Barranquilla y otros sectores de la costa, haya una tendencia a la homofobia que en lugar "de ser desestimada por las autoridades, hay un silencio que les da cierta permisividad".

Aunque el Atlántico es uno de los departamentos donde más casos de este tipo en el país, tan solo después de Antioquia y Arauca, Castañeda señala que no se debe tanto a la frecuencia de los mismos sino a que, junto a Bolívar, es uno de los departamentos donde hay mayor crecimiento poblacional. [(Le puede interesar: Que Duque no nos obligue a volver al clóset: Caribe Afirmativo)](https://archivo.contagioradio.com/duque-no-nos-obligue-a-volver-al-closet-caribe-afirmativo/)

Según la organización **por año se reportan entre 18 y 25 homicidios en la región Caribe contra la comunidad LGBTI**, una tendencia que se había mantenido desde 2011, sin embargo, tal como va la curva de crecimiento acercándose a los 17 o 18 casos para el mes de julio, parece que la tendencia a este fenómeno se incrementaría en 2019.

Por último, Castañeda indica que la presencia de Caribe Afirmativo en la región ha permitido tener conocimiento de estos casos, sin embargo la ausencia de organizaciones sociales en otras regiones y la ausencia de acciones preventivas por parte del Estado conlleva a que muchos de estos hechos permanezcan invisibilizados en otras partes de Colombia

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39373582" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39373582_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
