Title: Sin garantías para  participación ciudadana avanza proyecto sobre Reserva Van Der Hammen
Date: 2018-10-11 15:20
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: alcaldía bogotá, CAR, Reserva Thomas Van der Hammen
Slug: sin-garantias-para-participacion-ciudadana-avanza-proyecto-sobre-reserva-van-der-hammen
Status: published

###### [Foto: Archivo Particular] 

###### [11 Oct 2018] 

Luego de que se realizara la primera audiencia informal sobre la construcción del complejo Lagos de Torca que afectaría parte de la Reserva Thomas Van Der Hammen, el movimiento ambientalista denunció varias irregularidades y atropellos por parte del consejo directivo de la CAR, **entre ellos la falta de garantías para la participación ciudadana.**

Claudia Calau, integrante de la mesa de veeduría de la Reserva, afirmó que en principio esta audiencia "informal" no reconoció la participación de un tercero interviniente, que permitía un postura formal. Situación a la que se suma una negación rotunda de la participación ciudadana debido a que el proceso sobre la Thomas Van Der Hammen ya se adelantó con el inicio del requisito de sustracción.

Igualmente, Calau señaló que durante esa asamblea podían participar ciudadanos durante 5 minutos, ya que las instituciones, tanto privadas como distritales, tuvieron su propio escenario frente al consejo directiva de la CAR. Sin embargo, la ambientalista afirmó que al espacio llegó el gerente de Ciudad Norte, Juan Camilo González, que participó como ciudadano, hecho que para la Veeduría significó una **"falta de respeto por parte de la Alcaldía".**

### **Las falencia de Lagos de Torca** 

La Mesa de Veeduría ya había denunciado que el proceso de sustracción de vías públicas que viene adelantando la Alcaldía y que pretende retirar un área de la Reserva, no cuenta con la licencia ambiental correspondiente, hecho que podría "destruir" este espacio. (Le puede interesar: ["¿Ya hay una decisión de la CAR sobre la Van Der Hammen?"](https://archivo.contagioradio.com/ya-hay-una-decision-de-la-car-sobre-la-reserva-van-der-hammen/))

Además, Calau aseguró que la Alcaldía también viene adelantando una campaña de desprestigio en contra de la Reserva, afirmando a la comunidad de Chirrillos, que el progreso **no ha llegado hasta ese lugar, por la falta de intervención en la Van Der Hammen.**

Frente a estas situaciones ya se han interpuesto recusaciones contra tres personas debido a que estarían impedidos para continuar con el proceso de Lagos de Torca, entre ellos esta Luis Eduardo Mota, Julio César Turbay y Carlos Costas.

###### Reciba toda la información de Contagio Radio en [[su correo]
