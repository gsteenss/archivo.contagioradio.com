Title: Venezuela: Derrota continental
Date: 2015-12-09 11:55
Category: Cesar, Opinion
Tags: elecciones en venezuela, la perdida de la izquierda en america, PSUV
Slug: venezuela-derrota-continental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/venezuela-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: coquivacoatelevision 

#### [**[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)**] 

###### [8 Dic 2015] 

Sabíamos que la oposición avanzaría en su pretensión de obtener más escaños en la Asamblea Nacional. Pero no imaginamos que el triunfo alcanzaría la magnitud que hoy conocemos; sencillamente arrasó. 100 puestos es de hecho un nuevo gobierno.

[Los cambios que desde ya se perfilan sí que son imaginables: elecciones anticipadas para presidencia, derogación de leyes emitidas con base en el Estado de Excepción y en las “Leyes Habilitantes”, reemplazo de funcionarios en los organismos claves de la administración central (Consejo Nacional Electoral, etc.), liberación de los López y similares, inyección de mayores capitales extranjeros para la dinamización de la economía, orientación diferente en cuanto a política internacional …]

[¿Qué ha sucedido? ¿La intervención norteamericana es el factor central en la derrota del chavismo? ¿O fue la campaña de la oposición que engañó a millones de venezolanos? ¿Falló el PSUV?]

[Digamos que hay de todo, como en botica. Los partidos continentales de la derecha se movilizaron con sus maquinarias conocidas: discursos ideológicos injerencistas, medios de comunicación, redes sociales (que en esta oportunidad se potenciaron al máximo y proyectaron parcialmente la movilización ciudadana), financiación económica y demás. El partido conservador colombiano está entre los que más le apostaron a la derrota del chavismo. Y Estados Unidos, obvio, intervino abiertamente en cuanto escenario público domina, además de financiar a los sectores de la oposición. También intervinieron en contra la OEA, la ONU, y los Rajoy de Europa.]

[Pero, nos parece, el elemento principal de análisis debe ubicarse en el modelo de gestión estatal, en lo económico, lo político y lo social. Es decir, en la forma de adelantar lo que se ha llamado “socialismo del siglo XXI”. Una equivocación generalizada y una concesión interminable de favores a los sectores del capital, internos y externos.]

[El pueblo venezolano, chavista o no (que sin duda se ha visto beneficiado por distintos programas sociales gubernamentales), ha estado sufriendo las consecuencias de una desastrosa política económica gestionada desde el Estado que, sumada al boicot de la derecha acaparadora y desestabilizadora, no pudo controlar los altísimos niveles de inflación (17% mensual y 200% anual), ni el desabastecimiento de alimentos (pollo, huevos) y artículos básicos (papel higiénico, por ejemplo). En cambio sí permitía el creciente rendimiento financiero de la banca nacional y multinacional; la generación de una burocracia “roja, rojita” que se ha enriquecido ilícitamente y amplió los márgenes de corrupción; y las alianzas políticas con las elites, nuevas y antiguas. Desde éste ángulo, las largas filas para obtener lo poco que se ofrecía en almacenes de Estado y supermercados hacían patente la distorsión económica y la desesperación normal de las familias frente a la incapacidad gubernamental; en general se votó para castigar al gobierno de Maduro con beneficio, claro, para la derecha opositora.]

[Lo social tuvo sus momentos democráticos durante la vida de Hugo Chávez pero incluso en esta fase se implementaron mecanismos de control vertical que impidieron la ampliación de la democracia social por abajo, plebeya. La creación del Partido Socialista Unido de Venezuela (PSUV) fue y es una herramienta autoritaria que limitó la movilización del pueblo chavista y que le imponía todo tipo de decisiones partidistas y estatales. Como ya se sabe por experiencia y por tradición (]*[Erfahrung]*[), un]**proceso de transición comienza por la ampliación de la democracia**[; el PSUV la decapitó con la ayuda de las Fuerzas Armadas Bolivarianas.]

[Nunca hubo, en mi criterio, una]*[transición]*[; el “socialismo del siglo XXI” fue la etiqueta]*[light]*[ de los gobiernos “progresistas” para manifestar su rechazo a las políticas y prácticas neoliberales y para obtener importantes niveles de participación popular que legitimaran y proyectaran sus programas; pero una vez que la movilización se salía de lo “democráticamente” posible se impuso un rígido control estatal (Venezuela, Bolivia y Ecuador). Lo “políticamente correcto” se convirtió en razón de Estado (que es en lo que tanto ha insistido el socialdemócrata (?) García Linera, vicepresidente de Bolivia.]

[Nos acongoja la derrota en Venezuela; la asumimos como una de carácter continental de los pueblos de “Nuestra América”. Significa, además, el cierre de los gobiernos progresistas en América Latina. Pero no implica el cierre de la movilización popular de los vencidos ni de sus resistencias; estamos frente a un “alto” en la infinita bifurcación de los caminos. Las posibilidades de democracia siguen estando en manos de los de abajo y de sus políticas de independencia política. Pesimismo activo en toda la línea es la apuesta; ninguna confianza en la línea de “progreso” para salir del estancamiento y el “subdesarrollo”; ninguna alianza “estratégica” que sacrifique la independencia política; ningún dilema frente al “mal menor”.]
