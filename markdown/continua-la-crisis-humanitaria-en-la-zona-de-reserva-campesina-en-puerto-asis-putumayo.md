Title: Continúa la crisis humanitaria en la Zona de Reserva Campesina en Puerto Asís, Putumayo
Date: 2018-04-30 13:18
Category: DDHH, Movilización
Tags: Amenaza a líderes sociales, Puerto Asís, Putumayo, Zona de Reserva Campesina
Slug: continua-la-crisis-humanitaria-en-la-zona-de-reserva-campesina-en-puerto-asis-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [30 Abr 2018] 

Comunidades en Puerto Asís, Putumayo, denunciaron la grave situación humanitaria que están viviendo producto de la **situación de inseguridad** por la presencia de grupos armados que han amenazado y hostigado a las comunidades que trabajan por la sustitución voluntaria de cultivos de uso ilícito. Además las amenazas contra los líderes continúan, esta vez contra dos líderes de la Zona de Reserva Campesina.

De acuerdo con la denuncia, el pasado 19 de abril “fueron amenazados y hostigados los líderes de la Zona de Reserva Campesina, **Saúl Luna y Hugo Miramar** en el casco urbano de Puerto Asís”. Adicionalmente, seis días antes “por operaciones de control territorial”, un grupo de hombres con armas de largo alcance “asesinó a dos jóvenes reclutados forzosamente”.

### **Incumplimientos frente a la implementación de los Acuerdos de Paz ha generado nuevas dinámicas de violencia** 

Adicional, las comunidades expresaron que el incumplimiento en la implementación de lo acordado en la Habana frente a la sustitución de cultivos de uso ilícito, ha provocado que se **formen nuevos grupos armados** aumentando así la violencia contra los líderes campesinos que promueven alternativas agroambientales y protección ambiental.

Han dicho que “desde el casco urbano de Puerto Asís estructuras armadas han venido **evitando la sustitución voluntaria** y presionando a los sembradores de coca desde el primer semestre de 2017 con hostigamientos y amenazas”. (Le puede interesar:["Guerra contra líderes de sustitución voluntaria se está tomando al Putumayo"](https://archivo.contagioradio.com/lideres_sociales_putumayo_puerto_asis_asesinatos/))

### **Grupos armados anunciaron expansión de sus operaciones** 

Teniendo en cuenta esta situación, los grupos que **antes hacían parte de las FARC** y que han cuestionado los incumplimientos del Gobierno Nacional, “han anunciado que expandirán sus operaciones” hacia la Zona de Reserva Campesina de la Perla Amazónica.

Finalmente, los campesinos alertaron sobre la situación en la que viven varios líderes de la Zona de Reserva Campesina de la Perla Amazónica y de la Red de Comunidades Construyendo Paz en los Territorios, CONPAZ, quienes han recibido amenazas constantes desde 2017. Indicaron que **no ha habido respuesta gubernamental** y que las operaciones militares y policiales no han sido suficientes para garantizar la seguridad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
