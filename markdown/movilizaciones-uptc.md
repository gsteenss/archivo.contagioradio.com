Title: Suspensión de actividades no detiene movilizaciones en UPTC
Date: 2018-10-26 16:38
Author: AdminContagio
Category: Educación, Movilización
Tags: estudiantes, Movilización, Suspensión actividades, UPTC
Slug: movilizaciones-uptc
Status: published

###### [Foto: @Pedropsalas] 

###### [19 Oct 2018] 

Tras dos semanas en paro y campamento universitario, **directivos de la Universidad Pedagógica y Tecnológica de Colombia (UPTC)** expidieron un comunicado informando sobre la suspensión de todas las actividades en dicha institución, situación con la que, según algunos de sus integrantes, se busca romper el movimiento estudiantil. (Le puede interesar: ["Las marchas estudiantiles seguirán hasta que haya mesa de concertación con el Gobierno"](https://archivo.contagioradio.com/marchas-mesa-de-concertacion/))

Sin embargo, la medida no resulta extraña para la comunidad universitaria pues como lo mencionó **Daniel Fernandez, vocero de los estudiantes de la UPTC ante la UNEES**, a lo largo del semestre ya había sido decretado el cese de actividades mientras se realizaban las asambleas estudiantiles, medida que en su criterio se toma para fragmentar los espacios de movilización, porque muchos estudiantes optan por regresar a sus lugares de origen.

El vocero señaló que la comunicación resulta paradójica, en tanto decreta la suspensión de actividades, medida que ya se estaba llevando a cabo por parte de lo estudiantes en uso de su autonomía universitaria. No obstante, Fernandez aclaró que **seguirán buscando el acercamiento con las directivas de la Universidad para llegar a acuerdos**. (Le puede interesar: ["FECODE está dispuesto a dar espacio a los estudiantes en reunión con el Gobierno"](https://archivo.contagioradio.com/fecode-reunion-gobierno/))

### **¿Qué reclama la UPTC?** 

Fernandez aclaró que la Universidad enfrenta algunos de los problemas que se mencionan en el pliego de peticiones nacionales, pero también tienen exigencias locales relacionadas con la **autonomía y democracia universitaria;** así como el bienestar que presta la institución. Adicionalmente, piden que se revise el mecanismo de cobro de matriculas por parte de la Procuraduría y Contraloría, porque hay dudas sobre tales procedimientos.

El estudiante invitó a que se revise la distribución de los presupuestos universitarios, porque muchos de los dineros de la Universidad se concentran en Tunja, desconociendo otras sedes. Adicionalmente, se refirió al **mecanismo de elección de rector,** el cual fue creado por todos los actores de la comunidad universitaria, pero rechazado por parte del Consejo Superior.

En conclusión, el Vocero ante la UNEES, afirmó que la movilización estudiantil seguirá con una agenda local de actividades como círculos de estudio y espacios de debate sobre la situación actual de la universidad. Asimismo, confirmó la **participación de la UPTC en la marcha carnaval que se realizará el próximo 31 de octubre en todo el país**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
