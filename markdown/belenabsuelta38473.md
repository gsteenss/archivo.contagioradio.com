Title: Belén, encarcelada dos años por abortar fue absuelta por Corte de Argentina
Date: 2017-03-28 15:10
Category: DDHH, Mujer
Tags: aborto, Argentina, Belén, Feministas, mujeres
Slug: belenabsuelta38473
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/belen_4_caja.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Anfibia] 

###### [28 Mar. 2017] 

Luego de dos años de prisión **Belén, una joven Argentina de 27 años, acusada de abortar, recuperó su libertad luego de la decisión de absolución** dada por la Corte Suprema de Justicia de la provincia de Tucumán. Para los magistrados cabía el “beneficio de la duda” en este caso.

El caso fue revisado, luego de haberle dado a Belén 8 años de cárcel por encontrarla **“culpable del homicidio contra su hijo recién nacido”** reza la primera sentencia, situación por la que se le endilgó **el delito de homicidio agravado.**

La historia se remonta al mes de marzo del año 2014, en el que Belén (nombre ficticio para proteger su identidad), llegó al Hospital Avellaneda acompañada por su mamá, para que la revisarán por un fuerte dolor de vientre.

**“El personal médico la atiende, la medica para el dolor. Posteriormente Belén se dirige al baño y sufre un aborto espontáneo** que luego hace que se desmaye. Por este hecho el personal médico la denunció” aseguró Melina Sánchez, politóloga, feminista e integrante del movimiento Ni Una Menos en Argentina. Le puede interesar: [El mundo gritó ¡No más violencia de género!](https://archivo.contagioradio.com/movilizaciones-en-el-mundo-el-8-de-marzo/)

Además, relata Melina, que **cuando Belén vuelve en sí, en el Hospital había un personal oficial en su habitación y le habían abierto una causa por aborto**, que se convirtió en homicidio agravado. Le puede interesar: [Las mujeres ecuatorianas y el derecho a decidir sobre su sexualidad](https://archivo.contagioradio.com/mujeresviolenciaabortoecuador/)

A Belén no solo la culparon en su momento por haber asesinado “cuando fue un aborto espontáneo” recalca Melina, sino que **además también le incluyeron la comisión de alevosía de esta situación.**

“En este caso pudimos ver la complicidad de las instituciones médicas con las fuerzas de seguridad y el poder judicial. **Primero se viola el derecho al secreto profesional y además se le inventa una causa”** dijo Melina. Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

Después de 2 años de litigio y movilización, **Belén logró recuperar su libertad “después de mucho pelearla,** no solo en la cuestión jurídica sino también de la cuestión de las calles, de las organizaciones, hoy tenemos la respuesta de la Corte Suprema de Tucumán” contó Melina.

La Corte además dentro de su fallo recomendó **“se capacite a los profesionales y trabajadores de la salud para que no criminalicen ni denuncien casos como éste”.**

En la actualidad, Argentina no permite, de manera legal que una mujer realice la interrupción voluntaria del embarazo, a menos que se reconozca que existe un peligro para la vida de la madre o de haber existido una violación.

“En Argentina con el cambio de Gobierno estamos sufriendo un retroceso y una avanzada de la derecha frente a las concepciones conservadoras que se tienen con respecto a las mujeres (…). **la derecha avanza pero el feminismo jamás retrocede (…) Seguiremos luchando hasta que todas seamos libres” concluyó Melina. **Le puede interesar:[ Así fue el Paro de las Mujeres en el mundo](https://archivo.contagioradio.com/avanza-el-paro-de-las-mujeres-en-el-mundo/)

<iframe id="audio_17814750" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17814750_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
