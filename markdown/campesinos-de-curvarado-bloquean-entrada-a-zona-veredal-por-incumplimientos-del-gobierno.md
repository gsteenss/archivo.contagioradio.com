Title: Campesinos de Curvaradó bloquean entrada a Zona Veredal por incumplimientos del gobierno
Date: 2017-05-31 16:07
Category: DDHH, Nacional
Tags: Caracoli, Zona Veredal la Florida
Slug: campesinos-de-curvarado-bloquean-entrada-a-zona-veredal-por-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/curvarado-e1473650667277.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 May 2017]

En el territorio colectivo de comunidades negras del Curvaradó, en Chocó, donde se encuentra la Zona Veredal Transitoria de Normalización “La Florida” un grupo de personas, en su mayoría mujeres, **decidieron bloquear la entrada de insumos y materiales de construcción puesto que el gobierno se comprometió con la construcción de una carretera**, en el marco del acuerdo para que se pudiera ubicar allí la zona veredal.

Según los habitantes, desde que se hizo un primer contacto con la comunidad por parte delos encargados de adecuar las Zonas Veredales, se hizo un acuerdo verbal para adecuar tanques de almacenamiento de agua y **una carretera que llegara hasta la Zona Humanitaria “Caracolí”,** esto como contraprestación a la disposición del terreno para la construcción de la Zona Veredal. Le puede interesar: ["Unidad de Tierras mete un mico a la Ley 1448"](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/)

A pesar de que los y las integrantes de las **FARC llevan cerca de 3 meses asentados en el territorio y los trabajos de adecuación llevan un 50% completado**, la carretera no ha sido una de las labores y ni siquiera se han realizado trabajos de adecuación del terreno. Una vía de acceso es considerada de vital importancia para los pobladores, dado que sería la posibilidad de comercializar sus productos que son el sustento de más de 30 familias que habitan el sector.

Por eso, desde la mañana de este miércoles 31 de Mayo, un grupo de personas decidieron impedir la entrada de maquinaria que realiza los trabajos de adecuación, hasta que el gobierno y el MMV cumplan lo acordado. **De no llegarse a un acuerdo la comunidad afirma que impedirá el ingreso de trabajadores**. Le puede interesar: ["Asesinan a Rulber Santana, guerrillero indultado de las FARC-EP"](https://archivo.contagioradio.com/asesinan-rulber-santana-guerrillero-indultado-de-las-farc-ep/)

Una de las líderes de la región afirmó que hoy hicieron presencia dos personas, uno de ellos se identificó como Jaison Córdoba, delegado del Alto Comisionado para la paz, quien se comprometió a elevar un oficio manifestando las exigencias de la comunidad.

Sin embargo se desconoce alguna respuesta por parte del MMV o de la oficina del Alto Comisionado, razón por la que las comunidades se mantienen en su decisión y anuncian que no permitirán la entrada de personal hasta tener una respuesta real a la problemática que plantean.

###### Reciba toda la información de Contagio Radio en **[su correo](http://bit.ly/1nvAO4u) **o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por **[Contagio Radio](http://bit.ly/1ICYhVU).** 
