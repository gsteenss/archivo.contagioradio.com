Title: Atentan contra líder social de Aheramigua en Bogotá
Date: 2018-02-15 14:12
Category: DDHH, Nacional
Tags: Aheramigua, asesinatos de líderes sociales, defensores de derechos humanos, lideres sociales, violencia contra líderes sociales
Slug: atentan-contra-la-vida-de-un-lider-social-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/atentado-e1518713869698.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: T13] 

###### [15 Feb 2018] 

La Asociación de Hermandades Agroecológicas y Mineras de Guamocó, AHERAMIGUA, denunció que en la noche del 14 de febrero el líder social Germán Espinel **sufrió un atentado** en el barrio Alameda de la localidad de Ciudad Bolívar en Bogotá. El líder sobrevivió gracias a que vestía su chaleco antibalas. Sin embargo el riesgo permanece por las reiteradas amenazas contra su vida.

En la denuncia, la Asociación indicó que Espinel ha desarrollado actividades de interlocución para la **defensa de los derechos humanos** y había denunciado la presencia de grupos armados en diferentes territorios. “Durante el mes de abril de 2016 en el municipio de Santa Rosa, Bolívar, cuando realizaba acompañamiento a los compañeros de terreno de la organización”, había sido víctima de seguimientos e intimidaciones.

De acuerdo con Mauricio García, presidente de AHERAMIGUA, “a eso de las 9:30 pm Germán sale de su lugar de residencia hacia una tienda a comprar los enseres del desayuno, lo abordan dos hombres en una moto y **le propinan dos disparos**, uno en el pecho y otro en la espalda”. A lo que el líder intenta correr, “le gritan: eso le pasa por sapo”.

### **El líder social ha denunciado el control del paramilitarismo ** 

García confirmó que en repetidas ocasiones los integrantes de la organización han recibido **diferentes amenazas**. Esto en virtud de las denuncias que han hecho sobre, por ejemplo, “la retoma paramilitar que está ocurriendo en el Bajo Cauca antioqueño” y los asesinatos que han ocurrido allí. (Le puede interesar:["Paramilitares salieron de El Bagre en Antioquia, pero las FFMM no los han combatido"](https://archivo.contagioradio.com/43248/))

También han denunciado los hechos de violencia contra los ex combatientes de las FARC, que se encuentran en proceso de reincorporación, y que hay un corredor del cual tienen el **control los paramilitares** y es el Bajo Cauca y el sur de Bolívar. Creen, desde la Asociación que el atentado que se presentó contra Germán Espinel responde a que, “hace unos tres días denunciamos los asesinatos que se vienen presentando en el Bajo Cauca por los grupos paramilitares y el ELN”.

### **No hay certeza para realizar acciones que protejan la vida de los defensores** 

Luego del atentado, “se pusieron todas las alertas y se informó a la Policía de lo ocurrido”. Sin embargo, García manifestó que, a pesar de que se realizan las denuncias, **“no pasa nada”**.  Esto teniendo en cuenta que “nadie se pone en los zapatos de los defensores y las necesidades de seguridad que requerimos”.

Para la Asociación es de especial preocupación que, desde el Gobierno Nacional, **“no hay voluntad política** para darle solución a la situación de seguridad que viven los defensores de derechos humanos.” Luego de que solamente en enero hayan sido asesinados 27 líderes sociales, “no hay informes, investigaciones y acciones que dimensionen la gravedad de la situación”. (Le puede interesar:["Aumentan las amenazas contra líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Finalmente, la Asociación le exigió al Gobierno Nacional “llevar a cabo las medidas pertinentes que brinden **garantías y efectividad de los Derechos Humanos**, el Derecho a la Vida, la integridad personal y psicológica, al trabajo, la libre movilidad, y al acceso a la justicia de los miembros de Aheramigua”.

<iframe id="audio_23807282" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23807282_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
