Title: Los pueblos de Latinoamérica se unen contra el Neoliberlalismo y por la democracia
Date: 2017-05-03 21:31
Category: El mundo, Nacional
Tags: CLOC, neoliberalismo
Slug: los-pueblos-de-latinoamerica-se-unen-contra-el-neoliberlalismo-y-por-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cloc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cloc Vía Campesina] 

###### [03 May 2017] 

Más de 30 organizaciones y 200 delegados de Latinoamérica se reunieron en Santandersito, Cundinamarca para hacer el lanzamiento de la **Jornada Continental por la Democracia y en contra del Neoliberalismo,** y construir una agenda común que permita esbozar herramientas o iniciativas que afronten las dinámicas del sistema neoliberal.

Esta iniciativa, surgió a partir del análisis de otras experiencias que se habían dado en el pasado, como la Campaña Continental contra el Alza, en el año 2005, y que sirvieron de ejemplo para afrontar las transformaciones políticas actuales de **Latinoamérica, en donde han recobrado fuerza gobiernos de derecha que están profundizando las brechas sociales.**

Razón por la cual, **organizaciones nacionales y regionales del continente latinoamericano comenzaron a articularse** para sumar esfuerzos y construir agendas de movilización en torno a temas como la soberanía alimentaria o las resistencias sociales.

Frente a la democracia, las apuestas de esta iniciativa se enfocan en la **restructuración de las instituciones gubernamentales que permitan el fortalecimiento del poder popular**, Montón afirmó que otra de las apuestas tiene que ver con la concentración de la tierra, “es necesaria una reforma agraria integral y popular que vuelva a poner la tierra en función de los pueblos y que genere una democracia sustentable”.

De acuerdo con Diego Montón, coordinador de la Cloc Vía Campesina, este encuentro les ha permitido “conversar y trabajar sobre las expectativas de las jornadas que tienen que ver con **construir una pedagogía de la unidad y en la movilización y en la discusión sobre qué democracia se quiere para el continente**”.

El próximo encuentro se realizará en noviembre, en Montevideo Uruguay, en donde se espera que las organizaciones ratifiquen su compromiso con la democracia y se logre generar aportes en términos de la construcción de medios alternativos de comunicación y otras formas de dialogar entre las comunidades.

<iframe id="audio_18487000" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18487000_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
