Title: Desde Nariño presentan Pacto por la Vida y la Paz
Date: 2020-10-29 18:11
Author: AdminContagio
Category: Nacional
Tags: nariño, Pacto por la Vida y la Paz
Slug: desde-narino-presentan-pacto-por-la-vida-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Pacto-por-la-Vida-y-la-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Como una forma de fortalecer la defensa de la vida, la paz, el territorio y los Derechos Humanos, **este 28 y 29 de octubre en San Andrés de Tumaco, se estará realizando el lanzamiento del Pacto por la Vida y la Paz para el departamento de Nariño**, ante la creciente ola de violencia, masacres, asesinatos, desapariciones, desplazamientos, el abuso de la Fuerza Pública en los territorios y

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 10 de septiembre ya se había convocado a la suscripción de un "Pacto por la vida y a la paz" ante las continuas agresiones en contra de las comunidades y el territorio en la región del Pacífico y Suroccidente Colombiano. (Le puede interesar: [Organizaciones sociales convocaron a un Pacto por la Vida y la Paz](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las organizaciones convocantes, la firma de este primer pacto también fue motivada por el incumplimiento del acuerdo de paz y la negación al diálogo con el ELN por parte del gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora, este nuevo Pacto busca avanzar en una agenda regional del Acuerdo Humanitario de Paz con los diferentes actores del territorio y fortalecer los lazos de reconciliación para consolidar un país libre de guerra.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Quienes convocamos la suscripción de este pacto, animaremos e  
> impulsaremos acciones, encuentros y reflexiones para ampliar la firma de este Pacto por la Vida y la Paz entre autoridades locales y regionales, autoridades indígenas y afrodescendientes, campesinos, movimientos sociales, sectores económicos, gremios, académicos y ciudadanía en general.
>
> <cite>Comunicado</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Antes del lanzamiento y posterior firma del Pacto, se socializarán cada uno de los mandatos que conformarán el Pacto por la Vida y la Paz. Además, durante el evento, se presentará el foro «Liderazgo de la mujer en los movimientos sociales», que estará orientado al trabajo que realizan las lideresas en los territorios. <mark>(También lea: [Es asesinado Euloquio Pascal líder Awá, en Nariño](https://archivo.contagioradio.com/es-asesinado-euloquio-pascal-lider-awa-en-narino/))</mark>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También se desarrollará el espacio «Hagamos memoria: presentación de las experiencias del Paro Cívico del Chocó, la del Paro Cívico de Buenaventura y el Comité Cívico de Tumaco Unidos por la Vida», que permitirá un diálogo sobre el camino que se debe recorrer para llegar a un Acuerdo Humanitario.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
