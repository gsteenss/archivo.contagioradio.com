Title: "30 años esperando justicia" Homenaje audiovisual a las victimas del Palacio de Justicia
Date: 2015-10-29 19:44
Category: Nacional, Sin Olvido
Tags: 30 años buscando justicia, Carmenza Gómez, Consuelo Luzardo, Coral, Desaparecidos palacio de justicia, Diana Ángel, Ernesto Benjumea, Homenaje actores y actrices, Julio Correal, Julio Sánchez, Lully Bosa, Luz Estela Luengas, Nicolás Montero, Norida Rodríguez, Palacio de Justicia, ristina Umaña, Simona Sanchez, Toto Vega
Slug: homenaje-actores-palacio-de-justicia-homenaje-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/actores-palacio-de-justicia-homenaje1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [29 Oct 2015]

Con la participación de  familiares de los desaparecidos del Palacio de Justicia, familiares de los desaparecidos de Ayotzinapa y varios actores y actrices de la televisión y el cine colombiano, se realizó en Bogotá la premier del audiovisual "**30 años esperando justicia**", adportas de cumplirse 30 años del fatídico suceso.

Los actores y actrices; **Cristina Umaña, Carmenza Gómez, Julio Sánchez, Diana Ángel, Consuelo Luzardo, Julio Correal, ,  Lully Bosa, Simona Sanchez, Ernesto Benjumea, Luz Estela Luengas, Nórida Rodríguez, Toto Vega, Nicolás Montero,** asumieron la personalidad de cada uno de las y los desaparecidos del palacio de Justicia, bajo la dirección de Ricardo Coral y la producción del Colectivo de Abogados José Alvear Restrepo.

Con la frase "Un homenaje a las víctimas" los artistas concluyeron la presentación de la pieza audiovisual, que por unos minutos devolvió a la vida a cada uno de los desaparecidos. En el diálogo posterior a la proyeción, hubo espacio para escuchar la voz de los artistas que expresaron la importancia y el compromiso que debe asumir la sociedad en la búsqueda de verdad en el caso del Palacio de Justicia.

En respuesta las familias expresaron su agradecimiento a los actores y actrices del vídeo, poniendo de manifiesto la importancia  de su contribución y el aporte a construcción de la memoria, en un país que 30 años después no conoce la verdad de lo sucedido durante el 6 y 7 de noviembre de 1985.

\[embed\]https://www.youtube.com/watch?v=fMAHybZQXDo\[/embed\]
