Title: ZRC "Perla Amazónica" y corregimiento Piñuña blanco rechazan explotación sísmica del bloque petrolero
Date: 2015-02-16 21:11
Author: CtgAdm
Category: Comunidad
Tags: Conpaz, Corregimiento Piñuña Blanco, Explotación petrolera, explotación sísmica en bloque petrolero, Putumayo, zonas de reserva campesina
Slug: zrc-perla-amazonica-y-corregimiento-pinuna-blanco-rechazan-explotacion-sismica-del-bloque-petrolero
Status: published

El sábado 7 de febrero **organizaciones sociales del Putumayo y CONPAZ (Comunidades construyendo Paz)** se reunieron con las empresas Amerisur y Vector, manifestandoles la oposición a cualquier explotación sísmica y petrolera del Bloque Put 12 en los corregimientos de **Piñuña Blanco y Zona de Reserva Campesina Perla Amazónica de Puerto Asís.**

En la reunión también estuvo presente el enlace Regional para el sector de hidrocarburos del Ministerio del Interior Jorge Alberto Guerrero, a quién se le manifestó también la oposición a dichas explotaciones, quién propuso que la problemática se tratase en la Mesa Regional de Organizaciones Sociales, espacio para dialogar problemáticas de carácter medioambiental. Las organizaciones sociales aceptaron el espacio de discusión, siempre y cuando sea con la máxima celeridad.

Comunicado de las comunidades del corregimiento Piñuña Blanco:

Las comunidades campesinas, indígenas y afrodecendientes del Corregimiento Piñuña Blanco, Representadas en la ASOCIACION SINDICAL AGROECOLOGICA DE TRABAJADORES CAMPESINOS DEL PUTUMAYO (ASIAGRO), ZONA DE RESERVA CAMPESINA PERLA AMAZONICA representada por la ASOCIACION DE DESARROLLO INTEGRAL SOSTENIBLE PERLA AMAZONICA (ADISPA), PUEBLO NASA DEL PUTUMAYO, ASOCIACION DE JUNTAS DE PUERTO ASÍS (ASOJUNTAS), LA ASOCIACION CAMPESINA AGRICOLA DEL PUTUMAYO (ASCAP), LA ASOCIACION CAMPESINA DE TRABAJADORES AGRICOLAS DE PUERTO LEGUIZAMO (ATCAL), ASOCIACION DE TRABAJADORES CAMPESINOS DE MECAYA (ASTRACAM), LA MESA REGIONAL DE ORGANIZACIONES SOCIALES DEL PUTUMAYO, BAJA BOTA CAUCANA Y COFANIA JARDINES DE SUCUMBIOS, RED DE DERECHOS HUMANOS;

Informamos a la opinión pública nacional e internacional que  el día de hoy, sábado 7 de febrero de 2015 reunidos con la **Empresa Amerisur y Vector**, les manifestamos nuestra decisión de rechazar y **oponernos a toda actividad de exploración sísmica y explotación petrolera del Bloque Put 12,** dentro de los territorios del corregimiento Piñuña Blanco y Zona de Reserva Campesina Perla Amazónica de Puerto Asís, y en la Inspección Piñuña Negro, Corregimiento de Puerto Ospina, Inspección la Paya de Puerto Leguízamo, debido a los serios impactos ambientales, culturales  y sociales que éstas empresas y la industria petrolera vienen generando,  esta decisión ha sido tomada en conjunto desde el seno de las comunidades acorde a los planteamientos hechos desde la MESA REGIONAL DE LAS ORGANIZACIONES SOCIALES, en rechazo a la expansión Petrolera.

En dicho espacio hizo presencia el enlace Regional para el sector de hidrocarburos del Ministerio del Interior Jorge Alberto Guerrero, ante quien se manifestó la inconformidad de las comunidades frente al deterioro ambiental que generan las políticas petroleras en este país y se le exigió respeto, garantía, reconocimiento y cumplimiento a la decisión y derechos de las comunidades.

El funcionario por su parte propuso que este tema se discuta en los diálogos instaurados con el gobierno nacional desde el espacio de la Mesa Regional de Organizaciones Sociales por ser una problemática regional.

Desde las organizaciones **aceptamos la propuesta del Funcionario del Ministerio del Interior y esperamos  que  esta discusión se aborde de manera inmediata en el espacio de interlocución con el Gobierno Nacional** donde no solo se aborde la problemática petrolera, si no toda su  relación en violación de Derechos Humano e infracción al Derecho Internacional Humanitario (amenazas, detenciones ilegales, ordenes de capturas, restricción a la libre movilidad, Abuso de Fuerza en las comunidades, Tortura Física Psicológica).

Reiteramos nuestro llamado a la comunidad en general, a la organización a la unidad para que de esta forma podamos rechazar toda la actividad petrolera en nuestra región para que promovamos la **DEFENSA DE NUESTRA VIDA, TERRITORIO Y BIODIVERSIDAD.**
