Title: Organizaciones sociales exigen compromisos del gobierno para el desmonte del paramilitarismo
Date: 2018-04-17 12:43
Category: DDHH, Política
Tags: Comisión Nacional de Garantías, defensores de derechos humanos, desmonte de paramilitares, Gobierno Nacional, Paramilitarismo
Slug: comunidades-asediadas-por-el-paramilitarismo-solicitaron-su-desmonte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [17 Abr 2018] 

Organizaciones sociales, que integran el Proceso Nacional de Garantías, se reunieron con autoridades del Gobierno en el marco de la Mesa Nacional de Garantías donde le exigieron al Estado colombiano el **desmonte del paramilitarismo.** Es así como esperan que la nación cuente con un mecanismo para asegurar la vida de los defensores de derechos humanos, de las y los líderes sociales y de las comunidades que habitan los territorios que han sido copados por estos grupos.

De acuerdo con la Fundación Comité de Solidaridad con los Presos Políticos, la Mesa Nacional de Garantía, que es el espacio de interlocución entre la sociedad civil y el Gobierno Nacional, incluyó **por primera vez el fenómeno del paramilitarismo** dentro de sus discusiones. Allí presentaron sus propuestas para que sean adoptadas por el Gobierno como compromisos para desmontar dicho fenómeno.

### **Gobierno Nacional debe fortalecer y consolidar su presencia en los territorios** 

Una de las principales propuestas de las organizaciones sociales es que el Estado consolide su presencia “integral y efectiva en las zonas rurales, especialmente las más afectadas por el conflicto armado”. Con esto buscan que se **fortalezca el trabajo de las juntas de acción comunal,** los consejos comunitarios y las autoridades indígenas con sus mecanismos de protección como lo son las guardias indígenas y las guardias cimarronas.

Adicionalmente, las organizaciones le pidieron al Gobierno que implemente las medidas establecidas en el Acuerdo Final suscrito entre las FARC y el Estado colombiano como lo son los **PDET, los PENIS y los PISDA** para así priorizar los cambios estructurales en las zonas rurales. Esto, debe ir acompañado de la veeduría y monitoreo efectivo de la comunidad internacional. (Le puede interesar:["Ex paramilitares que se comprometan con la verdad podrán ingresar a la JEP"](https://archivo.contagioradio.com/exparamilitares-que-se-comprometan-con-la-verdad-podran-ingresar-a-jep/))

**Para proteger a las comunidades, Estado debe desmontar el paramilitarismo**

Teniendo en cuenta el aumento en el número de asesinatos de líderes sociales y la desprotección en la que se **encuentran las comunidades rurales**, en la mesa se discutió la necesidad de que el Estado se comprometa "a la pronta y efectiva puesta en marcha de los mecanismos para el desmantelamiento del paramilitarismo”.

Para esto, en el Acuerdo Final hay **unos mecanismos consagrados** para la tipificación penal del delito de paramilitarismo junto con los mecanismos para luchar contra la corrupción y así permitir el desarrollo de los compromisos para combatir los grupos locales.

En lo que tiene que ver con el trabajo de entes como la Procuraduría General de la Nación, las organizaciones le exigieron **que investigue y adopte medidas** “adecuadas en relación con las demoras en la implementación del Acuerdo Final”. Le pidieron además que aplique las sanciones disciplinarias “para los funcionarios que profieran estigmatizaciones a líderes y movimientos sociales”. (Le puede interesar:["Las amenazas paramilitares son reales y comprobables": Unión Patriótica"](https://archivo.contagioradio.com/las-amenazas-paramilitares-son-reales-y-comprobables-union-patriotica/))

### **Terceros con responsabilidad en actuar de los paramilitares deben ser investigados** 

Un tema importante para sancionar a quienes han cometido delitos relacionados con el paramilitarismo, es la **investigación de terceros** como los son funcionarios estatales no armados, militares de la Fuerza Pública o empresarios. El movimiento social le pidió al Estado que, la Unidad para el Desmonte del Paramilitarismo de la Fiscalía General de la Nación, “efectúe de manera ágil y efectiva las investigaciones correspondientes”.

Teniendo en cuenta que uno de los problemas que acrecienta el fenómeno del paramilitarismo es el narcotráfico, las organizaciones expusieron la necesidad de que se implementen los compromisos con la **política relacionada con los cultivos de uso ilícito.** Esto con la finalidad de que los campesinos puedan integrarse a la economía legal en el largo plazo.

### **Información relacionada con violencia contra defensores debe ser pública** 

Para las organizaciones es importante que la información que corresponde a los ataques contra defensores de derechos humanos **sea pública y sistematizada** “para esclarecer los móviles que permitan alcanzar a quiénes se benefician con este tipo de crímenes”. Esto, demostraría la determinación del Gobierno y del país en general para acabar con la violencia y alcanzar la paz.

Finalmente, las organizaciones le pidieron a la Comisión Nacional de Garantías que tenga voluntad para avanzar en el diseño e implementación de medidas que permitan el desmonte del paramilitarismo”. Cabe resaltar que el Gobierno Nacional, en esta reunión, consideró a los grupos sucesores del paramilitarismo **como crimen organizado**, “desnaturalizando el carácter represivo y de control social que estos grupos ejercen”.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
