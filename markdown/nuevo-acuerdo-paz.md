Title: Debate en El ecléctico: El nuevo acuerdo
Date: 2016-11-21 15:37
Category: El Eclectico
Tags: colombia, debate, nuevo acuerdo, paz
Slug: nuevo-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/506521_1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti_flag-e1478191764857.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Semana.com 

##### 21 Nov 2016 

Conscientes de la actual coyuntura por la que atraviesa Colombia, fueron los partidarios del Sí y los del No los que estuvieron en El Ecléctico exhibiendo sus reparos y aprobaciones frente al nuevo acuerdo de paz firmado entre el Gobierno Nacional y las Farc. Le puede interesar: [La firma del nuevo acuerdo, nueva esperanza para las víctimas](https://archivo.contagioradio.com/la-firma-del-nuevo-acuerdo-una-nueva-esperanza-para-las-victimas/).

Ángela María Robledo, Representante a la Cámara por la Alianza Verde y vicepresidenta de la Comisión de Paz del Congreso de la Republica, dos representantes del Partido Conservador, un militante de Paz a la Calle –iniciativa ciudadana que se llevara todas las miradas por el revolucionario éxito de los Cabildos Abiertos- y un representante de la iniciativa de paz de la Universidad de los Andes; ellos abarcaron la temática propuesta desde cada uno de sus grupos e intereses.

¿Goza el Congreso de la República de la suficiente legitimidad para refrendar el acuerdo de paz más importante en la historia de Colombia? ¿Es mejor este acuerdo que el anterior? ¿Se escuchó a los promotores del No en la renegociación del nuevo acuerdo? Éstas y otras preguntas fueron respondidas desde varios enfoques: escúchelos en nuestro debate de hoy en El Ecléctico.

<iframe id="audio_14941970" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14941970_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
