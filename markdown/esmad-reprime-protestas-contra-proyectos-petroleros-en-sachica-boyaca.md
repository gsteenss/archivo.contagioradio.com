Title: ESMAD reprime protestas contra proyectos petroleros en Sáchica, Boyacá
Date: 2018-06-25 15:49
Category: Ambiente, Nacional
Tags: Boyacá, Geofízika Torún, Sáchica
Slug: esmad-reprime-protestas-contra-proyectos-petroleros-en-sachica-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-minga-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Wikipedia] 

###### [25 Jun 2018] 

La Comunidad de Sáchica, Boyacá, denunció una fuerte arremetida por parte del ESMAD ayer, luego de que habitantes de ese municipio se opusieran a la intromisión que viene adelantando la empresa multinacional Geofízika Torún en el territorio. De acuerdo con los pobladores, uno de los trabajadores de esta empresa incluso arremetió contra **una persona causándole heridas con arma blanca, mientras que habrían otras personas con afectaciones por la inhalación de gases lacrimógenos. **

Según testimonio de una de las habitantes, el domingo en horas de la tarde llegaron los vehículos de la empresa al pueblo, cuando fueron increpados por personas de la comunidad, los trabajadores reaccionaron con violencia y según la testigo, uno de los empleados de **esta multinacional habría agredido un arma blanca a uno de los concejales de Sáchica que se encontraba en el lugar**.

Posteriormente, la testigo afirmó que los pobladores iniciaron un plantón pacífico en frente del lugar en donde habían ingresado los trabajadores, **“lo único que quería la comunidad de Sáchica era que ellos se fueran porque no son bien recibidos”**. Tiempo después llegó el Escuadrón Móvil Antidisturbios, que según los habitantes habrían dejado varias personas heridas, entre quienes se encuentran menores de edad, arremetiendo con gases lacrimógenos.

### **Las respuestas de las instituciones**

Los habitantes han denunciado que esta empresa tendría **presencia en 13 municipios del departamento en donde estarían realizando los estudios** para levar a cabo la explotación sísmica. Sin embargo, el gobernador Amaya habría emitido una orden de frenar tales estudios en el territorio hasta que se realice la audiencia con presencia de las comunidades el próximo 29 de junio en Chinquira.

En dicho encuentro estará presente el director de la ANLA, los alcaldes de los municipios y el gobernador del departamento para una socialización de los diferentes proyectos mineros y sísmicos que podrían desarrollarse en el territorio. (Le puede interesar: ["Existe Fracking en Boyacá"](https://archivo.contagioradio.com/existe-fracking-en-boyaca/))

Frente a esa situación los habitantes han manifestado no solo su rechazo a la realización de este tipo de actividades en los municipios, también las afectaciones que podrían ocasionar, “**La comunidad y toda nuestra provincia del Alto Ricaurte y del occidente, estamos muy preocupados** porque nos quieren hacer los estudios sísmicos en una región que es escaza de agua y en donde la vocación es agropecuaria y turística”.

<iframe id="audio_26723749" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26723749_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
