Title: Ciénaga Grande de Santa Marta a punto de desaparecer
Date: 2016-04-18 15:49
Category: Ambiente, Entrevistas
Tags: Change.org, Ciénaga grande Santa Marta, Ramsar
Slug: cienaga-grande-de-santa-marta-a-punto-de-desaparecer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cienaga-Grande-de-Santa-Marta-e1461004237548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Unal] 

###### [18 Abril 2016]

El bajo nivel de las aguas del Río Magdalena y el uso de los ríos que nacen en la Sierra Nevada para implementación de distritos de riego destinados a monocultivos de palma y banano, sumado a los efectos de la construcción de la Vía de la Prosperidad y a la ineficiencia de las obras hidráulicas de la vía entre Barranquilla y Santa Marta, tiene en alto riesgo de desaparición al complejo de humedales de la **Ciénaga Grande de la Magdalena, Reserva de Biósfera y Humedal Ramsar de importancia internacional.**

**"La situación de la Ciénaga es crónica y delicada, estamos entrando al cuarto año de un déficit hídrico,** de una sequía muy prolonga que ha magnificado los efectos de la falta de agua dulce", asegura Sandra Vilardy, profesora de la Universidad del Magdalena, denunciando el mal manejo del recurso hídrico y la irresponsabilidad ambiental de los proyectos agroindustriales**.**

Este complejo de humedales se alimenta del Río Magdalena y de los ríos que bajan de la Sierra Nevada, pero **éstos están siendo usados casi de manera exclusiva para los distritos de riego y los cultivos de palma y banano**, como lo han denunciado los campesinos de la zona, sin dejar caudales ecológicos que lleguen a la Ciénaga.

Esa situación ha sido denunciada incluso por Parques Nacionales Naturales de Colombia, pero las autoridades continúan permitiendo que se siga desviando el agua que es vital para el funcionamiento de la Ciénaga. "**Teniendo en cuenta la sequía que se vive, uno esperaría que se realizara una regulación de caudales,** pero no ha pasado nada", asegura la docente.

Además de tratarse de una crisis ambiental, estos bajos niveles de agua en la ecorregión de la Ciénaga Grande han impedido que los pobladores de zonas como Trojas de Cataca o Cerro de San Antonio, cuenten con agua dulce para su consumo. **El delta del río Aracataca hoy es un canal contaminado, aguas podridas y con vegetación muerta** y en Cerro ya no hay agua dulce, tampoco pesca y los pobladores han tenido que enterrar los peces muertos porque representaban un problema de salud pública.

El impacto de estas actividades nocivas para el ecosistema de ciénaga ha sido tal que en tan solo 10 años la producción de peces ha bajado de 27 mil toneladas anuales a 1725 toneladas anuales, según un estudio de científicos alemanes, lo que significa **una reducción de más del 90% en la producción pesquera, sustento de más de 5000 familias** del departamento.

**"La carretera es un problema adicional que afectaría el** **intercambio** **de agua con el mar, alterando el  funcionamiento de la ciénaga.** Además la vía de 'La Prosperidad', paralela al río Magdalena también desconoce la identidad de humedad de esta zona", asegura Sandra Vilardy, quien agrega que son múltiples las presiones que existen sobre esta reserva natural y que no han sido atendidos por las autoridades ambientales debido a que no hay un plan de manejo, no hay liderazgo efectivo de las instituciones, y no hay una buena gestión del recurso hídrico.

Por lo anterior, por medio del portal [Change.org,](https://www.change.org/p/ministerio-del-medio-ambiente-salvemos-a-la-ci%C3%A9naga-grande-de-santa-marta) se solicita al presidente Juan Manuel Santos, una acción urgente e integral para atender la situación de la Ciénaga Grande, entre las cuales se exige que **este complejo de humedales sea incluido en la lista Montreaux, donde se encuentran los humedales Ramsar en peligro.**

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
