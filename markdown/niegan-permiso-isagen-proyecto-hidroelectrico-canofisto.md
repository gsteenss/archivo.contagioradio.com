Title: Niegan permiso a ISAGEN para proyecto hidroeléctrico Cañofisto
Date: 2017-01-18 11:43
Category: Ambiente, Nacional
Tags: ANLA, Hidroituango, megaproyectos
Slug: niegan-permiso-isagen-proyecto-hidroelectrico-canofisto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Hidroituango-EPM-e1484757457706.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Hidroituango 

###### [18 Ene 2017] 

[La Autoridad Nacional de Licencias Ambientales, ANLA negó el permiso a ISAGEN para construir el proyecto hidroeléctrico Cañafisto, asegurando que se pone en riesgo el bosque seco tropical, que ya se encuentra en peligro por la construcción de otras represas como la de Hidroituango, ubicada a 40 kilómetros de Cañofisto.]

[Aunque permanece vigente la amenaza hacia el bosque seco tropical por el avance del proyecto en Ituango de Empresas Públicas de Medellín, la comunidad aplaude la decisión de la ANLA, pues lo ven como un paso hacia **el reconocimiento de los conflictos socio-ambientales que generan este tipo de megaproyectos.**]

[Para Juan Pablo Soler, integrante del Movimiento Ríos Vivos, “En términos ambientales es muy positivo el anuncio de la ANLA”, y agrega que con esta decisión se evita la inundación de más de 5 mil hectáreas de bosque ubicadas en 16 municipios del departamento (Pintada, Fredonia, Venecia, Titiribí, Armenia Mantequilla, Ebéjico, Támesis, Jericó, Tarso, Salgar, Concordia, Betulia, Anzá, Santa Fe de Antioquia, Olaya y Sopetrán). A su vez, impide el desplazamiento forzado de las comunidades ubicadas a orillas del Cañón del Río Cauca.]

[La medida se toma, pese a que **ISAGEN había presentado un recurso de** **reposición** donde indicaba que el bosque estaba muy degradado y que el ecosistema se fortalecería por medio del plan de ambiental que proponían.]

### [¿Por qué se debe proteger el Bosque seco tropical?] 

[Este tipo de bosque, es un ecosistema del cual **solo queda el 8% de las 9 millones de hectáreas que existían en Colombia**, según reportó  el libro Bio Diversidad 2015 del Instituto Alexander Von Humboldt.]

["Teniendo en cuenta que el Bosque Seco Tropical constituye un porcentaje muy pobre de las áreas del Sistema Nacional de Áreas Protegidas (…) es imperante establecer estrategias integrales para su gestión", dice la publicación, que agrega que "la mayoría de sus áreas están expuestas", cuando allí viven aproximadamente **2.600 especies de plantas, al menos 230 de aves, 60 de mamíferos, de las cuales 119 especies animales son endémicas.**]

[Puntualmente en esta zona de Antioquia, especies endémicas como la ranita cohete, la rana venenosa, tintín antioqueño, serpiente ciega, las aves cucarachero paisa y atrapamoscas apical, el tití gris, se encuentran en peligro por este tipo de megaproyectos.]

### [¿Por qué no se aplica la misma medida a Hidroituango?] 

[**Hidroituango se encuentra ubicado a tan solo 40 kilómetros de lo que hubiese sido el proyecto Cañofisto,** y también se desarrolla en el]Cañón del Río Cauca, más sin embargo no ha sido posible que se cancele la construcción de la hidroeléctrica, **pese a los daños sociales y ambientales ya causados**

Sin embargo, teniendo en cuenta la reciente decisión sobre Cañofisto, las comunidades y el Movimiento Ríos Vivos esperan que la labor de protección de la ANLA se siga fortaleciendo, por lo que [estarán a la espera de que esas mismas medidas se instauren frente a Hidroituango que actualmente tala cerca de 4.500 hectáreas de bosque seco tropical. [(Le puede interesar: Empresas Públicas de Medellín, EPM, inició la tala de 4.500 hectáreas de bosque seco tropical)](https://archivo.contagioradio.com/epm-inicio-tala-de-4-500-hectareas-de-bosque-seco-tropical/)]

<iframe id="audio_16560704" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16560704_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
