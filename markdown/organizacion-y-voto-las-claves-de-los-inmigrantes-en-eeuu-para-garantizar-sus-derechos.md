Title: Organización y voto, las claves de los inmigrantes en EEUU para garantizar sus derechos
Date: 2016-02-18 13:13
Category: DDHH, El mundo
Tags: Florida Coalition Inmigrant, inmigrantes estados unidos
Slug: organizacion-y-voto-las-claves-de-los-inmigrantes-en-eeuu-para-garantizar-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Inmigrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Toda Noticia] 

<iframe src="http://www.ivoox.com/player_ek_10486854_2_1.html?data=kpWhmpuceZWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbDmyMbby9%2FFp8qZpJiSpJjSb9qf19Th0YqWh4zgwtiYxdHFusbnjMnSjdHTt4zdz9LWydfFstXZ1JDS0JCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jose Luis Morantes] 

###### [18 Feb 2016] 

Diversas organizaciones de migrantes en Estados Unidos han desarrollado una serie de acciones como el boicot (no realizar compras), plantones, no trabajar, incluso jornadas de oración por las almas de quienes se oponen a la legalización de millones de migrantes apelando a la fe que profesan. Una de las actividades es la denominada “un día sin un inmigrante”, todo ello en el marco de las elecciones presidenciales.

Durante esta semana se han realizado varias actividades, según José Luis Morantes, de Florida Coalition Immigrant, lo que ha favorecido las protestas pacíficas, es que en Estados Unidos sigue respetándose el derecho de protestar y el momento es muy especial. Por ello las organizaciones de inmigrantes afirman que es necesario organizarse y evidenciar las consecuencias que puede tener dar el voto por uno u otro candidato.

Tanto en La Florida como en varios estados de Estados Unidos, se han incrementado e institucionalizado las leyes contra la población migrante y el “tranque” viene de parte de diversos congresistas integrantes del Partido Republicano como Marco Rubio y Donald Trump en medio de la contienda electoral y la campaña por las presidenciales en ese país.

Una de las leyes es la HB 675 propuesta por el republicano Larry Metz obligaría a los gobiernos locales de Florida a reportar a las autoridades migratorias cuando tengan en custodia de la Policía a un inmigrante indocumentado. Con la medida los gobiernos recibirían multas de hasta cinco mil dólares diarios si mantienen políticas tipo santuario negándose a cooperar con las autoridades federales.

Según la página [consultasmigratorias.co](http://consultasmigratorias.co), que cita al Centro Hispano Pew, 40,4 millones de inmigrantes viven en Estados Unidos, lo que representa 13% de la población. Más de 18 millones son ciudadanos naturalizados, 11 millones son residentes permanentes o temporales con permiso legal y más de 11 millones están en el país sin autorización.

Vigilias, visitas a legisladores y plantones serán las actividades que durante este mes se realicen en varios estados para buscar que la política de inmigración avance hacia la garantía de los derechos de todos y no solamente de unos cuantos como lo señala José Luis Marantes de Florida Coalition Inmigrant.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
