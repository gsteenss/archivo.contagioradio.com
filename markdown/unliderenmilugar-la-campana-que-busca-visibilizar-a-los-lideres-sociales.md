Title: #UnLíderEnMiLugar la campaña que busca visibilizar a los líderes sociales
Date: 2019-05-13 13:25
Author: CtgAdm
Category: Líderes sociales, Video
Tags: lideres sociales, youtubers
Slug: unliderenmilugar-la-campana-que-busca-visibilizar-a-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/lideres-sociales-youtubers.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La campaña \#UnLíderEnMiLugar en la que participan diferentes líderes sociales y reconocidos youtubers  busca visibilizar a hombres y mujeres que desde las diferentes regiones de Colombia defienden los derechos de las comunidades y del ambiente.
