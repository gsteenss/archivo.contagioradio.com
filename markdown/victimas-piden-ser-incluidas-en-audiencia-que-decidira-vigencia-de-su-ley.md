Title: Víctimas piden ser incluídas en audiencia que decidirá vigencia de su Ley
Date: 2019-10-04 16:26
Author: CtgAdm
Category: Judicial, Paz
Tags: audiencia pública, Corte Constitucional, Ley 1448, Ley de Víctimas
Slug: victimas-piden-ser-incluidas-en-audiencia-que-decidira-vigencia-de-su-ley
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Verdad-Abierta-e1570224330932.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Verdad Abierta  
] 

El próximo 10 de octubre la **Corte Constitucional realizará una audiencia pública en la que se discutirá la extensión de la Ley 1448 de 2011 o Ley de Víctimas**. Esta normativa se condicionó con un lapso de operación de 10 años, lo que significaría que en julio de 2021 dejaría de existir; sin embargo, víctimas, organizaciones sociales y acompañantes están pidiendo que se renueve el plazo de la misma, y que se escuche sus razones para hacer esta solicitud. (Le puede interesar: ["Los seis obstáculos a la restitución de tierras en Urabá"](https://archivo.contagioradio.com/obstaculos-restitucion-de-tierras/))

### **La Ley no ha desarrollado su propósito y no ha garantizado la participación de las víctimas en la audiencia** 

En una carta firmada por diferentes organizaciones defensoras de derechos humanos y acompañantes de víctimas, se expresan las razones para pedir que la Ley sea prorrogada, y se les tome en cuenta al momento de tomar esta decisión. Adicionalmente, la audiencia surge con ocasión de la demanda de inconstitucionalidad presentada contra la regla que establece el término de vigencia de la Ley, argumentando que es contraria al marco constitucional de la paz establecido en los Actos Legislativos 1 y 2 de 2017.

**Cesar Santoyo, director del Colectivo Sociojurídico Orlando Fals Borda (OFB),** señaló que en estos ocho años en que ha funcionado la Ley, aún no se ha desarrollado a cabalidad su propósito de resarcimiento, reparación ni brindado garantías de no repetición a las víctimas, por lo que están pidiendo una extensión de la misma para cumplir con estos mandatos. (Le puede interesar: ["En 25 años de Ley de tierras, más del 60% de fincas siguen sin derechos de propiedad"](https://archivo.contagioradio.com/ley-de-tierras-mas-fincas-siguen-sin-derechos-de-propiedad/))

Resaltó además, que en la audiencia a realizar por la Corte Constitucional, **deberían tener un espacio las víctimas para ser escuchadas,** al igual que las organizaciones de derechos humanos y acompañantes, para que la decisión sobre extender o no la vigencia de la Ley tenga suficiente elementos de análisis. (Le puede interesar: ["No podemos hablar de paz sin nuestra participación política: sobrevivientes del conflicto"](https://archivo.contagioradio.com/no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto/))

### **¿Qué se necesita para que la Ley funcione? y ¿qué pasará si no se extiende su vigencia?** 

Cuando Santoyo se refirió al poco desarrollo de la Ley, afirmó que esta no supera el 25% de su ejecución porque el Gobierno no ha hecho lo necesario para garantizar su cumplimiento. Es decir, se necesita una intención estatal para poner en marcha el cumplimiento de la normativa, cuyo efecto es que "toda la nación colombiana tenga la garantía de que no vamos a seguir pretendiendo la guerra como un ejercicio para resolver las controversias". (Le puede interesar: ["La ruptura de confianza entre la sobrevivientes y el Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/victimas-memoria-historica/))

En cuanto a lo que pasaría en caso de no prorrogarse su vigencia, el experto señaló que sus efectos ya se están viendo mediante "la acción de marchitamiento económico y administrativo" que han sufrido instituciones como la Unidad de Víctimas o el Centro Nacional de Memoria Histórica. En cambio, criticó el argumento usado por el Gobierno según el cual no hay recursos económicos para estas instituciones, puesto que "la comunidad internacional ha invertido millones para la implementación de la paz en el país".

### **El Sistema creado por el Acuerdo de Paz: una esperanza para las víctimas** 

Santoyo aseguró que si se crea un nuevo marco normativo o se extiende la vigencia de la Ley de Víctimas,  se debería incluir el resultado en el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), el cual, sigue siendo considerado para las víctimas un acompañamiento. Por último, el director del OFB afirmó que el país debería poder comprender que las víctimas son vulneradas y que se siguen afectando sus derechos "por las decisiones miopes de un Gobierno central que no ofrece garantías".

<iframe id="audio_42763114" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42763114_4_1.html?c1=ff6600"></iframe>**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
