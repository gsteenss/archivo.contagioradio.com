Title: Colombia es el segundo país con más desplazamiento interno del mundo
Date: 2015-05-07 08:57
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 11 millones desplazados internos 2014, 38 millones desplazados internos 2015, Consejo Noruego, Desplazamiento forzado, Global 2015 informe desplazados internos 2015, Segundo país del mundo desplazamiento interno Colombia
Slug: informe-global-2015-sobre-desplazados-arroja-las-peores-cifras-en-35-anos
Status: published

###### Foto:Librered.com 

El Observatorio de desplazamiento interno  IDMC que depende del Consejo Noruego para los refugiados presentó su **[informe Global 2015 sobre desplazamiento interno](http://bit.ly/1GOr2MZ)** en el mundo. El informe corrobora la cifra de **11 millones de personas desplazadas en 2014 y 30.000 personas al día tuvieron que abandonar sus casas**, lo que deja una cifra en total en lo que va de año de **38 millones de desplazados.**

El informe marca una cifra récord a nivel mundial y según Jan Egeland, secretario general de NRC  “...Se trata de **las peores cifras de desplazamientos forzados en una generación**, lo que pone de manifiesto nuestro fracaso absoluto para proteger a civiles inocentes...”.

[![desplazamiento-en-el-mundo-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/desplazamiento-en-el-mundo-contagio-radio.jpg){.size-full .wp-image-8230 .aligncenter width="599" height="411"}](https://archivo.contagioradio.com/informe-global-2015-sobre-desplazados-arroja-las-peores-cifras-en-35-anos/desplazamiento-en-el-mundo-contagio-radio/)

El informe arroja cifras sobre los conflictos armados actuales y además cifra **el 60%** del desplazamiento en cinco países**,** **Irak, Sudán del Sur, Siria, República Democrática del Congo y Nigeria.**

La peor cifra es para **Siria con 7 millones de desplazados más 4 de refugiados**, siendo esta cifra la valoración de cinco años de guerra.

Debido a la violencia perpetrada por el **Estado Islámico o la milicia Boko Haram**, países como Irak o Nigeria, con 2 millones el primero y casi un millón el segundo.

Países como **Sudán del Sur o la República democrática del Congo continúan encabezando altas cifras de desplazamiento empeorando con respecto al año anterior**, sumando ambos países un millón de desplazados más. Otras cifras hablan de los nuevos conflictos como el de Ucrania que han dejado 650.000 personas desplazadas.

También en el informe se hace especial mención a **la situación en el Mediterráneo**, donde la grave crisis humanitaria en África ha llevado a miles de personas a correr grandes riesgos en embarcaciones inseguras para **cruzar de Libia a Europa y poder escapar de la violencia política**.

En palabras de Volker Turk, Alto comisionado de la ONU para la protección “Y como hemos visto en el pasado, por ejemplo en el Mediterráneo, la **desesperación lleva a las personas a tomar riesgos**, incluso viajes peligrosos en barco. La solución más evidente consiste en un esfuerzo supremo para propiciar la paz en los países devastados por la guerra,”

**Colombia continua siendo el segundo país del mundo** en desplazamiento interno después de Siria, en la lista continúan los cinco países que este año han registrado las peores cifras.

Egeland, secretario del NRC, afirma que estas cifras auguran lo que viene en un futuro refiriéndose a los conflictos armados y su no resolución, además añadió que los estados no se están haciendo responsables y cada vez están colaborando menos con las ONG’S y organizaciones sociales de defensa de los DDHH.

**Informe** de IDMC para el Consejo Noruego para los refugiados: **http://bit.ly/1GOr2MZ**
