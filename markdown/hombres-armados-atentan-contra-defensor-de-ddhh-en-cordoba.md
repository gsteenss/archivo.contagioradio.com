Title: Hombres armados atentan contra defensor de DDHH en Córdoba
Date: 2016-06-27 14:34
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, marcha patriotica, paramilitarismo en Córdoba
Slug: hombres-armados-atentan-contra-defensor-de-ddhh-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Montelíbano-Córdoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal ] 

###### [27 Junio 2016]

Sobre las 2 de la madrugada de este lunes, hombres armados atacaron la vivienda del defensor de derechos humanos Arnovis Zapata, en el municipio de Montelíbano, Córdoba. **Pese a que dispararon en su contra, el líder salió ileso.** Al hecho se suman las amenazas que paramilitares de las Autodefensas Gaitanistas han proferido contra quienes socializan los acuerdos de paz con las comunidades.

El líder afirma que **las amenazas también se han dirigido contra los campesinos cocaleros de la región** que han asegurado estar dispuestos a hacer parte del programa de sustitución de cultivos ilícitos conforme a lo pactado en La Habana. La situación alarma a defensores y organizaciones que trabajan por hacer viable la construcción de paz en el departamento.

Zapata agrega que en el corregimiento de Tierradentro las amenazas son proferidas por reconocidos paramilitares, mientras **los policías permanecen en la estación encerrados sin patrullar el pueblo** para brindar garantías de seguridad para la población.

<iframe src="http://co.ivoox.com/es/player_ej_12043853_2_1.html?data=kpedlpiceZShhpywj5aVaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaLmz9Tjy9iPnsLkwtnOjZKPiMbaxtPg0dePqMafpam1qpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
