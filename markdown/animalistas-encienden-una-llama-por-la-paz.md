Title: Animalistas, encienden una llama por la paz
Date: 2015-03-20 18:00
Author: CtgAdm
Category: Animales, Fotografia, Nacional
Tags: Animalistas, consulta popular, defensa de los animales, derechos animales, una llama por la paz
Slug: animalistas-encienden-una-llama-por-la-paz
Status: published

###### Foto: Contagio Radio 

Este jueves 19 de marzo, los y las animalistas se congregaron en la Plaza San Martín en el centro de Bogotá, con el objetivo de encender la novena llama por la paz, esta vez por los animales no humanos.

En **‘Todos somos animales, animalistas encienden la llama por la paz’**, se reflexionó sobre cómo la construcción de paz en Colombia pasa por el respeto a todos los seres vivos, debido a que los animales también son víctimas del conflicto armado y de otras formas de violencia.

Durante la presentación del evento, Natalia Parra, directora de la Plataforma Alto e integrante del programa Onda Animalista de Contagio Radio, anunció que el próximo **martes 24 de marzo a las 2 de la tarde en la Plaza de Bolívar, se presentará la Consulta Popular,** para que las y los bogotanos se manifiesten en contra de la tauromaquia. Así mismo, se mostró apoyo a los animalistas amenazados en el panfleto enviado por "Águilas Negras".

Los asistentes a este acto por los animales, pudieron disfrutar de exposiciones animalistas, experiencias pedagógicas, lúdicas, gastronómicas, literarias y musicales, que dan paso a que la ciudadanía genere otras maneras de tratar a los animales.

\
