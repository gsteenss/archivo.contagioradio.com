Title: Coincidencias entre la condena a muerte de Jesús y las nuestras
Date: 2019-08-18 14:57
Author: CtgAdm
Category: Opinion
Tags: Jesus de nazaret, Teologia
Slug: coincidencias-entre-la-condena-a-muerte-de-jesus-y-las-nuestras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones, 1***

[(Romanos 2,24) ]

[Bogotá, 14 de agosto de 2019]

*["Me gusta tu Cristo... No me gustan tus cristianos. ]*

*[Tus cristianos son muy diferentes a tu Cristo"]*

[ Mahatma Gandhi.]

[Estimado]

**Hermano en la fe**

**Cristianos, cristianas, personas interesadas**

[Un cordial saludo,]

[Leyendo y analizando los textos bíblicos de la pasión y muerte de Jesús de Nazaret veo coincidencias con realidades actuales que influyen y/o determinan la manera de pensar, decidir y vivir de muchos creyentes, por esto le comparto esta reflexión.]

[Conozco personas creyentes buenas, bien intencionadas, que quieren hacer la voluntad de Dios, que actúan por convicciones religiosas pero que, con frecuencia, toman decisiones contrarias a]**lo que dijo e hizo Nuestro Señor Jesucristo**[, porque han sido mal “formadas” o son mal informadas por medios que las llevan a pensar, actuar y decidir de acuerdo a intereses que esconden en la religión sus intenciones contrarias al cristianismo. ]

[La fe es un tesoro que hay que proteger, no sólo de quienes la atacan desde fuera  de las iglesias sino también de quienes al interior de ellas, la colocan al servicio de intereses anticristianos, con frecuencia sin darse cuenta.  ]

[Al ver como actuaron los responsables de la muerte de Jesús, la forma cómo tomaron decisiones, las motivaciones y relacionarlos con lo que pasa hoy, aparecen muchas coincidencias. ]

[En la condena a muerte de Jesús, relatada por los 4 Evangelios (Marcos 15,6-15; Mateo 27,15-26; Lucas 23, 13-25; Juan 18, 39-19,16) coinciden en varias cosas: Jesús se mantiene firme y digno ante quienes lo condenan a muerte injustamente; Pilatos no ve en Jesús delito alguno y quiere dejarlo libre; la multitud, “el pueblo”, pide que Jesús sea crucificado y que, en su lugar, dejen libre a Barrabás (un preso famoso, encarcelado por robo y homicidio). ]

[Los hechos son muy fuertes. El pueblo, la multitud opta por Barrabás y pide la crucifixión de Jesús. Jesús buscó liberar al pueblo de las esclavitudes, predicó una vida digna para todas las personas, proclamó y practicó el amor, la igualdad, la justicia entre los seres humanos, comenzando por los marginalizados, unas nuevas relaciones con Dios, con los seres humanos y con la naturaleza. ¡Qué difícil y qué duro ver esta realidad, asumirla! ¡Cómo duele asumir que esto fue cierto!]**El pueblo decidiendo en su contra.  **

[Pero la historia se repite, el pueblo ha elegido a quienes se han apoderado, con trampas, corrupción, engaños y manipulación del dinero de la salud, la educación, los servicios públicos, la vivienda… sigue una larga y dolorosa lista; ha elegido a quienes han entregado los recursos del país a empresas nacionales e internacionales de las que son socios ellos o sus amigos; ha elegido a quienes no les importa el hambre, la desnutrición, el desempleo o subempleo y el desplazamiento forzado; a quienes acabaron con el campesinado, la industria, la pequeñas y mediana empresas, la estabilidad laboral, la producción nacional... El mismo pueblo, la multitud, ha despreciado a los profetas, a quienes dicen la verdad, a quienes no le hacen promesas inalcanzables, ni les mienten, ni engañan.]**¿Cómo entender esta realidad?**

[Los evangelios de Marcos (15,39) y Mateo (27,20) nos dan la “pista” para entender:]*[“Los principales sacerdotes]****incitaron, persuadieron****[ al pueblo, a la multitud para que pidiera la libertad de Barrabás y la crucifixión de Jesús”.]*[ El pueblo fue engañado, fue manipulado para que decidir a favor de un delincuente, de un asesino y pedir la muerte del hombre que había vivido para servirlo. Los sumos sacerdotes se quitaron “el problema” de Jesús de Nazaret, muy hábil e inteligentemente lograron que el pueblo asumiera sus intereses como propios, que se convenciera que lo que le decían era “por su propio bien”; así, ellos y sus amigos del poder pueden decir:]*[“el pueblo decidió, el pueblo lo pidió”, “hicimos la voluntad del pueblo”, “el pueblo tiene la razón”.]* **¿No les suena esto muy familiar?**

[Al pueblo lo engañaron] [diciéndole que Jesús era un blasfemo, es decir, que hablaba mal de Dios, que no respetaba la verdadera religión, que se hacía llamar Hijo de Dios, que no respetaba las tradiciones familiares, que andaba con malas compañías, que lo que predicaba no era la religión verdadera. ]**Y el pueblo, gente buena, bien intencionada o ingenua, les creyó.**

[Los sumos sacerdotes ocultaron el verdadero problema: que Jesús cuestionaba el sistema religioso que le permitía a ellos, a los demás dirigentes religiosos y sus aliados políticos y económicos explotar al pueblo en nombre de Dios, enriquecerse con el sistema cambiario del templo de Jerusalén y que para eso se habían aliado con los romanos. Es decir, “el poder” que convence al pueblo para pedir la muerte de Jesús, es “el poder” que se sostiene abusando del pueblo y es “el poder” que necesita eliminar a Jesús porque le está abriendo los ojos al pueblo, es “el poder” que legitima la muerte de Jesús en el  nombre de Dios. El pueblo, “se comió el cuento” que la muerte de Jesús era querida por Dios y no se dio cuenta del favor que le estaba haciendo a los intereses del “poder” y del daño que se hacía a sí mismo.]*[“Al pueblo hay que darle la información conveniente y prudente para que tome  decisiones acertadas”. ¿]***Acertadas para quién? **

[Esta realidad debe llevarnos a pensar, actuar y decidir críticamente, de acuerdo al mensaje de Jesús en los Evangelios y a no dejarnos manipular por los sumos sacerdotes y los poderes de ayer y de hoy.]

[Ahora entiendo para qué nos infunden miedos, odios y fanatismos. Con miedos, odios y fanatismos no pensamos por nosotros mismos, creemos lo que nos dicen, sin preguntar y sin cuestionar; nos cierran la mente y el corazón para dar y pedir razones y argumentos, así modifican nuestras emociones, creencias y valores. ]

[Recordemos ¿Cuantas noticias falsas o verdades a medias recibimos por diversos medios? ¿Los mensajes religiosos, cristianos que recibimos los contrastamos con el mensaje del Señor Jesús? ¿Los mensajes que recibimos y reenviamos están de acuerdo con los valores cristianos de la verdad, la justicia, la transparencia, la honestidad, el bien común...?  ]

[Como cristianos debemos tener como principio fundamental las palabras, pensamientos y actuaciones de Jesucristo, quien dijo:]*[“Yo para esto he nacido y para esto he venido al mundo: para dar testimonio de la verdad. Todo aquel que es de la verdad, oye mi voz”]*[ (Juan, 18,37). Estamos llamados a escuchar la voz de Jesús y no las voces que proclaman mentiras, verdades a medias y  noticias falsas.]*[“Si se mantienen fieles a mi palabra, serán realmente discípulos míos, conocerán la verdad y la verdad los hará libres”]*[ (Juan 8,31-32)]

[Fraternalmente, su hermano en la fe. ]

1.  **Alberto Franco, CSsR, JyP**

[francoalberto9@gmail.com]
