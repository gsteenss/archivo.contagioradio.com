Title: Con Plan Jarillón "no hay reubicación, hay desalojo y despojo del territorio"
Date: 2015-06-11 11:35
Author: AdminContagio
Category: Otra Mirada, Resistencias
Tags: ana eraso, Cali, congreso de los pueblos, Desalojo, plan jarillon, poder y unidad popular
Slug: con-plan-jarillon-no-hay-reubicacion-hay-desalojo-y-despojo-del-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Cali-e1434052654796.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: OrgsColombia 

<div>

</div>

<div>

</div>

<iframe src="http://www.ivoox.com/player_ek_4627412_2_1.html?data=lZufmZmVdo6ZmKiakp2Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DijLXZw9OPjsLmytHZh6iXaaOnz5CSlJfSs4zcwt6Y1MrZpsrXwsjWh6iXaaOnz4qfpZDMpdqfxcrgw9HTrtCf2pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ana Eraso, Poder y Unidad Popular] 

<div>

</div>

<div>

</div>

<div style="text-align: left;">

[11 jun 2015]

</div>

<div>

</div>

40 familias del Jarillón, el Sector Venecia y Las Vegas, en Cali, están siendo desalojadas de sus viviendas desde el pasado lunes 8 de junio. Aunque la Alcaldía asegura que las reubicará, hasta el momento no se ha hecho entrega de predios, viviendas, o recursos para garantizar su traslado de manera digna y segura.

<div>

Según denuncia la organización "Poder y Unidad Popular", la alcaldía esconde intereses comerciales tras el discurso del impacto ambiental: Con la implementación del Plan Jarillón construirán un muelle turístico y comercial que va a beneficiar a los empresarios de la región, a costa del despojo territorial de las familias que han vivido allí por medio siglo.

"Esto no es una reubicación - asegura Ana Eraso-, eso es falso porque las familias hoy no tienen donde dormir. Están literalmente en la calle".

Las familias afectadas dicen "Progreso sí, pero no así", y solicitan a la alcaldía que el proceso se haga de manera concertada: Por una parte, que se asegure reubicación en un lugar digno en el cual puedan seguir desarrollando sus formas de vida familiar y dinámicas económicas; y que el proceso sea planeado y no "a las carreras" como ha ocurrido hasta ahora.

En Cali, las organizaciones sociales adelantan la campaña "Hogar, digno hogar" para evaluar las alternativas de desarrollo urbano, que incluyan diálogo y consulta previa con las comunidades que viven en los territorios, para garantizar los derechos de los colombianos y las colombianas.

</div>
