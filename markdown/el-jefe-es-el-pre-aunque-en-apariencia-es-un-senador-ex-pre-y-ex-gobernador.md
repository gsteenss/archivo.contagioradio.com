Title: El Jefe es el Pre, aunque en apariencia es un senador, ex pre y ex gobernador
Date: 2018-06-26 12:28
Category: Camilo, Opinion
Tags: Alvaro Uribe, Centro Democrático, Iván Duque
Slug: el-jefe-es-el-pre-aunque-en-apariencia-es-un-senador-ex-pre-y-ex-gobernador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/uribe-y-duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Centro democrático 

#### Por **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 26 Jun 2018 

<div>

Ya se observan los rasgos característicos de estos próximos cuatro años o mejor de la apuesta de un proyecto de país que en 48 meses prolongara los sueños, valores, que encarna el uribismo.

Hoy el partido **Centro Democrático** es el único disciplinado en el congreso. Es un partido  que cuenta con una masa electoral ciega, sumisa, obediente. Estas capacidades ya demostradas se basan en una visión de defensa de la familia tradicional, de la seguridad para garantizar la inversión extranjera, el extractivismo,  reformas que aseguren a los oligopolios su expansión de negocios con exención de impuestos.

El sentimiento moralista inoculado, muy propio del catolicismo maniqueo, justifica un tratamiento represivo del tráfico de drogas asociado a los cultivadores de coca no a los carteles,  la restricción de las libertades a las identidades sexuales y de otro tipo, y un sentido castigador punitivo de la justicia para la violencia rebelde que sostenga con buenos ojos la Pax Neoliberal.

Sin embargo, paradójicamente la ofensiva contra el Acuerdo de Paz puede ser un factor de riesgo para los propósitos estratégicos de la inversión extranjera. El incumplimiento en esta materia ha prendido las alertas en las calificadoras de riesgo que manifiestan que reestructurar los acuerdos con el hoy partido de las FARC, podría suscitar nuevas dinámicas de violencia e indicar que hay poca seguridad jurídica con la institucionalidad colombiana.

La lectura de sectores  internacionales sobre Colombia tiene una identificación de proyecto de Uribe como el de una derecha retrógrada, ni siquiera comparable a la de Trump. Un partido negacionista de las realidades de la exclusión, con rasgos populistas para los sectores urbanos y medios que desconocen la premodernidad de la ruralidad. Esa misma comunidad sabe del rabo de paja del Centro Democrático vinculado a su máxima expresión, Uribe y su entorno,  con el tráfico de drogas y el paramilitarismo. Y también, sienten que el presidente electo podría estar atrapado en la red de decisiones de Uribe.

Las primeras actuaciones del elegido por Uribe, Iván Duque, están demostrando que el temor a la ausencia de independencia y autonomía es una realidad. El poder real en el nuevo gobierno es el del senador, del ex pre y el ex gobernador.

El primer hecho, el elegido por Uribe públicamente llamó a los senadores del Centro Democrático,  y a todos los que están ya acomodándose a su lado, a abstenerse de aprobar las reglas de procedimiento de la Jurisdicción Especial de Paz, JEP. Según, su invención hasta que la Corte Constitucional se pronuncie es un sinsentido avanzar. Horas después la Corte Constitucional manifestó que su pronunciamiento no era necesario para el trámite en el congreso de las reglas procesales.

Para reforzar la postura real Paloma Valencia, otra escudera de Uribe, ante una eventual conciliación de método entre Santos y su sucesor para resolver asuntos de la JEP, confirmó a través de una llamada directa a Uribe, a quién saludó como "Pre" que la posición era la del senador, no la del electo.

Los puntos de honor del uribismo pretenden  vaciar de contenidos el Acuerdo. Mantener la palabra JEP convirtiendo este en un tribunal de juicio contra las FARC, desmontando sustancias de la justicia restauradora. Adicionalmente,  evitar la posesión de los integrantes del secretariado de las FARC EP, hoy partido político, hasta que cumplan una sanción carcelaria. Igualmente, pretenden crear un tribunal exclusivo y distinto para los ex militares y terceros con otros magistrados, diferentes a los ya elegidos, que generen condiciones de "dignidad" a los agentes de Estado y empresarios. Así que se trata de mantener un paquete con contenidos distintos. Hacer trizas sin mostrar que se ha hecho trizas.

Y para reiterar quién manda. Alicia Arango, ex secretaria privada en el gobierno de Uribe expresó  en una entrevista radial, palabras más, palabras menos, si bien el presidente es Duque, el jefe es Uribe.

El uribismo que encarna el proyecto de país premoderno, el de las maquinarias y la corrupción está ahí, definiendo nuevos mecanismos de impunidad jurídica y social para ellos, y para quiénes les han protegido de una riqueza mal habida, que se ha protegido y acumulado a través del paramilitarismo. Aí Duque es un cambio de cara para que a través de ella se expresé lo mismo: Uribe.

Pero aún así con drones de aspersiones aéreas, con fracturas para la extracción petrolera en Boyacá, hay una nueva expresión política, más que un partido, con Petro y más allá de él, en el que se proyecta otra propuesta de país. Ese país de libertades está trascendiendo, potenciando un nuevo sentimiento de la política. Un país distinto a aquel del espejismo del 2002.

#### [**Leer más columnas Camilo De Las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 

</div>
