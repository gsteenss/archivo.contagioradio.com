Title: Ex-presos políticos Chile inician negociación con el Estado por reparación y justicia
Date: 2015-05-28 07:34
Category: El mundo, Otra Mirada
Tags: Chile, Ex-presos políticos Chile exigen justa reparación, Ex-presos políticos Chile inician negociaciones con el gobierno, Huelga de hambre, Huelga de hambre ex-presos políticos Chile, Nelson Aramburu, presos politicos, Torturados y presos políticos dictadura Chilena huelga de hambre por reparación
Slug: ex-presos-politicos-chile-inician-negociacion-con-el-estado-por-reparacion-y-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Huelga-Ex-Presos-Politicos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Radionuevomundo.com 

###### **Entrevista con [Nelson Aramburu], ex-preso político de la dictadura de Chile:** 

<iframe src="http://www.ivoox.com/player_ek_4556990_2_1.html?data=lZqimJ6ddI6ZmKiakp6Jd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidmh0dfS1dTXb9HjzYqwlYqliNXdxNTgjajMrc3Zz9TgjdPJq9DXysbbjcjTsozZzZDS1dnFqNCfzcaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [28 May.2015] 

**100 ex-presos políticos y torturados de la dictadura chilena han finalizado la huelga de hambre** que duró **22 días** y han firmado un acuerdo con el gobierno para establecer una **mesa de negociaciones** con el fin de que se cumpla la **reparación estatal** como víctimas de la dictadura.

La **mesa de diálogo** será presidida por el **obispo de Rancagua** Alejandro Goic., y en la que también participaran el ministro de hacienda, Segpres y de Desarrollo social, además de representantes del senado, de la cámara de representantes y del programa d DDHH del ministerio.

También participarán la **Iglesia y el Alto Comisionado de las Naciones Unidas para los DDHH. **La mesa debe de estar conformada a más tardar para el 30 de Mayo y tendrá un plazo de **45 días** para presentar las **propuestas**.

Según **Nelson Aramburu huelguista y represaliado** por la dictadura, su reivindicación es una reparación justa por las torturas y detenciones por parte del estado Chileno en la dictadura. Además, Aramburu añade que es de vital importancia el compromiso de que los responsables de los crímenes no queden en total impunidad.

Para **José Pradenas, uno de los ex -presos políticos** que ha secundado la huelga de hambre, ha declarado que “… Al Gobierno ya le dijimos que **si no respetan nuestros acuerdos con los cuales nos bajamos, vamos a volver nuevamente a las movilizaciones**, y nosotros vamos a tener movilizaciones permanentes. Este lunes, acá en Santiago, en Londres 38, empieza una vigilia. Además, todos los días miércoles nos vamos a marchar a La Moneda y vamos siempre a estar presentes, porque no nos podemos confiar, porque el Gobierno nos ha fallado tantas veces que creemos que lo puede volver a hacer…”.

Pradena, igual que Aramburu son **reticentes y sienten desconfianza de las negociaciones con el estado,** ya que para ellos si en 22 días de huelga el ejecutivo no ha respondido y en todo el tiempo transcurrido desde la dictadura hasta ahora de los **42.000 represaliados, 12.000 ya han muerto,** es difícil pensar que ahora lo hagan.

Para los huelguistas es inconcebible que los **torturadores tengan mejores pensiones que los torturados** y muchos de ellos aún no hayan sido juzgados por los crímenes que cometieron en la dictadura. Por otro lado, el gobierno no ha cumplido en materia de reparación compensatoria y no ha brindado ningún tipo de ayuda ni de pensión compensatoria.
