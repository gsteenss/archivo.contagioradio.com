Title: María Fernanda Cabal busca proteger a despojadores de tierras
Date: 2018-09-06 15:49
Author: AdminContagio
Category: DDHH, Nacional
Tags: Despojadores, Ley 1448, María Fernanda Cabal, Restitución de tierras
Slug: maria-fernanda-cabal-busca-proteger-a-despojadores-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/regresan-a-las-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Sept 2018] 

La propuesta que hizo la senadora María Fernanda Cabal, del Centro Democrático, para que se reforme la ley 1448, ha generado revuelo, ya que abriría la posibilidad para la creación de una segunda instancia para la revisión de los procesos de restitución de tierras que **legalizaría apropiaciones de tierras producto del conflicto armado.**

Según Cabal, esta ley se ha convertido en una “herramienta de despojo” a poseedores de tierra de buena fe que las adquirieron en medio del conflicto. Sin embargo, de acuerdo con la abogada Diana Muriel, de la Comisión de Justicia y Paz, la 1448 si brinda garantías procesales y judiciales para quienes intervienen en el proceso de restitución de tierras como opositores y **la intensión detrás de esta acción sería generar impunidad para los terceros. **

“La senadora pretende que la presunción de buena fe sea presumida y no demostrada, lo cual sería contrario a los derechos de las víctimas” afirmó Muriel, quien añadió que actualmente en un proceso de restitución lo que procede es que se estudian las pruebas llevas por los opositores que demuestren cómo adquirieron los predios y con base en ello se toma la decisión.

Además, la abogada manifestó que, a corte de julio de 2018, el 38% de las personas que se constituyeron como opositores obtuvieron beneficios por parte de los jueces, de esos casos **480 conservaron la propiedad de los previos**, en 18 casos los opositores fueron compensados con otros predios y en 109 recibieron subsidios por parte del Estado. (Le puede interesar:["En más del 90% de los casos de restitución de tierras existen intereses mineroenergéticos"](https://archivo.contagioradio.com/concesiones-minero-energeticas-en-restitucion-de-tierras-no-han-permitido-retorno-de-victimas-movice/))

De igual forma, Muriel recordó que **para el 2015 se sabía que 10 personas representaban el 50% de las oposiciones que se hicieron ante los jueces de restitución** de tierras y no lograron demostrar su buena fe, hecho que también ocurrió con las empresas y que lograron evidenciar cómo si existió un uso o aprovechamiento de la violencia para hacerse a predios que correspondían a comunidades campesinas que tuvieron que desplazarse de manera forzada de sus territorios.

<iframe id="audio_28397178" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28397178_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
