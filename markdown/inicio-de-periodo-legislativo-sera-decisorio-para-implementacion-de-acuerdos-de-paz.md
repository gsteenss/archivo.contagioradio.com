Title: Periodo legislativo será decisorio para implementación de Acuerdos de Paz
Date: 2017-07-21 13:21
Category: Paz, Política
Tags: Congreso de la República, implementación acuerdos de paz
Slug: inicio-de-periodo-legislativo-sera-decisorio-para-implementacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [21 Jul 2017] 

Los congresistas del Polo Democrático Iván Cepeda y Alirio Uribe, hicieron un llamado a las bancadas en el Congreso para que **se acelere la implementación de los Acuerdos de Paz en este inicio del periodo legislativo 2017-2018**, y anunciaron que convocaran a una audiencia pública en los próximos días en la que abordarán los puntos urgentes para continuar con la reincorporación de las FARC-EP a la sociedad.

Para Cepeda **“el Congreso tiene la labor prioritaria en este periodo de sacar adelante unos 15 proyectos** de ley y reformas que son indispensables para la buena implementación de los acuerdos de paz” motivo por el cual se hace urgente que los congresistas legislen de manera eficaz y finalice con éxito el proceso de implementación.

Los próximos proyectos de ley que deberán ser legislados son la **ley estatutaria de la Jurisdicción Especial para la Paz, la ley de Tierras, la reforma Política y la reforma a la ley de Víctimas**, que constituyen los ejes fundamentales entorno a los cuales gira el Acuerdo de Paz. (Le puede interesar: ["En septiembre se conocerán los nombres de los magistrados del Tribunal de Paz"](https://archivo.contagioradio.com/en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz/))

De igual forma Cepeda señaló que la audiencia pública también tiene como objetivo **hacer un control al gobierno para saber en qué ha avanzado en materia de implementación de los acuerdos** y si los proyectos están debidamente preparados para pasar a los debates en el congreso y prever que los congresistas en época pre electoral no vayan a abandonar su deber con la implementación.

Adicionalmente, uno de los temas que se abordará en la audiencia pública es la falta acción de por parte de la rama judicial en la implementación de la Ley de Amnistía, que generó una huelga de hambre de más de **1.500 presos políticos de las FARC-EP y fue levantada el pasado 20 de julio con la firma del decreto 1252**. (Le puede interesar: ["Próximo periodo legislativo será fundamental para la paz"](https://archivo.contagioradio.com/periodo-legislativo-paz-42504/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
