Title: Organizaciones Internacionales apoyan Acuerdos de Paz en Colombia
Date: 2016-09-29 12:05
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Apoyo al proceso de paz, Apoyo internacional, Comisión Interamericana de Derechos Humanos, Derecho Internacional Humanitario
Slug: organizaciones-internacionales-apoyan-acuerdos-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz-col.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las2Orillas 

###### 29 de Sept 2016 

**68 organizaciones de todo el mundo manifestaron su apoyo al “Acuerdo final para la terminación del conflicto y la construcción de una paz estable y duradera” en Colombia**, pactado entre el gobierno y las FARC. Así se conoció a través de un comunicado publicado en la página del Centro de Estudios Legales y Sociales de Argentina CELS.

Organizaciones de Mujeres, Derechos Humanos, Derechos sexuales y reproductivos, Asociaciones de Víctimas, Defensores de niños, niñas y jóvenes, Comisiones Ecuménicas y Familiares de desaparecidos, de Venezuela, México, Ecuador, Argentina, Indonesia, Egipto, Kenia y otros países son algunos de los firmantes.

En el comunicado **llaman la atención sobre la importancia de poner fin a una guerra de más de 50 años en la que se han cometido violaciones masivas y sistemáticas a los Derechos Humanos y al Derecho Internacional humanitario**, afectando a la población colombiana en general, [siendo la población rural la más afectada](https://archivo.contagioradio.com/mision-internacional-verificara-situacion-del-campesinado-colombiano/).

Las organizaciones **afirmaron su apoyo al momento que atraviesa Colombia y respaldan el Sí para el plebiscito que tendrá lugar este domingo 2 de Octubre.** También expresaron su interés en [ser parte de los organismos veedores de la implementación de los Acuerdos](https://archivo.contagioradio.com/comunicado-de-la-fiscal-de-la-cpi-evidencia-que-no-habra-impunidad-juan-carlos-henao/) para asegurar los principios de justicia, verdad, reparación y garantías de no repetición.

A este hecho **se suman los miembros del Parlamento Europeo, quienes manifestaron su respaldo al acuerdo de paz a través de un video divulgado en redes sociales,** donde por lo menos 20 europarlamentarios aseguraron que es necesario que se acabe la guerra en el país, para lo cual, manifestaron que es esencial que el pueblo colombiano apoye el acuerdo con el Sí.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
