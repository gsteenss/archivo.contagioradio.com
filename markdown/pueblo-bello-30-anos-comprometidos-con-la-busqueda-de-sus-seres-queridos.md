Title: Pueblo Bello: 30 años  comprometidos con la búsqueda de sus seres queridos
Date: 2020-01-14 18:25
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Masacre de Pueblo Bello, Pueblo Bello, Reconciliación, uraba
Slug: pueblo-bello-30-anos-comprometidos-con-la-busqueda-de-sus-seres-queridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Pueblo-Bello.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El 14 de enero de 1990 en **Pueblo Bello, corregimiento de Turbo, Antioquia,** cerca de 60 hombres pertenecientes a la organización paramilitar de “Los Tangueros” se llevaron a 43 personas del caserío, en colaboración con miembros del Ejército. Hoy 30 años después, la comunidad continúa trabajando en el esclarecimiento del paradero de sus familiares a quienes recuerdan a través de una conmemoración que mezcla la memoria, el arte y el deporte.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Katy Milena Fuentes, parte del Comité Impulsor de [Pueblo Bello](http://“Sin%20justicia%20no%20hay%20paz”%20familiares%20de%20las%20víctimas%20de%20Pueblo%20Bello)** e hija de Wilson Fuentes, una de las víctimas de la masacre, señala a propósito de la conmemoración que como sujetos de reparación colectiva, con el pasar de los años se ha hecho un trabajo de reconstrucción del tejido social y **"aunque el pueblo quedó bastante afectado, hemos podido resurgir y renacer de la cenizas"**, explica.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fsinolvidoesmemoria%2Fvideos%2F1209031225960948%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=411" width="800" height="411" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### Un aniversario que denuncia la ausencia de voluntad del Estado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, el abogado Sebastián Bojacá de la Comisión Colombiana de Juristas (CCJ), quien además ha representado a las víctimas de este corregimiento de Antioquia, afirma que el aniversario revive diferentes heridas como la falta de voluntad política de la Fiscalía para realizar la búsqueda de los familiares pese a que se conoce el lugar en el que están enterrados algunos de los cuerpos de las personas desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El aniversario revive diferentes heridas como la falta de voluntad política de la Fiscalía para realizar la búsqueda de los familiares "

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Asegura que el aniversario tiene como objetivo demostrar que el Estado aún no ha demostrado voluntad para continuar el proceso de búsqueda de las personas desaparecidas, ni cumplido con garantizar el acceso completo a la salud ordenado por la **Corte Interamericana de Derechos Humanos**, razón por la que muchos familiares han optado por salir del corregimiento. [(Le puede interesar: Estado no garantiza acceso a salud y atención psicosocial a víctimas del conflicto](https://archivo.contagioradio.com/estado-no-garantiza-el-acceso-a-la-salud-de-victimas-del-conflicto/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Conmemorar a través del arte

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, también existe un anhelo de superar las dificultades a través del arte, la música y el deporte; por medio de las medidas de reparación colectivas se ha consolidado un grupo que quiere recordar a sus familiares de la mejor manera, **"en ese sentido no se ven en el papel de víctimas, por el contrario manifiestan que a partir de su vida, puede hacer lo que sus familiares querían hacer"**, menciona el abogado

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque se requiere un mayor compromiso del Estado para garantizar la no repetición, como señala Bojacá, Katy asegura que desde hace 30 años **"tratamos de trabajar unidos y estamos luchando para que estos hechos no se repitan"**. Agregando que después de la desaparición de su padre adquirió un compromiso para dar con su paradero, "él es uno de los que aún se encuentra desaparecido y no voy a descansar hasta saber qué paso con sus restos, esa es la motivación que tenemos en este momento".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Katy relata que para estos 30 años de conmemoración donde participan cerca de 250 personas, la jornada se divide en varios encuentros, comenzando con un acto religioso seguido de una presentación de un sonoviso que alberga fotos de los familiares desaparecidos, además de la proyección de dos producciones que fueron desarrolladas junto a la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD), incluida una historieta llamada 'A Orillas del Río'. Luego se realizará un recorrido hacia el mural de la memoria donde están representados los rostros de los familiares, y el evento concluirá con una chocolatada.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pueblo Bello al interior del Sistema Integral de Justicia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Bojacá relata que las víctimas de Pueblo Bello se han constituido dentro del caso 004 de la Jurisdicción Especial Para la Paz (JEP) que investiga los hechos victimizantes del Urabá Antioqueño sin embargo, menciona que el caso no ha sido priorizado, aún cuando uno de los participantes de la masacre fue el teniente Fabio Rincón Pulido, quien se acogió al sistema de justicia transicional. [(Le puede interesar: Víctimas en Pueblo Bello completan 26 años exigiendo verdad y justicia)](https://archivo.contagioradio.com/victimas-pueblo-bello-26-anos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras que, el trabajo realizado de la mano de la Unidad de Búsqueda de Personas dadas por Desaparecidas ha permitido una labor que promueve la incidencia e impulsar la recuperación de los cuerpos en las zonas establecidas pero también de otros familiares que fueron trasladados por grupos paramilitares hasta otras partes por ahora desconocidas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46597437" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46597437_4_1.html?c1=ff6600"></iframe>  
<iframe id="audio_46597474" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46597474_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
