Title: Ante ola de asesinatos de líderes organizaciones de DD.HH acuden a CIDH
Date: 2017-03-17 17:27
Category: DDHH, Nacional
Tags: CCJ, CIDH, colectivo de abogados, implementación acuerdos de paz, MOVICE, paz
Slug: situacion-de-lideres-sociales-en-colombia-ante-la-cidh
Status: published

###### [Foto: Archivo] 

###### [17 Mar. 2017] 

Cerca de 13 organizaciones sociales llevarán ante la Comisión Interamericana de Derechos Humanos (CIDH), varias preocupaciones frente a la realidad que están viviendo líderes y lideresas en Colombia. En las audiencias tratarán temas como la **falta de garantías de no repetición en los crímenes contra integrantes de organizaciones sociales** y denunciarán la búsqueda de mecanismos de impunidad para agentes estatales en el marco de las negociaciones de paz adelantadas entre la guerrilla de las FARC-EP y el Gobierno nacional.

**Las audiencias se llevarán a cabo este 21 de marzo** y allí se plantearán, además, los desafíos que en la actualidad enfrenta el país para poder construir una paz estable y duradera dentro de los cuales se deben incluir las **“plenas garantías para las organizaciones sociales y los defensores de derechos humanos”** dice el comunicado entregado por las organizaciones que participarán. Le puede interesar: [CIDH responde ante vulnerabilidad de defensores de DDHH](https://archivo.contagioradio.com/cidh-ordena-medidas-cautelares-para-alberto-yepes-defensor-de-ddhh/)

Otras de las preocupaciones que las organizaciones pretenden dejar de manifiesto, son la **evidencia de un ataque generalizado del cual son víctimas los líderes y lideresas**, así como la estigmatización y persecución en contra de movimientos y organizaciones sociales.

En ese orden de ideas, las organizaciones esperan que **la CIDH inste al Estado a investigar los asesinatos de 120 líderes y defensores de derechos humanos** ocurridos entre el 1 de enero de 2016 y el 20 de febrero de 2017. Le puede interesar: [120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

De igual modo, esperan que en esta oportunidad el Estado muestre **voluntad política para reparar efectivamente a las víctimas,** así como la puesta en marcha de acciones que propendan por superar la crisis del sistema judicial. Le puede interesar: [Defensores de derechos humanos "Contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

Por ultimo, a través de la comunicación emitida por las organizaciones han reiterado que **en este espacio internacional reiterarán su apoyo y compromiso con la paz de Colombia** por lo que se comprometen a acompañar el proceso de implementación, así como los diálogos que se adelantan entre el Gobierno nacional y la guerrilla del ELN.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]{.s1}

 
