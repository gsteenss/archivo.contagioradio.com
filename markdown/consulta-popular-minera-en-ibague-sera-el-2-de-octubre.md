Title: Consulta popular minera en Ibagué sería el 2 de octubre
Date: 2016-08-09 12:31
Category: Ambiente, Nacional, Otra Mirada
Tags: Anglo Gold Ashanti, consulta popular, Ibagué, La Colosa, Tolima. minería
Slug: consulta-popular-minera-en-ibague-sera-el-2-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [9 Ago 2016]

Ya se definió la fecha para la consulta popular minera en Ibagué, de acuerdo con el decreto emitido por el Alcalde Guillermo Alfonso Jaramillo, **la población de esa ciudad podrá definir si aprueba o no la realización de proyectos mineros el próximo el 2 de octubre.**

La definición de esta fecha, implicó todo un trabajo por parte del Comité Ambiental del Tolima, que siempre estuvo acompañado de una fuerte movilización social en rechazo a estas actividades por parte de la multinacional **Anglo Gold Ashanti, que pretende realizar minería a cielo abierto para la extracción de oro.**

Tras la elección de Jaramillo como alcalde de Ibagué, el trámite de la consulta popular se materializó y el concejo de la ciudad la aprobó con 10 votos a favor, pese a las amenazas de sanciones por parte del Procuraduría hacia los concejales, y en medio de los ataques por parte del gobierno. Posterior a esa aprobación, y luego de cuatro meses de estudio, **el Tribunal Administrativo del Tolima, le dio su visto bueno de constitucionalidad.**

Después de emitirse el decreto de parte de la Alcaldía para que se realice la consulta, queda esperar que la Registraduría se pronuncie y proceda a tramitar los cerca de **600 millones de pesos que costaría** el llevar a cabo este mecanismo de participación popular, que deberá cumplir con **un umbral de 130 mil votos, es decir, que para que gane el NO a los proyectos mineros en esa región, ** se necesitará la mitad más uno.

De acuerdo con Alfonso Jaramillo y grupos ambientales, el proyecto de Anglo Gold Ashanti con el que esperan extraer 29 millones de onzas de oro desde 2020, afectaría la cuenca del río Coello, una de las principales fuentes hídricas de la ciudad y de otros municipios. Además, el alcalde afirma que la multinacional miente todo el tiempo, pues aunque en meses atrás se había dicho que la empresa había detenido sus actividades, lo cierto es que todavía **tiene más de 42 títulos mineros y además ha participado en las demandas que buscan impedir la consulta popular.**

**“Lo que queremos es que cualquier tipo de explotación deba pasar por una revisión exhaustiva del municipio**, pues somos nosotros los que decidimos sobre el uso del suelo y nosotros no estamos interesados en ese factor de desarrollo, no lo queremos, no lo necesitamos, no hemos vivido de eso”, expresa el alcalde de Ibagué, quien explica que la ciudad nunca ha recibido algún tipo de regalías por parte de estas explotaciones, pues la vocación de esa zona es netamente agrícola, por lo que es la principal despensa del departamento del Tolima.

<iframe src="http://co.ivoox.com/es/player_ej_12490812_2_1.html?data=kpehm5WcdZOhhpywj5eXaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncajpytHZx9fRs4y1zcvc0NjTb6vV08bay9HQs4ampJCuzsjFsMXZjK7Pw8zZqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
