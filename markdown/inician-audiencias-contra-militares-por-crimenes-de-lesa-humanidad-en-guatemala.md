Title: Inician audiencias contra militares por crímenes de lesa humanidad en Guatemala 
Date: 2016-02-03 11:26
Category: Nacional
Tags: Guatemala, Sepur Zarco
Slug: inician-audiencias-contra-militares-por-crimenes-de-lesa-humanidad-en-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Guatemala-sepur-zarco-e1454516619527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Acoguate 

<iframe src="http://www.ivoox.com/player_ek_10302370_2_1.html?data=kpWgkpeXe5Ghhpywj5aWaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncariysjWw9OPpdbYysrbxc7Ft4zX0NPh1MaPscrgytnO1MrXb9Hj04qwlIqldMTmhqigh6aoscbixtiSpZeJhZHYxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Andrea Ixchíu] 

###### [3 Feb 2016 ]

[Este lunes inició en la ciudad de Guatemala el juicio con el que se pretende sean sancionados el Teniente Coronel Esteelmer Francisco Reyes Girón y el Comisionado Militar Heriberto Valdez Asig acusados de los **delitos de desaparición forzada, asesinato, violación sexual y esclavitud doméstica cometidos contra 15 mujeres Maya Q’eqchi**, en 1982 en un destacamento militar conocido con el nombre de ‘Sepur Zarco’.]

[De acuerdo con la periodista Andrea Ixchíu, este batallón “fue utilizado como un área de recreación” en el que estas mujeres indígenas fueron abusadas sexualmente por militares de todos los rangos, así como también obligadas a lavar ropa y a cocinar sin contar con algún tipo de remuneración. Varias de ellas fueron **sometidas a practicarse abortos en contra de su voluntad, otras tantas fueron torturadas e incluso algunas mutiladas**.]

[Los asesinatos, la quema de viviendas, las prácticas de tortura y las desapariciones forzadas fueron usadas junto con la violencia sexual como **“una estrategia contrainsurgente y de terror contra las poblaciones indígenas”**, en un contexto agudo del conflicto armado guatemalteco que llevó a estas mujeres a quedar viudas y a disposición de los militares, quienes según afirma la periodista, están implementando “estrategias de litigio malicioso para retrasar las audiencias” que actualmente se llevan a cabo.]

[En este ciclo de audiencias se evaluaran testimonios e investigaciones que permitirán juzgar la responsabilidad militar en la comisión de delitos de lesa humanidad contra estas mujeres que durante treinta años guardaron silencio porque temían represalias contra sus vidas e impunidad en sus casos. Pese a ello insistieron en **“sentar un precedente legal para impedir que estos hechos vuelvan a ocurrir”** y actualmente enfrentan a “las estructuras militares vinculadas al crimen organizado que aún siguen vigentes en Guatemala”, asevera Ixchíu]

[**“En Guatemala la impunidad sigue siendo grave, la violencia contra las mujeres no para”** asevera la periodista guatemalteca quien concluye que “cuando violan a una mujer violan a toda la sociedad, porque el machismo y el patriarcado no permiten construir la paz, desvalorizan a las mujeres y refuerzan la **lógica de la guerra, en la que por ser mujer está permitido que te asesinen o te desaparezcan**”.]

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) ] 
