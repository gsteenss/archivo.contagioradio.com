Title: Los puntos en el congelador de los acuerdos de paz de la Habana
Date: 2016-05-04 15:43
Category: Otra Mirada, Paz
Tags: Acuerdos de paz en Colombia, Conversaciones de paz de la habana, FARC, Juan Manuel Santos
Slug: los-puntos-en-el-congelador-de-los-acuerdos-de-paz-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/congelador-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lasillavacia] 

###### [4 May 2016] 

Estos son los puntos que han quedado en el llamado congelador en medio de los acuerdos de paz y que deberían ser discutidos antes de la firma de un acuerdo final. Sin embargo ya surjen propuestas para trabajarlos, una de ellas es la que hace el abogado Enrique Santiago en la que afirma que estos puntos deberían ser discutidos como partida de la discusión de una Asamblea Constituyente.

### [**Tierras y desarrollo agrario integral**] 

Latifundio y delimitación de la propiedad: Erradicación del latifundio improductivo, inadecuadamente explotado u ocioso y redistribución democrática de la propiedad sobre la tierra.

Freno a la extranjerización del territorio: Se prohibirá la compra de tierras en grandes extensiones por parte de empresas transnacionales. Se podrán considerar excepciones,  
estableciendo límites a tales compras. En ningún caso se podrá afectar la soberanía alimentaria. Se desestimará toda compra destinada a proyectos de agrocombustibles, o que implique cambios en el uso de la tierra que entren en detrimento de la soberanía alimentaria y de la sostenibilidad ambiental.

Extracción minero-energética y conflictos de uso de la tierra: Solución de los conflictos de uso del territorio, en beneficio de la sociedad y la soberanía nacional. Declaración del carácter estratégico de los principales recursos naturales energéticos y mineros y recuperación de éstos en cabeza de la nación mediante la redefinición de las modalidades contractuales que han dado lugar a su usufructo en condiciones leoninas por inversionistas extranjeros.

Reformulación del régimen de regalías elevando de manera significativa el porcentaje de recursos a favor de la nación; eliminación del régimen de incentivos tributarios a inversionistas extranjeros. Delimitación territorial precisa de las economías de extracción de recursos naturales no renovables y aprovechamiento de los recursos recuperados en función del interés nacional, del buen vivir de la población. Suspensión indefinida del otorgamiento de nuevos títulos mineros y de nuevas concesiones para la exploración y la extracción petrolera hasta tanto no se establezca, mediante estudios previos y consulta previa con las respectivas poblaciones, la viabilidad y sostenibilidad socioambiental de tales actividades; regulación estricta o prohibición, según el caso, de la gran minería a cielo abierto; prohibición de manera efectiva de la explotación minero-energética en zonas de páramos, con ecosistemas frágiles y de reserva agroecológica. Imposición de obligaciones de reparación integral a las víctimas de la depredación socioambiental minero-energética, en cabeza de las empresas concesionarias y con responsabilidad compartida por el Estado. Acompañamiento y protección especial a la minería artesanal, contribuyendo a preservar su sostenibilidad socioambiental. Ello supone garantizar los derechos de los mineros artesanales y tradicionales, así como el respeto por su cultura; reconocimiento y legalización de los títulos mineros en manos de pequeños y medianos mineros. Terminación de la persecución y la criminalización de esta actividad; definición de regulaciones específicas por parte del Estado.

Regulación de la explotación del territorio para la generación de agro-combustibles: En el caso de los cultivos de larga duración, orientados a la producción de agrocombustibles, su localización, sus límites territoriales o su prohibición, según el caso, se fijarán atendiendo la sostenibilidad ambiental, la afectación de fuentes de agua y los requerimientos de abastecimiento alimentario. Explotaciones agrícolas desarrolladas con tecnologías intensivas en utilización de agroquímicos serán desestimuladas.

Revisión y renegociación de los Tratados de Libre Comercio contra la economía: Mientras se surten los efectos de la denuncia, o revisión de los tratados de libre comercio, se pondrán en marcha medidas de incentivo, protección y subsidio a la producción interna de alimentos y, en especial, a la producción campesina, indígena y afrodescendiente.

Ajustes al Ordenamiento Territorial: Definición de una nueva ley de ordenamiento territorial. Ordenamiento social y ambiental, democrático y participativo del territorio; reordenamiento territorial y usos de la tierra para la soberanía alimentaria y el abastecimiento nutricional y alimentario de la población. Sostenibilidad socioambiental, derecho al agua y protección de las fuentes hídricas y de los ecosistemas frágiles. Redefinición de territorios de producción agrícola y de alimentos; creación de nuevos asentamientos rurales para la producción agrícola y de alimentos. Explotación de los recursos naturales no renovables en función del interés nacional y del buen vivir de la población; minería artesanal digna, y pequeña y mediana minería con sostenibilidad socioambiental; sustitución de los usos ilícitos de los cultivos; catastro alternativo del desplazamiento y el despojo.

Financiación de la política de Desarrollo Rural y Agrario Integral: Con miras a garantizar su estabilización y proyección, así como los fines para los cuales han sido creados, los territorios campesinos contarán con recursos de origen constitucional, con destinación específica para ellos. Sin perjuicio de otras fuentes, los recursos provendrán del sistema general de participaciones, de las regalías, de una destinación específica creada de un porcentaje del impuesto al latifundio improductivo, ocioso o inadecuadamente explotado, así como de una cuenta específica creada dentro del presupuesto general de la nación, con un porcentaje fijo sobre el total. Todos estos recursos conformarán el Fondo nacional de financiación de territorios campesinos. Los criterios de asignación de recursos, así como el monto a asignar, serán definidos de manera concertada con las comunidades campesinas y sus organizaciones, las cuales administrarán autónomamente los recursos asignados de acuerdo con la localización geográfica, habrá fuentes adicionales de financiación provenientes de un porcentaje fijo de los presupuestos de los departamentos y municipios en donde se halle el respectivo territorio campesino.

Cuantificación del Fondo de Tierras: Definición del número de hectáreas que se incluirán en el Fondo de Tierras y las fuentes de financiación del proyecto. En nuestra propuesta el fondo estará conformado, por al menos 20 millones de hectáreas de tierras provenientes de latifundios improductivos, ociosos o inadecuadamente explotados, tierras baldías, tierras apropiadas mediante el uso de la guerra y el despojo, tierras incautadas al narcotráfico.

Creación del Consejo Nacional de la Tierra y el Territorio: Encargado de trazar y establecer pautas generales de ordenamiento territorial, de definir los usos de la tierra, así como de los conflictos que de ella se deriven. Además de los representantes de los poderes públicos y de los organismos de control, de los gremios sectoriales, el Consejo se conformará con representantes de las comunidades de los territorios campesinos, indígenas, afrodescendientes, raizales, palenqueros, interétnicos e interculturales, así como de las organizaciones campesinas y de trabajadores agrícolas, escogidos por ellas mismas.

Definiciones sobre el derecho real de superficie: Se establecerán regulaciones especiales para impedir el acaparamiento de tierras en manos de empresas transnacionales, o de grupos económicos, o de grandes empresarios nacionales, o de alianzas o asociaciones entre ellos. De manera especial, se implementarán medidas contra la especulación financiera de la tierra y el territorio. No se permitirá el derecho de superficie, según las características que apuntan a propiciar el despojo “legal” de la tierra y la descampesinización del campo.

### [**Participación Política**] 

1\. Reestructuración democrática del Estado y la reforma política en función de la expansión democrática. (Reafirmación de la soberanía; reconocimiento y apoyo a las formas de participación social y ciudadana que fortalecen la democracia directa; descentralización en función de la democracia local; reconversión de las Fuerzas Militares hacia la construcción de la paz y la protección de la soberanía nacional; reforma económica; reforma de la rama judicial que libere a la justicia de su politización y corrupción y le devuelva su independencia como rama del poder).

Revisión, reforma y democratización del sistema político electoral. (Creación del poder electoral implementando su modernización y tecnificación; reconfiguración de las circunscripciones electorales; conformación de una Cámara Territorial, que sustituya a la  
actual Cámara de Representantes; fortalecimiento de las circunscripciones especiales de comunidades y pueblos indígenas y afro descendientes, ampliando sus actuales niveles de representación; y creación de circunscripciones especiales para los campesinos).

Revisión y reforma de mecanismos de participación ciudadana (ley 134 de 1994) en lo concerniente a la eliminación de restricciones y limitaciones y al mayor reconocimiento de la iniciativa popular.

Proscripción del tratamiento militar a la movilización y la protesta. (Desmantelamiento del ESMAD; y revisión y modificación de las leyes de seguridad ciudadana).

Elección popular de los organismos de control de la Procuraduría y la Contraloría; Fiscalía General y la Defensoría del Pueblo, con base en propuestas programáticas.

Participación ciudadana en la definición de políticas de interés nacional, tales como las relaciones exteriores, la seguridad y defensa nacional o la administración de justicia.

Democratización del acceso al espacio radioeléctrico, la información y la comunicación, que impida la monopolización de los medios masivos de comunicación. (Democratización de su propiedad, del acceso a la tecnología de la información y las comunicaciones; fortalecimiento del carácter público y comunitario; mejoramiento de las condiciones laborales de los trabajadores de la

Ordenamiento territorial y estímulo a la participación de las regiones, los entes territoriales y los territorios. (Conformación del Consejo de la Participación Territorial en el que tengan asiento representaciones de las regiones, de los departamentos y municipios, y de los territorios campesinos, indígenas y afro descendientes; creación de las regiones, las provincias y distritos especiales como entidades territoriales; elevar el departamento del Chocó a categoría de entidad territorial especial).

Participación y control social y popular en el proceso de la política pública y de la planeación y, en especial, de la política económica, en los niveles nacional, regional, departamental y municipal. Política macroeconómica para el buen vivir de la población y un verdadero Estado social de derecho.

Participación social y popular en los organismos del Consejo Nacional de Política Económica y Social, CONPES, del Consejo Superior de Política Fiscal, CONFIS, y de la Junta Directiva del Banco de la República; contraloría social a los planes de desarrollo y presupuestos públicos; rediseño del régimen de sostenibilidad fiscal y de autonomía de la banca central.

Democratización y garantías de participación, reordenamiento territorial y descentralización para la justicia social urbana. (Establecimiento del Fondo de Compensación para superar las desigualdades sociales, la pobreza, el hambre y la miseria en las ciudades; Consejo Nacional de la Participación Política y Social con réplicas departamentales y municipales con representación de los estamentos de la sociedad).

Reconocimiento de derechos políticos que garanticen la participación política y social de comunidades campesinas, indígenas y afro descendientes, así como de otros sectores sociales excluidos, extendiendo la consulta previa y estableciendo del Poder Popular. (Se adoptará la “Declaración de los Derechos de los Campesinos” de la Organización de Naciones Unidas).

Participación social y popular en procesos de integración de Nuestra América, especialmente en la CELAC y la UNASUR.

Control social y popular y veeduría ciudadana sobre los tratados y acuerdos suscritos por el Estado colombiano, que incluya la denuncia de tratados y convenios suscritos por el Estado colombiano; medidas transitorias de protección frente a tratados y convenios que vulneren los derechos de la población.

### [**Cultivos ilícitos, narcotráfico y lavado de activo**] 

Nueva política criminal. En el proceso de redefinición de la política anti-drogas se debe proceder con el diseño de una nueva política criminal del Estado que concentre sus esfuerzos en la persecución y el encarcelamiento de los principales beneficiarios del mercado de drogas ilícitas, así como en el desmantelamiento de las redes transnacionales de tráfico y de lavado de activos.  
Las FARC-EP insisten en la conformación de la “Comisión para el diseño de una política nacional antidrogas democrática y participativa”, con grupos de trabajo de académicos y expertos que formule los lineamientos generales para esa nueva política criminal del Estado en esta materia. Sobre todo en momentos en que el país entero cuestiona el sistema judicial que rige.

Suspensión inmediata de las aspersiones aéreas con glifosato y reparación integral de sus víctimas.

Las FARC-EP consideran que en desarrollo de los lineamientos generales de la Nueva política antidrogas se debe proceder con la suspensión inmediata de las aspersiones aéreas con glifosato, o cualquier otro agente químico, y con la reparación integral de sus víctimas. Esto implica:  
– La identificación de las víctimas de las aspersiones aéreas con agentes químicos.  
– La reparación integral de las víctimas de las aspersiones aéreas con agentes químicos.  
– El establecimiento de un fondo para la reparación de las víctimas de las aspersiones aéreas con agentes químicos.

En definitiva, el viejo esquema de erradicaciones forzadas-fumigaciones, ha fracasado generando enormes daños al tejido ambiental y social, lo cual implica que deben buscarse alternativas urgentes.

Frente al punto 4.2.: -Reconociendo la importancia de lo acordado en este sub-punto, las FARC-EP consideran necesario concretar el compromiso de transformación estructural del sistema de salud pública, que permita encuadrar el desarrollo del programa y de los planes.

Frente al punto 4.3: -Las FARC-EP, consideran que es necesaria la realización, a instancias de la Mesa de Diálogos, de una Conferencia Nacional sobre política soberana de lucha contra las drogas, a fin de avanzar, también, en la concreción de los ajustes y adecuaciones normativas y de las acciones que requiere esta lucha, considerando las nuevas tendencias internacionales que enfatizan en el enfoque en derechos humanos, al momento de enfrentar el fenómeno de producción, consumo y comercialización de drogas ilícitas. Una de las tareas principales de esta Conferencia Nacional, debe ser la de analizar y arrojar conclusiones sobre el problema de la comercialización y producción de drogas ilícitas, como fenómeno ligado al paramilitarismo. La Conferencia también deberá abordar el asunto concerniente a la relación entre conflicto, narcotráfico e impacto en la institucionalidad.
