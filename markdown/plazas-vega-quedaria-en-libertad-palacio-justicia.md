Title: Es clara la responsabilidad de Plazas Vega en la desaparición de personas en Palacio de Justicia: Abogados
Date: 2015-12-14 15:35
Category: Judicial, Nacional
Tags: Alfonso Plazas Vega, CIDH, Comisión de Justicia y Paz, Corte Suprema de Justicia, Desaparecidos del Palacio de Justicia
Slug: plazas-vega-quedaria-en-libertad-palacio-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/plazas_vega_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

<iframe src="http://www.ivoox.com/player_ek_9722508_2_1.html?data=mpyflJqUfI6ZmKiak5WJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidSfxNHO1MaPsMKf08rg0tTSt8LWytHWxsbIb8XZjLXZw9%2FFt4zKxszOjcrSb83VjMnS1cbUpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Liliana Ávila, Justicia y Paz] 

###### [12 Dic 2015]

La **Corte Suprema de Justicia** anunciaría esta semana la decisión en torno al proceso de casación interpuesto por la defensa del **Coronel retirado Alfonso Plazas Vega**. Según la versión de Noticias Uno y de Liliana Ávila, representante de las víctimas, el sentido del fallo es absolutorio, sin embargo se espera que la decisión sea anunciada y resalta que el testimonio del Cabo Villamizar no es la única prueba que ha sido analizada por los tribunales.

Para la abogada, Plazas Vega comandó la operación de retoma del [Palacio de Justicia](https://archivo.contagioradio.com/page/2/?s=palacio+de+justicia) y tenía conocimiento de las órdenes que conllevaron a la desaparición forzada de **Carlos Rodríguez e Irma Franco**, “como se ha demostrado en los tribunales”  que han estudiado el caso. Además Ávila afirma que conocen de la presión política a la que ha estado sometido el caso que se ha considerado de interés internacional.

Hace  cerca de un año la **Corte Interamericana de Derechos Humanos condenó al Estado colombiano** por no investigar y esclarecer los hechos relacionados con la toma y retoma del Palacio de Justicia y según la sentencia le ordenó al Estado que se haga todo lo necesario para establecer justicia y verdad, sin embargo, si la decisión de la CSJ es favorable a Plazas se estaría evidenciando que Colombia sigue burlando a los tribunales internacionales concluye la abogada.

Respecto de ese fallo [hay una deuda grandísima con las víctimas](https://archivo.contagioradio.com/perdon-sin-verdad-no-es-perdon-familiares-de-las-victimas-del-palacio-de-justicia/), no se han establecido las identidades y responsabilidades en la cadena de mando de las FFMM, no se han esclarecido los hechos, no se han pagado las indemnizaciones, no se han hecho las publicaciones que ordena la Corte IDH. Deuda que se agrandaría en caso de una decisión desfavorable a los intereses de las víctimas.

Según la información dada a conocer, la Corte Suprema de Justicia revelaría su decisión esta semana, antes de la entrada en vigencia de la "vacancia judicial". Ya se conoce que la ponencia es favorable al Coronel Plazas Vega y al parecer la votación estaría 6 a favor de la absolución y 2 votos en contra.
