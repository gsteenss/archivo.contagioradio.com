Title: Bernarda Alba en blanco y negro
Date: 2018-03-01 16:55
Category: eventos
Tags: Bogotá, prisioneras, teatro
Slug: bernarda-alba-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/BERNARDAENB-N-5-e1519937761820.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bernarda Alba en blanco y negro 

###### 01 Mar 2018

Del 1ro al 24 de marzo llega al Teatro Charlot de Bogotá Bernarda Alba en Blanco y negro, de la directora y dramaturga Victoria Hernández y su compañía La puerta abierta, un montaje inspirado por la obra del poeta español Federico García Lorca, que, a partir de la historia de 3 reclusas, aborda las problemáticas que viven las mujeres en las cárceles.

La obra que fue montada inicialmente con el grupo de teatro de la cárcel de mujeres El buen pastor, del cuál Hernández era directora, presenta en esta ocasión un elenco conformado por reconocidas actrices como son Cony Camelo, Emilia Caballos, Maria Adelaida Palacios, Angélica Blandón, Dalyn Valdivieso, Alejandra Chamorro, Viviana Sierra, Gianina Arana y Bárbara Perea como Bernarda Alba.

En su experiencia de 3 años de trabajo en prisión, Hernández encontró en el teatro una herramienta para que las reclusas lograran exorcizar su dolor, creando una nueva historia inspirada en "La casa de Bernarda Alba", sintetizando en tres personajes, tres reclusas, las problemáticas de las mujeres en las cárceles.

Desde allí aborda la vida privada de la libertad, las relaciones que se tejen en situaciones de encierro y el poder de aquellos que ejercen como cuidadores. Todo ese círculo de represión que vive la mujer, tema fuerte en la obra en que se inspira esta pieza, se completa con la presencia de la guardiana, quien representa la moral, pero a la vez la impotencia frente a la injusticia y la directora del penal, Bernarda Alba, una figura postiza, reflejo de nuestra sociedad en donde lo importante es aparentar y no enfrentar la realidad.

[![bernarda](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/fcbkportada11-800x292.png){.alignnone .size-medium .wp-image-51870 width="800" height="292"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/fcbkportada11.png)

*“*La población carcelaria es grande y todas tienen historias diferentes, por eso las agrupé en tres mujeres representativas que las pudieran resumir a todas, y decidí hacerlo desde el espíritu lorquiano; me fui a Lorca directamente: una de ellas es Lorca el revolucionario; otra es Lorca el que quiere comerse el mundo, el poeta, y la otra es Lorca el hombre débil y rechazado, el diferente, el castrado por la iglesia*” *afirma Victoria, pero además la obra  se llama en blanco y negro porque en una cárcel no hay nada cálido, no hay nada intermedio.

Las cárceles son ese lugar donde no queremos mirar, nos avergüenzan y esta obra nos traslada a una de ella, a través de la puesta en escena ejecutada por el director de arte Miguel Goyeneche, donde los únicos colores son el blanco, el negro y los grises, que junto con las luces de neón y los sonidos producidos por las cadenas y las rejas dan un ambiente frío, metálico, como la música desgarrada que en algún momento suena.

El maquillaje en las actrices, la propuesta de vestuario, la paleta, la iluminación, cada detalle pensado minuciosamente para que el público tampoco pueda escapar y por un momento conozcan parte de lo que ocurre en una cárcel y los seres humanos que allí habitan.

Del primero al 24 de marzo, de jueves a sábado a las 8:00 p.m.

Bono de apoyo: \$ 30.000 general \$20.000 Estudiantes y tercera edad.

Teatro Charlot: Calle 70A \# 9-51
