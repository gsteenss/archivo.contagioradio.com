Title: ECOMÚN la primera cooperativa de las FARC aún no tiene los recursos prometidos
Date: 2017-07-04 12:27
Category: Economía, Nacional, Paz
Tags: acuerdos de paz, ECOMÚN, FARC-EP
Slug: ecomun-la-primera-cooperativa-de-las-farc-aun-no-tiene-los-recursos-prometidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/images-cms-image-0000609991.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Confidencial Colombia] 

###### [04 Jul 2017] 

El proyecto de ECOMÚN, que hace parte de los acuerdos de paz y que permite la reincorporación de los integrantes de las FARC-EP a la sociedad, podría **estar en riesgo si el Estado no cumple con los aportes con los que se había comprometido**, así lo denuncio Andrés Paris, miembro del secretariado de esta guerrilla.

De acuerdo con Paris, hasta el momento solo se estableció la personería jurídica de ECOMÚN, sin que aún el Estado haya otorgado los dineros correspondientes para poner en marcha esta cooperativa, que tiene como objetivo es **agrupar las posibilidades económicas de los integrantes de las FARC pactadas en el acuerdo y administrarlas colectivamente**.

"**Éramos un colectivo insurgente, hoy nos transformamos en un colectivo político, social, legal, ajustado a las leyes**" aseguró París. (Le puede interesar: ["Las preocupaciones de las FARC-EP luego de la dejación de armas total"](https://archivo.contagioradio.com/sin-armas-farc-exige-materializacion-de-los-acuerdos/))

Para el vocero de las FARC hay una preocupación latente, “No se viene cumpliendo con las implicaciones jurídicas, pues mucho menos van a cumplir con lo económico, que además se cruza con todo el andamiaje de la corrupción. **Ya se había denunciado la corrupción en las construcciones de las zonas veredales y esperamos qué pasará con lo económico**”.

Sin embargo afirmó que lucharán para que el gobierno cumpla esta exigencia "será el resultado de la lucha y la movilización de nosotros, no nos hacemos ningún tipo de ilusión, los pobres solamente tendrán felicidad sí es resultado de su lucha organizada".

### **Así funcionará ECOMÚN** 

La cooperativa Economías Sociales del Común (ECOMÚN) es producto de los acuerdos de paz, firmados en el Teatro Colón, bajo cuatro líneas de acción: la sociedad y el proceso de reincorporación que se gestará para la vida de los ex combatientes, la cultura y el respeto a la diferencia, la economía y que los proyectos que surjan de **ECOMÚN sean productivos y la ambiental y la generación de trabajo sin producir afectaciones a la naturaleza**.

En esta medida Andrés Paris expresó que “nosotros **estamos hablando de proyectos productivos con espíritu colectivo, estamos proyectado con mucha fuerza proyectos ecológicos**, queremos recoger toda la preocupación de la humanidad por el medio ambiente y unirla con su preocupación por la subsistencia”. (Le puede interesar: ["Presos políticos de las FARC-EP exigen que se cumpla Ley de Amnistía Páctada"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos/))

Hasta el momento hay 37 integrantes de las FARC-EP que terminaron el primer curso de capacitación de ECOMÚN y que funcionaría en las zonas veredales transitorias, que **tendrán que ser configuradas y estructuradas para ser útiles a los proyectos productivos** que también estarán coordinados con las comunidades en las que hacen presencia.

ECOMÚN tendrá la labor de administrar las iniciativas económicas que recibirán apoyos de hasta 8 millones de pesos y distribuirlas como está estipulado en los acuerdos de Paz. **Esta organización deberá presentar informes periódicos de la ejecución de recursos provenientes del Estado**. (Le puede interesar: ["Nos Unimos todos y ganamos o nos jodemos todos: Andrés París"](https://archivo.contagioradio.com/o-nos-unimos-todos-y-ganamos-o-nos-joden-a-todos-andres-paris/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
