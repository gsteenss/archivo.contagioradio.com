Title: En apoyo a la Minga documental "Sangre y Tierra" disponible online
Date: 2019-03-21 16:45
Category: 24 Cuadros
Tags: Cauca, documentales colombianos, indígenas, Minga
Slug: en-apoyo-a-la-minga-documental-sangre-y-tierra-disponible-online
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/567236176_1280x720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sangre y Tierra 

###### 21 Mar 2019 

El documental colombiano **Sangre y Tierra** que fue estrenado en 2015 ahora se encuentra de libre acceso online en apoyo a Minga que adelantan los pueblos del Cauca, en el sur occidente del país. \#YoApoyoLaMinga

Esta creación visual del director Ariel Arango Prada muestra **una retrospectiva histórica del movimiento indígena**, que busca dar cuenta de las luchas del pueblo Nasa en el Norte del Cauca.

Entre 2015 y 2016 varios apartados del territorio que habitan los Nasa cayeron en manos de las grandes industrias azucareras y principales productores de etanol en Latinoamérica. Sin quedarse de brazos cruzados, **el pueblo indígena ha hecho evidente la defensa por la autonomía de su territorio**, la jurisdicción especial indígena, la movilización social y la conservación de diversas manifestaciones de su cultura ancestral.

Les invitamos a ver el documental y compartirlo, en solidaridad y reconocimiento con la resistencia del pueblo Nasa por la defensa de sus derechos.

<iframe src="https://player.vimeo.com/video/160917960" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[SANGRE Y TIERRA - Resistencia Indígena del Norte del Cauca \[DOCUMENTAL\]](https://vimeo.com/160917960) from [entrelazando](https://vimeo.com/entrelazando) on [Vimeo](https://vimeo.com).
