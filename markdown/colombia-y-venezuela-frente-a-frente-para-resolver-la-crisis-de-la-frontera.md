Title: Colombia y Venezuela frente a frente para resolver la crisis de la frontera
Date: 2015-09-21 12:49
Category: Nacional, Política
Tags: crisis fronteriza, Encuentro de presidentes de Colombia y Venezuela, Juan Manuel Santos, Nicolas Maduro
Slug: colombia-y-venezuela-frente-a-frente-para-resolver-la-crisis-de-la-frontera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/maduro-y-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.t13.cl 

###### [21 sep 2015] 

En la ciudad de Quito Ecuador se realizará el día de hoy a las 2 de la tarde **el encuentro de los presidentes de Colombia Juan Manuel Santos y Venezuela, Nicolás Maduro** quienes t**rataran la situación que perjudica la frontera de ambos países, acordando soluciones inmediatas.**

Lo que se busca es que a partir del diálogo se pueda frenar las agresiones a miles de colombianos que fueron víctimas quedando atrapados en el vecino país.

Sin embargo, el Presidente colombiano asegura **que este encuentro no será suficiente para vencer el conflicto que existe entre ambos países** como es el paramilitarismo, el narcotráfico y el contrabando de extracción y a través de su cuenta en Twitter afirmó que viaja a Quito con disposición pero sin mucas expectativas lo cual ha generado reacciones negativas porque reduce las posibilidades de una solución pronta, según diversos analistas.

Nicolás Maduro en cambio mostró optimismo ante este encuentro y afirmó que está dispuesto a firmar el pacto de paz de convivencia con Colombia. **“Estoy listo para firmar el pacto de paz de convivencia con nuestra hermana Colombia”, manifestó el jefe de Estado.**

El mandatario venezolano insiste a Santos en que para lograr el objetivo ambas naciones se deben unir para eliminar el paramilitarismo, el tráfico ilegal de alimentos y combustible que perjudica la economía venezolana.
