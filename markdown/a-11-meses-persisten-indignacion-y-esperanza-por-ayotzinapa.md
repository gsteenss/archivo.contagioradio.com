Title: A 11 meses persisten indignación y esperanza por Ayotzinapa
Date: 2015-08-27 12:36
Category: El mundo, Entrevistas, Movilización
Tags: 15 jornada movilización en Ayotzinapa, 43 desaparecidos, Ayotzinapa, impunidad, mexico, Movilización social, Normalistas de Guerrero
Slug: a-11-meses-persisten-indignacion-y-esperanza-por-ayotzinapa
Status: published

###### Foto: Ayotzinapa Vive 

<iframe src="http://www.ivoox.com/player_ek_7636648_2_1.html?data=mJugmJuYfI6ZmKiak5aJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhYylkpDax9jJt4zkxtfgy9jYqc%2BfytPRy8zSpcTdhqigh6eXsoztjMrg0srWpc%2FuwpDd0dePhdrj1d%2Bah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alina Duarte, Revolución 3.0] 

###### [27 Ago 2015]

El día de ayer se realizó una nueva jornada de movilización mundial por el caso de la desaparición de los 43 normalistas de Ayotzinapa. Las protestas se dan después de 11 meses sin respuestas sobre el paradero de los estudiantes de la Normal Rural de Ayotzinapa.

La movilización se dirigió hacia el ángel de la independencia y la plaza del Zócalo en donde se pronunciaron distintos voceros encabezados por los padres de los estudiantes quienes han calificado este caso como “Crimen de estado”. Lastimosamente al final de la jornada la policía arremetió contra la movilización, dejando varios heridos.

La periodista mexicana Alina Duarte, integrante del equipo de Revolución 3.0, comentó que miles de ciudadanos y distintas organizaciones salieron con destino hacia 26 embajadas, incluyendo las de la Unión Europea “*para pedir que estas hagan presión al gobierno mexicano para esclarecer el caso de Ayotzinapa*”.

El Estado y los distintos entes investigadores mexicanos, dieron el caso por cerrado denominándolo como la “verdad histórica” a pesar de lo que ha indicado la (CIDH) frente al caso y las exigencias de la ciudadanía y los padres de familia que no han claudicado de su búsqueda. Vea también [No cesan las jornadas mundiales por Ayotzinapa](https://archivo.contagioradio.com/?s=ayotzinapa)

**Una crisis de Derechos Humanos:**

En un inicio hace 11 meses los padres de los normalistas de Ayotzinapa, denunciaron que el entonces gobernador de Guerrero, Ángel Aguirre, ofreció cuantiosas sumas de dinero para que se olvidaran de sus hijos, hecho que por supuesto aumentó la indignación.

Recientemente la CIDH presentó pruebas sobre la desaparición de vídeos y registros que comprometen al ejercito y al Estado en la comisión del crimen.

Alina Duarte indicó que a pesar de que “el clima es bastante tenso” por casos como el asesinato de Jiménez Blanco quien encabezaba las búsquedas de los normalistas y poseía bastante información sobre el caso, los padres no han desistido de sus exigencias.

Hoy México presenta un panorama desalentador, la frente a la confianza de los ciudadanos en el Estado, una crisis en cuanto a los derechos humanos, vínculos del narcotráfico y presenta a un presidente que no ha estado a la altura del país.
