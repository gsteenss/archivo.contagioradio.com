Title: En la Orinoquía empresas expropian a 13 comunidades indígenas
Date: 2018-12-03 11:20
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Orinoquía, territorios indígenas
Slug: empresas-expropian-13-comunidades-denuncian-indigenas-orinoquia
Status: published

###### Foto: Contagio 

###### 03 Dic 2018 

El pasado 30 de noviembre diversos líderes y representantes de comunidades indígenas de **Casanare, Meta y Vichada** se dieron cita en el foro **‘La Orinoquía en Emergencia’** para analizar y denunciar las problemáticas que afectan a las poblaciones de esta región del país y exigir al Estado que responda por sus derechos.

Durante el evento desarrollado en la Universidad Javeriana, diversos delegados de los pueblos **Sikuani, Saliba, Yaruros, Piapoco, Cubeo, Cuiba Waupijiwi, Maiben Masiware y las veredas Porvenir Inspección y Matarratón,** denunciaron la expropiación y reducción de sus territorios justificada por empresas y políticos con títulos formalizados, quienes continúan acumulando hectáreas para sus actividades.

**Violencia y expropiación**

En el encuentro se resaltó la fuerte presencia de actores armados ilegales y la posterior aparición de multinacionales que expropian y limitan el espacio de las comunidades indígenas, que ven reducidos sus territorios a pequeñas parcelas o han sido obligadas a desplazarse a las grandes ciudades negándoles la oportunidad de acceder a tierras productivas.

**Ana Villa**, representante de **Cumaribo, Vichada** manifestó que las multinacionales se han aliado con abogados que tramitan títulos protocolizados para apropiarse de las tierras y denunció que “si un campesino no se va a las buenas con esos títulos se va a las malas con los grupos armados pagados por las grandes empresas”. [(Le puede interesar La Orinoquia en Emergencia)](https://archivo.contagioradio.com/orinoquia-en-emergencia/)

**Omisión del Estado**

En el foro, se rechazó la nula participación de los pueblos indígenas en las decisiones que afectan a su comunidad. “El Gobierno no ha puesto sus ojos ni su interés en responder a las necesidades de titulación de las comunidades indígenas, ha flexibilizado la legislación agraria para permitir que las empresas se queden con las tierras” indicó Andrés Fuerte de Oxfam Colombia.

Entre algunas de las compañías y personas señaladas de acumular casi 350.000 hectáreas para su beneficio se encuentran **Corficolombiana, Riopaila, La Fazenda, Mónica Semillas, Poligrow y Pacific Exploration (antes Pacific Rubiales) y el ex senador Alfonso Mattos.**

Ante las políticas excluyentes del Gobierno y la falta de financiamiento en el agro, educación superior, salud, vivienda o servicios públicos, las comunidades no solo deben luchar por la igual de condiciones, algunas también enfrentan la extinción, así lo señaló el líder indígena **Maiben Ignacio Deiwa** al anunciar que **“en el resguardo indígena de Caño Mochuelo, en Casanare, solo quedan 80 habitantes de los tsiripus debido al hambre".**

Como alternativa, los pueblos indígenas de la Orinoquía continúan resistiendo bajo el mandato de permanecer en asamblea para garantizar una mayor representación en defensa de la vida, su territorio, costumbres y saberes.

###### Reciba toda la información de Contagio Radio en [[su correo]
