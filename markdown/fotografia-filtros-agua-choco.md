Title: Gracias a la fotografía filtros de agua llegan a comunidades del Chocó
Date: 2019-04-16 17:32
Author: CtgAdm
Category: Comunidad, Cultura
Tags: AGC, Agua, Chocó, Gaitanistas
Slug: fotografia-filtros-agua-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Fotografía-para-filtros-de-agua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ramón Campos Iriarte] 

La iniciativa ¡No hay agua! nació de un grupo de periodistas que reportan hechos relacionados con el conflicto, y ante el eminente abandono del Estado en ciertas regiones, decidió vender obras  de fotografía para recaudar dinero y traducir esos fondos en filtros de agua y medicina para las comunidades que visitaban. Grandes fotógrafos como Jesús Abad Colorado han donado sus obras a este colectivo, que realiza la subasta de las imágenes en los meses finales de cada año.

Como lo explicó **Ramón Campos Iriarte**, uno de los integrantes de la iniciativa, "para nadie es un secreto que el Chocó es un territorio abandonado por el Estado, y es un sitio que está lleno de oro", en una buena parte del departamento no se garantiza el acceso a servicios públicos y la gente tiene que tomar agua de afluentes contaminados por el uso del mercurio para la extracción de metales preciosos o de agua empozada, situación que genera complicaciones de salud en los niños.

Por lo tanto, el grupo de reporteros decidió comenzar a entregar filtros de agua y medicinas para palear estas situaciones, y porque es una "solución barata y no tan invasiva". Campos sostiene que no quieren suplantar al Estado y por el contrario, reivindican el deber que tiene para proteger la vida de las comunidades, pero mientras eso ocurre, esta es una forma de contribuir para que las personas mejoren de alguna forma su calidad de vida.

### **¿Qué pasó la última vez que estaban entregando los filtros de agua?**

Con los recursos que se recaudaron el pasado diciembre Campos y otros 4 reporteros entregaron la semana pasada 50 filtros en el corregimiento de Negría, en Itsmina (Chocó). Lamentablemente, mientras regresaban por el Río San Juan a este Municipio fueron abordados por dos hombres que se identificaron como integrantes de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), y con armas cortas los intimidaron para que entregaran sus equipos de imagen y vídeo.

Según el testimonio del periodista, los hombres los acusaban de estar haciendo inteligencia para las fuerzas del Estado o el Ejército de Liberación Nacional (ELN), pero los hombres explicaron su labor y pidieron respeto para el mismo. Finalmente, los armados les permitieron seguir su camino, no sin antes amenazarlos, incluso con la muerte del conductor de la embarcación en la que iban. (Le puede interesar:["Líderes de Chocó, sin protección y nuevamente amenazados"](https://archivo.contagioradio.com/lideres-choco-sin-proteccion-nuevamente-amenazados/))

En ese sentido, Campos expresó que el interés de ellos no es ser una amenaza para los grupos, sino ejercer su profesión reportando el daño ambiental que hay y el abandono estatal al que están sometidas las comunidades en la zona, y reiteró el llamado a respetar el derecho "a la libre locomoción por el territorio y la libertad de prensa". (Le puede interesar: ["Comunidad Embera expulsó a cinco paramilitares de las AGC de su territorio en Chocó"](https://archivo.contagioradio.com/comunidad-embera-agc/))

<iframe id="audio_34569768" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34569768_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
