Title: Papa pide acabar el conflicto étnico religioso entre cristianos y musulmanes
Date: 2015-12-01 11:47
Category: El mundo, Política
Tags: conflicto armado étnico religioso entre cristianos y musulmanes, Papa Francisco, República Centroafricana, visita del Papa Francisco a la República Centroafricana
Slug: papa-pide-acabar-el-conflicto-etnico-religioso-entre-cristianos-y-musulmanes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:europapress 

###### [30 nov 2015]

La visita del Papa Francisco a la República Centroafricana tuvo su centro de reflexión en el conflicto   étnico religioso entre cristianos y musulmanes.

La República Centroafricana se encuentra en una situación difícil desde que los ex rebeldes Séléka de mayoría musulmana derrocaron el Gobierno de François Bozizé en 2013 donde provocaron una reacción de las milicias anti-balaka cristianos y animistas.

Catherine Samba Panza, líder del gobierno de transición  ante esta situación pide perdón públicamente **“Confieso todo el mal que se ha hecho aquí en el curso de la historia y pido perdón desde el fondo de mi corazón”.**

Adicionalmente el próximo 13 de diciembre se realizará un referendo constitucional posteriormente  se realizarán las elecciones presidenciales,  se espera que la visita del papa genere menos enfrentamientos entre musulmanes y cristianos para que las próximas elecciones sean en su totalidad seguras.
