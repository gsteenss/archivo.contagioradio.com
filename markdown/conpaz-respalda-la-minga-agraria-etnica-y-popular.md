Title: CONPAZ respalda la Minga Agraria Étnica y Popular
Date: 2016-05-31 15:33
Category: Movilización, Nacional
Tags: etnica y popular, Minga Agraria, Paro Nacional Colombia, Red CONPAZ
Slug: conpaz-respalda-la-minga-agraria-etnica-y-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/conpaz_4_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [31 Mayo 2016] 

La Red 'Comunidades Construyendo Paz en los Territorios' CONPAZ, en su más reciente comunicado extiende el respaldo a la Minga Agraria, Étnica y Popular que inició este lunes por los incumplimientos del Gobierno ante los compromisos pactados en las anteriores movilizaciones y la inconformidad de productores y consumidores, frente a las actuales políticas económicas y ambientales que favorecen los intereses del sector privado.

Teniendo en cuenta que el actual gobierno se proyecta como a favor de las víctimas, desde la Red se exige que actúe con coherencia para que en el marco de este paro evite nuevas víctimas de la violencia estatal. Así mismo, manifiesta su apoyo al pliego de exigencias que incluye el reconocimiento del campesinado como sujeto de derechos, el reconocimiento de las zonas de reserva campesina, los territorios indígenas y afrocolombianos, y las zonas agroalimentarias; derogación de la ley ZIDRES; la defensa de la soberanía y seguridad alimentaria; la participación en la construcción de una Reforma Tributaría; derogación de la Ley de Seguridad Ciudadana; la garantía de los derechos a educación, salud, trabajo, y vivienda digna; la financiación de hospitales y universidades públicas; la defensa del agua como derecho fundamental; la protección de fuentes hídricas; la creación de programas de sustitución gradual de cultivos de coca de manera concertada.

<iframe src="http://co.ivoox.com/es/player_ej_11745955_2_1.html?data=kpaklpqdeZahhpywj5WWaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdfWydSPh8Ln1c7ZztSPcYy3sLO9o7%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
