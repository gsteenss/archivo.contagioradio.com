Title: Colombia y Venezuela logran acuerdo para normalización de frontera
Date: 2015-09-21 19:45
Category: Política
Tags: colombia, Crisis en frontera con Venezuela, Juan Manuel Santos, Nicolas Maduro, Venezuela
Slug: hemos-demostrado-que-con-voluntad-politica-si-se-puede
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/santos_maduro_contagioradio-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: efe 

###### [21 Sep 2015]

Nicolás Maduro y Juan Manuel Santos acordaron el retorno de embajadores, y afirmaron que realizarán una investigación de la frontera para identificar los problemas comunes. También se reunirá el equipo de ministros para tratar temas fronterizos, la progresiva normalización de la frontera, y continuar trabajando con el acompañamiento de Ecuador y Uruguay.

Otro de los acuerdos gira en torno al respeto mutuo  de los dos modelos de gobierno que desarrolla cada uno de los presidentes en sus gobiernos. La lectura del comunicado conjunto se dio por parte de Rafael Correa.

El presidente Nicolás Maduro aseguró que demostraron que “por la vía diplomática si se puede” y que la triunfadora de este encuentro fue la paz. Por su parte Juan Manuel Santos se refirió a la reunión como muy productiva.

Quedan pendientes las fechas de la reapertura de la frontera y de los encuentros de los grupos de trabajo que establecieron los presidentes en la reunión de este Lunes en Quito. A continuación el texto del acuerdo.

[![acuerdo\_santos\_maduro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/CPd5wYiWUAAuBhl.jpg){.aligncenter .size-full .wp-image-14401 width="599" height="670"}](https://archivo.contagioradio.com/hemos-demostrado-que-con-voluntad-politica-si-se-puede/cpd5wyiwuaaubhl/)
