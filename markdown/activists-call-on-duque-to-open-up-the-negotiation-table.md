Title: Activists call on Duque to "open up the negotiation table"
Date: 2019-11-28 11:28
Author: CtgAdm
Category: English
Tags: Alirio Uribe Muñoz, Great National Dialogue, Iván Duque, National Strike Council
Slug: activists-call-on-duque-to-open-up-the-negotiation-table
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-27-at-17.32.29.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Following six days of protests that have rocked the streets of Colombia, President Iván Duque has called for a “Great National Dialogue” to end the civic unrest,  a measure that protesters have denounced does not meet their demands. Some have criticized that the president has met with governors and mayors instead of the leaders of the strike.]

[“It’s absolutely absurd the useless dialogue that the government is proposing. The mayors and governors are not protesting. They do not represent the strike,” said Alirio Uribe Muñoz, spokesperson and representative of human rights organizations.]

[The National Strike Council came together with a government spokesperson for the first time on Nov. 26 for a meeting that ended with no deal. According to Uribe, the government is attempting to force an agenda without first listening to the demands of the citizens marching in the streets. He added that these talks should include a wider range of voices the represent the various factions marching.]

[“We propose to open up the negotiation table. The National Strike Council seems limiting to us with everything that the people are demanding in the streets,” Uribe said. “We need real dialogue.”]

### The demands of the strike

[The number of demands has grown since protests against a tax law reform bill initiated on Nov. 21. “Some of the demands are global such as the right to life, which has to do with the assassination of social leaders, human rights violations committed by the national police’s riot control unit and police repression,” said Uribe. ]

[Other pressing issues are the peace process and an uptick in violence. Human rights organizations have said that the failure to implement the accord has led to fresh violence in places such as Northern Cauca. “Much of what happens there in terms of illegal use crops has to do with the lack of implementation of the first point of the peace accord that has to do with agrarian reform and the fourth point, which corresponds to an anti-narcotics policy and the substitution of coca crops,” said Uribe.]

[In addition to these demands are calls related to better education, health and pension system as well as the protection of water resources and a ban on fracking.]

### Fear as a tactic of dispersion

On the second day of the strike, panic spread among protesters in Bogotá. "More than 850 people were detained in Bogotá in the context of the declaration of a curfew, which provoked acts of vandalism. Some of those responsible are the mayor and the Armed Forces because of infiltrations. The Inspector General hasn't investigated this seriously, nor the Attorney General, and the country deserves an explanation as to why there has been so much violence during these protests," Uribe said.

[The death of the high school student Dilan Cruz has intensified protests, in which protesters are demanding respect for life and exclusion of the Armed Forces in the street. For Uribe, the solution to the outbreaks of violence is to ban the ESMAD from the protests.]Another issue the actions of the Armed Forces and the curfew have provoked is the intensification of xenophobia against the Venezuelan community, who are being blamed for the violence.

Despite the difficulties, the "extreme right has been defeated by the banging pans. We haven't succumbed to fear. The people understand that the violence has come more from the state than the protestors, which underlines the necessity that the 11/90 protocols on social protests be fulfilled, that the marches not be militarized and that the Army and the police should refrain from acting as an invading force."

"I'm in talks with many sectors and organizations to see how to organize the agenda," said Uribe. The government is weak and deaf. We need to establish efficient dialogues quickly. As the mayor-elect of Bogotá Claudia Lopez said, neither the mayors nor the workers' unions and even less the NGOs represent those that are in the streets. That's why we have to recognize the social processes of the youth, students, women and indigenous protesters. This extended negotiation table has to be a strong space to ensure these social demands."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
