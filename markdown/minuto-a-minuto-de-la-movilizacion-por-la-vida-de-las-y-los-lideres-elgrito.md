Title: ? Minuto a minuto de la movilización por la vida de las y  los líderes #ElGrito
Date: 2019-07-26 11:20
Author: CtgAdm
Category: eventos, Movilización
Slug: minuto-a-minuto-de-la-movilizacion-por-la-vida-de-las-y-los-lideres-elgrito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Video-2019-07-26-at-09.05.13.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-09.05.12-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/MARCHA-MUNDIAL-POR-LOS-LIDERES-SOCIALES.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.19.53-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.19.53-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.10.37-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.07.17-AM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.07.17-AM-1-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.34.49-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.27.44-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.41.14-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.52.00-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.15.51-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.42.52-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.43.31-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-3.45.35-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-3.53.43-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190726-WA0083.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

EL GRITO POR LA VIDA DE LAS Y LOS LÍDERES SOCIALES
--------------------------------------------------

Así avanza en el mundo la movilización en apoya a las y los líderes sociales colombianos 

Minuto a minuto: 

![WhatsApp Image 2019-07-26 at 09.05.12 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-09.05.12-1.jpeg)  
Viernes 26 de julio- 10:50 a.m.  

##### Berlín - Alemania

Movilización en apoyo de líderes y lideresas sociales en Colombia, en Berlín (Alemania).  
Viernes 26 de julio- 10:50 a.m.  
![Concentración en Cucuta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.10.37-AM.jpeg)

##### Concentración en Cúcuta (Norte de Santander)

En Colombia diferentes ciudades se movilizan para defender la vida \#ElGrito  
![Madrid, España](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.19.53-PM-1.jpeg)

##### Desde Madrid, España

Desde Madrid plantón y marcha por la defensa de nuestros líderes y lideresas sociales.  
![Performance desde Cali](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.07.17-AM-1.jpeg)

##### Cali, Valle del Cauca

Performance en diferentes puntos de Cali para expresar la grave situación que enfrentan líderes sociales y ex combatientes de las Farc.  
![Desde Londres,](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.34.49-PM.jpeg)

##### Londres

Desde Londres, el apoyo de diferentes personas a la movilización mundial por los y las líderes sociales.  
![Valencia España](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.42.52-PM.jpeg)

##### Valencia, España

Desde Valencia expresaron su apoyo y muestras de solidaridad con los líderes sociales.

##### Desde Berlín

https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Video-2019-07-26-at-09.05.13.mp4  
![Madrid, España](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-11.27.44-AM.jpeg)

##### España.

El dolor por nuestros líderes se hace sentir desde diferentes puntos del mundo.  
![Boyaca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.41.14-PM.jpeg)

##### Macanal, Boyacá

Niños, niñas y jóvenes también sienten el dolor del asesinato de los líderes en Colombia.  
![Desde Madrid, España](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-12.52.00-PM-1.jpeg)

##### Madrid, España

Gran plantón en Madrid en España.  
![Apoyo desde Barcelona, España](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.15.51-PM-1.jpeg)  
Viernes, 26 de julio 2019  

##### Barcelona, España

En Barcelona, plaza Sant Jaume, personas se unen a la movilización mundial por la vida de las y los líderes sociales. \#MarchaPorLaVida \#26JMiGritoEs \#26julioElGrito https://t.co/hSUMAdVUfa  
Viernes, 26 de julio 2019  
![Apoyo desde Popayan](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-2.43.31-PM-1.jpeg)

##### Popaya, Cauca

"Sin olvido" así se movilizan los payanes en apoyo a todos los líderes y lideresas asesinados en Colombia.  
![Apoyo desde París Francia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-3.45.35-PM.jpeg)

##### París, Francia

Desde la Torre Eiffel (París) muestran solidaridad por las y los líderes en Colombia.  
Vía Instagram: @Ginacardenasu  
![Munich, Alemanía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-26-at-3.53.43-PM.jpeg)

##### Munich, Alemanía

Desde Munich, performance y plantón por las y los líderes sociales  
![IMG-20190726-WA0083](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190726-WA0083.jpg)

##### Quito, Ecuador

El apoyo de los ecuatorianos con las y los líderes sociales en Colombia.  
[![Asesinan a excombatiente de Farc, Carlos Alberto Montaño en Cali](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Asesinado-integrantes-de-las-farc-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali/)  

#### [Asesinan a excombatiente de Farc, Carlos Alberto Montaño en Cali](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali/)

El 24 de julio fue asesinado Carlos Alberto Montaño Mosquera excombatiente del antiguo grupo guerrillero FARC- EP en Cali (Valle…  
[![Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)  

#### [Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)

Se cree que los responsables pudieron ser disidencias de las FARC o el EPL, la Guardia denuncia que podría tratarse…  
[![? Minuto a minuto de la movilización por la vida de las y los líderes \#ElGrito](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/MARCHA-MUNDIAL-POR-LOS-LIDERES-SOCIALES-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/minuto-a-minuto-de-la-movilizacion-por-la-vida-de-las-y-los-lideres-elgrito/)  

#### [? Minuto a minuto de la movilización por la vida de las y los líderes \#ElGrito](https://archivo.contagioradio.com/minuto-a-minuto-de-la-movilizacion-por-la-vida-de-las-y-los-lideres-elgrito/)

EL GRITO POR LA VIDA DE LAS Y LOS LÍDERES SOCIALES Así avanza en el mundo la movilización en apoya…  
[![Debe parar Minería de AngloGold Ashanti en Jericó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33156256_1397768400322849_2324060057165103104_n-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/continuan-actividades-de-mineria-en-jerico/)  

#### [Debe parar Minería de AngloGold Ashanti en Jericó](https://archivo.contagioradio.com/continuan-actividades-de-mineria-en-jerico/)

Continua minería ilegal en Jericó, una comisión de la Contraloría inspeccionó actividades de exploración minera que está realizando AngloGold Ashanti.
