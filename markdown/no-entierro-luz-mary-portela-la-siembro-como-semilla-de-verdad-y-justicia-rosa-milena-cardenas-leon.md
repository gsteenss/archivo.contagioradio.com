Title: "No entierro a Luz Mary Portela, la siembro como semilla de verdad y justicia" Rosa Milena  Cárdenas León
Date: 2016-11-08 17:31
Category: Otra Mirada, Sin Olvido
Tags: Homenaje a víctimas del palacio de justicia, Luz Mary Portela
Slug: no-entierro-luz-mary-portela-la-siembro-como-semilla-de-verdad-y-justicia-rosa-milena-cardenas-leon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/luz-mary-portela-leon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 de Nov 2016] 

Eran las nueve de la mañana en la Plaza de Bolívar, justo en frente del Palacio de Justicia, Luz Mary Portela volvió después de 31 años al lugar en donde la desaparecieron. **Volvió para reunirse con los suyos y para presenciar como su familia**, ahora no solo conformada por sus hijos y nietos, sino también por los familiares de las otras diez víctimas, se despedían entre sentimientos agridulces y esperanza.

Una serie de discursos protocolarios como el de la Fiscalía, la Unidad de Víctimas y  la Cancillería dieron apertura a la entrega oficial de los restos de Luz Mary, que esperaban en una pequeña cajita de madera volver a los brazos de los que fue separada. S**in embargo, hacía falta su madre Rosalbina, quien desde el 7 de noviembre de 1985 hasta su último día con vida**, no descansó buscando a su hija, luchando contra la impunidad y un Estado que prolongo el encuentro 11.315 días.

**“Gracias mami Rosalbina por enseñarme a Luchar”**, esas fueron las palabras de Rosa Milena, hija de Luz Mary, en agradecimiento al amor, el cariño y la fuerza de su abuela Rosalbina. Rosa en compañía de su hermano Edison, escuchaban desde la primera fila cada intervención, con las emociones en la garganta y aferrándose a los pocos pero significativos recuerdos de su mamá.

“Hoy estamos acá para honrar la memoria de Luz Mary Portela, mujer a quién los sueños le fueron decapitados por los verdugos que protegían la patria y ¿en dónde están sus victimarios?, recorrimos un camino de injusticia e impunidad de un Estado corrupto y amañado, doy gracias a los funcionarios que por su valiosa ética han permitido logros como este, pero también **condeno a los corruptos que con su proceder permiten que los victimarios sean intocables, libres y gocen de sus beneficios**” afirmó Edison.

Y es que pese a que ya hay una sentencia por parte de la **Corte Interamericana de Derechos Humanos en donde condena al país por las desapariciones del Palacio de Justicia**, poco es lo que se ha avanzado en estas investigaciones, mientras los familiares continúan a la espera de respuestas y verdades. Le puede interesar: ["Se reafirma impunidad con absolución de Plazas Vega por caso de Palacio de Justicia"](https://archivo.contagioradio.com/se-reafirma-la-impunidad-en-caso-del-palacio-de-justicia/)

Para Rosa Milena, esta entrega significó, alcanzar un poco de paz para continuar en la búsqueda, **“sigo con los mismos sinsabores de la verdad, no sé cómo murió, en donde murió, por qué murió y quiénes son los responsables, hace falta toda la verdad”**. Una de esas explicaciones es por qué el cuerpo de Luz Mary es hallado en el cuarto piso según el informe de Medicina Legal, cuando en un vídeo se observa cuando ella sale del Palacio de Justicia.

Además, estos 31 años han pasado una factura alta, muchos de los padres y madres de las víctimas del Palacio de Justicia ya han fallecido, a algunos como Rosalbina el dolor y la amarga ausencia les impidió vivir ese momento por el que lucharon tanto tiempo. La entrega de los restos de Luz Mary  fue un **homenaje a las  y los incansables luchadores, que no se rindieron ni uno solo de sus días desde que les arrebataron a sus hijos.**

**La cajita de madera que con dignidad contenía los huesos de Luz Mary, fue abrazada, le pusieron fin a tantos deseos inconclusos, a los besos que no se dieron, los cafés de media tarde, a las visitas al parque** y hoy entre lágrimas y un poco de alivio, retornan a Rosalbina, quizás en otro lugar, tiempo y espacio se amen y se cuenten 31 años perdidos.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

<div class="ssba ssba-wrap">

</div>
