Title: Comisión Étnica viaja a EEUU para hablar de implementación de los Acuerdos de Paz
Date: 2017-07-27 12:20
Category: DDHH, Paz
Tags: Consulta Pevia, Pueblos étnicos
Slug: comision-etnica-se-reune-con-organizaciones-en-eeuu-para-dar-cuenta-de-implementacion-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Mesa-Étnica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [27 Jul. 2017] 

Tres días completa la delegación de la Comisión Étnica para la Paz y Defensa de los Derechos Territoriales en Washington (EE.UU.), realizando diversas reuniones en las que dan cuenta de la situación por la que atraviesan las comunidades afrodescendientes e indígenas en Colombia y del **estado actual de la implementación del capítulo étnico incluido en los Acuerdos de Paz** pactados en La Habana. En contexto: [Se creará Instancia con Pueblos Étnicos para implementación de Acuerdos de Paz](https://archivo.contagioradio.com/se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz/)

Las reuniones que han sostenido con organizaciones e instancias norteamericanas como el Comité de Relaciones Exteriores del Senado de EE. UU, la Oficina en Washington para Asuntos Latinoamericanos WOLA; el Departamento de Estado, entre otros, pretenden **contribuir al fortalecimiento de las relaciones y el apoyo político para la exigencia al Estado colombiano** de la pronta inclusión de este capítulo en la implementación.

“Continuaremos buscando el apoyo político con nuestros aliados en EE.UU., para que el **estado colombiano cumpla con lo pactado e incorpore la perspectiva étnica y cultural** en todas las medidas normativas con el que se implementará el acuerdo de paz, con las garantías políticas, sociales, económicas y culturales pertinentes para que todos los Pueblos podamos gozar efectivamente del desarrollo de los derechos universales como pueblos” aseguran en un comunicado.

Además, manifestaron que dicho capitulo étnico, no solo debe ser pensado con un enfoque diferencial de los pueblos ancestrales de Colombia, sino que además debe incluirse el enfoque de mujer, familia, género y generación de dichas comunidades. Le puede interesar: [Delegación de EE.UU insta al Gobierno a cumplir capítulo étnico del acuerdo de paz](https://archivo.contagioradio.com/capitulo-etnico-del-acuerdo37339/)

Agregaron que con estas actividades buscan salvaguardar los derechos fundamentales de los pueblos indígenas y negros – afrodescendientes de Colombia con miras a que sean tenido en cuenta e incluidos en el bloque de constitucionalidad y en las medidas normativas Fast Track para la implementación de los acuerdos. Le puede interesar: [Comisión Étnica pide participación en Tribunal para la Paz](https://archivo.contagioradio.com/comision-etnica-pide-participacion-en-tribunal-para-la-paz/)

Por último, recuerdan que **a la fecha dicho acuerdo cumple apenas con la Consulta Previa del 3% del 100 % “ignorando y agravando la situación de olvido** y exclusión histórica a nuestros pueblos, quienes hemos sufrido históricamente un conflicto bélico que ha dejado miles de víctimas, incluyendo nuestros territorios, principal damnificado”.

[Campaña de Incidencia de CE en Washinton](https://www.scribd.com/document/354882977/Campan-a-de-Incidencia-de-CE-en-Washinton#from_embed "View Campaña de Incidencia de CE en Washinton on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_2726" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/354882977/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-eJ6t79oqXGVBwxgOrxTr&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="osd-sms-wrapper">

</div>
