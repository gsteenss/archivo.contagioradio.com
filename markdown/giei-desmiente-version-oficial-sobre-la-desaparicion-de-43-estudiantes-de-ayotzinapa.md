Title: GIEI desmiente versión oficial sobre la desaparición de 43 estudiantes de Ayotzinapa
Date: 2015-09-07 15:55
Category: DDHH, El mundo, Entrevistas, Movilización
Tags: 43 estudiantes desaparecidos en Ayotzinapa, CIDH, Derechos humanos en México, Desaparición forzada en México, GIEI, Normal Rural de Ayotzinapa, Procuraduría General de la República de México
Slug: giei-desmiente-version-oficial-sobre-la-desaparicion-de-43-estudiantes-de-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/GIEI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] ciudadanosenred.com.mx 

<iframe src="http://www.ivoox.com/player_ek_8198137_2_1.html?data=mZammpaXe46ZmKiak5mJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi6q5qpDRx9jRrcbi1cqY2MrWt8qZpJiSpJjSb9DaysjWw9GPt9DW08qYzsaPqMbnwtXO1M7HrYa3lIqvldOPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carolina Bedoya Monsalve, Periodista mexicana] 

###### [07 Sept 2015] 

[El Grupo Interdisciplinario de Expertos Independientes, GIEI, designado por la CIDH para la investigación de la desaparición de 43 estudiantes de la Escuela Normal Rural de Ayotzinapa, presentó un **informe final que desmiente la versión oficial del gobierno mexicano.**]

### **Sobre la hipótesis de la incineración** 

[El informe, asegura que **no existen argumentos sólidos en las versiones de las autoridades gubernamentales**, según los expertos del GIEI, **para la incineración de estos cuerpos habrían sido necesarias 30 toneladas de madera que ardieran por 60 horas y provocaran llamas de hasta 7 metros y humos de 300**.]

[Además ni en el basurero ni en sus alrededores se comprobó la existencia de combustibles suficientes para cremar los cuerpos ni evidencia de esta conflagración que hubiera alertado a los pobladores aledaños, no se sabe más que de fuegos de pequeñas dimensiones sin fecha exacta.]

### **Un quinto autobús no aparece en las investigaciones de la PGR** 

[Las versiones oficiales aseguran que los estudiantes de la Normal se movilizaban en cuatro autobuses con destino al Distrito Federal, para asistir a una marcha el 2 de octubre de 2014, no obstante **el informe asevera la existencia de un quinto autobús que fue tomado a las afueras de Iguala por estudiantes que luego fueron transportados a la central de buses de este municipio y  abandonados allí** y que minutos después, solicitaron al resto de sus compañeros acudir al lugar para rescatarlos.]

[La importancia de este elemento para la investigación radica en que se argumenta que las agresiones sufridas por los estudiantes en Iguala, por parte de unidades policiales, tendría que ver con la intensión de impedir el tráfico de heroína en buses comerciales hacia Estados Unidos, del que se tiene registro en el municipio. Sin embargo, hay datos que señalan que **el autobús en el que se transportaban los 43 estudiantes desaparecidos no fue atacado como el resto de autobuses que estaban en la zona**.]

[Así mismo, cuando el GIEI solicitó a la empresa de transportes Costa Line, dueña del autobús en el que se movilizaban los estudiantes, la revisión del vehículo ésta presentó uno distinto por lo que no fue incluido en la investigación. Tampoco se ha indagado sí esta compañía de transporte tiene relación con las que en Estados Unidos manejan el tráfico de heroína. Por lo que **esta línea en la investigación queda abierta**.]

### **El asesinato y la desaparición: Una labor conjunta y coordinada** 

[El informe insiste en que **las autoridades policiales y militares tenían conocimiento del rumbo de los estudiantes, pues mantuvieron los autobuses en monitoreo constante**. Se tiene conocimiento de que hubo agentes de inteligencia del Ejército en al menos dos de los escenarios en los que los 43 estudiantes fueron detenidos por policías federales. Lo que da cuenta del nivel de **coordinación de entes estatales en las agresiones contra los estudiantes**.    ]

[El informe concluye afirmando que para la localizar a los estudiantes las autoridades de Policía no agotaron todas las opciones que tienen a su disposición. En el curso de la investigación hubo hechos no examinados y evidencias que fueron destruidas, por lo solicita al Gobierno inicie una nueva investigación considerando otros delitos que se pudieron presentar, así como otros testimonios, dado que los actuales no gozan de veracidad. ]

[Carolina Bedoya Monsalve, periodista mexicana, asegura que este informe muestra la **absoluta incompetencia y falta de voluntad política del Gobierno para encontrar y sancionar a los responsables** de la desaparición de los 43 estudiantes y que están **directamente implicados con el Estado**.]

[Horas después de la presentación del informe la **PGR en rueda de prensa, anunció que contemplará la posibilidad de incorporar estas averiguaciones del GIEI en la investigación que lleva a cabo**, así como iniciar un un nuevo peritaje y prolongar la estadía solicitada por el GIEI para continuar con sus indagaciones.   ]

[Según madres, padres y familiares de los 43 estudiantes desaparecidos, estas declaraciones son evasivas y poco contundentes por lo que **exigen al Gobierno  reestructure la investigación**, acatando las recomendaciones del GIEI, **para poder conocer la verdad y que los directos responsables, así como los funcionarios que han obstruido el acceso a la justicia, sean sancionados.   **]

[Junto a Organizaciones Defensoras de Derechos Humanos, anunciaron que sí el 10 de septiembre el presidente Peña Nieto no atiende su solicitud de reunión para el compromiso formal de inicio de la nueva investigación, el 23 de este mes iniciaran un ayuno. Así mismo, convocan a una gran movilización el próximo 26 de septiembre, a un año de los fatídicos hechos.]
