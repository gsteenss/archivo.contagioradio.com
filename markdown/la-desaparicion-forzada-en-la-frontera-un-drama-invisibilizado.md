Title: La desaparición forzada en la frontera, un drama invisibilizado
Date: 2020-10-15 12:48
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Colectivo Sociojurídico Orlando Fals Borda, Familias de víctimas de desaparición, frontera con ecuador, Víctimas de desaparición forzada
Slug: la-desaparicion-forzada-en-la-frontera-un-drama-invisibilizado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparecido-grafica-7.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparidas-grafico-11.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparecidas-grafico-12.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Ciudad Comuna

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles, 14 de octubre, [el Colectivo Sociojurídico Orlando Fals Borda -COFB](https://www.cofb.org.co/)-,y ASOVICOMPI entregó a la Unidad de Búsqueda de Personas Dadas por Desaparecidas –UBPD– [**el informe «Huellas de Encuentro: si estoy en tu Memoria, hago parte de la Historia», sobre desapariciones forzadas en la frontera con Ecuador.**](/Users/Akamaru/Downloads/Encuentro%20Huellas%20-%20Digital.pdf)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con datos del informe, en el 2020, hasta el 27 de agosto de 2020, se reportaron 138 casos de desaparición en los municipios de Tumaco, Pasto, Túquerres e Ipiales; 7 en Tumaco, sin resultado alguno a la fecha, 124 en Pasto, de los cuales, 34 se encuentran con vida, 4 muertos y 86 en condición de desaparecidos; 7 en Ipiales, sin información sobre su localización y en Túquerres, no se identificó ningún caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De los 138 casos, **el informe establece 40 casos de desaparición forzada en el contexto del conflicto armado en la zona frontera con Ecuador, principalmente en el departamento de Nariño, los municipios Puerres, Córdoba y Carlosama y en el municipio de Ipiales, en los corregimientos de la Victoria y Sucumbíos**. (Le puede interesar: [UBPD pide protección para sitio de inhumación en El Copey (Cesar)](https://archivo.contagioradio.com/ubpd-pide-proteccion-para-sitio-de-inhumacion-en-el-copey-cesar/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Variables de los 40 casos de desaparición
-----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con relación a los rangos de edad, la tendencia victimizante afecta en su mayoría a personas entre los 18 a 49 años de edad (3 de cada 4 víctimas). No  
obstante se presenta ausencia de información para tres casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respecto a la ancestralidad, el informe indica que hay una leve presencia de indígenas, con dos víctimas, «mientras las personas con ancestro mestizo representan a tres de cada diez víctimas reportadas, - aunque, también se reporta la ausencia de información para seis de cada diez casos-».

<!-- /wp:paragraph -->

<!-- wp:image {"id":91512,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparecido-grafica-7.png){.wp-image-91512}  

<figcaption>
*Cuando la información es escasa, incompleta o no suministrada, se codifica 9999999*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En relación a los casos denunciados a autoridades, el 57.5% (23) de los casos ha presentado la denuncia ante Fiscalía General de la Nación, 7.5% (3) casos fueron denunciados ante la Inspección de Policía y el 14% de los casos, no  
ha presentado denuncia ante autoridad. Los procesos denunciados, se encuentran activos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, los casos no reportan una posible ubicación para las víctimas, sin embargo, las narraciones en el proceso de victimización «refieren veredas o municipios relacionados con la comisión del ilícito». Con respecto a la modalidad de la desaparición, entre siete y diez de los casos tienen un componente forzoso (reclutamiento forzado o desaparición forzada), mientras los 30 restantes no cuentan con información en este aspecto.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91515,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparidas-grafico-11.png){.wp-image-91515}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En cuanto a la época en que se presentaron los casos, se dieron principalmente entre los años 2000 y 2005, en que la presencia de actores armados es alta, pues el territorio se convierte en área de corredores estratégicos.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91526,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-personas-desaparecidas-grafico-12.png){.wp-image-91526}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Adicionalmente, el informe señala que la desaparición afecta más a hombres que a mujeres, con 29 y 11 casos, respectivamente. **Sin embargo, las mujeres son quienes suelen asumir la búsqueda de su familiar, así como la carga familiar.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

«Encontrar a nuestros seres queridos vivos o muertos, pero los queremos».
-------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El informe también reconoce las implicaciones a nivel psicosocial que experimentan los familiares al momento de la desaparición, principalmente las **mujeres, madres, hermanas, tías, parejas y esposas que esperan encontrar a sus familiares y que «les ha tocado sacar a sus hijos y nietos adelante, sin tener muchas veces los recursos con que sostenerlos y teniendo además que sobrellevar las emociones que les causa no tener información clara sobre sus seres queridos».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante la entrega del informe, la representante de los familiares de las víctimas expresó **«no es suficiente encontrar y decir "desapareció" sin saber qué pasó con los familiares y porqué les sucedió esto \[...\] esto no es fácil, es muy duro, es como darle a uno un golpe».** (Le puede interesar: [528 años después continúa el exterminio para razas y sectores sociales](https://archivo.contagioradio.com/528-anos-despues-continua-el-exterminio-para-razas-y-sectores-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, agregó que se debe seguir luchando teniendo siempre presente el **«**encontrar a nuestros seres queridos vivos o muertos, pero  
los queremos**»** para obtener la verdad sobre lo que pasó con sus familiares y poder llegar a la vejez tranquilos y sin la angustia diaria.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/284148195928937/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/284148195928937/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
