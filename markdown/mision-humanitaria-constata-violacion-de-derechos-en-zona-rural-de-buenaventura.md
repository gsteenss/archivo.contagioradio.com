Title: Misión humanitaria constata violación de derechos en zona rural de Buenaventura
Date: 2020-02-05 17:16
Author: CtgAdm
Category: DDHH, Nacional
Tags: Armada Nacional, buenaventura, Derecho Internacional Humanitario, Misión Humanitaria
Slug: mision-humanitaria-constata-violacion-de-derechos-en-zona-rural-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Zona-rural-de-Buenaventura-scaled-e1580939241669.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Misión Humanitaria {#foto-misión-humanitaria .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras la realización de la Misión Humanitaria Buenaventura 2020, las 21 organizaciones que realizaron el recorrido por la zona rural del Puerto constataron múltiples obras inconclusas, violación al Derecho Internacional Humanitario (DIH) y la continuación de la violencia sufrida por las comunidades en el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Proteger la vida, cuidar el territorio"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La misión se llevó a cabo entre el 3 de enero y el 2 de febrero de este año, bajo el lema "Proteger la vida, cuidar el territorio". En el recorrido por los ríos Naya, Yutumangui, Cajambre, Raposo y Calima, se reunió con autoridades del territorio y miembros de las comunidades que lo habitan, para denunciar la "violación a los derechos civiles, políticos, económicos, sociales, culturales y ambientales".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este recorrido fue convocado por el Proceso de Comunidades Negras (PCN), NOMADESC, CODHES y la Alcaldía del Distrito Especial de Buenaventura; a la misión acudieron 18 organizaciones defensoras de derechos humanos, civiles y medios de comunicación, además contó con el acompañamiento de la Defensoría del Pueblo regional pacífico, MAPP- OEA y la Procuraduría General de la Nación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Violencia estructural: Corrupción, vulneración del derecho a la salud y la educación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En su recorrido, "la misión pudo comprobar que no existen servicios de agua potable, energía y puestos de salud", la infraestructura para garantizar el derecho a la salud y la educación está en estado de abandono; mientras algunos centros educativos fueron demolidos "de manera innecesaria" y las obras para reemplazarlos nunca se ejecutaron, obligando a niños, niñas y adolescentes a tomar clases en lugares como centros de acopio, y casas de familia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, las organizaciones señalaron que la contratación de docentes no corresponde con las necesidades de la población; y "**los promotores de salud no tienen garantías para el trabajo y no cuentan con los instrumentos de laboratorio necesarios** (...) para hacer exámenes adecuados". A ello se suma el desconocimiento de los médicos tradicionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades también ven vulnerado su derecho a la movilidad por el mal estado de los muelles, el alto costo de la gasolina y el robo de las lanchas comunitarias. A ello se suma una difícil situación ambiental relacionada con la "proliferación de la minería y los proyectos de exploración y explotación minera y de generación de energía por medio de hidroeléctricas".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Violaciones al DIH en zonas rurales de Buenaventura**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La misión manifestó su preocupación ante la observación de integrantes de la Armada Nacional en los centros poblados de las cuencas de los ríos, "violando el Derecho Internacional Humanitario y el principio de distinción". (Le puede interesar: ["¿Está intimidando la Fuerza Pública a jóvenes indígenas en Juradó, Chocó?"](https://archivo.contagioradio.com/esta-intimidando-la-fuerza-publica-a-jovenes-indigenas-en-jurado-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la misión aseguró que **los uniformados "se encontraban en los ríos Naya y Raposo haciendo presencia en las riberas, habitando escuelas, puestos de salud y casas de la comunidad"**, razón por la que le recomendó a la Armada Nacional, a través de los organismos de control, dar cumplimiento al DIH que protege especialmente a los civiles de los actores armados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Dudas de las comunidades sobre la implementación del Acuerdo de Paz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para finalizar, las organizaciones identificaron la preocupación de las comunidades ante "el incumplimiento de los acuerdos de paz con las FARC y el rompimiento de los diálogos entre el ELN y el Gobierno Colombiano". En ese sentido, la misión señaló que la inadecuada implementación del Acuerdo ha significado la continuidad en las lógicas de la guerra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al tiempo, se reseñó que no existen avances en la implementación de los Programas de Desarrollo con Enfoque Territorial (PDET), así como tampoco se ha avanzado en el Plan Nacional Integral de Sustitución, generando riesgos de consumo en menores. (Le puede interesar: ["Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

**Escenarios de riesgo consumados** en zona rural de Buenaventura
-----------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Misión humanitaria evidenció algunos de los[riesgos que se consumaron en los primeros días de este año](https://archivo.contagioradio.com/desplazamiento-y-amenazas-de-grupos-armados-en-zona-rural-de-buenaventura/), cuando comunidades del río raposo se desplazaron por temor a combates entre hombres del ELN y disidencias de las FARC. Más de 20 personas decidieron salir de sus casas ante el anuncio del ELN sobre una intención de disputar territorio a la Disidencias, y tras el desplazamiento de varios hombres de este último grupo al territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se sumó la denuncia de la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/control-paramilitar-en-comunidades-del-bajo-san-juan-y-bajo-calima/), según la cual, hombres que se presentaron como paramilitares en el muelle de Docordó, cabecera municipal del Litoral San Juan (Chocó), dijeron que "tomarían control territorial en el Bajo San Juan y el Bajo Calima en Buenaventura, Valle del Cauca y el Litoral San Juan, departamento del Chocó".

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SomosDef/status/1224757941145948168","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SomosDef/status/1224757941145948168

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
