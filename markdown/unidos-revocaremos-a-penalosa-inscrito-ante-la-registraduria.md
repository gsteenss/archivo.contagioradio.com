Title: “Unidos revocaremos a Peñalosa” inscrito ante la Registraduría
Date: 2017-01-02 21:19
Category: Movilización, Nacional
Tags: Alcaldía Enrique Peñalosa, revocatoria Peñalosa
Slug: unidos-revocaremos-a-penalosa-inscrito-ante-la-registraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Enrique-Peñalosa-e1461273574548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: “Unidos revocaremos a Peñalosa”] 

###### [2 Enero 2017] 

Quedo inscrito ante la Registraduría Nacional el comité que busca llevar a cabo la revocatoria del actual alcalde de Bogotá, Enrique Peñalosa, quién hasta el momento cumple un año siendo el burgomaestre de la capital, y que de acuerdo con miembros de “Unidos revocaremos a Peñalosa”, **esta medida es el reflejo del descontento social que hay frente a las decisiones que ha tomado Peñalosa.**

De acuerdo con Diego Pinto, miembro de “Unidos revocaremos a Peñalosa”, esta iniciativa surge como respuesta de la ciudadanía y de la desaprobación que existe hacia Peñalosa, “creemos que por un lado los sondeos, las encuestas dan una imagen desfavorable, que no es gratis y que se debe a la **disminución en derechos que hemos tenidos las y los ciudadanos en Bogotá”.**

El proceso de revocatoria busca visibilizar la problemática de fondo que afronta Bogotá y su modelo de ciudad, frente a este tema Pinto señala que si bien es cierto que la capital posee dificultades estructurales de más de 50 años, en los últimos gobierno **se habían alcanzado avances en materia de garantías de derechos humanos que en este periodo se han visto recortados**. Le puede interesar: ["50 mil firmas recolectadas para cabildo abierto contra venta de ETB"](https://archivo.contagioradio.com/60-mil-firmas-recolectadas-para-cabildo-abierto-contra-venta-de-etb/)

**“Hay un recorte permanente de derechos, por ejemplo la masacre laboral que ha sufrido el Distrito”** a este hecho se le suman la aprobación de la venta de acciones de la Empresa Pública de Teléfonos, el desplazamiento de cientos de trabajadores informales que aún no han sido re ubicados, el retroceso en medidas ambientales como la re apertura de la Plaza de Toros La Santamaría y la falta de medidas en torno a la problemática de movilidad. Le puede interesar: ["En enero regresan las corridas de Toros a Bogotá"](https://archivo.contagioradio.com/enero-vuelven-los-toros/)

Este comité está conformado por aproximadamente 50 organizaciones sociales y de ciudadanos que decidieron hacer evidente el sentimiento de inconformismo ante esta alcaldía. En un comunicado de prensa aseguraron que cualquier persona puede unirse a este comité y hacer parte de la lógistica para poner en marcha la recolección de las aproximadas **300.000 firmas.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
