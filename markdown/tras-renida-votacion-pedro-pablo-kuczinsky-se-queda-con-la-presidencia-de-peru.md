Title: Tras reñida votación Pedro Pablo Kuczinsky se queda con la presidencia de Perú
Date: 2016-06-09 17:33
Category: Nacional, Política
Tags: Keiko Fujimori, Pedro Pablo Kuczinsky, Perú
Slug: tras-renida-votacion-pedro-pablo-kuczinsky-se-queda-con-la-presidencia-de-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Pedro-Pablo-Kuczynski-e1465511453189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La República 

###### [9 Jun 2016] 

Luego de una reñida votación, **Pedro Pablo Kuczinsky (PPK),** candidato de Peruanos por el Kambio, obtuvo la presidencia del Perú en la segunda vuelta electoral con el** 50,12% de los votos frente al 49,88% que logró Keiko Fujimori, candidata por el partido** Fuerza Popular.

Tras conocerse los resultados finales, Kuczynski dijo por medio de una rueda de prensa que busca establecer conversaciones con los partidos opositores. Por su parte la **Oficina Nacional de Procesos Electorales (ONPE), aseguró que se trató de unas elecciones **totalmente transparentes.

Si bien, PPK representa la continuidad de un modelo neoliberal, analistas y organizaciones sociales del Perú, coinciden en que un gobierno de Fujimori reviviría las violaciones a los derechos humanos ocurridas durante el gobierno su padre **en el que 314.605 mujeres fueron esterilizadas en el marco del Programa Nacional de Planificación Familiar del gobierno de Alberto Fujimori, se [reprimió la protesta social](https://archivo.contagioradio.com/llegada-de-3200-militares-estadounidenses-a-peru-atenta-contra-la-soberania-de-ese-pais/) y se vendieron más de cien empresas públicas de alto valor comercial y estratégico.**

Asimismo, Keiko, respaldada por el partido Fuerza Popular, tiene una fuerte oposición por parte de diversos sectores de la sociedad debido a las múltiples acusaciones por corrupción, actualmente enfrenta un proceso de apelación por la entrega de dineros en actividades proselitistas denunciada ante el Jurado Electoral, que falló a favor de la continuación de su campaña.

Sin embargo, cabe recordar que **al ahora presidente de Perú, se le involucra con los Papeles de Panamá por la aparición de una nota dirigida a a Mossack Fonseca** en 2006 en la que recomendaba que decía le “era grato presentar al señor Francisco Pardo Masones” dando fe de conocerle de tiempo atrás, de sus cualidades y prestancia; un personaje que resultaría haciendo negocios en Cuba con pasaportes venezolanos.

Además se le atribuye la responsabilidad de permitir que la Internacional Petroleum Company (IPC) se llevara 17 millones de dólares del erario nacional, cuando se desempeñaba como asesor económico y gerente del Banco Central de Reserva, teniendo conocimiento que la empresa había sido nacionalizada en 1968.

Otro de los escándalos con los que la prensa relaciona **a Kuczinsky es el generado su ONG ‘Agua limpia’, por medio de la cual habría canalizado los recursos para su campaña con fondos de USAID y la CIA,** con la excusa de proveer agua potable y alcantarillado a poblaciones, dinero que sería recuperado por los inversionistas de resultar electo con la explotación de los recursos hídricos con un alto costo para los pobladores.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
