Title: "Hay que estar muy vigilantes en este nuevo periodo legislativo": Iván Cepeda
Date: 2016-07-21 15:29
Category: Nacional
Tags: Congreso Colombia 2016, Iván Cepeda, Nuevo periodo legislativo, Reformas institucionales para la paz
Slug: hay-que-estar-muy-vigilantes-en-este-nuevo-periodo-legislativo-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [21 Julio 2016]

En el centro de este nuevo periodo legislativo estará todo el proceso de implementación de los acuerdos de paz, desde la refrendación, hasta los proyectos y reformas constitucionales necesarias para habilitar todo lo que se pacte en La Habana, "ahí habrá un asunto delicado que va a requerir una gran atención y exigencia porque **en la labor legislativa podría quedar gravemente afectados e incluso severamente distorsionados los acuerdos de paz**", asegura el Senador del Polo Democrático Iván Cepeda.

Otro de los temas de discusión para este nuevo periodo es el del presupuesto general de la nación y teniendo en cuenta que la financiación de la paz dependerá de decisiones que se tomen a través de los CONPES, hay un tema de fondo y es el de la asignación para el sector de defensa y militar, que demostrará cuánto conviene al país terminar una parte sustancial del conflicto armado y ésto **cómo redunda en el fortalecimiento del presupuesto para otros sectores como el de la salud y la educación**, agrega el Senador.

La discusión de la reforma tributaria será el otro gran tema de discusión en este periodo, y de acuerdo con el Senador Cepeda, todo indica que esta reforma será profundamente regresiva porque va a afectar particularmente a la clase media y a los sectores populares, "fortaleciendo el pago del IVA, generando nuevos impuestos y nuevas cargas impositivas y **dejando intactos los beneficios y las gabelas tributarias que se le dan a los sectores más poderosos del país**".

"Muy seguramente habrá un intento por afectar gravemente los acuerdos de paz (...) y es necesario que todas las organizaciones sociales y los partidos políticos que trabajamos por una paz con justicia social estemos supremamente vigilantes acompañando el legislativo durante todo este proceso (...) partiendo de la base de que el cumplimiento de los acuerdos hará parte de la movilización social, esto hay que tenerlo claro, **no puede pensarse que los acuerdos vendrán simplemente porque estén suscritos**", concluye el Senador.

<iframe src="http://co.ivoox.com/es/player_ej_12298329_2_1.html?data=kpefm52Xdpqhhpywj5aWaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVhpewjajTssiijLXcztSPiMbh0Mjfh6iXaaKl1c7Q0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
