Title: Victimas de la Chinita y FARC avanzan con paso firme hacia la reconciliación
Date: 2019-01-23 15:05
Author: AdminContagio
Category: DDHH, Nacional
Tags: masacre de la chinita, Reconciliación, Urabá Antioqueño
Slug: victimas-de-la-chinita-se-reconocen-hoy-como-constructores-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/la-chinita-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 23 Ene 2019 

En el marco de la conmemoración de los 25 años de la MAsacre de la Chinita, tanto las propias víctimas como FARC lograron una ruta de trabajo para avanzar en la reparación integral y exigieron del gobierno su presencia, dado que en este hecho también ubican parte de responsabilidad por parte del Estado colombiano.

En la reunión de trabajo que se extendió por cerca de 6 horas, las familias de las víctimas aseguraron que es necesario que el gobierno haga presencia y brinde las garantías necesarias para que se cumplan los acuerdos en general, pero también para que se pueda avanzar en los compromisos a los que llegaron con integrantes de FARC para este hecho concreto.

De este acuerdo fueron facilitadoras organizaciones sociales como la Fundación Forjando Futuros, las Comunidades Construyendo Paz en los Territorios CONPAZ, la Comisión de Justicia y Paz y la Corporación Cultura Democrática.

[**Silvia Berrocal,** líderesa de la población de Apartadó** **en el **Urabá Antioqueño, afirma que ya han sanado sus corazones y que ya no existe espacio para el odio o el rencor, únicamente para la construcción de paz **en el marco de la conmemoración de los 25 años de la**** **Masacre de La Chinita** a manos del V frente de las Farc y en la que fueron asesinadas 35 personas.**  
**]

**“Queremos la paz y vamos a seguir luchando por ella”**

[La lideresa destaca el proceso de sanación que han realizado ella y los demás afectados de por la masacre, **“queremos salir adelante y estamos contribuyendo a la tan anhelada paz que queremos los colombianos”**, a su vez, lamentó los recientes hechos de violencia que han sucedido en el país, sin embargo expresó que también “siente felicidad de que las Farc hayan entregado sus fusiles y ya no disparen”[(Le puede interesar "Ahora el 23 de enero será de alegría" víctimas de la Chinita)](http://%22Ahora%20el%2023%20de%20enero%20será%20de%20alegría%22%20víctimas%20de%20la%20Chinita)]

### **Mientras haya odio en el corazón de la población, la guerra seguirá existiendo** 

[Adicionalmente la lideresa compartió sus deseos de contribuir a la paz de Colombia, en especial por quienes han perdido a sus familiares a raíz del conflicto, **“no queremos que otras personas sufran lo que nosotros sufrimos, es un dolor muy grande que solo entendemos los que lo vivimos”**  e invitó a la reconciliación al afirmar que mientras haya odio en el corazón de la población, la guerra seguirá existiendo.]

[25 años después del suceso, Silvia Berrocal está dispuesta a cerrar un ciclo que con el pasar del tiempo la ha fortalecido, “no me considero una víctima, me considero una gestora de paz” resaltó con firmeza la lideresa quien reiteró **“no soy víctima, soy una persona que me asesinaron a mi hijo, nunca lo voy a olvidar a él pero seguimos luchando por encontrar la paz”.**]

### **¿En qué van los compromisos de las Farc y el Estado con las víctimas?** 

[Silvia señala que no ha visto una voluntad real de reconciliación por parte del Gobierno Duque, y que aunque “las personas van pasando, las instituciones quedan” razón por la que continuarán exigiendo al Gobierno que brinde las garantís necesarias para que cumplan todo lo pactado dentro de las propuestas de reparación que ha realizado las víctimas a la Unidad para las Víctimas. Se han planteado 35 viviendas dignas, un centro histórico y educativo, sin embargo la entidad afirma que no hay presupuesto.  
]

Como parte de la conmemoración, la comunidad realizó  un conversatorio en el auditorio Rosalba Zapata entre, panelistas y víctimas donde se dieron a conocer los avances y falencias en el proceso de justicia y reparación, además de inaugurar el Parque del Sendero de la Reparación, “no queremos más cosas tristes, queremos cosas positivas, no nos vamos a quedar llorando nuestros muertos, queremos seguir adelante”, indicó Silvia.

<iframe id="audio_31725168" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31725168_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
