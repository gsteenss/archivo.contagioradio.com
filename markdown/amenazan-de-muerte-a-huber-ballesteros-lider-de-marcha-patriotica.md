Title: Amenazan de muerte a Huber Ballesteros líder de Marcha Patriótica
Date: 2017-04-17 13:03
Category: DDHH, Nacional
Slug: amenazan-de-muerte-a-huber-ballesteros-lider-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ballesteros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [17 Abr 2017]

A través de un mensaje de WhatsApp, el líder del Movimiento Marcha Patriótica, Huber Ballesteros fue amenazado de muerte por paramilitares. Según la denuncia el mensaje lo **recibió Ballesteros en la tarde del pasado Domingo 16 de Abril**, anunciando que desde las cero horas es declarado objetivo militar de las Autodefensas Unidas de Colombia, AUC.

\[caption id="attachment\_39256" align="aligncenter" width="334"\]![Imágen de la amenaza a Huber Ballesteros publicada por Marcha Patriótica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/amenaza-huber-ballesteros.jpg){.wp-image-39256 width="334" height="320"} Imágen de la amenaza a Huber Ballesteros publicada por Marcha Patriótica\[/caption\]

Huber Ballesteros es un reconocido integrante de la Junta Patriótica, nivel de liderazgo nacional de Marcha patriótica quien fue detenido, acusado de rebelión y dejado en libertad **luego de más de tres años de estar en la cárcel,** segúnel Juzgado 24 de control de garantías “se produjo decaimiento de los fines constitucionales para mantenerlo privado de la libertad”. ([Lea también: Huber Ballesteros en libertad](https://archivo.contagioradio.com/huber-ballesteros-en-libertad/))

Marcha Patriótica, en un comunicado publicado este lunes, hace referencia a las difíciles situaciones de seguridad por las que atraviesa el movimiento y recuerda que es necesario que se garantice la vida e integridad de todos los y las integrantes de esa organización, puesto que el nivel de riesgo va en aumento.

“Sobre el peligro que enfrenta el compañero HUBER DE JESÚS BALLESTEROS GÓMEZ y los integrantes del Movimiento Político y Social Marcha Patriótica en todo el país, cuyo nivel de riesgo cada vez es más crítico” afirma el comunicado. ([Lea tambien: No vamos a darles el gusto de desfallecer](https://archivo.contagioradio.com/cuanto-mas-podra-resistir-marcha-patriotica/))

En Noviembre del 2016 la organización hizo una serie de propuestas al gobierno nacional para impedir que siguieran siendo asesinados o amenazados sus integrantes, que para ese mismo **mes sumaban cerca de 125 agresiones, todas en la impunidad**. En ese momento se consideraba una urgencia vital que se implementara un esquema de seguridad propio para Marcha patriótica, sin embargo hasta el momento no se ha aplicado ninguna de esas recomendaciones.

###### Reciba toda la información de Contagio Radio en [[su correo]
