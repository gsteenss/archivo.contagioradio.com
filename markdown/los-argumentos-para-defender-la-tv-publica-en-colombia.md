Title: Los argumentos para defender la Televisión pública en Colombia
Date: 2018-06-05 12:20
Category: Nacional, Política
Tags: financiación tv pública, ley de regulación de la tv pública, televisión pública
Slug: los-argumentos-para-defender-la-tv-publica-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/De7-mmTW4AAa1nV-e1528218554791.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Señal Memoria] 

###### [05 Jun 2018] 

Con mensaje de urgencia entra a primer debate en las comisiones sextas de Senado y Cámara el proyecto de ley que busca **fusionar en una sola entidad** a la Autoridad Nacional de Televisión y la Comisión de Regulación de Comunicaciones. Los detractores de la iniciativa ven amenazado al futuro de la televisión pública en el país.

La nueva ley contempla que la Comisión de Comunicaciones, como nueva entidad a cargo del Ministerio de Tecnologías de la Información y las Comunicaciones, deberá regular la **prestación de servicios** en las áreas de telecomunicaciones, radiodifusión y televisión.

### **¿Qué contempla el proyecto y qué dicen los detractores?** 

Según el nuevo proyecto de ley, se va a crear un **fondo universal único** a cargo del MinTic, “bajo un ambiente de convergencia” donde “el presente Fondo Universal Único fusiona el Fondo de Tecnologías de la Información y las Comunicaciones”.

Frente a esto, los detractores indican que “para acceder a los recursos del fondo, se deberá hacer mediante **planes, programas y proyectos**”. Esto significa que “el derecho adquirido que tiene la televisión pública se pierde y se puede inferir que podrá hacerlo en igualdad de condiciones un canal comunitario, un canal local, un Youtuber, un bloguero, o cualquier otro agente del mercado, desvaneciéndose el concepto de operador público”.

Adicionalmente, el proyecto en su texto establece que “el objeto del Fondo único es **facilitar el acceso universal** de los habitantes del territorio nacional a las Tecnologías de la Información y las Comunicaciones y los servicios de audiovisuales bajo un principio de eficiencia y eficacia”.

Sin embargo, congresistas de la república y trabajadores del sector de la televisión pública señalan que estas medidas hacen que “se pierda como objeto principal **financiar la programación cultural y educativa** a cargo del Estado y los contenidos de interés público”. Además, se pierde el principio de independencia, es decir, que "para acceder a los recursos se deberá contar con el aval del poder ejecutivo”.

A esto se suma la preocupación de que el acceso a recursos esté limitado por **“criterios políticos”** que "constituyan censura y control previo de los contenidos”. También para tener financiación, la televisión pública deberá gestionar planes, programas y proyectos afectando el derecho adquirido de la misma y “se desvanece el concepto de operador público”.

### **Piden que se elimine el mensaje de urgencia del proyecto de ley 174** 

Según Santiago Rivas, locutor y presentador de la televisión pública, es necesario que la discusión en el Congreso de la República **se aplace para contar con mayores argumentos.** Indicó que el mensaje de urgencia no permite que se desarrolle una discusión seria sobre el articulado y evita que se tuviera en cuenta los argumentos de los detractores del proyecto.

Afirmó no hay claridad en el proyecto de ley especialmente en lo que tiene que ver con los **procesos de financiación de la televisión pública.** Le preocupa a Rivas que no haya claridad sobre la regulación de la televisión comunitaria “pues no se sabe si se considera como televisión pública”.

Adicionalmente, indicó que la dependencia de la entidad de regulación a un Ministerio implica que haya menor independencia y **“va en contravía de los lineamientos que pidió la OCDE”**. Estableció que el mensaje de urgencia para el proyecto de ley se dio antes de la entrada del país a esta organización pero al ya hacer parte “el mensaje de urgencia no se necesita porque se discute el proyecto a las patadas”.

### **Intereses de operadores privados se ven favorecidos** 

Adicionalmente informó que en el país no se ha planteado una regulación para las plataformas privadas de streaming como **Netflix y Amazon TV**, por lo que “no pagan impuestos” y no existe una inclusión de esas plataformas en el sistema de televisión colombiano. Esto quiere decir que el proyecto de ley, favorece a los intereses de estos operadores sobre los de la televisión pública.

Finalmente, Rivas manifestó que el articulado nuevo **“se cae de su propio peso”.** Recordó que la televisión pública cumple con un deber constitucional y “funciona como un termómetro de la democracia”. Por esto exigió que los congresistas escuchen sus argumentos y se les permita participar de un debate sin mensaje de urgencia.

### **Actualización** 

Tras la movilización ciudadana, el Congreso de la República hundió el proyecto de Ley 174.

######  

<iframe id="audio_26369712" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26369712_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
