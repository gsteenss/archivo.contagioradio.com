Title: Destitución de Cunha se da después de realizado el "trabajo sucio"
Date: 2016-05-05 17:37
Category: El mundo, Otra Mirada
Tags: Dilma Roussef impeachment, Eduardo Cunha, Golpe de estado Brasil
Slug: destitucion-de-cunha-se-da-despues-de-realizado-el-trabajo-sucio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cunha-golpe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Revista forum 

##### [5 May 2016] 

La suspensión del diputado Eduardo Cunha como presidente de la Cámara brasileña, tan solo un día después de ser presentado el informe de la comisión senatorial que recomienda aprobar la [solicitud de impeachment](https://archivo.contagioradio.com/impeachment-rompe-la-base-de-la-democracia-rousseff/) contra la presidenta Dilma Roussseff, demuestra una vez más la "ilegitimidad de la votación y la estrategia que se armo" para aprobar el proceso de juicio político.

Así lo afirma Fania Rodrígues, editora de Brasil de Fato, agregando que la decisión tomada por la justicia brasileña demuestra la existencia de una "combinación" que permitió a Cunha "hacer el trabajo sucio", y una vez realizado suspenderlo en busca de dar un "aire de legitimidad" al proceso; alianza que desde las organizaciones y movimientos sociales se lee como "algo arreglado".

Ante la posibilidad de que sean suspendidos más diputados, teniendo en cuenta que 36 de los 38 parlamentarios encargados de votar el impeachment están siendo investigados por delitos de corrupción, la periodista asegura que no se hacen ilusión que estos sean condenados o profundamente investigados al no ser este un proceso "serio", condición en la que en su criterio "el primer alejado de su cargo debería ser el vicepresidente Michel Temer", relacionado con los casos de corrupción en la estatal Petrobras.

Según el análisis que hace la comunicadora, la decisión tomada por parte de la Cámara baja, ha provocado que se construyan dos discursos en el ámbito político, por un lado el Partido de los Trabajadores (PT) y los aliados del gobierno quienes sostienen que todo se trata de un golpe de estado y el de la oposición que descalifica la capacidad de controlar y gobernar de la actual presidenta, solicitando su destitución sin argumentos jurídicos.

 "Es muy difícil en estos momentos que la [comisión](https://archivo.contagioradio.com/rousseff-presentara-defensa-ante-comision-liderada-por-la-oposicion/) que esta en estos momentos en el Senado sea imparcial" por la posición política de quienes la integran, entre estos el relator Antonio Anastasia, principal aliado de Aécio Neves del PSDB , derrotado por Dilma Rousseff en las más recientes elecciones presidenciales, sin embargo Rodrígues afirma que "el juego no esta definido aún" faltando la votación en plenaria del Senado, donde la oposición debe alcanzar 2 terceras partes para lograr su cometido.

La causa de rechazo al impeachment, a logrado reunir a diferentes organizaciones sociales, populares y movimientos políticos de izquierda, aun aquellos que se mantenian como oposición al gobierno, para salir a las calles y decir "No al golpe" y tratar de "constringir" a los políticos y su intención de pasar por encima de las decisiones democráticas, una causa que va más allá de la imagen favorable o no que tengan de Dilma Rousseff.

Ante la posibilidad sobre la posibilidad de una renuncia y llamado a nuevas elecciones, la periodista asegura que las opiniones están divididas, entre quienes dicen que "no sería bueno por que vendría una embestida conservadora" misma que controló el país por 30 años, mientras otros afirman que  "no hay otra solución directa" ante el panorama que llegaría con los políticos acusados de corrupción quienes no tendrían legitimidad para asumir el gobierno.

La actual situación, ha servido además para que los movimientos sociales hagan una "autoevaluación" debido que muchas desaparecieron durante los doce años de gobierno izquierdista, y es importante que lo hagan por que "no hay otra manera de movilizar al país" para salir a la calle a luchar, una ventaja con la que cuentan sobre la derecha.

Por último, Rodrigues, asegura que la crisis en que esta sumergida Brasil, y que empezó el año pasado, no termina con la salida de Dilma Rousseff sino que va a provocar "un cambio", promovido por los ciudadanos contra la forma en que los monopolios perjudican la democracia, en temas como el derecho a las comunicaciones dominado por la red Globo, una de las mayores promotoras del impeachment.

<iframe src="http://co.ivoox.com/es/player_ej_11425905_2_1.html?data=kpahlJqddJahhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncafVz87OjbfTqNPdyNrS1ZCRb6PmwtjWzpDIqYy6wtncj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
