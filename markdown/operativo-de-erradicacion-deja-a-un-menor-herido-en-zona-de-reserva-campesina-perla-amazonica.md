Title: Operativo de erradicación deja a un menor herido en Zona de Reserva Campesina Perla Amazónica
Date: 2020-11-04 10:02
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Erradicación con glifosato, Erradicación Forzada, Puerto Asís, Putumayo, Zona de Reserva Campesina Perla Amazónica
Slug: operativo-de-erradicacion-deja-a-un-menor-herido-en-zona-de-reserva-campesina-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/meno-de-15-anos-resulta-herido-en-medio-de-erradicacion.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/erradicacion-en-Zona-de-Reserva-Perla-Amazonica.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión de Justicia y Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[La Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/)denunció que en horas de la mañana de este lunes 2 de noviembre, **la Unidad Antinarcóticos de la Policía ingresó a la comunidad San Salvador Zona de Reserva Campesina Perla Amazónica, en el departamento de Puumayo con el fin de ejecutar un operativo de erradicación forzada con glifosato.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la comunidad del municipio de Puerto Asís arribaron dos helicópteros y sin establecer un diálogo previo, las unidades arrojaron bombas aturdidoras, gases lacrimógenos y amenazaron a los campesinos con las armas que portaban. (Le puede interesar: [En operativos de erradicación en Putumayo deja dos campesinos muertos y tres heridos](https://archivo.contagioradio.com/en-operativos-de-erradicacion-en-putumayo-deja-dos-campesinos-muertos-y-tres-heridos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de erradicar tres hectáreas de cultivos de hoja de coca de tres familias, **un niño de 15 años de edad de la familia Villarreal, e integrante del grupo Juventud Raíces de Dignidad Perla Amazónica, Juradipa resultó herido al recibir un impactado en el rostro.**

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":92172,"width":362,"height":271,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/meno-de-15-anos-resulta-herido-en-medio-de-erradicacion.jpeg){.wp-image-92172 width="362" height="271"}  
<figcaption>
Foto: Comisión de Justicia y Paz
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

Hacia el medio día, el menor fue trasladado a un centro médico en el centro urbano de Puerto Asís, en donde determinaron que había sufrido una fractura en el cráneo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos hechos, la Comisión denuncia que a pesar de que el pasado 29 de noviembre el juzgado Quinto Laboral del Circuito de Bogotá, a través de un fallo de tutela prohibió a la Fuerza Pública utilizar gases lacrimógenos y bombas aturdidoras, los uniformados continúa desconociendo decisiones judiciales y constitucionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, en lo que va del 2020, y a pesar de la emergencia sanitaria por la Covid-19, los operativos de erradicación forzada continúan en diferentes regiones del país; el más reciente fue llevado a cabo en la vereda Nueva Colombia del Municipio de Vista Hermosa, región del Guayabero en el departamento del Meta, en el que resultaron heridos 4 campesinos y 20 retenidos. (Le puede interesar: [Operativo de erradicación forzada en Guayabero deja 4 heridos y 20 campesinos retenidos por las FFMM](https://archivo.contagioradio.com/guayabero-erradicacion-forzada/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
