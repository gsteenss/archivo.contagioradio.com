Title: En 32 días de paro hospital de segovia ha atendido 50 heridos
Date: 2017-08-22 13:23
Category: DDHH, Nacional
Tags: ESMAD, hospital san juan de dios, paro minero, Remedios y Segovia
Slug: en-32-dias-de-paro-hospital-de-segovia-ha-atendido-50-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/segovia-2-hospital.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Prensa Rural ] 

###### [22 Ago 2017] 

Luego de 32 días de paro que se completa en los municipios de Remedios y Segovia en el nordeste antioqueño, el Hospital San Juan de Dios de Segovia Hasta el momento **han atendido a 50 personas, 3 han fallecido y a 3 más les han tenido que amputar alguna extremidad** por cuenta de los choques con el ESMAD que se encuentra en los alrededores de la institución hospitalaria.

<iframe src="http://co.ivoox.com/es/player_ek_20464469_2_1.html?data=k5WhmJmYepqhhpywj5aUaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncajdzcfS1NnTb67jz9nc28aPt9DW08qYzsaPt8ro1sbQy4qnd4a2lNOY09rJb8La09Tb1saPqc2fqdTgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Gilberto Montoya, encargado de las comunicaciones del Hospital de Primer Nivel San Juan de Dios en Segovia, **“el hospital está mal ubicado porque estamos en todo el centro del conflicto y el ESMAD tiene rodeado el lugar”**. Igualmente, manifestó que, si bien no han dejado de atender a los pacientes, el paso de personas que necesitan ser atendidas se dificulta porque “la población se siente amedrentada al llegar al hospital”. (Le puede interesar: ["Informe ratifica grave situación de DDHH en Segovia y Remedios, Antioquia"](https://archivo.contagioradio.com/situacion-de-ddhh-en-segovia-y-remedios/))

Montoya indicó que el traslado de los heridos al hospital de segundo nivel en el municipio de Yolombó a 2 horas de Segovia, **ha sido complicado por los procesos de remisión que toman mucho tiempo** y requiere de procesos y protocolos que el personal médico debe respetar.

**Insumos quirúrgicos se están acabando**

Montoya denunció que la situación está cada día más grave en la medida que **no tienen los insumos necesarios para atender a todos los heridos**, “estamos pidiendo colaboración para que nos ayuden con insumos de material médico quirúrgico, además no tenemos banco de sangre para atender a los pacientes que la requieren”.

Adicionalmente estableció que en el centro de salud han atendido a **50 personas entre manifestantes y agentes de policía** que han resultado heridos por los enfrentamientos. Dijo que en medio de las manifestaciones a las ambulancias no las han dejado pasar dificultando aún más la atención de los heridos. (Le puede interesar: ["Policía usa francotiradores para reprimir mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/))

Finalmente recordó que la situación en los municipios se agrava con el pasar de los días y **hay un desabastecimiento generalizado de alimentos** que pone en riesgo la salud de los habitantes. Indicó que ya le entregaron a las autoridades de Medellín un listado con los insumos que requieren y donde alertan sobre la problemática del hambre que se vive allí.

**¿Cómo sigue el paro en estos municipios?**

<iframe src="http://co.ivoox.com/es/player_ek_20471745_2_1.html?data=k5WhmZabeJahhpywj5WcaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncavVytLSjazFsM3ZyNSSlKiPvYzgwpDOxdnZpc3dxcbRjcnJsIzkwtfcjdLNssbm0JDS0JC3qcjj15KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Se completan ya 32 días de paro donde según Jaime Gallego, miembro de la Mesa Minera, aún **no hay una solución a la grave situación laboral y económica** que viven los mineros artesanales. Ante esto, afirmó que “el Gobierno Nacional y la empresa multinacional han hecho acercamientos que no le han informado a la Mesa Minera”. (Le puede interesar: ["Mineros de Remedios y Segovia completan un mes de paro"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-completan-un-mes-en-paro/))

Dijo que hay un **intento por desvirtuar la información que brindan los mineros sobre las exigencias** que le han hecho llegar al Gobierno Nacional y a la multinacional Grand Colombia Gold. Reiteró que necesitan que se formalice la actividad minera que realizan además de llegar a acuerdos equitativos con la multinacional para sostener la cadena de producción de la que viven los mineros.

Adicionalmente **rechazó las declaraciones del gobernador de Antioquia Luis Pérez Gutiérrez** asociadas a la criminalización de la protesta y la acusación al cuerpo de bomeberos voluntarios. Manifestó también que las movilizaciones se hicieron desde un principio de manera pacífica y la situación se ha dificultado debido a la presencia del ESMAD en el territorio.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
