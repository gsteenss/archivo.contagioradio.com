Title: Gobierno y ELN deben acercar sus lógicas para salvar el proceso de paz
Date: 2018-02-13 16:06
Category: Nacional, Paz
Tags: ELN, Gobierno, Gustavo Bell, Proceso de paz en Quito
Slug: gobierno-y-eln-deben-acercar-sus-logicas-para-salvar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Feb 2018] 

Las organizaciones sociales que se reunieron con el gobierno Santos y con la delegación de paz del ELN concluyen que tanto la guerrilla del ELN como el gobierno deberían dejar de realizar acciones que entorpecen el proceso. Además el ELN sigue viendo las conversaciones en una lógica formal y por tanto no estarían incumpliendo ningún acuerdo, y el gobierno actúa en lógica política y eso aleja las posiciones.

El fin de semana pasado, una comisión de delegados pertenecientes a organizaciones civiles se reunió con una comitiva del ELN, para dialogar sobre las formas de destrabar la mesa de conversaciones en Quito y **la importancia de disminuir la intensidad de las acciones del conflicto armado**. [** **]

[Sin embargo Luis Eduardo Celis, analista y quien participó en el encuentro, afirmó que tanto la guerrilla como el gobierno deben dejar de lado acciones que entorpezcan el proceso paz. (Le puede interesar: ["Paro armado tensiona más el ambiente para la mesa en Quito"](https://archivo.contagioradio.com/paro_armado_mesa_quito_eln_gobierno/))]

Para Luis Eduardo Celis, integrante de Ideas para la Paz, organización que participó en el evento, la crisis por la que está atravesando este proceso de paz, revela que hay dos lógicas distintas, por un lado, está la del ELN con una lógica formal, que para el analista tiene razón al decir que no está incumpliendo nada y que continúa dialogando en medio del conflicto. **Por el otro lado, está la lógica de sensibilidad política, de asumir que este momento es delicado**.

“Se requiere que ambos encuentren un campo de común entendimiento, es decir, se requiere un poco de lógica formal y lógica política para recomponer la situación” señaló Celis y agregó que en ese sentido hay iniciativas desde diversos sectores como la comunidad internacional, los países garantes, el movimiento social y la conferencia Episcopal, que continúan con esfuerzos para apoyar el proceso.

### **Las líneas rojas** 

Celis aseguró que la mejor forma de superar la inamovilidad que tiene el proceso en estos instantes se encuentra en que ambas partes salgan tanto de la lógica política como de la lógica formal, **además de discreción para que se logré un avance significativo**. (Le puede interesar: ["Proceso de paz con el ELN es un bien público y debe continuarse: Movimiento social"](https://archivo.contagioradio.com/proceso-de-paz-con-el-eln-es-un-bien-publico-y-debe-retomarse-movimientos-sociales/))

Sin embargo, acciones como la orden de captura desde la Fiscalía a la cúpula del ELN, para Celis, hacen parte de esos actos que “no ayudan” porque hay un proceso de paz en donde el gobierno ha reconocido jefes negociadores del ELN , así mismo señaló que el paro armado del ELN tampoco contribuye a un escenario de dialogo, **y que ambas acciones, entorpecen el camino**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
