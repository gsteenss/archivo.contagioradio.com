Title: Proceso entre ELN - Gobierno no será una Paz Express
Date: 2017-02-08 17:25
Category: Nacional, Paz
Tags: ELN, Proceso de paz en Quito
Slug: proceso-entre-eln-gobierno-no-sera-una-paz-express
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/sILVANA-GUERERO-ELN-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa] 

###### [8 Feb 2017] 

La mesa de conversaciones de paz entre el gobierno y el ELN arrancó hoy su primera ronda. Según lo establecido, los próximos 40 días las comisiones de gobierno y guerrilla se dividirán para trabajar los temas de **participación de la sociedad y sus mecanismos, y por otro lado las medidas humanitarias y de desescalamiento del conflicto.**

De acuerdo con el gestor de paz Carlos Velandia, lo más importante del momento de instalación fue el **“tono sereno, preciso y conciliador”** de ambas partes, y la claridad sobre que **no se hará un proceso de paz express, sino por el contrario será riguroso, con la celeridad necesaria**, “entendiendo la angustia del tiempo pero procurando avanzar sin maltratar el proceso”.

De igual forma resaltó que esa **valoración que se le dio a los diálogos de La Habana es positiva** en el marco de un proceso único y nacional de solución política del conflicto armado, por ende considera que la Jurisdicción Especial para la Paz será una de las herramientas más importantes en este proceso. Le puede interesar: ["Los retos de las conversaciones de paz ELN-Gobierno"](https://archivo.contagioradio.com/los-retos-de-las-conversaciones-de-paz-gobierno-eln/)

### **“Todos debemos cambiar” Pablo Beltrán**

Beltrán expresó la necesidad de garantías en  el ejercicio de la política y defensa de derechos humanos en Colombia para que “**la gente se anime a participar del proceso de paz**” e igualmente señalo que no es posible que mientras se habla de paz se continúe con el asesinato de líderes sociales y la estigmatización. A su vez, afirmó que como guerrilla que busca el tránsito a la vida política están dispuestos a asumir las responsabilidades que causaron durante el conflicto armado.

Para Velandia este discurso fue el reflejo de la construcción ideológica que ha hecho el Ejército de Liberación Nacional y  puso el acento en el carácter del proceso, es decir en la **superación del conflicto armado a cambio de más democracia, inclusión y soberanía.** Le puede interesar:["Para negociar es necesario abrir debates y para eso se requiere a la sociedad"](https://archivo.contagioradio.com/para-negociar-es-necesario-abrir-debates-y-para-eso-se-requiere-a-la-socieda/)

### **"Queremos dar pasos firmes hacia una Colombia en paz" Juan Camilo Restrepo**

Sobre la participación social en este proceso **Restrepo indicó que esta no será desbordada y que las propuestas que surjan de diferentes escenarios servirán como insumos** para el debate en la mesa de Quito, visión diferente a la que ha planteado el ELN.

Sin embargo durante su discurso Juan Camilo Restrepo condiciono las conversaciones al afirmar que **“sin una renuncia pública del ELN al secuestro, que es injustificable, será muy difícil alcanzar acuerdos, ni ganar confianzas para la mesa**”. Le puede interesar:["ELN y Gobierno cumplen acuerdos humanitarios"](https://archivo.contagioradio.com/cumplidos-los-acuerdos-humanitarios-para-iniciar-conversaciones-gobierno-y-eln/)

Velandia afirmó que el discurso de Juan Camilo Restrepo fue preciso al referirse a la agenda pactada el 30 de marzo de 2016 como la **“carta de navegación”** que dará paso a las conversaciones y frente a aclarar que la fase exploratoria ya pasó, enfatizando en que ahora la disposición por parte del gobierno será propositiva, hecho que de acuerdo con Velandia rompe con las posturas que el gobierno ha tenido en otros procesos de paz.

### **El nudo de la participación social**

La Mesa Social para la Paz estuvo durante la instalación de la Mesa, Marylen Serna, vocera de esta plataforma señaló que la participación debe ser “**directa, decisoria, protagónica, amplia y desde la diversidad social y vinculante”** motivo por el cual seguirán en la construcción de propuestas de paz para ser tenidas en cuenta en la mesa de Quito.

Sobre las declaraciones de Restrepo, Marylen Serna dijo que insistirán en que esa participación no se un ejercicio vacío para dar una idea de inclusión **“se deben convencer a más sectores de la sociedad y movilizarnos para que el gobierno nos escuche** y atienda las solicitudes y exigencias que tienen las comunidades a partir de sus necesidades”

Durante estos 40 días que vienen, la Mesa Social para la Paz, estará convocando más sectores a unirse al diálogo, de igual forma se creará el comité de impulso de este escenario y se **avanzará más en el trabajo territorial para tener una diversidad e inclusión real en este espacio.**

<iframe id="audio_16908416" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16908416_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
