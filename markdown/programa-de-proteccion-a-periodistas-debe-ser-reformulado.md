Title: Programa de Protección a Periodistas debe ser reformado
Date: 2015-08-27 15:41
Category: infografia, Nacional
Tags: Andiarios, Fabiola Posada, Federación Colombiana de Periodistas, FLIP, Libertad de Prensa, periodismo en colombia, Reporteros Sin Fronteras
Slug: programa-de-proteccion-a-periodistas-debe-ser-reformulado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/libertad_de_expresion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [FOTO: Díariolavoz.com] 

<iframe src="http://www.ivoox.com/player_ek_7637710_2_1.html?data=mJugmZyVdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZOmqcbmjdbZqYzmxsvc1NLZsMLmjMnSxdfJuNCf1NTP1MqPlNPjyNfOz8aPqMafsdfc1srHp8qZpJiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fabiola León Posada, Reporteros sin Fronteras] 

###### [27 ago 2015] 

En el marco de la campaña "Periodismo en Riesgo" desarrollada por la Fundación para la Libertad de Prensa, Reporteros Sin Fronteras y la Federación Colombiana de Periodistas, se evaluó el "Programa de Protección a Periodistas" mediante dos informes realizados por estas organizaciones, allí se concluyó que es necesario reformar el programa, que ya lleva 15 años, teniendo en cuenta que  no se están ofreciendo las garantías necesarias para ejercer el periodismo en Colombia.

Según el informe publicado por la FLIP, **“15 años de protección a periodistas en Colombia: esquivando la violencia sin justicia”,** los dos grandes problemas por los cuales no se están dando garantías a los periodistas, tiene que ver con la corrupción y la impunidad.

De acuerdo con Posada, la información investigada siempre termina siendo parcializada, y nunca se cuestiona los entes gubernamentales, que en algunos casos son los autores de las violaciones a los derechos humanos de los y las periodistas como ellos mismos lo han denunciado, asegurando que existen alianzas entre grupos armados, gobierno y empresarios. Razón por la cual se desconfía al momento de realizar la denuncia ante las autoridades.

Este año han sido asesinados 2 periodistas y hubo un intento de homicidio. Según la periodista de Reporteros Sin Fronteras, a**  la fecha hay 100 periodistas amenazados**, pero esta cifra aumentará con las elecciones del mes de octubre.

**El programa para proteger a los periodistas actualmente a atiende 131 personas**, para realizar el informe se recopilaron los testimonios del 78% de estos periodistas, quienes cuentan que en la mayoría de los casos les han brindado medidas blandas (Entregas de chalecos antibalas; celulares satelitales y apoyos re ubicación), además se denuncia que las medidas son tardías y paulatinas.

Por otra parte el informe data el papel del Estado a la hora de realizar los análisis de riesgo e implementar metodologías. La constante es, que el Estado indica si el periodista merece o no protección, afectando la articulación entre las entidades y a los amenazados.

[![info periodismo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/info-periodismo.jpg){.aligncenter .wp-image-12961 width="518" height="677"}](https://archivo.contagioradio.com/programa-de-proteccion-a-periodistas-debe-ser-reformulado/info-periodismo/)

[15 Años de Protección a Periodistas en Colombia](https://es.scribd.com/doc/276544319/15-Anos-de-Proteccion-a-Periodistas-en-Colombia "View 15 Años de Protección a Periodistas en Colombia on Scribd")

<iframe id="doc_46517" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/276544319/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
