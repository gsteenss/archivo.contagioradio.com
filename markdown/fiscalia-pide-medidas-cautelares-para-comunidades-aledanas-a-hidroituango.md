Title: Fiscalía pide medidas cautelares para comunidades aledañas a Hidroituango
Date: 2019-04-12 23:55
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Empresas Públicas de Medellín, Fiscalía General de la Nación, Hidroituango, Movimiento Ríos Vivos, Río Cauca
Slug: fiscalia-pide-medidas-cautelares-para-comunidades-aledanas-a-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Hidroituango-EPM-e1555166334695.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La Fiscalía General de la Nación solicitó medidas cautelares para 60 mil habitantes de los municipios aledaños al proyecto Hidroituango para "**frenar el grave daño a los recursos naturales, a la vida y la economía de los pobladores"** que fue evidenciado por un equipo interdisciplinario de expertos, conformado por el ente investigador.

Estos peritos recorrieron las zonas de influencia del proyecto, en **Sabanalarga, Valdivia, Toledo e Ituango** Antioquia, por dos semanas e identificaron **cuatro riesgos inminentes sobre el Río Cauca**. Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos - Antioquia,  indicó que esta investigación corroboró los hechos que la organización viene denunciando desde 2016.

Para la activista, la solicitud del fiscal Nestor Humberto Martínez representa un importante primer paso, sin embargo las comunidades le piden al Fiscal que no use su dolor y el tema de Hidroituango para favorecer su reputación. Ahora que la solicitudes se han realizado, espera que el juez de control de garantías asignado a este caso otorgue las medidas cautelares para todas las comunidades.

Adicionalmente, Zuleta sostiene que la Fiscalia también debería pedir medidas para detener **el incremento de casos de leishmaniasis en Antioquia**, que las comunidades señalan proviene de las pilas de árboles talados por Empresas Públicas de Medellín (EPM) que hoy se encuentran en un estado de descompisición por todo el municipio.

### **Los hallazgos de la investigación** 

En primer lugar, el equipo encontró que a unos 55 kilómetros al sur de la presa existe una invasión extensiva de la **planta acuática buchón, lo que ha disminuido la calidad de vida acuática y generado bloqueos de masa orgánica por el Río Cauca**. Este especie crece en aguas empozadas y como lo indica la Fiscalía, los esfuerzos de EPM de controlar su reproducción han resultado "ineficientes".

En menos de dos meses, la presencia de esta especie creció de manera "exponencial" y actualmente, cubre **8.5 kilómetros** del río, aproximadamente 70 cuadras de Bogotá. Por tal razón, la Fiscalía solicitó que se implemente **un plan de choque para la recolección del buchón** presente sobre el cauce del río.

El equipo especializado también encontró una **planta de asfalto abandonada en la que hay residuos peligrosos, incluso algunos con características cancerígenas,** que provienen de las obras contratadas por EPM para ampliar y reparar las vías entre Ituango, Valle de Toledo, Toledo y Puerto Valdivia. Según la Fiscalía, no se realizó un cierre técnico de esta planta ni una recolección adecuada de las sustancias como lo establecen las normas ambientales.

Dichas materias estarían cayendo a los cuerpos de agua y poniendo en riesgo la salud de las comunidades, en especial los niños de la zona. Por tal razón, el ente investigador pide **un cierre técnico de la planta, además de la recolección y disposición adecuada de los residuos**.

En tercer lugar, los investigadores descubrieron entre **2,5 a 3 millones de toneladas de residuos sólidos** en una escombrera conocida como El Higuerón, entre Puerto Valdivia y la hidroeléctrica. Además, encontró grietas de 60 metros de longitud y filtraciones de agua que podrían ocasionar un derrumbe sobre una vía y por lo tanto, represar el cauce del río.

Al respecto, la Fiscalía pide la estabilización de taludes y manejos de aguas superficies en la escombrera y el cumplimiento de las normas técnicas de cierre y abandono establecidas por la autoridad ambiental.

Finalmente, **la presencia de buchón y otros riesgos está reteniendo "la riqueza biológica y minerales del Río Cauca, aguas arriba**. Así lo denunció el equipo de expertos, quienes aseguran que el agua que pasa por la represa es filtrada y por ello fluye hacia las poblaciones del norte con consecuencias  para la sostenibilidad y la biodiversidad del río.

Los expertos encontraron que esta situación afecta a la población de peces, en calidad, tamaño, carne y en su misma reproducción, lo cual "golpea la seguridad alimentaria y la principal actividad económica de los pobladores del Río Cauca". Por lo tanto, la Fiscalía pide medidas inmediatas para mitigar los efectos adversos sobre las poblaciones de Ituango, Puerto Valdivia, Briceño, Cáceres, Taraza, Caucasia y demás municipios que sustentan la pesca y sustento diario de esta cuenca.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
