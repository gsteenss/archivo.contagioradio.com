Title: Indígenas de Caldono, Cauca recapturaron a quienes asesinaron a comunicador indígena
Date: 2018-03-07 13:04
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Caldono, Cauca, Eider Campo Hurtado, FARC, indígenas, indígenas cauca
Slug: indigenas-de-caldono-cauca-re-capturaron-a-quienes-asesinaron-al-comunicador-indigena
Status: published

###### [Foto:Ana Karina Delgado Diaz] 

###### [07 Mar 2018] 

Luego de que el pasado 5 de marzo fuera asesinado el comunicador indígena **Eider Arley Campo** en el resguardo indígena de Pioyá en Caldono, Cauca, los comuneros informaron que recapturaron a las personas responsables de estos hechos.

Hay que recordar que el asesinato del comunicador sucedió cuando la Guardia Indígena estaba realizando una persecución para tratar de atrapar a varios hombres que, de acuerdo con el CRIC **“tienen vínculos con las Fuerzas Armadas”**. Estas personas tenían intenciones de sacar del Resguardo a “tres delincuentes confesos y convictos en la Asamblea Comunitaria bajo jurisdicción indígena” y en ese momento se presentaron los enfrentamientos que acabaron con la vida del joven.

### **Indígenas re capturaron a ocho personas** 

De acuerdo con Pablo Piso, alcalde de Caldono, Cauca, los indígenas re capturaron a ocho personas que habrían estado involucrados en la fuga de los detenidos y en el asesinato de Eider Campo. Indicó que “**todos iban con prendas militares**, armados con cinco fusiles, dos pistolas, granadas y bastante material de intendencia que la Guardia Indígena entregó al Cuerpo Técnico de investigación para que puedan hacer la prueba balística”.

Afirmó que los indígenas **“van a iniciar el juzgamiento”,** y añadió que las personas que ingresaron al cabildo para sacar a los detenidos, “eran indígenas de la región de resguardos aledaños”. Aún los indígenas se encuentran en la búsqueda de una persona que no pudo ser capturada y que hizo parte de los hechos. (Le puede interesar:["Asesinan a comunicador indígena del resguardo de Pioyá en Caldono"](https://archivo.contagioradio.com/asesinan-a-comunicador-indigena-del-reguardo-de-pioya-en-el-cauca/))

### **FARC expulsó del espacio territorial a tres ex combatientes que participaron de los hechos** 

En un comunicado del Espacio Territorial Transitorio de Normalización Carlos Perdomo de Caldono, los excombatientes afirmaron que tres personas que hacían parte del proceso de reincorporación **están involucradas en los hechos**. Manifestaron que “actuaron de manera personal y sus actos son solo responsabilidad de cada uno ellos”.

Por esto, aclararon que “estas personas **quedan expulsadas del partido**, la cooperativa COOMEEP y el Espacio Territorial Transitorio de Normalización y quedan excluidos de cualquier beneficio de la reincorporación”. Además, rechazaron las actuaciones y los hechos que terminaron costándole la vida al comunicador indígena por lo que manifestaron su solidaridad con la familia y los indígenas del resguardo Pioyá.

Finalmente indicaron que **desconocen la procedencia de las armas**, “ya que estas estaban en nuestras manos, fueron dejadas ante la organización multilateral de Naciones Unidas como lo contempla el acuerdo de la habana”. Asimismo, reiteraron que respetan y respaldan “la decisión autónoma que frente a los hechos tomen las autoridades indígenas y la asamblea en contra de estas personas”.

<iframe id="audio_24272523" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24272523_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
