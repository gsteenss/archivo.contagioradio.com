Title: Piden que el Bosque Bavaria sea reserva forestal protegida: Ambientalistas
Date: 2019-07-12 16:07
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: bosque, Juez, Medida Cautelar, Peñalosa
Slug: piden-que-el-bosque-bavaria-sea-reserva-forestal-protegida-ambientalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-12-at-4.01.51-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo Salvemos el Bosque Bavaria  
] 

El pasado 10 de julio, un juez 22 administrativo de Bogotá concedió una medida cautelar sobre el Bosque Bavaria para evitar que sean talados sus árboles, hasta que se decida de fondo, qué ocurrirá con el proyecto que espera construir cerca de 9 mil viviendas en la zona. Integrantes del Colectivo **Salvemos el Bosque Bavaria** dijeron que la medida los alegraba, pero señalaron que el siguiente paso sería la declaración del lugar como reserva forestal protegida.

### **Una lucha de más de 9 años por el Bosque que rindió frutos** 

**Laura Cala, integrante del Colectivo Salvemos el Bosque Bavaria,** recordó que la lucha por defender las 48,6 hectáreas de cobertura vegetal inició hace 9 años, cuando se presentó una tala de 150 árboles. En su momento, se estableció una veeduría ciudadana, y las personas decidieron "movilizarse contra este ecocidio en la localidad de Kennedy, que es la más contaminada de Bogotá".

Para generar conciencia sobre la importancia de este lugar, los ciudadanos realizaron diferentes actividades, entre las que se encuentra un diplomado en sustentabilidad**."Bavaria es el único pulmón que tenemos al suroccidente de Bogotá", y es capaz de capturar mil toneladas de CO2 al año, y generar mil toneladas de oxígeno en el mismo periodo**, manifestó la Activista. (Le puede interesar:["Ordenan a Peñalosa detener tala de árboles en Parque Japón"](https://archivo.contagioradio.com/penalosa-detener-arboricidio-parque-japon/))

> Mantengo la posición: el bosque debe existir, pero no el mismo! La renovación urbana debe garantizar la salida de los eucaliptos, les he invitado a verlo en cerros orientales. Aprovechemos la oportunidad para mejorar la calidad ambiental de la localidad... <https://t.co/DeqVBO4j9W>
>
> — Brigitte LG Baptiste (@Brigittelgb) [11 de julio de 2019](https://twitter.com/Brigittelgb/status/1149385015547092992?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Sobre la postura de Brigitte Baptiste, directora del Instituto Humboldt, quien se manifestó negativamente contra las medidas cautelares, Cala manifestó que en principio ella no dijo nada de la tala de los árboles, luego estuvo en contra de la tala y ahora a favor. Sin embargo, con argumentos, Cala quiso mostrar las razones para proteger este ecosistema. (Le puede interesar:["B. Bavaria es la compensación del daño ambiental causado por la Empresa"](https://archivo.contagioradio.com/bosque_bavaria_tala_arboles/))

### **Las cinco razones de peso para defender el Bosque Bavaria** 

Cala aseguró que "el Bosque tiene un efecto termorregulador del sector"; **aísla la contaminación acústica y del aire generadas por la avenida Boyacá y la calle 13;** sirve de tránsito para aves migratorias, y otras que residen en Bogotá como el Suiriri y el Cardenalito; y porque en el lugar, además de Eucaliptos, hay Urapanes, Manos de Oso, Cipreses, Chicalas y otras especies de flora y fauna que requieren protección.

El fallo reconoció que no hay un inventario conjunto de árboles, razón por la que detuvo cualquier intervención en el predio; sin embargo, los ambientalistas señalan que en las cerca de 50 hectáreas de cobertura vegetal podrían haber entre 20 y 25 mil individuos. Por esos árboles, y las anteriores razones, la Ambientalista anunció que los siguientes pasos que darán en aras de proteger el Bosque serán intentar que se declare como reserva forestal protegida, y que se derogue el decreto 364 de 2017 que permite la ejecución del Plan Parcial Bavaria Fábrica.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38372692" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38372692_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
