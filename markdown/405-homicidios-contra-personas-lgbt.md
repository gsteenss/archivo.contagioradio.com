Title: 405 homicidios contra personas LGBT en los últimos 4 años
Date: 2016-12-23 16:00
Category: LGBTI, Nacional
Tags: Activistas LGBT, Asesinatos a líderes LGBT, Derechos LGBT
Slug: 405-homicidios-contra-personas-lgbt
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/homofobia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AlRosario] 

###### [22 Dic 2016]

El informe realizado por Colombia Diversa, Caribe Afirmativo y Santamaría Fundación, "Cuerpos excluidos, Rostros de Impunidad" reveló que **desde el 2011 hasta el año 2015 se registraron 405** homicidios contra personas LGBT por su condición de orientación sexual diversa.

Muestra de esa intolerancia, fue el hallazgo del cuerpo de un hombre en una tubería del municipio de Zona Bananera, Magdalena, que correspondería a un nuevo crimen de odio contra un integrante de la comunidad LGTBI en la región caribe colombiana. El cadáver identificado con el nombre de** Jhon Jairo Romero Cantillo,  activista gay de 29 años tenía muestras de múltiples formas de violencia. **

Wilson Castañeda, director de la organización Caribe Afirmativo, aseguró que el joven que había sido reportado desaparecido por su familia el 17 de Diciembre, fue asesinado y no se suicidó, como algunos medios locales aseguraron. Añade que las autoridades también determinaron que **el cuerpo de Romero tenía rastros de ataques con arma contundente y arma blanca.**

\[caption id="attachment\_34070" align="aligncenter" width="508"\]![Homicidios LGBT](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Captura-de-pantalla-2016-12-23-a-las-13.12.07.png){.size-full .wp-image-34070 width="508" height="579"} Homicidios LGBT\[/caption\]

### **La homofobia mata** 

Wilson Castañeda, advierte que si bien se han aumentado los liderazgos de personas LGBT en distintas regiones del país **"hay una ecuación que para nada nos gusta, a mayor visibilidad mayor riesgo de las personas LGBT". **

Por otra parte, el director de Caribe Afirmativo advierte que otra de las preocupaciones es la centralización de las acciones en defensa de estas comunidades, **"vemos que las labores investigativas y de seguimiento a crímenes se quedan en unas pocas ciudades,** mientras otras regiones como el Magdalena donde hay varios casos, las investigaciones no han tenido importantes avances".

Por último, Castañeda manifiesta que es fundamental para el 2017 "consolidar agendas sólidas desde las distintas organizaciones LGBT **para blindar y proteger los logros alcanzados en materia de Derechos Humanos para esta población". **

[Cuerpos Excluídos, Rostros de Impunidad - Informe Violencia LGBT Colombia DDHH 2015](https://www.scribd.com/document/334948431/Cuerpos-Excluidos-Rostros-de-Impunidad-Informe-Violencia-LGBT-Colombia-DDHH-2015#from_embed "View Cuerpos Excluídos, Rostros de Impunidad - Informe Violencia LGBT Colombia DDHH 2015 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_5565" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/334948431/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-2QjPKBnA5xQaZ4mtoWEi&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7504159733777038"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<iframe id="audio_15357701" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15357701_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
