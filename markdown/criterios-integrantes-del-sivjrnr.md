Title: Víctimas proponen criterios para escoger integrantes del SIVJRNR
Date: 2017-04-18 14:52
Category: Entrevistas, Judicial
Tags: comision de la verdad, JEP, SIVJRNR
Slug: criterios-integrantes-del-sivjrnr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/VÍCTIMAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [18 Abr 2017] 

En una carta dirigida al comité de escogencia, más de 60 organizaciones de Derechos Humanos y de víctimas propusieron una serie de **criterios** para la elección de las personas que van a hacer parte de los diferentes componentes del **Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).**

Además propusieron la realización de una **audiencia pública con diversas organizaciones de víctimas y de derechos humanos, para que se expongan de manera amplia los criterios** que se consideren concernientes en torno a la aplicación eficaz del SIVJRNR contemplado en los acuerdos de la Habana y que se están tramitando en el congreso de la república.

Las propuestas concretas giran en torno a **9 criterios generales y 12 específicos para los integrantes de la Comisión para el esclarecimiento de la verdad**, el director de la Unidad de Búsqueda de personas desaparecidas, los magistrados y magistradas de la Jurisdicción Especial de Paz y el director o directora de la Unidad Especial de investigación para el desmantelamiento de las organizaciones sucesoras del paramilitarismo.

### **Las propuestas de criterios generales** 

Los criterios generales estarían fundamentados en la **transparencia y publicidad del proceso, participación ciudadana, centralidad de las víctimas**, enfoque territorial y diferencial, enfoque étnico racial, enfoque de género, solución política y construcción de paz, integralidad y transparencia e imparcialidad. ([Le puede interesar: Si el Estado no juzga a los militares que lo haga la CPI](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/))

Para los órganos que hacen parte del Sistema propusieron criterios de **formación, experiencia, relacionamiento, enfoque psicosocial y enfoque de género**. También tienen en cuenta ciertos impedimentos que deberían ser revisados antes del nombramiento de los dignatarios de los cargos estipulados en el SIVJRNR. ([Lea también: Comisión de la verdad debe investigar la responsabilidad de las empresas en la guerra](https://archivo.contagioradio.com/comision-verdad-esclarecer-responsabilidad-empresarial/))

Para los firmantes estos criterios “reflejan la diversidad y amplitud de un proceso de confluencia” que también debería ser garantizado con la creación de un reglamento que tenga en cuenta la evaluación o un “seguimiento al cumplimiento de las funciones y obligaciones de las/os seleccionados/as”.

### **El comité de escogencia de integrantes de SIVJRNR** 

El comité de escogencia está integrado por el Centro Internacional de Justicia Trancisional, un delegado de la Sala Penal de la Corte Suprema de Justicia, un delegado por la Secretaría General de las Naciones Unidas, la Comisión Permanente del Sistema Universitario del Estado y un dDelegado por el Presidente de la Corte Europea de Derechos Humanos.

Se tiene previsto que este miercoles 19 de Abril se de una **primera reunión entre los cinco delegados y delegadas de ese comité para que se comience la elección de los magistrados que harán parte de Jurisdicción Especial de Paz.**

[Carta Al Comité de Escogencia. Criterios de Selección Candidatos SIVJRNR](https://www.scribd.com/document/345537227/Carta-Al-Comite-de-Escogencia-Criterios-de-Seleccion-Candidatos-SIVJRNR#from_embed "View Carta Al Comité de Escogencia. Criterios de Selección Candidatos SIVJRNR on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_18234874" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18234874_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_87455" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/345537227/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-uDCyWQ19mwBi0ezrG0XV&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
