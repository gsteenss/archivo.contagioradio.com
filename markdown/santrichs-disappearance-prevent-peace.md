Title: Could Santrich’s disappearance prevent peace? 
Date: 2019-07-20 18:24
Author: CtgAdm
Category: English
Tags: Jesús Santrich, Peace Agreement
Slug: santrichs-disappearance-prevent-peace
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Jesus-Santrich-en-entrevista-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[On June 29 the National Protection Unit (NPU) warned that Seuxis Pausias Hernández Solarte, better known as Jesús Santrich, abandoned his protection scheme at the“Territorial Area for Training and Reintegration” (TART)  in Tierra Grata (César department). Although this event has generated expectation, his lawyer Eduardo Matías said that it could have been the result of some security issue, while the journalist Patricia Lara said that it is necessary to continue with the implementation and not let this issue deviate the situation for the ex-combatants.]

### [«]**Security situation affected Santrich’s mood**[»] 

[Santrich has been criminalized, through the media, by the President of the Republic, the Ministry of Defense, the Vice President of the Republic and even the Procurator, "said Matías, adding that there is a campaign against the member of the FARC party, which "has put their physical integrity and freedom at risk".]

[These situations, according to the lawyer, could have caused Santrich to take action to avoid an attack against his life. On the other hand, Matias said that for the trial that will take place on July 9 in the Supreme Court of Justice, for drug trafficking against Hernández Solarte, the defense will assist and wait for his attorney to come forward. Otherwise, the Court will decide the procedure of the case.]

### **«It’s a matter of peace» ** 

[For the journalist and analyst Patricia Lara, the departure of Hernández Solarte was disconcerting, however, she emphasized that "Santrich is not peace, and we cannot  divert the process for the more than 10 thousand ex-combatants who have taken part in the process and who are fulfilling their part by abiding by the Agreements ». (Read also: [Colombian's Transitional Justice System goes to Inter-American-Commission on Human Rights](https://archivo.contagioradio.com/the-reason-why-the-special-jurisdiction-for-peace-will-be-in-the-inter-american-commission-on-human-rights-focus/))]

[To such extent, he said that in objective terms Santrich's should not have a greater impact, and he assured that "the process has to continue and peace must continue. If Santrich does not show up on July 9, he will have hurt the process, because he would give arguments to the peace retractors who will say: I told them so. "]
