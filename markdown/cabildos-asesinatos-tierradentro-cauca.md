Title: Comunidades indígenas denuncian incremento de la violencia en Tierradentro, Cauca
Date: 2019-09-17 17:18
Author: CtgAdm
Category: Comunidad, Nacional
Tags: acuerdos de paz, Asesinato de indígenas, Cauca, Comunicado
Slug: cabildos-asesinatos-tierradentro-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

[La Asociación de Cabildos Nasa Cxha Cxha emitió un comunicado denunciando la constante violencia que sufre la comunidad indígena en Tierradentro, Cauca. En el escrito, dirigido a la opinión pública, afirman que “después de la firma de los acuerdos de paz sigue la suma de asesinatos en nuestros territorios, mientras que el gobierno se lava las manos manifestando que viene haciendo todo lo posible para identificar el origen de las muertes”. ]

[En el documento también se denuncia el asesinato de dos comuneros indígenas en menos de ocho días en esa municipalidad: el pasado 8 de septiembre fue asesinado el comunero y guardia Indígena Mario Alberto Achicué en su casa. Posteriormente, el 13 de septiembre, el comunero Henrry Cayuy Puque también fue asesinado.]

[Ante estos hechos, afirman que “]**a nivel nacional el contexto es bastante aterrador**[, las amenazas, persecuciones, intimidaciones, asesinatos, atentados y masacres por parte de grupos armados legales e ilegales no cesan”. (Le puede interesar [Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios](https://archivo.contagioradio.com/comunidades-cauca-no-terror/))]

### Ausencia del Gobierno en Cauca

“Se debe a la disputa por el control del territorio. Frente a estos hechos, las comunidades han rechazado rotundamente estas actuaciones de grupos que quieren el control territorial y que asesinan a los comuneros que no se someten a lo que ellos quieren hacer”, afirma Hermes Pete, consejero mayor del Consejo Regional Indígena del Cauca (CRIC).

A esto se suma la falta de protección por parte del Gobierno y de las instituciones mientras que son las propias comunidades las que asumen un rol de control y protección de sus propios territorios. En ese sentido, reclaman a las autoridades[ “mantener y seguir brindando su apoyo a los procesos comunitarios de cuidado y defensa de la vida”.]

El consejero agrega que la única alternativa que les ofrece el Gobierno es la militarización de la zona, sin embargo señala que lo que en realidad necesita la comunidad es la implementación del acuerdo, con especial énfasis en cumplir con la sustitución de cultivos de uso ilícito, la continuidad de los Programas de Desarrollo con Enfoque Territorial (PDET) y realizar inversión social en el territorio.

<iframe id="audio_41721255" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41721255_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
