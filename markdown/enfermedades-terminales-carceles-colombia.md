Title: Tratamiento de las enfermedades terminales en las cárceles de Colombia
Date: 2018-08-21 10:00
Category: Expreso Libertad
Tags: presos politicos, salud carceles
Slug: enfermedades-terminales-carceles-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/51f9f8a5d88d44ee3caf174dc7c6df32.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: David Jacome 

###### 14 Ago 2018 

La muerte lenta, es el término que usas los reclusos en el país para referirse a las personas que son diagnosticadas con enfermedades terminales en las cárceles del país, debido a que al no recibir la atención médica, obligatoria constitucionalmente, son condenados a morir en prisión.

En este Expreso Libertad, conoceremos el testimonio de José Ángel, prisionero político de FARC, diagnosticado con cáncer que continua librando una batalla por su vida desde la Cárcel La Picota.

<iframe id="audio_27965520" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27965520_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
