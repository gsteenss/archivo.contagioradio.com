Title: Juegos Olímpicos en Brasil en medio de crisis política y social
Date: 2016-08-10 12:52
Category: El mundo, Nacional
Tags: Juegos Olimpicos de Brasil
Slug: juegos-olimpicos-en-brasil-en-medio-de-crisis-politica-y-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Brasil-protestas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Elpolítico] 

###### [10 de Agost] 

Desde el pasado 5 de agosto se dio inicio a los Juegos Olímpicos en Brasil en **medio de una fuerte tensión política y social debido a las profundas problemáticas de desigualdad que afronta el país**, la aprobación del senado de continuar con el juicio en contra de Dilma Russeff y la aplicación de las nuevas políticas del presidente interino Michel Temer, contexto al que **la ciudadanía ha respondido con protestas y manifestaciones en diferentes regiones del país**.

Durante el recorrido de la antorcha olímpica por Brasil, varios fueron los intentos de algunas personas por apagar una llama que de acuerdo con Marcía Curi, miembro del colectivo Hijos y Nietos por la memoria, verdad y justicia, tapa una realidad muy grande y hace parte de una gran mentira frente a una [situación política de golpe jurídico a Dilma Russeff,](https://archivo.contagioradio.com/protestas-contra-golpe-de-estado-en-brasil-paralizan-vias-en-7-estados/) el desmonte de las políticas laborales de derechos a los trabajadores, el aumento de la represión, sumado al **desplazamiento interno de las personas, aún sin re ubicar, para construir edificios que hacen parte del complejo deportivo para los Juegos Olímpicos.**

Curi, asegura que desde antes de que iniciarán los Juegos Olímpicos, **el presidente interino Michel Temer, dio la orden de llevar a la cárcel a personas que se encuentren realizando manifestaciones o protestas**, "Seguimos en un Estado dictatorial disfrazado de democracia, en donde no puedes hablar porque o si hablas te llevan preso" afirmó.

Las manifestaciones se han presentado en lugares como los aeropuertos, en donde brasileros le dan la bienvenida a las delegaciones internacionales con **mensajes alusivos a la crisis interna del país** y les hablan de las **desapariciones que se dieron en la dictadura y las que se están dando en la actualidad**. Estas actividades continuarán este  viernes 12 de agosto en donde se realizará un evento en el Bulevar Olímpico ubicado en la Plaza Maua sobre las desapariciones.

<iframe src="http://co.ivoox.com/es/player_ej_12503003_2_1.html?data=kpeikpiUdJShhpywj5aYaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5ynca7V08jWw5CnudPdhpewjajTsMbX1c7j0ZCsrcvj1JDmjbOtqdXj1JDd0dePsMKfzsra0dfNpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
