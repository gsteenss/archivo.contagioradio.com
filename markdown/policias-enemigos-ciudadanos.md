Title: Están volviendo a la Policía una enemiga de los ciudadanos: Alirio Uribe Muñoz
Date: 2019-03-02 15:59
Category: DDHH, Política
Tags: Abuso, código de policía, multas, policia
Slug: policias-enemigos-ciudadanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/12023163w.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Magazie Latino] 

###### [2 Mar 2019] 

La semana pasada **fue radicado el Proyecto de Ley 313 de 2019 en la Cámara de Representantes con el que se modificaría el Código de Policía**, permitiéndo a la Institución crear "Grupos Especiales de efectivos vestidos de civil a efectos de patrullar en las ciudades sin la necesidad de emplear el uniforme”. Una modificación, que para el abogado **Alirio Uribe Muñoz,** sería una fisura más que se agregaría a la ya difícil relación entre uniformados y ciudadanos.

Uribe Muñoz afirmó que este Proyecto de Ley muestra una vez más el carácter autoritario del **Centro Democrático,** puesto que el enfoque de defensa planteado por el representante a la cámara de ese partido, **Óscar Leonardo Villamizar**, entiende que entre más policías hallan, será menos la inseguridad. (Le puede interesar: ["Uniformado investigado por muerte del grafitero Diego Felipe Becerra podría recibir un ascenso"](https://archivo.contagioradio.com/policia-investigado-muerte-del-grafitero-diego-felipe-becerra-podria-recibir-ascenso/))

Este enfoque ha provocado que agentes de la Policía hayan estado involucrados en violaciones a los derechos humanos, pues como lo explicó el excongresista, es producto de "una Policía que ha estado adscrita al Ministerio de Defensa y a políticas de seguridad que no garantizan los derechos humanos" sino un concepto de orden público que no garantiza la participación efectiva de las personas en sus ciudades

### **La inteligencia para los criminales, la Policía para los ciudadanos**

Uribe Muñoz recordó que hay cuerpos de la Policía como la **DIJIN o la SIJIN** cuyos miembros no portan uniformes porque tienen encargada la labor de inteligencia para hacer judicializaciones de perpetradores de delitos. También se han creado cuerpos de Policía que hacen inteligencia y vigilancia de civil en Transmilenio para prevenir casos de acoso hacia mujeres en el transporte público.

Los policías que patrullan en las calles tienen la función de velar por la convivencia ciudadana, razón por la que resulta fundamental que estén identificados y uniformados; de tal forma que la medida no reduciría los niveles de inseguridad provocados por grandes criminales que ya son objeto de otros cuerpos policiales. (Le puede interesar:["Demolerán estación de P. donde fue violada y asesinada la 'siempreviva' en 1992"](https://archivo.contagioradio.com/demoleran-estacion-caso-siempreviva-1993/))

**¿Policía de civil para vigilar quién compra empanadas?**

En días recientes el Código de Policía ha suscitado polémicas en su aplicación, por las interpretaciones que algunos uniformados le están dando; lo que ha provocado sonados comparendos por comprar comidas en las calle, o filmar procedimientos de algunos miembros de la Fuerza Pública. (Le puede interesar: ["Enfrentamientos con el Esmad dejan 18 heridos durante paro campesino en Cauca"](https://archivo.contagioradio.com/enfrentamientos-con-el-esmad-dejan-18-heridos-durante-paro-campesino-en-el-cauca-2/))

Por esta razón, el Abogado Uribe asegura que el uso de la Policía para atropellar estudiantes, vendedores ambulantes y ciudadanos en general lo único que logra es que nadie quiera ver un policía. (Le puede interesar: ["Continúan agresiones del ESMAD contra resguardos indígenas del Cauca"](https://archivo.contagioradio.com/continuan-agresiones-del-esmad-contra-resguardos-indigenas-en-el-cauca/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
