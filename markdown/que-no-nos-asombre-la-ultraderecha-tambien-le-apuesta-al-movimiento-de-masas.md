Title: Que no nos asombre, la ultraderecha también le apuesta al movimiento de masas
Date: 2016-04-04 10:54
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Centro Democrático, marcha, uribismo
Slug: que-no-nos-asombre-la-ultraderecha-tambien-le-apuesta-al-movimiento-de-masas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/sjuan-gabriel-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:   [JuanFGiraldo]

#### Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/) 

###### 4 Abr 2016 

Muchísimo se habló y se sigue hablando por estos días, de la marcha convocada por la ultraderecha del país; muchos insisten en que no son ultraderecha sino Centro Democrático… ¿Que les llamemos Centro Democrático? ¡Eso es un eufemismo! Son ultraderecha y deben sentirse orgullosos por eso, porque para bien o para mal, de carambola, están dejando en ridículo a una cantidad grandota de sus escuderos, que ocultos en discursos pseudoacadémicos y empresariales, le siguen mintiendo al país y a la sociedad, sobre el supuesto fin de las ideologías. La izquierda ha hecho esfuerzos prominentes por insistir en que el país está ideologizado, muchos hemos insistido en que eso no es un pecado, ni constituye algún tipo de demérito. No obstante, para esos cobardes ocultos en discursos que defienden supuestamente la objetividad, la neutralidad, que ridiculizan al que habla de política o al que habla de derechos sociales, hoy por hoy han quedado sin sustento.

[La protesta del uribismo, no fue la congregación de gente apoyando a un caudillo. Fue la marcha de personas que, en este país, están dispuestas a aceptar las masacres paramilitares si ellas constituyen una forma de erradicar la guerrilla, fue la marcha de personas que no les importa si el expresidente está untado hasta el último cabello de corrupción, porque siempre y cuando maneje un discurso de “pantalones amarrados” y genere el odio al socialismo castro-chavista, entonces todo se le perdona.]

[La marcha del uribismo no fue la marcha por un caudillo, fue la marcha de una ideología, y eso no tiene por qué asombrarnos. He sido testigo de los intentos de burla de muchos sectores de izquierda frente a la movilización de masas que ha hecho la ultraderecha, y creo que están apuntando mal, pues si hasta el uribismo le está apostando al movimiento de masas, entonces es porque hay algo en la ideología izquierdista que tanto critican, que sí funciona. Además, como Obdulio fue del EPL y es admirador de Fouché, insisto: no debería asombrarnos.]

[Considero que la izquierda con el paro, y la derecha con la marcha uribista, han demostrado que, en este país, las ideologías están más que vivas; por supuesto causa vergüenza ver cómo algunos petardos cargan pancartas que abogan por el hermano del expresidente, cuando aún hoy, por el Urabá y muchas otras zonas, el vestigio de los doce apóstoles continúa sembrando el terror. Del mismo modo queda claro que noticias RCN, es el vocero del partido ultraderechista. Gurisatti ya lo ha dicho mil veces cuando defiende la toma de posiciones editoriales, eso tampoco debería asombrarnos; no obstante, como una reflexión anexa, allí creo que las cosas se complican demasiado, pues la izquierda no tiene un medio informativo tan grande, lo que constituye una clara desventaja.]

[En conclusión, considero que no es tiempo de centrar la crítica en la movilización de masas que ha hecho la ultraderecha, a mi juicio, contar si salieron muchos o poquitos, hacer memes burlones o similares, es cobardía. A la ultraderecha hay que enfrentarla, en el campo que ella elija, ya que evidentemente tiene el poder de definir las formas de la lucha política; hoy están apostándole al movimiento de masas y en eso la izquierda tiene más cancha. Hoy lo más valioso que rescato de la coyuntura, es el ridículo tan grande en que han quedado cientos de docentes de muchas universidades colombianas que están acostumbrados a omitir el debate político por considerarlo “una ideologización de la pedagogía” …. A esos profes les digo: están fritos.]
