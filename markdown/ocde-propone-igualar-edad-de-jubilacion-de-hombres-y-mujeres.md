Title: OCDE propone igualar edad de jubilación de hombres y mujeres
Date: 2017-05-27 12:07
Category: Economía, Otra Mirada
Tags: Colombia OCDE, jubilación, OCDE
Slug: ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/lahora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Hora] 

###### [27 May 2018] 

Luego de cuatro años de negociaciones de Colombia para ingresar OCDE, se conocieron nuevas propuestas que hace el organismo, que según ellos, mejorarían las condiciones económicas del país. Este jueves se conoció un último informe que indica que Colombia debería crecer **3.2% para 2017 y para ello debería equiparar la edad de jubilación de los hombres y las mujeres** y establecer una política de salario mínimo diferenciado, entre otras.

Aunque las medidas fueron halagadas por el Ministro de Hacienda ya son varias las voces que se unen para denunciar que ese tipo de acciones solamente irían en detrimento de la calidad de vida de los colombianos y colombianas. En Colombia, por ejemplo, las mujeres trabajan más que los hombres y ganan **20% menos de salario que ellos.**  Esto quiere decir que las mujeres, al jubilarse a la misma edad que los hombres (62 años), habrán trabajado más que ellos y recibirán una pensión significativamente menor.

De igual forma, los incumplimientos del gobierno a los compromisos de informalidad laboral, y violencia contra los sindicatos, reflejan la incapacidad que tiene el gobierno para hacer parte del "club de países más ricos". En repetidas ocasiones, las centrales sindicales han establecido que "el Gobierno está lejos de haber resuelto importantes problemáticas laborales. **Colombia es uno de los 10 países de América Latina donde los trabajadores y trabajadoras se enfrentan a las peores formas de violencia**, incluyendo asesinatos, amenazas, secuestros y violencia física". Le puede interesar: ["Colombia reprueba examen de la OCDE"](https://archivo.contagioradio.com/colombia-reprueba-examen-de-la-ocde/)

Además de que la población femenina del país tendría que trabajar mucho más para lograr jubilarse, Colombia no tiene un sistema pensional fuerte. En el país hay garantías para la jubilación remunerada de la población ocupada laboralmente en la actualidad. La gran informalidad y la desregulación del trabajo, sumado a la alta rotación en el empleo, no permite que se amplíe la cantidad de cotizantes y por ende la cobertura de pensionados. Es por esto que hay un número reducidos de pensionados en Colombia y hay una desesperanza en la juventud que no cree que se pueda jubilar con una remuneración algún día.

**El camino de Colombia para entrar a la OCDE**

La OCDE es una organización económica y de cooperación internacional compuesto por 35 Estados que busca coordinar políticas económicas y sociales. En las relaciones internacionales es conocida como el “club de los países ricos” Desde 1961 lleva asesorando a los países miembros para la creación de políticas públicas que impulsen el desarrollo. En sus esfuerzos por descifrar qué condice al cambio social, económico y ambiental, esta organización fija estándares internacionales.

Para Colombia, hacer parte de esta Organización significa que va a existir una necesidad de mejorar las políticas públicas porque los estándares de medición van a ser a nivel internacional y cada vez más altos. **Sin embargo, las propuestas de la Organización para garantizar la entrada a Colombia, parecen ir en contravía de la realidad actual del país. **Le puede interesar:["Mujeres cocaleras entregan 14 puntos mínimos para sustitución de cultivos" ](https://archivo.contagioradio.com/mujeres-cocaleras-entregan-14-puntos-minimos-para-sustitucion-de-cultivo-de-coca/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
