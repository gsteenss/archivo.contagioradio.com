Title: Palestinos podrán ser condenados por lanzar piedras a FFMM israelíes
Date: 2015-11-05 11:15
Category: El mundo
Tags: Cisjordania, Ejército Israelí, Franja de Gaza, Israel, Jerusalen, Palestina, parlamento de Israel ha aprobado una ley por la que lanzar piedras contra al ejército israelí serán penalizados a 10 a 20 años de cárcel
Slug: palestinos-podran-ser-condenados-por-lanzar-piedras-a-ffmm-israelies
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/palestina-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: rtve.es 

###### 4  nov 2015 

El parlamento de Israel aprobadó una Ley que tiene como objetivo castigar con **penas entre 10 y 20 años a los palestinos que lancen piedras contra el ejército israelí**. La decisión se dio con un total de **51 votos a favor y 17 votos en contra en el congreso de ese país.**

La medida se entiende como una manera más de atacar la protesta de Palestina frente al asedio de las FFMM de ese país que solo en el mes de octubre dejaron 68 palestinos asesinados y miles de personas heridas.

Los continuos enfrentamientos en Cisjordania y Franja de Gaza ocasiona que decenas de jóvenes tiren piedras en protesta ante esa situación,"**las escenas de ejecuciones que los soldados israelíes están llevando a cabo en Cisjordania y los ataques contra la mezquita de Al Aqsa (una de las que alberga la explanada) me vuelven tan loco que lo único que puedo hacer es expresar solidaridad con ellos y resistir esta ocupación bárbara**", explica Salim Wahdan, de 19 años y residente en el campo de refugiados de Yabalia (Gaza).

Además, durante una movilización realizada el pasado 9 de octubre convocada por jóvenes palestinos de Cisjordania y Jerusalén, más de seis jóvenes perdieron la vida y veinte fueron heridos a causa de disparos de las fuerzas israelíes, después de que varios jóvenes se manifestaron lanzando piedras y quemando neumáticos.

Pese a la situación, Nissan Slomiansky, presidente del Comité de la Constitución, el Derecho y la Justicia del Legislativo israelí afirma que “lanzar una roca es un intento de asesinato y debe tener por lo menos un castigo mínimo, una pena mínima es necesaria para crear un efecto disuasorio”.
