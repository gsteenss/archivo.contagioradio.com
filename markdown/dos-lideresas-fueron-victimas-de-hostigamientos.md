Title: Dos lideresas fueron víctimas de hostigamientos en el Cesar
Date: 2018-08-29 12:43
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Amenaza a líderes sociales, ANZORC, cesar, ELN, EPL, Iván Duque, lideres sociales, zonas de reserva campesina
Slug: dos-lideresas-fueron-victimas-de-hostigamientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Agost 2018] 

**Dos lideresas sociales fueron amenazadas esta semana** en El Departamento del Cesar. Se trata de Dormelina Romero, víctima de hostigamientos el pasado 27 de agosto, en el municipio de Pailitas y Rosa María García,  en el municipio de Curumaní. **Ambas mujeres son integrantes de ANZORC y hacen parte del proceso de constitución de la zona de reserva campesina del Perijá.**

### **Lideresas piden protección** 

En el caso de la señora Romero, **dos hombres llegaron a la vivienda de su madre, en donde la líder se encontraba, con la intensión de asesinarla.** Sin embargo, Dormelina logró escapar y acudió a las autoridades que le brindaron protección con uniformados de la Policía. [(Le puede interesar:"Es Hallado sin vida Jefferson Arévalo, integrante de la UP")](https://archivo.contagioradio.com/es-hallado-sin-vida-jefferson-arevalo-sobreviviente-de-la-up-en-el-meta/)

De igual forma, **hombres llegaron hasta la vivienda de Rosa María García y asesinaron a dos campesinos que en ese momento se encontraban cuidado la vivienda.** Posteriormente amenazaron a García manifestándole que se fuera del territorio y dejara de hablar sobre el proceso de zonas de reserva campesina. Actualmente cuenta con protección de la Policía.

De acuerdo con Carmeza García, presidenta de ANZORC, las dos líderes sociales también son integrantes de la Mesa de Víctimas del Perijá y se encuentran desarrollando actividades de empoderamiento de las mujeres en sus comunidades y **señaló que detrás de estos hechos podría estar la banda criminal de “Los Pelusos”**, sin embargo, afirmó que en estos lugares también hay presencia de las guerrillas del ELN y EPL.

Asimismo, si bien es cierto que las comunidades campesinas estas desarrollando formas de autoprotección como la guardia campesina, **es urgente que el Estado en cabeza del presidente Duque tome medidas que garanticen la vida de los líderes sociales y defensores de derechos humanos en los territorios**, una de ellas, según García, podría ser la construcción de un protocolo de seguridad o la puesta en marcha de las medidas colectivas de protección.

<iframe id="audio_28201054" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28201054_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
