Title: La escuela de asesinar y desaparecer estudiantes es la misma en toda América Latina
Date: 2014-12-26 16:03
Author: CtgAdm
Category: Otra Mirada
Slug: la-escuela-de-asesinar-y-desaparecer-estudiantes-es-la-misma-en-toda-america-latina
Status: published

#### foto tomada de: [www.sdpnoticias.com](http://www.sdpnoticias.com/internacional/2014/10/23/parlamento-europeo-aprueba-resolucion-sobre-caso-ayotzinapa-condena-los-hechos) 

**Entrevista Alejandro Cerezo de Comité Cerezo:**

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fs0e4QHU.mp3)

**Rueda de prensa PGR:**

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsGG7Oa5.mp3)

**Entrevista Oscar Londoño:**

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fs7S2fYy.mp3)

En rueda de prensa, la Procuraduría General de México confirmó la tarde del viernes 7 de noviembre, que **los 43 estudiantes de la Normal de Ayotzinapa desaparecidos el pasado 26 de septiembre, fueron asesinados.**

El crimen habría sido confesado hace 3 días por **paramilitares de Guerreros Unidos, quienes aseguran que los estudiantes fueron detenidos por la policía municipal, quienes se los entregaron al cartel de Guerreros Unidos**. Los estudiantes fueron trasladados en un camión, donde cerca de 15 estudiantes murieron asfixiados. Los estudiantes que llegaron con vida al basurero, punto de destino, fueron torturados y quemados vivos en una hoguera que se prolongó hasta 15 horas. Posteriormente sus restos fueron depositados en bolsas negras de basura, y arrojados al rio. Para el Procurador General, hubo claros esfuerzos por borrar toda evidencia del crimen, pues No solo fueron quemados los estudiantes con su ropa, sino la ropa de paramilitares involucrados”.  
La organización de Derechos Humanos, Comité Cerezo, denuncia que el** Estado no puede desentenderse de este crimen de lesa humanidad, pues es responsabilidad tanto del Ejercito como del mismo gobierno salvaguardar la integridad de las y los mexicanos, cosa que efectivamente no ocurrió en el caso de la Normal de Ayotzinapa**, por lo que las investigaciones y judicializaciones no pueden parar en este punto. Para el Comité Cerezo, es necesario que se establezca de dónde viene la escuela de desaparecer y asesinar.

La forma en la que **“El Estado mexicano administra la información y el miedo, tiene la intensión que este crimen sea ejemplificarte para todos los estudiantes y la sociedad mexicana, para que no se movilicen, para que no se organicen"**, señala Alejandro Cerezo. “En México no existe una ley contra la desaparición forzada, y peor aún, el paramilitarismo ha sido impulsado por el Estado para consolidar su estrategia de desarrollo económico”, enfatiza.  
Al mismo tiempo en que se adelantaba la rueda de prensa de la Procuraduría en México, organizaciones sociales en Colombia realizaban un plantón frente a la embajada del país centroamericano para demostrar su solidaridad con el caso de los estudiantes desaparecidos.

Para Oscar Londoño, de la Federación de Estudiantes Universitarios, organización que lleva la Secretaría de Derechos Humanos de la Organización Continental Latinoamericana y Caribeña de Estudiantes, el pueblo de América Latina debe movilizarse en su conjunto para rechazar este tipo de prácticas autoritarias y criminales que se han vivido en todo el continente a lo largo de la historia, como lo recuerdan el caso de la Noche de los Lápices en Argentina, y el 8 y 9 de junio en Colombia.  
“**Mientras el gobierno colombiano exporta modelos militaristas para reprimir la organización y movilización social, los pueblos deben exportar su solidaridad**” Concluye Londoño.
