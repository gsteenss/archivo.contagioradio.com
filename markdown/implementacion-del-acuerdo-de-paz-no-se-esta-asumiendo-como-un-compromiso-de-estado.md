Title: Implementación del acuerdo de paz no se está asumiendo como un compromiso de Estado
Date: 2018-10-29 12:41
Author: AdminContagio
Category: Nacional, Paz
Tags: acuerdo de paz, FARC, JEP
Slug: implementacion-del-acuerdo-de-paz-no-se-esta-asumiendo-como-un-compromiso-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/paz-colombia-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Rural 

###### 29 Oct 2018 

Diez integrantes del partido político **FARC** enviaron una carta de respuesta a la **JEP** sobre el apoyo y compromiso de ellos en la **implementación del acuerdo de paz.** El Senador **Iván Cepeda del Polo Democrático** afirma que el acuerdo tiene un estatus que lo convierte en un compromiso de Estado, que éste gobierno no está asumiendo como debería.

La carta tiene varios detalles, afirmó el Senador, que dan cuenta del compromiso que algunos sectores, sobre todo los propios integrantes del partido FARC y la comunidad internacional, han tenido con la implementación del acuerdo, y señaló que uno de los ejemplos del incumplimiento del Estado está evidenciado en el **Espacio Territorial de Reincorporación y Capacitación (ETCR) de Charras en San José del Guaviare**.

Sobre ese ETCR en particular, el senador señala que hay preocupaciones que pueden ser un ejemplo de lo que está sucediendo en materia de reincorporación e implementación, dado que **la zona en la que se encuentra es controlada por “Gentil Duarte” ex comandante del Frente 1ro de las FARC** y hay problemas serios en materia de seguridad para quienes habitan este espacio.

De hecho, **Iván Alí, encargado por parte de FARC de ese espacio, tuvo que salir por amenazas de grupos armados presentes en la zona** y por falta de protección efectiva de las FF.MM. o de policía; con el agravante como señala el Senador, de que en emisoras locales, al parecer por iniciativa de las FF.MM., se estaría ofreciendo recompensa por el líder político, agravando aún mas los problemas de seguridad que afrontan. (Ver: [FARC se presentan ante la JEP](https://archivo.contagioradio.com/ex-integrantes-de-las-farc-comparecen-ante-la-jep/))

### **Avances para fortalecer el proceso** 

Sin embargo, a pesar de las múltiples dificultades el senador Cepeda afirma que **hay puntos en los que se pueden mostrar avances concretos**, uno de ellos la puesta en marcha de la Jurisidcción Especial para la Paz y los organismos del SIVJRNR que tendrán que aportar en gran medida para que se garanticen los derechos de las víctimas.

Por último, Cepeda señaló que por parte de los medios de información s**e debe dar la oportunidad para que las víctimas de crímenes de Estado tengan un espacio para la reflexión pública**, así como se ha hecho énfasis en la tragedia del secuestro.

<iframe id="audio_29681881" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29681881_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Carta a la JEP integrantes FARC](https://www.scribd.com/document/391900565/Carta-a-la-JEP-integrantes-FARC#from_embed "View Carta a la JEP integrantes FARC on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_20524" class="scribd_iframe_embed" title="Carta a la JEP integrantes FARC" src="https://www.scribd.com/embeds/391900565/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Mz0HmWpnlFp2IZIrLmnD&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
