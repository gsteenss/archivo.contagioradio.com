Title: “Todas las decisiones acerca de nosotros, con nosotros”
Date: 2016-06-03 09:15
Category: CENSAT, Opinion
Tags: Agua, Ambiente, Mineria
Slug: todas-las-decisiones-acerca-de-nosotros-con-nosotros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/multinacionales-protesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 3 Jun 2016 

[El pasado 24 de mayo la Corte Constitucional declaró inexequible el artículo 37 del Código de minas (Ley 685 de 2001), el cual restringía las facultades de las autoridades regionales, seccionales y locales para establecer zonas excluibles de actividad minera. Este artículo ha sido discutido y analizado durante los últimos años por defensores del territorio, organizaciones populares y el gremio minero, quienes desde diferentes posiciones han puesto en el debate público el papel de las entidades territoriales en el ordenamiento ambiental y territorial y el derecho de participación de los pueblos para decidir sobre sus territorios.]

[La descentralización consignada en el artículo primero de la Constitución significa una “]*[transferencia de competencias y funciones de un sector central a otro”]*[ y fue pensada como un mecanismo o un ordenamiento para garantizar un mejor acceso de la población a sus derechos fundamentales, como una forma más eficiente de focalización de recursos para combatir la pobreza, para equilibrar las desigualdades territoriales y como una manera de profundizar la democracia representativa y participativa.  Luego de más de 20 años de la Constitución, es evidente que las grandes promesas de la descentralización no se han cumplido.]

[En los últimos años los gobiernos centrales han diseñado un Estado corporativo hecho a la medida de los intereses de las  trasnacionales y de espaldas a las realidades territoriales de las comunidades colombianas. Este “]*[estado agencia]*[” consolidado por los cuatro últimos gobiernos, ha sido el encargado de profundizar el modelo extractivo que se soporta en una concepción mercantilista de la naturaleza.  Este modelo encarnado en cualquiera de sus caras (minería, represas, agroindustria, hidrocarburos, etc.) ha expresado sus nefastas consecuencias ambientales y sociales en los territorios y sus dinámicas.]

[La descentralización, como principio autonómico para el gobierno y la administración territorial, además de las disposiciones constitucionales acerca del]**ordenamiento ambiental del territorio,**[ son fundamentales para este debate. El artículo 79 y 80 de la constitución, entre otros anteponen la conservación, cuidado y manejo de los bienes comunes a los intereses sectoriales de explotación y aprovechamiento. Si para la Constitución lo ambiental es un imperativo, nos preguntamos ¿por qué durante los últimos años lo sectorial y los intereses de las transnacionales mineras han dictado la política ambiental en las regiones?]

[Aunque desde el antiguo código minero (Ley 265 de 1988) la propiedad del subsuelo es del Estado, resulta paradójico que en la ley 685 la competencia de ordenamiento de éste excluya a los entes locales, que a su vez hacen parte del Estado. Según las últimas declaraciones dadas por el nuevo ministro de minas Germán Arce el 28 de mayo, la inexequibilidad del artículo 37 no afectaría las competencias de la Agencia Nacional Minera (ANM) para el usufructo del subsuelo. Pero ¿puede entonces seguir pensándose de manera fragmentaria dos espacios que naturalmente están interconectados y que al parecer del gobierno nacional están legalmente separados?]

No es posible llegar al subsuelo sin pasar por el suelo, y sin el artículo 37 en el código minero, los alcaldes y gobernadores así como el constituyente primario, es decir, las gentes, los pueblos podrán, en ejercicio de su autonomía, participar para decidir el destino de su territorio y sus riquezas naturales. ¡Como siempre ha debido ser! Ya que los legítimos dueños y propietarios han sido los pueblos quienes de diversas formas han habitado y manejado comunitariamente el territorio y tiene una concepción integral del mismo.

La decisión de la Corte resulta esperanzadora  no sólo para avanzar en el debate político acerca de la propiedad del subsuelo sino que ratifica los reclamos de la gente en las veredas, en el barrio, en la calle, en las vías acerca de su derecho a decidir sobre el ordenamiento de su territorio.

Las consultas previas y autónomas en las comunidades étnicas, así como las consultas populares realizadas (Piedras, Tauramena, entre otras) y la que se avecina en Ibagué, en donde esta semana se celebra la marcha carnaval en defensa del agua, la vida y el territorio son la voz fuerte y decidida de los pueblos por garantizar sus derechos y su posibilidad de futuro.

La corte está oyendo sus reclamos y nuestro compromiso es seguir apoyando estas justas causas.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
