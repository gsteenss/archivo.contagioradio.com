Title: Fiscalía General de la Nación pedirá medida de aseguramiento contra el General (r) Mario Montoya
Date: 2016-03-28 09:08
Category: Nacional
Tags: falsos positivos, general Henry William Torres, General Mario Montoya, operación orion
Slug: fiscalia-general-de-la-nacion-pedira-medida-de-aseguramiento-contra-el-general-r-mario-montoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Mario-Montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol ] 

###### [28 Mar 2016]

Según información publicada durante el fin de semana, el fiscal Eduardo Montealegre pedirá **imputación de cargos y medida de aseguramiento contra el General (r) Mario Montoya**, quien fue comandante de la [[Operación Orión](https://archivo.contagioradio.com/a-13-anos-de-operacion-orion-victimas-siembran-justicia-y-verdad/)] en la Comuna 13 de Medellín que dejó por lo menos 100 personas desaparecidas; la medida también cobija al general Henry William Torres a quien le será emitida orden de captura.

El General (r) Montoya será imputado por **emitir directrices que presionaban resultados, así como omitir los controles** para evitar la comisión de ejecuciones extrajudiciales conocidas como [['Falsos Positivos'](https://archivo.contagioradio.com/tapen-tapen-continua-la-impunidad-en-caso-de-falsos-positivos/)[, ]]{}cometidas en las siete divisiones del Ejército.

La práctica macabra de las ejecuciones extrajudiciales de las cuales se han reportado cerca de seis mil, pero de las que a la fecha apenas se investigan cuatro mil y 90% están en la impunidad, fueron motivadas por directrices de Montoya, que **otorgaban medallas a cambio de 200 0 300 muertos reportados**.

Por su parte, **al General Torres se le imputan dos homicidios en persona protegida ocurridos en 2007** contra Daniel Torres y Roque Julio Torres, padre e hijo menor de edad de 17 años, quienes eran testigos en varios casos de 'Falsos Positivos' y acudieron a la Defensoría del Pueblo y a la Brigada XVI para denunciar que habían sido amenazados en el marco de esos procesos.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
