Title: Asesinan a Gilberto Valencia, líder e integrante del grupo "Los Herederos" en Suárez, Cauca
Date: 2019-01-02 11:02
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Cauca, Gilberto Valencia, Los Herederos, Suárez
Slug: asesinan-a-gilberto-valencia-lider-social-e-integrante-del-grupo-los-herederos-en-suarez-cauca
Status: published

###### [Foto: Archivo Particulaar] 

###### [02 Ene 2019] 

La comunidad de Suárez, Cauca, **rechazó el asesinato del líder social Gilberto Valencia,** ocurrido en la madrugada del pasado primero de enero de 2019, cuando se encontraba celebrando con su familia, las festividades de fin de año. De acuerdo con la información de los habitantes, un joven, que al parecer se encontraba en estado de alicoramiento y bajo efecto de sustancias psicoactivas, sería quien le habría disparado.

Asimismo, las personas señalaron que a pesar de tener plenamente identificada a la persona que cometió el asesinato, este huyó del lugar y aún no ha sido capturada por las autoridades. Las primeras hipótesis señalan que al parecer, el agresor conocería a Valencia. (Le puede interesar: ["Denuncian asesinato de campesino a manos del Ejército en Córdoba"](https://archivo.contagioradio.com/denuncian-asesinato-de-campesino-por-miembros-del-ejercito-en-cordoba/))

Actualmente, el líder se desempeñaba como gestor cultural en su territorio, acompañando procesos de formación musical a niños y jóvenes. En el 2015, Valencia junto con su grupo **"Los Herederos" fueron reconocidos por la convocatoria RECON, por su iniciativa “Diálogos itinerantes para la paz”**, razón por la cual la organización, rechazó los hechos.

Además, "Los Herederos" han sido identificados como un grupo de música rap y hip hop en el departamento del Cauca, que se ha encargado de, a partir de la música, **generar escenarios de reconciliación y construcción de apuestas de paz en el territorio**, logrando hechos tan importantes como la consolidación de la Fundación de Arte y Cultura FUNDARTE, que a su vez, ha servido como productora musical para realizar grabaciones de otros artistas más jóvenes.

###### Reciba toda la información de Contagio Radio en [[su correo]
