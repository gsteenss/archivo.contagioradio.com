Title: Constructoras ponen en riesgo la "Reserva Forestal Río Blanco"
Date: 2017-05-17 16:05
Category: Ambiente, Voces de la Tierra
Tags: Cumanday, manizales, reserva forestal, Río Blanco
Slug: conctructoras-ponen-en-riego-la-reserva-forestal-rio-blanco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Rio-blanco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: On Productos Audiovisuales ] 

###### [17 May. 2017] 

En Cumanday, una comuna de la ciudad de Manizales, la comunidad ha decidido pelear por la vida, el agua y el territorio, luego de conocer que las **firmas Vélez Uribe Ingeniería y CFC Asociados planean construir una urbanización para 10 mil personas** en La Aurora, una hacienda contigua a la Reserva Forestal Protectora Río Blanco, considerada una de las zonas más biodiversas del mundo.

Esta **Reserva ha sido considerada de interés para la conservación y el avistamiento de aves,** allí se pueden encontrar 372 aves – el 20% de las especies de aves del país - y 40 especies de mamíferos, algunos muy vulnerables y que se encuentran en peligro de extinción. Además, en La Aurora hay humedales, ojos de agua, arroyos y gran variedad de flora y fauna que se verían afectados con la construcción de esta urbanización.

**Omar Vargas integrante del Movimiento Territorio Cumanday**, aseguró que se oponen a la construcción de estos edificios dado que se incrementaría la deforestación, se destruirá el corredor biológico de las especies que allí viven y se alimentan y se contaminarían las aguas que corren por este territorio, haciendo referencia, por ejemplo, a la Quebrada Siete Cueros. Le puede interesar: [En Montebonito, Caldas, no quieren proyectos hidroeléctricos](https://archivo.contagioradio.com/habitantes-monte-bonito-caldas-esperan-nieguen-licencia-proyecto-hidroelectrico/)

**“Estamos ante un proyecto para el que no se hicieron estudios técnicos. Nos van a afectar el agua para los demás,** es que ahora le quieren echar pavimento a todo lo que encuentran y lo que necesitamos los habitantes son zonas verdes y eso es lo que estamos protegiendo” añadió Vargas. Le puede interesar: [Hidroeléctrica el Edén acaba con el agua de Bolivia, Caldas](https://archivo.contagioradio.com/sin-agua-corregimiento-en-caldas-por-cuenta-de-la-hidroelectrica-el-eden/)

Según el defensor del territorio, **estos terrenos no son aptos para soportar este tipo de construcciones y que a largo plazo además de la reserva**, las personas que vivan allí se verán fuertemente afectadas “se construye sobre humedales y luego los primeros, segundos y terceros pisos empiezan a tener problemas por la humedad y luego vienen las demandas por la premura de comprar y vender”.

### **Las fuentes de agua pueden desaparecer con la construcción de urbanización** 

Así mismo, otra de las preocupaciones es que al afectar los corredores hídricos la población a largo plazo no pueda tener acceso al agua **“lo que tenemos que hacer es cuidar esa reserva con ese 35% de producción de agua,** porque hoy digamos hay muchos edificios y llegado el caso de mañana el agua que se nos escasea de qué se van a alimentar estas personas, cómo les van a dar ese servicio” cuestiona Vargas. Le puede interesar: [Micro-hidroeléctricas en Caldas han secado 19 fuentes de agua](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/)

**Con el daño de las aguas en este territorio, no solo se verían afectadas las comunidades aledañas sino toda la ciudad de Manizales** “no es solo expandir la ciudad con un desorden y sin prever los riesgos, porque por eso es que en la actualidad nos hemos visto obligados a agotar mecanismos como la acción de tutela, las acciones populares, para poder hacer valer los derechos ambientales, porque las autoridades ambientales a todo le dicen que si” recalcó Vargas.

### **Planifican movilización para defender la reserva Forestal Río Blanco** 

Las organizaciones de la zona aseguraron que **se movilizarán en los próximos días bajo la consigna “Todos somos Río Blanco”** así como la continuación de actividades de educación y sensibilización con la comunidad “la gente está muy preocupada con esto porque ellos dicen que en 4 meses se está empezando a mover tierra. Seguiremos movilizándonos” concluyó Vargas. Le puede interesar: [Comunidades esperan que Corpocaldas niegue licencia ambiental a Hidroeléctrica](https://archivo.contagioradio.com/comunidad-de-caldas-corpocaldas-niegue-licencia-ambiental-a-hidroelectrica/)

<iframe id="audio_18769227" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18769227_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
