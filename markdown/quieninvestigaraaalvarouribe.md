Title: ¿Quién debe investigar a Álvaro Uribe?
Date: 2018-07-25 13:20
Category: Nacional, Política
Tags: Alvaro Uribe, Corte Suprema de Justicia, falsos testigos, Fiscalía, Iván Cepeda
Slug: quieninvestigaraaalvarouribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/alvaro_uribe_29.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Herald] 

###### [25 Jul 2018] 

Tras la renuncia de Álvaro Uribe Vélez al Senado de la República, se abrió el debate frente a cuál organismo será el que deba asumir el proceso en contra del ex presidente. Si bien, aún la renuncia no se ha hecho oficial al Senado, se ha hablado de un conflicto de competencias entre si es la **Corte Suprema de Justicia la que debe seguir la investigación, la Fiscalía General de la Nación o la Comisión de Acusación del Congreso.**

### **El dilema de los fueros de Uribe** 

Al ser senador de la República, Uribe esta cobijado por el fuero político, una medida que permite que sea investigado por la Corte Suprema de justicia. Sin embargo, el artículo 235, que señala las competencias de esta Corte, especifica a través de un parágrafo, que así los aforados renuncien a esta medida, si las actividades por las que son investigados tienen relación con el cargo que desempeñaban, este organismo podrá continuar con su investigación.

En caso dado de que la **Corte Suprema manifieste que las acciones en indagatoria no tienen que ver con el cargo, será la Fiscalía General de la Nación** la que asuma el proceso. Frente a la investigación de Uribe, previó a esta decisión es necesario que primero se radique la renuncia oficial del senador, sea aprobada por el senado y posteriormente la Corte Suprema inicie el estudio, a través de una ponencia de uno de los magistrados, sobre si continúa con sus competencias o no en el caso.

El otro fuero que cobija a Uribe, es el que se les otorga a quienes han sido presidentes del país, en ese caso ni la Fiscalía ni la Corte Suprema de Justicia llevan a cabo la investigación, sino que es la Comisión de Acusaciones del Congreso de la República la que llevaría el proceso, y lo haría en caso dado **de que se compruebe que los delitos por los que se le acusa se dieron cuanto era presidente. **(Le puede interesar:["Corte Suprema abre investigación formal contra Álvaro Uribe y Álvaro Prada"](https://archivo.contagioradio.com/corte-suprema-abre-investigacion-formal-contra-alvaro-uribe-y-alvaro-prada/))

### **¿Qué hay detrás de la renuncia de Uribe?** 

El senador Iván  Cepeda manifestó que la principal intensión detrás de la renuncia de Uribe, tiene que ver con que precisamente sea la Fiscalía la que realice las indagaciones, como ha pasado en otros casos judiciales del país relacionados con la parapolítica.

Asimismo, señaló que este golpe podría afectar a los congresistas del Centro Democrático debido a que según el senador, "la bancada de Uribe, sin Uribe, es una anarquía y una bancada que pierde su peso político en el Congreso" y añadió que esto podría definir cómo quedaran las fuerzas al interior del Congreso de la República.

De acuerdo con la abogada defensora de derechos humanos Judit Maldonado, la renuncia de Uribe al senado le permitiría **"sustraerse de la competencia de la Corte Suprema, recuperar su fuero presidencial"** e incluso dejar de lado la investigación de la Fiscalía y dejar en manos de la Comisión de Acusación el proceso. (Le puede interesar:["El historial de investigaciones contra Álvaro Uribe Vélez"](https://archivo.contagioradio.com/el-historial-de-investigaciones-contra-alvaro-uribe-velez/))

<iframe id="audio_27238397" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27238397_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
