Title: Empresas deben garantizar bienestar y estabilidad de trabajadores: CUT
Date: 2020-03-20 21:29
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Covid-19
Slug: empresas-deben-garantizar-bienestar-y-estabilidad-de-trabajadores-cut
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Trabajadores-en-Colombia-scaled.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Cleyder Duque {#foto-cleyder-duque .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado jueves 11 de marzo el Ministerio de Trabajo expidió la resolución 0803 de 2020 en la que resuelve "ejercer de manera oficiosa el poder preferente respecto a todos los trámites radicados o que se radiquen en todas las Direcciones Territoriales y Oficinas Especiales" para adelantar suspensiones de contratos o despidos masivos de trabajadores.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Significa que las empresas tienen permiso para despedir trabajadores o suspender su contrato?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La abogada e integrante de la Central Unitaria de Trabajadores (CUT) Laura Perdomo recuerda que ante una actuación lenta del Gobierno para atender la situación generada por el Covid-19, las empresas empezaron a tomar decisiones como mandar a sus trabajadores a licencias no remuneradas, suspender contratos de trabajo o alegar que hay una fuerza mayor para "mermar sus derechos".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esta situación el Ministerio de Trabajo expidió una circular en la que sugiere a los empresarios aplicar al teletrabajo, horarios flexibles o rotativos. Luego emite dos resoluciones, una que dice que el Ministerio hará una vigilancia especial a las empresas ante las medidas que están tomando con sus trabajadores y la resolución 0803.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta resolución señala que las autorizaciones que pidan las empresas para suspender contratos o despidos masivos deben solicitarla directamente al ministerio del Trabajo. Según explica Perdomo, las empresas normalmente pueden tomar estas acciones con unas causas establecidas en la Ley pero ante oficinas territoriales o inspección del trabajo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la resolución centraliza la función en el Viceministerio del Trabajo, dejando la capacidad para otorgar estos permisos en esta entidad. Para la experta, esta medida parecería de cuidado y prevención, señalando que al estar centralizada, se espera que eviten suspensiones y despidos masivos de trabajadores.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que pueden hacer o no los empresarios, y los trabajadores**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La abogada de la CUT sostiene que están recibiendo denuncias de que algunas empresas están acudiendo a figuras ilegales como las licencias no remuneradas, obligando a sus trabajadores a firmar. Igualmente, han sabido de suspensiones del contratos, lo que también es una actuación irregular. (Le puede interesar: ["Microeconomías colombianas las más afectadas por el aislamiento del Covid-19"](https://archivo.contagioradio.com/microeconomias-colombianas-las-mas-afectadas-por-la-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contrario a estas acciones, sostiene que las empresas deben adoptar las medidas que prioricen la estabilidad de los trabajadores y su bienestar, para evitar nuevos casos. Algunas de las medidas que se podrían adoptar es el teletrabajo, los turnos rotativos o, en última instancia, acudir al artículo 140 del Código Sustantivo del Trabajo que permite mantener la [remuneración ante el cese de actividades](https://www.facebook.com/arturocalleoficial/photos/a.166446760038022/3344737022208964/?type=3&theater).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Perdomo concluye que si estas medidas no se pueden adoptar, en caso extremo se puede buscar una concertación con los trabajadores para tomar medidas extraordinarias, con vigilancia del Ministerio, tomando en cuenta siempre la salud y estabilidad del trabajador que es la parte débil de la relación laboral. (Le puede interesar: ["No tiene por qué haber inflación de precios, si Gobierno toma medidas urgentes ante Covid 19"](https://archivo.contagioradio.com/no-tiene-por-que-haber-inflacion-de-precios-si-gobierno-toma-medidas-urgentes-ante-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
