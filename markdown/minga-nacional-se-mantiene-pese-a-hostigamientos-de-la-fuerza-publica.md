Title: Minga Nacional se mantiene pese a hostigamientos de la fuerza pública
Date: 2016-05-31 17:10
Category: Movilización, Paro Nacional
Tags: Cumbre Agraria, Minga Nacional, Paro en Colombia
Slug: minga-nacional-se-mantiene-pese-a-hostigamientos-de-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Minga-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minga Nacional ] 

###### [31 Mayo 2016] 

Por lo menos **70 mil campesinos, indígenas y afros de Colombia se movilizan en las vías y carreteras de 27 departamentos**, concentrándose en más de cien puntos, en algunos de los cuales la fuerza pública ha agredido violentamente a los participantes, impidiéndoles las garantías para el libre ejercicio de la protesta social y agravando la ya difícil situación de derechos humanos en algunas regiones.

"Se han presentado graves atentados contra la vida e integridad personal, como la **muerte en medio de los hostigamientos por parte del ESMAD**, del indígena Embera Willington Quibarecama, de 26 años, y las heridas causadas con arma de fuego a Manuel Jovel Dagua, en su brazo izquierdo", denuncian las organizaciones.

En Lizama, Catatumbo y Sutatá, a algunos dirigentes los han fotografiado y grabado, a otros les han pedido números telefónicos, situación preocupante para las organizaciones, pues en ocasiones **este tipo de inteligencia ha sido utilizada para atentados y actos de victimización**.

Desde antes de iniciar la Minga se orquestó una campaña de difamación, hostigamientos y sabotaje, en la que la fuerza pública y el Gobernador de Norte de Santander aseguró que se trataba de un paro armado, "lo cual legitima, justifica y motiva el **uso desmedido y desproporcionado de la fuerza contra los ciudadanos campesinos**, indígenas y afrocolombianos que están exigiendo el respeto a sus derechos".

Los miles de pobladores rurales han logrado estar en los lugares de concentración pese a las múltiples intimidaciones. **En el páramo de Berlín, los conductores fueron amenazados para impedir que los pasajeros llegaran a los puntos de encuentro**. Algunos periodistas de medios alternativos no han podido llegar a cubrir los sitios en los que se desarrolla la Minga.

El Ejército Nacional ha asediado los espacios de concentración en Santander y Boyacá, donde también se han hecho sobrevuelos, perifoneos disuasivos contra las comunidades y **distribución de panfletos con la consigna 'Pare el paro'**. "Éste no puede ser el tratamiento a la justa y legítima protesta social de parte de un gobierno que se precia de defender y buscar la paz. Sus discursos son solo retórica si se sigue enfrentando de una manera tan brutal a esta Minga", aseguran las organizaciones.

"Queremos decirle que no vamos a aflojar y que a pesar de todo este tratamiento de guerra **nos vamos a sostener y vamos a seguir exigiendo los cambios requeridos para el sector agrario nacional**. Somos actores civiles y políticos legítimos y no se nos puede equiparar a un grupo armado", agregan, e insisten en que el Gobierno nacional ha incumplido con los acuerdos pactados en 2014, sobre algunos de ellos se ha avanzado pero ni siquiera en un 50%.

"Hay temas como el de la adjudicación de tierras, los proyectos de infraestructura y el tema de garantías y derechos humanos, en los cuales el avance es mínimo o casi nulo. Respecto a la línea de financiación a través del Fondo de Fomento Agropecuario, el Gobierno Nacional se comprometió a **invertir \$250 mil millones durante la vigencia de 2014 de los cuales la cumbre Agraria solo ha recibido 40%**".

"Lo que pedimos es que se atienda este nuevo llamado de la Cumbre Agraria, realizar cambios de fondo en la política nacional y no solo ofrecer unas migajas del presupuesto nacional con las cuales quiere callar la voz de miles de comunidades", concluyen.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
