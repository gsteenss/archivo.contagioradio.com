Title: "Colombia merece una paz con verdad y sin desaparecidos", ASFADDES
Date: 2019-05-30 13:13
Author: CtgAdm
Category: Memoria, Paz
Tags: ASFADDES, UBPD, Unida de Búsqueda de Personas dadas por desaparecidas
Slug: colombia-merece-una-paz-con-verdad-y-sin-desaparecidos-asfaddes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Desaparecidos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Desaparecidos_.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASFADDES] 

En el marco de la semana del Detenido-Desaparecido, la **Asociación de Familiares de Detenidos Desaparecidos (ASFADDES)** se une a esta conmemoración como cada día desde 1982 cuando decidieron hacer resistencia a la impunidad y reconstruir la memoria de sus seres queridos en un país donde, según estadísticas, 82.998 personas continúan sin ser halladas.

**Gloria Gómez, defensora de derechos e integrante de ASFADDES**, afirmó que la organización está comprometida con la memoria como "un medio y una razón de ser por la dignidad" de los desaparecidos. De igual forma, manifestó que  continuán denunciando y exigiendo al Estado, que reconozca su responsabilidad por acción directa u omisión de sus obligaciones.

### **Conmemorar a los detenidos - desaparecidos**

A propósito de la semana del detenido desaparecido, la Asociación conmemora en el parque de los periodistas desde las 9:00 am, en una jornada de sensibilización exponiendo los rostros de las víctimas, "para que los transeúntes se comprometan con la lucha de los familiares" y a su vez denuncien este crimen de lesa humanidad.

> Mañana estaremos en las calles como hace más de 36 años en la conmemoración de la semana del detenido desaparecido reconstruyendo la memoria de nuestros seres queridos. [@UnidadVictimas](https://twitter.com/UnidadVictimas?ref_src=twsrc%5Etfw) [@UBPDBusqueda](https://twitter.com/UBPDBusqueda?ref_src=twsrc%5Etfw) [@GloriaLGmezCor1](https://twitter.com/GloriaLGmezCor1?ref_src=twsrc%5Etfw) <https://t.co/D4xSwNQhde> [pic.twitter.com/rjCgsgOue1](https://t.co/rjCgsgOue1)
>
> — ASFADDES (@ASFADDES) [29 de mayo de 2019](https://twitter.com/ASFADDES/status/1133758806578663424?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### La Unidad de Búsqueda es la última esperanza que tenemos

Gloria Gómez también se refirió al trabajo que ha comenzado a realizar la **Unidad de Búsqueda de personas dadas por desaparecidas (UBPD)** y expresó que la creación de este mecanismo, exigido por lo familiares de los desaparecidos, es en la actualidad la única esperanza de localizar, identificar y recibir dignamente a sus seres queridos,

ASFADDES, "sigue apostándole a que Colombia merece una paz sin desaparecidos pero con verdad" indicó Gloria, quien además destacó la importancia de la participación de las víctimas en el  proceso de justicia restaurativa y la necesidad de recibir resultados concretos por parte de la UBPD.  [(Lea también: Inicia búsqueda de los 120 mil desaparecidos en los territorios)](https://archivo.contagioradio.com/inicia-busqueda-120-mil-desaparecidos/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
