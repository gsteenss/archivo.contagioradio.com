Title: La inteligencia Estatal una herramienta de persecución al movimiento social
Date: 2017-09-12 19:50
Category: DDHH, Nacional
Tags: Fuerza Pública, Somos defensores
Slug: la-inteligencia-estatal-una-herramienta-de-persecucion-al-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fotoilustracion_chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [12 Sept 2017] 

La organización Somos Defensores, presentó el informe titulado “Stop Wars”, un balance de los derechos humanos en Colombia, su segundo capítulo es “La Inteligencia en Colombia, el lado oscuro de la Fuerza” que busca dar un panorama de esta herramienta de la Fuerza Pública en el país y su historia basada en la doctrina del enemigo interno, como mecanismo de control a activistas, defensores de derechos humanos, líderes sociales, políticos y periodistas, **y la relación que existe con la impunidad en crímenes cometidos por el Estado.**

### **Los enemigos Políticos en Colombia** 

El informe expone como se fue construyendo el imaginario del enemigo interno, a partir de la  doctrina del enemigo, heredada de escuelas militares europeas, por tal razón manifiesta que Colombia siempre ha sido gobernada por élites y castas a quienes les convenía construir un **“otro” del cual desconfiar y temer, otro que se ha visto representado en todos aquellos que se oponen ante las formas de gobierno de esas élites.**

De esta manera, el informe demuestra que en diferentes momentos de la historia los campesinos que exigieron sus derechos se convirtieron en el enemigo interno al cuál seguir, luego los estudiantes, los militantes de partidos de izquierda, los periodistas y finalmente, sin distinción alguna, **aquellos que se encontraran en un activismo político o social, incluyendo a los defensores de derechos humanos**.

### **Los 80´s y el auge del terror Estatal** 

En ese sentido el documento también señala, que los enemigos internos han sido diferentes debido a las coyunturas de violencia que afrontó Colombia, hasta que finalmente en la década de los 80, se afianza la criminalidad estatal, debido a que prácticas como la desaparición forzada, el asesinato político, las masacres, **“se vuelven los ejes de accionar de las Fuerzas represivas oficiales y para militares aliadas del gobierno”**.

Se estima que en esta década se registraron **12.859 asesinatos políticos y 2 mil personas desaparecidas**. De igual modo se evidencia que estas estructuras encargadas de realizar la inteligencia del gobierno tendrían una estrecha relación con el surgimiento del paramilitarismo, muestra de ello serían las similitudes ideológicas y la colaboración para el traspaso de información. (Le puede interesar: ["44 años de prisión a Alexander Cerretero, comerciante de las vidas de los jóvenes de Soacha"](https://archivo.contagioradio.com/44-anos-de-prision-a-alexander-carretero-comerciante-de-las-vidas-de-los-jovenes-de-soacha/))

### **La crucial década de los 90** 

En 1990, Colombia afronta un cambio político con el proceso de paz del M-19 y la entrada en vigencia de su nueva constitución, sin embargo, archivos desclasificados han permitido establecer que **para ese entonces 32 defensores de derechos humanos eran objeto de amenaza y seguimiento por parte de militares**.

Sin duda alguna esta se convierte en una de las décadas más escalofriantes para el país, con el accionar del narcotráfico, el paramilitarismo, las guerrillas de las FARC- EP, ELN y EPL, y la Fuerza Pública, que deja como resultado el asesinato de 54 activistas, todos con alertas tempranas sobre l**as amenazas a sus vidas y quienes abrían sido calificados por la inteligencia militar como simpatizantes de la guerrilla**.

### **La seguridad democrática: gobernar con el miedo** 

Finalmente el informe da cuenta de cómo con el ingreso de Álvaro Uribe a la presidencia de Colombia, llega la política de la seguridad democrática y con ella una de las olas de agresiones más grandes a defensores de derechos humanos, líderes y activistas, en donde incluso, Uribe llegó a relacionar públicamente el trabajo de defensores de derechos humanos con actividades terroristas y transformando al movimiento por la defensa de los derecho humanos en un objetivo militar.

Una de las herramientas que desarrolló las más grande red de inteligencia a activistas fue el Departamento Administrativo de Seguridad, de acuerdo con el informe se pudo establecer que el **DAS proporciono información a estructuras paramilitares y que investigó a todo tipo de personas bajo la noción de la guerra política**. (Le puede interesar:["Condena de Noguera es tardía pero ratifica la existencia de los crímenes del DAS"](https://archivo.contagioradio.com/victimas-del-das-no-esperan-mucho-de-la-comision-de-acusacion-de-la-camara/))

**Desde el 2002 al 2006, se registraron 429 violaciones a los derechos humanos contra defensores de derechos humanos**, entre los que se incluyen amenazas, detenciones arbitrarias, 46 asesinatos, atentados, desapariciones, torturas y robo de información.

### **¿Qué ha pasado con los crímenes de la inteligencia en Colombia?** 

El documento manifiesta que pese a que hay cúmulos de pruebas que evidencian como a lo largo de la historia y sobre todo, desde la década de los 80 se profundizo el uso de la inteligencia como una herramienta **de persecución al movimiento en defensa de los derechos humanos, pocos son los avances que se tienen en materia de los autores intelectuales** de estos crímenes y menos en los responsables de construir esas redes de convivencia entre fuerzas estatales y paramilitarismo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
