Title: “Es más fácil vincular la verdad con la paz, que la paz con la justicia”: David Rieff
Date: 2018-02-18 06:00
Category: Cultura, Nacional
Tags: comision de la verdad, literatura, memoria
Slug: es-mas-facil-vincular-la-verdad-con-la-paz-que-la-paz-con-la-justicia-david-rieff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Evento-David-Rieff-CGD1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Feb 2018 

#### [**Por: Carolina Garzón para Contagio Radio**] 

#### **La memoria fue tema de debate en el Hay Festival de Cartagena** 

[En los últimos años el periodista, analista y escritor estadounidense **David Rieff** ha sido invitado a Colombia para exponer las tesis expuestas en sus libros “Contra la memoria”, de 2012, y “Elogio del olvido”, de 2017. En esta ocasión, en el marco del Hay Festival Cartagena, fue entrevistado por el periodista Rodrigo Pardo acerca de la polémica que suscitan sus escritos y su lectura sobre el momento de justicia transicional que atraviesa Colombia.]

[Contagio Radio estuvo en el evento y recogió las ideas principales de Rieff, quien invita a desacralizar la memoria y alimentar el debate sobre los derechos de las víctimas en países que han vivido largos conflictos armados y guerras.]

**Críticas generales a la memoria histórica**

[«La memoria histórica es siempre una selección», sostuvo Rieff argumentando que, en un estricto sentido, la memoria colectiva no existe ya que el ser humano únicamente es capaz de conservar una memoria individual que ni siquiera puede dar cuenta de hechos, sino de las interpretaciones que hace de dichos acontecimientos. Es decir, según el escritor, la memoria histórica es una metáfora, una construcción hecha de consensos sobre asuntos importantes del pasado. ¿Con qué objetivo? Servir al presente con el pasado.]

[Según las experiencias que ha documentado a lo largo de su trabajo como periodista en Bosnia, Ruanda, Liberia, Sierra Leona y Kosovo, se debe comprender que la memoria colectiva se ha convertido en un arma para hacer más fuerte la solidaridad, pero que esa solidaridad en sí misma no es buena, depende del objeto con el que nos estemos solidarizando, dado que puede utilizarse para fundamentar discursos de odio y segregación.]

**Memoria y Comisiones de la Verdad**

[Sobre las Comisiones de la Verdad que se han creado en otros países, Rieff sostuvo que su papel es importante en la medida que traten de establecer lo que ocurrió en una región durante un periodo de tiempo. «No soy crítico de las Comisiones, pero la reconciliación es una gran palabra.», manifestó ante la pregunta sobre las limitaciones de estos procesos de investigación.]

[Añadió que existen formas de distinguir entre el uso y el abuso de la memoria. Para ello es necesario hacerse preguntas como ¿Quién tiene la legitimidad para esas visiones que se imponen sobre otras? ¿Cuál es el pasado que se está construyendo? ¿A quién le sirve?]

**“Cada caso es particular”**

[Desde la publicación de sus libros y en cada conferencia en la que interviene, David Rieff es enfático en decir que cada caso es particular. Siempre que le preguntan puntualmente por el postconflicto colombiano, Rieff evita dar fórmulas o sentencias sobre nuestra realidad. Insiste en que cada país sabe qué requiere y que su único objetivo al participar de las discusiones que surgen en Colombia es brindar elementos para «mirar las distintas perspectivas».]

[Advierte que, en el mundo, «el movimiento de derechos humanos cree tener una fórmula para la justicia, la verdad y la paz», pero que debe ser más crítico al sopesar la importancia de cada uno de esos elementos pues, según su experiencia, es más fácil vincular la verdad con la paz, que la paz con la justicia.]

[El esfuerzo por darle lugar a la memoria colectiva en nuestro país apunta a construir la unidad necesaria para la superación del conflicto y la construcción de la convivencia y la reconciliación, por esta razón la entrevista de Rieff con el periodista Rodrigo Pardo se convierte es una invitación a un debate serio y argumentado sobre el lugar de la memoria en un país que, como Colombia, tiene la oportunidad de satisfacer los derechos de las víctimas del conflicto a través de la recuperación, construcción y divulgación de la memoria.]

[La tesis de Rieff, contrarias en ocasiones a las expuestas por organizaciones sociales y de derechos humanos, contemplan experiencias internacionales que pueden servir de ejemplo en Colombia para evitar los mismos errores y superar sus limitaciones.]
