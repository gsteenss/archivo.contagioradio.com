Title: Reclutamiento forzado, pobreza e ineficacia oficial, las tres amenazas para jóvenes de Soacha
Date: 2020-03-18 00:12
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cundinamarca, Grupos armados ilegales, reclutamiento, soacha
Slug: reclutamiento-forzado-pobreza-e-ineficacia-oficial-las-tres-amenazas-para-jovenes-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Soacha-Cundinamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Humanitas 360 {#foto-humanitas-360 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente la Fundación Paz y Reconciliación denunció el [reclutamiento forzado](https://pares.com.co/2020/03/15/alerta-por-reclutamiento-de-jovenes-en-soacha/) de 10 jóvenes en Soacha por parte de grupos armados ilegales, y el asesinato de uno de ellos en Antioquia. La Fundación además denunció la poca efectividad de la Policía para combatir estas estructuras en el territorio, así como la ausencia de acciones de las autoridades civiles para evitar nuevos reclutamientos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La denuncia de la Fundación hace eco del reporte de Marino Córdoba, director de la Asociación Nacional de Afrocolombianos Desplazados (AFRODES), que fue el primero en poner en conocimiento sobre los reclutamientos en el vecino municipio de Bogotá. Sin embargo, la Fundación profundizó en las condiciones que hacen prosperar a los grupos armados ilegales en la zona, como que el 53,8% de su población está por debajo de la línea de la pobreza y el 20,4% bajo la línea de la indigencia, entre otras. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Los problemas a los que se enfrenta Soacha**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Heiner Gaitán, concejal de Soacha por la Colombia Humana-UP, recordó que en los últimos dos años el municipio ha aparecido en, por lo menos, 3 alertas tempranas de la Defensoría del Pueblo, en las que se manifiestan la presencia de grupos armados organizados, herederos de  estructuras paramilitares. Dichos grupos harían especialmente presencia en la Comuna 3, 4 (Altos de Cazucá) y 6. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gaitán señaló que al igual que en otras regiones del país, organizaciones defensoras de derechos humanos han denunciado que los grupos armados ilegales aprovechan la falta de oportunidades y las necesidades de los jóvenes para engrosar sus filas. En Soacha tendría dos objetivos: Coptamiento de jóvenes para operar en el territorio como campaneros, en las estructuras de microtráfico y estableciendo fronteras invisibles; y en el otro caso, para apoyar fines delincuenciales en otros lugares, como ocurrió con los 10 jóvenes reportados. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta situación se suma la denuncia de que sectores de la Fuerza Pública actúan en connivencia con las estructuras delincuenciales, permitiendo los negocios ilegales e incluso, siendo parte de algunos de ellos. Todo esto, recuerda el concejal, ocurre en un territorio que es estratégico por su ubicación sobre la principal salida de Bogotá hacia el sur del país. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Qué pasa con las autoridades civiles del municipio?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gaitán sostuvo que el alcalde de Soacha, Juan Carlos Saldarriaga ha manifestado en diferentes ocasiones que no hay estructuras armadas ilegales en Soacha, lo que impide acciones de prevención y sanción de los hechos violentos contra la juventud y la comunidad en general. (Le puede interesar: ["La violencia neoparamilitar en la frontera sur de Bogotá"](https://archivo.contagioradio.com/neoparamilitar-frontera-bogota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el integrante de la Colombia Humana, esta negación se debe al desconocimiento de que los grupos armados están implementado la “tercerización del delito”, es decir, que ya no hacen presencia directamente en el territorio sino que ‘subcontratan’ a pandillas y grupos delincuenciales que tienen alcance barrial para lograr sus objetivos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta situación, denunciada también informes por organizaciones como Paz y Reconciliación, explica las razones por las que aunque no se ven hombres armados en Soacha, sí se ha alertado sobre el ingreso de grupos como Los Rastrojos e incluso el ELN. En ese sentido, Gaitán declaró que gran parte de las pandillas que están registradas en la zona responden a estructuras delincuenciales más grandes, por lo que es el momento de que las autoridades reconozcan este fenómeno y actúen en consecuencia para afrontarlo. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
