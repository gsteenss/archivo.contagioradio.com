Title: Maicol Guevara integrante de las FARC fue asesinado en San Vicente del Caguán
Date: 2017-09-15 15:29
Category: DDHH, Nacional
Tags: acuerdo de paz, asesinato, FARC, proceso de paz
Slug: maicol-guevara-integrante-de-las-farc-fue-asesinado-en-san-vicente-del-caguan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/retorno-familia-aljure-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Sept. 2017]

Según la denuncia de COORDOSAC el pasado 12 de septiembre en horas de la noche, **hombres que se movilizaban en una moto sin placas dispararon en siete ocasiones contra Maicol Guevara,** joven reincorporado de la Fuerza Alternativa Revolucionaria del Común – FARC - en la Vereda El Roble, municipio de San Vicente del Caguán.

“Siendo las diez de la noche del martes 12 de septiembre del año en curso mientras se dirigía a su casa, **siete impactos de bala acabaron con la vida de Maicol.** Según versiones de los habitantes hombres en una motocicleta nueva Yamaha TX color blanco sin placa” dice la comunicación.

Maicol Guevara se encontraba viviendo con su familia en la Vereda El Roble luego de haber cumplido los requisitos para la reincorporación previstos en el Acuerdo Final de Paz.

Según varios testimonios de las comunidades, en varias ocasiones **han denunciado la presencia de personas que no son de la zona** y que transitan a altas horas de la noche con capuchas que cubren sus rostros y no permiten su identificación.

Estas personas “perturban la tranquilidad de la comunidad que se encuentra en la Zona de Reserva Campesina Cuenca del Río Pato y Valle de Balsillas”. Le puede interesar: [Asesinado guerrillero de las FARC que había sido beneficiado con ley de amnistía](https://archivo.contagioradio.com/asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia/)

### **Organizaciones piden mayor seguridad y garantías** 

La Coordinadora Departamental de Organizaciones Sociales, Ambientales Y Campesinas Del Caquetá – **COORDOSAC lamentó profundamente este hecho** y se solidarizó con la familia y amigos de la comunidad que se vio afectada por este hecho.

Así mismo, aseguraron que es necesario “que de manera urgente **se activen los mecanismos de seguridad y garantías de protección para los miembros del partido Fuerza Alternativa Revolucionaria del Común**, los habitantes de la Zona de Reserva Campesina Cuenca del Río Pato y Valle de Balsillas y en general para los dirigentes y la sociedad civil pertenecientes a organizaciones sociales que trabajan por la consolidación de la paz”.

Exigen además investigaciones eficaces en este caso que tiene relación con otros homicidios, atentados y amenazas que se han presentado en los últimos meses en la región contra integrantes de las FARC y sus famlias, así como contra líderes sociales y personas que trabajan en la implementación del Acuerdo de Paz. Le puede interesar: [Entre 2009 y 2016 han sido asesinados 458 defensores de DD.HH en Colombia](https://archivo.contagioradio.com/entre-2009-y-2016-han-sido-asesinados-458-defensores-de-dd-hh-en-colombia/)

### **Llamado urgente a la misión de la ONU en Colombia** 

COORDOSAC manifiesta que si bien ha expresado toda su disposición de acompañar el proceso de denuncia por los hechos que acontecen en medio de la implementación de los Acuerdos de Paz dicen que es necesario que organizaciones como la ONU sienten una postura al respecto de las recientes muertes y amenazas.

**“Exigimos a la misión de la ONU, manifestarse frente al tema y ejercer presión para que se den garantías a los reincorporados**, a las organizaciones y dirigentes que lastimosamente se ven como objetivo de quienes quieren sembrar el terror y la incertidumbre en la región”. Le puede interesar: [Dos integrantes de las FARC asesinados en menos de 24 horas](https://archivo.contagioradio.com/septimo-integrante-de-las-farc-asesinado/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
