Title: 16 mil tortugas en peligro de extinción llegaron a Nicaragua a depositar sus huevos
Date: 2015-09-30 12:18
Category: Animales, Voces de la Tierra
Tags: animales en vías de extinción, conservación de especies, Costas del pacífico de Nicaragua, huevos de tortugas, Nicaragua, Refugio La Flor, tortugas marinas
Slug: 16-mil-tortugas-en-peligro-de-extincion-llegaron-a-nicaragua-a-depositar-sus-huevos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/nicaragua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [radiomacondo.fm]

Aproximadamente 16 mil tortugas marinas que se encuentran en peligro de extinción, llegaron a las costas del Pacífico de Nicaragua, para depositar sus huevos en la reserva **Vida Silvestre La Flor.**

De acuerdo con el equipo de Biodiversidad del Ministerio del Ambiente y los Recursos Naturales se trata exactamente de 1**5.816 tortugas Paslama (Lepidochelys olivácea) y Torita (Chelonia agassizii),** que desde el pasado 22 de septiembre arribaron al refugio donde se espera que lleguen más **de 40.000 hasta octubre, cada una de ellas pone un promedio de 100 huevos por anidada.**

Se calcula que cada año llegan alrededor de 250.000 tortugas a Nicaragua, donde anidan diversas especies como Verde, carey, laúd o tora, cabezona y paslasma, que están en vías de extinción por diferentes factores, entre ellas **el turismo, por lo que se han empezado a generar campañas educativas** con el fin de que las personas respete ciertas normas para la conservación de los nidos.

El número de reptiles es verificado diariamente gracias a la labor de guardaparques, militares, policías, ambientalistas, líderes comunitarios y trabajadores del Marena, que están distribuidos en 11 tramos de 100 metros cada uno en las costas de La Flor.
