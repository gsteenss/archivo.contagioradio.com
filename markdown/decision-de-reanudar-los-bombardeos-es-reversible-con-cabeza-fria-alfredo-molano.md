Title: Decisión de reanudar los bombardeos puede ser reversible. Entrevista a Alfredo Molano
Date: 2015-04-19 07:28
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Buenos Aires Cauca, cese unilateral de las FARC, Conversaciones de paz de la habana, ELN, FARC, FFMM, Juan Manuel Santos, ministerio de defensa
Slug: decision-de-reanudar-los-bombardeos-es-reversible-con-cabeza-fria-alfredo-molano
Status: published

###### Foto: contagioradio.com 

El sociólogo, escritor, periodista e integrante de la Comisión Histórica del Conflicto y sus Víctimas, Alfredo Molano, analiza los últimos acontecimientos en el marco del conflicto armado en Colombia, concretamente el sucedido en el municipio de Buenos Aires en Cauca, en el que murieron 11 integrantes de las Fuerzas Militares, 1 integrante de las FARC y cerca de 20 militares resultaron heridos.

**Contagio Radio**: ¿Esto ha pasado en otros procesos de conversación de paz?

**Alfredo Molano:** Si, yo pienso que es un formato que ha venido utilizándose en casi todos los procesos de paz. Yo recuerdo que los acuerdos de la Uribe fueron terminados, durante el gobierno de Barco, cuando un batallón de ingenieros entró a un sitio en el Caquetá y fueron atacados por la guerrilla, el argumento del ejército era que estaban haciendo una carretera, una carretera cuando había tregua, cuando se veían algunas soluciones a lo lejos. Lo mismo sucedió después en lo del Caguan, un extraño - insólito digamos un poco- secuestro de un Senador condujo a Pastrana a acabar con ese otro intento, y ahora me parece que se repite el guion, que es lo más preocupante, porque eso significa que dentro del gobierno hay enemigos, ya no del lado de la paz, sino actuantes.

**CR:** En el contexto actual de estas conversaciones, en el proceso de estas conversaciones, con el respaldo del 9 abril, podría suceder lo mismo que en otros procesos que usted menciona?, es decir, ¿se pueden romper?

**AM:** Yo y muchos colombianos tenemos la esperanza de que no se rompa, es decir, que sea un momento difícil pero que la situación se supere, así como se superó cuando secuestraron al general en el Chocó, es decir, ya se ha andado mucho para que por estos hechos se caiga el proceso. Yo no creo que sea irreversible lo que se ha andado, pero me parece que tiene suficiente fuerza hoy día, lo que se ha acordado y lo que se está acordando para superar este lamentable hecho de guerra.

**CR:** Otra hipótesis que se está manejando es que se está haciendo presión a las FARC para que avancen las negociaciones con mayor rapidez ¿podría ser esa la intensión del gobierno?

**AM:** El gobierno quiere presentarse a las elecciones con la bandera de los acuerdos logrados con las FARC, pero las FARC tienen otro tiempo, las FARC están pensando en un acuerdo de más largo plazo, más profundo y a mí me parece que dadas las distancias que hay entre las partes, no son fáciles de lograr. El tema justicia es el tema cárcel, es el tema extradición, y la dejación de armas es un proceso largo que naturalmente no es un acto sino un proceso que puede implicar un juego de garantías de contrapartes, entonces tampoco es que sea una cosa rápida de hacer.

Así que lo que es realmente para mi insólito es que hay 50 hombres del ejército, durmiendo en un polideportivo, sin postas, porque si hay postas no puede haber un ataque de semejante magnitud y sin apoyo de la aviación, que es lo que pasó hoy frente a las instalaciones de la brigada en Cali, con los familiares protestando seguramente porque los soldados estuvieron huérfanos del apoyo del ejército o de la fuerza aérea por los menos, entonces uno se pregunta ¿de qué se trata este hecho? repito, de guerra y muy lamentable.

**CR**: Ayer Pablo Catatumbo, denunciaba que este hecho podría presentarse en otros lugares del país. ¿Si estos hechos vuelven a suceder, podría darse al traste con lo que va avanzado del proceso de paz?

**AM:** De parte de la guerrilla el objetivo es, a mi manera de ver, lograr una tregua bilateral con el fin de que lo que suceda en Colombia no afecte lo que se desarrolla en la Habana, pero al mismo el gobierno lo que quiere es presionar un acuerdo rápido de paz, o un acuerdo antes de las elecciones de Octubre. Entonces son estrategias que van en contravía, pero de todas maneras yo espero, deseo fervientemente, que el proceso, pese a todo, se mantenga.

**CR**: ¿Es reversible la decisión del presidente Santos de reanudar los bombardeos?

**AM:** Pues con cabeza fría de las dos partes yo creo que es reversible, creo que es una decisión que se tomó en un momento oportuno y que volverá a tomarse en el caso de que las aguas bajen como es urgente. Porque fíjese usted, el hecho de que no haya una tregua unilateral habiendo negociaciones implica directamente que los hechos de guerra en Colombia tengan repercusiones en la Habana y además se manejen esos hechos de guerra como formas de impulsar en determinadas direcciones o acuerdos. Por lo tanto creo que es necesario que el país además de manifestarse en contra de la guerra, como se está haciendo, exija una tregua bilateral para impedir que lo que aquí sucede vaya erosionando lo que en la Habana se acuerde.

**CR:** Otros analistas dicen que los medios son carboneros de la guerra ¿usted qué piensa del manejo que le han dado los medios y qué debería hacerse para que los empresarios o el gobierno vayan ambientando la paz?

**AM:** A mí me parece un manejo muy irresponsable que han hecho los medios, pero es casi un dispositivo que se venía preparando frente a cualquier acción, es algo que se tiene ya calculado, ese salto que pegan, esa manera como desfiguran, como no analizan, como impiden el conocimiento y el análisis de los hechos, van juzgando y van sancionando y van, naturalmente, limitando las posibilidades de la negociación. Es que el fuelle que está dando, con franqueza, Uribe, echándole más fuego a la candela, más combustible a la candela, es muy peligroso, y esos medios que son bastante *amarillosos* y que tienen compromisos políticos, pues naturalmente que terminan haciendo lo que hacen.
