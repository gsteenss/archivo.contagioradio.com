Title: La memoria de Tomas Sankara, el Che africano
Date: 2016-02-04 17:26
Category: Eleuterio, Opinion
Tags: áfrica, ideología marxista, Thomas Sankara
Slug: burquina-faso-y-la-memoria-de-tomas-sankara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Thomas-Sankara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: african-di.org 

#### **[Eleuterio Gabón](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 4 Feb 2016 

María Rodríguez es una joven periodista andaluza que trabaja como freelance en el África Occidental. Ante los últimos cambios políticos ocurridos en Burquina Faso, nos trae a la memoria la historia de Thomas Sankara, conocido como el Che Guevara africano.

Al igual que en muchos países africanos tras la desconolización en los años 60, Burquina había sufrido una serie de golpes de estado que mantenían el país en una constante inestabilidad política y social. Sin embargo, en 1984 el levantamiento militar tiene un carácter especial, está encabezado por un joven general, Thomas Sankara. Junto a su mejor amigo Blaise Compaoré establecen un régimen de ideología marxista. “Sankara era panafricanista, defiende la idea de la unión y la solidaridad de los pueblos africanos para deshacerse del imperialismo. Tras la descolonización surgió un sentimiento común que no existía antes en el continente. Sankara era un tipo bromista, honesto, inteligente, con una fuerte personalidad. Consideraba que para el desarrollo del país era necesario producir y consumir de forma local. Muy admirado por la juventud, era ecologista y feminista, ideas avanzadas para su época.” Trabajó por el reconocimiento de la mujer en la sociedad, decía cosas como esta: La revolución y la liberación de la mujer van unidas. No hablamos de la emancipación de la mujer como un acto de caridad o por una oleada de compasión humana, es una necesidad básica para el triunfo de la revolución.

Había roto todos los esquemas, es famoso su discurso en Addis Abeba frente a la Unión Africana en julio de 1987. “Afirmó ante los líderes de los demás países que fueron los colonizadores quienes endeudaron África con sus prestamistas y que esas deudas eran ajenas a ellos y por tato no tenían por qué pagarla. Muchos se rieron de él pero todos sabían que tenía razón.” El 15 de octubre del mismo año, Tomas Sankara es asaltado y asesinado junto a otros 12 compañeros. Quien sube al poder es su amigo Compaoré que se plegó a los intereses franceses y traicionó a su compañero poniendo fin a un efímero mandato de 3 años. Sin embargo la huella de Sankara ha seguido viva.

“En octubre de 2014, Compaoré se disponía a arreglar las leyes para prolongar 5 años su mandato a los 27 que ya estaba sentado en el poder. Pero la gente estaba ya harta y quería un cambio.” Compaoré había convocado un referéndum para modificar a su favor la constitución. Para ello debía presentar un anteproyecto de ley que debía votarse en la Asamblea Nacional donde Compaoré tenía la mayoría. “Durante toda la semana previa la gente salió a manifestarse contra el gobierno.” El día antes de la votación, los diputados hicieron noche en un hotel cercano a la Asamblea por miedo a que la gente no les permitiera salir de sus casas y llegar a votar. “Pero una vez que estos lograron llegar a la Asamblea el pueblo prendió fuego al edificio e impidió la votación. Compaoré huyó al exilio. Fue todo un hito en la historia de Burkina.”

La transición política en Burkina se ha puesto en marcha y entre otras cosas, ha puesto sobre la mesa la intención de resolver el asesinato de Thomas Sankara para que se haga justicia a su figura casi 30 años después.
