Title: Indígenas de Cauca denuncian hostigamientos por combates entre el ELN y Policía
Date: 2017-05-02 15:47
Category: DDHH, Nacional
Tags: Ataque, Caloto, Cauca, ejercito, ELN, indígenas, policia
Slug: indigenas-de-cauca-denuncian-hostigamientos-por-combates-entre-el-eln-y-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vice] 

###### [02 May. 2017] 

La comunidad indígena de Huellas, en Caloto departamento del Cauca, aseguró este lunes que en el marco del proceso de liberación de la madre tierra que se encuentran realizando en la casa finca de **La Hacienda La Emperatriz, se presentó un ataque de un grupo armado a un Policía.** En la denuncia, se asegura que los hombres portaban brazaletes del ELN y armas de largo alcance.

Según Nelson Pacue, Gobernador del Resguardo Bellas en Caloto “desafortunadamente **grupos armados identificados con brazaletes del ELN entran a la Hacienda La Emperatriz, en donde permanece de manera constante la Policía** y le disparan con arma de fuego a un integrante de la Policía, donde involucran al movimiento indígena y al proceso de liberación de la madre tierra”.

En este territorio desde hace varios años la comunidad indígena esta de manera permanente, haciendo el ejercicio de la liberación de la madre tierra, razón por la que ven con preocupación que se presenten **este tipo de situaciones, que según el líder los pone en riesgo. **Le puede interesar: [Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca](https://archivo.contagioradio.com/continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca/)

“El día de ayer hicimos una marcha hacia el casco urbano de Caloto rechazando lo que sucedió en esa finca” agregó el Gobernador. Le puede interesar: [Diego Rodríguez 5to defensor de DD.HH asesinado en Cauca](https://archivo.contagioradio.com/diego-rodriguez-5to-defensor-de-dd-hh-asesinado-en-cauca/)

**La Policía, cuenta Pacue, respondió a este ataque por parte de los hombres armados sin embargo ya fue muy tarde**, dando como resultado la fuga “los que causaron esa desarmonía salieron huyendo y cuando la Policía reacciona ya había pasado lo que lastimosamente ocurrió”.

Adicionalmente, hacia las 6 de la tarde de este 1 de mayo, se dio a conocer **un comunicado en el que son amenazadas las comunidades y propiamente a las autoridades de este resguardo.** En dicha amenaza se manifiesta que no están de acuerdo con el control territorial que hace la guardia indígena.

“Es algo preocupante, **es la segunda vez que sale un comunicado de los Gaitanistas amenazando a la autoridad y a nuestros guardias indígenas** (…) los grupos armados quieren intimidar a la comunidad y lo que hemos dicho es que seguiremos en pie de lucha, de resistencia y que no aceptamos ningún grupo armado en nuestro territorio” aseveró Pacue.

Estos panfletos amenazantes son tirados en las vías y en algunas casas de la zona “lo que hemos visto es mucho personal extraño, que se mueve al interior de la comunidad”, relata el Gobernador. Le puede interesar: [José Yatacúe, integrante de las FARC-EP fue asesinado en Toribio, Cauca](https://archivo.contagioradio.com/jose-huber-yatacue/)

Luego de lo sucedido y de la marcha realizada, las comunidades pusieron en conocimiento de las respectivas autoridades y organizaciones de derechos humanos lo acontecido. Le puede interesar: [Asesinan a lider indígena Gerson Acosta en Timbio, Cauca](https://archivo.contagioradio.com/asesinan-gerson-acosta-timbio-cauca/)

En la actualidad la zona se encuentra militarizada y según el propio Gobernador, la **Alcaldía, La Policía y el Ejército conocen de estas situaciones que vienen pasando en los territorios** (…) uno se sorprende porque el Batallón se encuentra a dos minutos y uno no entiende porque suceden estas cosas”.

De igual modo, las comunidades se han manifestado en asamblea permanente “estamos organizando una audiencia pública que se va a hacer en Cali para rechazar todas esas amenazas, pero también los muertos que han surgido desde hace varios meses (…) **nos toca movernos porque nosotros siempre caminamos la palabra y defendemos la vida de las personas” concluyó Pacue.**

Este es el comunicado dado a conocer:

Ataque de grupo armado a policía ubicada en la casa finca de la hacienda la Emperatriz pone en Alto riesgo a la comunidad indígena de Huellas Caloto, y el proceso de liberación de la madre tierra.

Hechos, cerca de las 5 de la mañana del día de hoy 1 de Mayo, se presento hostigamiento con armas de largo alcance a unidades de la policía y ejercito nacional acantonadas en la casa finca de la hacienda la Emperatriz Caloto, el combate registrado aproximadamente 20 minutos, puso en alto riesgo a la comunidad de las Veredas de la Selva, Bodega Alta y la Selvita del resguardo de Toez.

La zona se encuentra militarizada, manifestamos nuestra preocupación por este hecho, y solicitamos a los organismos de DDHH a estar atentos al desarrollo de esta situación.

Tejido Defensa de la Vida y los DDHH de ACIN.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
