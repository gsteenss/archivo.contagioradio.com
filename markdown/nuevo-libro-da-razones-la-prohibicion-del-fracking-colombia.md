Title: Nuevo libro da razones para la prohibición del fracking en Colombia
Date: 2019-04-03 18:07
Category: Ambiente, Nacional
Tags: fracking
Slug: nuevo-libro-da-razones-la-prohibicion-del-fracking-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: http://blog.gesternova.com] 

###### [29 Mar 2019] 

[La Asociación Interamericana para Defensa del Ambiente (AIDA), en conjunto con la Fundación Heinrich Böll, presentará un nuevo libro titulado ***La prohibición del fracking en Colombia como un asunto de política pública*** el próximo 4 de abril. Con la colaboración de la Alianza Colombia Libre del Fracking, este libro une las perspectivas de ocho autores sobre el debate que se realiza sobre esta técnica de extracción en el escenario público.]

Unos de las temas centrales es la prohibición del fracking en Colombia, lo cual ya se ha implementado en otros países en Europa, como Francia e Irlanda. [Juana Hofman, abogada de la AIDA y coordinadora de la Red por la Justicia Ambiental en Colombia, resalta que ya se ha iniciado procesos en el Gobierno y con la ciudadanía para dar a conocer los argumentos para la prohibición del fracking y los efectos nocivos que causarían para el ambiente y la salud. ]

"Es importante realizar todo lo que ha hecho la ciudadania, lo que se ha logrado hasta hoy para que el fracking sea un discusión tan vigente, tan actual. Donde la gente ya está absolutamente concientizada que son los yacimientos no convencionales, los riesgos del fracturamiento" relató Hofman. (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")

Los últimos dos años se ha realizado este trabajo por parte de más de 85 organizaciones en las regiones del país. En particular, Hofman resalta el esfuerzo del equipo interdisciplinario de la Alianza que ha emprendido talleres, foros y conversatorios para que la información académica sobre esta técnica de extracción llegue a las comunidades.

A propósito, el libro, realizado a lo largo de sietes meses, aborda aspectos técnicos, impactos en los acuíferos, transición energético y la paz, además se incluyen conclusiones y recomendaciones para entender como la prohibición del fracking es un asunto de política pública urgente. "La idea es seguir el tema muy vigente y seguir abundando en el tema", concluyó Hofman.

El lanzamiento del libró se realizará en el Centro Cultural Gabriel Garcia Marquez en el centro de Bogotá desde las 5:30 de la tarde. En la charla titulada Fracking, Construcción de Paz y Transición Energética, habrá intervenciones de congresistas, miembros de las comunidades y activistas ambientales, seguido por un concierto de Fer Cely de los Rolling Ruanas.

<iframe id="audio_34097064" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097064_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
