Title: La agonía de Tasajera, Magdalena
Date: 2017-12-10 08:57
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente, basura, Ciénaga del Magdalena, Tasajera
Slug: la-agonia-de-tasajera-magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Tasajera-Magdalena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Maria Alejandra Aguilar 

#### **[Asociación Ambiente y Sociedad ](https://archivo.contagioradio.com/margarita-florez/)** 

###### 10 Dic 2017

Tasajera es un pequeño corregimiento olvidado de Pueblo Viejo – ubicado en la isla de Salamanca Magdalena, entre el mar y el manglar. El corregimiento es únicamente visible para los que transitan la vía Barranquilla – Ciénaga por la Troncal del Caribe, quienes pasan por el Km 19 son testigos de las condiciones extremas en las que viven sus habitantes.

[La primera impresión de la zona es dada por la acumulación de basura en frente de las casitas de concreto y latón que se levantan al margen de la carretera, cúmulos de plásticos y líquidos de colores extraños son los elementos más sobresalientes del paisaje; sobre la vía varios puestos de venta de conchas, huevos de tortugas, iguanas y pescado, que denotan el comercio de fauna silvestre y la economía de subsistencia que rige para esta comunidad, la descomposición rápida de estos elementos debido al calor atrae nubes de moscos y olores fuertes, un aire gris y árido deja a la vista una sensación de abandono que se confirma con los recurrentes bloqueos, “a la brava”, de la vía por parte de algunos de los pobladores.]

[Los bloqueos consisten en obstaculizar la troncal con tanques de agua y piedras con el fin cobrar un peaje en ambas direcciones a los carros y camiones que van y vienen; aquí no importa que en menos de 2km se repita el cobro legal de los derechos de tránsito que supuestamente garantizan la seguridad y el buen estado de la vía, ni que exista una base militar y un puesto permanente de policía a poca distancia, porque las autoridades allí no entran. Tasajera hace parte de las áreas calientes dónde el estado es etéreo.]

[La ausencia de gobierno no solo se ve reflejada en estos por menores de orden público; los bloqueos pueden ser una forma de producir algunos ingresos, sí, pero también una forma de protesta; los pobladores se quejan de la ausencia de servicios básicos sanitarios que es evidente, hay carencia de alcantarillado, el manejo de residuos es nulo y la disponibilidad de agua se rumora está sometida a su venta; según personas de Tasajera carros tanque de agua pasan esporádicamente vendiendo el vital recurso a cuatro mil pesos por galón, todo un lujo imposible para muchos, así que normalmente agua no hay.]

[La Ciénaga del Magdalena es también una damnificada de este olvido. A pesar de ser uno los ecosistemas de lagunas más importantes de Colombia, hábitat de múltiples especias endémicas, con una gran extensión de manglar y bosque seco, jamás tendrán un proceso de restauración ni conservación efectivo hasta que no se miren los conflictos sociales y las necesidades básicas de las comunidades aledañas. Todos estas basuras y aguas negras del municipio de Pueblo viejo van a dar de alguna y otra forma al mar o al manglar.]

[El corregimiento de Tasajera, es sólo un caso más de los miles que deben existir en Colombia sobre discriminación ambiental y abandono. La protección ambiental y el control de los riesgos de salud usualmente esta estratificado; la calidad de vida de la población y el bienestar del medio natural están directamente relacionados. La participación de la comunidad local es el primer paso para disminuir la degradación de los ecosistemas, sin embargo, ante las condiciones extremas que amenazan su diario vivir, el medio ambiente no es visto como una prioridad, y el ciclo de las problemáticas socioambientales se mantiene inmutable en el tiempo.]

#### **[Conozca aquí  las columnas de opinión de Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
