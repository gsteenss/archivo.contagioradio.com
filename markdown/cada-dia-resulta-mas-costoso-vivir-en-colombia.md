Title: Dos medidas que afectan el bolsillo de los colombianos
Date: 2016-07-06 16:27
Category: Economía, Nacional
Tags: Canasta familiar, costo de vida en colombia, Inflación en colombia, Tasas de interés
Slug: cada-dia-resulta-mas-costoso-vivir-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/quiebra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### [ 06 jul 2016] 

En lo que va corrido de 2016, el costo de vida para los colombianos sigue con tendencia al alza. De acuerdo con las cifras presentadas por el Departamento Nacional de Estadística DANE, **el Indíce de Precios al Comsumidor (IPC) durante el mes de junio fue de 0.48%**, un repunte significativo  al 0.10% registrado en el mismo periodo de 2015.

Como resultado de la tendencia al alza registrada durante **los primeros seis meses del año, el índice de inflación ya alcanza el 5,10% y el 8.60% acumulado en los últimos 12 meses**, cifra que sobrepasa ampliamente al 4% estimado como meta por el gobierno nacional para todo 2016.

La variación mensual presentada el mes anterior es la más alta registrada desde el año 2008, mientras que la inflación anualizada **ha llevado a que los colombianos paguen cerca de un  15% más  sobre el precio de los productos básicos de la canasta familiar**, ante lo cual el incremento del salario mínimo fijado en 7% resulta insuficiente y su poder adquisitivo a disminuido ostensiblemente.

El incremento en los alimentos con el 8,36%, salud 6.03% y educación 5,76% ha favorecido para que durante un solo semestre fuera superada la proyección anual estimada. Los alimentos que registraron las mayores variaciones de precios fueron el tomate, la cebolla, el tomate de árbol, la zanahoria y la carne de res.

Aunque desde el gobierno se asegura que el precio internacional del petróleo, el precio del dólar, el fenómeno del niño y recientemente el paro camionero son los causantes del encarecimiento del costo de vida, es innegable que **los TLC que viene firmando el país desde 2012, alteran las dinámicas de oferta y demanda**, encareciendo los insumos y disminuyendo la producción nacional.

**Y los intereses siguen subiendo.**

Luego de incrementar en 0.25 puntos  las tasas de interés finalizando el mes de junio, para tratar de mantener el indice de inflación controlando la sobreoferta de moneda, **el Banco de la República acudiría posiblemente a un nuevo aumento en  las tasas de interés** que actualmente se encuentra en el 7,50%, perjudicando a quienes poseen créditos bancarios y al crecimiento económico del país.
