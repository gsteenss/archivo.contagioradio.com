Title: Indiscriminada tala de árboles en humedal La Conejera
Date: 2015-06-12 14:09
Category: Ambiente, Nacional
Tags: Derechos ambientales, Humedal La Conejera, Ministerio de Ambiente, Praga S.A, tala árboles
Slug: indiscriminada-tala-de-arboles-en-humedal-la-conejera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/conejera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Humedal Conejera Acción Comunitaria] 

##### [12 Jun 2015] 

En horas de la mañana inició la tala de más de 10 árboles en la zona humedal la Conejera, dejando dos especies de aves sin hogar y ocasionando un grave daño ambiental.

[[![CHT9b46WsAIszOw](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CHT9b46WsAIszOw.png)

Según Gina Díaz, defensora del humedal La Conejera, la constructora Praga S.A. dio inicio hace dos días a la tala de algunos árboles seleccionados, con conocimiento de que 11 de ellos no podían ser talados. Sin embargo, el día de hoy estos fueron cortados incumpliendo con el protocolo establecido.

Ante el evento no se ha visto la presencia del Ministerio de Ambiente ni de alguna entidad que se cerciore de la situación.

En el lugar se encuentran alrededor de 50 agentes de la policía junto con fuerza disponible, y diversos defensores del ambiente, quienes han exigido el cese del talado de los árboles, manifestando su gran preocupación ante el evento que pone en grave riesgo la flora y la fauna del sector.

Es preciso agregar que el Humedal ha sido víctima repetidas veces de la constructora Praga S.A, la cual, a través de su proyecto Reserva Fontanar ha puesto en riesgo la fauna, la flora y la vida de los habitantes del sector, pues se detectaron gases fuertemente contaminantes
