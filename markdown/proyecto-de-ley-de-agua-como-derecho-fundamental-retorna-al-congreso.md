Title: Proyecto de ley de Agua como derecho fundamental retorna al Congreso
Date: 2017-03-22 15:55
Category: Ambiente
Tags: Censat agua Viva, Defensa del agua
Slug: proyecto-de-ley-de-agua-como-derecho-fundamental-retorna-al-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/minutode-dios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minuto de Dios] 

###### [22 Mar 2017] 

El proyecto de ley que buscaba que el agua se convirtiera en un derecho fundamental en la Constitución de Colombia, volverá a ser radicado en el Congreso, el senador Jorge Prieto, ponente principal del proyecto señaló que espera que esta vez **el gobierno no permita que un tema que es “de vida o muerte” para el país, se quede en los debates.**

Este proyecto de ley, se cayó en su primera proposición en el congreso, faltándole un debate para su aprobación, debido a que el presidente de la Comisión Primera de la Cámara no citó a sesiones a la comisión, **hundiendo el proyecto por falta de tiempo para debate.** Le puede interesar: ["Hundido proyecto de ley para que el agua fuese un derecho fundamental"](https://archivo.contagioradio.com/hundido-proyecto-de-ley-para-que-el-agua-fuese-un-derecho-fundamental/)

Prieto señala que la importancia de esta iniciativa es la prioridad al consumo humano y la obligación al Estado para que proteja las fuentes hídricas, “es preocupante que el gobierno de nuestro país no se preocupe por cuidar las fuentes de agua, sino que todo lo contrario, **ha sido una lucha permanente de las comunidades contra el Estado que protege a las Petroleras y empresas mineras**”.

### **El agua no puede verse solamente desde la visión antropocéntrica** 

Sin embargo, para Tatiana Roa, integrante de CENSAT Agua Viva, estas iniciativas tienen que ir mucho más allá en la defensa del agua, de modo tal que su única finalidad no sea la protección para el consumo humano, **sino la recuperación de otras nociones del agua y de su importancia en el equilibrio natural**. Le puede interesar: ["La radiografía del derecho fundamental del agua en Colombia"](https://archivo.contagioradio.com/la-radiografia-del-derecho-fundamental-al-agua-colombia/)

Roa indicó que en diferentes lugares del territorio el agua ya ha desaparecido “la situación es dramática, cuando vienen las estaciones secas vemos como gran parte del país no tiene agua, **la mayoría de municipios pequeños no logra proveerse de agua, sino que la ven un par de veces a la semana**, esto pasa en la Costa Caribe, llego un momento en donde más de 300 municipios estaban sin agua”.

Otro caso, que afana a organizaciones defensoras del ambiente, tiene que ver con el uso del agua por parte de empresas petroleras en el Llano, que de acuerdo con la integrante de CENSAT, ya han pensado en comprar el agua de los municipios, e incluso establecieron que las **Autoridades Ambientales le han recomendado a estas empresas, que se creen proveedores que vendan el agua en bloque**.

Tatiana Roa, manifestó que es de vital importancia continuar con acciones e iniciativas que velen por la defensa del agua como el referendo por el agua, **apoyar las consultas populares y seguir fortaleciendo marchas como el carnaval del Tolima**, que para este año se espera se realice en todo el país. Le puede interesar: ["Nuevos obstáculos para la Consulta Popular en Cajamarca"](https://archivo.contagioradio.com/nuevos-obstaculos-para-la-consulta-popular-en-cajamarca/)

<iframe id="audio_17714058" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17714058_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
