Title: Vender el 20% de la EEB es una pérdida millonaria para el distrito
Date: 2016-09-29 14:40
Category: Economía, Nacional
Tags: Privatización, Privatización acueducto, Privatización de la salud, privatizacion etb, segunda administración peñalosa
Slug: vender-el-20-de-la-eeb-es-una-perdida-millonaria-para-el-distrito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/EEB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana ] 

###### [29 Sept 2016] 

Negación y oposición ha recibido la más reciente medida anunciada por el alcalde Enrique Peñalosa de vender el 20% de las acciones de la Empresa de Energía de Bogotá, por parte de diversos sectores como el Sindicato de Trabajadores de la Energía de Colombia quienes aseguran que **no es aceptable que se privaticen bienes estatales sobre todo cuando generan utilidades de \$1.4 billones** para el distrito, como es el caso de esta empresa.

Pablo Santos, presidente de Sintraelecol asegura que la Empresa de Energía de Bogotá es altamente rentable gracias a que ha sido muy bien administrada durante los últimos cinco años, razón por la que se rechaza la pretensión del alcalde capitalino de **invertir los \$3.5 billones de la venta de las acciones en la construcción de vías en la** [[Reserva Tomas Van der Hammen](https://archivo.contagioradio.com/los-enredos-de-los-funcionarios-que-estan-detras-de-la-urbanizacion-de-la-van-der-hammen/)].

Para el líder **no es acertado que el distrito renuncie de manera permanente a un porcentaje importante de las utilidades de la empresa** y mucho menos cuando lo que pretende es construir las avenidas Longitudinal de Occidente; Suba-Cota; Ciudad De Cali; Boyacá; Laureano Gómez; Alberto Lleras Camargo; El Polo; Arrayanes; El Jardín y Guaymaral, en las 1.400 hectáreas de la Reserva, entre otras porque este espacio [[garantiza la conectividad ecológica de la capital](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/)].

El que la Empresa de Energía de Bogotá sea la propietaria del 51% de Condensa y Emgesa, es otra de las razones por las que el Sindicato se opone a la venta de parte de sus acciones, pues su porcentaje en estas dos empresas también se vería afectado, así como las inversiones en temas de infraestructura y educación, teniendo en cuenta que **el 76% de las utilidades de la EEB ayudan a la financiación de obras de inversión social**.

Las privatizaciones lo único que dejan es miseria, desfinanciación y más impuestos, afirma el sindicalista quien agrega que la Empresa de Energía de Bogotá no le pertenece a la alcaldía de la capital, sino a todos los bogotanos a quienes convoca a hacer parte de las **diversas actividades que se pondrán en marcha para detener la enajenación**, como la presión que se ejercerá en el Concejo de Bogotá.

Vea también: [[60 mil firmas recolectadas para cabildo abierto contra venta de ETB](https://archivo.contagioradio.com/60-mil-firmas-recolectadas-para-cabildo-abierto-contra-venta-de-etb/)]

<iframe id="audio_13114449" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13114449_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
