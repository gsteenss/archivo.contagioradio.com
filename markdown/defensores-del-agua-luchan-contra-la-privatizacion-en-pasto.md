Title: Defensores del agua luchan contra la privatización en Pasto
Date: 2015-11-10 17:32
Category: Movilización, Nacional
Tags: Ambiente, Empopasto, Mandato popular por la defensa del agua, Movimiento en defensa del agua, páramos, Privatización acueducto
Slug: defensores-del-agua-luchan-contra-la-privatizacion-en-pasto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/pasto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fredy A. Gamez 

<iframe src="http://www.ivoox.com/player_ek_9343928_2_1.html?data=mpihlZ6WfI6ZmKiak5iJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbaxtPg0dfJt4zYxtGYw8zZpYzg1sjVw9OPp9Di1dfOjdHFb9HmytvO1s7epcTdhqigh6eXsozZz5Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camilo Salazar, vocero comunidad] 

###### [10 nov 2015] 

Habitantes de Pasto denuncian la pretención de la Administración Municipal y la junta directiva de la Empresa de Obras Sanitarias de Pasto (EMPOPASTO) de entregar la gestión del recurso hídrico y la operación del servicio de acueducto a empresas privadas, a través de la constitución de una nueva empresa de carácter mixto.

Camilo Salazar, vocero del Movimiento social por la defensa del agua, advierte de la intención de las empresas "**Proactiva S.A"**, filial de la empresa Veolia de capital francoespañol  y **"Aqualogy"**, filial de aguas de Barcelona perteneciente al **grupo francés SUEZ,** de quedarse con la prestación del servicio que en la actualidad se brinda a cerca de 80.000 usuarios, con ingresos aproximados de 38 mil millones de pesos al año.

El llamado de los ciudadanos es a suspender el proceso en tanto la empresa se encuentra en la actualidad en una "situación viable en términos financieros" y a realizado enormes inversiones por medio de créditos y regalías, infraestructura que se entregaría a las postulantes en una escritura de "derecho real de usufructuo" para la explotación y acomulación de capital,  según manifiesta Salazar.

A la indignación de la ciudadanía por la pretención gubernamental se suma la incognita que rodea la intención de apresurar el proceso  a pocas semanas de terminar la administración, que a la luz de la comunidad resulta **“inconveniente”, “ilegal” e “ilegítimo”**. El vocero de los ciudadanos subraya que resulta alarmante que la privatización se de a un operador extranjero, con lo que se incrementarán los costos de operación que se revertirán en los cobros a los usuarios finales del servicio.

Ante el actual panorama los habitantes se movilizaron la mañana de este martes para exigir a la administración que no se realice el proceso de privatización con el que además **se coptarían los acueductos comunitarios en las zonas rurales** de Pasto, poniendo en riesgo las fuentes de agua que vienen de los páramos, y se avanzaria en negocios como la construcción de hidroeléctricas y embotellamiento de agua.

Salazar indica que la comunidad ha adelantado las acciones correspondientes, pero afirma que **se han “agotado las vías de diálogo directo”**, por ejemplo se convocó a la administración a una reunión y esta “no asistió”, además que se han presentado dos acciones populares y una demanda de nulidad, sin embargo la gerencia de las empresas y la dirección de Pasto insisten en mantener este proceso de manera intransigente.

Entre las acciones que adelantarán los ciudadanos se ha **convocado para el día viernes 13 de noviembre**,  un cabildo abierto y popular que de continuidad al **“primer mandato popular por la defensa del agua”,** pero si no se logra resolver nada con la administración y continúa en esta posición se **empezará a preparar un paro cívico municipal,** que pueda detener las pretensiones del alcalde.

> El [\#agua](https://twitter.com/hashtag/agua?src=hash) no se vende el [\#agua](https://twitter.com/hashtag/agua?src=hash) se defiende [@CuyigansPasto](https://twitter.com/CuyigansPasto) [@Contagioradio1](https://twitter.com/Contagioradio1) [@KiweNasa](https://twitter.com/KiweNasa) [@rcnlaradio](https://twitter.com/rcnlaradio) [@HOLLMANMORRIS](https://twitter.com/HOLLMANMORRIS) [pic.twitter.com/SbBcuq4hYa](https://t.co/SbBcuq4hYa)
>
> — Fredy A Gamez (@fgamez30) [noviembre 10, 2015](https://twitter.com/fgamez30/status/664124392796999680)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

