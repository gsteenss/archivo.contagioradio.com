Title: Tres películas colombianas salen a la conquista del público internacional
Date: 2016-08-27 15:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, festival de cine, Fragmentos de Amor, Santa y Andrés
Slug: dos-nuevas-peliculas-colombianas-salen-a-la-conquista-del-publico-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/A-canada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [25 Agos 2016]

Más películas colombianas siguen viajando a los festivales de cine más importantes del mundo. Nuevas producciones que además de dejar en alto el nombre del país, aportan al crecimiento en diversidad de la cinematografía nacional.

En los últimos meses, varios films nacionales han pasado con éxito por las grandes pantallas internacionales; El abrazo de la serpiente, Todo comenzó por el fin y La Tierra y la Sombra, han pisado la alfombra roja de Festivales tan importantes como Cannes, Berlín y los premios Oscar.

El turno ahora es para los largometrajes Fragmentos de Amor, Pariente y la coproducción Santa y Andrés.

El primero esta inspirado en la novela “Fragmentos de amor Furtivo” del escritor colombiano Héctor Abad Faciolince y es dirigido por Fernando Vallejo. Con apenas dos semanas en la cartelera nacional, el film ha recibido la nominación en la categoría mundial de Operas Primas en el Festival de Cine de Montreal en Canadá, donde competirá con otras 21 películas. Fragmentos de Amor, cuenta la historia  de un afinador de pianos, enamorado de una profesora de natación y escultura que gurda un terrible secreto.

<iframe src="https://www.youtube.com/embed/_bXU4ZcIZmY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La segunda película que viaja, también a Canada, es Pariente, Ópera prima del director Iván D. Gaona y la productora Diana Pérez Mejía, elegida dentro de la Selección Oficial del 41º. Festival Internacional de Cine de Toronto, que tendrá lugar entre el 8 y el 18 de septiembre en dicha ciudad canadiense. Pariente cuenta la historia de Willington, un camionero transportador de caña de azúcar que busca recuperar el amor de su ex novia Mariana, al tiempo que ella pretende casarse con su nuevo novio.

<iframe src="https://www.youtube.com/embed/HDQodqUcdkU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por último, la coproducción cubano- colombo- francesa **Santa y Andrés,** segundo largometraje del director Carlos Lechuga, tendrá su premier mundial en la sección oficial Contemporary World Cinema del **Toronto International Film Festival** y luego en la edición 64 del **Festival de San Sebastián** del 16 al 24 de septiembre; una película que narra la historia de un escritor homosexual y una campesina que se verán obligados a convivir y conocerse en un mismo espacio.

<iframe src="https://www.youtube.com/embed/bItT-BT2_u8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Ambos largometrajes demuestran que la producción nacional es cada vez más atractiva para los organizadores y críticos de festivales internacionales de cine, con nuevas historias que traen nuevas maneras de formar y de entretener a los espectadores.
