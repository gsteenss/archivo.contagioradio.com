Title: La actitud contradictoria de muchos cristianos y cristianas
Date: 2019-07-29 09:04
Author: A quien corresponde
Category: Opinion
Tags: Cristianos, Fe, Iglesia
Slug: a-quien-corresponde-la-actitud-contradictoria-de-muchos-cristianos-y-cristianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Con la medida que midan a los otros, los medirán a ustedes **

[(Mateo 7,2) ]

[Bogotá, 20 de julio del 2019]

*[Hay cristianos que no]* ***se reconocen****[ pecadores ]*

*[y andan exigiéndole a los demás que no pequen. ]*

[Estimado]

**Hermano en la fe**

[Cristianos, cristianas, personas interesadas]

[Cordial saludo,]

[Comienzo reconociendo que todas las personas, yo me incluyo, tenemos contradicciones entre lo que decimos y lo que hacemos, entre lo que pensamos y decimos, entre lo que queremos ser y lo que podemos ser. Esta contradicción, está presente en diversos niveles, por diversas causas y en diferentes proporciones en nuestro interior, en nuestra sociedad y en nuestras Iglesias; está presente en la vida cotidiana, en la vida de fe, en la vida política, en la vida social, en la relación con la naturaleza, en la relación familiar, es decir, en todas las relaciones y en toda la vida, de creyentes y no creyentes. ]

[Hay una contradicción interna entre la tendencia al bien y la tendencia al mal que habitan en nosotros, unas veces con mayor fuerza y otras con menor. Hay una distancia grande entre la fe que decimos tener y el cumplimiento de sus exigencias religiosas, éticas y sociales; entre lo que predicamos y lo que aplicamos.]

[Un riesgo, de los creyentes en general y de los cristianos en particular, es juzgar muy duro lo que los otros hacen, sus fallas, sus equivocaciones, su manera de actuar y de relacionarse, sin darnos cuenta que, con frecuencia, tenemos maneras de ser y de actuar contrarias al mensaje del Evangelio. ]

[A la mayoría de creyentes nos hace falta profundizar la Palabra de Dios, los Evangelios, el conocimiento de la vida, obra y mensaje de Jesús de Nazaret. Podemos tener creencias religiosas fuertes y arraigadas, podemos ser fieles y cumplidores de normas religiosas, pero, fácilmente, vivimos, actuamos y hablamos en contradicción con el mensaje de Jesús, es decir, nos parecemos a los fariseos y maestros de la ley del tiempo de Jesús que eran muy religiosos y cumplidores de la ley, pero fueron incapaces de reconocer al Hijo de Dios y su mensaje. Siendo muy religiosos a menudo actuamos como “anti-cristianos”, es decir, en contra de lo que decía y hacia Jesús de Nazaret. ]

[Frecuentemente juzgamos con dureza a las personas ateas, sin religión, agnósticas, incrédulas, no “practicantes” o de “las otras iglesias” sin darnos cuenta que muchas de ellas pueden ser más honestas y respetuosas, mejores personas, mejores ciudadanos, más informados y responsables que nosotros, olvidando las palabras de Jesús: “]*[No todo el que diga: ¡Señor, Señor!, entrará en el reino de los cielos, sino el que haga la voluntad de mi Padre del cielo. Cuando llegue aquel día, muchos me dirán: ¡Señor, Señor ¿No hemos profetizado en tu nombre? ¿No hemos expulsado demonios en tu nombre? ¿No hemos hecho milagros en tu nombre? Y yo entonces les declararé: Nunca los conocí; apártense de mí, ustedes que hacen el mal”]*[(Mateo 7,21-23) ]

[Muchas personas religiosas y de fe, tenemos “doble medida” para juzgar los mismos hechos, dependiendo de si son “los nuestros” o "los otros"; si son de nuestras mismas creencias religiosas, sociales, políticas, sexuales o culturales. Cuando son “los otros”  quienes tienen fallas, cometen delitos, son corruptos, etc. los tratamos como endemoniados, terroristas, perversos o depravados y agrandamos sus fallas y pecados, sin darles derechos a defenderse o expresar su punto de vista; pero si las mismas cosas las hacen “los nuestros” las minimizamos, las silenciamos, las justificamos alegando que son acusaciones falsas, que es persecución religiosa, política, racial o social; que son envidias, mentiras o ignorancia, sin tomarnos el trabajo de preguntar por las evidencias y las razones de sus acciones. Nuevamente olvidamos la Palabra de Dios: “]*[Con la medida que midan a los otros, los medirán a ustedes]*[(Mateo 7,2) ]

[Reconozco, personalmente, que nosotros, por ser creyentes, no somos mejores que los demás; que por hacer parte de la iglesia, no hacemos lo que Dios quiere; que para “salvarnos” no son suficientes la oración, el culto o las prácticas religiosas, es necesario seguir a Jesús haciendo realidad el Reinado de Dios, que es Justicia, amor, solidaridad, respeto a todo ser humano, a toda la creación, a la naturaleza, porque ]*[“Si el modo de obrar de ustedes no supera a los letrados y fariseos, no entrarán en el Reino de los cielos”]* [(Mateo 5,20).]

[Si los creyentes no somos buenas personas, buenos ciudadanos, más solidarios, más justos, mejor informados, defensores de la naturaleza, de la creación… no nos salvamos, y hacemos quedar mal a Dios ante el mundo.  ]

[Es normal que veamos el mundo desde nuestra perspectiva, desde nuestra mirada, que miremos a los demás desde nosotros, pero no considero adecuado calificar nuestra manera de ver el mundo y la realidad como mejor, más buena, la única, moralmente superior, sin ser autocríticas. Con frecuencia, nos sumamos a la lógica que divide el mundo entre nosotros y los otros, entre los de aquí y los de allá, entre los buenos y los malos, entre los salvados y los condenados, negando el valor y la dignidad de los otros, de los de allá, de los malos, de los de la otra religión, mientras Jesús de Nazaret continuamente nos recuerda:]*[“Aquel de ustedes que esté libre de pecado, que tire la primera piedra”]*[ (Juan 8,7)]

[Fraternalmente, su hermano en la fe,]

[P. Alberto Franco, CSsR, J&P]

[[francoalberto9@gmail.com]](mailto:francoalberto9@gmail.com)[ ]
