Title: ¿Realmente se necesita El Quimbo con pleno fenómeno de El Niño?
Date: 2016-01-04 21:49
Category: Ambiente, Nacional
Tags: cambio climatico, El Quimbo, Fenómeno de El Niño
Slug: realmente-se-necesita-el-quimbo-con-pleno-fenomeno-de-el-nino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/20150623203904186FotoNota2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario el Huila 

###### [4 Enero 2015] 

Durante la primera semana del 2016, en las imágenes del río Magdalena, **se observa cómo apenas sobrepasa los 30 centímetros, debido al fenómeno de El Niño** que se ha recrudecido. Puertos Wilches, Santander, Bodega Central, Sur del Bolívar, son los lugares más críticos, donde se estima que el nivel del agua se encuentra entre 30 y 45 centímetros, cuando antes el nivel no era menor a 1.40 metros.

Por su parte, el gobierno colombiano ha asegurado que la decisión de la Corte Constitucional de apagar la represa El Quimbo, no ha sido una buena decisión frente al fenómeno del Niño que actualmente se vive, pero lo cierto, es que e**ste tipo de mega proyectos como las hidroeléctricas contribuyen al cambio climático** y a su vez, acaban con la fauna y flora de los territorios donde se desarrollan estos proyectos.

El presidente Juan Manuel Santos a través de su cuenta de twitter ha asegurado que **“Reabrir la represa ‪\#ElQuimbo es urgente!** Con su funcionamiento evitaremos daños ambientales, sociales y económicos”, argumentado que de prenderse nuevamente la represa se podría “prevenir la muerte de 19mil toneladas de peces en el embalse de Betania pues el agua que le surte El Quimbo está estancada y deteriorándose”.

Sin embargo, este argumento del presidente Santos contrasta con las múltiples protestas de los pescadores artesanales, que desde que se inició la construcción de El Quimbo y hasta el día de hoy vienen denunciando que por cuenta de esta represa y de Betania, ha habido una preocupante reducción de producción de peces.

De acuerdo con los pobladores del departamento del Huila, se han registrado millonarias pérdidas debido a **la contaminación del agua y el bajo caudal de los ríos como el Magdalena, generando que la producción de pesca se haya reducido en un 90%.** Desde ASOQUIMBO, se había señalado que el llenado del embalse ocasionaría la muerte de 30 mil toneladas de peces y afectaría notablemente el ecosistema del río Magdalena, además afirman que cuando empezó a funcionar la hidroeléctrica, los niveles de agua bajaron y fueron evidentes los escombros de madera en estado de descomposición que empezaron a afectar la salud de los pescadores.

Juan Manuel Gutiérrez, gerente de la Asociación de Piscicultores del Huila, Asopishuila, afirma: “**Que lleguen 36 metros cúbicos por segundo desde la hidroeléctrica El Quimbo hasta al embalse de Betania afecta la piscicultura. La calidad del agua será de baja calidad.** En el embalse quedaron sin talar ni recoger alrededor de 1.000 hectáreas de bosque. Cuando se llene, esa vegetación continuará ahí, se descompondrá y el agua llegará al embalse de Betania”.

Por otra parte, según la organización ambientalista, Greenpeace, el aprovechamiento de la energía hidráulica con fines eléctricos implica el control artificial de ríos y cuencas fluviales,  lo que a su vez promueve y contribuye al aumento de la temperatura de la tierra, ya que los embalses de **las grandes hidroeléctricas que existen en el mundo emiten al menos 4% del total de gases de efecto invernadero**, debido principalmente a la emisión de metano generado por la materia orgánica en descomposición sumergida en las grandes reservas de agua, sin contar con la emisión de gases de dióxido de carbono y otros gases que se liberan durante la cadena de producción de hidroelectricidad. Cabe recodar que de acuerdo a la NASA el calentamiento global que actualmente atraviesa el planeta es un factor nuevo que influye en los efectos y la naturaleza del fenómeno de El Niño.

Esto sumado a que **los mayores consumidores de agua del país son el sector agrícola y el de energía que en total consumen casi el 70% del recurso hídrico,** hacen pensar, ¿Será que realmente el país necesita El Quimbo y otros megaproyectos hidroeléctricos? ¿realmente estaríamos en riesgo de apagón o más bien se estaría dando un pequeño paso para evitar mayores impactos ambientales y sociales?
