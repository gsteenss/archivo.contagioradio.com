Title: Alcalde y personero de Valdivia criminalizan al Movimiento Ríos Vivos
Date: 2016-06-07 15:35
Category: Paro Nacional
Tags: Antioquia, ESMAD, Minga Nacional, Valdivia
Slug: alcalde-y-personero-de-valdivia-criminalizan-al-movimiento-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/CkG85E_XEAAy_31.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Ríos Vivos 

###### [7 Jun 2016] 

El pasado 3 de junio, desde las 7:30 de la mañana, cerca de 500 integrantes del Movimiento Ríos Vivos de Antioquia, decidieron tomarse pacíficamente la Alcaldía de Valdivia con el objetivo de conseguir respuestas por parte del alcalde, sin embargo, hacia las 4:30 de la tarde lo que llegó fue el ESMAD para desalojar violentamente  a los manifestantes, dejando a un niño herido en medio   de la represión de la fuerza pública.

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, denuncia que el comandante de la Policía de Antioquia, dijo públicamente que el objetivo de la ocupación de las instalaciones de la alcaldía era robar un computador, **criminalizando la protesta,** sin tener en cuenta las exigencias y los motivos políticos por los cuales el Movimiento Ríos Vivos se había tomado la Alcaldía.

Aunque **el Alcalde Jonas Darío Henao Cardona desmintió que se hubiesen robado algún elemento, el Personero Didier Pastrana** y el mismo alcalde han insistido en criminalizar a la movilización y a Isabel Cristina Zuleta por delitos como daño en bien ajeno, robo e impedir el ejercicio de las funciones públicas.

“Después de la ocupación pacífica ha habido una serie de calumnias sobre el movimiento y sobre mí, el alcalde insiste en  acusarme de motivar la ocupación y dijo que hubo agresiones por parte de los manifestantes. Incluso **el personero, que parece abogado del alcalde y quien no cumple sus obligaciones como funcionario público, ha repetido las calumnias y dijo que no hubo agresión contra el niño por parte del ESMAD**”, cuenta la líder de Ríos Vivos, quien añade que existen videos donde se evidencia que la fuerza pública si agredió al menor de edad.

Además, Isabel Cristina asegura que las personas salieron voluntariamente de la alcaldía debido a que el personero prometió una reunión con la secretaría municipal, pero 30 minutos después dijo que la reunión no sería posible.

Zuleta señala que todos los días el alcalde habla por en la emisora del pueblo señalando de vándalos a los manifestantes, y en cambio no se permite que los líderes de la movilización en Valdivia puedan defenderse  por ese mismo medio.

“El alcalde dice que somos un movimiento violento y que lo hemos agredido, lo que se está haciendo es una campaña por hacernos ver como violentos y no como el movimiento pacífico que somos. **Dice que nos ofrece los vehículos para devolvernos, pero que aquí no nos van a atender nuestras peticiones”** dice Zuleta, quien concluye diciendo que desde Ríos Vivos hacen un llamado a la Cumbre Agraria para tener participación en la mesa nacional.

Lo que si continúa es la represión. Ayer se realizó una marcha sobre la vía Valdivia - Caucasia, donde se podía ver más agentes del Ejército y del ESMAD que manifestantes. También se denuncia que el temor aumenta debido a las **presiones de los grupos paramilitares que están  amenazando a los comerciantes que ayudan con comida a las personas concentradas en Valdivia apoyando la Minga Nacional** y que ahora se encuentran en el Coliseo del municipio.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Isabel-Cristina-Zuleta.mp3"\]\[/audio\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
