Title: Anuncian demanda contra Iván Duque y Carlos Holmes por crímenes de lesa humanidad
Date: 2020-09-17 17:37
Author: PracticasCR
Category: Actualidad, Nacional
Tags: brutalidad policial, Carlos Holmes Trujillo, Corte Penal Internacional, Crímenes de lesa humanidad, Iván Duque
Slug: anuncian-demanda-a-ivan-duque-y-carlos-holmes-por-crimenes-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Por-crimenes-de-lesa-humanidad-denunciaran-a-Ivan-Duque-y-Carls-Holmes-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves el senador Iván Cepeda informó que radicarán ante la Fiscal de la Corte Penal Internacional -[CPI](https://www.hrw.org/es/topic/international-justice/corte-penal-internacional)-, una comunicación sobre **la presunta responsabilidad del presidente Iván Duque y el Ministro de Defensa, Carlos Holmes Trujillo en crímenes de lesa humanidad como masacre y torturas cometidos por la Policía Nacional, institución que está bajo el mando de estas dos figuras.** (Lea también: [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1306620609078669314","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1306620609078669314

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El senador Cepeda señaló que en el Congreso, particularmente en la Comisión Segunda, se escucharon las voces de las víctimas de la brutalidad policial y sostuvo que este actuar es sistemático y se extiende, a mucho antes, de los hechos recientes como el asesinato de Javier Ordóñez y la utilización indiscriminada de armas de fuego en contra de civiles por lo que aseguró que **«*la excusa de las “manzanas podridas” ya no cabe más*».** (Lea también: [Antes del 9 de septiembre se presentaron por lo menos 1.708 denuncias de abuso policial](https://archivo.contagioradio.com/antes-del-9-de-septiembre-se-presentaron-por-lo-menos-1-708-denuncias-de-abuso-policial/)**)**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**«*En menos de 24 horas, 14 personas fueron asesinadas en 9 localidades de Bogotá y otras 218 resultaron heridas, 75 por armas de fuego. Videos captados por ciudadanos demuestran el uso doloso de armas de fuego por la Policía. Se trata de un crimen de lesa humanidad*»**, señaló Cepeda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Cepeda aseguró que el presidente Iván Duque es responsable bien sea por acción o por omisión, al igual que su ministro de Defensa Carlos Holmes Trujillo, sobre quien declaró, que tiene que renunciar al cargo. Cepeda manifestó que acudirán a la Corte Penal Internacional porque **el Estado colombiano ha dejado que reine la impunidad en casos de brutalidad policial, para lo cual se refirió al caso de Dilan Cruz que luego de casi un año, no ha arrojado condenas, ni responsables.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Reformas estructurales a la Policía

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador **señaló que es momento de una reforma estructural que desmilitarice la Policía, regule el uso de la fuerza y forme al personal en derechos humanos.** Frente a esto, anunció que en los próximos días un grupo de congresistas de la oposición presentarán un proyecto de ley frente a la materia. (Le puede interesar: [Antes del 9 de septiembre se presentaron por lo menos 1.708 denuncias de abuso policial](https://archivo.contagioradio.com/antes-del-9-de-septiembre-se-presentaron-por-lo-menos-1-708-denuncias-de-abuso-policial/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1306658444888477697","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1306658444888477697

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otra parte, solicitó a la Procuraduría General de la Nación aplicar el poder preferente para  conocer de forma inmediata las 65 investigaciones que se adelantan en la Policía, advirtiendo que recurrirán a instancias y tribunales internacionales en caso de que las investigaciones se remitan a la Justicia Penal Militar, aspecto que también se espera que sea abordado y modificado en la reforma que se propone. (Lea también: [Cinco normativas que deben cambiarse para frenar delitos de la Policía](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
