Title: Grave situación humanitaria en Tumaco
Date: 2016-12-14 01:18
Category: DDHH, Nacional
Tags: Asesinatos a líderes comunitarios, Comunicado Diócesis de Tumaco, Diócesis de Tumaco, Tumaco
Slug: grave-situacion-humanitaria-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CCAI-Colombia] 

###### [13 Dic 2016] 

La Pastoral Social de la Diócesis de Tumaco denunció a través de un comunicado su preocupación por el registro de **132 asesinatos a líderes y lideresas, ocurridos entre Enero y Noviembre del presente año** y hace un llamado a entes investigadores y al Estado en general para que preste atención a esta crisis humanitaria en Nariño.

La Diócesis manifiesta en la misiva, que según el Observatorio del Delito del Municipio de Tumaco, “en el mes de agosto fueron asesinadas 19 personas, en el mes de septiembre 15 personas, en el mes de octubre 15 personas y en el mes de noviembre 8 personas”.

Además la organización eclesial señala que **“hay patrones que se repiten en los distintos casos denunciados,** secuestros, amenazas, extorsiones y hombres encapuchados en moto que abordan a las personas, las autoridades saben de estos hechos pero **aún no hay investigaciones ni hay culpables”.**

Por último, la diócesis exige a los actores armados cesar los hechos violentos, al gobierno nacional, **celeridad en la implementación de los acuerdos y a las instituciones estatales, protección a los derechos humanos y avances en los esclarecimientos de los 132 asesinatos.**

<iframe id="audio_14996612" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14996612_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
