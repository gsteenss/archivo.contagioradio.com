Title: En Colombia hay reserva moral para defender los derechos alcanzados
Date: 2019-04-04 15:34
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Bruselas, Derechos Humanos, Organizaciones sociales
Slug: colombia-defender-derechos-alcanzados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Organizaciones-en-Bruselas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @asociacionminga] 

###### [4 Mar 2019] 

Más de 30 representantes de organizaciones sociales y políticas, así como defensores de derechos humanos se encuentran en Bruselas, Bélgica, para expresar sus preocupaciones ante la Unión Europea sobre a implementación del Acuerdo de Paz, la situación de derechos humanos en el país y el riesgo que sufren quienes ejercen algún liderazgo social. Sin embargo, el mensaje también estará cargado de esperanza, y ánimo para defender las victorias alcanzadas.

Diana Sánchez, directora de la Asociación Minga y una de las 30 representantes que está en Bruselas, afirmó que en Europa esperaban tener un gran impacto a varios niveles "porque el momento que vive el país es neurálgico". En principio, las organizaciones se refieren al poco avance que ha tenido la implementación del Acuerdo de Paz por parte del gobierno Duque, sin embargo, resaltan una serie de problemas que superan esta situación.

Sánchez explicó que se trata de la regresión misma en términos de derechos que se habían ganado en los últimos 5 años, y que en el marco del Acuerdo de Paz se había logrado proponer una agenda relacionada con derechos humanos, garantías sociales y enfoque de seguridad humana, pero que este Gobierno está poniendo en riesgo. Teniendo en cuenta que la Unión Europea fue una aliada en la búsqueda de la salida dialogada al conflicto armado, las organizaciones esperan continuar con este lazo y fortalecerlo.

### **Es un momento clave para mantener el apoyo de la comunidad internacional** 

Aunque Sánchez sentenció que el Uribismo, y ahora el Centro Democrático, "siempre ha sido alérgico a la comunidad internacional, no solamente regional sino también con países del norte", señaló que no se puede desconocer el contexto internacional, y que las relaciones establecidas con otros Estados no son de carácter coyuntural sino de largo plazo, y tomando en cuenta el aporte de la U.E. a la paz de Colombia, es clave seguir manteniendo esta comunidad como un aliado y un garante de la implementación.

Adicionalmente, Europa ha propiciado la búsqueda de la salida negociada al conflicto con el ELN, y se ha mostrado preocupada sobre las alertas emitidas por el Comité Internacional de la Cruz Roja (CICR) sobre afectaciones contra la población civil, así como el riesgo que sufren quienes defienden los derechos humanos, en ese sentido, esos temas también se expresarán en las visitas.

Sánchez señaló que en el pasado la comunidad internacional había creído que con el proceso de paz todas las violaciones a los DD.HH. habían finalizado, pero indicó que esa idea cesó, y Europa ya ha entendido que "estas situaciones se siguen presentando, y que no se puede regresar al pasado". (Le puede interesar:["Desplazamiento forzado y aumento en la violencia: Las preocupaciones del CICR en Colombia"](https://archivo.contagioradio.com/desplazamiento-forzado-cicr-colombia/))

**Tenemos que mirar de forma positiva y esperanzadora todo lo que viene**

La Activista dijo que en Bruselas plantearán de forma general todas las complicaciones que se presentan en el país, y enfatizarán la instrumentalización que viven las comunidades por parte del Gobierno que las usa para firmar acuerdos que luego son incumplidos, lo que genera una "desistitucionalización y lleva a una inestabilidad en la región".

Sin embargo, Sanchez aseveró que en Colombia se está trabajando para crear alianzas sólidas para trabajar los próximos años en favor de los derechos, y por lo tanto se debe tener la esperanza de que en Colombia hay "reserva moral para frenar la violencia endémica y seguir buscando la salida negociada al conflicto"; por esta razón, tenemos que mirar de forma positiva y esperanzadora todo lo que viene para el futuro.

[Líderes de Colombia visitan Bruselas para hablar sobre la paz en Colombia](https://www.scribd.com/document/404952499/Lideres-de-Colombia-visitan-Bruselas-para-hablar-sobre-la-paz-en-Colombia#from_embed "View Líderes de Colombia visitan Bruselas para hablar sobre la paz en Colombia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_12526" class="scribd_iframe_embed" title="Líderes de Colombia visitan Bruselas para hablar sobre la paz en Colombia" src="https://es.scribd.com/embeds/404952499/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-FHarPTb8TV4Usba09Qvp&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_34097659" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097659_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
