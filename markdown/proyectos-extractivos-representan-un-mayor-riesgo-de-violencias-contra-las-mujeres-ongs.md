Title: Proyectos extractivos representan un mayor riesgo de violencias contra las mujeres: ONG´s
Date: 2020-11-05 17:50
Author: AdminContagio
Category: Actualidad, Nacional
Tags: defensores ambientalistas, extractivismo, Lideresas ambientales, Mujeres y extractivismo, Proyectos extractivos
Slug: proyectos-extractivos-representan-un-mayor-riesgo-de-violencias-contra-las-mujeres-ongs
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Violencia-proyectos-extractivos.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Mujeres-proyectos-extractivos.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un estudio realizado en **Colombia, Perú, Guatemala y Honduras; reveló que** **las mujeres corren un mayor riesgo de sufrir violencias, en zonas donde se instalan proyectos extractivos por parte de empresas que explotan recursos naturales.** (Lea también: [Juana Perea, líder ambientalista es asesinada en Chocó](https://archivo.contagioradio.com/juana-perea-lider-ambientalista-es-asesinada-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe elaborado por las ONG’s Derecho, Ambiente y Recursos Naturales -[DAR](https://dar.org.pe/)- y la Fundación para el Debido Proceso -[DPLF](http://www.dplf.org/es)-; titulado **“*Género e industrias extractivas en América Latina*”** destacó que las actividades extractivas **impactan principalmente a las mujeres indígenas, campesinas y a las defensoras ambientales**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el informe documenta que uno de los mayores peligros que enfrentan las mujeres en zonas en las que se adelantan proyectos extractivos, es **una mayor exposición a las violencias** que pueden provenir de **trabajadores foráneos** de las empresas que se instalan en las comunidades, de los **agentes del Estado** por las relaciones de poder que ejercen sobre los(as) pobladores(as) y en algunos casos hasta de **sus propias parejas** debido al cambio de dinámicas que trae la irrupción de agentes y actividades externos en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El estudio establece riesgos y agresiones de índole física, psicológica y sexual que sufren las mujeres por parte de diversos agentes, entre los cuales se encuentran también **ataques contra mujeres embarazadas, violencia contra sus hijos, y el asesinato o la desaparición de lideresas de las comunidades.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":92301,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Violencia-proyectos-extractivos.png){.wp-image-92301}  

<figcaption>
Fuente: Estudio DAR & DPLF

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Según el estudio, los proyectos extractivos y, en general, los megaproyectos de inversión suelen traer consigo cambios en las relaciones de género,** ahondando muchas veces las brechas de desigualdad entre mujeres y hombres, debido a la alteración de las condiciones sociales, económicas y ambientales de sus comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese al negativo panorama, con base en un análisis del marco normativo y las políticas públicas de los cuatro países estudiados, el informe señala que “*ni los Estados ni las empresas que operan en América Latina y el Caribe han prestado suficiente atención a sus respectivas obligaciones con relación a los derechos de las mujeres*”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El extractivismo en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

El estudio dispuso un acápite especial para delinear el contexto colombiano frente a esta materia, señalando que **durante las últimas dos décadas, Colombia ha impulsado un modelo económico extractivista,** aprovechando el relativo auge del valor de los bienes primarios y esto llevó a que **diferentes gobiernos focalizaran la inversión estatal en el sector minero-energético por sobre otros, como el de la actividad agrícola.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe este modelo económico, aunado a la existencia de un conflicto armado interno en los últimos 60 años, exacerbó los conflictos socioambientales, siendo especialmente afectadas las comunidades campesinas e indígenas que son perseguidas y asesinadas por su liderazgo y defensa del ambiente. (Lea también: [Colombia es el país con más asesinatos de ambientalistas en el mundo](https://archivo.contagioradio.com/colombia-es-el-pais-con-mas-asesinatos-de-ambientalistas-en-el-mundo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, se reseñó en el informe la adopción de marcos legales que incrementaron los conflictos socioambientales, por ejemplo con la expedición del Código de Minas en 2001, “*diseñado para favorecer la inversión privada extranjera, al otorgar títulos de concesión sin mayores consideraciones en materia ambiental*” lo que “*conllevó al incremento de la violencia y graves violaciones de derechos humanos*”.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
