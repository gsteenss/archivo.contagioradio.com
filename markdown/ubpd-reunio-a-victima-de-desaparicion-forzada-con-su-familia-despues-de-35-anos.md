Title: UBPD reunió a víctima de desaparición forzada con su familia después de 35 años
Date: 2020-11-30 16:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Desaparición forzada, UBPD
Slug: ubpd-reunio-a-victima-de-desaparicion-forzada-con-su-familia-despues-de-35-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/UBPD.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/UBPD.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: UBPD

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado domingo, la **directora de la [Unidad de Búsqueda de Personas dadas por Desaparecidas](https://twitter.com/UBPDcolombia)(UBPD), Luz Marina Monzón,** anunció que la institución que hace parte del Sistema Integral de Justicia encontró con vida a una persona que a raóz del conflicto armado en el departamento de Arauca había permanecido desaparecida desde 1985. Para 2019, la UBPD se había reunido con más de 700 familiares en 20 departamentos del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se trata de un hombre que en la actualidad tiene 60 años y que a la edad de 25 tuvo que huir de la finca familiar en el departamento de Arauca tras ser perseguido por hombres armados que buscaban reclutarle en un grupo al margen de la ley. Pese a que logró escapar, su familia fue víctima de desplazamiento forzado y quienes le ayudaron fueron amenazados o asesinados. [(Le puede interesar: UBPD recuperó 24 cuerpos en acción humanitaria en Caldas)](https://archivo.contagioradio.com/ubpd-recupero-24-cuerpos-en-accion-humanitaria-en-caldas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que antes de realizar la solicitud de búsqueda ante la UBPD, la familia no había acudido a ninguna entidad del Estado por temor a que se cumplieran las amenazas que habían recibido en su contra, sin embargo, señalan que acudieron a la Unidad de Búsqueda por **"ser un mecanismo humanitario y extrajudicial".** [(Lea también: La desaparición forzada en la frontera, un drama invisibilizado)](https://archivo.contagioradio.com/la-desaparicion-forzada-en-la-frontera-un-drama-invisibilizado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fue finalmente en enero de 2020 cuando los familiares de la víctima presentaron su solicitud de búsqueda y mediante el registro civil comenzó a recolectarse la información disponible en fuentes oficiales y no oficiales que permitiera no solo determinar que la persona seguía con vida sino determina su identidad y descubrir que vivía en otra región a **"343 kilómetros del lugar en donde fue visto por última vez, en 1985".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El contacto a través de la UBPD se estableció el pasado 31 de julio con el fin de comprobar la voluntad de reencontrarse con sus seres queridos y garantizar un reencuentro dignificante. Según la Unidad, de forma posterior se tomaron muestras decadactilares para corroborar su identificación y confirmar que se trataba de la misma persona.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos reunidos, estoy muy alegre, mi corazón parece que se va a salir de la alegría, no me puedo explicar cómo trabajaron, pero estoy muy agradecida por estar hoy reunido con ellos, mirando a mi hermano cara a cara" expresó la hermana del hombre que había permanecido 35 años desaparecido.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### UBPD sigue trabajando en los territorios

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Cartografía de Desaparición Forzada en Colombia, entre 1958 y 2018 en el departamento de Arauca **se han registrado 1.688 víctimas, lo que corresponde al 27,39% de las personas por cada 100.000 habitantes;** siendo Tame el lugar con el mayor registro de este crimen con 572 personas.[(Le recomendamos leer: Existe un 99,5% de impunidad en casos de desaparición forzada en Colombia)](https://archivo.contagioradio.com/existe-un-995-de-impunidad-en-casos-de-desaparicion-forzada-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Hoy en Arauca una familia vive lo que muchas quisieran vivir y es encontrar con vida a un ser querido, estas acciones humanitarias son un gran aporte, no solo a la construcción de paz, sino que contribuye a la equidad y solidaridad que debemos tener como humanidad frente al dolor que viven miles de familias”, expresó la directora de la Unidad de Búsqueda de Personas dadas por Desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luz Marina Monzón Cifuentes expresó que incluso en medio de la pandemia, continúan las acciones humanitarias pues para la Unidad **"es una prioridad avanzar con la búsqueda porque sabemos que las familias esperan respuestas que les lleve a salir de la incertidumbre”.** [(Lea también: JEP hace entrega de restos de personas desaparecidas en Dabeiba)](https://archivo.contagioradio.com/jep-hace-entrega-restos-de-personas-desaparecidas-en-dabeiba/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
