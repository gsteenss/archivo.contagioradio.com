Title: Comunidades de Hacarí aún no pueden retornar a sus territorios por falta de garantías
Date: 2018-11-08 15:25
Author: AdminContagio
Category: DDHH, Nacional
Tags: ELN, EPL, Hacarí, Norte de Santander
Slug: aumento-de-presencia-de-fuerza-publica-en-hacari-norte-de-santander
Status: published

###### [Foto: ]AscamcatOficial 

###### 08 Nov 2018 

Continúan los desplazamientos masivos en el municipio de Hacarí, Norte de Santander, estos se han venido agudizando producto de las confrontaciones armadas entre las guerrillas del ELN y el EPL. En total serían **más de mil personas las que han salido de sus territorios y no han podido retornar por falta de garantías, según las denuncias de la Asociación Campesina del Catatumbo**.

De acuerdo Aleider Contreras, las veredas más afectadas hasta el momento son Mesitas, Aguablanca y Castrellón, producto de los enfrentamientos que sucedieron hasta el  pasado lunes 5 de noviembre, según el vocero ASCAMCAT no se han registrado más enfrentamientos en cercanías a las comunidades, pero las familias no han podido retornar a sus hogares, **debido a la falta de garantías y seguridad por parte del Estado.**

"La situación de las familias es bastante complicada, no es fácil salir y dejarlo todo botado. Hay personas que solo salieron con lo que tenían puesto debido a los fuertes combates que se registraron" manifestó Contreras. (Le puede interesar:["Aumentar pie de fuerza en el Catatumbo no resolverá crisis humanitaria: ASCAMCAT"](https://archivo.contagioradio.com/aumentar-pie-de-fuerza-el-catatumbo-no-resolvera-crisis-humanitaria-ascamcat/))

Otras de las preocupaciones de las más de 400 familias que se han desplazado forzadamente de sus hogares, tiene que ver con su temor a retornar, debido al aumento de la presencia de Fuerza Pública en las veredas, que de acuerdo con Contreras, **podría significar la continuación de los enfrentamientos.**

### **Hace falta más trabajo desde las instituciones Estatales** 

Frente a la respuesta por parte de las autoridades a esta grave crisis humanitaria, el líder señaló que la Alcaldía municipal de Hacarí ha agotado todas las herramientas para garantizar la protección de las familias y han sido las mismas personas, junto con integrantes de iglesias, quienes se han organizado para ayudar a quienes han llegado desde las diferentes veredas. De igual forma, expresó que hay instituciones estatales como la **gobernación departamental que no se han apersonado de la crisis, dificultado el manejo de la misma.**

La Comisión por la vida, la Reconciliación y la Paz, también aseguró que las soluciones que se le den a esta situación deben tener en cuenta la inversión en salud, educación y otros derechos fundamentales y no la erradicación de cultivos de uso ilícito o la militarización del territorio que vulneran a las comunidades.

<iframe id="audio_29939269" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29939269_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
