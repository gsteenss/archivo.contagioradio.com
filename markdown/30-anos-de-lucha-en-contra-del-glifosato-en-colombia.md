Title: 30 años de lucha en contra del Glifosato en Colombia
Date: 2015-05-15 17:44
Author: CtgAdm
Category: Hablemos alguito
Tags: 30 años de lucha en colombia en contra de Glifosato, Colombia prohíbe el uso del Glifosato, Fumigaciones de Glifosato contra cultivos ilícitos, Glifosato en Colombia, Glifosato y Monsanto, Informe OMS Glifosato es cancerígeno
Slug: 30-anos-de-lucha-en-contra-del-glifosato-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/images.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Pulzo.com 

###### **Entrevista con [Pedro José Arenas], Coordinador observatorio de cultivos de uso ilícito Guaviare**: 

<iframe src="http://www.ivoox.com/player_ek_4501115_2_1.html?data=lZqdk5aVeY6ZmKiakpaJd6Kkkoqgo5WUcYarpJKfj4qbh46kjoqkpZKUcYarpJKgkpDFaaSnhqee0diPqMafzdrQysaPqc%2BfpNTZ0dLGrcKfxNTb1tfFb8bgjKzZy8vTt8Lo0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El **uso de la planta de la coca ha sido la generalidad** en los pueblos originarios de la **cordillera Andina, así como en otros de la Amazonía,** por tanto su **uso medicinal** y terapéutico está avalado por cientos de años de historia.

Es cuando comienza ha procesarse y extraer la **pasta de coca base** cuando comienza a denominarse el cultivo de coca como cultivo de uso ilícito.

De tal manera y siguiendo las directrices de **Naciones Unidas** se convierte en un **cultivo ilegal** de masiva producción en países como **Colombia, en regiones como Putumayo, Guaviare o Cauca.**

A partir de los **años 70** es cuando comienza la **bonanza de la venta de la pasta base** de la coca, pero también es cuando se **inicia la lucha en contra de su cultivo** y la guerra por el control del negocio.

A partir de los **años 90 el Estado Colombiano comienza a usar el plaguicida conocido como el Glifosato,** comercializado por la multinacional Estadounidense **Monsanto** como estrategia para erradicar el cultivo de uso ilícito.

De tal forma al **fumigar las plantaciones** se destruye el cultivo cortando así con la fuente de financiación de la insurgencia. Estableciéndose así una **estrategia contrainsurgente**

Pero para **Pedro José Arenas, coordinador del Observatorio para cultivos ilícitos del departamento del Guaviare**, la cuestión de fondo es más complicada, ya que las **fumigaciones no han terminado con el cultivo**, pero sí han traído todo tipo de problemáticas para la salud de las comunidades afectadas.

Según Arenas, las **fumigaciones han sido una estrategia para controlar los precios del mercado de la pasta de coca,** ya que al cultivarse en grandes cantidades el precio baja, con las fumigaciones se **controla la cantidad de cultivos y así la producción. **

Este no es el único fin de las **fumigaciones, según Arenas, el Estado también las ha utilizado** para afectar los cultivos lícitos de las regiones donde se cultiva la hoja de coca, afectando gravemente a la salud de sus moradores, **desplazándolos así de su territorio, para luego poder utilizar los territorios para el extractivismo** o cultivos como la Palma africana, **destruyendo el modo de vida del campesinado.**

Además ha sido la **excusa perfecta para militarizar las regiones y ejercer un férreo control sobre las poblaciones.**

Ahora la **Organización Mundial de la Salud ha emitido un informe donde confirma que el uso del Glifosato es cancerígeno**. El gobierno **Colombiano** ha sido el **último en el mundo en utilizarlo**. Cientos de **personas han sido afectadas por sus consecuencias** como abortos brotes epidérmicos o cáncer.

El gobierno de **Santos ha tenido que rectificar y prohibir su uso** en todo el país, ante el informe y la presión de las organizaciones sociales de defensa del campesinado y de los DDHH.

Ahora y en medio de los Diálogos de La Habana entre insurgencia y gobierno, queda en el futuro la incertidumbre del problema del cultivo de la coca, la amapola y la marihuana. El **gobierno no tiene ninguna solución y tampoco escucha las propuestas de las comunidades afectadas.** Todo sucede en un mundo donde productores y consumidores sufren de un negocio controlado por mafias y grupos de presión a nivel mundial.
