Title: Ni en Colombia, ni en Venezuela; bancada de la alternativa dice NO a la guerra
Date: 2019-02-12 16:24
Author: AdminContagio
Category: DDHH, Política
Tags: Bancada Alternativa, Gobierno, gobierno colombiano, Venezuela
Slug: ni-en-colombia-ni-en-venezuela-bancada-de-la-alternativa-dice-no-a-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/IMG_20190212_110159264-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 12 Feb 2019

[Congresistas de la bancada alternativa dieron a conocer una carta suscrita por más de 700 firmas de 22 congresistas, 368 organizaciones sociales y por 313 personalidades de diversos sectores del país en la que expresan su rechazo y preocupación ante una eventual intervención de los Estados Unidos en Venezuela apoyada por Colombia, tema que con seguridad será abordado por el presidente Iván Duque y el presidente Donald Trump  en una reunión en Washington que tendrá lugar  el próximo 13 de febrero. ]

Según explicó el senador Iván Cepeda, del Polo Democrático, la carta, hace referencia a seis [hechos que denotan una **"actitud complaciente del Gobierno frente a una eventual acción militar contra el gobierno de Nicolás Maduro"** y que tendrían serias implicaciones para el país y para la región. ]

“Después de 60 años de conflicto armado y de trabajar por la paz en Colombia no vamos a asistir como espectadores pasivos ante esa terrible posibilidad que dejaría un saldo de destrucción muerte y sufrimiento, no solamente en nuestro país hermano sino para también para nuestro pueblo” afirmó el senador.

### **La complacencia del Gobierno ** 

Dentro de los hechos que son mencionados en el documento, destacan las declaraciones del secretario general de la Organización de Estados Americanos, Luis Almagro, quien afirmó que “en cuanto a la opción militar para derrocar a Maduro, no debemos descartar ninguna opción” o lo expresado por el embajador de Colombia en Estados Unidos, Francisco Santos quien  declaró que "debe darse una respuesta colectiva a esta crisis. Pero creemos, y, déjeme ser bastante claro, que todas las opciones deben ser consideradas".

**Otros hechos mencionados en la carta y que han despertado la preocupación de la bancada son la negativa del Gobierno colombiano ante la firma **[**de la declaración del Grupo de Lima**, en el que otros gobiernos de la región plasmaron su rechazo a cualquier acto de violencia en Venezuela , la visita a Cúcuta del jefe del Comando Sur, almirante Craig Faller, con un despliegue militar inusitado en la frontera.  ]

### **"No queremos tener dos guerras"**

Por su parte,  congresistas como Ángela María Robledo y Antonio Sanguino señalaron que son partidarios de soluciones civilizadas como lo han manifestado países del continente como México y Uruguay y el grupo internacional de contacto de la Unión Europea, naciones que **han optado por respaldar unas elecciones democráticas.**

La representante a la Cámara de la Colombia Humana reafirmó que con la carta buscan que Colombia tenga un rol de facilitadora y no sea una "profundizadora de la crisis, tal como ha hecho al convertirse en un factor más de polarización y de radicalización respecto a lo que está ocurriendo en Venezuela".

[Carta Bancada Alternativa p...](https://www.scribd.com/document/399492366/Carta-Bancada-Alternativa-para-el-presidente-Ivan-Duque#from_embed "View Carta Bancada Alternativa para el presidente Iván Duque on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Carta Bancada Alternativa para el presidente Iván Duque" src="https://www.scribd.com/embeds/399492366/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=false&amp;access_key=key-mQboYMjNLVtxc2HpW7Da" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
