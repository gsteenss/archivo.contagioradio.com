Title: Habitantes de Calle exigen se respeten sus derechos
Date: 2016-08-25 16:56
Category: DDHH, Nacional
Tags: abuso de autoridad, Desalojo de habitantes de calle
Slug: habitantes-de-calle-se-encuentran-hacinados-en-la-carrera-30-con-calle-sexta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/habitantes-de.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:caracol.com] 

###### [25 Ago 2016] 

**Más de 400 habitantes de calle continúan confinados en el caño de la carrera 30 con calle 6 en Bogotá,** sin obtener mayor respuesta de la Alcaldía y enfrentándose a diferentes riesgos como otra posible avalancha de agua por las lluvias, las pésimas condiciones de salubridad en las que se encuentran y los abusos por parte de la fuerza pública.

Algunas de las denuncias que los habitantes de calle le han dado a conocer al Padre Gabriel Gutiérrez, miembro de la comunidad Franciscana,** alertan sobre personas heridas,** es el caso de una **mujer en estado de embarazo que perdió a su bebe al ser golpeada por un agente**, un hombre que fue atropellado por la tanqueta del ESMAD en uno de los desalojos y una señora que fue golpeada por un policía en la cabeza y tuvo que ser cosida.

Los habitantes de calle expresaron el miedo que tienen de volver a ser agredidos por las autoridades y de vivir otro desalojo, agregando que[ podría presentarse la mal llamada “limpieza social”](https://archivo.contagioradio.com/se-teme-nueva-ola-de-mal-llamada-limpieza-social-con-habitantes-del-bronx-alirio-uribe/), al no tener conocimiento del paradero de algunos de sus compañeros, panorama que se agrava con la aparición de **panfletos en el barrio Ricaurte, en los que se prohíbe su presencia en el sector**.

La comunidad ha tomado medidas de confinamiento con respaldo de la Policía y han [restringido la movilidad de los habitantes de calle](https://archivo.contagioradio.com/60-millones-por-metro-cuadrado-el-negocio-tras-la-crisis-humanitaria-del-bronx/), además el Padre Gutiérrez informó que las autoridades, en días pasados, estaban **impidiendo el ingreso de alimentos al lugar en donde se encuentran.**

“Con hambre, enfermos, lloviendo, totalmente mojados, están hacinados los habitantes de calles, **esto es una violación profunda en contra de los derechos humanos**, por eso estamos llamando la atención sobre aspectos muy concretos: que se dé la re ubicación y que se declare la emergencia humanitaria” afirmo el Padre Gutiérrez.

<iframe src="http://co.ivoox.com/es/player_ej_12678092_2_1.html?data=kpejmZ2UfZOhhpywj5aWaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncbHW09SYqcbGtsrZzZC019nNaaSnhqam1NfJvoampJCw0dLZssrYwsmYqNfFssTd1MjO0MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
