Title: Se lanza en Buenos Aires Coordinadora por los Derechos de los Pueblos
Date: 2016-04-18 16:09
Category: Otra Mirada, yoreporto
Tags: Argentina, Derechos de los pueblos, presos politicos
Slug: se-lanza-en-buenos-aires-coordinadora-por-los-derechos-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Coordinadora-Americana-por-los-Derechos-de-los-Pueblos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 18 Abr 2016 

#### Por: **[Voz a Vos Comunicaciones- Transmitiendo la Independencia - Yo Reporto]** 

[El pasado Viernes 15 de Abril se desarrolló en Buenos Aires – Argentina el Lanzamiento de la Coordinadora Americana por los Derechos de los Pueblos y las Víctimas de la Prisión política, una articulación de distintas organizaciones de Derechos Humanos a lo largo del continente Americano entre las que se encuentran la Alianza por la Justicia Global de los EE.UU.; la Agrupación de Familiares de Ejecutados Políticos de Chile, la Comisión de Familiares de Detenidos Desaparecidos de Honduras, la Fundación Lazos de Dignidad de Colombia, la Liga Argentina por los Derechos del Hombre, la Liga Mexicana por los Derechos Humanos, y el Observatorio de la Escuela de las Américas con sedes en Chile y EE.UU, convocadas con numerosos objetivos entre los que se encuentran la lucha incansable por la liberación inmediata de todas y todos los prisioneros políticos de la región, el respeto por los derechos de los pueblos que componen la América y la unidad de los mismos con fines emancipatorios y soberanos.]

El lanzamiento además de contar con palabras de las organizaciones que componen esta Coordinadora, contó la presencia de Edith Glaif, una compañera internacionalista de la Liga Argentina por los Derechos del Hombre reconocida en 2013 con la Orden de la Amistad de los Pueblos del Gobierno Cubano por sus más de sesenta años de lucha por la defensa de los derechos humanos y la transformación del continente, quien compartió sus experiencias de militancia y trabajo inicialmente durante la Dictadura Cívico Militar de Pinochet en Chile, y posteriormente en Argentina, antecediendo en la palabra a Alicia Lira, la presidenta de la Agrupación de Ejecutados Políticos de Chile, quien narró sobre aquellos años de dictadura en los que luchar por la transformación del mundo implicaba años de cárcel e inclusive la entrega de la vida y cómo, después de tantos años de finalizado este período, la situación de Derechos Humanos en Chile sigue siendo crítica no sólo respecto militantes de movimientos políticos y sociales en las ciudades, sino también en el caso de militantes Mapuches en el sur del país que han sido asesinados o se encuentran encarcelados por defender la soberanía de sus territorios.

Es de importancia resaltar que el Lanzamiento se realiza en el marco de la situación política y social del continente, donde cobra una especial importancia y vigencia teniendo en cuenta la voluntad del gobierno de Macri de pasar por alto el derecho al refugio político del que hacen uso numerosos luchadores y luchadoras desde hace más de 30 años en el país, la grave situación de Derechos Humanos y salubridad que atraviesan las prisiones en el Continente como en el caso del centro de tortura de La Tramacúa en Colombia, entre otras, y sobre todo, la reacomodación neoliberal en la región donde es fundamental la unidad para lograr la defensa efectiva de los Derechos de nuestros pueblos, haciendo efectiva la solidaridad no sólo con los pueblos hermanos de la América, sino también con luchas como la Palestina, la Saharaui y la de todos los pueblos oprimidos y perseguidos del mundo.

[Página Web de la Coordinadora:][[https://derechosdelospueblos.net/2016/03/25/presentacion/]](https://derechosdelospueblos.net/2016/03/25/presentacion/)

Sobre situación de DDHH en Colombia:

<iframe src="http://co.ivoox.com/es/player_ej_11176741_2_1.html?data=kpaemZubeJKhhpywj5WVaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncbHmytjW0dPJttDnjN6Y0tfNt8rjz8rfw9iPtNDghqigh6aouMrXwtiYx9OPh9Dg0NLPy8aRaZi3jrLO1MjMpYzEwtnfy4qnd4a2lNnWxcaPqc%2BfotfUx9PYrc%2FVjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
