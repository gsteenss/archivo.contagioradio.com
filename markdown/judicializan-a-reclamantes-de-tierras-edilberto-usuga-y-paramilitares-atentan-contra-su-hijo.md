Title: Judicializan a reclamantes de tierras, Edilberto Úsuga y paramilitares atentan contra su hijo
Date: 2019-12-13 17:50
Author: CtgAdm
Category: DDHH, Nacional
Tags: Agresiones contra líderes sociales, despojo de tierras, Urabá Antioqueño
Slug: judicializan-a-reclamantes-de-tierras-edilberto-usuga-y-paramilitares-atentan-contra-su-hijo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/HERIBERTO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Forjando Futuros] 

En horas de la noche del pasado 12 de diciembre, líderes sociales del Urabá denunciaron un nuevo atentado contra restitución de tierras del corregimiento de Macondo, en Turbo, Antioquia. Esta vez, contra Heriberto Úsuga, aunque el campesino logró escapar, teme que cuando se produzca un acercamiento a las autoridades, la Policía lo detenga irregularmente como ha sucedido con un total de nueve líderes de la vereda Guacamayas en el último mes, quienes también hacen parte de procesos de restitución.

Según Julio César León, director de la **Corporación de Líderes Sociales y Víctimas de Colombia**, Heriberto ('Beto', como es conocido en la región) habría sido atacado por hombres armados que pertenecerían al Clan del Golfo,  disparando contra él mientras se encontraba en una de las fincas de la zona en la que se realiza restitución de tierras.  [(Le recomendamos leer: Denuncian captura de ocho líderes reclamantes de tierras en Guacamayas en el Urabá)](https://archivo.contagioradio.com/denuncian-captura-de-ocho-lideres-reclamantes-de-tierras-en-guacamayas-en-el-uraba/)

El líder social explico que Heriberto pudo informar sobre la agresión antes de escapar en medio del tiroteo junto a su primo, hasta internarse en medio de extensas zonas de cultivo de plátano.  Es necesario resaltar que Heriberto es hijo de Edilberto Úsuga, uno de los nueve líderes sociales reclamantes de tierras que fueron detenidos por la Policía el pasado 28 de noviembre, en un caso que para organizaciones sociales  es considerado un falso positivo judicial. [(Le puede interesar: En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras)](https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/)

### No existen garantías judiciales ni de seguridad para los reclamantes de tierras 

No se conoció mayor información sobre el campesino hasta que en horas de la mañana logró establecer comunicación, afirmando que aunque no se encontraban herido, habían caminado toda la noche. Pese a encontrarse a salvo, Heriberto ha señalado que, debido a los antecedentes y capturas contra su padre y líderes reclamantes de la región, teme que al ser rescatado, sea judicializado por la Policía debido a la permisividad, y en ocasiones complicidad, que han denunciado los afectados contra la Fuerza Pública.

Esta no es la primera vez que se presentan atentados contra reclamantes de tierras en Guacamayas, desde 2016 las comunidades han sido confinadas en varios ocasiones por grupos paramilitares que obedecen a intereses de empresarios y buscan desplazar a las familias nuevamente de sus territorios. En días anteriores, la también lideresa Ayineth Pérez, presidenta de Tierra y Paz recibió la amenaza de un carro con hombres armados que la obligó a salir de su hogar.  [ (Le puede interesar: Paramilitares arremeten de nuevo contra reclamantes de Las Guacamayas) ](https://archivo.contagioradio.com/paramilitares-arremeten-nuevo-reclamantes-las-guacamayas/)

Durante la entrega del informe **‘La mejor esquina de América: Territorios Despojados’,** la  lideresa Ayineth denunció ante la Jurisdicción Especial para la Paz (JEP) y la Comisión para el Esclarecimiento de la Verdad (CEV) que en la región se han dado al menos **21 asesinatos contra reclamantes de tierras en la ultima década.** [(Lea también: Comunidades exigen poner fin a la impunidad del despojo de tierras en el Bajo Atrato) ](https://archivo.contagioradio.com/comunidades-exigen-poner-fin-a-la-impunidad-del-despojo-de-tierras-en-el-bajo-atrato/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
