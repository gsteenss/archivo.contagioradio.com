Title: Misión Salud
Date: 2016-09-12 09:38
Author: AdminContagio
Slug: mision-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/mision-salud.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mision-salud.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### MISION SALUD

[](https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/)  

###### [Las licencias obligatorias, una herramienta de política pública que salva vidas](https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/)

[<time datetime="2018-11-10T07:00:26+00:00" title="2018-11-10T07:00:26+00:00">noviembre 10, 2018</time>](https://archivo.contagioradio.com/2018/11/10/)En salud se conoce como licencia obligatoria la autorización que da un gobierno a un particular para producir o importar y comercializar temporalmente versiones genéricas de un medicamento  
[](https://archivo.contagioradio.com/cuando-el-rio-suena/)  

###### [Cuando el río suena…](https://archivo.contagioradio.com/cuando-el-rio-suena/)

[<time datetime="2018-10-01T08:43:14+00:00" title="2018-10-01T08:43:14+00:00">octubre 1, 2018</time>](https://archivo.contagioradio.com/2018/10/01/)Preocupante e inesperado desde la esquina de la salud el anuncio a los medios por el Canciller Trujillo de que habrá una reunión bilateral Colombia-EE.UU
