Title: "Duque le está dando la espalda a las víctimas"
Date: 2019-02-05 16:17
Author: AdminContagio
Category: DDHH, Paz
Tags: Gobierno, Iván Duque, lideres sociales, víctimas
Slug: duque-espalda-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/LIDERES-SOCIALES-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [5 Feb 2019] 

A principios de este mes se realizó la Cumbre Nacional de Víctimas en Bogotá, a la que asistieron más de 1.500 representantes de diferentes organizaciones que corroboraron lo que individualmente sospechaban: que **el Gobierno de Iván Duque le ha dado la espalda a las víctimas**. Esta afirmación se sustenta en 3 pilares que son la implementación del proceso de paz, la aplicación de la Ley 1448 de 2011, y la presentación del Plan de Desarrollo del Gobierno.

En el documento final de la Cumbre se lee que **las víctimas tienen esperanzas en la implementación del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR),** pero los ataques a la Jurisdicción Especial para la Paz (JEP) y la consigna de "hacer trizas" el Acuerdo con las FARC empujan al país a "tiempos de la seguridad democrática". (Le puede interesar: ["V. de La Chinita y FARC avanzan con paso firme hacía la reconciliación"](https://archivo.contagioradio.com/victimas-de-la-chinita-se-reconocen-hoy-como-constructores-de-paz/))

Situación que se suma a la repuesta que el Gobierno ha dado al asesinato sistemático de líderes sociales, la investigación de los homicidios y las acciones tendientes a evitar los mismos, entre ellas, el desmonte del paramilitarismo. Todas estas, situaciones que afectan el derecho de las víctimas a la no repetición de los hechos violentos.  (Le puede interesar: ["Aguilas Negras amenazan a mesas locales de sobrevivientes y defensores de derechos humanos"](https://archivo.contagioradio.com/aguilas-negras-amenazan/))

En segundo lugar, según **Luis Alfonso Castillo, uno de los asistentes a la Cumbre, las organizaciones ven con preocupación la desfinanciación de la Ley 1448 de 2011 o Ley de Víctimas**, circunstancia que se ve reflejada en las pocas reparaciones materiales para quienes han sufrido el conflicto. En ese sentido, Castillo señala debería extenderse la vigencia de la Ley, y modificarla completamente para poder recibir en nuevas fechas de inscripción a personas que se identifican como víctimas, y no pudieron ser reconocidas como tal.

El tercer aspecto, y quizá uno de los argumentos más relevantes para demostrar la posición de Duque con las víctimas tiene que ver con el **Plan de Desarrollo 2018-2022 del Gobierno, en el que no hay financiación para las víctimas**, y se las quiere tratar como un sujeto invisible, y homologarlo como población vulnerable.  (Le puede interesar: ["Mujeres piden no cerrar la puerta del diálogo con el ELN"](https://archivo.contagioradio.com/queremos-la-paz-lideresas-comunitarias-piden-al-gobierno-no-cerrar-puerta-al-dialogo-con-eln/))

### **"El país está siendo empujado por el Gobierno al debacle de la guerra"** 

La preocupación expresada en la Cumbre supera incluso los hechos que los involucran como víctimas, y en la declaración final también expresan la preocupación sobre el **acrecentamiento de un "lenguaje bélico", que incluso ha llegado a pensar que Colombia puede servir como plataforma de lanzamiento para una intervención armada a Venezuela**; discurso que junto al fin de las negociaciones con el ELN hace pensar a Castillo, que "el país está siendo empujado por el Gobierno a la debacle de la guerra".

Por estos argumentos, la Cumbre ha decidido participar en la convocatoria de organizaciones sociales y políticas, en una reunión nacional a desarrollarse los próximos 9 y 10 de febrero, en la que esperan pronunciarse contundentemente contra el asesinato de líderes sociales, así como organizar un gran paro cívico. De igual forma, anunciaron la realización de una movilización nacional el próximo 9 de abril, día de las víctimas del país, para rechazar la forma en que están siendo tratados por parte del Gobierno.

[Declaración Final II Cumbre...](https://www.scribd.com/document/398886113/Declaracion-Final-II-Cumbre-de-Victimas#from_embed "View Declaración Final II Cumbre de Víctimas on Scribd") by on Scribd

###### <iframe class="scribd_iframe_embed" title="Declaración Final II Cumbre de Víctimas" src="https://www.scribd.com/embeds/398886113/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=false&amp;access_key=key-eet8biV59lmKAmV14gvF" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>  
<iframe id="audio_32292642" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32292642_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
