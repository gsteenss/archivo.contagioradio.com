Title: Partidos minoritarios logran acuerdo para impulsar reforma política
Date: 2017-08-01 17:22
Category: Nacional, Política
Tags: campañas electorales, partidos politicos de oposición, partidos politicos minoritarios, Reforma Electoral, Reforma Política
Slug: partidos-minoritarios-buscan-fortalecer-su-incidencia-en-la-contienda-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Votaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [01 Ago 2017] 

Los partidos minoritarios, alternativos y de oposición presentaron **un acuerdo para “impulsar una reforma política que profundice la democracia y ataque la corrupción”.** El acuerdo plantea temas como las coaliciones entre partidos minoritarios, la reestructuración de la financiación de las campañas electorales y la arquitectura institucional.

<iframe src="http://co.ivoox.com/es/player_ek_20137516_2_1.html?data=k5WelZyZdZehhpywj5WbaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8Pgxsnch5enb9Tjw9fSjdHFb8OZpJiSpKbXtdbZxcaYxsqPsNDnjNXO1NnNqNDnjNLW0NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para la Representante a la Cámara, Ángela María Robledo, **los partidos minoritarios están buscando fortalecer su incidencia en el Congreso de la República** en un momento donde el país se alista para recibir a las FARC como nuevo actor político.

Ella manifestó que con el acuerdo buscan priorizar la realización de las coaliciones entre los partidos minoritarios, fortalecer la financiación pública que garantice la participación, un enfoque de Género transversal a la reforma electoral y el fortalecimiento del Consejo Nacional Electoral.

De igual manera, Robledo manifestó que el acuerdo político busca que **sean los mismos partidos los que ataquen las fuentes de corrupción,** “la primera instancia de control son los partidos políticos quienes dan avales y quienes deben hacer seguimiento a las fuentes de financiación y a las alianzas en los territorios”.

Manifestó además que el Consejo Nacional Electoral debe hacer seguimiento al cumplimiento de los avales, las inhabilidades y los topes de financiación electoral. (Le puede interesar: ["Sin Corte Electoral ni listas cerradas la reforma electoral va a Fast Track"](https://archivo.contagioradio.com/sin-corte-electoral-ni-listas-cerradas-la-reforma-electoral-va-a-fast-track/))

### **En Colombia la ciudadanía no conoce el sistema electoral** 

<iframe src="http://co.ivoox.com/es/player_ek_20137777_2_1.html?data=k5WelZybe5ihhpywj5WcaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpDg0cfWqYzZzZDOxdrJtsXjjMnSjdfJqtDmzsaY0tTQaaSnhqax1s7HpYzkwtfOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El Representante a la Cámara, Alirio Uribe, fue enfático en que la reforma política se debe hacer para superar el problema de la poca participación electoral, que según él, es de menos del 50%, y también, **garantizar la transparencia del sistema**. Él manifestó que “hay que rodear el Congreso para exigirle reformas sobre las practicas del sistema electoral, del sistema de partidos y de las campañas, si no lo hacemos no va a haber ningún cambio”.

Igualmente, Uribe fue enfático en que es necesario una reforma a la forma como se financian las campañas y los partidos políticos. **“Este sistema electoral está diseñado para la corrupción** y solo los que tienen plata pueden acceder a la participación política y esto se evidencia en que un líder social no puede acceder al Congreso”. (Le puede interesar: ["Hay sectores que nos les interesa una reforma electoral: MOE"](https://archivo.contagioradio.com/hay-sectores-que-no-les-interesa-reforma-electoral-moe/))

Igualmente manifestó que los **partidos minoritarios han estado en riesgo siempre por la falta de recursos** y es importante que, con la llegada del partido político de las FARC, se garantice que los partidos minoritarios no van a salir de la contienda electoral.

### **Preocupaciones de los partidos minoritarios y de oposición ante el proyecto de acto legislativo** 

En el acuerdo, los partido políticos minoritarios establecieron su preocupación ante temas como **la limitación a pérdida de investidura donde el proyecto de acto legislativo elimina tres causales** y no garantiza las sanciones a los congresistas. Además les preocupa que los congresistas puedan ocupar cargos en el ejecutivo y que el presidente de la República haga parte del Consejo Electoral Colombiano. (Le puede interesar: ["Estas son las recomendaciones de la Misión Electoral para la reforma política"](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/))

Adicionalmente, ven con preocupación que se mantenga la facultad de la Procuraduría para limitar el ejercicio de los derechos políticos pues esto va en contravía de la potestad de los jueces como los únicos actores que pueden limitar los derechos políticos. Finalmente, **ven que hay una contradicción con el Acto legislativo de la reincorporación de las FARC** y el proyecto de acto legislativo. Consideran que no hay una excepcionalidad que garantice la participación de este nuevo actor político.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
