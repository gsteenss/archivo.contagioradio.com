Title: Asesinan a tres personas en Micay, Cauca
Date: 2020-04-22 23:31
Author: CtgAdm
Category: Nacional
Slug: micay-asesinato-tres-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Micay.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Según la información preliminar con la que se cuenta, esta tarde fueron asesinados tres hombres pertenecientes al Consejo Afro Renacer el Micay. Los hechos se habrían presentado hacia las 4 de la tarde en medio de una reunión que organizó la comunidad para establecer medidas de protección pues en días anteriores, integrantes de las disidencias de las FARC habrían amenazado de muerte a estas tres personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la denuncia, confirmada por la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/asesinados-por-disidencia-de-farc-dos-integrantes-de-consejo-comunitario-afrorenacer/), afirman que por amenazas anteriores decidieron realizar una reunión comunitaria hasta la que llegaron disidencias de FARC quienes ordenaron sacar de la reunión a estas tres personas para luego asesinarlas a pocos metros del lugar ubicado en zona limítrofe entre los municipios de Argelia, López y El Tambo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las mujeres que se encontraba en la reunión relató que asesinaron a los tres jóvenes, entre ellos dos identificados como Jesús Albeiro Riascos y Sabino Angulo Advíncula, la comunidad habría rogado por la vida de las personas sin que se haya logrado salvarles la vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidad de Micay no ha podido hacer el levantamiento de los cuerpos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la organización defensora de DDHH las disidencias de FARC que perpetraron el asesinato siguen presentes en la vereda y las personas no han podido hacer el levantamiento de los cuerpos. Adicionalmente no se entiende por qué no hay presencia del ejército en una zona con altos niveles de militarización. [Lea también: En Cauca las comunidades somos el peon de los armados](https://archivo.contagioradio.com/en-cauca-las-comunidades-somos-el-peon-de-los-grupos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas de las personas de la comunidad han afirmado que una de las posibilidades que les queda es el desplazamiento forzado pues está demostrado que la región está azotada por múltiples violencias y hasta el momento no hay una respuesta del Estado, más allá de una militarización insuficiente del territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->
