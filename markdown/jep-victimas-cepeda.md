Title: JEP debería dar el mismo tratamiento a todas las víctimas: Iván Cepeda
Date: 2018-10-24 18:38
Author: AdminContagio
Category: Paz, Política
Tags: ELN, Implementación del Proceso de Paz, Iván Cepeda, JEP, víctimas
Slug: jep-victimas-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Diseño-sin-título-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

Tras las primeras audiencias en las que la **Jurisdicción Especial para la Paz (JEP)**, recibió informes de víctimas de las FARC, el **Senador Iván Cepeda** pidió que se brindara el mismo espacio para las de actores estatales. Adicionalmente, el Congresista se refirió a la implementación de los acuerdos de paz, así como el congelamiento de la mesa de conversaciones con el ELN.

Desde el 22 de octubre, la JEP ha recibido 6 testimonios de víctimas de secuestro cometidos por las FARC, que serán incluidos en el [caso 001](https://archivo.contagioradio.com/citaciones-y-audiencias-nuevo-capitulo-de-jep/); sin embargo, para el Senador del Polo, **estos procedimientos deberían realizarse también con las víctimas de crímenes de Estado, por ejemplo, en el caso del General (r) Mario Montoya,** situación que debería realizarse prontamente, de forma tal que se garantice para todas las personas el acceso al sistema de justicia transicional.

Cepeda sostuvo que en esta etapa de la implementación de los acuerdos de paz, es fundamental **acompañar el funcionamiento de las instituciones del Sistema Integral de Verdad, Justicia, Reparación y No Repetición**, de forma tal que goce de legitimidad. Además, deberían brindarse las garantías a todas las personas participes de la JEP, y puntualizó su crítica al señalar que aún no está listo el sistema de defensa autónomo que garantice a los exguerrilleros una defensa, según el debido proceso.

Sin embargo, el Senador se mostró preocupado ante la situación de los excombatientes de las FARC, quienes aún no se presentan ante la Jurisdicción. Hecho que se suma a la **dificultad en el proceso de reincorporación**, y en la implementación normativa de la paz; por ejemplo, en términos de la **Reforma Rural Integral, o la reforma política** que busca eliminar el voto preferente, y que Cepeda afirmó es una **"contra reforma política"**.

Sobre el proceso de negociación entre Gobierno y el ELN, el congresista que integra la Comisión de Paz del Senado, señaló que **es necesario descongelar la mesa, flexibilizar posiciones, y que el Gobierno permita las conversaciones** para que se pueda avanzar en la agenda con esa guerrilla. (Le puede interesar:["Jalón de orejas a la JEP por parte de víctimas en audiencia de Gral. Montoya"](https://archivo.contagioradio.com/jalon-orejas-jep/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
