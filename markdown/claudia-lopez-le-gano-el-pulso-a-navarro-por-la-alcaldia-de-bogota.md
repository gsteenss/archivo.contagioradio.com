Title: Claudia López le ganó el pulso a Navarro por la Alcaldía de Bogotá
Date: 2019-04-10 21:08
Author: CtgAdm
Category: Nacional
Tags: Claudia López, navarro
Slug: claudia-lopez-le-gano-el-pulso-a-navarro-por-la-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/claudialopez3-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

> Conéctense a nuestra rueda de prensa en donde daremos a conocer los resultados de la encuesta y sabremos quien liderará la candidatura a la Alcaldía de Bogotá por el partido Alianza Verde <https://t.co/YS0phM3UFW>
>
> — Claudia López (@ClaudiaLopez) [10 de abril de 2019](https://twitter.com/ClaudiaLopez/status/1116082980940918784?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En rueda de prensa adelantada en Bogotá, los precandidatos a la alcaldía de Bogotá Claudia López y Antonio Navarro Wolff, anunciaron los resultados de la encuesta que definiría el candidato único por el partido Alianza Verde, anunciando que López será la aspirante por esa colectividad.

La exsenadora del Partido Verde y una de las figuras más importantes de la campaña presidencial de Sergio Fajardo para las pasadas elecciones presidenciales, ganó la encuesta interna en la que se enfrentaba a Antonio Navarro. Según las palabras de la ahora precandidata, la siguiente etapa de la contienda será una consulta con otros partidos como Colombia Humana.

Antonio Navarro, aseguró que como lo ha hecho toda la vida, seguirá trabajando en política y anunció que respaldará la aspiración de López a pesar de haber resultado derrotado en este primer escaño de la carrera a la alcaldía de la capital, el segundo puesto más importante en cargos de elección popular en el país, después del presidente.
