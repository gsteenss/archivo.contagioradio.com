Title: Participación de la sociedad civil será clave en desmonte del paramilitarismo
Date: 2016-06-27 12:28
Category: Nacional, Paz
Tags: Derechos Humanos, FARC, Garantías de seguridad, Jurisdicción Especial de Paz, Paramilitarismo
Slug: participacion-de-la-sociedad-civil-sera-clave-en-desmonte-del-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paramilitares_gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: eluniversal] 

###### [27 Jun 2016]

El acuerdo sobre garantías de seguridad y desmonte del paramilitarismo revelado por las delegaciones de paz del gobierno y las FARC durante este fin de semana contiene puntos claves para integrantes de organizaciones de DDHH y de las víctimas, uno de ellos es la creación de una comisión de alto nivel y otro la **creación de una comisión especial del CTI para investigar y perseguir a esas organizaciones.**

### **Comisión Nacional de Garantías** 

Será presidida por el "presidente de la república y conformada por el Ministro del Interior, Ministro de Defensa, Ministro de Justicia, Fiscal General de la Nación, Defensor del Pueblo, Director de la Unidad Especial de Investigación , el Comandante General de las Fuerzas Militares, el Director General de la Policía Nacional, dos (2) representantes del nuevo movimiento que surja del tránsito de las FARC-EP a la actividad política legal, dos (2) voceros en representación de las plataformas de derechos humanos y paz, y podrá invitar representantes de los partidos y movimientos políticos."

Además podrá invitar a representantes de organizaciones internacionales para realizar las consultas que crean pertinentes. Una de las tareas cruciales de esta comisión será la de **generar políticas públicas** que permitan definir el desmonte de las [estructuras paramilitares](https://archivo.contagioradio.com/?s=paramilitarismo) como política de Estado.

En este sentido, la presencia de organizaciones sociales y en especial de DDHH será fundamental para que las [políticas y mecanismos definidos contengan la expresión de las mayores víctimas del paramilitarismo](https://archivo.contagioradio.com/refrendacion-de-acuerdos-de-paz-queda-en-manos-de-la-corte-constitucional/). Dentro de las posibilidades que se establecen ya suenan algunos nombres de organizaciones como el Movimiento de Víctimas de Crímenes de Estado, MOVICE, la Coordinación Colombia Europa Estados Unidos y la Plataforma DESC, plataformas que agrupan más de 500 organizaciones de víctimas y de DDHH.

### **Unidad Especial de Investigación** 

Una de las características que se resaltan de esta unidad es que tendrá autonomía e independencia del fiscal general de la nación para su funcionamiento, es decir, que se definirá de manera independiente a las políticas generales formuladas por esa institución una vez se defina el nombre del nuevo fiscal que está en proceso de elección y que puede ser Nestor Humberto Martínez.

Según el texto del acuerdo esta Unidad tendrá como objetivo “**la investigación, persecución y acusación de las organizaciones criminales responsables de homicidios, masacres, violencia sistemática de género**, o que atentan contra defensores/as de derechos humanos, movimientos sociales o movimientos políticos, incluyendo las organizaciones criminales que hayan sido denominadas como sucesoras del paramilitarismo y sus redes de apoyo, y la persecución de las conductas criminales que amenacen la implementación de los acuerdos y la construcción de la paz”

Para Jairo Ramírez, el punto crucial para el trabajo de desmonte de esas estructuras es que hay que depurar las fuerzas militares, ya que persiste en su interior una filosofía del enemigo interno que hay que desarticular, puesto que de continuar, genera un efecto contrario al que se expresa en la filosofía de los acuerdos.

[Acuerdo Garantias de Seguridad](https://www.scribd.com/doc/316862210/Acuerdo-Garantias-de-Seguridad "View Acuerdo Garantias de Seguridad on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_87011" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/316862210/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ABATPojHu1T39D9nZ4sV&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12042076_2_1.html?data=kpedlpeUe5ehhpywj5aUaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYrMbNttCfs8bah6iXaaK408rnjZKPhdTZ1NTfh6iXaaK4wpC319eJh5SZoqnRy8jFb7bHsJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
