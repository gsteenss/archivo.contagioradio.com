Title: ¿Adversarios o enemigos?
Date: 2019-02-22 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: ¿Adversarios o enemigos?, lideres sociales, uribismo
Slug: adversarios-o-enemigos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### **19 Dic 2018**

Chantal Mouffe comenta profundas e interesantes ideas sobre la diferenciación entre antagonismo (relación con el enemigo) y agonismo (relación con el adversario). Por un lado, esta entrada no alcanza para analizar con profundidad académica dicho planteamiento, y dejo claro que allí se encuentran serios elementos para controvertir las palabras que aquí consignaré. Pero, por otro lado, creo que sí alcanza el espacio para expresar en algún sentido, lo que habita en el corazón de miles y miles de colombianos.

[Queríamos ser adversarios, pero al adversario no se le mata. Al adversario no se le secuestra por su acción comunitaria y luego se le pega tiros. De la muerte del adversario no puede solo quedar una oportunidad de negocio.  ]

[Quisimos ser adversarios, pero se burlaron de nuestras apreciaciones, a tal punto de patologizar nuestros fundamentos.]

[Quisimos ser adversarios, pero nos equipararon con bestias salvajes, mientras éramos nosotros los que llorábamos los muertos. Allí supimos que había una gran diferencia entre llorar con convicción siendo una bestia salvaje y opinar con conocimiento siendo un respetuoso animal de carga.  ]

[Quisimos ser adversarios, pero hicieron trizas nuestras propuestas, destruyeron argumentos con balas. Trazaron mentiras con la palabra, empalagaron con su inagotable marketing político y perdimos el debate mientras recogíamos las trizas… por si no sabían, con las trizas no logramos armar sino ataúdes. Mientras tenían supuesta certeza de lo que pasaba en otro país, pusieron en duda el exterminio de cientos de personas.]

[Quisimos ser adversarios, pero nuestra filosofía fue tratada con inferioridad, sin equidad, con estigma que hiere, que hace daño, que perjudica, que sindica, que fomenta la incomprensión, la burla intelectual, el desprestigio tecnocrático y el odio vulgar… este último tan peligroso.  ]

[Quisimos ser adversarios porque Europa, luego de demostrar ser la sociedad más violenta del mundo con sus dos guerras mundiales, nos trató de anunciar lo que ocurre cuando una guerra no se termina de raíz. Con toda razón creímos en su criterio, pero lo que se les olvidó a algunos por aquí, fue dejar de creer que en Colombia es como en Europa y lo que hicieron o siguen haciendo, es aplicar métodos, discursos, conceptos “porque así debe ser” mientras no solo Colombia sino nuestra América Latina negada, acallada, se hunde en la más profunda soledad.  ]

[Quisimos ser adversarios, pero no solo nos matan a nuestra gente, sino que no le dicen a la sociedad cuánto tarda una persona en formarse, en cultivarse, en crecer como líder social. No les basta con matar, suman a eso ignominia del trato a los hechos; categorizaron la muerte de nuestra gente, cuestionan nuestras causas y nuestra realidad, cosa que nunca hacen cuando se trata de sus afirmaciones o alaracas moralistas; ¡sean francos! les importó un comino el dolor de aquellos muertos que solo querían ser adversarios.  ]

[Quisimos ser adversarios y fuimos tratados deshonrosamente, muchos no tuvieron el honor de morir como mueren los enemigos, es decir: de frente a sus enemigos, sino que las balas entraron por la espalda.]

[Quisimos ser adversarios, también para empatizar con la gente light que lee un poco de Gramsci y lo acompaña con una copa de Gato Negro. Pero cuando fuimos tratados como enemigos, cuando fuimos perseguidos, creímos que esa gente light se comportaría, aunque como sea amiga, aunque sea como conocida, aunque sea como aparecida, pero no… se comportaron como completos desconocidos.]

[Quisimos ser adversarios para no morir, pero estamos muriendo como los enemigos sin declarar, porque pareciera, que declararse enemigo de la barbarie que gobierna hoy Colombia es un atentando más grave que ser la barbarie.]

[Quisimos ser adversarios, porque nos inculcaron (ahora sabemos quiénes) que si luchábamos por la justicia social con los mismos mecanismos del régimen, entonces seríamos igual o peor que el régimen. Pero menos mal descubrimos que no hay planteamiento lógico más perverso. Es perverso porque nos tapa la boca mientras exige una explicación. Nos ata la mente al conformismo disfrazado de rabia mediática o de ese rejoneo de algunos entre frases y posteos que les alivian la noche y les tranquiliza su conciencia chiquitica. Nos ata porque ser adversario lo confundieron con estar a merced de las acciones de los más poderosos. Ya no podemos ni hablar porque nos dicen violentos, ya no podemos rechazar la barbarie porque nos llaman polarizadores, ya no podemos pedir justicia porque nos ven las lágrimas por la gente asesinada y las confunden con odio. ]

[Sí, perdimos la vanguardia. Nos obligaron observar y callar, porque va y no sea que se ofenda el aire, o de pronto empujemos sin culpa a la lluvia, ojalá y ni miremos a la luna y que del sol ni hablemos. Pero eso sí, nos obligaron a aceptar que nuestros adversarios sí nos vean como enemigos…menos mal ahora sabemos que un gran fuego no se puede apagar con viento.]

[En memoria de los líderes sociales asesinados, podemos decir que quisimos ser adversarios, pero sospechamos que es mejor ser enemigos, para que aunque sea bajo ese título que no es incoherencia, deshonra o locura, logremos fomentar el respeto que nos merecemos.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
