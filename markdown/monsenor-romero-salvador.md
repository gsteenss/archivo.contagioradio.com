Title: Juez ordena reabrir caso de asesinato de Monseñor Romero
Date: 2017-05-18 13:05
Category: El mundo, Otra Mirada
Tags: Amnistia, El Salvador, Moseñor Romero
Slug: monsenor-romero-salvador
Status: published

###### [Foto: Archivo] 

###### [18 May 2017] 

Un juez de instrucción en El Salvador ordenó **reabrir el caso correspondiente al asesinato de Monseñor Oscar Arnulfo Romero** y determinó que la fiscalía se pronuncie frente a la situación del capitán del ejército **Álvaro Rafael Saravia**, **único acusado por el crimen,** quien fue absuelto en 1993 por sobreseimiento.

La determinación del juez **Ricardo Chicas, títular del juzgado Cuarto de Instrucción de San Salvador**, se hace efectiva tras la derogación que se hiciera el año pasado a la Ley de Amnistía que prohibía juzgar a los responsables de crímenes y violaciones de Derechos Humanos cometidos durante la guerra civil que vivió ese país entre 1980 y 1992.

En su solicitud, el juez pide a la fiscalía que se pronuncie respecto a la situación del capitán Saravia asegurando que por haber sido sobreseído, a pesar de las pruebas existentes en su contra, **la ley de amnistía que lo cobija "alude más al olvido de los delitos cometidos antes que al perdón por una responsabilidad penal**"

En marzo de este mismo año, defensores de Derechos Humanos y organizaciones sociales en el Salvador, habían solicitado ante un tribunal de justicia de ese país **[investigar y procesar a los autores materiales e intelectuales del asesinato](https://archivo.contagioradio.com/mosenor-romero-asesinato/) de monseñor Oscar Arnulfo Romero**, ocurrido el 24 de marzo de 1980.

### **Se abre una puerta para el fin de la impunidad** 

**El Estado Salvadoreño había sido condenado por la el tribunal internacional por su responsabilidad  en el crimen**, sentencia que incluyó la orden de re abrir la causa judicial y aplicar acciones de reparación. En marzo de 2010, cuando se cumplieron 30 años del homicidio, el entonces presidente **Mauricio Funes reconoció la responsabilidad estatal y pidió perdón por el magnicidio** que atribuyó en su momento a "la violencia ilegal que perpetró un escuadrón de la muerte".

Sin embargo por la mencionada ley de amnistía, declarada en 1993 por el gobierno derechista de la Alianza Republicana Nacionalista, **los autores materiales del crimen no fueron procesados judicialmente**. Ley que tras ser derogada por la Corte Suprema del país, deja sin impedimentos y excusas la continuidad de la investigación sobre delitos de lesa humanidad y crímenes de guerra ocurridos por cuenta de la represión militar.

###### Reciba toda la información de Contagio Radio en [[su correo]
