Title: MIDBO 18 años viviendo el cine documental
Date: 2016-10-24 09:43
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine en Colombia, MIDBO, Muestra documental
Slug: midbo-18-anos-viviendo-el-cine-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/GH6T4zKQ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MIDBO 

###### 24 Oct 2016 

Bajo el lema "Memorias en movimiento”, este 24 de octubre inicia la 18 edición de la **Muestra Internacional Documental de Bogotá MIDBO**, donde se exhibirán más de **70 documentales nacionales e internacionales provenientes de 15 países**, performances, video instalaciones, talleres, presencia de directores y el s**eminario internacional 'Pensar lo real'**.

El festival, que se consolida como uno de los eventos de cine más importantes de la ciudad, trae una gran selección de documentales en las siguientes categorías: **Muestra Nacional**, compuesta por 8 obras en competencia y 10 en Panorama; **Muestra internacional "Otras miradas"** que presentará 15 obras provenientes de Japón, España, México, Bélgica y Brasil entre otros; **Muestra temática "Espejos para salir de horror**" con 13 obras que mostrarán el conflicto armado y la imaginación del post-conflicto en diferentes y regiones; y la **Muestra Estudiantil "Miradas emergentes"** que en este año seleccionó 19 obras provenientes de Colombia y Latinoamérica.

### **Documental expandido** 

Por tercer año consecutivo, MIDBO incluye **la franja Documental Expandido**; que incluye trabajos de performance, instalaciones y documentales interactivos de autores nacionales e internacionales: La video instalación de **Albertina Carri** *Punto impropio*, que recopila las cartas que la madre de la artista escribió antes de ser asesinada en el tiempo de la dictadura en Argentina; *Transmestizx* de la ecuatoriana **Daniela Moreno** quien presentará una **instalación de realidad virtual que expone las historias de mujeres y generaciones confrontando la memoria**; **Luis Macias y Adriana Vila**, quienes mediante una única presentación mostrarán una serie de fotografías en 35 mm acompañadas de una composición electroacústica que permite ver un entramado de vibraciones ópticas y sonoras; y el trabajo instalativo del colombiano Juan Carlos Arias sobre la **memoria femenina de las víctimas del conflicto armado**.

El invitado de honor internacional para la presente edición será el **Reino de los paises Bajos,** con una selecta muestra de documentales de reconocimiento mundial por la calidad de sus producciones: **Strange love affair with ego, Need for meat y Boudewijn de Grootc- Come closer**.

### **Seminario Internacional Pensar lo real** 

Este e**spacio de académico, que se desarrollará del 24 al 27 de octubre en el Centro Ático de la Universidad Javeriana y la Cinemateca Distrita**l, contará con invitados nacionales e internacionales. El evento será de carácter libre, previa inscripción por medio de la web oficial de la muestra. Y para los niños y niñas que también estén interesadas en núcleo de formación, podrán asistir igualmente con previa inscripción, al taller **"Cine sin cámara", experiencia de animación sobre película cinematográfica en 16mm y 35mm** que el laboratorio de creación español Crater Lab ofrecerá en Casa B los días 29 y 30 de octubre.

La Muestra Internacional Documental de Bogotá 18 **MIDBO es organizada por la Corporación Colombiana de Documentalistas** - ALADOS Colombia y cuenta con el apoyo del Instituto Distrital de las Artes, IDARTES, Alcaldía Mayor de Bogotá, Cinemateca Distrital, Universidad Central, la Embajada del Reino de los países Bajos y el apoyo de Proimágenes Colombia, Ministerio de Cultura, Universidad Javeriana y otras entidades públicas y privadas.

24Cuadros, el espacio dedicado al cine de Contagio Radio, recomienda:

**El botón de Nacar, Patricio Guzmán, Chile, 2015**

<iframe src="https://www.youtube.com/embed/m8kdxpJZEj4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Jueves 27 Octubre 6:30 pm. Auditorio Fundadores, Universidad Central

**Todo Comenzó por el fin, Luis Ospina, Colombia, 2015**

<iframe src="https://www.youtube.com/embed/e3yo00qvSYE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Lunes 24 Octubre 2:30 pm. Auditorio Fundadores, Universidad Central  
Miércoles 26 Octubre 5:00 pm. Cinemateca Distrital

**Pier Paolo,  Miguel Ángel Barroso, España, 2015**

<iframe src="https://www.youtube.com/embed/JIi0_GzcP5M" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Martes 25 Octubre 2:30 pm. Auditorio Fundadores, Universidad Central  
Jueves 27 Octubre 7:00 pm. Cine Tonalá, Sala Kubrick

[Arbol de vida, Edna Higuera Peña Colombia 2015](https://archivo.contagioradio.com/arbol-de-vida-la-lucha-por-salvar-el-patrimonio-de-la-mineria-en-ciudad-bolivar/)

<iframe src="https://www.youtube.com/embed/v1yvtQHhFxw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Martes 25 Octubre 12:00 pm. Biblioteca el Tunal  
Martes 25 Octubre 12:00 pm. Cinemateca Distrital  
Viernes 28 Octubre 4:30 pm. Auditorio Fundadores, Universidad Central\*  
*\*Encuentro con Edna Higuera Peña, directora de la película*

**Quintin Lame, Raíz de los pueblos, Pedro Pablo Tattay Colombia 2015**

<iframe src="https://www.youtube.com/embed/TPJsfnTJ8xw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Martes 25 Octubre 5:00 pm. Cinemateca Distrital\*  
Jueves 27 Octubre 12:00 Pm. Cinemateca Distrital  
\*Encuentro con Pedro Pablo Tattay, director de la película

Para mayor información consulte[ MIDBO](http://www.midbo.co/)
