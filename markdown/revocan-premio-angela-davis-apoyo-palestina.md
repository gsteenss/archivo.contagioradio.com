Title: Revocan premio que entregarían a Angela Davis por su apoyo a Palestina
Date: 2019-01-10 15:54
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: angela davis, BDS, conflicto palestina israel
Slug: revocan-premio-angela-davis-apoyo-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/7aecce143b6231559cf4875f7050d44d-620x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mujeres Riot 

###### 10 Ene 2019 

Tras la presión ejercida por diferentes personalidades y organizaciones en favor de Israel, el Instituto de Derechos Civiles de Birmingham (BCRI), ubicado en el condado de Alabama (EE.UU), decidió **retirar un reconocimiento que iba a ser entregado a la reconocida activista Angela Davis**, aparentemente por su apoyo a la causa Palestina.

La activista exintegrante del Partido de las Panteras Negras, del Comité Coordinador de Estudiantes No Violentos y del Partido Comunista de EEUU, recibiría en febrero próximo el **Premio de Derechos Humanos Fred L. Shuttlesworth**, decisión que fue echada atrás argumentando que Davis no cumplía con todos los criterios para recibir el galardón.

Sin embargo, trascendió que desde el mes de diciembre, **el BCRI venia recibiendo una serie de peticiones por parte de "simpatizantes y otras organizaciones interesadas"** externas y locales, para que reconsideraran su decisión, retractación que diversos activistas  aseguran fue motivada por la posición crítica de Davis contra Israel.

"He dedicado gran parte de mi propio activismo a la solidaridad internacional y, específicamente, a vincular las luchas en otras partes del mundo a las campañas populares de los Estados Unidos contra la violencia policial, el complejo industrial de las prisiones y el racismo en general", señaló la activista de 74 años en una declaración el lunes en la noche.

La determinación que había sido tomada en septiembre de 2018, fue en su momento alabada por la directora del BCRI, **Andrea Taylor, quien declaró sentirse "encantados" por entregar el galardón a Davis**, a la que incluso situaban entre los defensores de derechos humanos más reconocidos a nivel mundial.

Otras personalidades como el alcalde de Birmingham, Randall Woodfin, fueron más concretos al asegurar en declaraciones a AP, que **el cambio de posición del instituto se produjo tras “las protestas de la comunidad judía local y algunos de sus aliados“**, en lo que algunos medios internacionales han titulado como un ataque por parte del lobby israelí.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
