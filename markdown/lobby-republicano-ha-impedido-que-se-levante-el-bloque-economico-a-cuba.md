Title: Este es el impedimento para que se levante el bloqueo económico a Cuba
Date: 2016-03-22 17:31
Category: El mundo, Política
Tags: bloque económico, Cuba, Estados Unidos, Obama
Slug: lobby-republicano-ha-impedido-que-se-levante-el-bloque-economico-a-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Obama-en-Cuba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: M[undo Sputnik News]

<iframe src="http://co.ivoox.com/es/player_ek_10900305_2_1.html?data=kpWmkpWXdJahhpywj5eUaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncbHZ1MqYw9GPaaSnhqam2s7Ys4zk0NHW1s7Hs4zYxpDjy9jNuMKfwpCw18fFaZO3jMrgjcnNqoa3lIqupsjNsIzljoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Felix Albisu, Prensa Libre] 

###### [22 Mar 2016]

**La visita de Barack Obama a Cuba, fue “un éxito”, en términos políticos,** según afirma el director para Colombia de Prensa Latina, Felix Albisu. Sin embargo, ve muy lejano que se logre levantar el bloqueo económico, comercial y financiero, pues el  lobby de un congreso estadounidense que  en su mayoría es republicano, ha impedido que se tumbe esa medida que impide el progreso de Cuba.

“**No se puede esperar mucho más de lo que ha ocurrido, el principal obstáculo es el bloqueo económico,** Obama expresó su voluntad  es seguir luchando para que se levante el bloqueo, pero posiblemente salga del poder y no se haya resuelto”, asegura Albisu, quien añade que todo dependerá del consenso que se logre en el congreso norteamericano, pues no es una decisión que el presidente pueda tomar unilateralmente.

De acuerdo con el periodista, Cuba ha dado algunos pasos para avanzar en la normalización de las relaciones bilaterales con Estados Unidos, como permitir que ahora se puedan  realizar transacciones internacionales a partir del dólar, lo que no significa que la isla haya renunciado a la revolución **“Al contrario Obama se han referido al fracaso que ha representado esta política de bloqueo contra Cuba**”, lo que ha representado una victoria para la resistencia del pueblo cubano.

Por su parte, Felix ve que los isleños se encuentran muy positivos por los avances con Estados Unidos, pues han debido vivir años de limitaciones producidas por el bloque económico. Además ven con agrado y simpatía las palabras de Obama que en su discurso de este martes, sostuvo que "**ha llegado el momento de dejar el pasado, debemos ver el futuro en conjunto, los hijos y los nietos de la revolución y el exilio son fundamentales para el futuro cubano".**

Durante la visita continuó la **estrategia mediática que busca desaparecer la revolución cubana, y que tiene como objetivo, sabotear el proceso de normalización** de relaciones entre Estados Unidos y la isla, según señaló el periodista de Prensa Latina.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
