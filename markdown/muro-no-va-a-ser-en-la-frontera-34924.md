Title: "El muro no va a ser en la frontera sino en México entero"
Date: 2017-01-20 12:06
Category: DDHH, El mundo
Tags: Donald Trump, Estados Unidos, mexico, Muro de Mexico
Slug: muro-no-va-a-ser-en-la-frontera-34924
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Muro-Trump.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telesur ] 

###### [20 Ene. 2017] 

Este viernes Estados Unidos y el mundo está abocado a la **posesión de Donald Trump, actividad que sucede en medio de numerosas movilizaciones en contra del primer mandatario y sus propuestas de Gobierno**. Aunque diversos analistas han restado importancia a las palabras dadas por Trump, los ciudadanos por el contrario las han tomado muy en serio.

Para Francisco Herrera activista y gestor cultural en California, Estados Unidos, luego de conocer el gabinete de Trump “**se ve que las promesas que ha dicho las va a tratar de cumplir, por ello la gente ha decidido movilizarse”** y manifiesta que “existe entre las personas una combinación de mucha esperanza, en el sentido que se están organizando más y a la vez mucha tensión y preocupación por lo que pueda suceder”.

Según Herrera dentro de la sociedad sigue existiendo “vergüenza”, dice **“es un poco surreal, ridículo, que una persona que ha admitido abusar de mujeres, ha calumniado a todo un grupo de personas como latinos y musulmanes y ha amenazado con hacer las mismas cosas que hizo Hitler y que haya podido ganar la presidencia.** Es mandarnos al mundo de los vaqueros y de la ley del más fuerte”.

Diversas organizaciones sociales y de la sociedad civil entre los que se encuentran latinos, musulmanes e integrantes de colectivos de mujeres y LGBTI, han continuado con un sin número de actividades en contra de Trump y sus políticas, además de movilizaciones. Le puede interesar: [La protesta de FEMEN contra Donald Trump](https://archivo.contagioradio.com/femen-protesta-contra-donald-trump/)

“Hay un grupo de mujeres que van a estar haciendo una marcha nacional que tiene como epicentro Washington D. C, en San Francisco también estarán y cerrarán las carreteras. **En Los Ángeles se está intentando tener 1 millón de personas para repudiar la actitud y las promesas del señor “trompas” como le decimos**” asevera Francisco.

Ya diversos sectores sociales y analistas han aseverado que puede ser posible que el nuevo presidente de Estados Unidos, Donald Trump, no alcance a cumplir sus promesas y organizaciones internacionales como **Human Rigths Watch (HRW) ha asegurado que “incluso si el presidente Trump actúa sólo en un diez por ciento de sus propuestas de campaña más problemáticas supondrá un nuevo revés a los Derechos Humanos”. **Le puede interesar: [Ola de movilizaciones en EE.UU rechaza posesión de Donald Trump](https://archivo.contagioradio.com/34791/)

Ante esto Francisco agrega que en cuanto al muro anunciado por Trump eso es una “ridículez” dado que “lo que físicamente se podía hacer de cerco ya se hizo que son unas 1700 millas donde hay posibilidades (…) y es mentira que se va a hacer un muro”.

Según las informaciones entregadas por Francisco de lo que si se podría hablar, sería de “muros virtuales” que es “por ejemplo el dinero que va a lo que es el plan Mérida-Panamá, y están destinados para presionar a México a tomar medidas más fuertes en  contra de los migrantes. **Como quien dice el muro no va a ser en la frontera México-Estados Unidos, es México en sí, porque el dinero va a financiar ese tipo de presión interna en México de bloquear la migración**, por ejemplo”.

Y, lo que sí temen diversos sectores sociales es que todo lo que respecta a inversión social y temas de acceso a la educación y a la salud, sufra fuertes recortes sociales “**los de más bajos recursos serán los más afectados, de tal manera que afecta a millones de personas. Todo eso ha hecho que las personas se continúen organizando**” manifiesta Francisco.

Adicionalmente, dice Francisco que no le temen a que finalmente el gobierno de Trump comience a sacar a la gente de Estados Unidos, sino que “se comience a meter a las personas a las prisiones, porque allí los pueden poner a trabajar por 10 centavos la hora, por ejemplo en Soledad, California la cárcel produce 600 mil dólares al mes de muebles (...). **Lo de la deportación es un circo, lo real es que van a meter a las personas en las cárceles para ponerlos bajo esclavitud”**.

Pese a las represiones que se dan Washington a las movilizaciones, éstas continuarán durante todo el día y en los siguientes, como símbolo de rechazo al nuevo presidente Donald Trump “**va a haber protestas de jóvenes, de docentes, de mujeres, de sindicatos, se tomarán el Puente de la Bahía (…) ya están muy fuertes las manifestaciones en todo el país,** también en señal de rechazo a las reacciones agresivas de las personas que saludan las propuestas de Trump”.

**Galería Movilizaciones en Estados Unidos por la posesión de Donald Trump**

\

<iframe id="audio_16562226" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16562226_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>
