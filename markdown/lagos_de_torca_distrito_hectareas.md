Title: Privados le abren la puerta al proyecto urbanístico Lagos de Torca
Date: 2018-03-08 16:49
Category: Ambiente, Nacional
Tags: Lagos de Torca, María Mercedes Maldonado, Reserva Thomas Van der Hammen
Slug: lagos_de_torca_distrito_hectareas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/547584_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Secretaría de Planeación] 

###### [7 Mar 2018] 

Un total de 66 hectáreas de suelo ya fueron cedidas al distrito por parte de privados que tienen terrenos en el lugar donde se piensa establecer el proyecto de urbanización de Lagos de Torca, que pondrían en grave riesgo la conectividad del borde norte de la ciudad donde se encuentra la Reserva Thomas Van der Hammen.

De acuerdo con María Mercedes Maldonado el problema sería que otros privados empiecen a hacer lo mismo, teniendo en cuenta que la mayoría de los predios de esa zona son privados. Según explica Maldonado, se trata de un privado que **entregó dichas hectáreas al fideicomiso Lagos de Torca**.

"No se trata de un regulo", dice María Mercedes, lo pueden hacer porque los dueños dichos terrenos deben pagar un impuesto predial, y cuando están para proyectos urbanísticos se vuelve muy costoso, por tanto resulta beneficioso ceder esos predios. "La sesión es una contraprestación por la posibilidad de construir**. El privado recibe unos metros cuadrados que podrá construir en otras zonas",** indica.

### **¿En qué va el proyecto?** 

Cabe resaltar que la acción de incumplimiento no detiene la ejecución de todo el plan. El distrito puede seguir con la implementación del plan de urbanización. Asimismo, es de recordar que quienes defienden la reserva, interpusieron la acción de cumplimiento para que solo ajuste 86 hectáreas de l**as que contempla el proyecto 1800, pues esas 86 hectáreas hacen parte de la porción de terreno que conecta la reserva con los cerros.**

También, señala Maldonado que en Lagos de Torca aún no se ha empezado ningún tipo de obras, ya que es necesario que exista un volumen de recursos específico con el que aún no se cuenta, sumado a que también se debe tener una proporción de tierra, con lo cual ya se ha avanzado poco con las 66 hectáreas cedidas.

La cesión del predio se firmó el pasado 25 de enero. Pese a que el Distrito no especificó para que serán destinados dichas hectáreas, cabe mencionar que con el proyecto se planea la construcción de 128.000 viviendas, una sede del Hospital Simón Bolívar, centros comerciales, seis nuevas troncales de Transmilenio, la ampliación de la avenida Boyacá, la carrera Novena y autopista Norte, y la construcción de cuatro nuevas avenidas en las calles 200, 209, 222 y 235.

<iframe id="audio_24295585" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24295585_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
