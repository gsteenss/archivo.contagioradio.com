Title: Boletin Informativo Mayo 25
Date: 2015-05-25 17:09
Category: datos
Tags: Actualidad informativa, Amenazas Universidad Nacional, Ciclo 37, contagio radio, Fosas comunes en Malasia, Noticias del día, Primer ministro China, Proceso de paz en Colombia, Proyecto de Ley 087, Río Magdalena, Victoria izquierda en España
Slug: boletin-informativo-mayo-25
Status: published

*Noticias del Día: *

<iframe src="http://www.ivoox.com/player_ek_4547986_2_1.html?data=lZqhmZ6ceo6ZmKiakpqJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5qah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Mañana martes 26 de mayo se debatirá en plenaria de Cámara de Representantes **el Proyecto de Ley 087 de protección animal**, con autoría del representante a la Cámara **Juan Carlos Losada en compañía de diversas organizaciones animalistas**. El proyecto tiene el tercer lugar en la agenda legislativa. Habla **Natalia Parra, Secretaria de la bancada animalista del congreso de la República**.

-**Rusia emite una ley que permite vetar a ONG's** consideradas por el gobierno como "indeseables". **La medida ha recibido fuertes críticas** de las oposición y de la comunidad internacional, así como de ONG's y organizaciones sociales de DDHH.

-De **los panfletos electrónicos firmados por las Águilas Negras** en que se amenazaba a estudiantes y profesores de universidades públicas, imponiendo una fecha límite para que no regresaran a las aulas; **se pasó a las amenazas directas y personales, cerca al lugar de residencia de los jóvenes**. Habla la estudiante **Itayosara Rojas**.

-**Malasia ha descubierto más de 30 fosas comunes** en las que habrían al menos **139 cuerpos de inmigrantes ilegales desaparecidos,** víctimas de la trata de personas. También se han encontrado **17 campamentos donde eran recluidos los inmigrantes** por las mafias ilegales que controlan el tráfico humano en la zona.

-Diversas **organizaciones ambientalistas colombianas** que trabajan en torno a la **problemática del Río Magdalena**, enviaron una carta al **Primer Ministro de la República Popular de China Li Keqiang.** **Tatiana Roa, Coordinadora general de Censat agua Viva** habla sobre el objetivo de la misiva.

-**Victoria de la izquierda en España dependerá de pactos post electorales**. Según los diputados, el trabajo de las izquierdas en contra de la corrupción y a favor de los más perjudicados por los recortes sociales ha permitido que gran parte de la ciudadanía pudiese participar a través de las **Candidaturas de Unidad Popular** en las elecciones. Habla **Jordi Sebastiá de compromís del País Valenciano.**

-**El proceso de conversaciones de paz** que se reanudó hoy en la Habana en su **ciclo 37,** atraviesa **uno de los peores momentos de estos 3 años** de desarrollo. En un mensaje de esperanza, la guerrilla de las **FARC** afirma que no se van a perder estos años de trabajo. **Alejo Vargas, director del Centro de Pensamiento de la Universidad Nacional, **afirma que es la hora de sumarle apoyos al proceso.
