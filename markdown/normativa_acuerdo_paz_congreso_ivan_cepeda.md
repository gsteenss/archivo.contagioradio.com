Title: Vía decretos debería continuar trámite de normativa del acuerdo de paz: Iván Cepeda
Date: 2018-03-13 16:08
Category: Nacional, Paz
Tags: acuerdo de paz, Aida Avella, Congreso de la República, Iván Cepeda
Slug: normativa_acuerdo_paz_congreso_ivan_cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOnqVVdWAAAlr5G-e1510770947262.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ojo a la Paz] 

###### [13 Mar 2018] 

La implementación del acuerdo de paz, es hoy uno de los temas inmediatos. No obstante, el congreso actual, parece no tener mucha voluntad política para lograrlo, y por eso, el **senador Iván Cepeda, reelegido como congresista para el periodo 2018 - 2022,** asegura que la mejor forma de sacar adelante diferentes proyectos que le apuntan al acuerdo firmado en La Habana sería vía decreto presidencial, teniendo en cuenta el panorama que dejan las recientes lecciones parlamentarias.

<iframe src="https://co.ivoox.com/es/player_ek_24406217_2_1.html?data=k5mhkpuWdZihhpywj5aXaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncarqhqigh6aVsoy3xtXSxsaJdqSf1NTP1MqPsMLnjMbZ1srWssLoytvO1ZDVucafycbmjdXFtsKfytLdzsqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Así lo ve el senador respecto a temas puntuales como sobre el funcionamiento de la JEP, el proyecto de desmonte de las bandas posdesmovilización del paramilitarismo, la circunscripciones especiales de paz, además de otros proyectos del acuerdo en materia social y económica, que no se han tramitado y que son esenciales para la implementación normativa del acuerdo de paz.

"Hay una preocupación seria por los meses que quedan porque es una realidad política que se han fragmentado totalmente los sectores del congreso que apoyaban la paz. **Veo con desconfianza la posibilidad de que se legisle sobre los aspectos procedimentales de la JEP,  por eso creo que el camino debe ser que los magistrados de la JEP desarrollen su propio reglamento",** señala Cepeda con respecto a uno de los temas ya nombrados.

De igual forma sostiene que de esos temas  necesitan una rápida implementación, y de entrar a ser discutidos en el congreso, no sería acelerado su trámite y en cambio estarían en grave riesgo si se dejan en manos del nuevo congreso.

Dichos proyectos "no deberían ser tramitados por el congreso, es exponer a cualquier iniciativa a los vaivenes de la política electoral. Deberían (tramitarse) bajo una decreto o decretos ejecutivos que permitan una rápida implementación", dice el congresista electo.

### **El trabajo en el congreso de las fuerzas alternativas** 

El senador Iván Cepeda asegura que su victoria es "agridulce", ya que su fórmula a la cámara y compañero de luchas en el Congreso, Alirio Uribe, no logró los votos suficientes para mantener  su curul **"La ausencia de Alirio se va a sentir. Ha hecho un gran trabajo en la defensa de los derechos humanos"**, dice.

No obstante, reconoce que con estas elecciones se han logrado fortalecer otras fuerzas políticas en favor de la paz, la defensa de las comunidades, y el ambiente, entre otros temas. "**Hay una nueva configuración del congreso que ya muestra unas bancadas independientes de centro izquierda que van a hacerse sentir en el congreso**, fuerzas que no están en el tradicionalismo político".

Y agrega que "el  trabajo que tenemos ahora tiene varias dimensiones, se deben organizar y coordinar las fuerzas alternativas en el senado", expresa el congresista. Así también lo manifestado la senadora electa, Aida Avella, que con más de 50 mil votos obtuvo una curul por la lista de la Decencia, quien resalta que tras el 20 de julio el congreso podría contar con una **bancada alternativa de unos 25 parlamentarios que deberán legislar por la paz,** pero también por "los trabajadores enfermos que los retiran de las empresas sin ninguna liquidación, la vinculación laboral de las mujeres, y la corrupción".

<iframe src="https://co.ivoox.com/es/player_ek_24406257_2_1.html?data=k5mhkpuWeZihhpywj5aZaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaLdxcaYo9vJsM3VjNjcxNfJb9TpjMrZx8jHrYa3lIqvldOPp9Dh0JDQ0dPLtsbnytjhw5DUs9OfzcaYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
