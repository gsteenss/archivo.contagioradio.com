Title: Demandan a Brigadier General Pinto Lizarazo por ejecuciones extrajudiciales
Date: 2018-02-09 14:38
Category: DDHH, Nacional
Tags: ejecuciones extrajudiciale, falsos positivos, Huila
Slug: demandan-a-brigadier-general-pinto-lizarazo-por-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [09 Frb 2018] 

Víctimas interponen denuncia penal contra el **Brigadier General Pinto Lizarazo por su responsabilidad en 21 casos documentados de homicidios de civiles**, mal llamados “falsos positivos”, que se registraron entre el 2007 al 2008, cuando estaba a cargo del Batallón de Infantería No.10 Atanasio Girardot, adscrito a la Cuarta Brigada, y el Batallón 27 Magdalena, adscrito a la Novena Brigada.

Actualmente Pinto Lizarazo es el director del Centro de Estudios Militares, CEMIL, ascenso que fue duramente cuestionado por la organización defensora de derechos humanos Human Rights Watch. En un informe, esta organización evidenció que la Novena Brigada estuvo relacionada con al menos **119 casos de ejecuciones extrajudiciales, entre el 2004 al 2008, mientras que la Cuarta Brigada estuvo relacionada en 412 casos** entre 2002 al 2008.

### **Las víctimas reclaman justicia en la cadena de mando** 

Eyre Urquina, es la hermana de **Ever Urquina, asesinado en el 2007 a manos del Batallón 27 Magdalena**. Por este hecho hay 5 personas condenadas de ser los autores materiales y hay sanciones disciplinarias sobre 12 de ellos desde el 2015. Para ella, la demanda busca que la Fiscalía abra investigaciones a los altos mandos en este proceso.

“En el caso del General Pinto, él era el comandante del Batallón Magdalena cuando pasó lo de mi hermano, y como comandante debió haber tomado las medidas preventivas para que estos hechos de sangre no ocurrieran” afirmó Eire y agregó “**el general era el que dirigía la tropa y sabía de estas acciones por qué no actuó** y si es ascendido como Brigadier General”. (Le puede interesar: ["Mi hijo para mí es un gran héroe: María Sanabria, una de las madres de Soacha")](https://archivo.contagioradio.com/maria_sanabria_madres_soacha_falsos_positivos/)

Según el Colectivo de Abogados José Alvear Restrepo, este mismo Batallón también ha sido responsabilizado por los crímenes de **Juan Perdomo Claros y Alber Augusto Lizcano, dos habitantes de calle**. En este caso se encuentran condenados 9 autores materiales y el comandante de operaciones del Batallón 27 Magdalena, Francisco Adrían Álvarez.

### **El avance de la justicia en los casos de Falsos Positivos** 

Eyre Urquina manifestó que el proceso desde el asesinato de su hermano afectó la vida de todos sus familiares y que el proceso de reparación continúa, “en cada audiencia se revive el dolor y no hay reparación de ninguna clase que pueda reparar el dolor, solo la justicia”.

De acuerdo con las cifras del Ccajar, en Colombia hay **5.700 denuncias por casos de ejecuciones extrajudiciales, de las cuales 3.430 están en investigación**. Así mismo el Colectivo señaló que hay 903 condenas contra los autores materiales, en el eslabón más bajo de la cadena de mando. (Le puede interesar: ["5 Divisiones y 10 Brigadas en la mira de la Corte Penal por Falsos Positivos"](https://archivo.contagioradio.com/cinco-divisiones-y-diez-brigadas-en-la-mira-de-la-cpi-por-los-llamados-falsos-positivos/))

<iframe id="audio_23661049" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23661049_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
