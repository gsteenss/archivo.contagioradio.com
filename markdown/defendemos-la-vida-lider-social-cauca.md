Title: Somos más los que defendemos la vida: Líder social del Cauca
Date: 2018-07-26 13:31
Category: DDHH, Nacional
Tags: Cauca, CRIC, lideres sociales, ONIC
Slug: defendemos-la-vida-lider-social-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/lideres1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Jul 2018] 

El Consejo Regional Indígena del Cauca (CRIC) y las Mesas de Víctimas del departamento, en rueda de prensa expresaron su preocupación por la situación de amenazas y zozobra permanente que viven los líderes y defensores de Derechos Humanos en la región, pues según el último reporte de la Defensoría del Pueblo, **entre enero de 2016 y junio del 2018, han asesinado a 78 líderes sociales.**

La Mesa Departamental de Víctimas del Cauca aclaró que, aunque no hay combates, si hay sistematicidad en el asesinato y las amenazas contra defensores de Derechos Humanos, pues **desde 2016 han "recibido 33 panfletos amenazantes y hay 500 denuncias de amenazas individuales"**, lo que para ellos demuestra además la incapacidad del Estado para garantizar la vida.

De igual forma, la Mesa Municipal de Víctimas de Popayán recordó que **no hay una política pública clara en relación con la protección de Defensores de Derechos Humanos,** y pidió que se profundicen los derechos de las víctimas y sobrevivientes, así como que se asegure su participación en el Sistema de Verdad, Justicia, Reparación y Garantías de No Repetición.

De otra parte, Yordi Yunda, consejero del CRIC, aseguró que a pesar de las amenazas, los pueblos indígenas mantienen su empeño por construir paz en el país. (Le puede interesar: ["Bajo constante amenaza viven líderes sociales en Cauca"](https://archivo.contagioradio.com/bajo-amenaza-viven-lideres-en-cauca/))

### **¿Qué pasa con la implementación del acuerdo de paz?**

Aida Quilcué, Consejera de Derechos Humanos y Paz de la ONIC, afirmó que **la implementación de los acuerdos permitiría trascender en el camino de la paz,** razón por la que piden al nuevo Gobierno que se "implementen los acuerdos y no que se hagan trizas".  (Le puede interesar: ["Dos líderes sociales asesinados durante el 20 de julio"](https://archivo.contagioradio.com/lideres-asesinados-el-20-de-julio/))

Uno de los énfasis en las exigencias de las mesas departamentales y municipales de víctimas, es iniciar el trabajo de la Unidad de Búsqueda de Personas Dadas por Desaparecidas en la zona, y que se planteen estrategias de políticas públicas concertadas por las comunidades, para proteger a los defensores de Derechos Humanos.

### **La única respuesta del Estado es la militarización de los territorios**

Uno de los puntos de acuerdo entre las Mesas de Víctimas y los representantes de los Pueblos Indígenas es **que la respuesta del Estado ante la crisis humanitaria no sea la militarización de los territorios,** dado que la situación requiere que se investigue a los autores intelectuales y materiales de los asesinatos, y que se permita el acompañamiento internacional que cuide a los procesos sociales.

Otro punto en común con el que se buscó concluir el evento fue el compromiso con la construcción de la paz, y la persistencia en la defensa de los Derechos Humanos; fue así que un líder del Cauca manifestó que **"veremos si somos más los que defendemos la vida, o los que proclaman la muerte".** (Le puede interesar: ["Con militarización no se soluciona la crisis que afronta el Cauca"](https://archivo.contagioradio.com/el-cauca-no-necesita-militarizacion-necesita-planes-de-sustitucion-indepaz/))

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
