Title: La última piedra en el zapato
Date: 2019-03-01 10:29
Category: Uncategorized
Tags: Caracol Noticias, caracol radio, Darío Arizmendi, Medios de información, redes sociales
Slug: la-ultima-piedra-en-el-zapato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/piedra-en-el-zapato-medios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### **1 Mar  2019** 

[Ya no les queda de otra. Lo intentaron todo. Intentaron mantener su punto de vista hegemónico, pero no lograron convertir su versión de la realidad en la verdad. Pretendieron la validez que les otorga el poder de frecuencia, el pago de los patrocinadores, pero ni así, con esa validez altamente asimétrica lograron constituirse en lo que tanto desean: ser “la verdad”.]

[Los medios masivos de información mantienen la hegemonía en el discurso, pero la duda que despierta el desbalance de la manifestación en redes sociales les procura un grado de desespero que pretenden cubrir con alcurnias arribistas basadas en la retórica, que es válida, pero ni así les otorga la verdad. Primero critican al político que no les sirve, pero luego señalan de criticones a los que rechazan al político que sí les sirve. Se molestan cuando los llaman parcializados, pero a Arizmendi solo le falta lanzar él mismo las bombas sobre el palacio de Miraflores, tanto como a Morales solo le resta darle una paliza desenfrenada a Petro. La lógica de los medios de información es perversa, pero es. Y mientras ellos son, nosotros estamos; asimilando cómo las redes sociales, que no son el oro, pero tampoco el bronce, se vienen constituyendo en la última piedra en el zapato que no les ha dejado poner el punto final en la narración de la realidad.  ]

[¿Por qué las redes sociales son la piedra en el zapato de los medios informativos más poderosos? Porque no les pueden marcar la línea editorial. Porque son, nos incomode o no, un campo de lucha simbólica, efímera, constante y sobre todo política. Los grandes medios sacan una noticia y al rato tienen comentarios que les desmienten, que los desvirtúan, que los contradicen. Sustentan con un video y se les responde con otro. Acuden a la pretensión de validez a partir de su experiencia como informadores del país, y resulta que en las redes sociales su tradición no logra tener espacio.]

[¿Qué les queda? Censurar. Por supuesto, en medio de todo su cacareo por la libertad de expresión, los gurús del periodismo no pueden simplemente abogar por censura abiertamente; esas vulgaridades de “plomo es lo que viene” o “te peleamos” sabemos que son producto final inconsciente de las líneas editoriales que implementan dichos “periodistas” y que se manifiestan en esas mentes que no hacen más sino escucharlos y verlos. No obstante, desde los medios de información, esos gurús del periodismo, amigos de golf y copas, cuñado y socios no pueden darse el lujo de solicitar la censura.]

[¿Cómo lo van a hacer? la estrategia tiene un grado de complejidad más alto, para nada brillante, pero sí es más alto. Para nadie es un secreto que los grandes medios de información trabajan en llave con los grandes grupos de poder político y económico del país. Hasta allí, el agua moja. Pero, la no famosa “Ley Lleras”, el proyecto de “Ley TIC”, el fracaso de la defensa mediática al fiscal Martínez, la lucha con imágenes de Venezuela vs imágenes del Chocó, los niños comiendo basura en Colombia vs los venezolanos comiendo basura en Colombia, las hipérboles de las embarradas de Duque o Peñalosa vs las hipérboles de las embarradas de Maduro; simbolizan un problema tácito para poder político dominante en Colombia:]**aún no tienen el control de las comunicaciones.**

[A lo que responderán introduciendo la sospecha de un mal, de una falla, de un peligro; más pronto que tarde insertarán el miedo a las redes sociales, “miedo” por no ser fiables. ¡Claro! las redes sociales contienen un alto grado de duda, pero a la par un grado de alternatividad, un grado anti hegemónico, que por ejemplo permitió últimamente atender con indignación el tema del río Cauca. ¿Si no hubieran publicado videos desde un celular, acaso los Bogotanos se hubieran enterado masivamente del desastre que cometió la EPM? Puedo asegurar, a priori, que, para no manchar la imagen de Uribe, para no manchar el prestigio de la EPM, para no interrumpir a Fajardo en su celebración del día internacional del gato, lo hubieran tapado todo con la compota de Venezuela.  ]

[No pueden abogar por la censura sin antes generar miedo a partir de configurar a las redes sociales como supersticiosas. No pueden abogar por la censura sin antes cambiar la palabra “censura” por “]*[castigar excesos en las redes]*[” o por “]*[regular uso de redes]*[” como ya lo están proponiendo a través de encuestas Caracol Radio y Radio Duque (perdón señor cuñado del presidente número 2) Blue Radio. Lo constituirán como algo “necesario”, instrumentalizarán el discurso para que no se vea opuesto a la libertad de expresión así en sus almuerzos sigan pensando “¡nos tienen jodidos hagamos algo!”]

[Encuesta tras encuesta, luego vendrá la invitación de los ministros a los programas de radio, luego la llamada a algún experto (que esté a favor), luego por variar lo mezclarán con un tema sobre Venezuela, y así así, dejarán todo listo para que se penalice una expresión política contraria a su hegemonía. Se lavarán las manos como siempre, harán luchar a muchos por su propia esclavitud, harán que les rodeen por ser los únicos “con experiencia” para mantenerlos informados, harán que los rodeen llenos de miedo; mientras,]**quizá más temprano que tarde no podamos publicar contenido político en ninguna parte sin su permiso.**

[No sé cuándo ocurrirá, pero, con esas encuesticas que ya propagan, todo indica que hacia allí va la cosa. El problema para ellos es que al igual que cuando se intenta controlar un mercado surge un mercado negro, se puede sospechar que la información ya no podrán controlarla. Solo les quedará propagar el totalitarismo en la cultura como ya lo intentan con la reinterpretación de la historia a través de la ideologización del CNMH, El Archivo Nacional y la Biblioteca Nacional. Y aun así, tendrán que apretarnos más, porque no han sido capaces de callarnos ni con medio millar de asesinados, porque cuando se saquen la piedra del zapato y crean haber logrado su anhelado control comunicativo, probablemente surja lo que llamaron alguna vez la “guerrilla comunicativa” que no les permitirá jamás, jamás ¡jamás! poner el punto final en la narración sobre la realidad.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
