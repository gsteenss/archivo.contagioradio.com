Title: Mensajes de Papa Francisco en Ecuador tienen alto contenido político
Date: 2015-07-07 18:33
Category: Otra Mirada, Política
Tags: Papa Francisco, Rafael Correa, visita ecuador
Slug: mensajes-de-papa-francisco-en-ecuador-tienen-alto-contenido-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Papa-francisco-Ecuador-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elnuevoherald.com 

###### <iframe src="http://www.ivoox.com/player_ek_4733687_2_1.html?data=lZyglZuce46ZmKiak5aJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLkwpCz1MbSp8rnxNSY2M7XrdXVjKrQ18bIs9OijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabelo Cortés ALER] 

###### [07 Jul 2015]

Continua la agenda correspondiente a la visita papal a Ecuador, primera parada de la gira en la que visitará a Bolivia y Paraguay, que inicio este lunes en Guayaquil donde cerca de un millón doscientas mil personas se congregaron para recibir al máximo jerarca de la iglesia católica.

Las actividades programadas para este martes en Quito, iniciaron en el parque Bicentenario, antiguo aeropuerto de la ciudad, con asistencia masiva de ecuatorianos, ecuatorianas y visitantes provenientes de otros países de Latinoamérica y el mundo. Ante la cantidad de asistentes, el sumo pontífice realizó un recorrido por la periferia del lugar con el propósito de saludar a quienes no lograron ingresar.

Durante su estadía en el vecino país, el Papa Francisco ha enviado mensajes que, de acuerdo con Isabelo Cortés periodista de ALER, coinciden con posturas asumidas desde el gobierno en particular las relacionadas con la **inequidad y desigualdad en la distribución de la riqueza**, y se recogen en las dos últimas propuestas de ley presentadas por el gobierno.

Cortés asocia el planteamiento de Correa con el del Papa Francisco, en que los ricos deben aportar a la sociedad, en particular a los sectores que han estado marginados, aquellos que no han tenido la posibilidad de mejorar su condición de vida, y a ello van las leyes de plusvalía y herencia presentadas por el mandatario al Congreso Nacional Ecuatoriano, generando diferentes reacciones por parte de los grupos de poder.

El comunicador asegura que **el mensaje ha calado bien en la sociedad ecuatoriana, al ser una realidad de nuestros países en los que no existe distribución de la riqueza**, en la que los gobiernos y la iglesia tienen papeles trascendentales, situación expresada en la enciclica papal donde expone la amenaza del consumismo y acusa sin rodeos a la política, a la tecnología y a las finanzas de depredar el medio ambiente y generar pobreza.

La visita coincide con una delicada situación política que se presenta en el país, un "*oportunismo político por parte de grupos del poder económico, mediático y la derecha política*" quienes, a través de sus máximos dirigentes, han tomado como bandera los discursos del Papa, en lo referente a la inclusión necesaria para el diálogo integral donde se necesitan "todas las voces".

Por último, el periodista asegura que **la dimensión real y los efectos de la visita del Papa, se verá con lo que ocurra tras su partida rumbo a Bolivia**, ante los diferentes anuncios, antes y durante su estadía, de protestas en las calles, por parte de algunos grupos de inconformistas que se movilizan en contra de las acciones del gobierno.
