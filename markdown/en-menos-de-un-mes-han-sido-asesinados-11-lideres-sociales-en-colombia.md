Title: En menos de un mes han sido asesinados 11 líderes sociales en Colombia
Date: 2016-09-13 13:12
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Asesinatos defensores DDHH, Cese al fuego bilateral
Slug: en-menos-de-un-mes-han-sido-asesinados-11-lideres-sociales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/informe_derechos_onu.-lasdororillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

###### [13 Sept 2016] 

<div class="msg msg-group">

<div class="message message-chat message-in tail message-chat">

<div class="bubble bubble-text has-author">

[⁠⁠⁠⁠] entre el Gobierno colombiano y las FARC-EP, han sido asesinados 11 líderes sociales en distintas regiones del país, en las que **adelantaban actividades de pedagogía por la paz y defensa del medio ambiente**, así como en contra de la minería]{dir="ltr"} y la siembra de cultivos ilícitos, según afirma el programa Somos Defensores.

Entre las víctimas se encuentra el [líder indígena Awá, [[Camilo Roberto Taicus Bisbicusm](https://archivo.contagioradio.com/entre-enero-y-agosto-de-2016-han-asesinado-a-77-personas-en-tumaco/)], quien fue asesinado el pasado viernes 26 de agosto, cuando regresaba a su comunidad y fue **interceptado por un taxi y dos motos que lo detuvieron y le dispararon**. Ese mismo día fue denunciado el plan que organizaciones criminales de Norte de Santander preparan para atentar contra la vida de la defensora [[Judith Maldonado Mojica](https://archivo.contagioradio.com/judith-maldonado-denuncia-que-organizaciones-criminales-planean-asesinarla/)].]{dir="ltr"}

Según lo denunció el Comité de Integración del Macizo Colombiano, el pasado lunes 29 de agosto fueron asesinados en el Cauca los reconocidos ambientalistas [[Joel Meneses, Ariel Sotelo y Mereo Neneses](https://archivo.contagioradio.com/asesinan-a-tres-ambientalistas-en-cauca/)]. Los hechos ocurrieron sobre las 8 de la mañana, cuando **hombres con uniformes militares y armas largas dispararon contra las víctimas** en el sitio conocido como Guayabillas del corregimiento de Llacuanas, municipio de Almaguer.

[Ese mismo lunes sobre las 11 de la mañana, fue asesinado Diego Alfredo Chirán Nastacuas quien apareció con signos de tortura y siete impactos de bala en La María, municipio La Barbacoa, **a pocos metros de donde está ubicado el Ejército Nacional**. Mientras se llevaban a cabo sus honras fúnebres, fueron asesinados los hermanos Luciano y Alberto Pascal García, [[indígenas Awá](https://archivo.contagioradio.com/indigenas-awa-a-punto-de-salvar-sus-territorios-de-la-explotacion-petrolera-de-colombia-energy/)] en zona rural del municipio de Llorente.]{dir="ltr"}

El pasado viernes 2 de septiembre en horas de la tarde, la defensora de derechos humanos y de los derechos de las mujeres afrodescendientes, indígenas y campesinas e integrante del MOVICE, [[Blanca Nubia Díaz](https://archivo.contagioradio.com/amenazan-a-blanca-nubia-dias-integrante-del-movice/)], fue abordada por un sujeto que portando un arma blanca le arrancó un botón distintivo de la lucha contra la [[desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)] y en un acto intimidante le aseguró **"Si sigues jodiendo te vamos a picar, bien picadita"**.

[La Asociación de Hermandades Agrícolas y Mineras de Guamoco, Aheramigua, denunció que un ex integrante de la entidad aseguró que activos **paramilitares están ofreciendo dinero para que les entreguen al presidente** de la organización, [[Mauricio Sánchez](https://archivo.contagioradio.com/comunidades-denuncian-17-asesinatos-a-manos-de-paramilitares-en-el-bagre-antioquia/)], y que lo más aconsejable es retirarse de la corporación porque en sus palabras "esto se va a poner caliente".]{dir="ltr"}

[El pasado martes 6 de septiembre fue **asesinada Cecilia Coicué, lideresa indígena de Corinto**, Cauca, integrante de la Asociación de Trabajadores Campesinos, de la Federación Nacional Sindical Unitaria Agropecuaria, del proceso de Unidad Popular del Suroccidente Colombiano y del Movimiento Marcha Patriótica. La lideresa era propietaria de un terreno que funcionará como una de las [[zonas veredales de concentración](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)] de las FARC-EP.]{dir="ltr"}

[La comunidad de Paz de San José de Apartadó y la Asociación Campesina de San José de Apartadó denunciaron el pasado jueves la presencia de por lo menos **50 paramilitares en las veredas Arenas Altas, La Resbalosa y El Porvenir**, quienes portaban armas de largo alcance y haciéndose llamar [[Autodefensas Gaitanistas de Colombia](https://archivo.contagioradio.com/autodefensas-gaitanistas-anuncian-paro-armado/)] pasaron de casa en casa anunciando que ahora si venían para quedarse. ]{dir="ltr"}

[El pasado viernes 9 de septiembre, en Barbosa, Antioquia,fue asesinada María Fabiola Jiménez, lider comunitaria de 69 años, quien fue **atacada a tiros cuando se movilizaba en un bus de servicio público**.]{dir="ltr"}

[Los más recientes hechos se presentaron este domingo en Cesar y Sur de Bolívar, dónde fueron asesinados [[Néstor Iván Martínez](https://archivo.contagioradio.com/asesinan-a-nestor-martinez-lider-comunitario-en-la-sierrita-cesar/)] y [[Álvaro Rincón](https://archivo.contagioradio.com/comunidad-asegura-que-ejercito-nacional-asesino-a-lider-campesino/)]. El primero, líder del Congreso de los Pueblos y miembro del Consejo comunitario de las comunidades negras de la Sierra, El Cruce y La Estación y el segundo, afiliado a la Junta de Acción Comunal de la vereda Patio Bonito, municipio de San Pablo.]{dir="ltr"}

De acuerdo con el programa Somos Defensores, en los primeros seis meses de este año fueron [[asesinados 35 líderes sociales en todo el país](https://archivo.contagioradio.com/aumentan-los-riesgos-para-defensores-de-ddhh-en-colombia/)], una lamentable radiografía que se constituye como un gran reto para el Gobierno colombiano, que deberá **brindar garantías para la supervivencia de quienes defienden los derechos** **humanos y ambientales**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

</div>

</div>

</div>

<div>

<div>

<div>

⁠⁠⁠⁠

</div>

</div>

</div>
