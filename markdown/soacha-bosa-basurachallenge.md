Title: Soacha y Bosa se unen al #BasuraChallenge en defensa de la naturaleza
Date: 2019-03-15 15:21
Category: Ambiente, Comunidad
Tags: basura, bosa, soacha
Slug: soacha-bosa-basurachallenge
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/53706664_1076229549226732_8580728441270173696_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Chucuita Ogamora GAES CiV 

###### 15 Mar 2019 

Con el boom que por estos días vienen generando en redes sociales tendencias como **\#BasuraChallenge y \#TrashtagChallenge** un grupo de ciudadanos decidió utilizarlas como estrategia para la protección y defensa de los ecosistemas en el **Cerro de Chucuita y el Humedal Chucuita Ogamora**, al sur de Bogotá.

Los promotores de la iniciativa, vienen convocando a quienes deseen sumarse para este **domingo 17 de marzo**, con una jornada especial de limpieza con **challenge fotográfico** por el planeta en el **Cerro de La Chucuita**, así como sensibilización por el territorio.

El reto viral que ha dado la vuelta al mundo, en el que se incluye una **fotografía del antes y después de los lugares que son intervenidos**, ha servido para que se repliquen este tipo de iniciativas en otras ciudades y países. En el cerro, según reportan los activistas, ya existe contaminación, lo que motiva la promoción de esta campaña.

Adicional a la recogida de basura, en el recorrido **se realizará una pequeña exposición sobre el territorio y su importancia para Ciudad Verde, Soacha y Bosa** de estos ecosistemas. El punto de reunión para iniciar será el **Parque Alameda Frente al Prado Verde a las 8:00am** (Calle 17 con carrera 38 y Avenida Potrero Grande Calle 33).

Los organizadores recomiendan la puntualidad en la asistencia, considerando particularmente que varias personas vendrán fuera de la ciudad en apoyo, así como llevar suficiente **hidratación, bolsas de basura, guantes, tapabocas, ropa cómoda y protección solar. **
