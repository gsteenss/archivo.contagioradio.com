Title: Humedal de La Guajira en riesgo por exploración sísmica
Date: 2016-06-13 15:58
Category: Ambiente, Nacional
Tags: ANLA, Humedales, La Guajira
Slug: humedal-de-la-guajira-en-riesgo-por-exploracion-simica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/LAGUNA-WASHINGTON-MAICAO-1-e1465851353807.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  La Guajira Hoy 

###### [13 Jun 2016] 

La Guajira nuevamente es víctima del modelo extractivista en el que se basa la economía colombiana. Esta vez, la compañía Hocol busca realizar exploración de gas cerca al único humedal que tiene el municipio de Maicao, y **donde se ubica la laguna Washington, una reserva natural donde toman agua varias comunidades indígenas Wayúu**.

**Aunque desde la Autoridad Nacional De Licencias Ambientales, ANLA, se asegura que actualmente no existe una licencia ambiental para esa actividad,** el proceso ya se tramita en Corpoguajira, y  según denunció la emisora La Fm, que fue a la zona,  trabajadores de la compañía están trabajando a escasos metros del humedal donde llegan aves migratorias desde Estados Unidos.

Por su parte, desde la oficina de Asuntos Indígenas, se asegura que la empresa está engañando a las comunidades Wayúu y no se ha hablado sobre las afectaciones ambientales y sociales.* *Incluso los líderes indígenas aseguran no tener claro en qué consiste el proyecto, porque en principio creían que se trataba de unas trochas para pasar gasolina.

Frente a esta situación, Manuel Rodríguez Becerra, director del Foro Nacional Ambiental, aseguró que este tipo de actividades mineras en zonas productoras de agua son absolutamente improcedentes, **además añade que, pese a las declaraciones, la ANLA, es un objeto de total desconfianza,** y el Ministerio de Ambiente no actúa como defensor de la naturaleza.

Cabe recordar que el departamento de **La Guajira posee más de 30 humedales que favorecen la mitigación de inundaciones y la erosión costera,** y que además este tipo de reservas naturales constituyen el hábitat de varias especies de flora y fauna, incluso Laguna Washington, es uno de los humedales más importantes de ese departamento.

<iframe src="http://co.ivoox.com/es/player_ej_11901911_2_1.html?data=kpamkpaddZKhhpywj5eXaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5ynca7dyNrSzpC2pc6ZpJiSo6nWqdufjpC70dnNu8Lt1tqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
