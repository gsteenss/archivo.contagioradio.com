Title: Atrapasueños Tikkun, una apuesta de jóvenes para recuperar su territorio en Suba
Date: 2017-06-29 16:32
Category: Educación, Nacional
Tags: jovenes, Suba
Slug: atrapasuenos-tikkun-apuesta-de-los-jovenes-para-recuperar-su-territorio-en-suba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/jovenes-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Atrapasueños Tikkun] 

###### [29 Jun 2017] 

Docentes, estudiantes y artistas del graffiti, han venido desarrollando en el Colegio Distrital República Dominicana, en Suba la Gaitana, una **propuesta para recuperar los espacios públicos, deshabitados por el miedo a la delincuencia y las bandas del microtráfico que operan en este sector, a partir del arte, la pintura y los colores**.

Atrapasueños Tikkun es el nombre de esta iniciativa que este año tuvo su primera muestra cultural y que de acuerdo con Sandra Gaitán “**el objetivo es trasformar las dinámicas de los jóvenes y las formas en las que asumen los espacios en donde conviven**”. Los colectivos Bogotá Graffiti (de Colombia) y Atrapasueños (de Argentina), participaron en la creación de este escenario que se desarrolló este año.

![jovenes 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/jovenes-4.jpg){.alignnone .size-full .wp-image-42951 width="780" height="1040"}

Gaitán, expresó que la propuesta ha permitido una apropiación no solo por parte de los jóvenes sino también desde la comunidad “**para los chicos es hacer otras apuestas, para la comunidad ha sido resignificar el espacio incluso como un lugar turístico**”. Le puede interesar: ["Cortometrajes hechos por jóvenes se verán en incinerante"](https://archivo.contagioradio.com/cine-colombiano-incinerante/)

Ahora las paredes del Colegio Distrital República Domicana, hacen parte de las galerías callejeras de la capital, mientras que le permite a los jóvenes ocupar los espacios para actividades culturales y tener otras posibilidades para salir de las redes de microtrafico y vandalismo en la ciudad. Le puede interesar: ["Día del estudiante caído"](https://archivo.contagioradio.com/dia-del-estudiante-caido/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
