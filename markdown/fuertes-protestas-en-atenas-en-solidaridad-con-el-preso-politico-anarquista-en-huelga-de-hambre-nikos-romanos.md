Title: Fuertes protestas en Atenas en solidaridad con el preso político anarquista en huelga de hambre Nikos Romanos
Date: 2014-12-12 14:24
Author: CtgAdm
Category: El mundo
Slug: fuertes-protestas-en-atenas-en-solidaridad-con-el-preso-politico-anarquista-en-huelga-de-hambre-nikos-romanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/thumb-25.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En lo que ya ha sido calificada como la manifestación más grande del movimiento libertario griego, miles se  solidarizaron con el preso político Nikos Romanos, en huelga de hambre desde el 13 de Noviembre, para exigir que le permitan continuar con sus estudios accediendo a los permisos necesarios, y que el propio estado le ha negado. Se presentaron fuertes disturbios en el barrio de Exarchia al finalizar la marcha de más de 10.000 personas. Aprovechando la coyuntura entrevistamos a Stavros Kassis miembro de los movimientos sociales, quien nos explica la situación de los presos pero también la del país después de los duros ajustes impuestos por la Troika y las graves consecuencias a nivel político, social y económico.
