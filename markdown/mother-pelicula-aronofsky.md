Title: Mother: Una obra maestra en el cine del nuevo siglo
Date: 2017-10-25 08:18
Category: Columnistas invitados, Opinion
Tags: Cine
Slug: mother-pelicula-aronofsky
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Mother-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Paramount Pictures 

###### Por: [Jake Sebastián Estrada Charris](https://archivo.contagioradio.com/?s=jake+estrada) 

Durante su exhibición en el festival de cine de Venecia, fue aplaudida por unos y chiflada por otros. A groso modo, ***Mother*** (2017) es una película que ha divido opiniones, pero podría decirse que la mayor cantidad de comentarios publicados por la llamada crítica especializada son negativos. Antes de que continúe leyendo esta crítica, usted debe saber que me encuentro del otro lado de la balanza, porque con convicción afirmo que la polémica cinta del director *Darren Aronofsky* tiene las facultades de ser una de las joyas en el cine de comienzos de este nuevo siglo.

En un principio, el largometraje pretende contar este sencillo relato: un novelista (*Javier Bardem*) se encuentra estancado y no logra escribir algo que lo satisfaga. Mientras tanto, su joven esposa (*Jennifer Lawrence*) reconstruye la casa paradisiaca en donde conviven; las cosas cambiarán cuando el escritor acceda a alojar a unos extraños huéspedes (*Ed Harris* y *Michelle Pfeiffer*).

Lo que podría ser fácilmente alguna historia para cualquier thriller taquillero, se convierte en una cinta transgresora que expone los vicios de la sociedad moderna junto con la volatilidad de comportamientos humanos existentes en cualquier civilización, que al ser dañinos en su mayoría, tienen la capacidad de destruir el estado ideal de posibles realidades con las cuales se nos garantizaría fácilmente un mundo que todos aspiramos encontrar.

*Jennifer Lawrence* brilla en la cinta. Según ella, este ha sido el papel más complejo de su carrera. “*Me llevó a un lado completamente diferente de mí misma*”, afirmó. Por su parte*, Javier Bardem*, *Ed Harris* y *Michelle Pfeiffer* lideran un equipo de personas que hace un gran trabajo: desde todo el equipo de producción de la obra, pasando por la música compuesta por *Jóhann Jóhannsson*, también a través de la fotografía a cargo de *Matthew Libatique*, se genera la armonía perfecta para que esta película escrita por *Darren Aronofsky* en tan solo 5 días, sea tan perturbadora como se deseaba.

Soy de los que hubiese aplaudido *Mother* en Venecia porque el filme logra simbolizar el amor, la divinidad, la repulsión y la violencia en dosis adecuadas de dolor, melancolía y confusión. Todo esto puede apreciarse en los colores empleados: negro (oscuridad), azul (divinidad) y naranja (éxtasis); aunque es claro que se emplean otro tipo de tonalidades en diversas situaciones.

<iframe src="https://www.youtube.com/embed/hXWaiBhZpgI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Debe tenerse valentía para realizar una película que contenga juicios de valor directos a temas tan sensibles como la religión usada en función para destruír; también resulta más que interesante percibir todas esas apreciaciones visuales del director que buscan interpelar a la divinidad. Mother tiene el tiempo suficiente para abordar asuntos coyunturales que van desde el extremismo, el asesinato, la trata de personas y hasta la indiferencia que tenemos nosotros con todos ellos.

A lo largo de su carrera como cineasta, Aronofsky ha buscado materializar cualidades a través de sus personajes. La **obsesión** en El Cisne Negro (2010) o la **desolación** en Réquiem por un sueño (2000) son claros ejemplos de ello; no obstante, *Mother* es eso y mucho más porque se hace difícil descubrir en qué momento las ideas se convierten en individuos para desarrollar una dinámica bidireccional de transformaciones evidentes.

La cinta protagonizada por Jennifer Lawrence es un largometraje que ofrece mucho al espectador; resulta por lo menos complicado olvidarla inmediatamente. Como dato adicional, se sabe que *Aronofsky* se inspiró en el largometraje ***El ángel exterminador (1962),*** de *Luis Buñuel*.

Hay que tomarse tiempo para apreciar *Mother* y reconocer que los acontecimientos contienen un subtexto que cuanto menos resulta interesante. El director *Martin Scorsese* escribió una extensa columna para la revista *The Hollywood Reporter* luego de ver la película y enterarse de los muchos comentarios destructivos que recibió el filme en publicaciones especializadas. Vale la pena leer este fragmento:

“*¿Es una película que debe ser explicada? ¿Qué pasa con la experiencia de ver Mother!'? Era tan tangible, tan hermosamente escenificada e interpretada: la cámara subjetiva y los ángulos inversos del punto de vista, siempre en movimiento... el diseño de sonido, que llega al espectador desde las esquinas y te lleva a profundizar más y más en la pesadilla... el desarrollo de la historia, que muy gradualmente se convierte en más y más terrible, conforme la película avanza. Sólo un verdadero, apasionado cineasta podría haber hecho esta película, que todavía estoy experimentando semanas después de verla”.*

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
