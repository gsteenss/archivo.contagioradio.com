Title: Colombia, el cuarto país del mundo con mayor número de periodistas asesinados
Date: 2016-02-09 11:17
Category: DDHH, Entrevistas
Tags: día del periodista, FLIP, Libertad de Prensa
Slug: libertad-de-prensa-en-colombia-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Libertad-de-prensa-e1455032374635.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Otra Cara 

<iframe src="http://www.ivoox.com/player_ek_10373084_2_1.html?data=kpWgmZiUfJWhhpywj5eVaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncaTjzdTaxM7FaZO3jMrZjcjZpdPo0JDdw4qnd4a1pdiYxsrQb87pz8ncjcjTsozhwt7c1JDSaaSnhqeuz8rWs4zYjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [9 Feb 2016]

En el marco del Día Nacional del Periodista y de la conmemoración de los 20 años de la Fundación para la Libertad de Presa, la FLIP presentó el informe “Paz en los titulares, miedo en la redacción”, en el que se evidencia que **durante el 2015 se registraron 147 agresiones directas contra la prensa en Colombia**, con un total de 232 víctimas; entre las que se incluye los asesinatos de los periodistas Luis Antonio Peralta y Flor Alba Núñez.

El informe indica que la impunidad sigue siendo una constante, el **47% de los asesinatos de periodistas durante los últimos 35 años, han prescrito**, es decir, que por el paso del tiempo se venció la potestad del Estado para continuar las investigaciones. Además, existen 74 casos en que la justicia había dejado actuar para sancionar a los responsables.

Aunque se destacan algunos avances respecto al programa de protección a periodistas, según la FLIP, las **autoridades siguen actuando con mucha lentitud para prevenir las violaciones a los derechos humanos de la prensa en Colombia**, así mismo, las investigaciones no avanzan generando mayor impunidad. Es por ello, que desde la FLIP se recomiendo realizar varios ajustes para proteger la vida de los comunicadores, siendo Colombia el cuarto país del mundo con mayor número de periodistas asesinados.

“El balance general muestra que los esquemas de seguridad todavía se demoran mucho en ser implementados y la reacción de las autoridades ante riesgos inminentes sigue siendo lenta. Adicionalmente, en el interior del programa se han hecho varios cambios que propiciaron el **aumento en la burocracia para la toma de decisiones, los casos de corrupción y se han presentado sobrecostos**”, asevera el informe.

Durante el 2015, la FLIP “identificó, que la **Fiscalía y los jueces de la república actuaron como censores en repetidas ocasiones**. El Fiscal General, Eduardo Montealegre, citó varias veces a periodistas para que declararan sobre hechos relacionados con su oficio. Algo similar ocurrió el 3 de julio de 2015 cuando, luego de unas explosiones de baja intensidad en Bogotá, el Fiscal les pidió a los periodistas que hubieran registrado los hechos en audios y vídeos que entregaran el material a la Fiscalía o la Policía. Montealegre **advirtió que si esos archivos llegaban a manos de periodistas o medios de comunicación, se estaría incurriendo en un delito**”.

Cabe recordar, el  9 de febrero de 1791 se origina la celebración del Día del Periodista en el país, con el lanzamiento del primer periódico bogotano, “Papel Periódico de la Ciudad de Santafé de Bogotá”. Desde ese momento comienza el Colombia el ejercicio del periodismo profesional el Círculo de Periodistas de Bogotá (CPB) lo ratifica con la Ley 51 del 18 de diciembre de 1975.

<p>
<script id="infogram_0_libertad_de_prensa_2015" title="Libertad de prensa 2015" src="//e.infogr.am/js/embed.js?qss" type="text/javascript"></script>
</p>
<div style="padding: 8px 0; font-family: Arial!important; font-size: 13px!important; line-height: 15px!important; text-align: center; border-top: 1px solid #dadada; margin: 0 30px;">

[Libertad de prensa 2015](https://infogr.am/libertad_de_prensa_2015)  
[Crear infográficos](https://infogr.am)

</div>

[Informe Anual 2015 FLIP](https://es.scribd.com/doc/298709293/Informe-Anual-2015-FLIP "View Informe Anual 2015 FLIP on Scribd")  
<iframe id="doc_20884" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/298709293/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
