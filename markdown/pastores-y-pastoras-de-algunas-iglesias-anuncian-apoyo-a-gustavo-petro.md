Title: Pastores y pastoras de algunas iglesias anuncian apoyo a Gustavo Petro
Date: 2018-06-06 17:44
Category: Nacional, Política
Tags: Angela Maria Robledo, elecciones 2018, Gustavo Petro, Pastoras, Pstores
Slug: pastores-y-pastoras-de-algunas-iglesias-anuncian-apoyo-a-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/gustavo-petro-colombia-rtr-img.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El sonajero] 

###### [06 Jun 2018]

Pastores y pastoras de diferentes iglesias en el país, manifestaron a través de un comunicado de prensa, su apoyo a la candidatura de Gustavo Petro por la presidencia de Colombia. De acuerdo con la misiva, durante la primera vuelta más de la mitad del país expresó que **"no quiere más la guerra, la corrupción, la falta de justicia y la exclusión económica"**.

En ese sentido manifestaron que como pastores y pastoras de la iglesia, su ética les impide hacer proselitismo político desde los púlpitos, reconociendo la diversidad de ideologías políticas que conforman sus comunidades "a las que se deben y respetan". (Le puede interesar:["Jóvenes de diferentes tendencias políticas se reúnen para impulsar el Acuerdo por lo fundamental"](https://archivo.contagioradio.com/jovenes-de-diferentes-tendencias-politicas-se-reunen-para-impulsar-el-acuerdo-por-lo-fundamental/))

Sin embargo, manifiestan que reconocen el momento político e histórico que atraviesa el país que les indica la urgencia de tomar una opción política, y en donde también consideran que el voto en blanco no es una de ellas, debido a que no es válida en segunda vuelta. **Razón por la cual manifiestan su apoyo a la campaña electoral de Gustavo Petro y Ángela María Robledo**

"El pueblo ahora es consciente que los dirigentes políticos tradicionales no tienen vergüenza, antes del 27 de mayo, manifestaban contradicciones, odios y descalificaciones con las cuales hacían que el pueblo los acompañara en esos comportamientos y ahora se juntan “como uno solo”. Nos muestran que son mentirosos. Pero tal vez, esta sea la oportunidad histórica de derrotar la mentira", afirmaron en el comunicado de prensa.

[Carta de Pastores y Pastoras](https://www.scribd.com/document/381354033/Carta-de-Pastores-y-Pastoras#from_embed "View Carta de Pastores y Pastoras on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Carta de Pastores y Pastoras" src="https://www.scribd.com/embeds/381354033/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Vd5PlXsDmGMhenNpoKUY&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_45518" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
