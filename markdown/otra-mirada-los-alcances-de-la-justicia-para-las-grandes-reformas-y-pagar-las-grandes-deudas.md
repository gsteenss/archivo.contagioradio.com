Title: Otra Mirada: Los alcances de la justicia para las grandes reformas y pagar las grandes deudas
Date: 2020-07-28 21:24
Author: PracticasCR
Category: Nacional, Otra Mirada, Otra Mirada, Programas
Tags: Álvaro Uribe, Diego Cadena, justicia
Slug: otra-mirada-los-alcances-de-la-justicia-para-las-grandes-reformas-y-pagar-las-grandes-deudas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/justicia-transicional-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: probono.cl

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay mucha expectativa en cuanto al funcionamiento actual y lo que pueden ser los cambios, positivos o negativos, en los organismos de justicia; esto en relación al reciente cambio de la mesa directiva del senado, a la audiencia de imputación de cargos llevada el 27 de julio en contra de Diego Cadena, abogado del expresidente Álvaro Uribe y al rechazo de la solicitud de sometimiento de Salvatore Mancuso por parte de la Sala de Reconocimiento de Verdad y Responsabilidad de la Jurisdicción Especial para la Paz (JEP).(Le puede interesar: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para analizar cómo se ve el panorama y qué se puede esperar con esta imputación de cargos, participaron en el conversatorio Reinaldo Villalba, abogado y vicepresidente de la Federación Internacional de Derechos Humanos, Daniel Prado, abogado apoderado por las víctimas del grupo paramilitar en el caso de los “Doce Apóstoles'' y Adriana Arboleda, directora de la Corporación Jurídica Libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados analizan qué simboliza para el país que se estén realizando estos procesos contra Diego Cadenas y Álvaro Uribe y cuál se esperaría que fuera el resultado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explican qué es lo que se podría esperar con la nueva configuración particularmente del Senado y qué habría que hacer en temas de reforma para que se adelanten investigaciones cuyo proceso ha sido lento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También comentan cómo influye la pandemia, la cuarenta y la virtualidad en procesos de justicia y para fializar, se toma un enfoque en relación a las víctimas y qué es lo que requiere la justicia en general para terminar con la impunidad y revelar todas esas verdades que no conocemos y que el país merece saber.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 24 de julio: [Otra Mirada. Se vale protestar](https://www.facebook.com/contagioradio/videos/314114349967734)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://www.facebook.com/contagioradio/videos/300195014433539"} -->

<figure class="wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/300195014433539

</div>

</figure>
<!-- /wp:embed -->
