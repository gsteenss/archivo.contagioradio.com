Title: En marzo el cine latinoamericano viaja en "Cicla"
Date: 2018-03-01 15:40
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cinemateca Distrital, Latinoamérica
Slug: cicla-cine-latinoamericano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Afiche-Cicla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CICLA 

###### 01 Mar 2018 

Llega a Bogotá la muestra de cine latinoamericano Cicla 2018, un evento organizado por la Asociación de Agregados Culturales de América Latina en Colombia y la Cinemateca Distrital, que en su sexta edición cuenta con una programación compuesta por 16 películas provenientes de catorce países: Colombia, Bolivia, Argentina, Chile, México, Paraguay, Ecuador, República Dominicana, Brasil, Panamá, Guatemala, Uruguay, Perú y Costa Rica.

Durante once días los capitalinos podrán disfruta de lo nuevos géneros del cine latino con las películas: ***El Rey Negro*** de Paola Gosalvez​, ***Dos Disparos*** de Martín Rejtman​, ***La Casa Lobo*** de Joaquín Cociña y Cristóbal León​, ***La Región Salvaje*** de Amat Escalante​, ***El Tiempo Nublado*** de Arami Ullón​, ***Mi Tía Toti* **de León Felipe Troya​, ***Ciro y Yo*** de Miguel Salazar​, ***Mañana no te olvides*** de José Enrique Pintor​, ***Bixa Travesty* **de Kiko Goifman y Claudia Priscilla​, ***El Almanaque* **de José Pedro Filipovich​, ***Piripkura*** de Mariana Olvia​, Bruno Jorge y Renata Terra​, ***Es hora de enamorarse*** de Guido Bilbao​, ***Abrazos*** de Luis Argueta​, ***El Soñador* **de Adrián Saba​, ***Nosotros las piedras* **de Álvaro Torres Crespo​ y ***El libro de Lila***  de Marcela Rincón.

[![lacasalobo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/lacasalobo-690x372.png){.alignnone .size-medium .wp-image-51868 .aligncenter width="690" height="372"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/lacasalobo.png)

###### Fotograma de *La casa del Lobo* 

“En esta sexta edición buscamos seguir consolidando la muestra como un espacio anual de reflexión sobre el cine latinoamericano, además de acercar sus relatos a fieles y nuevos públicos", asegura Paula Villegas, directora de la Cinemateca Distrital​.

Además, la programación incluye una muestra itinerante de la 58 versión de Cine de Cartagena de Indias- FICCI, los asistentes encontraran un panel académico sobre el cine latinoamericano y la proyección especial de *La Casa del Lobo*, una historia que muestra los miedos de la infancia a través de elementos de terror presentes en los cuentos infantiles, obra del chileno Cristóbal León, quien será el invitado de honor de la muestra.

El Festival de Cine Cicla 2018 se realizará del 8 al 18 de marzo en la Cinemateca Distrital, si desea saber más sobre los horarios y boletería y la programación consulte [aquí](http://www.idartes.gov.co/sites/default/files/noticia_documentos/Programaci%C3%B3n%20Cicla%202018.pdf?utm_source=phplist2865&utm_medium=email&utm_content=HTML&utm_campaign=En+marzo%2C+viva+el+cine+Latinoamericano).

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
