Title: Profesores a paro por 24 horas en defensa de la educación pública
Date: 2018-10-22 12:38
Author: AdminContagio
Category: Educación, Movilización
Tags: educación pública, fecode, Movilización, paro
Slug: profesores-paro-por-24-horas
Status: published

###### [Foto: @vdcediel] 

###### [19 Oct 2018] 

**Este 23 de octubre inicia el paro nacional de la Federación Colombiana de Trabajadores de la Educación (FECODE), que tendrá una duración de 24 horas**, y estará acompañado por una jornada de movilización y toma de las capitales del país. Los profesores reclaman que los 500 mil millones asignados como incremento para la educación pública son insuficientes, y se requieren al menos 900 mil más, para garantizar este derecho.

La decisión de movilizar a los docentes fue tomada por la junta nacional de la Federación, integrada por 34 sindicatos, quienes se reunieron el pasado jueves y optaron por emprender esta acción pacífica para que se inyecten mayores recursos a la educación pública, y se cumplan los acuerdos pactados el año pasado con el Gobierno. (Le puede interesar:["FECODE marcha este jueves en defensa de la educación pública"](https://archivo.contagioradio.com/fecode-marcha-defensa-educacion/))

**Miguel Angel Pardo, Secretario de Asuntos Pedagógicos, Educativos y Científicos de FECODE**; afirmó que desde el 3 de octubre se estableció una mesa de conversaciones con la Ministra de Educación para pedirle que retire el Proyecto de Ley 057, y realice la reforma constitucional pactada en 2017, permitiendo a los municipios salir de la crisis financiera en la que se encuentra el sector educación.

Pardo recordó que el martes de la semana pasada el **Proyecto de Ley 057**, que afectaba aún más los recursos de los municipios para la educación, **fue retirado**; sin embargo, esta exclusión no se debió a la presión del magisterio, sino a una dinámica interna del Gobierno. El secretario sostuvo que a pesar de este logro, **los motivos que los llevaron al paro nacional son los mismos que están exigiendo los estudiantes de educación superior.**

### **"Necesitamos 1,4 billones más para la educación pública"** 

El docente recordó que el Gobierno Nacional no cumplió con los acuerdos pactados con FECODE en 2017, siendo el principal la reforma constitucional que devolviera a la educación pública del país los **73 billones de pesos** que han dejado de recibir en 16 años, producto de dos reformas constitucionales que redujeron la participación del gobierno central en los entes territoriales de 46 a 24%.

Pardo sostuvo que con los dineros aprobados por el Presupuesto General de la Nación para la educación pública (básica, media, rural, de adultos y para niños con necesidades especiales), no se alcanza a cubrir todo el 2019. Por lo tanto, el aumento de 500 mil millones destinados a este sector debería aumentar 900 mil millones más, para llegar hasta los **1,4 billones**. (Le puede interesar: ["Profesores se suman a la defensa de la educación pública"](https://archivo.contagioradio.com/profesores-defensa-educacion-publica/))

El docente sostuvo que a estos problemas se suman la crisis que vive el régimen de salud en el magisterio, lo que ha llevado a que muchos profesores dependan de la Ley 100; y se sometan a los "paseos de la muerte" y otras deficiencias que tiene el sistema. (Le puede interesar: ["Profesores de Universidades públicas protestan con huelga de hambre"](https://archivo.contagioradio.com/profesores-huelga-de-hambre/))

### **En Bogotá la movilización comenzará en la Secretaría de Educación** 

La toma de la capital por parte de los profesores iniciará con una **movilización que partirá desde la Secretaría de Educación Distrital (cll. 26 \#66-63) y llegará hasta la Plaza de Bolivar**, donde los docentes exigirán a la administración Peñalosa mayores recursos para implementar la jornada completa y que se mantengan modalidades y áreas que están siendo cerradas.

Por parte de la Unión Nacional de Estudiantes por la Educación Superior (UNEES), se afirmó que acompañarían la jornada de movilización, razón por la que en Bogotá, a lo largo del camino, se integrarán a la marcha hasta el centro de la ciudad. (Le puede interesar:["¿Por qué están marchando los estudiantes?"](https://archivo.contagioradio.com/marchando-los-estudiantes/))

<iframe id="audio_29516392" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29516392_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
