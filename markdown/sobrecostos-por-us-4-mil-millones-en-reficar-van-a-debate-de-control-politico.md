Title: Sobrecostos por US$ 4 mil millones en Reficar van a debate de control político
Date: 2016-05-03 11:35
Category: Economía, Nacional
Slug: sobrecostos-por-us-4-mil-millones-en-reficar-van-a-debate-de-control-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Reficar-e1462452253102.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: The City Paper ] 

###### [3 Mayo 2016]

Los sobrecostos en la ampliación de la Refinería de Cartagena (Reficar), el proyecto que más dinero le ha costado a Colombia en toda su historia, se calculan en por lo menos 4 mil millones de dólares de acuerdo con la Contraloría General de la República, entidad que en días recientes ha denunciado **irregularidades en el suministro de información por parte de las firmas Reficar y CB&I**.

La institución ha tenido que **reiterar más de treinta veces las solicitudes de información y en la mayoría de los casos las firmas han solicitado prorrogas,** para justificar porque el proyecto pasó de un presupuesto inicial de 3.993 millones de dólares a un gasto total de 8.016 millones de dólares, incrementándose en 101%.

Por este [[millonario detrimento patrimonial](https://archivo.contagioradio.com/reficar-le-costo-al-pais-la-venta-de-isagen-4-veces/)], este martes se realizará un debate de control político en el Senado, en el que la Procuraduría y la Contraloría presentarán los **hallazgos financieros, ambientales y laborales**; encuentro al que deberán acudir Juan Carlos Echeverry, actual presidente de Ecopetrol, Reyes Reinoso presidente de la filial Reficar, así como el Ministro de Hacienda y el de Minas y Energía, según afirma el Senador Antonio Navarro Wolf.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
