Title: Ucrania: Antes y después de la guerra
Date: 2014-12-17 16:43
Author: CtgAdm
Category: Hablemos alguito
Slug: ucrania-antes-y-despues-de-la-guerra
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsPAFcPp.mp3)  
Después de las diversas crisis que ha sufrido el país comenzando con el Maidan, pasando por las revueltas en las provincias del este y, ya, en los combates vividos hasta la tregua decretada en el mes de Septiembre.  
Analizamos el devenir del conflicto intentando simplificarlo para poder entenderlo.  
Entrevistamos a Eloy Fontán, periodista de la Diagonal (estado español).Él ha estado investigando en el propio terreno y en la zona donde se han realizado los combates De tal manera entrevistándolo desmentimos todas las noticias falsas y manipulaciones que ha habido en el transcurso del conflicto, como por ejemplo la politización ya sea de derechas o de izquierdas de las milicias independentistas o prorusas. También contextualizamos las causas y consecuencias profundas, así como la intervención de otras potencias mundiales y sus propios intereses en la región.
