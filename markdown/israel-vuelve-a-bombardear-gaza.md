Title: Israel vuelve a bombardear Gaza
Date: 2015-05-27 14:55
Category: DDHH, El mundo
Tags: Ataque de la yihad islámica contra Israel, Franja de Gaza, Hamas detiene a miembros de la yihad islámica, Israel, Israel bombardea Gaza, Palestina, Rota tregua Israel-Palestina, Yihad Islamica
Slug: israel-vuelve-a-bombardear-gaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/image53bb3e3135fad9.05228027.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Losandes.com.ar 

**Israel ha vuelto a bombardear Gaza** tras un **ataque por parte de la Yihad Islámica** contra la población de Ashdod. En ambos ataques **no se han reportado víctimas.**

Oficiales del ejército Israelí han acusado a Hamas del último ataque, y han bombardeado posiciones del sur de Gaza. No ha habido ninguna víctima, y ,según ciudadanos Palestinos, se han **atacado campos de entrenamiento de la Yihad Islámica.**

El ministro de defensa Moshe Yaalon afirma que “...Israel no va a permanecer con los brazos cruzados tras del disparo de un cohete contra sus ciudadanos por parte de la Yihad Islámica...”.

Por otro lado, **Hamas** ha negado el incidente y ha **detenido a varios dirigentes de la Yihad Islámica,** acusados de romper la tregua decretada entre ambos estados. Medios de comunicación Palestinos como "Walla", afirman que el último ataque es debido a disputas entre las distintas facciones Palestinas.

Todo sucede en un contexto en el que **Amnistía Internacional** ha emitido un informe donde acusa a **Hamas de haber realizado ejecuciones extrajudiciales, torturas y persecuciones** contra **colaboradores Palestinos con Israel** en la última invasión de Gaza.

En total, habrían sido **23 ejecuciones extrajudiciales** y cientos de torturas, ya que los líderes de Hamas habrían permitido a sus fuerzas de seguridad actuar sin ningún tipo de límite.

En medio del fuego cruzado, **Netanyahu realizó declaraciones** en las que afirmó que en unos meses el ejecutivo se había planteado la posibilidad de **retomar las negociaciones** con los Palestinos después de un año de bloqueo.

**Gaza** permanece totalmente **destruida y su economía en crisis** debido a la última ocupación Israelí, donde **murieron en total 2.200 civiles** y por la que se le **acusa a Israel de crímenes de guerra** y de lesa humanidad.
