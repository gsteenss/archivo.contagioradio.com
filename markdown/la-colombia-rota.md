Title: La Colombia rota
Date: 2015-03-06 21:40
Author: CtgAdm
Category: Laura D, Opinion
Tags: biocombustibles, Grupo de Memoria Histórica, Karibá, Laura Duque
Slug: la-colombia-rota
Status: published

Por [**Laura Duque**](https://archivo.contagioradio.com/laura-duque/)

De alguien aprendí que Karibá es el País de las aguas y, para los Tayrona, el nombre ancestral de nuestra vertiginosa Colombia. Somos el único país de América del Sur que tiene costas tanto en el océano Pacífico como en el Atlántico; nuestro territorio es además el más diverso del mundo después de Brasil, que es más extenso. Sin embargo, Colombia es uno de los diez principales responsables de la destrucción de la mitad de la biodiversidad de la Tierra. Tristemente, la plutocracia y la famosa “inversión extranjera” o las llamadas “locomotoras” del agro y la minería, han destruido –y se prevé destruirán— gran cantidad de ecosistemas y comunidades.

El País de las aguas se ha convertido en el garaje de los países ricos y en el gran potrero de los empresarios o terratenientes locales. Las multinacionales y los señores feudales vienen explotando desde hace décadas el territorio que conforma a la vencida Nueva Granada, lo hacen en concierto con los malos gobiernos y el poder parainstitucional. La locura energúmena de unos y otros ha devastado nuestras selvas, y nuestro pueblo.

Esta patria está rota porque se fundó en la desigualdad. Los poderosos a lo largo de la historia se han apropiado de miles de hectáreas de tierra por medio de estratagemas de índole jurídico y, aún más preocupante, de la violencia. Según los datos recogidos por el Grupo de Memoria Histórica (GMH), desde 1985, alrededor de 8,3 millones de hectáreas fueron despojadas o abandonadas por sus auténticos propietarios: los campesinos inermes. Quienes no huyeron a causa de la guerra contrainsurgente, salieron despavoridos por cuenta de la consigna paramilitar: “o me vende o le compro a la viuda”.

Nuestra historia como republiqueta ha sido un hilo largo y grueso de sangre, sólo por nombrar uno de los tantos casos de barbarie, traeré a colación el acaecido en los territorios colectivos de Curvaradó y Jiguamiandó –pertenecientes al territorio del Carmen del Darién, en el Bajo Atrato—, allí más de 4.000 campesinos fueron obligados por los paramilitares a dejar sus tierras, sus animales y plantas, incluso tuvieron que dejar los cuerpos sin vida de familiares y vecinos; de esta manera, y en función del dinero, los señores del terror sembraron miles de hectáreas de palma africana.

Las violaciones contra dichas comunidades chocoanas datan de mediados de los 90’s. Los ‘paras’ llegaron con el pretexto de acabar con la guerrilla, pero detrás de los ataques contrainsurgentes se escondían intereses netamente económicos. Masacres, homicidios selectivos, descuartizamientos, torturas, desapariciones, desplazamiento y despojo… violencias que configuraron el espectro criminal que rodeó el proyecto de palma africana liderado por el fallecido paramilitar Vicente Castaño Gil con el beneplácito de los gobiernos de turno –1997-2007—.

La desidia y las atrocidades en contra de las mencionadas comunidades, así como en un sinnúmero de zonas del país, son una vergüenza, por decirlo menos. Es inconcebible que mientras el pueblo pide a gritos comida –ni siquiera hablemos de servicios básicos, salud, educación, etc.— el Estado responda con proyectos a gran escala como el aquí citado. Porque, recordemos, las palmas de aceite no se comen.

Lo que sí se dan el lujo de decir los poderosos es que la palma de aceite genera “biocombustibles”, eufemismo con el cual lo presentan; ¡no nos hagamos de la vista gorda! y nombremos las cosas como son: agrocombustibles, y bajo ninguna mirada su producción es ecológica, así como tampoco la agroindustria trae prosperidad para todos. ¡Seamos claros!, el desarrollo agroindustrial sólo es el cúmulo de unas prácticas productivas encaminadas a la maximización de la ganancia de unos pocos que van y vienen entre la legalidad y la ilegalidad. A ellos no les interesa, ni la agroecología y, mucho menos, la suerte que corran nuestros humildes campesinos.

Por último, no podría pasar por alto nuestro papel en este cloacal. Nosotros, los ciudadanos de a pie, también alimentamos este sistema capitalista y consumista, ese sector agroindustrial y toda la basura que tras él se esconde. Es una suerte de Leviatán mecánico hecho de engranajes que ruedan  hace mucho tiempo y todo cuanto ocurre en su interior es en función de los intereses del “gran arquitecto”, en este caso, de los que detentan el poder.

Y aunque las pérdidas que dejó la violencia son irreparables, albergo el aliento de lo que aún nos queda: diversidad y un pueblo que resiste. De quienes se creen los amos de la tierra sólo me resta decir que me dan lástima. Porque la tierra no es del hombre, de la tierra es el hombre; sólo somos una especie más en el planeta y una mota de polvo en el universo.
