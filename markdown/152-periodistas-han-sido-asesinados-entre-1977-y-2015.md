Title: 152 periodistas han sido asesinados entre 1977 y 2015
Date: 2015-12-06 08:39
Category: DDHH, Nacional
Tags: Fundación para la Libertad de Prensa -FLIP-, Informe La Palabra y el Silencio, La palabra y el silencio, Periodistas amenazados en Colombia, Periodistas asesinados en Colombia
Slug: 152-periodistas-han-sido-asesinados-entre-1977-y-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/La-palabra-y-el-silencio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CNMH 

###### [4 Dic 2015 ]

[De acuerdo con el último informe del Centro Nacional de Memoria Histórica CNMH, titulado 'La palabra y el silencio', **entre 1977 y 2015 han sido asesinados 152 periodistas**,** **por motivos ligados a las investigaciones que adelantaban sobre hechos de corrupción o asociados al conflicto armado. El **50% de estos crímenes han quedado en la impunidad** y han sucedido mayoritariamente en los departamentos de Valle del Cauca, Antioquia, Santander, Bogotá y Caquetá. ]

[Pedro Vaca, miembro de la Fundación para la Libertad de Prensa FLIP, asegura que esta investigación es producto de 2 años de trabajo del CNMH en los que la FLIP contribuyó para **radiografiar los actos violentos contra la prensa en el marco del conflicto armado** y que permitieron documentar por periodos tanto los patrones de violencia como los actores que los perpetraron. ]

[Según este informe entre **finales de la década de los 80 y principios de los 90, los sectores asociados al narcotráfico** fueron los principales responsables de los crímenes cometidos contra periodistas, luego **entre 1990 y el año 2002** lo serian los **miembros activos de las estructuras paramilitares** en auge durante esta época. ]

[De acuerdo con Pedro Vaca, entre los hallazgos más significativos del informe se destacan las pautas de mejoramiento interno para los medios de comunicación que en sus páginas se señalan, pues “los **periodistas no cuentan con garantías de protección sobre todo en las regiones**, en dónde en no pocas ocasiones quedan en riesgo frente a las decisiones que toman las editoriales en las casas matrices de los medios de información”. ]

[Desde iniciado el Proceso de Paz se podría hablar de una reducción en las acciones violentas por parte de las FARC principalmente; sin embargo, en zonas que han sido históricamente dominadas por esta guerrilla aún opera una fuerte autocensura, “**el hecho de que existan noticias sobre ceses unilaterales o eventuales ceses bilaterales genera una presión de autocensura muy fuerte** para los periodistas” que les impide publicar con tranquilidad noticias relacionadas con estos temas, según indica Pedro Vaca.]

[“Tampoco creemos que el simple descenso de agresiones sea igual a mejores condiciones para el ejercicio de la profesión”, indica el periodista, pues pese a que desde 2014 han disminuido los asesinatos de comunicadores, en lo que va corrido del 2015 han aumentado las amenazas en su contra.]

[“La labor de la prensa es la misma en tiempos de guerra o en tiempos de paz: **mantener una sociedad informada sobre los asuntos que le interesan para que puedan tomar una decisión conforme a lo que mejor consideren**”, razón por la este informe se considera como una herramienta frente a la “preocupación sobre las mejores condiciones para ejercer la libertad de expresión desde todos los ángulos posibles”.  ]

[ ]
