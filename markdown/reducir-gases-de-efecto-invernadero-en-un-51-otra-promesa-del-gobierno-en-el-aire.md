Title: Reducir gases de efecto invernadero en un 51%  ¿otra promesa del Gobierno en el aire?
Date: 2020-11-27 19:15
Author: AdminContagio
Category: Actualidad, Ambiente
Tags: Ambiente, emisiones de gases de efecto invernadero, Presidente Ivan Duque
Slug: reducir-gases-de-efecto-invernadero-en-un-51-otra-promesa-del-gobierno-en-el-aire
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/GEI-e1478282894893.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

El presidente Iván Duque [anunció](https://www.minambiente.gov.co/index.php/noticias/4877-colombia-reducira-en-un-51-sus-emisiones-de-gases-efecto-invernadero-para-el-ano-2030) que Colombia tiene como meta reducir en un 51% las emisiones de gases de efecto invernadero para el año 2030, como un compromiso para combatir el cambio climático. Sin embargo, algunos sectores cuestionan los compromisos adquiridos por el mandatario al señalar que mientras en los medios y frente a la comunidad internacional dice una cosa, en la práctica, da paso a actividades que ponen en riesgo el medio ambiente, como **permitir la realización de fracking y la explotación minera en áreas protegidas.  **

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

«Al año 2030 nos vamos a comprometer con una agenda clara y específica, multisectorial para que se tenga esa reducción de 51% en los GEI. Este es un compromiso importante en el contexto latinoamericano y también global que muestra que unidos debemos cumplir con ese propósito», señaló el mandatario (Le puede interesar: [Así deben proceder en la protección del Valle de Cocora](https://archivo.contagioradio.com/asi-deben-proceder-en-la-proteccion-del-valle-de-cocora/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que durante su campaña presidencial, Duque aseguró que se iban a proteger los páramos y los humedales. En este aspecto, muchos consideran que el presidente traicionó a la ciudadanía, a quienes defienden el ambiente y a sus compromisos al promover los pilotos de fracking en el páramo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de esos ecosistemas en riesgos es el páramo de Santurbán, en el que, mientras ambientalistas y comunidades luchan por la defensa del agua y por que se prohíba la explotación minera en Santander, otros buscan privilegiar los intereses de las empresas que quieren intervenir el páramo. (Le puede interesar: [«ANLA no tomó decisión de fondo sobre la licencia ambiental de Minesa»](https://archivo.contagioradio.com/decision-de-anla-es-un-logro-pero-no-toca-de-fondo-licencia-de-minesa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Igual de preocupante es la situación en la Amazonía en donde, esperando que por la pandemia disminuyera la deforestación, la minería y la extracción, estas no han hecho más que aumentar. Así se evidencia en las afirmaciones realizadas por el vicepresidente de la Comisión Mundial de Áreas Protegidas de la Unión Internacional para la Conservación de la Naturaleza (UICN), Cláudio Maretti, quien expresó que **los pactos internacionales para la protección de la Amazonía no han dado resultado**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a la reducción de gases efecto invernadero, el presidente afirmó que este objetivo se logrará, entre otras cosas, **con transición energética, movilidad limpia, reducción en la tasa de deforestación, la protección de la Amazonía y de los páramos.** Asimismo, señaló que «el objetivo es cumplir en 2022 la meta de sembrar los 180 millones de árboles».

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que las emisiones de gases de efecto invernadero en el país representan **solo el 0,4% del total de las emisiones de gases invernadero en el nivel global,** Colombia es **no de los países más vulnerables a los efectos del cambio climático**. Ante estos precedentes la meta de reducir en un 51% dichas emisiones pone en duda si lo expuesto por el presidente no se quedará en promesas como ha ocurrido en otros ámbitos de preservación del ambiente. (Le puede interesar: [Amazonía habria perdido 19.600 millones de árboles en los últimos meses](https://archivo.contagioradio.com/amazonia-deforestacion/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1332133145488056325?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1332244002817642497%7Ctwgr%5E%7Ctwcon%5Es3_\u0026amp;ref_url=https%3A%2F%2Fwww.contagioradio.com%2Fwp-admin%2Fpost-new.php","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1332133145488056325?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1332244002817642497%7Ctwgr%5E%7Ctwcon%5Es3\_&ref\_url=https%3A%2F%2Fwww.contagioradio.com%2Fwp-admin%2Fpost-new.php

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
