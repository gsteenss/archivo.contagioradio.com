Title: Sin voz y voto no hay participación política efectiva: Vivian Morales
Date: 2015-11-04 18:44
Category: Entrevistas, Paz
Tags: FARC, Juan Manuel Santos, Mesa de conversaciones de paz de la habana, Vivian Morales
Slug: sin-voz-y-voto-no-hay-participacion-politica-efectiva-vivian-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/vivian_morales_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: bluradio 

<iframe src="http://www.ivoox.com/player_ek_9273820_2_1.html?data=mpeklZ2WdI6ZmKiak5uJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DhytjWh6iXaaOnz5C5x8zNt83V1c7jw5DUpdPVjNHOjbXFvozX08rOxsaPtMLmwpDRw9ePqtbZ09%2Bah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Vivian Morales, partido Liberal] 

###### [4 Nov 2015]

En el trámite que se da en el Congreso de la República a la reforma legislativa para la **implementación de los acuerdos** no se está negando la posibilidad de la participación política porque tener voz pero no tener voto no es participar efectivamente según lo explica la senadora **Vivian Morales**, integrante del partido liberal. No se puede hablar de participación con voz pero sin voto, señala “para eso los escuchamos en una audiencia”.

Según la Senadora para que los integrantes de las FARC puedan tener voz y voto en la comisión legislativa habrá que reformar el marco jurídico para la paz, de lo contrario otorgar voz y voto es romper el marco constitucional. Sin embargo, para Morales es posible que la comisión especial que se está proponiendo el gobierno no avance, “los senadores no quieren esa comisión” afirma.

Por un lado porque una comisión tan grande no sería muy funcional, los debates serían muy largos y además los integrantes de las comisiones primeras de Senado y Cámara estarían legislado sobre temas que no manejan, es mejor que cada comisión legisle sobre las materias en los que son expertos, cada comisión con su tema y no una sola para todos los temas.

En cuanto a la necesidad de legislar, pero sin que esta comisión especial haya sido parte de acuerdos parciales para refrendación, Morales hace la salvedad puesto que aunque no está acordada esa manera de refrendar los acuerdos si se puede avanzar en un camino que va a ser necesario.

La guerrilla de las FARC, luego de la visita del grupo de congresistas reiteró su postura en torno a la necesidad de una Asamblea Constituyente. A este respecto Vivian Morales manifestó que un constituyente debe ser no solamente para los acuerdos de paz sino para todas las reformas necesarias como la reforma política y la reforma a la justicia entre otras.

Además manifestó que en el congreso actual o en una posible constituyente los líderes políticos más representativos de todos los partidos están y que es con ellos y ellas con quienes se debe legislar a pesar de la falta de credibilidad que señalan las encuestas.
