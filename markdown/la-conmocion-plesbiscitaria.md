Title: La conmoción plebiscitaria
Date: 2016-10-21 09:06
Category: Cesar, Opinion
Tags: plebiscito por la paz, proceso de paz
Slug: la-conmocion-plesbiscitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/plebiscito-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

#### Por  **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 20 Oct 2016 

[La ofensiva contra el delito político, la restitución de tierras y la justicia transicional resultó ganadora el pasado 2 de octubre; para ello fueron utilizados viejos y sucios métodos de guerra, incluyendo el uso calculado y sistemático de estrategias comunicacionales. Estas artimañas ahora han sido denunciadas bajo el cargo de fraude electoral. La proyección del miedo como hilo conductor del tejido del discurso del NO durante la campaña fue la característica de la acción política de la fracción opositora hacendada-ganadera de derecha; como lo hizo mientras gobernó durante 8 años. Así mismo, las bandas paramilitares y las fuerzas anti-restitución de tierras han impuesto el miedo mediante las amenazas a comunidades enteras y asesinatos de líderes sociales, campesinos e indígenas.]

[La contra-corriente que dirige un ex presidente pretende erigirse ahora como la única representante del amplio electorado que votó negativamente. Quiere revertir los acuerdos firmados el 26 de septiembre, desmontar la justicia transicional y eliminar, en consecuencia, lo poco que queda de la figura del delito político y del derecho de asilo; también quiere ahora el reconocimiento de derechos plenos a quienes se apropiaron de amplias extensiones de tierras mediante la corrupción y el desplazamiento forzado; busca con todas sus fuerzas, entre otras, que las altas Cortes “respeten”, frente a La Habana, el “fallo” del 2 de Octubre; privilegia al victimario y ofende a las víctimas; pretende la firma de un gran “Pacto Nacional”, de elites claro, que desvirtúe el acuerdo del 26 de septiembre; aunque ha sido rechazada por la]*[Fundación Fuimos Héroes]*[, que representa a unos 6 mil militares víctimas del conflicto, grita que defiende a los uniformados; y promueve marchas para “fortalecer el No”. Esta contra-corriente termidoriana ansía frenar la ola actual de movilización y resistencia ciudadana, campesina, estudiantil y de los trabajadores.]

[Así, dos semanas después  del resultado plebiscitario la situación en Colombia está más compleja que antes. En la historia de los pueblos nada está escrito; tan sólo hay bifurcaciones políticas que anuncian posibilidades. La apuesta cubre el plano del azar y lo profano de la movilización social.]

[Seguramente hay vías jurídicas que abran puertas para salir del atolladero; con certeza hay voluntades para dialogar sobre las nuevas propuestas en torno a los acuerdos. Pero en cuanto a los mortales se refiere, sólo se cuenta con la movilización social y con las direcciones que ella ha ido construyendo. No es el momento para depositar confianzas en sectores “democráticos” del capital, ni en optar por el “mal menor” como hicieron los “socialistas” de verdad verdad en la coyuntura de la segunda vuelta presidencial de hace un par de años. Independencia política ante todo, defensa de los acuerdos del 26 de septiembre y paz con justicia socioambiental.]

[Ahora que de la pluma de varios analistas la tinta se esparce en el listado de las factores que incidieron en el triunfo del NO, tenemos que anotar que aún queda mucha tela que cortar. Y el lienzo tiene nombre de autocrítica, comenzando por las direcciones. ¿Cuál fue el papel del movimiento sindical? ¿Cuál el de los estudiantes? ¿Cuál el de la Academia? ¿Cuál el de los movimientos sociales?.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
