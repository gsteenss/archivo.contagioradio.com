Title: ESMAD atacó a campesinos que protestaban contra erradicación forzada
Date: 2017-02-23 17:48
Category: Ambiente, Nacional
Tags: COCCAM, Erradicaciones dorzadas
Slug: campesinos-exigen-al-gobierno-frenar-erradicaciones-forzadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/el-universal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal ] 

###### [23 Feb 2017] 

**Campesinos de Tumaco fueron agredidos por el  ESMAD  cuando se encontraban en la vía Panamericana, entre Tumaco-Pasto, protestando para exigirle al gobierno que frene la erradicación forzada** de cultivos en sus territorios y cumpla con lo pactado en el punto cuatro de los Acuerdos de Paz de La Habana y los programas de sustitución de cultivos ilícitos.

**Varios campesinos fueron agredidos con balas de perdigón, mientras que otros fueron afectados por la inhalación de los gases lacrimógenos**, el desalojo se produjo sobre las 7 de la mañana. De acuerdo con Daivin Hurtado, integrante de COCCAM (Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana), hay 6 campesinos heridos, sin embargo están intentando establecer si habrían más. En el momento hay apróximadamente **1.300 campesinos regresando al lugar de la protesta.**

Daivin Hurtado señala que la fumigación va en contravía de lo acordado con las comunidades campesinas, **porque no genera un proceso en donde los cultivadores de hoja de coca puedan entrar y aplicar los programas de sustitución y realizar el remplazo**. Le puede interesar: ["Ejercito continúa con erradicación forzada en Putumayo"](https://archivo.contagioradio.com/ejercito-continua-erradicacion-forzada-de-coca/)

**“Los campesinos hemos trabajado en COCCAM planes de sustitución y afirmamos que estos deben acoplarse a cada departamento porque las necesidades son diferentes**” afirmó Hurtado y agregó que hasta el momento el gobierno no se ha sentado a dialogar con los campesinos de Nariño. Le puede interesar: ["Erradicaciones forzadas y sustitución no son compatibles con la paz: Cesar Jerez" ](https://archivo.contagioradio.com/erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez/)

En estos momentos los campesinos se encuentran **adelantando un censo para determinar cuántos de ellos cultivan hoja de coca** y cuál sería la forma de implementar el punto cuatro sobre la sustitución de cultivos ilícitos a partir de sus necesidades. Le puede interesar : "3.000 campesinos del Cauca rechazan la erradicación forzada"
