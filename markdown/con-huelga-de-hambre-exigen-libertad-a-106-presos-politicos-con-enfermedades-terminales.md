Title: Con huelga de hambre exigen libertad a 106 presos políticos con enfermedades terminales
Date: 2015-11-24 16:46
Category: DDHH, Entrevistas
Tags: Alberto Castilla en La Picota, Alirio Uribe en La Picota, Comité de Solidaridad con los Presos Políticos, Diálogos de paz en, Huelga de hambre presos en Colombia, Iván Cepeda, Presos políticos en Colombia, Profesor Miguel ángel Beltrán
Slug: con-huelga-de-hambre-exigen-libertad-a-106-presos-politicos-con-enfermedades-terminales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Presos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Aguijón ] 

<iframe src="http://www.ivoox.com/player_ek_9494793_2_1.html?data=mpmmlpydd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjc%2FY1tHh0ZDSs4zdz8jZ197Jb8KfkpaY0tfJt9DnjMrbjdXJsMrb09SYxsqPsdbZ09nSjdXTto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Miguel Ángel Beltrán] 

###### [24 Nov 2015 ] 

[En más de 20 cárceles colombianas cerca de 850 presos políticos adelantan una jornada de protesta y huelga de hambre que ya completa 16 días, exigiendo la liberación **de** **106 recluidos que padecen enfermedades terminales, son lisiados de guerra o madres gestantes**, pues pese al anuncio de liberación de 30 guerrilleros presos no hay soluciones concretas para estos casos críticos. ]

[El profesor Miguel Ángel Beltrán, recluido en la cárcel La Picota, asegura que es saludable este gesto humanitario a pesar de que se haya dado 3 años después de la instalación de la Mesa de Diálogos, pues **son graves las condiciones de salud que padecen los prisioneros de guerra y los responsables directos son CAPRECOM y el INPEC**.]

[Beltrán llama la atención frente al listado en el que se anuncia la liberación de un total de 80 guerrilleros de las FARC, entre quienes hay **prisioneros en estado crítico de salud**, sin embargo asegura que falta incluir otros nombres como el de Orlando Albeiro Traslabiña quien está **a punto de perder la vista y el oído** y que por ende, necesita una inmediata atención. ]

[La visita humanitaria de los Congresistas Iván Cepeda, Alirio Uribe y Alberto Castilla a la cárcel La Picota, permitió constatar las circunstancias que actualmente padecen los presos políticos y que se relacionan con **el hacinamiento y la precariedad en los servicios de salud para reclusos con enfermedades terminales o mujeres embarazadas**, una situación no exclusiva de los recluidos por delitos políticos sino que se extiende a todas las personas recluidas.]

[El profesor Beltrán solicita que organizaciones sociales o sectores de la salud independientes puedan participar de las brigadas de salud que el Gobierno ha anunciado harían parte del gesto humanitario, pues **hay desconfianza por sucesos ocurridos con anterioridad en el marco de acciones como la que se esperan adelantar**.     ]

[Beltrán concluye extendiendo la invitación a organizaciones y a la sociedad civil en su conjunto a la **concentración que se planea llevar a cabo en la cárcel La Picota para este jueves** y que busca llamar la atención frente a la necesidad de un gesto humanitario con los presos políticos en grave estado de salud y las madres gestantes.]

[Entretanto prisioneros políticos del Establecimiento Penitenciario y Carcelario de Alta y Mediana Seguridad de Valledupar 'La tramacua', integrantes del Colectivo Benkos Biohó, en un comunicado a la opinión pública nacional e internacional alertan sobre el comportamiento de la **cónsul de derechos humanos de esta cárcel Lilibeth Rua Andrade, quien no les ha garantizado la atención  médica y el suministro de sueros para hidratar a los huelguistas,** buscando entorpecer y desprestigiar la protesta.]

[Concluyen llamando la atención sobre el plazo de un año que se ha cumplido del fallo de la tutela T-282 que ordenaba poner fin a las condiciones indignas en que se encuentran confinados en esta cárcel que ellos denominan Guantánamo, pues **hasta el momento no se ha producido un cambio favorable para la garantía y respeto de los derechos humanos de estos reclusos**. ]
