Title: Comunidades Construyendo Paz temen rompimiento de conversaciones de paz
Date: 2015-05-22 14:46
Category: Nacional, Paz
Tags: cese al fuego unilateral FARC-EP, Comuidades Construyendo Paz, Conpaz, Conversaciones de paz de la habana, FARC, Frente Amplio por la PAz
Slug: comunidades-construyendo-paz-exigen-cese-bilateral
Status: published

<iframe src="http://www.ivoox.com/player_ek_4535496_2_1.html?data=lZqgl5mdeo6ZmKiakpeJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FtjLjWztvFb6ri1crU1MbSuMafxcqYzsaPscbnwpDRy9fJp9Xd18aYxsqPh7DCsabHj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jani Silva] 

Las **Comunidades Construyendo Paz "CONPAZ"** elaboraron un comunicado en el que exigen al gobierno nacional acordar un cese bilateral con la **guerrilla de las FARC** de manera inmediata.

De acuerdo con **Jani Silva**, miembro de la mesa directiva de CONPAZ, **es necesario que el Gobierno también de un paso que se sume a la voluntad** que ha presentado las FARC cuando decidieron realizar un cese unilateral el año anterior.

La lidereza asegura que **"**el cese bilateral se hace necesario, porque las personas que día a día están muriendo son del pueblo, tanto soldados como guerrilleros, esas muertes a nosotros nos duelen**"**.

La insistencia por acordar un cese bilateral para las comunidades que habitan en zonas de conflicto radica en que de acuerdo con Silva **"nos da temor que incluso el proceso de paz se llegue a romper, por que nosotros, los que vivimos en estos territorios somos los más afectados"**.
