Title: Después de 30 años continúa la búsqueda de justicia y verdad en el caso Carlos Pizarro
Date: 2020-04-26 10:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Fiscalía, M19
Slug: despues-de-30-anos-continua-la-busqueda-de-justicia-y-verdad-en-el-caso-carlos-pizarro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Carlos-Pizarro-Leongómez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Carlos Pizarro

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 26 de abril se cumplen 30 años del asesinato del político y máximo comandante del Movimiento 19 de abril, Carlos Pizarro Leongómez, tres décadas después de ocurrido el crimen, este permanece sin ser resuelto, evidenciando una ausencia de obligación estatal con la investigación, enjuiciamiento y sanción de todos los responsables del crimen, incluidos los agentes estatales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El asesinato de Pizarro, acribillado en 1990 dentro de una aeronave de Avianca, en presencia de su esquema de protección del antiguo Departamento Administrativo de Seguridad (DAS) hizo parte de una serie de homicidios cometidos contra importantes líderes políticos y sociales del movimiento AD/M-19 y de partidos de oposición, como la Unión Patriótica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde 2009 se solicitó llevar el caso a la Comisión Interamericana de Derechos Humanos ante la continúa impunidad y agotados los recursos disponible, **diez años después, en 2019 el caso fue finalmente admitido, según explica Javier Alfonso Galindo, abogado de la Corporación Colombiana de Juristas (CCJ)**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en el Sistema Interamericano ha existido un alto flujo de casos y pocos funcionarios que puedan dar respuestas a estas solicitudes, con este paso dado comenzará una etapa que permitirá indagar argumentos, alegatos y material probatorio para posiblemente expedir un informe que permitirá se avance en material internacional y garantizar a las víctimas el acceso efectivo a la justicia. [(Lea también: Con fallo de Lesa humanidad se avanza en justicia y verdad: hijo de Freytter Romero)](https://archivo.contagioradio.com/fallo-lesa-humanidad-justicia-verdad-freytter-romero/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Colombia la Fiscalía no avanza en el caso Pizarro

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en 2002 fueron condenados como reos ausentes los jefes paramilitares Carlos y Fidel Castaño, por su participación en el crimen, y en 2010 se declaró el homicidio de Carlos Pizarro como crimen de lesa humanidad, el Estado no ha custodiado de las pruebas ni ha dado un impulso a la investigación del caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Juan Carlos Niño, representante de las víctimas insiste en que la Fiscalía debe adelantar el proceso y sin embargo se trata como un caso más pues no ha sido asignado un solo fiscal para que le de prioridad como es el caso de Luis Carlos Galán.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En 2013, el arma con la que se asesinó a Carlos Pizarro y prueba esencial del caso, fue fundida por orden del fiscal encargado de su custodia, quien además de no haber sido investigado, hoy se encuentra en un cargo más alto en la Fiscalía General de la Nación. En la actualidad es imposible constatar la historia del arma y si pudo hacer parte de una compra estatal, desde la CCJ advierten que se trataría de una supresión de material probatorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**En 2017 la Fiscalía acusó a Jaime Ernesto Gómez Muñoz, exagente del DAS, parte del esquema oficial de protección de Carlos Pizarro**, de ser el coautor del asesinato como parte de una alianza criminal con otros actores. Actualmente el caso se encuentra en la JEP y se está esperando a que la jurisdicción tome una decisión frente al caso y si este caso tiene una relación con el conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado Niño relata que también han sido vinculado a los hechos otros exagentes del DAS de mayor jerarquía, como **Manuel Antonio González Henríquez, Flavio Trujillo Valenzuela quienes no han sido llevado a juicio y Miguel Alfredo Maza Márquez exdirector del DAS**, quien aún no ha sido vinculado a la investigación a pesar de que se le compulsaran copias en 2016, los tres están involucrados en otros homicidios de importantes líderes políticos de la época.

<!-- /wp:paragraph -->
