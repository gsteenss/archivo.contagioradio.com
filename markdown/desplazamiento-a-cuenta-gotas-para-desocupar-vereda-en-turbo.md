Title: Desplazamiento a cuenta gotas para desocupar vereda en Turbo
Date: 2016-08-01 14:17
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, Desplazamiento vereda Guacamayas, Restitución de tierras
Slug: desplazamiento-a-cuenta-gotas-para-desocupar-vereda-en-turbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Guacamayas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

###### [1 Ago 2016] 

Desde principios de junio, miembros del grupo paramilitar Autodefensas Gaitanistas vienen hostigando y amenazando a las familias que fueron desplazadas a finales de los 90 y que hoy reclaman la efectiva restitución de sus territorios en Guacamayas, zona rural del municipio de Turbo. El pasado viernes en horas de la noche los hombres armados rondaron algunas fincas y **le dijeron a dos familias que les daban dos días para desocupar sus predios**, según afirma el poblador Julio Correal, quien agrega que las familias tuvieron que huir a un pueblo cercano.

De acuerdo con Correal pese a las denuncias, el **Ejército asegura que no puede permanecer en la zona porque es muy grande** y que no pueden estar en dos sitios a la vez, razón en la que se escudan para no brindar las garantías de seguridad que los pobladores han exigido tanto al Gobierno colombiano, en cabeza de la Defensoría del Pueblo y la Personería, como a los organismos internacionales con quienes los pobladores no han logrado reunirse, a pesar de que ya les habían asegurado que irían a la zona para atender la emergencia que deben enfrentar.

"No tenemos seguridad y eso nos preocupa también porque la salida es muy peligrosa, tenemos que pasar por los sectores donde mantienen ellos", asegura Correal y agrega que el ambiente en la zona está muy tenso porque **las familias no quieren salir del territorio y "dejar la tierra a merced de los empresarios"**, por lo que se mantienen en su posición de resistencia, ante la [[falta de atención de las autoridades locales](https://archivo.contagioradio.com/autodefensas-gaitanistas-hostigan-a-familias-de-la-vereda-guacamayas-turbo/)], que les hace pensar que pueden haber vínculos entre estos grupos armados y las instituciones, que tampoco han brindado avances significativos para la restitución de sus tierras.

<iframe src="http://co.ivoox.com/es/player_ej_12407003_2_1.html?data=kpehkpyUdJShhpywj5WcaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjajTttPZwtGSlKiPlNDWzcbR0dePi9bVxMbaw97Ft4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
