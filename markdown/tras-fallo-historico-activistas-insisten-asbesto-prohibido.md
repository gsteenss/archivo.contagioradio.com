Title: Tras fallo histórico, activistas insisten que asbesto debe ser prohibido
Date: 2019-03-06 16:30
Author: CtgAdm
Category: Ambiente, Judicial
Tags: asbesto, colombia sin asbesto, ley ana cecilia niño
Slug: tras-fallo-historico-activistas-insisten-asbesto-prohibido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/asbesto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Greenpeace] 

###### [06 Mar 2019] 

En un fallo histórico, el juez [39 administrativo de Bogotá le ordenó al Ministerio de Trabajo y de Salud implementar una política de sustitución de asbesto por materiales menos dañinos, en un plazo de cinco años. Grupos en contra del uso de asbesto, como Greenpeace y la Fundación Ana Cecilia Niño, celebraron esta decisión porque afirmó que **la exposición a este material causa efectos nocivos para la salud y por tal razón, debería ser restringido por completo.**]

“Lo que dice esta sentencia es que el asbesto no es seguro y que los ciudadanos no pueden seguir expuestos a él, debido a sus nefastas y hasta mortales consecuencias. Es un fallo macizo y contundente que, además, deja expuesto que **el Estado y el sector privado han permitido el uso indiscriminado de esta peligrosa sustancia durante décadas**”, dijo Silvia Gómez, directora de Greenpeace Colombia, en un comunicado.

Esta decisión jurídica se da a conocer después de más de una década de fallidos intentos de prohibición del asbesto en el Congreso. Daniel Pineda, líder del movimiento Colombia sin asbesto, sostiene que este fallo deja al Congreso sin argumentos para dilatar el proyecto de ley "Ana Cecilia Niña", nombrada por la esposa de Pineda quien murió en el 2017 de cáncer de mesotelioma tras años de exposición al material. (Le puede interesar: "[En Colombia mueren 320 personas el año por cáncer asociado al asbesto](https://archivo.contagioradio.com/en-colombia-mueren-320-personas-al-ano-de-cancer-asociado-a-la-inhalacion-de-asbesto/)")

Pineda, quien también es director de la Fundación Ana Cecilia Niño**, **insiste que **este material carcinógeno debe ser prohibido** con este proyecto de ley en el Congreso para proteger la salud de miles de colombianos. Según el Instituto Nacional de Cancerologia, la inhalación o manipulación del asbesto causa **cánceres de pulmón, laringe, ovario y mesotelioma, así como asbestosis**. En Colombia, se registran al menos 540 casos de cáncer del púlmon cada año en el país por asbesto, según la Contraloría.

Sin embargo, Pineda afirma que "no existe un protocolo en Colombia o una normatividad que sea exigente alrededor del manejo del asbesto". Lo que es particularmente preocupante porque esta sustancia se usa para fabricar varios materiales en el país como tejas, envases médicos, frenos para vehículos, tanques de agua y tuberías. Asimismo, Pineda sostiene que se usa como ingrediente en ciertos polvos exportados de China.

Al respecto del fallo, Pineda lo calificó como "fuerte y contundente", pero sufre de limitaciones. Por un lado, el líder indica que **la remoción de este material se puede realizar en solo seis meses**, en cambio de los cinco años que plantea el juez, porque "l[as empresas dicen que ya tienen los elementos para sustituir el asbesto". Por el otro lado, el fallo no incluye un componente educativo sobre el uso de esta sustancia, lo cual es necesario porque "el desconocimiento causa todavía más víctimas".]

Este mes, se presentará la ley Ana Cecilia Niña en la Comisión Séptima de la Cámara de Representantes con la intención de que sea debatida y aprobada en sesión plenaria.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
