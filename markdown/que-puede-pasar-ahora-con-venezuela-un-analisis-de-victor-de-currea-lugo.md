Title: ¿Qué puede pasar ahora con Venezuela? análisis de Víctor de Currea Lugo
Date: 2019-03-04 16:19
Category: Paz, Política
Tags: Democracia en Venezuela
Slug: que-puede-pasar-ahora-con-venezuela-un-analisis-de-victor-de-currea-lugo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/We_Are_Millions_march_Venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Dominio público 

###### 4 Mar 2019 

Con el regreso del líder opositor Juan Guaidó a Venezuela son muchas las dudas que surgen sobre cuál será el destino del país vecino. Esto después, de que el autoproclamado presidente, con la ayuda de otros dirigentes de Suramérica, entre ellos el presidente Duque, convocaran un evento que no recaudó monetariamente lo esperado y que no movilizó el suficiente número de personas para gestar un movimiento en el pueblo.

El profesor y analista **Víctor de Currea Lugo** formula que ante el "fracaso estrepitoso de la mal llamada ayuda humanitaria con la que se esperaba convocar a las multitudes",  se podría pensar en una eventual salida militar, que ofrecería tres opciones para una eventual transición en el Gobierno, la primera:  una fractura dentro de las Fuerzas Militares de Venezuela que dén paso a un golpe militar, la segunda: una rebelión popular, "lo que sería mucho más lento y sangriento, descartándolas  pues no se ve en realidad "una insurrección popular armada"

Por tal motivo, el profesor indica que la única opción viable y su tercer análisis, sería una  intervención militar por parte de fuerzas extranjeras que podría darse y que de ser así sería muy similar a lo que sucedió durante la década de los 2000 con Irak, cuando EE.UU ingresó al país con un fin extractivo frente al petroleo.

Sin embargo, indica que **en cualquiera de dichas opciones, debido a la alta migración, el país más golpeado "sin duda sería Colombia"** **e** incluso podría verse involucrada en escenarios de acciones militares.

**Hay que leer qué es lo que está pasando realmente en Venezuela**

El analista manifiesta que en el caso de Venezuela cualquier pretexto es valido para que suceda una intervención militar y recuerda la situación vivida en 2003, cuando a pesar que el Consejo de Seguridad de la ONU y millones de personas en el mundo se opusieron, el Ejército de EE.UU efectuó una incursión fallida en el país.

De igual forma, **advierte que el desabastecimiento en el país vecino es real y llega al 85%, sin embargo, indica que se trata de una problemática que tiene varias aristas.** Además del bloqueo económico: como la inflación y el contrabando extractivo, por lo que recomienda que "no podemos atarnos a una sola respuesta de un fenómeno tan complejo". Finalmente indica que es necesario crear un frente contra la guerra que no puede ir ligado a la aprobación de las acciones del presidente Maduro.

###### Reciba toda la información de Contagio Radio en [[su correo]
