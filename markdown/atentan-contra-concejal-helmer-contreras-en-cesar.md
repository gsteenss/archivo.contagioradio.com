Title: Atentan contra Concejal Helmer Contreras en Cesar
Date: 2020-11-10 09:34
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Atentado, Concejal Helmer Contreras, Partido Verde
Slug: atentan-contra-concejal-helmer-contreras-en-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Atentan-contra-Helmer-Contreras-del-Partido-Verde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@AntonioSanguino](https://twitter.com/AntonioSanguino)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 9 de noviembre en horas de la mañana el Concejal del Partido Verde, Helmer Contreras Ortega fue víctima de un atentado en la terminal de transporte de la ciudad de Aguachica, departamento del Cesar.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AntonioSanguino/status/1325834225770229766","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AntonioSanguino/status/1325834225770229766

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con el concejal**, un hombre lo estaba esperando fuera del lugar** **y cuando llegó a su camioneta, el hombre sacó un arma de fuego.** «El man desenfunda un revolver, yo lo empujo, salgo corriendo y él saca un poncho , de ahí saca un revolver y dispara; cuando yo salgo corriendo **el man me persigue con el revolver en la mano mientras yo grito "me van a matar, me van a matar"»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El autor del ataque, quien no ha podido ser identificado hasta el momento, huyó del lugar en una moto que lo estaba esperando a las afueras del terminal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Helmer Contreras agregó que “no sé si sería un atraco, lo que yo sí quiero es que las autoridades investiguen este caso porque ya hemos venido recibiendo hace días amenazas”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El concejal se ha destacado por adelantar labores de control político a la administración municipal y ha denunciado presuntos hechos de corrupción dentro de la misma.** La denuncia más reciente que se hizo fue el hallazgo de aproximadamente 1.000 mercados que se dañaron en una oficina de la alcaldía.(Le puede interesar: [Es asesinado Jorge Solano, reconocido líder social de Ocaña](https://archivo.contagioradio.com/es-asesinado-jorge-solano-lider-social-de-ocana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, Contreras, junto a tres concejales y un líder social, ya había recibido amenazas en contra de su vida el pasado mes de julio al haber recibido un sufragio que contenía flores una cruz y una crema utilizada para calmar el dolor . (Le puede interesar: [Atentan contra el senador Feliciano Valencia](https://archivo.contagioradio.com/atentan-contra-la-vida-del-senador-feliciano-valencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los concejales habían afirmado que estas intimidaciones podían estar relacionadas con las denuncias que habían realizado con respecto a presuntos hechos de corrupción en la contratación de ayudas para enfrentar la emergencia por la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A raíz de esta amenaza, se han llevado a cabo dos consejos de seguridad, sin embargo, el político ha indicado que las investigaciones sobre las amenazas no han avanzado.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
