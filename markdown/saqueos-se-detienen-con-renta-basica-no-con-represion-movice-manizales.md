Title: Saqueos se detienen con renta básica no con represión: MOVICE Caldas
Date: 2020-04-24 17:37
Author: AdminContagio
Category: Actualidad
Tags: manizales, MOVICE, Renta básica
Slug: saqueos-se-detienen-con-renta-basica-no-con-represion-movice-manizales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Manizales-y-Covid-19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Medea\_material {#foto-medea_material .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) capítulo Caldas alerta sobre posibles violaciones a los derechos humanos en el marco del aislamiento preventivo, por cuenta de la creación de grupos antisaqueos y antiatracos creados por la Policía en Manizales, pese a que en la ciudad no se han presentando hechos que justifiquen esta acción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según denunció el MOVICE, la Policía de Manizales anunció el pasado 1 de abril en su página la creación de grupos Antisaqueo y Antiatracos compuestos por 172 efectivos, distribuídos por las 12 comunas de la ciudad para "brindar seguridad a los comerciantes y garantizar la cadena de abastecimiento".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Víctor Parra, secretario técnico del Movimiento en Caldas señala que tienen "gran preocupación" ante la posibilidad de responder de manera excesiva a posibles saqueos o actos motivados por el desempleo, o el hambre en un estado de emergencia como el que estamos ahora; y aclara que hasta la fecha no se han presentado abusos por parte de la Fuerza Pública ni intentos de saqueo en la ciudad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Parra sostiene que la preocupación también se explica en que no hay claridades sobre las funciones que cumplirán estos grupos ni el grado de fuerza que pueden usar; así como tampoco su composición, es decir, si serán integrantes del Ejército dotados con armas largas o uniformados de la Policía. (Le puede interesar: ["Las razones para pedir a la Corte Constitucional la revisión del decreto de estado de emergencia"](https://archivo.contagioradio.com/las-razones-para-pedir-a-la-corte-constitucional-la-revision-del-decreto-de-estado-de-emergencia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El manejo de la Pandemia en Manizales

<!-- /wp:heading -->

<!-- wp:paragraph -->

El [MOVICE](https://movimientodevictimas.org/alerta-sobre-posibles-violaciones-de-derechos-humanos-en-la-ciudad-de-manizales-en-el-marco-de-la-cuarentena-y-el-aislamiento-social/)señala que en la ciudad el 40,7% de la población se encuentra en la informalidad y se estima que habitan más de 16 mil víctimas del conflicto armado, "de las cuales 1.634 no puede acceder efectivamente a medidas de atención y reparación", lo que se traduce en una vulnerabilidad económica en un sector de los habitantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero Parra sostiene que desde la Alcaldía se ha dado seguimiento tanto a los casos reportados de Coronavirus como a las personas que requieren asistencia del Estado, de tal forma que las ayudas están llegando a los barrios más necesitados. Pese al reconocimiento, el integrante del MOVICE también resaltó que se requieren medidas de largo aliento para enfrentar la pandemia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es con el uso de la Fuerza, es con la renta básica

<!-- /wp:heading -->

<!-- wp:paragraph -->

Reconociendo que un mercado puede durar máximo un mes, Parra recuerda que una propuesta relevante en este momento es la de una renta básica para aliviar "algo que no es resultado del Covid 19, sino de unos problemas históricos y estructurales de desigualdad que se han visto potenciados por el estado de emergencia".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto, concluye que es con políticas públicas que permitan redistribuir de manera efectiva la riqueza como se saldrá de la crisis, y no mediante el uso de la fuerza pública, porque "la represión no calma el hambre ni los problemas de fondo". (Le puede interesar:["De la renta básica, a la garantía total de derechos"](https://archivo.contagioradio.com/de-la-renta-basica-a-la-garantia-total-de-derechos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
