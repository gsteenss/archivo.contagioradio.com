Title: Desde el Congreso buscan alejar a los niños de la violencia de la tauromaquia
Date: 2016-04-12 13:38
Category: DDHH, Nacional
Tags: Derechos de los niños, ONU, tauromaquia
Slug: desde-el-congreso-buscan-alejar-a-los-ninos-de-la-violencia-de-la-tauromaquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Violencia-de-la-Tauromaquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Change ORG 

###### 12 Abr 2016 

Pese a que la Organización de las Naciones Unidas ha instado a Colombia a apartar la infancia de **"la violencia de la tauromaquia", como la misma ONU lo ha calificado**, en el país los niños y niñas siguen siendo participes de este tipo de espectáculos crueles con animales, vulnerando sus derechos.

Esa situación se evidenció en la sesión celebrada en Ginebra los días 21 y 22 de Enero de 2015, cuando la delegación enviada por **el Gobierno colombiano, no ofreció explicaciones al Comité respecto a las medidas que se tomarán desde el Ejecutivo** para evitar que se sigan vulnerando los derechos del niño en las actividades taurinas como corralejas o corridas de toros.

Es por eso, que la Bancada Animalista del Congreso de la República ha citado a la ciudadanía y los congresistas al “Foro Infancia sin viOLEncia hacia las recomendaciones de la ONU” este 12 de abril a las 2:30 de la tarde en el salón Boyacá.

Allí se evidenciará las implicaciones que existen cuando **los niños asisten a este tipo de espectáculos desde los 10 años acompañados por sus padres**, como lo permite el Estatuto Taurino. Además desde cortas edades ingresan a las escuelas taurinas, e incluso, son impulsados a ingresar como al mundo laboral de la tauromaquia como torerillos.

Es por eso que la ONU ha instado a que se tomen las  medidas legales, administrativas y educativas para prohibir el ingreso de niños, niñas y adolescentes, a este tipo de actividades, pues **son "peligrosas y degradantes para los niños”** explica Anna Mulá, coordinadora de la Campaña Infancia sin violencia de la Fundación Franz Weber.

En el foro, la Bancada Animalista del Congreso anunciaría que tipo de medidas se van a implementar en Colombia para asegurar la aplicación de la Convención Internacional sobre los Derechos del Niño, por lo cual **se esperaría que mediante le Ley se aleje a la infancia de la violencia de la tauromaquia.**

<iframe src="http://co.ivoox.com/es/player_ej_11141082_2_1.html?data=kpaelpaUfJOhhpywj5WaaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5yncaXZ1MnSjcrQb6Tjz8zfx9jTb8Pp1MjO0JDFsMbewteYw5DQs9Sfz86SpZiJhpLj1JDRx5DQpYzqytTZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

[![infancia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/infancia.png){.aligncenter .size-full .wp-image-22545 width="480" height="480"}](https://archivo.contagioradio.com/desde-el-congreso-buscan-alejar-a-los-ninos-de-la-violencia-de-la-tauromaquia/infancia/)

 
