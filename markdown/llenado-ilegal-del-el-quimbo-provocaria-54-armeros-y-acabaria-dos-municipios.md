Title: Llenado ilegal del El Quimbo provocaría 54 "Armeros" y acabaría dos municipios
Date: 2015-07-02 14:15
Category: Ambiente, Nacional
Tags: ANLA, ASOQUIMBO, Capilla San José de Belén, Carlos Mauricio Iriarte, Codensa, EMGESA, Germán Vargas Lleras, gobernación de Huila, Huila, José Antonio Vargas Lleras, Juan Manuel Santos, llenado de El Quimbo, Patrimonio cultural, Quimbo
Slug: llenado-ilegal-del-el-quimbo-provocaria-54-armeros-y-acabaria-dos-municipios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Llenado-de-El-Quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.tusemanario.com]

<iframe src="http://www.ivoox.com/player_ek_4714916_2_1.html?data=lZyelp6Veo6ZmKiak5qJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkM3Zz8bR0ZDNsMbbwtGYxsrQb6bgjLbiy9LGs4zk09Tj0cjFtoa3lIqupsaPeZWfhpefo9fRqdPj1IqflJDdcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, ASOQUIMBO] 

La gobernación del Huila y ASOQUIMBO, denuncian que el **llenado del embalse "El Quimbo", fue autorizado por el presidente Juan Manuel Santos de manera ilegal.** De acuerdo con Miller Dussán, investigador de ASOQUIMBO, la ANLA permitió el llenado de manera irresponsable, teniendo en cuenta que la licencia ambiental  había establecido condiciones para realizar dicho procedimiento, y ninguna de estas ha sido cumplida.

Pese a que existían dos resoluciones de la ANLA en las que se estipulaban condiciones para realizar dicho procedimiento, nunca fueron legalizadas. Allí se establecían  que el embalse tenía que estar adecuado para el llenado, la obligación de **compensar los daños ambientales y sociales,** y **debía existir un sistema de estudio de seguimiento sísmico**, teniendo en cuenta que en ese terreno hay fallas geotécnicas, señala Dussán.

**El gobernador del Huila, Carlos Mauricio Iriarte,**  está sorprendido al saber que Santos habría dado un espaldarazo al llenado de la hidroeléctrica, lo que implicaría que el presidente estaría actuando en contra de sus propias normas, faltando además al acuerdo registrado en las actas de la Comisión de Concertación y seguimiento a El Quimbo. Iriarte, califica como** “poco oportuno y una burla”** el llenado del embalse.

Además, desde la gobernación se había firmado un acta, en la que se establecía que **EMGESA no podría realizar ese procedimiento hasta no reparar a las familias afectadas por la represa,** aún así desde las 5 de la mañana del martes se inició el llenado, sin tener en cuenta que de 5200 hectáreas de tierras que tenían que ser restituidas a las víctimas, “no ha sido restituido un milímetro”, como lo asegura el investigador de ASOQUIMBO.

Este tipo de procedimientos traerán consecuencias negativas para el ambiente y la vida de los pobladores que subsisten del río, afirma Dussan y alerta sobre el alto riesgo de sismicidad que provocaría **54 “Armeros”** aludiendo a la tragedia vivida en 1985, **arrasando con dos municipios. Además,** el llenado ocasionará la **muerte de 30 mil toneladas de peces y afectará notablemente el ecosistema del río Magdalena.**

El investigador, comenta que el pasado 27 de junio, un grupo de personas de la empresa **“Bautista y Bautista”, contratista de EMGESA,** llegaron en camionetas, motos y con maquinaria hasta  "San José de Belén" vereda del Municipio del Agrado, Huila, área de influencia de El Quimbo, **con el propósito de iniciar el desmonte de la Capilla** ubicada en esa zona rural.** **

[![capilla](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/capilla.jpg){.aligncenter .size-full .wp-image-10756 width="500" height="333"}](https://archivo.contagioradio.com/llenado-ilegal-del-el-quimbo-provocaria-54-armeros-y-acabaria-dos-municipios/capilla/)

**La capilla declarada Patrimonio Cultural del Departamento del Huila,** mediante Decreto No. 423 de 1982, no fue derrumbada por la intervención de la comunidad, y aunque EMGESA aseguro en el pasado que garantizaría el traslado integral de la iglesia, según Miller Dussán, es claro que la empresa quiere inundarla.

Cabe recordar que José Antonio Vargas Lleras, **hermano del Vicepresidente de la república Germán Vargas Lleras, hace parte de la junta directiva de EMGESA y CODENSA,** relación que derivaría en una decisión a favor de intereses familiares que atentan “la soberanía, la región y las comunidades”, expresa Dussán.

El próximo 10 de julio, la comunidad iniciará una manifestación desde las nueve de la mañana en todo el departamento del Huila, como rechazo manifiesto a esta decisión. Desde la gobernación, como desde ASOQUIMBO, se anuncian acciones jurídicas.
