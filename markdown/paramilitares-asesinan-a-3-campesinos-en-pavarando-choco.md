Title: Paramilitares asesinan a 3 campesinos en Pavarandó, Chocó
Date: 2016-01-13 17:35
Category: DDHH, Nacional
Tags: campesinos asesinados en Pavarandó, Paramilitares en Chocó
Slug: paramilitares-asesinan-a-3-campesinos-en-pavarando-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Notimundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimundo ] 

###### [13 Ene 2016 ] 

[De acuerdo con la denuncia de la ‘Comisión Intereclesial de Justicia y Paz’ a finales del mes de diciembre **miembros de estructuras paramilitares asesinaron a 3 campesinos en el corregimiento de Pavarandó**, municipio de Mutatá, en el marco de la reactivación de su plan de control territorial y ante la presencia de la Policía de Urabá y el Batallón de Selva No. 54.]

[Según indicaron los pobladores, el 24 de diciembre en horas de la mañana identificaron **paramilitares vestidos de civil con radios de comunicación y armas cortas en Pavarandó, Llano Rico, San Rafael, Cetino, Brisas, Pueblo Pipón y El Cerrao**, dónde intimidaron a la comunidad levantándose las camisas y afirmando “miren el regalito que nos trajo el niño Dios, necesitamos que nos arrienden casas por que venimos para quedarnos”.]

[El pasado 9 de enero, el paramilitar conocido con el alias de ‘Paleto’ arribó en una de las fincas que presenta problemas de linderos no resueltos por el INCODER, dentro del proceso de restitución ordenado por la Corte Constitucional. **El armado manifestó a uno de los ocupantes de buena fe que se estaba metiendo en tierras ajenas** y que el ‘El Patrón’, venia bajando para ordenarlas y organizarlas, recordando que por esos territorios habían ejecutado a Argenito Díaz y Manuel Ruíz.]

[**Para algunos pobladores ‘El Patrón’ sería Freddy Rendón Herrera**, conocido con el alias del ‘Alemán’, quien el año pasado recobró la libertad luego de cumplir su condena en el marco de la ley 975 de ‘Justicia y Paz’. **Otros, consideran que se trata de testaferros de empresarios antioqueños** con proyecciones de inversión en agronegocios, como es el caso de Darío Montoya quien entregó las tierras que habría malogrado para la construcción inconsulta del Batallón de Selva No. 54.]
