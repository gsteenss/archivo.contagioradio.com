Title: Indígenas U'wa se toman planta de Ecopetrol ante incumplimientos del gobierno
Date: 2016-07-21 15:07
Category: Nacional
Tags: Ecopetrol, indígenas, Norte de Santader, Uwa
Slug: indigenas-uwa-se-toman-planta-de-ecopetrol-antes-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/uwa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

###### [21 Jul 2016] 

Cerca de **400 indígenas de la Nación U'wa, decidieron tomarse las instalaciones de Ecopetrol en Toledo, Norte de Santander** donde actualmente se encuentra ubicada la planta de gas Gibraltar, cuyo territorio ha pertenecido ancestralmente a la comunidad, que ha tenido que sufrir serias consecuencias ambientales  y culturales tras la construcción de la planta.

Es por ello, que desde este miércoles las instalaciones de la planta de gas Gibraltar estan bajo el control en el ingreso y salida por parte del personal de la Guardia y la Nación U'wa, como lo han asegurado en un comunicado, donde también señalan que **el objetivo de esta acción es presionar al gobierno para que cumpla con lo pactado en mayo de 2014.**

Los indígenas exigen el reconocimiento de sus títulos coloniales, el saneamiento del resguardo, el pago de la deuda histórica con la comunidad, y que las áreas protegidas sean declaradas en coordinación con el saber ambiental de las comunidades, entre otras reclamaciones.

La Nación indígena U'wa ubicada en los departamentos de Boyacá, Arauca, Norte de Santander, Casanare, Santander **es una de las comunidades indígenas en peligro de desaparecer según la Corte Constitucional.** [Los indígenas han debido enfrentarse a diferentes amenazas](https://archivo.contagioradio.com/las-luchas-de-la-nacion-indigena-uwa/) entre las que se encuentra el modelo extractivista y el ecoturismo, generando “la destrucción de sus santuarios y sitios sagrados, rompiendo el equilibrio natural y espiritual a raíz de la extracción de los recursos naturales, que dan la supervivencia de toda la vida existente”, asegura la Asociación de Autoridades Tradicionales de Cabildos U’wa ASOU’WA.

De acuerdo con Bladimir Moreno, presidente de ASOU’WA, de parte del gobierno no hay voluntad política para cumplir a la comunidad por lo que desde hace 50 días han decidido  concentrarse en los puntos de Zizuma, la China, Cubogón "Callejón de la muerte" y fincas Santa Rita, Bellavista y Vega Rica (Pozo de gas Gibraltar). Los indígenas esperan avances con el gobierno nacional, y señalan que **hasta que no les defina soluciones concretas no saldrán de las instalaciones de Ecopetrol.**

<iframe src="http://co.ivoox.com/es/player_ej_12297450_2_1.html?data=kpefm5yYeZGhhpywj5WcaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncbfgwsnWz87Wb67j08rb0YqWh4y1tLTCh6qWaZmkhp6muaaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
