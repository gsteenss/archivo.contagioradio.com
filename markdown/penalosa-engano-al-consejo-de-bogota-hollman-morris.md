Title: Peñalosa engañó al Concejo de Bogotá: Hollman Morris
Date: 2018-08-06 15:26
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Hollman Morris, metro elevado
Slug: penalosa-engano-al-consejo-de-bogota-hollman-morris
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cb2cbcb05372f3c0e805aeed82be2db5_1475097756_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Espectador ] 

###### [06 Ago 2018] 

El concejal por la Alianza Progresista Hollman Morris, instaurará una demanda contra el Alcalde de Bogotá por los delitos de fraude procesal y prevaricato que se habrían dado en el proyecto para la construcción del metro elevado en Bogotá. De acuerdo con Morris, Peñalosa **"engañó al Concejo"** al hacer que este aprobara una iniciativa sin los estudios suficientes.

Morris manifestó que en septiembre del año pasado el Consejo aprobó una vigencias ordinarias por un valor de **7 billones de pesos**, hecho que no podría haberse dado, debido a que "para ese momento no se contaba con los estudios de factibilidad", situación que para el concejal se traduce en fraude.

Según Morris, la Alcaldía le habría afirmado a la instancia correspondiente en el gobierno nacional, tener los estudios de factibilidad para que esta le otorgara los documentos CONPES. Posteriormente se le manifestó al Concejo de Bogotá que ya se tenía, tanto el estudio de factibilidad como los documentos CONPES, para aprobar las vigencias futuras.

"Si usted no tiene los estudios de factibilidad y aprueba recursos esta faltando a la ley y los sobrecostos v**an a ser muchos, mientras que los riesgos de corrupción serán todos"** señaló Hollman Morris.

### **Las pruebas de Morris contra el Metro Elevado** 

Las pruebas que según el concejal, demuestran que la Alcaldía no tenía los documentos para que se aprobará el Metro elevado ya fueron presentadas a la Fiscalía. Entre ellas estaría el llamado de atención que hizo el Consorcio SENER, interventor del proyecto, en el mes de enero, señalando que **"encontró inconsciencia de fondo en la presentación que hizo ySstra Ingetec, contratado para hacer el proyecto"**.

En mayo, Morris afirmó que se modificó un otrosi, de manera ilegal, para ampliar el plazo del contrato a 27 meses, con el objeto de hacer los estudios de factibilidad. (Le puede interesar: ["Consejo de Bogotá parece un comité de aplausos para Peñalosa: Manuel Sarmiento](https://archivo.contagioradio.com/concejo-de-bogota-parece-un-comite-de-aplausos-para-penalosa-manuel-sarmiento/)")

Si en el proceso investigativo que abra la Fiscalía se comprueba que hay una cantidad de documentación adecuada par dar paso a la demanda, la puesta en marcha del Metro elevado podría detenerse.

<iframe id="audio_27619650" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27619650_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
