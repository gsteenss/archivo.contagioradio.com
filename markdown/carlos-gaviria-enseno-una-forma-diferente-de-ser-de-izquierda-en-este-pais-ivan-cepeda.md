Title: “Carlos Gaviria enseñó una forma diferente de ser de Izquierda en este país” Iván Cepeda
Date: 2015-04-01 18:07
Author: CtgAdm
Category: Entrevistas, Política
Tags: carlos gaviria, Corte Constitucioinal, Ideas de Carlos Gaviria, Iván Cepeda, Izquierda en Colombia
Slug: carlos-gaviria-enseno-una-forma-diferente-de-ser-de-izquierda-en-este-pais-ivan-cepeda
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4295520_2_1.html?data=lZeml5qWdI6ZmKiak5iJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nh8LmzdTgjazFusrmysaYx9PXqYa3lIqvk4qnd4a2lJDi0MaPqtDmzsaYxs7KqdPZz9nSjcnJb9TZ05DRj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Senador Iván Cepeda] 

“Cómo hace de falta Carlos Gaviria en un país que ve tan estridentes escándalos en la Corte Constitucional” señala el Senador Iván Cepeda, que resalta que el ex magistrado de la Corte Constitucional, ex candidato presidencial, ex senador de la república, quien fallece este 31 de Marzo en la clínica Santa Fe en Medellín.

Para Cepeda, Gaviria fue un revolucionario en el campo del derecho constitucional que también introdujo jurisprudencia muy progresista con relación a los derechos humanos, la autonomía y la libertad personales y por ello es muy digno de ser recordado y estudiado, además de “un ejemplo que debemos seguir”. “En el campo de la jurisprudencia… un legado de que hay que estudiar” señala Cepeda.

En el plano político Gaviria enseño, según Cepeda, que hay una manera diferente de ser de izquierda, una manera abierta, argumentativa, una izquierda que puede resolver los problemas, no solamente describirlos y enunciarlos.

En el plano personal, Gaviria era un hombre de ideas, afirma el Senador Cepeda con una gran formación e intelectualmente muy capaz, con una gran inteligencia y una visión estética de la vida, también muy profundo en su forma de argumentar. Era muy generoso, prestaba mucha atención a desarrollar diálogos profundos con personas que no tienen acceso a la justicia, con sectores campesinos y otros, nunca se negó a tener esos diálogos.
