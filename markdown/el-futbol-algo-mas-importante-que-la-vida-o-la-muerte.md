Title: El fútbol ¿Algo más importante que la vida o la muerte?
Date: 2015-12-24 08:57
Category: Javier Ruiz, Opinion
Tags: corrupción, futbol
Slug: el-futbol-algo-mas-importante-que-la-vida-o-la-muerte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/futbol1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

#### **[Javier Ruíz](https://archivo.contagioradio.com/javier-ruiz/)- [@ContraGodarria ](http://contragodarria/)** 

###### 23 Dic 2015 

Alguna vez el técnico inglés Bill Shankly  dijo lo siguiente: “Algunos creen que el fútbol es solo una cuestión de vida o muerte, pero es algo mucho más importante que eso” y posiblemente eso se ve acá en estos lares porque Colombia es un país muy futbolero, donde cada uno de sus habitantes se apasiona mucho por este deporte y dejan de lado sus preocupaciones para poder disfrutar un partido de la selección nacional o un partido importante que está disputando algún club de nuestro fútbol local.

Dante Panzieri alguna vez dijo que el fútbol es el deporte más democrático de todos en donde los pobres podían tener acceso y porque ningún otro deporte había penetrado tanto en barrios pobres, favelas, barrios obreros, etc. Es un deporte que despierta una gran pasión y quizá una gran dedicación de tiempo cuando hablamos de esto con los amigos, vecinos, compañeros de trabajo o la misma familia y da la ilusión de que no hay exclusiones de ningún tipo.

Hace poco se disputo una nueva final del fútbol colombiano entre dos equipos denominados grandes porque se han destacado, tanto en el torneo local, como en sus participaciones en los torneos internacionales a nivel de clubes. Atlético Nacional de Medellín contra Atlético Junior de Barranquilla querían a toda costa ganar el trofeo del segundo semestre y darle la gloría a sus hinchas, a sus ciudades y a sus regiones.

Como es costumbre, el país se paraliza para saber quién va a quedar campeón. Los hinchas impregnan de alegría a sus ciudades y dan la sensación que las diferencias políticas, ideológicas y socio-económicas desaparecieran para formar una unidad en torno al equipo que están apoyando. Lo anterior es una expresión que le da colorido al fútbol como espectáculo pero si ese fanatismo no se sabe manejar se originan los regionalismos que en algunos casos han causado actos de violencia y muertes por no tolerar las diferencias, acusando a sus rivales de “provincianos” por no pertenecer a su ciudad y que al día de hoy sigue siendo uno de los males endémicos del fútbol colombiano.

Por otro lado, algunos hinchas han notado que con la irrupción del denominado “fútbol moderno” acá en el país han querido quitarle el carácter democrático a este bello deporte para convertirlo en una mercancía más de consumo (negocio) y no en lo que es, en un bien público. Un ejemplo de esto son las transmisiones de TV en donde el que quiere acceder tiene que pagar hasta sumas altas de dinero para poder ver a su equipo favorito por TV excluyendo a muchos fanáticos de poder  seguir a su equipo generando monopolios para favorecer a un grupo económico en particular que entendieron que la pasión al fútbol es un negocio rentable.

De volver el fútbol en mercancía rentable se han desatado los peores males y las peores perversiones sobre este deporte como los partidos amañados, actos de corrupción en las directivas del fútbol, involucramiento de mafias, narcotráfico, y criminalidad y mala gerencia en los equipos colombianos que han manchado la pelota, al decir de Maradona, y han dejado un manto de duda sobre algunos títulos ganados de algunos equipos colombianos.

Terminó un torneo más y Nacional se alzó con la copa. Celebran los “verdolagas” y los “tiburones” tristes esperaran una nueva oportunidad.

Puede que el fútbol sea algo más que un asunto de vida o muerte y como decía el entonces secretario general del partido comunista italiano Enrrico Berlinguer: “Señores, lo más importante para el pueblo es el partido y el partido empieza a las ocho”.
