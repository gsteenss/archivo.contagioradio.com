Title: Los «castigos brutales» con los que Grupos Armados instauran su propio régimen contra el Covid-19
Date: 2020-07-15 22:46
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Grupos armados ilegales
Slug: los-castigos-brutales-con-los-que-grupos-armados-instauran-su-propio-regimen-contra-el-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Grupos-Armados-AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Human Rights Watch -HRW- denunció este miércoles que grupos armados como el ELN, disidencias de las FARC y estructuras paramilitares como las Autodefensas Gaitanistas de Colombia -AGC-, **han cometido asesinatos y otros abusos contra civiles para imponer sus propias medidas en contra de la propagación del Covid-19.**   
  
La ONG manifestó que desde la llegada del nuevo coronavirus a Colombia, grupos armados en varias partes del país han impuesto «toques de queda, cuarentenas y otras medidas para evitar que se propague el virus», **llegando incluso a asesinar a quienes consideran que no están cumpliendo sus reglas.** (Lea también: [Siguen los asesinatos contra líderes y excombatientes en medio de la pandemia](https://archivo.contagioradio.com/siguen-los-asesinatos-contra-lideres-y-excombatientes-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Este brutal control social refleja las históricas falencias del Estado para establecer una presencia significativa en zonas remotas del país que permita proteger a las comunidades en riesgo»**
>
> <cite>José Miguel Vivanco, director de Human Rights Watch para las Américas </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

José Miguel Vivanco, director para las Américas de HRW, hizo un llamado al Gobierno Nacional para hacer presencia efectiva en los territorios y proteger a la población civil que está siendo víctima de «castigos brutales» por parte de estos grupos armados. (Le puede interesar: [No aparece el informe de la Comisión Presidencial de Excelencia Militar: HRW](https://archivo.contagioradio.com/no-aparece-el-informe-de-la-comision-presidencial-de-excelencia-militar-human-rights-watch/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El gobierno debe intensificar sus esfuerzos para proteger a estas comunidades, garantizar que tengan acceso adecuado a alimentos y agua, así como para proteger su salud de los efectos del Covid-19»**
>
> <cite>José Miguel Vivanco, director de Human Rights Watch para las Américas</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Al menos 9 asesinados y 10 heridos han sido registrados

<!-- /wp:heading -->

<!-- wp:paragraph -->

HRW **documentó nueve asesinatos en tres departamentos de Colombia que estuvieron vinculados con medidas impuestas por los grupos armados**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ocho civiles habrían sido asesinados por «no cumplir con las medidas» y la víctima restante fue un líder comunitario que, al parecer, habría sido asesinado por denunciar la imposición de este régimen ante las autoridades locales en Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También se han registrado un total de diez personas heridas y la incineración de algunos vehículos en los ataques cometidos por estos grupos armados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 11 departamentos han sido blanco de las imposiciones de estos Grupos Armados

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según las investigaciones de HRW,  los grupos armados informaron a las poblaciones locales que estaban imponiendo reglas para evitar la propagación del Covid-19 en al menos 11 de los 32 departamentos de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Arauca, Bolívar, Caquetá, Cauca, Chocó, Córdoba, Guaviare, Huila, Nariño, Norte de Santander y Putumayo son los territorios que fueron blanco de estas imposiciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de panfletos y mensajes de Whatsapp, los grupos armados alertan a las comunidades sobre la adopción de medidas que incluyen toques de queda; cuarentenas; restricciones a la circulación de personas, automóviles y embarcaciones; límites con respecto a los días y horarios de apertura de tiendas; y hasta la prohibición de acceso a los territorios a extranjeros y personas de otras zonas del país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/hrw_espanol/status/1283338099750318082","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/hrw\_espanol/status/1283338099750318082

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
