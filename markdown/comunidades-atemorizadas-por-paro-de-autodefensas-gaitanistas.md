Title: Paro armado de Autodefensas Gaitanistas se extendería hasta el domingo
Date: 2016-03-31 12:43
Category: Nacional
Tags: Autodefensas Gaitanistas de Colombia, Paro armado paramilitarismo
Slug: comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Autodefensas-Gaitanistas-e1459446173432.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Stemmer fra Latin-Amerika 

###### [1 Abril 2016] 

El grupo paramilitar 'Autodefensas Gaitanistas de Colombia', ha circulado cientos de panfletos amenazantes en diferentes zonas del norte de Colombia donde se anuncia paro armado, advirtiéndole a las comunidades el **cese de sus actividades económicas, laborales y académicas cotidianas**. "Decretamos un paro pacífico a partir de las 00 horas del 31 de marzo, hasta las 00 horas del 1 de abril de 2016. El paro es pacífico, por lo que invitamos a las comunidades en donde hacemos presencia a que nos acompañen, cesando todo tipo de actividades económicas, laborales y académicas". De acuerdo con los últimos reportes este paro se extendería hasta el próximo domingo.

### Bolívar: 

De acuerdo con la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar CISBCSC, los municipios de Monte Cristo, Tiquisio, Norosi y Arenal se encuentran sitiados por los paramilitares, además, denuncian que la **fuerza pública presente no actúa**, frente a estas acciones que tienen atemorizadas a las comunidades.

Desde el miércoles los municipios amanecieron con panfletos en la zona urbana y rural, por lo que varios establecimientos educativos y comerciales han sido cerrados. Según la Comisión "desde el anuncio del paro armado se han asesinados 2 personas, una en Mina Estrella en Norosi y otra en el Coco Tiquisio". Finalmente denuncian que según versiones de la comunidad en la zona de La Ventura hay un nuevo comandante paramilitar.

### Antioquia: 

En varios municipios de Antioquia hay fuerte presencia de este grupo paramilitar. En [[Briceño](https://archivo.contagioradio.com/?s=brice%C3%B1o)], las Autodefensas Gaitanistas convocaron este miércoles a los transportadores para informales que no podrían desplazarse. Las escuelas no han abierto sus puertas y "el pueblo se encuentra **completamente desolado con la mayoría del comercio cerrado** por cuenta de las múltiples amenazas que recibieron los comerciantes", según denuncia Isabel Cristina Zuleta, vocera del 'Movimiento Ríos Vivos en Antioquia'.

De acuerdo con Zuleta, el lunes hubo **enfrentamientos entre policía y miembros de 'Los Urabeños'** en Briceño donde avanza el plan piloto del desminado humanitario, así mismo, se denuncia que el pasado fin de semana se vio llegar un grupo de hombres armados.

Por otra parte, en Puerto Valdivia, se han presentado enfrentamientos desde el pasado sábado, dejando **varias viviendas averiadas**. Según el reporte de la activista, este miércoles fueron amenazados los barequeros, transportadores, empleados públicos y comerciantes.

Según, Isabel Cristina Zuelta, "la **respuesta institucional ha sido disminuir el problema y no dar la transcendencia necesaria**, cuando continúan los asesinatos de lideres campesinos", y agrega que en las últimas horas en Valdivia han sido quemados dos vehículos en medio de este paro. También se reporta que un soldado fue asesinado por los paramilitares mientras viajaba con su esposa y su hijo hacia el municipio de San Pedro de Urabá.

En general en el bajo Cauca y el Urabá antioqueño, las tiendas, restaurantes, colegios e, incluso los hospitales de los municipios  este jueves amanecieron  cerrados en medio de la zozobra y el temor generalizado.

Hacia la 1 p.m a través de redes sociales se conoció el asesinato de un camionero en el Caserio  Buenos Aires, bomba Puerto Nuevo a 5 minutos  de Puerto Valdivia,  por oponerse a la quema de su tractomula.

<iframe src="http://co.ivoox.com/es/player_ek_10998961_2_1.html?data=kpWmm52depKhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncarnwsfSzpC%2Buc3Z1caY1dTGtsaf0cbf0ZDIqYyZk5eu19nTqMbaxtPgw9iPi8Ld1cbby9jYpdSZk5eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Isabel Cristina Zuleta]

De acuerdo con Zuleta, la jornada que se vivió este jueves en varios municipios antioqueños demuestra la capacidad que tienen los grupos paramilitares de "sembrar el terror" ante la inoperancia de las fuerza pública y de la institucionalidad, la población estuvo **"todo el día en constante tensión"**, por cuenta de los bloqueos que se presentaron en la vía que de Valdivia conduce a la costa, en la que se quemaron por lo menos tres vehículos y algunas motocicletas de personas humildes.

En este municipio, según afirma la vocera, la administración "trabajó a puerta cerrada" y las **instituciones educativas, de salud y bancarias siguen sin abrir sus puertas**, por cuenta del anuncio de continuación del paro que alerta y atemoriza a la población, cuyos líderes fueron hostigados por el Ejército para impedir el registro de imágenes de lo que venía sucediendo en medio del paro.

Caucasia continúa paralizada, en Yarumal se quemaron algunos vehículos y una mujer comerciante que intentó abrir su negocio fue gravemente herida y se encuentra en cuidados intensivos. En Briceño, los comerciantes que abrieron sus negocios fueron alertados ante la continuación del paro, agrega Zuleta, quien afirma que en varios de los municipios afectados por la actividad minera como Pueblo Rico y [[El Bagre](https://archivo.contagioradio.com/?s=el+bagre+)] se quemaron vehículos.

### Urabá: 

Establecimientos comerciales de esta región no prestaron sus servicios durante este paro, así mismo los colegios y guarderias decidieron cerrar sus puertas, y pese a que en la mañana de este viernes se anunció que algunos comerciantes mantenían cerrados sus lugares de trabajo, según los últimos reportes ya hay normalidad en la actividad comercial y el transporte está funcionando.

[En medio de este paro convocado por las AGC el presidente del organización 'Tierra y Paz',]Carlos Yamid Páez,[ lider de la lucha por la restitución de tierras para los campesinos desplazados de esta región, **recibió amenazas de muerte a través de su celular**. ]

### Chocó: 

Las comunidades habitantes del [[Bajo Atrato](https://archivo.contagioradio.com/?s=Bajo+Atrato)] chocoano también fueron alertadas con los panfletos distribuidos por las 'Autodefensas Gaitanistas' y aseguran que este jueves los establecimientos de comercio estuvieron cerrados y que el transporte fluvial y terrestre estuvo paralizado. La mayor preocupación de los pobladores es que en el marco de las actuales negociaciones **no se han desmontado las estructuras paramilitares**, con presencia de vieja data en la región y quienes actualmente intimidan a los jóvenes para que consuman drogas.

<iframe src="http://co.ivoox.com/es/player_ek_11011525_2_1.html?data=kpadk5aZdpahhpywj5WYaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaPVy9SYo9nWpdXjjNfS0tTWuMafscbf0ZClts7VxdSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### Sucre: 

En ciudades como Sincelejo, [en la comuna 1 y 9 se decretó toque de queda por el paro de los paramilitares. Por otra parte, en el municipio de San Onofre se paralizaron todas las actividades tanto comerciales, como en colegios. Los panfletos aparecieron además en los municipios de Los Palmitos, Sampués y El Roble.]

En San Onofre, este viernes los pobladores amanecieron con el temor de salir de sus casas para ir a trabajar, pues **una situación como la de este jueves nunca la habían vivido**, según afirma uno de los habitantes.

<iframe src="http://co.ivoox.com/es/player_ek_11011632_2_1.html?data=kpadk5aad5Ohhpywj5WZaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncbPZ0dTf1sqPl8LijLTb0cvWqYampJC9w9fTb6LmzsbR0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Este viernes en Sincelejo, se establece una mesa de interlocución entre organizaciones y autoridades para lograr garantías de seguridad frente al peligro en el que se encuentran las comunidades, **ningún miembro de la fuerza pública brindó acompañamiento en las calles de la capital**, ni se realizó un Consejo de Seguridad, hechos que según denuncia el poblador dan fuerza al actuar de las estructuras paramilitares en la región.

### Córdoba: 

Tras la circulación de panfletos que llamaban a paro del 31 de marzo al 1 de abril, con el objetivo de“conmemorar” la muerte de Francisco Morelo Peñate, alias el 'Negro Sarley', uno de los miembros de las 'Autodefensas Gaitanistas' en municipios como Buenavista, Lorica y Monteria, los establecimientos comerciales cerraron sus puertas y los pobladores comunidades **continúan atemorizados por los actos de violencia perpetrados en el marco de este paro**.

En Buenavista, un comunicador fue abordado por agentes de la Policía quienes luego de conducirlo a una estación le **prohibieron violentamente que tomara registros del paro**, le quitaron sus pertenencias y borraron algunas de las fotografías que ya había tomado.

<iframe src="http://co.ivoox.com/es/player_ek_11011855_2_1.html?data=kpadk5aceZahhpywj5WXaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbHZ087cxs7XuMKfxtOYr9TSuMbmysaYssbWs4y109LOxtSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En una vía del municipio de Monteria un vehículo fue atacado con disparos, en él se movilizaba **un hombre que fue herido en uno de sus brazos**, en compañía de su esposa y su hijo.

<iframe src="http://co.ivoox.com/es/player_ek_11011768_2_1.html?data=kpadk5abepmhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaXZz9rbxc7Fb8nZ087R0diPqc%2BfrtTb1srWrcKfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Un poblador del municipio de Lorica alerta a la comunidad de los municipios vecinos para que tengan cuidado al salir de sus casas, **se presume que hayan balaceras y explosiones** en sitios concurridos de Monteria y según afirma se está ante el peligro de que vuelva "la guerra a Córdoba".

<iframe src="http://co.ivoox.com/es/player_ek_11011718_2_1.html?data=kpadk5abdZmhhpywj5WVaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaXZz9rbxc7Fb9HV09SYw9fRpcXjjLHc1M7HpYampJCwh6iXaaOn08ncxMaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En Puerto Libertador, San José de Uré y Montelibano, al sur de Córdoba, todas las tiendas estuvieron cerradas, y de acuerdo con la denuncia de [[ASCSUCOR](https://archivo.contagioradio.com/?s=ASCSUCOR)] las **autoridades civiles y militares se sometieron** ante este paro anunciado por las Autodefensas Gaitanistas, pues no se hicieron presentes en la zona para atender la situación de alarma.

Sobre las 11 de la mañana fueron **asesinados dos jóvenes en la vía que de Montelibano conduce a Puerto Libertador**, quienes al parecer “desobedecieron” la orden paramilitar ya que ejercían labores de mototaxismo, según afirma la organización.

Dirigentes campesinos y defensores de derechos humanos de la ASCSUCOR, fueron detenidos en los corregimientos de Tierradentro y Juan José, Montelibano, por hombres armados, presuntos integrantes del 'Clan Usuga', mientras ejercían labores de campo, los obligaron a bajar de las motos, **les amenazaron de muerte y les dijeron que quemarían los vehículos**.

**Imágenes en las redes sociales y panfletos:**

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
