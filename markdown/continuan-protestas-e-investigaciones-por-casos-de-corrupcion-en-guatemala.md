Title: Continúan protestas e investigaciones por casos de corrupción en Guatemala
Date: 2015-07-16 16:06
Category: El mundo, Otra Mirada
Tags: Andrea Ixchiu, Comisión Contra la Impunidad en Guatemala, Corrupción en Guatemala, Guatemala, Iván Velásquez, movilizaciones en gutemala, Otto Pérez, Prensa Comunitaria
Slug: continuan-protestas-e-investigaciones-por-casos-de-corrupcion-en-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Protestas-Guatemala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [cmiguate.org]

<iframe src="http://www.ivoox.com/player_ek_4879653_2_1.html?data=lZ2km5uZd46ZmKiak5iJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1c7bh6iXaaO1wtOY0tfTuMbn1cbgjcqPrc%2Fqxtjhy8zFp8rjz8rgjdXTtozX0Nff19XHrYa3lIqvldOPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrea Ixchiu, periodista Prensa Comunitaria] 

###### [16 ,Julio, 2015] 

Tras revelarse los casos de corrupción de funcionarios públicos vinculados al presidente guatemalteco Otto Pérez, **continúan las protestas y las investigaciones por corrupción** en  recaudación aduanera, desfalco al Instituto de Seguridad, al Ministerio de Gobernación y otras  instituciones de servicio público.

Desde el pasado 25 de abril, las movilizaciones ciudadanas han sido la constante, **“la gente se sintió enojada por que el dinero de los impuestos estaba sirviendo para el enriquecimiento de una elite** y no para los servicios públicos básicos que necesita la población”, expresa Andrea Ixchiu, periodista del medio guatemalteco Prensa Comunitaria.

Ixchiu cuenta que a partir de 2013, se vivió una crisis en el sistema de salud y educativo, y desde el gobierno siempre se presentaba como **excusa la baja recaudación fiscal, “pero en realidad todo ese dinero se lo habían robado”**, señala la periodista.

Al ser comprobado el hecho y volverlo verdad, las personas han decidido movilizarse por el cansancio generalizado en la población teniendo en cuenta además que desde 2012, “**este gobierno ha sido muy intransigente y represivo,** ha criminalizado a los movimientos sociales”, dice Andrea.

Las movilizaciones han empezado a tener un fundamento político, y se piensa transformar la construcción política del país, por lo que se quiere reformar la ley de partidos políticos con el fin de evitar que la actual clase política sea reelegida. A largo plazo se pretende impulsar  una transformación estatal que reconozca la diversidad étnica y cultural.

**Indígenas, mujeres, jóvenes y hasta el sector privado de Guatemala** “han decidido dar el salto del miedo a la acción política y movilización ciudadana pensando en un cambio que tomará tiempo”, expresa Ixchiu.

La Comisión Contra la Impunidad en Guatemala, dirigida por el exmagistrado colombiano Iván Velásquez, destapó el caso de corrupción. Desde allí se asegura que la actual situación es saludable porque ha despertado la indagación de la ciudadanía, y ha generado hechos como la **renuncia de la vicepresidenta del país centroamericano.**
