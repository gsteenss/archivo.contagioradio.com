Title: Listo Campamento por la paz en Cali
Date: 2016-10-25 12:20
Category: Nacional, Paz
Tags: Cali, Campamento por la Paz, estudiantes
Slug: listo-campamento-por-la-paz-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/lafm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LaFM] 

###### [25 de Oct 2016] 

Se instala nuevo **Campamento por la paz en Cali** – Colombia, **aproximadamente 30 carpas se  situaron en la Plazoleta San Francisco** para respaldar los acuerdos de paz y exigir una pronta solución por parte del gobierno frente al proceso con la guerrilla de las FARC-EP.  Hasta el momento se han sumado unas 200 personas y el campamento iría hasta el 31 de diciembre.

Con Cali ya son tres las ciudades que tienen campamentos por la paz. **Estas concentraciones cuentan con diferentes actividades enmarcadas en la pedagogía de los acuerdos de la Habana** que busca interactuar con las personas, a su vez campesinos, indígenas y miembros de la comunidad del Naya. Le puede interesar: ["Montería instala campamento por la paz"](https://archivo.contagioradio.com/monteria-instala-campamento-por-la-paz/).

De acuerdo con Daniel Tellez, la iniciativa es adoptada por la plataforma Voz Universitaria por la Paz que a través de la cultura y la toma pacífica pretende construir paz. “**Acá vamos a hablar con cada  persona que se acerque a la Plazoleta**, para pasar un buen rato y compartir” afirmó.

Las personas que se encuentran en el lugar han expresado a la comunidad que **necesitan donaciones de alimentos como enlatados, no perecederos y carpas o plásticos** debido a las lluvias que por estos días se presentan en Cali.

<iframe id="audio_13475250" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13475250_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
