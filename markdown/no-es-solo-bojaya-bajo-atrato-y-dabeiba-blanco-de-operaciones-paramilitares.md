Title: Control paramilitar de AGC se extiende desde frontera con Panamá, Bajo y Medio Atrato
Date: 2020-01-02 22:44
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, Bojaya, militares, paramilitares
Slug: no-es-solo-bojaya-bajo-atrato-y-dabeiba-blanco-de-operaciones-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cacarica2016_52.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Luego de 17 años de la tragedia en la que murieron casi un centenar de personas en Bojayá en medio de los enfrentamientos de estructuras paramilitares y guerrilleras, permanece el terror paramilitar y se extiende a varios municipios y territorios colectivos del Chocó, en el Medio y Bajo Atrato. (Le puede interesar: ["Neoparamilitares amenazan la zona humanitaria de Nueva Vida en Cacarica, Chocó"](https://archivo.contagioradio.com/neoparamilitares-amanezan-la-zona-humanitaria-de-nueva-vida-en-cacarica-choco/))

Paradójicamente la ocupación paramilitar ocurre en medio de un anuncio de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) de cese unilateral de fuego que no se materializa en acciones concretas a favor de las poblaciones sitiadas y controladas por esa estructura no solamente desde este Diciembre, sino desde hace varios años en los que las comunidades persisten en sus esfuerzos por alivios humanitarios regionales

### **Operaciones paramilitares en 12 municipios del Bajo y Medio Atrato**

El pasado 30 de diciembre, la Comisión Intereclesial de Justicia y Paz denunció el aumento de control de paramilitares de las autodenominadas AGC en las áreas rurales del Urabá antioqueño, el Bajo Atrato, Murindó y Bojayá en el medio Atrato. La alerta se realizó a través de la cuenta de Twitter de la Comisión, y fue activada a través de la Alta Consejería de Derechos Humanos, en alusión a operaciones de las AGC en Necoclí, Turbo, Apartadó, Chigorodó, Mutatá, Dabeiba, Belén de Bajirá, Carmen del Darién, Riosucio, Frontino, Urrao y Bojayá.

En la denuncia, la ONG informó sobre el incremento del control paramilitar de las AGC en diferentes comunidades indígenas y afrocolombianas del **Bajo Atrato como Cacarica, Curbaradó, La Larga, Pedeguita Mancilla y Jiguamiandó**; donde pobladores se vieron obligados a desplazarse ante las amenazas de los paramilitares, y el control territorial total que ejercen en Cacarica, con excepción de la Zona Humanitaria de Nueva Esperanza.

De igual forma, denunció el censo poblacional infantil que se realiza en todo el Bajo Atrato, con la entrega de regalos navideños, y la expresa prohibición a los pobladores de denunciar la actuación paramilitar, so pena de ser asesinados o desplazados forzosamente del territorio.

Ante la situación, líderes y lideresas optaron por abandonar la región, señalando que no creen en el Estado y que dan por fracasado el proceso de paz, porque han visto como excombatientes de FARC y ELN ahora son paramilitares y que las AGC actúan de la mano con militares y policías.

Los pobladores de las zonas han señalado a Contagio Radio que los **Planes de Desarrollo con Enfoque Territorial (PDET)** se quedaron en el papel, y el poder real es ejercido por las AGC, y por quienes ocupan sus tierras para agronegocios. Adicionalmente, han señalado que se van y solo piensan regresar cuando la paz sea una realidad, porque temen ver a sus hijos reclutados y atacando a sus propias comunidades o a sus hijas ejerciendo la prostitución, ante la ausencia de oportunidades económicas.

### **La denuncia de la Comisión, y su Constancia Histórica** 

> [\#BajoAtrato](https://twitter.com/hashtag/BajoAtrato?src=hash&ref_src=twsrc%5Etfw) Pobladores que logran salir de cerco de los paramilitares AGC, denuncian que entregaron reses a diversas comunidades y regalos a niños. AGC amenazaron con asesinar a lideres que les denuncien.SItiados, silenciados por AGC en medio de Fuerza Pública siguen comunidades.
>
> — Comisión de Justicia y Paz (@Justiciaypazcol) [December 30, 2019](https://twitter.com/Justiciaypazcol/status/1211770885725659139?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La Comisión denunció el pasado 30 de diciembre que paramilitares de las AGC obligaron a diferentes comunidades a recibir regalos a niños y cabezas de ganado a familias, como una forma de fomentar una deuda de estas hacía el grupo armado. En el hilo de Twitter se explica que el temor de las comunidades es tal, que incluso no han querido denunciar los desplazamientos forzados de los cuales son víctimas, mientras sus líderes son amenazados con la muerte, y obligados a abandonar los territorios.

El control por parte de las AGC, se realizaría mediante la instalación de los llamados "puntos", casas en las que hombres vestidos de civil y con armas cortas vigilan las poblaciones, y hay campamentos de personas vestidas con camuflados que usan armas largas. Situaciones que ocurren "en medio de fuerzas militares, que afirman que controlan el territorio en todas las áreas rurales". Adicionalmente, dicho control estaría relacionado con el ingreso de negocios que "desconocen derechos humanos, territoriales y ambientales".

No obstante, la denuncia de la Comisión no es nueva, el pasado 24 de diciembre la Organización publicó una[Constancia Histórica](https://www.justiciaypazcolombia.com/control-socioterritorial-paramilitar-despojo-judicializacion-de-lideres-de-restitucion-de-tierras-y-connivencia-entre-militares-y-agc-en-el-bajo-atrato/) sobre la situación en Bajo Atrato y Antioquia, **el aumento de la presencia paramilitar en los Territorios Colectivos de Curbaradó, Jiguamiandó, Pedeguita y Mancilla, La Larga Tumaradó y La Balsita (Dabeiba)**. En la constancia se revelan hechos ocurridos desde septiembre, como la construcción de casas para establecer 'puntos' de control, el despojo de tierras a legítimos y legales ocupantes, así como la presencia de proyectos plataneros y mineros.

En la Constancia también se hace referencia a operaciones militares, que ocurren simultáneamente con acciones de los paramilitares en cercanías de Zonas de Biodiversidad, establecidas por las comunidades como formas de protección socio-ambientales. También se denuncia la retención de líderes indígenas, y la i**rrupción de armados en actividades organizativas de comunidades en Cacarica y Dabeiba.** (Le puede interesar:["Denuncian incursión paramilitar en Comunidad campesina de Dabeiba, Antioquia"](https://archivo.contagioradio.com/denuncian-incursion-paramilitar-comunidad-campesina-dabeiba-antioquia/))

### **¿Las AGC decretaron un cese unilateral al fuego?** 

El pasado 18 de diciembre, a través de su página web y en volantes que entregaron a las comunidades, las autodenominadas AGC señalaron que decretaban un cese al fuego unilateral entre el 20 de diciembre de 2019 y el 10 de enero de 2020, respetando las festividades de fin de año. En su comunicado, los paramilitares invitaban a otros grupos armados a hacer lo mismo, de allí que la población fuese sorprendida por la continuidad en las acciones armadas por parte de este grupo.

Por otra parte, comunidades de algunas zonas han señalado que el Ejército de Liberación Nacional (ELN) manifestó estar dispuesto también a establecer un cese humanitario al fuego. Mismo, que ha sido también propuesto por comunidades en diferentes momentos, como una forma de desescalar la violencia en los territorios. (Le puede interesar: ["¿Qué implicaciones tiene el cese al fuego unilateral del ELN para semana santa?"](https://archivo.contagioradio.com/implicacionesl-cese-al-fuego-unilateral-del-eln/))

### **La toma de Pogue, Bojayá** 

El 31 de diciembre, la Comisión denunció que en el Medio Atrato, paramilitares de las AGC se tomaron la comunidad de Pogue, municipio de Bojayá (Chocó). Yeison Mosquera, integrante de la organización Tierra y Vida, afirmó que en ese Municipio constantemente se presentan combates entre el ELN y las AGC. De igual forma, sostuvo que al Chocó no llegó el proceso de paz, y por eso, la gente no tiene otra opción diferente a sembrar Coca, lo que a su vez, alimenta un negocio que tiene en la zona de Bojayá un corredor importante de salida hacia el Pacífico.

Mosquera cuestionó que no se preste atención a las denuncias de las comunidades, que en [anterior entrevista con Contagio Radio](https://archivo.contagioradio.com/bojaya-cerrar-dolor/) **habían advertido sobre la posibilidad de que se repita la masacre de 2002,** debido a la presencia e interés de distintos grupos armados en la zona. (Le puede interesar: ["Estado debe reconocer su responsabilidad y dar la cara a Bojayá y al mundo"](https://archivo.contagioradio.com/estado-debe-reconocer-su-responsabilidad-y-dar-la-cara-a-bojaya-y-al-mundo/))

Sobre esta toma paramilitar, la Séptima División del Ejército, que opera en la zona, afirmó que se desplazaría hasta allí para evaluar la situación y hacer control, asimismo, sostuvo que enviaría 50 hombres más para reforzar la acción del centenar de uniformados que operan en el territorio. En horas de la mañana, de este 2 de enero, el Comandante de esta División, Juan Carlos Ramírez, en entrevista para medios de comunicación aseguró que las unidades militares no podían estar en todas partes, pero están en los puntos donde los grupos armados se pueden mover.

Frente al anuncio de enviar más hombres al territorio, Mosquera afirmó que esto no garantiza la vida de las comunidades, y en su lugar, **"lo que necesitamos es que se reanuden las conversaciones de paz con el ELN, y segundo, el sometimiento de las Autodefensas Gaitanistas de Colombia,** no BACRIM, Autodefensas Gaitanistas de Colombia". (Le puede interesar: ["Paramilitares de las AGC sitian a 800 habitantes de Juradó, Chocó"](https://archivo.contagioradio.com/paramilitares-de-las-agc-sitian-a-800-habitantes-de-jurado-choco/))

<iframe id="audio_46087214" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_46087214_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
