Title: Por medio de dibujos y fotografías, niños Embera reconstruyen la memoria
Date: 2019-05-23 18:46
Author: CtgAdm
Category: Comunidad, Cultura
Tags: Alto Guayabal, Chocó, Festival de las Memorias, niñez, TDH
Slug: por-medio-de-dibujos-y-fotografias-ninos-embera-reconstruyen-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Memorias.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Leonardo Lovera 

Durante el Festival de las Memorias celebrado el pasado 18 y 19 de mayo, en Alto Guayabal, Chocó y que congregó a a comunidades Emberas y diferentes organizaciones alrededor de la reconciliación y un encuentro cultural,  también se destinó un espacio para que los niños y niñas indígenas participaran de su propia reconstrucción de la memoria de una forma lúdica a través del dibujo y la fotografía.

El realizador audiovisual  **Leonardo Lovera, frecuente colaborador de TDH Alemania**, organización que trabaja por la defensa de la niñez de Colombia, fue uno de los asistentes al festival, relató que en medio de los espacios dedicados al recuerdo de las personas desaparecidas, los niños plasmaron a través de  una serie de dibujos los eventos que han afectado su modo de vida en particular los hechos de desplazamiento en la comunidad. [(Lea también: Festival de las Memorias en Alto Guayabal: un encuentro para construir la paz)](https://archivo.contagioradio.com/festival-de-las-memorias-en-alto-guayabal-un-encuentro-para-construir-la-paz/)

También hizo presencia el Enfoque Curso de Vida de la Comisión de la Verdad, con la idea de documentar las expresiones de verdad de los niños, niñas y adolescentes (NNA), habitantes de estos territorios.

### Una mezcla de imaginación, memoria e inocencia

Con cámaras fotográficas que fueron llevadas al lugar, también se pudo registrar algunos escenarios de los Embera,  "cosas que los hacen a ellos auténticos y manifestar todo lo que tiene que ver con su cultura, tradición y  los elementos que les permiten que estos hechos no se vuelvan a repetir", manifestó Lovera.

Para el realizador audiovisual lo más significativo es que a través de la cámara se tuvo acceso a la imaginación de los niños y niñas, lo que demostró su autenticidad, **"las fotografías reflejaron su interior y su inocencia al mostrar elementos no tangibles como la amistad, la unión y  la cultura"** conceptos que a pesar de ser difíciles de plasmar resultaron lo más valioso del ejercicio.

Leonardo indicó que TDH Alemania busca replicar este ejercicio en otros encuentros de la memoria junto a nuevas comunidades para consolidar sus dibujos y fotografías en un informe y lo más importante es que ese material al final sea  regresado a los verdaderos protagonistas de esas historias, sus autores.

</p>
######   

<figure class="gallery-item">
[![Memoria Alto Guayabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477-300x225.jpg){width="300" height="225" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477-768x576.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477-1024x768.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477-370x278.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477.jpg 1288w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0477.jpg)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67832">
Foto: Contagio Radio  

</figcaption>
</figure>
<figure class="gallery-item">
[![Memoria Alto Guayabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008-300x225.jpg){width="300" height="225" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008-768x576.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008-1024x768.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008-370x278.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008.jpg 1280w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0008.jpg)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67835">
Foto: Contagio Radio  

</figcaption>
</figure>
<figure class="gallery-item">
[![Memoria Alto Guayabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517-300x225.jpg){width="300" height="225" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517-768x576.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517-1024x768.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517-370x278.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517.jpg 1288w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0517.jpg)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67831">
Foto: Contagio Radio  

</figcaption>
</figure>
<figure class="gallery-item">
[![Memoria Alto Guayabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470-300x225.jpg){width="300" height="225" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470-768x576.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470-1024x768.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470-370x278.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470.jpg 1288w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_0470.jpg)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67827">
Foto: Contagio Radio  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07-300x169.png){width="300" height="169" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07-300x169.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07-768x432.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07-1024x576.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07-370x208.png 370w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-07.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67711">
Foto: Leonardo Lovera  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06-300x169.png){width="300" height="169" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06-300x169.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06-768x432.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06-1024x576.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06-370x208.png 370w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-06.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67706">
Foto: Leonardo Lovera  

</figcaption>
</figure>
<figure class="gallery-item">
[![Memoria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01-300x169.png){width="300" height="169" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01-300x169.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01-768x432.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01-1024x576.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01-370x208.png 370w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DIBUJO01.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67704">
Foto: Leonardo Lovera  

</figcaption>
</figure>
<figure class="gallery-item">
[![Foto: Leonardo Lovera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04-300x169.png){width="300" height="169" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04-300x169.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04-768x432.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04-1024x576.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04-370x208.png 370w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMAGEN-04.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-67720">
Foto: Leonardo Lovera  

</figcaption>
</figure>
Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<iframe id="audio_36297966" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_36297966_4_1.html?c1=ff6600"></iframe>
