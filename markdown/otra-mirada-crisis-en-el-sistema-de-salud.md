Title: Otra Mirada: Crisis en el sistema de salud
Date: 2020-07-27 22:05
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Coronavirus, Crisis de salud, Sector salud
Slug: otra-mirada-crisis-en-el-sistema-de-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/salud.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @MinSaludCol

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente la ocupación de las Unidades de Cuidados Intensivos en casi todo el país alcanza el 95%, asimismo, el resultado de las pruebas que se hacen para detectar COVID-19 tarda en promedio 10 días, es decir, supera las 72 horas que se han puesto como estándar, siendo para muchos, una de las causas por las que el virus no se ha podido controlar. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, este 23 de julio, integrantes de agremiaciones médicas expresan que los profesionales de la salud están pasando por sobrecarga laboral y continúan exigiendo al gobierno que sus salarios sean pagados, agregando que la solución para combatir el virus está en el diagnóstico oportuno y no en la habilitación de más UCI y más equipos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todos estas situaciones, están demostrando que la crisis en el sistema de salud no está dejando de crecer y que el gobierno podría no estar viendo adecuadamente la crisis y en consecuencia, tomando decisiones incorrectas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para analizar la situación, los invitados estuvieron desde diferentes sectores de la salud y fueron Marlene Vélez, presidenta del Colegio Nacional de Bacteriología, Yesid Camacho, secretario de salud y seguridad social de ANTHOC, Cecilia Vargas, presidenta de la Organización Colegial de Enfermería y Carolina Corcho, vicepresidenta de la Federación Médica Colombiana. (Si desea saber más: [El sistema de salud está totalmente colapsado afirman gremios de la salud](https://archivo.contagioradio.com/el-sistema-de-salud-esta-totalmente-colapsado-afirman-gremios-de-la-salud/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este espacio, los expertos explicaron qué está ocurriendo en los laboratorios y la razón por la que los diagnósticos están demorando tanto en salir, así como cuáles son las garantías que se le han dado tanto a la llamada, primera línea como a los bacteriólogos. También comentan cómo está la crisis de salud en los territorios que han sido los más afectados desde el inicio de esta pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, comparten su perspectiva con respecto a las iniciativas y propuestas que se han presentado y cuestionan si la culpa de esta crisis es del virus o de un modelo de salud que podría tener a su vez el trasfondo de un modelo económico para beneficiar a unos pocos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, confirman que estamos en una crisis y opinan que la visión del gobierno podría no estar contextualizada en la realidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 22 de julio: [Construir paz en los territorios](https://www.facebook.com/contagioradio/videos/1400891780099615)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo del programa:

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/412854579652436","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/412854579652436

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
