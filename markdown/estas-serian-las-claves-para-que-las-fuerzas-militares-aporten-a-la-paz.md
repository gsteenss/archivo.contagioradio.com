Title: Estas serían las claves para que las fuerzas militares aporten a la paz
Date: 2019-11-07 22:38
Author: CtgAdm
Category: Entrevistas, Paz
Tags: defensa, ministerio de defensa, Ministro de defensa, seguridad
Slug: estas-serian-las-claves-para-que-las-fuerzas-militares-aporten-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @FuerzasMilCo] 

Tras la renuncia de Guillermo Botero al Ministerio de Defensa Nacional, el presidente Duque nombró como encargado de la cartera al general Luis Fernando Navarro, una situación que fue cuestionada por diferentes voces porque sería la primera vez después de la Constitución de 1991 que un uniformado ejerce como ministro. El nombramiento también es la oportunidad de hablar sobre lo que requieren las fuerzas militares para lograr generar las garantías necesarias para la protección de la vida, y la seguridad nacional.

### **Un nombramiento constitucionalmente 'problemático'** 

En su cuenta de twitter, el jurista Rodrigo Uprimny recordó que constitucionalmente la Fuerza Pública no es deliberante y sus miembros no pueden participar en política, razón por la que consideró como 'problemático' el nombramiento de un general activo como ministro de defensa encargado. Apropósito del trino, el abogado y defensor de derechos humanos Reinaldo Villalba, señaló su acuerdo, y recordó que antes de la Constitución del  91 los ministros de defensa fueron militares "pero la experiencia fue nefasta" y por eso en la nueva carta magna se impulsó que civiles estuvieran al frente de esta cartera.

Por lo tanto, para Villalba, el nombramiento del general Navarro "es un muy mal mensaje" enviado por Duque a la sociedad colombiana que en conjunto reclamó la salida de Botero luego de no atender adecuadamente la situación en Cauca, y tras conocerse que en un bombardeo de las fuerzas militares resultaron masacrados ocho niños. El Defensor de DD.HH. explicó que es un mal mensaje porque el país espera que ejerza como ministro un civil, comprometido con la democracia, la paz y el respeto por los derechos de las personas, de otra manera, "puede ser peor el remedio que la enfermedad".

En momentos en que los territorios están viviendo un aumento en la violencia directa evidenciada en el incremento de amenazas, asesinatos contra líderes sociales, presencia de actores armados ilegales y denuncias por actuaciones irregulares de la fuerza pública, Villalba sostuvo que se requería respuestas concretas, "y la mejor respuesta es un cambio de conducta". (Le puede interesar: ["Botero debe asumir responsabilidad política y penal por bombardeo a niños"](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/))

Contrario a ello, el partido de Gobierno ha justificado el bombardeo a niños, y como lo resalta el Abogado, "ni el Presidente de la República, ni el Ministro de Defensa ni ninguna autoridad ha pedido perdón a la sociedad colombiana ni a los familiares". Villalva igualmente criticó que tampoco entes de control como la Fiscalía o la Procuraduría se han manifestado en el sentido de iniciar investigaciones sobre el hecho para determinar los responsables, y así evitar que estas situaciones se repitan.

### **El país requiere unas fuerzas militares para la paz** 

Entre los elementos específicos que se requieren para tener un sector de defensa que genere confianza, y cumpla la labor de brindar seguridad a los colombianos, el abogado resaltó que se requiere "una fuerza pública para la paz", lo que implica cambios profundos: Depuración inmediata de aquellos que violan los DD.HH., cambiar el 'chip' en la formación de la fuerza pública y eliminar las doctrinas que han motivado violaciones de derechos, así como eliminar las doctrinas de seguridad nacional. (Le puede interesar: ["Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional"](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/))

Villalba manifestó que era necesario que el respeto por los derechos humanos sea el fundamento de todo accionar, y se requieren investigaciones y despidos, cuando haya lugar, contra quienes violen los derechos de las y los ciudadanos, pero declaró que "no veo al presidente Duque con ese ánimo", por lo tanto, dijo que la sociedad colombiana tenía que seguir reclamando esos cambios profundos, hasta que el Presidente entienda que se deben hacer.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44237305" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44237305_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
