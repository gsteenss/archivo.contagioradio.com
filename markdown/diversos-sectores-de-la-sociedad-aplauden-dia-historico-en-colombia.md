Title: Diversos sectores de la sociedad aplauden día histórico en Colombia
Date: 2016-06-23 16:09
Category: Nacional, Paz
Slug: diversos-sectores-de-la-sociedad-aplauden-dia-historico-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Frente-Amplio-por-la-Paz-e1454017570870.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Frente Amplio 

###### [23 Jun 2016] 

### Rodrigo Castillo, Representante Comunidades Construyendo Paz en los territorios, CONPAZ 

Para el integrante de CONPAZ, es un día histórico que representa el fruto de un trabajo constante de más de 3 años de conversaciones y de muchos esfuerzos por parte de las comunidades. Señala que desde el inicio de las conversaciones en la Habana ellos y ellas han tenido un trabajo intenso de elaboración de propuestas en torno a los acuerdos, propuestas para sustitución de cultivos, de restitución de tierras, en torno a la Jurisdicción especial de paz y a la materialización de los terrepaz.

Ahora, según Castillo, viene otra tarea que es la de consolidar todas las condiciones para que haya justicia social, implementar los acuerdos sobre sustitución de cultivos de uso ilícito, para acabar con la impunidad y afirman que están listos para conocer la verdad del horror de la guerra, la verdad del paramilitarismo que los desplazó y afirman que es necesario que la paz también incluya al ELN.

### Carol Sánchez, periodista de ‘El Ecléctico’ 

Señala como un día histórico para Colombia, pero además como una oportunidad para que se ponga atención sobre otras problemáticas que viven las comunidades del país. Además asegura que se trata de una posibilidad para que las FARC se conviertan en movimiento político a partir de la democracia, además es una oportunidad para evitar millones de muertes y para que haya tranquilidad en zonas donde no la había desde hace años.

### John Jairo Mena, Territorio Colectivo de CAVIDA, Cacarica 

Para uno de los integrantes de las comunidades que ha vivido de cerca la guerra y además lo ha marcado, significa un “día especial para Colombia, sobre todo para las víctimas”, y asegura que lo más importante será que este sea un “acuerdo verdadero y duradero”.

Así mismo, Mena envió un mensaje a quienes en las ciudades no apoyan el proceso de paz: “En solo 4 años se ha podido llegar a un acuerdo definitivo para una guerra de 50 años, esto significa que si se puede lograr la paz a través del diálogo, con las balas no se soluciona nada. Es posible la expresión de la paz sin empuñar las armas”.

### Omar Fernández, Coalición de Movimientos y Organizaciones Sociales de Colombia. 

Para el integrante de COMOSOC, es un gran logro que debe apreciar todo el pueblo colombiano, un paso esencial para vivir en paz y en una verdadera democracia. Fernández, indica que es claro que las personas de las ciudades son escépticas a este momento en parte por la forma como los medios masivos han  hecho el cubrimiento de estos casi 60 años de guerra en el país, lo que ha generado el apoyo a iniciativas que van en contravía de la paz. En ese marco, asegura que para el posacuerdo la construcción y fortalecimiento de los medios alternativos es vital para acabar con ese tipo de discursos como los que lidera el senador Álvaro Uribe Vélez y todo su bloque.

### Camilo Villa, del Movimiento de Víctimas de Crímenes de Estado 

"Llevábamos más de 3 años esperando este día", asegura Villa, en relación con la firma del punto del fin del conflicto e insiste en que este proceso debe implicar el fortalecimiento de la democracia y la estabilización de los demás frentes de guerra, como el ELN y los grupos paramilitares. "En las regiones hay esperanza por el cese", **las comunidades esperan que éste venga acompañado de medidas integrales** pues "cualquier asedio militar puede generar tensiones (...) y lo que queda ahora es verificar implementación de los acuerdos".

Pese a las contradicciones como la aprobación del nuevo código de Policía, éste puede ser el inicio del fin absoluto de la guerra, en el que se puedan re litigar casos que han quedado en la impunidad y en el que se puedan empezar a ver señales reales del acuerdo.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
