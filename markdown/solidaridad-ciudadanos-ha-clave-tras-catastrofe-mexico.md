Title: Solidaridad de ciudadanos ha sido clave tras catástrofe en México
Date: 2017-09-20 16:12
Category: El mundo, Movilización
Tags: Emergencia, mexico, Sismo, Terremoto
Slug: solidaridad-ciudadanos-ha-clave-tras-catastrofe-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Mexico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Scoopnest] 

###### [20 Sept. 2017] 

Luego del terremoto de 7,1 grados en la escala de Richter en Mexico, la solidaridad y el trabajo mancomunado entre la comunidad, los miembros de la Cruz Roja y las fuerzas militares no se ha hecho esperar. Hasta el momento **suman 225 personas fallecidas en el Distrito Federal y las demás poblaciones en donde se sintió el sismo.**

Además, los daños a las infraestructuras son incontables. Hasta el momento se reportan tan solo en el DF el desplome de 39 edificios y otros por lo menos 600 que deberán ser revisados por ingenieros y arquitectos para determinar su viabilidad de uso. Le puede interesar: [Cientos de testimonios y videos revelan el fraude patrocinado por el PRI en México](https://archivo.contagioradio.com/cientos-de-testimonios-y-videos-revelan-el-fraude-patrocinado-por-el-pri-en-mexico/)

### **Pueblo mexicano ha unido sus manos para levantarse de los escombros** 

Lorena Peralta, ciudadana del DF agradeció por toda la solidaridad que diversas naciones han manifestado luego de la catástrofe **“la ayuda de ayer fue espontanea, así como cuando sucedió en el 85. La ciudadanía ponía sus manos, su fuerza** y toda la disposición para mover piedras en cadenas humanas y para dotar de agua y alimentos para quienes están haciendo esa tarea. Aún con mucho miedo se apoyó. Anoche hubo mucha gente despierta por el miedo”.

Otra de las iniciativas que se han impulsado luego del sismo está enfocada al cuidado desde y para las mujeres por tal razón, cuenta Peralta, **se han organizado brigadas feministas y también personas que van en bicicleta para trasladar alimentos,** agua e insumos, pero también para conocer el estado de diversos lugares.

Así mismo, algunas pequeñas empresas han decidido donar materiales como palas, picos y otras herramientas que posibiliten el rescate y se haga de manera más amplia. Además las ferreterías han donado todo lo que han necesitado las personas y la gente se ha organizado para dar a alimentos y agua a las personas que han asumido el rescate de personas bajo los escombros.

### **¿Cómo han actuado las instituciones para atender la emergencia?** 

Relata Peralta que Protección Civil ha estado trabajando de manera fuerte y también de la mano de la ciudadanía y con el grupo Topos México que surgió a raíz del terremoto de 1985 y así sumando experiencias rescatan personas.

**“Policías de seguridad también están trabajando, haciendo lo que les toca hacer, aunque ha sido insuficiente.** Además, ya lo que se ha informado es que llegó la Marina y está limitando la participación de la ciudadanía, diciéndoles que ya no es necesaria su ayuda, que se vayan, que estorban”.

Además, a manera de anécdota, Peralta cuenta que **el Secretario de Gobernación se ubicó en lugar de desastre para tomarse una fotografía “selfie”** y lo que hizo la ciudadanía fue correrlo “lo que la ciudadanía le decía es más bien pon tus manos y saca piedras, no vengas aquí a promocionarte de alguna manera”.

### **Aún no llega ayuda de otros países** 

Pese a que varios mandatarios de diversos países lamentaron lo sucedido en México y expresaron su apoyo hasta el momento, asegura Peralta que **hasta el momento no se ha conocido de la llegada de ayuda.** Le puede interesar: [Ningún lugar de Colombia se salva de los impactos del cambio climático](https://archivo.contagioradio.com/listo-proyecto-de-ley-para-poner-en-marcha-acuerdo-sobre-cambio-climatico/)

Por el momento la Cruz Roja Internacional trabajan en colaboración con la Cruz Roja en México “pero por ahora no hay presencia de algún cuerpo internacional de apoyo, no hay conocimiento. Solo son mensajes de solidaridad de mandatarios y organizaciones”.

### **Xochimilco necesita ayuda** 

A través de redes sociales la comunidad de Xochimilco está denunciando que nadie esta ayudándoles y que la gran mayoría de la ayuda se está direccionando hacia Santa Cruz Acalpixca y San Gregorio.

"La gente se quedó sin casa, están sacando a la gente de los escombros inconsciente. Si alguien puede ayudar, o sabe de cómo organizarse se los agradecería mucho (...) Xochimilco necesita ayuda" dice una publicación enviada en Whatsapp

### **Mexicanos afirman que políticos no deben sacar provecho de la tragedia** 

Frente a la actitud que ha asumido el primer mandatario de México, Enrique Peña Nieto, **los mexicanos se encuentran desconfiados y enojados por cómo se ha enfrentado la situación de los habitantes** afectados por este y por el sismo sucedido el pasado 7 de septiembre y que dejó afectadas a muchas familias y varias infraestructuras.

“La reacción de las autoridades federales y de tránsito lo que hicieron fue lucrarse con ese dolor. Entonces yo creo que hay mucho descreimiento hacia lo que se dijo ayer por parte de Enrique Peña Nieto, porque finalmente él tenía la obligación de salir como mandatario a decir algo, pero pues veremos cómo va a ser la coordinación con las autoridades”.

Por ese motivo, **lo que viene luego de la fase de rescate, es hacer de veedores para los alimentos, medicamentos y demás ayudas** que lleguen a México para los afectados por el terremoto. Le puede interesar: [Cinco perturbadoras consecuencias del cambio climático](https://archivo.contagioradio.com/consecuenciasde_cambio_climatico_ambiente/)

### **Delincuentes estarían asaltando casas y locales** 

Por último, se ha denunciado que, pese a la solidaridad y el apoyo, hay otras personas que están aprovechando que no hay luz para ingresar a las casas para robar “creo que hay de todo, están las de a puño, muy hermanadas, pero también están las otras y no hay que dejarlas de subrayar también”.

<iframe id="audio_21002159" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21002159_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
