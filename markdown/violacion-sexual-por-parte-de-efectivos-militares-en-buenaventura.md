Title: Denuncian caso de violación contra niño discapacitado en Buenaventura
Date: 2015-08-18 12:26
Category: DDHH, Nacional
Tags: Agresiones sexuales, buenaventura, Paramilitarismo, William Alomia Ortiz
Slug: violacion-sexual-por-parte-de-efectivos-militares-en-buenaventura
Status: published

##### Foto: eltiempo.com 

<iframe src="http://www.ivoox.com/player_ek_7042515_2_1.html?data=mJWhlJqVeY6ZmKiakp2Jd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb8TV1NSYxsqPusrjzcbQy4qnd4atlNOYxdTSuNPVjNPWh6iXaZql0JDRy9jHpdHVxM7hw8nTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jackeline Alomia Ortiz, madre del menor] 

###### [18 Ago 2015 ]

El pasado domingo 16 agosto, sobre las 5 de la tarde, en la vereda La Gloria del municipio de Buenaventura, **fue abusado sexualmente el joven de 17 años y en condición de discapacidad William Alomia Ortiz**. La madre del menor, Jackeline Alomia Ortiz, asegura que el responsable de la violación es Álvaro Andrés Gonzales quien actualmente presta el servicio militar obligatorio y que presuntamente habría desertado.

La comunidad de La Gloria, afirma que **Gonzales usaba prendas militares en el momento del abuso hacia William** y que reaccionó violentamente y aseguró pertenecer a estructuras paramilitares cuando los pobladores evitaron que huyera.

Este hecho llama la atención pues agrava la crisis humanitaria y  de violación de derechos humanos en Buenaventura, por cuenta del control paramilitar y los intereses económicos que pesan sobre los territorios colectivos.
