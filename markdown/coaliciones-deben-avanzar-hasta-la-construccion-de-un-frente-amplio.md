Title: Coaliciones deben avanzar hasta la construcción de un Frente Amplio
Date: 2017-09-20 15:53
Category: Nacional, Paz
Tags: Claudia López, elecciones 2018, Jorge Robledo
Slug: coaliciones-deben-avanzar-hasta-la-construccion-de-un-frente-amplio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/alianza_verde-_polo-_fajardo_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [20 Sept 2017] 

El pasado 18 de septiembre se lanzó la coalición de Sergio Fajardo, Jorge Robledo y Claudia López para las elecciones presidenciales de 2018, unión que, de acuerdo con Ángela María Robledo, debe fortalecerse mucho más con un **“frente de unidad” que lo que finalmente impida sea la llegada de la ultraderecha** al poder que termine acabando con los avances que se han logrado en términos de paz.

### **La coalición entre Fajardo, Robledo y López debe ser más amplia** 

“Hay que sumar con otros actores políticos muy importantes en Colombia como lo son **Clara López, Piedad Córdoba, Humberto de La Calle y como lo es el mismo Petro**” afirmó la congresista y agregó que la intensión es impedir la llegada de Uribe o Vargas Lleras a la presidencia. (Le puede interesar: ["Listos los 78 preseleccionados para conformar la JEP"](https://archivo.contagioradio.com/jep-magistrados/))

Frente a la coalición Ángela María Robledo, expresó que celebra la alianza, pero no es suficiente ni tiene una formulación clara sobre la paz posible, **“bastante tibios sobre el apoyo a la paz**, acá no se puede pasar la página sobre la guerra, no la hemos pasado y esto debe estar en la primera línea”.

Sobre la participación de los otros candidatos a las elecciones que hace parte de la izquierda u otras tendencias políticas, Robledo manifestó que las posibilidades de que esta unión se dé pueden ser altas “hace años veía imposible una alianza entre Robledo y Fajardo, sin embargo, han entendido que hay que hacer la tarea”.

### **La corrupción de la que no habla Claudia López **

Una de las banderas que ha acogido Claudia López en su programa político es la lucha contra la corrupción, sin embargo, de acuerdo con la representante Ángela María Robledo, hace falta que la senadora aclare sus posturas frente a la construcción de paz, pues ha **votado negativamente varios proyectos y la actual alcaldía de Enrique Peñalosa**.

“Ella ha guardado un silencio que a mí me parece bastante sospechoso frente a esta administración” afirmó Robledo y agregó que, sobre la falta de planeación, los frenos que Peñalosa a puesto a la construcción del metro, entre otras problemáticas, no ha escuchado un pronunciamiento por parte de López. (Le puede interesar: ["No hay excusa para seguir dilatanco la revocatoria a Pañalosa: Unidos Revocaremos a Peñalosa"](https://archivo.contagioradio.com/no-hay-excusa-para-dilatar-revocatoria/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
