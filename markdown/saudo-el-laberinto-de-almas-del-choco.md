Title: Terror en el Pacífico con "Saudó Laberinto de Almas"
Date: 2016-08-05 10:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Chocó, Cine Colombiano, Saudó
Slug: saudo-el-laberinto-de-almas-del-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/57150_3928_imagen__-e1470416324689.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Antorcha films 

##### [04 Ago 2016] 

Bajo la dirección del cineasta chocoano Jhonny Hendrix Hinestroza llega a las salas de cine la película “Saudó Laberinto de Almas” un film de suspenso y de terror producido por Antorcha Films, que enseña parte de las leyendas y mitología del Pacífico colombiano.

[![57150\_3928\_imagen\_\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/57150_3928_imagen__-e1470416324689.jpg "Foto: Proimagenes Colombia")

El largometraje se enfoca en la historia de Elías, un joven  afrodescendiente que al cumplir los 13 años de edad, logra escapar de Saudó, un lugar  marcado por los rezos y ritos de brujería. Después de varios años, ya casado y convertido en  médico, Elías tendrá que verse con su pasado cuando su hijo Francisco, empieza a sufrir de extrañas pesadillas al cumplir la misma edad que tenía él cuando  huyó.

Para ayudar a su hijo deberá embarcarse en un largo viaje que lo llevará de vuelta a Saudó, un pueblo olvidado y lúgubre, en donde buscará la manera de salvarlo.

<iframe src="https://www.youtube.com/embed/2UiUK9gBeLw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[La película que empezó su rodaje en 2014 y que se encuentra desde hoy  en las principales salas de cine del país, muestra  un misticismo  muy escaso en la filmografía colombiana, que enmarca esas historias de los abuelos que alguna vez fueron contadas, y que hacen parte de la tradición cultural colombiana.]
