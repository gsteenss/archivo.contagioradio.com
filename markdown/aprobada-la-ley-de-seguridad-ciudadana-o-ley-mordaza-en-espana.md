Title: Aprobada la ley de seguridad ciudadana o ley Mordaza en España
Date: 2014-12-12 14:18
Author: CtgAdm
Category: El mundo
Slug: aprobada-la-ley-de-seguridad-ciudadana-o-ley-mordaza-en-espana
Status: published

Foto: sinaloadiario.mx

Calificada por organizaciones de DDHH y movimientos sociales como grave retroceso de los derechos de libertad y expresión. Hoy ha sido aprobada con los votos del conservador Partido Popular, actualmente en el gobierno. No solo criminaliza las fuertes protestas que está viviendo el país a causa de los recortes sociales y económicos impuestos por la Troika, sino que también persigue al inmigrante ilegal, respondiendo así a las directrices de la UE, con el objetivo de blindar Algunos ejemplos de la represión que va a significar:  
-Realizar fotos a policías (600 euros)  
-Ocupar un banco a modo de desobediencia civil (300 euros)  
-Realizar asambleas en zonas públicas (600 Euros)  
-Manifestarse en frente del congreso (30.000 euros)
