Title: Esta semana se define si se penaliza o no el maltrato animal en Colombia
Date: 2015-12-01 14:41
Category: Animales, Nacional
Tags: Animales, Claudia López, Congreso de la República, derechos de los animales, Juan Carlos Lozada, Juan Manuel Galán, Ley Animal, Ley Animalista, Natalia Parra, Plataforma Alto, Proyecto de Ley, Proyecto de Ley 172
Slug: esta-semana-se-define-si-se-penaliza-o-no-el-maltrato-animal-en-colombia
Status: published

###### Foto: contagioradio.com 

###### [01 Nov 2015] 

Este martes primero de diciembre, está programada la votación de la Ley 172, también conocida como 'Ley Animalista' en el Senado de la República, en donde se busca garantizar los derechos de los animales y sancionar a quienes los vulneren. Si el proyecto de ley es aprobado, los animales dejarían de ser bienes muebles, penalizando el maltrato hacia ellos.

Natalia Parra, directora de la Plataforma ALTO y Secretaria Técnica de la Bancada Animalista del Congreso de la República, señaló que la senadora Claudia López presentó dos importantes proposiciones: la primera se trata de fortalecer el tema de la sintiencia de los animales en el Código Civil; y la segunda está dirigida a que no se puedan destinar recursos públicos para espectáculos donde se maltrate animales. "No es exactamente prohibir, pero por lo menos con los impuestos de los ciudadanos no se podrán financiar esos eventos crueles, así que no hay razón para el pataleo del lobby taurino", expresa la defensora.

Con esta iniciativa, que ha sido impulsada por el senador Juan Manuel Galán, las conductas como el maltrato animal, la zoofilia, los tratos crueles y denigrantes serían castigadas. Además, se reformaría el Estatuto Nacional de Protección de Animales con el objetivo de actualizar el valor de las multas y facilitar la competencia de las autoridades de policía.

Para el cumplimiento de la Ley, en caso de que sea aprobada, se le otorgaría a la Policía Nacional la facultad de realizar aprehensiones preventivas de animales que están siendo objeto de maltrato. También, se crearía un título en el Código Penal que consagre el tipo penal de maltrato animal y cinco circunstancias de agravación punitiva y además se modificaría el Código Procedimiento Penal.

De acuerdo al representante Juan Carlos Lozada, la penalización del maltrato hacia los animales otorgaría “un nuevo estatus a los considerados como seres sintientes". Según afirma, el proyecto está fortalecido, puesto que en los tres debates de la Comisión Primera del Senado en donde se ha discutido, ha sido aprobado por unanimidad. De pasar en este debate, la Ley Animalista, continuaría su paso a sanción presidencial y a revisión de la Corte Constitucional.
