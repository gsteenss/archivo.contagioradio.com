Title: "Es imposible llenar el vacío que dejó Carlos Muñoz"
Date: 2016-02-01 12:57
Author: AdminContagio
Category: 24 Cuadros
Tags: Carlos Muñoz, Festival Internacional de cine de Santander, Oscar Fonseca FICS
Slug: es-imposible-llenar-el-vacio-que-dejo-carlos-munoz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/93569d6fd12d4761aaedbda610c81a01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombia.com 

<iframe src="http://www.ivoox.com/player_ek_10276347_2_1.html?data=kpWfmZuXeJihhpywj5aUaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncYamk6rgjc7RtNDnysfZx5DQsMbiwteYx9GPusLXhqigh6aos4zl1sqYxsrOaaSnhqegjajFts3j1JC614qnd4a2ktTnh5eWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Oscar Fonseca, Dir. FICS] 

###### [01 Feb 2016.] 

La octava edición del Festival Internacional de Cine de Santander (FICS), una de las citas culturales con mayor acogida del oriente colombiano, estará enmarcada por el recuerdo del recientemente fallecido actor Carlos Muñoz, mentor y principal vocero del evento cinematográfico de su región natal.

Desde noviembre de 2015, meses antes de la partida del consagrado actor nacido en Puente Nacional, Oscar Fonseca quien hasta entonces había desempeñado la gerencia del Festival desde su nacimiento, asumió las riendas del FICS con la responsabilidad de continuar con el destacado trabajo realizado por su predecesora inmediata Erika Salazar.

En entrevista con Contagio radio, Fonseca compartió algunos detalles de lo que será el evento, desde su antesala en el mes de febrero y las actividades de sala que tendrán lugar entre el 2 y el 6 de agosto próximo.

**Tributo al maestro Carlos Muñoz.**

El sentimiento de pesadumbre y tristeza que dejó la noticia del fallecimiento del protagonista de seriados como “Yo y tu”, “Caballo Viejo”, “San Tropel” entre muchas otras, es compartido por el director del Festival, quien considera “imposible llenar el vacío que deja Carlitos Muñoz, no solo para nosotros como amigos, ni al festival sino a todos los santandereanos” al ser a su juicio el mayor embajador de sus paisanos en el país y en el mundo.

El importante legado, plasmado en el trabajo realizado delante y tras las cámaras, particularmente en la organización del Festival, merece todos los reconocimientos que el actor no quiso recibir en vida argumentando que el “estaba para trabajar y no para recibir homenajes” afirma Fonseca y que el tributo que se rendirá en la presente edición es devolverle tan sólo el “0.01% de todo lo que él nos ha brindado”.

La organización del FICS, trabaja en recopilar las cerca de 20 producciones en las que participó el actor santandereano, que por su antigüedad y falta de conservación resulta ser una difícil labor que vale la pena realizar a fin de realizar una retrospectiva el 4 de agosto, con la que el público revivirá las interpretaciones del maestro en la gran pantalla.

**Formar públicos continua siendo el reto.**

Como viene ocurriendo desde su concepción, el Festival Internacional de cine de Santander, tiene como apuesta principal la formación de públicos, una de las ruedas que aún no engranan en lo que al cine nacional se refiere, a través de la iniciativa “Semilleros de cineastas” de la que participan cerca de 10 mil estudiantes de instituciones educativas públicas del Departamento.

Además de enseñar a los más jóvenes a ver otros tipos de cine al que habitualmente se ofrecen en las salas comerciales, entre el numeroso grupo de estudiantes, se realiza una selección primero de 300 y luego de 30 que demuestren mayor gusto y afinidad por la propuesta, quienes recibirán talleres de formación que les permita realizar sus propios cortometrajes.

Seguir trabajando con los jóvenes realizadores de la región que se preparan profesionalmente en la academia, con talleres teórico- prácticos especializados, que sirvan para complementar su formación y seguir llevando cine a las diferentes regiones de Santander en un proceso de “descentralización” son otros de los grandes retos que tiene Fonseca en la dirección del FICS, y que hacen parte del legado construido por Carlos Muñoz.

**Convocatoria**

El próximo 10 de febrero a través de la página www.ficsfestival.co estará abierta la convocatoria para las producciones interesadas en hacer parte de la sección en competencia del evento. Tanto realizadores nacionales como internacionales podrán inscribirse, incluyendo como categoría especial en esta edición una destinada al cortometrajes de estudiantes universitarios.

**Sobre el nuevo director**.

Óscar Fonseca es administrador de empresas de la Corporación Educativa de Colombia IDEAS de Bogotá y comunicador social y periodista de la Universidad Remington. Su experiencia en múltiples seriados de televisión desde 1996, tales como Aprendiendo a vivir, Luz de luna, Mi ángel de la guarda, entre otros, lo llevaron a vincularse en la industria audiovisual.  
Su proyecto más reciente fue la realización de cinco cortometrajes en unión con el Canal TRO, Además de la Autoridad Nacional de Televisión. También, ha sido invitado al Mercado de Industrias Culturales del Sur, Micsur, para compartir sus conocimientos en televisión regional.
