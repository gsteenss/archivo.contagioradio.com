Title: Condonación de deuda, control a capitales y emisión productiva: La posible trilogía económica
Date: 2020-04-08 20:57
Author: CtgAdm
Category: Actualidad, Economía
Tags: Deuda pública, Duque, Fondo Monetario Internacional, Gobierno
Slug: condonacion-de-deuda-externa-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Deuda-externa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @graffitiborrao {#foto-graffitiborrao .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Expertos y líderes políticos de latinoamérica han señalado la necesidad de que se condone la deuda del continente (el más desigual del mundo), para que los gobiernos tengan la liquidez suficiente para afrontar la crisis económica generada por el Covid-19, cuyos efectos ya empiezan a sentir las poblaciones más vulnerables. Sin embargo, la medida es solo una de varias propuestas que han surgido para atender la situación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Condonación de la deuda?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las organizaciones que apoya la iniciativa es el Centro Estratégico Latinoamericano de Geopolítica [(CELAG)](https://www.celag.org/la-hora-de-la-condonacion-de-la-deuda-para-america-latina/), que específicamente solicita "la condonación de la deuda externa soberana de los países de América Latina por parte de FMI (Fondo Monetario Internacional) y de otros organismos multilaterales" como el Banco Interamericano de Desarrollo (BID), Banco Mundial (BM) o el Banco de Desarrollo de América Latina, antes Corporación Andina de Fomento (CAF).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la CELAG insta a los prestamistas privados internacionales que acepten un proceso de reestructuración de la deuda "que contemple una mora absoluta de dos años sin intereses". Acciones tendientes a evitar que el pago de la deuda sea un obstáculo más que añadir al ya "complejo reto de superar este momento social y económico tan crítico".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el Centro referencia que hay un sinfín de casos en los que la deuda externa fue perdonada, uno de ellos fue el de Alemania tras la segunda guerra mundial a la que se le condonó "cantidades sustanciales de sus deuda". (Le puede interesar:["La propuesta de FARC para salvar la economía popular"](https://archivo.contagioradio.com/la-propuesta-de-farc-para-salvar-la-economia-popular/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Álvaro Gallardo, profesor de economía e integrante del Grupo de Socioeconomía, Instituciones y Desarrollo de la Universidad Nacional agrega que en los años 80's, cuando se presentaron crisis de la deuda, también hubo condonaciones. Sin embargo, sostiene que la crisis en esta ocasión es diferente porque no tiene origen directo en el mundo económico y financiero.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La trilogía posible**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gallardo sostiene que no obstante, la condonación de la deuda por sí sola "no sería útil y podría ser más problemática", por lo que habría que evaluar otras medidas conjuntas para evitar que la economía se vea afectada por una masiva fuga de capitales producto de la especulación. Por esta razón propuso lo que él llamó la trilogía posible: Condonación de la deuda, control de capitales y emisión productiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El profesor explica que Colombia tiene una devaluación alta del peso, enfrenta una expectativa de ingresos futuros precarios porque depende esencialmente del petróleo (cuyo precio está relativamente bajo) y el nivel de exportación es reducido también por lo tanto no cuenta con recursos. Adicionalmente, señala que en el próximo semestre se prevé una fuga de capitales dada la incertidumbre del momento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, sostiene que es necesaria una emisión del banco central que garantice las cadenas de abastecimiento sin generar inflación, y no emitiendo dinero nominal para ayudar a bancos ni a través de endeudamiento, "como es la característica hoy". (Le puede interesar: ["Microeconomías colombianas las más afectadas por el aislamiento del Covid-19"](https://archivo.contagioradio.com/microeconomias-colombianas-las-mas-afectadas-por-la-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente el control de capitales, mediante impuestos a las grandes transacciones o topes a las mismas, evitaría la salida masiva de dineros del país como se tiene previsto. (Le puede interesar: ["La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz"](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La condonación sería el último elemento paliativo para enfrentar la crisis, según explica Gallardo, en este momento estamos utilizando 14,5 billones de pesos para la contención y mitigación de sus efectos pero esto es insuficiente. "Nosotros en deuda externa tenemos cerca de 23 billones de pesos", sostiene, y añadiendo que incluso este dinero sería insuficiente porque los efectos reales de lo generado no se verán ahora sino después.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es decir, el Gobierno requerirá flujo de caja para intentar salvar el aparato productivo: pequeñas y medianas empresas, productores campesinos, sectores exportadores. Por lo tanto, afirma que "la misma deuda es un elemento paliativo", y son necesarias medidas de choque heterodoxas y fuertes para que Colombia tenga los ingresos necesarios para mejorar su economía en el largo plazo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
