Title: "No hay logística ni recursos para que la consulta antitaurina sea el 13 de agosto"
Date: 2017-08-07 14:51
Category: Animales, Nacional
Tags: Animalistas, consulta antituarina, corridas de toros
Slug: corte-constitucional-no
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DSC_0478-1280x640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ALTO 

###### [7 Ago 2017] 

Aunque en los últimos días diversos medios de comunicación han asegurado que la consulta antitaurina será un hecho el próximo 13 de agosto, los promotores de dicha iniciativa han señalado que, "**En realidad lo que la Corte Constitucional expresó, es que no es la competente para decidir fechas**. Tan abierta queda la posibilidad, de que sea el 13 de agosto, como cualquier fecha, que no expresa ninguna", dice en un comunicado la Plataforma ALTO.

Según explican los animalistas, a lo que ha dado paso la Corte es a que sea **la administración, en coordinación con la Organización Electoral, quienes decidan el día más pertinente** "para que se den todas las garantías de la participación ciudadana y además se respete la moralidad administrativa reduciendo el costo público en la realización del ejercicio", ya que hasta el momento, **no existen los requerimientos financieros ni logísticos para que en menos de una semana pueda haber consulta**.

Teniendo en cuenta el panorama en el que se visualiza una suma de más de **40 mil millones de pesos para realizar la consulta**, y además, el 13 de agosto se lleva acabo las pruebas saber 11, la Plataforma ALTO insiste en la necesidad de reprogramar la fecha. La alcaldía "puede replantearla, en el entendido que **la ley 1757 de 2015** no prevé la situación para los casos en los cuales no hay disponibilidad de recursos, únicamente establece por un lado el término dentro del cual se deben llevar a cabo los comicios, o por otro lado, el vencimiento del plazo indicado para ello".

### **Los factores** 

Los promotores argumentan que de **permitir la consulta con las elecciones ordinarias de congresistas**, se cumpliría con el principio de racionalización del gasto público **logrando la optimización y el mejoramiento de las finanzas públicas**.

Asimismo, resaltan que **el gobierno ya avisó que no existe el presupuesto para organizar unos comicios extraordinarios**, faltando tan pocos días. "**Si la consulta se hace en cualquier fecha, le costaría al erario cerca de 40 mil millones de pesos, si se hace en unas ordinarias se reduciría ese gasto más de un 90%**", explican.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
