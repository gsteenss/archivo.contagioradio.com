Title: Asesinan Manuel Ramírez Mosquera, lider de comunidades negras en Truando, Chocó
Date: 2017-08-17 19:42
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Chocó, Manuel Ramírez, Truandó
Slug: manuel-ramirez-mosquera-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [17 Ago 2017]

Otro lamentable asesinato de una persona, reclamante de tierras se conoció la tarde de este jueves. Se trata de Manuel Ramírez Mosquera, uno de los lideres de los 2500 habitantes del Bajo Atrto Chocoano que desde hace varios meses se encuentran desplazados en el casco urbano de Rio Sucio debido al accionar de los paramilitares en sus terrtorios.

**Según las primeras informaciones, Manuel Ramírez se habría embarcado en un bote desde Rio Sucio hacia la comunidad “La Nueva”** ubicada en el territorio colectivo de Truandó, los pobladores afirman que se embarcó con su hermano **para ir abastecerse de algunas cosechas o cultivos de Pan Coger y regresar al casco urbano**. Sin embargo, tan solo dos horas después se conoció que el líder habría sido asesinado cerca de la comunidad a la que se dirigía.

### **Bajo Atrato sigue sitiado por paramilitares ante la mirada de las FFMM** 

Manuel hace parte de las mas de 320 familias que desde Mayo pasado salieron huyendo de sus territorios en el Bajo Atrato Chocoano, **cuando la toma del poder por los grupos paramilitares y los reiterados enfrentamientos entre esas estructuras paramilitares** y el ELN obligaron a muchas familias a estar confinadas y a muchas de ellas a huir.

**Desde esa fecha se ha denunciado que la situación humanitaria y de seguridad continúa siendo preocupante** dado que en los albergues no cuentan con las condiciones necesarias, el agua potable escasea, las lluvias los han inundado, los niños tienen problemas de salud y la atención ha sido casi nula. [Lea también: 600 personas habitantes del chocó desplazados en riosucio por acciones paramiliitares](https://archivo.contagioradio.com/38661/)

También el asesinato de Manuel se conoce justo el día en que se ha revelado que durante el primer semestre de 2017 51 defensores de DDHH fueron asesinados y más de 300 fueron amenazados. Además se han denunciado también **los asesinatos de cerca de 160 personas entre defensores y líderes sociales. **

Este asesinato se suma a los recientes hechos similares que cegaron la vida de Bernardo Cuero, William Mina y Alberto Acosta. Todos ellos líderes sociales que estaban impulsando propuestas de protección propias como la guarida campesina, **o que se habían comprometido directamente con la implementación del acuerdo de paz.**

###### Reciba toda la información de Contagio Radio en [[su correo]
