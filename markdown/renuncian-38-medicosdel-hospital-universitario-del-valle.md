Title: Renuncian 38 médicos del Hospital Universitario del Valle
Date: 2015-12-17 11:24
Category: DDHH, Nacional
Tags: Crisis en Hospital Universitario del Valle, EPS, Hospital Universitario del Valle, julian mora, Médicos, mÉDICOS SUBCONTRATADOS
Slug: renuncian-38-medicosdel-hospital-universitario-del-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/huv-22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: valledelcauca.gov.co 

<iframe src="http://www.ivoox.com/player_ek_9770323_2_1.html?data=mpykkpiWd46ZmKiak5qJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbi1tPQy8bSb5SsjNKSpZiJhZrYysjc1ZDIqc2fqdTg0s7Ypc2fttPW2MrWt8rowtfW0ZDIqc2ft8aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Julian Mora, HUV] 

###### [16 Dic 2015] 

A estos profesionales de la salud, se les sumarían 5 más que renunciarían a su cargo, lo que dejaría un saldo de 58 doctores entre los que se encuentran médicos generales y especialistas. Sus motivaciones tienen que ver con las pésimas condiciones laborales en las que se encuentran, además de la ausencia de insumos clínicos y especialistas que puedan atender los requerimientos de la población dentro del Hospital Universitario del Valle.

Julián Mora, quien es representante de médicos generales de la entidad, añade que la falta de apoyo especializado y las difíciles condiciones de los trabajadores del hospital “son incompatibles con la ley, puesto que hay unos principios éticos que se están violando permanentemente y además no existen las condiciones mínimas para la prestación del servicio”.

Mora ha expresado que han intentado establecer un puente de diálogo con la administración del hospital para mejorar la situación, pero no han recibido respuestas concretas; por el contrario, señala que la concertación no se ha llevado a cabo debido a que no hay existe la voluntad de la contraparte. “Lo que ellos han dicho en los últimos días es que los servicios que se prestan están cubiertos, a pesar de que el hospital se quedó sin especialistas, ortopedistas, anestesiólogos y radiólogos, solo con nosotros que somos médicos generales y no estábamos produciendo absolutamente nada”.

Por otra parte, el representante agrega que la reunión que se tenía programada con funcionarios del Ministerio de Salud se ha ido dilatando: “había programada una junta directiva el 15 de diciembre, pero se canceló y trasladó para el 21 de diciembre por la urgencia de la citación en Bogotá de alguno de los participantes”. En ese orden de ideas, los médicos han informado a la comunidad por distintos medios acerca de la situación que atraviesa el Hospital Universitario del Valle con la intención de poner en conocimiento el problema a la sociedad.

Los problemas de los médicos tienen que ver con dos aspectos fundamentales: el primero orbita sobre el aspecto laboral y contractual: según Mora, hay 3 sentencias de la Corte Constitucional, un decreto presidencial y una circular de la Procuraduría que establecen un marco jurídico en donde se dice que todo empleado de un hospital público debe ser contratado de manera directa. “Pero lo que pasa en  el el HUV es que tienen un modelo de contratación a través de falsos sindicatos que hacen contrataciones sin ningún tipo de control por el Ministerio del Trabajo”. Además, se denuncia que bajo la modalidad de la tercerización laboral, los médicos están cotizando EPS sobre un Salario Mínimo Legal Vigente y carecen de prestaciones sociales.

Un segundo aspecto tiene que ver con la prestación y el servicio. Vale la pena destacar que el centro médico es un hospital de tercer y cuarto nivel, en donde se requiere de un amplio número de especialistas que estén dispuestos a prestar cualquier tipo de servicio médico y en este momento, Mora reitera que los profesionales que aún están vinculados contractualmente en el Hospital no son los suficientes para cubrir la demanda de pacientes.

Este próximo viernes 18 de diciembre los médicos, junto con estudiantes de la Universidad del Valle y otras organizaciones tienen programada una movilización en donde se realizará una protesta para la mejora de las condiciones del Hospital Universitario del Valle, uno de los hospitales más importantes de esa región del país.
