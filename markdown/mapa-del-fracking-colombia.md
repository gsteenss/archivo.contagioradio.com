Title: El mapa del fracking en Colombia
Date: 2016-12-13 18:01
Category: Ambiente, Voces de la Tierra
Tags: fracking, fracturamiento hidráulico, petroleo
Slug: mapa-del-fracking-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fracking-e1472854044186.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Silla Vacía 

###### [13 Dic 2016] 

Son 43 bloques petroleros en Colombia los que aparecen clasificados como **‘yacimientos no convencionales de hidrocarburos’,** es decir, que podrían ser destinados para fracturamiento hidráulico o fracking, como lo evidencia un mapa realizado por Fidel Mingorance, de acuerdo con los datos del informe ‘Mapa de tierras’ de la Autoridad Nacional de Hidrocarburos, ANH, publicado el pasado 19 de octubre de este año.

Según el mapa, de esos bloques, hay licitados 7 para exploración, existe uno en reserva, y ninguno para explotación. Sin embargo, Camilo Prieto, vocero del Movimiento Ambientalista Colombiano aseguró en una entrevista a Contagio Radio, que realmente se trata de 22 bloques autorizados y seis contratos suscritos.

Estos bloques se encuentran ubicados en **Cesar, San Martín; Tolima; Santander; Norte de Santander; y en Boyacá y Cundinamarca cerca al páramo de Chingaza.** Allí, Ecopetrol, Exxon Mobil, Parex, [Conocophillips](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/) y CNE-Oil, son las empresas que tienen concesiones sobre esos territorios.

### ¿Qué dicen las instituciones? 

La Autoridad Nacional de Licencias Ambientales, ANLA, y el Ministerios de Ambiente, ha aseverado que solo existe un permiso de exploración denominado La Loma, en el Cesar. Sin embargo, también es cierto que Orlando Velandia, presidente de la ANH, ha señalado que Colombia necesita el fracking para salir de la dependencia de los yacimientos convencionales.

A su vez, Juan Carlos Echeverry, presidente de Ecopetrol ha dicho, **“No podemos darnos el lujo de no hacer fracking”**, incluso, **en la Ronda Colombia 2014, se ofreció un total de 98 bloques para explotación de hidrocarburos**, 19 fueron ofrecidos para hacer fracking de petróleo. A su vez, Echeverry ha argumentado que “el petróleo será el combustible para la paz”.

Por su parte, el Minambiente ha indicado que las aguas subterráneas aptas para el ser humano se encuentran entre los 300-500 metros, y **los yacimientos no convencionales están entre los 1.500 y los 2.400 metros de profundidad**, lo que pone en riesgo la contaminación de esos acuíferos durante el proceso del fracking. Pese a ello, desde ese gabinete se ha afirmado que “con un apropiado diseño y construcción de los pozos, verificación en su integridad y monitoreo permanente de la misma se previene la potencial contaminación de los acuíferos”. [(Le puede interesar: Las incoherencias de Colombia frente al fracking).](https://archivo.contagioradio.com/las-incoherencias-de-colombia-despues-del-acuerdo-de-paris-frente-al-fracking/)

### Las consecuencias 

Camilo Prieto, reconoce que la demanda interna de petróleo que tiene el país sobrepasa la producción petrolera actual, de hecho, actualmente apenas se cuenta con 7 años de reservas tanto de gas como de petróleo, pero también advierte que se debe pensar en los costos ambientales que generaría la práctica del fracking en un país donde el **75% de los recursos hídricos se encuentran en las fuentes subterráneas.**

El fracturamiento hidráulico requiere de entre **9 y 26 millones de litros de agua por pozo y año, pero** **solo puede reutilizar un 15% de esa agua,** explica Prieto. Aunque Colombia estudia la manera de reducir las tasas de material radioactivo en el agua que sale residual del fracking, no se ha logrado en ninguna parte del planeta disminuir esos índices hasta obtener los niveles aceptados por los entes de salud pública del mundo, para que se pueda demostrar que esa técnica se puede realizar de manera responsable.

Ell vocero del Movimiento Ambientalista explica que **en el país no existen estudios de geotectónica amplios.** Asimismo, indica que los análisis de hidrogeología son completamente limitados, lo que quiere decir, que no hay mapas geológicos certeros sobre la situación en la que se encuentra la zona donde se pensaría realizar el Fracking, y si llega a haber una fuente de agua subterránea donde se lleve a cabo esa actividad, sería muy posible que esta termine con **residuos tóxicos y sustancias radioactivas que pueden ocasionar cáncer, o el gas metano puede generar que el agua se vuelva inflamable.** [(Le puede interesar: Informe revela impactos del fracking en América Latina)](https://archivo.contagioradio.com/impactos-del-fracking-en-6-paises-de-latinoamerica/)

\[caption id="attachment\_33683" align="aligncenter" width="419"\]![Fidel Mingorance](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Mapa-del-fracking-en-Colombia.png){.wp-image-33683 .size-full width="419" height="592"} Fidel Mingorance\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]
