Title: Inicia la tercera sesión de la Cátedra Abierta: Sujetos territoriales de paz con justicia socio-ambiental
Date: 2016-10-11 16:18
Category: Nacional, Paz
Tags: Carta abierta de CONPAZ, Dignidad de las víctimas, paz
Slug: inicia-la-tercera-sesion-de-la-catedra-abierta-sujetos-territoriales-de-paz-con-justicia-socio-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/catedra-justicia-y-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 de Oct 2016] 

Del 9 al 14 de octubre, se llevará a cabo en Bogotá, la tercera sesión de la Cátedra Abierta: Sujetos territoriales de paz con justicia socio-ambiental, el evento contará con la participación aproximadamente **100 líderes y lideresas, entre jóvenes y adultos de diferentes regiones del país afectadas por la violencia.**

La Cátedra consta de tres áreas de formación, **Territorio y Ambiente, Género e Identidades y Participación y Afirmación de Derechos**, que se desarrollarán a lo largo de estos días y que finalizan dos siclos previos realizados en regiones como el Bajo Atrato Chocoano. Lea también: ["Avanza la Cátedra Abierta: Sujetos territoriales de paz con justicia socio-ambiental en el Bajo Atrato Chocoano"](https://archivo.contagioradio.com/avanza-la-catedra-sujetos-territoriales-con-paz-y-justicia-socioambiental-en-el-bajo-atrato-chocoano/)

Para esta sesión se ha contado con los aportes de panelistas como Victoria Sandino, miembro de la delegación de paz de las FARC-EP, David Flores, vocero de Marcha Patriótica,  Alberto Castilla, senador del Polo Democrático, entre otros.

**El cierre de la Cátedra contará con el lanzamiento del disco “Voces de Paz” una recopilación musical de canciones hechas por víctimas del conflicto armado,** algunas de ellas pertenecientes a las Comunidades Construyendo Paz (CONPAZ), que a través de los ritmos y las letras encontraron otra forma de construir vida y dignidad en sus territorios, la entrada será gratuita y el lugar será el Aula Magna de la Universidad Santo Tomas, a las 6:00pm.

<iframe id="audio_13276929" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13276929_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
