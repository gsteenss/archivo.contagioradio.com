Title: ¿Qué implicaciones tiene el cese al fuego unilateral del ELN para semana santa?
Date: 2019-04-12 22:27
Author: CtgAdm
Category: Paz
Tags: #DefendamosLaPaz, Cese al fuego, ELN, paz
Slug: implicacionesl-cese-al-fuego-unilateral-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado jueves 11 de abril se divulgó un comunicado del Ejército de Liberación Nacional (ELN), en el que anunció un cese al fuego unilateral desde el domingo 14 de abril hasta el domingo 21 del mes; decisión tomada como respuesta a la iniciativa \#DefendamosLaPaz, que solicitó a esa guerrilla establecer un cese al fuego unilateral e indefinido para demostrar su voluntad de diálogo. Sin embargo, la muestra de voluntad no tendría los efectos esperados de cara al Gobierno, pues el Alto Comisionado para la Paz desestimó la acción.

> No, comisionado Ceballos, los ceses al fuego no son “presagios de tragedias”. Son actos humanitarios que ahorran vidas y sufrimientos, y gestos que abren paso al diálogo. Un asunto tan serio como la paz debería merecer un tratamiento serio y no demagógico por parte del gobierno
>
> — Iván Cepeda Castro (@IvanCepedaCast) [12 de abril de 2019](https://twitter.com/IvanCepedaCast/status/1116535593691959296?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Como lo explicó el analista y profesor Luis Eduardo Celis, la reacción del alto comisionado Miguel Ceballos, fue dura al señalar que tras cada cese al fuego "viene una arremetida muy fuerte del ELN", y que los términos del Gobierno son un cese al fuego unilateral indefinido, con liberación de todos los secuestrados. Condiciones expresadas por el presidente Duque antes de su posesión, y mantenidas a lo largo de este tiempo.

Para Celis, bajo esas condiciones la decisión del ELN resulta ser una buena noticia para las comunidades "donde se da la confrontación", pero no se traduce en oportunidades reales para que haya algún contacto entre Gobierno y ELN para continuar los diálogos de paz. En perspectiva del Profesor, la negociación es tan difícil porque ambos actores siguen en "lógicas muy distantes". (Le puede interesar: ["Romper conversaciones con ELN implica un nuevo ciclo de violencia"](https://archivo.contagioradio.com/romper-conversaciones-con-eln-implica-un-nuevo-ciclo-de-violencia/))

Por una parte, el Gobierno que no ha cambiado su discurso frente a las exigencias hechas desde el principio, pese a la liberación el año pasado por parte de la guerrilla de más de 20 secuestrados y los ceses al fuego decretados y cumplidos; y por otra parte del ELN, que responde con un cese de una semana al pedido hecho por \#DefendamosLaPaz en lugar de un cese indefinido. Razón por la que en el corto plazo, Celis no cree que ambos vayan a cambiar en algo sus lógicas.

### **Una Paz Sin Dolientes**

El próximo sábado 4 de mayo, Celis presentará el libro "Una Paz Sin Dolientes en la Feria del Libro de Bogotá, en el que el lector encontrará lo que ha sido la relación del ELN con el Estado a lo largo de varios periodos presidenciales. Serán 8 capítulos con 8 Gobiernos, iniciando desde el expresidente Betancur, considerado como el comienzo de las negociaciones de paz contemporáneas, en las cuales no participó esta guerrilla. Con el documento, el Profesor asegurò que busca afianzar la idea de que la negociación "debe madurar", produciendo esfuerzos sólidos para alcanzar la paz.

> "Una Paz sin dolientes", es una mirada a los procesos de diálogos y negociaciones con el ELN, para seguir trabajando por la superación de esta rebelión armada. Gracias a ⁦[@NCprensa](https://twitter.com/NCprensa?ref_src=twsrc%5Etfw)⁩ ⁦[@ManuelBol\_NC](https://twitter.com/ManuelBol_NC?ref_src=twsrc%5Etfw)⁩ por hacerlo posible. Presentación en la ⁦[@FILBogota](https://twitter.com/FILBogota?ref_src=twsrc%5Etfw)⁩ Sábado 4. [pic.twitter.com/Pc5r6XdrUS](https://t.co/Pc5r6XdrUS)
>
> — Luis Eduardo Celis (@luchoceliscnai) [8 de abril de 2019](https://twitter.com/luchoceliscnai/status/1115251321815412737?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
[Comunicado ELN, abril 11 de 2019](https://www.scribd.com/document/405954237/El-ELN-anuncia-un-nuevo-cese-al-fuego-en-Semana-Santa#from_embed "View Comunicado ELN, abril 11 de 2019 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_17053" class="scribd_iframe_embed" title="Comunicado ELN, abril 11 de 2019" src="https://es.scribd.com/embeds/406065914/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bxJEt0WKhoLvYu7ajskJ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_34414356" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34414356_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
