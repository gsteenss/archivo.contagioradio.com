Title: Control paramilitar y empresarial impide restitución de tierras en Curvaradó y Jiguamiandó
Date: 2015-09-12 11:24
Category: DDHH, Judicial, Nacional
Tags: Abogados Sin Fronteras Canada, Comisión de Justicia y Paz Colombia, Curvarado, Desplazamiento forzado, Empresas palmeras, impunidad, Jiguamiandó, Paramilitarismo en Chocó, Red CONPAZ
Slug: control-paramilitar-y-empresarial-impide-restitucion-de-tierras-en-curvarado-y-jiguamiando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG_3048.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Sept 2015] 

[Según el informe “Una mirada al desplazamiento forzado: persecución penal, aparatos organizados de poder y restitución de tierras en el contexto colombiano", de Abogados sin Fronteras Canadá y la Comisión Intereclesial de Justicia y Paz, **las comunidades de las cuencas del Curvaradó y el Jiguamiandó no han han tenido la restitución efectiva de sus tierras.**]

[Los aparatos organizados de poder responsables de su desplazamiento forzado no han sido investigados ni sancionados y **la presencia actual de paramilitares en la región constituye una amenaza inminente de posibles nuevos desplazamientos. **]

[La creación de la Ley de víctimas, con un capítulo dedicado a restitución de tierras, en la vigencia del conflicto armado, resulta contradictorio e impide que las comunidades despojadas retornen y permanezcan en sus territorios.]

[La **presencia y actuación de grupos paramilitares articulados con sectores empresariales, en el Curvaradó y Jiguamiandó no garantiza condiciones de seguridad**, ni posibilidades de que sus pobladores ejerzan soberanía en el uso del suelo.]

[Se denuncia que estos grupos armados, en conjunto con empresas palmicultoras, ganaderas y bananeras, están limitando la participación política de los afromestizos en los Consejos Comunitarios Mayores, así como **impidiendo la libre movilidad en la carretera que conduce de Pavarandó hacia Mutatá y emitiendo órdenes de desalojo**. ]

[Pese a las sentencias proferidas por la Corte Constitucional, **no se han investigado ni sancionado a todos los políticos, altos mandos de las fuerzas militares y policiales, agencias de cooperación internacional y miembros de la iglesia católica**, involucrados directamente en la comisión del delito del desplazamiento forzado en esta región, por el financiamiento y expedición de órdenes a los grupos paramilitares que adelantaron operaciones de despojo.   ]

[El informe concluye con que es necesario consolidar sistemas de información amplios y en red, para poder conectar denuncias sobre hechos particulares con las estructuras de poder que intervienen en sucesos de mayor incidencia como el desplazamiento forzado; así como construir **mecanismos participativos para las comunidades en el curso de procesos penales, que posibiliten la indagación de pruebas técnicas** y no solamente testimoniales, como ocurre actualmente.]
