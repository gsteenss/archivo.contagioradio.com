Title: Abierta la convocatoria para el premio Franco-Alemán de Derechos Humanos "Antonio Nariño"
Date: 2016-10-19 16:03
Category: DDHH, Otra Mirada
Tags: Alemania, colombia, defensores de derechos humanos, francia, premio Antonio Nariño
Slug: abierta-la-convocatoria-para-el-premio-franco-aleman-de-derechos-humanos-antonio-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/d731bd888fd90f35-ab6ab.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Premio Antonio Nariño 

##### 19 Oct 2016

Hasta el 21 de octubre está abierta la recepción de propuestas para participar en la **7° edición del Premio Franco-Alemán de Derechos Humanos "Antonio Nariño"**, una distinción que desde su creación en 2009, reconoce la valiosa labor de asociaciones que trabajan en su divulgación, promoción defensa y protección desde distintas regiones colombianas, inspirados en la Declaración Universal establecida en 1948.

Los invitados a postular sus iniciativas, son **personas naturales, instituciones, empresas, organizaciones o grupos que trabajen en favor de la Derechos Humanos, con proyectos que estén siendo implementados en el pais**, como han sido la “Asociación Tierra y Vida” en 2010, las “Tejedoras de Vida” del Putumayo en 2011, “La Fundación Nydia Erika Bautista” en 2012, la Diócesis de Tumaco y su comisión “Vida, Justicia y Paz” en 2013, la Corporación REINICIAR en 2014 y el Museo Casa de la Memoria en 2015.

La elección de la actividades y/o proyectos, nacionales o internacionales, individuales o colectivos que resulten ganadores de **"La Edad de Oro", obra de arte elaborada por Nadin Ospina**, recae en el jurado calificador compuesto, entre otros, por los Embajadores de Alemania y Francia, el Representante para Colombia de la OACNUDH y por lo menos, dos miembros elegidos cada año por los permanentes, todos ellos con voz y voto. El Premio comprende además **un viaje temático a Francia y Alemania organizado por las embajadas de ambas naciones**.

Luego de la selección por parte de los jurados durante los meses de octubre y de noviembre, l**os resultados serán presentados y difundidos en los medios de Bogotá y del resto del país.** La entrega del Premio se realizará en un acto especial que tendrá lugar en el mes de diciembre.

Encuentre [Aquí](http://www.bogota.diplo.de/Vertretung/bogota/es/__pr/MR-Preis_20Ausschreibung_202016.html) las bases y formulario de inscripción para participar.
