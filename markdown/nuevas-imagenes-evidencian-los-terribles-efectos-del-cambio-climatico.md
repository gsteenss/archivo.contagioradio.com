Title: Nuevas imágenes evidencian los terribles efectos del cambio climático
Date: 2015-12-11 01:15
Category: Animales, Voces de la Tierra
Tags: Calentamiento global, Kerstin Langenberger, National Geographic, Oso polar, Paul Nicklen, víctimas del calentamiento global
Slug: nuevas-imagenes-evidencian-los-terribles-efectos-del-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/oso-polar-e1513015973269.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Cristina G Mittermeier] 

###### [11 Dic 2017] 

Esta es la nueva imagen que circula en redes sociales. En ella se evidencia los graves efectos del cambio climático en Baffin Island, Canadá donde un oso polar sufre las consecuencias de las **actividades humanas, que han conllevado a que actualmente el mundo viva en medio de esta crisis climática, que lo ha dejado a él y otros seres vivos sin comida.**

Se trata de una foto donde se muestra al animal desnutrido e intentado buscar comida tomada por la fotografa de National Geographic, Cristina Mittermeier que se ha hecho viral[** por el mensaje que envía. "Es una de las cosas más tristes que he visto", dice la fotógrafa mientras toma las tristes imágenes.**]

<div>

Asimismo la fotógrafa describió el sentimiento de impotencia que le generaba ver esa situación, y le pidió a los lectores que se tomen el cambio climático enserio. Al lado de la fotógrafa, estaba también tomando imágenes el fotógrafo y biólogo de National Geographic, Paul Nicklen, quien no pudo dejar de expresar su indignación ante tal escena.

"Todo mi equipo estaba luchando contra las lágrimas y las emociones cuando grabamos a este oso polar moribundo. Es una escena que destroza el alma y que todavía me atormenta, pero sé que necesitamos compartir tanto lo bello como lo descorazonador si queremos romper los muros de la apatía. **Así es el hambre. Los músculos se atrofian. No hay energía. Es una muerte lenta y dolorosa**".

Cabe recordar que anteriormente una imagen de una osa polar en grave estado de desnutrición, tomada por la fotógrafa alemana Kerstin Langenberger, también llamó la atención del mundo y produjo el rechazo de ambientalistas y animalistas, así como la imagen del Paul Nicklen que publicó a través de su Instagram hace varios meses un **oso muerto con claras señales de desnutrición.**. [(Ver: El cambio climático se frena con voto consciente y movilización social: Susana Muhamad](https://archivo.contagioradio.com/el-cambio-climatico-se-frena-con-voto-consciente-y-movilizacion-social-susana-muhamad/))

[![Oso-polar-770x400](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Oso-polar-770x400-770x400.jpg){.alignnone .size-medium .wp-image-49874 width="770" height="400"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Oso-polar-770x400.jpg)

[![oso-polar-desnutrido-cambio-climatico-566x318 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/oso-polar-desnutrido-cambio-climatico-566x318-1.jpg){.aligncenter .size-full .wp-image-14149 width="712" height="400"}](https://archivo.contagioradio.com/nuevas-imagenes-evidencian-los-terribles-efectos-del-cambio-climatico/oso-polar-desnutrido-cambio-climatico-566x318-1/)

</div>
