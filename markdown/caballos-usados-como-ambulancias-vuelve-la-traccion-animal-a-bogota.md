Title: Caballos usados como ambulancias: vuelve la tracción animal a Bogotá
Date: 2016-07-27 15:19
Category: Animales, Nacional
Tags: Concejo de Bogotá, Maltrato animal, protección animal, Salud, secretaria de salud
Slug: caballos-usados-como-ambulancias-vuelve-la-traccion-animal-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/caballo_ambulancia_2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alcaldía de Bogotá 

###### [27 Jul 2016] 

Nuevamente los caballos son objeto de explotación animal en Bogotá, esta vez por **una medida avalada la Secretaría Distrital de Salud y el Hospital Nazareth** de la localidad de Sumapaz, donde los caballos están siendo usados para prestar el servicio de ambulancia a falta de un sistema efectivo que pueda llegar a las áreas rurales.

Pese a que **con la administración distrital anterior se había logrado erradicar las "zorras" como se conocen popularmente,** o los vehículos de tracción animal, ahora se promociona la estrategia del ‘Caballo Ambulancia’ que promueve la explotación animal y con ello, el maltrato hacia los caballos como lo han denunciado organizaciones animalistas.

Se trata del programa **Respuesta Rural Equina que cuenta con una inversión promedio anual de 750 millones de pesos,** con el que se estaría buscando una forma rápida de transportar a los pacientes al centro de salud. Sin embargo, para Julio César Acosta, concejal de Bogotá por Cambio Radical, debería buscarse otro tipo de vehículo que llegue a estas zonas apartadas pues “estamos en una época donde uno de los lineamientos básicos es la protección de los animales”.

A su vez, para Acosta, este programa con caballos evidencia que el centro regulador de ambulancias de la ciudad se ha quedado corto por ineficiencia operativa. “La Secretaria de Salud es la encargada de hacer la distribución de ambulancias. **Hay una incoherencia en la política pues existe un problema en el envío de estos vehículos”**, y agrega que actualmente el distrito cuenta con 77 ambulancias para una ciudad de 8 millones de personas.

Por su parte, para Reinere de los Ángeles Jaramillo, alcaldesa local de Sumapaz, **“La estrategia busca responder al programa establecido por el Distrito en materia de salud**. El objetivo fundamental es brindarle a la comunidad una solución rápida, eficiente y con un mayor cubrimiento en salud”.

Pero para el concejal de Cambio Radical, lo que debe hacerse es modernizar el despacho de ambulancias que debe ser coordinado por **la Secretaria de Salud que también debería controlar las 680 ambulancias de entidades privadas que se están desperdiciado,** o se usan únicamente para servicios lucrativos, lo que ha generado que se esté usando un “sistema que es arcaico, precario que se hace a mano y sin un control efectivo de las ambulancias”.

Por el momento, la bancada animalista del Concejo de Bogotá también se ha pronunciado sobre este  tipo de maltrato animal, a su vez, se espera un informe de parte de Secretaria de Salud donde se responda varios interrogantes sobre esa estrategia, pues finalmente **se debe garantizar la vida de los animales y un sistema integral de ambulancias, como lo afirma el concejal Acosta.**

<iframe src="http://co.ivoox.com/es/player_ej_12357289_2_1.html?data=kpegl5yWfJqhhpywj5aYaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjaiJh5SZop7gw9ePhcTj1NnOh5enb6Tjz8jSzMbQb8XZjKfcydTYaaSnhqaej4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
