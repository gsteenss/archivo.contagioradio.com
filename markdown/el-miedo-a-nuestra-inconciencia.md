Title: El miedo a nuestra inconciencia
Date: 2016-06-27 08:46
Category: Camilo, Opinion
Tags: FARC-EP, La Habana, proceso de paz, Santos
Slug: el-miedo-a-nuestra-inconciencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/paz-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### 27 jun 2016 

También me sumo a los que estamos contentos; no felices, pero sí contentos. El acuerdo sobre cese bilateral, terminación del conflicto y el del enfrentamiento, pasará a la historia y será juzgado por la actitud de las FARC y de sectores militares para consensuar, pero eso no significa felicidad histórica.

Me alegra saber que  la guerrilla de las FARC EP desde el jueves no usará la violencia en la política. Me temo, sin embargo,  que no será lo mismo con las Fuerzas Militares y policiales, y sus sectores paramilitarizados, porque el espíritu que les sigue inspirando es que son los victoriosos, como lo expresan incluso los opuestos al uribismo.

¡Y acaso puede ser victoriosa la muerte; la cultura de la indiferencia! ¡Qué vanidad  la de los poderosos de nuestra clase política! ¿Los más de 45 mil desaparecidos forzados, los más 220 mil muertos, la mayoría de ellos civiles, según la fuente oficial del Centro de Memoria; o los 6 millones de desplazados internos, esa es la victoria?  o ¿los desarraigados afectados con esa ley de víctimas, que priorizó el techo fiscal sobre la calidad de vida de la mayoría de estos, que son pobres y medianos, que antes eran rurales, y sobre los cuales los que definen las áreas para ser restituidas son los militares, más allá de los jueces?

Ellos seguirán usando y usufructuando desbordadamente la fuerza, cuántas veces lo estimen conveniente para guardar la formalidad del este Estado de Derecho. La violencia legal, poco de legitimidad auténtica ha tenido; la que tiene se basa en la estrategia mercantil y la guerra sicológica del enemigo interno, que solo algunos descubren o reconocen. Su misión constitucional basada en la Doctrina de la Seguridad Nacional ha servido para sostener un orden injusto; para proteger y asegurar la riqueza de una minoría, en las propias fuerzas, y de un sector de políticos y actores privados, corruptos e influyentes, y del propio paramilitarismo. Esas fuerzas han sustentado con la fuerza el acaparamiento de tierras,  la destrucción de las aguas, los bosques, los páramos, los animales y el poder multinacional.

Me alegra la formalidad del jueves porque esa es una  realidad con la que las FARC EP cumplirán. Incluso, me atrevo a expresar, que si existiesen reductos que usen su experticia para desarrollar otras formas de criminalidad, ellos mismos evitarán su reproducción con la ética civilista.

Creo en que las FARC EP dejarán las armas en el ejercicio de la política. Del otro lado, solo tengo una postura escéptica, incrédula, aunque creo que hay sectores militares proclives a cambiar. Desde el establecimiento no hay garantías de no repetición.

Estamos lejos de que esa formalidad comprometa realmente al ejecutivo y al amplio sector del establecimiento que en él  se expresa. La realidad santista de la paz es la Pax neoliberal como la  de Uribe y la de Vargas Lleras; no tienen diferencias sustanciales, solo matices.

Esas realidades del origen del derecho a la rebelión serán realmente transformadas en otro ejercicio de poder en todos los órganos de la formalidad democrática colombiana, incluyendo los cambios doctrinales en la fuerza pública. Hasta que el pueblo ejerza un poder consciente, será posible la paz. Cuando ese pueblo sea mayoría en el congreso, sea elegido en el ejecutivo y tenga sus expresiones en las cortes, se abrirá la posibilidad de la paz.

Ésta apuesta de poder consciente tiene posibilidades de avanzar en la ética de la paz,  en la Búsqueda de los Desaparecidos; la Comisión de Esclarecimiento de la Verdad, la Jurisdicción Especial de Paz, en la Comisión de Alto Nivel para el desmantelamiento del paramilitarismo, y los avances que se puedan lograr en los asuntos de reforma agraria integral y participación. Y creo, si el ELN empieza la mesa en desigualdad pero con posibilidad de ganar espacios.

Sin llamarnos a engaños, lo histórico será que el gobierno de Santos y los sucesores cumplan; mientras tanto, es imposible estar felices. Y esa felicidad se logra con un ejercicio ético del poder.

Las contradicciones reales para cimentar las bases de salida al alzamiento están más allá de los acuerdos. Santos ha jugado su juego. Y como se trata de videopolítica, hizo bien su presentación en La Habana. La realidad muestra que el asunto de la paz poco importa a los de a pie. Así se manifestó la mayoría de la ciudadanía en el marasmo de su costumbre. Esa ciudadanía el pasado jueves, que pasaba indiferente ante los mismos y unos pocos más que nos aglutinamos en distintas ciudades, es lo que genera miedo y hasta temor. Y qué no decir de las ciudades intermedias donde en absoluto paso nada.

De ahí que lo más grave no es el uribismo, ni  la astucia de Santos, para lograr en medio de unas políticas que van contra la paz social, paz ambiental y paz social, la firma. Tampoco, el haber apelado al imaginario del terror para referirse a la posibilidad de la ofensiva urbana de las FARC EP, abonando argumentos a su seguro sucesor, Vargas Lleras, que se refirió al Acuerdo, diciendo “ojalá cumplan”.

Lo que genera miedo es  la ausencia de una nueva dirigencia, fresca, creativa, que realice los cambios que requiere el país. Eso solo es posible en lo nuevo, y aún hoy la inmadurez de las izquierdas y los demócratas está a la orden del día. El país nacional es distante del país político, incluso de la izquierda armada que queda y de la desarmada a la que se sumarán las FARC EP.

 Santos llevará en los próximos dos años a iniciar una fase de implementación del Acuerdo con las FARC EP en algunos temas;  quizás empezando una mesa con el ELN, guerrilla que audazmente podría aportar a construir con prontitud un escenario político civil más allá del político militar.

Pero la deuda histórica, la que expresa Santos, Uribe, Vargas Lleras, los Gaviria, los Samper, los Betancour, no se resolverá en este proceso de La Habana, ni el que empiece en Quito. Solo un nuevo poder que rompa sectarismos, distancias con los de abajo, falsas lealtades partidistas, que reconozca lo distinto fuera de los partidos, podrá lograrlo. En esa dirección la dirigencia de las FARC sin armas podrán aportar, lo mismo que un el ELN, más a tono con la realidad, reconociendo lo dominante y las fisuras. Un tiempo sin vanguardismos ni basismos enfrentará el miedo de la indiferencia, a la inconciencia.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
