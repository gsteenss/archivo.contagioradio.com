Title: Iván Márquez y gobierno acompañarán la última marcha del frente Martín Caballero
Date: 2017-01-31 15:48
Category: Nacional, Paz
Tags: acuerdo farc y gobierno, Zonas Veredales Transitorias de Normalización
Slug: ivan-marquez-y-gobierno-acompanaran-la-ultima-marcha-del-frente-martin-caballero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ninos-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [31 Ene 2017] 

El Frente Martín Caballero será el último en llegar a su punto de transición, ubicado en la zona veredal de Los Pondores, en La Guajira. Se movilizarán a primera hora en la mañana en compañía de una delegación conformada por **Sergio Jaramillo, Alto Comisionado para la Paz, Iván Márquez, comandante de las FARC-EP  y el General Javier Flórez**, que no pudieron llegar hoy al lugar por inconvenientes logísticos.

Alirio Córodoba, Comandante del Frente Martín Caballero, expresó que pese a la situación de retrasos e incumplimientos por parte del gobierno el optimismo es total, **“hemos firmado un acuerdo que le da esperanzas al país y que nos da la posibilidad de transformar realmente las condiciones sociales y económicas” **Le puede interesar:["Arrancó la marcha de las FARC-EP hacia la construcción de paz"](https://archivo.contagioradio.com/arranco-la-marcha-de-las-farc-ep-hacia-la-construccion-de-paz/)

En las dinámicas del día a día de este frente hay actividades de pedagogías de paz intensas “**nuestras unidades están concentradas en un contacto permanente con las comunidades** comentando los acuerdos y hablando con el ciudadano del común sobre como con los acuerdos el pueblo gana, y en ese mensaje permanente nuestra gente se va nutriendo” señaló Córoba, de igual forma se están llevando a cabo censos internos sobre los intereses académicos que tienen los integrantes de este frente.

Frente a la situación de la zona veredal de Pondores, Córdoba afirmó que el **gobierno podría intentar solucionar a través de planes de contingencia** las problemáticas, esto debido a que entre las personas que habitarán este lugar se encuentran mujeres embarazadas y niños recién nacidos.

### **Propuestas de contingencia** 

Entre las medidas de contingencia que el gobierno podría usar para momentáneamente solucionar el estado de las zonas veredales y de acuerdo con Alirio Córdoba, se encuentran: tener **brigadas médicas que puedan atender los problemas de salud que puedan aparecer, apresurar la construcción de acueductos** y mientras tanto tener carro tanques y tener un transporte de emergencia en la zona.

Otra de las problemáticas que han surgido  es la arremetida de grupos paramilitares en lugares que antes ocupada la guerrilla de las FAERC-EP  que para Córdoba significa **“una preocupación porque no ha disminuido su presencia y pone en riesgo a la población debe acelerarse el proceso del desmonte de las estructuras paramilitares".** Le puede interesar: ["Este fin de mes estaremos en laz zonas veredales: Jesús Santrich"](https://archivo.contagioradio.com/35424/)

<iframe id="audio_16754859" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16754859_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
