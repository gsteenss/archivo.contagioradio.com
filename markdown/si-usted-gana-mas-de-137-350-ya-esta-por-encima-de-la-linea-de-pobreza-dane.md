Title: Si usted gana más de $137.350  ya está por encima de la línea de pobreza: DANE
Date: 2020-10-15 12:37
Author: AdminContagio
Category: Actualidad, Nacional
Tags: DANE, pobreza, Pobreza en Colombia, Pobreza Extrema
Slug: si-usted-gana-mas-de-137-350-ya-esta-por-encima-de-la-linea-de-pobreza-dane
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Pobreza-DANE.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Pobreza-extrema-DANE.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Incrementa-la-pobreza-en-Colombia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El más reciente [informe](https://www.dane.gov.co/files/investigaciones/condiciones_vida/pobreza/2019/Presentacion-pobreza-monetaria_2019.pdf) del Departamento Administrativo Nacional de Estadística -[DANE](https://www.dane.gov.co/)- reveló que **la pobreza en Colombia aumentó en un punto porcentual (1%) en el último año, al pasar de 34,7% en 2018 a 35,7% en 2019; lo cual significa que cerca de 17 millones y medio de personas en el país cerraron en condiciones de pobreza el año pasado.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":91524,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Pobreza-DANE-1024x576.jpg){.wp-image-91524}  

<figcaption>
Fuente: DANE

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El informe reveló a su vez, que **el crecimiento de la pobreza extrema, fue aún superior, alcanzando un incremento de 1,4% respecto a la anterior medición, pasando de un 8,2% en 2018 a un 9,6% en 2019.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":91525,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Pobreza-extrema-DANE-1024x590.jpg){.wp-image-91525}  

<figcaption>
Fuente: DANE

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Según el DANE, ambos incrementos son «*estadísticamente significativos*» y se traducen en que **661.890 personas entraron a una condición de pobreza, y otras 728.955 se ubicaron debajo de la línea de pobreza extrema.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Juan Daniel Oviedo, director del DANE, el **aumento del desempleo** en el país el año pasado, fue una de las principales razones para que más personas se ubicaran por debajo de las líneas de pobreza y pobreza extrema; esto especialmente en las zonas rurales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe anotar que este informe documenta los indicadores del año 2019, es decir no contempla los efectos altamente nocivos que ha tenido la pandemia frente al empleo y la economía; por lo que, una proyección con las cifras de desempleo que se presentaron este año, las cuales en algunos momentos superaron el 20% —según las propias cifras del DANE— llevaría a pensar que más personas ingresarán a las líneas de pobreza y marginalidad en la próxima medición. (Le puede interesar: [La desigualdad económica es nuestra pandemia](https://archivo.contagioradio.com/la-desigualdad-economica-es-nuestra-pandemia/)**)**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1316114484281389059","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1316114484281389059

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1316352491882270725","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1316352491882270725

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### ¿Cuál es el ingreso que delimita las líneas de pobreza y pobreza extrema?

<!-- /wp:heading -->

<!-- wp:paragraph -->

El panorama se muestra aún más preocupante cuando aunado al incremento en las cifras, se entra a considerar cuáles son los estimados que maneja el DANE para definir las líneas de pobreza y pobreza extrema.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El DANE reveló, que una persona cuyos ingresos suman 137.350 pesos al mes, ya está por encima de la línea de pobreza extrema**, y que para el caso de un hogar conformado por cuatro personas, \$549.400 pesos mensuales bastan para superar ese margen.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte, el DANE considera que no son pobres aquellas personas cuyos ingresos alcanzan los \$327.674 pesos cada mes,** esto es, menos de la mitad de un salario mínimo legal mensual vigente que en la actualidad es de \$877.802 pesos sin contar el auxilio de transporte. (Le puede interesar: [De la renta básica, a la garantía total de derechos](https://archivo.contagioradio.com/de-la-renta-basica-a-la-garantia-total-de-derechos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para algunos sectores, el hecho de que el Gobierno Nacional fije muchas de sus políticas sociales y económicas en este tipo de matrices estadísticas, lleva a que no haya una política pública efectiva y acorde con las necesidades de las personas, que permita paliar o contener los índices de pobreza en el país. (Lea también: [La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1316151733782020098","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1316151733782020098

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/C_CaballeroR/status/1316374871228788739","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/C\_CaballeroR/status/1316374871228788739

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
