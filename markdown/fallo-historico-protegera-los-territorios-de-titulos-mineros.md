Title: Fallo histórico protegerá los territorios de títulos mineros
Date: 2018-12-17 17:35
Author: AdminContagio
Category: Ambiente, Nacional
Tags: ANLA, Iván Cepeda, Ministerio de Minas, titulos mineros
Slug: fallo-historico-protegera-los-territorios-de-titulos-mineros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/minutode-dios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [17 Dic 2018] 

El Tribunal Administrativo de Cundinamarca suspendió la concesión de títulos mineros en Colombia, durante tres años. Fallo histórico que se logra luego de que el senador Iván Cepeda junto con organizaciones defensoras de derechos ambientales demandaran a **5 instituciones estatales por no realizar los procedimientos adecuados, ni con el conocimiento suficiente en la aprobación de estos títulos.**

"Esta es una lucha que venimos dando con las organizaciones que protegen el medio ambiente, el agua, la vida y el territorio, y que comenzó en el año 2013, cuando instauramos una acción popular que tenía que ver con **la re apertura de la ventanilla  minera**" afirmó Cepeda.

Esa ventanilla minera era un proceso en el que se otorgaban títulos para la exploración o explotación de hidrocarburos y minerales, que durante los años 2006 al 2010 tuvo su mayor auge, incrementando **la superficie de áreas para esta clase de actividades, que según Cepeda provocó daños y afectaciones  a la tierra irremediables**.

Con este fallo, se prohíbe durante 3 años que se concedan estos títulos hasta que no se produzca una delimitación rigurosa, resultada de estudios científicos, de zonas estratégicas en materia ecológica, como lo son los páramos, los humedales, fuentes de agua y las reservas naturales.

### **Las instituciones estatales deben hacer su tarea** 

La decisión del alto Tribunal le ordena al Ministerio de Minas y Energía, la Agencia de Minas, el Ministerio de Ambiente y Desarrollo Sostenible, el Ministerio del Interior y la Agencia Nacional de Licencias Ambientales que **examinen y revisen, de una manera rigurosa, los procedimientos que han utilizado para entregar las licencias** y de esa forma no provocar daños irremediables a la tierra.

Además crea un comité, integrado por quienes instauraron la acción popular, los Ministerios y la Procuraduría General de la Nación, que tendrá la labor de verificar el cumplimiento de lo fallado por el Tribunal Superior. (Le puede interesar[: "En tres departamentos de Colombia empezará el Fracking en 2019"](https://archivo.contagioradio.com/iniciaria-fracking-2019/))

"Este fallo detiene temporalmente la explotación inmisericorde de los recursos estratégicos para el país, y puede ser utilizado como una jurisprudencia que podría poner en tela de juicio las concesiones que se hicieron en el pasado, porque el Tribunal ha reconocido que se han violado los derechos colectivos al medio ambiente sano, al equilibrio ecológico y al patrimonio público" explicó Cepeda.

<iframe id="audio_30855542" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30855542_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
