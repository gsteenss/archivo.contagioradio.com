Title: ¿Cuáles son los argumentos de los abogados para que los 13 jóvenes no vayan a la cárcel?
Date: 2015-07-28 21:36
Category: DDHH, Otra Mirada
Tags: 13, audiencia, Congreso, falsos, Fiscalía, inocentes, judiciales, paz, positivos, pueblos
Slug: cuales-son-los-argumentos-de-los-abogados-para-que-los-13-jovenes-no-vayan-a-la-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/los-13-jovenes-detenidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [28 jul 2015]

La audiencia citada para las 9 de la mañana por Irma Francisca Cifuentes Prieto, juez 72 Penal Municipal de Control de Garantías, para definir la situación de medida de aseguramiento de las y los 13 jóvenes detenidos el pasado 8 de julio en Bogotá, inició solo hasta las 10:45 de la mañana. La juez se tomó cerca de una hora para hacer una síntesis de los argumentos que esgrimieron tanto Fiscalía como Procuraduría para imputar cargos contra las 13 personas, y posteriormente, en media hora resumió los argumentos de la defensa para rebatir esa solicitud.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Audiencia-1.mp3"\]\[/audio\]

##### [Juez 72 Penal Mpal. Irma Francisca Cifuentes y Abogado Ricardo Gaviria] 

A las 12:18 del medio día, la juez anunció su decisión, según la cual los delitos imputados a los jóvenes son suficientemente graves, y que por lo tanto, "el juzgado 72 penal municipal con función de control de garantías de esta ciudad resuelve imponer medida de aseguramiento privativa de la libertad en establecimiento carcelario en el lugar que determine el INPEC" en contra de los procesados. La bancada de la defensa presentó recurso de apelación, y uno de los abogados presentó recurso de reposición en subsidio de apelación.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Audiencia-2.mp3"\]\[/audio\]

##### [Juez 72 Penal Mpal. Irma Francisca Cifuentes y bancada de abogados de la defensa] 

Según argumentó parte de la bancada de la defensa durante la presentación del recurso de reposición y de apelación, la decisión tomada por la juez se basó única y exclusivamente en la exposición de la Fiscalía, ignorando las contradicciones presentadas por la defensa, lo que consideran una violación al derecho a la defensa.

Añadieron que la justificación de la juez según la cual la excepción de la medida de aseguramiento se presenta en tanto los jóvenes procesados son "personas que optan por no seguir un patrón que garantice el bienestar de la sociedad, porque salen de la convivencia normal que todos queremos" es absurda. "Pensar distinto no es un indicativo ni criterio para imponer medidas de aseguramiento: Siquiera hay individualidad, personas y pensamientos diferentes", aseveró el abogado Ricardo Gaviria.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Audiencia-3.mp3"\]\[/audio\]

##### [Abogado Ricardo Gaviria] 

Por su parte, el abogado David Uribe en entrevista con Contagio Radio, cuestionó “cuándo se habla de peligro para la comunidad ¿de qué comunidad se está hablando? si es la comunidad movilizada que está exigiendo la libertad; o la comunidad internacional, que está pidiendo garantías judiciales; y la comunidad académica que ha respaldado el trabajo político y académico de algunas de las personas detenidas… entonces la gran pregunta es ¿qué peligro para la sociedad es el que se está argumentando?”.

La defensa argumenta que la medida de aseguramiento es drástica y enormemente desproporcionada, teniendo en cuenta que la mayoría de los jóvenes están siendo procesados por una protesta social, de la cual a demás no se tienen argumentos sólidos, pues todas las pruebas son indirectas; y por el contrario, se cuenta con poco material probatorio.

Uno de los argumentos que esgrime con más fuerza la bancada de la defensa es, por lo tanto, que la juez no individualizó el proceso de cada uno de los 13 jóvenes, ignorando los argumentos de las y los abogados, y desestimando las pruebas presentadas. Aseguran que la Fiscalía ha dudado en los cargos que debían imputarles, y que tal como se reconoció durante la audiencia, a 10 de los jóvenes ni siquiera se les relaciona con el delito de "rebelión".

"La protesta social no genera temor y zozobra", afirmó la abogada Gloria Silva durante la audiencia de imputación de cargos, y le recordó a la juez que el presidente de la república y el General Palomino habían hecho juicios anticipados a los procesados ante los medios de comunicación, y que eso constituye presión sobre la juez y el proceso que se adelanta.

La audiencia en que la bancada de la defensa continuará exponiendo sus recursos de apelación se reanudará este miércoles 29 de julio a las 8 de la mañana en el complejo judicial de Paloqueao.
