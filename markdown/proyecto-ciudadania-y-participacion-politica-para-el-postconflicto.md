Title: Proyecto Ciudadanía y Participación Política para el Postconflicto
Date: 2014-12-17 16:34
Author: CtgAdm
Category: Hablemos alguito
Slug: proyecto-ciudadania-y-participacion-politica-para-el-postconflicto
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fs8rTf06.mp3)  
Ciudadanía y Participación política en el postconflicto, es un proyecto que hace parte del IDPAC (Instituto distrital para la participación y acción comunal), y de la Asociación social Comunidad y Vida. Entendiendo el postconflicto como la terminación del conflicto armado, pero no de los problemas que lo han causado, y la participación como la inclusión de la sociedad civil en la solución al propio conflicto.  
Generar procesos de cualificación, conformar una red ciudadana para la paz, buscando la participación cualificada de la gente del común, es decir de la gente que no esta dentro de quien está trabajando en la paz desde distintas organizaciones sociales...etc. son los objetivos del propio proyecto. Conversando con Andrés Torres, politólogo y coordinador general del proyecto, Carlos Arbeláez, coordinador de comunicación, nos explican cómo ha sido la implementación del proyecto, las fortalezas, debilidades y oportunidades, así como las distintas localidades donde han trabajado.  
Por último escuchamos a Juan Diego Pulido y Carolina Camacho del colectivo Canto y memoria. Ellos nos deleitan con música combativa de carácter andino, y de cómo la participación también va ligada a la cultura y el arte popular en Bogotá.
