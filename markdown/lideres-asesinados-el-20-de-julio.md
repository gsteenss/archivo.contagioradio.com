Title: Dos líderes sociales asesinados durante el 20 de julio
Date: 2018-07-23 17:12
Category: DDHH, Paz
Tags: Independencia de Colombia, lideres sociales asesinados, Restitución de tierras, Sustitución Voluntaria
Slug: lideres-asesinados-el-20-de-julio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/lideres-sociales-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Jul 2018] 

El viernes 20 de julio, mientras se conmemoraba la independencia de Colombia, fueron asesinados dos líderes sociales: José Osvaldo Taquez Taquez, presidente de una Junta de Acción Comunal (JAC) en Orito, Putumayo y Horacio Triana Parra, presidente de otra JAC en Otanche, Boyacá.

Según Timaco de la Cruz, líder social de Orito, Osvaldo Taquez era presidente de la Junta de Acción Comunal de la vereda Remolinos, Putumayo, y se había destacado por su liderazgo en la sustitución voluntaria de cultivos de uso ilícito. Precisamente, su asesinato tuvo lugar tras una reunión en la que se evaluaban los avances de esos procesos.

De la Cruz añadió que en el sector en el que vivía Taquez, había llegado una amenaza que intimidaba con desplazar a todas las personas que se habían sumado al proceso de sustitución y el líder asesinado le había expresado su temor por la seguridad de la gente.

De acuerdo con Timaco de la Cruz, en Orito hay cerca de 3 mil familias inscritas en el proceso de sustitución voluntaria, y todas ya cumplieron con la primera etapa que corresponde a erradicar los cultivos de coca. Sobre este proceso, el líder asegura que ya hubo verificación de las Naciones Unidas, pero falta el cumplimiento de parte del Gobierno Nacional.

Para el líder, esto demuestra que las personas han tenido la voluntad de sustituir los cultivos, pero el gobierno no tiene voluntad política para solucionar ese problema; y concluye al señalar que "el problema no es la coca, es el abandono de la gente de parte del Estado".

### **Asesinado defensor del Parque Regional Natural Serranía de las Quinchas**

Horacio Triana Parra también fue asesinado el viernes 20 de julio. Triana era presidente de la JAC de la vereda El Carmen, en el municipio de Otanche, Boyacá, y era reconocido en la región por su defensa de la Serranía de las Quinchas. (Le puede interesar:["En riesgo parque natural de la Serranía de Quinchas por derrame de petróleo"](https://archivo.contagioradio.com/en-riesgo-parque-natural-de-la-serrania-de-quinchas-por-derrame-de-petroleo/))

Las autoridades no se han referido sobre los posibles autores materiales o intelectuales  de la muerte de Triana o de Taquez; sin embargo como lo resalta Timaco de la Cruz, hay una sensación de miedo en diferentes sectores del país, pues de acuerdo con información brindada por el Centro Nacional de Consultoría (CNC) y la Consultoría para los derechos humanos y el desplazamiento (CODHES), en Colombia han sido asesinados 311 líderes sociales entre enero de 2016 y Junio del 2018.

De acuerdo con este estudio, de los 311 reportados, 45% eran líderes comunitarios o presidentes de JAC, 2% eran líderes de restitución de tierras y 5% eran líderes juveniles, de mujeres, ambientalistas o mineros. Mientras tanto, el Gobierno Nacional insiste en que los asesinatos contra líderes sociales no son sistematicos, ni obedecen a patrones que se pueden trazar en el tiempo. (Le puede interesar: ["Hay un plan para asesinar líderes en Chocó: Comisión de Justicia y Paz"](https://archivo.contagioradio.com/plan-para-asesinar-lideres-en-choco/))

\[caption id="attachment\_55006" align="aligncenter" width="300"\][![Centro Nacional de Consultoría](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-23-a-las-4.12.28-p.m.-300x655.png){.wp-image-55006 .size-medium width="300" height="655"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-23-a-las-4.12.28-p.m..png) Centro Nacional de Consultoría\[/caption\]

######  

<iframe id="audio_27197025" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27197025_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
