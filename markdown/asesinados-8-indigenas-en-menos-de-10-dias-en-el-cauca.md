Title: Asesinan 8 indígenas en menos de 10 días en Cauca y Chocó
Date: 2015-04-24 16:52
Author: CtgAdm
Category: DDHH, Nacional
Tags: Buenos Aires, Chocó, Conversacioines de paz en Colombia, Embera Katio, ESMAD, Frente Amplio por la PAz, Guillermo Pabil, Julio Maprieta, Luis Fernando Arias, Merfa Tequia Situa, ONIC, paramilitares y FFMM, paz, Suárez
Slug: asesinados-8-indigenas-en-menos-de-10-dias-en-el-cauca
Status: published

###### Foto: ACIN 

En el marco del 4to informe del Frente Amplio por la Paz, se denuncian ocho (8) asesinatos a indígenas por parte del **ESMAD, paramilitares y FFMM** en el departamentos de Cauca y Chocó.

**Luis Fernando Arias**, denunció que entre el 10 y el 18 de Abril se perpetuaron asesinatos a los indígenas **Guillermo Pavi, Julio Maprieta**, cinco integrantes de una familia y a la indígena Embera **Melba Tequia Situa**.

Según el consejero, el indígena Guillermo Pavi fue asesinado en la Finca Emperatriz por el Escuadrón Movil Anti Disturbios (ESMAD), quien según dictamen de medicina legal, resultó con 3 impactos de bala.

Según la denuncia entre 13 y 15 de abril paramilitares en asocio con narcotraficantes asesinaron a 5 integrantes de una familia en el **municipio de Suárez, resguardo de Cerro Tijeras.** Los paramilitares viajaban en camionetas de civiles en una zona fuertemente militarizada.

En el resguardo de Rio sucio, Caldas de Cañamon, fue asesinado **Julio Maprieta, presidente de la Asociación de Mineros Artesanales de Indígenas y Campesinos**; y denuncia el asesinato de Merfa Tequia Situa, indígena Embera del pueblo Embera Katio de Alto sinú, en el Alto Andaya, departamento del Chocó, por parte del Ejército Nacional, quien realizó un bombardeo en esa zona del país.

De igual manera, informa que en la mañana del 23 de Abril, se presentaron **bombardeos del Ejército en territorios de campesinos**, indígenas y afrodescendientes en los municipios Buenos Aires y Suarez, aumentando el terror y la zozobra en la población habitante de esa región.

El Frente Amplio por la Paz reitera el llamado al presidente y a los medios de comunicación a la coherencia, a parar la guerra y a terminar rotundamente los **"exterminios silenciosos contra las comunidades indígenas"**.

Video de la denuncia:

\[embed\]https://www.youtube.com/watch?v=DlzlMajwfQ0\[/embed\]  
 
