Title: "Ranking de universidades es para regular el mercado"
Date: 2015-07-16 16:41
Category: Educación, Nacional
Tags: Derecho a la educación, eduación, educación como negocio, gina parody, Ley 30 de 1992, mane, Mesa Amplia Nacional Estudiantil, Mineducación, rancking de universidades
Slug: ranking-de-universidades-es-para-regular-el-mercado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/DSC01517.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4880502_2_1.html?data=lZ2lkpqUdo6ZmKiak5eJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOms8bbxdDNssifxcqY19PNusbm1M7Rw8nJt4zZ1JDi0MaPqdTo08bhx8zNpYzYxpDax9fHpcXjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sara Abril, Mesa Amplia Nacional Estudiantil ] 

###### [16, Julio, 2015] 

**“La intensión del ranking de universidades de Ministerio de Educación es un tema de regulación del mercado**", señala Sara Abril, representante al Consejo Superior Universitario de la Universidad Nacional, refiriendose a la lista de mejores y peores instituciones de educación superior del país.

Se trata del Modelo de Indicadores del Desempeño de la Educación Superior, MIDE, que el Ministerio de Educación define como una nueva herramienta que permite saber cómo están 187 universidades en materia de calidad educativa, a partir de una evaluación de 18 indicadores, divididos en tres ejes: **estudiantes, docentes y entorno.**

“**Desde el Ministerio siempre se sacan rankings pero nunca se hace nada para mejorar la calidad de la educación,** la forma de ayudar a la universidad pública es endeudándola”, dice Abril, quien además añade que la mala educación es política de Estado del gobierno actual, situación que tiene a las universidades públicas con un **déficit de 16 billones de pesos.**

Para la vocera de los movimientos estudiantiles, aunque hay algunos indicadores que si servirían para medir la calidad de la educación, es claro que "algunos **indicadores están viciados** y solo sirven para favorecer algunas universidades privadas".

“**No es un buen resultado que la Universidad Nacional esté en el segundo puesto**, eso debería ser una preocupación para el gobierno, sin embargo lo único que se ha hecho es la aprobación de un cupo de endeudamiento por 200 mil millones de pesos con intereses supremamente costosos con los que no va a poder”, dice Sara Abril.

Con la Ley 30 de 1992, las universidades han entrado en déficit presupuestal, por ejemplo la **Universidad Nacional presenta un déficit de más de 93 mil millones de pesos,** por otro lado, la Universidad del Tolima, institución que presentó un puntaje bajo en el MIDE, el año pasado entró en una política de austeridad al igual que la Universidad Nacional.

**Apenas 8 universidades públicas de 20 se encuentran en el rancking de las mejores** instituciones de educación superior, lo que demuestra la falta de compromiso del gobierno con el mejoramiento de la calidad, ni se le ha inyectado el  presupuesto necesario, explica la vocera estudiantil.
