Title: Nudo en la garganta
Date: 2020-10-30 15:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, johan mendoza, opinion, politica
Slug: nudo-en-la-garganta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/audio-johan.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Portada-Colombia-acuerdo-paz-portada.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por:  Juan Carlos Pachón  {#foto-por-juan-carlos-pachón .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Decantada la libertad en la patria de los muertos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Floreciente el engaño que se va poniendo el traje a la medida de la realidad. Pienso en la novela de Vélez de Guevara y su refrán en desuso que dice *el que tiene el tejado de vidrio, no tire piedras al de su vecino, porque al parecer allí residen esos clandestinos deseos de justicia que se confunden con los deseos de venganza.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
¿Quién es hoy el propietario de la ideología si no aquellos que se presentan como neutrales? ¿Quiénes son los propietarios de la ideología, si no esos que dominan los medios comunicativos de la sociedad con su conducta detestable, con su lenguaje elegante pero promotor de la barbarie?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Por supuesto que sí señores del gobierno. En todo estoy en contra, respecto a su modo de pensar este mundo que nos ha tocado compartir; pero que dejen ya los cobardes, los estúpidos y los astutos de escandalizarse cuando clamo por la polarización, que no se amarguen si explico claro, que no me desprecien sin haberme escuchado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####   
Hay un desierto enorme de somnolencia intelectual y la universidad es ese pájaro que era muy hermoso pero que ahora vuela envuelto en llamas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por sus pasillos ya ni los pasos lúgubres. Petrificados están los símbolos de la revolución y la crítica; han sido intercambiados por los efectos de las prácticas tranquilizantes que solicita la pequeña burguesía que ya no cree tanto en Adam Smith como sí en el reciclaje de la cultura oriental adaptado a la necesidad de consumir una paz interior… pura agua fresca para el fascismo actual, que necesita de la estabilidad para que su pobre orden siga pobremente intacto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Dónde se hallan las ilusiones intelectuales? ¿Somos como el Lucien de Rubempré que nos entregó Balzac, con ilusiones perdidas y egos robustos? ¿Ese adulto de derecha que marchitó sus sueños de izquierda, no por el peso del mundo, sino por el peso de su cobardía?  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Nudos en la garganta habitan como inquilinos poco gratos, a medida que la realidad golpea y golpea a los colombianos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya suenan los tambores de las elecciones y mientras tanto, violencia y  
engaño se maquillan y se emperifollan, a sabiendas que son el binomio perfecto que contribuye a la caracterización de nuestro débil y manipulado temperamento. Pronto nos dirán que leamos las propuestas de los candidatos, para seguramente encontrar el punto donde dice “y entonces robaré los esfuerzos de tu trabajo”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nos dirán también que veamos los debates para definirnos… ¿definir qué? ¿Qué tan bueno fue el espectáculo? ¿Qué tan buena fue la blasfemia o el improperio? ¿Cuán enterrada está la política en el abismo de la mercadotecnia? ¿o tal vez quien repite más la frase vacía “lucharé contra la corrupción”?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¡Nudo en la garganta! Cuando personajes que citan a Pablo Escobar, que defienden a Uribe Vélez, que son fascistas al peor y más necio estilo de Salud Hernández, nos recuerdan desde sus micrófonos sobre la importancia de votar… nudo en la garganta por estas ganas de gritar **¡que viva la oclocracia!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esfuerzos en el mercadeo de eslóganes e ideas superfluas darán resultado cuando las masas iracundas que no conocen la historia del país, que sólo están atentas a la siguiente serie que arrojará la industria cultural, que se definen “a-políticos” (como si eso acaso existiera) que se tranquilizan con el reciclaje de la cultura oriental a conveniencia, sean finalmente las encargadas de, en el nombre de la libertad, definir mediante el voto a un tirano u otro.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Nudos en la garganta habitan cuando sentimos el olor de la sangre que invade a Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pareciera que no, que la situación no es tan grave, en la medida en que el noticiero no lo mencione y nos cambien las noticias trágicas por chistes sobre las mascotas, o pensamientos positivos que nos alejen de los malestares que produce esa realidad que resulta mejor no ver para  
no preocuparse…para no preocuparlos… para no incomodar la estabilidad de la barbarie.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nudo en la garganta, al reconocer que la devoción de algunos por este régimen que gobierna Colombia ha degenerado en fanatismo. Que la defensa de medidas económicas bastante racionales, termina degenerando en mezquindad. Que el capitalismo del ocio degenera en disipación del inconformismo. Que la sabiduría se transforma en orgullo. Y al final la modestia en el más profundo abatimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nudo en la garganta por saber si duermo o si deliro; si es verdad toda esta infamia que vive nuestra patria, o es tal vez un invento de la peor de nuestras noches. Nudo en la garganta porque muchos queremos ver luz al final del túnel, pero otros no quieren avanzar por él, porque la oscuridad se ha hecho segura, porque allí es donde reposan las frías cenizas de los autores de esos días de longeva juventud, en la que todo lo creían posible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este régimen que gobierna Colombia, yo le temo tanto como lo respeto, pero no podemos quienes lo rechazamos, seguir jugando a las soberanías, seguir profundizando el imperio de la identidad por sobre el de la posibilidad real de transformar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Nudo en la garganta que nos produce vomitar.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Del vómito a la sed como decía Octavio Paz; sed por arrojarnos a una lucha prolongada contra la barbarie que hoy gobierna esta patria. Nudo en la garganta por tratar de gritarle a toda la fauna y flora de la izquierda colombiana, que no solo ame y no solo odie una causa política, sino que la comprenda.  
Que no hay fórmulas por más estudios que tengamos, y que quizás sea necesario potenciar el acercamiento a la casualidad electiva que narraba André Breton, allí, en ese rincón del agua callada de la realidad, donde el deseo y el azar se combinan y nos permiten dejar de pensarlo tanto y actuar, y nos permiten dejar de sentirlo solamente y pensar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Nudo en la garganta que solo cura una noche solitaria con terrones de letras para el alma en espera del alba.

<!-- /wp:paragraph -->

<!-- wp:audio {"id":92118} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/audio-johan.mp3">
</audio>
  

<figcaption>
[Nota completa por Johan Mendoza](http://scienti.colciencias.gov.co:8081/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000088261)

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
