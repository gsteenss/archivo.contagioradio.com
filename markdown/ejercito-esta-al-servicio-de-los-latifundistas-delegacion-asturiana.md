Title: "Ejército está al servicio de los latifundistas": Delegación Asturiana en Colombia
Date: 2018-03-09 13:48
Category: DDHH, Nacional
Tags: acuerdo de paz, comunidades, Delegación Asturiana, ejercito, paramilitares, violación a los derechos humanos
Slug: ejercito-esta-al-servicio-de-los-latifundistas-delegacion-asturiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/delegación-asturiana-e1520620580776.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Marz 2018] 

La delegación Asturiana se encuentra en Colombia realizando una misión de verificación en diferentes lugares de Colombia como en Buenaventura. Recalcaron que **la paz aún no ha llegado a las comunidades** más apartadas y que debe hacerse un esfuerzo para que se retrate internacionalmente la realidad que se vive en el país.

De acuerdo con Javier Orozco Peñaranda, integrante de la Delegación Asturiana, “hay que resaltar la **capacidad de organización de las comunidades** para hacer resistencia por la defensa de la vida y el territorio”. Dijo que, a pesar del terror, hay un gran nivel organizativo de sindicatos, comunidades afros e indígenas.

### **Cuestionaron el accionar de las Fuerzas Militares** 

Sin embargo, manifestó que la delegación encontró que la Fuerza Pública en las zonas de Urabá, “**no acata las órdenes de los jueces** que les ordenan acompañar la restitución de predios que fueron despojados con violencia”. Enfatizó en que “parece que el ejército está al servicio de los latifundistas, los ganaderos, los palmeros y los bananeros”.

Además, cuestionó las acciones del Ejército en la medida en que **“no cuida a sus ciudadanos** y no tiene mando civil”. Por esto, entiende la delegación que “quien manda es el capital agroindustrial y un ejército de ocupación”. (Le puede interesar:["Territorios continúan sin saber que es la paz: Misión internacional en Colombia"](https://archivo.contagioradio.com/informe_mision_internacional_acuerdos_paz/))

### **En Yopal el poder civil está reemplazado por el poder del complejo industrial** 

Una problemática adicional tiene que ver con que en algunos territorios como Yopal hay un **poderío del complejo industrial minero** y “la movilización tiene un tratamiento militar sin opciones de diálogo entre el Gobierno Nacional y las comunidades movilizadas”.

"Somos testigos de que en el corregimiento el Morro en Yopal, había un enorme despliegue policial **cuidando a Equión, la antigua Pacific Rubiales** mientras que los líderes a escondidas se reunían con nosotros sin protección alguna”, dijo. Asimismo, hizo alusión a los hechos que ocurrieron en San José de Apartadó donde “los paramilitares ingresaron echando tiros y amenazaron a los dirigentes que estaban durmiendo”.

### **“No es verdad que hay paz en los territorios”** 

Teniendo en cuenta lo anterior, la delegación Asturiana indicó que la paz no ha llegado a los territorios y que “el Gobierno colombiano **ha vendido un discurso en Europa** donde dice que, después de la desmovilización de las FARC, hay un estado de normalidad”. Sin embargo, ellos han encontrado que hay un incremento de los asesinatos selectivos, amenazas de muerte y despojo territorial, como se evidencia en Buenaventura.

Ante esto, Orozco manifestó que hay un reto para contrarrestar la propaganda oficial “que **lanzó algarabías** de que con la desmovilización de las FARC llegaba la paz y lo que llegó fue el copamiento territorial por parte de los paramilitares y nuevos factores de riesgo”. Por esto, propuso que debe haber una organización en el terreno para reconstruir el tejido social organizativo “roto por la guerra”. (Le puede interesar:["Campesinos e indígenas están en riesgo inminente: Misión Internacional Vía Campesina"](https://archivo.contagioradio.com/campesino-e-indigenas-estan-en-riesgo-permanente/))

### **Debe haber presión a nivel internacional** 

La delegación ha resaltado que debe haber una presión internacional “para que personas como **Álvaro Uribe Vélez** respondan por los crímenes de lesa humanidad que se cometieron durante su mandato”. Afirmó que el Estado debe cambiar la doctrina del enemigo interno de la Fuerza Pública “porque siguen actuando sin respetar los derechos humanos”.

Finalmente enfatizó en que debe haber un diálogo nacional que “cuestione y le ponga freno a la locomotora minero energética porque **ese es el factor de desplazamiento masivo** en la etapa del llamado posacuerdo”. Y concluyó que para Europa “es inexplicable que se venda como progreso el despojo territorial”, a la vez que enfatizó que hay empresas europeas que tienen relación con dicha práctica criminal.

<iframe id="audio_24315991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24315991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
