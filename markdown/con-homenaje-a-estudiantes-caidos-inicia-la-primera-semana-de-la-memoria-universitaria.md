Title: Con homenaje a estudiantes caídos inicia la primera Semana de la Memoria Universitaria
Date: 2019-05-09 15:22
Author: CtgAdm
Category: Comunidad, Memoria
Tags: 16 de mayo, estudiantes, memoria
Slug: con-homenaje-a-estudiantes-caidos-inicia-la-primera-semana-de-la-memoria-universitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/59393188_1538145609655199_5104073808113827840_n.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/16-de-mayo-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ACEU] 

Con un homenaje al estudiante **Jesús "Chucho" León Patiño**, asesinado en 1984, este jueves 9 de mayo inicia la **Primera Semana de la Memoria Universitaria**, un espacio que en esta oportunidad busca reivindicar y conmemorar **35 años de una serie de sucesos violentos contra estudiantes, profesores y trabajadores universitarios**, particularmente los ocurridos en 1984.

[Rodrigo Torrejano, miembro del proyecto **Archivos del Buho**, cuenta que desde hace un poco más de dos años vienen realizando un ejercicio de registro, sistematización, acopio y divulgación de documentos asociados a la historia de la memoria del Movimiento estudiantil, ligado a la **sistematicidad en la violación de sus derechos humanos.** En el marco de esa investigación identificaron una serie de hechos emblemáticos para la historia de la universidad Nacional entre esos los sucedidos el **9 y el 16 de mayo de 1984,**]

[Considerando que estos episodios de violencia cambiaron la historia de la Universidad, el colectivo encontró la necesidad de **reivindicarlos y realizar un espacio de encuentro, de conmemoración y de solicitud de esclarecimiento por verdad, justicia y memoria.** Una de las finalidades es que los estudiantes, particularmente de nuevas generaciones, puedan a**propiarse de la historia, contando lo que sucedió por encima de los relatos que circulan informalmente,** así como de los lugares por los que a diario transitan.]

**Chucho León, el rostro de la primera conmemoración**

Rodrigo recuerda que Jesús [Chucho León Patiño, nacido en el departamento de Nariño, **era estudiante de 5to semestre de odontología de la UN en Bogotá, y era líder de Cooperación Estudiantil,** y lidero la retoma de las residencias universitarias y participaba de las gestiones del comedor universitario.  Cuando se dirigía a su ciudad de origen **fue desaparecido, torturado, asesinado en la ciudad de Cali el 9 de mayo,** y su cuerpo finalmente fue abandonado en un terreno aledaño a la Universidad del Valle. Como homenaje, los estudiantes convocaron a una **jornada de repudio el 16 de mayo,  espacio que fue violentado por una actuación desmedida de la fuerza pública** que terminó con el cierre por un año del campus.]

**Cuatro estaciones de memoria**

[Este jueves en 9 de mayo, las actividades inician con un recorrido por **4 estaciones** establecidas dentro del campus universitario, iniciando a las 4:30 de la tarde en la **estación Verdad**, frente a la placa de Hugo López en la entrada a la facultad de Diseño, en memoria del estudiante muerto en una confrontación con la policía a inicios de los años 80. **La segunda estación, Justicia** frente a la escultura Amerika, ubicada en el jardín Freud, se homenajeará a los personajes que la elaboraron y lo que implicó su construcción.]

[En la **tercera estación- memoria,** se realizará un acto en la placa a Patricio Silva, estudiante asesinado en 1982 por parte del ejército frente al edificio de Física, estadística y matemáticas (FEM); y el **cierre** en un acto ecuménico en la Plaza Ché Guevara por los Chucho y todos los estudiantes víctimas del conflicto en Colombia.]

 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/59393188_1538145609655199_5104073808113827840_n-300x300.jpg){.aligncenter .wp-image-66645 width="555" height="554"}

 

[El **13 de mayo se realizará la pega de un papelón artístico en la Plaza Ché,** y se hará un mural colectivo en Facultad de Ciencias Económicas. El 16 de mayo se hará un acto de **lanzamiento oficial de un corto documental sobre los sucesos del 84** realizado en colaboración con la Cinemateca Distrital; evento al que han invitado a que la **Comisión para el Esclarecimiento de la Verdad** y a las directivas de la Universidad, buscando generar unos impactos y compromisos con la memoria de la universidad y el esclarecimiento de lo ocurrido en Bogotá y en todo el país.]

Además, se han logrado hacer algunas articulaciones con otras instituciones para la recolección y sistematización de archivos de memoria de estudiantes que han sido **asesinados en otras ciudades como en la UN de Medellín**, quienes también se sumarán con actividades de conmemoración este 16 de mayo.

<iframe id="audio_35613326" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35613326_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
