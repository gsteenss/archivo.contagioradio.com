Title: Comunidades de Orito Putumayo le ganan la pelea a proyecto petrolero
Date: 2017-01-07 13:54
Category: Ambiente, Nacional
Slug: comunidades-de-orito-putumayo-ganan-pelea-proyecto-petrolero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Enero 2017] 

La solicitud radicada el 2 de diciembre de 2013, por la petrolera Gran Tierra Energy, que pretendía exploración y explotación petrolera en Orito, Putumayo y que **afectaría gravemente la biodiversidad que habita en la zona, denominada por la petrolera como Área de Perforación Exploratoria Garza y a comunidades originarias,** fue negada por la Autoridad Nacional de Licencias Ambientales ANLA.

Según lo dispuesto en el auto 4152 de la ANLA, las coordenadas del área escogida para la exploración del subsuelo, **se cruzan con territorios legalmente titulados de los Resguardos Indígenas del Alto de Orito y Simorna.**

Con el proyecto, Gran Tierra Energy pretendía la adecuación y construcción de tres plataformas multipozo, denominadas **Garza 2, Garza 3 y Garza 4, que contarían con tres pozos cada una.** Además, buscaba la construcción de **cuatro helipuertos y un centro de producción**, ubicados todos en la vereda El Caldero, cercana al área de perforación exploratoria Garza.

Por otra parte, según cifras de Agencia Nacional de Hidrocarburos, entre 2013 y 2015 **fueron presentadas 11 tutelas por parte de comunidades indígenas y afrodescendientes, solicitando consultas previas e indemnizaciones** por los impactos ambientales y culturales generados por las empresas operadoras y de transporte de hidrocarburos.

Aunque la petrolera tiene presencia en los departamentos de La Guajira, Meta y Huila, las **comunidades del Putumayo son quienes más acciones legales en contra de la multinacional canadiense Gran Tierra Energy, han adelantado.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
