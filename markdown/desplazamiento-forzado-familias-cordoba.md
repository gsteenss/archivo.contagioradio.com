Title: Denuncian desplazamiento forzado de 160 familias en Córdoba
Date: 2019-03-23 16:02
Author: CtgAdm
Category: DDHH, Nacional
Tags: Armados, cordoba, Desplazamiento, San Jorge
Slug: desplazamiento-forzado-familias-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Desplazamiento_Apartado-e1553374753387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [23 Mar 2019] 

La Fundación Cordoberxia denunció que desde el pasado viernes se ha presentado el desplazamiento masivo de cerca de 160 familias de las veredas Santa Rosa y Flechas en el municipio de Libertador, Córdoba. De acuerdo a información de la Organización, hombres armados estarían ordenando el éxodo de las familias. (Le puede interesar:["5 grupos armados se disputan el control en el sur de C."](https://archivo.contagioradio.com/grupos-armados-disputan-cordoba/))

La Fundación ha hecho constantes denuncias sobre la grave situación en materia de derechos humanos que se presenta al sur de Córdoba, señalando el riesgo para la vida de líderes sociales y habitantes de la región del San Jorge, razón por la que han pedido al Gobierno Nacional, instituciones encargadas de velar por los derechos y la protección de los ciudadanos hacer presencia en la zona, evitando nuevos desplazamientos y amenazas.

Noticia en desarrollo.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
