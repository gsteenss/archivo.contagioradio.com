Title: CONPAZ participa del Foro: "Por la Democracia la Paz y la Unidad Popular" en el Putumayo
Date: 2015-02-24 23:14
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Comunicadores CONPAZ, Conpaz, Democracia y Unidad Popular" Putumayo, Encuentro "Paz, Putumayo, ZRC "La perla amzónica"
Slug: conpaz-participa-del-foro-por-la-democracia-la-paz-y-la-unidad-popular-en-el-putumayo
Status: published

###### Foto:Absolut-Colombia.com 

##### **Entrevista a [Sandra Lagos]:** 

<iframe src="http://www.ivoox.com/player_ek_4127958_2_1.html?data=lZafmZ6ZfI6ZmKiakpqJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2FX1srb1tfTb9Hj05DZw5C0pduZk6iYzsaPqMbh0Mjfw8jNpYztjNHOjbrSrcXVxZDd0dXZsMLmjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El fin de semana se realizó en el **Putumayo el encuentro "Por la Democracia, la Paz y la Unidad Popular", al que acudieron casi la totalidad de organizaciones sociales de la región**, sumando más de 100 organizaciones de defensa del campesinado, el territorio, las comunidades y los derechos humanos.

Con los **objetivos de valorar el proceso de paz y la misma en la región, la democracia, es decir la gobernabilidad y el buen gobierno y la unidad popular entre las organizaciones sociales.**

Las conclusiones extraídas son parte de la fuerte crítica al proceso de paz, por el **peligro que conlleva para éste , el recién presentado Plan Nacional de Desarrollo y la ley 133 o de terrenos baldíos, la megaminería, el extractivismo y las fumigaciones ilegales.**

Por otro lado la **corrupción de los gobernantes y la poca representación de estos sobre sus pobladores y la no defensa de sus intereses ante el gobierno central y las multinacionales**.

Entrevistamos a Sandra Lagos, comunicadora de la ZRC "La perla amazónica", que es parte de CONPAZ ( Comunidades construyendo Paz) , quién nos explica que en este foro se ha trabajado en **la creación de la participación de las comunidades y organizaciones sociales en la política mediante la unidad popular**.
