Title: Por lo menos US$100 mil millones tendrían colombianos en paraísos Fiscales
Date: 2016-04-05 14:55
Category: Otra Mirada
Tags: Panama, Paraisos fiscales
Slug: por-lo-menos-us100-mil-millones-tendrian-colombianos-en-paraisos-fiscales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Papeles-Panama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Movimiento Político resistencia 

###### [5 Abr 2016]

Los papeles de Panamá han sido una de las mayores filtraciones de información sobre el uso de los paraísos fiscales para ocultar grandes sumas de dinero en el mundo y se estima que **por parte de Colombia existirían cerca de 850 personas en la lista con una suma aproximada de cien mil millones de dólares,** según afirmaciones del ex director de la Dian Ricardo Ortega.

Los paraísos fiscales son territorios libres de impuestos y en donde los capitales que se encuentran tienen reserva bancaria, esto significa que ningún otro país puede investigar el origen de los fondos, la rentabilidad de los mismos y la aplicación del capital. Estas características permiten que los dineros que entran a los paraísos fiscales estén bajo la legislación de cada país y que aumente la evasión de impuestos.

De acuerdo con Jairo Bautista, miembro de la red por la Justicia Tributaria y docente universitario, los paraísos fiscales tienen formas muy complejas de esconder el capital y son muy difíciles de seguir, a su vez suelen usar diferentes métodos como firmas o **empresas de papel para ocultar el proceder del dinero, algunos provienen del narcotráfico, el tráfico de armas, la trata de personas o corrupción.**

En el caso particular de Colombia la publicación de esta información solo puede tomarse como sospecha hasta que se tenga la suficiente evidencia para demostrar evasión de impuestos y elusión tributaria. Hasta el momento en la lista se encuentran 850 colombianos, entre los que figuran políticos como **Luis Alfredo Ramos, senador del Centro Democrático y Roberto Hinestrosa, presidente del Consejo.**

Para Jairo Bautista “**en este país el poder judicial es un poder cooptado por los grandes poderes económicos,** se han sacado algunos nombres que sirven para ser la comidilla de los medios de información, pero se estima que los colombianos deben tener en Panamá sien mil millones de dólares, entonces estamos hablando de peces gordos, de personas que combinan de una manera muy astuta los negocios legales y formales con la ilegalidad en los paraísos fiscales, relaciones que impedirán que se lleve a cabo las investigaciones pertinentes por otro lado debe ser dudoso el hecho de que una figura pública deba ocultar los dineros que gana”.

Serán las autoridades colombianas las que decidan si tomarán el hecho de aparecer en la lista como evidencia de posible evasión de impuestos o lavado de activos.

<iframe src="http://co.ivoox.com/es/player_ej_11055850_2_1.html?data=kpadl5qceZGhhpywj5aYaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncbHj05DZ0ZDRqc%2Fj1JDCtYqWeJKkkZDay9GPscrgzdTbx9iPuMbixdeSpZiJhaXVz5DQ0dHTscPdwtPc1ZCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
