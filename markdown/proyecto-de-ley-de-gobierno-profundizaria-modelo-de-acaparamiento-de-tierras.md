Title: Proyecto de Ley de Gobierno acabaría con el Banco de Tierras para la Paz
Date: 2017-04-22 20:54
Category: Entrevistas, Paz
Tags: acuerdos de paz, Ley Zidres, reforma agraria
Slug: proyecto-de-ley-de-gobierno-profundizaria-modelo-de-acaparamiento-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Restitucion-Tierras-Uraba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de prensa IPC] 

###### [23 Abr 2017] 

Las organizaciones sociales y en defensa de los derechos humanos lanzaron una alarma frente al proyecto de ley impulsado por el gobierno, sobre las disposiciones de Ordenamiento social de la propiedad y tierras rurales, pasaría por alto los acuerdos de paz de La Habana, **acabarían con el Banco de Tierras para la Paz y profundizaría el modelo actual agrario y la concentración grandes cantidades** de tierras en manos de multinacionales, empresarios y latifundistas.

Además, las organizaciones señalan que este **acto legislativo no tuvo un proceso de participación ni consenso amplio con la intervención de las comunidades rurales**, que han sido las más afectadas por el conflicto armado y víctimas del despojo de tierras. Le puede interesar:["Ley ZIDRES para acaparadores de tierras afecta territorio colectivo"](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)

### **Entrega y democratización de Tierra**s 

El primer punto pactado en los Acuerdos de Paz, tenía que ver con la Reforma Rural Agraria que se comprometía con la entrega y democratización de la tierra **a favor de las familias campesinas que carecen de ellas o que no tienen la cantidad suficiente para cultivar.**

En este sentido el **proyecto de ley elimina la figura de la Unidad Agrícola Familiar**, a través de un cambio de concepto, completamente diferente al acordado en La Habana, que además la sustituye por el término beneficiario y permitiendo, de acuerdo con Jeniffer Mojica, integrante de la Comisión Colombiana de Juristas, que cualquier persona, empresa o industria pueda recibir tierras. Le puede interesar:["20 Empresas deberán devolver 53.821 hectáreas de tierras despojadas" ](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/)

De igual forma se habla de establecer unos mecanismos de producción que fortalezcan la economía campesina, la soberanía alimentaria y que conforme un modelo con enfoque territorial y de respuesta al desarrollo rural. De ser aprobado el proyecto de ley, Mojica señala que el **modelo profundizaría mucho más el acaparamiento de tierras y con ellos la producción.**

### **Banco de Tierras para la Paz** 

Otra de las propuestas que estaba dentro del Acuerdo de paz era la creación de un banco de tierras baldías, que se le otorgarían a campesinos víctimas del conflicto armado, sin embargo, los requerimientos que se debían cumplir para entrar en la adjudicación de estas tierras fueron transformados, **permitiendo que personas que no necesariamente estén en condición de vulnerabilidad obtengan tierras baldías.**

Mojica señala que “**no hay ninguna intención real de alimentar ese banco de tierras,** solo se limita a una cuenta de recursos especiales que se manejaran para la adquisición de tierras y unos subsidios de tierras integrales, que se implementó desde el 2007 y que ha fracasado”. Le puede interesar: ["Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz"](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

La abogada denuncia que el **proyecto de ley abandona por completo la intención de búsqueda de fuentes de tierras**, como la recuperación de baldíos que estén en manos indebidas, la sustracción de fuentes forestales y la expropiación de tierras.

Finalmente se crea la figura de **“título oneroso” que permitiría entregar tierras a particulares**, que tienen grandes patrimonios económicos o a las agroindustrias con grandes extensiones.

Mojica señaló que frente a este proyecto de ley las **organizaciones campesinas, afro e indígenas deberán volcarse a la defensa de los Acuerdos de paz de la Habana y exigir que se cumplan,** a través de los mecanismos jurídicos que sean necesarios y exigir las instancias amplias de participación que permitan recoger las propuestas que ya han salido desde el campesinado para la reforma agraria.

<iframe id="audio_18310260" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18310260_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
