Title: Francisco Ceballos, el ciudadano que defiende la Serranía de los Paraguas
Date: 2019-03-07 18:04
Author: ambiente y sociedad
Category: Ambiente, Entrevistas
Tags: biodiversidad, Chocó, conservación, La Serranía de los Paraguas, Valle del Cauca
Slug: francisco-ceballos-ciudadano-defiende-la-serrania-los-paraguas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Serraniagua] 

###### [07 Mar 2019] 

La **Serranía de los Paraguas**, ubicada entre el Valle del Cauca y el Chocó, es uno de los lugares de mayor riqueza de biodiversidad del planeta pero también uno de los más amenazados por la ganadería extensiva y el monocultivo. Zonas de la Serranía como el Cañon del Río Garrapata, antes cubiertas por una gran variedad de bosques, **hoy son predios de pastoreo que sufren de graves afectaciones sobre el suelo**.

Así lo afirma Francisco Ceballos, consultor en temas agropecuarios y de ordenamiento territorial, quien **en los últimos 22 años ha logrado la conservación de más de 175 hectáreas de bosque subandino** a través de su proyecto, **el Bongo Negro**. En esta bioreserva, ubicada en la cordillera norteoccidental del Valle del Cauca, la flora y fauna silvestre han regresado paulatinamente a este terreno, antes deteriorado por el pastoreo. (Le puede interesar: "[La deforestación, uno de los mayores riesgos para los animales en Colombi](https://archivo.contagioradio.com/la-deforestacion-uno-de-los-mayores-riesgos-para-los-animales-en-colombia/)a")

**En cambio de usar la resiembra para recuperar los bosques de esta zona, Ceballos dejó que estos ecosistemas se regenerara por si mismo, en total aislamiento**. En 2012, instaló cercas en los perímetros de la finca para impedir la intervención de los humanos y el ganado en este territorio. Esto fue seguido por unas siembres de especias precursores y después "la naturaleza hizo su trabajo", dijo el conservacionista. En solo siete años, logró rehabilitar la zona y crear tres corredores biológicos. (Le puede interesar:

Hoy, Ceballos espera que este modelo de conservación se pueda replicar en otros regiones del país. Junto a la organización ambiental Serraniagua, buscan la creación de nuevas zonas de reservas forestales desde la sociedad civil en áreas de la Serranía en el Valle del Cauca y el Chocó. Asimismo, adelanta trabajos con autoridades ambientales  en  tres municipios del Valle, para proteger a más de 40.000 hectáreas de bosque.

<iframe id="audio_33198809" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33198809_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
