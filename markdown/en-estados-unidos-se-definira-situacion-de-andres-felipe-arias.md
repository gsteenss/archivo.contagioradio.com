Title: Andrés Felipe Arias y el escándalo de corrupción de Agro Ingreso Seguro
Date: 2016-08-24 11:32
Category: Judicial, Nacional
Tags: agricultura, agro ingreso seguro, Andres Felipe Arias
Slug: en-estados-unidos-se-definira-situacion-de-andres-felipe-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Andres-Felipe-Arias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo ] 

###### [24 Ago 2016 ] 

Este miércoles la Embajada de Estados Unidos en Colombia notificó a la Corte Suprema de Justicia la captura del ex ministro de Agricultura Andrés Felipe Arias, condenado a 17 años de prisión por los delitos de **celebración de contratos sin cumplimiento de requisitos legales esenciales y peculado por apropiación en favor de terceros**, en el marco del proceso que cursa en su contra por las irregularidades que se detectaron en la asignación de recursos públicos del programa Agro Ingreso Seguro.

<div>

<div id="cke_pastebin">

La Fiscalía General de la Nación, aseguró que los contratos celebrados entre 2007 y 2009 por un monto total de \$288 mil millones, entre el Ministerio de Agricultura y el Instituto Interamericano de Cooperación Agrícola, fueron sólo una fachada pues los recursos públicos que se debían destinar a la asistencia técnica y científica, fueron asignados **sin contar con licitaciones abiertas, estudios previos y términos de referencia**, e incluyendo cláusulas exorbitantes.

De acuerdo con la investigación, la celebración de los contratos benefició directamente al ex ministro quien **aprovechó el programa Agro Ingreso Seguro como plataforma política** para su aspiración presidencial. Por otra parte, confirma que tanto el Instituto Interamericano de Cooperación Agrícola como particulares apropiaron de forma ilícita  no menos de \$43.608.131.299 del programa, a través del fraccionamiento de predios o de la aceptación de auxilios cuando no se cumplían los requisitos.

Según se conoce el ex ministro deberá presentarse en la tarde de este miércoles a una **audiencia en la que se definirá su situación jurídica**, pues tanto él como su familia habían logrado desde hace dos años un asilo temporal en los Estados Unidos y estaban a la espera de que le fuera otorgado un asilo político en este país; sin embargo la condena de 17 años y 4 meses de prisión está en firme así como su obligación de cancelar una multa de 50 mil salarios mínimos.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

</div>

</div>
