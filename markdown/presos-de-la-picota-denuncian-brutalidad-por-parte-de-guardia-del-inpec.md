Title: Presos de la Picota denuncian agresiones y torturas por parte del INPEC
Date: 2019-02-01 11:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: abitantes del municipio El Mango, CRI, Eron, INPEC, presos
Slug: presos-de-la-picota-denuncian-brutalidad-por-parte-de-guardia-del-inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Carcel_La_Picota_de_Bogotá_Cund_-_Colombia_3-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [31 Ene 2019]

Los reclusos del patio 12 del establecimiento Eron-Picota, en Bogotá, denunciaron el abuso de autoridad y el desmedido uso de la fuerza por parte del Comando de Reacción Inmediata del INPEC, el pasado 30 de enero, que dejó **3 personas heridas, dos de ellas con heridas de gravedad, de acuerdo con las versiones de los internos. **

En la denuncia, las personas señalan que el capitan Ramírez, fue el encargado del operativo de seguridad que se realizó a las 5:00am y que inició de forma agresiva, golpeando las celdas de los internos con **"el fin de presionar psicológicamente a las personas privadas de la libertad para salir de manera presurosa del patio, estando la mayoría en descanso"**.

Durante este procedimiento, los dragoneantes golpearon a José Geovanny Mendoza, quien tiene heridas de gravedad en su oreja derecha, su oblicuo, brazo y muñeca del lado derecho, Jairo Ferney Martinez, quien fue golpeado en el pecho y Paul Gutiérrez, que fue golpeado con un bolillo en el brazo derecho y luego roseado con gas pimienta, hecho que le provocó conjuntivas.

### **Atención médica en la cárcel** 

Los reclusos manifestaron que cuando solicitaron servicio médico para las personas heridas, puntualmente en el caso de José Mendoza, el médico del centro de reclusión fue negligente, debido a que el doctor Arturo Benicore, solamente autorizó un examen de rayos X prioritario, pero no atendió las demás heridas.

Posteriormente, a las 10:30 a.m. hubo una nueva valoración médica por parte de la doctora de turno, quien no quiso hacer valoración escrita, "pues indicaba que el paciente no presentaba lesiones y **que volviera el día viernes para decidir sobre el estado de salud**". (Le puede interesar: ["Prisioneros políticos denuncian violación de DD.HH desde directivos de la Picota"](https://archivo.contagioradio.com/prisioneros-politicos-denuncian-violacion-de-dd-hh-desde-directivas-picota/))

Por estos hechos, los reclusos denuncian que constantemente el CRI ha venido realizando abusos en diferentes centros penitenciarios del país, en los múltiples operativos realizados, los cuales, aunque rutinarios, "representan graves violaciones a los Derechos Humanos, pues su métodos contienen operaciones psicológicas, tortura, destrucción de ambientes propicios para la convivencia; lo que afecta moral, física y psicológicamente a los internos".

######  Reciba toda la información de Contagio Radio en [[su correo]
