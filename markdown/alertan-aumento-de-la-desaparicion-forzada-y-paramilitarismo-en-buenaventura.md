Title: Alertan aumento de desaparición forzada y paramilitarismo en Buenaventura
Date: 2015-03-05 21:33
Author: CtgAdm
Category: DDHH, Nacional
Tags: buenaventura, Casas de pique, Paramilitarismo, paz
Slug: alertan-aumento-de-la-desaparicion-forzada-y-paramilitarismo-en-buenaventura
Status: published

###### Foto: Santiago Saldarriaga 

<iframe src="http://www.ivoox.com/player_ek_4171576_2_1.html?data=lZakk5qbeo6ZmKiakpqJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnwtXO1M7HrYa3lIqvldOPqtDm28bRw5Ddb9HV08bay9HNuMLmytja0ZDFq9bYyt%2FO0JDHtsrnytiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [José Manuel Vivanco, Director de Human Right Watch para América Latina] 

La organización Human Right Wacth entregó esta semana un informe en el que documenta como graves violaciones a los Derechos Humanos persisten e incluso aumentan en Buenaventura, desde el último informe presentado en marzo del 2014.

Para el Director de HRW para las Américas, José Manuel Vivanco, si bien el gobierno ha enviado un importante número de agentes militares al puerto para contrarrestar esta realidad, e incluso se percibe una disminución en el número de homicidios registrados; Lo cierto es que en la práctica, el número de desapariciones forzadas ha aumentado, lo que podría dar pie a entender que las casas de pique y fosas comunes como formas de homicidio silencioso, se están generalizando. También le preocupa a la organización de Derechos Humanos el aumento en la vinculación de menores a organizaciones paramilitares, así como la existencia de fronteras invisibles de control territorial a partir de extorsiones, en los barrios de Buenaventura.

Según lo expuesto por Human Right Wacth, aún falta documentar las denuncias hechas por los habitantes del puerto, que señalan complicidad entre las fuerzas militares que hacer presencia en Buenaventura y el despliegue paramilitar.

Finalmente, la organización recomienda el fortalecimiento de las estancias judiciales que haga posible el acceso a la justicia por parte de todos las y los ciudadanos de Buenaventura, con castigos ejemplificantes que permitan a los habitantes aspirar a un futuro distinto.

\[embed\]https://www.youtube.com/watch?v=U-d65k7EXPg\[/embed\]  
 
