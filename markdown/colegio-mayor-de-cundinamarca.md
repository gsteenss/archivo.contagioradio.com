Title: Directivos del Colegio Mayor de Cundinamarca se niegan a permitir el ingreso de los 'Hijos de la Manigua'
Date: 2018-11-21 16:42
Author: AdminContagio
Category: Educación, Movilización
Tags: Colegio Mayor de Cundinamarca, educacion, ESMAD, Hijos de la Manigua, policia
Slug: colegio-mayor-de-cundinamarca
Status: published

###### [Foto: Contagio Radio] 

###### [21 Nov 2018] 

A su llegada a Bogotá, los **"Hijos de la Manigua"**, estudiantes que caminaron desde la Amazonía más de 500 kilómetros por la educación superior, quisieron pernoctar en las sedes de las universidades públicas que hay en la Capital; sin embargo, el movimiento estudiantil ha denunciado que **los directivos del Colegio Mayor de Cundinamarca no solo se oponen a esta iniciativa, sino que han pedido la presencia de la policía para evitar que los estudiantes ingresen al campus** de la Institución.

Según miembros de la comunidad estudiantil, desde el martes en la noche se buscó el permiso por parte de las directivas de la Universidad para permitir que los estudiantes pasaran la noche en el campus universitario, ubicado cerca al Museo Nacional; pero la respuesta por parte de la administración fue negativa, puesto que señalaron que no había garantías para su estadía, ni claridades sobre el tiempo que durarían allí.

Después de esta negativa, la administración dio orden al cuerpo de seguridad de la Universidad que no permitieran el ingreso a los estudiantes, y posteriormente **hubo presencia de la Policía al interior del recinto universitario** aduciendo que necesitaban un baño, hecho que resulta incoherente, porque frente a la Universidad se encuentra una estación de policía. (Le puede interesar: ["De Caquetá a Bogotá: 550 kilómetros caminando por la educación superior"](https://archivo.contagioradio.com/550-km-caminando-educacion-superior/))

Posteriormente, la orden de las directivas de la Institución dieron orden de desalojar la Universidad, de tal forma que profesores, administrativos y estudiantes que se encontraban en el Campus, tuvieron que salir del Recinto; y ahora, **miembros de la comunidad académica temen por la posible presencia del ESMAD en la zona**, evitando que los Hijos de la Manigua pernocten en el Colegio Mayor de Cundinamarca.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F254333638572566&amp;width=500" width="500" height="490" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
