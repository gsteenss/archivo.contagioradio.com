Title: Entre los Morales y los Garzón
Date: 2018-01-27 10:47
Category: Camilo, Opinion
Tags: Canal RCN, Claudia Morales, Jaime Garzon, memoria, Sin Olvido
Slug: entre-los-morales-y-los-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/jaime-garzon-y-claudia-morales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo / John Gonzales 

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 26 Ene 2017 

Está abriéndose en el debate de la memoria, el derecho al silencio y  la distorsión de las memorias en lo social. Dos hechos recientes la producción televisiva sobre la vida del asesinado Jaime Garzón, “Garzón Vive” y la periodista Claudia Morales revelando una violación sexual en su columna “Una Defensa del Silencio”.

En las redes sociales se debatió la memoria televisiva del Canal RCN del célebre Jaime Garzón..La postura de esa empresa contra el proceso de paz, y la ficción creada del personaje, distante, para algunos,  del verdadero Garzón, socavaron la credibilidad del producto televisivo.  En medio de las diferencias familiares entre hermanos, Alfredo Garzón, expresó un “Mea Culpa” . “Me veo enfrentado a una serie que parecen dos. Por un lado, me conmueve “volver a ver a mi hermano” pero me temo que esa resurrección derive en un sinfín de verdades a medias que desdibujen su vida y su muerte. Lo cual sería como un doble crimen. Un crimen que, a propósito, continúa en la impunidad y cuya investigación está abierta. Esta desazón solo me lleva a concluir que no debí haber firmado el consentimiento. RCN falsifica la verdad, creando un universo donde no necesitas los hechos, simplemente puedes mentir”

La reconstrucción de la violencia sexual vivida por Claudia Morales manifestando en su escrito su derecho al silencio, desató las más diversas reacciones entre la solidaridad y  la revictimización. Álvaro Uribe con desdén la trató de "la señora", en una nueva expresión de poder dominante, para defenderse de las especulaciones en las que se le atribuye ser el violador. Quizás su despectiva reacción obedeció a su identificación psicológica con el llamado en la columna como “Él”, un espejo.

Garzones y Morales, miles de historias guardadas en el corazón de personas, familias y comunidades, dichos de diversas maneras. En  esos silencios o palabras se expresan los perfiles del sicario o del determinador, el gran ser humano o un hombre de papel. En muchas ocasiones, las víctimas lo saben, pero lo callan hay que sobrevivir ante un Estado incapaz, cuando no, el mismo Estado es criminal. Y también, la honra de la memoria pública sin contexto o caricaturizada deshace en la vida privada las motivaciones profundas que llevaron a que la víctima fuera eliminada.

Las verdades se aseguran negadas, silenciadas y tergiversadas judicialmente cuando se contraponen a la expresión de un sistema de poder patriarcal, militar o político excluyente. Los testimonios son sometidos a las dinámicas de poder dominante expresado en la incredulidad, el enemigo interno, el machismo o la lealtad a la democracia de sangre y mentira que refleja el juez.

La mayoría de las veces el enunciador del testimonio se acerca al filo de la muerte, al desarraigo o experimentar la impunidad estructural con nuevas fórmulas, entre ellas, el desprestigio, porque la.justicia es de unos, esos poderosos

El derecho al silencio sobre las y los innombrables, también lejos de ser cobardía es una apuesta que pesa sobre los responsables generando la permanente zozobra y el temor a ser descubierto públicamente. Quizás, el temor a una pena mayor, que las que se profieren en cárceles que distantes de reeducar llevan a odiar y conspirar contra el denunciante.

Esa palabra sin nombrarles es demoledor para los responsables. Ellos saben, que muchos lo saben, y temen es a que sus nombres salgan a relucir. Si salen a la luz nace el ardid para silenciar, para distorsionar y revictimizar. Mientras los afectados ejercen el derecho al silencio y la palabra,  los perpetradores al acecho ejercen el derecho (el torcido) de silenciar o distorsionar. Son máquinas de muerte,  lo hacen con balas o con mentiras.

En la reconstrucción de las memorias y de los hechos victimizantes el silencio de identidades o  su revelación, cuando se supera el sentimiento de la venganza o se reconoce el perpetrador como alguien al que es posible ver a los ojos, la verdad emancipa. Asumir el miedo para reconocer al perpetrador y dejar en él la carga de su responsabilidad es libertad. En la enfermedad esta la posibilidad de la sanidad, del salto opresivo a la libertad, de la negación al reconocimiento,

En los campos de la memoria están  en debate las verdades vividas  en los núcleos íntimos y aquellas que se conocen del hombre  o la mujer pública, distorsionados o no. La pública es la faceta más conocida, las heroizadas,  en las que se traslucen valores por una forma de vivir.  Esas memorias se narran de muchas maneras en novelas, teatro, cine, audios. Sus contenidos pueden ser desprovistos de la condición humana, de la naturaleza de nuestra extirpe y solo referidas a la heroicidad.

A veces o siempre las circunstancias intimas llevan a motivaciones profundas concientes e inconcientes por las que se vive de una u otra manera. Sin embargo,  la vida privada  distorsionada puede pretender desdibujar la historia pública llevar a que los televidentes así lo experimenten o también en una construcción estética, aún de ficción, a comprender nuestra propia naturaleza y en ella las bellas apuestas por las que se vivió y por las que se le asesinó.

Desdibujar el fondo de la criminalidad  es negar. Así como la violencia sexual es una expresión de poder totalizante, la violencia que segó la vida del mejor humorista político de los últimos 30 años, es la expresión de un poder totalizante. Ambas violencias son la de poderosos sexuales" que imponen su fuerza o la de un Estado manejado por súper privilegiados que usan de la fuerza ante su incapacidad de reírse de sí mismos y de la vano del poder.

Aún hoy, en el debate de la memoria, en la asimetría de poderes es posible construir hipótesis que nos lleven más allá de hechos victimizantes, de las heridas y el sentimiento de venganza, comprensible pero limitado, a abordar el trasfondo. En el derecho al silencio o el derecho a la palabra ante la distorsión del silencio o de lo narrado es posible cimentar narrativas, comprensiones e interpretaciones que permitan ver nuestras verdades como en un espejo en deshecho en el que existen hilos que identifican un sistema de poder patriarcal, excluyente, discriminador, clasista, mercantil e indolente

Ese es el poder real ante el poder consciente. Un poder consciente que se refleja en la apuesta pública de esa violencia oculta, dicha entre el derecho al silencio y la palabra que revela intimidades que según se anuden dibujan a la víctima en su dignidad real, humana.

Asumir esa realidad del poder real en el poder consciente comprende aquellas facetas ocultas de Garzón como otra parte de su ser, sin invalidar su existencia. O comprender que más allá de los silencios de la periodista Morales ante las actuaciones militares de alguno de sus familiares, ella se encuentra al lado de los mismos afectados por el establecimiento.

La memoria restauradora y el derecho a la verdad es un espacio en que lo excluido, lo rechazado, lo negado o lo no percibido por los poderes dominantes encuentran el sentido y el lugar que le corresponden en la historia de un país de millones de victimizados.

Ojalá la Comisión de Esclarecimiento de la Verdad logre superar la mirada casuística y ausculte el alma de la violencia en las regiones para que la vida fluya en sintonía con la construcción de una democracia incluyente. Para que quizás reconocidos en las verdades en la Verdad,  se resuelva la crisis negada del silenciador del sistema.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
