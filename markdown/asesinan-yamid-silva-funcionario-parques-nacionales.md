Title: Asesinan a Yamid Silva, funcionario de Parques Nacionales en El Cocuy
Date: 2020-02-06 18:18
Author: CtgAdm
Category: Ambiente, DDHH
Slug: asesinan-yamid-silva-funcionario-parques-nacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Parques-Nacionales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Parques Nacionales

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/carolina-jarro-sobre-asesinato-funcionario-parques_md_47539905_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Carolina Jarro | Directora Parques Naturales

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

La mañana de este jueves 6 de febrero fue asesinado Yamid Alonso Silva, funcionario de [Parques Nacionales](http://www.parquesnacionales.gov.co/portal/es/parques-nacionales-naturales-lamenta-profundamente-y-rechaza-el-asesinato-de-yamid-alonso-silva-torres-guardaparque-del-parque-nacional-natural-el-cocuy/) en la vereda La Cueva, zona rural del municipio de Güicán, Boyacá. **Para esta misma fecha en 2019, había un total de 17 casos de amenazas grupales y directas contra trabajadores de esta entidad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La noticia fue confirmada por el alcalde de El Cocuy, Tomas Ruíz quien explicó que los sucesos ocurrieron en el sector del Valle de Lagunilla por el sendero para ir a la Sierra. Cabe resaltar que en el municipio de El Cocuy, el día martes fue asesinado el líder comunal Libardo Arziniegas. [(Le puede interesar: Temor en El Cocuy luego del asesinato de Libardo Arciniegas)](https://archivo.contagioradio.com/temor-en-el-cocuy-luego-del-asesinato-de-libardo-arciniegas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Yamid de 38 años se desempeñaba como operario contratista del Parque Nacional Natural El Cocuy, participando en labores de control al ecoturismo en el sector La Esperanza desde el 2018. [(Le puede interesar: Las pistas tras el asesinato de la ecóloga Nathalia Jiménez y su esposo Rodrigo Monsalve)](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ParquesColombia/status/1225530661596999697","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ParquesColombia/status/1225530661596999697

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a los peligros que afrontan los parques naturales de Colombia ante **amenazas como presencia paramilitar, deforestación, minería ilegal y megaproyectos, **otros funcionarios como Tito Ignacio Rodríguez, jefe del Parque Nacional Natural Sierra Nevada de Santa Marta y defensor de derechos ambientales, han tenido que salir del país para salvar su vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre los recientes hechos de violencia en el municipio de El Cocuy, líderes comunales han afirmado que desde la Defensoría del Pueblo se han lanzado alertas tempranas, sin embargo no han sido tenidas en cuenta. En cuanto a las amenazas a funcionarios de Parques Nacionales de las que se ha tenido conocimiento, se trata de grupos armados vinculados a la deforestación y ocupación del terreno. [(Lea también: Existen 17 casos de amenazas grupales y directas contra trabajadores de Parques Nacionales)](https://archivo.contagioradio.com/existen-17-casos-de-amenazas-grupales-y-directas-contra-trabajadores-de-parques-nacionales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
