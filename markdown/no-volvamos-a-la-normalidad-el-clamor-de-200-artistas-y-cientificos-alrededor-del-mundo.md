Title: "No volvamos a la normalidad", el clamor de 200 artistas y científicos alrededor del mundo
Date: 2020-05-15 10:53
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Calentamiento global, Consumismo, Covid 19, DD.HH
Slug: no-volvamos-a-la-normalidad-el-clamor-de-200-artistas-y-cientificos-alrededor-del-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: "No volvamos a la normalidad" WikiCommons

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una carta escrita por **el filósofo Aurélien Barrau y la actriz Juliette Binoche,** con el apoyo de numerosos artistas del séptimo arte y el mundo de la música, se ha lanzado un llamado a los líderes mundiales y a la sociedad civil para que a medida que se retomen las actividades y las personas vuelvan a la cotidianidad interrumpida por el Covid-19, esta sea una oportunidad de realizar un cambio en pro del planeta, la conservación del ambiente y la calidad de vida humana.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ad portas de vivir la mayor crisis financiera y sanitaria del siglo, el llamado realizado por el físico y filósofo francés es que esta crisis sirva para reconstruir la sociedad y para que generaciones venideras vivan en un mundo mejor.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La carta firmada por 200 artistas y científicos y publicada originalmente en el diario francés [Le Monde](https://twitter.com/lemondefr) advierte cómo el **consumismo y la productividad** han restado valorar a la existencia y lo que se entiende como ‘normalidad’ es un sinónimo de crisis ecológica de la que ahora es posible apartarse, por lo que conceptos como “nueva normalidad” puede ser aceptable, no deben implicar retroceder a las rutinas de antes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La misiva, firmada a su vez por una veintena de premios Nobel de Física y Química y el Premio Nobel de la Paz alerta sobre el problema estructural de la sociedad, y el problema ecológico que viene en curso desde hace décadas que podría llevar a una extinción de la humanidad, por lo que este cambio en la forma de vida forzado por el Covid 19 es la oportunidad de reformar los modelos de vida, la economía y  **nuestro mayor tesoro: la naturaleza.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La carta fue firmada por diferentes artistas incluidos los directores de cine **Alfonso Cuarón, Alejandro González Iñárritu, Pedro Almodóvar, y actores y actrices como Javier Bardem, Ralph Fiennes, Jeremy Irons, Jim Jarmusch, Penélope Cruz, Adam Driver, **Residente****, **Willem Dafoe, Cate Blanchett, Monica Bellucci, Sting**, **Joaquin Phoenix, Robert De Niro, Ricardo Darín, Barbra Streisand, Madonna, Marion Cotillard y Eva Green** quienes apoya la idea de superar el individualismo centrado en el consumo, el éxito individual y la desigualdad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "La pandemia del COVID-19 es una tragedia. Sin embargo, esta crisis nos está invitando a examinar lo que es esencial. Y lo que nosotros vemos es simple: los 'ajustes' no son suficientes. El problema es sistémico.
>
> La catástrofe ecológica que estamos viviendo es una meta-crisis: la extinción masiva de la vida en la Tierra ya no está en duda, y todas las señales apuntan a una amenaza existencial directa. A diferencia de una pandemia, por muy grave que sea, un colapso ecológico global tendrá consecuencias inconmensurables.
>
> Por ello pedimos solemnemente a los líderes, y a todos los que somos ciudadanos, que dejemos atrás la lógica insostenible que todavía prevalece y que llevemos a cabo una revisión profunda de nuestros objetivos, valores y economías. [(Le puede interesar: "La crisis no viene de la Tierra, sino del modelo de habitarla")](https://archivo.contagioradio.com/la-crisis-no-viene-de-la-tierra-sino-del-modelo-de-habitarla/)
>
> La búsqueda del consumismo y una obsesión con la productividad nos han llevado a negar el valor de la vida misma: la de las plantas, la de los animales, y la de un gran número de seres humanos. La polución, el cambio climático y la destrucción de las zonas naturales que aún quedan han llevado al mundo a un punto crítico.
>
> Por estas razones, junto con unas desigualdades sociales crecientes, creemos que es impensable que volvamos a la normalidad.
>
> La transformación radical que necesitamos, a todos los niveles, requiere valentía y coraje. No ocurrirá sin un compromiso masivo y determinado. Tenemos que actuar ya. Es tanto una cuestión de supervivencia como de dignidad y coherencia."

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Joaquin Phoenix, ganador del Oscar a Mejor Actor se refirió durante aquella ceremonia a los problemas que ha enfrentado la humanidad colectivamente: la desigualdad de género, el racismo, la ausencia de derechos para la población LGBTI e indígenas y de cómo **parte de la sociedad vive en una cosmovisión egocéntrica de la realidad, explotando los recursos del planeta,** por lo que invitó a ser compasivos con el ambiente y sus especies.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=UNAx3kf1-6Y","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=UNAx3kf1-6Y

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
