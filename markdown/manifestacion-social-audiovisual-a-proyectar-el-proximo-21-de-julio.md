Title: Manifestación Social Audiovisual  ¡A proyectar el próximo 21 de julio!
Date: 2020-07-13 16:25
Author: CtgAdm
Category: Cultura, Movilización
Tags: Manifestación Social Audiovisual, Movilización social
Slug: manifestacion-social-audiovisual-a-proyectar-el-proximo-21-de-julio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Masa_fly3.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Masa_fly2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Masa_fly1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Con la intención de visibilizar y ejercer el derecho fundamental a la manifestación social , nace **MA.S.A - Manifestación Social Audiovisual, una iniciativa popular desde distintos territorios por medio de procesos de creación y proyección audiovisual colectiva.** Dichos espacios de lucha social y expresión ciudadana buscan el continuo fortalecimiento de procesos de denuncia, resistencia digital, liderazgo social, memoria, paz territorial y vida en dignidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Julian Santana-Rodriguez, Coordinador del laboratorio ciudadano "**Little Brother and Sister are watching you",** taller enfocado a la exploración del lenguaje audiovisual a partir del registro de movilizaciones ciudadanas y luchas sociales expresó que en las manifestaciones que se dieron a final del 2019, "vimos surgir muchos movimientos artísticos y colectivos que se manifestaban, en diferentes modos y medios, uno de ellos fueron los medios audiovisuales que circulaban en las redes sociales", haciendo referencia la importancia de la manifestación social, y los medios audiovisuales como un artefacto de protesta.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Es un cultivo de creatividad, todos los colectivos están inventando todo el tiempo maneras de contar, es un espacio permanente que nace de la necesidad de crear
>
> <cite>Julian Santana-Rodriguez, Coordinador del laboratorio ciudadano "**Little Brother and Sister are watching you"**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

De igual manera, Julian menciona la importancia del registro que se realiza con los dispositivos móviles como evidencia, "el registro ciudadano nos llama mucho la atención, obviamente lo que pasó con Dilan Cruz... su asesinato, fue registrado por los medios ciudadanos de manera masiva y aun sigue la presión que busca la justicia por parte de su familia." [(Le puede interesar: En veinte años el ESMAD ha asesinado a 34 personas)](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F420089758948255%2F%3Ft%3D1903&amp;show_text=false&amp;width=734&amp;appId=1237871699583688&amp;height=413" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

**MA.S.A** es una práctica ciudadana abierta y colaborativa para la realización de acciones públicas de manifestación audiovisual que surge en el marco del laboratorio ciudadano **“**LITTLE BROTHER & SISTER ARE WATCHING YOU” y coordinada actualmente por VIDEOCRÁTICA, MOVIIE y PURPLE TRAIN ARTS.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El próximo 21 de Julio se realizará la segunda ***Manifestación Audiovisual (MA.S.A)***, que pretende circular mensajes de modo masivo y simultáneo en espacios públicos y fachadas, divulgando los contenidos de los informes publicados por el programa **SOMOS DEFENSORES**, en torno a las **violencias ejercidas en este año contra líderes sociales y otros defensores de Derechos Humanos en Colombia.** [(Lea también: Con tutelas, altos mandos militares quieren censurar mural sobre 'falsos positivos')](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Usted también puede participar de la próxima proyectaton, la convocatoria esta abierta para todo público, ciudadanxs, colectivx, medios independientes, organizaciones que tengan la posibilidad de proyectar hacia el espacio publico, o que estén dispuestos a crear y compartir contenidos visuales para la realización de una acción pública colaborativa el día martes 21 de julio entre las 7:00pm y 9:00pm.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El formulario de inscripción lo encuentra [aqui](https://forms.gle/o7SEjCWnffzKYsAR8).

<!-- /wp:paragraph -->

<!-- wp:image {"id":86684,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Masa_fly2-1024x551.png){.wp-image-86684}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Apoya: Contagio Radio

<!-- /wp:paragraph -->
