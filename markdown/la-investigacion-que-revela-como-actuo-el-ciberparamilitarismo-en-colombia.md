Title: La investigación que revela cómo actuó el Ciberparamilitarismo en Colombia
Date: 2019-10-23 17:56
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Castaño, paramilitares, Redes, Web
Slug: la-investigacion-que-revela-como-actuo-el-ciberparamilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Ciberparamilitar.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contratar Hacker  
] 

¿Usó el paramilitarismo la internet para difundir sus ideas? ¿Cómo siguen usando los grupos armados ilegales las páginas web y redes sociales? Son algunas de las preguntas que se plantean al escuchar el término ciberparamilitarismo, una categoría de análisis acuñada por la doctora en estudios políticos y relaciones internacionales **Lina Manrique**, en su libro "Ciberparamilitarismo en Colombia: Agencias y complicidades mediáticas".

### **¿Qué es el ciberparamilitarismo?** 

Según explicó Manrique, el ciberparamilitarismo se construyó como **una categoría de análisis que combina el fenómeno del paramilitarismo con la pregunta sobre si existió una estrategia en internet** por parte de los grupos paramilitares para divulgar su pensamiento, "como ocurre con otros actores ilegales en Colombia". El libro se centró en el uso de las páginas de internet por parte de las Autodefensas Unidas de Colombia  (AUC) en el periodo de 1997 a 2005.

Adicionalmente, el libro abordó casos específicos como el del Hacker Sepúlveda y un exmiembro de las AUC que tenía el alias de 'parabólico', tomando en cuenta la relevancia que tuvo el caso de Sepúlveda en el marco de un proceso electoral, así como de las negociaciones de paz que desarrollaba el Gobierno en La Habana, Cuba, con las FARC. (Le puede interesar: ["Aumenta el control paramilitar en Chocó"](https://archivo.contagioradio.com/aumenta-control-paramilitar-en-choco/))

La doctora en estudios políticos y relaciones internacionales explicó que **el libro se centró en la producción de contenido por parte de las AUC,** por lo que valdría la pena hacer otro estudio sobre cómo se comportan las audiencias respecto a estos contenidos. De igual forma, señaló que el libro propone recomendaciones respecto a la política pública que se debe emplear en términos de lo que pasa en internet, para controlar la producción de estos contenidos.

### **El paramilitarismo 1.0** 

La investigación centró su estudio en momentos en que el internet en Colombia apenas se estaba masificando, y las páginas de internet se limitaban a alojar contenidos dispuestos unidireccionalmente desde el creador hacía las audiencias. Según referenció Manrique, los hermanos Castaño reconocieron la importancia de hacer propaganda también en la web, y por eso **pusieron en funcionamiento páginas, crearon videojuegos y reclutaron a diseñadores e ingenieros que operaran estos sitios.**

Respecto a estas formas de propaganda, la investigadora señaló que los líderes del paramilitarismo tenían clara una estrategia mediática para dar a conocer sus ideas, que buscaron instalar de muchas formas "La cultura trasciende elementos hay mucha música con contenido ideológico, páginas web, libros e incluso, programas de televisión, a veces con mensajes directos y explícitos, y a veces con mensajes que deben ser filtrados". Manrique señaló que para hacer esas labores, se requirió de personas capacitadas y que recibieron un pago por sus servicios, o que por afinidad ideológica trabajaron en los contenidos.

### **¿Violencia 2.0, 3.0...?** 

En estudios sobre internet se habla de la web 2.0 como aquella en la que se comenzaron a crear sitios en los que se puede generar discusiones, fue así como afloraron los Blogs y espacios como las primeras redes sociales. En ese sentido, Manrique señaló que sería importante estudiar cómo se usan las redes sociales para continuar haciendo publicidad a ideas violatorias de los derechos humanos, como las que promovía el paramilitarismo. (Le puede interesar: ["Avance paramilitar en Puerto Guzmán, Putumayo deja tres personas asesinadas"](https://archivo.contagioradio.com/avance-paramilitar-en-puerto-guzman-putumayo-deja-tres-personas-asesinadas/))

Ahora, con la posibilidad de interacción que permiten plataformas como Facebook o Youtube a través de las transmisiones en vivo, así como otras plataformas de transmisión, miembros de cárteles mexicanos han mostrado en tiempo real las ejecuciones contra sus rivales. Esta situación pone de presente las cuestiones sobre cómo se controlarán este tipo de contenidos, y cómo los mismos afectan las percepciones y gustos de las audiencias que los reciben.

### **Complicidades mediáticas, lo que esperan las audiencias y lo que reciben**

Por último, la Manrique se refirió a lo que determinó como 'complicidades mediáticas', porque los medios masivos de comunicación están sujetos a intereses de grupos económicos que determinan lo que se denomina la *Agenda Setting, *esto significa que determinan lo que quieren informar, y por lo tanto, lo que quieren hacer visible o no. En ese sentido, afirmó que había complicidades cuando un medio no publica informaciones que tiene, o publica aquellas que ubica como su interés, en cuyo caso la pregunta tiene que ver con cuáles son los criterios de selección de la información, y qué eligen las audiencias para consumir.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43539631" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43539631_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
