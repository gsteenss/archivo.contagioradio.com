Title: Este 9 de abril Colombia marcha por la paz
Date: 2016-04-07 12:15
Category: Movilización
Tags: andes, Marcha 9 de Abril, MOVICE
Slug: este-9-de-abril-colombia-marcha-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/colombia_marcha-paz_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vistazo ] 

###### [7 Abril 2016] 

El próximo sábado 9 de abril, en Bogotá, Medellín, Cúcuta y Barranquilla se llevará a cabo la 'Marcha por la Paz', en memoria de las víctimas de crímenes de Estado y en **exigencia de garantías de no repetición**.

De acuerdo con Alfonso Castillo presidente de la Asociación Nacional de Desplazados ANDES  y miembro del Movimiento de Víctimas de Crímenes de Estado, esta movilización es una apuesta por la Paz y una conmemoración a las víctimas del conflicto armado en el país; "hemos integrado desde hace varios años una agenda de **respaldo al proceso de paz y en solidaridad y por los derechos de las víctimas**", afirma.

Según afirma Castillo, esta movilización "no se está viendo, ni se está pensando, ni como una respuesta, ni como una retaliación a la jornada de movilización de respaldo a la guerra" del pasado 2 de abril, en este jornada **diferentes comunidades mostrarán su apoyo al proceso de paz**, "nosotros anhelamos la paz y la movilización la construye".

Se espera que esta jornada nacional de movilización por la paz y en conmemoración a las víctimas sea multitudinaria y pese que Calí e Ibague aún no han confirmado la ruta que tomará la marcha, la **ciudadanía ha sido convocada para participar**.

En Bogotá desde las 10:00 am se planean concentraciones en la Plaza Eduardo Umaña Mendoza y en el Centro de Memoria Histórica, en este punto comunidades provenientes de distintas partes del país harán un ritual e iniciarán la **'Cadena Humana por la Vida, las Víctimas y la Paz'** que finalizará en la Plaza de Bolívar.

En Medellín se realizará una rueda de prensa en la sede de la Corporación Jurídica Libertad a las 8:00 am, y luego se hará un **plantón en el Museo Casa de la Memoria** sobre las 9:00am.

En Cúcuta se llevará a cabo la **'Gran vigilia por las víctimas y la Paz con justicia social'**, además se realizará la exposición de la cámara de la Memoria Histórica, el punto de encuentro será el parque Telecom a las 5:00pm.

Otros puntos de encuentro serán:

Cali: Parque de las Banderas.  
Barranquilla: Plaza de la Paz.  
Ocaña: Plazuela del Complejo Historico.  
Tunja: Plaza de Bolivar.  
Manizales: Parque Antonio Nariño.  
Armenia: Parque de los Fundadores.  
Yopal: Parque Resurgimiento.  
Pereira: Plaza Bolivar.  
Sincelejo: Plaza Olaya Herrera.  
Santa Marta: Parque Simón Bolivar.  
Villavicencio: Parque de Los Fundadores.  
Bucaramanga: Plazuela Luis Carlos Galan.  
Ibague: Parque Calarca

<iframe src="http://co.ivoox.com/es/player_ej_11082103_2_1.html?data=kpadmpeVdJShhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncabn1cqYm5DIqYzVw9fWzpCns83jzsfWw5DRpdPXycaY0tTWb83VjNXO3JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
