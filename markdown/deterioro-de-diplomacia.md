Title: Actitud del Gobierno de Colombia es un deterioro de la diplomacia
Date: 2019-02-06 16:56
Author: AdminContagio
Category: Nacional, Política
Tags: Diplomacia, Duque, ELN, Jesús Santrich, Venezuela
Slug: deterioro-de-diplomacia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Feb 2019] 

Lo ocurrido con el Protocolo pactado con el ELN, la posición del Gobierno colombiano sobre la crisis con Venezuela y el manejo dado al proceso de Santrich dejan ver que la diplomacia en el país está cambiando su manera de proceder, y **parece retomar los modelos realistas de hace tres siglos**; situación que podría afectar la confianza construida durante años con otras naciones.

En análisis del **experto en relaciones internacionales y profesor de la Universidad Jorge Tadeo Lozano, Mario Adolfo Forero**, el tema de los Protocolos establecidos con el ELN que Duque pidió a Cuba romper tras el atentado a la General Santander es una muestra de ese cambio, en el que se evidencia el manejo de estos compromisos internacionales como pactos de Gobiernos y no de Estados, **"actuando como en el siglo XVIII"**.

En ese caso, el Profesor cree que Duque intentó ganar acogida y popularidad gracias al atentado, tomando una decisión de carácter demagógica; no obstante, la decisión significó una disputa mediante comunicados de diferentes embajadas que rechazaban la violación del Protocolo, así como pudo resultar en el cierre a toda posibilidad de establecer procesos de diálogo y negociación en el futuro.

En segundo lugar, el reconocimiento a Guaidó como presidente interino de Venezuela, y el papel que ha jugado Duque, apoyando a la oposición del vecino país y reconociendo que tiene intereses económicos más allá de Cúcuta ha significado tomar partido, situación que podría traer problemas a futuro para Colombia. Sobre todo, como señala Forero, si se tiene en cuenta que **en Colombia se debería estar hablando de derechos humanos o migración, en lugar de una posible invasión**.

Por último, la situación ocurrida con el trámite de **la carta sobre Jesús Santrich, en la que se envía un documento oficial por correo corriente y no a través de la embajada**, aumenta la desconfianza sobre el manejo de las interacciones con otros países; situación a la que se suma el juicio extrajurídico y completamente político emitido por la vicepresidenta Marta Lucía Ramírez, que pedía a la JEP extraditar a Santrich, incluso sin tener pruebas de sus supuestos delitos.

En el caso Santrich, Forero concluye que puede tratarse de otro ataque a la JEP; sin embargo, uniendo todas las situaciones, el panorama resulta preocupante por **el tipo de relación que ha establecido Duque con otras naciones y lo que ello pueda significar para el país**. (Le puede interesar: ["El Fiscal buscará la forma para que Santrich permanezca en la cárcel: Gustavo Gallardo"](https://archivo.contagioradio.com/fiscal-santrich-carcel/))

### **Si la diplomacia de Colombia no es buena, la del mundo no es mejor** 

El experto en relaciones internacionales afirma que los lenguajes de los ministerios y actores exteriores se están volviendo prosaicos, muestra de ello son las comunicaciones sobre Venezuela que han emitido el Ministerio de Chile; Luis Almagro, secretario general de la OEA; y  las posiciones asumidas por España y una parte de la Unión Europea. Reacciones que han llevado a la radicalización de los lenguajes en torno al presidente Maduro, pese a que **la idea de la diplomacia es precisamente lo contrario, asumir posiciones de calma y conciliadoras**.

Forero añade que otro gran actor también interviene para que la diplomacia actué de forma contraria a sus propios principios: Estados Unidos. La contradicción radica en las propias tensiones que vive el gobierno de Trump, en el que se lo acusa de ser aliado del Presidente de Rusía; situación que lo obliga a retirar tropas de Afganistán, pero endurecer su discurso contra Venezuela, e incrementar la confrontación en la región.

La salida a este tipo de enfrentamientos ideológicos siempre ha sido la diplomacia, pero dadas las condiciones actuales, el profesor teme que esté pasando con Venezuela algo similar a lo ocurrido con Cuba, cuando se cerraron los canales diplomáticos y la isla fue sometida a un bloqueo económico, mientras el mundo se balanceó al borde de la guerra. (Le puede interesar: ["Duque le está dando la espalda a las víctimas"](https://archivo.contagioradio.com/duque-espalda-victimas/))

<iframe id="audio_32335849" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32335849_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
