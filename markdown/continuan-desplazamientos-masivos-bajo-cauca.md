Title: Continúan desplazamientos masivos en Bajo Cauca Antioqueño
Date: 2019-04-18 16:48
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Antioquia, Chocó, desplazamientos, Ituango
Slug: continuan-desplazamientos-masivos-bajo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Desplazamientos-masivos-en-Ituango-Antioquia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Colombia\_hist] 

Cerca de 50 personas campesinas que habitan el sector de Las Flechas, en Ituango (Antioquia) se vieron obligados a desplazarse hasta el casco urbano de este Municipio, aparentemente por intimidaciones contra su vida por parte de grupos armados ilegales que operan en la zona. Aunque las autoridades del lugar anunciaron que ya tomaron cartas en el asunto, es preocupante la situación del territorio porque esta semana se presentó otro desplazamiento masivo en Tarazá. (Le puede interesar: ["Fuerza de Tarea Aquiles no evitó desplazamiento de más de 120 familias en Bajo Cauca"](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/))

En el Bajo Cauca Antioqueño se vive una confrontación por el territorio que tiene enfrentados a las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y Los Caparrapos. En la zona hay cultivos de uso ilícito y es un corredor estratégico para llegar a la costa pacífica y atlántica, al Parque Nacional Natural Paramillo y al centro del país. (Le puede interesar: ["Se libra una guerra por tomar el control del Nudo de Paramillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

Por esta razón, las instituciones oficiales aún revisan las razones por las cuales se desplazaron cerca de 50 personas hacía el casco urbano de Ituango. Sin embargo, los problemas de violencia en la zona no son un secreto, en entrevistas con Contagio Radio, organizaciones sociales del sur de Córdoba y Antioquia advirtieron la guerra que se vive en la frontera entre ambos departamentos y las posibles afectaciones a la población civil entre las cuales resaltaron asesinatos de líderes, confinamiento por siembra de minas antipersona y desplazamientos masivos.

Las organizaciones también alertaron por los incumplimientos en el Programa Nacional Integral de Sustitución de Cultivos, la falta de instituciones civiles del Estado en la zona y la pasividad del Ejército ante la acción de las estructuras neoparamilitares que hacen presencia en el territorio. (Le puede interesar: ["Al Bajo Cauca no llega la sustitución pero si la extorsión y el desplazamiento"](https://archivo.contagioradio.com/al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento/))

### **La OCHA advierte sobre desplazamientos masivos y confinamiento en Chocó**

La Oficina de las Naciones Unidas para la Coordinación de Asuntos Humanitarios (OCHA) para Colombia advirtió desde marzo el confinamiento al que se ven sometidas poblaciones afro e indígenas pertenecientes a las cuencas de los ríos Bojayá, Opogadó y Napipí. Esta situación se debe al aumento de acciones por parte del Ejército de Liberación Nacional (ELN) y las AGC; sin embargo, el pasado martes la oficina emitió una nueva alerta por un aumento en el número de comunidades afectadas, llegando a 9 (4 indígenas y 5 afro).

El resultado de limitar la movilidad de las comunidades se refleja en la calidad de vida que tienen las 2.778 personas afectadas que no pueden cosechar sus siembras ni pescar, impide que los niños asistan a la escuela, al tiempo que genera daños a la salud de adultos mayores y niños. Sobre esta situación la Defensoría del Pueblo emitió una alerta temprana, y la OCHA pidió que se asegure la protección a la vida de las comunidades afectadas. (Le puede interesar: "Tumaco:[Sin Gobierno, sin agua y con mucha violencia"](https://archivo.contagioradio.com/tumaco-gobierno-agua-violencia/))

> \[EHP\] Flash Update N° 2 - Confinamientos en Bojayá (Chocó) - <https://t.co/mTS3jLWWUT> [@GerardGomez59](https://twitter.com/GerardGomez59?ref_src=twsrc%5Etfw) [pic.twitter.com/pA8nHtqOmA](https://t.co/pA8nHtqOmA)
>
> — Ocha Colombia (@ochacolombia) [16 de abril de 2019](https://twitter.com/ochacolombia/status/1118280536844308481?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
