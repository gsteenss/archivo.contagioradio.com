Title: “Nuevo código de Policía otorga poderes exorbitantes a los uniformados”
Date: 2017-01-25 15:26
Category: DDHH, Otra Mirada
Tags: alirio uribe, código de policía, ESMAD, policia
Slug: nuevo-codigo-de-policia-otorga-poderes-exorbitantes-a-los-uniformados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Ene. 2017] 

Luego de los disturbios y las agresiones sufridas por las personas asistentes a la movilización antitaurina a manos del ESMAD, vuelve a ser tema de discusión el nuevo código de Policía que entra en vigencia el 30 de enero, sobre todo teniendo en cuenta que en éste no se hace referencia al uso desmedido de la fuerza por parte de los uniformados.

**Para el Representante a la Cámara, Alirio Uribe**, lo que sucedió en la movilización antitaurina fue una muestra de que el comportamiento del ESMAD “**lo que hace es generar violencia, lo cual preocupa** dado que como ya lo han manifestado instituciones del gobierno, se está lejos de cerrar el ESMAD, lo que se nos ha dicho es que se va a fortalecer”.

Sin embargo, esta no es la única preocupación del cabildante, quien agrega que **este es un código prohibicionista que lo que va a lograr es que los colombianos odien a los policías**; “no va a cooperar a la convivencia sino que va a generar abusos permanentes; es un código hecho por policías para otorgarle poderes exorbitantes a la Policía" asegura.

Según Uribe, el ESMAD genera una mala imagen para la Policía como institución e hizo referencia al caso del joven Sebastián Díaz “vemos el caso de este joven al que un integrante del ESMAD le disparó en la cara, ya hemos hecho debates mostrando cómo armas que en principio son no letales, son utilizadas de mala fe y terminan en muertos o en mutilaciones”.

El nuevo código de Policía da cuenta de las facultades para el uso de la fuerza, por lo que el representante a la cámara invitó a la sociedad a leer de manera “juiciosa” la normativa para conocer las nuevas directrices. Le puede interesar: ["Código de Policía es para una dictadura, no para el posconflicto": Alirio Uribe](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/)

En total, el Código consta de 243 artículos, de los cuales 230 quedaron como habían sido presentados en la Cámara de Representantes. Por ello y en vista de la inconformidad de varios sectores, **en la actualidad se han presentado más de 49 demandas a la Corte Constitucional en contra de la normativa,** de las cuales 23 fueron admitidas y están en trámite.

### **El ESMAD como provocador en las movilizaciones** 

Según el cabildante del Polo Democrático, hay que tener en cuenta que en las marchas por lo general se presentan personas que son infiltradas para producir violencia y deslegitimar las manifestaciones “como se vio en las movilizaciones del domingo en la Plaza La Santamaría”.

Y agregó que muchas veces el ESMAD incita a las personas asistentes a las movilizaciones, lo que termina en actos violentos con resultados preocupantes y violaciones a los derechos humanos.

Por ello, hizo la invitación a las personas convocantes y asistentes a estas marchas a estar atentos y a “ejercer un control” para que eviten que sus movilizaciones sean infiltradas, e instó a la Alcaldía de Bogotá a que el ESMAD no haga presencia en las próximos eventos de los animalistas “deberían estar gestores de convivencia y así evitar se violen los derechos de unos u otros”.

### **ESMAD en cifras** 

Según la Comisión Nacional de Derechos Humanos de la Mesa Nacional Agropecuaria y Popular de Interlocución y Acuerdos, durante los 20 días de protestas del Paro Agrario de 2013, se produjeron denuncias sobre 660 casos de violaciones a los derechos humanos individuales y colectivos, 262 detenciones arbitrarias, 485 heridos, 21 personas heridas con arma de fuego, 52 casos de hostigamientos y amenazas,  y 51 casos de ataques indiscriminados.

También se indica que **son 13 los asesinatos en los que estaría directamente implicado el ESMAD, desde el año 2001 al 2016.**  Le puede interesar: [El prontuario del ESMAD](https://archivo.contagioradio.com/el-prontuario-del-esmad/)

### **¿Cuál sería el Código de Policía “ideal” para el posconflicto?** 

Ya en otras oportunidades el representante Alirio Uribe había dicho que **el nuevo código de Policía no está pensado para una Colombia en posconflicto**, puesto que esta inscrito en la misma lógica.  Le puede interesar: ["Perdimos la oportunidad de tener una policía para el posconflicto", Alirio Uribe](https://archivo.contagioradio.com/perdimos-la-oportunidad-de-tener-una-policia-para-el-posconflicto-alirio-uribe/)

Según Uribe, lo que habría que hacer, antes de pensar en más códigos de Policía o reformas a los mismos, es “fortalecer la cultura ciudadana, pues es el único componente fuerte e importante para mejorar la convivencia. **Yo no tengo ninguna duda que necesitábamos un nuevo código de Policía pero había que hacerlo en función del posconflicto,** lo que significaba reestructurar muchas cosas al interior de la Policía”.

¿ESMAD con licencia para matar?

<div class="fb-post" data-href="https://www.facebook.com/angelamrobledo/videos/10154091744387740/" data-width="500" data-show-text="true">

> ¿ESMAD con licencia para matar?  
> Comparte, \#QueSeAcabeElESMAD  
> Posted by [Angela María Robledo](https://www.facebook.com/angelamrobledo/) on [jueves, 4 de agosto de 2016](https://www.facebook.com/angelamrobledo/videos/10154091744387740/)

</div>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
