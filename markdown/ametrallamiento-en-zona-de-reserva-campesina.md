Title: Denuncian ametrallamiento en Zona de reserva campesina
Date: 2015-01-24 20:05
Author: CtgAdm
Category: DDHH, Paz, Resistencias
Tags: acvc, Zona de Reserva Campesina del Valle del Río Cimitarra, zonas de reserva campesina
Slug: ametrallamiento-en-zona-de-reserva-campesina
Status: published

/\*  
¡Bienvenido a Custom CSS!  
CSS (Hojas de estilo en cascada) es un tipo de lenguaje de programación  
que indica al navegador cómo procesar una página web. Puedes eliminar  
estos comentarios y comenzar con tus personalizaciones.  
Por defecto, tu hoja de estilo se cargará después de las hojas de estilo  
del tema, lo que significa que tus normas pueden prevalecer y anular las  
reglas CSS del tema. Solo tienes que escribir aquí lo que quieres cambiar,  
no es necesario copiar todo el contenido de la hoja de estilo de tu tema.  
\*/  
.logo\_h img {  
margin-left: 40px;  
margin-top: 30px;  
}  
\#cnss\_widget-5 {  
float: right;  
}  
\#widget\_sp\_image-2 {  
margin-top: 40px;  
}  
.wpcf7-form {  
background: \#FFFFFF;  
padding: 8px;  
font-size: 20px;  
font-family: "oswald";  
}  
.wpcf7-form-control {  
color: \#ffffff;  
}  
.wpcf7-list-item-label {  
color: \#000000;  
}  
li.tab-1 a {  
color: \#4086aa;  
}  
li.tab-2 a {  
color: \#4086aa;  
}  
.banner-wrap {  
border: 0;  
}  
\#cnss\_widget-3 {  
margin-left: 75px;  
}  
.wpcf7-textarea {  
color: \#000000;  
}
