Title: La mujer palestina que con sus grafittis derriba muros
Date: 2017-06-15 12:19
Category: Onda Palestina
Tags: Apartheid, BDS Colombia, Cultura, Palestina
Slug: palestina-israel-apartheid
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Grafitera-palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 15 Jun 2017 

Laila Ajjawi es una grafitera palestina que vive en uno de los campos de refugiados en la ciudad de Irbid en Jordania. Sus abuelos, como los de muchos palestinos en Jordania, fueron desplazados en 1948 de sus tierras en Jenin al crearse el Estado de Israel, y desde entonces se asentaron en el campo de refugiados de Irbid, como muchos palestinos en ese momento, pensaron que esto sería temporal y que antes de lo imaginado podrían volver a sus tierras, desafortunadamente no fue el caso.

Laila tiene 26 años, y nació en Irbid al igual que su madre y padre, pero cada uno de ellos reivindica su identidad como palestinos. En una entrevista para la BBC Ajjawi afirma, "el graffiti es como una puerta que se abre directamente hacia la gente en la calle y que habla directamente con todos los transeúntes”. El mensaje que logra comunicar por medio de su graffiti encierra una dualidad muy interesante, pues los muros que buscan muchas veces separar, dividir, apartar y negar el libre tránsito de las personas, se terminan convirtiendo en el lienzo donde ella comparte su visión del mundo y de la vida.

Su arte busca quebrarlos, mandarlos abajo, o por lo menos cambiarles su función, porque estos muros con la pintura y los colores permiten transmitir infinidad de mensajes, muchos de ellos sobre la alegría, la vida, el amor, la fuerza y la resistencia.

Ajjawi hace parte de la iniciativa Women on Walls, -WOW- que funciona como una red de mujeres grafiteras y artistas visuales de Medio Oriente. Ella resalta que el mundo del arte callejero es muchas veces dominado por los hombres, pero afirma con toda seguridad que ella pertenece a ese mundo, y decide resaltar a la mujer en su arte. Subrayar, además, el poder interior de las personas para enfrentar la adversidad, afirmando: “Me gusta centrarme en lo positivo. No representarnos como víctimas o débiles, sino decirle al resto de mujeres: ‘tienes una voz en el mundo y debe ser oída”.

En esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), un programa dedicado a la solidaridad con Palestina desde Colombia, discutimos los sucesos de la última semana en los territorios ocupados, conversamos con un compañero de la campaña BDS en chile donde tuvieron una gran victoria esta semana, discutimos los actos de conmemoración y denuncia de los 50 años de ocupación y compartimos información sobre la campaña colombiana contra la empresa Hewlett Packard por su complicidad con la ocupación y el apartheid en Palestina.

<iframe id="audio_19286847" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19286847_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
