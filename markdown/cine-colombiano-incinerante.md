Title: Cortometrajes hechos por jóvenes se verán en Incinerante
Date: 2017-06-09 16:13
Category: Cultura
Tags: Cine, Festivales, Medellin
Slug: cine-colombiano-incinerante
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Afiche-Convocatoria-Muestra-Incinerante.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Incinerante Fest 

###### 09 Jun 2017 

Como una nueva apuesta por el cine colombiano llega la primera versión de Incinerante, festival de cine independiente de Medellín, que busca mostrar las problemáticas que viven los jóvenes a través del cortometraje.

Incinerante busca recoger las producciones que representar algunos de los conflictos que viven los jóvenes entre los 12 y 17 años de edad, contados través del lente, la imaginación y creatividad de los realizadores.

El Festival que se desarrollará del 25 al 29 de julio en la capital antioqueña, contará con dos espacios en su programación, por un lado la exhibición de producciones en diferentes sales y espacios de la ciudad y por otro una serie de talleres y conversatorios donde los participantes podrán aprender de expertos en la realización audiovisual.

Otra de las iniciativas interesantes del evento es "Bicicine", experimento que se introdujo en el marco del Festival Invazion de 2016,en el que un vehículo ecológico difumina las fronteras de acceso al cine mientras se apropia de los espacios públicos y configura nuevas formas de habitar la ciudad a través de la exhibición al aire libre.

La convocatoria para el festival estará abierta hasta el próximo 30 de junio. Los interesados deben enviar sus piezas audiovisuales finalizadas a partir de enero de 2015 y que no superen los 30 minutos y que aborden las temáticas estipuladas en el marco del evento.

<iframe src="https://www.youtube.com/embed/W2HG-P2sHEc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Todo el proceso de inscripción lo podrá hacer por la [página oficial](https://incinerantefest.wixsite.com/festivaldecinejoven/convocatoria-incinerante-2017) de Incinerante. Las piezas elegidas harán parte de la muestra central del Festival, una poderosa selección de miradas libres en historias que no se dejan encasillar.
