Title: Lámpara de la paz de Francisco de Asís en manos de Santos
Date: 2017-01-12 10:15
Category: Abilio, Opinion
Tags: Papa Francisco, Santos
Slug: lampara-de-la-paz-de-francisco-de-asis-en-manos-de-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Lampara-de-san-francisco-de-asis-a-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cesar Carrión - SIC 

###### 12 Ene 2017 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

[A mediados de diciembre del año pasado, el presidente Santos  recibió en Asís Italia la **Lámpara de la Paz de San Francisco** de parte de la comunidad Franciscana. No sabemos qué tanto sepa él del Santo ni qué intérprete del reconocimiento, aparte de su discurso en el que ninguna alusión hizo al subversivo mensaje del que ha sido portador históricamente el pobre de Asís. Ante tantos homenajes de la comunidad internacional por la firma del acuerdo, mucho nos tememos que sea una adulación más en este mundo de complacencias y de egos en el que los éxitos son los que resultan  premiados, máxime cuando  es el  poder el que los porta.]

**Sería deseable un reconocimiento similar  a los miembros de las FARC-EP y del ELN comprometidos  con una salida política negociada al conflicto**; como también  a los movimientos y organizaciones, en especial a las  de víctimas, que desde la  invisibilidad  se la están jugado por la paz, en medio de los riesgos a su vida e integridad personal.

**No sabemos qué tanto  crea  en San Francisco de Asís y   si se ha dejado interpelar por  su vida.**  Lo cierto es que el legado de Francisco, más allá del lugar común de la paz,  sin que entremos a preguntarnos el significado de  “Hazme un instrumento de Tu Paz”, está en franca contravía de decisiones del orden económico, de hondas implicaciones para el ambiente,  tomadas por Santos, como por ejemplo su política minera, su política agroindustrial,  su política financiera que, como la ley Zidres,   contradicen las reformas propuestas en los mismos acuerdos firmados.

**El papa Francisco en *Laudato Sí***[**, pone el acento justo en las implicaciones  del testimonio histórico de San Francisco**  para nuestra relación con el mundo: “(...) si ya no hablamos el lenguaje de la fraternidad y de la belleza en nuestra relación con el mundo, nuestras actitudes serán las del dominador, del consumidor o del mero explotador de recursos, incapaz de poner un límite  a sus intereses inmediatos”.  (No 11) Recepción del  testimonio de San Francisco que debería resonar en el presidente de Colombia.]

Francisco de Asís, como se sabe,  fue  hijo de un hombre de poder en la edad media, quien hastiado por la vaciedad que le provocaba esa vida de Señor Feudal, desafío a su  padre desnudándose  de las prendas de su casta para luego vestirse de harapos y vivir en las calles de Asís, construir una comunidad de seguidores de Jesús, el pobre de Belén, cuyo nacimiento recreó en el pesebre.

Desde ahí interpeló a su propia iglesia invitándola a la coherencia en el seguimiento de Jesús de Nazaret  y con la construcción de la paz, desde el reconocimiento sincero del otro, sobre todo de los “monstruos” que el poder suele edificar para justificar su muerte, tal como ocurrió con los  musulmanes con quienes Francisco sostuvo un extenso diálogo logrando convencerlos de disponerse a acordar la paz con los católicos,  con quienes se disputaban militarmente los lugares santos de Jerusalén, logro que no fue correspondido por sus hermanos en la fe.

También en un célebre diálogo con el papa  Inocencio III en 1209, donde le habló de las distancias del poder y la riqueza, con el Evangelio de Jesús. Enorme signo de contradicción  con el poder de dominación que se erige en democracias como la colombiana, donde aún en tiempos de implementación  de acuerdos de paz, opera el paramilitarismo con la complicidad, omisión y aquiescencia  de agentes del  Estado, asesinando a personas cercanas al proceso, vinculadas a movimientos sociales y políticos de oposición.

También desde su vida de pobreza, pudo romper con el antropocentrismo que hacía de la naturaleza  medios para la satisfacción de las necesidades humanas, dándole el lugar de hermanas y hermanos, como el perseguido lobo de Gubbio, o como lo dejó consignado en el hermoso  himno a las criaturas del otoño de 1225:   “el señor hermano sol”, “la hermana luna y las estrellas”,  “el hermano viento el aire y la nube y el cielo sereno”, “el hermano fuego”, “la hermana  nuestra hermana tierra, la cual nos sostiene y gobierna y produce diversos frutos con coloridas flores y hierbas”.

**La Lámpara de la Paz** en las manos del presidente Santos, antes que posicionarlo como “embajador de la paz mundial”, como él mismo se proclamó en la  recepción, debería iluminarlo para que asuma de modo contundente  el desmonte efectivo  de las estructuras paramilitares en Colombia,  que pueden dar al traste con el proceso, así no  haya hecho nada por cambiar el modelo económico, político y social generador de la pobreza y de la crisis  ambiental, que defendió a ultranza en los diálogos con la guerrilla de las FARC-EP y que pretende seguir defendiendo en los diálogos con el ELN.

#### [**[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**] 

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
