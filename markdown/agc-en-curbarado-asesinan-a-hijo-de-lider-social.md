Title: AGC en Curbaradó asesinan a hijo de líder social
Date: 2020-08-13 18:14
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: AGC, Chocó, control social, Curbaradó, Lider social
Slug: agc-en-curbarado-asesinan-a-hijo-de-lider-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/AGC-en-Bajo-Cauca-A.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 12 de agosto la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/agc-asesina-a-joven-hijo-de-lider-social-de-llano-rico/),**denunció el asesinato de Esneider Díaz Ospina** en el corregimiento de Llano Rico, territorio colectivo de **Curbaradó, Chocó** , a manos de integrantes de las Autodefensas Gaitanistas de Colombia (AGC).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización el hecho se presentó el martes 11 de agosto a las 4:45 pm, **mientras el joven de 26 años atendía su negocio ubicado en la escuela del corregimiento en Curbaradó**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí ingresaron dos hombres integrantes de las AGC, quienes le dispararon y luego se retiraron *"en una moto de alto cilindraje con completa libertad"*, señala la Comisión, y agregan que **en el cuerpo del joven dejaron un mensaje que decía, *"por rata y faltan otros más".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Esneider era hijo del líder social Luis Licenio Días Ospina**, patriarca de la comunidad de Llano Rico; además según la organización el pasado 2 de agosto en horas de la mañana **integrantes de este grupo paramilitar habían sentenciado a muerte al joven,** afirmando que le quedaban pocos días de vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la denuncia, también señalan qué **2 días luego del asesinato no hizo presencia en el territorio ninguna autoridad** que realizará el levantamiento del cuerpo, razón que obligó a que los familiares tuvieran que hacerlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, indican que esta **estructura paramilitar continúa desarrollando operaciones de[control social](https://archivo.contagioradio.com/en-riesgo-20-000-persona-por-accionar-de-grupos-armados-en-narino/)por medio de la instalación de dos puntos en el corregimiento de Llano Rico,** y en donde determinan que pueden, o no hacer los pobladores, y además impiden que las comunidades denuncian o asistan a reuniones.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Están obligando a los pobladores a que en sus lugares de habitación pernocte los integrantes de las AGC, operaciones que se realizan en medio de actuaciones de control de las Fuerzas Militares".*
>
> <cite>Comisión Intereclesial de Justicia y Paz</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
