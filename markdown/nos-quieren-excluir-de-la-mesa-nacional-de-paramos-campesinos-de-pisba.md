Title: Nos quieren excluir de la Mesa Nacional de Páramos: Campesinos de Pisba
Date: 2020-01-21 16:46
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, campesinos, Mesa Nacional de Páramos, páramos, Pisba
Slug: nos-quieren-excluir-de-la-mesa-nacional-de-paramos-campesinos-de-pisba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOP1wx_X0AMzQnh.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/15d8ccfb-gp0stqm4i_high_res-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/15d8ccfb-gp0stqm4i_high_res-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/DAR2231-768x512-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/DAR2231-768x512-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @GreenpeaceColom

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Mesa Nacional de Páramos (MNP) denunció por medio de un comunicado que se han convocado diferentes reuniones a nombre de la organización con voceros que no representan los intereses de la comunidad, ni son reconocidos como habitantes de los municipios cercanos al Páramo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Estas acciones han sido atribuidas por los integrantes de la Mesa a Wilson Castillo, quien según el comunicado "*ha querido tomarse la vocería del páramo de Pisba, siendo él habitante del páramo Tota Bijagual Mamapacha, del cual no presenta ningún informe de trabajo*".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además agregaron que, "*un empresario minero del páramo de Pisba integrante de la MNP le hacía consignaciones públicas para sus gastos de desplazamiento, no sabemos actualmente quien le colabora con estos gastos*". (Le puede interesar: <https://archivo.contagioradio.com/dia-de-la-ecologia-ecosistemas-colombia/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una de las principales razones que hizo que los campesinos del Páramo de Pisba, municipios de Socha y Tasco hicieran pública esta situación se da luego de que se realizara el pasado 13 de enero una reunión en las oficinas de la empresa minera Agrocoal LTDA, entidad que actualmente explota carbón en el Páramo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La convocatoria fue privada, algunas pocas personas se enteraron a última hora y asistieron sin haber sido invitados, buena parte de los asistentes son mineros o empleados mineros".*
>
> <cite> campesinos del Páramo de Pisba </cite>

<!-- /wp:quote -->

<!-- wp:image {"id":79485,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/DAR2231-768x512-2.jpg){.wp-image-79485}  

<figcaption>
Foto:* Daniel Reina Romero*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Al mismo tiempo en el comunicado denunciaron que *"en algunos municipios del páramo de Pisba sus alcaldes son mineros a patrocinados por ellos, así como también los concejales"*. (Le puede interesar: <https://archivo.contagioradio.com/fallo-historico-protegera-los-territorios-de-titulos-mineros/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Los mineros deberían hacer su defensa como tal y no como campesinos"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En el mismo documentos señalaron que en las reuniones de la MNP hay varios mineros del páramo de Pisba, *"ellos deberían hacer su defensa como mineros y no como campesinos habitantes del páramo"*; ante esto pidieron por medio de carta a la Corte Suprema de Justicia que haga mayor investigación en el momento de emitir los fallos y las delimitaciones en el Páramo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

*"Solicitamos que no sigan desinformando al campesinado y utilizándolo para la defensa de sus intereses mineros*, *nunca se preocuparon por los campesinos paramunos y ahora si nos quieren utilizar*".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una defensa que se da hace 20 años por el Páramo de Pisba

<!-- /wp:heading -->

<!-- wp:image {"id":79473,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Páramos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOP1wx_X0AMzQnh-1024x576.jpg){.wp-image-79473}  

<figcaption>
@EntreOjosCo

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El páramo de Pisba ubicado en entre los departamentos de Boyacá y Casanare es considerado uno de los mas grandes de Colombia con **103.315 mil hectáreas** , y provee de agua a los ríos de Magdalena y Orinoco.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo es considerado uno de los más poblados con más de **6.300 familias** que lo habitan y luchan hace más de 20 años por la conservación del hábitat y encontrar una forma de sustento rentable para sus familias diferente a la minería.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pisba produce entre un millón y un millón 500 mil toneladas de carbón, según las últimas cifras de la Agencia Nacional de Minería, acción que han intentado detener los campesinos que han defendido por años este recurso. (Le puede interesar: <https://www.justiciaypazcolombia.com/carta-a-rodrigo-monsalve-y-a-nathalia-de-sus-companers-de-antropologia-de-la-universidad-nacional/> )

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hemos sido agredidos, demandados y algunos amenazados, hemos tenido que hacer peticiones, acciones populares, marchas, bloqueos, etc, hasta lograr sacar del páramo algunas multinacionales*"
>
> <cite> Campesinos del Páramo de Pisba </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente los campesinos hicieron un llamado a la comunidad a conocen los procesos del páramo de Pisba, *"los invitamos a informarse adecuadamente y no apoyar ciegamente posiciones que manipulan y desinforman"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
