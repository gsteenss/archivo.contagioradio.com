Title: San Juan de Dios es monumento nacional que debería reforzarse y no demolerse
Date: 2020-07-07 18:37
Author: CtgAdm
Category: Actualidad, Política
Tags: Bogotá, Hospitales de Bogotá, san juan de dios
Slug: san-juan-de-dios-es-monumento-nacional-que-deberia-reforzarse-y-no-demolerse
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/San-Juan-de-dios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Hospital San Juan de Dio/ Paola Valencia Camacho

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Después del anuncio hecho por la alcaldesa de Bogotá, [Claudia López](https://twitter.com/ClaudiaLopez), sobre al acta firmada para empezar la demolición de la torre central del Hospital San Juan de Dios y la creación de una nueva torre hospitalaria, son varias las voces que han advertido que se trata de una decisión va en contra vía de lo técnico, lo legal, lo patrimonial y lo salubre y que no ha sido tenido en cuenta por la Empresa de Renovación y Desarrollo Urbano de Bogotá (ERU).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Jaime Urrego, médico docente e investigador en Salud Pública** explica que una demolición del hospital que ha prestado sus servicios desde 1564 y que sucumbió al abandono estatal al inicio del milenio, no es necesaria, partiendo del hecho de que la torre central de hospital, junto a todos los edificios que conforman la sede, son un bien patrimonial y un bien de interés cultural protegido por leyes de patrimonio como la ley 735 del 2001 que consagra al San Juan de Dios como monumento nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El patrimonio nacional está clasificado en varias categorías y aunque durante la administración de Gustavo Petro se compró el predio y se inició un desalojo de los trabajadores que vivieron ahí durante 17 años, bajo la administración de Enrique Peñalosa junto al Ministerio de Cultura se realizó un Plan Especial de Manejo y Protección que determinó que de las 24 estructuras del San Juan de Dios, 17 fueron definidas como de conservación total o arquitectónica, y 7 podrían ser intervenidas, algo que fue interpretado como incluso ser demolidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Algunas edificaciones pueden ser conservadas, reutilizadas, y otras reemplazadas o liberadas, bien sea por obsolescencia o porque fueron resultado de necesidades funcionales específicas", establece la resolución 995 de 2016 del Ministerio de Cultura definida en aquel entonces.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello y según un estudio de vulnerabilidad sísmica realizado por la Universidad de los Andes en el 2016, el San Juan de Dios no cumple con las normas de sismo resistencia al estar construido bajo un método antiguo usado en los años 50, una condición que según explica Urrego, padecen al menos **"el 70 % de los edificios que prestan servicios de salud en el país",** sin embargo tras evaluar las opciones se concluyó que entre tumbar el edificio o reforzarlo estructuralmente no hay una diferencia abismal en los costos por lo que resulta mejor la segunda opción si se mezcla "lo técnico con lo juridico".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Ni con pandemia ni sin pandemia debe tumbarse, debe ser reforzado" pues cuando el hospital ofrecía su servicio tenía a su disposición 550 camas incluyendo 40 UCI, sin embargo no se reconstruiría esta sede sino se crearía una nueva torre para la Unidad Médica Santa Clara cercana al San Juan de Dios, **de cerrarse Santa Clara se dejaría de prestar un servicio de 250 camas y 40 UCI para la construcción de un hospital nuevo que pasaría a tener 312 camas en lugar de las 800 que podrían existir si se reforzara el San Juan y el Santa Clara.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El metro y los contratos que tampoco convencen

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Urrego señala que la eventual remodelación del Santa Clara está ligada a la ruta del metro elevado que pasaría por aquel sector de la ciudad y que requeriría dicho espacio en la Avenida Caracas en Bogotá, para su movilidad total por lo que conviene a la Alcaldía una modificación en aquel lugar. [(Lea también: Retos, oportunidades, fortalezas y amenazas del gabinete de Claudia López)](https://archivo.contagioradio.com/retos-oportunidades-fortalecas-y-amenazas-del-gabinete-de-claudia-lopez/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega, que el contrato de adquisición para esta obra por el valor de 560.000 millones de pesos y que comprende la demolición, el diseño, la construcción, dotación y operación por cinco años de la nueva sede, serían entregadas a la empresa española Copasa, compañía con "un grave problema reputacional debido a ser señalada en escándalos de corrupción y soborno" por el Banco Mundial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Están entregándole a un contratista una gran cantidad de dinero, sin estudios para que cuando los haga diga que que no vale eso sino que vale mucho más", advierte Urrego. Copasa estaría a cargo de la sede, desde la fecha en que se culmine la obra hasta los siguientes cinco años, es decir eventualmente desde 2025 hasta el 2029.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
