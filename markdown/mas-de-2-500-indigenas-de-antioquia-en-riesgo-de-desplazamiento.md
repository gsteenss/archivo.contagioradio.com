Title: Más de 2.500 indígenas de Antioquia en riesgo de desplazamiento
Date: 2020-04-28 18:52
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Antioquia, embera, indígenas
Slug: mas-de-2-500-indigenas-de-antioquia-en-riesgo-de-desplazamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Organización-Indígena-de-Antioquia-en-Dagua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo OIA {#foto-archivo-oia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Organización Indígena de Antioquia (OIA) denuncia que por presencia de actores armados, el pasado viernes 24 de abril cerca de 300 integrantes del pueblo Embera Eyábida se desplazaron desde su comunidad Santa María hacia Llano Jacinto en el municipio de Urrao, Antioquia, por presencia de actores armados ilegales en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de un [comunicado](https://twitter.com/OIA_COLOMBIA/status/1254944609404420096/photo/1), la Organización manifestó que serían 70 las familias desplazadas por grupos armados ilegales en el resguardo Majoré, en el suroeste antioqueño. Situación que se presenta pese a que el espacio cuenta con medidas de protección colectiva por parte de la Unidad Nacional de Protección (UNP).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, según Alexis Espitia, Consejero DD.HH. y Paz de la OIA, entre las familias hay menores y mujeres que están en la escuela de la comunidad de Llano Jacinto en la que, sin embargo, no han recibido la debida atención porque no cuentan con las medidas para atender una emergencia de tal dimensión.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Entre 2.500 y 3.000 indígenas en riesgo de desplazamiento por acción de grupos armados** en Antioquia

<!-- /wp:heading -->

<!-- wp:paragraph -->

La OIA ha denunciado constantemente el sufrimiento de comunidades indígenas en los municipios de Frontino, Murindó y Vigía del Fuerte, en el departamento de Antioquia, por presencia de grupos armados que generan desplazamientos y confinamientos mediante su actividad en la zona, en combate con otras estructuras o mediante el uso de artefactos explosivos improvisados en los caminos que usan las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Específicamente, en el resguardo Majoré, Espitia alertó por nuevos desplazamientos que puedan producirse y afectarían a las tres comunidades que integran el espacio y están compuestas por entre 2.500 y 3.000 mil personas. (Le puede interesar: ["Antioquia y Chocó registran el mayor número de reclutamiento a menores"](https://archivo.contagioradio.com/antioquia-y-choco-registran-el-mayor-numero-de-reclutamiento-a-menores/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, la OIA llamó a los organismos de control, seguimiento y protección a que se activen las rutas para atender las comunidades desplazadas que aún en medio de la pandemia del Covid-19 tienen que enfrentar la violencia en sus territorios. (Le puede interesar: ["Un adulto y dos menores indígenas, víctimas fatales de minas en Antioquia y Chocó"](https://archivo.contagioradio.com/un-adulto-y-dos-menores-indigenas-victimas-fatales-de-minas-en-antioquia-y-choco/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
