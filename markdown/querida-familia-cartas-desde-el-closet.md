Title: Querida familia - Cartas desde el Closet
Date: 2017-10-30 06:00
Category: Cartas desde el Closet, Opinion
Tags: LGBTI, LGBTI Colombia
Slug: querida-familia-cartas-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cartas-desde-el-closet.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: camara.leg 

[Octubre, 2017]

#### **Querida familia** 

Varias veces me han reclamado porque últimamente estoy muy poco en almuerzos, cenas y encuentros familiares; y que cuando estoy, parezco ausente, diferente a como era antes.

La verdad es que los diálogos y el ambiente familiar se volvieron insostenibles para mí desde que empezaron a repetir constantemente, en esos espacios, mensajes religiosos y políticos contra la llamada ideología de género y con una actitud que impide una discusión más o menos seria.

Pero además, me sentí muy herido el día que intenté decir que no todos los mensajes religiosos y políticos sobre el tema eran ciertos, que había muchas mentiras que se decían sobre las pretensiones de los homosexuales de imponer la orientación sexual de niños y niñas, que los argumentos supuestamente científicos repetidos por pastores, curas y políticos que “defienden” la familia y las buenas costumbres eran muy cuestionados por científicos serios; varios de ustedes lo que hicieron fue cuestionar mi identidad sexual, de lo que hablaré en otro momento.

Por ahora les digo que, en el movimiento feminista, el mismo que ustedes descalifican “por defender la ideología de género”, hay muchas y diferentes visiones sobre este tema, incluso hay feministas muy religiosas. En el fondo, les une el reclamo por sus derechos, el rechazo a la violencia contra la mujer, la discriminación contra ellas y el trabajo por eliminar el machismo.

Algo parecido pasa con quienes llaman despectivamente “homosexuales”, de quienes ustedes dicen que todos son depredados, perversos y que son así por su maldad natural.

No fue posible ayudarles a entender que detrás del nombre genérico de “homosexual” está la población LGTBI (Lesbianas, Gays, Transgénero, Bisexuales e Intersexuales) que son personas muy diversas entre ellas; que no han hecho ningún acuerdo para tomarse el país ni para imponer su condición sexual a niños y niñas; que en cada grupo hay miradas muy distintas de la sexualidad, de la religión, de la familia y que son personas a quienes los prejuicios sociales y religiosos les han generado mucho sufrimiento que moral y socialmente tiene miradas divergentes y a veces antagónicas de la familia, de la moral, de la religión y la política.

A estas personas las une el rechazo social, familiar y religioso que padecen; el ser condenas sin ser escuchadas, sin mirarlas como seres humanos; la exigencia de respeto de la sociedad, que es lo mínimo que debemos pedir para toda persona humana, independiente de su condición sexual, social, racial y religiosa. No podemos imponer nuestra mirada sobre estos temas ni permitir que nos la impongan.

Espero que algún día podamos hablar de estos temas y de otros con respeto a las diferencias, argumentos e información más o menos seria, sin fundamentalismos políticos y religiosos.

Con mucho respeto,

[Un integrante de la familia]

**Parche por la vida 9 - Ver más [Cartas desde el Closet](https://archivo.contagioradio.com/cartas-desde-closet/)**
