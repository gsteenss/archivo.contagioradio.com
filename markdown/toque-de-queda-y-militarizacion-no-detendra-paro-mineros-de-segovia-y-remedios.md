Title: "Toque de queda y militarización no detendrán paro" mineros de Segovia y Remedios
Date: 2017-08-02 15:40
Category: DDHH, Entrevistas
Tags: Antioquia, mineros, Paro de mineros, Remedios, Segovia
Slug: toque-de-queda-y-militarizacion-no-detendra-paro-mineros-de-segovia-y-remedios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marmato_mineros_medoro_a39-e1497381473274.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [02 Ago. 2017] 

Después de **13 días de paro convocado por los mineros de Segovia y Remedios en el departamento de Antioquia**, el gobernador de Antioquia decretó toque de queda y militarizó la región, además de reforzar la presencia del ESMAD. Según los manifestantes estas decisiones dejan un saldo de 10 personas heridas y alejan la posibilidad de diálogo.

Los mineros en manifestación denunciaron que el ESMAD realizó una fuerte arremetida contra la movilización pacífica que están realizando para pedir la legalización de la minería como práctica ancestral, entre otros temas.

### **ESMAD ocupó instalaciones del hospital regional** 

**Jaime Mongo, Vicepresidente de la Mesa Minera de Remedios y Segovia** aseguró que estas dos semanas han sido de protesta pacífica, sin bloqueos de vía, sin disturbios, hasta el momento que con gases lacrimógenos fueron sorprendidos por el Escuadrón Móvil Antidisturbios –ESMAD-.

“Tipo 10 a.m. el lunes se ubicaron en un alto del pueblo donde queda el Hospital San Juan de Dios de la Población y **hubo confrontación con la población porque ellos comenzaron a atacar.** La población estaba de manera pacífica pero ya cuando ven a este grupo del ESMAD ya se alteran. Le pedimos a las autoridades que retirarán este grupo y la respuesta fue que por encima del que fuera traían el ESMAD”

Si bien Mongo dice que no puede dar un número exacto de los efectivos del ESMAD que se encontraban en el municipio dice que “**eso fue una cantidad los que llegaron y se atrincheraron en el Hospital**, prácticamente dejando encerrados los empleados y desde allí disparaban con proyectiles de fusil, hiriendo a 4 personas”. Le puede interesar: [Corte Constitucional pone freno a minería en Resguardo Cañamomo y Lomaprieta](https://archivo.contagioradio.com/corte-constitucional-pone-freno-mineria-resguardo-canamomo-lomaprieta/)

### **Hubo acercamientos con la Multinacional, pero fueron fallidos** 

Dicen los mineros de Segovia y Remedios que, pese a la negativa de la multinacional Gran Colombia Gold, de reconocer la existencia de la minería ancestral en la zona y mejorar las condiciones laborales de los mineros que trabajan para ellos pudieron concretar una reunión que se realizaría este martes en el Batallón de Seguridad de la localidad de Segovia, pero a último minuto fue cancelada. Le puede interesar: [Mineros antioqueños suspenden paro tras acuerdos con el gobierno](https://archivo.contagioradio.com/mineros-antioquenos-suspenden-paro-tras-acuerdos-con-el-gobierno/)

“Hubo un acercamiento a ver si nos sentábamos a las 2 de la tarde, pero resulta que sorpresivamente la multinacional incumplió y **quería hacer una negociación de la magnitud del problema que tenemos en el municipio por Skype** y nosotros no permitimos que las cosas se hagan por Skype, sino que queremos que vengan al territorio”.

### **Las protestas podrían estar infiltradas por paramilitares** 

El Gobernador de Antioquia, Luis Pérez aseguró que el paro podría estar siendo infiltrado por paramilitares o por el ELN, sin embargo, el lider fue enfático en su rechazo a estas declaraciones ya que ponen en riesgo a las más de 7 mil personas que se encuentran hoy concentradas en el paro minero. Le puede interesar: [Asesinan abogado de la Defensoría del Pueblo William García](https://archivo.contagioradio.com/asesinan-abogado-de-la-defensoria-del-pueblo-william-garcia/)

“Somos unos mineros que estamos pidiendo al gobierno y a la multinacional que nos deje trabajar, que nos de espacio y que queremos formalizarnos. Es la comunidad, el pueblo en general está protestando pacíficamente. **Para ellos justificar la represión del Estado inventan todas estas situaciones, pues si ellos saben en dónde están que los cojan** si ellos son las autoridades, pero no criminalicen la protesta pacífica”.

### **¿Qué es lo que está sucediendo con los mineros en Remedios y Segovia?** 

En Segovia desde hace 7 años llegó la multinacional Gran Colombian Gold que obtuvo el permiso de la Agencia Nacional de Licencias Ambientales sobre el territorio, razón por la cual los mineros trabajan para esta sin recibir mayores beneficios.

Aseguran los mineros que antes de ese hecho, nunca hubo problemas en el territorio por la minería artesanal-que buscan sea legalizada- con otras empresas que estaban, como la multinacional Frontino Gold Mines. Le puede interesar: [Mineros Artesanales de Marmato le ganan el pulso a la Gran Colombia Gold](https://archivo.contagioradio.com/mineros-artesanales-de-marmato-le-ganan-el-pulso-a-la-gran-colombia-gold/)

**“Lo que quiere la Gran Colombian es acabar con los pequeños mineros, nos ofrecen contratos, pero son agresivos** y el minero no los puede cumplir. Si un minero se mete con esos contratos en 2 meses va a la quiebra, porque ellos manejan una fórmula de contraprestación por ser los dueños del título, el minero incluso por eso tienen que pagarles a ellos. Pretenden acabar con la cadena productiva, que es la que sostienen a la población”.

Es decir, la propuesta que fue entregada por la multinacional y rechazada por los mineros, es que ellos lleven todo el material aurífero a la empresa, dejando sin trabajo a las chatarreras que son quienes recogen el material que no es de utilidad para los mineros, a los arrieros que trasladan el material y sin trabajo a los comercializadores.

“Por ponerle un ejemplo, si usted le **lleva una producción de \$200 mil pesos a la multinacional, ellos se quedan con \$180 mil, le devuelven \$20 mil pesos al minero** y el minero con \$20 mil pesos para pagar seguridad social, el salario a los trabajadores, al sostenimiento de la mina, no da”. Le puede interesar: [Comunidades afrodescendientes buscan impulsar la minería ancestral](https://archivo.contagioradio.com/comunidades-afrodescendientes-buscan-impulsar-la-mineria-ancestral/)

Para evitar ese tipo de contratación y la explotación de la multinacional, los mineros siguen exigiendo el reconocimiento y la legalización de la minería artesanal, de modo que "se deje de criminalizar. Los mineros de Segovia se quieren formalizar, estar en la ley pero que sea una cosa equitativa para el bien de nuestra población".

### **Los mineros presentaron su propuesta.** 

Como Mesa Minera presentaron una propuesta que, según ellos, permitirá que los mineros no vayan a la quiebra y que la multinacional pueda seguir estando en sus territorios, sin embargo, la multinacional no la aceptó porque “**ellos lo único que quieren es imponer su contrato y no llegar a una solución.** Pero aclaramos que queremos llegar a una solución, estamos dispuestos al diálogo, tenemos las puertas abiertas”.

La propuesta que presentarán en la próxima reunión que sostengan con la multinacional y con el Gobierno nacional, estará **basada en el sostenimiento de la cadena productiva** en el que no deban entregar todo lo que los mineros extraen de las minas a la Gran Colombian Gold. Le puede interesar:[ Inicia ampliación del proyecto minero Buriticá en Antioquia](https://archivo.contagioradio.com/inicia-ampliacion-del-proyecto-minero-buritica-antioquia/)

### **Mineros seguirán en paro pese a toque de queda y militarización.** 

En el centro de concentración de Segovia se encuentran **más de 7 mil personas, que han manifestado continuarán en paro** pese al toque de queda, a la fuerte militarización y a los heridos que resultaron luego de la arremetida del ESMAD este lunes.

Advertencia: Imágenes que pueden herir susceptibilidades

\  
<iframe id="audio_20135760" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20135760_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
