Title: Así fue el anuncio del fin de la guerra con las FARC
Date: 2016-08-24 18:23
Category: Nacional, Paz
Tags: FARC, Gobierno, La Habana, paz
Slug: gobierno-y-farc-anuncian-cierre-de-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/terminación-de-las-conversaciones-y-el-cierre-del-Acuerdo..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [24 Ago 2016]

Las delegaciones del Gobierno Nacional y de las FARC-EP anuncian que han logrado llegar a un Acuerdo Final, integral y definitivo, sobre la totalidad de los puntos de la Agenda del Acuerdo General para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera en Colombia.

Aquí el comunicado conjunto de las partes.

[Comunicado Conjunto](https://es.scribd.com/document/322103288/Comunicado-Conjunto#from_embed "View Comunicado Conjunto on Scribd")

<iframe id="doc_68610" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322103288/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
