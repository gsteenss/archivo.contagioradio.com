Title: Las violaciones a la libertad de prensa por parte del Ejército Nacional
Date: 2020-08-19 11:29
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Ejército Nacional, Libertad de Prensa, Seguimientos ilegales
Slug: las-violaciones-a-la-libertad-de-prensa-por-parte-del-ejercito-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Atentados-del-Ejercito-Nacional-contra-libertad-de-prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de las denuncias de la comunidad de Corinto, Cauca, sobre el accionar del Ejercito Nacional, que habría provocado la muerte del comunicador indígena Abelardo Liz ; la Fundación para la Libertad de Prensa -[FLIP](https://flip.org.co/index.php/es/informacion/pronunciamientos/item/2566-las-graves-violaciones-a-la-libertad-de-prensa-por-parte-del-ejercito-nacional-en-el-2020)- documentó junto a este hecho, una serie de **ataques de los que ha sido blanco la prensa por parte de la Fuerza Pública.** (Lea también: [En Corinto tras intervención de fuerza pública se reportan dos muertos y tres heridos](https://archivo.contagioradio.com/en-corinto-tras-intervencion-de-fuerza-publica-se-reportan-dos-muertos-y-tres-heridos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la FLIP el asesinato de Abelardo Liz es la violación a la libertad de prensa más grave que registrada en el 2020. No obstante, la Fundación ha documentado otros casos durante el año en los que también estarían vinculados miembros de las Fuerzas Armadas; como el ocurrido el pasado 4 de junio en el que **Fernando Osorio perdió tres dedos de su mano derecha luego de recibir disparos por parte de miembros del Ejército Nacional en Guayabero, Meta.** (Lea también: [Medios independientes son la otra víctima de la Fuerza Pública en la movilización](https://archivo.contagioradio.com/medios-independientes-son-la-otra-victima-de-la-fuerza-publica-en-la-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, según la FLIP, Osorio y tres de sus compañeros del colectivo Voces del Guayabero han denunciado **amenazas contra su vida y detenciones irregulares por parte de los uniformados, en medio de los cubrimientos que realizan a los operativos de erradicación forzada** en la vereda de Vista Hermosa, Meta y otros más en el Norte del Guaviare. (Lea también: [Operativo de erradicación forzada en Guayabero deja 4 heridos y 20 campesinos retenidos por las FFMM](https://archivo.contagioradio.com/guayabero-erradicacion-forzada/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, la FLIP registró también los seguimientos y perfilamientos ilegales de los que fueron víctimas 130 personas, incluidos varios periodistas. **Según la Fundación, el 11 de junio la Procuraduría General de la Nación confirmó a través de un informe. que la cifra de periodistas perseguidos por parte del Ejército Nacional ascendía a 52 perfiles. **

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las violaciones a la libertad de prensa no arrojan castigos ni responsables

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la FLIP, a pesar de las solicitudes de investigación que han elevado ante el Ejército Nacional, **hasta la fecha no se han esclarecido los hechos denunciados, ni se ha informado sobre las sanciones o condenas contra los funcionarios responsables de estas graves violaciones a la libertad de prensa.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contrario a ello, la FLIP denuncia que a través de sus comunicados de prensa el Ejército Nacional «*continúa negando que la sociedad civil esté siendo afectada por sus acciones, y señala de insurgentes o guerrilleros a periodistas y miembros de las comunidades para justificar sus acciones desproporcionadas*». 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, aseguró que estas vulneraciones se dan omitiendo órdenes expresas de la Comisión Interamericana de Derechos Humanos -CIDH- sobre el deber de los Estados de prevenir y sancionar estas acciones, y adicionalmente que **«*la sistematicidad con la que el Ejército ha actuado en detrimento de la prensa, es un grave indicio de que no se trata de actuaciones aisladas por parte de uniformados individuales, sino que existe una instrucción en el interior de las fuerzas militares para limitar el ejercicio periodístico*».** (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
