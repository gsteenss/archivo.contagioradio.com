Title: "La historia cierta del Pueblo Wiwa", informe que revela la violencia sufrida en el conflicto armado
Date: 2019-11-18 22:16
Author: CtgAdm
Category: DDHH, Nacional
Tags: Ccajar, CEV, Informe, JEP, Pueblo Wiwa, SIVJRNR
Slug: la-historia-cierta-del-pueblo-wiwa-informe-que-revela-la-violencia-sufrida-en-el-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.18.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.19.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.20.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.21.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.22.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.23.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.24.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.25.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Entrega-informe-pueblo-wiwa.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo / Contagio Radio] 

De las manos de la comunidad fue entregado el informe "**La historia cierta del Pueblo Wiwa desde el corazón del mundo** en el marco del conflicto armado" al sistema integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), documento que busca que se conozca la historia de violencia que ha vivido el pueblo ancestral durante los últimos 27 años, y que se adelanten investigaciones que esclarezcan quienes y por qué, se ha perseguido a este pueblo de la Sierra Nevada de Santa Marta.

En San Juan del Cesar se reunieron más de 150 indígenas Wiwa, para entregar el trabajo en el que registran **por lo menos 142 graves violaciones a los derechos humanos**, entre ellas 14 ejecuciones extrajudiciales, dos masacres, homicidios cometidos por grupos paramilitares y guerrilleros, desapariciones, desplazamiento forzoso, afectaciones de violencia en contra de las mujeres, contra los sitios sagrados y contra el territorio. Este documento se elaboró durante once meses y se realizó con la recopilación de entrevistas y testimonios.

\[caption id="attachment\_76705" align="aligncenter" width="1024"\]![Maria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.21-1024x682.jpeg){.wp-image-76705 .size-large width="1024" height="682"} Ana Iris Loperena/ Contagio Radio\[/caption\]

### **“Si nos toca morir por la verdad lo hacemos”** 

Durante las más de cuatro horas se escucharon los testimonios del Pueblo Wiwa, entre ellos el de **Ana Iris Loperena,** “nos sentimos orgullosos de que hoy estemos en esta entrega, pero también nos sentimos tristes por todos nuestros seres queridos que perdimos en la violencia". Igualmente en su intervención resaltó el papel que han tomado las comunidades de mantenerse fuertes e hizo el llamado a los cuatro pueblos de la Sierra Nevada de Santa Marta, a actuar en unidad por la **defensa del territorio contra los actores armados y contra los proyectos de gobierno** que hoy los siguen aquejando.

> “No nos vamos a callar, vamos a seguir hablando porque nosotros no queremos que nuestro futuro y nuestras nuevas generaciones vivan la violencia que nosotros hemos vivido”, Ana Iris Loperena

 

\[caption id="attachment\_76710" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Entrega-informe-pueblo-wiwa-1024x682.jpeg){.wp-image-76710 .size-large width="1024" height="682"} La comunidad presenta por primera vez un informe acerca de la violencia que sufrió en el conflicto armado / Contagio Radio\[/caption\]

Entre las voces también estuvo **Jomaira Nieves**, indígena Wiwa que perdió a tres de sus familiares en medio del conflicto armado: el primero de ellos fue asesinado en el año 1999 por la guerrilla de las FARC-EP; luego, en 2002, su segundo hermano es asesinado por el Ejército y posteriormente presentado como integrante de las AUC ,dado de baja en combate y exhibido en un Consejo de Seguridad por el entonces presidente Alvaro Uribe Vélez; y la última víctima de la familia fue su hermana, embarazada,  asesinada y presentada como guerrillera de la entonces guerrilla de las FARC-EP.

> Para ella, la importancia de la entrega de este informe radica en la verdad y en la posibilidad de saber qué ocurrió, “aunque sea doloroso para uno saber la verdad, pues que la digan... , más doloroso es vivir uno en la impunidad” afirmó.

### **Que se conozca la historia del pueblo Wiwa** 

**Jomary Ortegón**, integrante del **Colectivo de Abogados José Alvear Restrepo** (CAJAR), organización que hizo parte de este informe, aseguró que, además de hablar del dolor y las violaciones de derechos humanos, el documento también da cuenta de la resistencia y el hermanamiento de los pueblos y la pervivencia de la cultura Wiwa.

Asimismo, para la jurista es **importante que la sociedad conozca los hechos de violencia que sufrieron las comunidades, porque de esta manera** “los familiares tienen presente lo que significó el dolor o qué significa el desarraigo, lo que significó la muerte, lo que significó la destrucción, también lo que ha significado la resistencia. Quienes no creemos que lo tengan presente es la sociedad colombiana y el Estado colombiano, lo que queremos es que no se quede entre nosotros y nosotras que conocemos la historia cierta del Pueblo Wiwa, sino que pueda divulgarse ampliamente”.

\[caption id="attachment\_76704" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.20-1024x682.jpeg){.wp-image-76704 .size-large width="1024" height="682"} Jomary Ortegón, abogada CAJAR / Contagio Radio\[/caption\]

### **Recomendaciones para las SIVJRNR** 

El informe busca que **la JEP establezca las responsabilidades,** hasta los más altos mandos , detrás de la violencia vivida por el pueblo Wiwa, cometida por todos los grupos armados. De igual forma se espera que la Comisión para el Esclarecimiento de la Verdad (CEV) **recoja los relatos** para la comprensión histórica de las causas, responsabilidades institucionales, impactos diferenciados en los pueblos étnicos y mujeres, y las recomendaciones para la no repetición de la violencia provocada en su territorio.

Igualmente se pretende que la Unidad de Búsqueda (UBPD) **diseñe un plan** con las comunidades y víctimas para dar cuenta del paradero de personas víctimas de reclutamiento forzado, retención ilegal o desaparición forzada cuyo paradero hoy se desconoce (Le puede interesar: "[Tras 16 años Estado pedirá perdón por masacre y desplazamiento del pueblo Wiwa](https://archivo.contagioradio.com/estado-perdon-wiwa/)")

**Por su parte la Dra. Reinere Jaramillo**, Magistrada Presidenta de la Sección de primera instancia en los casos ausencia de reconocimiento de verdad y responsabilidad de hechos y conductas en la JEP, manifestó el compromiso de éste órgano con las comunidades indígenas afirmando que "ustedes pueden tener la certeza de que el informe va a ser cuidadosamente estudiado y analizado por toda la JEP, institución que debe y tiene la obligación de dar respuesta a las víctimas del conflicto armado".

 

\[caption id="attachment\_76702" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.18-1024x682.jpeg){.wp-image-76702 .size-large width="1024" height="682"} Entrega del Informe a la JEP / Contagio Radio /\[/caption\]

Igualmente, la Magistrada se comprometió con las comunidades indígenas en llevar su propuesta de apertura del macrocaso de la Sierra Nevada de Santa Marta ante la JEP.

"Este informe permite construir con el Pueblo Wiwa esos procedimientos dialógicos que hoy viene impulsando la Jurisdicción, que puedan participar de manera directa y articular los diferentes canales de la JEP. Este informe será claro para las rendiciones de cuentas que tendrá que dar la JEP al país".

La entrega del informe concluyó con la música de Bartolo Loperena, cantante Wiwa, que a través de sus letras refleja los hechos de violencia que vivió este pueblo y que inspiró el nombre de este informe.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
