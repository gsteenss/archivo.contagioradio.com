Title: Una mirada común a los "Días y noches entre guerra y paz"
Date: 2017-10-11 12:10
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine, Cultura, Documental, paz
Slug: dias-y-noches-entre-guerra-y-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/21125778_1708116686162332_2409624201244926169_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Uli Stelzner 

###### 11 Oct 2017 

Sin el fuerte trabajo de producción e investigación que caracteriza a otras producciones de **Uli Stelzner**, director de cine alemán, encontró en un viaje a Colombia la posibilidad de mostrar con su cámara otra mirada del proceso de paz, resultando de esta exploración el documental "**Días y noches entre guerra y paz**".

"Yo estaba invitado para dar un taller de cine y video en el Valle del Cauca y Antioquia y no llevaba ni equipo ni recursos para filmar, pero cuando llegue a Colombia, dos semanas antes de la firma de la paz, **sentí esa energía, esa relación incluso histórica y sentí la necesidad de grabar de lo que veía**" asegura Stelzner sobre la producción.

En relación con su mirada a la cultura latinoamericana desde el documental, el director asegura no ser "inocente" ya que **la mayor parte de su filmografía se ha realizado en Guatemala**, país donde presenció un complejo proceso de post conflicto. Allí, desde su rol como director de la Muestra Internacional de cine Memoria de la justicia, fue invitado a Bogotá y de esa mnera inició su proceso para llegar a lo que es hoy el producto audiovisual.

<iframe src="https://player.vimeo.com/video/215744705" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[Dias y noches entre guerra y paz](https://vimeo.com/215744705) from [Rehilete Post](https://vimeo.com/rehiletepostproduccion) on [Vimeo](https://vimeo.com).

"**No tenía una visión prefabricada o una postura fija frente al proceso**, más que nada me deje llevar por la intuición y la curiosidad de **un cineasta quién quiere saber que piensan las personas comunes de este proceso**", factor que a su juicio se convierte en un diferencial a producciones documentales rodadas dentro de la misma coyuntura histórica como "[El silencio de los fusiles](https://archivo.contagioradio.com/silencio-fusiles-documental/)" y "El fin de la guerra"

El carácter independiente de la producción, donde el mismo S**telzner trabajó como director, camarógrafo y sonidista**, le dio la posibilidad abierta de ser creativo al momento de editar el material compilado, proceso para el cual buscó apoyo económico, consciente que en la mayoría de canales y fundaciones de fomento respaldan poco los proyectos en un estado de realización tan avanzado.

Sumando ayudas de amigos, logró realizar la corrección de color del material y la traducción del documental a inglés y alemán, "**ha sido un proceso satisfactorio aunque fuese con las condiciones mínimas y en tan poco tiempo, pero estoy muy satisfecho con el trabajo**" afirma el director.

Actualmente el producto se encuentra en etapa de exhibición fuera del circuito comercial por la complejidad que implica realizar una distribución efectiva. Espacios académicos y comunitarios han servido para compartir el producto e invitar simultáneamente a la reflexión sobre la paz y la esperanza depositada en el proceso. Actividades que hasta la fecha han tenido lugar en Bogotá y Cali.

Las proyecciones del documental proseguirán de la siguiente forma:

Lugar: Atrio de la iglesia Santa Bárbara, Granada, Antioquia.  
Fecha: Sábado 14 de octubre.  
Participan: Uli Stelzner (Cineasta), Gilberto Guerra (líder comunitario). Modera: Dubian Giraldo (Director de la Emisora Grana Stereo).  
Hora: 7:00 pm.

Lugar: Atrio de la iglesia del Corregimiento de Santa Ana, Granada, Antioquia.  
Fecha: Domingo 15 de octubre.  
Participan: Uli Stelzner (Cineasta) y Gilberto Guerra (líder comunitario).  
Hora: 7:00 pm.

Lugar: Museo Casa de la Memoria, Medellín.  
Fecha: Martes 17 de octubre.  
Participan: Uli Stelzner (Cineasta), Gilberto Guerra (líder comunitario) y Libardo Andrés Agudelo (Director de Ciudad Comuna).  
Hora: 4:00 pm.

Lugar: Sala de Cine Luis Alberto Álvarez, Universidad de Antioquia, Medellín.  
Fecha: Miércoles 18 de octubre.  
Participan: Uli Stelzner (Cineasta), Gilberto Guerra (líder comunitario) y Juan David Ortíz Franco (Periodista y docente de la Facultad de Comunicaciones de la Universidad de Antioquia).  
Hora: 5:00 pm.

Lugar: Centro de Fe y Culturas, Medellín  
Fecha: Jueves 19 de Octubre  
Participantes: Uli Stelzner  
Hora: 6:00 pm.
