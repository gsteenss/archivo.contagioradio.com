Title: ¿Qué cambia en la JEP con la aprobación de la Ley Estatutaria?
Date: 2018-08-17 12:58
Category: Nacional, Paz
Tags: Congreso, Corte Constitucional, JEP, Patricia Linares
Slug: que-cambia-en-la-jep-con-la-aprobacion-de-la-ley-estatutaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/jep.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jurisdicción Especial de Paz ] 

###### [17 Ago 2018] 

Las penas alternativas, la participación en política, el papel de la Fiscalía, el acogimiento de terceros y agentes del Estado no combatientes, la inhabilidad de los magistrados y la extradición son los temas fundamentales en los que falló la Corte Constitucional en las modificaciones del proyecto de ley Estatutaria de la JEP.

Para Patricia Linares, presidenta de la JEP, Este hecho significa que ya se puede contar con un marco normativo completo, incluido una norma estatutaria, que **fija el derrotero de las competencias y el alcance de las mismas sobre los temas que se tratarán en la JEP**.

### **Los condicionamientos de la Corte Constitucional** 

Referente a las modificaciones que se habían hecho desde al Congreso a las penas alternativas por delitos de violencia sexual**, la Corte manifestó que esta institución no tenía las competencias para realizar esos cambios, además**, Linares señaló que este tipo de delitos no son amnistiables y agregó que es importante que se tenga claridad sobre el tipo de sanciones que hay dentro de la JEP teniendo en cuenta los aportes a la verdad, la reparación y a la no repetición que realicen quienes comparezcan.

La primera sanción es la denominada como propia que se impone cuando exista pleno aporte del compareciente. La segunda es la sanción alternativa que supone **privación de la libertad** y se establecen cuando el reconocimiento por parte del compareciente es tardío. La última sanción se coloca cuando no hay ningún tipo de reconocimiento por parte del compareciente.

Referente a este punto, la Corte señaló que quienes estén ejerciendo un rol de participación política y sean llamados por la JEP y sancionados con penas privativas de su libertad no podrán continuar en su cargo de elección popular. Asimismo, referente a la extradición la Corte concluyo que **ninguna de las personas que se hayan sometido a la JEP podrán ser extraditadas**, a menos que los delitos se cometan tiempo después de la firma del Acuerdo de paz.

Frente a las inhabilidades que se habían impuesto a quienes ejercen el papel de Magistrados de la JEP, la Corte señaló que el Congreso no puede colocar este tipo de requisitos, adicionales a los que ya se encuentran en la Constitución. (Le puede interesar: ["Citaciones y Audiencias: el nuevo capítulo de la JEP"](https://archivo.contagioradio.com/citaciones-y-audiencias-nuevo-capitulo-de-jep/))

Sobre los terceros y agentes del Estado no combatientes que no se hayan presentado voluntariamente a la JEP, pero se vean involucrados en sus llamados, será la Fiscalía quien priorice los casos y las investigaciones. De igual forma la Corte Constitucional expresó que la JEP no podrá dejar de seleccionar los casos de delitos no amnistiables y que adquieran la connotación de lesa humanidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
