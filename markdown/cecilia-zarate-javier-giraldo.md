Title: En memoria de Cecilia Zarate (Necrologio) Por Javier Giraldo
Date: 2017-06-06 22:25
Category: Nacional, yoreporto
Slug: cecilia-zarate-javier-giraldo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Cecilia-Lucia-Zarate-Laun_1486564786384_5806416_ver1.0_640_360.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: channel3000] 

###### [06 Jun 2017] 

**Por Javier Giraldo S.J **

Cecilia Zárate -Laun falleció el domingo 5 de febrero en el Hospital Santa María de Madison, estando en compañía de su esposo y de tres hermanas. Había nacido el 10 de mayo de 1945 en el departamento de Santander, Colombia. Fue la mayor de 5 hermanas, hizo su bachillerato en Bucaramanga y se graduó como Nutricionista en la Universidad Nacional de Colombia en Bogotá en 1966. Se vinculó entonces a la facultad como profesora de Nutrición y fue beneficiaria de una beca para estudiar en los Estados Unidos donde obtuvo el grado de Magister por la Universidad de Wisconsin.

Regresó a Colombia donde fue vinculada como nutricionista en el Plan Nacional de Nutrición del Gobierno Colombiano. En 1976 contrajo matrimonio con John (Jack) Laun a quien había conocido mientras estudiaba en los Estados Unidos.

La pareja regresó a Estados Unidos en 1977 y repartía su tiempo entre una casa campestre junto al Lago Elkhart a la que ella llamaba emotivamente “mi cajita de sorpresas” y un apartamento en Madison. Celebraron 40 años de matrimonio el 24 de abril de 2016. Cecilia fundó una fábrica y restaurante para empanadas llamadas “Coquitos” en la cual ella y sus socios trabajaron 8 años. En 1987 Cecilia y Jack fundaron Colombia Support Network (CSN) organización que durante los siguientes 30 años se convirtió para Cecilia en el principal proyecto de vida.

Como Directora del Programa, Cecilia organizó más de 50 delegaciones de residentes de Estados Unidos a Colombia, apoyando la formación de capítulos de CSN en los Estados Unidos y hermanándolos con comunidades rurales en zonas de conflicto con el fin de impulsar paz con justicia en Colombia y una transformación de la política de Estados Unidos hacia Colombia. Como parte de su larga y comprometida dedicación a los derechos humanos, Cecilia llegó a ser miembro del consejo directivo de la Liga Internacional de Mujeres por la Paz y la Libertad y fue muy activa en el Capítulo local de dicha Liga.

También fue elegida en el Subcomité Latinoamericano del Comité Americano de Servicio de los Amigos (Quakers) y prestó servicio por varios años en la Junta Directiva de la Red de Wisconsin por la Paz y la Justicia (WNPJ). En octubre de 2015 Cecilia recibió el Premio Anual de Ciudadanía Global por parte del Capítulo de Madison de la Asociación de Naciones Unidas. En el desarrollo de su trabajo en pro de la paz con justicia en Colombia, Cecilia descubrió varios internos valiosos del Departamento de Estudios Internacionales de la Universidad de Wisconsin.

También desarrolló programas en el Departamento de Estudios Latinoamericanos e ibéricos de la Universidad. Como muestra de su energía, luchó valerosamente con un cáncer por cerca de 4 años siendo capaz de continuar en su trabajo de apoyo a la humanidad.

Le sobreviven su esposo Jack y 4 hermanas, Elsa y María Teresa en Bogotá, Colombia, María Helena en Madison y Marta en Charleston, Illinois. Además sus sobrinos René Walters en Monte Horeb, Michael Sullivan en Madison, Steve Sullivan en Seattle, Washington, Juan Carlos Mantilla y Andrés Mantilla en Bucaramanga, Colombia, y María Lucía Aguilar en Bogotá, Colombia.

El 25 de febrero se realizó un servicio fúnebre por Cecilia en el Monasterio de Santa Sapiencia en Wisconsin. Según su expresa voluntad. Al llegar a Bogotá se oficiará una Eucaristía en la Iglesia de Nuestra Señora de la Soledad, el 8 de junio de 2017.

Invitamos a todas las organizaciones sociales, de derechos humanos y amigos y deudos de Cecilia a estar presentes el Jueves 8 de junio a las 5:30 p. m. en la Iglesia de nuestra Señora de la Soledad Carrera 25 No. 39-67 - Barrio La Soledad.

###### **Para YoReporto** 
