Title: Familias de internos de la Tramacua también son víctimas de tratos inhumanos
Date: 2016-01-21 17:13
Category: Judicial, Nacional
Tags: CTI, INPEC, La Tramamcua
Slug: vsitantes-tramacua-vicitmas-tratosinhumanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/TRAMACUA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón. 

<iframe src="http://www.ivoox.com/player_ek_10156388_2_1.html?data=kpWel5uXfJmhhpywj5WZaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncafVzs7Zy8bXb8XZjM7b1srWstDnjMnSjdHFb7XmwtLOxdrFb9XVzsfWh6iXaaKtz5Dg0dOPuoa3lIqupsjYrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [[(]21 Ene 2015[)]]

Desde la cárcel[La Tramacua,](https://archivo.contagioradio.com/?s=La+tramacua+) los voceros de los internos denuncian que el pasado 17 de enero, el Cuerpo Técnico de Investigación, **CTI, de la Fiscalía junto al INPEC,** continuaron realizando actos denigrantes contra los prisioneros y ahora contra sus familias. Esta vez, por medio de un operativo **retuvieron todo un día a 24 mujeres que iban a visitar a sus familiares,** argumentando que ingresarían a la cárcel sustancias alucinógenas en sus partes íntimas.

Nuevamente se demuestra que La Tramacua, es “El centro número uno de tortura”, afirman en un comunicado los prisioneros, quienes señalan de “falso positivo”, esta nueva acción del INPEC y la Fiscalía, como una forma de seguir degradando a los reclusos.

**“Al INPEC no le fue suficiente los tratos crueles degradantes, de tortura e inhumanos contra los internos.** No satisfechos con eso, la administración en cabeza del nuevo director el Capitán Luis Francisco Perdomo, arreció contra las familias visitantes. La fiscalía y el CTI realizó una operación que se convirtió en la excusa para reprimirlos”, dice la denuncia.

Las 24 mujeres señalas de portar sustancias alucinógenas, fueron llevadas a la Unidad de Reacción Inmediata de la Fiscalía, en donde **permanecieron todo el día sin tomar una gota de agua.** Mientras tanto requisaron e hicieron registro fotográfico a las casas de las detenidas, en donde esperaban encontrar algún tipo de droga psicoactiva, pero lo cierto, es que no se encontró nada, y a las 10 de la noche debieron soltar a las mujeres que aseguraron haber sido humilladas.

María Yaneth Casallas, fue una de las víctimas de las arbitrariedades del INPEC. Ella llevaba dos años sin ver a su esposo Luis Alberto Abril, recluido en la Tramacua, sin embargo pese a que tuvo que comprar los pasajes para desplazarse de Bogotá a Valledupar, y pagar un hotel durante 3 días, no pudo ver a su esposo, debido al hecho ya anunciado.

Tras esa situación, las mujeres que habían sido detenidas interpusieron una denuncia penal y otra disciplinaria contra los funcionarios del INPEC. Por su parte, lo internos se preguntan **“¿Quién responde por los daños psicológicos y morales sufridos por sus familiares? ¿Quién responde por los daños económicos? ¿será que los visitantes volverán a visitarnos?”.**

Por el momento, los prisioneros continúan exigiendo el cierre de la cárcel de mediana y alta seguridad, tras la visita de verificación donde se constató el nivel de hacinamiento en el que viven los internos.
