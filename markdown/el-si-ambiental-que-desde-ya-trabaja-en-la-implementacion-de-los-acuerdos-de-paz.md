Title: El 'Sí Ambiental', que desde ya trabaja en la implementación de los acuerdos de paz
Date: 2016-09-20 10:59
Category: Ambiente, Entrevistas, Paz
Tags: Ambiente, FARC, paz
Slug: el-si-ambiental-que-desde-ya-trabaja-en-la-implementacion-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14359101_676859825825733_4065680596841525094_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sí Ambiental 

###### [20 Sep 2016] 

Porque la paz también es para los animales y la naturaleza, nace la campaña Si Ambiental, con la que además de impulsar el voto por el Sí en el plebiscito del próximo 2 de octubre, se empieza una labor de impulso para la implementación de los acuerdos junto a las comunidades y los guerrilleros que retornan a la vida civil.

“Desde 1998 estamos trabajando por la paz, que debe ser también para la naturaleza. La guerra ha generado muchos daños ambientales por medio de los derrames de petróleo, los bombardeos, adicionalmente **la guerrilla y los paramilitares aislaron al país de zonas muy importantes de biodiversidad para conocer**”, dice Carlos Fonseca, vocero de la campaña y exviceministro de ambiente.

Justamente las organizaciones ambientalistas que conforman esta iniciativa, hace algunos meses entregaron a la delegación de paz de las FARC, **una propuesta con 11 puntos, para evidenciar el daño que se le ha hecho a la naturaleza y a su vez generar medidas para la conservación de los ecosistema**, entre ellas se habló de declarar moratoria selectiva a la minería, moratoria contra al fracking, una transición a energías limpias e incorporar la ciencia y la tecnología para la innovación en defensa del ambiente.

Y es que de acuerdo con Fonseca, “En términos económicos, la naturaleza genera más riqueza que si sumamos todas las riquezas que producen los humanos, es un error inmenso dañar nuestros ecosistemas, el modelo agropecuario se convirtió en ganadería extensiva, expulsando a los campesinos y desaprovechando esas tierra. El **caribe colombiano tiene el 42% de las mejores tierras del país para agricultura, pero el 95% está en ganadería** extensiva”.

Es por ello, que **la labor de esta campaña se centra también en la implementación de los acuerdos con un enfoque de conservación ambiental.** En ese sentido se piensa hacer un diagnóstico ambiental de cada municipio que será entregado en la próxima cumbre ambiental que se realizará en junio de 2017. Allí, con la información recopilada se construirá un gran atlas ambiental que será presentado a los próximos gobiernos para que tomen las medidas necesarias para  hacer frente a la situación ambiental de cada municipio.

Otra de las labores que realizará el ‘Si Ambiental’, será en junio del próximo año, “Vamos a armar **grupos con las universidades en las 22 zonas veredales y 6 campamentos de las FARC para hacer talleres con la comunidad y los excombatientes** para diseñar cuales serían los usos del territorio que deberían establecerse que sean ambientalmente incluyentes y sostenibles”, explica Fonseca.

La primera actividad se realiza este 20 de septiembre en la Universidad Nacional. Donde diferentes organizaciones ambientalistas, empiezan a trazar la hoja de ruta, frente a la campaña por el Sí y las tareas para la implementación de los acuerdos con enfoqie ambiental.

<iframe id="audio_12984035" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12984035_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
