Title: Agresiones contra defensores de DD.HH. ascendieron a 184 entre julio y septiembre
Date: 2020-11-27 10:21
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Agresiones contra defensores de DDHH, Somos defensores
Slug: 184-agresiones-contra-defensores-de-dd-hh-entre-julio-y-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-1-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-1-2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defensores de DD.HH / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El programa Somos Defensores presentó su más reciente [Boletín Trimestral](https://t.co/ZZpKzDdDKQ?amp=1) sobre agresiones contra personas defensoras de derechos humanos en Colombia con el que reveló **que entre los meses de julio a septiembre de 2020 se registraron 184 agresiones contra personas defensoras de derechos humanos en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De los 184 casos registrados, 182 fueron los defensores y defensoras afectados por estos crímenes. Esta cifra evidencia un panorama sostenido de violencia contra defensoras y defensores frente a las 183 agresiones registradas en el mismo período de 2019.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SomosDef/status/1331973031883890688","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SomosDef/status/1331973031883890688

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Los departamentos más afectados son **Cauca que registró 48 casos (25%) de agresiones, seguido por Nariño con 40 agresiones (con un incremento del 22%), Cundinamarca con 21 hechos (12%) y Córdoba con 10 casos (6%).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De las 184 agresiones, 44% de los casos ocurrieron en septiembre, el 30% en agosto y el 26% en julio, representando una variación con respecto a 2019 y 2018 en que el mes con mayor número de agresiones fue agosto. (Le puede interesar: [En primer semestre del 2020 se registraron 95 asesinatos contra defensores de DDHH](https://archivo.contagioradio.com/en-primer-semestre-del-2020-se-registraron-95-asesinatos-contra-defensores-de-ddhh/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":93334,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores.png){.wp-image-93334}  

<figcaption>
Agresiones individuales. Fuente: Informe «El virus de la Violencia»

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto a las agresiones por género, el 83% fueron cometidas contra hombres y el 17% contra mujeres, sin embargo, el informe señala que aunque las agresiones hayan sido menos contra las mujeres, no significa que la violencia contra ellas sea menos grave.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93348,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-1-1.png){.wp-image-93348}  

<figcaption>
Fuente: Informe «El virus de la Violencia»

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading -->

Atentados contra defensores incrementaron un 113%
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El estudio distingue** diferentes tipos de agresiones: amenazas, atentados, detenciones arbitrarias, desapariciones forzadas, judicializaciones y robo de información.** Los registros dan cuenta además de 40 asesinatos(54%) y 121 amenazas (17% menos que el año pasado). Los datos más preocupantes son el aumento de los atentados, que presentaron un incremento del 113%.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93354,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/agresiones-contra-defensores-de-DD.HH-Somos-defensores-2.png){.wp-image-93354}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Por otro lado, **a quienes se le atribuyó el mayor número de agresiones** fueron actores desconocidos con un total de 88 (48%), seguidos de paramilitares 54 (30%), disidencias de las FARC, 20 (11%), ELN 11 (6%), Fuerza Pública 8 (4%) y la Fiscalía 1 (1%). (Le puede interesar: [Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Liderazgos indígenas son los más afectados
------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el [informe](https://twitter.com/SomosDef). **el liderazgo más agredido fue el indígena, con 69 casos que corresponden al 37%**, le siguen los defensores de derechos humanos con 19%, los líderes comunitarios con 10%, campesinos con 9%, comunales con 8%, sindicales con 4%, líderes LGBTI con 4%, afrodescendientes con 4%, juveniles con 3%, líderes de víctimas con 2% y líderes ambientales con 1 caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto a los asesinatos, la cifra asciende a 40 defensores y defensoras que perdieron la vida, con un incremento del 54% en comparación con el mismo período del 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los departamentos donde más asesinatos se cometieron fueron Cauca (11), Chocó (4), Bolívar (3), Huila (3), Meta (3), Nariño (3), Valle del Cauca (3). 6 corresponden a mujeres y 34 a hombres.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para organizaciones como la impulsora de este informe, el panorama resulta desolador y la acción del Estado, insuficiente si se tienen en cuenta los registros presentados por [el Instituto de Estudios para el Desarrollo y la Paz (Indepaz)](http://www.indepaz.org.co/lideres/) que a la fecha contabiliza un total de 259 líderes y defensores de DD.HH. asesinados y [77 masacres](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/) ocurridas en el presente año.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
