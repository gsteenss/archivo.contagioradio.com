Title: Presidente Macri incumple Ley de protección de glaciares
Date: 2016-11-29 12:07
Category: Ambiente, El mundo
Tags: Argentina, glaciares, Mauricio Macri
Slug: macri-incumple-ley-proteccion-glaciares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/glaciares-argentina-e1480438694430.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Econoticias 

###### [29 Nov 2016] 

Cerca de 44 proyectos mineros tienen en riesgo los glaciares argentinos. Así lo demuestra **un informe técnico del Ministerio de Ambiente de Argentina,** que pone en evidencia que el presidente Mauricio Macri no está cumpliendo con la **Ley 26.639 que promueve la **de protección de estos ecosistemas.

La denuncia la han hecho organizaciones como Greenpeace, que aseguran que **el país con 14.500 glaciares,** ha eliminado las retenciones a la exportación de minerales. Una decisión que benefició a las empresas mineras, impulsado la llegada de estas multinacionales al territorio argentino.

La Ley de protección de glaciares, indica que el país debe* "establecer los presupuestos mínimos para la protección de los glaciares y del ambiente periglacial con el objeto de preservarlos como reservas estratégicas de recursos hídricos para el consumo humano; para la agricultura y como proveedores de agua para la recarga de cuencas hidrográficas; para la protección de la biodiversidad; como fuente de información científica y como atractivo turístico constituyendo a los glaciares como bienes de carácter público".*

Sin embargo, de acuerdo con ese informe oficial **en total son 322 proyectos mineros** los que se desarrollan en el país y 77 hacen parte del inventario del Instituto Argentino de Nivología, Glaciología y Ciencias Ambientales. De ellos, 30 los llevan a cabo empresas canadienses que buscan extraer oro y cobre.

### Impactos en los glaciares 

**La minería afecta gravemente a los glaciares tanto en la etapa de exploración como en la fase de explotación.** Greenpeace explica mediante su informe ‘Barrick: minería responsable de destruir los glaciares’, que la remoción de hielo, la construcción de caminos, la perforación y uso de explosivos, el levantamiento de polvo que acelera su derretimiento, y la cobertura de los mismos por materiales de descarte, que terminan acabando fácilmente con los glaciares.

En protesta a esos daños que pueden sufrir los ecosistemas, a inicios de noviembre, integrantes de Greenpeace realizaron una manifestación con carros mineros llenos de calaveras color oro frente a la Casa Rosada. La actividad la llevaron a cabo para denunciar que el presidente argentino apoya la minera canadiense Barrick Gold, que se encuentra explotando oro a cielo abierto en un área que está protegida por la Ley.

Los activistas exigieron a Macri el cierre de la mina de Veladero, que explota **Barrick Gold en San Juan, y que en septiembre produjo un derrame de cianuro**. Además hace un año ya se había registrado otro escape de 1.072 metros cúbicos del mismo componente.

Por otra parte, un reciente informe del Centro de Derechos Humanos y Ambiente señala que el proyecto El Pachón de Xstrata, de la multinacional Glencore en la zona suroeste de la provincia de San Juan Argentina, **impactará a más de 200 glaciares**. El proyecto también pone en riesgo a extensivas áreas de ambiente periglacial, que comprende un 20% del área estipulada para la mina.

Lo anterior son solo algunos de los ejemplos que pueden causar estos 44 proyectos que impactarán directamente los glaciares  que representan las mayores reservas de agua dulce del planeta. "**Es importante que Macri se ponga la camiseta de los glaciares y cierre la mina de Veladero**", señalan los activistas de Greenpeace.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
