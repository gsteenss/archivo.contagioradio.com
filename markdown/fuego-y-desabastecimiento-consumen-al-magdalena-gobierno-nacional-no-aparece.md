Title: Fuego y desabastecimiento consumen al Magdalena. Gobierno nacional no aparece
Date: 2020-03-30 08:58
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: FOME, Magdalena
Slug: fuego-y-desabastecimiento-consumen-al-magdalena-gobierno-nacional-no-aparece
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Magdalena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Magdalena /@carlosecaicedo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de hacer frente a la pandemia del Covid-19, el departamento del Magdalena enfrenta un incendio que no ha podido ser controlado y que desde el pasado 20 de marzo ha consumido cerca de 220 hectáreas en en el sector de La Tigrera. A ello se suman el estado de calamidad en cinco municipios y desabastecimiento de agua en la región; pese a la situación, desde la Gobernación del departamento denuncian que el Gobierno no ha atendido la emergencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

[Jorge Rojas](https://twitter.com/jerojasrodrigue), asesor de despacho de la Gobernación explica que se han solicitado al presidente Duque diez medidas para enfrentar la emergencia sanitaria, además de disponerse todo el presupuesto posible, cerca de 200.000 millones de pesos incluidas regalías para para adecuar la red pública hospitalaria, realizar pruebas de Covid-19, adecuar camas UCI, equipos de respiración asistida y apoyo al personal médico. [(Le puede interesar: Comunidades indígenas generan espacios de protección contra el COVID - 19)](https://archivo.contagioradio.com/comunidades-indigenas-generan-espacios-de-proteccion-contra-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Recursos no se destinan a las necesidades locales sino para el sector privado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tras la creación del Fondo de Mitigación de Emergencias (FOME) desde la Gobernación alertan que los recursos no están siendo reorientados para mitigar las necesidades locales sino para ser trasnferidos al sector privado y garantizar la liquidez del banco, sin tener cuenta otras fuentes de ingresos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fue sugerido al Gobierno utilizar recursos de las reservas internacionales, que incluyen un monto de 53.000 millones de dólares de los que sugieren se utilicen 7.500 millones de dólares. También sugieren que el Banco de la República realice un prestamos para mitigar el impacto económico y declarar una moratoria para destinar los recursos de la deuda externa a las poblaciones más vulnerables del país. [(Le recomendamos leer: "Los decretos no pueden ser solamente para los ricos": Aída Avella)](https://archivo.contagioradio.com/los-decretos-no-pueden-ser-solamente-para-los-ricos-aida-avella-2/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Pido reconsiderar el decreto y privilegiar la autonomía de los entes territoriales para el manejo de la crisis" - gobernador Carlos Caicedo

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Rojas afirma que desde la Gobernación del Magdalena, fueron puestos a disposición 70.000 millones para un plan de asistencia alimentaria, de igual formase ha propuesta una renta básica y un mínimo vital para atender a la población más vulnerable del departamento que suma cerca 1.3 millones de habitantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Existen cinco municipios en calamidad pública por falta de agua y desabastecimiento donde sus habitantes afirman que no existe siquiera la forma de lavarse las manos. Por ahora se ha realizado por parte de la Defensa Civil la entrega de ayudas humanitarias como mercados, tapabocas, agua, en los territorio de San Sebastián, Puebloviejo, Zona Bananera, El Retén, Salamina, Fundación, Pedraza, Sitionuevo, Aracataca, El Difícil, Plato, Pijiño del Carmen, , El Banco, Chivolo, Guamal , Algarrobo y Tenerife.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Sentimos que las regiones estamos siendo tratadas como si fuéramos incapaces de gestionar los asunto de nuestras comunidades", expresó el gobernador Caicedo, resaltando que en el Magdalena el 86% de la población trabaja en la informalidad por lo que no se cuenta con un ahorro para hacer frente a la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los incendios en la Sierra Nevada continúan

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras se vive esta crisis, la Sierra Nevada continúa con más de 8 focos de puntos calientes que impiden apagar el fuego en un lugar donde la topografía, la velocidad del viento y la altura de las llamas en la Tigregra y Huaca. impide el acceso a algunas partes, lo que pone en peligro la integridad de los bomberos. El llamado a que sean usados helicópteros de la Fuerza Aérea y del 'Bambi' para combatir las llamas desde el aire.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organismos de socorro apuntan que las quemas irregulares que se realizan para preparar la tierras para cultivos podrían ser la principal causa del fuego, a su vez, los cafeteros piden que se refuercen las acciones para sofocar las llamas. [(Le puede interesar: Las prácticas turísticas que más dañan los ecosistemas según Pueblos Indígenas de la Sierra)](https://archivo.contagioradio.com/las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
