Title: “Grecia demuestra que se puede pasar de la movilización social a la política y ganar”
Date: 2015-07-07 13:15
Category: Entrevistas, Movilización
Tags: Banco Central Europeo, Banco Mundial, Fondo Monetario Internacional, Frente Amplio por la PAz, Grecia, Unión europea
Slug: grecia-demuestra-que-se-puede-pasar-de-la-movilizacion-social-a-la-politica-y-ganar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Grecia-referendo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 20minutos.com 

<iframe src="http://www.ivoox.com/player_ek_4733086_2_1.html?data=lZyglZWceo6ZmKialJaJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9PZxM7OjcnJsdbZ1Nnfw5DVucaf1MqY0trJqMaf0cbgw9ePqMafzcaYz9Tarc3d28bQy4qnd4a2lNOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### **[Entrevista con Victor de Currea Lugo - analista ]** 

###### [7 Jul 2015] 

Tras el referendo realizado el fin de semana pasado en Grecia, Alemania y Francia, potencias económicas de la UE, están esperando la propuesta  del primer ministro griego para el pago de la deuda o la implementación de un plan de “salvamento” que algunos plantean en fases. Sin embargo, la decisión de los griegos **tiene implicaciones más allá del pago de la deuda o de la aceptación de las condiciones de la Troika**, señala Victor de Currea Lugo.

El referendo en Grecia tiene **alcances tanto políticos, como económicos**. Por una parte obligó a la Unión Europea a quitarse la desgastada máscara de ser la Unión de los Pueblos de Europa, con los últimos acontecimientos el interés financiero ha sacado la cara y se ha puesto por delante del interés y las propias decisiones de los pueblos. Pero **Grecia también ha ganado un estatus político y de dignidad frente a las corporaciones financieras.**

De Currea, afirma que el referendo griego es muy simbólico, porque en Atenas nació la democracia que ahora es víctima de censura, por ello, la “*quitada de máscara*” de la Unión Europea es muy importante, puesto que se decía que la UE era fundada para unir a los pueblos de europa y no como una apuesta de inversión o de empresa. “*las ganancias son privadas pero las deudas son públicas*” concreta el investigador.

Un ejemplo de ello está en que aproximadamente el **10% del dinero de los planes de salvamento de la Troika es el que llega a los ciudadanos**, el resto es para pagar las mismas deudas que se tienen con el BM, el BCE o el FMI, “es decir, le presto plata para que me pague lo que me debe” pero se deben generar ajustes en los rubros de inversión social.

Según Currea, en ese sentido la **opción que asume el primer ministro griego es el más digno y acertado**, porque le pregunta a la gente si quieren asumir la deuda con esos mayores ajustes. El problema para la Unión Europea es que no sabe cómo responder a una opción democrática. Lo que  preocupa a la UE también radica en el ejemplo que está dando Grecia y que ya realizó Islandia, que ya **demostró que no pagar la deuda no es igual al apocalipsis, se puede no pagar y no morir.**

Otra de las ganancias radica en que Grecia, como Islandia **está demostrando que es posible pasar de la movilización social a la movilización política y ganar**. Esta característica, que asumen otras expresiones políticas no solo en Europa sino en América Latina, es una de las que resalta De Currea puesto que el peso simbólico de lo que logra Grecia puede abrir una ventana por la que pasen muchas otras expresiones democráticas o de inconformidad.
