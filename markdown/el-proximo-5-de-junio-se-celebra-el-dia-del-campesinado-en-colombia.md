Title: El próximo 5 de Junio se celebra el día del campesinado en Colombia
Date: 2015-06-03 08:00
Category: Otra Mirada
Tags: Bogotá, Boyacá, campesinado, colombia, Comité de Interlocución Campesino y Comunal, Corabastos, Cundinamarca, Eberto Enrique Díaz, Meta, Tolima
Slug: el-proximo-5-de-junio-se-celebra-el-dia-del-campesinado-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/mercados-campesinos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mercadoscampesinos 

###### [3 Jun 2015] 

En el marco de la celebración del día del **campesinado** que se llevará a cabo el 5 de Junio, el **Comité de Interlocución Campesino y Comunal** “CICC”,  y la Alianza Campesina y Comunal **“ALCAMPO”**, convocaron al conversatorio “Aporte del proceso Mercados Campesinos al Abastecimiento Alimentario en Bogotá” con el fin de afianzar las relaciones interinstitucionales entre la población campesina y  la urbana.

En términos legales, los **mercados campesinos** están basados en el **Decreto 315** de 2006 en el cual se adopta el Maestro de abastecimiento de alimentos y seguridad alimentaria para Bogotá en el que se aclara que la economía campesina constituye una fortaleza puesto que aportan alrededor del **65%** de los alimentos de la canasta básica bogotana.

Dentro del conversatorio se trataron temas de importancia para los mercados campesinos, entre ellos, la problemática más importante que se mencionó es que el gobierno nacional pretende vender a **Corabastos** y las plazas de mercado, en ese caso, si la economía campesina se acaba, la alimentación de las ciudades sufrirá drásticas consecuencias, lo que significaría “el campo sin campesino”.

Los temas tratados estuvieron centrados en los departamentos aledaños, **Cundinamarca, Boyacá, Tolima y Meta**, por la ubicación geográfica de Bogotá, **Eberto Enrique Díaz** del **CICC** expresó que la Unidad de campesinos es fundamental y que ellos son quienes contribuyen al desarrollo del país, aportan alimentos frescos por traerlos desde distancias cortas y ayudan con la economía de los colombianos al ofrecer alimentos sin cobrar el IVA.

La unidad de campesinos exige que sus problemáticas sean tenidas en cuenta por el gobierno, por medio de la formulación e implementación de políticas públicas adecuadas, expresan su inconformidad con las empresas privadas que no los apoyan por ser competencia y aseguran que los organismos no comprenden la magnitud de la labor que realizan.

Los campesinos instalarán sus lugares de trabajo desde la media noche del **5 de Junio** en la **Plaza de Bolívar** para celebrar el día del campesinado en Colombia.

[![Campesinado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Campesinado.jpg){.aligncenter .size-full .wp-image-9574 width="449" height="631"}](https://archivo.contagioradio.com/el-proximo-5-de-junio-se-celebra-el-dia-del-campesinado-en-colombia/campesinado/)

 
