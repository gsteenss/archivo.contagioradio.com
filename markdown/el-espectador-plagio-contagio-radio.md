Title: Redacción judicial de El Espectador plagió a Contagio Radio
Date: 2017-09-06 11:59
Category: Otra Mirada
Tags: plagio el espectador
Slug: el-espectador-plagio-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/plagio-el-espectador2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [6 Sept 2017] 

El pasado 30 de agosto Contagio Radio realizó una nota sobre el último  informe del Alto Comisionado de las Naciones Unidas para los Refugiados (ACNUR) sobre el pacífico colombiano, el título del artículo fue "[Tres causas de la crisis humanitaria en el Pacífico según la ACNUR](https://archivo.contagioradio.com/tres-causas-de-la-crisis-humanitaria-en-en-pacifico-segun-la-acnur/)", luego el 3 de septiembre la Redacción Judicial del segundo diario más leído en el país, El Espectador, publicó una nota con el mismo titular en su edición digital  "[Tres causas de la crisis humanitaria en el pacífico según la Acnur](http://www.elespectador.com/noticias/judicial/tres-causas-de-la-crisis-humanitaria-en-el-pacifico-segun-la-acnur-articulo-711381)".

Pero el plagio no fue solo del título sino la nota original sin subtítulos con algunos cambios mínimos, que provenía de una entrevista realizada por Contagio Radio a Rocío Castañeda, oficial de información de la ACNUR  en nuestro programa Otra Mirada, a la que la redacción de ese medio le quitó algunos párrafos y cambió algunas palabras.

![plagio-el-espectador](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/plagio-el-espectador1.png){.alignnone .size-full .wp-image-46215 width="835" height="1727"}

Lamentamos que uno de los diarios más reconocidos del país haga esto. Como medio  de comunicación no tenemos problema alguno en que utilicen nuestra información para hacer su trabajo informativo, pero reprochamos la falta de ética periodística al no citar el lugar del que es tomada la información. Esperamos que la dirección de **El Espectador, en cabeza de Fidel Cano, y la editora de esta sección, Diana Durán,** tomen acciones sobre este caso, emitan en su página una disculpa pública y eviten que se sigan presentado este tipo de prácticas.

La nota también fue publicada en redes sociales del medio de comunicación.

![Pantallazo Fcebook El Espectador](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-06-at-9.55.50-AM.jpeg){.alignnone .wp-image-46274 .aligncenter width="425" height="542"}

Anexamos la carta dirigida al diario El Espectador

[Carta El Espectador](https://www.scribd.com/document/358188244/Carta-El-Espectador#from_embed "View Carta El Espectador on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_32588" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/358188244/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-1A5MvqMDfqUH0uT6lNI3&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
