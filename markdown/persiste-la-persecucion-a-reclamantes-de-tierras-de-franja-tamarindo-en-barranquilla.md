Title: Persiste la persecución a reclamantes de tierras de franja "Tamarindo" en Barranquilla
Date: 2015-09-10 15:19
Author: AdminContagio
Category: DDHH, Nacional, Resistencias
Tags: Amenazas, Asotracampo, El tamarindo, Reclamantes de tierra, Restitución de tierras, víctimas del desplazamiento
Slug: persiste-la-persecucion-a-reclamantes-de-tierras-de-franja-tamarindo-en-barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Tamarindo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagiordio.com 

<iframe src="http://www.ivoox.com/player_ek_8300087_2_1.html?data=mZidkpWce46ZmKiakpuJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMbm1M7g1sqPsMKf0crf1crHucTdhqigh6eXsozVjNfSxdHFscLi1crgjcnJb9Xdxtffw9iPqMafx9eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juán Martínez García] 

###### [10 sep 2015]

Juán Martínez García, reclamante de tierras y representante legal de Asotracampo, denunció que fue víctima de hostigamientos y seguimientos en dos ocasiones durante la primera semana de Septiembre. Uno de los sujetos realizó registros fotográficos del lider comunitario. Hasta ahora, distintos integrantes de la comunidad han recibido cerca de 40 amenazas sin que haya respuesta a las denuncias formuladas.

Juán Martínez denuncia que los intereses particulares de las familias Char, Abdala Saieh, y Muvdi además de Inversiones Agropecuaria S.A.S  buscan sacarlos de los predios que ocupan hace 12 años luego de varios desplazamientos forzados al punto que “solo queda una franja de terreno de espacio humanitario”, indica el representante de Asotracampo.

Martínez hace nuevamente un llamado pidiendo que “**nos reubiquen para nuestro proyecto de vida**, somos víctimas del conflicto armado y requerimos una solución pronta del Estado”.
