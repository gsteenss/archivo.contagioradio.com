Title: Otras denuncias por las que se debe investigar a Rodolfo Palomino
Date: 2016-02-17 17:03
Category: Judicial, Nacional
Tags: corrupción, Policía Nacional, red de prostitución masculina, Rodolfo Palomino
Slug: es-decepcionante-que-pueda-mas-el-morbo-que-las-denuncias-muy-serias-y-solidas-que-hay-hace-muchos-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Palomino-e1455746608883.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Zona Cero 

<iframe src="http://www.ivoox.com/player_ek_10473849_2_1.html?data=kpWhmZiceJqhhpywj5WaaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncYamk6rgjcnJp8bkxM7c0MbSuMaf0trSjdXZqcXVjNKSpZiJhZLnjMrZjdLTtsPjjNbix5DQpdSfxcrb19ORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Claudia López, Alianza Verde] 

###### [17 Feb 2016]

Esta mañana el general Rodolfo Palomino, presentó su renuncia debido a la investigación que adelanta en su contra la Procuraduría General de la Nación, por incremento injustificado en su patrimonio, seguimientos a periodistas y conformación de una red de prostitución en la Institución.

Durante una rueda de prensa en la mañana el general anunció, “He tomado la decisión, en el seno de mi hogar, junto al cuerpo de generales y de cara al país, de pedirle al señor presidente que me aparte del cargo como director general de la Policía Nacional. Estoy en los próximos minutos presentando mi solicitud de retiro, sabiendo de mi absoluta inocencia frente a los cargos que se me imputan”.

La renuncia ya fue aceptada por parte del gobierno nacional, como lo anunció el ministro de defensa, Luis Carlos Villegas. Ahora el general Jorge Eduardo Nieto, asume el puesto como comandante de la Policía Nacional.

“**Esa era la decisión acertada, desafortunadamente se demoró mucho y le ha causado un enorme daño a la policía como institución”**, dijo Claudia López, senadora de la alianza Verde, quien añade, “Quien más ha fallado es el presidente de la República, que es el comandante  la fuerza pública y a quien le correspondía tener el liderazgo de hacer unas investigaciones serias y rigurosas, tomar las medidas a tiempo y no lo hizo”.

### Denuncias contra Rodolfo Palomino no son nuevas 

La Senadora asegura que el video con contenido sexual en el que sale involucrado Carlos Ferro, viceministro del interior, “no era clave ni determinante, es decepcionante que pueda más el morbo que las denuncias muy serias y sólidas que hay hace muchos años”.

De acuerdo con la congresista, hace varios años se habían presentado “denuncias muy serias”, en las que varios policías habían afirmado ser extorsionados y  sometidos para prestar servicios en esta red de prostitución, así mismo, se refiere a las denuncias que ya han presentado los periodistas sobre enriquecimiento ilícito.

López, lleva más de dos años denunciando al general por abuso de poder y corrupción, entre estas denuncias se evidencia **el tráfico de combustible desde Venezuela al interior de ciudades como Bogotá y Medellín,** "gracias a una actitud de complacencia por parte de Palomino.¿".

Así mismo, la senadora asegura que el general **“protegía a policías corruptos que trabajaban en la Guajira y el Cesar que trabajaban con Kiko Gómez”** y recuerda que en los medios  de comunicación se pudo evidenciar “cómo miembros de la policía sacaron a patadas a agentes de la Fiscalía cuando fueron a atrapar al señor Kiko Gómez”, tras las denuncias, Claudia López señala que **Palomino lo que hizo fue ascenderlos y trasladarlos, y nunca investigarlos.**

**“A este punto no solo se necesita cambiar al comandante, se necesitan controles  institucionales efectivos  y externos a la policía”, dice la congresista,** y añade que la comisión anunciada hace dos meses para investigar los escándalos al interior de esta institución, se trata de una estrategia de dilación para no tomar decisiones serias.
