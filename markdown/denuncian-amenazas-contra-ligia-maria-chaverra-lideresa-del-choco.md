Title: Denuncian amenazas contra Ligia Maria Chaverra, lideresa del Chocó
Date: 2017-12-08 14:50
Category: DDHH, Nacional
Tags: Curvarado, María Chaverra
Slug: denuncian-amenazas-contra-ligia-maria-chaverra-lideresa-del-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/mural-ligia-maria-chaverra6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Contagio Radio] 

###### [07 Dic 2017]

Al asesinato del líder social, se suma la amenaza de la cual fue víctima la lideresa social **María Ligia Chaverra** en el territorio colectivo de Curvaradó en Riosucio, Chocó. Las comunidades denunciaron la presencia de un grupo armado que porta prendas militares y tiene armas de largo alcance al parecer integrantes de las AGC. (Le puede interesar: ["Colombia entre los 4 países más letales para los defensores de DDHH"](https://archivo.contagioradio.com/colombia-4-paises-letales-defensores-de-ddhh/))

Los hombres armados se instalaron en el punto que comunica a los caseríos de Llano Rico y Apartadorcito **“ejerciendo restricción al ingreso y salida** de las personas del consejo comunitario”. Las comunidades indicaron que se encuentran en estado de zozobra por la fuerte presión que está ejerciendo las AGC y por la ausencia de presencia del Estado.

De acuerdo con la información, **la lideresa había sido intimidada el 4 de diciembre** cuando, hacia las 4:30 pm, “la recepcionista del kiosco Vive Digital, ubicado dentro de la Zona Humanitaria de Camelias, territorio colectivo de Curvaradó, un sujeto sin identificarse, hizo una llamada en donde expresó que debían avisar a la comunidad que ya sabían quién es la líder”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
