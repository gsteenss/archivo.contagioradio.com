Title: Roban información de Corporación Nuevo Arco Iris relacionada con conflicto armado
Date: 2019-03-19 12:37
Author: CtgAdm
Category: DDHH, Nacional
Tags: Derechos Humanos, Memoria Colectiva, parapolítica
Slug: por-tercera-vez-corporacion-nuevo-arco-iris
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 19 Mar 2019 

La sede de la **Corporación Nuevo Arco Iris** en Bogotá, organización promotora de iniciativas en relación con la superación del conflicto armado, en pro de la paz y el post-conflicto, fue saqueada en la madrugada del 18 de marzo. Según la denuncia durante el hurto desaparecieron varios equipos que contenían información valiosa concerniente a las investigaciones adelantadas por la organización. **Desde 2008 esta es la tercera vez que la Corporación es víctima de robos. **

**Fernando Cuervo, director de la organización,** explicó que en la lista de elementos hurtados se encuentran 11 CPU, 10 monitores, y 2 discos duros que contenían investigaciones sobre el conflicto armado, incluyendo varias sobre parapolítica, actores armados sucesores del paramilitarismo, documentos sobre memoria histórica, reparación colectiva e información administrativa y contable.

Cuervo desconoce la identidad de los autores materiales o intelectuales del crimen, sin embargo señala que podría tratarse de "servicios de inteligencia o personas que quieren desestabilizar el proceso de paz por el que está luchando el país"  además agrega que** se trata de una nueva amenaza contra los defensores de DD.HH.** y la labor desarrollada durante los 25 años de trabajo de la Institución.

Aunque el caso ya fue llevado a la Fiscalía, cabe resaltar que el ente de control no ha presentado avances en la investigación de los robos anteriores ni en las denuncias que realizó la Corporación **en 2013 o 2014 cuando recibieron amenazas de sufrir un atentado con un carro bomba, ni la correspondiente a 2018 cuando fueron amenazados por las denominadas Águilas Negras. **

<iframe id="audio_33513052" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33513052_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
