Title: 200 millones de Mujeres en el mundo han sido víctimas de ablación
Date: 2017-02-07 08:50
Category: DDHH, Mujer
Tags: Ablación, Mutilación Genital Femenina, Tolerancia Cero con la Ablación
Slug: 200-millones-de-mujeres-en-el-mundo-han-sido-victimas-de-ablacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ablación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kenia] 

###### [6 Feb 2017] 

Más de 200 millones de mujeres y niñas en todo el mundo han sido víctimas de la práctica de Mutilación Genital Femenina –MGF– o más conocida como ablación, cada 6 de febrero se conmemora el Día Internacional de Tolerancia Cero contra la Mutilación Genital Femenina. Según la ONU esta práctica **viola los derechos de las mujeres a la salud, la seguridad, la integridad física, el derecho a no ser sometido a torturas y tratos crueles**, inhumanos o degradantes, y el derecho a la vida.

Según la representante de Naciones Unidas Federica Mogherini, **la ablación es "un acto de violencia contra las mujeres y niñas"**, que se concentra en 30 países de África, América, Oriente Medio y Asia.

### ¿Por qué continúa esta práctica aún en el siglo XXI? 

Expertos afirman que los motivos argumentados para su aplicación son mayoritariamente culturales, como la **“eliminación del placer femenino, la manifestación de desconfianza hacia la naturaleza de las mujeres y prevenir la tendencia a la promiscuidad”.**

Los últimos datos recogidos por la ONU, indican que los países con mayor prevalencia de MGF, en mujeres de los 15 a los 49 años son Somalia con 98%, Guinea 97% y Yibuti 93%. Los mismos estudios, indican que **en América Latina, los países donde se practica la ablación son Brasil, Colombia, México y Perú.**

La Asociación Flor de África, señaló que entre las complicaciones físicas más comunes están el **dolor agudo, sangrado severo, problemas urinarios, menstruales, cistitis e infecciones**, y a nivel psicológico provoca **depresión, ansiedad, desórdenes postraumáticos y baja estima.**

La misma asociación reveló que durante 2016, hubo 72 condenas en los 30 países donde prevalece la práctica y varias de ellas **“involucraron a personal médico que aceptó realizar las intervenciones y en otros, a familiares”,** aún siendo prohibidas constitucionalmente.

Por otra parte, distintas organizaciones de derechos humanos que han trabajado en mitigar la realización de dicha práctica, manifiestan que la solución de este problema no pasa por condenar a padres o abuelos de las víctimas, sino se trata de **“convencerles de erradicar esta práctica” a través de estrategias educativas y de sensibilización.**

Distintas organizaciones de mujeres, feministas, defensoras de derechos humanos y de la niñez, manifestaron en varios lugares del mundo, su rechazo rotundo a esta práctica e invitaron a la sociedad en general a **exigir en sus países la prohibición y condena de la Mutilación Genital Femenina.**

Hasta la fecha, sólo estos países han prohibido constitucionalmente esta práctica: Egipto, Gambia, Mauritania, Guinea-Bissau, Eritrea, Somalia, Senegal, Kenia, Burkina Faso, Uganda, Etiopía.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
