Title: Hoy cumplía años un defensor de derechos humanos asesinado en Medellín
Date: 2016-03-18 10:06
Category: Fernando Q, Opinion
Tags: defensor de derechos humanos, Juan David Quintana, Medellin
Slug: hoy-cumplia-anos-un-defensor-de-derechos-humanos-asesinado-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Juan-David-Quintana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Imagen de Juan David Quintana Duque, defensor de Derechos Humanos, asesinado en Medellín. Foto cortesía 

#### **[Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano) ** 

###### 18 Mar 2016 

[El 27 de mayo de 2015, fue asesinado Juan David Quintana Duque en el barrio Popular II. Era un defensor de Derechos Humanos y participaba de la Mesa de Derechos Humanos Valle de Aburrá, espacio del cual fui uno de sus fundadores. También  lideraba el Núcleo del Pensamiento y laboraba en la red de Bibliotecas de Medellín. Además, trabajaba por los derechos humanos en la comuna 6 (Doce de Octubre) de la ciudad. Antes de morir, estaba preocupado por los tejemanejes que se realizaban en el Presupuesto Participativo, al parecer, sabía que algunos líderes y organizaciones podrían estar favoreciendo los intereses de políticos y criminales a través de proyectos, o el cobro de la vacuna según el monto del contrato, o permitiendo el pagadiario para pagar las obligaciones debido a las demoras que la Administración Municipal tenía.]

[Era incansable, soñador y luchador. Algunos dirán: “es que no hay muerto malo”; pues sí lo hay: Juan David no era malo, era un hombre bueno, empeñado en transformar la comuna seis y aportar al cambio de la Medellín innovadora, la que tiene el 70% de su territorio en manos del Pacto del fusil.]

[Más de 20 disparos de una subametralladora acabaron con su vida, sicarios lo interceptaron en un sitio de la ciudad que se supone es tranquilo por el pacto tácito que allí se vive, murió en un sitio en el que existen cuadrantes de la policía permanentes.]

[Hoy, al parecer, no se sabe quiénes fueron los autores materiales e intelectuales de su asesinato. Y eso es lo que suponen sus verdugos, pero pronto se les acabará la fiesta de impunidad que los protege: Análisis Urbano y la ONG Corpades adelantan una investigación que abriría luces a este caso y demostraría que su muerte fue ordenada desde otra comuna. Los sicarios salieron del Doce y no de la estructura criminal de los Triana quienes son los que operan en la zona donde cayó asesinado, ellos pudieron abrir el campo para que se perpetrara el asesinato pero no participaron.]

[Algunos desde la institucionalidad, el bajo mundo e incluso algunos líderes y  organizaciones sociales se empeñaron en dañar su imagen de líder carismático; los últimos, ingenua o estúpidamente, se tragaron el cuento que la Policía y la Alcaldía propagaron, que Juan David era una persona mala y con antecedentes penales en Medellín y otras ciudades de Colombia, ciudades que ni siquiera conoció.]

[La estrategia de estigmatizar, señalar, perseguir, judicializar es muy aplicada por el establecimiento para evitar que le hagan contrapeso o cuestionen sus malas actuaciones y decisiones políticas, claro está que también la usan ciertos personajes que se ocultan en la piel de demócratas y defensores de Derechos Humanos.]

[Casos hay muchos: Judith Adriana Vergara, según esos esparcidores de rumores, la mataron por cambiar de novio cada ocho días; Haider Ramírez, “asesinado por deudas”; Ana Fabricia Córdoba, “le quitaron la vida por puta”; a Luis Fernando Wolf “lo mataron por robarle”; y Viviana Agudelo, representante de la JAL del corregimiento San Antonio de Prado, “fue asesinada por problemas pasionales y por el cobro de un seguro de vida”. Rumores, rumores en la sociedad que a pie de letra copia lo que dicta la voz oficial.]

[Hoy cumplía años Juan David Quintana Duque, hoy llora una familia su pronta partida; hoy se agazapan en el nuevo gobierno municipal, en cabeza Federico Gutiérrez, algunos que no escucharon su clamor, y otros se esconden en la Policía Metropolitana del Valle de Aburrá. “Estoy amenazado, me van a matar”, dijo. Nadie lo protegió y lo mataron.]

[Este proceso no debe quedar en la impunidad y sus asesinos deben sancionarse con todo el peso que pueda imprimir la Ley. Y también deben pagar quienes por omisión desde el Estado, lo dejaron morir.]

[Hoy ninguno de la administración se pronunció y eso sólo aclara algo:][[Medellín asesina a sus líderes; ahora quiere olvidarlos]](http://analisisurbano.com/?p=16830)[.]

------------------------------------------------------------------------

**Píldora urbana:**[ Tres defensores de derechos humanos, Juan David Quintana Duque entre ellos, fueron asesinados en Medellín durante el 2015. Y a finales de febrero y en las dos semanas que van de marzo, siete más han sido asesinados. Cuál es  la paz que ofrece el gobierno de Santos: ¿Paz en su tumba?]
