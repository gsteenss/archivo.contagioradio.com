Title: Eutanasia y libre albedrío en un supuesto Estado laico
Date: 2015-04-22 07:17
Author: CtgAdm
Category: Carolina, Opinion
Slug: eutanasia-y-libre-albedrio-en-un-supuesto-estado-laico
Status: published

#### Por [[**Carolina Garzón Díaz**](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ](@E\_vinna) 

Cuando le preguntamos a las autoridades de la Iglesia o a nuestros familiares ¿por qué Dios no nos obliga a hacer lo que él cree que está bien o con su infinito poder nos hace amarlo? Ellos siempre responden: es por el libre albedrío. Aún en mi vida cotidiana me pregunto por esta supuesta libertad que tiene más cara de trampa, porque aunque tengo la posibilidad de ir contra lo que Dios quiere, siempre las consecuencias de mi desobediencia serán fatales.

En la coyuntura actual, luego que el Ministerio de Salud en Colombia dio a conocer apartes de la resolución que regulará la eutanasia en el país en cumplimiento de la sentencia T-970 de 2014 de la Corte Constitucional, ya se han generado distintas reacciones, todas muy respetables. Sin embargo, las exigencias de la Iglesia Católica y el Procurador Alejandro Ordóñez han dejado ver el tinte autoritario que tienen las camándulas en un supuesto Estado laico y se han consolidado en acciones que buscan interferir en contra del derecho a morir dignamente.

Por parte del Episcopado Colombiano el padre Pedro Mercado envió una carta Ministro de Salud, dónde asegura que a este Ministerio no le compete la reglamentación de la eutanasia porque no existe una ley del Congreso sobre esa materia y respalda su afirmación en los pronunciamientos que tiene al respecto el Consejo de Estado. En el mismo sentido, el Procurador Ordóñez, fue más allá de su cargo y basado en sus creencias personales envió una carta perentoria a Alejandro Gaviria, ministro de Salud,[ ]{.Apple-converted-space}para que detenga cualquier el proceso administrativo que regule la eutanasia. Recordemos, además, que contra la Sentencia T-970 de 2014 de la Corte Constitucional, que ordenó la expedición del protocolo para regular la eutanasia cursa un incidente de nulidad formulado por Ordóñez.

Sin embargo, es de aclarar que estas actuaciones del Episcopado y del Procurador están en contravía de los derechos de los ciudadanos colombianos que no profesan la religión católica u otra doctrina y están de acuerdo con la eutanasia como una alternativa de muerte digna en un momento decisivo.

En este punto me pregunto dos cosas ¿acaso no estábamos en un Estado laico? ¿Acaso los creyentes no tenemos el libre albedrío? Para mi primera respuesta, creo que Colombia es realmente un país diverso: según la Encuesta Barómetro de las Américas el 58% de su población es católica, el 30% evangélica y pentecostal, el 2.6% protestante, el 1.8% Mormón, Testigo de Jehová, Espiritualista y Adventista del Séptimo Día, otro 1% son Judíos, Musulmanes, Budistas, Hinduistas, Taoistas y más del 7% no pertenecen a ninguna religión o son ateos. Y a todos ellos y ellos se les debe garantizar por ley la posibilidad de elegir, de acuerdo a su religión y/o sus creencias de vida, sí desean o no recurrir a la eutanasia; y que, además, quienes lo hagan, tengan todas las garantías que brinda la ley.

Finalmente, respecto a mi segunda pregunta, ¿Acaso los creyentes no tenemos el libre albedrío?, creo que la Iglesia católica y especialmente el Episcopado debe empezar a aprender de Dios y otorgarle a sus fieles el Libre albedrío. Lastimosamente la educación de una sociedad con pensamiento crítico y autonomía nunca ha estado entre los intereses de los jerarcas de la Iglesia católica y en eso coinciden con el Estado colombiano.
