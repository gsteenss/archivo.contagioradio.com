Title: José Arquímedes Moreno, tercer líder social asesinado en el Catatumbo en 2019
Date: 2019-02-11 11:34
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Catatumbo
Slug: lider-social-jose-arquimedes-moreno-fue-asesinajose-arquimedes-moreno-tercer-lider-social-asesinado-en-el-catatumbo-en-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Líder-Social-José-Arquímedes-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @Otohiguita 

###### 11 Feb 2019

El pasado 10 de febrero en Tibú, Norte de Santander,  fue asesinado el líder comunal José Arquímedes Moreno, quien según la Asociación Campesina del Catatumbo (ASCAMCAT),  trabajaba como parte del Comité de integración social del sector "Río de oro" en cercanías a la frontera con Venezuela; con su muerte, **José se convierte en el tercer dirigente asesinado en esta región y el número 19 en lo que va corrido del 2019 en toda Colombia.**

Según información recolectada por los autoridades, el líder fue atacado por dos hombres armados que se desplazaban en moto y dispararon contra la humanidad de Moreno mientras se desplazaba hacia su casa en  la vereda **T-25 los Patios**, posteriormente los desconocidos huyeron del lugar.

Aunque aún se desconocen los autores intelectuales y materiales, este es un departamento en el que hay presencia de diferentes grupos armados como el ELN, el EPL, disidencias de las Farc y diversas bandas de narcotráfico. [(Le puede interesar: Asesinan a Luis Contreras, líder de sustitución de cultivos en el Catatumbo) ](https://archivo.contagioradio.com/asesinan-luis-contreras-promotor-sustitucion-de-cultivos-catatumbo/)

A consideración de las Naciones Unidas, los municipios de **Tibú, Hacarí, San Calixto, El Tarra, Convención y Teorama** son algunos de los territorios que representan mayor riesgo para los líderes sociales en el país, una alerta que se contrasta con las cifras de la Defensoría del Pueblo, la cual informó que existen 400 amenazas a líderes sociales, indígenas y campesinos en Norte de Santander.

###### Reciba toda la información de Contagio Radio en [[su correo]
