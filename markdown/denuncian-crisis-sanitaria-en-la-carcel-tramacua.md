Title: Denuncian crisis sanitaria en la carcel Tramacua
Date: 2016-04-04 17:57
Category: DDHH, Nacional
Tags: cárcel la tramacua, crisis carcelaria, Equipo Jurídico Pueblos
Slug: denuncian-crisis-sanitaria-en-la-carcel-tramacua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/tramacua-3-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: defensoria] 

###### [04 Abril 2016] 

Desde el pasado sábado el INPEC no suministra agua potable suficiente para satisfacer las necesidades de las personas privadas de la libertad en ese establecimiento carcelario. Según la denuncia el sábado 2 y el domingo 3 de abril[solamente se han abierto los registros del agua durante 10 minutos](https://archivo.contagioradio.com/cierre-de-carcel-la-tramacua-de-valledupar-seria-un-acto-de-humanidad/), tiempo que es **insuficiente para abastecer las necesidades de los 1600 internos para los que esa cárcel tiene cupo.**

Ante el llamado de los internos a los encargados del INPEC, han denunciado que la respuesta es un compromiso que no se cumple. El escrito relata que le manifestaron al Sargento Lucio, que el agua era insuficiente, ante lo cual no hubo respuesta efectiva. Además miembros del Instituto Nacional Penitenciario y Carcelario han afirmado que la situación con el agua **“es solo el principio” como si se estuviera volviendo a los tiempos de la guerra por el agua.**

La situación es tan crítica que los internos de la Tramacua han hecho un llamado a la comunidad habitante de Valledupar para que les donen agua para tomar y a las empresas privadas del país para que hagan donaciones para el consumo.

Adicionalmente solicitaron al INPEC y a los organismos de control para que se permita el uso de los ventiladores, que ya fueron autorizados, al interior de las celdas, puesto que la temperatura asciende hasta los 35 grados, situación que se hace insoportable, aún más cuando no hay agua potable para el consumo en los horarios de más calor.

**Esta situación se suma a las múltiples denuncias por hacinamiento, malos tratos y condiciones indignas de vida** que desde hace más de dos años se vienen denunciando y que no han tenido respuestas efectivas por parte del INPEC. Desde el mes de Enero se esperaba que la Corte Constitucional ordenara el cierre del establecimiento, pero el tribunal de Cesar frenó el cierre creyendo en las ["buenas intensiones" de las autoridades de la carcel.](https://archivo.contagioradio.com/miles-de-personas-en-colombia-se-manifiestan-por-el-cierre-de-la-carcel-tramacua/)
