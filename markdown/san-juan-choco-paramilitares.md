Title: Confinadas comunidades indígenas y afros en el Litoral de San Juan Chocó
Date: 2017-04-05 12:25
Category: Comunidad, Otra Mirada
Tags: AGC, Chocó, confinamiento, paramilitares
Slug: san-juan-choco-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/cms-image-000012078.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 360radio Colombia 

###### 5 de Abr 2017 

Un mes completan las comunidades de Los esteros, Las bocanas y Quebradas, en el Municipio Litoral del San Juan en Choco, bajo el control de grupos autodenominados como los Urabeños y Autodefensas Gaitanistas de Colombia, ocasionados según los pobladores por los enfrentamientos de  grupos armados tras la salida de las FARC-EP de esos territorios.

Un integrante de las comunidades asegura que **las familias han estado obligadas al confinamiento puesto que en el territorio no pueden realizar sus prácticas tradicionales como caserías, trabajo y pesca** "A uno le dicen no se meta por allí, porque por ahí hay nuestra gente y no respondemos, eso se convierte en una amenaza para la gente".

"Es la única vía de transporte fluviales que salen y entran la población a la cabecera municipal del Litoral del San Juan a llevar enfermos y traer los víveres para el abastecimientos de cientos de** personas entre niños, mujeres, hombres y adultos mayores que viven en estas orillas**", asegura un miembro de la comunidad.

Ante al actual situación **muchos de los pobladores no se atreven a decir algo o a denunciar lo que vienen pasando** a diario con el flagelo de los grupos que tienen azotada la población, por el temor a ser amenazados o asesinados por los armados. Le puede interesar: [600 personas del Río Truandó en Chocó fueron desplazadas por presencia paramilitar.](https://archivo.contagioradio.com/38661/)

Lo que claman los habitantes al Gobierno Nacional, es a prestar atención a los llamados de la comunidad por que "**esos grupos andan rondando nuestros territorios y amenazando a nuestros líderes, lo queremos es tranquilidad y paz en nuestros territorios**".

------------------------------------------------------------------------

###### *![bernardino](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/bernardino-150x150.jpg){.wp-image-27947 .size-thumbnail .alignleft width="150" height="150"}Reportaje elaborado por Bernardino Dura Ismare, Comunicador de CONPAZ,   de la comunidad de Pichimá Quebrada*

###### Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realiza una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras. 
