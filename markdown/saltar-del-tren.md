Title: Saltar del tren
Date: 2018-03-07 08:45
Category: Eleuterio, Opinion
Tags: ambientalistas, Berta Cáceres, honduras
Slug: saltar-del-tren
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/tren-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **Por [Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 7 Mar 2018 

**Gustavo Castro es un activista mexicano** defensor de derechos humanos. En marzo de 2016 fue invitado por el COPIHN en Honduras para dar unos talleres de formación a sus miembros. Durante su estancia se hospedó en casa de [**Berta Cáceres**](https://archivo.contagioradio.com/el-viaje-de-berta/). La noche en que la asesinaron también le dispararon a él, lo dieron por muerto pero sólo estaba herido. Lo que aquí recogemos es un relato a modo de metáfora que nos dejó en su conversación durante un encuentro con activistas medioambientales y defensores de derechos humanos.

*[La humanidad entera viaja toda en el mismo tren. Un tren enorme, rápido, que sigue su camino con determinación. En este tren hay vagones de primera, segunda, tercera y hasta de cuarta clase. Incluso hay, y no son pocos, quienes viajan agarrados como pueden por fuera o trepados al techo de los vagones.]*

*[Son estos quienes primero advierten que apenas a unos 500 metros, hay en la vía un socavón enorme hacia el que el tren se dirige a toda velocidad. De continuar así, se precipitarán inevitablemente hacia el abismo. Son pues, los más pobres, los viajeros marginados, quienes dan la voz de alarma.]*

*[Ante el desastre inminente la gente se apresura a movilizarse ententa dar soluciones. Hay que avisar al maquinista, decirle que se pare, que detenga el tren. Pero eso es imposible, para llegar hasta el maquinista hay que atravesar todos los vagones, ir pidiendo permisos para pasar de uno a otro. Además, el maquinista es un mandado, hay que rellenar solicitudes, hacer requerimientos para hablar con quienes dan las ordenes y lograr que manden al maquinista detener el tren. Demasiado complejo, ya no llegamos.]*

*[ Hay quien arguye que no puede ser decisión de unos pocos el tomar una resolución tan importante y que repercute en todos, en la humanidad entera. Es necesario comenzar a convocar asambleas en cada vagón para que la gente debata, opine y tome resoluciones que puedan después ser puestas en común en una asamblea general por los representantes de cada vagón. ¡Uy, no! Pues ya quedan 400 metros, tampoco nos da para organizar todo eso.]*

*[Siguen las propuestas y las discusiones a penas a 300 metros para llegar al gran socavón que pondrá fin a la historia del tren de la humanidad. El problema es que hay que informar a la gente de lo que está pasando para poder revertir la situación. Hay quienes nunca han salido de su vagón, ni se han levantado de su asiento, ni si quiera han mirado por la ventanilla. Podemos montar antenas encada vagón y emitir por radio contando lo que sucede, la gente reaccionará. Aunque reaccionaran, no queda tiempo para todo eso.]*

*[Pues cambiemos el combustible del tren para que no contamine tanto, para que vaya más lento, reduzca la velocidad y nos dé tiempo a pensar mejor las cosas. Pero es igual, aunque vaya más lento, la dirección es la misma y vamos derechitos al precipicio.]*

*[A penas queda ya tiempo, unos 100 metros para el fatal desenlace… ¿Y si probamos parar el tren tratando de frenarlo con nuestros propios pies? Pues la mitad vamos a perder la pierna sin lograr nada. Lo que hay que hacer es tomar el control por la fuerza, llegar hasta el maquinista, sacarlo y ponernos a los mandos nosotros. Pero es que somos gente pacífica, no queremos causar violencia contra nadie, defendemos los derechos humanos para todas las personas. No, eso tampoco puede ser.]*

*[No se engañen, ya no queda de otra. Hay que salirse de este sistema, saltar del tren en marcha, aunque no todos sobrevivan a la caída… como le pasó a Bertita…]*

#### [**Leer columnas de opinión de  Eleuterio Gabón **](https://archivo.contagioradio.com/eleuterio-gabon/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
