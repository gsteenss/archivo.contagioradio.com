Title: Luego de 100 años Turquía continua negando el genocidio armenio
Date: 2015-02-03 18:58
Author: CtgAdm
Category: DDHH, El mundo
Tags: conflicto armenio-azarí, Genocidio armenio, Turquía niega el genocidio armenio
Slug: luego-de-100-anos-turquia-continua-negando-el-genocidio-armenio
Status: published

###### Foto:mdzol.com 

Conocido también como **holocausto armenio, fue perpetrado por el imperio Otomano entre 1915 y 1923 contra población civil armenia**, y es considerado como el "primer genocidio moderno" y quizás el mas estudiado después del Holocausto judío.

La política de exterminio encabezada por el gobierno de"**Los Jóvenes Turcos**" del imperio, también alcanzó a otros grupos étnicos como asirios, serbios, griegos o pónticos.

En total fueron deportados y exterminados una cifra **de un millón y medio a dos millones de armenios**.El 24 de Abril de 1915 es la fecha oficial del inicio del genocidio, cuando comenzaron las detenciones de armenios en la capital Estambul.

**Turquía como sucesora del imperio Otomano, reconoce la masacre pero no el genocidio y tampoco se responsabiliza de lo sucedido, a**duciendo que hace parte del comienzo de la primera guerra mundial y que no se ajusta a la definición de genocidio, en la que incluiría un plan sistemático con la finalidad de exterminar una etnia.

Actualmente pocos estudiosos lo niegan, incluso en Turquía, y gracias a la presión internacional de la comunidad armenia son ya 22 los estados a nivel mundial que lo reconocen.

**El parlamento europeo ha exigido a Turquía el reconocimiento del genocidio**, como una de las condiciones para poder entrar en la UE.

Hoy en día debido a problemas territoriales y a la guerra armenio-azarí, las fronteras entre Turuquía y Armenia están cerradas y las relaciones congeladas, a la espera de una negociación que incluiría el reconocimiento del genocidio.
