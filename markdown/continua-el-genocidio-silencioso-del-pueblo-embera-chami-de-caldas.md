Title: Continúa el genocidio silencioso del pueblo Embera Chamí de Caldas
Date: 2020-06-02 17:08
Author: CtgAdm
Category: Otra Mirada, yoreporto
Tags: Cridec, Emberas, genocidio, MOVICE
Slug: continua-el-genocidio-silencioso-del-pueblo-embera-chami-de-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/GAB2694.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

"*En memoria de todos nuestros muertos,  
quienes nos heredaron la lucha por nuestra dignidad,  
el territorio y la autonomía,  
a quienes la guerra les arrebato sus vidas*"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Por: Movice / Cridec*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Las comunidades indígenas del Pueblo Embera Chamí de Riosucio, Caldas, víctimas de la criminalidad del Estado y de los grupos paramilitares, entregamos ante la Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición el informe ‘El Genocidio Silencioso del Pueblo Embera Chamí de Caldas. Masacres contra el Pueblo Embera Chamí de Riosucio, Caldas’. Este es un acto simbólico y político a través del cual queremos contribuir al esclarecimiento, visibilización y reconocimiento de un genocidio continuado contra nuestras comunidades, frente al cual el Estado no ha tomado medidas efectivas para salvaguardar nuestro [derecho a la vida](https://www.justiciaypazcolombia.com/paramilitares-sostienen-que-les-apoyan-los-militares-en-disputa-con-el-eln/) y a la integridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de este informe retratamos el genocidio y etnocidio que se ha perpetuado contra nuestro Pueblo a través de violaciones sistemáticas a los derechos humanos expresadas en masacres, desapariciones, asesinatos, violencia de género, discursos de estigmatización y doctrinas de homogenización y aculturación, que ponen en vilo nuestra pervivencia física y cultural, y que han tenido como responsables a miembros de  
la Fuerza Pública y grupos paramilitares, quienes a través de sus prácticas de guerra y violación a los derechos humanos y territoriales fracturaron nuestro tejido social, rompieron nuestra relación con el territorio, afectaron nuestra identidad, y tradiciones espirituales y culturales como comunidades indígenas, además de limitar el fortalecimiento de nuestros procesos de organización política y gobierno propio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy reivindicamos que, hechos como las masacres de la Rueda, de la lideresa María Fabiola Largo y de la Herradura, fueron graves violaciones a los derechos humanos e infracciones al derecho internacional humanitario que simbolizan la persecución contra nuestras comunidades, la sistematicidad de los hechos, la impunidad reinante, el despojo de nuestros territorios, la segregación racial, la omisión y responsabilidad del Estado en  
estos crímenes, que hoy en día continúan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Expresamos nuestro rechazo frente a los hechos ocurridos durante el mes de marzo del presente año en el Resguardo de origen Colonial Cañamomo Lomaprieta, donde fueron provocados diferentes incendios que destruyeron trapiches comunitarios, generando miedo y zozobra en la comunidad. Esta situación representa una clara violación a los derechos de las comunidades y la amenaza permanente de repetición de los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hacemos un llamado a las autoridades para que inicien la investigación, entreguen respuestas efectivas a la comunidad frente a la conexión de estos crímenes y se construyan acciones de prevención y protección colectiva del territorio, que permitan continuar los procesos de soberanía alimentaria y organización comunitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alzamos nuestras voces para exigir ¡nunca más! y para contribuir a la construcción de la verdad desde nuestras cosmovisiones, luchas históricas y legados culturales, esperando que en el país se edifique una verdad sanadora, resiliente, incluyente, compleja y territorial que transite de las verdades judiciales hacia relatos que se apropien socialmente y contribuyan al escenario de justicia restaurativa, no repetición, reparación  
integral y a la garantías de nuestros derechos al territorio y la pervivencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar leer: ([Especial: 50 años de promesas fallidas a los Embera](Especial:%2050%20años%20de%20promesas%20fallidas%20a%20los%20Embera))

<!-- /wp:paragraph -->
