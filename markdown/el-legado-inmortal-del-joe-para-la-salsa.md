Title: El legado inmortal del "Joe" para la salsa
Date: 2015-12-29 15:44
Category: En clave de son
Tags: Fruko y sus tesos, Joe Arroyo, Programa Joe Arroyo, Salsa colombiana
Slug: el-legado-inmortal-del-joe-para-la-salsa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 

Mucho se ha dicho acerca de Alvaro José Arroyo, el Joe, mientras estuvo vivo y después de su muerte. Algunas historias verídicas y otras tantas que hacen parte ya de la leyenda nacida en Cartagena el 1ro de Noviembre de 1955.

El hombre que desde temprana edad recorría las calles de la ciudad amurallada para ganar unos centavos con su particular tono de voz, habilidad que fue puliendo canto tras canto, vinculándose al coro de la catedral y más adelante en grandes grupos, uno de los primeros sería los tradicionales "Corraleros de Majagual".

Arroyo saltó de las esquinas a la fama que le aguardaba en la ciudad de Barranquilla, donde se convirtió en vocalista de la agrupación "La Protesta" para luego hacer historia con "Fruko y sus Tesos", una de las orquestas más destacada de salsa durante varios años en el país y con la que interpretaría varios de sus éxitos, entre ellos "Tania".

Hacia finales de los años 80 concluye su carrera con la orquesta de Julio Ernesto Estrada (Fruko), apartandose por un tiempo de los escenarios por problemas de salud relacionado con los abusos en el consumo de drogas, hasta que en 1989  lanza su  álbum "Fuego", con el que visita varios países del mundo. Este seria el disco que pondría su nombre junto al de grandes salseros como Benny Moré o Héctor Lavoe.

EL Joe ganó 18 veces el congo de oro y 4 super congos en el Carnaval de Barranquilla y en el año 2011 fue galardonado con el  premio Grammy Latino en un homenaje póstumo en la ciudad de las Vegas.

En Clave de Son los invita durante dos horas a recordar la música del Joe, a cantar y bailar  "En Barranquilla me quedo", "Tal Para cual", "La Rebelión" y muchas más de este grande de la salsa caribeña.

<iframe src="http://www.ivoox.com/player_ek_9914675_2_1.html?data=mp6elpubeY6ZmKiakpeJd6KkmYqgo5qZcYarpJKfj4qbh46kjoqkpZKUcYarpJKyzpDQqcjVxdSYy9PRs9PowtGYxsrQb4amk6%2Fcx4qWdozkwtfOjdHFb9TVzdjOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
