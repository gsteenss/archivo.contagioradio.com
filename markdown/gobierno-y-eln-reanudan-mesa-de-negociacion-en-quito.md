Title: Con llamado a cese bilateral, se reanudan diálogos entre ELN y Gobierno
Date: 2018-03-12 14:27
Category: Nacional, Paz
Tags: Cese al fuego, ELN, Gobierno Nacional, negociaciones en Quito, V ciclo de conversaciones
Slug: gobierno-y-eln-reanudan-mesa-de-negociacion-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  EFE] 

###### [12 Mar 2018] 

El Ejército de Liberación Nacional y el presidente de la República Juan Manuel Santos indicaron que se **sentarán nuevamente para iniciar el V Ciclo de Conversaciones en Quito**, Ecuador. Además, en el comunicado del anuncio, el ELN afirmó que tienen “la convicción que es mejor hacer el diálogo en medio de un cese bilateral, y que la agenda pactada hay que desarrollarla con rigurosidad y celeridad.”

### **Conversaciones se reanudan tras un largo periodo de suspensión** 

Las conversaciones se reiniciarán luego de que, según el ELN, “el Gobierno se retirara de la Mesa”.  Estas conversaciones fueron aplazadas desde el pasado 9 de enero, fecha en la que **culminó el cese bilateral al fuego** y después de que el grupo armado realizara un cese unilateral con el motivo de la jornada electoral del 11 de marzo. Indicó el grupo que las negociaciones inician “el mismo día que terminó el histórico cese al fuego bilateral de 101 días de duración”.

De acuerdo con el ELN, el retiro de la mesa de conversación por parte del Gobierno Nacional se dio **“para satisfacer a los sectores más extremos de la derecha colombiana”** teniendo en cuenta que “la lógica de los sectores ultraderechistas (..) aplauden el retiro de la Delegación gubernamental de la Mesa de conversaciones”.

Sin embargo, ese grupo **acudirá al llamado del presidente Juan Manuel Santos** “para reiniciar las conversaciones” pues “este es un aporte para proseguir la construcción de unas mayorías por la solución política del conflicto y por unas transformaciones que hagan posible la paz”, expresaron en el comunicado.

El Gobierno Nacional se sentará con esa guerrilla en aras de conseguir un cese bilateral al fuego e ir construyendo el **camino hacia la paz**. El presidente de la República manifestó que le dio instrucciones al jefe del equipo de paz del Gobierno, Gustavo Bell, para reactivar los diálogos “con el objetivo de lograr avanzar en los puntos de la agenda y lograr un nuevo cese al fuego y de hostilidades amplio y verificable”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
