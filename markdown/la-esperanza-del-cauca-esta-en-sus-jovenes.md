Title: La esperanza del Cauca está en sus jóvenes
Date: 2020-01-21 12:40
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, jovenes
Slug: la-esperanza-del-cauca-esta-en-sus-jovenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0261-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0429-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0429-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0346.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0346-1-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En Popayán se dieron cita jóvenes de los municipios de Argelia, Inzá y Galíndez. Durante el encuentro ellos y ellas manifestaron, a través de pinturas, hilos que tejieron con lana, canciones, risas y escritos su visión de la realidad actual del departamento, el análisis que hacen de lo que está pasando en este momento del país, pero sobre todo compartieron sus esperanzas y sus sueños de construcción de verdad y de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la primera parte de la jornada y con pinceles y pinturas en mano, representaron lo que cada uno de ellos es y cómo siente su territorio.  
El Río que desapareció por la acción de las empresas mineras, sus experiencias de fe, sus lugares favoritos, los platos que más les gusta comer y los sueños que tienen para sus vidas en sus territorios, fueron la puerta de entrada a una realidad que muchas veces es invisible a los ojos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las verdades de los jóvenes del Cauca**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A través de diversos personajes, unos de carne y hueso, y otros que personificaron los sueños y las esperanzas los y las jóvenes, dejaron en claro la estrecha relación que tienen con su territorio, como ese lugar en el que viven y crecen, vive y crece con ellos y ellas. Una primera gran conclusión es que ellas y ellos son uno solo con el territorio, con el río, con la montaña, con la cancha de micro, con el parque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para las jóvenes de Argelia, las identidades de género y el respeto por las diferencias se hizo presente y es otra de las conclusiones de este encuentro. Las y los jóvenes no están dispuestas y dispuestos a eliminar la diferencia, para ellas la diferencia está presente, es natural y es respetable. Una lección para otros y  afortunadamente una situación construida socialmente para ellas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta reflexión, con anécdotas de partidos de fútbol, de tareas en grupo, de encuentros de tarde, es sin lugar a dudas una de las claves con las que estos jóvenes construyen su dignidad en medio de la violencia estructural que se traduce en falta de oportunidades, en ausencia de posibilidades de educación superior, o en presencia de la guerra que se rehúsa a abandonar sus tierras.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":79408,"width":589,"height":393,"sizeSlug":"large","className":"is-style-default"} -->

<div class="wp-block-image is-style-default">

<figure class="aligncenter size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0429-1-1024x683.jpg){.wp-image-79408 width="589" height="393"}  
<figcaption>
Lectura de Fanzines elaborados por los jóvenes durante la reflexión
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:html -->  
\  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### S**obre la guerra y los juegos **

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A pesar de que el Cauca es uno de los departamentos más golpeados con la guerra, con el asesinato de líderes sociales, (88 líderes indígenas entre 2016 y 2019) la lucha imparable por la propiedad de las tierras que han asumido las comunidades indígenas en contra de los grandes terratenientes y empresas, el narcotráfico que los rodea y de una u otra manera los condiciona y los pretende moldear, los y las jóvenes resisten y construyen.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El narcotráfico, ubicado como uno de los principales problemas, que pretende condicionar el actuar de los jóvenes, “la plata fácil” la “vida cómoda” no ha logrado doblegar las sonrisas de muchos y muchas. Y es que no es fácil ver como un niño de 12 años opta por el negocio y tiene una moto, mientras que otro que quiere terminar de estudiar se ve en “las duras y las maduras” para conseguir la alimentación o el vestido.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El Cauca en cifras**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según los informes Simci de las Naciones Unidas, los cultivos de coca aumentaron en más de 4500 hectáreas entre 2016 (12.595 hectáreas), año que se firmó el Acuerdo de Paz, y el 2018 (17.117 hectáreas) en el Cauca. Argelia es el segundo municipio con más cultivos de coca en Cauca, con 1.956 hectáreas, después de El Tambo, que tiene 6.661. En Argelia 11.400 familias de Argelia expresaron su voluntad de sustituir los cultivos de uso ilícito (de un total de 26 mil habitantes), es decir, más de la mitad de la población tiene relación con ese cultivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al final de cuentas, parece que el narcotráfico quisiera definir las reglas de la ética, de lo que es bueno y lo que no, de quiénes \*son bobos\* y quienes son vivos, pero a pesar ello los delegados del grupo juvenil de Argelia y el Mango, tomaron la decisión de trabajar con los niños y las niñas, desde la parroquia o desde el colegio. Las jóvenes de Argelia optan por el estudio, por las huertas escolares y por los campeonatos de microfútbol, sellando así la derrota, por lo menos temporal, de las fuerzas del mal, que se visten de camuflado y cobran vacunas o recogen la “hoja”.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, por encima de una realidad tan avasalladora y que pareciera desesperanzadora, los y las jóvenes dejaron claro que su opción es otra, es vivir, ser felices, alejarse de los problemas de la guerra y construir una realidad distinta, para ellas y ellos y para sus familias, lo que, sin lugar a dudas será una salida para el departamento que a veces pareciera perder la esperanza en medio de tanta muerte.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":79410,"width":569,"height":380,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/IMG_0346-1-1024x683.jpg){.wp-image-79410 width="569" height="380"}  
<figcaption>
Sede de la Comisión de la Verdad, Popayán
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:block {"ref":78980} /-->
