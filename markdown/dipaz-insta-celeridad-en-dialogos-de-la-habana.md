Title: Diálogo Intereclesial por la Paz insta celeridad en diálogos de La Habana
Date: 2016-11-10 14:47
Category: Paz
Tags: acuerdo de paz, Acuerdo de paz de la habana, comunidades, DIPAZ, Iglesias, La Habana, paz, proceso de paz, Renegociación acuerdos de paz
Slug: dipaz-insta-celeridad-en-dialogos-de-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/DiPaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DiPaz] 

###### [10 Nov. de 2016] 

**El Diálogo Intereclesial por la Paz – DiPaz**, ha dado a conocer un comunicado en el cuál **saludan la superación de los diversos obstáculos en la fase de ajustes, precisiones y cambios al Acuerdo de Paz** y recordaron que organizaciones sociales y de la sociedad civil, se encuentran aún a la expectativa de conocer los Acuerdos Definitivos y los mecanismos de implementación de los mismos.

Así mismo, **instaron a las partes a actuar con celeridad, pero con profundidad y responsabilidad para cerrar esta fase de re negociación de los Acuerdos de Paz** hechos en La Habana. Etapa que según DiPaz, ha generado incertidumbre y zozobra. Le puede interesar: [Más de 500 organizaciones de víctimas respaldan Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/mas-de-500-organizaciones-de-victimas-respaldan-jurisdiccion-especial-para-la-paz/)

Finalmente, reiteraron toda la disposición y apoyo en todo lo que se considere necesario “para asegurar la construcción de una sociedad en paz con justicia social protegiendo los derechos de todas las víctimas sin distinción y la inclusión de los derechos de sectores rurales y grupos humanos vulnerables”. Le puede interesar: [Iglesias y organizaciones de fe de DiPaz acompañarán las zonas veredales transitorias](https://archivo.contagioradio.com/iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias/)

DiPaz ha acompañado la verificación del cese al fuego bilateral desde que fue decretado en medio de los diálogos de paz. Y en la actuallidad, continuan dicho trabajo de verificación desde dos casas que tienen presencia en Antioquia y Cauca.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
