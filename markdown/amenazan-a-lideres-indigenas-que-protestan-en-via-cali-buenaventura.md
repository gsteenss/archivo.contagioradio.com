Title: Amenazan a líderes indígenas que protestan en via Cali - Buenaventura
Date: 2015-10-21 14:35
Category: Movilización, Nacional
Tags: Batallón de Alta Montaña número 4, Fernando Chirimía, Movilización indígena en Buenaventura, Resguardo Nasa Kiwe Embera Chami La Delfina
Slug: amenazan-a-lideres-indigenas-que-protestan-en-via-cali-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/indigenas_buenaventura_contagioradio-e1490306482847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ORIVAC ] 

<iframe src="http://www.ivoox.com/player_ek_9116305_2_1.html?data=mpaemJiUeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhc7Zz8bnw9OPpYzghqigh6aoqMbmxtiYy9PIaaSnhqaxycrSpdSf0trSjdXWs9XZ1NnO0JDJsozqysaYpcbQcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fernando Chirimía] 

###### [21 Oct 2015] 

[Tras **11 días de minga de resistencia y asamblea permanente** que ya completan 5 pueblos indígenas del Resguardo Nasa Kiwe Embera Chami La Delfina, en Buenaventura, ante el incumplimiento de garantías para el retorno del pueblo Woaunaan desplazado desde noviembre de 2014, han sido **intimidados por militares y varios de sus líderes han recibido amenazas**.]

[El pasado jueves 15 de octubre, entre las 2:30 pm y 3:00 pm, **2 líderes indígenas** de la organización ACIVA R.P y ORIVAC **recibieron llamadas** del celular 3205456310 en las que una persona les amenazó afirmando que debían quitar la guardia porque “estamos bien armados” de no hacerlo “serán **objetivo militar**”.  ]

[Sobre las 4 de la tarde del lunes 19 de octubre, 2 hombres de civil en una motocicleta de color negro y sin placas, fotografiaron con un celular a los indígenas en el km 50, vía Buenaventura-Cali. Al ser abordados por la Guardia Indígena y defensores de derechos humanos, 1 de los hombres se identificó como el **Capitán Narváez del Batallón de Alta Montaña número 4 y afirmó que era de inteligencia militar**. La comunidad exigió eliminar los registros fotográficos, luego de hacerlo les permitieron retirarse.]

[La movilización pacífica se adelanta en el marco de **estigmatizaciones por parte de organismos de seguridad y la fuerza pública**, sin que el Estado colombiano resuelva los asuntos de fondo. Fernando Chirimía, asegura que el día lunes una comisión se trasladó a Cali para llegar a un acuerdo, pero no hubo respuesta positiva por parte de las autoridades, por lo que estas comunidades han decidido continuar en asamblea permanente.]

[Las movilizaciones en la vía Buenaventura-Cali no han bloqueado el paso de vehículos, ese es un acto simbólico que se realiza al lado de la vía y que ha sido **distorsionado por algunos medios de comunicación y la fuerza pública**, afirma Chirimía, agregando que la decisión es mantenerse allí hasta que las exigencias sean resueltas oportunamente pues “lamentablemente, sólo cuando se asumen vías de hecho es que son escuchadas las demandas”.]

[Vea también]: [[Indígenas heridos y detenidos por ESMAD en Vía Cali - Buenaventura](https://archivo.contagioradio.com/4-indigenas-heridos-y-2-detenidos-deja-represion-de-esmad-en-via-cali-buenaventura/)]
