Title: "Es absurdo decir que el paro es conducido por la guerrilla": César Jeréz
Date: 2016-06-02 16:46
Category: Paro Nacional
Tags: Minga Nacional, ministerio de defensa, Paro Nacional
Slug: es-absurdo-decir-que-el-paro-es-conducido-por-la-guerrilla-cesar-jerez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/b18d7211-6263-4ad2-82ae-bc9369507500-e1464903613868.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Minga Nacional 

###### [2 Jun 2016]

Frente a los ataques por parte del ESMAD, el Ejército y la Policía contra la movilización en el marco de la Minga Nacional, **nuevamente el gobierno pretende invisibilizar esta situación y criminalizar la protesta legítima de los campesinos,** según denuncia César Jeréz, vocero de la Cumbre Agraria Étnica, campesina y popular.

El Ministro de Defensa, Luis Carlos Villegas, aseguró que el paro agrario que se desarrolla en diferentes regiones del país, las movilizaciones fueron infiltradas por la guerrilla del ELN en el Chocó, Cauca, Norte de Santander y Cesar, a lo que Jeréz, afirma que esta declaración del ministro corresponde a **“la constante del gobierno en insistir en un libreto para estigmatizar la protesta…** Es absurdo decir que el paro es conducido por la guerrilla, es más de lo mismo, un argumento del gobierno que busca generar una matriz de opinión en contra de la movilización”.

Así mismo, Villegas,  indicó que el gobierno no permitirá vías de hecho ni bloqueos, una orden del gobierno que **ya ha dejado 3 personas muertas y decenas de heridos**, en su mayoría en el departamento del Cauca.

La Cumbre Agraria ha anunciado que está dispuesta a iniciar nuevamente un diálogo con el gobierno en una mesa única nacional donde se generen soluciones puntuales a las demandas de las comunidades afro, indígenas y campesinas.

<iframe src="http://co.ivoox.com/es/player_ej_11760945_2_1.html?data=kpakmJWdeJahhpywj5WaaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1Mbfja%2FJtsbujJKY2NTHqdPjjKjiz8fWqYy1yNfO1M7FcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
