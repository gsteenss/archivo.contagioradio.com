Title: Francia: Primera visita oficial de mandatario Occidental a Cuba
Date: 2015-05-11 15:40
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Francia rompe cerco a Cuba, Hollande visita a Cuba, Hollande visitará Raúl Castro y Fidel Castro, Primera visita mandatario europeo a Francia, Visita presidente francés a Cuba
Slug: francia-primera-visita-oficial-de-mandatario-occidental-a-cuba
Status: published

###### **Foto:20minutes.fr** 

[El presidente de **Francia Hollande** es el **primer mandatario Europeo** en romper el bloqueo político contra **Cuba**. El primer mandatario francés ha llegado esta mañana a la isla con una **delegación de empresarios** con el objetivo de romper el cerco y poder reestablecer relaciones económicas con el país.]{lang="ES"}

[Después de que **Washington restableciera las relaciones bilaterales con Cuba el pasado Diciembre** y de la visita del **Vaticano**, Francia no ha esperado a coordinar la visita con los otros miembros de la UE y ha sido el primero en dar el paso.]{lang="ES"}

[En declaraciones del presidente francés al llegar a la isla, **¨Es una visita histórica"**. La UE siempre ha sido proclive a tener buenas relaciones con el país y a finalizar el embargo Estadounidense. En la visita oficial hay programada para esta noche una **entrevista con Raúl Castro** y otra de carácter confidencial con **Fidel Castro**.]{lang="ES"}

[Aunque la principal razón de la visita del mandatario francés son los acuerdos económicos con la isla y las **6 empresas francesas** que allí operan como Bouygues, Total, Alstom, Alcatel-Lucent, Pernod-Ricard (Rhum Havana Club), Accor y Air France.]{lang="ES"}

[Antes de la partida del mandatario francés la ONG **Reporteros sin Fronteras** ha enviado al mismo una carta con el objetivo de **defender los derechos de la libertad de prensa**, exigiéndole a Hollande que trate temáticas referidas a los DDHH.]{lang="ES"}

[Hollande ha explicado a los medios de comunicación que entre sus prioridades para reestablecer las relaciones están la defensa de los DDHH y la libertad de prensa.]{lang="ES"}

[Entre las principales temáticas que se van a bordar durante las reuniones con los altos mandatarios Cubanos están las de poder **estrechar lazos en temas referidos a la sanidad y el turismo.**]{lang="ES"}
