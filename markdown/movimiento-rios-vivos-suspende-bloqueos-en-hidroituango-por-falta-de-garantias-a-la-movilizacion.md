Title: Movimiento Ríos Vivos suspende bloqueos en Hidroituango pero continúa la movilización
Date: 2015-09-08 14:25
Author: AdminContagio
Category: DDHH, Nacional, Resistencias
Tags: ANLA, Cumbre Agraria, EPM, ESMAD, Fuerzas militares, Gobernación de Antioquia, Hidroituango, Isabel Cristina Zuelta, Ituango, Movimiento Ríos Vivos
Slug: movimiento-rios-vivos-suspende-bloqueos-en-hidroituango-por-falta-de-garantias-a-la-movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/img_148312.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

<iframe src="http://www.ivoox.com/player_ek_8199065_2_1.html?data=mZamm5WaeY6ZmKiak5mJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs4zGhqigh6aos9Sft87j0diPt9bn0crbxsqPps3j0trS0diPqc%2Bfqc7R1NTNuNbVz8yah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabel Cristina Zuleta, Movimiento Ríos Vivos ] 

###### [8 Sept 2015] 

[Desde el lunes 31 de agosto y hasta el pasado domingo 6 de septiembre, [afectados por el proyecto Hidroituango adelantaron bloqueos](https://archivo.contagioradio.com/movimiento-rios-vivos-bloquea-entradas-a-hidroituango-para-frenar-danos-ambientales/)para impedir el desarrollo de las actividades de la represa, la jornada de protesta debió suspenderse debido a la presión que generó en los manifestantes la presencia de agentes de la policía que aseguraban trabajar para Empresas Públicas de Medellín, EPM, que lleva a acabo el megaproyecto. ]

[En Briceño, Ituango, Toledo, Puerto Valdivia y el Aro en Antioquia, los pobladores exigían el **cumplimiento de los acuerdos pactados para hacer frente a los impactos socio-ambientales provocados por la construcción de la hidroeléctrica desde el año 2009**.]

[Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, aseguró que **pese al compromiso del gobierno departamental de garantizar condiciones para la movilización, los bloqueos y marchas, las protestas fueron disueltas** por unidades del ESMAD, la Policía y miembros de las Fuerzas Militares, quienes amenazaron con capturar a algunos pobladores e hirieron a otros.]

[Las movilizaciones adelantadas con el fin de exigir acciones concretas frente a los impactos ambientales y a las poblaciones de las partes altas y bajas que circundan la hidroeléctrica, llaman la atención también sobre la **crisis en materia de derechos humanos que padece la región y que coincide con la aplicación de las 9 modificaciones que ha tenido la licencia ambiental** en la que se ampara el proyecto hidroeléctrico.]

[Además del aumento en el acaparamiento de territorio y agua por parte de EPM y sus consorcios, posibilitado por las aprobaciones de la ANLA, es "**indignante que los ingenieros de EPM cuenten con piscinas, casinos, restaurantes, canchas de tejo y de tenis al interior de Hidroituango, mientras los barequeros y pobladores no cuentan con comida, territorio, ni servicios públicos", expresa Zuleta.**]

[El Movimiento Ríos Vivos continuará exigiendo a las autoridades de orden departamental y nacional, como la ANLA y las fuerzas militares, que se reúnan con las comunidades afectadas, reconozcan su responsabilidad en la agudización de la problemática y "**actúen coherentemente y no según los intereses de las multinacionales presentes en Colombia", dice la vocera del movimiento**.  ]

[![ituango 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ituango-3.png){.aligncenter .wp-image-13637 width="403" height="437"}](https://archivo.contagioradio.com/movimiento-rios-vivos-suspende-bloqueos-en-hidroituango-por-falta-de-garantias-a-la-movilizacion/ituango-3/) [  
](https://archivo.contagioradio.com/movimiento-rios-vivos-suspende-bloqueos-en-hidroituango-por-falta-de-garantias-a-la-movilizacion/ituango-2/) [![ituango 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ituango-1.png){.aligncenter .wp-image-13639 width="396" height="387"}](https://archivo.contagioradio.com/movimiento-rios-vivos-suspende-bloqueos-en-hidroituango-por-falta-de-garantias-a-la-movilizacion/ituango-1/)

![ituango 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ituango-2.png){.aligncenter .wp-image-13638 width="522" height="292"}
