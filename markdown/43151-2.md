Title: Investigación destaca aporte de las mujeres en el proceso de paz de La Habana
Date: 2017-07-05 14:32
Category: Mujer, Nacional
Tags: Acuerdos de La Habana, enfoque de género, mujeres, participación de mujeres en la Habana
Slug: 43151-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cuba-colombia-farc-un-peace-presser_7928980-e1499282166398.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [05 Jul 2017]

Algunas de las más de **140 mujeres que participaron de las conversaciones de paz entre el Gobierno Nacional y las FARC en Cuba**, se reunieron hoy para presentar el libro “Vivencia, aportes y reconocimiento: Las mujeres en el proceso de paz en La Habana” como un reconocimiento al trabajo permanente y el aporte de una perspectiva de género en la construcción del acuerdo de paz.

El libro fue producto de una investigación de la Corporación de Investigación y Acción Social y Económica CIASE y la Corporación Humanas. Las investigadoras realizaron entrevistas a las mujeres que estuvieron presentes **en la Habana para visibilizarlas y reconocerlas** debido a que “aún hoy permanecen anónimas numerosas mujeres que aportaron de manera significativa al avance de las negociaciones y al logro del acuerdo final de paz”. (Le puede interesar: ["Mujeres excombatientes en la implementación de los acuerdos de paz"](https://archivo.contagioradio.com/el-papel-de-las-mujeres-excombatientes-en-la-paz/))

**Experiencias de mujeres en los diálogos de paz de la Habana**

<iframe src="http://co.ivoox.com/es/player_ek_19646743_2_1.html?data=kp6jlpubeJShhpywj5WXaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncabgxtPOjabRptPj1M6SlKiPvYzn1pDS2tXJtsrZz8jWw5DIudPVz9nSjcrQb9Hm0MjS1dSPqMaf0ZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Una de las mujeres que participó en la Habana y que se tuvo en cuenta para la realización de la investigación fue Elena Ambrosi. Ella estuvo presente en los diálogos de paz desde “la parte secreta de los diálogos” y duró **5 años siendo parte de la delegación del gobierno en la Habana**. Para ella, haber ayudado a crear la Subcomisión de Género en el 2014, “significó una experiencia enriquecedora y llena de aprendizajes. Allá se vive una situación bastante tensa y aprender a manejarla fue muy importante, también fue un sacrificio que valió la pena”.

De igual manera, Ambrosi destacó que **“fue posible transversalizar el tema de los derechos de las mujeres en todo el acuerdo”**. Ambrosi recalcó que el trabajo de las mujeres en la Habana logró visibilizar la victimización que han sufrido miles de colombianas por la guerra para lo que se acordaron medidas diferenciales de atención para ellas. (Le puede interesar: ["Participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental"](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/))

<iframe src="http://co.ivoox.com/es/player_ek_19646704_2_1.html?data=kp6jlpubdJWhhpywj5WWaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1M7Fb7TVz8nW0NSJdqSf1NTP1MqPqc2fxNTa0tfTscrn0JDRx5DQpdSfztrXx9fJt4zZz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Victoria Sandino, representante de las mujeres de las FARC que también estuvo presente en Cuba, manifestó que “**el compromiso de las mujeres colombianas hizo que fuera posible sacar adelante el enfoque de género** donde logramos que se mantuviera en los acuerdos y que hoy seguimos luchando para que se mantengan”.

Tanto Ambrosi como Sandino reconocieron que **“las mujeres de Colombia debemos apropiarnos de este logro y seguir trabajando para continuar siendo protagonistas de este acuerdo de paz”**. Ellas, después de haber tenido un rol activo en la mesa de negociación, enfatizaron en que “es un momento muy importante para las mujeres de Colombia para abrir espacios de participación en lo que viene ahora que es la implementación de los acuerdos”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
