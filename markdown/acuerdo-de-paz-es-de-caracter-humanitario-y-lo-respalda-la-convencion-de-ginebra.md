Title: Análisis del blindaje a los acuerdos de paz por ICTJ  y Alfredo Molano
Date: 2016-05-13 16:32
Category: Entrevistas, Paz
Tags: acuerdos de paz, Conversacioines de paz en Colombia, FARC
Slug: acuerdo-de-paz-es-de-caracter-humanitario-y-lo-respalda-la-convencion-de-ginebra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/alfredo-molano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cercapaz] 

###### [13 May 2016]

El reciente acuerdo parcial anunciado por la mesa de conversaciones de paz no implica una violación al cuerpo constitucional y legal de Colombia puesto el contenido del acuerdo final estará ligado a alguna manera de aprobación por parte de los ciudadanos colombianos, bien sea un plebiscito o la propia constituyente. **Lo que se logra elevando a la calidad de acuerdo internacional es soportarlo jurídicamente** para que sea respetado por otros gobiernos siguientes, explican los analistas.

Camila Moreno, directora del Centro Internacional para la Justicia Transicional explica que la metodología acordada garantiza la participación en todas las funciones por parte del Congreso y la Corte Constitucional. Igualmente, afirma que en ningún momento del proceso se viola el marco constitucional y **no se pasa por encima de las funciones del Congreso** puesto que ese escenario será el encargado de agregar y [legislar en torno al este acuerdo final](https://archivo.contagioradio.com/page/3/?s=acuerdo+de+paz+ONU) y en cada uno de los puntos del acuerdo final.

Según Moreno  la formula lograda combina diferentes mecanismos para lograr el blindaje al acuerdo, por una parte **elevar el acuerdo final a acuerdo especial**, figura que se encuentra en el artículo 3 común de los convenios de Ginebra, es decir que se le da un estatus internacional que será depositado en Suiza.

Por otra parte se incluye la acotación en que el presidente, en cabeza del Estado, emita una declaración, ante el consejo de seguridad de las Naciones Unidas pidiendo que se **anexe el acuerdo final a la [resolución que emitió la ONU](https://archivo.contagioradio.com/presencia-de-onu-es-garantia-de-imparcialidad-y-de-presion-para-la-aplicacion-de-acuerdos-de-paz/)** para acompañar los mecanismos de verificación del acuerdo, sin embargo este alcance es limitado al bloque de constitucionalidad en Colombia.

Para Alfredo Molano, sociólogo y escritor lo que está de fondo es la garantía del cumplimiento del acuerdo “cómo se va a cumplir lo que se acuerde, esa ha sido la gran nube”. Estas garantías se dividen en dos partes, una cómo se va a acabar con el paramilitarismo y la otra es que se cumpla. Sin embargo este paso pone el acuerdo de paz en un plano de cumplimiento **tanto en la institucionalidad colombiana y de la comunidad internacional, eso es “un doble remache”.**

En cuanto a las reacciones del Centro Democrático y otros, lo que afirma Molano es que el primer “tiro” del ex presidente Uribe es deslegitimar el acuerdo y otro el llamado a la acción. “primero manifestaciones públicas” y por otro lado conducir a la “gran batalla” contra los acuerdos. “yo sigo sospechando que ahí hay una orden implícita” para aquellos sectores de la extrema derecha que se oponen a los acuerdos “ahí hay un peligro” que puede influir sobre el orden público.

<iframe src="http://co.ivoox.com/es/player_ej_11523654_2_1.html?data=kpailJiaeZWhhpywj5abaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Zw5Cxs9PZz9SYj5Cnqc%2Fo09SYq9PYqdPiwsjW0dPFsIzkwtfOjdHFb6vp1NnWxc7Fb7Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_11523563_2_1.html?data=kpailJiZepShhpywj5aUaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaLgx9fSxtSPkdDgwtPcjZKPhc%2FVzc7g1saJdqSf0crfy9TIaaSnhqax1dnFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
