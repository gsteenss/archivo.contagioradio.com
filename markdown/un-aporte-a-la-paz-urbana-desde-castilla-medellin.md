Title: Un aporte a la paz urbana desde Castilla, Medellín
Date: 2016-04-28 06:19
Category: Fernando Q, Opinion
Tags: CORPADES, Medellin, Oficina del Valle de Aburrá, Paramilitarismo, posconflicto
Slug: un-aporte-a-la-paz-urbana-desde-castilla-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/CASTILLA-MEDELLIN-LUGAR-DE-PAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mauricio Agudelo 

#### **[Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano) ** 

###### 28 Abr 2016

Cien días lleva la nueva Alcaldía en la ciudad de Medellín y hasta ahora demuestra que un nuevo estilo de gobernar se impone: el sí a todo. En otras palabras, el reconocimiento a la realidad del conflicto urbano sin tapujos deja atrás las cortinas de humo que lo cubrían todo. Es entendible que al desvelarse la realidad, con ella afloren múltiples problemáticas que estaban ocultas por la fuerte inversión en publicidad engañosa, pero, al unísono, también surgen propuestas y posibles salidas. ¿Será que el alcalde Federico Gutiérrez tendrá la sapiencia para entender y actuar frente a los nuevos retos que se verá abocado a enfrentar? Espero que sí, por el bien de la ciudad metropolitana.

Las problemáticas son diversas y complejas: extorsión generalizada, pagadiario, explotación sexual de niños, niñas y adolescentes; apuestas ilegales, contrabando, control territorial criminal, pacto del fusil, desaparición forzada, desplazamiento forzado intraurbano, regulación de la violencia por parte de actores armados, entre otros.

Las propuestas también lo son: por un lado están las de tipo social que son abundantes, la mayoría impulsadas por el interés público que buscar fortalecer la sociedad civil y la comunidad en general -aunque hay algunas (pocas) que buscan satisfacer intereses particulares en detrimento del corpus social-. Y por el otro, están las que plantean que la represión y la negociación a oscuras debe ser la hoja de ruta de la estrategia de seguridad.

Para cumplir con estas últimas, se requieren más cámaras y más policías, pactos secretos con el crimen, y la aplicación errónea de la política de combate a la criminalidad con énfasis en la captura y el decomiso, que sólo controla y no desmantela la ilegalidad armada.  Así mismo, se generan acuerdos con representantes del bajo mundo, que son tratados como peones de poner y quitar en el juego de ajedrez, cuyo sacrificio no importa, con los que se pacta la regulación de la violencia y mantener baja la cifra de homicidios. Sin importar cómo lo hagan, lo que se necesita es que ayuden a la cosmética de la ciudad.

Pero también están quienes proponen que la estrategia de seguridad debe ser integral, basándose en el desmantelamiento de las estructuras paramafiosas y sus bandas paramilitarizadas, desmantelamiento que no sólo debe identificar a cada miembro de esas agrupaciones -sean peones, subjefes o jefes-, también debe identificar con claridad fuentes de financiación, inversiones, testaferros, bienes muebles e inmuebles, auspiciadores y algo que es bien importante: los beneficiarios de sus actuaciones que no son pocos. Con esta propuesta me identifico, y es la que se viene impulsando hace años desde Corpades y Análisis Urbano.

En esa línea, la estrategia de seguridad integral para el desmantelamiento del crimen y el freno a la violencia desmedida que ejerce, debe activar otras herramientas que se ajusten más al momento histórico que vive Colombia: el cierre definitivo de la guerra y la llegada de la Paz, la cual espero sea completa: paz rural y paz urbana, posconflicto para todos.

Así que insisto en la necesidad del diálogo y la negociación que permita el sometimiento a la justicia, con dignidad pero sin impunidad, de toda la cadena del crimen: campaneros, gatilleros, colaboradores, subjefes, jefes y patrones de las 350 bandas que están al servicio en su mayoría de la Oficina del Valle de Aburrá. Esta última también deberá ser desmantelada con su junta directiva, la cual representa intereses de clanes u organizaciones mafiosas del Cartel de Medellín, ya que la Oficina es sólo la fachada que lo protege.

Lo mismo debe hacerse con la otra estructura que opera en Medellín, las Autodefensas Gaitanistas de Colombia (AGC). Y esto, inevitablemente, lleva a plantear que los diálogos deberían ser de carácter metropolitano, incluso de carácter regional y nacional, por la presencia que tienen ambas estructuras en otras subregiones del departamento de Antioquia y algunos departamentos.

La propuesta de diálogos y negociación no se debe basar en la resolución de conflictos porque esto no ha funcionado y termina todo igual a como inició; se debe construir con bases sólidas surgidas de la gestión, tratamiento y transformación de los conflictos con enfoque diferencial. Las soluciones solo serán posibles si las propuestas son acordes a la realidad.

Observemos con atención la apuesta de paz que se está dando en la ciudad de Medellín, desde Castilla, comuna 5 de la ciudad. La oferta de entrega de armas que piensan hacer 160 jóvenes que tienen radio de acción en un sector de esta comuna, es un buen inicio para abrir la discusión sobre lo urgente y necesario que es la paz urbana. Esto abre la posibilidad de generar un laboratorio de paz urbano, donde se tengan en cuenta aciertos y desaciertos de los procesos de paz anteriores y de sometimientos a la justicia, para aplicar lo positivo que dejaron.

El primer paso deberá ser ganar el concurso y la participación protagónica de las comunidades barriales del entorno, afectadas por el accionar criminal; el segundo, tener una oferta institucional generosa que permita mostrar que sí es posible recorrer el camino de lo ilegal a lo legal.

Por ahora, y mientras la propuesta termina de cuajar, gana apoyos necesarios y gana atención del gobierno nacional. Corpades y la Corporación Nelson Mandela para la Libertad y la Paz (Cnemalipaz) ven con buenos ojos esta iniciativa, la cual se espera sea  seria y sirva como paso inicial para que otros grupos sigan su ejemplo. Yo espero que la Oficina del Doce de Octubre, los Mondongueros, su aliado los Machacos, y la Matecaña, entre otros miren, piensen y actúen en esa dirección. La hora de la paz urbana ha llegado, espero que ustedes den el paso firme hacia ella.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
