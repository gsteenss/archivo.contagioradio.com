Title: 500 mil judíos viven en 141 asentamientos ilegales en Palestina
Date: 2017-01-23 17:29
Category: DDHH, El mundo
Tags: Asentamientos ilegales, Donald Trump, Israel, Netanyahu, Palestina
Slug: 500-mil-judios-viven-141-asentamientos-ilegales-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Palestina-asentamientos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre] 

###### [23 Ene. 2017] 

**Pese a que los asentamientos en territorio Palestino violan el Derecho Internacional, las expansiones continuarán, esta vez el gobierno de Israel aprobó la construcción de 566 nuevas casas en territorios palestinos**, más exactamente en la colonia Maalé Adumim que se encuentra ubicada en la ciudad ocupada de Al-Quds en Jerusalem del este. Le puede interesar:[ ONU declara ilegales las colonias israelíes en territorio palestino](https://archivo.contagioradio.com/onu-declara-ilegales-las-colonias-israelies-territorio-palestino/)

La aprobación de este proyecto fue dada a conocer por el Gobierno de Netanyahu y anunciada por Meir Turjeman, presidente del Comité de Planificación Israelí. **En la actualidad los 500 mil judíos en asentamientos ilegales, completan casi la tercera parte de la población Palestina.**

La colonia de Malé Adubim fue creada en 1975 y es la tercera con más población en Cisjordania. **De continuar construyendo viviendas en estos territorios, éste se partiría en dos lo que haría casi imposible crear un Estado palestino** que tuviese una continuidad en lo que respecta a su geografía.

Ante estas aseveraciones la respuesta por parte de Palestina no se hizo esperar, quien a través del **portavoz de la presidencia de ese país, Nabil Abu Rudeina manifestó que la decisión israelí es un desafío al Consejo de Seguridad de la ONU** “no se puede desconocer la reciente votación en la que afirma el carácter ilegal de las colonias” aseveró.

**La construcción de estas casas había sido detenida por el Gobierno de Israel a la espera de la llegada de Donald Trump al poder**, pues según lo anunció el propio mandatario de Estados Unidos continuará apoyando a Israel. Para ello nombró como embajador en Israel a David Friedman, quien es un reconocido economista defensor de los asentamientos judíos en territorios palestinos.

Para Turjeman con la llegada de Trump al poder “las reglas del juego han cambiado. Ya no tenemos las manos atadas, como en la época de Barack Obama" afirmó en una reciente alocución. Le puede interesar:[ Las nuevas fichas de Trump](https://archivo.contagioradio.com/las-nuevas-fichas-trump/)

Y agregó que la construcción de viviendas continuará **“estas 566 viviendas sólo son el inicio. Tenemos planes para construir 11.000 viviendas”.**

Cabe recordar que según el Instituto de Investigación Aplicada Jerusalén (ARIJ, por sus siglas en inglés) señala que la construcción de asentamientos en dicho territorio ha tenido un incremento del 189 por ciento. La mayor parte de este crecimiento colonial ilegal tiene como epicentro a Jerusalén Este. Le puede interesar: [Israel derriba casas y desplaza familias palestinas](https://archivo.contagioradio.com/israel-derriba-casas-palestinas-34317/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
