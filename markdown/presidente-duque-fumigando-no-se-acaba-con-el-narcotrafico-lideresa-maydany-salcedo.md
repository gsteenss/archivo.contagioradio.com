Title: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo
Date: 2019-07-31 14:52
Author: CtgAdm
Category: Ambiente, DDHH
Tags: amenazas contra líderes sociales, Cauca, Piamonte, PNIS
Slug: presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Maydany.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

La Asociación de Trabajadores Campesinos de Piamonte, Cauca (ASIMTRACAMPIC) ha alertado sobre nuevas amenazas en contra de su dirigente **Maydany Salcedo, lideresa social quien viene impulsando la sustitución de cultivos de uso ilícito** y que hoy se ha visto obligada a dejar su tierra para proteger su vida.

Según relata la lideresa, quien actualmente está fuera de su territorio, el pasado 24 de julio fue contactada por la delegación de DD.HH. de la Policía, la que le informó que habían descubierto, mediante mecanismos técnicos que se buscaba atentar contra su vida, razón por la que tuvo que salir de Piamonte sin conocer cuáles podrían ser los motivos de la amenaza ni quienes podrían ser los responsables.

"Como dirigente social quiero saber cuáles son los hechos, la Policía aún me dice que me mantenga en Florencia pero mi llamado es que se nos aclare lo que está pasando, ¿Quiénes me quitaron mi vida, quién ordeno mi muerte y por qué razón? ¿Cuál es el mal que hacemos los dirigentes sociales cuando exigimos nuestros derechos?" inquiere la lideresa.

### Maydany defiende el plan de sustitución de cultivos de uso ilícito en su territorio

Una de las consecuencias de esas acciones de erradicación forzada es que de las 932 personas que hacían parte del proceso en el municipio,  actualmente solo hay presencia de 732, lo que demuestra una ausencia de implementación completa.

> "**Hoy en día el Gobierno cerró las puertas, solo piensan en echarnos glifosato y dañar nuestros cultivos de pan coger, señor Duque, abra más inscripciones del PNIS, le cuesta menos que hacernos daño al fumigar".**

Ante el posible regreso de una política de fumigación aérea, la lideresa mantiene una posición firme al asegurar, "los campesinos no vamos a permitir que nos fumiguen más, no vamos a permitir que hagan daño a nuestra fuentes hídricas". [(También puede leer: Campesinos del sur de Córdoba no permitirán fumigación con glifosato ni erradicación forzada)](https://archivo.contagioradio.com/campesinos-del-sur-de-cordoba-no-permitiran-fumigacion-con-glifosato-ni-erradicacion-forzada/)

### Volver

Como representante de asistencia técnica del PNIS y también como dirigente de ASIMTRACAMPIC,  Maydany tiene bajo su responsabilidad a otras 24 personas que trabajan por 732 personas que hacen parte de la sustitución voluntaria y otras  693 familias que esperan ingresar a este plan.

"Me duele haber salido de Piamonte, quiero volver porque necesito organizar las cosas, continúo haciendo mis tareas desde acá pero quiero saber cuándo puedo regresar a mi municipio, ¿cuando puedo volver a estar entre mi campesinado?".  [(Le puede interesar: Amenazan organización que lidera la sustitución de cultivos en Piamonte, Cauca)](https://archivo.contagioradio.com/amenazan-organizacion-que-lidera-la-sustitucion-de-cultivos-en-piamonte-cauca/)

Pese a que se ha intentado contactar con los integrantes de la Policía que advirtieron sobre las amenazas hacia la lideresa, no ha sido posible obtener más información sobre el origen de la información.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39186787" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39186787_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
