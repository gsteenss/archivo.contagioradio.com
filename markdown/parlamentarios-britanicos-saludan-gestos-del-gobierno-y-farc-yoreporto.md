Title: Parlamentarios Británicos saludan gestos del Gobierno y FARC #YoReporto
Date: 2015-07-16 16:29
Category: Nacional, Paz
Tags: Cese Bilateral de Fuego, Conversaciones de paz de la habana, ELN, FARC, Juan Manuel Santos, Justice for Colombia, Mariela Kohon, Parlamento Británico
Slug: parlamentarios-britanicos-saludan-gestos-del-gobierno-y-farc-yoreporto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/westminster-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [tn.com.ar]

###### [16 Jul 2015] 

26 miembros del **Parlamento del Reino Unido** de cuatro partidos han saludado el reciente comunicado del Gobierno de Colombia y las FARC que les compromete a tomar los pasos para el desescalamiento del conflicto en Colombia. Los parlamentarios saludaron el anuncio de las FARC de un **cese al fuego unilateral** que comenzará el día 20 de julio y la respuesta positiva del Gobierno. También enfatizaron su apoyo por la implementación de un cese bilateral de fuego lo más pronto posible. Abajo se puede leer el comunicado y los firmantes.

Declaración:

"Nosotros, los abajo firmantes, saludamos el comunicado conjunto hecho por los negociadores del Gobierno de Colombia y las FARC el Domingo 12 de Julio donde dejan claro la intención de tomar pasos para crear confianza y desescalar el conflicto, y para crear las condiciones para un eventual cese bilateral de fuego. Esto sigue el anuncio de las FARC de un cese al fuego unilateral empezando el 20 de Julio, el cual también recibimos con entusiasmo.

Es alentador que el Gobierno ha acordado responder al gesto de las FARC con pasos para desescalar el conflicto. Creemos que es imprescindible que las partes puedan acordar un cese bilateral de fuego lo más pronto posible. Creemos que esto ayudará a crear las condiciones necesarias para una conclusión exitosa a las conversaciones, evitar más bajas, y prevenir que la población sufra más como resultado del conflicto.

Seguiremos ofreciendo nuestro apoyo en cualquier manera que pueda contribuir a llegar a un acuerdo de paz exitoso. **Esperamos también que el Gobierno y el ELN inicien pronto unas conversaciones de paz formales**. Reiteramos nuestro firme compromiso de apoyar a las partes y la sociedad civil en su búsqueda de la paz.

Firmado:

Dave Anderson MP, Richard Burgon MP, Micky Brady MP, John Cryer MP, Nic Dakin MP, Pat Doherty MP, Jeffrey Donaldson MP, Rob Flello MP, Mike Gapes MP, Fabian Hamilton MP, Stephen Hepburn MP, Kelvin Hopkins MP, Helen Jones MP, Ian Lavery MP, Danny Kinahan MP, John McDonnell MP, Francie Malloy MP, Paul Maskey MP, Chris Matheson MP, Ian Mearns MP, Madeleine Moon MP, Grahame Morris, Gavin Robinson MP, Jo Stevens MP, Tom Watson MP, Jim Shannon MP."

**Por Mariela Kohon, Jutice for Colombia**
