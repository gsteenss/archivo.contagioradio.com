Title: Asesinan a Jhon  Montaño, hijo de excombatiente de FARC en Algeciras, Huila
Date: 2020-06-02 20:42
Author: CtgAdm
Category: Actualidad, DDHH
Tags: asesinato de excombatientes, FARC, Huila
Slug: asesinan-a-jhon-montano-hijo-de-excombatiente-de-farc-en-algeciras-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Foto: FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Jhon Carlos Montaño fue asesinado el pasado domingo 31 de mayo después de ser víctimas de dos disparos con arma de fuego en zona rural del municipio de Algeciras, Huila. **El joven de 19 años era hijo de José Montaño, excombatiente de FARC** quien ya había sido víctima de un atentado contra su vida. [(Le puede interesar: El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes)](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este hecho se suma a los más de **200 asesinatos de los que han sido víctimas, firmantes del Acuerdo de Paz y sus familiares** a nivel nacional y particularmente en el departamento del Huila donde apenas en la última semana se registraron los **homicidios de Herney Betancourt Ortiz y Manuel Olaya Arias**, también excombatientes, quienes fueron asesinados en este departamento. [(Le recomendamos leer: Fue asesinado Rigoberto García, firmante de paz)](https://archivo.contagioradio.com/fue-asesinado-rigoberto-garcia-firmante-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Familiares de excombatientes de FARC también son víctimas de ataques

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Un hecho similar se presentó el año pasado con el asesinato de Manuel Antonio González el pasado 13 de diciembre de 2019, Manuel fue frimante de la paz e hijo de Élmer Arrieta, quien fuera comandante del frente 18 en la antigua guerrilla y quien en la actualidad es uno de los líderes más reconocidos de los excombatientes que habitan en el espacio territorial de Santa Lucia en Ituango, Antioquia. [(Lea también: Antonio Gallego Mesa, exguerrillero de las FARC es asesinado en La Macarena, Meta)](https://archivo.contagioradio.com/antonio-gallego-mesa-exguerrillero-de-las-farc-es-asesinado-en-la-macarena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El [partido](https://twitter.com/PartidoFARC) FARC viene denunciando estos hechos que implican para el colectivo, un **“exterminio sistemático”**, es por ello que la semana pasada a través de un encuentro virtual solicitaron a la Comisión Interamericana de Derechos Humanos (CIDH) medidas cautelares para proteger a más de 10.000 excombatientes que se están reincorporando a la vida civil. [(Lea también: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes)](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
