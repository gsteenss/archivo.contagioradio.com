Title: Gobierno le hace 'conejo' a las FARC: Andrés París
Date: 2017-08-10 15:58
Category: Entrevistas, Paz
Tags: acuerdos de paz, Andrés París, Entrega de bienes de las FARC, FARC
Slug: gobierno-le-hace-conejo-a-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Andres-paris.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [10 Ago. 2017] 

¿Cómo va el retorno de los guerrilleros y guerrilleras a la vida civil? ¿Qué ha pasado con la salida de guerrilleros y guerrilleras de las cárceles en Colombia? ¿Cuáles son los bienes que entregará las FARC? Son preguntas que surgen a propósito de los 9 meses de implementación de los acuerdos de paz en el país y que **Andrés París, integrante de las FARC** resume diciendo que el Gobierno ha estado haciéndoles 'conejo' .

### **Dos mil guerrilleros siguen sin salir de las cárceles** 

Aunque la huelga de hambre de los guerrilleros y guerrilleras de las FARC, que se extendió por 22 días se dio por terminada luego de un acuerdo entre el Gobierno Nacional y esa guerrilla, a la fecha no se ha cumplido la salida de 2 mil guerrilleros que están en las cárceles. En contexto: [Estado sigue sin cumplir ley de amnistía a 22 días de huelga de hambre de presos políticos de las FARC](https://archivo.contagioradio.com/presos-politicos-de-las-farc/)

“Han salido pequeños grupos en forma gradual pero el 1 de agosto que se había establecido como fecha límite pues no se cumplió la salida de los guerrilleros. **Estamos ante un Estado que funciona así para todos los colombianos** y está vez nos tocó a nosotros padecer como Jesucristo en calvario”.

### **¿Y las Zonas Veredales qué?** 

Muchos han sido los tropiezos que han encontrados los y las guerrilleras al llegar a las Zonas Veredales Transitorias de Normalización, aún les falta ser adecuadas para vivir de manera digna en ellas “pareciera que la tendencia del Gobierno es que como ya se acaba la vigencia de estas Zonas ya no van a seguir construyéndolas”.

**Dichas zonas pasarán a ser "aldeas de paz" motivo por el cual deberá velarse por su mantenimiento por varios años** en tanto que los ex combatientes estarán allí en su proceso de tránsito a la vida civil “la denominación jurídica implica los compromisos del Estado con esas Zonas (…) aunque el Gobierno viene haciendo 'conejo' en toda la implementación”.

En cuanto a los terrenos, para cada Zona Veredal habrá una forma jurídica específica y las FARC esperan que desde el Gobierno pueda adquirir dichos terrenos de modo que se permita a los insurgentes hacer el tránsito. De lo contrario, la propuesta es encontrar áreas cercanas donde se puedan desarrollar proyectos productivos en favor de los y las guerrilleras.  Le puede interesar: ["Hay un 80% de incumplimientos por parte de gobierno en Zonas Veredales: FARC"](https://archivo.contagioradio.com/gobierno-ha-incumplido-en-un-80/)

### **¿Estaciones de Policía en las Aldeas de Paz?** 

Luego de la propuesta del gobernador de Antioquia, Luis Pérez Gutiérrez, en el que sugirió se instalarán estaciones de Policía, París afirma que hay que estudiar la propuesta **“Seguramente no será muy conveniente contar con estaciones de Policía enclavadas en el centro de las Aldeas**, pero si hay que crear mecanismos con el Gobierno, con la Policía, con el Ejército que permita proteger a las concentraciones de ex combatientes”.

### **Extracción de las caletas y contenedores de armas de las FARC** 

Luego del percance presentado en días pasados en los que un grupo de integrantes de la ONU sufrió un atentado mientras realizaba labores de extracción de caletas en Cauca, París dice que ese hecho es muestra de la desatención en materia de seguridad incluso para los integrantes del organismo internacional. Le puede interesar: [Dejación de armas de las FARC –EP marca un cambio en la historia del país](https://archivo.contagioradio.com/42169/)

**“Estamos interesados que este trámite de entrega de armas y caletas se surta de la mejor manera.** Porque hemos cumplido, se debe garantizar con la seguridad del traslado de los container. Nosotros queremos entregar las armas porque así aceleramos todos los requisitos que se han firmado para nuestra participación activa en política”.

### **Proyectos educativos para las FARC** 

“No hay nada significativo en ese terreno por parte del Gobierno” asevera Paris, quien agrega que lo que quieren hacer es que los ex combatientes se vuelquen al mercado y sean parte del rebusque y la supervivencia, sin condiciones mínimas de proyección. Le puede interesar: [Zonas Veredales se convertirán en espacios de estudio y salones de clase](https://archivo.contagioradio.com/zonas-veredales-se-convertiran-en-zonas-de-estudio-y-salones-de-clase/)

### **¿Extinción de domino para los bienes de las FARC?** 

Ante las declaraciones entregadas por el Fiscal General, Néstor Humberto Martínez en las que asegura que las FARC podría hacer ‘conejo’ en la entrega de bienes y por tal motivo debería hacerse extinción de dominio, París asegura que esta postura tiene el propósito de sabotear el proceso de reincorporación y de paz.

**“Al Fiscal se le ve mala voluntad, él no puede disponer de esos medios en tanto que las FARC no somos una figura jurídica,** no puede darnos trato de narcotraficantes. El señor Fiscal sueña con vernos en la cárcel, pero primero se va él para la cárcel por el caso Odebretch que cualquier guerrillero imputado falsamente por la Fiscalía”.

### **¿Cuáles son realmente los bienes de las FARC?** 

Pequeñas fincas, algunos animales que se requerían para la alimentación, mano de obra o construcción de vías son algunos de los bienes que se relacionan en la entrega de los y a los cuales además le otorgaron un valor económico.

**“Estos bienes se adquirieron lícitamente** y eso también desdice la calumnia de que las FARC expropiaban a los campesinos, jamás tomamos de los campesinos una aguja”. Le puede interesar: [Precariedad, corrupción e inseguridad flagelan las zonas veredales](https://archivo.contagioradio.com/precariedad-corrupcion-e-inseguridad-flagelan-las-zonas-veredales/)

### **Ex guerrilleros de las FARC serán escoltas de la Unidad Nacional de Protección** 

Otro de los compromisos que se adquirió en el Acuerdo de Paz, es que los integrantes de las FARC que deseen pueden convertirse en escoltas y brindar seguridad a los integrantes de esa guerrilla. De ese proyecto de ley solamente queda pendiente la sanción presidencial, luego de ser aprobado en el Senado.

“En eso se ha venido avanzando, pero muy lentamente, superando todos los tropiezos que establecieron en la Corte Constitucional. **Ya salen los primeros 300 escoltas a incorporarse a los esquemas de seguridad acordados con el Gobierno** y se prepara el arribo de otro grupo que recibirá entrenamiento”.

### **Las FARC lista para constituir su nuevo partido político** 

París dice además que pese a la estrategia Uribista del miedo que han sembrado algunas personas como Plinio Apuleyo a través de sus columnas de opinión y de toda la matriz mediática que usan los que dicen que Santos le entregó el país a las FARC, la creación del nuevo partido político de esa guerrilla va y que luego del Congreso constitutivo darán a conocer las líneas bajo las cuales trabajarán.

“La forma como Apuleyo Mendoza presenta al nuevo partido político está dirigido a escandalizar a los colombianos con una matriz de guerra. Este es uno de los señores que no se ha dado cuenta que en Colombia se firmó la Paz. Apuleyo habla una vez más a nombre de los guerreristas de Colombia y del mundo”. Le puede interesar: [Partido de las FARC empezará vida política el primero de septiembre](https://archivo.contagioradio.com/partido-de-las-farc-empezara-vida-politica-el-primero-de-septiembre/)

El Congreso Constitutivo se realizará el próximo 26 de agosto en Bogotá y en el participarán Pablo Catatumbo, Iván Márquez, Pastor Alape entre otros integrantes de esa guerrilla.

<iframe id="audio_20273714" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20273714_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
