Title: Multitudinarias movilizaciones en Brasil rechazan intentos de golpe de Estado
Date: 2016-03-18 19:25
Category: El mundo, Otra Mirada
Tags: Brasil de Fato, Brasil. Lula Da Silva, Gobierno de Dilma Rousseff, Golpe de Estado en Brasil
Slug: multitudinarias-movilizaciones-en-brasil-rechazan-intentos-de-golpe-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Manifestaciones-en-Brasil-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brasil de Fato] 

<iframe src="http://co.ivoox.com/es/player_ek_10882162_2_1.html?data=kpWlmpeVepOhhpywj5eVaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5ynca7pzdnW1trIrc%2FV087O1ZDRs9fdzc7nw8jNs8%2FZ1JDS0JCmtsLnytGY1MrHrMLuwtOYy9PYqc%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Vivian Fernandes, Brasil de Fato] 

###### [18 mar 2016] 

Hoy, cientos de  miles de personas se movilizan en Brasil para defender la democracia. Según diversos movimientos sociales, el intento de golpe de Estado en contra del gobierno  y la serie de acciones emprendidas [por sectores de la derecha que pretenden la salida de Dilma Rouseff](https://archivo.contagioradio.com/hasta-la-derecha-de-brasil-considera-ridiculo-pedir-detencion-de-lula/) **han empujado a las calles a las personas que no quieren que se repitan gobiernos alejados de los sectores populares.**

Para Vivian Fernandes, integrante de Brasil de Fato, si bien, el gobierno del partido de los trabajadores no realizó cambios profundos, si se lograron algunos acuerdos importantes que no se lograron con ningún otro gobierno desde hace casi 40 años en ese país. **Además desde algunos sectores de la derecha se está exigiendo la renuncia de Rouseff pero el regreso de gobiernos parecidos a las dictaduras.**

Las multitudinarias movilizaciones de defensa de la democracia que se realizaron este 18 de Marzo obedecen a que en la gente hay una conciencia de que un proceso de golpe de Estado está en desarrollo. Vivian Fernandes asegura que hay tres formas que históricamente han servido para derrocar presidentes en ese país, una presionar una renuncia **a través de movilizaciones de calle y mensajes muy fuertes en medios de información**, otra, presionar un juicio político y otra un golpe militar, y las tres formas están en proceso en este momento.

Fernandes agrega que es muy evidente la intensión de la derecha, puesto que ya han sido derrotados en 4 ocasiones en las urnas y no están dispuestos a perder una elección más. En el caso de las elecciones del 2018 el ex presidente Lula podría ser un firme candidato con amplio respaldo y por ello, aunque los organismos justicia no tienen podrían comprobar la responsabilidad de Rouseff o Lula en los escándalos de corrupción, tanto los medios como algunos personajes de la fiscalía y de los aparatos de justicia ya dan por responsable al expresidente.

Respecto de las movilizaciones de hoy, que estaban planificadas para el 31 de Marzo en el aniversario del golpe militar, y que se adelantaron por la coyuntura, hay reportes de Recife con más de 20 mil personas, **Fortaleza con más de 10 mil, Salvador con 20 mil personas y Sao Paulo con 100 mil, se espera que en los próximos días se sigan realizando manifestaciones de defensa de la democracia.**

En redes sociales se han respaldado las movilizaciones con la etiqueta \#VemPraDemocracia y se pueden seguir las rutas y el discurso del expresidente Lula a través de la página [http://cobertura.brasildefato.com.br](http://cobertura.brasildefato.com.br/).

\
