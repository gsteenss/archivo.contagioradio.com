Title: Enfoque de Género no se negocia
Date: 2016-10-12 15:27
Category: Mujer, Nacional
Tags: Comunidad LGBTI, enfoque de género en los acuerdos de paz, mujeres
Slug: enfoque-de-genero-no-se-negocia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mujeres-colombianas-desnudas-protestando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 de Oct 2016] 

Después de la reunión que sostuvieron el presidente Santos y el ex procurador Ordoñez este último afirmó que "va a purgar los acuerdos de paz de la ideología de género", sin embargo para Ana Teresa Bernal, este hecho **atenta en contra de los derechos fundamentales y no es posible debido a que el enfoque de género hace parte del bloque constitucional de los acuerdos de paz. **

Las modificaciones serían hechas por uno de los sectores más conservadores del país, que abanderaron el No durante el plebiscito por la paz, para Ana Teresa Bernal, miembro de un millón de mujeres por la paz y defensora de derechos humanos, **esa propuesta no cabe, es una postura totalmente contraria a los que es la paz**, “yo creo que la paz es la síntesis de los derechos de las personas, por lo tanto el enfoque de género debe sostenerse”. Lea También: ["Subcomisión de Género presentará sus resultados el próximo domingo"](https://archivo.contagioradio.com/subcomision-de-genero-presentara-sus-resultados-el-proximo-domingo/)

“**El presidente no puede negociar algo que existe por derecho fundamental en la constitución**, Santos debe mantenerse porque el enfoque de género esta integrado a todo el bloque de constitucional colombiana y por lo tanto esta integrado al acuerdo pactado con la guerrilla de las FARC-EP" afirmó Bernal.

Por otro lado, Víctoria Sandino, miembro de la delegación de paz de la Habana, expresó que **no están dispuestos a retroceder frente al enfoque de género** que pretende velar por los derechos de las mujeres y la comunidad LGTB, que son más de la mitad de la población del país. Argumento que se podría generar una pedagogía más amplia de lo que es el enfoque de género pero no excluirlo. Lea también ["Incorporación del Enfoque de Género: un hecho histórico que ningún proceso de paz ha logrado"](https://archivo.contagioradio.com/incorporacion-del-enfoque-de-genero-un-hecho-historico-que-ningun-proceso-de-paz-ha-logrado/)

Para Ana Teresa Bernal, el enfoque de género surge a raíz de las diferencias que tienen las personas y la necesidad de proteger los derechos de la diversidad, sin embargo considera que el término “ideología de género” se usó para atemorizar a las familias bajo la falsa premisa de atentar en contra de la familia. De igual forma aseguro que **el paso siguiente es aclarar el concepto de enfoque de género, después de las informaciones mal intencionadas de la campaña del NO.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

<iframe id="audio_13290387" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13290387_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
