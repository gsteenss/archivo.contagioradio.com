Title: Víctimas piden "toda la verdad" a reclusos que se acogerían al SIVJRNR
Date: 2019-02-15 17:58
Author: AdminContagio
Category: Comunidad, Paz
Tags: carceles, JEP, verdad, víctimas
Slug: victimas-verdad-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/victimas-la-chinita-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Feb 2019] 

Tras conocerse la carta de más de 300 personas detenidas en diferentes cárceles del país que integraron diferentes grupos armados al margen de la Ley en la que manifiestan su intención de aportar a la verdad sobre el conflicto, víctimas reconocieron la trascendencia de esta comunicación y pidieron al **Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR)**, revisar los testimonios de los internos firmantes.

Las víctimas aseguran que tras superar sus temores a hablar y pronunciarse sobre el asunto, decidieron elevar su petición al Sistema para que estudie los testimonios de los 300 convictos quienes podrían tener informaciones precisas, "con verdades desconocidas y sustento probatorio contundente" para esclarecer hechos ocurridos en el conflicto armado. (Le puede interesar: ["Actores del conflicto listos para decir la verdad desde las cárceles"](https://archivo.contagioradio.com/la-verdad-esta-lista-para-salir-de-la-carcel/))

En la comunicación de las víctimas también se expresa la preocupación ante la impunidad y ausencia de verdad para las víctimas en el marco de la justicia ordinaria y la Ley 975 de 2005 o Ley de Justicia y Paz; razón por la que reiteran su apoyo al Sistema Integral, y creen que la declaración de los prisioneros, así como los actos que le sigan a la misma en torno a aportar verdad generan la confianza que requiere este momento histórico.

[**Martha Aguirre, presidenta de la Fundación Sonrisas de Colores**, una organización que ha venido trabajando por la paz y las víctimas del conflicto  desde hace 20 años señala que la JEP debe tener la seguridad de decidir si esta petición es seria y responsable además de que las verdades que puedan surgir de este proceso sean satisfactorias para las víctimas.]

Por último, las víctimas piden que tomando en consideración los riesgos para quienes cuenten la verdad y sus familias sus vidas sean protegidas, y resaltan que la cárcel para perpetradores está lejos de restituir los derechos de quienes sobrevivieron al conflicto, razón por la que se hace necesario que la verdad, escondida en los sitios de reclusión, salga a la luz y permita avanzar en la restauración y reparación.

### **Rito Alejo del Río debe decir toda la verdad** 

Por otra parte, los integrantes de CAVIDA, víctimas de la operación Génesis aseguraron que si el General Rito Alejo del Río manifiesta que no va a seguir callando tiene que decir toda la verdad acerca que quiénes le dieron las órdenes para cometer los crímenes y a quiénes se beneficiaban de ese tipo de operaciones mientras fue comandante de la brigada XVII en el Urabá.

El video en que del Río asegura que los delincuentes no son los militares sino quienes dieron las órdenes se conoce justo antes de que las víctimas de la Operación Génesis en 1997 presenten ante la JEP y la CEV su propuesta de Justicia Restaurativa en el aniversario de dicha acción conjunta ocurrida entre el 27 y 28 de Febrero de 1997.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
