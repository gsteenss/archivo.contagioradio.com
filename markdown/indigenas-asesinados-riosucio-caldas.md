Title: No paran asesinatos de líderes indígenas en Riosucio, Caldas
Date: 2018-12-05 12:23
Author: AdminContagio
Category: Nacional
Tags: Asesinados, Caldas, Comunero, Riosucio
Slug: indigenas-asesinados-riosucio-caldas
Status: published

###### Foto: La Patria 

###### 5 Dic 2018 

El Consejo Regional Indígena de Caldas, CRIDEC, denunció que **el día martes 4 de diciembre fue asesinado el comunero Edison de Jesús Naranjo Navarro**, perteneciente a la comunidad de El oro, resguardo indígena Nuestra Señora La Candelaria de la montaña, del municipio de Riosucio.

Según el comunicado compartido por la organización, **Naranjo de 41 años de edad, fue asesinado cerca de las 8 de la mañana tras recibir un impacto de bala a la altura del torax**, mientras se dirigía en su motocicleta a la planta de refrigeración y acopio de leche de la comunidad de El oro.

La información da cuenta de que el comunero **había denunciado con anterioridad amenazas en contra de su vida**. Naranjo era esposo de una de las hijas de la gobernadora del resguardo indígena Cañamomo Lomaprieta, Amobia Moreno Andica. (Le puede interesar: [Familia Embera es asesinada en Riosucio, Caldas](https://archivo.contagioradio.com/familia-embera-es-asesinada-en-riosucio-caldas/))

Las autoridades del resguardo como desde el CRIDEC, exigen al gobierno nacional se designe un **Fiscal encargado de investigar los hechos**, así como la muerte de tres comuneros indígenas de San Lorenzo ocurrida el 23 de noviembre, de una comunera de 73 años ocurrido el 2 de diciembre en el resguardo Escopetera y Pirza, todos del mismo municipio de Riosucio.

De igual forma, exigen que **se brinden garantías de protección a la vida e integridad de las comunidades indígenas del Departamento**, frente a la fuerte presencia de actores armados y hechos de violencia que ponen en peligro su pervivencia física y cultural, y el respeto a sus derechos humanos vulnerados de manera sistemática.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

<div class="osd-sms-title">

</div>

</div>
