Title: Definición de bosques de la FAO amenaza al ambiente y legitima los monocultivos
Date: 2015-12-21 11:49
Category: Ambiente, Nacional
Tags: cambio climatico, FAO, Monocultivos
Slug: definicion-de-bosques-de-la-fao-amenaza-al-ambiente-y-legitima-las-plantaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Monocultivos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Entre el agua y el aceite de Palma 

###### [21 Dic 2015]

La organización Salva la Selva, denunció a través de su sitio web, que la nueva definición de bosques de la Organización de las Naciones Unidas para la Agricultura y Alimentación, FAO, **permite la deforestación con el fin de destinar esos territorios para las plantaciones de monocultivos industriales de árboles. **

Durante el Congreso Forestal Mundial en Durban sobre el 'futuro sostenible' de los bosques, fue  donde se redefinió el concepto, de tal manera que, de acuerdo con Salva la selva, **la FAO “se niega a definir los bosques por su diversidad biológica, social, cultural y espiritual** mientras promueve la expansión de plantaciones industriales de árboles a gran escala a expensas de comunidades y bosques reales”.

Es así como la FAO define los bosques como una simple superficie cubierta de árboles, sin tener en cuenta que los bosques naturales albergan innumerables especies animales y vegetales, razón por la cual es necesaria su protección y recuperación, en el marco de las medidas que se definieron en la COP21 para enfrentar el cambio climático.

“Así, **se pueden arrasar bosques y reemplazarlos por plantaciones de caucho, o cortar bosques templados o subtropicales altamente biodiversos para dejar sitio a plantaciones estériles** de pinos o eucaliptos –y nada de esto se clasificaría como 'deforestación neta'. Si la industria arranca pastizales o toma y convierte tierras campesinas en monocultivos industriales de árboles o desiertos verdes, eso para la FAO es reforestación”, señala la organización ambientalista, que a su vez afirma que con esta definición se promueven falsas soluciones a la crisis climática al considerar los bosques como meros almacenes de carbono.

Cabe recordar el caso de Mapiripán en Colombia, donde la empresa Poligrow tiene un proyecto de plantación de 70 mil hectáreas de **palma aceitera** que más adelante podrían generar sequías como las que conoció Colombia en el año 2014 en Paz de Ariporo, y que ocasionó la muerte de miles de chigüiros; ya que este tipo de monocultivos ** acaba con las palmas de moriche y las fuentes hídricas subterráneas.**

Grupos ecologistas, movimientos sociales y científicos rechazan la definición  de bosques de la FAO, y piensan confrontarla el próximo año en el marco del **Congreso Forestal Mundial,** donde se discutirá las causas reales de la destrucción de los bosques y sus responsables. Así mismo, las organizaciones defensoras del ambiente están reuniendo firmas a través la iniciativa [¡Las plantaciones NO son bosques!](https://www.salvalaselva.org/peticion/1013/las-plantaciones-no-son-bosques?t=361) para evidenciar este rechazo frente al significado de bosques de las Naciones Unidas.
