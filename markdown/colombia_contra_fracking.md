Title: Ambientalistas colombianos se unen contra el Fracking
Date: 2017-08-25 15:48
Category: Ambiente, Nacional
Tags: fracking, san martin
Slug: colombia_contra_fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fracking-e1472854044186.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Silla Vacía 

###### [25 Ago 2017] 

Cerca de **70 organizaciones y colectivos ambientalistas del país han desarrollado la primera Asamblea de la Alianza Colombia Libre de Fracking**, en el marco de del Cuarto Foro “Los impactos del fracking en los territorios” realizado en la ciudad de Barrancabermeja este 24 y 25 de Agosto.

Se trata de un movimiento que nace a partir de la negativa de las comunidades hacia el fracking, entre ellos, los habitantes de San Martín, Cesar, que señalan de “absolutamente irresponsable con los Colombianos”, la reciente decisión del Ministerio de Ambiente de permitir la explotación con dicha práctica. [(Le puede interesar: Ministerio de Ambiente da vía libre al Fracking)](https://archivo.contagioradio.com/fracking/)

**“No aceptamos chantajes ni falsos dilemas de la industria que pretende vender el fracking como la única opción para el país”,** han manifestado los durante el encuentro,  en que también han solicitado al Gobierno fortalecer otras actividades económicas como la agrícola o el ecoturismo, y además exigen que se avance en el desarrollo de energías renovables en Colombia.

Ante dicha situación, han expresado que la Alianza “será el inicio de un proceso de movilización social amplio y diverso que buscará sumar a muchos más actores y aliados”, para que a una sola voz la sociedad Colombiana le exija al Gobierno Nacional “la suspensión de los contratos de exploración y explotación de hidrocarburos en Yacimientos No Convencionales que actualmente están vigentes, así como cualquier proyecto que con esta técnica se piense desarrollar en Colombia.” [(Le puede interesar: Fracking amenaza abastecimiento de agua de Bogotá)](https://archivo.contagioradio.com/fracking-amenaza-abastecimiento-agua-bogota/)

### Insisten en el principio de precaución 

De acuerdo a los voceros de la Alianza, “la evidencia científica existente está demostrando graves e irreversibles daños que han motivado fuertes protestas y restricciones en otras regiones del mundo donde se está implementando el fracking” por lo que “consideramos necesario que el Gobierno colombiano aplique el principio de precaución, y por lo tanto implemente medidas urgentes para prevenir los potenciale**s impactos ambientales y sobre la salud pública que puedan ser causados por el fracking”**, y se declare la moratoria al  en perspectiva de prohibir su aplicación en todo el país. [(Le puede interesar: las principales consecuencias del fracking)](https://archivo.contagioradio.com/las-principales-consecuencias-que-podria-haber-en-colombia-si-se-practica-fracking/)

<iframe id="audio_20525993" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20525993_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
