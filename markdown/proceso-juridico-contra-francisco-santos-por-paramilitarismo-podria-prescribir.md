Title: Proceso jurídico contra Francisco Santos por paramilitarismo podría prescribir
Date: 2015-02-18 19:22
Author: CtgAdm
Category: Judicial, Nacional
Tags: Bogotá, Francisco Santos, Paramilitarismo
Slug: proceso-juridico-contra-francisco-santos-por-paramilitarismo-podria-prescribir
Status: published

###### Foto: AFP 

<iframe src="http://www.ivoox.com/player_ek_4102142_2_1.html?data=lZadlJaYdo6ZmKiakpqJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjxMrg0ZDOudOZpJiSo6nIrcTjjMjc0NnWpYy608bbxc7Xp9CftMbb1tTXb9Hj05Ddw9fFscrgytmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sebastián Escobar, Comisión Colombiana de Juristas] 

La **Comisión Colombiana de Juristas** presentó una recusación en contra de la Fiscalía, por su inacción en la investigación en el caso contra **Francisco Santos por posibles vínculos con el paramilitarismo en Bogotá.**

Para la Comisión, es -cuando menos- inquietante que la **Fiscal Maria Claudia Merchán**, quien lleva el caso en contra de Francisco Santos, no se haya pronunciado ni haya adelantado el proceso en más de 1 año y medio. Según lo expuesto por los abogados, esta misma Fiscal fue la responsable de absolver a Jairo Enrique Merlano Fernández el juicio por parapolítica el 19 de junio del 2008, mientas era juez tercera especializada de Bogotá. Este es un antecedente que indicaría que la independencia de la rama judicial está comprometida en la investigación contra el **vicepresidente el gobierno de Álvaro Uribe.**

El peligro que encierra la inacción por parte de la Fiscal Merchán, es que el proceso contra Francisco Santos prescriba por vencimiento de términos, dejando en la impunidad "hechos graves que han generado impactos en la sociedad colombiana, y que merecen ser conocidos", señaló el abogado Sebastián Escobar.

"**Nosotros queremos que la Fiscalía se comprometa con la investigación**", enfatizó el abogado. "Si esta de moda que el Centro Democrático llame la atención diciendo que existe una persecución política contra su partido, y no han escatimado en señalar a la Fiscalía como un ente que los ha perseguido, este caso demuestra todo lo contrario, pues se encuentra en los anaqueles de la justicia".

La Fiscalía tiene 5 días hábiles para entregar respuesta ante esta recusación interpuesta.
