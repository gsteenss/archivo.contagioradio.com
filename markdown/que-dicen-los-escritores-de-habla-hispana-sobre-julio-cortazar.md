Title: ¿Qué dicen los escritores de habla hispana sobre Julio Cortázar?
Date: 2015-02-12 11:00
Author: CtgAdm
Category: Cultura, Viaje Literario
Tags: conmemoracion Cortazar, cortazar, Mario Benedetti, Rosa Montero
Slug: que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/Cortazar-Perec-bienal-grandes_CLAIMA20120320_0105_4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div>

###### 12 Feb 2016. 

Al conmemorarse 32 años de la muerte de Julio Cortázar,  uno de los escritores más destacados de la literatura latinoamericana y universal, recordamos las palabras de grandes colegas y amigos que han opinado sobre el hombre y la escritura del autor de Rayuela, La vuelta al día en ochenta mundos, Historias de cronopios y de famas entre muchas otras.

### [**Garcia Márquez sobre Cortázar**] 

[**[![cortazar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/cortazar.jpg)

  “Era el hombre más alto que se podía imaginar, con una cara de niño perverso dentro de un interminable abrigo negro que más bien parecía la sotana de un viudo, y tenía los ojos muy separados, como los de un novillo, y tan oblicuos y diáfanos que habrían podido ser los del diablo si no hubieran estado sometidos al dominio del corazón”, describía el joven periodista Gabriel García Márquez

</div>

### **Borges sobre  Cortázar** 

Prólogo a “Cartas de mamá”  
Hacia 1947 yo era secretario de redacción de una revista casi secreta que dirigía la señora Sarah de Ortiz Basualdo. Una tarde, nos visitó un muchacho muy alto con un previsible manuscrito. No recuerdo su cara; la ceguera es cómplice del olvido. Me dijo que traía un cuento fantástico y solicitó mi opinión. Le pedí que volviera a los diez días. Antes del plazo señalado, volvió. Le dije que tenía dos noticias. Una, que el manuscrito estaba en la imprenta; otra, que lo ilustraría mi hermana Norah, a quien le había gustado mucho. El cuento, ahora justamente famoso, era el que se titula “Casa Tomada”. Años después, en París, Julio Cortázar me recordó ese antiguo episodio y me confió que era la primera vez que veía un texto suyo en letras de molde. Esa circunstancia me honra. Muy poco sé de las letras contemporáneas. Creo que podemos conocer el pasado, siquiera de un modo simbólico, y que podemos imaginar el futuro, según el temor o la fe; en el presente hay demasiadas cosas para que nos sea dado descifrarlas. El porvenir sabrá lo que hoy no sabemos y cursará las páginas que merecen ser releídas. Schopenhauer aconsejaba que, para no exponernos al azar; sólo leyéramos los libros que ya hubieran cumplido cien años. No siempre he sido fiel a ese cauteloso dictamen; he leído con singular agrado Las armas secretas de Julio Cortázar y sus cuentos, como aquel que publiqué en la década del cuarenta, me han parecido magníficos. “Cartas de mamá”, el primero del volumen, me ha impresionado hondamente. Una historia fantástica, según Wells, debe admitir un solo hecho fantástico para que la imaginación del lector la acepte fácilmente. Esta prudencia corresponde al escéptico siglo diecinueve, no al tiempo que soñó las cosmogonías o el Libro de las Mil y Una Noches. En “Cartas de Mamá” lo trivial, lo necesariamente trivial, está en el título, en el proceder de los personajes y en la mención continua de marcas de cigarrillos o de estaciones del subterráneo. El prodigio requiere esos pormenores. Otro rasgo quiero indicar. Lo sobrenatural, en este admirable relato, no se declara, se insinúa, lo cual le da más fuerza, como en el “Izur” de Lugones. Queda la posibilidad de que todo sea una alucinación de la culpa. Alguien que parecía inofensivo vuelve atrozmente. Julio Cortázar ha sido condenado, o aprobado, por sus opiniones políticas. Fuera de la ética, entiendo que las opiniones de un hombre suelen ser superficiales y efímeras.  
Buenos Aires, 1984.

### **Mario Benedetti sobre Cortázar** 

Julio lo conocí en París, creo que en 1966, en casa de Darwin Flakoll y Claribel Alegría, amigos comunes. Desde el pique me pareció un tipo entrañable, sin falsas modestias ni caricaturas de vanidad. El posterior conocimiento, el trabajo conjunto y las muchas horas de conversación mantenidas a través de los años en diversos puntos del conturbado planeta, me han confirmado su actitud generosa, su sincera y eficaz militancia en defensa de las conquistas revolucionarias de Cuba y Nicaragua.

### **Osvaldo Bayer sobre Cortázar ** 

El ciudadano Julio Cortázar, el bondadoso. Cuántas veces nos vimos en la casa de Soriano, en ese París. Cortázar, que se nos fue hace veinte años, era el hombre del bolsillo abierto, con el corazón en esa América latina de los Sandino y los Zapata. Nos llenó de letras mostrándonos nuevos caminos e interminables sueños e ilusiones en sus libros irrepetibles. Cortázar terminó en la pureza corroborada por el hecho de que el presidente de la Rosada no lo recibió. A Cortázar, el puro. Me acuerdo del último encuentro, cómo acariciabas a esa muchacha, tu amor. Tus ojos adolescentes revivían como si estuvieras jugando a la “Rayuela” y llegaras al cielo para siempre, acompañado.

### **Eduardo Galeano sobre Cortázar** 

### **[![galeano\_cortazar\_foto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/galeano_cortazar_foto-e1440615910647.jpg){.wp-image-12884 .size-full .aligncenter width="580" height="395"}](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/galeano_cortazar_foto/)**

"Julio Cortázar transitó desde el desaliento a la esperanza, y esto es lo que no le perdonaron quienes en su momento lo acusaron de ingenuo y hoy lloran lágrimas de cocodrilo porque se murió".

"Cortázar hombre viajó de la soledad a la solidaridad, y esto le posibilitó sentirse y ser cada vez más joven, otro de los puntos envidiados por muchos. Además logró tener todas las edades, con esa capacidad de asombro que es también capacidad de pasión".

"Nos unían cosas muy hondas que no eran solamente políticas en el sentido limitado que la gente le da a la palabra "política", sino una vocación compartida, que era también una vocación solidaria, una pasión de libertad compartida que en los dos pasaba por la magia".

"El mayor mérito literario de Cortázar es habernos ayudado a comprender hasta dónde es natural eso que llamamos sobrenatural. Julio incorporó a la vida cotidiana esas energías secretas que andan en el aire del modo más natural, más espontáneo. No hizo una literatura fantástica opuesta a una literatura de la realidad, sino que hizo una literatura de la realidad y, por lo tanto, hizo una literatura fantástica".

### **Carlos Fuentes sobre Cortázar** 

**[![bunuelFuentesCortazar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/bunuelFuentesCortazar.jpg){.size-full .wp-image-12883 width="800" height="448"}](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/bunuelfuentescortazar/)**

Para Cortázar, la realidad era mítica en este sentido : estaba también en el otro rostro de las cosas, el mínimo más allá de los sentidos, la ubicación invisible sólo porque no supimos alargar la mano a tiempo para tocar la presencia que contiene.

Por eso eran tan largos los ojos de Cortázar: miraban la realidad paralela, a la vuelta de la esquina; el vasto universo latente y sus pacientes tesoros, la contigüidad de los seres, la inminencia de formas que esperan ser convocadas por una palabra, un trazo de pincel, una melodía tarareada, un sueño.

El afuera y el adentro. Toda esta realidad en vísperas de manifestarse era la realidad revolucionaria de Cortázar. Sus posturas políticas y su arte poético se configuraban en una convicción, y ésta es que la imaginación, el arte, la forma estética, son revolucionarias, destruyen las convenciones muertas, nos enseñan a mirar, pensar o sentir de nuevo.

Cortázar era un surrealista en su intento tenaz de mantener unidas lo que él llamaba "La revolución de afuera y la revolución de adentro".

[![cortazar\_09\_b (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/cortazar_09_b-1.jpg){.aligncenter .size-full .wp-image-12888 width="1259" height="793"}](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/cortazar_09_b-1/)

### **Rosa Montero sobre Cortázar** 

Su físico era extraordinario como su mundo literario, porque Julio Cortázar era lo que se dice raro, con un cuerpo filiforme e interminable, provisto de accidentados saledizos: esos brazos que revoloteaban en su camino tronco abajo, esas piernas de arácnido que nunca acababan de plegarse, esos tobillos picudos y lamentables que se empeñaban en destacar por debajo de un pantalón definitivamente corto. Corría el mes de febrero de 1982, y el escritor estaba en Madrid con Carol, su mujer, para presentar un último libro de cuentos, "Queremos tanto a Glenda". Cortázar cruzó el restaurante en el que habíamos quedado con un desencuaderne acompasado, y se desplomó a cámara lenta en una silla, doblando las piernas con la misma parsimonia con que se iza un puente levadizo, mientras sus rodillas subían y subían hasta hacerse omnipresentes. Una vez conquistado el asiento, el hombre rebulló un instante, afinando su acomodo. Después abrió sus ojos verdes, pestañeó, sonrió complacido y rugió un poco. Y en ese momento le reconocí, comprendí que Cortázar era el ogro de mis cuentos infantiles, un ogro descabellado y bondadoso que fumaba puros y hablaba con frenillo, atracando sus erres en un rugido sin lugar a dudas amistoso. El escritor tenían entonces 66 años, pero no los representaba en absoluto. En realidad no parecía tener ninguna edad, porque los personajes fantásticos poseen una cronología diferente. "Yo me siento extremadamente joven y con la intención de vivir lo más posible", me decía el ogro Cortázar contemplando amorosamente a su princesa Carol, rubia, joven y guapa, como corresponde a las heroínas de los cuentos, la bella de esa entrañable bestia.  
Pero también los seres de ficción son acosados por el tiempo, y a los escasos meses de aquella entrevista, Carol falleció fulminantemente de leucemia, y al poco, el 12 de febrero de 1984, le siguió a la tumba su desolado y formidable monstruo, tras rendir a su princesa el supremo homenaje de elegir la misma enfermedad y la misma muerte. "Hay una cosa que no me preocupa del futuro", dijo Cortázar en aquella entrevista, cuando el cuento de hadas duraba y estaban los dos muy vivos y felices: "Y es la noción de la supervivencia literaria, el prestigio, la fama, lo que yo seré dentro de 20 años. Con la aceleración histórica que estamos viviendo, ninguno será nada dentro de 20 años \[...\]. Yo me pregunto cuál será el destino del libro; dudo que sea algo más que un inmenso archivo de microfilmes para los historiadores. Y anda tú a leer "Rayuela" en microfilme: ¿a quién le va a importar?". Y sonrió, cansado y descomunal, con su cara de ogro plácido y decente.
