Title: 'Manos Rojas' contra la vinculación de niños y niñas a la guerra
Date: 2016-02-12 11:34
Category: DDHH
Tags: Día de las manos rojas, DNI, reclutamiento infantil
Slug: manos-rojas-contra-la-vinculacion-de-ninos-y-ninas-a-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Manos-Rojas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

<iframe src="http://www.ivoox.com/player_ek_10413331_2_1.html?data=kpWhk5iXd5Khhpywj5WbaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5yncYammLLO0NTXb7Pjy8bgh5ebb8Tjz9nfw5DQpYzqytPQ19HFp8qZpJiSpJjSb8XZjNPWh6iXaaOl0NiY25DSrYa3lIqvk8bXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fernando Sabogal, DNI] 

###### [12 Feb 2016 ] 

Este viernes en Buenaventura, Popayán, Mitú y Bogotá se adelantan diversas actividades en el marco de la conmemoración del Día de las Manos Rojas, una fecha que simboliza "la **indignación mundial por las manos de los miles de niños y niñas que son usadas para accionar un arma** en vez de estar estudiando, pintando o construyendo un mundo de sueños”, como asevera Fernando Sabogal, actual vicepresidente de la organización 'Defensa de Niños y Niñas Internacional' DNI.

De acuerdo con Sabogal, en el mundo cerca de 300 mil niños y niñas han sido vinculados a la guerra, de ellos **7 mil 800 se han vinculado en Colombia en el contexto y en razón del conflicto armado**. Durante el año pasado 121 niños y niñas provenientes en su mayoría de Antioquia, Chocó, Cauca y Putumayo, fueron reclutados por las FARC, el Clan Úsuga, el ELN y los grupos paramilitares que controlan negocios ilícitos y actividades de extracción minera, en lugares como Buenaventura.

Durante este día a propósito de la conmemoración de la fecha en la que la ONU aprobó el protocolo de no vinculación de ningún menor a la guerra, niños, niñas y adolescentes de Bogotá se reúnen en el Centro de Memoria para **construir algunas propuestas que serán presentadas en un evento público a autoridades locales, nacionales e internacionales**, quienes asumirán el compromiso de darles cumplimiento durante este año, con el fin de evitar la inmersión de menores en la guerra.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
