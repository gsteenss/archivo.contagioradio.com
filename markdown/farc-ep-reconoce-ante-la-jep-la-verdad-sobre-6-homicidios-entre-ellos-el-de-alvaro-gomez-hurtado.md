Title: FARC- EP reconoce ante la JEP la verdad sobre 6 homicidios, entre ellos el de Álvaro Gómez Hurtado
Date: 2020-10-03 13:08
Author: CtgAdm
Category: Actualidad, Nacional, Paz
Slug: farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este sábado 3 de octubre la jurisdicción especial para la paz anunció, qué FARC- EP en vocería de su ex secretariado, **se comprometerá a esclarecer la verdad asumiendo la responsabilidad en 6 homicidios de líderes políticos y militares,** entre ellos el magnicidio el 2 de noviembre de 1995 del líder político [Álvaro Gómez Hurtado.](https://archivo.contagioradio.com/historico-por-primera-vez-presos-estan-dispuestos-a-revelar-verdades-sobre-el-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1312424619521998849","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1312424619521998849

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por medio de la vocería de la presidenta de la JEP, **[Patricia Linares](https://archivo.contagioradio.com/comision-de-acusaciones-de-la-camara-actua-con-celeridad-pero-contra-la-jep/), se conoció que luego de una reunión el pasado 25 de septiembre,** recibió una carta en representación del antiguo secretariado de las FARC- EP **en la que extendían *"su compromiso de aportar a la verdad, esclarecer los hechos ocurridos y asumir tempranamente la responsabilidad en los hechos ocurridos entre 1987 y 2002".***

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="blob:https://archivo.contagioradio.com/f4521b05-b0e7-4a04-a726-cb59d5e7cf18">
</video>
  

<figcaption>
Audio declaratorio, presidenta de la JEP

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Dentro de los homicidios reconocidos por FARC- EP, se encuentra inicialmente el del representante a la Cámara, **Pablo Emilio Guarín, asesinado el 15 de noviembre de 1987,** seguido por el líder político **Álvaro Gómez Hurtado asesinado el 2 de noviembre de 1995**, junto a ellos, **Hernando Pizarro León Gómez** (25 de febrero de 1995).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo en la lista registran, el **general (r) Fernando Landazabal Reyes**, asesinado el 15 de noviembre de 1987, **el exguerrillero de José Pedro Rey**, (30 de julio de 2002), y finaliza la lista el economista y pensador, [**Jesús Antonio Bejarano**](http://fenadeco.org/quien-fue-jesus-antonio-bejarano/), asesinado el 15 de septiembre de 1999.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La carta que recibió la jurisdicción fue entregada el 30 de septiembre de este año, y está firmada por los ex integrantes de FARC- EP, **Julián Gallo Cubillos, Pastor Lisandro Alape, Pablo Catatumbo, juntos a ellos las firmas de sus abogados.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Esta Colombia preparada para la verdad entregada por FARC- EP?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pese a que este trámite apenas inicia en la Sala de Reconocimiento de Verdad y Responsabilidad, organismo competente para hacer la investigación, seguimiento y esclarecimiento de la verdad en los hechos ocurridos y FARC- EP, múltiples cuestionamientos de esta verdad han empezado a aflorar en redes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fefernandezt/status/1312436080214339584","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fefernandezt/status/1312436080214339584

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HELIODOPTERO/status/1312438850363383809","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HELIODOPTERO/status/1312438850363383809

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/andresportillo_/status/1312432979625607170","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/andresportillo\_/status/1312432979625607170

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Develando que no solamente piden una verdad completa ante estos 6 magnicidios y otros cientos que aún faltan por esclarecer, sino que también las razones entregadas por los ex militantes de FARC- EP, carecen de argumentos, y qué es necesario que la investigación ahora en manos de la JEP sea más exhaustiva para determinar si efectivamente las afirmaciones entregadas corresponden efectivamente a estos asesinatos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SuarezTrinando/status/1312436909205987328","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SuarezTrinando/status/1312436909205987328

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Un argumento al que dio razón hace algunos meses el ex congresista Alvaro Leyva, quién indicó que el día que se conozca la verdad, toda la función pública tiene que adecuarse a la verdad y al hecho de que se debe superarla.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Aquí hay enemigos para todo por un gran desconocimiento, aquí se piensa que se está viviendo en un momento de felicidad colectiva, pero para llegar a esa felicidad colectiva tenemos que destapar una olla podrida"*
>
> <cite>Alvaro Leyva</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
