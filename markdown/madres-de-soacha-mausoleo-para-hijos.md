Title: Madres de Soacha esperaron más de 10 años para tener el mausoleo de sus hijos
Date: 2019-05-16 18:15
Author: CtgAdm
Category: DDHH, Memoria
Tags: ejercito, Fuerzas militares, madres de soacha, soacha
Slug: madres-de-soacha-mausoleo-para-hijos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/madres_soacha-camila_ramirez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Camila Ramírez] 

Este jueves las madres de víctimas de ejecuciones extrajudiciales en Soacha, Cundinamarca formalizarán **la entrega de lotes para construir los mausoleos en los que finalmente podrán reposar los cuerpos de los jóvenes**. Para ellas, la donación de los terrenos será un alivio porque significará dejar de pagar costosos arriendos en cementerios privados y públicos, en los que reposan desde 2008 los cuerpos óseos de sus hijos. (Le puede interesar:["Queremos saber quién ordenó los falsos positivos: Madres de Soacha"](https://archivo.contagioradio.com/queremos-saber-quien-ordeno-los-mal-llamados-falsos-positivos-madres-de-suacha/))

### **¿Por qué es tan importante esta noticia para las madres de Soacha?**

Como lo explicó la **abogada de la Asociación Minga Pilar Castillo,** el proceso de donación de los lotes lleva dos años; y resulta trascendente porque el tema sobre dónde inhumar los cuerpos comenzó en 2008, cuando fueron encontrados en Ocaña, Norte de Santander. Desde entonces, se inició el proceso de traslado a Bogotá, y  sepultarlos en  los cementerios Campos de Cristo, El Apogeo, de Chapinero y Jardines de Bosa. (Le puede interesar: ["Una galeria para recordar a jóvenes de Soacha a 10 años de su desaparición"](https://archivo.contagioradio.com/jovenes-soacha-galeria-memoria/))

Al ser parte de una investigación judicial, los mismos no podían ser exhumados; pero ello obligaba a las familias de los jóvenes a renovar prórrogas de arrendamiento en los cementerios cada cuatro años, y pagar cánones de arrendamiento que se hacían cada vez más difíciles de cubrir. Según Castillo, **los cementerios amenazaron incluso con retirar los restos y llevarlos a una fosa común en caso de que no se pagaran los arriendos**; ello motivó la búsqueda de un lugar definitivo de reposo para los jóvenes.

### **Ejército se comprometió en la construcción de los Mausoleos**

En 2017 el General José Alberto Mejía, comandante de las Fuerzas Militares se comprometió en la financiación del Mausoleo; en ese momento se diseñaron los planos para los mismos y fueron aprobados por las madres, pero fue difícil encontrar los lotes para su construcción. La Abogada afirmó que en ese año el tema presupuestal ya estaba resuelto, y las FF.MM., dijeron que tardarían unos 4 meses en la construcción.

Dos años después, finalmente el párroco de Soacha trabajó para donar los terrenos en los que se construirían los mausoleos, y hoy se hará la entrega oficial. Luego que esto ocurra, las madres tendrán que volver a discutir el tema con **el general Luis Fernando Navarro, nuevo comandante de las Fuerzas Militares;** y adelantar junto a él todo el tema administrativo y presupuestal que se requiere. (Le puede interesar: ["Madres de Soacha rechazan verdades a medias de militares en la JEP"](https://archivo.contagioradio.com/madres-soacha-rechazan-verdades-medias-jep/))

Sin embargo, Castillo sostuvo que no esperan ninguna dificultad en este proceso, puesto que la decisión de apoyar las madres vino directamente del Ministerio de Defensa; y se entendería que la decisión siguen en firme. Para la Abogada, este sera un paso importante para que las madres tengan un lugar digno, construido con mucha simbología y que honra la memoria de sus hijos. (Le puede interesar:["Reclutador de falsos positivos de Soacha se compromete con la verdad"](https://archivo.contagioradio.com/falsos_positivos_preacuerdo_madres_soacha/))

<iframe id="audio_35950568" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35950568_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
