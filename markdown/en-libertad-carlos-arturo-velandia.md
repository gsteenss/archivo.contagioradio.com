Title: En libertad Carlos Arturo Velandia
Date: 2016-08-01 19:52
Category: Nacional, Paz
Slug: en-libertad-carlos-arturo-velandia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/EN-LIBERTAD-e1470087312947.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [1 Agos 2016]

De acuerdo con el oficio 1271 Penal del circuito Especializado de Cali, se levanta la medida de aseguramiento de detención preventiva impuesta por la Fiscalía General de la nación y se otorga libertad provisional a Carlos Arturo Velandia Jagua, alias Felipe Torres, por su presunta participación en el secuestro masivo ocurrido en el kilometro 18 de la vía Cali- Buenaventura el 17 de septiembre del año 2000.

Velandia, quien habia sido detenido el 20 de junio del presente año por los delitos de secuestro extorsivo, homicidio culposo y hurto calificado y agravado, fue designado como gestor de paz a través de la [[resolución Nº 207 de 2016](https://archivo.contagioradio.com/levantan-orden-de-captura-contra-carlos-velandia-y-antonio-bermudez/)] firmado por el Presidente Juan Manuel Santos el día 25 de julio.

La orden presidencial designa como gestores de paz a Velandia Jagua y a Antonio Bermúdez Sánchez, alias Francisco Galán para que a través de su experiencia contribuyan a generar acercamiento con las guerrillas y grupos armados al margen de la ley.

[![boleta de libertad - carlos velandia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/boleta-de-libertad-carlos-velandia.png){.aligncenter .size-full .wp-image-27120 width="419" height="521"}](https://archivo.contagioradio.com/en-libertad-carlos-arturo-velandia/boleta-de-libertad-carlos-velandia/)
