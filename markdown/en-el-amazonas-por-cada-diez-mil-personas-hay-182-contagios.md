Title: En el Amazonas por cada diez mil personas hay 190 contagios
Date: 2020-05-26 13:50
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Amazonas, #ComunidadesIndígenas, #covid19, #TicoyaPorLaVida
Slug: en-el-amazonas-por-cada-diez-mil-personas-hay-182-contagios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-25-at-6.16.46-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Una de las regiones más afectadas en Colombia por la pandemia del Covid 19 es el Amazonas, territorio en donde se estima que por cada **diez mil habitantes, hay 190 personas contagiadas**. Por ese motivo distintas organizaciones lanzan la campaña \#TicoyaPorLaVida, con la que se busca recolectar donaciones para proteger 11 comunidades indígenas que suman más de tres mil personas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

S.O.S al Amazonas
-----------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sebastían Gómez, integrante del Centro de Alternativas al Desarrollo, afirma que el riesgo en está región es complejo debido a su condición de frontera dado que en **Brasil y Perú** los sistemas de contención del COVID 19 han sido poco eficientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"En el tránsito de población entre estos tres países se han aumentado las cifras de contagio de manera exponencial; y sin duda alguna, el hecho de que por ejemplo en Puerto Nariño únicamente hayan 5 camas hospitalarias y ninguna Unidad de Cuidados Intensivos, hace que la situación sea muy difícil de manejar" señala.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son solamente 70 camas hospitalarias para 70 mil pacientes

<!-- /wp:heading -->

<!-- wp:paragraph -->

Panorama desalentador para un departamento que tan solo cuenta con dos hospitales para atender a una población de **79 mil habitantes y que en total cuenta con 70 camas hospitalarias.** (Le puede interesar: "[Covid-19 amenaza con extinguir la sabiduría indígena ancestral del Amazonas](https://archivo.contagioradio.com/covid-19-amenaza-con-extinguir-la-sabiduria-indigena-ancestral-del-amazonas/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, Sebastían Gómez asegura que en vista de los hechos se esperarían acciones mucho más urgentes por parte del gobierno de Iván Duque. Sin embargo la falta de medidas posiciona a Colombia como el segundo país con mayor población indígena afectada.

<!-- /wp:paragraph -->

<!-- wp:heading -->

\#TicoyaPorLaVida
-----------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En vista del contexto, distintas organizaciones se encuentran lanzando la campaña \#TicoyaPorLaVida. Acción con la que esperan recolectar una serie de ayudas para la compra de insumos de bioseguridad y desinfección que protejan a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Sebastían, "Ticoya quiere decir Tikuna, Cocama y Yagua que es uno de los resguardos de la frontera sur de la Amazonía. Ubicados en el municipio de Puerto Nariño", población a la que le llegarán estos aportes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además se espera garantizar la protección de 300 guardias indígenas que actualmente se encuentran vigilantes de la contención del virus en el territorio y de las y los gestores de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para sumarse a \#TicoyaPorLaVida puede ingresar al siguiente link <https://vaki.co/vaki/TICOYAPORLAVIDA> y realizar su aporte. Asimismo, puede estar pendiente de esta iniciativa en las redes sociales de las siguientes organizaciones: Manifiesta, CEALDES, Corporación de Salud Abran la Puerta, TICOYA, Observatorio de Soberania y Seguridad Alimentaria, Contagio Radio, Comisión de Justicia y Paz y SIN OLVIDO.

<!-- /wp:paragraph -->

<!-- wp:image {"id":84740,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-25-at-6.16.46-PM-1024x1024.jpeg){.wp-image-84740}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
