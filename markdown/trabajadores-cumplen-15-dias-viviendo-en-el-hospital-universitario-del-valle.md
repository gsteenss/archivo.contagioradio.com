Title: Trabajadores cumplen 15 días viviendo en el Hospital Universitario del Valle
Date: 2016-11-17 16:54
Category: Movilización, Nacional
Tags: Crisis en Hospital Universitario del Valle, Trabajadores
Slug: trabajadores-cumplen-15-dias-viviendo-en-el-hospital-universitario-del-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cable-noticias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cable Noticias] 

###### [17 Nov 2016] 

Se cumplen 15 días, desde que los trabajadores del Hospital Universitario del Valle decidieron quedarse a vivir al interior de las instalaciones debido a los despidos masivos de 400 personas. Sin embargo un **juez de Cali habría ordenado la reincorporación de 202 trabajadores sindicalizados a sus labores en el Hospital.**

No obstante, la situación del Hospital Universitario del Valle no mejora, de acuerdo con Jorge Rodríguez, presidente de Sintrahospiclínicas, **el hospital solo está funcionando con el 50% de sus capacidades y el musculo hospitalario del municipio ya no da abasto** para responder a la crisis que se está extendiendo al resto de clínicas y hospitales.

Frente a este hecho y una tutela instaurada por los trabajadores, el juez quinto administrativo falló el pasado 10 de noviembre y **dictaminó que se dé el reintegro de 202 trabajadores oficiales, es decir 177 despedidos y 25** que quedaron en una planta transitoria, retomando sus funciones con contrato de trabajo como lo tenían establecido antes de ser despedidos. Además el juez ordenó la suspensión de la reestructuración de la planta del Hospital, hasta tanto no se conozcan los estudios técnicos.

Sin embargo, pese a la sentencia **aún no se han reintegrado los trabajadores** y se espera que en los próximos días la gobernadora Dilian Fransisca Toro se pronuncie frente a esta situación. De igual forma los trabajadores seguirán en las instalaciones del hospital hasta tanto no se cumpla con el falló. Le puede interesar: ["Decidimos vivir en el Hospital Universitario del Valle ante despido masivo injustificado: Sintrahospiclínicas"](https://archivo.contagioradio.com/decidimos-vivir-en-el-hospital-universitario-del-valle-ante-despido-masivo-injustificado-sintrahospiclinicas/)

“**Hace 15 días despidieron a los vigilantes de planta del hospital y contrataron el servicio con la empresa Nápoles por tercerización laboral** y así va a pasar son las demás áreas que están sin empleados” afirmó Rodríguez.

Entre tanto, los afectados siguen siendo la ciudadanía en general, los trabajadores y los estudiantes de medicina siguen viéndose afectados con la falta de soluciones frente al Hospital Universitario del Valle, estos últimos han visto afectadas sus residencias, reduciendo los mínimos de calidad de los aprendizajes en medicina.

<iframe id="audio_13805266" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13805266_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
