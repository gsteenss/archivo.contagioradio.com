Title: Con Petro y más allá de Petro.
Date: 2018-05-20 09:41
Author: ContagioRadio
Category: Camilo, Opinion
Slug: petro-mas-alla-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-de-las-flores101316_160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div dir="auto">

</div>

Por Camilo De Las Casas

18 de mayo de 2018

Lo nuevo se sigue expresando está en las calles reales y virtuales, y en amplios sectores que dicen de su indignación ante un orden corrupto, excluyente, mentiroso y violento. Se dice lo nuevo en la tendencia electoral, en los indecisos del 8% y en el escepticismo de muchos que están molestos con lo qué pasa.

El gran aporte de la decisión de dejación de armas de las FARC fue abrir ese espacio de lo político sin la mediación de la fuerza quitando un pretexto a una clase dirigente incapaz de construir el bien común. Así de cierto,  y con independencia de que se le reconozca a las FARC es el gran aporte político para cambios históricos en Colombia.

Tal decisión ha tenido un altísimo costo para el partido de las FARC, que en medio de sus tensiones internas, creyó en la palabra de su contradictor Santos y las garantías que brindaría pos desarme en medio de la Pax Neoliberal.

Sin embargo, el nobel en medio de sus silencios y acomodamiento ha sido partícipe de la continuidad de la destrucción política de las FARC por medios cualificados en una estrategia política que combina la judicialización internacional como arma de guerra, la desnaturalización del quehacer político de la ex guerrilla a través de una bancada de gobierno obstaculizadora en los trámites legislativos y una burocratización que ha impedido la incorporación social con eficacia.

Prolongación de la guerra de odio, una a cocción lenta, y otra abierta, la que anunció hacer trizas los acuerdos. La silenciosa por parte de sectores del establecimiento ante los que el presidente solo guarda silencio o en imprudentes expresiones para congraciarse con sus opositores. Ambas pretenden lo mismo la Pax Neoliberal.

La lentitud en dar respuesta a las amnistías e indultos, cuando no en su obstaculización; el asesinato de cerca de 40 excombatientes por distintos móviles; el evidente montaje judicial contra Jesús Santrich que le ha llevado a una decisión sacrificial ante el horror de una cárcel injusta en Estados Unidos en dónde se desnaturaliza su opción vital para mostrarlo como un traficante de drogas, son hechos que se asocian al incumplimiento estructural en la Pax Neoliberal.

Hechos uno tras otro, como la corrupción galopante, expresada en Reficar, Odebrecht, HidroItuango, a los que se suma la serie de asesinatos de líderes sociales y ambientales, el congelamiento de los Planes de Desarrollo con Enfoque Territorial, el banco de tierras de papel que no lleva ni a una reforma agraria liberal, y la aplicación perezosa de los planes de sustitución voluntaria.

Y aún,  en esas circunstancias, con todas las criticas que provienen de los intelectuales de corazón transformante, pero de escritorio, que validan el proceso con el ELN como el ideal, estamos observando lo nuevo naciente.Incluso, las bases simpatizantes de esa guerrilla, desobedeciendo los principios del no voto, están acercándose a esos nuevos ciudadanos jóvenes, frescos, distantes de discursivas ideologizadas y farragosas.

Ahí están sumándose en un programa de Colombia Humana, insisto imposible, si las FARC persistían en su lucha armada. Programa que refleja esperanzas y anhelos básicos en lo social, en lo ambiental y  ante la corrupción, y los derechos de las víctimas. Propuesta programática que se asocia a Petro que ni Piedad ni De La Calle lograron. Petro es solo una expresión la consciente de la posibilidad de la transición hacia otra democracia.

Lo que ha logrado Petro y su movimiento es sin precedentes, porque es un movimiento, más que un Partido. Colombia Humana logró tomar a ese poder consciente ciudadano, y más allá de las propias  vanidades del candidato, el programa aglutina, suma y expresa la paz que se cimienta en la justicia.

 Lo que se define el próximo 27 es o la transición o la pre-modernidad o la apuesta por la paz con justicia socio ambiental o la pax neoliberal. El asunto del 27 de mayo es con Petro y más allá de Petro.
