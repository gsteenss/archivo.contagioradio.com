Title: El modelo socio-económico causante de la crisis, se dice creyente. ¿Será verdad?
Date: 2020-04-22 19:22
Author: A quien corresponde
Category: A quien corresponda, Columnistas
Tags: a quien corresponda, cristianismo., neoliberalismo, politica, Religión
Slug: el-modelo-socio-economico-causante-de-la-crisis-se-dice-creyente-sera-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### ***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones*** (Romanos 2,24) 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Bogotá, 9 de abril del 2020  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Me gusta tu Cristo... No me gustan tus cristianos. * *Tus cristianos son muy diferentes a tu Cristo"* Mahatma Gandhi.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hermano en la fe**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cristianos, cristianos, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordial saludo,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El “paro de la naturaleza”, por el COVID-19, está evidenciando el carácter inequitativo, injusto y destructivo de la sociedad actual, sus graves repercusiones para la vida (humana y del planeta) y el costo humano y social de convertir “todas las cosas” en negocio –incluida la salud-; está cuestionando los “valores” engañosos que la orientan y  los riesgos del llamado “progreso” y “desarrollo; está poniendo al descubierto el poder, económico y financiero, que  ha apropiado del mínimo vital necesario para la vida de millones de personas, deteriorando el planeta con las consecuencias que hoy vivimos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Esta es una sociedad produce víctimas** con las violencias,  el hambre, las balas, la represión, la exclusión, el racismo… No podemos ignorar que los cristianismos (católico, evangélico, protestante…) hemos ayudado a construirla, por acción o por omisión. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Te escribo esta carta porque, a pesar de nuestras diferencias, nos duelen las profundas injusticias sociales y el uso de la fe para justificarse. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los especialistas y personas informadas, nos dicen que este virus que se expande fácil, silenciosa y rápidamente, ese es su gran riesgo; que un número reducido de infectados necesitaran hospitalización, más reducido aún irán a cuidados intensivos y pocos  morirán; que el riesgo está en el colapso de los sistemas hospitalarios, por rápida expansión y contagio, con las personas mayores edad y con quienes tienen enfermedades preexistentes o bajo nivel inmunológico sin importar la edad; que a las personas con buen sistema inmunológico no les pasará nada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Coinciden en señalar que su verdadero peligro lo constituye las profundadas inequidades e injusticia sociales (falta de empleos dignos y estables para las mayorías), en la falta de educación y la precariedad e insuficiencia de los sistemas de salud.   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¿Por qué la inequidad e injusticia social son un riesgo?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Porque las personas con los mínimos vitales cubiertos, con recursos económicos suficientes o trabajos dignos y estables, pueden sobrellevar una cuarentena sin mayores problemas; porque una alimentación sana y una buena calidad de vida, aseguran un sistema inmunológico fuerte frente al virus; porque mientras haya una sola persona contagiada, toda la sociedad está en riesgo. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¿Por qué la falta de educación?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Porque un buen nivel educativo permite comprender mejor los riesgos, asumirlo con realismo, sin miedo y tomar decisiones más informadas y por eso más eficaces.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¿Por qué la precariedad del el sistema de salud?** 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Porque una suficiente cobertura en salud, una buena red hospitalaria, unas buenas condiciones laborales para todo el personal sanitario, una adecuada dotación que garantice la bioseguridad para todo el personal y un buen sistema de salud preventiva permite reducir las hospitalizaciones, garantizar a quienes lo requieran una atención a tiempo y una mayor y mejor superación de las urgencias. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Pero la realidad es que esta sociedad es profundamente injusta.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En una carta anterior le recordaba que el 1% de la población mundial tiene casi tanta riqueza como el 99% restante y que 8 millonarios tienen la riqueza de 3.500 millones de personas, que en Colombia de los 48.258.000 de habitantes, 13.029.000 no alcanzan a cubrir las necesidades básica, que 9.450.000 tienen múltiples carencias básicas y que 3.508.000, viven en la pobreza extrema, ordinariamente pasan hambre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El acceso a la cultura y a la educación de calidad y a las condiciones necesarias para estudiar es restringido para la mayoría. La falta de acceso a educación es causa y consecuencia de la injusticia social. La salud, en los últimos 30 años ha pasado de enferma a grave y de grave a cuidados intensivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pueblo ha pagado sobre costos escandalosos por las medicinas, los robos multimillonarios a la salud, conoce la impunidad de la que gozan los grandes “ladrones”, padece las consecuencias del deterioro de las condiciones laborales del personal de la salud, ha vivido experiencias dolorosas e indignantes con las EPS, incluyendo daños irreparables a la vida de sus seres queridos; todo porque la salud ser convirtió en un lucrativo negocio de los grandes grupos económicos con la complicidad del Estado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Se colocó el dinero por encima de la vida del pueblo y el pueblo ha sido responsable por permitirlo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una imagen que circula por redes describe muy bien las prioridades del país: en una foto, están los miembros del **ESMAD** (Escuadrón móvil anti disturbios) con su excelente y técnico sistema de protección y en la otra, el personal de salud con su sistema de protección hecho con bolsas plástica de basura. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra cara de la sociedad actual, es la **destrucción del planeta** y el riesgo de desaparición de miles de especies, incluida la humana. [Así lo describe Gian Lucca Garetti, analizando los **SARS**, virus familiares del COVID-19:](https://www.anred.org/2020/03/21/no-va-a-estar-todo-bien/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->

“*alrededor el 70% de las enfermedades infecciosas emergentes y casi todas las pandemias recientes son zoonosis, es decir, con origen animal (la mayoría salvajes) que al invadir su habitat pasan  a los personas, aumentando la exposición a diversos agentes infecciosos.* Y concluye: “*Esta pandemia es* *una prueba general de cómo el neoliberalismo, con la contaminación, con el cambio climático, con la exacerbación de las desigualdades, nos está llevando derecho hacia la sexta extinción masiva”*. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Qué es el **neoliberalismo**, cuál es su origen y por qué lo señalan como causante de la actual crisis humano-ambiental? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para ubicarnos en la realidad concreta del neoliberalismo, te invito a hablar con tus padres o abuelos y que te cuenten como era la economía hace 40 o 50 años, cuando con una persona que trabajara en la familia (que era numerosa) alcanzaba para la comida, el vestido, la educación y la habitación, austera pero suficientemente. Por ejemplo, el agua, la luz, la salud, la educación, el transporte y la alimentación eran más baratos, la mayoría de colombianos vivían del campo y producían suficiente comida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego, que te cuenten cuándo y cómo empezó el éxodo de campesinos a la ciudad por la crisis del campo, el desplazamiento forzado y el despojo de las tierras; cuándo y cómo empezó el deterioro de las condiciones laborales de todos los trabajadores; cuándo y cómo se privatizaron el agua, el aseo, la luz, el transporte, el teléfono, las carreteras, bancos, la salud… y una larga lista de entidades de servicios públicos que pasaron a ser lucrativos negocios privados; cuándo y cómo acabaron con la pequeña y mediana industria, con pequeños y medianos comercios, cooperativas… Te sugiero que  luego hagan la comparación con la situación económica de hoy.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estos cambios son causados por el neoliberalismo. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

**El neoliberalismo es un sistema político económico y financiero mundial** que impuso a los estados **la privatización** de los bienes y servicios públicos (agua, energía, transporte, salud, educación, aseo, carreteras, bancos…), **la desregulación gubernamental** para evitar el control de los estados a los grandes capitales financieros (bancos, fondos privados, bolsas de valores) y **el recorte al gasto social,** es decir, que los estados no deben gastar a favor de la sociedad, del pueblo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El principio neoliberal es que todo lo social y publico debe convertirse en negocio privado de empresas nacionales y multinacionales, sin ningún control, ni beneficios para las mayoría de los países. Los Estados deben estar al servicio del capital financiero y de las grandes empresas multinacionales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El neoliberalismo, fue creado a finales de los años 60 en la Faculta de Economía Universidad de Chicago por **Milton Friedman** y para imponerlo utilizó las investigaciones sobre el uso del shock en las personas y en la sociedad, realizadas por **Ewen Cameron** en el Instituto de Memoria Allan de la Universidad McGill de Montreal y se impuso en el mundo las décadas de los 70, 80 y 90. M. Friedman, estaba convencido que *“sólo una crisis –real o percibida- da lugar a un cambio verdadero” y * el neoliberalismo era un profundo cambio, por eso para su expansión utilizó la doctrina del shock, como lo documentó **Noami Klein**: “*guerras, atentados terroristas, golpes de Estado y desastres naturales. Luego,* \[los países\] *vuelven a ser  víctimas del shock a manos de las empresas y los políticos que explotan el miedo y la desorientación fruto del primer shock para implantar una terapia del shock económica. Después, cuando la gente se atreve a resistirse a estas medidas políticas se les aplica un tercer shock* *si es necesario, mediante acciones policiales, intervenciones militares e interrogatorios en prisión*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La regla económica impuesta por Friedman fue: MV = PT (Dinero x Velocidad = Precio x Transacciones), sin importar las consecuencias humanas y ambientales. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El neoliberalismo se impuso con el apoyo, por acción o por omisión, del cristianismo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[El Fondo Monetario Internacional -FMI,](https://rebelion.org/la-teologia-del-libre-mercado-de-los-chicago-boys/) promovió la “teología del capitalismo total” que presenta al neoliberalismo económico y financiero como “*el único camino realista para cumplir sus exigencias* \[de la teología cristiana\] *la opción preferencial por los pobres y la encarnación económico-social del Reino de Dios”.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es doloroso y escandaloso reconocer el uso de la religión y la teología para un proyecto tan contrario al proyecto de Jesús de Nazaret.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la carta anterior te recordaba que Jesús de Nazaret, en la parábola del juicio final (Mateo 25,31-43) colocaba como condición para entrar al Reino (la salvación) las acciones concretas frente a los hambrientos, sedientos, desnudos, encarcelados, enfermos, forasteros-migrantes y afirmaba que todo lo que hicieron o dejaron de hacerle a *“estos hermanos más pequeños, conmigo lo hicieron o dejaron de hacerlo*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde antes la Palabra de Dios afirmaba: “*Entre ustedes no deberá haber pobres, porque el Señor tu Dios te colmará de bendiciones en la tierra que él mismo te da para que la poseas como herencia. Y así será, siempre y cuando obedezcas al Señor tu Dios y cumplas fielmente todos estos mandamientos que hoy te ordeno”* (Deuteronomio 15, 4-5).   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La implantación del neoliberalismo ha producido hambre y muertos por hambre, sed y destrucción de las fuentes de agua, emigración y desplazamientos forzado, desnudez y carencia de vivienda, enfermedad y negocio en la salud, cárceles desbordadas e inhumanas, destrucción el planeta para  reproducción y acumulación de capital.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta implantación tuvo una teología (de la prosperidad) y el apoyo de sectores cristianismos (sin el conocimiento y consentimiento de la mayoría de creyentes) a pesar de estar en abierta contradicción con el Reino de Dios. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Te invito a que reconozcamos el pecado (de pensamiento, palabra, acción y omisión) personal, social, estructural y ecológico de la implantación por la fuerza (de la corrupción, la violencia, el dinero, el engaño) de este modelo social y económico que mata, de diversas maneras, la vida de las personas y del planeta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pidamos perdón a todas las víctimas, a la naturaleza y a Dios (porque colocamos el dinero por encima de la vida y de Dios).     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P <francoalberto9@gmail.com>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le [puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  

<!-- /wp:paragraph -->
