Title: La propuesta del Centro Democrático que va en contra de la Consulta Anticorrupción
Date: 2018-08-08 11:07
Category: Política
Tags: Angélica Lozano, Claudia López, Consulta anticorrupción, Iván Duque
Slug: centro-democratico-contra-anticorrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-08-a-las-11.24.39-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PartidoVerdeCol] 

###### [8 Ago 2018] 

Durante el discurso de posesión como presidente de Colombia, **Iván Duque afirmó que presentaría un paquete de proyectos tendiente a acabar con la corrupción,** sin embargo, no se refirió en ningún momento a la Consulta Anticorrupción que los colombianos tendrán la oportunidad de votar este 26 de Agosto.

**Claudia López,** ex senadora y principal vocera de la iniciativa popular, afirma que espera que la omisión en el discurso de Duque sea de buena fé, pues **"parece que hay un doble discurso sobre la Consulta"**, manifestándose sobre lo dicho por el presidente del Senado, Ernesto Macías, quien se refirió a la corrupción pero evito mencionar el mecanismo de participación. Además, porque tanto el presidente del Senado como el Presidente de Colombia se comprometieron con la presentación de Proyectos de Ley que buscan similares resultados a los de la Consulta.

### **Uribe celebró que Iván Duque no se comprometió con la Consulta Anticorrupción** 

En videos difundidos por Noticias Uno se supo que el líder del Centro Democrático, Álvaro Uribe Vélez, celebró que Iván Duque no habló de la Consulta Anticorrupción, e igualmente elogió el discurso de Ernesto Macías, quien propuso una serie de Proyectos de Ley que involucran puntos que están en la Consulta, **menos el de reducción de salarios para congresistas y altos funcionarios.**

### **"Las iniciativas anticorrupción vía Congreso se hunden"** 

Ante la posibilidad de que se tramite por el legislativo un Proyecto de Ley que implemente lo que se está proponiendo en la Consulta Anticorrupción, la ex senadora señaló que **"el Congreso es un nido de corrupción"** y por eso, las iniciativas que busquen evitar la 'mermelada' o los hechos de fraude a la nación se hunden. Además, aseguró que es una experiencia que ya conoce Iván Duque como senador, dado que ambos (Duque y López) ya intentaron legislar sobre ese tema, y el Proyecto se hundió.

Para López, la única forma de reducir el salario a los congresistas, dar cárcel a los corruptos y obligar a que se contrate de forma transparente con el Estado (entre otras iniciativas) es vía **Consulta Popular, cuyo resultado favorable obligaría al Congreso y al presidente de la República a cumplir con esos mandatos.** (Le puede interesar:["¿Cómo se debe votar la Consulta Anticorrupción?"](https://archivo.contagioradio.com/como-se-debe-votar-la-consulta-anticorrupcion/))

La Consulta requiere que participe una tercera parte del total de ciudadanos habilitados para votar, es decir **que 12 millones de personas salgan a las urnas, y que la mitad de ellos más 1, vote en favor de las 7 preguntas que encontrarán en el tarjetón.** Claudia López es optimista y espera que vayan a las urnas 15 millones de personas, igualmente, espera que Iván Duque ratifique el compromiso con la Consulta que ya había establecido antes de posesionarse como Presidente de la República.

\[caption id="attachment\_55434" align="aligncenter" width="498"\][![Captura de pantalla 2018-08-08 a la(s) 10.56.46 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-08-a-las-10.56.46-a.m.-498x695.png){.wp-image-55434 .size-medium width="498" height="695"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-08-a-las-10.56.46-a.m..png) Estos son los 7 mandatos de la Consulta Anticorrupción. Imagen de: www.vencealcorrupto.com\[/caption\]

######  

<iframe id="audio_27703314" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27703314_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
