Title: Los Acuerdos de paz cojean en el Congreso de la República
Date: 2017-11-09 16:44
Category: Nacional, Paz
Tags: acuerdos de paz, Congreso, Implementación
Slug: los-acuerdos-de-paz-cojean-en-el-congreso-de-la-republica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Congreso-Colombia-e1507068163688.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [09 Nov 2017]

La implementación de los Acuerdos de Paz continúa afrontando dificultades, ayer luego de más de 5 horas, **no se completó el quorum para aprobar la Jurisdicción Especial para la Paz en Senado**, mientras que, en Cámara, Rodrigo Lara propuso que las circunscripciones fuesen un escenario al que partidos tradicionales pudiera acceder, pasando por alto el espíritu de inclusión de los Acuerdos hacia las comunidades olvidadas por el estado y la política.

Frente a estas complejas situaciones, que ponen en riesgo los Acuerdos de La Habana, Antonio Madariaga, director de Viva la Ciudadanía, manifestó que diferentes organizaciones sociales y civiles se han dado a la tarea de hacer un ejercicio de seguimiento a la implementación que ha dado como resultado la evidencia de estrategias por parte de partidos **como Cambio Radical o el Centro Democrático por dilatar y entorpecer este proceso**.

### **Centro democrático opera con poco respeto a la deliberación democrática** 

“El Centro Democrático inclusive opera con poco respeto por las reglas y la deliberación democrática, hace un conjunto de proposiciones y **a la hora de la votación se retiran del recinto y no participan, lo que demuestra un interés dilatorio”** afirmó Madariaga y agregó que, si a esto se le suma la coyuntura electoral, da como resultado esta apatía por parte de los congresistas. (Le puede interesar: ["Reforma política esta en riesgo de muerte"](https://archivo.contagioradio.com/reforma-politica-esta-en-riesgo-de-muerte/))

Frente a la participación ciudadana en la defensa de los Acuerdos de Paz, Madariaga señaló que, aunque las movilizaciones sociales son una herramienta de presión para que congresistas cumplan con su deber de legislar, **la veeduría de estar al tanto de lo que hace cada congresista en los debates puede gestar una presión mucho más fuerte**.

“Los sectores de la sociedad civil, no hemos abandonado la presión permanente en el congreso, hemos estado activos” afirmó Madariaga y señaló que se viene generando una agenda de acciones para la defensa de los acuerdos que culminarán con un gran balance el próximo año de cómo ha sido la implementación de los mismos.

<iframe id="audio_21984090" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21984090_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
