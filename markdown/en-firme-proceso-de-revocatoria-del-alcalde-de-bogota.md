Title: En firme proceso de revocatoria del Alcalde de Bogotá
Date: 2017-06-20 11:31
Category: Nacional, Política
Tags: Bogotá, Peñalosa, revocatoria Peñalosa
Slug: en-firme-proceso-de-revocatoria-del-alcalde-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/revocatoria-del-alcalde-de-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: espectador] 

###### [20 Jun 2017] 

Según el Comité Unidos Revocaremos a Peñalosa, la Registraduría avaló más de 470 mil firmas de las 700 mil presentadas por el comité que impulsa la propuesta de revocatoria del Alcalde de Bogotá, Enrique Peñalosa. Con esa decisión el presidente tendría 2 meses para convocar a las urnas y se iniciaría también la campaña tanto a favor como en contra de la iniciativa ciudadana.

En total la Registraduría avaló 473.000 firmas del 706.000 presentadas por el comité, sin embargo, ese número **supera en más de 200 mil las necesarias para convocar a las urnas**. A partir de ahora  corren 5 días hábiles para que quienes no estén de acuerdo con el conteo presenten las objeciones ante la entidad.

Según la Registraduría se encontraron 473.700 firmas válidas de las 700.000 presentadas, de las cuales, el mayor problema fue que 86.646 no se encontraban dentro del censo de los bogotanos y 37.000 números de cédula no correspondían al nombre inscrito, **sin embargo esos errores fueron minúsculos y se superó el umbral.**

Sin embargo el proceso de recolección y conteo ha sido limpio y apegado a la ley, así lo han manifestado tanto el vocero de la inciativa, Gustavo Merchán, como otros representantes de organizaciones sociales que se han sumado a la campaña. [Lea también: la revocatoria sigue su curso.](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/)

Por otra parte, frente a la información que circuló la semana pasada en torno a posibles faltas en las cuentas presentadas por el Comité, Merchán asegura que no han sido notificados oficialmente y que están a la espera de una citación para presentar los recibos y demás material sobre ese punto en particular, de manera que no quede ninguna duda de la transparencia del proceso.

Comunicado de Registraduría

![registraduria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMG-20170620-WA0032.jpg){.alignnone .size-full .wp-image-42442 width="717" height="947"}

<iframe id="audio_19379880" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19379880_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
