Title: Mujeres rurales siguen exigiendo garantía de sus derechos
Date: 2017-03-08 15:21
Category: Mujer, Nacional
Tags: Día internacional de la mujer, mujeres, Paro Internacional
Slug: mujeres-rurales-siguen-exigiendo-garantia-de-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/1Conamuri-0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 

###### [Foto: Mujeres Libres de Violencias] 

###### [08 Mar. 2017] 

Las mujeres rurales en Colombia también se unieron a la **conmemoración del Día Internacional de las Mujeres**. Desde diversos lugares del país, campesinas, afros, mestizas e indígenas siguen demandando **mejoras en las condiciones para ejercer su trabajo, igualdad en el acceso a la tierra, créditos y subsidios con enfoque diferencial, mejores salarios, entre otros.**

Recientemente el Programa para las Naciones Unidas **(PNUD) aseguró en su reporte para el desarrollo, que son cerca del 43% de las mujeres rurales quienes tienen que vivir con estas realidades** y agrega que este escenario no solo acontece en Colombia, sino en el mundo entero. Le puede interesar: [Mujeres rurales trabajan más que los hombres y ganan menos](https://archivo.contagioradio.com/mujeres-rurales-trabajan-mas-que-los-hombres-y-ganan-menos/)

<iframe src="https://co.ivoox.com/es/player_ek_17438558_2_1.html?data=kpyhlZ2ZeZmhhpywj5aWaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKZlKeYvLenb7HZ09HOjabRpduZpJiSpJjSrcTVjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Jani Silva, integrante de Comunidades Construyendo Paz en los Territorios (CONPAZ) desde el Putumayo manifestó que en la actualidad **“como mujeres campesinas se nos siguen violando los derechos**, no hay facilidad para el acceso a la tierra y a los créditos es aún más difíciles. El trabajo de la mujer campesina no es valorado como se debe”. Le puede interesar: [Solo el 24% de las mujeres rurales pueden tomar decisiones sobre sus tierras](https://archivo.contagioradio.com/solo-el-24-de-las-mujeres-rurales-pueden-tomar-decisiones-sobre-sus-tierras/)

**Las jornadas de las mujeres rurales por lo general comienzan antes de las 5  a.m. y terminan sobre las 9 p.m.,** y a diario se dividen entre preparar a los hijos para ir a la escuela “sin luz, sino con un mechero” agrega Jani, cocinar y antes de salir a sus cultivos o al trabajo “deben darle de comer a las gallinas, alimentar el cerdo, dejar barrida la casa. Luego vuelve a la casa y atiende a los hijos, mira los pollos”.

Labores que se convierten en mucho más complejas por l**a falta de condiciones en varios hogares donde no hay acueducto, alcantarillado, electricidad, agua potable** y cuando las mujeres que se enferman evitan salir al casco urbano por tener que dejar las labores del hogar o del campo quietas. Realidades a las que también se enfrentan indígenas y afro.

<iframe src="https://co.ivoox.com/es/player_ek_17438578_2_1.html?data=kpyhlZ2Ze5mhhpywj5WdaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5yncavVxNDSzs7SqYzG0NLS1NSJd6Ofp9rS1N_Fb8XZjNLizMrWqdSfuMbm14qnd4a2opKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En el caso de las mujeres indígenas Wayúu, Jackeline Romero de Mujeres Fuerza Wayúu, dice que ha habido un error por parte de los hombres que han querido ver la exigibilidad de derechos como una competencia, “cuando **las mujeres indígenas no competimos por ser igual a los hombres, sino por tener igualdad de derechos, por tener vocería en toma de decisiones (…).** Lo que somos, como algunos dicen, es más atrevidas”.

### **Enfoque Diferencial para las mujeres campesinas e indígenas** 

Jani asegura que lo que se debe hacer es escuchar a las mujeres campesinas “somos nosotras las que tenemos esa problemática. **Se debe hacer una política pública con enfoque diferencial construida con las mujeres. Tienen que escuchar el sentir de las necesidades del día a día que vivimos las mujeres.** Pero que se aplique, porque en el papel todo es muy bonito pero en la realidad no se aplican”.

<iframe src="https://co.ivoox.com/es/player_ek_17438589_2_1.html?data=kpyhlZ2ZfJqhhpywj5WYaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5ynca3p25C6w9fNssKfpNrQytrRpsaZk6iYroqnd4a1pcnS1JDNssWZpJiSo6nLqc_VjMnSzpCnpdbXwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A lo que Luz Marina Cuchumbe, líder indígena del Cauca agrega “el Estado no nos ha posibilitado una serie de oportunidades en acceso a educación por lo que nosotras mismas hemos estado en varias comunidades capacitando, tratando de unirnos para trabajar en lo ecológico. Cuidar el territorio. Pero **el Estado debería voltear a ver y garantizar la educación”**. Le puede interesar: [El trabajo de las mujeres en Colombia](https://archivo.contagioradio.com/trabajo-las-mujeres-colombia/)

### **Paz y mujeres** 

Ante un escenario de posconflicto, las mujeres han manifestado de manera contundente que **la “paz sin las mujeres no va”** y si bien se lograron avances con el trabajo de la subcomisión de género, el camino por recorrer aún es largo y el trabajo por hacer es arduo. Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

**“Es difícil que llegue la paz sino hay justicia social. Es incluso difícil que exista la paz incluso para las mujeres excombatientes de la guerrilla**, en la actualidad vemos en las Zonas Veredales a las mujeres en muy malas condiciones, sin garantías de salud, acueducto. Tampoco para sus hijas e hijos. Eso es indignante para mí como mujer” dijo Jani.

Mientras que Luz Marina, aseveró que **la paz para las mujeres del campo es importante “pero siempre y cuando se nos tenga en cuenta**, porque tenemos problemas puntuales en cada lugar de Colombia”. Le puede interesar: [Así ha avanzado la lucha feminista en Colombia](https://archivo.contagioradio.com/asi-ha-avanzado-la-lucha-feminista-colombia/)

### **Participación política de las mujeres** 

Para la representante a la Cámara por el Partido Verde, **Ángela María Robledo este es un tema en el que hay que continuar el trabajo** porque "si bien ha habido un aumento en el número de mujeres que llegan en particular al Congreso de la República, muchas son elegidas con votos ajenos con votos de sus esposos, hermanos, parapolíticos y en ese punto hay que reflexionar".

Las exigencias de las mujeres en este tema es poder **tener el 50% de la participación en instancias donde se toman decisiones importantes para las mujeres y para el país** en general "insistimos en la paridad" agrega Robledo.

La representante concluye diciendo "**en el tema de las mujeres rurales el reto es enorme,** el acceso a la tierra es absolutamente limitado, determinar su condición de propietarias tiene enormes dificultades, la salud y la educación de las mujeres rurañes pues no hay una política pública que las sustente. **Pero todo esto puede convertirse en una oportunidad". **Le puede interesar: [710 mil trabajadores domésticos tendrán derecho a prima](https://archivo.contagioradio.com/710-mil-trabajadores-domesticos-tendran-derecho-a-prima/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
