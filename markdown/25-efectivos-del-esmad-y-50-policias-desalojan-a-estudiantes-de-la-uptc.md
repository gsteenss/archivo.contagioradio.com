Title: 25 efectivos del ESMAD y 50 Policías desalojan a estudiantes de la UPTC
Date: 2016-05-31 16:18
Category: Movilización, Nacional
Tags: Paro en la UPTC, Universidad Pública Colombiana, UPTC
Slug: 25-efectivos-del-esmad-y-50-policias-desalojan-a-estudiantes-de-la-uptc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/UPTC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [31 Mayo 2016]

Este lunes sobre las 4:30 de la tarde, por lo menos **25 efectivos del ESMAD, 50 Policías y 10 vehículos desalojaron a los estudiantes** de la Universidad Pedagógica y Tecnológica de Colombia que estaban acampando en la institución, en el marco del paro que adelantan desde el pasado 4 de mayo.

El desalojo no fue violento; sin embargo, para los estudiantes el despliegue de la fuerza pública, tanto en los alrededores como al interior del claustro, **fue desproporcionado y generó intimidación**, pese a que estuvo presente la Defensoría del Pueblo, verificando que se respetaran los derechos humanos.

El movimiento estudiantil rechaza la presencia de la Policía y el ESMAD en la universidad, así como los hechos que afectan las dinámicas de movilización con las que ejercen su legítimo derecho a la protesta; e insiste en que **se mantiene el pliego de exigencias, a pesar del periodo de vacaciones impuesto arbitrariamente** por la administración.

<iframe src="http://co.ivoox.com/es/player_ej_11745886_2_1.html?data=kpaklpqcfJehhpywj5WaaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncbXV1c7O0MaPmNPdwtPOjZKPmc%2Fd18rf1c7IpcWfscrRw8yJh5SZo5jUy8jFb9qftcrQ0NTQaaSnhqegyc7HpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
