Title: El verdadero costo de la Consulta Antitaurina
Date: 2015-08-26 14:25
Category: Animales, Economía, Nacional, Política
Tags: Bogotá Sin Toreo, corridas de toros en Bogotá, crueldad animal, Maltrato animal, novilladas, Plataforma Alto, Registraduría Distrital, Registraduría Nacional
Slug: realizacion-de-consulta-antitaurina-realmente-costaria-11-mil-millones-de-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/CONSULTA-ANTITAURI-e1502312696803.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Youtube.com 

<iframe src="http://www.ivoox.com/player_ek_7618687_2_1.html?data=mJuempuce46ZmKiakpiJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbbytjh1MbIs9Ofxc7g1tfNuMLgjK%2FOy9LJb7Tphqigh6aVtsbujMbgx8zZtoa3lIqvlZDVucaf08rOzs7ecYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jaime Hernando Suárez, registrador distrital] 

<iframe src="http://www.ivoox.com/player_ek_7618743_2_1.html?data=mJuempyYd46ZmKiakpiJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbVzc7nw8jNaaSnhqeg0JDIqYy30NPg19HYpYy1z9nW1sbZtsriwpDfx8bQscbi1cqYxdTXuMLmhqigh6aocYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Albeiro Ulloa, "Bogotá Sin Toreo"] 

Tras el [aval del Tribunal Administrativo de Cundinamarca frente a la realización de una consulta popular ](https://archivo.contagioradio.com/tribunal-administrativo-avalo-popular-sobre-corridas-de-toros-en-bogota/) para que el próximo 25 octubre la ciudadanía decida sobre el futuro de las corridas en la ciudad, el registrador distrital Jaime Hernando Suárez, aseguró que la realización de esta consulta tendría un costo aproximado de **40 mil millones de pesos**, sin embargo, **de acuerdo a una respuesta de la misma Registraduría** frente a un derecho de petición del colectivo “Bogotá Sin Toreo”, **el costo sería de 11 mil millones de pesos.**

"Mínimo se requiere unos 60 o 70 días por los términos contractuales. En Bogotá cualquier mecanismo de participación ciudadana requiere unos 40 mil millones de pesos. La Registraduría no cuenta en este momento con esos recursos, **cuando nos notifiquen procederemos a hacer la solicitud a la Registraduría Nacional** para que se tramiten y dependiendo del tiempo se determinará si es viable o no hacerlo en una determinada fecha", dijo el registrador distrital.

Desde el colectivo se había enviado a la Registraduría un derecho de petición donde se explicara cuánto le costaría al país hacer la consulta popular antitaurina. El pasado 3 de agosto, el ente** respondió especificando cada uno de los rugros en los que se incurriría, y de acuerdo a ese documento, el costo sería de 11´061.193.718,** si la consulta se realiza con las elecciones regionales y locales.

Por otro lado, **si la consulta popular se realizara en una fecha distinta, el costo exacto, según la propia registraduría sería de 34´471.248.005,** “es decir 6 mil millones de pesos menos a los que se está asegurando”, explica Albeiro Ulloa, vocero de “Bogotá Sin Toreo”, quien añade que es probable que la respuesta de la registraduría se deba a que el decreto expedido por el Alcalde Gustavo Petro no le ha llegado al registrador distrital.

Cabe recordar, que en el año 2000 en Bogotá, el Alcalde Mayor remitió el 23 de agosto de ese año la solicitud de la realización de la consulta popular en el Distrito Capital sobre los temas de "Día sin carro a partir del año 2001" y "Restricción vehícular a partir del año 2015", tras pasar el procedimiento, es consulta popular se logró realizar el 29 de octubre, día en que también se realizaron elecciones locales y regionales.

“La convocatoria para la consulta popular la hace por decreto el alcalde de Bogotá, cuando  nos sea comunicado tendremos que hacer la solicitud de los recursos a la Registraduría nacional (que es quien hace la contratación) y ellos a su vez se dirigirán al **Ministerio de Hacienda**”, explicó Suárez.

Por el momento se espera que llegue la notificación a la Registraduría Distrital, para que el próximo 25 de octubre **917.000 bogotanos salgan a votar NO a la realización de corridas** de toros y novilladas en Bogotá, teniendo en cuenta que el **umbral necesario es de 1´833.000 ciudadanos.**

   
   
[![?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carta.jpg){.wp-image-12889 width="588" height="809"}](https://archivo.contagioradio.com/realizacion-de-consulta-antitaurina-realmente-costaria-11-mil-millones-de-pesos/carta-2/)  
[![34 mil millones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/34-mil-millones.png){.aligncenter .wp-image-12898 width="654" height="371"}](https://archivo.contagioradio.com/realizacion-de-consulta-antitaurina-realmente-costaria-11-mil-millones-de-pesos/34-mil-millones/)[![11 mil millones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11-mil-millones.png){.aligncenter .wp-image-12897 width="674" height="359"}](https://archivo.contagioradio.com/realizacion-de-consulta-antitaurina-realmente-costaria-11-mil-millones-de-pesos/11-mil-millones/)
