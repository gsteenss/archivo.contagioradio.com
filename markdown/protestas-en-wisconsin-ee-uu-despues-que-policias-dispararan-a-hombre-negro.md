Title: Protestas en Wisconsin, EE.UU. después que policías dispararan a hombre negro
Date: 2020-08-25 11:29
Author: AdminContagio
Category: El mundo
Tags: Abuso de la Fuerza Pública, Estados Unidos
Slug: protestas-en-wisconsin-ee-uu-despues-que-policias-dispararan-a-hombre-negro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/jacob-blake-Estados-Unidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@AttorneyCrump](https://twitter.com/AttorneyCrump)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana cientos de personas salieron a protestar en el estado de Wisconsin, Estados Unidos, después de que la policía disparara repetidas veces contra un hombre negro que según fuentes, intentaba detener una pelea entre dos mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se dio a conocer por medio de un video publicado en internet y en el que se muestra al hombre, identificado como Jacob Black recibiendo varios disparos en la espalda mientras intenta entrar a su vehículo en la ciudad de Kenosha.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los habitantes de la ciudad marcharon hacia la sede de la policía el domingo por la noche. Durante las protestas, hubo incendios a vehículos y los manifestantes gritaron "No retrocederemos". 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A raíz de estas protestas, las autoridades de la ciudad declararon un toque de queda nocturno de emergencia y se reportó que utilizaron gases lacrimógenos para tratar de dispersar a los manifestantes que permanecieron en las calles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tony Evers, gobernador de Wisconsin, expresó en su cuenta de twitter que rechazaba el uso excesivo de la fuerza, añadiendo que apoya a quienes siguen exigiendo justicia. (Le puede interesar: [Nace Asociación Colombiana de Economias Negras](https://archivo.contagioradio.com/asociacion-colombiana-de-economistas-negras-hacen-un-llamado-a-la-dignidad-laboral/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el Departamento de Policía de Kenosha dijo que los agentes involucrados habían estado respondiendo a un "incidente doméstico". Sin embargo, no dieron detalles sobre lo que provocó este accionar por parte de los agentes, cuyos nombres aún no se conocen oficialmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un abogado de derechos civiles, encargado del caso de Blake confirmó en su cuenta de twitter que tres de los hijos de Jacob Blake estaban en el auto cuando le dispararon y añadió que este suceso podría dejarlos traumatizados para siempre. 

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las p**rotestas no solo fueron en las calles**
----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al conocerse este suceso, miles de personas firmaron una petición en la que pedían que los agentes que estuvieron involucrados en este suceso, fueran acusados. (Le puede interesar:[Así es el movimiento antirracista que conocimos después del asesinato de George Floyd](https://archivo.contagioradio.com/george-floyd-antirracismo-mundial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, la indignación y el rechazo no han parado de mostrarse a través de las redes sociales y el lema *Black Lives Matter (Las vidas negras también importan)* vuelve a estar en ruedo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Miles de personas vuelven a cuestionar el uso de la fuerza por parte de agentes y los hechos de racismo que se siguen presentado en el país norteamericano. Además, continúan exigiendo al gobierno, tomar medidas en cuanto a los métodos de violencia usados por la policía.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
