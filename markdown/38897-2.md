Title: FARC contará la verdad de atentado al Club El Nogal y pedirá perdón
Date: 2017-04-06 16:54
Category: Nacional, Paz
Tags: El Nogal, FARC, Perdon, víctimas
Slug: 38897-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/whatsapp_image_2017-04-05_at_18.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [06 Abr. 2017] 

**¿Cuáles fueron las razones para perpetrar el atentado contra el Club El Nogal en Bogotá en el año 2003?** Esa será una de las preguntas que responderán próximamente la guerrilla de las FARC, en un evento que junto con familiares y víctimas de dicho atentado pactaron en días pasados, que entre otros objetivos intenta ayudar a conocer la verdad y poder avanzar en procesos judiciales que se encuentran detenidos.

En el documento de una página que fue firmado entre **Bertha Lucía Fries, líder y vocera de un grupo de víctimas del atentado al Club El Nogal y Carlos Antonio Lozada, representante del secretariado de las Farc**, ambas partes hicieron el compromiso de realizar un evento público de manera que se inicie el reconocimiento de dicho atentado y haya un acercamiento a la verdad.

"Mi celular está a reventar, las reacciones han sido muy positivas y todas las manifestaciones de jubilo y alegría porque las FARC están haciendo un compromiso (...) ya serán los de las FARC que si no lo cumplen, pues será ellos los que fallan" dijo Fríes.

De esta manera, las **FARC también se comprometieron a declarar ante los tribunales de la Jurisdicción Especial para la Paz – JEP** y agregan “los representantes de las FARC- EP que esta organización determine, estarán listos a comparecer ante la Comisión de Esclarecimiento de la Verdad y ante la Sala de reconocimiento de verdad y de responsabilidades de la JEP, para el esclarecimiento de los hechos relacionados con el atentado al Club El Nogal de Bogotá”.

Así mismo, aseguraron que desde la guerrilla de las FARC-EP “**tienen toda la voluntad de implementar y participar directamente en un proceso de Reconciliación** con Valores con todas las víctimas del atentado al Club El Nogal”. Le puede interesar: [El Estado sabía que se iba a perpetrar un atentado en el Club el Nogal](https://archivo.contagioradio.com/estado-sabia-que-se-iba-a-perpetrar-el-atentado-del-club-el-nogal/)

Ante las reacciones de personas como el ex ministro Fernando Londoño, quien ha asegurado que a los socios del club no se les ha consultado sobre esa decisión Fríes añadió "primero el señor Fernando Londoño no es miembro de la Junta, es un socio y dos esto es un tema de víctimas que somos quienes estamos pidiendo que nos digan la verdad".

E hizo una invitación para que toda la comunidad del Club diga "que tan bueno que nos van a decir la verdad, eso es un principio, desde que yo estoy chiquita a mi me han enseñado que hay que decir la verdad y bajo ese principio no estamos afectando a nadie".

Por último, Bertha Fríes manifestó que se encuentran trabajando para que **el acto público de perdón pueda coincidir con la visita del Papa Francisco a Colombia** de tal manera que el sumo pontífice pueda estar presente.

"El Papa dice busquemos y tendamos puentes las víctimas para perdonar, entonces yo le hago la invitación al señor Londoño y a los que no están a favor de que pongan su mano en el corazón y se den la oportunidad de perdonar (...) entiendo que el señor Londoño es muy católico y me parece importante que escuche al Papa, no me escuche a mí, escuche al Papa" recalcó Fríes.

Fríes señaló que ven con esperanza la posibilidad de la participación del Papa Francisco como garante y testigo de un acto en el que tendría prioridad la verdad y así lograr un proceso de reconciliación.

"Hay que pedir perdón, hay que decir la verdad y ojalá que uno se pueda reconciliar porque eso si es de dos, entre víctimas y victimarios, eso es un proceso" manifestó Fríes.

<iframe src="https://co.ivoox.com/es/player_ek_18028601_2_1.html?data=kp2dlJ2adJKhhpywj5aXaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaPZ09nOjavWrcbnhpewjduJh5SZoqnQ1s7RpdSfpNHixJDJsIzC0MzOzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **REUNIONES DEL MINISTERIO DE DEFENSA CON PARAMILITARES EN EL CLUB EL NOGAL** 

Por su parte las víctimas esperan que el Estado cuente la verdad sobre las reuniones que estaba sosteniendo la **Ministra de Defensa, Marta Lucía Ramírez**, con varios líderes paramilitares, entre los cuales se menciona a **Mancuso**, en las instalaciones del Nogal, y espera que se explique por qué esas reuniones no se realizaban en la Casa de Nariño como era debido si se trataba de acercamientos oficiales.

"Nosotros mandamos una carta enn el 2014 a La Habana y pedimos que nos dijeran la verdad, ¿por qué la Fisclaía, el DAS tuvo la información en su momento de que iba a haber un atentado, porque no lo previnieron? en los expedientes reposan hasta nombres de funcionarios. El Estado tiene que pedir perdón y eso no es nuevo" concluyó Fríes.

[Acuerdo Victimas de El Nogal](https://www.scribd.com/document/344301143/Acuerdo-Victimas-de-El-Nogal#from_embed "View Acuerdo Victimas de El Nogal on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_70206" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/344301143/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-mzv6rFgohmSzqVzyXUaK&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7274193548387097"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
