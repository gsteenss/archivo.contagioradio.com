Title: Marcha estudiantil del viernes termina en nuevas  agresiones por parte del ESMAD
Date: 2019-09-29 11:34
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: ESMAD, estudiantes, exceso de fuerza pública, manifestacion pacifica, Movimiento estudiantil Colombia, Universidades
Slug: marcha-estudiantil-agresiones-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/3822176_n_vir6.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Captura-de-Pantalla-2019-09-28-a-las-7.44.36-p.-m..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título-1-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo ] 

 

Este viernes  27 de septiembre estudiantes  de más de 10 universidades públicas y privadas, salieron  a las calles a  exigir al Estado acciones sobre los casos de corrupción al interior de la Universidad Distrital y un alto al  abuso de fuerza  por parte del Escuadrón Móvil Antidisturbios (ESMAD). La movilización había sido convocada de forma pacífica, y a pesar de ello los estudiantes denunciaron infiltraciones a la marcha y agresiones del ESMAD que dejaron un saldo  aproximado de 75 detenciones arbitrarias  y un sin número  de heridos, entre ellos el estudiante Juan Guerrero de la Universidad Distrital quien perdió un ojo tras el impacto de un proyectil del escuadrón.  (Le puede interesar:[A la calle, universidades en Bogotá se unen por el desmonte del ESMAD](https://archivo.contagioradio.com/a-la-calle-universidades-en-bogota-se-unen-por-el-desmonte-del-esmad/))

### ¿Quiénes fueron los agredidos por el ESMAD ?

En un comunicado emitido por la  *Campaña Defender la Libertad: asunto de todas* , presentan que fueron 75  detenidos, al rededor de 15 eran defensores de derechos humanos , 4 extranjeros, 2 menores de edad y  3 personas ajenas a la movilización, el resto fueron estudiantes de universidades como la Distrital, Pedagógica, Andes, CUN, Católica, Santo Tomás, Nacional, Antonio Nariño, Javeriana, entre otras. (Le puede interesar:[Corrupción en la Universidad Distrital](https://archivo.contagioradio.com/corrupcion-universidad-distrital/))

Además del caso de Juan Guerrero, estudiante de la Universidad Distrital, también se encuentra el caso de un niño de 10 años que se encontraba perdido, y denunció ante un defensor de derechos humanos que un policía lo amenazó con golpearlo con un bolillo, ante esto la Campaña  hizo un  llamado a la **Personería de Bogotá**, a la **Defensoría del Pueblo**, a la **Procuraduría General de la Nación** y al **Ministerio del Interior** a investigar  el uso desmedido de la fuerza por parte de miembros de la Policía Metropolitana de Bogotá y del ESMAD.

### Los hechos...

En el comunicado emitido por la Campaña señalaron que todo iba según lo acordado hasta que a las 5:10pm en el Parque de los Periodistas, el ESMAD decidió  intervenir la marcha sin ninguna razón,  "era claro que la movilización ya había terminado y los estudiantes y demás personas se encontraban en el parque concentrados de manera pacífica" señala la organización. (Le puede interesar:[Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención](https://archivo.contagioradio.com/esmad-olvida-que-tiene-protocolos-intervencion/))

Así mismo resaltaron  que  luego de  las acciones de agresión por parte del ESMAD, activaron la Alerta Temprana preventiva que tienen preparada para situaciones como esta, y así evitar nuevas violaciones a derechos humanos como judicializaciones y agresiones a la integridad física y psicológica de  los estudiantes que se encontraban allí; sin embargo a pesar de esto fueron inevitable los arrestos, vulneraciones y ataques hacia los asistentes, "la intervención del ESMAD generó confrontaciones con los manifestantes, empiezaron a utilizar aturdidoras y marcadoras y luego se dieron varias detenciones arbitrarias hacia quienes veíron ´sospechoshos´", describieron en el comunicado.

Finalmente  destacaron  que las detenciones se realizaron tanto a manifestantes como a  personas ajenas a la movilización, arrestos que tuvieron que ser levantados por falta de argumentos que inculparan a los detenidos,"alegamos que sus detenciones, en la mayoría, fueron arbitrarias", señalaron en el comunicado. (Le puede interesar: [Las 10 exigencias del movimiento estudiantil al Gobierno Nacional](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

### La lucha sigue

Ante estos casos reiterativos de violación a sus derechos humanos, los estudiantes convocan a una movilización nacional  este 10 de octubre, bajo el lema *´fuerza y resistencia´* donde marcharán en contra de la represión estatal ,respeto de los acuerdos firmados en diciembre y por la defensa de la educación pública y de calidad.(Le puede interesar: [Estudiantes de la Universidad Distrital tienen razones de sobra para protestar](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
