Title: Comunidad de Pinchanaqui en Perú no permitirá permanencia de Pluspetrol
Date: 2015-02-12 17:21
Author: CtgAdm
Category: El mundo, Entrevistas, Resistencias
Tags: Frente de Defensa Ambiental de Pinchanaqui, Perú, Pluspetrol
Slug: comunidad-de-pinchanaqui-en-peru-no-permitira-permanencia-de-pluspetrol
Status: published

###### Foto: radioexitosa.pe 

<iframe src="http://www.ivoox.com/player_ek_4074914_2_1.html?data=lZWklp6VeI6ZmKiak5mJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmzdTgjajMpdfV09fWw4qWh4y608rb1sqPqMafs8rgy9jYqc%2FXysaYo9LGrcbi1cbZjcnJb7HdjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Chavarria, Frente de Defensa Ambiental de Pinchanaki] 

Desde el 9 de Febrero la comunidad del **Pinchanaqui, en Perú**, decidió no permitir que la empresa Pluspetrol desarrolle actividades de exploración o explotación en sus territorios. Los antecedentes son claros, en otros municipios como Junin, la empresa destruyó las fuentes de agua e impidió que se pudiera desarrollar la vida por los **altos niveles de metales pesados, tanto en la tierra, como en el aire y el agua.**

La denuncia de esta situación la hace, desde hace varios años, el **Frente de Defensa Ambiental de Pinchanaqui**, del cual hace parte Carlos Chavarría. Según la información de la comunidad, la mañana de este jueves se pudo **confirmar la muerte de tres personas durante la represión policial** del martes y miércoles pasado.

Según el relato de Chavarría, la comunidad decidió entrar a la base militar dentro de la cual se ubica el campamento de la empresa para destruir la maquinaria e impedir la destrucción ambiental, pero la reacción de la policía fue **disparar con armas largas y cortas en contra los manifestantes.**

Desde el año 2012, las comunidades campesinas e indígenas que habitan la región de Pinchinaqui, vienen **exigiendo la presencia del gobierno nacional** para expresar su decisión, pero ante la negativa de los organismos del Estado, decidieron actuar y se mantienen en firmes en que no permitirán la entrada de la empresa Pluspetrol.

Para este jueves se espera la llegada de una comisión de gobierno que, según Chavarría, solo se logró luego de la fuerte represión y la visibilidad internacional que ha tenido la situación.
