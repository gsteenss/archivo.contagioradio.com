Title: MIDBO 20 años de documental abre sus convocatorias
Date: 2018-05-28 15:25
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, MIDBO
Slug: midbo-20-convocatoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/MIDBO-20.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:MIDBO 

###### 29 May 2018 

En su aniversario número 20 de trabajo ininterrumpido por la exhibición, la reflexión y el diálogo alrededor del cine de lo real, la Muestra Internacional Documental de Bogotá MIDBO, abre sus convocatorias para que los realizadores nacionales y extranjeros participen con sus producciones hasta el próximo 30 de junio.

Alados, corporación colombiana de documentalistas responsable del evento, busca que participen de la convocatoria todas aquellas expresiones del cine de lo real como son: el documental de autor, de investigación, experimental y etnográfico, expresiones audiovisuales comunitarias, étnicas y de género, el ensayo, las exploraciones del universo de lo íntimo, los formatos híbridos y el collage, entre otros.

Entre los requisitos que deben cumplir las producciones se encuentran el haber sido finalizadas a partir de enero de 2017, de duración, formato o temática libre. La selección de las obras estará a cargo de un comité integrado por curadores nacionales e internacionales, en las categorías que para este año son:

· **Selección Nacional:** acoge producciones recientes de autores colombianos residentes en el país o en el extranjero.

· **Otras miradas:** selección internacional conformada por películas documentales recientes, de autores cuya nacionalidad sea diferente a la colombiana.

· **Mirada Emergentes:** ofrece un espacio para autores latinoamericanos\*, cuyas obras hayan sido producidas en el marco de un programa de formación académica de pregrado en el campo audiovisual, así como aquellas realizadas en el marco de programas de educación no formal, o de procesos comunitarios. De igual forma, la categoría está abierta a primeras obras de autores empíricos o autodidactas.

\* Brasil y países de habla hispana de Centro América, Sur América y el Caribe.

Las inscripciones de las obras se realizarán a través de la plataforma online MOVIBETA: [www.movibeta.com](http://festival.movibeta.com/web/controllers/siteController.php). Los interesados en participar deben diligenciar y enviar el formulario de inscripción. Los documentales seleccionados serán publicados el 30 de julio y deben ser enviados entre el primero y el veinte de agosto. Las bases completas de la convocatoria se ecuentran [aqui](http://festival.movibeta.com/web/views/bases/1526642446.9462.pdf).

![MIDBO2018\_FB](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/MIDBO2018_FB-800x296.jpg){.alignnone .size-medium .wp-image-53589 .aligncenter width="800" height="296"}

**Sobre la MIDBO**

La Muestra Internacional Documental de Bogotá ha proyectado cerca de 2.000 películas provenientes de 70 países. Aproximadamente 80 mil personas han asistido a las salas de exhibición y un gran número de creadores nacionales y extranjeros, han tenido la oportunidad de encontrarse con el público en eventos académicos y de exhibición.

La 20ª Muestra Internacional Documental de Bogotá, que tendrá lugar entre el 3 y 10 octubre, se plantea como una oportunidad para revisar el desarrollo del documental en los últimos 20 años. Con este propósito, la programación de la MIDBO se articula alrededor de la presentación de películas nacionales e internacionales, la muestra de Documental Expandido, y la realización del Encuentro Internacional Pensar lo Real, que incluye conferencias magistrales de cineastas invitados, ponencias de investigadores y el desarrollo de diferentes grupos de trabajo.

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 

######  
