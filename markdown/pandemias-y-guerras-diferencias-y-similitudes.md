Title: Pandemias y guerras: diferencias y similitudes
Date: 2020-05-01 13:31
Author: CtgAdm
Category: Opinion
Tags: pandemia Estados Unidos guerra Vietnam Colombia Covid 19
Slug: pandemias-y-guerras-diferencias-y-similitudes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/gas-mask-469217_640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Por:* ***Jaime Ordóñez***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el inicio de la actual pandemia se ha escuchado en muchas ocasiones que el mundo está en guerra contra el COVID-19, esta afirmación se ha vuelto un lugar común. Cuando esto sucede se pierde la fuerza para llamar la atención, pues ya “hace parte del paisaje” y sólo se vuelve un término que no tiene un significado concreto. Para poner en perspectiva lo que significa el término “la guerra contra el COVID-19”, **sería útil comparar lo que se está observando en el mundo y viviendo Estados Unidos, en particular, con un suceso histórico de magnitud similar**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los Estados Unidos iniciaron su participación con tropas militares en la guerra de Vietnam el 8 de marzo de 1965 con el desembarco de los primeros 3.500 marines cerca a Na Dang, en el sur del actual Vietnam. Hasta entonces, su vinculación había sido a través de asesores militares que no estuvieron directamente en el campo de guerra. Su retiro de Vietnam, el cual permitió finalizar este choque bélico, sucedió el 30 de abril de 1975 con la evacuación de los últimos diez marines que quedaban en la embajada americana en Saigón. **Durante los 3.706 días en los que Estados Unidos participó militarmente en ese conflicto, murieron 58.220 soldados**, según los Archivos Nacionales de ese país (archives.gov). Esta guerra ha sido uno de los fenómenos históricos sobre el cual se han hecho más películas, desde Apocalipsis Now, de Francis Ford Coppola, pasando por Forrest Gump, hasta la recientemente premiada The Post.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nadie duda que esta fue una guerra terrible, que generó muchas víctimas en todos los bandos y en la que Estados Unidos de forma voluntaria, por las razones que fueran, decidió participar. Y en la que, de forma voluntaria, por las razones que fueran, decidió retirarse. Esa es la ventaja de participar en una guerra en suelo extranjero, se puede entrar o salir cuando se quiera, sólo se necesita un argumento para justificar el ingreso o el retiro. Lo único que realmente importa es que la guerra no sea dentro del propio territorio de quien desee participar en ella, pues todo lo que se destruye en una guerra al final hay que reconstruirlo, y ese es un costo que cualquier gobierno quiere evitar. Una de las principales características que tuvo **la guerra de Vietnam fue la de ser la primera guerra que fue televisada en directo, tanto las confrontaciones armadas como la llegada de soldados en ataúdes cubiertos con la bandera de su país**. Esas imágenes, de tantos soldados muertos, fueron precisamente uno de los aspectos que más contribuyó a las protestas de la opinión pública contra su gobierno, pidiéndole el retiro de las tropas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante los 10 años que Estados Unidos participó en la guerra de Vietnam fallecieron en promedio casi 16 soldados por día, cada 92 minutos durante 3.706 días, un soldado estadounidense, fue asesinado una verdadera tragedia por donde se le mire.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 28 de abril de 2020, la Universidad Johns Hopkins reportó en su sitio web que las muertes por COVID-19 en Estados Unidos llegaron a la terrible cifra de 59.266 personas. La primera víctima se presentó el pasado 29 de febrero, pero el 80% de estas muertes se han presentado en los últimos 23 días. Es decir, **diariamente han fallecido casi 2.040 personas, cada 42 segundos ha fallecido una persona en los Estados Unidos por COVID-19 durante los últimos 23 días**, durante casi 130 veces más que los muertos por día que hubo en la guerra de Vietnam. Una enfermedad de la que no se sabía de su existencia al iniciar el año, pero que muy posiblemente será la palabra más usada en Google al final de 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La diferencia entre las guerras y las pandemias es que las personas y los gobiernos pueden decidir cuándo comenzar y cuándo terminar las guerras**, pueden decidir el campo de batalla en el que prefieren luchar, pueden decidir la cantidad de personas y armamento que deseen utilizar. En las guerras, hasta el enemigo se puede elegir. Esa es la principal diferencia con las pandemias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es un fenómeno natural, por tanto, **no hay forma de decidir cuando comienza ni cuando termina, no hay forma de elegir el campo de batalla, no hay forma de elegir al enemigo.** El enemigo nos elige, aunque nosotros no queramos. Es la fuerza de la naturaleza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero las pandemias también tienen cosas comunes con las guerras, las personas y los gobiernos pueden desarrollar estrategias para contenerlas y mitigar su daño económico y su afectación a la salud. De hecho, es interesante que la vigilancia en salud pública (también llamada vigilancia epidemiológica), que es la herramienta que se utiliza para hacer seguimiento al comportamiento de los fenómenos que afectan la salud de las poblaciones, tiene bastante terminología militar: vigilancia, seguimiento de caso, reporte obligatorio, entre otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Las pandemias, al igual que las guerras, se enfrentan con estrategia, con astucia, con un pensamiento holístico que incluye evaluar los resultados sobre la salud y la economía**. Con equipos multidisciplinarios que permitan considerar todas las opciones y todas las visiones posibles del problema, sin hipocresía, porque las guerras sólo dejan perdedores, eso significa que la sociedad también debe determinar un umbral para establecer hasta dónde está dispuesta a aceptar pérdidas de vidas y pérdidas económicas. Las guerras nunca dejan vencedores. Unos perderán más que otros, pero al final de una guerra, sólo hay perdedores. Por eso es importante establecer hasta dónde se está dispuesto a perder, porque de eso depende mucho de hasta dónde se está dispuesto a invertir para disminuir las pérdidas. Porque en las guerras hay que gastar recursos para disminuir las pérdidas, no hay ganancias. Y los recursos son de capital humano y económico. Así que entre más bajo sea el nivel de tolerancia ante las pérdidas, mayor esfuerzo debe hacerse para acabar con la guerra lo más pronto posible, por eso debe establecerse ese umbral de pérdidas desde el principio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero lo que ha hecho la gran diferencia en todas las guerras, y ha ayudado a que la balanza se incline hacia determinado bando, es lo que suele llamarse la inteligencia militar. Que no es otra cosa que información fiable obtenida en el menor tiempo posible. Ambas características deben cumplirse. Si la información es fiable, pero se necesita demasiado tiempo para obtenerla no será útil, porque las pérdidas económicas y en vidas humanas, tanto en la guerra como en las pandemias, serían inaceptablemente altas para la sociedad. **Si la información se obtiene rápido pero no es fiable tampoco es útil, porque las decisiones que se tomen con información incorrecta indefectiblemente serán decisiones incorrectas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nadie desea tener que enfrentar una guerra, pero si no hay opción, lo ideal es que esta sea tan corta como sea posible, con el fin de tener la menor cantidad de pérdidas económicas y de vidas. **Lo mismo sucede con las pandemias, así pues, el tiempo no es el factor más importante para tomar decisiones en el manejo de una pandemia, es el ÚNICO factor importante**. Pero las decisiones deben estar soportadas en información fiable, de lo contrario, sería como pilotear un avión sin instrumentos, no sabríamos dónde estamos, ni a dónde nos dirigimos, ni cuándo vamos a aterrizar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La información de mayor importancia en una pandemia es la identificación oportuna de las personas infectadas, con el fin de aislarlas lo más pronto posible para que no infecten a otros individuos y para que reciban tratamiento médico en caso de ser necesario. No sólo se trata de hacer pruebas de laboratorio para identificar a las personas infectadas y aislarlas, también deben hacerse en el menor tiempo posible. Uno de los aspectos más graves del COVID-19 no son las personas con síntomas sino las personas sin síntomas, que son la mayoría de los infectados, pues estos individuos dan por sentado que no tienen la enfermedad dado que no tienen síntomas, y en realidad estos son los que infectan a más personas porque no están aislados. La información necesaria en esta guerra contra el COVID-19 no es otra cosa que un programa masivo de pruebas de laboratorio que incluya no sólo a los sospechosos de tener la enfermedad sino también a muchas personas que aparentemente no tienen la infección, con el fin de aislarlos oportunamente. **Desafortunadamente, el país no ha hecho el número de pruebas necesarias que permitan identificar rápidamente a los infectados**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aproximadamente medio centenar de países, especialmente en Asia y Europa, ya han logrado controlar la pandemia. ¿Cuál fue su secreto? Información fiable y rápida. Estos países hicieron pruebas en al menos al 1% de la población, esto permitió aislar rápidamente a los infectados sin síntomas para para evitar que no contagiasen a otras personas. **Lo que se ha observado en diferentes países es que entre más tiempo se tardan en hacer pruebas, más se tarda en controlar la pandemia, así que esto no es nada nuevo,** deben hacerse muchas pruebas en un corto período de tiempo que incluya a personas sin síntomas, esto se llama un sistema de vigilancia activo, es decir, se busca a los infectados (aunque ellos mismos no sospechen que están infectados) en vez de esperar que aquellos que tengan síntomas vayan a consultar, que es un sistema de vigilancia pasivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque no son economías comparables entre sí, Corea del Sur, cuyo tamaño de población es ligeramente mayor al de Colombia, a la fecha sólo ha reportado 244 muertes, mientras que Colombia ha reportado 269. En los últimos cinco días, en Corea del Sur han fallecido tres personas, en Colombia han fallecido 54. La economía ya se ha reactivado normalmente en el país asiático. Debe subrayarse que ningún otro país ha logrado un control tan rápido de la pandemia como lo hizo Corea del Sur, y dadas las diferencias socioeconómicas entre ambos países, no puede pretenderse obtener tales resultados en Colombia. Pero también debe evitarse a toda costa llegar al punto en el cual se encuentra Estados Unidos, pues tampoco tenemos la capacidad económica para soportar una muerte cada 42 segundos durante 23 días consecutivos por causa de COVID-19. **Estas son las dos caras de la moneda al evaluar los resultados que ha dejado esta pandemia en el mundo, y su principal diferencia radica en la velocidad y esfuerzo que ambos países han hecho para enfrentarla**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Está claro que enfrentar la guerra contra COVID-19 por medio de un sistema de vigilancia pasivo gastará mucho tiempo, lo que significará un alto costo en vidas y para la economía. Por eso es tan importante establecer el umbral de sacrificio que tiene una sociedad, porque esto permite determinar cuánto debe invertirse para disminuir las pérdidas. **Nadie quiere una guerra, pero cuando esta llega, debe acabársela los más pronto posible**.

<!-- /wp:paragraph -->
