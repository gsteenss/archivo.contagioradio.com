Title: Luego de reunión de exparamilitares y Farc-Ep ¿Sí van a decir la Verdad?
Date: 2017-07-24 10:54
Category: Abilio, Opinion
Tags: comision de la verdad, FARC, fredy rendon herrera, paramilitares
Slug: la-reunion-de-exparamilitares-y-las-farc-ep-si-van-a-decir-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/reunion-paramilitares-y-farc-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo particular 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 24 Jul 2017 

Ante la rampante impunidad de nuestro país, la  Verdad es la única esperanza de avanzar hacia una auténtica reconciliación. En su momento mientras  se discutía en la Habana  los términos de la Comisión para el Esclarecimiento de la Verdad preocupaba a  algunos sectores de la sociedad el que el informe que  se pudiera producir no tuviera alcances jurídicos. Para  otros, justo  el que no los tuviera, se convertía en  garantía de que no se dijeran verdades a cuenta gotas, amañadas, a conveniencia, tal como ocurrió en el caso de los procesos de la ley 975, respondiendo a los cálculos de  la sanción penal o hasta restaurativa que se pudiera imponer y los poderes que se pudieran tocar.

Apostamos por que el informe de la Comisión de la Verdad  tenga valor en sí mismo, que la verdad en una sociedad   en que  sus gobernantes ejercen el poder desde la mentira, pueda salir a la luz pública sin tapujos, sin medir los costos;  que garantice efectivamente el esclarecimiento de crímenes, de sus responsables directos, pero fundamentalmente de sus determinadores quienes son los beneficiarios de la impunidad del Estado,  vinculados a políticos, empresarios, sectores de las iglesias, medios de información, altos mandos militares, jefes de Estado y otros países.

Ahora el problema está en si existen las condiciones suficientes para garantizar que la verdad que expresen sectores vinculados con el paramilitarismo,  se exprese. Ya en la reforma constitucional que hizo el congreso para dar vía libre a la Jurisdicción Especial de Paz,  el miedo a esa verdad por parte de los congresistas, la mayoría de ellos de la “Coalición por la paz” echó al traste con la posibilidad de que comparecieran ante el Tribunal Especial de Paz los terceros responsables, excepto si se demuestra que una colaboración económica a los paramilitares, condujera a un crimen de lesa humanidad, algo casi imposible de probar. La financiación del  paramilitarismo dejó de ser grave  para los hacedores de leyes en Colombia. Ante esta realidad, queda entonces la posibilidad de que la verdad, al menos, sea confesada  ante los Comisionados de la Comisión de Esclarecimiento de la Verdad.

Pero también está la disposición real de quienes conocen la verdad y que  han sido victimarios, para confesar lo que conocen en toda su extensión: ¿Cuando, cómo, dónde, quienes ordenaron, financiaron y  se beneficiaron de sus crímenes?.

Este fin de semana nos sorprendimos con la noticia de la reunión sostenida entre miembros de las Farc-Ep y exparamilitares. Trascendió  en medios de información la alegría presidencial por el encuentro del  que “le gustó la foto”, las satisfacción que produjo  para algunos de los mediadores y sectores de víctimas por indicar un paso hacia la “reconciliación” y por parte de alguno de los paramilitares su  manifestación de “comprometerse  con la verdad”.

Los exparamilitares ya en otros momentos han hablado de  su “compromiso con la verdad”, otros han dicho que el país “no está preparado para conocer la verdad” y algunos de ellos en sus versiones ante fiscales y jueces de “Justicia y Paz” dijeron algunas verdades, en algunos casos tan a medias que no tuvieron efectos directos en los sectores de poder que resultaron mencionados, en particular en los empresarios. Quizás quien más se ha atrevido a decir verdades es el paramilitar-empresario bananero Raul Hazbún.

Un ejemplo de la tímida verdad expresada lo representa uno de los exparamiltares asistentes a la reunión con los miembros de las Farc-Ep, Fredy Rendón Herrera -El Alemán-. De empresas presentes en la región de Urabá y Bajo Atrato,  ha dicho  que algunas  le financiaron. Pero poco ha dicho de las circunstancia de modo, tiempo,  en que lo hicieron. Pero más aún. Nada ha dicho de la alianza empresarial con ellos ni del   proyecto político, social, económico del que fueron solo una pieza.

En  la nueva fase que estaría iniciándose públicamente con  la divulgación de éste dialogo, ¿estaría dispuesto a hablar el “Alemán”, por ejemplo, de todos los pormenores de la conformación, financiación, constitución legal, de la C.I Multifruits S.A? ¿Hablaría de los pormenores de la   alianza comercial que estableció con la transnacional estadounidense Del Monte? ¿Hablaría de los detalles  de la estructuración, conformación  y vínculos institucionales de alto nivel con el gobierno de Alvaro Uribe a través de la Asociación Asocomun? Estaría dispuesto a revelar sus relaciones con miembros  de la junta directiva de empresas bananeras como la C.I Unibán?

Las verdades al alcance de estos exparamilitares es la que necesita conocer el país, pero hay muchos poderes que serían tocados. ¿Estarán sinceramente  dispuestos a hacerlo?

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
