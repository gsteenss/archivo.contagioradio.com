Title: Llega por segunda vez el International Film Festival a Bogotá
Date: 2016-09-18 13:00
Category: Cultura, eventos
Tags: Biif, Cine colombianao, Festivales de cine
Slug: llega-por-segunda-vez-el-international-film-festival-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/2016-BIFF.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BIFF 

###### 16 Sep 2016 

En su segunda versión llega a la capital el Bogotá Film Festival (Biff), el cual se realizará entre el 6 y 15 de octubre, un evento dirigido a los amantes del cine, que para este año presentará varios largometrajes nacionales e internacionales.

En esta edición se proyectarán 55 películas de 29 países, como “Heidi” del director Mohamed Ben Attia, ganadora a mejor ópera prima y un Oso de Plata; "United States of Love" del director Tomasz Wasilewski, ganadora del Oso de Plata al Mejor Guión y Valderrama de Abbas Amini nominada a Mejor Ópera Prima, todas estas en la reciente edición del Festival Internacional de Cine de Berlín.

A su vez se exhibirán otras piezas audiovisuales como el documental “Safari” del director austriaco Ulrich Seidl, “"Toni Erdmann" de la alemana Maren Ade, nominada en el más reciente Festival de Cannes y ganadora del premio Fripresc y “Café Society” del director Woody Allen, siendo esta última la que inauguró el Biff.

En cuanto al cine colombiano se proyectarán las películas: “La mujer del animal” de Víctor Gaviria, “Pariente” la ópera prima de Iván Gaona, “Jericó, el infinito vuelo de los días” de Catalina Mesa y “La nueva Medellín” de Catalina Villar.

Adicionalmente, el biif este año sumará en su programación por primera vez una fusión con el mundo de Hitchcock, Murnau y Ruttmann junto con cuatro DJ´s que traerán la rumba y diversión a los asistentes. La presentación de una retrospectiva de la casa productora Rizoma por sus 15 años, con la exposición del largometraje “Gigante” de Adrián Binieze, una de las cintas latinoamericanas más premiadas de la última década. Del mismo modo, se realizará un Foro de Industria que promoverá la reflexión, producción y desarrollo local de contenidos.

Las funciones estarán en las salas de cine de Avenida Chile, Centro Comercial Andino, Hacienda Santa Bárbara, La Cinemateca Distrital y Calle 100.

 
