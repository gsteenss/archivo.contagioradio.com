Title: Comunidades de la Guajira fueron excluidas de discusión sobre el futuro del Arroyo Bruno
Date: 2019-07-04 19:12
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: arroyo Bruno, Guajira, río Ranchería, Wayuu
Slug: arroyo-bruno-afecta-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Arroyo-Bruno.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

Este jueves 4 de julio **las comunidades Wayúu y Afro de la Guajira denunciaron su exclusión y la de expertos independientes**, en la mesa interinstitucional que tuvo lugar el 5 y 6 de junio en el marco de la discusión **sobre el desvío del Arroyo Bruno por parte de las entidades del gobierno**, y en particular, de los delegados del Ministerio de Ambiente y Desarrollo Sostenible (MADS).

Según la denuncia, a puerta cerrada, las instituciones decidieron no levantar el tapón que actualmente desvía el afluente que afecta no una sola comunidad sino también cascos urbanos como Maicao, el municipio de Barranca, Albania y de Ato Nuevo. Así mismo, este fallo judicial en contra de la Empresa y de las instituciones, advierte que la problemática que se presenta por **esta explotación de carbón está afectando a 400 comunidades**, y en especifico aquellas que están relacionadas con el Arroyo Bruno, pero solo hasta el 6 de junio las comunidades fueron convocadas para comunicárseles de esta decisión.[(Lea también Comunidad afro de La Guajira bloquea entrada a sede del Cerrejón)](https://archivo.contagioradio.com/comunidad-afro-de-la-guajira-bloquea-entrada-a-sede-del-cerrejon-2/)

La mesa fue creada por el Tribunal de Riohacha en mayo de 2016 en virtud de la acción de tutela interpuesta por las comunidades en contra de las obras que amenazan el ecosistema del bosque seco-tropical, y la supervivencia de los habitantes de esta región. En ese sentido, la población debería ser consultada sobre la decisión de retirar el tapón que en este momento desvía el Arroyo Bruno, uno de los principales afluentes del Rio Ranchería, para extraer 35 millones de toneladas de carbón del yacimiento El Cerrejón y que representa un riesgo para las comunidades como señala la Corte Constitucional [SU-698 del 2017](http://www.corteconstitucional.gov.co/relatoria/2017/SU698-17.htm). [(Le puede interesar: Sigue el riesgo para el Arroyo Bruno a pesar de fallos de la Corte Constitucional)](https://archivo.contagioradio.com/cerrejon_arroyo_bruno_mineria_corte_constitucional/)

Por su parte, la abogada Rosa María Mateus Parra, integrante del Colectivo de Abogados José Albear Restrepo (CAJAR), denunció la falta de participación efectiva en esta mesa por parte de las comunidades y sobre todo, de los técnicos independientes que tendrían como objetivo clarificar informaciones según lo establecido por la sentencia emitida por la Corte Constitucional.

**«Estamos en frente de un panorama de asimetría de poderes, la participación de estas comunidades que tendría que ser garantizada no es real»**, declaró la abogada.

Por esta razón, los congresistas de oposición han convocado para el próximo martes 9 de julio desde las 8 a.m., en el Centro Cultural de Riohacha la Audiencia Regional: **“Conflictos ambientales y vulneración a los derechos a la salud, agua, soberanía alimentaria y a la participación, en ocasión del proyecto de desviación del cauce del arroyo Bruno en La Guajira”**; posterior a la visita de verificación de la obra programada para los días 7 y 8 de julio con las mismas comunidades, a cargo del CAJAR, el Centro de Investigación y Educación Popular (CINEP) y CENSAT AGUA VIVA.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
