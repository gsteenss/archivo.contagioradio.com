Title: Los retos de Dolly Montoya como rectora de la Universidad Nacional
Date: 2018-03-23 15:38
Category: Educación, Nacional
Tags: Dolly Montoya, Heiner Gaitan, Universidad Nacional
Slug: los-retos-de-dolly-montoya-como-rectora-de-la-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/universidad-nacional-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: skyscrapercity] 

###### [23 Mar 2018] 

Dolly Montoya es la primera mujer nombrada como rectora de la Universidad Nacional y llega en un momento crucial, en donde deberá asumir retos frente a la financiación de la universidad y el déficit que la misma tiene, la autonomía universitaria, **la búsqueda de recursos para la investigación y la construcción de una academia para la generación de paz.**

Montoya es egresada de Química Farmacéutica de la Universidad Nacional, PhD en Ciencias y de acuerdo con Heiner Gaitán, representante estudiantil de posgrados al Consejo Académico, de Montoya el estudiantado espera compromisos firmes frente a recuperar la universidad pública.

### **Los retos** 

El primer reto que debe afrontar Montoya, según el representante estudiantil es la financiación de la universidad **“la rectora debe asumir una posición política ante el gobierno nacional para que se aumente el financiamiento** y el presupuesto para la Universidad Nacional”. (Le puede interesar: ["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

El segundo reto, de acuerdo con Gaitán, tiene que ver con “modificar el círculo vicioso para que no sean los posgrados los que financien la deuda y el pasivo que tiene la universidad”, sino que se busquen otras formas de saldar esta deuda y así garantizar un mayor acceso a los estudios de posgrados.

El tercer reto que debe asumir Montoya es la investigación de las facultades, que, para Gaitán, no solo puede tener un fin lucrativo, sino que también debe estar al servicio de las necesidades y problemáticas de la sociedad **“la investigación se ha convertido en un instrumento con el cual se obtienen recursos sin importar a quién se le investiga**”.

La autonomía universitaria también entra dentro de la lista de prioridades desde los estudiantes para la rectora, en donde deberá garantizar que la injerencia de actores externos, no corte el funcionamiento de la universidad ni afecte las dinámicas del movimiento estudiantil ni sindical.

Finalmente está el reto de la academia y la construcción de paz que ha venido desempeñado la Universidad Nacional, como una de las instituciones más importantes que no solo apoyo al proceso de paz entre el gobierno y la FARC, sino que también se ha comprometido con la implementación y **pedagogía de los acuerdos de paz, camino que se espera Montoya continúe profundizando**.

### **La falta de democracia en la Universidad Nacional** 

Sin embargo, Heiner Gaitán recordó que la elección con la que se designó a Montoya como rectora de la Universidad Nacional sigue careciendo de democracia y participación por parte del estudiantado, debido a que ella fue seleccionada **por el Consejo Superior y no por la comunidad académica en general**.

En ese sentido, Andrés Salazar, representante de los estudiantes en esta instancia, se abstuvo de votar y presento una moción para que en la próxima sesión se estudie el mecanismo de elección del rector para que la decisión **de la comunidad académica no sea solamente consultoría sino también vinculante. **(Le puede interesar: ["Elecciones a rector a la Universidad Nacional, sin garantías para la democracia universitaria")](https://archivo.contagioradio.com/elecciones-rector-universidad-nacional/)

<iframe id="audio_24767807" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24767807_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
