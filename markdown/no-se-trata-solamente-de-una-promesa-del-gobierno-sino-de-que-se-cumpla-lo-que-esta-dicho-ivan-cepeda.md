Title: Ivan Cepeda: "No se trata solamente de una promesa del Gobierno, sino de que se cumpla lo que está dicho"
Date: 2020-04-24 11:52
Author: CtgAdm
Category: Actualidad, Entrevistas
Slug: no-se-trata-solamente-de-una-promesa-del-gobierno-sino-de-que-se-cumpla-lo-que-esta-dicho-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-79.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*[Ivan Cepeda Castro](https://www.ivancepedacastro.com/inicio/)*, defensor de derechos humanos y senador de la República en dialogo con Contagio Radio, desarrolló diferentes temas como el fondo de Ministerio de Defensa, los decretos que mas le preocupan y la urgencia de iniciar diálogos entre el Gobierno y ELN.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### Contagio Radio: ¿Los 10 mil millones de pesos que designó el Ministerio de Defensa para combatir la crisis actual es un aporte o solo un lavado de cara de las Fuerzas Militares? {#contagio-radio-los-10-mil-millones-de-pesos-que-designó-el-ministerio-de-defensa-para-combatir-la-crisis-actual-es-un-aporte-o-solo-un-lavado-de-cara-de-las-fuerzas-militares .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Ivan Cepeda:** Considero que esto es un avance. En Colombia el presupuesto de las Fuerzas Militares ha sido tradicionalmente intocable; a pesar de que se solicitó un billón de pesos, destinando inicialmente a la compra de armas, y que hoy es vital para el sector de salud, para los sueldos, los centros médicos y los insumos vitales para salvar vidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien no era lo que deseábamos, es una decisión a la cual le haremos seguimiento, **esto no se trata solamente de una promesa, sino de que se cumpla lo que está dicho.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### CR ¿cómo haría esta veeduría ? {#cr-cómo-haría-esta-veeduría .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

IC: Lo hará una comisión de senadores, quienes van a evaluar si efectivamente se han destinado esos recursos a los objetivos señalados en el documento que nos entregó el ministerio de defensa, y sí por supuesto se cumple o no con ese compromiso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### CR ¿ En los últimos días hemos visto un aumento de la erradicación forzada, y agresiones de la Fuerza Pública, es posible una discusión sobre estos temas con el Ministro de Defensa? {#cr-en-los-últimos-días-hemos-visto-un-aumento-de-la-erradicación-forzada-y-agresiones-de-la-fuerza-pública-es-posible-una-discusión-sobre-estos-temas-con-el-ministro-de-defensa .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

IC: En los próximos días tendremos una sesión de la plenaria del senado con el señor Ministro para abordar este y otros temas que nos preocupan mucho, como las anunciadas operaciones militares en las aguas del mar Caribe de una flota de embarcaciones militares de Estados Unidos, una flota para, supuestamente, realizar tareas de intervención de narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### CR ¿Cuáles son los decretos expeidos en emergencia que más le preocupan senador Ivan Cepeda? {#cr-cuáles-son-los-decretos-expeidos-en-emergencia-que-más-le-preocupan-senador-ivan-cepeda .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

IC: No son precisamente tres, por ejemplo el decreto que le impone al personal de la salud obligaciones laborales, lo cual es evidentemente una violación de sus derechos, o el decreto mediante el cual el gobierno le quita los recursos a las autoridades locales, también el decreto de la emergencia carcelaria, o el propio decreto que crea las sesiones virtuales del congreso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo hoy en día p**ara obtener información sobre lo que está haciendo el Gobierno nos toma 20 días, lo que antes tardaba cinco**; con el agravante de que la autoridad a la cual le formulamos peticiones puede tomar otros 20 días para darnos información, eso es un obstáculo enorme para realizar un control político de calidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### CR: ¿Qué se está haciendo en el Congreso para salvar vidas en lugar de empresas? {#cr-qué-se-está-haciendo-en-el-congreso-para-salvar-vidas-en-lugar-de-empresas .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

IC: Hay que pensar en que el confinamiento no puede ser eterno pero efectivamente es irresponsable sacar a las calles cerca de siete millones de personas, sin ningún tipo de control, una flexibilización que no solo está en los sectores que pueden circular ahora, sino también en medidas como la posibilidad de salir a ejercitarse, lo cual puede convertirse en una situación incontrolable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero lo mas grave es que el confinamiento tenía por objeto generar condiciones de [atención médica](https://archivo.contagioradio.com/de-la-peste-profunda-al-salto-cuantico/) así como un cerco epidemiologico, es decir poder determinar claramente dónde están las personas contagiadas o en mayor riesgo, y eso no se ha hecho Colombia. (...)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### CR ¿En el tema del ELN; muchas personas temen lo que pase luego del 30 de abril, para usted cuál es el paso a seguir? {#cr-en-el-tema-del-eln-muchas-personas-temen-lo-que-pase-luego-del-30-de-abril-para-usted-cuál-es-el-paso-a-seguir .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

IC: Nosotros constatamos con un optimismo moderado, registramos como un hecho importante que [el ELN ha cumplido su compromiso](https://archivo.contagioradio.com/cese-unilateral-del-eln-completa-18-dias-con-balance-positivo/) de realizar un alto al fuego, y hasta donde hemos podido también tener noticia lo propio ha hecho el Ejército y las Fuerzas Militares es decir hay una reducción evidente de los enfrentamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algo que hay que propagar, a otras organización como el Clan del Golfo, las disidencias de FARC, las cuales han aprovechando para aumentar sus acciones violentas. He invito no solo a los grupos armados sino también al Gobierno para que se realice la apertura de los diálogos de paz, que finalicen con un acuerdo entre el Estado y el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Esta entrevista puede verla completa en:*

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/3609145189114359/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/3609145189114359/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
