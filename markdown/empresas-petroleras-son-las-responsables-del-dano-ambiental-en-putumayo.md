Title: Empresas petroleras son las responsables del daño ambiental en Putumayo
Date: 2015-06-09 14:27
Category: Entrevistas, Otra Mirada
Tags: ANLA, Comisión mineroenergética, Explotación petrolera, Mesa regional de Putumayo, Plan de Desarrollo Integral Andinoamazónico, Putumayo
Slug: empresas-petroleras-son-las-responsables-del-dano-ambiental-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/derrame-de-petroleo-putumay_4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elpais.com.co]

<iframe src="http://www.ivoox.com/player_ek_4616774_2_1.html?data=lZuemJybeI6ZmKiak5yJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLjzcaYo9fZssXpwszOjcnJb83VjMjcz87XrYa3lIqvldOPscrixtfcx9PJtsiZpJiSo57YrcTVjMnSjdHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Paola Artunduaga, Mesa Regional de Putumayo] 

###### [09 Jun 2015] 

Las comunidades campesinas, indígenas y afrodescendientes del Putumayo, denuncian que la crisis ambiental que existe actualmente en el departamento, se debe a principalmente a **la explotación petrolera**, especialmente a las actividades desarrolladas por parte de la **empresa VETRA.  **

De acuerdo a Paola Artunduaga, quien hace parte de la comisión mineroenergética de la Mesa Regional de Putumayo, **más de 20 nacimientos de agua se han secado** por explotación petrolera de esa empresa, además, en el Corredor Puerto Vega- Teteyé existen **32 pozos petroleros en 13 veredas que están afectando a más de 1200 familias que se han quedado sin agua.**

Así mismo, cabe resaltar que debido a esta explotación en el año 2010 la población de la vereda La Floresta, debió desplazarse por los daños causados por la explotación del crudo.

Desde la Mesa Regional, las comunidades aseguran que el Estado obedece a los intereses económicos y no a los de las comunidades, teniendo en cuenta que la **ANLA no ha querido tomar medidas frente a las faltas ambientales de la compañía VETRA**, ya que cuando empezó la explotación también inició la afectación al ambiente y a las comunidades, debido a que las aguas subterráneas se contaminaron con el crudo ya que “esta actividad no se está desarrollando adecuadamente”, señala la integrante de la comisión minero energética.

Cabe recordar que precisamente, la Comisión es una propuesta que  nace desde la mesa regional el pasado 19 de septiembre de 2014, cuando la Mesa Regional y el gobierno se sientan a dialogar para generar soluciones frente a las problemáticas que vive el departamento. Es así, como la **comisión mineroenergética se crea para presentar argumentos técnicos sobre el daño ambiental por explotación petrolera**, que pese a que ha disminuido, la contaminación continúa porque las medidas del Estado y de la misma empresa no compensan el daño ambiental, teniendo en cuenta que el pasado 31 de mayo salió resolución donde se amplía la explotación petrolera.

Es por eso, que a través del **PLADIA, Plan de Desarrollo Integral Andinoamazónico,** se espera generar un escenario de interlocución con la estructura económica alrededor del petróleo “que no respeta a las comunidades”, dice Paola Artunduaga.
