Title: Convocan velatón en homenaje a Mauricio Lezama
Date: 2019-05-13 18:00
Author: CtgAdm
Category: eventos
Tags: Arauca, Cine, Mauricio Lezama
Slug: convocan-velaton-homenaje-maurico-lezama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/1557512932.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/60105680_990019171195548_3461263875549167616_n.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Particular 

En conmemoración de [Mauricio Lezama, cineasta y gestor cultural asesinado el pasado 9 de mayo en el municipio de Arauquita, Arauca](https://archivo.contagioradio.com/asesinan-al-gestor-cultural-y-audiovisual-mauricio-lezama-en-arauca/) , diferentes integrantes del sector audiovisual y cultural han convocado a una **Cinetón, un espacio que a manera de velatón en el que buscan hacer reflexión sobre la protección de la vida en todas sus formas, la defensa de la libertad de expresión y la protección de los creadores de contenido como relatores de memoria**.

"No podemos permitir que sigan apagando en Colombia de manera violenta las vidas de líderes sociales, artistas, creadores, gestores culturales y ciudadanos en general, que aportan desde sus áreas del conocimiento, elementos invaluables para la construcción de nuestra cultura e identidad", manifestaron en un comunicado los promotores del evento.

**Ciudades confirmadas para el homenaje**

Al llamado han atendido más de 14 ciudades y municipios de Colombia: Medellín, Cali, Santa Marta, Tunja, Bucaramanga, Pereira, Buga, Arauca, Pasto, Ibague, Manizales, Sahagún (Cordoba), Cartagena, Bogotá entre otras, que se sumarán a la Cinetón Nacional dedicada a la memoria de Mauricio de manera simultánea.

• Bogotá: Calle 19 entre carreras 2 y 3 frente a la Nueva Cinemateca.  
• Medellín: Cl. 51 \#36-66 Plazoleta Museo Casa de la Memoria.  
• Ibague: Media torta, Universidad Cooperativa de Colombia, sede Ibagué Centro.  
• Pasto: Pinacoteca Departamental de Nariño.  
• Cali: Cinemateca del Museo de la Tertulia.  
• Bucaramanga: Plaza Cívica Luis Carlos Galán.  
• Tunja: Plazoleta San Ignacio.  
• Arauca: Plazoleta de la Alcaldía Arauca (6:30 pm).  
• Pereira: Teatro Cine con alma-Cámara de Comercio de Pereira (Car 8 \#23- 09) 16 de mayo.  
• Santa Marta: Plazoleta central, Universidad del Magdalena.  
• Manizales: Cinespiral Car 23 \#75-200 barrio Milán (6:30 pm).  
• Sahagún (Cordoba): Cancha frente del cementerio, barrio Rancheria.  
• Arauquita (Arauca) Casa de la cultura.  
• Buga (Valle) Plazoleta UNIVALLE sede Buga. 6:00 pm (23 de mayo).

A través de Proimágenes Colombia y el Fondo para el Desarrollo Cinematográfico, **Mauricio se encaminaba a narrar las historias del conflicto armado en Arauca, un relato que comenzaría con el testimonio de la lideresa Mayo Villareal**, sobreviviente y víctima del genocidio de la Unión Patriótica.

 
