Title: "Si tu me olvidas" Pablo Neruda
Date: 2015-09-23 16:56
Category: Viaje Literario
Tags: Pablo Neruda aniversario muerte, Pablo Neruda Si tu me olvidas, Poemas Pablo Neruda
Slug: si-tu-me-olvidas-pablo-neruda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/NERUDA_00_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 

###### 

###### 

###### 

###### [Ilustración: Pedro Uhart] 

###### [  
<iframe src="http://www.ivoox.com/player_ek_8572359_2_1.html?data=mZqklJiZfY6ZmKiakpiJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmtM6Y1tqPscaf0NHjy8nFt4amk5C9w8fQs4zCxtfixsaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>] 

###### ["Si tu me olvidas" Pablo Neruda] 

###### [23 Sep 2015] 

Ricardo Eliécer Neftalí Reyes Basoalto más conocido como Pablo Neruda nació en Parral Chile el 12 de julio de 1904. Es considerado uno de los poetas más destacados del siglo XX, tanto así que Gabriel García Márquez se refirió a él como “el más grande poeta del siglo XX”.

Pablo Neruda tambiñen fue un destacado político, senador y miembro central del partido comunista chileno, además que fue precandidato a la presidencia y embajador de Francia. Entre sus múltiples reconocimientos destacan el premio Nobel de literatura en 1971 y un doctorado honoris causa de la Universidad de Oxford.

Entre sus obras se encuentran 20 poemas de amor y una canción desesperada (1924), Residencia en la tierra (1925-1931), Canto General (1950), Estravagario (1958),  Cien sonetos de amor (1959), Memorial de isla negra (1964), Las piedras del cielo (1970), Pablo Neruda murió en Santiago de Chile el 23 de septiembre de 1973.

Hoy, recordando la fecha de su fallecimiento, compartimos con ustedes el poema "Si tu me olvidas" incluído en su libro "Los versos del Capitán" del año 1952.
