Title: Paramilitares amenazan a familias de El Porvenir en Puerto Gaitán Meta
Date: 2016-12-13 16:42
Category: DDHH, Nacional
Tags: Amenazas a reclamantes de tierras, grupos paramilitares, Intimidaciones y amenazas a lídere comunitarios de Cajamarca, Los Urabeños, Sikuani-Kubeo
Slug: paramilitares-amenazan-familias-porvenir-puerto-gaitan-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### ** **[Foto: Contagio Radio ] 

###### [13 Dic 2016 ] 

El sábado 10 y el lunes 12 de diciembre la Señora Rosalba Castillo, lideresa del Proceso de Tierras de la vereda el Porvenir, en Puerto Gaitán Meta, recibió amenazas de paramilitares, por vía telefónica un hombre que se identificó como integrante de los Urabeños y los Carranceros, **le dijo que debía abandonar su trabajo comunitario y entregarle 5 millones de pesos para no poner en riesgo la vida de sus hijos.**

Estos hechos, se suman a los ocurridos desde el mes de Noviembre contra Héctor Sánchez líder sindical de la USO, Jenny Cárdenas encargada de la secretaria de la mujer en Puerto Gaitán y otros líderes y lideresas, organizaciones de derechos humanos **advierten que se puede tratar de una estrategia sistemática para obstaculizar las titulaciones de tierras en el departamento.**

En lo corrido del año, también recibieron amenazas y hostigamientos, el señor Melquisedec, Omar Delgado, Miguel Briceño, Gabriel Gutiérrez y Luis Chipiaje, La Corporación Claretiana Norman Pérez la organización defensora se pronunció a través de un comunicado en el que hacen un llamado de atención al Estado colombiano por **no dar respuesta efectiva a los hostigamientos y brindar garantías de seguridad a reclamantes de tierra y defensores de derechos humanos.**

### **No hay garantías de Paz Territorial** 

La organización defensora de derechos humanos señala en el comunicado, el sentimiento de algunos de los habitantes quienes afirman que “en el Porvenir **rondan las amenazas, intimidaciones, asesinatos y las autoridades no presentan resultados efectivos,** no combaten a los grupos paramilitares, invasores de tierras que dan cuenta de los vestigios de poderes terratenientes, mafiosos y paramilitares que como dioses controlan la vida y la muerte en la región”.

Denuncian que con los hechos registrados hasta la fecha “se evidencia una intensión real contra la población, un **patrón de violación a los derechos humanos bajo la modalidad de amenaza que genera miedo, incertidumbre y posibles desplazamientos de las comunidades** (…) esta situación, amenaza la supervivencia física y cultural de toda la comunidad que habita el Porvenir”. Le puede interesar: [Corte constitucional ordena adjudicación de tierras a comunidades de Puerto Gaitán.](https://archivo.contagioradio.com/corte-constitucional-ordena-adjudicacion-tierras-comunidades-puerto-gaitan/)

Por último exigen a través del comunicado, que las autoridades correspondientes **inicien acciones investigativas y brinden de inmediato protección a la Sra. Rosalba Castillo, a su familia, en general a las comunidades de El Porvenir,** a quienes hacen parte del Asentamiento Ancestral Indígena Kubeo-Sikuani y al personal de acompañamiento de la Corporación Claretiana.

[Denuncia Publica n. 18 en El Porvernir Rondan Los Paramilitares (1)](https://www.scribd.com/document/334125684/Denuncia-Publica-n-18-en-El-Porvernir-Rondan-Los-Paramilitares-1#from_embed "View Denuncia Publica n. 18 en El Porvernir Rondan Los Paramilitares (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_80655" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/334125684/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-IM7DCnZYBSm7GlKCjOYz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
