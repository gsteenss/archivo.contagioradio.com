Title: En el Bajo Cauca Antioqueño la violencia se está tomando el poder
Date: 2019-07-11 22:49
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, Antioquia, Discidencias, Ituango, paramilitares
Slug: bajo-cauca-antioqueno-violencia-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Ituango-Bajo-Cauca-Antioqueño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia] 

El pasado miércoles, comunidades de Ituango (Antioquia) denunciaron que **grupos armados ilegales estarían reuniendo a la población, para pedirles datos sobre los jóvenes de la comunidad y hacer control territorial**. Para defensores de derechos humanos, la situación es una alerta sobre la situación de riesgo vivida actualmente por la comunidad, y la posibilidad de que aumenten los asesinatos selectivos y desplazamientos masivos en la región del Bajo Cauca Antioqueño.

**Óscar Yesid Zapata, integrante de la Coordinación Colombia-Europa-Estados Unidos (COEUROPA),** dijo tener conocimiento sobre el control que se estaría ejerciendo sobre las Juntas de Acción Comunal por parte de estructuras armadas ilegales hace meses. De acuerdo a Zapata, "como los grupos armados están controlando el territorio obligan a las comunidades a pasar información; están reclutando niños, niñas y adolescentes; están obligando a sembrar coca".

### **¿Cuáles son los grupos armados que están operando en el Bajo Cauca Antioqueño?** 

En la denuncia, las comunidades no identificaron el grupo que estaría buscando hacer el reclutamiento a menores y control territorial; sin embargo, Zapata explicó que tras la salida de las FARC del territorio, Ituango y en general el Bajo Cauca Antioqueño se convirtió en un objetivo estratégico porque es un punto clave para la movilidad hacia el mar y porque hay presencia de cultivos de uso ilícito. (Le puede interesar:["Se libra una guerra por tomar el control del Nudo deParamillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

La lucha por el control territorial estaría entonces entre un grupo que no se acogió al proceso de paz y está reivindicando la lucha guerrillera; y las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), llamados por el Gobierno como Clan del Golfo. "Para nosotros, las AGC es un grupo paramilitar que lleva operando en el territorio hace 13 o 14 años (...), son una propuesta contrainsurgente" que tiene una estrategia política, territorial y económica, aseguró Zapata.

Esta situación de violencia en las montañas del Bajo Cauca ha generado desplazamientos forzados hacia el sur de Córdoba; aunque esto no ha ocurrido como tal en Ituango, para Zapata, si el Estado no plantea soluciones para la región "se prevé que se realizarán desplazamientos forzados así como el aumento de asesinatos selectivos". (Le puede interesar: ["Según la ONU, ya son 2.159 personas desplazadas forzadamente en Córdoba"](https://archivo.contagioradio.com/segun-la-onu-desplazamientos-forzados-en-cordoba-ascienden-a-2-159-personas/))

### **"Parece que las autoridades no gobiernan el territorio y pareciera que no lo quieren hacer"**

El Integrante de COEUROPA criticó el papel del Estado, cuya función es "ejercer soberanía de armas y de territorio"; pero ha sido débil y **"ha permitido que esas estructuras controlen los territorios"**. De igual forma, cuestionó a las figuras de orden civiles de Ituango, porque "parece que las autoridades no gobiernan el territorio y pareciera que no lo quisieran hacer".

En esa medida, Zapata sostuvo que las denuncias de la comunidad deberían ser tomadas como una alerta temprana que debería mover una acción del Estado, porque si no se adopta una estrategia integral, "este fenómeno se le saldrá de las manos" al Gobierno. (Le puede interesar: ["Grupos ilegales se disputan el Bajo Cauca ante la ausencia del Estado"](https://archivo.contagioradio.com/grupos-ilegales-se-disputan-el-bajo-cauca-antioqueno-ante-la-ausencia-del-estado/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38364018" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38364018_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
