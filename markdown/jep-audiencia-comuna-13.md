Title: La JEP nos hizo sentir protegidas: Víctimas en la primera audiencia pública de la Comuna 13
Date: 2019-07-21 19:07
Author: CtgAdm
Category: Judicial, Paz
Tags: arenera, escombrera, JEP, Medellin, personas desaparecidas
Slug: jep-audiencia-comuna-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Audiencia-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CorpoJuridicaLi  
] 

Este 18 de julio culminó la primera audiencia pública ante la Jurisdicción Especial para la Paz (JEP) por la desaparición forzada en la Comuna 13 de Medellín, en la cual se solicitó la protección de la Escombrera y Arenera, donde se contrarían algunas de las víctimas de este delito. Los elementos claves de esta audiencia, según las organizaciones, fueron las garantías de la participación efectiva de las víctimas y la muestra de la negligencia por parte de las instituciones públicas frente a sus obligaciones de búsqueda, localización y exhumación.

De esta manera, durante la Audiencia se logró evidenciar un compromiso por parte de la JEP para el acompañamiento y reconocimiento de los derechos de las y los principales afectados: “La JEP generó un escenario de respeto y dignificación de los familiares y de las mismas víctimas de desaparición forzada, se garantizó que fueran escuchadas…” **aseguró la directora de la Corporación Jurídica Libertad, Adriana Arboleda.** (Le puede interesar: ["JEP realizará primera audiencia pública por víctimas de desaparición forzada en la Comuna 13 de Medellín"](https://archivo.contagioradio.com/jep-realizara-primera-audiencia-publica-por-victimas-de-desaparicion-forzada-en-la-comuna-13-de-medellin/))

### **"(los magistrados de la JEP) nos hicieron respetar... nos sentirmos protegidas"**

En ese sentido, y de acuerdo con las diferentes declaraciones de organizaciones, la JEP logró cumplir con la participación efectiva de los principales afectados del conflicto armado.  Así lo corroboró **Margarita Restrepo integrante de Mujeres Caminando por la Verdad** de la Comuna 13: “**La Audiencia, nos generó dolor y alegría, ellos nos hicieron valer a nosotros, a nuestros derechos… nos hicieron respetar… nos sentimos protegidas**”.

Es importante destacar que mediante esta primera audiencia se logró la participación de las entidades públicas implicadas en el proceso de búsqueda, y también de las organizaciones sociales como el Movimiento Nacional de Víctimas de Crímenes de Estado (Movice), la Organización Mujeres Caminando por la Verdad y el Grupo Interdisciplinario de Derechos Humanos. (Le puede interesar: ["Desaparecidos en Colombia superarían 4 veces los de la dictadura en Argentina"](https://archivo.contagioradio.com/desaparecidos-superarian-dictadura/))

Mediante la denuncia de estas organizaciones, y la voz de la magistratura se logró evidenciar la actuación de las entidades estales en cuanto a su labor de búsqueda y localización de cuerpos. Aportes importantes de magistrados como Gustavo Salazar y la Fiscal Nancy Posada, pusieron al tanto a la opinión pública de la descoordinación, la desarticulación y la falta de acciones concretas por parte de la Fiscalía General, la Alcaldía de Medellín, el Ministerio Público y la Gobernación de Antioquia.

Así lo afirmó la directora Arboleda: “En la audiencia salieron elementos muy importantes, por ejemplo, la Gobernación debió aceptar que no ha hecho ningún papel de protección de los sitios, e **incluso se ha prorrogado la concesión minera para estos sitios**”. (Le puede interesar:["Así debería ser la búsqueda de personas desaparecidas en La Escombrera"](https://archivo.contagioradio.com/asi-deberia-ser-la-busqueda-de-personas-desaparecidas-en-la-escombrera/))

En relación a ello, y de acuerdo a  lo expresado por Arboleda, han pasado **17 años desde la desaparición forzada en 2002 dentro de la Comuna 13,** en lo que se conoció como Operación Orión, y desde entonces, no ha habido ninguna actuación diciente, por parte de las entidades respectivas, frente a estos hechos. Beneficiando, como mencionó la directora de la Corporación Jurídica Libertad, a terceros: “han pasado cinco alcaldías, cinco gobernaciones y ninguno ha hecho nada. Es una prueba irrefutable de la actitud de las entidades públicas del Estado para favorecer a los empresarios y no para proteger los derechos de las víctimas… los grandes beneficiados de esta guerra y de las graves violaciones a los derechos humanos terminan siendo privados”.

Con esta Audiencia Publica las víctimas esperan superar finalmente estas barreras que han puesto las entidades públicas, lograr que la JEP consolide la Escombrera y Arenera como lugares de conservación y protección para la búsqueda efectiva de los restos que se presumen allí ubicados. El panorama es positivo para las organizaciones de víctimas, según lo expresado por Restrepo: “La audiencia permitió reconocer que el Estado siempre ha ido negligente con las víctimas… **el resultado de esta audiencia es esperanzador, volvimos a tener alegría y esperanza para cerrar y tener medidas cautelares de estos y otros sitios**”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38691636" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38691636_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
