Title: La violencia marcó el primer fín de semana de septiembre
Date: 2019-09-08 21:53
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas, asesinato, Lideres, violencia electoral
Slug: violencia-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Fotos-editadas1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Desde el viernes la violencia se hizo presente con el asesinato de tres excombatientes de FARC que adelantaban su proceso de reincorporación en Cúcuta (Norte de Santander) y Quibdó (Chocó), y el homicidio de José Cortés Sevillano Llorente, en zona rural de Tumaco (Nariño). A este crimen se sumó el de Orley García, candidato a la alcaldía de Toledo (Antioquía), que sufrió un atentado con arma de fuego y falleció mientras era trasladado a un centro hospitalario. (Le puede interesar:["Asesinan a excombatientes Miltón Urrutía y José Peña en Cúcuta"](https://archivo.contagioradio.com/asesinan-excombatientes-cucuta/))

### **José Cortés, profesor y líder social de Llorente** 

El crimen de José Cortés ocurrió en las horas de la noche del viernes, mientras se encontraba en un establecimiento público cuando llegó un hombre armado que disparó contra su humanidad. Cortés era licenciado en educación física y presidente de la Junta del KM 63, adicionalmente, era miembro de la asociación porvenir campesina (ASOPORCA). (Le puede interesar: ["El colegio de Tumaco que se cansó de la muerte: 21 asesinatos de estudiantes en 4 años"](https://archivo.contagioradio.com/el-colegio-de-tumaco-que-se-canso-de-la-muerte-21-asesinatos-de-estudiantes-en-4-anos/))

> Líder social asesinado en Nariño
>
> Anoche en el corregimiento de Llorente en [\#Tumaco](https://twitter.com/hashtag/Tumaco?src=hash&ref_src=twsrc%5Etfw), asesinaron al líder y docente [\#JoséCortezSevillano](https://twitter.com/hashtag/Jos%C3%A9CortezSevillano?src=hash&ref_src=twsrc%5Etfw).
>
> Como tantos otros líderes sociales trabajaba en defensa de su gente.  
> Solidaridad con su familia.
>
> ¡Que pare la masacre ya![\#NoMás](https://twitter.com/hashtag/NoM%C3%A1s?src=hash&ref_src=twsrc%5Etfw)! [pic.twitter.com/pU45O5ht0W](https://t.co/pU45O5ht0W)
>
> — Camilo Romero (@CamiloRomero) [September 7, 2019](https://twitter.com/CamiloRomero/status/1170395645578031105?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Orley García, líder político asesinado en contexto electoral**

Orley García era candidato a la alcaldía de Toledo por el Centro Democrático y había denunciado amenazas en su contra. García recibió un atentado el sábado con diferentes impactos de bala, fue llevado a un centro asistencial pero por la gravedad de sus heridas se pidió un helicóptero para ser atendido en Medellín, sin embargo, durante el viaje perdió la vida. (Le puede interesar:["La MOE ha registrado 228 hechos de violencia contra líderes sociales en últimos 8 meses"](https://archivo.contagioradio.com/moe-228-hechos-violencia-lideres/))

### **Los efectos de la violencia en contexto electoral** 

Según un informe de la Misión de Observación Electoral (MOE), entre octubre de 2018 y mayo de 2019 se han presentado 228 hechos de violencia contra líderes políticos, sociales o comunales, generando en el 29% de los casos (66) un homicidio.  La violencia en contexto electoral afecta el proceso democrático porque desincentiva la participación de las personas, tanto para asistir a las urnas como para presentarse a cargos de elección popular. (Le puede interesar: ["Tres medidas para frenar la violencia política según la MOE"](https://archivo.contagioradio.com/tres-medidas-para-frenar-la-violencia-politica-segun-la-moe/))

Precisamente, este sábado 7 de septiembre, el candidato a la alcaldía de Samaniego (Nariño) Darío Dorado Galindo decidió renunciar a su candidatura por amenazas en su contra. En su comunicado a la opinión pública, Dorado manifestó que tomando en cuenta el contexto violento por el que pasa el país, y las amenazas contra su vida y su familia, decidió apartarse de las elecciones y continuar aportando a Samaniego desde su rol como ciudadano.

En el escrito, expresó que no sentía rencor contra quienes quisieron amedrentarlo, agradeció al partido UP/Colombia Humana por dar aval a su candidatura, y deseó que se adelanten unas elecciones de forma pacífica en este municipio de Nariño. (Le puede interesar: ["Preocupante situación de seguridad en Suárez, Norte del Cauca"](https://archivo.contagioradio.com/seguridad-norte-del-cauca/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
