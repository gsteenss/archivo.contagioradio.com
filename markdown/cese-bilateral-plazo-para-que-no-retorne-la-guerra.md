Title: Cese Bilateral: plazo para que no retorne la guerra
Date: 2016-10-05 16:44
Category: Nacional, Paz
Tags: acuerdo de paz, Apoyo al proceso de paz
Slug: cese-bilateral-plazo-para-que-no-retorne-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carmela-maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cese al Fuego] 

###### [5 de Oct 2016] 

**Solo el movimiento social podrá ejercer presión al gobierno** para que establezca un camino que supere el impasse, este es el análisis que hace Carlos Medina, frente al posible fin del cese bilateral y el anuncio por parte de las FARC-EP de mover todas sus unidades a zonas seguras.

Medina, docente y miembro del Centro de Pensamiento de la Universidad Nacional, afirmó que el fin del cese bilateral, que se daría el 31 de octubre, es consecuencia de que no se hayan aprobado los acuerdos y ahora solo puede generarse una prórroga a partir de **las voluntades tanto del gobierno como de las FARC, para mantener el cese bilateral y el movimiento de las tropas a las zonas seguras sin inconvenientes**.

Este anuncio genera cambios en las lógicas que se venían dando dentro del proceso de paz para ambos bandos, por un lado Medina afirma que el momento político que afronta las FARC-EP los lleva a tomar **“una actitud defensiva de sus fuerzas y prepararse para el combate”,** mientras que por el otro lado, las Fuerzas Militares que también venían adelantando un proceso de transformación y pedagogía hacia la paz, deberán retornar a la dinámica de guerra, [esto si no se establece una salida o continuación al cese bilateral antes del 31 de octubre.](https://archivo.contagioradio.com/no-somos-una-patria-boba-y-vamos-a-demostrarlo-pazalacalle/)

Finalmente el analista explica que **solo el movimiento social podrá ejercer una presión fuerte al gobierno**, para que se busquen prontas soluciones o propuestas tanto al cese bilateral, como a los acuerdos de paz en la Habana que han dejado al país en incertidumbre.

**En departamentos como Arauca, Meta y Putumayo**, ya se ha confirmado la movilización de unidades de las FARC-EP hacía zonas seguras, igualmente organizaciones sociales como las indígenas y Afros, [han ofrecido sus territorios, autónomos para que allí se implementen los acuerdos de paz.](https://archivo.contagioradio.com/victimas-del-pais-exigen-participacion-directa-en-pacto-politico-nacional/)

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
