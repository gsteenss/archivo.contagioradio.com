Title: Duplicar resultados a toda costa, la orden a militares en el gobierno Duque
Date: 2019-05-18 14:40
Author: CtgAdm
Category: DDHH, Nacional
Tags: cúpula militar, Ejecuciones Extrajudiciales, Ejército Nacional
Slug: duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:hispantv.] 

A través de una entrevista realizada por el diario**[The New York Times](https://www.nytimes.com/es/2019/05/18/colombia-ejercito-falsos-positivos/?smid=wa-share-es)**, oficiales militares quienes mantienen su identidad anónima,  relataron cómo se les ha ordenado a las tropas duplicar la cantidad de muertes en combate incluso si eso significa no exigir exactitud en sus operaciones, una orden que implicaría la reducción de protección a la población civil y que ya fue vista a principios de la década de los 2000 dejando como resultado más 5.000 ejecuciones extrajudiciales.

Según el testimonio que entregó uno de los oficiales al diario, también se les ordenó a los comandantes **“hacer lo que sea” para mejorar sus resultados, incluso si eso significaba establecer alianzas con grupos criminales** en los territorios para obtener información sobre objetivos de otros actores armados.

Asímismo, relatan que se les ha pedido que firmen compromisos en los que se comprometen a "intensificar los ataques y mostrar el número de días que pasan sin estar en combate por lo que son amonestados si no lo hacen con frecuencia. También se les dijo a los militares que si incrementaban sus muertes en combates, se les otorgarían incentivos adicionales como vacaciones extra.

### La nueva cúpula de militares

Estas instrucciones coinciden con el cambio en la Cúpula militar de las Fuerzas armadas que el presidente Duque  
realizó en diciembre del 2018, nombrando a oficiales que han sido investigados por falsos positivos, entre ellos al Mayor General Nicacio Martínez Espinel, comandante del Ejército. ([Lea también Nueva cúpula militar: ¿vuelve la seguridad democrática?)](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/)

El mismo general Martínez ha reconocido que ordenó a sus hombres duplicar sus resultados en las operaciones argumentando que "la amenaza criminal se incrementó" según lo relatado por los oficiales entrevistados, esa misma orden indica, según lo relatado por el New York Times que las operaciones deben ser lanzadas "con un 60-70% de credibilidad y exactitud”, lo que para los oficiales deja un margen de error que puede ser cuestionable.

Esta instrucción se suma a la reunión convocada por Martínez que según los oficiales incluyó a 50 de los principales coroneles y generales del país donde se les pidió que "enumeraran la suma de presentaciones voluntarias, capturas y muertes  resultado de operaciones militares  durante el 2018 y establecieran una meta para el siguiente.

### El caso de Dimar Torres

Poco tiempo después de darse esta reunión y de darse las ordenes que incluían aliarse con grupos paramilitares para obtener información y "generar resultados", según los oficiales, pudieron identificar asesinatos o arrestos sospechosos.[  (Lea también: Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres)](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/)

De estos, el caso más notable es el Dimar Torres, un excombatiente quien cumplía con su proceso de reincorporación en Convención, Norte de Santander y que fue asesinado por un cabo del Ejército, un suceso que fue descubierto por la comunidad cuando intentaban oculta el crimen y que permitió poner a la luz el homicidio. Aunque el caso de Dimar haya alcanzado trascendencia nacional, los oficiales reconocieron que es probable que otros asesinatos hayan pasado inadvertidos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
