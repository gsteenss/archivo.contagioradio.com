Title: Paz y participación en Colombia
Date: 2017-07-25 10:56
Category: CENSAT, Opinion
Tags: acceso al agua, Ambientalismo, consultas populares, participación popular
Slug: paz-y-participacion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Minga social y comunitaria -2008. Archivo Censat Agua Viva 

###### 25 Jul 2017 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

La participación en la construcción de paz es uno de los temas que ronda debates diversos de la opinión pública en Colombia. Este aspecto no es nuevo, y en los últimos años ha revestido características que implican pensarlo más allá de su simple enunciación o de su carácter reivindicativo. Ejercicios como las Iniciativas Populares Normativas, Cabildos Abiertos, el Referendo por el Agua y, más recientemente, las Consultas Populares, dan cuenta de mecanismos de participación popular que sin duda deben relacionarse con la construcción de paz desde los territorios.

En relación directa a la participación en un país como Colombia, resultan temerarias, entre muchas otras, las afirmaciones publicadas por el periódico El Tiempo el 27 de junio, en las que el director de la Asociación Colombiana de Petróleo -Francisco Lloreda- señaló que “hay que impulsar la actividad exploratoria con audacia, para lo cual es fundamental que se supere la incertidumbre jurídica que existe, y que las empresas puedan operar en el territorio, porque se ha vuelto muy difícil hacerlo \[…\] las consultas populares son una de las amenazas de la industria, porque a través de ellas se ha encontrado la forma de poner en jaque al sector”.

Asistimos no solamente a la negación de mecanismos que fueron consagrados en la Constitución y las leyes como formas populares de la participación, también se devela la imposibilidad de pensar en las transformaciones para la paz basadas en el respeto a la decisión de los pueblos respecto a las posibilidades de permanencia como culturas asociadas al intercambio con la naturaleza, transformaciones para la paz impregnadas de la reconciliación ambiental.

En el presente año, además de las múltiples consultas populares que se han llevado a cabo y se seguirán realizando -a pesar de los comerciantes y mercantilizadores de la vida-, la negociación de paz entre el gobierno del derechista Juan Manuel Santos y la izquierdista guerrilla del Ejército de Liberación Nacional -ELN- ha puesto también el tema de la participación en un primer plano. La posibilidad de construir paz, en el caso de las negociaciones entre el gobierno y la insurgencia pasa, para este último actor, por la participación de la sociedad en la manera en que se administran los territorios de los que la sociedad, tanto urbana como rural, es constitutiva.

Ahora, la exigencia de participación popular en la definición de los aspectos que implican la construcción de paz en Colombia tiene expresiones diversas, y no debe concebirse reduciéndola a una exigencia sectorial, o de la insurgencia; es, más bien, el resultado de reflexiones históricas acerca de la manera en que se han desarrollado en el país las relaciones de poder sobre los territorios y los cuerpos. También es producto del análisis de la manera en que unas clases privilegiadas han dispuesto de los espacios naturales y simbólicos de las clases menos favorecidas -las clases populares / los pueblos- promoviendo la guerra, la militarización, la criminalización y la violencia para ejercer controles territoriales y usurpar los patrimonios naturales, para la acumulación privada.

La exigencia de participación de los pueblos en la construcción de paz es hoy la de un sujeto político popular que se ha fraguado en la defensa territorial, es una exigencia por la apertura a espacios propios, autónomos, legítimos y vinculantes en los que la voz de las mayorías se corresponda con el ordenamiento territorial, con la planificación del manejo y la gestión de los bienes comunes -como las selvas, los bosques, las montañas andinas, las semillas, las aguas-, con la protección de territorios esenciales frente a los planes de explotación neoliberal. Sus expresiones se encuentran no solo en la utilización de mecanismos garantizados en la Constitución y las leyes, también en construcciones autónomas mandatadas en décadas de articulación social de aquellas/os excluidas/os de los espacios de representación tradicional funcionales a la negación de los derechos y a la negación de la decisión frente al propio destino de la vida. Sin temor a equivocarse se puede afirmar que la cuestión ambiental en la construcción de paz implica, necesariamente, la participación, el intercambio orgánico -el metabolismo de vida- para salvar lo que nos queda de dignidad humana.

Para quienes hemos contribuido desde el ambientalismo a la defensa de la vida en el país, la participación es una condición necesaria de la paz con justicia social y ambiental, y debe exigirse y ejercerce más allá de cualquier coyuntura de negociación política de la guerra que a este país ha consumido. La participación es la posibilidad de transformación si y solo si se respeta como decisión autónoma y vinculante frente al manejo de  nuestros cuerpos y nuestros territorios.

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
