Title: Literatura sobre la lucha campesina
Date: 2017-11-16 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: COCCAM, Cumbre Agraria, Minga Agraria, Movilización
Slug: literatura-sobre-la-lucha-campesina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/lucha-campesina-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: COCCAM] 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**

[¿Qué carajo importa una discusión en las redes sociales entre dos sujetos que a duras penas leen los problemas del país pero no los viven ni los sienten sinceramente? ¿Qué carajo importa que varios columnistas de opinión, hoy estén promoviendo la sumisión más absoluta al régimen democrático que tiene perjudicada a la mayoría de personas en el país? ¿qué carajo importa que la derecha esté dividida si a fin de cuentas son los únicos que han atendido al llamado: “capitalistas del mundo uníos”? ¿qué importa que la izquierda intelectual en las ciudades ahora padezca de]*[prudenciatitis aguda]*[ y muchos de sus representantes literalmente sigan haciendo lo único que saben hacer (esconderse aterrorizados porque otros hacen lo que ellos no tuvieron el valor de hacer, y para evitar la sensación de frustración, emprenden la criticadera no al enemigo, sino al amigo)?]

[¿Qué carajo importa, el reconocimiento de la libertad de expresión, sin la posibilidad real de la libertad política? ¿Para qué nos quieren convencer de que votemos por estos o por aquellos, si de igual manera nos quieren seguir ocultando la dictadura terrateniente que se vive en el campo? ¿para qué discutir sobre un sistema electoral que en nada afecta el régimen de poder latifundista que se ha impuesto desde tiempos coloniales en el campo colombiano?]

**En el campo está radicada la fuente del poder político de quienes gobiernan las ciudades. En el campo, es donde están matando a la gente por causas políticas. En el campo se libró la guerra. En el campo se siembra gran parte del alimento de Colombia. En el campo, se conserva una perspectiva en donde no se “invierte el tiempo”, sino que se cultiva la vida. En el campo es donde se desarrolla actualmente el nivel de lucha político más avanzado de Colombia. En el campo es donde reside la posibilidad de la transformación política y por tanto económica del país.  En el campo pasa todo… y muchos hacen como si no pasara nada.  **

[En las ciudades, algunos creen que la pobreza y la barbarie que se viven en el campo, son una ficción. Otros, incluso creen que los campesinos, los indígenas, no son capaces de desarrollar una lucha política y creen que “necesitan quién les enseñe”, cuando sin cansancio han demostrado que pese a toda la violencia, pese a todo el olvido, pese a toda la tergiversación de la que han sido sujetos, han sostenido activamente durante los últimos años una lucha social y política que tiende a volverse cada vez más enraizada; una combinación de sangre y tierra, que sin necesidad de ser metafórica sigue convocando generación tras generación a la comprensión simple, pero certera, de que la lucha política es el único camino para lograr vivir una vida digna.]

[Actualmente, en el país no hay lugar a promover la consigna]*[“lucha contra el capitalismo”,]*[ sin antes  solucionar el problema agrario. Que dejen de fregar en las ciudades y más bien comprendan por el amor a la contundencia, que toda la maquinaria política, toda la corrupción política, todo el poder desproporcionado que existe en Colombia, se fundamenta en la inequitativa tenencia de la tierra.]

[Es la tierra el sinónimo de poder en Colombia, es la tierra por lo que se desgañitan los poderosos, no importa que tengan cien mil hectáreas, ellos querrán un millón, y luego más, y luego más, no serán detenidos mediante la ley, porque son ellos mismos los que hacen la ley, no serán detenidos en su codicia mediante protestas esporádicas y peticiones momentáneas que dan soluciones momentáneas, no serán detenidos solo con rabia… es necesaria la organización.]

[Si se pretende una vida digna en el campo, no es posible pretender que nos regalen un plato de sopa, una botella de aguardiente y una verbena]*[ni la hijueputa]*[ para luego despertar enguayabados, empobrecidos y agradecidos con quien empobrece.]

[Si los habitantes del campo colombiano pretenden una vida digna, es mejor solicitar el jornal y la tierra, no “para mí”, no solo “para nosotros”, SINO PARA TODOS. Después peleamos. Primero la tierra. Después discutimos si me llaman moreno, negro o afro, a fin de cuentas en la vereda siempre me llaman por mi nombre, primero la tierra. Después discutimos si cacao o plátano, primero la tierra. Después discutimos si reserva o resguardo, primero la tierra.  Después discutimos…. Después. Primero tengamos la tierra.  ]

[La movilización por una vida digna, se ha sembrado y ha germinado generación tras generación, puesto que a pesar de los incumplimientos, la exclusión, el engaño, el asesinato, los campesinos e indígenas de Colombia continúan perseverando, continúan desarrollando una librada lucha que despierta las consciencias de grandes y chicos, que hace milagros para que la moral no decaiga, para que no se olvide que los que protestan en el 2017, son los nietos de los que protestaron en los años 70, son aquellos que lejos de aprender a decir]*[“eso deje así”,]*[ por el contrario, continúan solidarizándose, organizándose, promoviendo la protesta porque]***“si no luchamos contra la miserable realidad sistemática de un exterminio, entonces ésta, dejaría de ser la historia de nuestra resistencia”.***

[A los campesinos de Colombia: no todos los que observan sus luchan son indiferentes. No todos los que observan la capacidad de su resistencia condenan los legítimos actos de defensa ante las agresiones del Esmad o la defensa contra las vociferaciones que desde los escritorios, algunos muy leídos les imparten. La mayoría en Colombia tiene sus raíces familiares en el campo, y los mensajes absurdos repartidos desde los medios masivos, no borran la memoria familiar, no la borran, sencillamente porque no pueden.]

[De hecho muchos aprendemos de su lucha y sugerimos que la organización gremial con un propósito amplio y no sectario podría traer mejores resultados. Apoyamos desde el debate urbano la utilidad de la protesta, debatimos la utilidad del bloqueo de carreteras puesto que sin los bloqueos es obvio que el gobierno jamás pondría atención; a la par, se discute sobre el supuesto “secuestro” de ciudades como argumento desarrollado por el régimen cuando los campesinos e indígenas hacen paros ¿Si un paro es un “secuestro” de un municipio, por qué la miseria indigna que generan los terratenientes no es considerara “genocidio”? ¡¡que no vengan esos leguleyos con su retórica escuálida a confundir la realidad!!]

[Reconocemos desde la ciudad que individualizar las causas de los problemas es el peor error en política, esta vaina no es culpa de un alcalde ni tampoco la arregla un presidente; esta vaina se arregla es organizándose, comprendiendo el problema y promoviendo la abolición del inequitativo sistema de tenencia de la tierra que existe en Colombia; por su parte, ustedes de forma ejemplar, demuestran que la solidaridad y lo comunitario sigue vivo en el país.]

[Resulta estremecedor ver murales de su lucha política, ya no simplemente grafitis con aerosol pintados en la carrocería de un 600 o de una tractomula, sino murales, murales pintados por la comunidad, sobre sus humildes edificaciones, pintados con un mensaje que lo ve tanto el que vive como el que pasa por allí… ese tipo de mensajes cohesionan, fundamentan el poder enraizado de una lucha que más que entenderse, se siente. Los murales políticos desestabilizan la tranquilidad de quienes tienen el poder, es una pequeña y sutil respuesta a la tergiversación que expulsan todos los benditos días los medios masivos de información.   ]

[A los campesinos de Colombia: organización, lucha, solidaridad, batalla en carreteras, pintura de las fachadas de las casas con mensajes políticos de organización comunitaria, lucha contra la inequitativa tenencia de la tierra, lucha contra el sectarismo y el inmediatismo de las soluciones que brindan los políticos para “quedar bien” o para “cumplir el indicador” pero que luego los dejan viendo un chispero.]

[Es en el campo donde se halla la vanguardia de la política realmente revolucionaria de Colombia, pues en las ciudades de tanto Facebook y twitter andan como atolondrados. Juntos, recordemos día a día, que si no se obtiene una victoria de la lucha política en el campo, jamás será posible ganar en la ciudad, y lastimosamente, por los siglos de los siglos, la clase política y terrateniente colombiana seguirá intacta con su fino traje, sin una sola mancha de ese revuelto de tierra y sangre que jamás ha sido una metáfora sino una realidad tremenda.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
