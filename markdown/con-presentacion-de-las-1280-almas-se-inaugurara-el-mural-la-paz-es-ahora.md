Title: Con presentación de las 1280 almas se inaugurará el mural “La paz es ahora”
Date: 2015-09-11 11:00
Category: Cultura, eventos
Tags: 1280 almas, Centro de Memoria Paz y reconciliación, Encuentro Nacional de Paz: La paz es ahora., Mural "La paz es ahora"
Slug: con-presentacion-de-las-1280-almas-se-inaugurara-el-mural-la-paz-es-ahora
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Concierto-e-inauguración-del-mural-la-paz-es-ahora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Centro de memoria, Paz y Reconciliación 

###### [Sep 10 2015]

El viernes 11 de septiembre el Centro de Memoria Histórica, Paz y Reconciliación (CMPR) se inaugurará el mural “La paz es ahora” con un concierto donde estarán las 1280 almas, una de las agrupaciones más reconocidas del rock colombiano.

Con canciones como La 22, El platanal, Hombre bomba, Marinero y Surfeando en Sangre, la agrupacion bogotana acompañará la presentación del mural elaborado por "Ark" y "Chirrete", artistas del Colectivo Animal en la Av. 26 con carrera 19B, frente a la esquina principal del Centro de Memoria, y a unos pasos del busto de Jorge Eliécer Gaitán.

El mural, de dimensiones heroicas, 60 metros de largo por seis de alto, evoca el tema del renacimiento de la nación de las manos de sencillos trabajadores y de las etnias, en medio de un bosque esperanzador donde no faltan un jaguar, un tulcán y un zorro, entre otros animales. Al final del fresco, en la esquina oriental, descolla el lema y consigna central del Encuentro Nacional de Paz: La paz es ahora.

\[caption id="attachment\_13548" align="aligncenter" width="640"\][![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/mural.jpg){.wp-image-13548 .size-full width="640" height="480"}](https://archivo.contagioradio.com/?attachment_id=13548) Antes de la intervencion\[/caption\]  
El concierto tendrá lugar frente al CMPR a las cuatro de la tarde con entrada libre. También se presentará la Banda de Jazz del IDIPRON y La Perla.

(Con información de centromemoria.gov.com)

 
