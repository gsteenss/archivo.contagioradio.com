Title: ¿Adiós a las chazas en la Universidad Nacional?
Date: 2018-02-05 17:20
Category: Educación, Nacional
Tags: Chazas, estudiantes, Universidad Nacional
Slug: adios-a-las-chazas-en-la-universidad-nacional
Status: published

###### [Foto: Universidad Nacional] 

###### [05 Feb 2018] 

Estudiantes de la Universidad Nacional, sede Bogotá, **rechazaron la creación de una nueva acta de compromiso por parte de las directivas que se haría firmar a las y los estudiantes** que tengan puesto de venta al interior de la institución y en donde se les compromete a dejar de realizar esta actividad.

El acta sería aplicada a los estudiantes que sean descubiertos realizando este tipo de actividades y en ella se comprometen a dejar de hacer ventas ambulantes, colaborar con terceros que ingresen a la institución a realizar la misma acción y, en dado caso de reincidir en este tipo de hechos o de ser denunciando ante una facultad, **serán sometidos a que se les abra una investigación disciplinar** en el Comité de Resolución de Conflictos y asuntos Disciplinarios de la respectiva facultad.

De acuerdo con Daniela Herrera, representante de los estudiantes por la Facultad de Medicina, **actualmente en la universidad podrían haber más de 90 chazas** (tiendas ambutantes) y en ellas trabajarían tanto estudiantes de la institución como personas externas y la inconformidad del estudiantado en general radicaría en que no existe una propuesta alrededor de un espacio de diálogo para escuchar a las personas que trabajan en estos espacios.

Además, muchos de los estudiantes han afirmado que trabajan allí para poder pagar el semestre, el transporte público, los gastos en fotocopias y su alimentación, situación que, de acuerdo a **Daniela Herrera, se podría solventar si existiese mejoras en el Bienestar Universitario.**

### **Las propuestas de los estudiantes** 

Frente a esta problemática Daniela Herrera afirmó que en otras universidades del país se ha generado una regulación efectiva a este tipo de puestos de trabajo, ejemplo de ello sería la Universidad de Antioquia, **donde permiten que haya una sana convivencia entre el campus universitario y el trabajo**. (Le puede interesar: ["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

Sin embargo, para Herrera, si no hay una disposición por parte de las directivas y de Bienestar Universitario para generar un espacio de diálogo entre estudiantes, personas externas que trabajan en estos lugares y la rectoría, será muy difícil definir el destino de las “chazas” y de las personas, alumnos o no, que sobreviven de este trabajo.

<iframe id="audio_23565311" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23565311_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

![Acta Universidad Nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/WhatsApp-Image-2018-02-05-at-6.53.03-AM.jpeg){.alignnone .size-full .wp-image-51213 width="480" height="621"}

###### Reciba toda la información de Contagio Radio en [[su correo]
