Title: La movilización por "la familia" se basó en mentiras y manipulaciones: Fernando Giraldo
Date: 2016-08-11 18:11
Category: Entrevistas, LGBTI
Tags: Diversidad sexual, genero, Juan Manuel Santos, Ministerio de Educación, ONU
Slug: la-movilizacion-de-ayer-se-baso-en-mentiras-y-manipulaciones-fernando-giraldo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/LGBT.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

###### [11 Ago 2016] 

**“Se está manipulando tramposamente; actores políticos como el procurador aprovechan lo que la democracia les permite,** ayer la gente no se movilizó sobre la base de un debate abierto, sino sobre mentiras”, es el análisis que hace el politólogo, Fernando Giraldo sobre lo que significó la movilización del día de ayer frente al Ministerio de Educación, por parte de un sector de la sociedad que defendía lo que ellos han llamado “la familia original”.

De acuerdo con Giraldo, lo que lograron ayer algunas figuras políticas es fomentar  una lucha contra la libertad, es decir, contra un principio fundamental, pues el Ministerio de Educación lo que estaba haciendo es cumplir con una orden de la Corte Constitucional, por medio de la cual se pretende fortalecer la educación sexual en los colegios y con ello promover la tolerancia hacia la diversidad sexual con el objetivo de impedir la discriminación en las aulas.

La polémica había empezado este fin de semana por la circulación en redes sociales de falsas cartillas con imágenes de contenido sexual que, según las publicaciones habían sido repartidas por funcionarios del Ministerio de Educación en algunos colegios del país. Una situación que de inmediato generó total rechazo por un amplio sector de padres de familia y directivos de algunos centros educativos, a partir de **“la manipulación de videos, fotos, datos, y sacando de contexto afirmaciones que están n los principios de la Constitución Política”**, dice Giraldo.

“Estamos discutiendo sobre una postura mentirosa, sobre un montaje y un espectáculo público, se están inventando cosas dando por hecho que se va a imponer una ideología de género”, y agrega el politólogo, que a lo que le llaman 'ideología de género' no es más que reconocer la diversidad del otro. **“A las personas se les olvida que solo podemos ser libres si reconocemos que somos diversos.** Cuando no se reconoce la diversidad, créame que nadie es libre ni nadie está protegido de la discriminación”.

Para el analista, discursos como el de la diputada de Norte de Santander, Ángela Hernández, o el del procurador Ordoñez, son equívocos al hablar en nombre del pueblo, pues “el pueblo no es sustituible, y él mismo es el único que puede avanzar en ejercer su soberanía”. Ahora, para el Fernando Giraldo, lo importante será impulsar un instrumento que continúe abriendo la puerta para que se  respeten los derechos de la comunidad LGBTI.

Sin embargo, actuaciones masivas con base en la desinformación y la manipulación, al parecer habrían logrado su cometido, pues mientras esta mañana la ONU estaba lamentando que en Colombia continúe la discriminación por orientación sexual, horas después el presidente Juan Manuel Santos anunció que el **Ministerio no acogerá la cartilla sobre educación sexual en el que se trabajaba de la mano de **Unicef y Colombia Diversa.

<iframe src="http://co.ivoox.com/es/player_ej_12517041_2_1.html?data=kpeik5yUeJKhhpywj5idaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncafZ09PO0MnTb6jd08bZxtSJdqSfwtPOzs7XuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
