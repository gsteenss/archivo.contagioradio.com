Title: Listos formatos de recolección de firmas para revocatoria de Peñalosa
Date: 2017-01-13 13:19
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Unidos revocaremos a Peñalosa
Slug: listos-formatos-de-recoleccion-de-firmas-para-revocatoria-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/formulario-de-revocatoria1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidos Revocaremos a Peñalosa] 

###### [13 Enero 2017] 

A partir de este momento se encuentran disponibles los formatos para llevar a cabo la recolección de firmas de la revocatoria de Enrique Peñalosa. Serán en total 300.000 mil  firmas  las que deberán recolectar los comités  para que se den nuevas elecciones en la capital y **tendrán como plazo hasta el próximo 12 de junio.**

**El formato fue entregado a cada uno de los comités inscritos ante la Registraduría Nacional** quienes ya han expresado que se encuentran en busca de voluntarios que se ubicarán en diferentes partes de la ciudad para recolectar las firmas. Le puede interesar: ["Estas son las razones para la revocatoria a Enrique Peñalosa"](https://archivo.contagioradio.com/estas-las-razones-la-revocatoria-enrique-penalosa/)

Unas vez este proceso se lleve a cabo y el ente regulador, en este caso la misma Registraduría, tendrá un **plazo de 45 días para verificar las firmas**, es decir que se tendrían los resultados sobre mediados del mes de agosto y una vez se declaren **válidas, se realizará la votación con una pregunta sobre si o no a la revocatoria de Peñalosa.**

**De ser ganar en la votación el sí a la revocatoria, habrá un alcalde de turno mientras se convoca a nuevas elecciones en menos de dos meses**. Sin embargo se estima que con todas las movidas jurídicas e impugnaciones que realice la alcaldía el tiempo de todo este proceso puede tardar menos de un año, a partir de la recolección de las firmas.

###### Reciba toda la información de Contagio Radio en [[su correo]
