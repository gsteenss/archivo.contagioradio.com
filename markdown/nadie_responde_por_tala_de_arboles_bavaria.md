Title: Nadie responde por los árboles 25 mil árboles que quiere talar Bavaria en Bogotá
Date: 2017-08-23 14:43
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Bavaria, Enrique Peñalosa
Slug: nadie_responde_por_tala_de_arboles_bavaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/bosque-bavaria-e1503517306915.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Publimetro 

###### [23 Ago 2017] 

Nadie quiere responder por uno de los pulmones que aún le quedan a Bogotá: el bosque de Bavaria, que poco a poco están tumbando. Así lo denuncia la comunidad, de la voz de Carlos Bustos, uno de los voceros, quien expresa: **"Siguen talando los árboles, y uno siente que le están desmembrando un brazo".**

Justificando que el terreno es de propiedad privada, se buscaría consolidar la construcción de un proyecto de viviendas por lo que la meta es sería acabar con un total de 25 mil **árboles que podrían tener cerca de 60 años.** Por el momento tienen permiso de tumbar 365.

No obstante, la comunidad señala que los daños ambientales que ha ocasionado Bavaria tras la pues en marcha de la fábrica, al menos podrían recompensarse manteniendo intacto el bosque que purifica el aire de esa zona, y que por tanto, al acabar con ese pedazo de naturaleza, **afectaría la calidad del oxígeno de varios de los habitantes de la capital.**

### "Oídos sordos" 

"En esos árboles del parque Bavaria hay pájaros que anidan, cuando lo tumben, tumbarán sus casas", indica Bustos. Sin embargo, la administración distrital se empeña en afirmar que no habrá ningún tipo de implicaciones ambientales. **"Tuvimos una audiencia pero fue un diálogo de sordos",** y agrega que "el alcalde Enrique Peñalosa ha asegurado que los árboles se pueden talar y eso no tendrá implicaciones", y en cambio "Resulta que ahora el Alcalde le dio permiso a Bavaria para que desarrolle un plan de vivienda".

Ante la indignación, el lider comunitario indica **"Nos estamos sumando a corriente de opinión de revocar a Alcalde,** porque sino va a tumbarnos todo el bosque". Por eso, desde que se han enterado, todos los martes llevan a cabo un plantón en defensa de la naturaleza, e incluso los niños se han sumado a las actividades  realizando murales para pedir que no se talen los árboles.

"Lo que lo vecinos estamos diciendo es que, si Bavaria contaminó durante tantos años a nuestro barrio y a nuestra gente, es importante que **Bavaria nos deje ese pulmón como una contraprestación a toda esa contaminación que nos dejó durante muchos años**” manifesta Bustos. [(Le puede interesar: Habitantes del occidente de Bogotá buscan impedir que Bavaria tale 25 mil árboles)](https://archivo.contagioradio.com/habitantes-buscan-impedir-tala-de-25-mil-arboles-en-el-occidente-de-bogota/)

<iframe id="audio_20491068" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20491068_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
