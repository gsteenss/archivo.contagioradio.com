Title: La otra cara de Starbucks
Date: 2014-12-17 16:42
Author: CtgAdm
Category: Hablemos alguito
Slug: la-otra-cara-de-starbucks
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsBqNy2d.mp3)  
“Starbucks” conocida por ser la multinacional del café a nivel mundial, con su llegada a Colombia, investigamos sobre su otra cara. Para ello entrevistamos a Mercedes López Martínez del colectivo Vía Orgánica de la ciudad de México DF, quién nos realiza un análisis sobre toda la trayectoria empresarial y económica de la multinacional, desde su origen hasta los días de hoy.Ella nos explica como sus alimentos no provienen de comercio justo y a nivel orgánico utilizan transgénicos o el uso de hormonas de crecimiento en sus productos, por otro lado también desarrolla la forma en la que va hundiendo económicamente al pequeño comercio. Pero como ante todo es realmente una amenaza contra el pequeño productor de café, para dar un salto hacia la agricultura extensiva donde el campesino se convierte en un jornalero explotado de la gran corporación que produce. Para comprender el alcance de la llegada de la multinacional al país líder en producción de café entrevistamos a Alberto Vanegas ,director del departamento de trasnacionales de la CUT, del sindicato  Central Unitaria de Trabajadores de Colombia, quién nos explica las repercusiones laborales y productivas que puede tener ésta sobre el campesinado y los trabajadores.
