Title: Hay que recuperar el congreso que es la representación del pueblo: Iván Marulanda
Date: 2020-07-22 15:24
Author: CtgAdm
Category: Actualidad, Política
Tags: Arturo Char, Congreso, Congreso de la República, democracia, Ivan Marulanda, Senado
Slug: hay-que-recuperar-el-congreso-que-es-la-representacion-del-pueblo-ivan-marulanda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Senado-colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este [20 de julio](https://archivo.contagioradio.com/marcha-por-la-dignidad-arribara-el-20-de-julio-a-bogota-buscando-una-verdadera-independencia/) y pese a a los antecedentes por las investigaciones en su contra, fue elegido como **Presidente del Senado, con 76 votos a favor el representante del partido Cambio Radical Arturo Char Chaljub**, oriundo de Barranquilla .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que algunos lo denominaron como uno de los **senadores menos visibles del país, e incluso señalaron que lo oyeron por primera vez en su posesión, argumento que se justifica con 140 ausencia a las sesiones**, Char se ubica en su nuevo cargo con el 70% del respaldo de los votantes y como representare de una de las [familias más poderosas](https://archivo.contagioradio.com/acusaciones-de-aida-merlano-deben-llegar-a-la-fiscalia/) e investigadas del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El segundo lugar en esta terna fue para el senador **Iván Marulanda, representante de la Alianza Verde y quién disputaba este cargo por la [bancada alternativa](http://www.ivanmarulanda.com/acepte-la-candidatura-la-presidencia-del-senado/)**, y que además obtuvo 20 votos de congresistas que lo respaldaban para que esté obtuviera el cargo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**El poder del presidente del senado es brutal, por qué es quien ordena el trabajo del Senado**, es quien dice cuáles son los temas, y cuales proyectos van, y cuáles se dejan morir´"* , señaló el senador Iván Marulanda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, que de este depende el uso de la palabra, y más aún en un contexto de virtualidad, en donde *"**con un solo botón le da la palabra a quienes ellos quieren y les interesa oír,** y a quienes no, simplemente no tenemos como hacernos escuchar".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Nos quitaron el Estado, lo capturaron unas personas para hacer lo que les dé la gana**, lo convirtieron una empresa que pueden comprar con plata esas minorías que Gobiernan al país"*
>
> <cite>**Iván Marulanda** | Senador **Alianza Verde**  
> </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Afirmó también que **estas elecciones son solo reflejo de lo que se ha convertido la democracia en Colombia**, *"atravesada por un alto interés económico como la compra de votos, el fraude electoral, en otros, que han puesto por años parte del Congreso a su antojo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que el senador Marulanda, denominó como una ***"usurpación al pueblo"***, resaltando que estas elecciones presentan a personas que no representan a la comunidad y sus necesidades, sino que al contrario *"privilegia los intereses de estas minorías poderosas, poniendo al Congreso al servicio del Gobierno y la economía "*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una mirada al funcionamiento del senado por Ivan Marulanda

<!-- /wp:heading -->

<!-- wp:paragraph -->

Señalando que el *"Congreso es del pueblo",* y que allí deben estar todas las representaciones étnicas, religiosas, culturales, ideológicas, de género entre otras, para generar un diálogo entre diferentes pensamientos y realidades, el senador sostuvo, que esta diversidad es sesgada con la aglomeración de un poder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y afirmó, que a pesar de la participación de una minoría de la que hace parte, ***"estamos aquí diciéndole al país que lo representamos, representamos la Constitución** y además recibimos todas las críticas, la estigmatizan y malos tratos para que no seamos escuchados".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explicó que el país debe conocer las dinámicas que se dan en el Congreso, algo que debe ser no solamente público como se da en las diferentes trasmisiones en vivo, sino claro y de fácil entendimiento para todo mundo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entendiendo que no todo mundo puede y tolera hasta casi 12 horas de sesión, **Ivan Marulanda explicó lo que ocurre al interior de la Comisión Tercera del Senado**, a la cual él hace parte y en donde se debaten los temas económicos del país, uno de ellos la [Renta Básica](https://archivo.contagioradio.com/proyecto-de-renta-basica-de-emergencia-contaria-con-mayorias-en-el-congreso/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***"Desde su presentación fue tema que tuvo retrasos, algo que duraba segundos para ser tramitado en la Cámara Tercera, terminó convirtiéndose en 12 días"***, tiempo que se incremento según el senador, tras la demora en el envío de un correo con las propuestas por parte del entonces presidente del Senado, [Lidio García](https://archivo.contagioradio.com/en-manos-del-presidente-del-senador-esta-la-decision-de-dar-vida-a-las-curules-de-las-victimas/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posterior a ello, contó que de estas propuestas presentadas **fueron seleccionadas 8 de las cuales 4 eran de representantes de Barranquilla, desconociendo la realidad de los territorios**. Algunos días después este proyecto se hundió, y fue presentado nuevamente por parte de la bancada alternativa el 20 de julio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto agregó que las **4 propuestas se dan, incluso desconociendo las realidades de la comunidad de Barranquilla, en donde más del 70% de la población vivía de la informalidad**, *"en medio de la pandemia el hambre reinó, y los hizo salir a las calles en búsqueda de un sustento, acción que generó las cifras más altas de contagio por covid en el país".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Mientras múltiples regiones pasaban por condiciones alarmantes, sus mismos representantes en el senado estaban obstruyendo la solución para poder ofrecer una vida digna de las personas"*
>
> <cite>**Iván Marulanda** | Senador **Alianza Verde**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Son 4 meses de lucha por la renta básica", Ivan Marulanda

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante la Renta Básica el senador agregó que en Colombia hay 9 millones de hogares que afrontan difíciles condiciones económicas, *"**llevamos 4 meses diciendo que el Estado tiene la obligación ética y política de socorrer a las familias**, y no debería haber nada más urgente que resolver".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto agregó que existe una clara falta de voluntad por parte del Gobierno y muchos congresistas, señalando que, *"**en diciembre del año pasado le regalaron 10 billones de pesos en beneficios tributarios a los sectores más poderoso**s, en contraposición a los 20 billones de pesos que requiere esta iniciativa"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Diferencia entre renta básica e ingreso mínimo

<!-- /wp:heading -->

<!-- wp:paragraph -->

El nuevo Presidente de Senado llega impulsando una propuesta denominada ingreso mínimo, la cual pretende **entregar \$160.000 mensuales a cada hogar** en condición vulnerable, *"según el DANE cada hogar tiene 3.2 personas, osea **van a dar \$1.600 diarios por persona, eso es una limosna, una vergüenza".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó qué esta iniciativa esta pensada para dos millones ochocientos mil hogares, de nueve millones que lo necesitan. Mientras que el proyecto de **renta básica pretende entregar un salario mínimo por hogar** (**\$877.803**) por durante 3 meses, y a 9 millones de familias.

<!-- /wp:paragraph -->

<!-- wp:heading -->

*"No hay más remedio que seguir en esta lucha, qué consiste en despertar la conciencia"*
----------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente señaló que hoy como senador de la bancada alternativa, de la manos de quienes comparten sus ideales no tienen *"más remedio que seguir en esta lucha, qué consiste en despertar la conciencia este país".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tarea que inicia, *"explicando a cada colombiana y colombiano que **el país le pertenece, que el Estado le pertenece, y que los puestos que están allí en el Congreso son seleccionados y pagados con sus impuestos"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y así según el senador, generar conciencia que permita a la ciudadana entender que, *"vender el voto es vender su propiedad en el Estado, su pedazo de Colombia a una persona que no lo representa y que además pone en riego la democracia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Conozca la entrevista completa con Ivan Marulanda, y escuche también las opiniones de Angela María Robledo (Rep. Cámara por **Colombia Humana**), **Sandra Ramirez**, (**senadora Partido Farc**) y **Germán Robayo** (MOE)*, el programa completo en Otra Mirada ¿vientos de cambio en el Congreso o mas de lo mismo?.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D291521582185652&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
