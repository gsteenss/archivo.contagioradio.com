Title: ¿Está intimidando la Fuerza Pública a jóvenes indígenas en Juradó, Chocó?
Date: 2019-06-06 08:12
Author: CtgAdm
Category: DDHH, Nacional
Tags: Chocó, comunidades indígenas, Juradó
Slug: esta-intimidando-la-fuerza-publica-a-jovenes-indigenas-en-jurado-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cogfm-arc-seguridad-jurado-choco-indigenas-16.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

La **Organización Nacional Indígena de Colombia (ONIC)** denunció la retención arbitraria e intimidación por parte de la Fuerza Pública contra ocho jóvenes Embera Katio del resguardo Dos Bocas en Juradó, en el que cerca de 1600 personas viven situaciones de  confinamiento y desplazamiento desde el 26 de abril.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw)| 8 jóvenes indígenas fueron retenidos arbitrariamente por el [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) en Resguardo Embera Katío, Juradó, Chocó. Denuncian que les han limitado su derecho a movilizarse en el territorio. Desde [@ONIC\_Colombia](https://twitter.com/ONIC_Colombia?ref_src=twsrc%5Etfw) solicitamos investigar esta acción [@mindefensa](https://twitter.com/mindefensa?ref_src=twsrc%5Etfw) [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw)
>
> — Organización Nacional Indígena de Colombia - ONIC (@ONIC\_Colombia) [3 de junio de 2019](https://twitter.com/ONIC_Colombia/status/1135690794504007681?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Las comunidades de Juradó denuncian 

Según información proporcionada por la ONIC, integrantes del Batallón 23 de la Armada nacional que hace presencia en el lugar, retuvo a ocho jóvenes Embera Katio mientras estos recolectaban plátano en el lugar, acusándoles de pertenecer a la guerrilla, interrogándoles e instándoles a que abandonen el lugar.

Según los testimonios, los jóvenes fueron retenidos de 8:00 de la mañana a 2:00  de la tarde por los uniformados quienes les prohibieron transitar por el territorio, la ONIC señala que debido a la forma en que las personas tuvieron que abandonar sus viviendas como consecuencia del desplazamiento, la mayoría de ellas permanecen indocumentadas.

También se responsabilizó a integrantes de la Fuerza Pública de haberse alimentado con los animales y aves que fueron dejados por los indígenas en sus resguardos, sin embargo no existen pruebas de que miembros de la Armada hayan realizado tales acciones.

Según el personero de Juradó, Carlos Mario Cardona, estos casos y declaraciones de integrantes de la comunidad, " ya fueron remitido a la Personería", sin embargo señala "que no puede afirmar ni negar que que se hayan dado estos abusos" contrastándolo con su testimonio, a través del cual ha podido ver un "trato ameno entre fuerza pública y comunidades indígenas".

### Más de 1000 personas regresarían a sus resguardos 

Según Cardona, las comunidades que fueron desplazadas y que permanecen  en la vereda de Dos bocas definirán el próximo 12 de junio  su retorno voluntario  a través de un comité de justicia transicional, mientras, la Personería seguirá haciendo entrega de la ayuda humanitaria brindada por la Unidad de víctimas.

En la información proporcionada por la Fuerza Pública a la Personería, se asegura que actualmente no hay presencia de estos grupos alrededor de los resguardos indígenas, por lo que las condiciones de seguridad están dadas para su retorno, "esperamos que sea cierto lo que manifiestan las autoridades" indicó el personero. [(Lea también: Paramilitares de las AGC sitian a 800 habitantes de Juradó, Chocó)](https://archivo.contagioradio.com/paramilitares-de-las-agc-sitian-a-800-habitantes-de-jurado-choco/)

<iframe id="audio_36744083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36744083_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
