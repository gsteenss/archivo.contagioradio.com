Title: Islandia demuestra que hay otra forma de salir de la crisis
Date: 2015-02-19 20:18
Author: CtgAdm
Category: Economía, Otra Mirada
Tags: Islandia crisis, Islandia no paga deuda de bancos, Islandia sale de la crisis, Revolución de los vikingos
Slug: islandia-demuestra-que-hay-otra-forma-de-salir-de-la-crisis
Status: published

###### **Foto:Vozpopuli.com** 

7 años después del inicio de la crisis financiera en Islandia, y luego **de no haber pagado la deuda externa de sus bancos**, **su economía se ha recuperado totalmente** hasta el punto de continuar creciendo, igual que su tasa de desempleo que se sitúa entre las mas bajas en comparativa con las de los países europeos.

En 2008 Islandia sufrió una grave crisis económica debido a la quiebra de los tres bancos principales del país Landsbanki, Kaupthing y Glitnir.

Fue entonces cuando **el país decidió no seguir las sendas impuestas por el FMI y el BM, así como de otros países europeos, que le recomendaban una solución de carácter neoliberal**, que consistía en socializar la deuda de los bancos para poder sacarlos a flote, es decir poner la deuda a disposición del libre mercado, igual que ha sucedido con otros países afectados como España, Italia, Irlanda o el peor de los casos, Grecia.

La alternativa consistió en **nacionalizar los bancos** para así hacerse cargo el propio gobierno de la deuda, pero esta ya superaba lo que las propias arcas públicas podían pagar. De tal forma el gobierno decidió no pagar la deuda externa que estos habían contraído, que en total ascendía a 3.600 millones de euros.

Para ello judicializó a los responsables y siguiendo las vías democráticas, **convocó hasta dos referendos, uno en 2008, con el 58,9% a favor de no pagar , y en 2009 con el 93,2% también a favor de no pagar la deuda** contraída principalmente con Reino Unido, y Holanda.

Las reacciones por parte de los dos principales países inversores en Islandia fueron utilizar al FMI para sus propios intereses con el objetivo de que presionara al país para que pagara la deuda y poner como condición para su entrada a la UE, el pago de la deuda, a lo que los islandeses han respondido que a pesar de que es una prioridad su entrada en la UE, dichas condiciones son inadmisibles.

Hoy Islandia tiene una **tasa de desempleo situada en el 3%, en comparación del 11,9% que tenía cuando comenzó la crisis**, su PIB, en continua expansión, actualmente es del 3,3%. El turismo, y las exportaciones pesqueras son la base de la economía.

Pero sobre todo lo más importante es que **no han sido aplicadas las medidas de austeridad** que tan **graves consecuencias han tenido en otros países**, como Grecia o España, sobre la perdida en derechos laborales que se ha traducido en una bajada de salarios y de poder adquisitivo de toda la sociedad. La destrucción de todo el empleo público, la desaparición de los derechos sociales y con ello el estado del bienestar, con consecuencias como la aparición de cifras alarmantes de pobreza y **el ascenso a tercera fuerza política de un partido de carácter neonazi en Grecia.**

En comparación con el camino tomado por Grecia, es decir el de las políticas neoliberales dictadas por la Troika, el FMI, y la UE, que han dejado al **país Heleno la actualidad con la mayor tasa de desempleo de la UE, pasando del 12% al 26%, con una contracción de la economía interior del 25%**. Una deuda actualmente traducida en tres rescates financieros, que el propio gobierno ha reconocido imposible de pagar.
