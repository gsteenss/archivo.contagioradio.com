Title: Duque llevará al Congreso la misma Ley de Financiamiento que le devolvieron
Date: 2019-10-17 17:37
Author: CtgAdm
Category: Economía, Política
Tags: Aumento del Desempleo, Congreso de la República, Ley de Financiamiento
Slug: duque-llevara-congreso-misma-ley-de-financiamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Ley-de-Financiamiento.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Diseño-sin-título-4.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MinHacienda] 

Después de conocerse la decisión de la Corte Constitucional de hundir la Ley de Financiamiento y la consecutiva instrucción del Gobierno Duque de presentar una segunda versión de esta iniciativa con el mismo texto, al considerar que el único problema de esta normativa existió en su trámite de aprobación, el representante a la Cámara, David Racero advierte que no solo se trata de un tema procedimental  sino de fondo, ya que no soluciona los problema de la economía nacional.

Para Racero , la decisión de la Corte Constitucional  debería leerse como un proyecto con insuficiencias, que al ser creada y promovida por el Gobierno, realizó al Congreso y al país dos promesas que tras 10 meses de funcionamiento, no han sido cumplidas.

En primer lugar, indica el congresista, esta reforma prometía financiar al Estado y subsanar un hueco fiscal que pudiera aportar recursos al presupuesto de este año y aunque hay ingresos de corto plazo, fruto de la declaración de renta de personas que ganan más de 35 millones de pesos,  la exención de impuestos a grandes empresas abre las puertas a que el mismo Estado se vea desfinanciado al privar al país de este recaudo.

La segunda promesa está vinculada a las exenciones tributarias a las grandes empresas, que según estimaciones del Gobierno, paulatinamente iban a posibilitar una generación de empleo, sin embargo, hoy el país está llegando a los indicadores más altos de desempleo de los últimos 15 años.

Cifras del Departamento Nacional de Estadística (DANE) apuntan a que en agosto de 2019, la tasa de desempleo en el total de 13 ciudades y áreas metropolitanas de Colombia fue de 11,4%, simbolizando un aumento de 1,3 puntos porcentuales en comparación del mismo mes en 2018, es decir, en agosto 391.000 personas perdieron su trabajo, sumándose a un total de 2,6 millones de personas que no tienen un empleo.

El representante rechaza el hecho de que se asuma la derogación de la Ley de Financiamiento como un problema del Congreso donde no fue tramitada de manera correcta, "esto obedeció a la presión del Gobierno de tener que aprobar a pupitrazo un proyecto en función de beneficiar a grandes empresas", señala. [(Lea también: Corte Constitucional tumba Ley de Financiamiento)](https://archivo.contagioradio.com/corte-constitucional-tumba-ley-de-financiamiento/)

### Ley de Financiamiento 2.0 no garantiza un panorama alentador 

Racero afirma que si el propósito del Gobierno es llevar al Congreso la misma ley sin realizar cambios, no estaría haciendo frente a esas debilidades, pues el hueco fiscal que existe para el presupuesto de la nación de los próximos años, no se debe arreglar con reformas tributarias "que aumentan impuestos a clase media y trabajadores", emulando la misma perspectiva económica de hace 30 años.

Ante propuestas aprobadas como el artículo 44 del Presupuesto General de la Nación, que permite que las universidades públicas paguen con recursos asignados a la  educación las sentencias en contra de la Nación, Racero señala que los colombianos deben prepararse pues propuestas y reformas de tipo laboral y pensional, vienen en camino.

**"En la mayoría del Congreso es donde no impera la razón sino la fuerza política",** afirma Racero concluyendo que en el país, en lugar de confrontar que se está manejando de forma incorrecta la economía, le atribuyen el desempleo y la inflación anual que se ubica en un 3,8%,  a factores  internacionales como el aumento del dólar o los choques externos con otras naciones.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_43274734" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43274734_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
