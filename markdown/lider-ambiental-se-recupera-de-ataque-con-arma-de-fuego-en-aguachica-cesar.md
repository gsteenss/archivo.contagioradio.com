Title: Crisotómo Mansilla, líder social se recupera de ataque con arma de fuego en Aguachica, Cesar
Date: 2017-07-22 21:29
Category: Ambiente, Nacional
Tags: CORDATEC, fracking, san martin
Slug: lider-ambiental-se-recupera-de-ataque-con-arma-de-fuego-en-aguachica-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/san_martin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [22 Jul 2017] 

La Corporación Defensora del Agua, Territorio y Ecosistemas CORDATEC, denunció que Crisotómo Mansilla, integrante de esta organización y líder de la junta de acción comunal de la vereda El Loro, **fue víctima de un ataque con arma de fuego mientras estaba en su finca el pasado 20 de julio**, en estos momentos se encuentra recuperándose en un centro hospitalario en Aguachica.

Mansilla es un líder social reconocido en la zona por su importante labor en la defensa del agua, el territorio y los ecosistemas **ante los proyectos de minería que se pretenden desarrollar en la parte alta del municipio de San Martín, Cesar**. (Le puede interesar: ["Conoco Phillips no cumple los requisitos de licencia ambiental para Fracking en San Martín"](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/))

A estos hechos se suman las amenazas que recibieron Marina Media, presidenta de la Junta de Acción Comunal del barrio La Floresta y José Eliecer Torres, presidente de la Junta de Acción Comunal del barrio San Vicente de Paul, quienes también se han opuesto a la **venta del agua en bloque a la multinacional Conoco Phillips para el proyecto de fracking que se adelanta en la región**.

De igual forma integrantes de CORDATEC denunciaron que el pasado 14 de julio, durante el desarrollo de Fracking, organizado por Ecopetrol, ACIPET, y la Unión Sindical Obrera, en Barrancabermeja, las acciones de estigmatización y señalamiento hechas por Milton Jerez líder social de la petrolera. (Le puede interesar: ["Gases y aturdidoras contra la población de San Martín, Cesar"](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/))

Frente a esta situación CORDATEC expresó que ya ha puesto en conocimiento a las diferentes autoridades, de los actos de intimidación y violencia hacia sus integrantes, por tal motivo le **exigen a las instituciones a nivel nacional, departamental y local que tomen medidas urgentes y necesarias en materia de investigaciones, protección y garantías para el ejercicio de la defensa de los derechos humanos** y manifestaron que rechazan cualquier acto de agresión cometido en contra de la vida e integridad de las personas.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
