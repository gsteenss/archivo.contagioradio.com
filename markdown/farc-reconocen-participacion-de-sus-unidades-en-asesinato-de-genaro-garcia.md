Title: FARC reconocen participación de sus unidades en asesinato de Genaro García
Date: 2015-08-24 09:44
Category: Nacional, Paz
Tags: cese bilateral, cese unilateral de las FARC, Cumbre Agraria, ELN, FARC, Frente Amplio por la PAz, Mesa de conversaciones de paz
Slug: farc-reconocen-participacion-de-sus-unidades-en-asesinato-de-genaro-garcia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/genaro_garcia_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div>

###### Foto: Actualidadurbana 

###### [24 Ago 2015] 

En un comunicado público, la delegación de paz de las FARC reconoce que hay comprometidas unidades militares de esa guerrilla en el asesinato de Genaro García el pasado 3 de Agosto. García era el representante del Consejo Mayor del Alto Mira y Frontera y fue asesinado en circunstancias que comprometían amenazas por parte de esa guerrilla.

</div>

Tanto la organización Afrodescendiente CONPA, como el Proceso de Comunidades Negras PCN, habían exigido que la guerrilla de las FARC se pronunciara sobre el asesinato. Por parte de esa guerrilla se presentó un comunicado en el que anunciaban las investigaciones.

Este Lunes la delegación de paz de las FARC publicó un comunicado en el que reconoce la participación de sus unidades en el asesinato y anunció que habrá justicia. Así mismo dentro de las investigaciones anunciaron que un integrante del Estado Mayor hará parte de los juicios.

El comunicado también agrega que esa guerrilla lamenta que situaciones como estas se presenten y expresaron sus condolencias a la familia de García.

A continuación el comunicado completo...

<div>

##### *"El Bloque Occidental Comandante Alfonso Cano de las Fuerzas Armadas Revolucionarias de Colombia – Ejército del Pueblo, frente a los hechos acaecidos en el área rural  de Tumaco el pasado 3 de agosto en los que fue asesinado el líder étnico Genaro García, representante legal del Consejo Comunitario de Alto Mira y Frontera, informa a la opinión pública*

##### *1-Tal como nos habíamos comprometido,  hemos adelantado investigaciones internas y en el terreno sobre el mencionado caso, las cuales conducen a la conclusión de que efectivamente unidades de la Columna Móvil Daniel Aldana se encuentran comprometidos en la comisión de tan condenable acto.*

##### *2-Hechos como este que golpean directamente los procesos de organización y de lucha popular con los que nos sentimos identificados, contradicen la política de las FARC-EP sobre el comportamiento con la población civil y el respeto a las comunidades étnicas, lo que constituye un preocupante agravante a la luz de nuestra legislación interna.*

##### *3-Violaciones  como ésta a la disciplina y a la ética revolucionaria implican, a la luz de nuestra normatividad y juridicidad guerrillera, supervisión del Secretariado Nacional del Estado Mayor Central para la investigación y sanción de todos los mandos y combatientes involucrados en la comisión de dicho delito.*

##### *4-De parte de todos los guerrilleros y guerrilleras del Bloque Occidental Comandante Alfonso Cano de las FARC-EP, hacemos llegar nuestras condolencias a los familiares del señor Genaro García, a las comunidades negras de Tumaco y, en particular, al Consejo Comunitario de Alto Mira y Frontera.*

##### *5- Actos tan repudiables no pueden volver a cometerse por unidades pertenecientes a las FARC EP. Nos comprometemos a sancionar el hecho y a tomar las disposiciones y medidas correspondientes para evitar su repetición.*

##### *Estado Mayor del Bloque Occidental Comandante Alfonso Cano*

##### *Fuerzas Armadas Revolucionarias de Colombia – Ejército Del Pueblo"*

</div>

<div>

</div>
