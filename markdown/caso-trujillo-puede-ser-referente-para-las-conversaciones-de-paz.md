Title: Caso Trujillo puede ser referente para las conversaciones de paz
Date: 2016-04-07 17:24
Category: DDHH, Nacional
Tags: masacre Trujillo, trujillo, valle, víctimas Trujillo
Slug: caso-trujillo-puede-ser-referente-para-las-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz ] 

###### [7 Abril 2016 ]

Tras 22 años de las denuncias por las graves violaciones a los derechos humanos conocidas como [['los hechos violentos de Trujillo'](https://archivo.contagioradio.com/el-23-de-abril-el-estado-debera-pedir-perdon-a-las-victimas-de-trujillo/)], ocurridos entre 1989 y 1992, se suscribió entre el Estado colombiano y los representantes de las víctimas un 'Acuerdo de Solución Amistosa' ante la CIDH, que de cumplirse a cabalidad podría desmontar el paramilitarismo, generar garantías para la vida e integridad de los civiles y ser un referente para las conversaciones de paz.

Según afirma Danilo Rueda, integrante de la 'Comisión de Justicia y Paz', el mayor alcance de este Acuerdo es el compromiso que ha pactado el Gobierno nacional para desmontar las estructuras paramilitares que continúan operando en la región, en un plazo establecido. "Sí ese plan se cumple, creemos que podrían darse plenas garantías para el goce de los derechos de la población y para las garantías de no repetición", agrega Rueda.

Rueda asegura que fue posible llegar a este Acuerdo luego de años de parálisis en las investigaciones penales que cursan, que lograron ser dinamizadas con las audiencias que desde hace cuatro años se llevan a cabo en la CIDH y con los litigios del abogado Eduardo Carreño, integrante del 'Colectivo de Abogados José Alvear Restrepo'.

Este Acuerdo hará posible el reconocimiento de 76 víctimas en total, 34 ya reconocidas y 42 que serían reconocidas el próximo 24 de abril, en un acto público en el municipio de Trujillo, y según asevera Rueda el reconocimiento de las víctimas no terminaría con este acto pues queda abierta la posibilidad de esclarecer nuevas víctimas, incluso aquellas de pena moral, un hecho que inédito y que se logra por la labor de dignificación, el deber de la memoria y el derecho a la verdad que las víctimas han promovido a través de la creación y el cuidado del 'Parque Monumento'.

##### [Danilo Rueda, Comisión Intereclesial de Justicia y Paz] 

<iframe src="http://co.ivoox.com/es/player_ej_11084734_2_1.html?data=kpadmpmbd5Whhpywj5WZaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncaTV1NSYttfZrsrgzdSY0trJqMaf1MrfjdfJqsbmxtPhx5DUpdPVjNHO1ZDHs8%2Fqxtfgw8jNs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
