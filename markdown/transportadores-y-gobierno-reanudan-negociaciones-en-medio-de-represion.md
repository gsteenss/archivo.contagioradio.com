Title: Transportadores y Gobierno reanudan negociaciones en medio de represión
Date: 2016-07-01 15:29
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, paro camionero colombia 2016, Paro Colombia 2016
Slug: transportadores-y-gobierno-reanudan-negociaciones-en-medio-de-represion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Paro-Camionero-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asesoría ATC ] 

###### [1 Julio 2016]

El paro camionero avanza en diferentes regiones con 33 puntos de concentración y según denuncia Pedro Aguilar, de la Asociación Colombiana de Camioneros, en departamentos como Valle, Caldas y Risaralda, **efectivos del ESMAD han agredido con gases lacrimógenos y aturdidoras a quienes apoyan la movilización.** "Es triste decir que la protesta pacífica de los camioneros el Gobierno la quiere violentar para podernos sacar y golpearnos (...) y seguiremos protestando hasta que se nos arregle la situación laboral de nuestros conductores y el flete a los propietarios", afirma el líder.

"Nos preocupa que llevamos 24 días en paro y el Gobierno no busque solucionar, que trate de romper, de desestabilizar, de crear problemáticas que no existen, que en cabeza del Ministro de Transporte busquen reprimir la protesta, nosotros los camioneros no estamos de acuerdo, porque nosotros no estamos cerrando vías (...) **nosotros sabemos que hay infiltrados en nuestras protestas, son personas que tratan de descomponer nuestra protesta lanzando piedras a los vehículos**", asevera Aguilar.

El líder sostiene que las últimas reuniones con funcionarios de alto nivel, han sido productivas y esperan que Clara López, actual Ministra de Trabajo, comunique los **actos administrativos a través de lo que se dignificará el carácter de los transportadores de carga pesada**. El vocero insiste en que las exigencias que le hacen al Gobierno tienen que ver, entre otras, con que "el dinero recolectado del patrimonio de los camioneros aparezca y no se pierda".

Frente a la situación los gremios de transportadores emitieron este comunicado en el que aseguran que las negociaciones con el gobierno se reanudan este viernes a las 4 de la tarde.

[![Comunicado paro camionero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Comunicado-paro-camionero.jpg){.aligncenter .size-full .wp-image-26003 width="560" height="1092"}](https://archivo.contagioradio.com/transportadores-y-gobierno-reanudan-negociaciones-en-medio-de-represion/comunicado-paro-camionero/)

<iframe src="http://co.ivoox.com/es/player_ej_12094255_2_1.html?data=kpedm5mWeZahhpywj5WaaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncbHZxdfcjabLucrgwteYj5C6s8TZ09SYo9jTp8rVxM6SpZiJhpTijMnSjajFscrjz8rf0diRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
