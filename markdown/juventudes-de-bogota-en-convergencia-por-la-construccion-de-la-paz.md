Title: Juventudes de Bogotá en convergencia por la construcción de la paz
Date: 2015-06-26 17:05
Author: AdminContagio
Category: Comunidad, Resistencias
Tags: juco, juventud rebelde, Juventudes, organizaciones juveniles, progresistas, tejuntas
Slug: juventudes-de-bogota-en-convergencia-por-la-construccion-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/jventud.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Notiagen 

##### <iframe src="http://www.ivoox.com/player_ek_4693361_2_1.html?data=lZumlZiadY6ZmKiakpuJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtbqxtPh18nJt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Jeisson Quintero, miembro de TELEJUNTAS y Congreso de los Pueblos] 

##### **26 Jun** 

El viernes 26 de junio se presentó el Encuentro de Unidad Juvenil, que iniciará el día 1ro de agosto, escenario en el que convergirán múltiples organizaciones juveniles con la intención de discutir su papel en la construcción de una Bogotá y un país en paz y con justicia social y vida digna.

Juventudes liberales, progresistas, comunistas, del Congreso de los Pueblos, Marcha Patriótica, entre otros procesos sociales, hicieron el viernes la presentación del Encuentro en el Centro Cultural El Rehuso,  con el fin de generar vínculos amistosos entre sí; afirma Jeisson Quintero, miembro de la coordinadora de organizaciones TEJUNTAS,  quien agrega que el encuentro futbolístico entre Colombia y Argentina que se llevó a cabo esa tarde, aportó a la integración entre colectivos y organizaciones.

La intención de los y las jóvenes al realizar este Encuentro es hacer un diagnóstico que genere insumos para la construcción de las condiciones para la población juvenil que garanticen su participación en la sociedad. Cabe decir que en otras ocasiones la participación de este sector ha sido desvalorada, como en el Plan de Desarrollo 2014-2018, donde fueron nombrados únicamente tres veces. Ver: ["El plan nacional de desarrollo aprobado solo menciona a los jovenes 3 veces"](https://archivo.contagioradio.com/el-plan-nacional-de-desarrollo-aprobado-solo-menciona-3-veces-a-los-jovenes/). Por esto, exigen que sus aportes sean tenidos en cuenta en la construcción de la paz.

El Encuentro de agosto se llevará a cabo en el Centro Memoria Paz y Reconciliación, desde las 8:00 A.M hasta las 2:00 P.M. Se vivirá un ambiente de "discusión y charla" para que la juventud tenga la posibilidad de hablar con la ciudadanía respecto a su responsabilidad en el aporte a la paz, afirma Jeisson.

El evento culminará con un espacio cultural a partir de las 4:00 p.m.
