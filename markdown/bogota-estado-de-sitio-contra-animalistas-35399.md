Title: Bogotá parecía en estado de sitio contra los animalistas: Plataforma ALTO
Date: 2017-01-30 12:42
Category: Animales, Nacional
Tags: Animalistas, Antitaurinos, Corte Constitucional, Taurinos, Toros
Slug: bogota-estado-de-sitio-contra-animalistas-35399
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Protestas-animales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [30 Ene. 2017] 

En Bogotá los animalistas continuaron la movilización pacífica contra la temporada taurina, esta vez lo hicieron más lejos de la Plaza de Toros La Santamaría dado los más de 3 mil hombres que se dispusieron por la administración distrital para custodiar la zona. **Bajo el slogan “sí a la lectura, no a la tortura” casi 3 mil animalistas se sentaron a leer y repudiar la “fiesta brava”.**

Para **Natalia Parra directora de la Plataforma Alto**, se exageró en las medidas de seguridad que tomó la Alcaldía “parecía que estuviéramos en estado de sitio contra el movimiento animalista” dijo. Le puede interesar: [Nadie quiere ser animalista](https://archivo.contagioradio.com/nadie-quiere-ser-animalista/)

Natalia hizo hincapié en el alto gasto público que representó el dispositivo de seguridad que planeó la Alcaldía “a nuestro juicio **se destinaron unos recursos físicos y humanos para un evento tan específico cuando la ciudad necesita a estos policías cuidando a las personas**, que no están pagando una boleta costosísima pero que son ciudadanos que necesitan ser protegidos”

En el lugar, como lo contó en entrevista para Contagio Radio Natalia, habían diversos tipos de fuerzas policiales como Policía y personas en las terrazas altas de ciertos edificios “insisto exageraron en esto” aseveró la animalista. Le puede interesar: [“Los animalistas somos pacifistas”](https://archivo.contagioradio.com/los-animalistas-somos-pacifistas-35020/)

De igual modo, Natalia aseguró que pese a lo difícil que fue llegar al punto donde se habían citado para realizar la actividad lograron convocar a más de 3 mil manifestantes, quienes además de la actividad de lectura, cantaron y lanzaron arengas como forma de reivindicar las exigencias de los animalistas y rechazar el toreo.

También manifestó que contrario a lo dicho por algunos taurinos y por el director del Consorcio Colombia Taurina, Felipe Negret “**la plaza de toros no se llenó** – solo dos tercios de la Plaza de Toros- no porque la gente tuviera miedo de ir, sino **porque es un espectáculo costoso y los que seguirán yendo serán los más ortodoxos”** y agregó “estamos seguros que van hasta a regalar boletas porque no es sostenible ir todos los domingos”.

### **Corte Constitucional sigue sin pronunciarse** 

Este lunes, los animalistas seguirán esperando el resultado de las votaciones que deben llevarse a cabo en la Corte Constitucional, frente a las dos ponencias que plantean podrían acabarse o no a las corridas de toros en Colombia, por ello, Natalia afirmó que seguirán movilizándose y haciendo que se respete la vida de todos los animales en Colombia.

“**Hay nuevos factores que la Corte debe analizar en estas ponencias"**, por ejemplo el coleo y las peleas de gallos, sin desconocer lo avanzado en el cuidado de animales domésticos, por ello **si la Corte llega a tomar una medida más acorde a la vida animal saldrán a celebrar** antes del Domingo.

Sin embargo, dijo que **si la Corte llegará a decidir que “en nuestro país siga en la barbarie y no de un paso a la civilización pues muy seguramente saldremos a manifestarnos**, espero mucho más masivamente que siempre porqué ya la indignación llegaría a un nivel de tope”. Le puede interesar: [Que la Corte, corte la tortura en Colombia: Animalistas](https://archivo.contagioradio.com/que-corte-la-tortura-en-colombia-animalistas/)

<iframe id="audio_16724907" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16724907_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
