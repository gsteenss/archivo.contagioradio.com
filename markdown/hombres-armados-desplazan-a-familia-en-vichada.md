Title: Paramilitares usurpan 13 mil hectáreas de tierras en Vichada
Date: 2016-07-25 17:13
Category: DDHH, Nacional
Tags: Comunidad Taoísta en Colombia, Desplazamiento en Colombia, Vichada
Slug: hombres-armados-desplazan-a-familia-en-vichada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Llanera ] 

###### [25 Julio 2016]

Desde el pasado 2 de marzo la señora Marianela Cruz, líder de la Comunidad Taoísta de la vereda El Triunfo, en Vichada, se vio obligada a desplazarse a la vereda La Primavera, tras ser víctima, junto a su esposo, de un violento ataque por parte de hombres armados con fusiles y vestidos de camuflado, quienes entraron en la noche a su finca de 670 hectáreas, saquearon la tienda de la que viven, **los agredieron físicamente y les dijeron que debían abandonar sus predios porque volverían para matarlos**.

"Siendo las ocho de la noche estábamos mirando televisión cuando sentimos la llegada de gente (...) encendí la linterna cuando vi que entró un fusil y enseguida un hombre (...) que atacó a mi esposo, diciéndole que venía a matarnos, que nosotros teníamos problemas en la zona (...) lo amarraron, lo tiraron al piso, me sacaron y me pusieron unas armas en la espalda y me ubicaron como a cinco metros de él (...) nunca nos quisieron decir cuál era el problema (...) ellos entraron a mi cuarto, me buscaron documentos, papeles de las fincas y plata (...) **me arrodillaron me dijeron que me iban a pegar un tiro, me pusieron un fusil en la frente**", narra Marinela.

Pese a que los armados no se identificaron como miembros de algún grupo en particular, la pobladora logró identificarlos como habitantes de la zona y aunque llevó su reporte a la Policía, hasta el momento siguen en libertad, inclusive ya han construido seis viviendas en las 13.500 hectáreas que componen los terrenos aledaños a su finca, y que pertenecen a la Comunidad Taoísta que ella administra; llegando incluso a ofrecer algunas hectáreas a algunos pobladores. Ante la falta de atención por parte de la Policía, **la Comunidad ha determinado recuperar por vía propias sus tierras. **

Durante estos últimos cuatro meses la señora Marianela **ha solicitado a la Sijin, a la Policía, al Ejército y a la Unidad de Víctimas acciones concretas para proteger sus derechos** y volver a su finca; sin embargo, ninguna entidad le ha respondido, sigue esperando que la Policía vaya a la zona para inspeccionar, como le aseguraron que lo harían cuando ella se acercó para poner la denuncia.

[![13843364\_10154130197960020\_1181413711\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/13843364_10154130197960020_1181413711_o1.jpg){.aligncenter .size-full .wp-image-26793 width="720" height="1280"}](https://archivo.contagioradio.com/hombres-armados-desplazan-a-familia-en-vichada/13843364_10154130197960020_1181413711_o-2/)

[![13844124\_10154130197995020\_1248906338\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/13844124_10154130197995020_1248906338_o.jpg){.aligncenter .size-full .wp-image-26794 width="720" height="1280"}](https://archivo.contagioradio.com/hombres-armados-desplazan-a-familia-en-vichada/13844124_10154130197995020_1248906338_o/)

<iframe src="http://co.ivoox.com/es/player_ej_12334367_2_1.html?data=kpeglZmXepihhpywj5abaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5ynca7V087O0MrQpYy309rnh5enb82ZpJiSo6nIqdOfxcqYzsaPh9Dh1tPWxsbIb7XV0IqwlYqliNTowpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
