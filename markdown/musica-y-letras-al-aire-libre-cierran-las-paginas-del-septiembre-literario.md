Title: Música y letras al aire libre, cierran las páginas del "Septiembre literario"
Date: 2015-09-26 08:00
Category: Cultura, eventos
Tags: Entreviñetas, Fundalectura, IDARTES, Jota Mario Arbelaez, lectura al aire libre, Libro al viento, Parque Nacional, Rocksito Paula Ríos, Santiago Rivas los puros criollos, Septiembre Literario, Velandia y la tigra
Slug: musica-y-letras-al-aire-libre-cierran-las-paginas-del-septiembre-literario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/parque-nacional-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: fundalectura.org 

###### [25 Sep 2015]

En el cierre del "**Septiembre literario**" programado por el Instituto Distrital para las Artes IDARTES para el fin de semana, el Parque Nacional de Bogotá será el epicentro de múltiples actividades musicales y literarias para el disfrute de todas y todos los capitalinos.

Durante los dos días, se desarrollarán actividades gratuitas entre las que destaca **un recital con lecturas** de los poetas **José Luis Díaz Granados**, **Juan Manuel Rocca**, **Jota Mario Arbelaez** y **Mery Yolanda**, entre otros, además se leerán fragmentos de poesía de **Gómez Jattin** y un **homenaje al gran poeta colombiano León de Greiff** que tendrá lugar en la Fuente de Poesía.

Otro de los eventos es el concurso “**Voces en el cuadrilátero**”, dirigido por el periodísta **Santiago Rivas**, presentador del espacio televisivo "**Los puros criollos**" de Señal Colombia, en el que escritores, músicos y artistas compartirán sus oficios con el público asistente.

La programación incluye los tradicionales "**Picnic Literario**" y "**Trueque el Libro**", además de algunos espacios en los que librerías itinerantes presentarán distintas ofertas editoriales para todos los gustos, y la "**Feria La Vagabunda**" que abre un lugar al cómic y la novela gráfica gracias al proyecto editorial independiente "**Entreviñetas**".

La programación musical tendrá como invitados especiales a **Edson Velandia**, cantautor santandereano integrante del grupo "**Velandia y la tigra**", caracterizado por el sonido de la rasca y la irreverencia de su imagen; **Rockcito**, un interesante y melódico proyecto de rock infantil, dirigido por **Paula Ríos** cantante colombiana, junto a ex integrantes de las agrupaciones Kraken, Pirañas Amazónicas y Bonka; y la intervención de la agrupación **Cinemacinco**, conformada en el año 2005 quienes definen su música como “música mestiza, folk para el mundo”.

Conozca las diferentes actividades, para que se programe este fin de semana con las letras, aquí encontrará toda la [programación](http://www.idartes.gov.co/images/articles/Literatura/2015/09-Septiembre/folleto_curvas_2.jpg?utm_source=phplist1386&utm_medium=email&utm_content=HTML&utm_campaign=ESTE+FIN+DE+SEMANA%2C+EL+PARQUE+NACIONAL+SE+VISTE+DE+LITERATURA).
