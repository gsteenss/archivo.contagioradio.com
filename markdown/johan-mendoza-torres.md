Title: Johan Mendoza Torres
Date: 2016-02-09 15:09
Author: AdminContagio
Slug: johan-mendoza-torres
Status: published

**Johan Mendoza**

[![Periodismo en riesgo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/periodismo-en-riesgo/)  

#### [Periodismo en riesgo](https://archivo.contagioradio.com/periodismo-en-riesgo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-06-09T08:40:40-05:00" title="2019-06-09T08:40:40-05:00">junio 9, 2019</time>](https://archivo.contagioradio.com/2019/06/09/)No pocos años permanecerá trinando un expresidente sin darse cuenta el daño que causa, y el impulso tan serio que…[Leer más](https://archivo.contagioradio.com/periodismo-en-riesgo/)  
[![La quiebra de la ideología neutral](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-24-at-16.07.10-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)  

#### [La quiebra de la ideología neutral](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-25T07:30:36-05:00" title="2019-05-25T07:30:36-05:00">mayo 25, 2019</time>](https://archivo.contagioradio.com/2019/05/25/)Ya pronto serán 30 años desde que el mundo occidental daba cantos de victoria ante el fin de la guerra…[Leer más](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)  
[![El fascismo del siglo XXI](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1-1-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)  

#### [El fascismo del siglo XXI](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-14T10:03:27-05:00" title="2019-05-14T10:03:27-05:00">mayo 14, 2019</time>](https://archivo.contagioradio.com/2019/05/14/)No pocos años permanecerá trinando un expresidente sin darse cuenta el daño que causa, y el impulso tan serio que…[Leer más](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)  
[![Se quemó Guaidó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D5kHMpUWkAAi0Vu-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/guaido-se-quemo/)  

#### [Se quemó Guaidó](https://archivo.contagioradio.com/guaido-se-quemo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-02T12:45:20-05:00" title="2019-05-02T12:45:20-05:00">mayo 2, 2019</time>](https://archivo.contagioradio.com/2019/05/02/)Guaidó se quemó, nada que hacer, es un mamarracho. Y esto no es un agravio, es una literatura de nuestra…[Leer más](https://archivo.contagioradio.com/guaido-se-quemo/)  
[![La ideología de Daniel Coronell](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/La-ideología-de-Daniel-Coronell-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)  

#### [La ideología de Daniel Coronell](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-04-22T09:59:04-05:00" title="2019-04-22T09:59:04-05:00">abril 22, 2019</time>](https://archivo.contagioradio.com/2019/04/22/)Daniel Coronell es un excelente profesional, pero obedece con claridad a esa naturaleza del periodismo, que cuando se manifiesta de…[Leer más](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)  
[![Cauca: ¿ponderar derechos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56232221_1676961222406256_7904919769650823168_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/cauca-ponderar-derechos/)  

#### [Cauca: ¿ponderar derechos?](https://archivo.contagioradio.com/cauca-ponderar-derechos/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-04-05T16:09:55-05:00" title="2019-04-05T16:09:55-05:00">abril 5, 2019</time>](https://archivo.contagioradio.com/2019/04/05/)La protesta no es el problema, es solo el síntoma. El problema no es el gobierno, este es solo un…[Leer más](https://archivo.contagioradio.com/cauca-ponderar-derechos/)  
[![¿Adversarios o enemigos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/adversarios-o-enemigos/)  

#### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-22T06:00:16-05:00" title="2019-02-22T06:00:16-05:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)Quisimos ser adversarios, pero nos equipararon con bestias salvajes, mientras éramos nosotros los que llorábamos los muertos.[Leer más](https://archivo.contagioradio.com/adversarios-o-enemigos/)  
[![Arias: no lloraremos por ti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/61229-2/)  

#### [Arias: no lloraremos por ti](https://archivo.contagioradio.com/61229-2/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-07T06:00:43-05:00" title="2019-02-07T06:00:43-05:00">febrero 7, 2019</time>](https://archivo.contagioradio.com/2019/02/07/)or más de que los medios corporativos de información anden en una campaña casi melodramática para recomponer la imagen de…[Leer más](https://archivo.contagioradio.com/61229-2/)  
[![¿Polarizar o no polarizar Dr. Wasserman?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  

#### [¿Polarizar o no polarizar Dr. Wasserman?](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-01-19T07:24:22-05:00" title="2019-01-19T07:24:22-05:00">enero 19, 2019</time>](https://archivo.contagioradio.com/2019/01/19/)16 Ene 2019El movimiento a favor de la polarización no es tan furioso como lo presentan, pero sí es un…[Leer más](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  
[![Mensaje de navidad para un Estado secuestrado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  

#### [Mensaje de navidad para un Estado secuestrado](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-12-19T06:00:46-05:00" title="2018-12-19T06:00:46-05:00">diciembre 19, 2018</time>](https://archivo.contagioradio.com/2018/12/19/)A esa gente, que tiene secuestrado al Estado Colombiano y que participa de los mecanismos de ese secuestro, un solo…[Leer más](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  
[![De la onírica a la política](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/De-la-onírica-a-la-politica-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)  

#### [De la onírica a la política](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-28T09:44:04-05:00" title="2018-11-28T09:44:04-05:00">noviembre 28, 2018</time>](https://archivo.contagioradio.com/2018/11/28/)Lo que hoy hacen por ejemplo Uribe, Carrasquilla y Martínez es la prueba tangible del que llamo yo, el secuestro…[Leer más](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)  
[![Desde el sótano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/desde-el-sotano/)  

#### [Desde el sótano](https://archivo.contagioradio.com/desde-el-sotano/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-09T15:33:08-05:00" title="2018-11-09T15:33:08-05:00">noviembre 9, 2018</time>](https://archivo.contagioradio.com/2018/11/09/)Según Schopenhauer toda verdad pasa por tres etapas. En la primera es ridiculizada. En la segunda es criticada y en…[Leer más](https://archivo.contagioradio.com/desde-el-sotano/)  
[![La protesta sumisa   ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-protesta-sumisa/)  

#### [La protesta sumisa   ](https://archivo.contagioradio.com/la-protesta-sumisa/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-10-25T09:55:22-05:00" title="2018-10-25T09:55:22-05:00">octubre 25, 2018</time>](https://archivo.contagioradio.com/2018/10/25/)Lograron ofender a los manifestantes más por un grafiti que por las razones de una movilización; lograron preocupar más a…[Leer más](https://archivo.contagioradio.com/la-protesta-sumisa/)  
[![Civilización y barbarie](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/lideres-sociales-770x400-2-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/civilizacion-y-barbarie/)  

#### [Civilización y barbarie](https://archivo.contagioradio.com/civilizacion-y-barbarie/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-10-12T07:00:29-05:00" title="2018-10-12T07:00:29-05:00">octubre 12, 2018</time>](https://archivo.contagioradio.com/2018/10/12/)Civilización y barbarie fundamentan todos los intentos por mantener intacto el poder, y todos los intentos por desarticularlo, por democratizarlo…[Leer más](https://archivo.contagioradio.com/civilizacion-y-barbarie/)  
[![¡Golpe de Estado!  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/golpe-de-estado-770x400-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/golpe-de-estado/)  

#### [¡Golpe de Estado!  ](https://archivo.contagioradio.com/golpe-de-estado/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-09-25T12:13:29-05:00" title="2018-09-25T12:13:29-05:00">septiembre 25, 2018</time>](https://archivo.contagioradio.com/2018/09/25/)Hay tipologías del Golpe de Estado. el que queda, El Golpe de Opinión, parece una utopía, pero no está tan…[Leer más](https://archivo.contagioradio.com/golpe-de-estado/)

#### [Un fantasma recorre Colombia…](https://archivo.contagioradio.com/un-fantasma-recorre-colombia-2/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-09-11T06:00:05-05:00" title="2018-09-11T06:00:05-05:00">septiembre 11, 2018</time>](https://archivo.contagioradio.com/2018/09/11/)Ese autodenominado centro que ataca a la izquierda y a la derecha empleando el concepto populismo como recurso peyorativo, sin…[Leer más](https://archivo.contagioradio.com/un-fantasma-recorre-colombia-2/)  
[![La corrupción es un síntoma, no la enfermedad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/corrupcion-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-corrupcion-es-un-sintoma-no-la-enfermedad/)  

#### [La corrupción es un síntoma, no la enfermedad](https://archivo.contagioradio.com/la-corrupcion-es-un-sintoma-no-la-enfermedad/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-08-27T08:00:19-05:00" title="2018-08-27T08:00:19-05:00">agosto 27, 2018</time>](https://archivo.contagioradio.com/2018/08/27/)Al final algo más de 11 millones de votos dejaron boquiabiertos hasta a los “nimierdistas” más radicales. La votación fue…[Leer más](https://archivo.contagioradio.com/la-corrupcion-es-un-sintoma-no-la-enfermedad/)  
[![Recuperemos la libertad política: abajo el show electoral](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/debate2-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/recuperemos-la-libertad-politica-abajo-el-show-electoral/)  

#### [Recuperemos la libertad política: abajo el show electoral](https://archivo.contagioradio.com/recuperemos-la-libertad-politica-abajo-el-show-electoral/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-05-11T06:00:31-05:00" title="2018-05-11T06:00:31-05:00">mayo 11, 2018</time>](https://archivo.contagioradio.com/2018/05/11/)Una sociedad no es democrática porque tenga derecho a votar, para hablar de democracia, los de abajo, la gente común…[Leer más](https://archivo.contagioradio.com/recuperemos-la-libertad-politica-abajo-el-show-electoral/)  
[![Necesitamos esperanza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/marcha-de-flores-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/necesitamos-esperanza/)  

#### [Necesitamos esperanza](https://archivo.contagioradio.com/necesitamos-esperanza/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-04-03T09:24:19-05:00" title="2018-04-03T09:24:19-05:00">abril 3, 2018</time>](https://archivo.contagioradio.com/2018/04/03/)La perdición de la política es que sea una realidad asumida sin perspectiva histórica, que sea asumida solo como “el…[Leer más](https://archivo.contagioradio.com/necesitamos-esperanza/)  
[![Oh problema: Los emocionales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/amor-y-odio-politica-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/oh-problema-los-emocionales/)  

#### [Oh problema: Los emocionales](https://archivo.contagioradio.com/oh-problema-los-emocionales/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-02-22T09:31:24-05:00" title="2018-02-22T09:31:24-05:00">febrero 22, 2018</time>](https://archivo.contagioradio.com/2018/02/22/)El problema político más grave que tienen Colombia consiste en que no se ha comprendido para qué sirve la política.[Leer más](https://archivo.contagioradio.com/oh-problema-los-emocionales/)  
[![Palabras ante un exterminio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Diseño-sin-título-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/palabras-ante-un-exterminio/)  

#### [Palabras ante un exterminio](https://archivo.contagioradio.com/palabras-ante-un-exterminio/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-02-04T16:11:58-05:00" title="2018-02-04T16:11:58-05:00">febrero 4, 2018</time>](https://archivo.contagioradio.com/2018/02/04/)Temístocles Machado, Jair Cortés, Liliana Astrid Ramírez, por supuesto estos son solo algunos de los nombres de personas que fueron…[Leer más](https://archivo.contagioradio.com/palabras-ante-un-exterminio/)  
[![Palabras para inicio de semestre](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/inicio-de-semestre-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/palabras-para-inicio-de-semestre/)  

#### [Palabras para inicio de semestre](https://archivo.contagioradio.com/palabras-para-inicio-de-semestre/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-01-17T06:00:59-05:00" title="2018-01-17T06:00:59-05:00">enero 17, 2018</time>](https://archivo.contagioradio.com/2018/01/17/)El anticonformismo utópico, produce tanta o igual conciencia como una propaganda de Coca-cola, y mantiene lugares comunes como, la idea…[Leer más](https://archivo.contagioradio.com/palabras-para-inicio-de-semestre/)  
[![¡Ten Piedad Petro! … lo arruinaste todo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/PETRO-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/ten-piedad-petro-lo-arruinaste-todo/)  

#### [¡Ten Piedad Petro! … lo arruinaste todo](https://archivo.contagioradio.com/ten-piedad-petro-lo-arruinaste-todo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2017-12-08T08:48:59-05:00" title="2017-12-08T08:48:59-05:00">diciembre 8, 2017</time>](https://archivo.contagioradio.com/2017/12/08/)Petro la embarró al mezclar la necesidad de fuerza política, con la vinculación de sus planes de gobierno progresista, con…[Leer más](https://archivo.contagioradio.com/ten-piedad-petro-lo-arruinaste-todo/)  
[![El deber común (Mensaje a la fuerza pública)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/fuerza-publica-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-deber-comun-mensaje-a-la-fuerza-publica/)  

#### [El deber común (Mensaje a la fuerza pública)](https://archivo.contagioradio.com/el-deber-comun-mensaje-a-la-fuerza-publica/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2017-11-29T07:30:45-05:00" title="2017-11-29T07:30:45-05:00">noviembre 29, 2017</time>](https://archivo.contagioradio.com/2017/11/29/)La fuerza pública tiene un deber. Tiene un Estado que proteger, pero no tiene por qué defender a una clase…[Leer más](https://archivo.contagioradio.com/el-deber-comun-mensaje-a-la-fuerza-publica/)
