Title: Sin Corte Electoral no estamos haciendo mucho: MOE
Date: 2017-05-16 16:15
Category: Nacional, Política
Tags: Corte Electoral, Reforma Electoral
Slug: sin-corte-electoral-no-estamos-haciendo-mucho-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 May 2017] 

Pese a que aún no se conoce como quedará la Reforma Electoral, Alejandra Barrios, directora de la Misión de Observación Electoral, hizo una advertencia frente a las modificaciones que se le hicieron a la propuesta y a la posibilidad de quitar la Corte Electoral: **“si dejamos el tema de justicia electoral, tal cual como está, pues no estamos haciendo mucho**”.

Esta semana finalizará con el debate sobre la reforma Electoral y se conocerá por fin si esta incluirá o no la Corte Electoral, institución que se encargarían de hacer una veeduría sobre el accionar de los partidos políticos, **si habrá modificaciones al Consejo Nacional Electoral y cómo serán las listas para las próximas elecciones**.

Barrios señaló que la propuesta enviada por la Misión Electoral Especial tenía tres puntos: la creación de la Corte Electoral, que es el tema más álgido del debate, las modificaciones del **Consejo Nacional Electoral, que pasaría a ser el Consejo Electoral Colombiano**, adquiriendo autonomía y de esta forma restringiendo que ambas instituciones sean cuotas políticas o estén permeadas por intereses políticos.

Y el tercer punto eran las listas cerradas, que, para la directora de la MOE, “**ayuda no solo a fortalecer la democracia interna de los partidos, sino sobre todo a controlar el tema de la financiación de las campañas políticas**. Hoy es muy difícil hacer seguimiento y las listas abiertas lo que han permitido es una competencia mucho más dura”. Le puede interesar: ["Estas son las recomendaciones de la Misión Electoral para la Reforma Política"](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/)

Además, Barrios que hay unos sectores que de manera muy clara han demostrado que les interesa mantener un statos quo, sin que el panorama democrático cambie “dentro de los temas a los que más se han opuesto esta la Corte Electora, **el aumento de las sanciones y de la capacidad de fiscalización de los partidos políticos y los procesos electorales**”. Le puede interesar: ["Hay sectores que no les interesa reforma electoral: MOE"](https://archivo.contagioradio.com/hay-sectores-que-no-les-interesa-reforma-electoral-moe/)

Durante la tarde de hoy, la MEE se reunirá con el Ministro del Interior, Juan Fernando Cristo y mañana ya se tendrá conocimiento sobre los cambios que tendría esta propuesta y como quedaría la Reforma Electoral.

###### Reciba toda la información de Contagio Radio en [[su correo]
