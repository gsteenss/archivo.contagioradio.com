Title: Inminente desalojo a comunidad Wayúu Katsaliamana en la Guajira
Date: 2017-05-09 14:37
Category: DDHH, Nacional
Tags: Cerrejón, Comunidades Wayúu, Denuncias, resistencia
Slug: inminente-desalojo-a-la-comunidad-wayuu-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/guajira2-e1494358244369.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nación Wayúu] 

###### [09 Mayo 2017] 

Indígenas miembros de la comunidad Wayúu, que hacen parte del resguardo Katsaliamana ubicado a la altura del kilómetro 70 sobre la vía ferrea, **llevan más de 150 días de resistencia ante la presión de desalojo** por parte de los funcionarios del Cerrejón quienes insisten en sacarlos de sus territorios ancestrales.

A través de un comunicado las autoridades Tradicionales y Lideres Indígenas de la Gran Nación wayuu, denunciaron que **los funcionarios del Cerrejón anunciaron con circulares a la comunidad Katsaliamana que se debían retirar de las vías ferreas del Cerrejón.** Le puede interesar:["El Cerrejón buscaría desalojar a 27 familias Wayúu para desviar el arroyo Bruno" ](https://archivo.contagioradio.com/cerrejon-buscaria-desalojar-27-familias-wayuu-desviar-arroyo-bruno/)

José Silva, miembro de la Nación Wayúu del municipio de Uribia en la Guajira, denuncia que las intenciones de desalojo, que se han presentado desde 2016 en este departamento, no cesan. **Además de las confrontaciones que han sostenido con el ESMAD y de haber interpuesto varias denuncias,** los indígenas aseguran que líderes de las comunidades han recibido amenazas de muerte.

Desde diciembre de 2016, la asamblea permanente del resguardo Katsaliamana ha denunciado **el constante deterioro de recursos naturales como ríos y arroyos** que son vitales para las comunidades así como la invasión de territorios ancestrales. "El Cerrejón nos tiene que respetar, nos están secando los ríos, los arroyos y nos están sometiendo a la miseria". Le puede interesar:["Robos, golpes e insultos de ESMAD a indígenas Wayúu en la Guajira"](https://archivo.contagioradio.com/robos-golpes-e-insultos-de-esmad-a-indigenas-wayuu-en-guajira/)

Según Silva, las funcionarios del Cerrejón no ha cumplido los acuerdos de reubicación y resarcimiento de daños sociales y ambientales que se habían trazado y por el contrario los han mantenido bajo presión desde el momento, que de forma pacífica, se tomaron las vías ferreas del Cerrejón.

A los atropellos se suma la denuncia de los indígenas frente al **accionar del ICBF que estaría provocando que más de 5 mil niños se encuentren sin atención medica o alimentaria,** producto del programa que está en marcha de esta institución, impuesto por los operadores gubermanetales que no tiene comunicación alguna con los territorios ancestrales.  
Para José Silva es un programa que tiende al fracaso. "A los niños los van a dejar morir porque el programa del ICBF no está diseñado para las necesidades de las comunidades Wayúu". Le puede interesar:["Comunidades Wayúu  vuelven a bloquear la vía ferrea del Cerrejón"](https://archivo.contagioradio.com/comunidades-wayuu-exigen-derecho-a-la-autonomia-regional/)

El pasado 6 de febrero 7 autoridades ancestrales fueron capturados de manera ilegal, en medio de otro desalojo. **Días después los indígenas salieron del centro de reclusión**, sin embargo se encuentran en libertad condicional y no pueden salir de sus casas. Silva asegura que estos actos hacen parte del temor que pretenden infrigir en la comunidad Wayúu para que abandone el territorio.

<iframe id="audio_18590301" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18590301_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
