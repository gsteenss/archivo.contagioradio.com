Title: Fotos: Así fue la tercera "bailatón" exigiendo #ConsultaAntitaurina
Date: 2015-09-14 14:49
Category: Animales, Voces de la Tierra
Tags: Alcaldía de Bogotá, Consulta Antitaurina, corridas de toros en Bogotá, Gustavo Petro, Maltrato animal, Ministerio de hacienda, Registraduría Nacional
Slug: fotos-asi-fue-la-tercera-bailaton-exigiendo-consultaantitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bailaton-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Constagio Radio 

El pasado 10 de septiembre frente a la sede de Registraduría Nacional del Estado Civil en Bogotá, se llevó a cabo **la tercera "bailatón" por la consulta antitaurina, donde participaron aproximadamente treinta ciudadanos y ciudadanas**, exigiendo que la consulta popular para definir el futuro de las corridas de toros en Bogotá, se realice el próximo 25 de octubre.

La Registraduría hizo la solicitud extemporánea al **Ministerio de Hacienda del dinero para llevar a cabo la consulta, por la suma de 34 mil millones de pesos,** lo que significaría que se piensa realizar en una fecha distinta al 25 de octubre. Es por eso que los defensores de la vida de los toros,  han reclamado que la consulta popular se lleve a cabo con las elecciones regionales, atendiendo el derecho a la democracia, la participación ciudadana, y además para que no exista detrimento patrimonial teniendo en cuenta que si la consulta se realiza con las elecciones, **únicamente costaría 1600 millones de pesos.**

“Buscamos establecer que aquí habría una falta a la moralidad pública porque el registrador como funcionario público puede ahorrar un dinero considerable, pero opta por la forma más costosa de favorecer un gusto en el que se genera un impacto fiscal”, asegura Natalia Parra, directora de la Plataforma ALTO.

**Sin embargo, el costo sería aún menor, debido a que el alcalde de la capital, Gustavo Petro, afirmó que el distrito asumiría la impresión de los tarjetones que tendrían un costo de 300 millones de pesos. **Petro anunció que entablará acciones penales contra el Estado por no permitir la realización de la votación para el 25 de octubre.

 \
