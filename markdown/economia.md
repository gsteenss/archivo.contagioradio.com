Title: Economía
Date: 2014-11-25 15:42
Author: AdminContagio
Slug: economia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/economia_verde2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

ECONOMÍA
--------

[![Menos impuestos para las empresas, menos salario para los trabajadores](https://archivo.contagioradio.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400.jpg "Menos impuestos para las empresas, menos salario para los trabajadores"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/)  

###### [Menos impuestos para las empresas, menos salario para los trabajadores](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/)

[<time datetime="2018-12-21T17:47:19+00:00" title="2018-12-21T17:47:19+00:00">diciembre 21, 2018</time>](https://archivo.contagioradio.com/2018/12/21/)Para el 2019 el salario mínimo subirá únicamente \$46.874, sectores como la Central Unitaria de Trabajadores se mostraron en desacuerdo.[Leer más](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/)  
[![Trabajadores buscan que aumento de salario mínimo sea de doble dígito](https://archivo.contagioradio.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1.png "Trabajadores buscan que aumento de salario mínimo sea de doble dígito"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1.png 770w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1-300x156.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1-768x399.png 768w, https://archivo.contagioradio.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1-370x192.png 370w"}](https://archivo.contagioradio.com/aumento-salario-minimo/)  

###### [Trabajadores buscan que aumento de salario mínimo sea de doble dígito](https://archivo.contagioradio.com/aumento-salario-minimo/)

[<time datetime="2018-12-11T18:25:50+00:00" title="2018-12-11T18:25:50+00:00">diciembre 11, 2018</time>](https://archivo.contagioradio.com/2018/12/11/)El aumento del salario mínimo propuesto por trabajadores sería de entre 78 mil y 93 mil pesos, los indicadores económicos respaldarían estas cifras[Leer más](https://archivo.contagioradio.com/aumento-salario-minimo/)  
[](https://archivo.contagioradio.com/hueco-fiscal-hacienda/)  

###### [Hueco fiscal se lo invento el ministro de Hacienda: Eduardo Sarmiento](https://archivo.contagioradio.com/hueco-fiscal-hacienda/)

[<time datetime="2018-11-27T15:13:09+00:00" title="2018-11-27T15:13:09+00:00">noviembre 27, 2018</time>](https://archivo.contagioradio.com/2018/11/27/)Según el economista, el hueco fiscal fue un problema creado por el aumento de gasto del Gobierno, y que ahora se quiere solucionar con el cobro del IVAc[Leer más](https://archivo.contagioradio.com/hueco-fiscal-hacienda/)  
[](https://archivo.contagioradio.com/aumento-iva/)  

###### [Aumento del IVA: Única propuesta del Gobierno que puso de acuerdo al Congreso](https://archivo.contagioradio.com/aumento-iva/)

[<time datetime="2018-11-21T13:38:00+00:00" title="2018-11-21T13:38:00+00:00">noviembre 21, 2018</time>](https://archivo.contagioradio.com/2018/11/21/)Aunque el Gobierno intenta obtener recursos con el IVA a la canasta familiar, congresistas proponen formulas mejores para lograr los dineros que hacen falta[Leer más](https://archivo.contagioradio.com/aumento-iva/)
