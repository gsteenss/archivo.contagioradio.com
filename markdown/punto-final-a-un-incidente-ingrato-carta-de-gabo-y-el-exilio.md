Title: "Punto final a un incidente ingrato" carta de 'Gabo' y el exilio
Date: 2015-04-19 07:30
Author: CtgAdm
Category: Educación, Nacional
Tags: El Tiempo, Exilio, FFMM, Gabriel García Márquez
Slug: punto-final-a-un-incidente-ingrato-carta-de-gabo-y-el-exilio
Status: published

###### Foto: mediaisla.net 

Se cumple un año de la muerte de Gabriel García Márquez, autor de estremecedoras obras como *Cien años de soledad, El coronel no tiene quién le escriba, Del amor y otros demonios*, entre otras, quien ha sido recordado como el hombre que a través de la literatura, la prosa y la retórica, contó historias fantásticas con fuertes pincelazos de realidad.

El autor, nacido en Aracataca, un pequeño pueblo del departamento del Magdalena, impregnó sus obras con diversos sucesos acontecidos en Colombia, cargándolas de críticas y análisis literarios. Sus firmes y claras convicciones políticas y estrecha amistad con el líder de la revolución cubana, Fidel Castro, lo llevarían al exilio en México en 1981.

Diversos fueron los rumores de las razones por las que dejó el país. Unos, afirmaban que se habría ido de Colombia para darle una mayor resonancia publicitaria a su próximo libro; otros, que lo hizo en apoyo de una campaña internacional para desprestigiar al país.

[Punto Final a Un Incidente Ingrato por Gabo](https://es.scribd.com/doc/262298480/Punto-Final-a-Un-Incidente-Ingrato-por-Gabo "View Punto Final a Un Incidente Ingrato por Gabo on Scribd")

<iframe id="doc_67341" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/262298480/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

Estas fuertes afirmaciones publicadas en El Tiempo por un anónimo, en el 81, desataron una sorpresiva reacción del premio Nobel de literatura, impulsándolo a responder con una extensa carta (Punto final a un incidente ingrato, año 1981) dirigida a la oligarquía y publicada en El País, de España, donde expuso dignamente las verdaderas razones por las que, contra su voluntad, debió irse a México para siempre.

En ese mismo año, con el gobierno de Julio César Turbay y expedido el Estatuto de Seguridad, el escritor recibió un mensaje del Ejército Nacional para interrogarlo sobre presuntos vínculos con el M-19. Tales acusaciones implicaban medidas judiciales que lo llevarían a la cárcel. De esta manera, Gabriel García salió del país junto con su compañera de vida, Mercedes, en medio de un fuerte conflicto y persecución colombiano.

En respuesta al artículo de El Tiempo respondió: “*No puedo terminar sin hacer una precisión de honestidad. Desde hace muchos años, el tiempo ha hecho constantes esfuerzos por dividir mi personalidad: de un lado, el escritor que ellos no vacilan en calificar de genial, y del otro lado, el comunista feroz que está dispuesto a destruir a su patria. Cometen un error de principio: soy un hombre indivisible, y mi posición política obedece a la misma ideología con que escribo mis libros. Sin embargo, el tiempo me ha consagrado con todos los elogios como escritor, inclusive exagerados, y al mismo tiempo me ha hecho víctima de todas las diatribas, aun las más infames, como animal político.”*

Gabriel García, como muchos otros artistas literarios que relatan historias y cuentan verdades, fue víctima de estigmatización política al optar por una firme posición ante la cruenta gobernanza de un sector dogmático y radical. Él, como muchos otros personajes, se fue de su país, alejándose de sus raíces de la forma más brusca posible, pero sin olvidar que Colombia sería una de sus razones principales para continuar con sus obras y hacer crítica por medio del “Realismo Mágico”, realismo que, a fin de cuentas, ha sido “Realismo Crítico”.
