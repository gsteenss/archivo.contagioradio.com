Title: Paramilitares de AGC intentan asesinar a líder en Zona Humanitaria de Jiguamiandó, Chocó
Date: 2019-02-23 17:24
Author: CtgAdm
Category: Comunidad, DDHH, Líderes sociales
Tags: AGC, denuncia, ELN, humanitario
Slug: neoparamilitares-intentan-asesinar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/AGC-Y-ELN-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

###### [23 Feb 2019] 

Desde hace más de una semana la Comisión Intereclesial de Justicia y Paz ha denunciado el aumento de actividad neoparamilitar en los territorios de Jiguaminadó y Curvaradó, en Chocó; esta situación elevó aún más el riesgo para los habitantes de la zona, que este sábado denunciaron la presencia de 8 hombres armados que intentaron asesinar al líder Luis Cogollo, de la Zona Humanitaria Nueva Esperanza, en Jiguamiandó.

Según la denuncia de la Comisión, 8 neoparamilitares encapuchados intentaron asesinar al líder Luis Cogollo y otras personas que residen en la Zona Humanitaria Nueva Esperanza; los hombres, que afortunadamente no lograron su cometido, huyeron en una moto del lugar. (Le puede interesar: ["Alerta por fuertes operaciones paramilitares en territorios de Curvaradó y Jiguaminadó"](https://archivo.contagioradio.com/operaciones-neoparamilitares-curvarado-jiguamiando/))

> [\#Urgente](https://twitter.com/hashtag/Urgente?src=hash&ref_src=twsrc%5Etfw) Ahora. 8 paramilitares con dos metralletas y armas cortas intentaron asesinar lider Luis Cogollo y otros de ZH Nueva Esperanza.Encapuchados huyen en moto. [@PBIColombia](https://twitter.com/PBIColombia?ref_src=twsrc%5Etfw) [@kolko\_mail](https://twitter.com/kolko_mail?ref_src=twsrc%5Etfw) [@gimena\_wola](https://twitter.com/gimena_wola?ref_src=twsrc%5Etfw) [@ABColombia1](https://twitter.com/ABColombia1?ref_src=twsrc%5Etfw) [@IvanCepedaCast](https://twitter.com/IvanCepedaCast?ref_src=twsrc%5Etfw) [@angelamrobledo](https://twitter.com/angelamrobledo?ref_src=twsrc%5Etfw) [@AidaAvellaE](https://twitter.com/AidaAvellaE?ref_src=twsrc%5Etfw) [@PizarroMariaJo](https://twitter.com/PizarroMariaJo?ref_src=twsrc%5Etfw)
>
> — Comisión Justicia y Paz (@Justiciaypazcol) [23 de febrero de 2019](https://twitter.com/Justiciaypazcol/status/1099352210503282689?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Población vive asediada por la guerra** 

Según información de habitantes en la zona, el jueves se presentaron enfrentamientos armados entre el Ejército de Liberación Nacional (ELN) y otra parte armada, aún por establecer si se trataba del Ejército o las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) que también hacen presencia en el territorio. (Le puede interesar: ["Incursión armada de AGC en comunidad indígena de Jiguamiandó"](https://archivo.contagioradio.com/incursion-armada-de-agc-en-comunidad-indigena-de-jiguamiando/))

El viernes, cerca de las 10:30 de la mañana habitantes escucharon cerca de la Zona Humanitaria Nueva Esperanza la reactivación de combates, en medio de el punto conocido como Zapayal y el Río Jiguamiandó entre el ELN y las AGC. Los habitantes también señalan que vieron al Ejército dirigirse a la zona donde se escuchaba el enfrentamiento. (Le puede interesar: ["Combates entre ELN y AGC provocan desplazamiento de 12 familias en Jiguaminadó, Chocó"](https://archivo.contagioradio.com/combates_eln_agc_desplazamiento_jiguamiando_choco/))

### **Urge una acción de verificación humanitaria** 

En la zona, además del atentado contra el líder Cogollo y la reclusión a la que se ven obligados los habitantes de la región se han conocido panfletos amenazantes contra líderes, pobladores e integrantes de la Comisión de Justicia y Paz; desde la Organización se han denunciado estos hechos, y han recordado que la situación vivida ahora se asemeja a la operación de "sitiamiento" que se desarrolló entre 1999 y 2000, para generar desplazamientos masivos y la entrada de multinacionales a la tierra.

Adicionalmente, la Comisión señala que tanto las AGC como el ELN se mueven por el territorio sin ser controladas por las fuerzas del Estado, razón por la que hacen un llamado urgente a la institucionalidad, así como a organizaciones de derechos humanos para hacer una verificación humanitaria en terreno que permita tomar acciones para salvaguardar la vida de las poblaciones que allí se encuentran.

### **Paramilitares también ingresaron a Zona Humanitaria Pueblo Nuevo** 

Luego de varias horas de tensión y miedo las comunidades negras del territorio colectivo de Jiguamiandó denunciaron que paramilitares de las Autodefensas Gaitanistas de Colombia con armas largas intimidando a la población del lugar humanitario reconocido por la CIDH.

La denuncia de la Comisión Intereclesial de Justicia y Paz resalta que el ingreso de los armados se presentó hacia las 6 pm de este sábado 23 de febrero y que la permisividad para la circulación de los neoparamilitares por parte de la fuerza pública es evidente, dado que este territorio también ha sido afectado por una fuerte militarización que no ha impedido el control territorial por parte de los paramilitares.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
