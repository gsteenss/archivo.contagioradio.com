Title: Pueblo indígena Awá fue víctima de una nueva masacre
Date: 2020-07-30 20:57
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Exterminio Indígena, ONIC, Pueblo indígena Awá, UNIPA
Slug: pueblo-indigena-awa-fue-victima-de-una-nueva-masacre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Comunidad-Indígena-Awá-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Minga

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a las reiteradas denuncias y alertas sobre las graves amenazas sobre **el pueblo indígena Awá, este fue blanco de una nueva masacre** perpetrada en el Resguardo Ñambi Piedra Verde con jurisdicción en el municipio de Barbacoas, Nariño. Le puede interesar: [ONIC denuncia intento de masacre contra comunidad indígena Awá](https://archivo.contagioradio.com/onic-denuncia-intento-de-masacre-contra-comunidad-indigena-awa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se registró el miércoles 29 de Julio sobre las 8 de la noche, cuando un grupo armado irrumpió en la vivienda del exgobernador, **Fabio Alfonso Guanga García**, lo sustrajeron y **lo asesinaron ante la mirada de sus familiares a escasos metros de su residencia.** (Lea también: [Líderes indígenas son los principales blancos de asesinatos en contra de defensores del medio ambiente](https://archivo.contagioradio.com/colombia-es-el-pais-con-mas-asesinatos-de-ambientalistas-en-el-mundo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Seguidamente, se registraron varios estruendos que provenían de una vivienda a la que llegaron varios impactos de fusil de largo alcance y **fueron lanzadas dos granadas de las cuales una explotó causando la muerte de Sonia Lorena Bisbicus Ortíz** de 24 años y dejando heridas a tres personas; un hombre y dos menores indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta masacre se suma **el asesinato del también indígena Awá,** **James Canticuz**, que ocurrió el martes, es decir un día antes, también en el municipio de Barbacoas, Nariño; donde hombres armados lo interceptaron en una vía y luego de preguntar por su identidad, le propinaron 11 impactos de bala. **James dejó a 6 hijos menores de edad y a su esposa que se encuentra en estado de embarazo.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### «Exterminio» contra el pueblo indígena Awá

<!-- /wp:heading -->

<!-- wp:paragraph -->

Todos estos hechos de violencia han llevado a la Organización Nacional Indígena de Colombia -[ONIC](https://www.onic.org.co/)- y a la Unidad Indígena del pueblo Awá -[UNIPA](https://twitter.com/UNIPAcomunica)- a señalar que esta comunidad **está siendo víctima de un «exterminio» ante la inacción total del Estado.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1288513392433672195","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1288513392433672195

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Y es que según la UNIPA con los tres asesinatos registrados entre el 27 y 28 de julio, **la cifra de asesinados contra integrantes de la comunidad indígena Awá ascendió a 9 en apenas 4 meses.** (Lea también: [Tras asesinato de indígena, Pueblo Awá exige garantías en medio de las balas y el Covid-19](https://archivo.contagioradio.com/tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19/))

<!-- /wp:paragraph -->
