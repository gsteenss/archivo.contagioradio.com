Title: Con violencia no se acaba violencia: La historia de Colombia lo demuestra
Date: 2019-02-05 10:56
Author: AdminContagio
Category: DDHH, Nacional
Tags: Disidencias de las FARC, doctrina, Fuerzas militares, guerra
Slug: violencia-no-acaba-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-04-a-las-6.10.18-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @COL\_EJERCITO] 

###### [4 Feb 2019] 

Luego que las Fuerzas Armadas hicieran público el operativo que terminó con la muerte de **Edgar Mesías Salgado Aragón, alias "Rodrigo Cadete"** en Caquetá, el **director del Instituto de Estudios Sobre Paz y Desarrollo (INDEPAZ), Camilo Gonzalez Posso** afirmó que es una acción que se corresponde con la legitimación del Estado cuando se tienen retos armados, pero no debe ser la única respuesta a la violencia en los territorios.

El doctor en historia afirmó que se deben leer estas operaciones como un remanente de confrontaciones armadas que quedan en el país luego de más de 50 años en guerra, además, se circunscriben en la labor de la Fuerza Pública para garantizar la legitimidad del Estado en todo el territorio nacional. No obstante, Gonzalez Posso sostuvo que **el enfoque para acabar con la violencia no puede ser únicamente militar, y debe estar acompañado de estrategias integrales**.

El experto señaló que cuando un jefe de estos grupos es "neutralizado", la organización misma se ve afectada en la medida en que ve afectada su capacidad de organización, que en el caso de Cadete estaría relacionada con establecer estructuras de la disidencia en Putumayo y Caquetá. Sin embargo, Gonzalez resaltó que **las poblaciones requieren iniciativas más solidas por parte del Estado**, "porque el efecto es negativo si la única presencia del Estado es la militarización" para cumplir labores como la erradicación forzada o labores de guerra.

El analista e historiador recordó que la carta de presentación del Estado en muchos territorios ha sido siempre la captura o "neutralización" de objetivos de alto valor; pero luego de más de 50 años en guerra, nunca ha sido impactando en temas de fondo, como puede ser la inversión en lo social o generación de perspectivas de vida digna, **lo que tendría un efecto directo en la eliminación de dinámicas de violencia**.

### **No más doctrina de guerra interna** 

El experto declaró que la violencia que se está evidenciando en departamentos como Cauca, Nariño o en la región pacífica responde a una reacomodación de estructuras armadas que tras la salida de las FARC de los territorios, han comenzado una disputa por posiciones geográficas que les permitan ofertar seguridad para los distintos tráficos de la zona. (Le puede interesar: ["Líder social Jorge Castrillón Gutiérrez es asesinado al sur de Córdoba"](https://archivo.contagioradio.com/lider-jorge-castrillon-gutierrez-es-asesinado-al-sur-de-cordoba/))

En dichas zonas, y siguiendo la linea planteada sobre el caso de Cadete en Caquetá, el Estado debería cambiar de la doctrina de guerra interna y de contra insurgencia, "porque lo que corresponde ahora no es ganar la guerra sino hacer una transición para la implementación de la paz", en ese sentido, la doctrina de ahora debería tener como pilares la seguridad ciudadana, económica y jurídica por parte del Estado. (Le puede interesar:["Organizaciones sociales piden al papa que intervenga en negociación con el ELN"](https://archivo.contagioradio.com/papa-negociacion-eln/))

<iframe id="audio_32240124" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32240124_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
