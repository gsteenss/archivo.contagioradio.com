Title: La lluvia no impidió que marcharan en Bogotá contra la brutalidad policial
Date: 2015-02-25 17:59
Author: CtgAdm
Category: Movilización, Nacional
Tags: Brutalidad policial Colombia, campaña nacional contra la brutalidad policial Colombia, Desmonte ESMAD, víctimas ESMAD
Slug: la-lluvia-no-impidio-que-marcharan-en-bogota-contra-la-brutalidad-policial
Status: published

##### Entrevista a[ Sergio Ubillas]:  
<iframe src="http://www.ivoox.com/player_ek_4133062_2_1.html?data=lZaglZWado6ZmKiakpyJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLiysvS1dnFp8qZpJiSpJjSb8Tjz9nfw5DQpYzW09rhw9HNqMLYjNXczs7HrcLgjMrbjafTq9Dohqigh6aVcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### Entrevista a[ Oriana Galvis]: 

<iframe src="http://www.ivoox.com/player_ek_4133082_2_1.html?data=lZaglZWcdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRmoa3lIqupsjYrc7VjMnSjdHFb8Pm1tnOzs7IpcWf0dTZy8jNpc2fxtOYpNTLs9WZpJiSo5aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A pesar de las **fuertes lluvias cientos de personas marcharon por el centro de Bogotá en contra de la brutalidad policial y de la represión estatal** contra el derecho a la protestar y a la desobediencia pacífica y activa en Colombia.

Contagio Radio asistió a la marcha en la que confluyeron organizaciones sociales, estudiantiles y barriales, reunidas en la Campaña Nacional contra la brutalidad policial.

La marcha transcurrió desde el **Planetario Distrital hasta el lugar donde se encuentra la placa en memoria a Nicolás Neira**, estudiante asesinado el 1 de mayo de 2005 por el ESMAD, allí se se leyeron manifiestos en contra de la represión y las violaciones de derechos humanos en el marco de la protesta.

Asimismo la actividad continuo con actos culturales, entre ellos un mandala con la imagen de Nicolas Neira, realizado por el colectivo Whipalas, por otra parte estuvieron presentes las denuncias y las palabras de familiares de víctimas de la represión estatal, entre ellos el hermano de [Carlos Pedraza, líder social,  asesinado el mes de enero de 2015](https://archivo.contagioradio.com/ddhh/asesinado-defensor-de-derechos-humanos-carlos-pedraza/) y la madre de Jhonny Velasco Galvis, asesinado en las protestas de Suba en 2013

En el transcuso de la manifestación entrevistamos a Oriana Galvis, madre de Jhonny Galvis y a Sergio Ubillas, miembro de la **Campaña Nacional contra la brutalidad policial** en Colombia, quienes nos relatan su lucha para exigir justicia y los próximos objetivos que se propone la campaña.

\
