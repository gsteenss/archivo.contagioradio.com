Title: "Cese unilateral del ELN distensiona la mesa en La Habana" Luis Eduardo Celis
Date: 2018-05-15 10:35
Category: Paz, Política
Tags: cese unilateral, Elecciones presidenciales, ELN, Gobierno, Gustavo Bell, La Habana, proceso de paz
Slug: cese-unilateral-del-eln-distensiona-la-mesa-en-la-habana-luis-eduardo-celis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [15 May 2018]

El anuncio del ELN en donde manifestó que hará un cese unilateral a partir del 25 de mayo hasta el próximo 27 del mismo mes para garantizar las elecciones en primera vuelta  la presidencia en paz, es de acuerdo con el analista Luis Eduardo Celis, **una continuación de la expresión de la voluntad de esta guerrilla por llegar a buen puerto en la  mesa de La Habana**.

Para el analista, "todo lo que sea distensionar el conflicto es recibido de manera positiva", que a su vez, le quita un poco de presión a la mesa de cara a pactar un cese bilateral al fuego y continuar en la construcción del proceso de paz entre ambas partes. (Le puede interesar:["La iglesia no es neutral, le apuesta a la paz: Monseñor Monsalve"](https://archivo.contagioradio.com/avanzan-esfuerzos-para-recuperar-cuerpos-de-trabajadores-de/))

### **Los tiempos de la mesa entre el ELN y el Gobierno**

El próximo 27 de mayo se realizará la primera vuelta de elecciones para la presidencia de Colombia y luego de esto quedarán tan solo 6 semanas para la elección de un nuevo presidente. Sin embargo, de los candidatos solo dos de ellos, Gustavo Petro y Humberto De La Calle, han mencionado su voluntad por continuar con este diálogo.

Del otro lado, Fajardo a expresado la importancia de continuar pero con cambios en las reglas de juego, mientras que Iván Duque y Vargas Lleras han afirmado la clausura total de ese proceso.

Frente a esta situación, Luis Eduardo Celis señaló que si la mesa es capaz de demostrar su funcionamiento y efectividad, el siguiente gobierno, proveniente de cualquier corriente política deberá darle continuación, "**es importante que en este tiempo las dos partes demuestren un camino de trabajo en positivo**, en esa medida la  mesa tiene muchos retos, pero tiene ta una experiencia ganada".

De igual forma, aseguró que la sociedad con mayor acción debe acompañar el proceso, sobretodo aquellos departamentos que ya han presentado propuestas para el desescalonamiento del conflicto armado, como lo son **puntualmente el Chocó y la región del Catatumbo**.  Asimismo, afirmó que la sociedad tendrá un papel fundamental de garante en el momento que se pacte un cese bilateral y cuando se implemente el mecanismo de participación social.

<iframe id="audio_26010921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26010921_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
