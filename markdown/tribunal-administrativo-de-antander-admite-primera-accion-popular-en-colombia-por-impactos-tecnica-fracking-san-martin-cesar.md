Title: Tribunal Administrativo de Santander admite acción popular contra el fracking
Date: 2017-03-30 16:55
Category: Ambiente, Voces de la Tierra
Tags: fracking, san martin
Slug: tribunal-administrativo-de-antander-admite-primera-accion-popular-en-colombia-por-impactos-tecnica-fracking-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/San-Martin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País Vallenato 

###### [30 Mar 2017] 

[La falta de respuesta y acciones por parte del gobierno ante la inminente amenaza del fracking en San Martín, Cesar, propició que el Colectivo de Abogados Luis Carlos Pérez, CCALCP, junto a la Corporación Defensora del Agua, Territorio y Ecosistemas- CORDATEC, interpusieran una acción popular, que ya fue aceptada por el Tribunal Administrativo de Santander, **con el fin de que se suspendan las actividades que se adelantan para implementar el fracturamiento hidráulico en esa zona.**]

[La acción se hace en contra de entidades gubernamentales como la Agencia Nacional de Hidrocarburos, ANH; la Autoridad Nacional de Licencias Ambientales, ANLA; las empresas CONOCOPHILLIPS y CNE OIL & GAS; la Alcaldía de San Martín y la Corporación Regional del Cesar. De acuerdo con Julia Figueroa, abogada del CCALCP, la acción se interpone ante el Tribunal de Santander debido a que la posible realización de fracking, no solo afecta a San Martín, sino también **varios municipios de Santander, además de otras zonas del territorio colombiano.** [(Le puede interesar: Gobierno colombiano prepara dos nuevos bloques para fracking)](http://Reciba%20toda%20la%20información%20de%20Contagio%20Radio%20en%20su%20correo%20o%20escúchela%20de%20lunes%20a%20viernes%20de%208%20a%2010%20de%20la%20mañana%20en%20Otra%20Mirada%20por%20Contagio%20Radio.)]

[Se trata de una acción constitucional que ampara derechos colectivos, como el ambiente sano teniendo en cuenta que de practicarse fracking, se corre el riesgo de que se genere un desequilibrio a nivel ecológico al comprometerse las vertientes hídricas y con ello la salud de la población. Asimismo se basa en **la falta de moralidad administrativa por parte de las entidades estatales,** debido las omisiones de las autoridades pese a las advertencias de entes como la Contraloría General, además de la respuesta represiva del Estado, violando derechos humanos y civiles de quienes se oponen a esta técnica para extraer petróleo.]

[Un hecho a destacar es que, cuando la Magistrada Solange Blanco Villamizar del Tribunal Administrativo de Santander dispuso la admisión de la Acción Popular,  o**rdenó además vincular a la misma a la Corporación Autónoma Regional para la Defensa de la Meseta de Bucaramanga (CDMB), el municipio de Aguachica, en Cesar, y el municipio de Rionegro**, Santander.]

[“Con esta acción se pretende la suspensión inmediata de toda obra o actividad de explotación de petróleo mediante la técnica de fracturamiento hidraúlico o “Fracking” en Yacimientos No Convencionales que se pretenda realizar en el Pozo Picoplata 1, por parte de las empresas CONOCOPHILLIPS COLOMBIA VENTURES LTD SUCURSAL COLOMBIA y CNE OIL & GAS S.A., en el municipio de San Martín (Cesar), pero además, se busca abrir una puerta judicial que conlleve a la prohibición nacional de explotación de petróleo mediante la técnica Fracking”, dice el CCALCP.]

[Aunque es incierto saber cuándo habrá respuesta por parte del tribunal, las organizaciones demandantes, invitan  a participar de esta acción, **“presentando sus intervenciones, coadyuvancias y/o amicus dentro del trámite que se surte en el Tribunal Administrativo de Santander,** así como a adelantar acciones políticas de incidencia y movilización que permitan seguir denunciando las irregularidades que se presentan en la ejecución de contratos en Yacimientos No Convencionales y que sean determinantes para la protección de la vida, los recursos naturales y el medio ambiente no solo en San Martín, sino en toda Colombia”. [(Le puede interesar: Fracking amenaza abastecimiento de agua de Bogotá)](http://Reciba%20toda%20la%20información%20de%20Contagio%20Radio%20en%20su%20correo%20o%20escúchela%20de%20lunes%20a%20viernes%20de%208%20a%2010%20de%20la%20mañana%20en%20Otra%20Mirada%20por%20Contagio%20Radio.)]

<iframe id="audio_17871117" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17871117_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
