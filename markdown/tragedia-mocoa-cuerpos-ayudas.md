Title: Demoras en entrega de cuerpos otra tragedia en Mocoa
Date: 2017-04-04 17:04
Category: DDHH, Entrevistas
Tags: avalancha, Ayuda humanitaria, cuerpos, Mocoa
Slug: tragedia-mocoa-cuerpos-ayudas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/58e06df58eb26.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:El País 

###### 04 Abr 2017 

Para las víctimas de la avalancha ocurrida en Mocoa el pasado fin de semana la tragedia parece no terminar. Durante los últimos dos días se han conocido varias denuncias de **irregularidades que se viene presentando en la entrega de los cuerpos sin vida**, la administración de los albergues y ayudas humanitarias que están siendo aprovechadas por varias personas que no resultaron afectadas por la tragedia.

De acuerdo con Carlos Fernández, defensor de la Comisión Intereclesial de Justicia y Paz en el Putumayo, se han conocido denuncias que alertan de "**personas inescrupulosas y funerarias que estarían cobrando por entregar los cuerpos**" así como la restricción de entrada y salida a varios de los albergues dispuestos para acoger a quienes lo perdieron todo.

Puntualmente, el defensor de Derechos Humanos se refirió al caso reportado en la **Funeraria Capillas de paz de la ciudad de Pitalito**, donde un funcionario "pidió por lo menos 300 mil pesos" a los familiares para poder acceder a un ataúd y transporte fúnebre y que incluso otras personas aseguran les han llegado a pedir un millón de pesos, cuando desde el gobierno se había comprometido a asumir todos los gastos.

La situación, que se ha venido denunciando reiteradamente, es preocupante si se tiene en cuenta que **algunos cuerpos fueron identificados por sus familias desde el mismo día de la tragedia** pero no han logrado que se les haga entrega de los restos mortales. Una situación que teniendo en cuenta la alta cifra de personas fallecidas "se esta convirtiendo en un problema de salubridad pública" por el avanzado estado de descomposición.

En cuanto a la relación de fallecidos y desaparecidos el defensor asegura que las cifras vienen en aumento. La situación de los heridos es preocupante pues se han presentado inconvenientes con la atención de personas en Mocoa, Puerto Asis, Neiva y Popayán, poniendo como ejemplo el caso de **un comunero Nasa**, quien a pesar de haber sido arrastrado por la avalancha y presentar lesiones fuertes, **fue dado de alta por que según explico el hospital "tenían casos mucho más graves"**.

El registro de las zonas se continua haciendo aunque "**la preocupación sigue siendo que muchas de las personas afectadas no se encuentran en los albergues**, no están registradas y se esta tratando de establecer que pasó con ellos" manifiesta Fernández. Le puede interesar: [Corrupción también tuvo su parte de responsabilidad en avalancha de Mocoa](https://archivo.contagioradio.com/corrupciontambienfueculpabledeavalnchademocoa/).

Otra de las situaciones tiene que ver con la distribución de las ayudas humanitarias que llegan desde diferentes lugares, incluso las enviadas desde las Zonas Veredales de Concentración. "**La ayuda se esta centralizando, (...) no se ha permitido el paso de organizaciones humanitarias que han también las han venido recogiendo**", asegura el defensor refiriéndose a una denuncia sobre la intervención de la Unidad de Gestión del riesgo para el acceso de las agencias de Cooperación internacional a la zona.

El trabajo de la Comisión de Justicia y Paz en Putumayo, esta enfocado en lograr establecer los comuneros y comuneras del pueblo Nasa afectados. Hasta ahora tienen un registro de **más de 400 personas afectadas, 60% de ellas mujeres, 40 desaparecidas, 10 muertos** y varios integrantes que de acuerdo al reporte lograron sobrevivir y se encuentran en veredas cercanas a la ciudad de Mocoa.

<iframe id="audio_17962782" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17962782_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
