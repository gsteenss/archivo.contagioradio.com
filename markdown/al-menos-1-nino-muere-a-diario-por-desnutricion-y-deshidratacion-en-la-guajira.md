Title: Al menos 1 niño muere a diario por desnutrición y deshidratación en La Guajira
Date: 2015-10-05 15:01
Category: DDHH, Entrevistas
Tags: Desalojos por parte del Cerrejón, Desnutrición en La Guajira, El Cerrejón en La Guajira, Extracción carbonífera en La Guajira, la Gran Minería Envenena Eres Tú quien la frena, Mesa de Seguimiento y Solidaridad con La Guajira, Muerte de niños en La Guajira, Tribunal Popular contra transnacionales por la Guajira
Slug: al-menos-1-nino-muere-a-diario-por-desnutricion-y-deshidratacion-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Niños-Wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: scp.com.co] 

<iframe src="http://www.ivoox.com/player_ek_8804379_2_1.html?data=mZ2dlpibfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhc2fzsrb0diPdYziyoqwlYqmddCfztrS1MqPpYzYysbfy9SPtNDmjMnS1dPZuNPdxM6SpZiJhpTijN6YxsrXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gustavo Gallardo, Coordinador] 

###### [05 Oct 2015] 

[Entre las afectaciones a la salud, los daños al medio ambiente, la vulneración de derechos laborales y los desplazamientos de familias causados por la actividad extractiva del carbón en La Guajira, la **muerte diaria de entre 1 y 3 niños por desnutrición y deshidratación resulta ser el efecto más perverso**, así lo concluyó el *Tribunal Popular Contra las Transnacionales* llevado a cabo en Rioacha el pasado fin de semana.   ]

[Para la extracción de carbón El Cerrejón usa y contamina los escasos afluentes hídricos presentes en este departamento, de acuerdo a la campaña *La Gran Minería Envenena Eres Tú quien la frena*, “**el agua que consume El Cerrejón en un día equivale al agua que consumen los habitantes de La Guajira (900.000 personas) en un mes**” lo que afecta principalmente a la población infantil cuya desnutrición y deshidratación son las principales causas de muerte.]

[Gustavo Gallardo, miembro del equipo coordinador del Tribunal, afirma que aproximadamente 500 pobladores del alta, media y baja Guajira se concentraron en Riohacha para diseñar propuestas de orden social, jurídico y político que permitan hacer frente a los impactos del extractivismo en la región y reafirmar la **necesidad de suspender las concesiones de El Cerrejón para la explotación del carbón**.  ]

[Ante los magistrados populares del Tribunal fueron denunciados los **desplazamientos** de poblaciones enteras para la extracción de carbón, entre las que se destaca la de la comunidad de la Roche, **cerca de 300 familias a las que les fueron destruidas sus viviendas** y de las que **tan sólo 10 continúan en resistencia para no ser despojadas. **]

[Según las pruebas presentadas en el Tribunal, las **comunidades son desalojadas con falsas promesas por parte del Cerrejón**, quien les asegura reasentamientos en condiciones dignas de vivienda. Sin embargo, lo cierto es que estas poblaciones deben enfrentarse a vivir en territorios distintos a los que han habitado ancestralmente, **sin garantías de vida digna y sumidas en la desatención estatal**. ]

[Para enfrentar la situación, se planteó la creación de la **Mesa de Seguimiento y Solidaridad con La Guajira**, integrada por organizaciones nacionales e internacionales, que hará el seguimiento a las propuestas asumidas por los participantes y brindará **permanente acompañamiento jurídico, psicosocial y en salud a las comunidades afectadas**.]

[Gallardo asegura que desde el Tribunal se proyecta un **debate de control político** en torno a las afectaciones de la explotación carbonífera en La Guajira, en el que se presentará la compilación y sistematización de la problemática con el fin de entablar **acciones jurídicas para frenar la “maquina asesina de la extracción del carbón”**, extendiendo a su vez la invitación a diversas organizaciones para articular “la lucha por la vida, el agua y la niñez de La Guajira”. (Ver también: ][[El Cerrejón en la mira del tribunal popular contra las transnacionales en La Guajira](https://archivo.contagioradio.com/se-realizara-tribunal-popular-contra-las-transnacionales-en-la-guajira/))]
