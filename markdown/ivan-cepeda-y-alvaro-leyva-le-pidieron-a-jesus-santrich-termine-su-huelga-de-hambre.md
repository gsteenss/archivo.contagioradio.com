Title: Piden a Jesús Santrich que abandone la huelga de hambre y siga trabajando por la paz
Date: 2018-05-08 14:55
Category: Paz, Política
Tags: Huelga de hambre, Iván Cepeda, Jesús Santrich, proceso de paz
Slug: ivan-cepeda-y-alvaro-leyva-le-pidieron-a-jesus-santrich-termine-su-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SANTRICH.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 May 2018] 

Por medio de una carta, el senador del Polo Democrático Iván Cepeda y el ex negociador de paz Álvaro Leyva, le pidieron al integrante del partido Fuerza Revolucionario del Común, Jesús Santrich, que **de por terminada la huelga de hambre** que adelanta desde hace un mes teniendo de presente sus condiciones de salud. La petición la hicieron aludiendo a la difícil situación que atraviesa la implementación de lo acordado en la Habana.

Jesús Santrich, a quien la justicia en Estados Unidos le abrió un proceso **por el delito de narcotráfico** por el cual se encuentra detenido en una cárcel de Bogotá, completa un mes de huelga de hambre. Con esta huelga busca rechazar dichas acusaciones y diferentes voces han pedido que su proceso de investigación se lleve a cabo en Colombia.

### **“Viva para construir más paz”** 

En la carta, tanto Cepeda como Leyva manifestaron su preocupación “en momentos en que usted, **pieza clave de la construcción de los Acuerdos de la Habana**, pone en vilo el valor de su mismísima existencia por considerar que sido ofendido moralmente con una acusación que lo destruye por ser quien es y que niega su andar de años por caminos de búsqueda de la paz nacional”.

En diálogo con Contagio Radio, el senador Iván Cepeda manifestó que cada día que pasa de la huelga de hambre, **se debilita la situación de salud** “que ya de por si es una condición precaria” teniendo en cuenta la huelga de hambre que realizó hace algunos meses para protestar por los prisioneros políticos que no han salido de las cárceles como lo establece la Ley de Amnistía. (Le puede interesar:["Pruebas contra Jesús Santrich son "ridículas": Gustavo Gallón"](https://archivo.contagioradio.com/pruebas-contra-santrich-son-ridiculas-gustavo-gallon/))

### **Aporte a la paz de Santrich es fundamental** 

Cepeda afirmó que es necesario reconocer el aporte que ha hecho Jesús Santrich al Acuerdo de Paz. En la carta, manifestaron que fue Santrich quien “llamó a los suyos a cumplir y **hacer realidad los seis puntos del Acuerdo de la Habana** y el quehacer político en democracia con el compromiso de acogerse al Sistema Integral de Justicia, Verdad, Reparación y No Repetición”.

De igual forma, indicó que es necesario que el caso de Santrich sea tratado en Colombia en principio por la **Jurisdicción Especial para la Paz.** Recordó que la figura de la extradición “no brinda ninguna clase de garantías para que su derecho al debido proceso, a la defensa y a un juicio justo se resperte en los estrados judiciales en Estados Unidos”.

Finalmente enfatizó en el que el Acuerdo de Paz en Colombia “no puede depender de lo que una agencia estadounidense **nos diga con relación a lo que supuestamente sabe** o lo que supuestamente ha armado como proceso en contra de personas que son fundamentales en el proceso”argumentando que la detención de Santrich “puede llevar una inestabilidad muy grande del proceso de paz”.

<iframe id="audio_25839503" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25839503_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
