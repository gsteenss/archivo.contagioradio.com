Title: Piedad Córdoba propone celebrar firma al Cese Bilateral con repique de Campanas
Date: 2016-06-22 11:52
Category: Nacional, Paz
Tags: Cece Bilateral, El último de la guerra
Slug: piedad-corodoba-propone-celebrar-firma-al-cece-bilateral-con-repique-de-campanas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Piedad-Cordoba-e1471357042411.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: NoticiasRPTV 

###### [22 Jun 2016]

La vocera de la plataforma política Colombianas y Colombianos por la Paz, Piedad Córdoba, envió una carta dirigida al Monseñor Luis Augusto Castro Quiroga, presidente la Conferencia Episcopal, en donde propone que el 23 de junio, sobre el medio día se pueda **escuchar el repique de las campanas de las iglesias en Colombia, celebrando la firma del Cese Bilateral del Fuego entre el Gobierno y la guerrilla de las FARC-EP.**

En la carta, Piedad Córdoba expone que luego de [4 años de diálogos entre las partes negociadoras que el día de mañana se pueda dar la firma del Cese Bilateral se constituye](https://archivo.contagioradio.com/manana-se-firmara-acuerdo-sobre-cese-al-fuego-bilateral/) como un hecho histórico en el país, ya que "abre las puertas al Cese de las confrontaciones en los lugares epicentros de la guerra y por ende, abre la posibilidad de una definitiva firma del Acuerdo General para la Terminación del Conflicto".

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/270386613&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false" width="100%" height="166" frameborder="no" scrolling="no"></iframe>

### Texto Oficial de la Carta 

*Monseñor*  
*Luis Augusto Castro Quiroga*  
*Presidente de la Conferencia Episcopal*  
*Bogotá, D.C*

*Respetuoso saludo.*  
*Luego de casi cuatro años de proceso de paz entre el gobierno nacional y la guerrilla de las FARC-ep, el día jueves 23 de junio del presente año se hará pública la firma del CESE Bilateral del Fuego, este hecho constituye un hito histórico en nuestro país ya que **abre las puertas al cese de las confrontaciones armadas en los lugares epicentros de la guerra** y por ende abre la posibilidad de una definitiva firma del Acuerdo General para la Terminación del Conflicto, que sumado a los demás aspectos de la c**onstrucción de la paz estable y duradera regocijará los corazones de los colombianos que empezaran a creer que el diálogo es la mejor ruta para la resolución de conflictos.***

*Como colombianas y Colombianos por la Paz sabemos y hemos sido testigos de la voluntad de la iglesia católica y de sus esfuerzos para que en el país se logre superar el capítulo de la guerra y la violencia, así como de sus esfuerzos por rodear el actual proceso de paz para que llegue a buen término, por esta razón queremos proponerle que en todo el país el día de la firma del Cese Bilateral, **jueves 23 de junio a medio día se pueda escuchar el repique de las campanas de todas las iglesias**, como símbolo de júbilo por esa gran noticia para todo el país. Creemos que de esta manera se podrá **colmar de fe y esperanza los corazones de la gente que ha creído en el proceso de paz, y también de los que aun desconfían del proceso.***

*Esperando contar con su gran apoyo para esta iniciativa*

*Atentamente,*

*Piedad Córdoba Ruíz*  
*Vocera Colombianas y colombianos por la Paz*

*Con copia: padre Darío Echeverri, secretario de la Comisión de Reconciliación.*

Mañana sobre las **11 de la mañana se realizará la transmisión en vivo del evento de la firma al Cede Bilateral, en cabeza del presidente Santos en la avenida Jimenez con séptima**.  A la ceremonia contará con la participación de líderes internacionales como Ban Ki-Moon, la presidente de Chile Michelle Bachelet, el canciller noruego Borge Brende, entre otras personalidades.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
