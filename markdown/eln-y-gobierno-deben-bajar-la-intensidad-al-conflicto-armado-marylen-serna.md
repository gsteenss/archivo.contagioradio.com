Title: Mesa en Quito necesita acciones para desescalar el conflicto armado
Date: 2018-02-08 15:10
Category: Nacional, Paz
Tags: ELN, Gobierno, Marylen Serna, Mesa Social para la paz., Proceso de paz en Quito
Slug: eln-y-gobierno-deben-bajar-la-intensidad-al-conflicto-armado-marylen-serna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [08 Feb 2018] 

Una delegación de organizaciones de la ciudadanía que apoya en el proceso de paz en Quito, se reunió con el presidente Juan Manuel Santos, el Alto Comisionado para la Paz, Rodrígo Rivera y Gustavo Bell, jefe del equipo negociador, para plantear una propuesta que permita destrabar la mesa de diálogos, desescalar **las acciones armadas por parte de ambos bandos, y recuperar la credibilidad en este escenario**. Esta misma actividad se llevará acabo con un equipo del ELN.

De acuerdo con Marylen Serna, vocera de Congreso de los Pueblos, el equipo de diálogo desde el gobierno tuvo una disposición para escuchar a las organizaciones y **se planteó un objetivo común: alcanzar la paz**.

### **La propuesta para el gobierno y el ELN** 

Durante el diálogo con el equipo de gobierno las organizaciones civiles plantearon dos temas importantes  con las que buscan  aportar  a la construcción de este proceso de paz: **las garantías para la vida de los líderes sociales y desescalar el conflicto.**

Sobre la primera, el equipo del gobierno respondió que se están haciendo todos los esfuerzos para proteger la vida de las comunidades y de sus líderes, sin embargo, para Marylen Serna, las medidas que se están tomando desde las instituciones “no están siendo efectivas **para prevenir las acciones de grupos armados que están golpeando los territorios**”.

El segundo tema que tiene que ver con la necesidad de bajar la intensidad a la confrontación militar, el equipo de organizaciones civiles propuso que tanto el gobierno como el ELN, generen gestos y compromisos para superar la situación actual. De la misma manera, resaltaron la importancia de que, una vez se retorne a la mesa, el primer paso sea evaluar el cese bilateral. (Le puede interesar: ["Proceso de paz con el ELN es un bien público y debe retomarse: Organizaciones sociales"](https://archivo.contagioradio.com/proceso-de-paz-con-el-eln-es-un-bien-publico-y-debe-retomarse-movimientos-sociales/))

### **El paro armado anunciado por el ELN** 

El Ejército de Liberación Nacional anunció en un comunicado de prensa que a partir del próximo 10 de febrero hasta el 13 del mismo mes, iniciará un paro armado. Serna asegura que las organizaciones sociales que acompañan la mesa con esa guerrilla "rechazan todos los actos que tengan que ver con violencia y que afecten a la población”, y agregó que esa declaratoria del ELN debe ser una razón más para insistir y retomar la mesa de negociación.

“Es una demostración de fuerza que hacen todos los actores que están en una mesa y en un momento de suspensión de diálogos, sentimos que es difícil de entender pero que al mismo tiempo hace parte de la lógica de la guerra” afirmó la vocera del Congreso de los Pueblos. (Le puede interesar: ["Paro armado del ELN tensiona más el ambiente para la mesa en Quito": Luis Eduardo Celis"](https://archivo.contagioradio.com/paro_armado_mesa_quito_eln_gobierno/))

<iframe id="audio_23642720" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23642720_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radi](http://bit.ly/1ICYhVU)o 
