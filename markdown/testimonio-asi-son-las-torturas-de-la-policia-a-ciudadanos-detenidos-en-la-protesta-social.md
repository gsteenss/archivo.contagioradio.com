Title: Testimonio: Así son las torturas de la Policía a ciudadanos detenidos en la protesta social.
Date: 2020-09-23 08:03
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #CAI, #ESMAD, #Policía, #Tortura
Slug: testimonio-asi-son-las-torturas-de-la-policia-a-ciudadanos-detenidos-en-la-protesta-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Represión-de-la-policía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En el marco de las capturas, asesinatos y violaciones a derechos humanos denunciadas entre el 9 al 11 de septiembre, se encuentra la de Carlos, **quien fue torturado** en reiteradas ocasiones por agentes de la Policía Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*El nombre de esta persona no será expuesto debido al miedo por las represalias que puedan existir en su contra por parte de la Fuerza Pública. Razón por la cual será llamado Carlos.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carlos fue capturado, de forma arbitraria, el 9 de septiembre cuando según su relato; tras ver en noticias de un plantón pacífico en contra de la brutalidad policial, se acerca al CAI Galán, en la localidad de Kennedy.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esos primeros momentos, Carlos asegura que la movilización fue dispersada violentamente sobre las 9:30 de la noche, por parte de integrantes del ESMAD. Su captura se produce, a las 9:40, cuando intenta escapar de ella en su bicicleta. Sin embargo, un agente, de forma violenta lo baja de la misma y lo trasladan hasta el CAI Galán.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la denuncia, integrantes de la Policía lo botaron al suelo y le propinaron varios golpes. Entre esos una bofetada en su oído izquierdo provocando un sangrado interno, que actualmente lo tiene con menos del **20% de audición.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Traslado a la URI de Puente Aranda
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Carlos afirma que luego de esa golpiza, fue trasladado junto con otras personas, en un CAI móvil, hacia las instalaciones de la URI de Puente Aranda. Allí es esposado junto a otros hombres y mujeres en la cancha que hay esas instalaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí, nuevamente es víctima de una golpiza por integrantes de la Policía, hasta que uno de los comandantes ordena que frenen esas acciones. Las mujeres esposadas, posteriormente fueron golpeadas por agentes mujeres. (Le pude interesar: "[Agresiones de la fuerza pública contra la protesta social han sido sistemáticas: Corte Suprema](https://archivo.contagioradio.com/corte-suprema-ordena-a-min-defensa-ofrecer-disculpas-por-exceso-en-el-paro-nacional/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia de Carlos, uno de los comportamientos sistemáticos durante esas golpizas, consistió en no propinar golpes al rostro de manera frontal, sino todos ellos hacia el cuerpo y los oídos. Asimismo, señala que los integrantes de la Fuerza Pública reiteraron que estas acciones eran producto de las protestas en los distintos CAIS en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Continúa la tortura en traslado a la Fiscalía
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Carlos manifiesta que después de estar en este lugar, fue trasladado a la Fiscalía, en donde le informan que debía dejar sus pertenencias. No obstante no hay ningún lugar en donde pueda guardarlas y es obligado a dejar su chaqueta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Enseguida es revictimizado por Medicina Legal. En este lugar, solamente le revisan heridas externas, porque según la persona que hace el examen, en esta institución solo se tienen en cuenta los "golpes visibles". (Le puede interesar: ["Agresiones a defensoras de J&P y periodista de Contagio Radio"](https://www.justiciaypazcolombia.com/agresiones-a-defensoras-de-jp-y-periodista-de-contagio-radio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, luego de haber estado despierto durante más de 24 horas, Carlos asegura que intentó recostarse en el piso; sin embargo, un agente le manifestó que no podía dormir y que estaría de pie todo el resto de día.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Instantes después, es trasladado a la cárcel que se encuentra en la Fiscalía. Allí, tras la requisa de rutina, es llevado por dos agentes a lo que Carlos identifica como un baño, en donde una vez más; **recibe otra golpiza por parte de los integrantes de la Fuerza Pública.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Montaje judicial de la Policía Nacional
---------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente Carlos es trasladado a los Juzgados de Paloquemao, en donde en medio de una audiencia se **dan testimonios de tres polícias**. Uno de ellos afirma haber visto a Carlos incendiar el CAI y dos de ellos apoyar la labor de detención de personas defensa del lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, las aseveraciones del policía, que asegura haberlo visto; manifiesta que la hora en la que Carlos ayudó a incendiar el CAI, fue a las 11 pm, pero su captura se produjo dos horas antes. Además, según la defensa, no existen pruebas de que ese CAI si hubiese sido incinerado. Los otros dos relatos de los agentes, no señalan ni haber visto a Carlos en medio de los hechos, ni hacer las acciones señaladas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente, Carlos tiene abierto un proceso judicial por los delitos de **violencia contra servidor público y daño en bien ajeno con agravante de bien público.**

<!-- /wp:paragraph -->
