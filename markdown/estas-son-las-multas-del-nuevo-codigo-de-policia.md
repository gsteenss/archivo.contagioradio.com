Title: Estas serían las multas del nuevo Código de Policía
Date: 2015-06-25 16:54
Category: DDHH, infografia
Tags: abuso de autoridad, código de policía, Congreso de la República, multas
Slug: estas-son-las-multas-del-nuevo-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/paromedellin-017.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [  
](https://archivo.contagioradio.com/estas-son-las-multas-del-nuevo-codigo-de-policia/multas-codigo-de-policia-2/)Foto: [[abusodelafuerzapublica.blogspot.com] 

En el Congreso de la República se debate el nuevo Código de Policía,  que ha generado bastante polémica debido a que diversos sectores de la sociedad denuncian que se trata de un Código de Policía autoritario y dictatorial, por medio del cual las autoridades podrían **[abusar de su poder.]**

**[Cabe recordar que con el nuevo código, que muchos califican como]**un **[“régimen dictatorial”]**, se criminaliza la protesta social, taponando las vías legales para impedir las manifestaciones que promueven la exigencia de derechos de los colombianos y las colombianas, entre otras medidas. [(Ver nota: Nuevo Código de Policía es dictatorial y viola DDHH: Alberto Yepes )](https://archivo.contagioradio.com/nuevo-codigo-de-policia-es-dictatorial-y-viola-ddhh-alberto-yepes/)

[![multas-codigo-de-policia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/multas-codigo-de-policia2.png){.aligncenter .size-full .wp-image-10513 width="1000" height="1167"}](https://archivo.contagioradio.com/estas-son-las-multas-del-nuevo-codigo-de-policia/multas-codigo-de-policia-3/)
