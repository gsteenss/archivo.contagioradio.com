Title: Por crisis el HUV reduce prestación de servicios en un 40%
Date: 2015-10-02 13:34
Category: DDHH, Nacional
Tags: crisis de la salud, Crisis Hospital Universitario del Valle, EPS's, Ministerio de Salud, Universidad del Valle, Valle del Cauca
Slug: por-crisis-el-huv-reduce-prestacion-de-servicios-al-40
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/HUV.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carlos Rocha 

###### [2 oct 2015] 

La situación del Hospital Universitario del Valle (HUV) **continúa siendo crítica**, recortes presupuestales, falta de insumos para prestar  servicios y un colapso en la red hospitalaria del Valle son algunas de los problemas que ha generado la iliquidez y la reducción de la planta de trabajadores.

El HUV es el **único** hospital público de tercer nivel en el Valle y con los **recortes de presupuesto** se ha visto obligado a reducir las camas en cuidados intensivos, neurología, además que presenta una falta de insumos para hematooncología, la capacidad del hospital “*está reducida en un 40%, lo que quiere decir que se ha dejado de atender mucha gente*”, indica indica José Serna médico residente del HUV y estudiante de la Universidad del Valle

Serna indica que “*La* ***situación económica del hospital es de 0 pesos*”**  y que para solucionar esta crisis se ha trabajado con diferentes estancias departamentales y nacionales para conseguir recursos, sin embargo la deuda es grande, [las EPS's le adeudan cerca de 192 mil millones de pesos al hospital.](https://archivo.contagioradio.com/epss-deben-al-hospital-universitario-del-valle-192-mil-millones-de-pesos/)

Serna indica que en las próximas semanas se **ejecutará un plan de choque** en el que el departamento ha participado para salvar el hospital, sin embargo, resalta que el gobierno nacional frente al desabastecimiento de recursos y servicios que existe debería declarar una “***emergencia sanitaria***”.

Como estudiante de la Univalle, **Serna indica que la participación del movimiento estudiantil y de la comunidad** **han sido un gran apoyo**, que con distintas actividades como movilizaciones y plantones se han solidarizado con esta crisis. Desde hace 3 semanas la [Universidad del Valle se encuentra en cese de actividades para defender el hospital](https://archivo.contagioradio.com/estudiantes-se-suman-a-la-defensa-del-hospital-universitario-del-valle/).

José Serna aclara que el tema del HUV “*es un problema para solucionar el tema de recursos, pero también es un tema de reestructuración al interior del hospital*”. Esta es una crisis que nos compete a todos porque “*recordemos que todos somos pacientes*” aseveró.
