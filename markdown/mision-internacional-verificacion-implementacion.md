Title: Proceso de implementación afronta serias dificultades: Misión internacional de Verificación
Date: 2017-10-30 16:36
Category: Entrevistas, Paz
Tags: acuerdos de paz, FARC, Gobierno
Slug: mision-internacional-verificacion-implementacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/censo-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Oct 2017] 

Las organizaciones PBI, Mundubat y Oidhaco, se encuentran realizando en Colombia una misión internacional de verificación para analizar cómo va la implementación de los Acuerdos de Paz. Sin embargo, en lo que llevan de recorrido, la misión ya ha notado serias dificultades en torno a los puntos sobre **participación política, el fin del conflicto y el punto 5 sobre garantías para las víctimas del conflicto armado**.

De acuerdo con Garbiñe Biurrum, Magistrada de la Corte Suprema del País Vasco e integrante de esta misión, el objetivo es contribuir al esfuerzo que se realiza desde la comunidad internacional en **el seguimiento, impulso y verificación de los Acuerdos de paz entre el gobierno y las FARC**. (Le puede interesar: ["Dura carta de las FARC al gobierno por asesinatos de integrantes de ese partido político"](https://archivo.contagioradio.com/farc_carta_presidente_juan_manuel_santos/))

### **Las preocupaciones sobre la participación política** 

Hasta el momento, en lo que ha recorrido la misión, una de las grandes preocupaciones que ha salido es la participación política y con ella la imposibilidad de lograr una apertura democrática **para construir la paz y la garantía de derechos y garantías plenas para el ejercicio de la oposición política**.

En ese sentido la misión tendrá que verificar cómo se están estableciendo los mecanismos de democráticos de participación y particularmente cómo se están gestando los de las mujeres que de acuerdo con Biurrum “son claves para la implementación y desarrollo de los acuerdos”. (Le pude interesar:["ELN y FARC le están apostando a la paz: Víctor de Currea"](https://archivo.contagioradio.com/eln-y-farc-quito/))

### **El punto 3 el fin del conflicto** 

De igual forma, la misión ha manifestado que el punto tres sobre el fin del conflicto es otra de las tareas a revisar dentro de la implementación de los acuerdos de paz, sobre todo el proceso de reincorporación de los ex milicianos de las FARC, en términos económicos, culturales, sociales y políticos hacia una vida civil.

En esa vía la misión verificara el avance o no, de las zonas territoriales de capacitación, en donde hasta el momento han establecido que se “está produciendo un abandono bastante importante de integrantes de las FARC” que podría ser producto de problemas **como la falta de respuesta económica por parte del gobierno hacia la financiación de estos lugares**.

En este mismo punto, también se ha manifestado una alarma por la implementación del acuerdo de garantías de seguridad y **lucha contra las organizaciones y conductas criminales que son responsables de masacres que todavía se están registrando en el país**.

### **El punto 5 y las víctimas del conflicto armado** 

Así mismo el punto 5 sobre las víctimas ha significado una preocupación para la misión, en términos de la implementación del Sistema **Integral de Justicia, Verdad, Reparación y no Repetición, en el que aún no se ha avanzado**.

Los integrantes de esta misión se reunirán con sectores tanto estatales como de las FARC, organizaciones civiles y visitarán los territorios, para tener un panorama mucho más completo de la implementación de los Acuerdos de paz.

La misión dará a conocer los resultados de las observaciones que realicen la próxima semana, a través de una rueda de prensa a las 9 de la mañana en la sede del Cajar, en que publicarán las recomendaciones pertinentes tanto al Estado como a las FARC para que se avance en la implementación de los acuerdos de paz, de igual forma se presentará en Bruselas, en el mes de noviembre.

<iframe id="audio_21778236" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21778236_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
