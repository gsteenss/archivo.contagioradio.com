Title: Preservar los documentos del DAS para esclarecer la verdad y hacer justicia
Date: 2019-01-31 18:59
Author: AdminContagio
Category: Nacional, Paz
Slug: documentos-das
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título1-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ámbito Jurídico] 

###### [31 Ene 2019] 

Este miércoles, la Jurisdicción Especial para la Paz (JEP) desarrolló una audiencia para inaugurar la mesa técnica que tendría como función definir el manejo que se dará a los documentos del DAS que reposan en el Archivo General de la Nación, y que pasarán a ser elementos de estudio por parte del Tribunal. Durante la diligencia, magistrados que integran la Jurisdicción escucharon opiniones y pedidos de organizaciones sociales y defensoras de derechos humanos sobre estos archivos.

La abogada del Colectivo de Abogados José Alvear Restrepo, Jomary Ortegón, explicó que la diligencia que se adelantó en la JEP sirvió para que organizaciones defensoras de derechos humanos aportaran en la construcción de criterios para preservar archivos de inteligencia y contrainteligencia del DAS. Actualmente, dichos documentos cuentan con una medida cautelar de protección para evitar su pérdida, pero muy pocas personas han podido acceder a ellos

Ortegón recordó que gracias a diferentes investigaciones judiciales como de prensa se estableció que el Departamento Administrativo de Seguridad (DAS) participó en hechos de tortura en la década de los 80\`s, esta relacionado con grandes magnicidios como el de Jaime Garzón o Bernardo Jaramillo en los 90\`s y estuvo involucrado en casos de espionaje a gran escala en la última década.

[Estos hechos, explicó la abogada, convierten los archivos de DAS en materia de derechos humanos a los cuales se debe poder acceder, convirtiéndolos en documentos patrimonio de la sociedad colombiana. Al convertir estos archivos en material de derechos humanos, se reconocería la historia de violación a los derechos humanos que allí se evidencia.]

De igual forma, la declaratoria obligaría a hacer una conservación cuidadosa de estos archivos, al tiempo que eliminaría las reserva sobre los mismos, de forma tal que la JEP podría acceder a información de inteligencia y contra inteligencia, lo que permitiría establecer las formas en que operó el DAS para cometer crímenes como los anteriormente mencionados.

No obstante Ortegón reconoció que podría haber información susceptible para la seguridad nacional así como identidades de personas que es necesario ocultar, pero aclaró que para no comprometer ambas informaciones existen mecanismos actuales de protección.

Por su parte, la oficina de derechos humanos de la Organización de las Naciones Unidas (ONU) en Colombia, representada por Alberto Brunori, expresó su preocupación ante la posibilidad de que los archivos guardas sean destruidos, atentando contra el derecho a la verdad de las víctimas.

> ?|| [\#COMUNICADO](https://twitter.com/hashtag/COMUNICADO?src=hash&ref_src=twsrc%5Etfw)[@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw) instaló Mesa Técnica para verificar condiciones de preservación y acceso a los archivos del [\#DAS](https://twitter.com/hashtag/DAS?src=hash&ref_src=twsrc%5Etfw). [pic.twitter.com/7HtZE4BAe4](https://t.co/7HtZE4BAe4)
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [30 de enero de 2019](https://twitter.com/JEP_Colombia/status/1090746219536748546?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
