Title: Las cinco modificaciones que tendría incluído el proyecto Metro de Bogotá
Date: 2019-07-21 14:43
Author: CtgAdm
Category: Nacional, Política
Tags: Concejo de Bogotá, Licitación, Manuel Sarmiento, Metro
Slug: modificaciones-proyecto-metro-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Metro-de-Bogotá-e1563738060935.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: bogota.gov.vo  
] 

La bancada de oposición del Concejo de Bogotá presentó nuevos documentos, en medio de la demanda contra la licitación del metro de Bogotá, que probarían una serie de irregularidades durante el proceso. Los documentos evidenciarían el aumento de costos en la ejecución del Proyecto, entre otros temas, cumpliendo con la advertencia que la misma oposición había hecho sobre el documento CONPES aprobado para desarrollar el metro de Bogotá. (Le puede interesar:["Irregularidades y mentiras sobre el Proyecto  Metro de Bogotá"](https://archivo.contagioradio.com/mentiras-metro-bogota/))

### **¿Un CONPES aprobado sin los documentos necesarios?**

En uno de los[debates](https://archivo.contagioradio.com/metro-elevado-corrupcion-sistemica/) sobre el proyecto Metro en el Concejo de Bogotá, Hollman Morris, integrante de la bancada de oposición denunció que la alcaldía de Enrique **Peñalosa había modificado el documento que viabilizaba el metro subterráneo en la cláusula de alcance y estudio.** Segun lo denunció el Concejal, se usarían los mismos estudios para hacer el metro elevado y su costo se elevaría de 12 billones de pesos (de acuerdo a lo prometido por la Alcaldía) a entre 27 y 30 billones.

Posteriormente, Manuel Sarmiento, también integrante de la oposición en el Concejo instauró una tutela en la que afirmaba que el Proyecto estaría violando el principio de planeación al incumplir en los estudios de factibilidad. En esta acción judicial, Sarmiento señaló que el Proyecto Metro se habría aprobado sin que se presentaran los estudios finales de factibilidad, es decir, con los estudios preliminares, lo que podría conducir a actos de corrupción. (Le puede interesar:["Concejales piden medidas cautelares para evitar corrupción en Metro de Peñalosa"](https://archivo.contagioradio.com/evitar-corrupcion-metro-penalosa/))

### **Las modificaciones que se habrían hecho al proyecto metro de Bogotá  
**

Tras la acción judicial, Concejales por el Polo pidieron medidas cautelares ante el Consejo de Estado para que suspenda la licitación del metro de Bogotá. A esas medidas se sumaron los estudios de costos de inversión y operación de la primera línea del metro, que comprobaría las advertencias hechas en el pasado sobre los resultados de licitar un Proyecto sin los estudios de factibilidad que se requieren; Sarmiento sostuvo que es posible decir que **se está licitando un metro con características distintas a las que fueron aprobadas en el CONPES** que dió su viabilidad.

Entre los cambios que se realizan en la licitación, respecto a lo expuesto en el CONPES se incluyen **un aumento de más de 800 mil millones de pesos por concepto de riesgos del proyecto**; el aumento de una estación, pasando de 15 a 16, pero elevando el costo en más de 400 mil millones de pesos; aumentar "los parámetros de cimentación de pilas o pilotes, pero se redujo en un 42% (\$655.000 millones) el presupuesto para hacerlo".

Adicionalmente, según lo denunciado por Sarmiento, **se redujo de 195 a 39 km la distancia de vías destinadas para el desvío del tráfico,** necesario al momento de realizar las obras. A ello se suma que se redujo en más de la mitad (pasando de 650 a 300 mil pasajeros diarios) la capacidad de transporte, entre el documento con el que se solicitó el préstamo al Banco Interamericano de Desarrollo (BID) y el estudio del CONPES.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
