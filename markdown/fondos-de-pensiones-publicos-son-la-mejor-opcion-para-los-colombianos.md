Title: Fondos de pensiones públicos son la mejor opción para los colombianos
Date: 2020-03-05 15:11
Author: AdminContagio
Category: Actualidad, Entrevistas
Tags: Colpensiones, economia, Ernesto Macías, Fondos de pensión
Slug: fondos-de-pensiones-publicos-son-la-mejor-opcion-para-los-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Fondos-de-pensiones.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/alejandra-trujillo-integrante-fescol-experta-en_md_48622305_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Alejandra Trujillo, integrante de Fescol

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

En las últimas semanas el expresidente del Congreso y actual senador Ernesto Macías fue noticia por un traslado 'expres' que hizo, de un fondo de privado de pensiones a Colpensiones, el fondo público. Para expertos en materia de pensiones, el traslado es más que justificado, teniendo en cuenta que mientras unos fondos son manejados como negocio con Colpensiones se asegura un sustento básico al concluir la vida laboral.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo básico para entender los dos sistemas de pensiones**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La abogada Alejandra Trujillo, integrante de Fescol y experta en el tema pensional, explica que en Colombia hay dos formas de pensionarse: con el fondo público administrado por Colpensiones en el que cuando se cumplen 57 años para mujeres, 63 años para hombres y 1300 semanas de trabajo, la persona se puede pensionar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

O con los fondos privados, que funcionan como un sistema de ahorro individual en el que cada persona aporta según su ingreso y con base en el dinero que tenga al final de la vida laboral se puede obtener, o no, la pensión. De esta manera, lo importante con estos fondos no es la edad o el tiempo laborado, sino el dinero que se alcanzó a recoger para poder sostenerse sin tener que trabajar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cambio, con Colpensiones las personas aportan a 'una bolsa común' y se acude a la solidaridad intergeneracional, esto quiere decir que quienes actualmente cotizan en este fondo están cubriendo el ingreso de quienes ya están pensionados. De igual forma, al final de su vida laboral, las personas más jóvenes pagarán su pensión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Abogada recuerda que la creación de los fondos privados fue posible gracias a la Ley 100 de 1993, por lo que es un sistema reciente que está madurando y es el momento en que se empiezan a ver los problemas. (Le puede interesar: ["Sistema de pensiones en Colombia favorece a los más ricos"](https://archivo.contagioradio.com/las-pensiones-en-colombia-benefician-a-las-entidades-privadas-desigualdad-en-el-pais-ambito-laboral-jubilacion/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Con Colpensiones hay más de 1 millón de pensionados, con los fondos privados menos de 100 mil**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Trujillo señala que muchas personas se trasladaron en su momento a los fondos privados con la idea de que ahorrando lo suficiente, podrían pensionarse antes de la edad de retiro. Argumento al que acudió Macías para explicar las razones de su traslado de un fondo a otro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, la experta señala que dado que los salarios en Colombia son bajos, la mayoría de los trabajadores en el país llegan a la edad de retiro y no alcanzan a tener el mínimo necesario para poder dejar de trabajar. A ello se suma que son manejados como "un negocio", y los dueños de los mismos son también los dueños de los grandes grupos económicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ello redunda en que los dineros de los cotizantes son invertidos en los negocios de sus grupos económicos, por ejemplo, los de Porvenir en la construcción del puente de Chirajara a cargo de empresas de Sarmiento Angulo. Adicionalmente, Trujillo añade que los fondos cobran por administrar las pensiones, razón por la que así una persona no esté cotizando, recibirá descuentos de su ahorro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El efecto de esta situación es que mientras hay cerca de 1.400.000 colombianos pensionados por Colpensiones, son solo cerca de 97 mil las personas que alcanzan este derecho con los fondos privados. Y en muchos de los casos, las personas "están con la garantía de pensión mínima, que es una garantía del Estado", manifestó la integrante de Fescol.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Solo el 24% de las personas mayores de 65 años tienen pensión, ¿qué hacer?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo a Trujillo, solo 24% de las personas mayores de 65 años alcanza el derecho a descansar luego de toda una vida de trabajo. Para solucionar esta situación la respuesta estaría en la 'jugadita' de Macías, apoyar Colpensiones que dadas sus cifras, ha demostrado ser el modelo efectivo para garantizar la pensión de los colombianos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, vale la pena resaltar que el Gobierno ha mostrado que en su hoja de ruta no está fortalecer el sistema público, sino el privado. Dichas movidas aún no se han concretado en una reforma, razón por la que la abogada invitó a consultar a Colpensiones si se tienen dudas para realizar traslados o sobre el funcionamiento del sistema pensional.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Fortalezcamos Colpensiones. [pic.twitter.com/clcIz0E30A](https://t.co/clcIz0E30A)
>
> — MIGUEL ANGEL ALDANA PULIDO (@miangelaldana) [March 5, 2020](https://twitter.com/miangelaldana/status/1235621485726887941?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:block {"ref":78980} /-->
