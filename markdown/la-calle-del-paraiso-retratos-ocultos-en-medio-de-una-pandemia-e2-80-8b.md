Title: La calle del Paraíso, retratos ocultos en medio de una pandemia​
Date: 2020-04-19 12:18
Author: AdminContagio
Category: Nacional
Slug: la-calle-del-paraiso-retratos-ocultos-en-medio-de-una-pandemia%e2%80%8b
Status: published

##### reportaje

Las calles del Paraíso, retratos ocultos en medio de una pandemia
=================================================================

Habitantes del barrio Paraíso de la localidad de Ciudad Bolívar se han visto obligados a salir de sus casas para buscar alimentación a pesar de la amenaza de contagiarse por COVID-19.  

<figure>
![carlos-zea](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/carlos-zea-oly8bkjn8fdf6xi0rfsha6cdi9qv1lxt0nxu4i4doc.png "carlos-zea")  

<figcaption>
Textos y fotos: Andres Zea/Contagio Radio

</figcaption>
</figure>
###### 18 Abril 2020

El pasado 17 de abril se convocó a un plantón frente a la estación de Transmicable del barrio Paraíso en la localidad de [Ciudad Bolívar](https://archivo.contagioradio.com/alimentacion-salud-agua-y-acuerdo-humanitario-exigen-110-comunidades-a-duque/), para exigirle al gobierno que cumpla su función y promesa de entregar ayudas alimentarias debido a la falta de ingresos por aislamiento preventivo ante la pandemia del COVID-19. Decidimos recorrer las calles junto a Abel Gutiérrez uno de los líderes comunales del barrio y esto fue lo que evidenciamos.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-683x1024.jpg){width="683" height="1024" sizes="(max-width: 683px) 100vw, 683px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-683x1024.jpg 683w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-200x300.jpg 200w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-768x1152.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-1024x1536.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-1365x2048.jpg 1365w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7-370x555.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/7.jpg 1500w"}  

<figcaption>
</figcaption>
</figure>
###### Una de las calles del caserío ubicado debajo del Transmicable 

Mientras caminábamos por las laderas que se convirtieron en las calles de este caserío, el ladrido de los perros indicaba la presencia de personas extrañas por lo que la gente se asomaba para ver quién andaba por ahí. Al ver la cámara salían curiosos a preguntar la razón de nuestra presencia y nos manifestaron la precaria situación por la que están pasando debido a la cuarentena. 

>  “Muchacho usted es de la prensa ¿cierto?...Venga, por qué no nos graba, nosotros necesitamos ayuda y aquí nadie viene, solo ustedes”. Decía una mujer desde la puerta de su hogar

Al entrar a su casa construida con madera, ladrillos y cemento, pudimos notar la situación en la que vivía: el baño se encontraba al lado de la cocina separado por una teja de zinc; la luz que entraba por una ventana sobre la cocina servía también de área de secado para su ropa...Doña Deisy, se encontraba incapacitada por una herida en su dedo que se infectó, por lo que no ha podido trabajar y por lo tanto tampoco ha tenido muchos recursos para alimentarse. Ella nos dijo que ha intentado rebuscarse el sustento de otras formas, pero con la [cuarentena](https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/) se le dificulta el doble conseguir algo.

<figure>
[![Doña Deisy vive con dos perros y su hija](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/1-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/1.jpg)  

<figcaption>
Caserio debajo de Transmicable

</figcaption>
</figure>
<figure>
[![Una casa humilde pero limpia y organizada](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/2-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/2.jpg)  

<figcaption>
interior casa Ciudad Bolívar

</figcaption>
</figure>
Donde come uno comen dos  
=========================

Bajamos hasta una de las últimas casas que se encontraban en este caserío, el humo de la cocina y la oscuridad dejaban ver solo las siluetas de quienes se encontraban allí. Al adentrarnos en esta casa cimentada en grandes palos con paredes en tejas y madera no dejábamos de encontrarnos con distintas personas. En ese pequeño espacio conviven aproximadamente ocho personas en cabeza de Flor Marina y doña Dora, algunos son migrantes [venezolanos](https://archivo.contagioradio.com/si-la-ciudadana-importara-colombia-y-venezuela-tendrian-un-dialogo/) que encontraron un refugio y trabajo en esta casa. Días antes de nuestra visita parte de la vivienda se vino abajo y ellos se están encargando de reconstruir el tramo que se derrumbó.

El gobierno desde el 2016 está desalojando los predios que se encuentran en esta zona de alto riesgo, pero la falta de garantías de reubicación obligó a familias como la de Flor Marina a quedarse allí a pesar de peligro inminente en el que viven; ahora con la cuarentena solo exigen que el gobierno se haga presente y vea su situación para que los auxilien.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5-1024x682.jpg){width="1024" height="682" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5-1024x682.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5-768x511.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5-370x246.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5.jpg 1502w"}

###### La familia de Flor Marina y sus constructores extranjeros 

<figure>
[![La falta de servicios públicos los obliga a cocinar en leña](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/4-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/4.jpg)  

<figcaption>
La falta de servicios públicos los obliga a cocinar en leña

</figcaption>
</figure>
<figure>
[![La única presencia que hace el gobierno](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/6-1-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/6-1.jpg)  

<figcaption>
La única presencia que hace el gobierno

</figcaption>
</figure>
Al mal tiempo, buena cara  
==========================

Luz Marina Prieto y Leidy Moreno nos acompañaron también en el recorrido por el caserío hasta que llegamos a una casa de paredes de madera azules con un número “009-42” Asignado como una dirección provisional. Ellas viven con sus 5 hijos e hijas en la misma vivienda y nos invitaron a seguir.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/8.jpg 1500w"}

###### Algunas de las familias no tienen espacio en sus casas para secar la ropa por lo que deben hacerlo afuera pero con el riesgo de perderlas por robo. 

> Que pena con usted muchacho mire este desorden, si hubiera sabido que venía la organizaba” Dijo Luz Marina mientras tendía la cama con afán.

La casa estaba llena de juguetes, peluches y carteles, muy acogedora, en momentos se olvidaba la precaria situación en la que se encontraban. Al salir Luz y Leidy nos comentaron que el gobierno había venido de pasada y les hicieron un sinfín de promesas sobre la educación de sus hijos, la reubicación en un mejor lugar y un apoyo para su alimentación. Hasta hoy nada de esto se ha cumplido y tienen miedo de salir porque su rebusque es en la calle y por miedo a dejar a los niños solos en sus casas prefieren salir con ellos.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/11.jpg 1500w"}

###### Juguetes, peluches, cama, niños y niñas comparten este sitio que es como el museo de la alegría, protegiéndola como un tesoro en medio de una situación que raya en la miseria.  

> Usted nos ve alegres porque no queremos asustar a los niños pero en realidad tenemos miedo de salir a trabajar por contagiarnos y también por quedarnos y morirnos lentamente de hambre” 

Mientras hablábamos con Luz y Leidy una de las vecinas salió a ver qué pasaba, ellas la invitaron a que contara también su situación, doña Alejandra aceptó y nos invitó a seguir a su casa. Ella vive con sus dos hijos y un perro muy dócil que nos lamía las manos mientras entrabamos a su hogar, la situación de doña Alejandra es muy similar a la de los habitantes de este sector: falsas promesas, una zozobra constante por la falta de oportunidades y el olvido por parte del gobierno.

<figure>
[![Un saludo de bienvenida que sirve a su vez de tapete](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/10-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/10.jpg)  

<figcaption>
Un saludo de bienvenida que sirve a su vez de tapete

</figcaption>
</figure>
<figure>
[![Doña Alejandra con su hija y una sobrina que esta cuidando](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/9-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/9.jpg)  

<figcaption>
Doña Alejandra con su hija y una sobrina que esta cuidando

</figcaption>
</figure>
La hermandad puede más que la hambruna en medio de la pandemia
==============================================================

Don Abel nos comentó sobre un caso que quería que documentáramos así que nos dirigimos cerca del nuevo parador del SITP para encontrarnos con otro líder comunal que nos llevaría hasta allí, empezamos a subir por la vía que conecta con Mochuelo en Usme. Al lado de la entrada de la casa se encontraba un abuelo con un gorrito azul a quien saludamos pero no nos escuchaba muy bien pues sufre de sordera. Don Miguel Calderón salió y nos invitó a seguir hasta su cuarto donde convive con su hermano y el amigo que encontramos en la entrada.

Al ingresar a su habitación vimos a un adulto mayor acostado en una cama mientras varias moscas lo rondaban, don Miguel se sentó a su lado y nos empezó a contar su historia: Él -don Miguel, de 72 años- y el hombre acostado -Abelino Calderón, de 84 años- son hermanos pero por la condición de salud de don Abelino, tiene que hacerse cargo de la casa y de los cuidados de su hermano.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/13.jpg 1500w"}

###### don Miguel y don Abelimo viven en un pequeño cuarto al lado de la cocina en la que preparan sus alimentos con leña, lo que empeora aún más su salud en medio de la pandemia  

Mientras estuvimos hablando con don Miguel siempre se estuvo tocando el pecho, hace un tiempo algunos policías lo habían golpeado y le quedaron secuelas en su cuerpo desde esa vez. Aún así, tenía que anteponerse al dolor porque debía cuidar a su hermano, nos contó que él trabajaba en Abastos pero que ahora por su edad y la cuarentena no lo dejan entrar, entonces se está quedando sin alimentos.

<figure>
[![Don Miguel y su vecino de cuarto a la entrada de su pieza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/14-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/14.jpg)  

<figcaption>
Don Miguel y su vecino de cuarto a la entrada de su pieza

</figcaption>
</figure>
<figure>
[![Uno de últimos caseríos del Paraíso lleno de trapos rojos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/15-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/15.jpg)  

<figcaption>
Uno de últimos caseríos del Paraíso lleno de trapos rojos

</figcaption>
</figure>
<figure>
[![Líderes y líderesas reunidos para ver que medidas tomar para ayudar en su comunidad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/12-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/12.jpg)  

<figcaption>
Líderes y líderesas reunidos para ver que medidas tomar para ayudar en su comunidad

</figcaption>
</figure>
Al salir de la casa de don Miguel nos dirigimos nuevamente hasta el paradero del SITP donde se encontraban reunidas varias líderesas del sector quienes se nos acercaron para pedirnos nuestros contactos y ver de qué manera podíamos ayudar en su situación. Ellas nos manifestaron que gran parte de la gente está pasando por situaciones muy similares o peores que las [historias](https://www.justiciaypazcolombia.com/porque-no-nos-ha-dado-dios-espiritu-de-cobardia-sino-de-poder-de-amor-y-de-dominio-propio/) que acaban de leer, que es necesaria y urgente la presencia del Estado y de las organizaciones sociales, no solo por la falta de alimentos sino por el abandono estatal que sufren los barrios de la parte alta de la montaña en Ciudad Bolívar, que necesitan que de alguna forma su situación sea evidenciada para así lograr que mejore algo en medio de todo lo malo.  

https://www.youtube.com/watch?v=xCd1fvU6vY0&feature=youtu.be  
[![Fue asesinado Rigoberto García, firmante de paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Asesinan-a-Edwin-de-Jesús-Carrascal-3.jpg){width="370" height="189"}](https://archivo.contagioradio.com/fue-asesinado-rigoberto-garcia-firmante-de-paz/)  

#### [Fue asesinado Rigoberto García, firmante de paz](https://archivo.contagioradio.com/fue-asesinado-rigoberto-garcia-firmante-de-paz/)

Este 17 de abril cerca de las 7:00 pm de la noche fue asesinado en el firmante de…  
[![Teodomiro Sotelo, segundo líder asesinado en El Tambo, Cauca en un mes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Teodomiro-Sotelo-Anacona-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/teodomiro-sotelo-segundo-lider-asesinado-en-el-tambo-cauca-en-un-mes/)  

#### [Teodomiro Sotelo, segundo líder asesinado en El Tambo, Cauca en un mes](https://archivo.contagioradio.com/teodomiro-sotelo-segundo-lider-asesinado-en-el-tambo-cauca-en-un-mes/)

Teodomiro Sotelo pertenecía al Consejo Comunitario Afro Renacer, del Coordinador Nacional Agrario y de Congreso de los Pueblo  
[![Asesinado menor de edad del pueblo Nasa en Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/ACIN-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/asesinado-menor-de-edad-del-pueblo-nasa-en-cauca/)  

#### [Asesinado menor de edad del pueblo Nasa en Cauca](https://archivo.contagioradio.com/asesinado-menor-de-edad-del-pueblo-nasa-en-cauca/)

Yilber Andrés Yatacué de 14 años fue herido y posteriormente falleció producto de los combates entre Fuerza Pública…  
[![Emergencia por COVID llegó al INPEC y tampoco hay protocolo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Inpec-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo/)  

#### [Emergencia por COVID llegó al INPEC y tampoco hay protocolo](https://archivo.contagioradio.com/emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo/)

De acuerdo con el dragoneante Jhon Alracón, presidente del sindicato SINTRAPECUN del INPEC, no existen un plan para…  
[![Decreto presidencial no resuelve hacinamiento ni emergencia por COVID](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/)  

#### [Decreto presidencial no resuelve hacinamiento ni emergencia por COVID](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/)

El decreto 546 expedido por el gobierno nacional, con el que se buscaba controlar la crisis del Covid…
