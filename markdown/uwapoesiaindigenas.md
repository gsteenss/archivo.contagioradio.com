Title: El pueblo U´wa y el origen de la poesía colombiana
Date: 2017-03-21 13:15
Category: Viaje Literario
Tags: Dia mundial de la poesía, Nación Uwa, poesía indígena
Slug: uwapoesiaindigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/u´wa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 21 Mar 2017 

Desde 1999, la Organización de las Naciones Unidas proclamó el 21 de marzo como el Día Mundial de la poesía como una forma de apoyar la diversidad lingüística a través de la expresión poética y dar la oportunidad a las lenguas amenazadas de ser un vehículo de comunicación artística en sus comunidades respectivas.

En el libro "Historia de la poesía colombiana", publicado por la Casa de poesía Silva en 1992, el escritor William Ospina dedica un aparte especial a la poesía indígena, en el que destaca la relevancia histórica que para la cultura y letras en el país tiene el legado del pueblo indígena u´wa y su tradición oral, anterior a la llegada de la lengua castellana a estos territorios.

Anterior a la narrativa mítica de la edad de bronce griega y nórdica, están los mitos ancestrales de los u´wa, relatos que apenas empiezan a ser transcritos y meditados, y que pueden considerarse como las primeras manifestaciones culturales precolombinas de estos pueblos asentados en la Sierra Nevada del Cocuy y la Coordillera Oriental hasta la Sierra de Mérida en Venezuela.

En 1985 la Fundación de Investigaciones Arqueológicas Nacionales del Banco de la República, publicó la traducción de "El vuelo de las tijeretas", resultado del trabajo adelantado desde 1983 por la antropóloga e investigadora inglesa Ann Osborn. Un documento incunable por tratarse de los orígenes de la poesía colombiana y parte de la construcción patrimonial del país.

Ospina hace referencia a "los vuelos del canto" que inicia en su traducción latina "Shishara (horizonte donde terminan los ríos), Shakira, Tirira, Karouwa, Tha Kuma, Bekana, Raiayna, O'runa, Beragdrira,Th'thumbria, Yokumbria, Akatra, Barima, IthKwitra, Okitra, Sherina, Botruna, Bukwarina, Barawiya,| Sherowiya ("la mujer del lugar del solsticio" y "la mujer joven del sol", es decir, las regiones donde habitan estas divinidades), Waiyana (que tal vez signifique "en dirección a Guayana, en el este")..." en el que cada tantas palabras un estribillo explica, detiene y matiza la numeración.

Como explica el escritor, las palabras que allí aparecen "son nombres propios que designan regiones del mundo de los u'wa, casi al modo de esas secuencias que en los documentos legales definen los límites de un territorio". Luego aclara que el canto hace más que nombrar los lugares, narrando el vuelo de las águilas migratorias que viajan de norte a sur del continente y que se ven "en grandes bandadas atravesando el paso de las cordilleras".

De una de esas águilas proviene el nombre U´wa, recuerda el escritor, "Hombres de forma de pájaros que ocuparon el espacio en un vuelo inicial y sus descendientes, vuelven a recorrer en el canto el itinerario que periódicamente repiten las águilas", buscando renovar su pertenencia al territorio sagrado y al sentimiento de remontarse no sólo a una tradición sino a un origen mágico. Le puede interesar: [Un siglo y un cuarto de Cesar Vallejo](https://archivo.contagioradio.com/cesarvallejopoesiaperuana/)

El legado de los U´wa, representa otra manera de relacionarse los hombres con el mundo, otro camino de la poesía en occidente que pudo llevar a la humanidad a otro tipo de poesía y otro tipo de civilización. En estos mitos la poesía tuvo un papel fundador de una cultura, evidenciando un alto grado de refinamiento verbal, una civilización poética arraigada en el mundo físico y aún no exaltada o extraviada en abstracciones

"Los U'wa sienten que pertenecen a la tierra, son hijos de las águilas y de los árboles, tienen mitos para sus bosques, sus aves, sus ríos y peces, y las demás criaturas que pueblan su región", escribía Ospina añadiendo que los indígenas han hecho del lenguaje un instrumento para ordenar y sacralizar el mundo, nombrado minuciosamente su territorio, con nombres que no son meras comodidades de orientación, como lo son para nosotros, sino que representan una consagración y un vínculo.

El escritor cierra ese apartado reflexionando sobre lo distante de este tipo de mitología, vinculada a la naturaleza y distante de las fantasías de las religiones de oriente, que cataloga de "imperiosas y despóticas" y de las religiones que han tiranizado y ensangrentado al mundo. Asegura que estos relatos son motivo de alegría y pueden alentar en nosotros alguna discreta esperanza.
