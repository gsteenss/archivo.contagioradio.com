Title: Comunidades índigenas y campesinas de Guzman y Caicedo rechazan explotación minera en Putumayo
Date: 2015-02-16 21:22
Author: CtgAdm
Category: Comunidad
Tags: Conpaz, Explotación petrolera Putumayo, Pueblo Naya, Putumayo
Slug: pueblo-nasa-se-suma-al-rechazo-de-explotacion-petrolera-en-corregimiento-pinuna-blanco-en-putumayo
Status: published

###### **Foto:Contagioradio.com** 

**Nuevamente las comunidades indígenas y  campesinas del Putumayo rechazan la exploración sísmica y explotación petrolera en sus territorios.**

Comunicado del Pueblo Nasa y otras organizaciones sociales:

La Asociación Consejo Regional del Pueblo Nasa del Putumayo KWE’SX KSXA’W, los Cabildos y Resguardos indígenas Nasa, de los municipios de Puerto Caicedo, Villagarzón y Puerto Guzmán, La Asociación de Trabajadores Campesinos del Alto Mecaya (Atcam), La Coordinadora Campesina de Unidad Popular, los presidentes de juntas de acción comunal, La Mesa Regional de Organizaciones Sociales, informamos a la opinión pública nacional e internacional que reunidos el día de hoy, Sábado 14 de febrero de 2015 con la empresa Fundescoop, quien trabaja para la empresa PetroNova dueña del Bloque Petrolero Put 2, les manifestamos nuestra decisión de rechazar y oponernos a toda actividad de exploración sísmica y explotación petrolera dentro de los territorios de las comunidades indígenas y campesinas de los municipios de Puerto Caicedo y Puerto Guzmán, por las serias afectaciones, sociales, culturales y ambientales que ha venido provocando la industria petrolera en nuestra Amazonía

Nosotros y Nosotras nos sumamos hoy a las diversas expresiones de rechazo de las comunidades y procesos organizativos en los diferentes municipios del departamento, evidenciadas en los últimos meses en las movilizaciones y pronunciamientos en los municipios de Puerto Asís, Puerto Leguízamo, Orito y Villagarzón.

En este sentido hacemos un llamado al gobierno nacional, a que se aborde de manera inmediata en el marco de los diálogos sostenidos con la Mesa Regional de Organizaciones Sociales, la problemática petrolera y todas sus consecuencias.

Es urgente que el gobierno nacional escuche el sentir del pueblo putumayense y su defensa de la vida el territorio y la biodiversidad.

** **

**Por la protección del Agua, la Vida, el Territorio y la Biodiversidad**

**Por la Reivindicación de nuestros derechos**

**Caminamos la Palabra**
