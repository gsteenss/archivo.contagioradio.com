Title: Asesinan a la lideresa Carlota Salinas en San Pablo, Bolívar
Date: 2020-03-25 20:52
Author: CtgAdm
Category: Actualidad, DDHH
Tags: bolivar
Slug: asesinan-a-la-lideresa-carlota-salinas-en-san-pablo-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Carlota-Salinas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Foto: Cortesía OFM

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El día 24 de marzo cerca de las 8:00 de la noche fue asesinada la lideresa social Carlota Salinas en el municipio de San Pablo en Bolívar, un municipio considerado como uno de los más afectados y con mayor número de víctimas del conflicto armado en Colombia y a su vez el primero en hacer parte de los Programas de Desarrollo con Enfoque Territorial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la [Organización Femenina Popular (OFM)](https://twitter.com/OFPMujeres), organización a la que estaba vinculada Carlota, hombres armados llegaron hasta su vivienda ubicada en el barrio Guarigua, la obligaron a salir para luego asesinarla con arma de fuego. En medio de la cuarentena, la defensora de DD.HH. dedicó su último día a hacer recolecta de alimentos para llevar a familias vulnerables del municipio, "solo el acto de ayer da cuenta del liderazgo y lo humana que era Carlota", la recuerda Gloria Suárez, representante legal de la organización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Organización Femenina Popular nace en 1972, en Barrancabermeja, como una propuesta de la Iglesia Católica para organizar a las mujeres en torno a la superación de la violencia intrafamiliar, el sometimiento de la mujeres y la injusticia social, desde entonces su trabajo se ha enfocado en la defensa de la vida y los derechos humanos integrales, reivindicando los derechos de la mujer. [(Lea también: Lideresas sociales guardan dolores emocionales que se deben atender: Limpal)](https://archivo.contagioradio.com/lideresas-sociales-guardan-dolores-emocionales-que-se-deben-atender-limpal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Carlota Salinas, quien fue la coordinadora de Mujeres del Valle del Río Cimitarra perteneció por más de 10 años a la OFM, donde promovió el trabajo de defensa de los derechos de las mujeres, "era una mujer solidaria, comprometida que siempre estuvo en las acciones en las que se rechazaba la violencia contra las mujeres".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Municipios como San Pablo, Simití y Santa Rosa del Sur son un punto estratégico, no solo por su potencial minero, también por el paso del río Magdalena y sus límites con la Serranía de San Lucas, conectando a Antioquia, Cesar y Santander. Aunque la firma del Acuerdo de Paz disipó la presencia de las FARC, desde entonces, ELN y las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) han intensificado su operar en medio de otros grupos ilegales como Los Rastrojos, Los Vagos y los Mellizos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para la representante legal, el asesinato de Carlota Salinas, en una época en la que se supone las personas permanecen en sus casas seguras y a salvo, evidencia que no hay realmente una respuesta adecuada de protección ni seguridad por parte del Estado para los líderes sociales, que en medio de una situación donde hay un riesgo sanitario, pasan a un segundo plano "mientras la persecución y los asesinatos siguen"[(Lea también: Acatando cuarentena en su hogar, asesinan a dos líderes indígenas en Valle del Cauca)](https://archivo.contagioradio.com/cuarentena-asesinan-indigenas-valle/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Las mujeres de la organización están muy golpeadas, es una pérdida de una lideresa y de una defensora de la vida y de la comunidad de San Pablo"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

A este hecho se suman el asesinato de los líderes indígenas Omar y Ernesto Guasiruma del pueblo Embera que permanecían en su hogar como parte de la cuarentena decretada por el Gobierno como respuesta al Covid 19 y a los homicidios de Marco Rivadeneira, líder social del Putumayo y ocurridos la semana pasada.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
