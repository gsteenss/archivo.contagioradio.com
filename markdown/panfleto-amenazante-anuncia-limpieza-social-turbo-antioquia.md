Title: Panfleto amenazante anuncia "limpieza social" en Turbo, Antioquia
Date: 2016-01-19 16:49
Category: DDHH, Nacional
Tags: Bajo Atrato, paramilitarismo en Colombia
Slug: panfleto-amenazante-anuncia-limpieza-social-turbo-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/IMG-20160119-WA0001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

<iframe src="http://www.ivoox.com/player_ek_10129596_2_1.html?data=kpWelJ6ZfZehhpywj5WbaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncbHVz8vZx9nTb8LhxtPO3MbSuMafwtPi0MjNpYyZk5fZy9LUrcbuwpDg0cjNpc2Zk5eYx9OPmNbmw9SSlKiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [19 Enero 2016]

Desde hace dos semanas, habitantes del municipio de Turbo, en el departamento de Antioquia denuncian que a varias casas de la zona, a los correos y a las redes sociales, está llegando un panfleto amenazando con la realización de una **“limpieza social”, por parte del grupo paramilitar los Urabeños.**

El panfleto se estaría repartiendo en horas de la noche. Allí aseguran tener identificadas a **las personas junto a sus familias, que serían nuevo objetivo militar del grupo armado.**

Esta situación, sumada a la incursión paramilitar en otras zonas del [Bajo Atrato](https://archivo.contagioradio.com/?s=bajo+atrato) y al asesinato de 5 pobladores en Cacarica, ha afectado la seguridad y movilidad de los habitantes de la zona noroccidental del país. ([Ver también: 5 Campesinos asesinados por paramilitares en el Bajo Atrato chocoano](https://archivo.contagioradio.com/5-campesinos-asesinados-por-paramilitares-en-el-bajo-atrato-chocoano/))

[Conozca el panfleto: ]  
[![IMG-20160119-WA0001 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/IMG-20160119-WA0001-1.jpg){.aligncenter .size-full .wp-image-19344 width="571" height="768"}](https://archivo.contagioradio.com/panfleto-amenazante-anuncia-limpieza-social-en-turbo-antioquia/img-20160119-wa0001-1/)

Reciba toda la información de Contagio Radio en su correo <http://bit.ly/1nvAO4u> o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
