Title: Las exigencias del movimiento ambientalista y animalista a Iván Duque
Date: 2018-06-18 17:39
Category: Ambiente, Movilización
Tags: Ambiente, Animalistas, consultas populares, Duque, POT
Slug: las-exigencias-del-movimiento-ambientalista-y-animalista-a-la-presidencia-de-ivan-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto Archivo] 

###### [18 Jun 2018] 

Oscar Sampayo, miembro de la Liga Contra el Fracking y del Grupo de Estudios Sociales y Ambientales del Magdalena Medio (GEAM) y Natalia Parra, integrante de la Plataforma Colombiana por los animales ALTO calificaron como preocupante la elección de Iván Duque como presidente de Colombia por los riesgos ambientales que existen en su propuesta económica basada en el sector minero-energético.

### **Los reparos con el proyecto Duque** 

<iframe src="https://co.ivoox.com/es/player_ek_26602733_2_1.html?data=k5ujkpebd5Shhpywj5WZaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbDnxMbfjbjFsdHV2tSSlKiPscrZzsff0ZDIqYzgwpC5y8zFb6Tjz9nfw5DJsIy608bQzc7Sq46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Sampayo y Parra coincidieron en señalar que hay un precedente que les genera temor y algún grado de certeza sobre lo que puede significar un gobierno del Centro Democrático: El primer periodo de Álvaro Uribe como presidente. Dicho precedente, significó en términos ambientales el ingreso de economías extractivas a ecosistemas frágiles, así como el avance de la siembra de Palma de Africana y el posterior desplazamiento de flora y fauna nativa.

Natalia Parra, señaló que, pese a que las propuestas de Iván Duque tienen en cuenta el maltrato animal, éste se refiere únicamente a los animales de compañía (gatos y perros) y algunos de fauna silvestre pero no se refiere a otros como toros, los caballos utilizados en cabalgatas y menos a animales que están en ecosistemas y pueden verse afectados por el glifosato, el fracking y los proyectos extractivos.

De igual forma, Oscar Sampayo declaró su preocupación “por el avance de la minería en los páramos y el fracturamiento hidráulico en los territorios”, lo que para él se traduciría en una evidente lucha social y jurídica por la defensa de los ecosistemas naturales.

Adicionalmente, tanto Sampayo como Parra alertaron sobre la posibilidad de que se quiera reducir o incluso eliminar los mecanismos de participación ciudadana que permiten incidir en los planes de acción territoriales. (Le puede interesar: ["Delimitación de paramos debe ser concertada con las comunidades: Ambientalistas "](https://archivo.contagioradio.com/delimitacion-de-paramos-debe-ser-concertada-con-las-comunidades/))

### **Las exigencias** 

<iframe src="https://co.ivoox.com/es/player_ek_26602769_2_1.html?data=k5ujkpebepqhhpywj5aXaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5ynca_V1cbZy8aPlMLm08aSlKiPiMrmxsjh0dfFb7HgwtnOyNTWscKforHBsZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para ambos activistas, es fundamental preservar la capacidad de las personas de determinar lo que ocurre en sus territorios, garantizando los mecanismos de participación que existen para ello. Así como permitir la integración activa de la ciudadanía en los Esquemas de Ordenamiento Territorial (EOT) y los Planes de Ordenamiento Territorial (POT).

A ello se añade la demanda que hace Natalia Parra, para que se preserven los avances en materia legislativa y de regulación que se han logrado a favor de los animales, y que se busque activamente la protección al medio ambiente.

### **El llamado a la ciudadanía** 

Natalia Parra afirmó que “a veces lo que estos procesos significan es que quienes no estaban activos políticamente se incorporen buscando la construcción de un país incluyente” por lo que el llamado de parte de Parra y Sampayo es a la creación de un frente amplio en el que confluyan tanto ambientalistas como animalistas y en el cual tengan cabida todas las reivindicaciones ciudadanas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
