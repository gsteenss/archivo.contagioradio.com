Title: En riesgo la vida de lider de restitución de tierras Eliodoro Polo
Date: 2020-08-24 13:22
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Bajo Atrato chocoano, Restitución de tierras
Slug: lider-territorial-heliodoro-polo-senala-que-su-vida-corre-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2018-09-13-at-4.17.35-PM-750x330-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Comisión Intereclesial de Justicia y Paz, acompañante de diferentes comunidades en el tema de restitución de tierras, denunció este 22 de agosto el riesgo que corre la vida del **líder de restitución de tierras Eliodoro Polo, quién ha recibido amenazas de muerte en los últimos días.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eliodoro Polo, **es habitante y líder territorial la Zona de Biodiversidad, La Esperanza, ubicada en Bijao, territorio colectivo de Pedeguita y Mansilla, en el departamento del [Chocó](https://archivo.contagioradio.com/en-quibdo-han-sido-asesinados-97-jovenes-en-2020/),** y en su proceso defensa territorial ha sido objeto de amenazas por evitar la implementación de proyectos bananeros con ocupa antes de mala fe en su territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Comisión, el pasado 22 de agosto el líder territorial recibió una amenaza a través de un mensaje de texto enviado por un número celular, en donde le decían, ***"no te vas a hacer matar por esa tierra, no te queremos en esa finca".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Amenaza que se da luego de que la familia Polo recibiera una comunicación en la cual les informan que su finca sería parcelada**, una decisión que según la Comisión, desconoce la disposición al diálogo y además dispone de un predio habitado ancestralmente por la familia Polo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El territorio perteneciente durante generaciones a Heliodoro Polo y su familia **tiene una longitud de 160 hectáreas de tierra, de las cuales hoy hacen uso de menos del 50% a causa de los abusos de integrantes del consejo comunitario** quienes implementan diferentes [mecanismos de despojo](https://www.justiciaypazcolombia.com/amenazas-contra-el-lider-eliodoro-polo/), señala la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Acciones que según la organización son permitidas por el representante legal del Consejo Comunitario**, quién *"desconoce las medidas cautelares entregadas por el Tribunal de Restitución de Tierras de Quibdo quién suspendió el proyecto bananero"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como la ocupación de terceros en el predio, pese a esto la organización señala que **este proyecto que además cuenta con el respaldo de la Agencia Nacional de Tierras continúa hasta la fecha su curso.** ([Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto a pesar de los de abusos de autoridad y arbitrariedades que han sido puestas en conocimiento de todas las autoridades, del Gobierno Nacional, y de las entidades del Estado colombiano, en una acción que no solamente atenta contra la restitución de tierras en este territorio, sino también contra la vida, la tranquilidad y el derecho al territorio del líder territorial y la familia Polo.

<!-- /wp:paragraph -->
