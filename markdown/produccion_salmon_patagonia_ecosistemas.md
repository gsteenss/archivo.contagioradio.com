Title: Producción de salmón tiene en riesgo ecosistemas marinos de la Patagonia
Date: 2017-06-10 13:30
Category: Ambiente, Voces de la Tierra
Tags: Chile, criaderos, salón
Slug: produccion_salmon_patagonia_ecosistemas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/chile-salmon-farm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: One Fish Foundation 

###### [10 Junio 2017] 

La producción de salmón está impactando gravemente las especies marinas del **tramo de la Patagonia Sur de Chile. **Según explican organizaciones como AIDA y Justicia Ambiental, animales como ballenas azules, delfines chilenos, pingüinos y elefantes marinos, se ven en amenaza por la producción de **salmón industrial.**

Teniendo en cuenta que Chile es el segundo mayor productor mundial de salmón. "La industria desarrollada a lo largo de las costas ubicadas más al norte de Magallanes, ha puesto las ganancias económicas por encima de la protección ambiental, creando a su paso zonas oceánicas desprovistas de vida", explican las organizaciones ambientalistas.

**Según indican, las condiciones de la Región de Magallanes hacen que sus ecosistemas sean menos capaces de soportar la intervención humana.** De manera que es preocupante que los criaderos de salmón hayan comenzado a operar en este rincón único de la Patagonia.

### La producción de salmón 

De acuerdo con informes del gobierno chileno, el 81% de las nuevas concesiones de salmonicultura se han localizado en Magallanes, y además del total de las granjas de salmón, más de la mitad han reportado falta de oxígeno, lo que amenaza la vida marina.

Aunque diferentes organizaciones defensoras de la naturaleza han reportado el daño que ocasionan esas granjas, no ha sido posible que el gobierno chileno legisle sobre el tema, cumpla las regulaciones internacionales, y por tanto, **no se han realizado los estudios necesarios para visibilizar de manera más concreta el impacto ambiental.**

No obstante, para producir el alimento de los salmones se genera una importante presión sobre los bancos de peces. Además, las aguas reciben **cantidades considerables de desechos, como el alimento no consumido por los peces, que sedimenta en el fondo marino. También se introducen antibióticos y sustancias químicas al ecosistema** con el objeto de combatir enfermedades, sin embargo lo que se produce muchas veces es el aumento de la probabilidad de expansión de enfermedades en el medio. Además, en el proceso de faena del producto se generan desechos que muchas veces terminan en los cursos de agua, como lo señala una investigación del Consejo Latinoamericano de Ciencias Sociales, *CLACSO.*

En ese marco, **a través de una petición de[The Action Network](https://actionnetwork.org/petitions/protege-la-patagonia-dile-a-chile-que-mejore-la-regulacion-de-las-granjas-de-salmon?source=direct_link&), ciudadanos del mundo le están pidiendo al Gobierno de Chile que haga cumplir las regulaciones** que existen para las granjas de salmón, que investigue los daños que la industria está generando en la Patagonia, y sancione a las empresas responsables.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
