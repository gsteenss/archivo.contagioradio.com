Title: Atentan contra la vida del dirigente comunal Pedro Guzmán en la vereda Nueva Etapa, Caquetá
Date: 2018-02-10 11:37
Category: DDHH, Nacional
Slug: atentan-contra-la-vida-del-dirigente-comunal-pedro-guzman-en-la-vereda-nueva-etapa-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/pedro-gusman-e1518280616563.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RecpSur] 

###### [10 Feb 2018]

En horas de la tarde, del pasado 9 de febrero, en la vereda Nueva Etapa, Caquetá, el líder social Pedro Guzmán, fue víctima de un atentado, cuando dos sicarios que se movilizaban en una moto, le propinaron **4 disparos con un arma de fuego, en el momento en que Guzmán llegaba a la casa de su padre**.  Debido a la gravedad de las heridas su diagnóstico es reservado.

Guzmán es dirigente comunal de la Junta de Acción Comunal de la vereda Nueva Etapa y se desempeña como tesorero de la Asociación  Troncales Guacamayas de San Vicente del Caguán, de igual forma ha desarrollado **un papel importante en la defensa de los derechos humanos y garantías sociales para el campesinado en su territorio**. (Le pude interesar: ["Asesinan a 2 líderes sociales en el municipio de Guapi, Cauca"](https://archivo.contagioradio.com/asesinan-a-3-lideres-sociales-en-el-municipio-de-guapi-cauca/))

Frente a estos hechos, la Unión de organizaciones Sociales, Ambientales, Indígenas, Campesinas y Gremiales de San Vicente del Caguán, UNIOS, le exigen al gobierno del presidente Juan Manuel Santos **"tomar las medidas pertinentes que brinden garantía efectivas de los derechos de las comunidades de San Vicente del Caguán"**.

En ese mismo sentido, le piden a las autoridades correspondientes que inicien las investigaciones pertinentes que de con la identificación de los responsables y le solicitan a la comunidad nacional e internacional, un acompañamiento permanente a los territorios amenazados en el país.

\[caption id="attachment\_51399" align="alignnone" width="742"\]![Red de comunicadores Populares del Sur](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Pedro.jpg){.size-full .wp-image-51399 width="742" height="960"} Red de comunicadores Populares del Sur\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
