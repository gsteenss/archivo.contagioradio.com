Title: "En Colombia estamos importando hasta la papa"
Date: 2020-05-11 18:07
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: colombia
Slug: en-colombia-estamos-importando-hasta-la-papa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EXwUxBxWkAE2Rx-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: @JuventudesUPSP*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la expedición del **Decreto 523 del abril 20 de 2020**, diferentes organizaciones exigen por medio del hastag [\#ConsejoDeEstadoPilasConLasSemillas](https://twitter.com/hashtag/ConsejoDeEstadoPilasConLasSemillas?src=hashtag_click) que se elimine y se reconozca el trabajo de miles de campesinos y campesinas en la producción de alimentos locales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este Decreto se da en medio de la crisis económica social y ecológica declarada por el Gobierno, y tiene como fin establecer temporalmente el **0% de aranceles a la importación de productos como la soya, el maíz amarillo y el sorgo, alimentos producidos por más de 390 mil familias en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El decreto que fue apelado desde el 4 de mayo cuando los integrantes de la Alianza por la Agrodiversidad entre otras organizaciones pidieron que se declarara inconstitucional, argumentando que se debe fomentar la venta local y no a los productores a gran escala.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/C_Pueblos/status/1259870882165489665","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/C\_Pueblos/status/1259870882165489665

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En [Otra Mirada](https://web.facebook.com/contagioradio/videos/3012121345510565/) hablamos con **Germán Vélez, director de la Corporación Grupo Semilla**, quién afirmó que **en Colombia se importa más del 60% de los alimentos**, 85% del maíz y 95% de la soya. (Le puede interesar: <https://archivo.contagioradio.com/la-crisis-no-viene-de-la-tierra-sino-del-modelo-de-habitarla/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"La importación aumentó significativamente luego el Tratado de Libre Comercio con Estados Unidos, ingresando materia prima a bajos costos volviendo inviable la agricultura nacional"*, afectando directamente a los pequeños campesinos e indígenas productores de alimentos. Estos agricultores prácticamente o no pueden competir con los precios de un mercado global

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"En Colombia el 30% son medianos productores el 60% son pequeños y el 10% grandes productores,** por eso poner este arancel en cero lo que hace es permitir que el país se vea mucho más inundado de importación de materia prima acabando con la producción de alimentos nacional"*
>
> <cite>**Germán Vélez | Director de la Corporación Grupo Semilla**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### ¿Por qué importar estos productos?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Vélez señaló que ante la sobre producción de soya en el mundo, y la guerra comercial entre China y Estados Unidos, se cerraron las puertas en muchos países al mercado de la soya y maíz proveniente de Estados Unidos; y es allí donde tienen que buscar que hacer con tanto producto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Estados Unidos aprovechando las obligaciones que tiene Colombia con el TLC **atrae todo su mercado alimentario explotado y modificado, sin importa el costo que este representa para la producción Nacional** de alimentos"*, afirmó Vélez.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1259864479761915909","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1259864479761915909

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Conociendo esto, diferentes organizaciones campesinas hicieron una intervención en el Consejo de Estado para que se revise este Decreto y se declare su inconstitucionalidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"*El Gobierno en vez de hacer normas regresivas que beneficien solo algunos sectores poderosos, debe fortalecer y apoyar la agricultura campesina, local y comunitaria", destacó Vélez y afirmó que se debe promover una producción con enfoque agroecologico sostenible y no una producción masiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo señaló que **se debe reconocer el papel de la zona rural y de los campesinos en la sostenibilidad y la atención a la crisis alimentaria y ambiental actual** y su rol fundamental para evitar futuras crisis producto de el modelo insostenible de producción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Colombia las normas temporales se vuelven definitivas

<!-- /wp:heading -->

<!-- wp:paragraph -->

*Para Vélez, "en el país las normas que se plantean como temporales y prorrogables sabemos que históricamente terminan siendo temporales, este es el caso del IVA , el 4 por mil y todas las normas que se dicen que no nos van a afectar significativamente*"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó que con el Decreto 423 no se puede pasar lo mismo, "***ni siquiera se puede permitir que sea temporal, porqué hoy importamos varios productos de la canasta familiar básicos como la papa"***, un mandato que a futuro va a desestabilizar y eliminan completamente la economía rural y la independencia alimentaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente hace un llamado a fortalecer los sistemas de producción campesina y local, *"**no es presentar cualquier sistema de producción agrícola, diciendo que hay que modernizar el campo, invirtiendo en más pesticidas agroquímicos** , lo que tenemos que hacer es promover una estructura agroecologica sostenible y amigable con el medio ambiente que evite una nueva crisis producto de los abusos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*(También le puede interesar el podcast Voces de la Tierra sobre la importancia de las semillas nativas y los errores del decreto 523 de 2020*)

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://web.facebook.com/contagioradio/videos/232026871399949/","className":""} -->

<figure class="wp-block-embed">
<div class="wp-block-embed__wrapper">

https://web.facebook.com/contagioradio/videos/232026871399949/

</div>

</figure>
<!-- /wp:embed -->
