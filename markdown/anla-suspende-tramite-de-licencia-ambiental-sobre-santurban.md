Title: ANLA suspende trámite de licencia ambiental sobre Santurbán
Date: 2020-03-19 19:06
Author: AdminContagio
Category: Actualidad, Ambiente
Tags: ANLA, Minesa, santurbán
Slug: anla-suspende-tramite-de-licencia-ambiental-sobre-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/ETSKcA_X0AEJfFM.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/ETSKcA_X0AEJfFM-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: [@Veroez\_](https://twitter.com/Veroez_)  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la crisis sanitaria actual son varios sectores afectados y que rechazan enfáticamente las decisiones del Gobierno, a pesar de ellos la **ANLA** dio una buena noticia al movimiento ambientalista; se trata de la suspensión del proceso de licenciamiento de [Minesa](https://archivo.contagioradio.com/por-medio-de-mentiras-minesa-pretende-desacreditar-el-movimiento-social/)en **[Santurban](https://archivo.contagioradio.com/en-tiempos-de-sequia-se-reactiva-defensa-del-paramo-santurban/)**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La acción se da este 19 de marzo, luego de que el Comité por la defensa del Páramo de Santurbán solicitara al director de la Autoridad Nacional de Licencias Ambientales (ANLA), **Rodrigo Suárez suspender los términos en el licenciamiento ambiental del Minesa**; al que Suárez respondió pocas horas confirmando su aceptación a la petición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como lo afimó el Comité este sería un "acto de transparencia” por parte de la ANLA teniendo en cuenta que por la crisis del COVID- 19, se suspendió la movilización en la defensa del agua que se tenía agendada para el 16 de marzo desde Bucaramanga y en donde lo ambientalistas pretendían exigir un alto al plan de explotación minera en el páramo de Santurbán.  

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Leonidasgomezg/status/1240767511806779392","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Leonidasgomezg/status/1240767511806779392

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Una razón que se suma a la reacción inmediata por parte de la Autoridad es la huelga de hambre que realizaba David Guerrero, quién desde hace varios días habitaba uno de los árboles ubicado cerca a la ANLA en Bogotá, exigiendo una conversación con las autoridades ambientes y una pronta reacción al caso de Santurbán.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuventudesUPSP/status/1239571983592980480","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuventudesUPSP/status/1239571983592980480

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Mayerly López vocera del Cómite por la Defensa de Santurbán señaló que son tres solicitudes las que se presentaron a la Autoridad, la primera hace referencia a la suspensión de las adiencia de licenciamiento, *"no se ordenará el desarrollo de las audiencias públicas para el trámite del proyecto Soto Norte por la situación sanitaria actual, sin embargo cuando se levante dicha situación se procederá a la convocatoria en los casos que aplique", señala el documento expedido por la ANLA.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto López afirmó, *"**esto nos da un parte de tranquilidad, garantizando de alguna manera que este proceso siempre se va garantizar los derechos fundamentales a la participación de ciudadanía,** para que tengan la información pertinente en el tiempo de este proceso de licenciamiento"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo afirmó que acatando las peticiones del Comité junto con la Procuradoría General solicitan también que se vincule a técnicos nacionales e internacionales en el consejo consultivo que va a tomar la decisión de esta licencia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Necesitamos personas con independencia que no están subordinados al Gobierno Nacional, y que nos den garantías de que esta decisión se va a tomar bajo argumentos técnicos y no bajo intereses. **No se puede tener sólo la información de un tercero con intereses en explotar los recursos naturales de Santurbán"**
>
> <cite>Mayerly López | Vocera ambiental </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y agregó, *"una empresa no va a gastar miles de millones de pesos para presentar un estudio en que la conclusión sea que su proyecto es inviable, por eso es que estas empresas pagan esos mismos estudios ambientales para asegurar la licencia ambiental".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ésta petición la ANLA informó, *"se estudiará la solicitud y se atenderá a una pronta respuesta que será presentada el 24 de marzo, con relación principalmente con la participación de expertos nacionales e internacionales en el consejo técnico consultivo*" . (Le puede interesar: <https://www.justiciaypazcolombia.com/somos-miles-de-miles-jorge-velosa-andrea-echeverri-cesar-lopez/> )

<!-- /wp:paragraph -->
