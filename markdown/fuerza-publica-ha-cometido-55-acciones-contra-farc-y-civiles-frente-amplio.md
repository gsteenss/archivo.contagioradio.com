Title: Cese unilateral de las FARC estaría en riesgo
Date: 2015-05-04 19:44
Author: CtgAdm
Category: Nacional, Paz
Tags: Cese al fuego bilateral, cese al fuego unilateral FARC-EP, Derechos Humanos, FARC, Frente Amplio por la PAz, Fuerza Pública, Fuerzas militares, paz
Slug: fuerza-publica-ha-cometido-55-acciones-contra-farc-y-civiles-frente-amplio
Status: published

##### Foto: Contagio Radio 

En el último informe del Frente Amplio por la Paz, **cumplidos 134 días del Cese Al Fuego Unilateral de las FARC-EP**, se concluye que urge el desescalonamiento del conflicto armado, debido a que las comunidades rurales se encuentran en grave peligro.

De acuerdo al informe, desde el mes de enero de 2015  hasta el 14 de abril, la Fuerza Pública ha realizado **55 acciones operativas ofensivas, entre ataques y hostigamientos hacia las FARC-EP y contra la población civil.**

Según denuncias analizadas por el Frente Amplio, durante ese periodo se presentaron 15 ataques de parte de las fuerzas militares contra la guerrilla, 11 acciones operativas en territorios de parte de la fuerza pública, 3 cercos militares a las FARC EP, quienes a su vez realizaron 2 ataques defensivos. Además las FFMM realizaron 4 operaciones de persecución contra las estructuras guerrilleras y 8 bombardeos indiscriminados.

Las denuncias presentadas evidencian que “**hay  afectaciones y violaciones a la población civil, que atacan a la vida, la integridad, bienes públicos, así como hurtos y destrucción de bienes civiles.** Como resultado de las operaciones militares de la fuerza pública, se han producido resultados fatales contra la población civil, de ellos se reporta la muerte de 3 civiles, dos de ellos menores de edad que murieron por artefactos explosivos”, informa el Frente Amplio.

En base a ese balance que realiza, el Frente Amplio por la Paz reitera la inminente necesidad de trabajar por el **desescalonamiento** y con ello, acordar un **cese bilateral.**

Cabe recordar que el Frente Amplio aceptó la veeduría al cese unilateral de las FARC-EP desde el 20 de diciembre de 2014. A partir de ese momento ha escuchado a los actores en el conflicto, y ha puesto especial atención a las voces de las comunidades que viven en medio de la guerra.

 
