Title: Termina con victoria huelga de presos palestinos
Date: 2017-06-01 11:04
Category: Onda Palestina
Tags: Huelga, Palestina, presos
Slug: huelga-presos-palestinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/bds.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 30 May 2017 

Después de 40 días sin comida, cientos de prisioneros palestinos han suspendido su [huelga de hambre en las cárceles israelíes](https://archivo.contagioradio.com/presos-palestinos-huelga-israel/). El final de la huelga se produjo después de 20 horas de intensas negociaciones entre los líderes de la huelga, entre ellos Marwan Barghouti un importante líder del partido político Fatah, y el Servicio Penitenciario de Israel, según un comunicado emitido el sábado por la mañana por el comité de solidaridad de los prisioneros.

El comité celebró el acuerdo como una &quot;victoria para el pueblo palestino y los prisioneros en su épica defensa de la libertad y la dignidad. La declaración dice que las autoridades israelíes aceptaron algunas de las demandas de los prisioneros, pero no proporciona detalles. Sin embargo, personas del Servicio Penitenciario de Israel han declarado que el acuerdo alcanzado entre Israel, el Comité Internacional de la Cruz Roja (CICR) y la Autoridad Palestina, concedería a los presos una segunda visita familiar mensual financiada por la Autoridad Palestina.

En esta emisión de Onda Palestina hablaremos además de adhesiones al boicot cultural en Sudáfrica y al boicot académico en Inglaterra, presentaremos la primera parte de nuestro informe sobre el TLC entre Israel y Colombia, y hablaremos de un caricaturista palestino que ha tenido mucho éxito a nivel internacional. Todo esto acompañado de la mejor música Palestina.

<iframe id="audio_19009823" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19009823_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
