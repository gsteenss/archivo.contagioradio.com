Title: 32 años después Fiscalía sigue negando a los desaparecidos del Palacio de Justicia
Date: 2017-11-07 15:45
Category: DDHH, Entrevistas
Tags: Bernardo Beltrán, CIDH, Palacio de Justicia, víctimas
Slug: 32-anos-despues-fiscalia-sigue-negando-a-los-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/PALACIO-DE-JUSTICIA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Nov 2017] 

Ya han pasado 32 años de la toma y retoma del Palacio de Justicia y las familias de las víctimas que allí desaparecieron continúa a la espera de la verdad. A la lista ahora se suman los nombres de magistrados desaparecidos y se abren más preguntas, sobre quiénes **son los responsables de estas desapariciones, qué pasó con las víctimas y por qué luego de 32 años no hay verdad**.

### **“Los desaparecidos no existieron”** 

<iframe src="https://co.ivoox.com/es/player_ek_21962133_2_1.html?data=k5ammJeVd5Shhpywj5WdaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncbTVz8nfw5Cmqc3o04qwlYqldc-f2pDZ0diPt8ri1MbP0dfJt4zYxpDZw5DJstXmxszOjcnJb9PZ1Nncj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Sandra Beltrán, hermana de Bernardo Beltrán y a quien el pasado sábado 5 de noviembre le entregaron los restos de Bernardo, la respuesta de los avances de investigación por parte de la Fiscalía continúan revictimizando a los familiares al aseverar que “los desaparecidos no existieron, que los familiares deben aportar pruebas reales, porque para el Estado Colombiano, **luego de 32 años de batallas jurídicas aún tienen en duda que las personas hayan desaparecido**”.

Puntualmente lo que manifestó el fiscal delegado Jorge Hernán Diaz sobre las investigaciones de los desaparecidos fue "debemos aprender de las experiencias pasadas y aceptar que no podemos desgastar de nuevo la justicia colombiana con juicios penales que no tienen la convicción total, de que en efecto, **la persona fue desaparecida forzadamente**"

Con Bernardo son 5 las personas que han sido entregadas a sus familias, sin embargo, la recuperación de algunos de estos cuerpos ha abierto la puerta a que, otros sean desaparecidos. **Este es el caso de Bernardo, que aparece luego de la exhumación de la tumba del auxiliar de magistrado Jorge Echeverry,** ahora desaparecido. (Le puede interesar:["No hay día en que uno no se acuerde de Bernardo Beltrán: Sandra Beltrán"](https://archivo.contagioradio.com/no-hay-dia-en-que-uno-no-se-acuerde-de-bernardo-beltran-sandra-beltran/))

Además, un vídeo comprobó que Bernardo sale del Palacio de Justicia con vida, no se explica entonces por qué sus restos son encontrados al interior del Palacio, razón por la cual Sandra aseguró que el paso siguiente es continuar el proceso por la búsqueda de la verdad “hay que decirle al Estado colombiano que nos esta revictimizando y **que 32 años después no le están echando gasolina a los cuerpos de las víctimas del Palacio** sino a sus familias”.

### **La familia que nació por el dolor** 

<iframe src="https://co.ivoox.com/es/player_ek_21962177_2_1.html?data=k5ammJeVe5ihhpywj5aVaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncbHdzcbfjbPFusLm08rhx4qWh4zn0Mffx5DQpdSf1MrQ18rQpdSfx8bay9HNpdPZ1JDRx5DQpdSfxZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Durante los 32 años que han tenido que sobrellevar los familiares de las víctimas del Palacio de Justicia, se ha logrado estrechar lazos que permitieron que naciera una gran familia, que, de acuerdo con Pilar Navarrete, **compañera de Héctor Jaime Beltrán, ha servido como apoyo las veces** que ha sido necesario para continuar en la búsqueda de sus seres queridos.

“Ha sido un caso de total silencio, de impunidad, de miedo a contar, muchos familiares fuimos tildados de lo peor, todo eso hizo mella en los familiares, han pasado muchas cosas que se han reflejado en los sentimientos y el pensar de los familiares” afirmó Pilar.

### **Los incumplimientos al fallo de la CIDH** 

De igual forma, las familias han denunciado en reiteradas ocasiones que el Estado ha incumplido el fallo emitido por la CIDH, en el año 2014, sobre las medidas de reparación, además de acuerdo con Pilar Navarrete aún **no existe el protocolo en salud para tener atención especializada, ni se ha prestado atención sicosocial.**

“Esa forma de revictimizar del Estado y de atender a unos y a otros no, de seguir metiendo el dedo en la herida es la que no nos ha permitido sanar este dolor de 32 años” afirmó Pilar. (Le puede interesar: ["Gobierno incumple reparación en el caso del Palacio de Justicia"](https://archivo.contagioradio.com/gobierno-incumple-reparacion-en-el-caso-del-palacio-de-justicia/))

###### Reciba toda la información de Contagio Radio en [[su correo]
