Title: 30 años de La Rochela, 3 décadas de impunidad
Date: 2019-01-16 09:15
Author: AdminContagio
Category: Sin Olvido
Tags: crímenes de estado, Masacre de La Rochela
Slug: 30-anos-de-la-rochela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-15-at-9.02.25-AM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 17 Ene 2018 

Con actos de reivindicación por la vida este viernes 18 de enero de 2019, **se conmemoran 30 años de la Masacre de La Rochela** y tres décadas de impunidad del asesinato de la comisión compuesta por **15 funcionarios judiciales que se desplazaba por carreteras de Santander**.

Las actividades iniciarán a las 8:30 de la mañana con una **rueda de prensa** en el Complejo Judicial de Paloquemao de Bogotá; acto seguido promediando las 10 de la mañana se realizará la **proyección de un documental** sobre el caso; a las 11 a.m. una **ceremonia religiosa** y para cerrar un acto de memoria y una **ofrenda floral** cerca del medio día.

**Sobre la Masacre**

El acto de barbarie fue planificado por ganaderos, comerciantes y militares del Ejército. Los hechos se presentaron cuando una comisión judicial de **15 funcionarios se encontraba al interior de Simacota, investigando una serie de masacres y asesinatos** sistemáticos ejecutados en el Magdalena Medio.

En el camino, la comisión fue abordada por aparentes miembros de la Fuerza Pública, que luego de retenerlos, les indicaron que siguieran su camino. **Más adelante los emboscaron cegando la vida a doce de los funcionarios**, mientras que tres lograron sobrevivir al hacerse pasar por muertos.

Meses después, **Jesús Baquero Agudelo**, líder del grupo paramilitar de los Mesetos, fue capturado y confeso **cómo se planifico y quiénes operaron en el ataque paramilitar**; además los tres funcionarios sobrevivientes aportaron testimonio sobre los hechos, lo que dio apertura a una investigación.

Pese a que **la Corte Interamericana de Derechos Humanos declaro culpable al Estado colombiano en 2007**, el mismo Estado ha sido promotor de la impunidad, **de las 36 personas implicadas en el caso solo dos han sido sentenciadas**.

**En 2018**, **el caso fue archivado contra los exgenerales Alfonso Vacca Perilla y Juan Salcedo Lora** presuntos coautores de la masacre, y la Fiscalía General de la Nación concluyo que estos exmilitares no tuvieron nada que ver con lo ocurrido.

Las víctimas y sus familiares siguen a la espera de garantías de justicia y reparación. Este año se cumplen tres décadas de exigencias al Estado colombiano, pero también **tres décadas de memoria sobre lo ocurrido**.

###### Reciba toda la información de Contagio Radio en [[su correo]
