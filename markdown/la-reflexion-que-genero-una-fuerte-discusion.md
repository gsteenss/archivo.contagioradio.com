Title: Reflexión a partir de una fuerte discusión
Date: 2019-09-02 18:41
Author: A quien corresponde
Category: Opinion
Tags: Asesinato contra líderes sociales, cristianismo., Cristo, JEP, lideres sociales
Slug: la-reflexion-que-genero-una-fuerte-discusion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Con la medida que midan a los otros, los medirán a ustedes**

(Mateo 7,2)

Bogotá, 26 de agosto del 2019

 

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales,  *

*mientras exigimos a los demás que no pequen o los criticamos por pecadores.  *

 

Estimado

**Hermano en la fe**

Cristianos, cristianas, personas interesadas

En días pasados hablé con un amigo tuyo, compañero de tu iglesia y tuvimos una discusión muy fuerte.

Yo estaba indignado por el asesinato de líderes sociales y de exguerrilleros comprometidos con los acuerdos, por el incremento de la violencia a zonas que estuvieron más tranquilas gracias a los acuerdos de paz”, por los policías y militares muertos en confrontación, lo que no ocurría hacía mucho tiempo, por los ataques a la JEP, por las declaraciones engañosas de miembros del gobierno y del congreso, etc.

En la conversación tu amigo afirmó que *“los atentados contra militares, policías y cadetes son culpa de ese maldito proceso de paz y ustedes son cómplices”.*

Indignado le dije que era no cierto y dije: *“¿No era eso lo que querían los enemigos del proceso de paz, ¿de qué se quejan ahora?; detrás del cuento de* ***paz sí, pero no así,*** *lo que querían era que la guerra siguiera, para mantener sus negocios y tapar lo que había hecho; es muy fácil hacer la guerra con los hijos del vecino ¿Cuántos promotores del No al plebiscito y de los ataques contra la JEP están enviando sus hijos o sus nietos a la guerra?”. *

Con rabia y dolor, tu amigo gritó: “¡*Cómo va a querer alguien que la guerra siga, que la gente se mate, eso no lo puede decir usted!*”

Quise darle razones de mi afirmación, pero,al ver el dolor y la rabia en su rostro, no seguí la discusión.

Me quedé pensando en su dolor, más que en su rabia. Pensé que en el fondo era sincero, que no quería guerra, ni la muerte. Esto me llevó a esta reflexión. Espero la lea algún día.

Por el rostro desencajado, le dolían sinceramente los muertos y la guerra, pero su comprensión de la realidad del conflicto tenía poca consistencia. Afirmar que la culpa de esas muertes la tiene el proceso de paz riñe con la verdad; decir que, los cómplices de esos hechos dolorosos somos quienes estamos por la paz y en contra de la guerra, quienes creemos que es mejor una paz imperfecta a una guerra perfecta y prolongada, no es cierto. Creo que él es sincero en lo que dice y cree, pero **razona, decide y habla basado en informaciones tergiversadas o sin fundamento** y con una visión de la realidad, asumida sin darse cuenta, que contradice los principios y valores de la fe cristiana que proclama.

Él, y muchas personas bien intencionadas, le creen ciegamente a sus dirigentes políticos y religiosos, creen en sus mensajes engañosos repetidos de muchas formas y asumen ciegamente sus ideas por eso no ven sus fallas o defectos y los defienden fielmente, impidiéndoles escuchar la advertencia de Jesús: “*Cuídense de los falsos profetas que se acercan disfrazados de ovejas y por dentro son lobos rapaces”* (Mateo 7,15).

En la discusión con tu amigo, caí en la cuenta que muchos de quienes trabajamos por la paz con con justicia social y verdad, quienes conocemos intereses torcidos, mentirosos y anticristianos de quienes se oponen a la paz, no hemos comprendido que hay gente buena y bien intencionada que ve y vive la guerra desde otro lado, ya sea por experiencias doloras no elaboradas o por la información tergiversada y no nos hemos comunicado adecuadamente para ayudarles a ver la otra cara de esos mensajes convincentes pero engañosas que leen o escuchan.

Muchas personas como tu amigo, no se han dado cuenta, que en la guerra y en los conflictos, la primera víctima es la verdad, que cada quien cuenta las cosas desde su punto de vista, que hay intereses muy grandes y poderos con miedo a la verdad y a la justicia y por eso atacan a la Comisión para el Esclarecimiento de la Verdad y a la Jurisdicción Especial para la Paz (JEP) y se esconden detrás de discursos a favor de las víctimas y en contra de la impunidad.

Varias cosas que le dije a su amigo son una realidad, quizá debí decirlas de otra manera o en otro momento. Son hechos bastante reconocidos y documentados que:

1.  Desde antes de la firma de los acuerdos para la terminación del conflicto entre el gobierno y las FARC, los expertos internacionales alertaban sobre el peligro del retraso en su implantación porque generaba desconfianza, inseguridad jurídica y riesgos que estimulaban la deserción de combatientes, lo cual está ocurriendo y se suma al asesinato de ex guerrilleros, al retraso en los proyectos productivos y a la campaña de desprestigio del proceso, todo lo cual está incrementando la violencia en muchas regiones.
2.  Muchas personas **votaron no al plebiscito** manipuladas por la propaganda engañosa, como lo reconoció el gerente de la compaña por el No, el Dr. Juan Carlos Vélez. Después de ganar el plebiscito, los dirigentes “del no”negociaron con el gobierno y la guerrilla la inclusión, en el “nuevo acuerdo”, de puntos claves para su visión del país o sus intereses. Una vez logrado su objetivo, empezaron a atacar los acuerdos.
3.  Hay sectores con mucho poder que les interesa la guerra y por eso quieren volver a tiempos nefastos; basta recordar las mentiras y/o verdades a medias del Ex fiscal Néstor Huberto Martínez o lo dicho por algunos dirigentes: “*vamos a hacerlos trisas”*(los acuerdos), *“lo prefiero 80 veces al guerrillero en armas que al sicario moral difamando*”, olvidando que en su partido hay ex guerrilleros como Ever Bustamante, Rosemberg Pavón, entre otros.

Si quieres comprobar que  lo que te estoy diciendo es cierto basta que revises las versiones de dos o tres medios de información, así te darás cuenta por ti mismo de estas y muchas otras cosas.

La mayoría de los opositores al proceso de paz, lo atacan con mentiras o verdades a medias y lo hacen usando lenguaje y argumentos religiosos. En la práctica, muchos cristianos les creen más a esos opositores que  a la Palabra de Dios que dice: *“Bienaventurados los que trabajan por la paz, porque se llamarán hijos de Dios”* (Mateo 5, 9); *“Los que trabajan por la paz, siembran la paz y cosechan justicia”* (Santiago 3,18); *“Me alegré mucho cuando vinieron unos hermanos y dieron testimonio de tu conducta fiel a la verdad. No hay para mi mayor alegría que oír que mis hijos son fieles a la verdad* (3Juan 1,3-4); “*Guarda tu lengua del mal, tus labios de la mentira; apártate del mal, obra el bien, busca la paz y sigue tras ella”* (Salmo 34, 14-15).

Fraternalmente, su hermano en la fe,

Alberto Franco, CSsR, J&P

<francoalberto9@gmail.com>
