Title: Cambio Climático afecta con mayor fuerza a la población vulnerable del mundo
Date: 2016-10-13 15:27
Category: Ambiente, Nacional
Tags: cambio climatico, deslizamientos, inundaciones, ONU, peligro, pobreza
Slug: cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/invasion_bajo_cervantes_-martes_mm_022.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La patria 

##### 13 Oct 2016

De acuerdo con el último informe de la Organización de las Naciones Unidas, ONU, **los cambios climáticos afectan más fuertemente a las poblaciones pobres y vulnerables del mundo, **producto de la falta de planeación y medidas, por parte de los gobiernos que salvaguarden el bienestar de los habitantes.

Según el documento, esta situación de vulnerabilidad se genera debido a que los cordones de miseria se ubican en las zonas que por su posición geoespacial de tierra, **no están acondicionadas para vivir** y son proclives a sufrir deslizamientos, calores o lluvias extremas, contaminación del agua e inundaciones.

El informe indica que **durante los últimos 20 años 4.2 millones de personas fueron afectadas por estos desastres**, mientras que los países ubicados en **América Latina, el Caribe y el Sudeste Asiático** son en donde las poblaciones tiene mayor riesgo de sufrir los fenómenos producidos por el cambio climático.

Para Lenni Montiel, subsecretario general en la División de Políticas Social y Desarrollo de la ONU, “**los grupos más pobres están sujetos a los más grandes impactos de los efectos del cambio climático**. Sin duda los pequeños campesinos, sin duda las mujeres, las personas adultas, los niños que viven en áreas de especial fragilidad”, afirmó.
