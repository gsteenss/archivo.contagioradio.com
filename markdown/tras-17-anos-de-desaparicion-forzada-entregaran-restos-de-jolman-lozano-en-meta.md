Title: Tras 17 años de desaparición forzada entregarán cuerpo óseo de Jolman Lozano en Meta
Date: 2018-03-05 16:08
Category: DDHH, Nacional
Tags: desaparecido meta, desaparecidos, jolman lozano
Slug: tras-17-anos-de-desaparicion-forzada-entregaran-restos-de-jolman-lozano-en-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/jolman-lozano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Claretiana] 

###### [05 Mar 2018] 

Los días 6,7 y 8 de marzo de 2018 se llevará a cabo la entrega digna de los restos de Jolman Lozano quien fue **desaparecido el 12 de noviembre de 1999**. La entrega se realizará en Medellín del Ariari en el municipio de El Castillo en el Meta y los familiares han exigido justicia y verdad por la desaparición del integrante de una misión médica de Salud Total.

De acuerdo con la Corporación Claretiana, Jolman Lozano “fue **desaparecido por paramilitares**  cuando conducía un vehículo que transportaba una misión médica”. Su cuerpo fue encontrado en la Finca Chirajara en el Municipio de San Martín en el Meta luego del testimonio de un paramilitar.

Con esto en mente, el 16 de noviembre de 2016 se **realizó la exhumación** y la Fiscalía General de la Nación, bajo la coordinación del grupo de exhumaciones, “logró establecer la identidad plena de este cuerpo óseo” que pertenece a Jolman Lozano. Por esto, sus familiares junto con la Misión Claretiana de Medellín del Ariari y “bajo los principios de concertación y entrega digna” realizarán el acto de entrega durante tres días.

### **Sus familiares no dejaron de buscarlo año tras año** 

De acuerdo con Azucena Loaiza, hermana de Jolman Lozano, mientras su hermano viajaba hacia Villavicencio “lo siguieron dos motos y en la vía Llano Grande lo pararon y el **médico y las enfermeras** con las que iba fueron liberados”. A su hermano lo desaparecieron y en ese momento empezaron una búsqueda que duró 17 años.

Loaiza recorrió diferentes municipios del Meta buscando a su hermano y de la misma forma interpuso diferentes denuncias. Sin embargo, desde las autoridades no le dieron información y **“nunca apareció la denuncia que yo interpuse”**. Hasta el 2011, logró establecer que el caso de su hermano lo estaba llevando a cabo la Fiscalía encargada en Villavicencio. (Le puede interesar:["32 años después Fiscalía sigue negando a los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/32-anos-despues-fiscalia-sigue-negando-a-los-desaparecidos-del-palacio-de-justicia/))

En el 2017, a la familia de Jolman le informa que **él se encontraba vivo** por lo que la búsqueda de Azucena se intensificó de municipio en municipio. Meses más tarde pudo confirmar que su hermano había sido desparecido y asesinado. Sus familiares esperan que tras haberlo encontrado, “se haga justicia y se sepa la verdad”.

<iframe id="audio_24247101" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24247101_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
