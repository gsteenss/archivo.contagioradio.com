Title: En riesgo 30 familias indígenas por enfrentamientos entre paramilitares y ELN
Date: 2017-04-26 16:10
Category: DDHH, Nacional
Tags: Chocó, ELN, Enfrentamientos, indígenas, paramilitares
Slug: riesgo-inminente-para-30-familias-indigenas-por-enfrentamientos-entre-paramilitares-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/desplazamiento-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

###### [26 Abr. 2017] 

Más de 30 familias del Resguardo Indígena Jagual en Truandó, Chocó se encuentran sitiadas por el desarrollo de **operaciones armadas de paramilitares en connivencia con militares de la zona**, autodenominados Autodefensas Gaitanistas de Colombia - AGC - y por operaciones del ELN, quienes están desarrollando la instalación de minas antipersona. Le puede interesar: [Comunidades indígenas de Jiguamiando, Chocó asediadas por paramilitares](https://archivo.contagioradio.com/pparamilitares_jiguamiando_choco/)

La denuncia de los indígenas que fue entregada a defensores de derechos humanos de la Comisión de Justicia y Paz, asegura que desde **el 15 de abril el ELN ha reclutado a 6 jóvenes, de los cuales 2 son mujeres y 4 hombres** "el ELN señaló a los indígenas los lugares por los cuales la población no podría transirar porque instalaron minas antipersona para enfrentar a los neoparamilitares" dicen a través de un comunicado.

Según datos de la Comisión de Justicia y Paz, **desde 2015 las AGC instalaron un retén en Quebrada Churidó en el Chocó** y allí durante las últimas semanas "han amenazado de muerte a pobladores, entre ellos, líderes que han denunciado la situación que viven". Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

"En ese punto se realiza  un saqueo de bienes de supervivencia, se limita el tránsito libre de personas y **los paramilitares de las AGC afirman que mataran a sapos** y colaboradores de la guerrilla" concluye la denuncia. Le puede interesar: [Persiste la presencia de paramilitares sobre el Río Tamboral en el Chocó](https://archivo.contagioradio.com/persiste-la-presencia-de-estructuras-neoparamilitares-sobre-el-rio-tamboral-en-el-choco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
