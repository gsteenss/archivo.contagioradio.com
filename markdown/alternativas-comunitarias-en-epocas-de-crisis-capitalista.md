Title: AlterNativas comunitarias en épocas de crisis capitalista
Date: 2020-08-06 09:23
Author: Censat
Category: Ambiente, CENSAT, Opinion
Tags: Ambiente y Sociedad, CENSAT, Censat agua Viva, Territorios
Slug: alternativas-comunitarias-en-epocas-de-crisis-capitalista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/17-02-09-Colombia-prison-la-modelo-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/índice.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/IMG_4265.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Rios Vivos {#foto-de-rios-vivos .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Los procesos comunitarios que defienden la vida construyen las alterNativas para sembrar esperanzas en medio de las crisis multidimensionales que desarrolla el sistema capitalista.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El manejo dado a la pandemia por los gobiernos neoliberales -en particular el de Colombia- ha permitido profundizar las raíces mismas que la provocan y lanzar un salvavidas a las grandes industrias que la soportan, con el sector financiero a la cabeza, menoscabando los derechos fundamentales de la población y profundizando la desigualdad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este contexto y con la necesidad de fortalecerse, los procesos organizativos en los territorios construyen, desarrollan o se consolidan como alterNativas comunitarias desde la diversidad que les caracteriza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Enfatizamos en estas trochas de esperanza que se construyen desde lo propio, desde el fogón, la vereda, la plaza, el parque, la esquina, el barrio, lo cercano; desde los procesos de configuración histórica, ecológica, cultural y geográfica de cada territorio.

<!-- /wp:paragraph -->

<!-- wp:image {"id":87923,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/índice.jpeg){.wp-image-87923}

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### Caminos que se contraponen a la virtualización e individualización propias del sistema capitalista anteponiendo y construyendo deseos otros y sentires desde poéticas, estéticas y éticas del cuidado de lo común: la vida. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Corazonamos (pensar con el corazón) que esta crisis da la razón a las comunidades campesinas, indígenas, afros y urbano populares y los procesos organizativos que defendemos los territorios. Nos pone los pies sobre la tierra al recordarnos que la vida (que vale la pena ser vivida) se construye en comunidad, enraizando memorias, cuidando la salud, sembrando alegría, tejiendo autonomías y salvaguardando la diversidad cultural y ecológica que hemos construido como pueblos en procesos de larga duración. Por eso, más que hurgar reflexiones o prácticas “novedosas” para enfrentar la crisis, invitamos a un breve recorrido por tres territorios donde se recrean estos caminos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Iniciamos por Santander, donde las integrantes del **Movimiento Social por la defensa de los Ríos Sogamoso y Chucurí** nos plantean que la crisis no las tomó desprevenidas. Nos dicen que “*en esta coyuntura, ellas sí saben qué hacer, porque conocen la ciencia maravillosa que es la naturaleza, cómo nace y cómo crece un árbol, cómo se reproduce la alimentación*”. Nos recuerdan la importancia de recuperar y tejer memorias ambientales para enfrentar el olvido y fortalecer nuestros entramados comunales. Durante el 2017 realizaron un proceso de recuperación de memoria colectiva a través de diversos ejercicios del recordar y narrar entre niñas, niños, jóvenes, hombres, mujeres y adultos mayores de San Vicente de Chucurí, Zapatoca, Betulia, Lebrija, Sabana de Torres y Barrancabermeja. La arpillería les sirvió para plasmar sus experiencias frente al conflicto armado y el daño ambiental generado por la represa, además de tejer hilos de reconocimiento y apoyo mutuo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el Suroeste antioqueño el **Cinturón Occidental Ambiental (COA)** continúa con un trabajo pedagógico y comunicativo que les ha permitido reflexionar las causas, efectos y retos que deben asumir en la coyuntura actual.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La Escuelas de Sustentabilidad “Polinizando el territorio”, las Escuelas Agroecológicas COA y las Cátedras del Territorio han servido para continuar abonando los pensamientos y las acciones intergeneracionales e interculturales.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mediante estos procesos de educación popular y comunicación alternativa han logrado consolidar mandatos populares y planes de vida comunitarios, propuestas con las que dialogan con alcaldías y concejos municipales para la instalación de la Mesa Plan de Vida Comunitario en perspectiva de incidir en los planes de desarrollo y esquemas de ordenamiento territorial.  Además, el tejido organizativo desde las familias, les permite avanzar en la comprensión de las crisis estructurales y ambientales, reafirmando la necesidad de confrontarlas mediante la articulación local y regional para avanzar en la construcción de Territorios Sagrados para la Vida. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Cumaral, el **Colectivo Ambiental Chambires**, plantean que sembrar el alimento, cuidar la semilla, sanar-recomponer la relación con los ecosistemas y tejer comunidad desde la solidaridad, la amistad, el trueque y el diálogo de saberes, son formas de caminar las alterNativas que permitan hacerle frente a la actual crisis capitalista.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la mirada colonial del conocimiento proponen el diálogo de saberes desde las Escuelas Agroecológicas y la Bioescuela Semillas del Territorio (Educación Popular Ambiental con niñxs y jóvenes) y ante la hambruna y la destrucción de los ecosistemas avanzan en la agroecología comunal y familiar en la Reserva Natural y Centro Experimental El Conuco. En Villavicencio, la **Red de Acueductos Comunitarios ACER Agua Viva**, nos recuerda que es fundamental el cuidado intergeneracional del agua-territorio, que nos sirva para fortalecer la gestión comunitaria, las autonomías y la justicia hídrica como formas de incentivar la consciencia ambiental para el cuidado de la naturaleza y tejido comunal en torno al agua.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Las alterNativas al modelo minero energético impuesto toman cuerpo y palabra en el caminar de los procesos territoriales y entramados comunitarios que luchan por la reproducción de la vida.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Es en la defensa de sus cuerpos territorios donde se teje la posibilidad de seguir soñando, recuperando los saberes y memorias ribereñas y campesinas; autoconstruyendo barrios y veredas ligados a la tierra, el agua y las semillas, sanando desde el alimento y comprendiendo el cuidado como ética de interacción comunal. Y es desde allí donde se construirá un mundo donde quepan muchos mundos, pues la salida a la crisis no se encuentra en el individuo alienado, desnaturalizado y artificializado sino en el florecimiento de seres y procesos comunitarios conscientes de la inter-eco-dependencia que se teje con la trama multicolor de la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Mas fotos de Ríos Vivos](http://veredasogamoso.blogspot.com/2017/10/rios-vivos-santander-exposicion-de.html)

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Vea otras columnas de Opini](https://archivo.contagioradio.com/opinion/columnistas/)[ó](https://archivo.contagioradio.com/opinion/columnistas/)[n](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
