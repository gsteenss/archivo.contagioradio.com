Title: La paz en Colombia es paz con participación o no es paz
Date: 2019-08-05 16:49
Author: Foro Opina
Category: Opinion
Slug: la-paz-en-colombia-es-paz-con-participacion-o-no-es-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\*Por la Fundación Foro Nacional por Colombia

El 27 de octubre elegiremos nuevos mandatarios municipales y departamentales. No es una elección cualquiera. El país enfrenta unos desafíos que no podrán ser ajenos a la gestión de alcaldes y gobernadores: en primer lugar, la posibilidad de construir la paz en todos los rincones del país. Serán las primeras elecciones regionales que se lleven a cabo después de la firma del Acuerdo entre el gobierno y las FARC. Las autoridades elegidas deberán formular planes de desarrollo municipales y departamentales que incluyan las tareas del Acuerdo y enfrenten el reto que impone la construcción de un país en paz, con progreso y justicia social. Pero no lo podrán hacer sin el concurso de las comunidades, del sector empresarial y de los/as líderes/as sociales y políticos del lugar. Tendremos que elegir hombres y mujeres demócratas, convencidos(as) del papel que juega la ciudadanía en el logro de la paz y el desarrollo.

Las nuevas autoridades deberán, además, ayudar a resolver dos problemas: la violencia contra los líderes y lideresas sociales y la multiplicación de grupos armados ilegales en varias zonas del país. Si bien la respuesta a estos fenómenos exige el liderazgo del gobierno nacional, lo que hasta ahora no ha ocurrido, los alcaldes y alcaldesas tendrán que producir alertas tempranas, planes de seguridad y control del orden público, siempre privilegiando el respeto de los derechos humanos.

Una segunda razón de la importancia de las próximas elecciones es que por primera vez se aplicará, a nivel departamental y municipal, el estatuto de oposición, que entrega garantías e instrumentos para el ejercicio de la política a las organizaciones y movimientos que se declaren opuestos al gobierno. Ya no tendremos alcaldes y gobernadores que negocien a puerta cerrada con concejales y diputados amigos las decisiones claves en su territorio, sino que habrá voces que puedan declarar en igualdad de condiciones su desacuerdo y sus fórmulas alternativas de política pública. En otras palabras, elegiremos gobiernos municipales y departamentales, pero también a los voceros de la oposición.

En tercer lugar, estas elecciones serán la oportunidad para que los alcaldes y gobernadores impidan que el gobierno nacional siga acaparando competencias y recursos, como lo ha venido haciendo en las últimas dos décadas. Los nuevos mandatarios deberán defender el principio de autonomía territorial consagrado en la Constitución colombiana y convertirse en contrapesos del gobierno nacional. Ello no significa romper la relación con este último sino cambiar la forma de relacionarse con él, tener la capacidad suficiente de negociación para acordar, con la participación de la ciudadanía, las inversiones que permitan enfrentar los problemas de su territorio. Por supuesto, será necesario mejorar la capacidad de gestión de las alcaldías y las gobernaciones, combatir con energía la corrupción y fortalecer la participación ciudadana. Solo así dejarán de ser apéndices del gobierno nacional.

Por último, estas elecciones van a servir para que los partidos y movimientos políticos midan fuerzas en la mira de las elecciones presidenciales de 2022. En los últimos años se ha venido conformando un mapa electoral de fuerzas políticas, alineadas en torno a proyectos políticos divergentes, marcados por temas como la paz y la convivencia, el poder de los territorios, la participación ciudadana, el modelo de desarrollo, las relaciones entre el gobierno nacional y las autoridades territoriales, la justicia transicional y otros más. En medio de la polarización del país, cargada de discursos que promueven el odio y la descalificación, y marcada por la conflictividad social y el recrudecimiento de la violencia, el gran reto del próximo 27 de octubre es volver al debate respetuoso, libre de intolerancia y agresividad, centrado en las propuestas de los candidatos y en el propósito de renovar la política a la luz de valores democráticos. Ello implicará, entre otras cosas, la renovación de los liderazgos políticos, a fin de superar las componendas entre minorías políticas, clanes familiares y grupos armados al margen de la ley.

La responsabilidad es enorme. Nuestro voto podrá contribuir a afianzar el actual estado de cosas o, por el contrario, a iniciar el camino de renovación política que necesita Colombia. Por ello, es de suma importancia conocer los programas de los(as) candidatos(as), evaluar sus alcances y efectos, analizar su conveniencia y decidir cuáles son los que mejor representan el cambio hacia una Colombia de paz y progreso.

Por eso, es de suma importancia que nuestro voto sea informado y consciente. **En las elecciones del próximo 27 de octubre no podemos botar el voto.**

** **

**La Fundación Foro Nacional Por Colombia**

-   Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).

**Contáctenos: 316 697 8026 –  282 2550**
