Title: "Fiscalía no está sirviendo en el país"
Date: 2020-07-06 21:00
Author: CtgAdm
Category: Actualidad, Otra Mirada, Programas
Tags: feminicidios, Fiscalía General de la Nación, Francisco Barbosa, justicia, lideres sociales, victima de crimenes de estado
Slug: fiscalia-no-esta-sirviendo-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Investigaciones-Fiscalía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En los últimos días se han conocido diferentes acusaciones en contra de la Fiscalía y las decisiones que ha tomado en casos como la violencia de género, la ñeñe política e incluso otros temas que han dejado de lado como las agresiones a los liderazgos sociales; **a tal punto que han pedido la renuncia del Fiscal General.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Francisco Barbosa, llega luego de la renuncia de[Néstor Humberto Martínez](https://archivo.contagioradio.com/renuncia-el-fiscal-general-de-la-nacion-nestor-humberto-martinez/) en medio de cuestionamientos a las decisiones de la Corte y la necesidad urgente de tener una cabeza en la institución encargada de las investigaciones judiciales en el país. **Con poco mas de cinco meses en el cargo, hoy el país exige nuevamente una renuncia del Fiscal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El pensar que el problema del país recae solo en el narcotráfico, la minería ilegal o la presencia de grupos armados, es motivo suficiente para entender que **realmente la Fiscalía no está investigando**",* es la afirmación que hace Leonardo Diaz, Coordinador de protección del programa Somos Defensores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregando que **la Fiscal tiene sólo tres prioridades básicas, y deja de lado temas como la disputa por las tierras**, los megaproyectos, la defensa del Acuerdo de Paz, disputa por las semillas, las comunidades negras, entre otros temas, *"no desconozco la importancia de los temas que tiene en su hoja de ruta, pero es claro que estos son los únicos de competencia del Fiscal".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto el defensor agrega que la concentración de riesgos según el Gobierno está solo en 75 municipios, ***"según esto sólo en el 7% del país hay situaciones de problemáticas sociales"***; además de la declaración por parte de la Fiscalía de solo 308 casos de asesinatos de líderes sociales priorizados, de los cuales registran cerca de 180 con un esclarecimiento de 58 a 60%.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por el contrario, **según las plataformas de Derechos Humanos este esclarecimiento no pasa de un 14%,** *"hoy se habla de un esclarecimiento del 60% de los 308 casos, pero eso no es cierto, son cifras fácilmente manipulables, aquí tenemos una falla estructural por parte de la Fiscalía y qué afecta la garantía de la vida y de los Derechos Humanos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agrega que, *"la participación de Barbosa en medio de esta pandemia ha sido casi que nula"*, esto frete al aumento de casos de asesinatos de líderes sociales, y firmantes de paz cifra que en tan solo el 2020 ya haciende a los 200.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"No creemos que haya un avance con esta Fiscalía, pareciera que no hubiera un programa establecido para que el tema de los líderes sociales y Derechos Humanos avan*ce"
>
> <cite>Leonardo Diaz |Coordinador de protección del programa Somos Defensores.</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "El crimen de estado tiene casi el 99% de impunidad y a la Fiscalía no le interesa"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Luz Marina Hache, vocera Nacional del Movice, s**eñaló que ante los crímenes de desaparición forzada, desde el nombramiento de Barbosa no hay habido tampoco ningún cambio, *"históricamente el crimen de estado tiene casi el 99% de impunidad, y además a la Fiscalía no le interesa llevar investigaciones de casos de desaparición forzada"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que el organismo de investigación solo se ha limitado a entregarle los casos a la Unidad de Búsqueda de Personas dadas por Desaparecidas, *"**la Unidad no es la encargada de investigar ellos sólo tienen que ubicar a las personas desaparecidas, la investigación le corresponde a la Fiscalía quién solo ha dejado en standby todos los casos**"* , situación que según la defensora afecta aún más los procesos de búsqueda y verdad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Le decimos a la Fiscalía que ya fue suficiente el tema de la pandemia, lograron efectivamente tenernos a los colombianos en unas condiciones de aislamiento, y en donde las victimas han quedo en el olvido y la impunidad"*
>
> <cite>**Luz Marina Hache | vocera Nacional del Movice**</cite>

<!-- /wp:quote -->

<!-- wp:heading -->

"No hay justicia sin investigación"
-----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado **Elena Hernández abogada penalista** y acompañante de procesos de violencia de género, señaló que ante los evidentes ca[sos de violencia género](https://www.justiciaypazcolombia.com/42-mujeres-asesinadas-en-68-dias-de-cuarentena/) presentados en el país, *"**el objetivo de la justicia no es solo aumentar las penas, sino al contrario incrementar las investigaciones e impedir que siga en aumento la impunidad** qué es la que reina en estos tipos de casos"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó que con la creación de la ley Rosa Elvira Cely, la Fiscalía debe tener claro que el asesinato de una mujer en un contexto de violencia no es un homicidio sino como feminicidio, imputación que muchas veces es dejada de lado y que afecta directamente los procesos de justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Cuando iniciamos una investigación mal lo que ocurre es que todo el proceso que la acompaña va a ser deficiente,** y a medida que avanza el proceso esto no se va a poder mejorar"*, afirmación que hace ante la imputación que hizo la Fiscalía del caso de violación sexual de la [niña Embera](https://archivo.contagioradio.com/retiran-del-ejercito-a-sargento-que-denuncio-el-abuso-de-nina-embera/) en Risaralda, la cual fue procesada como acceso carnal abusivo y no acceso carnal violento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pasos para elegir un buen Fiscal General en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Jomary Ortegón, presidenta del colectivo abogados José alvear Restrepo** señaló los diferentes puntos que se deben tener en cuenta e la ejecución del cargo de Fiscal General de la Nación desde las denuncias de los diferentes sectores sociales. ([La llegada de un "camaleón rabioso" a la Fiscalía](https://archivo.contagioradio.com/la-llegada-de-un-camaleon-rabioso-a-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El primero paso es la selección de los candidatos al cargo**, el cuál según la abogada **debe alejarse del circulo cercano del Gobierno de turno,** además de aplicarse el [decreto 450 de 2016](http://www.suin-juriscol.gov.co/viewDocument.asp?id=30020281#:~:text=DECRETO%20450%20DE%202016&text=450%20DE%202016-,por%20el%20cual%20se%20establece%20el%20tr%C3%A1mite%20para%20la%20integraci%C3%B3n,del%20Presidente%20de%20la%20Rep%C3%BAblica.)el cuál fue derogado por el Gobierno de Duque, pero permite hacer una veeduria y participación ciudadana en donde las hojas de vida de los postulante es pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una hoja de vida que debe incluir un amplio recorrido académico y practico, en el que se debe tener el cuenta el conocimiento del Derecho Penal, además, ***"debe tener sensibilidad en temas de Derechos Humanos, además de una disposición al cumplimiento y defensa de los Acuerdos de Paz".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, debe en cuenta los procesos del Sistema Integral de Verdad y Justicia, lo que implica que la Ficalía debe seguir investigando los temas en relación a la Fuerza Pública, los crímenes de estado, el despojo entre otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tercero, *"constitucionalmente los fiscales son autónomos, es decir no deberían depender de las decisiones del Fiscal General, pero en la práctica no ocurre así";* Ortegón señaló que se debe garantizar la independencia de los procesos fiscales, *"hemos visto que hay investigaciones que se detienen de un momento a otro, y esto pasa cuando la agenda del fiscal se convierta en la agenda que el Gobierno de turno "*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Esperamos que haya una elección prontamente, esto tras la renuncia del actual fiscal que permita avanzar en los procesos de justicia en el país" .

<!-- /wp:heading -->

<!-- wp:paragraph -->

La abogada concluyó diciendo que en panorama esperanzador el fiscal Barbosa, aplicará las recomendaciones nacionales e internacionales para la correcta ejecución de su cargo, *"y pedirá perdón ante las falencias cometidas como Físcal",* pero en términos reales, ***lo correcto es que presente su renuncia, no hay forma de impartir justicia cuando sus intereses personales están por encima".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto en relación a la amistad que públicamente Barbosa a reconocido con el Presidente Duque, así como la cercanía casi que familiar con la Contraloría en donde *"la esposa de cada uno de los funcionarios que encabezan los organismos, hace parte del equipo de trabajo del otro".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo enfatizó que la ciudadanía debe exigir medidas que garanticen la elección transparente fiscal, *"esperamos que haya una elección prontamente esto tras la renuncia del actual fiscal que permita avanzar en los procesos de justicia en el país"* .

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Es hora de actuar como ciudadanía

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y en medio de las propuestas y acciones que se empiezan a cultivar desde diferentes colectivos sociales, se conoció una demanda interpuesta por dos estudiantes de derecho de la universidad Nacional, quienes solicitar una modificación del tiempo de ejecución del cargo de Fiscal General.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Período que según las estudiantes es una garantía del equilibrio de poderes, agregando a esto que *"la elección del actual fiscal resulta nociva para el ordenamiento jurídico del país",* esto en torno a los intereses que tiene Francisco Barbosa como fiscal y las acciones que se vienen realizando desde el Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el fin de lograr un diálogo equitativo entre las distintas ramas del poder y los órganos que lo conforman, por medio de esta demanda pretenden que la balanza no se incline hacia ninguno de los poderes del Estado, proponiendo que el periodo de duración de un fiscal deje de ser personal, osea de cuatro años, y pasea a ser institucional acorde con el sistema de pesos y contrapesos establecido desde 1991.

<!-- /wp:paragraph -->
