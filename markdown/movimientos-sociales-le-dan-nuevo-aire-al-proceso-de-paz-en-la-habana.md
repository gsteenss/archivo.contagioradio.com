Title: Movimientos sociales han refrescado proceso de paz en la Habana
Date: 2016-11-03 13:41
Category: Nacional, Paz
Tags: La Habana, Movimientos sociales, paz, Propuestas
Slug: movimientos-sociales-le-dan-nuevo-aire-al-proceso-de-paz-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Habana-e1478198248759.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jorge Rojas] 

###### [4 Nov. de 2016] 

Numerosas organizaciones sociales y movimientos de paz, que fueron invitados por la delegación de las Farc en La Habana para abrir un diálogo que se realizó en 6 partes, han dado a conocer recientemente los acuerdos y propuestas a las que han llegado luego de reunirse.

Dichos movimientos han expresado su **decidido respaldo a la Mesa de Conversaciones de La Habana** que ahora deberá hacer los ajustes, precisiones y explicaciones para lograr el Nuevo Acuerdo. Le puede interesa: [Más de 500 organizaciones de víctimas respaldan jurisdicción especial para la paz](https://archivo.contagioradio.com/mas-de-500-organizaciones-de-victimas-respaldan-jurisdiccion-especial-para-la-paz/)

Jorge Rojas del Movimiento Progresista y uno de los invitados a esta reunión, aseguró para Contagio Radio que **han instado y exigido que se mantenga el Cese Bilateral al Fuego y las Hostilidades y que el Nuevo Acuerdo se produzca a la mayor brevedad posible.**

Así mismo, Rojas aseveró que lo que sucedió en la Habana fue  “**un gran diálogo, abierto, sincero, desde la autonomía de los movimientos sociales, respetando la diversidad y la diferencia pero con el anhelo común de paz**” y manifestó que esta posibilidad abierta por las delegaciones ha refrescado las propuestas.

Según Rojas, el cronograma se desarrolló a través de diversos encuentros en donde el primero fue con los movimientos sociales, la segunda con partidos políticos, la tercera fue con organizaciones de mujeres, la cuarta con la comunidad LGBTI que de manera paralela se realizaron reuniones con víctimas del conflicto armado y jóvenes y estudiantes.

De igual manera Rojas aseguró que las Farc le manifestaron a los movimientos presentes en La Habana, que no darán marcha atrás en la búsqueda de la paz **“nos dijeron que no están pensando en regresar a la guerra, que lo que ya se ha hecho es muy importante para que podamos concretar la dejación de armas y la desmovilización.** De igual modo afirmaron que están abiertos al dialogo con todos los sectores, los del sí y los del no y los abstencionistas, pero no se puede prolongar en el tiempo” agregó.

Según se conoció, es probable que la próxima semana viajen a La Habana otro grupo de delegaciones de los movimientos sociales y territoriales que construyen paz.

<iframe id="audio_13603216" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13603216_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
