Title: Proceso de Paz salió fortalecido de la crisis
Date: 2014-12-02 17:00
Author: CtgAdm
Category: Uncategorized
Slug: proceso-de-paz-salio-fortalecido-de-la-crisis
Status: published

[![ImagenGrande](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2014/12/ImagenGrande-300x150.jpg){.alignnone .wp-image-178 width="780" height="390"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2014/12/ImagenGrande.jpg)  
Según Camilo González, director de INDEPAZ y del Centro de Memoria “hay que hablar ya del cese al fuego”. Hay una comisión que está encargada de pactar un cese, pero es un trabajo que no se da de la noche a la mañana. Según González eso requiere un diseño y una arquitectura que no se define en un mes o dos meses, pero ese trabajo debe comenzar ya, agrega que puede haber un acuerdo tácito en que las fuerzas armadas de suspendan operaciones ofensivas, a lo que llama “ceses unilaterales simultáneos”.

**Camilo González** 
-------------------

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2014/12/fslKiA0s.mp3"\]\[/audio\]  
 

**Jorge Restrepo** 
------------------

<p>
\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2014/12/fs1LmCA9.mp3"\]

<script><br />
$(window).load(function() {<br />
    var audioContainer = $('.mejs-audio');<br />
    if ( audioContainer.length > 0 ) {<br />
        audioContainer.each(function(){<br />
            var audioLink = $(this).find('.mejs-mediaelement a');<br />
            var audioUrl = audioLink.attr('href');<br />
            $(this).append('
<div class="download-file"><a href="' + audioUrl + '">Descargar este archivo</a></div>
<p>');<br />
        });<br />
    }<br />
});<br />
</script>
\[/audio\]

</p>

