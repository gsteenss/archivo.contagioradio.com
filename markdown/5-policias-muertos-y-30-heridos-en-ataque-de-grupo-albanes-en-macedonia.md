Title: 5 policías muertos y 30 heridos en ataque de grupo armado Albanés en Macedonia
Date: 2015-05-09 21:02
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 5 policas muertos y 30 heridos en Macedonia, Ataque grupo Albanés en Macedonia, Frontera Kosovo Serbia, Gjorge Ivanov, Kosovo, Kotevsk, Kumanovo, UCK Albania
Slug: 5-policias-muertos-y-30-heridos-en-ataque-de-grupo-albanes-en-macedonia
Status: published

###### Foto:Foxnews.com 

**5 policías muertos y 30 heridos** deja el ataque de un grupo indeterminado de origen albanés en la localidad de **Kumanovo** a 40 Km de la capital Skopje.

Según la información emitida por el ministerio del interior el ataque habría sido perpetrado por un **grupo venido de un país fronterizo,** en este caso Serbia o Kosovo, totalmente armado y organizado que ha contado en todo momento con el apoyo de la población vecina de la localidad.

El ataque iba dirigido **contra las instituciones públicas de Kumanovo**, en un intento de desestabilizar la región.

En palabras del portavoz del ministerio del interior Ivo Kotevski habría sido un "grupo terrorista bien entrenado" y  el objetivo era "lanzar ataques a instituciones estatales".

Más tarde ha informado la ministra del interior Gordana Jankulovska que había un **numero indeterminado de bajas entre los atacantes** y que una gran parte de la población había huido por el miedo a que se repitan los combates de 2001.

**Macedonia** se encuentra en una profunda **crisis económica y política** debido a los numerosos escándalos de **corrupción del partido conservador** actualmente en el gobierno.

Numerosas manifestaciones en contra del gobierno han acabado en fuertes disturbios como la del pasado jueves, además la oposición socialdemócrata ha organizado una **gran marcha en contra del gobierno para el 17 de Mayo.**

En el país ex-Yugoslavo un **30% de la población pertenece a la etnia albanesa**. En **2001** después de la guerra en Kosovo, grupos armados albaneses protagonizaron durante **6 meses combates con el ejercito Macedonio** con el objetivo de proclamar la independencia.La contienda finalizó con los **acuerdos de paz** que garantizaba la plena autonomía para la región.
