Title: Desaparición forzada una práctica sistemática y vigente
Date: 2015-08-31 23:17
Category: DDHH, El mundo, Movilización
Tags: Ayotzinapa, Campaña Nacional contra desaparición forzada, Comité Cere, Desapariciones forzadas en México, Desplazamiento en México, mexico, Transnacionales en México, Violaciones a derechos humanos en México
Slug: desaparicion-forzada-una-practica-sistematica-y-vigente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/México.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sputnik Mundo] 

<iframe src="http://www.ivoox.com/player_ek_7717054_2_1.html?data=mJyemZWZeI6ZmKialJmJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfxtGYh6iXaaO1zdnWz9SPpYa3lIqvk9SPrNbW0JClmJWPpcjmxtjW0dPJt4zX0NPh1MaPqMbaxtPg0dfJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Nadin Reyes, vocera de la Campaña Nacional Contra la Desaparición Forzada] 

###### [31 Ago 2015 ] 

[Nadin Reyes, vocera de la Campaña Nacional Contra la Desaparición Forzada, afirma que pese a que **México no cuenta con registro nacional oficial sobre desaparición forzada, la cifra de víctimas se estima entre 30.000 y 60.000.** A este alarmante incremento se suman algunas falencias con las que se enfrentan los familiares de personas desaparecidas forzadamente, relacionadas con **la inoperancia y falta de voluntad política del Estado mexicano y sus instituciones para hacer frente a la problemática**.   ]

[A la inexistencia de un censo nacional de víctimas de desaparición forzada, se suma la falta de claridad en los criterios para la investigación de los casos denunciados; la **carencia de un protocolo de búsqueda inmediata** y la **no tipificación del delito en la jurisdicción mexicana**, que perpetua la impunidad, como impedimentos para que los familiares o no puedan denunciar o sus denuncias sean atribuidas a delitos menores.]

[La confianza en las autoridades mexicanas es nula, entre otras porque **han incrementado las amenazas a los familiares de las víctimas para que no denuncien los hechos**, sumado a que en ocasiones han sido revictimizados, pues les han entregado cuerpos que no corresponden a los de sus desaparecidos, ante la falta de una institución confiable para el cotejo de cuerpos.  ]

[Reyes, asegura que estas agresiones se enmarcan en una **política sistemática generalizada desde los años 70 y agudizada actualmente, bajo la excusa de combate al narcotráfico**, pero en realidad dirigida contra los sectores sociales, particularmente los defensores de derechos humanos y extendida a los sectores no organizados, incluyendo a menores de edad. En suma es una **estrategia de terror para difundir el miedo en la sociedad y tener vía libre en la aplicación de políticas neoliberales**.]

[La documentación de los casos de **violaciones a derechos humanos**, ha permitido establecer relaciones directas entre la **implementación de proyectos extractivos** en estados como Nuevo León, Chiguagua, Guerrero, Chiapas, Hidalgo y Veracruz por parte de empresas multinacionales, con los desplazamientos, expropiaciones, asesinatos y ejecuciones extrajudiciales de las que han sido víctimas las bases sociales allí presentes. Como una muestra del **terrorismo de Estado mexicano implementado a favor de intereses económicos extranjeros**.]

[Frente al retroceso que experimentan las organizaciones mexicanas que defienden los derechos humanos, en relación con las garantías para el ejercicio de su labor, éstas han programado actividades políticas y culturales para que la sociedad civil se sensibilice ante la gravedad de la situación y las autoridades asuman medidas concretas que combatan este crimen, como la caminata programada para el 3 de septiembre en la que exigirán frente al Senado de la República que la iniciativa de ley presentada el pasado 19 de agosto, sea recibida formalmente para su discusión.   Vea también ][[Presentan proyecto de ley contra la desaparición forzada en México](https://archivo.contagioradio.com/presentan-proyecto-de-ley-atacar-la-desaparicion-forzada-en-mexico/)]
