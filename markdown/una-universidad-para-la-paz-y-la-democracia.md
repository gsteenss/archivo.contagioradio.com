Title: Una Universidad para la paz y la democracia
Date: 2016-02-10 06:00
Category: invitado, Itayosara, Opinion
Tags: educacion, proceso de paz, universidad para la paz
Slug: una-universidad-para-la-paz-y-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/universidad-para-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: Contagio Radio ] 

#### **[[Itayosara]

###### [10 Feb 2016] 

Hace unas semanas el presidente Santos visitó la Universidad Nacional de Colombia, para hablarles a los estudiantes de primer semestre que hacían su ingreso al alma mater. El propósito era básicamente venderles la bandera política de su gobierno: la paz. Fue una jornada agitada, que al día siguiente desencadeno variados artículos de opinión en las redes sociales y medios de comunicación debido a esta visita y a la inusitada respuesta de una de las representantes estudiantiles  al presidente.

Esta visita, además de  debates entre estudiantes, profesores, miembros de la comunidad académica y medios de comunicación, llegó más o menos a una misma conclusión: La universidad debe hacer un gran aporte a la construcción de  paz,  su aporte es tan significativo que hasta se propuso el auditorio León de Greiff como escenario para la firma final de los acuerdos alcanzados por la insurgencia de las FARC-EP y el gobierno nacional.

En efecto, la idea de que la universidad debe erigirse como uno de los principales escenarios para la construcción de paz no es un tema nuevo, desde el año pasado intelectuales desde todas las posiciones políticas y epistemológicas se han referido al respecto.  Sus tesis en cuanto a la universidad como escenario de paz, se han difundido ampliamente, y son tan relevantes que hasta se ha llegado a hablar de una universidad para el “post-conflicto”.  Así pues, nadie puede refutar el preponderante papel que debe tener la universidad, en particular la universidad pública para la construcción de la paz o para el post-conflicto, como quiera llamársele.

Sin embargo, a pesar de que existe un reconocimiento explícito a este rol, ninguna de las opiniones, debates o construcciones por parte de la comunidad académica han establecido claramente cómo sería la universidad que ayudaría a construir la paz. Es entonces preciso llamar la atención sobre lo que consideramos todos como una universidad para la paz.

Y el primer aspecto, que en mi parecer hay que tomar en cuenta para una universidad para la paz, es que la universidad debe ser una institución en la cual se  forma para la democracia. Un escenario de post-acuerdo devendrá en una apertura democrática que requiere la participación de todos los actores sociales, entre ellos las comunidades académicas. Entre tanto el papel de las universidades deberá considerar formar ciudadanas y ciudadanos para la democracia, y en este aspecto nos avocamos a un reto fundamental: ¿Cómo formar para la democracia y en la democracia?

Ante este interrogante, deberíamos tomar como premisa principal que la mejor manera de formar para la democracia, de enseñar democracia es precisamente en el ejercicio de esta. Es decir, solo a través del ejercicio democrático para guiar los destinos de colectividades, en este caso comunidades académicas, estamos formando para la democracia. Y un país que busca superar la guerra y abrir el espectro de la política requiere de hombres y mujeres para la democracia.

Con este horizonte de sentido, habría que evaluar sí los órganos de representación existentes en las universidades públicas, su forma de funcionamiento, y los mecanismos por los cuales se toman las decisiones están en consonancia con el principio de formar para la democracia.

Es preciso entonces una discusión más clara sobre las formas mediante las cuales se determina el destino de la universidad, una discusión que trascienda los foros, escenarios de debate y consultas, para hacer de estos u otros, parte de una serie de mecanismos que habrán de generar propuestas con repercusiones reales en la vida cotidiana de la comunidad académica. Una reforma a los estatutos generales de la universidad para que se encuentren en consonancia con los principios de la paz y la democracia parece un imperativo. Y al contrario de lo que creen algunos sectores al interior de la universidad, no hace parte de una agenda política de “las izquierdas”, sino que por el contrario es un requisito para una institución que busca hacerle aportes sustanciales a un país en busca de la reconciliación, la paz y la democracia.

Si bien, la propuesta de un proceso constituyente al interior de las universidades ha sido una demanda por parte de algunos sectores estudiantiles, esta cobra hoy en día mayor vigencia a la luz de re-pensarse la universidad como escenario para la paz, cuyo principal aporte quizás sea entregarle a la nación colombiana hombres y mujeres formadas para la democracia y en medio de ella.

Ojalá hoy en día todos los intelectuales que tienen en boca el tema de la universidad para la paz, no solo lo consideren de forma retórica y puedan aportar  a un rediseño institucional de las universidades, de manera que existan las posibilidades y garantías institucionales para que las comunidades universitarias elaboren mecanismos democráticos que reconozcan espacios de deliberación y toma de decisiones. En los cuales exista una amplia participación sin que se excluya o ignore ninguno de los actores que hacen posible la existencia del Alma Mater. De manera que en las universidades se fortalezca la subjetividad política que requiere una democracia estable y duradera, es decir, una formación académica que aporte ciudadanías para la democracia y la paz.
