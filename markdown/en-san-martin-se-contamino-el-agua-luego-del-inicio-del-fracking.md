Title: En San Martín se contaminó el agua luego del inicio del Fracking
Date: 2018-02-27 14:21
Category: Ambiente, Nacional
Tags: agua contaminada, cesar, fracking, san martin
Slug: en-san-martin-se-contamino-el-agua-luego-del-inicio-del-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fracking-e1460586646301.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Burgos Digital] 

###### [28 Feb 2018] 

Los habitantes de la vereda Pita Limón del municipio de San Martín en el Cesar, **llevan 17 días** denunciando la contaminación del agua potable posiblemente producto de las actividades de fracking. Además, manifestaron que la empresa Conoco Phillips se ha negado a entregar los resultados de análisis y afirman que no tienen la responsabilidad de entregar agua potable a los afectados.

De acuerdo con Carlos Andrés Santiago, integrante de la Alianza Colombia Libre Contra el Fracking, en ese municipio “han venido sucediendo toda **una cadena de ilegalidades** desde el inicio de trabajos de Conoco Phillips”. En reiteradas ocasiones, tanto las comunidades como los ambientalistas han manifestado que las actividades de fracking las está realizando esta empresa “sin la licencia ambiental para yacimientos no convencionales”.

Teniendo en cuenta estas actividades, las comunidades llevan tres semanas argumentando que el agua que sale de sus pozos y que utilizan para el consumo “está saliendo contaminada por una sustancia que es una **mancha de aceite**”. Sin embargo, aún no se tiene certeza de qué es y porqué está sucediendo esta contaminación.

### **Comunidades están a la espera de respuestas concretas a la problemática** 

Por esto, **ya pusieron las denuncias pertinentes**, le notificaron al ministro de Medio Ambiente, a la Agencia de Licencias Ambientales y al Ministerio de Minas. Santiago recalcó que “ninguna autoridad por parte del Gobierno Nacional se ha puesto en contacto con la comunidad” y les han dicho, desde el Ministerio que “eso no es responsabilidad de ellos sino de la ANLA”.

A pesar del ir y venir, lo que tiene claro la comunidad de San Martín es que uno de los pozos de agua “de donde se toman el agua **está contaminado**”. Han recordado que esta contaminación se detectó después de que la empresa Conoco Phillips empezara la primera fase de exploración en yacimientos no convencionales. (Le puede interesar: ["Habitantes de San Martín, Cesar denuncian contaminación en el agua tras acciones de fracking"](https://archivo.contagioradio.com/habitantes-de-san-martin-cesar-denuncian-contaminacion-en-el-agua-tras-acciones-de-fracking/))

La única autoridad que ha hecho presencia en la comunidad ha sido Corpo Cesar quienes hicieron **una toma de muestras del agua** y se comprometió con entregarle los resultados a los habitantes. Teniendo esto en cuenta, le solicitaron a la empresa el plan de manejo ambiental y los resultados de las muestras de agua de años anteriores a través de un derecho de petición al cual respondieron que no están en la obligación de revelar estos resultados.

Frente al abastecimiento de agua, Santiago recalcó que las comunidades están accediendo a ella por medio de **pozos vecinos y agua en bolsa**. Han denunciado que cuando le preguntan a la empresa qué medidas está tomando ante la problemática, “dicen que ellos no están en la obligación de responder ni de proveer agua potable a los afectados”.

Finalmente le han hecho un llamado al Ministerio de Ambiente, quien había dicho que Colombia **no está preparada para realizar actividades de fracking** pues no se conocen los sistemas acuíferos del país. Por esto, le han pedido que haga frente a la problemática y esperan que los resultados de las muestras sean presentado de manera pronta.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10155237758770812%2F&amp;width=500&amp;show_text=true&amp;height=465&amp;appId" width="500" height="465" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_24112321" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24112321_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
