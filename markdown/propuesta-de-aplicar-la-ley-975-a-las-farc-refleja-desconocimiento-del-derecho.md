Title: Propuesta de aplicar la ley 975 a las FARC refleja desconocimiento del derecho
Date: 2016-10-06 12:08
Category: Entrevistas, Judicial
Tags: Fiscal, Jurisdicción Especial de Paz, Ley 975, Paramilitarismo, víctimas
Slug: propuesta-de-aplicar-la-ley-975-a-las-farc-refleja-desconocimiento-del-derecho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/nhmartinez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: marchapatriotica] 

###### [06 Oct 2016]

La propuesta del Fiscal General Nestor Humberto Martinez, de aplicar la Ley 975 a integrantes de las FARC, **refleja un serio desconocimiento del derecho penal y también el oportunismo político** y además desconoce el sentido profundo del acuerdo que pone a las víctimas en el centro del acuerdo y resalta el derecho a la verdad como principal herramienta de combate a la impunidad.

Según el jurista Alberto Yepes, esta propuesta solamente se puede hacer cuando **hay un desconocimiento del derecho penal porque desconoce el fracaso que fue la Ley 975** en la que de 31500 paramilitares, solamente 4000 se acogieron a la ley que solamente ha producido apenas 42 sentencias que cobijan a menos de 100 integrantes de esos grupos.

Uno de los asuntos más preocupantes es que en el marco de la Ley 975 los paramilitares han confesado más de 40.000 crímenes que afectaron a 51.000 personas, entre ellos cerca de mil masacres, 25.000 asesinatos, más de 3.500 desapariciones forzosas que involucran, entre otros, a más de 1.400 agentes del Estado, **sin embargo solamente existen 42 sentencias contra 100 de los postulados y la mayoría de los crímenes está en la impunidad.**

Además para Yepes, la ley 975 fue **un golpe durísimo para las víctimas que no han tenido acceso a la verdad**, no solamente porque los máximos jefes de esas estructuras fueron extraditados sorpresivamente cuando preparaban una versión conjunta en la que involucraban directamente al hoy senador Alvaro Uribe, sino también porque las víctimas que acudieron a las versiones libres **tuvieron que soportar la burla de los paramilitares** que no reconocieron los crímenes o que simplemente afirmaban que no recordaban lo sucedido.

Así las cosas, la **propuesta del fiscal Martínez niega el sentido del acuerdo en que las víctimas están en el centro** porque impide la garantía de sus derechos a la Verdad, la Justicia, la reparación y las garantías de no repetición, sino que garantiza la impunidad para los crímenes cometidos por las fuerzas militares que aún  no han sido investigados, caso contrario al de los crímenes de las FARC que han sido investigadas y juzgadas.

<iframe id="audio_13210301" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13210301_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
