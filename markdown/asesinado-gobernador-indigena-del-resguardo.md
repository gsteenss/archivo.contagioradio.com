Title: Asesinado Gobernador Indígena de Resguardo Catrú, Dubasa y Ancosó en el alto Baudó, Chocó
Date: 2017-10-25 11:39
Category: DDHH, Nacional
Tags: Asesinato de indígenas, asesinatos de líderes sociales, indígenas, Resguardo Indígena
Slug: asesinado-gobernador-indigena-del-resguardo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/gobernados-asesinado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ONIC] 

###### [25 Oct 2017] 

Diferentes organizaciones sociales y movimientos indígenas, **denunciaron el asesinato del gobernador indígena** del Resguardo Catrú, Dubasa y Ancosó en el Alto Baudó, Chocó, Aulio Isarama Forastero. El hecho ocurrió en la tarde del 24 de octubre y se suma a los más de 25 asesinatos de indígenas que han ocurrido en lo que va del 2017. Además, continúa secuestrado el docente indígena Jhon Eriberto Isarama Forastero.

La Mesa de Diálogo y Concertación de los Pueblos Indígenas del Chocó, manifestó que los hechos ocurrieron a las 5:00 pm, "cuando **aproximadamente 5 hombres** armados identificados con prendas del ELN, del frente resistencia Cimarrón, lo intimidaron y se lo llevaron engañado diciéndole que van a hablar con los jefes de ellos".

Tras la intimidación, "2 horas después **la comunidad escucha varios tiros** a lo que cree que fue en ese momento cuando acabaron con la vida del líder indígena. Posteriormente, este grupo envía la razón a la comunidad que pueden ir a reclamar el cuerpo".

### [**La organización denuncia que un docente indígena aún permanece secuestrado**] 

La organización también denunció que Jhon Eriberto Isarama Forastero, se **encuentra secuestrado desde el 07 de octubre**.  Fue secuestrado cuando se encontraba con su familia y un grupo de hombres armados, que también tenían prendas del ELN "lo raptaron y lo llevaron a un lugar que aún se desconoce".  Indicaron que los hombre le quitaron los celulares a los demás indígenas para que no denuncien los hechos que se vienen presentado.

Frente a estos hechos, la Mesa de Concertación manifestó que a pesar de los diálogos de paz entre el Gobierno Nacional y el ELN, la violencia por parte de ese grupo "en los territorios y comunidades indígenas, **no ha cesado"**. Indicaron que además del asesinato del líder indígena, "siguen sembrando minas antipersonas y amenazando los territorios".

### **Crisis humanitaria en el Alto Baudó** 

Desde hace más de dos años varias organizaciones, con presencia en el territorio, han denunciado que la presencia de paramilitares y guerrillas en la región han **llevado al desplazamiento forzado de varias comunidades** y el confinamiento de muchas otras que se han visto agredidas en medio del fuego cruzado por el control del territorio.

Finalmente, exigieron **el retiro de los grupos armados del territorio** indígena y que se garantice la autonomía y gobernabilidad de los mismos. Además, pidieron que se cumpla el documento de acuerdo humanitario en el Chocó, contemplado en la Mesa de negociaciones de Quito. Responsabilizaron al Gobierno Nacional de los hechos ocurridos y le exigieron que "detenga la guerra".

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
