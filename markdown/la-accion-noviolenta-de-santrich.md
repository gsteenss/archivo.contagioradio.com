Title: La Acción Noviolenta de Santrich
Date: 2018-04-28 07:00
Author: ContagioRadio
Category: Abilio, Opinion
Tags: Acción Noviolenta, acuerdos de paz, FARC, santrich
Slug: la-accion-noviolenta-de-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Jesus-Santrich-en-entrevista-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### 28 Abr 2018 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

[Independientemente de los resultados dentro de una justicia colombiana empujada por la norteamericana, a la que es vano pedir garantías del  debido proceso, un desenlace que parece ser cierto es el final de la vida biológica de Santrich por su propia decisión de no ingerir alimentos.]

[Llama profundamente la atención que un hombre creyente del levantamiento armado no haya pronunciado siquiera una palabra invitando a los miembros de su  organización política a volver a las armas por los incumplimientos a los acuerdos de paz pactados entre el gobierno de Colombia y la entonces guerrilla de las FARC-EP, que están claramente documentados por informes de organismos internacionales y nacionales.]

[Por el contrario, siendo quien con más claridad y contundencia ha denunciado los incumplimientos y anticipado la persecución judicial, incluida la extradición, toma la decisión de emprender una nueva Acción Noviolenta, la huelga de hambre,  que según ha expresado a un medio masivo de información terminaría, con la inanición. Ya antes lo había hecho y no por su caso personal. Lo hizo para urgir la puesta en libertad de los presos de su organización que se habían acogido a la ley de amnistía,  permaneciendo aún hoy más de quinientos en la cárcel sin que se les aplique el beneficio a que tienen derecho.]

[El dicho popular reza que “la esperanza es lo último que se pierde”. Santrich ya perdió la esperanza en el cumplimiento de los acuerdos  más significativos y habla de la traición por parte del Estado a los mismos.]

[Eso no es nuevo. Ya había ocurrido  con casi todos los anteriores acuerdos del siglo 20 desde el de Uribe Uribe, al que asesinaron cerca de la Plaza de Bolivar, en Bogotá,  luego de pactar el final de la guerra de los Mil Días; como le pasó a Guadalupe Salcedo y a Dumar Aljure luego de pactar el fin de las guerrillas liberales en el Llano; como le pasó a Jacobo Prías Alape luego de pactar con el gobierno el fin de las Autodefensas Campesinas;  como ocurrió con la Unión Patriótica luego de pactar la creación de un movimiento político; como pasó con Pizarro luego de pactar el fin de M-19 en armas.]

[El  caso muestra que  luego de perder la esperanza queda algo más por perder: la dignidad.  Esto puede estar representando el gesto Noviolento de inmolación, para alguien que confió  en el cumplimiento de lo acordado para iniciar cambios desde la vía política y así acabar con una guerra de más de cincuenta años.]

[ Los teóricos de la Noviolencia hablan de la fuerza de la conciencia, de la indignación como motor fundamental de acciones que pretenden superar una injusticia, poniendo en riesgo la propia vida biológica, pero sin afectar la vida de quienes determinan el daño estructural o de quienes lo representan cuando deciden hacer uso de la represión. La acción Noviolenta es ética y política, no jurídica; y desobedece las leyes injustas que se construyen y acomodan para segar la tenue luz de cambios más profundos que se filtran  por los resquicios del establecimiento.]

[Gandhi estuvo preso en las cárceles de la India por luchar por la independencia de su país ante el imperio británico y ayunó hasta el  borde de la muerte, para frenar la violencia de su propio pueblo y luego entre los musulmanes y los hindúes. Advirtió que la Noviolencia estaba lejos de ceder a las injusticias  de los sectores dominantes. La vida la entregó a la causa en que creyó y no hubo poder humano que pudiera sobre su dignidad, así haya terminado asesinado.]

[La Noviolencia, como dice el teólogo Gerard Lokfink, a propósito del texto del Nuevo Testamento,  tremendamente mal interpretado de poner la otra mejilla, de caminar dos yardas cuando lo quieren obligar a caminar una y entregar manto y túnica cuando le piden dar sólo  el manto, hace este llamado: “No respondas a la violencia con violencia. Pero, cuando se haya producido la injusticia no te quedes de brazos cruzados, no adoptes una pasividad inoperante. Haz frente a tu oponente. Responde a su coacción o brutalidad con una bondad avasalladora”.]

[Las acciones  noviolentas contundentes muestran, como en este caso de Santrisch que ha respondido con entrevistas, dibujos y poemas,  que lo último a perder no es la esperanza; que después de ella queda aún algo en juego: la dignidad.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
