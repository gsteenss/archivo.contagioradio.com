Title: Ley de víctimas solo ha restituido 200 mil de 6 millones de hectáreas despojadas en Colombia
Date: 2016-06-10 14:55
Category: DDHH, Entrevistas
Tags: Desplazamiento forzado, Ley de Víctimas, MOVICE, Paramilitarismo, Unidad de Víctimas
Slug: ley-de-victimas-solo-ha-restituido-200-mil-de-6-millones-de-hectareas-despojadas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/regresan-a-las-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [10 Jun 2016] 

Al cumplirse 5 años de vigencia de la ley 1448 o ley de víctimas la situación de las propias víctimas no ha cambiado, siguen sin tener acceso a sus derechos y la restitución de tierras solamente ha logrado restituir, en el papel, 200 mil hectáreas de las 6 millones que se han calculado como despojadas en Colombia. Así lo señala la Asociación Nacional de Desplazados ANDAS, organización que hace parte del Movimiento de Víctimas de Crímenes de Estado, MOVICE.

Según Alfonso Castillo, presidente de ANDAS, la ley ha carecido de dos cosas fundamentales  para hacer efectiva su aplicación. Por una parte el **presupuesto que ha sido asignado a la Unidad de Víctimas y Restitución de Tierras por parte del gobierno nacional ha sido insuficiente** y por otra las instituciones **no han demostrado tener la voluntad política para aplicar los términos de la [ley](https://archivo.contagioradio.com/?s=ley+de+victimas)** ni para la atención prioritaria de las personas registradas y mucho menos para las que siguen llegando.

Castillo señala que el problema de la [Ley 1448 es un problema estructural](https://archivo.contagioradio.com/resarcir-a-las-victimas-esta-en-el-centro-del-acuerdo-delegacion-de-paz-de-farc/) que ha tenido fallas desde su formulación, trámite y aprobación tanto en el congreso como en el gobierno. Una de las fallas principales es que desde el primer momento hubo una ausencia de las opiniones de las víctimas que debían ser consultadas. Además la reglamentación, en cuanto a los parámetros de atención, **no ha sido aplicada con eficiencia y eficacia por las instituciones a cargo** como la propia unidad de víctimas o la fiscalía general de la nación.

Una muestra de la ausencia de la voz de las víctimas ha sido que en el asunto de las  reparaciones administrativas no se han tenido en cuenta parámetros de reparación integral de los daños causados y en la filosofía de la ley se entiende la reparación monetaria de manera individual y no colectiva como debería ser, si se entiende que el daño **causado por la guerra afectó gravemente el tejido social en que las personas fueron victimizadas.**

Castillo asegura que es necesario que haya una reforma estructural en la ley, con un proceso amplio de consulta, que inyecte recursos suficientes y que defina de manera clara los mecanismos de atención a las personas. **Hace falta voluntad política y sobre todo es necesario acabar con las situaciones que producen las víctimas como el desmonte del paramilitarismo** y el fin del conflicto armado con todas las insurgencias.

<iframe src="http://co.ivoox.com/es/player_ej_11857620_2_1.html?data=kpall5yadpGhhpywj5WdaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncaLgx9Tb1dSPh8Ln1c7ZztSPcYy1r6mutZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
