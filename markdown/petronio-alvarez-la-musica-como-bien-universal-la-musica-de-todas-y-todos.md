Title: Petronio Álvarez: La música como bien universal, la música de todas y todos
Date: 2019-08-13 11:26
Author: CtgAdm
Category: Cultura, Nacional
Tags: Cultura, pacífico, Petronio Álvarez
Slug: petronio-alvarez-la-musica-como-bien-universal-la-musica-de-todas-y-todos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IMG_9827.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: petronio.cali.gov.co] 

A propósito del lanzamiento del **Festival de Música del Pacifico Petronio Álvarez** y la participación de la Comisión de la Verdad en el evento, las comisionadas Ángela Salazar y Lucía González, abordaron esta temática desde su experiencia y conocimiento de la cultura y en particular de la región del Pacífico y los aportes que sus comunidades han hecho como una forma de reconocerse y reafirmar la identidad  de un país.

"Yo soy del Chocó y de un aguacero hacemos una fiesta y en otra cultura de pronto no lo entienden porque en el Chocó llueve casi todo los días"  relata la comisionada Ángela para expresar la espontaneidad que ha sido insignia en su pueblo desde los más pequeños hasta aquellos que acuden al arte para dar vida a sus saberes. [(Lea también: Con un canto a la mujer del Pacífico vuelve el Petronio Álvarez)](https://archivo.contagioradio.com/petronio-festival-pacifico/)

\[caption id="attachment\_72107" align="aligncenter" width="421"\]![Comisión de la Verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IMG_9827.jpg){.size-full .wp-image-72107 width="421" height="750"} Foto: Contagio Radio\[/caption\]

### La Comisión en el Petronio Álvarez 

La Comisión estableció entre sus líneas de trabajo una estrategia cultural y artística, no solo para realizar un esclarecimiento de la verdad, sino para que el país la asuma como un bien público necesario, "la idea de participar de estos eventos es llegar a otros públicos, desde otros lenguajes y narrativas y hacer entender que construir la verdad nos compete a todos", afirma la comisionada Lucia.

Es por tal motivo que a lo largo de la semana, se contarán con diversos escenarios que además de ofrecer diversos conservatorios para sus asistentes, también explorará la gastronomía, la música y el arte como formas de narrar y llegar a una verdad primaria en el Pacífico.

El miércoles 14 de agosto se realizará el lanzamiento de la canción “Yo soy la verdad” de Eliana Angulo, interpretada por la agrupación Bombo Negro con arreglos de Hugo Candelario mientras jueves 15 tendrá lugar el encuentro académico “Historias en Kilómetros para la Verdad”.

Ese día en la tarde se realizará la muestra de expresiones tradicionales del Pacífico. Cocina en vivo con Maura Caldas. **“El sabor de la verdad”** con la comisionada Ángela Salazar y el viernes 16 será la presentación de las cantaoras “Integración Pacífica”.

### El arte pese al conflicto 

La comisionada González señala que la cultura en particular la afro ha logrado expresar a través  del arte sus costumbres, deseos y voluntad, pero de igual forma a través de las canciones han sabido expresar el dolor de la guerra y las resistencias, "aún cuando están narrando las tristezas hay una fuerza vital", señala.

A propósito del paso del conflicto armado por numerosas comunidades obligándolas a dejar de lado en muchas ocasiones sus tradiciones, también es valido destacar las múltiples expresiones que surgieron para poder narrar los hechos tan dolorosos que dejó la guerra a través del arte, algo que no solo abordaron los artistas sino las comunidades como una expresión natural.

"A nosotros siempre nos han dicho que tenemos un dejo de tristeza", históricamente hemos mantenido la resistencia través de la música, del arte, agrega la comisionada Salazar.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
