Title: Amenazan a activista LGBTI de la Universidad de los Andes
Date: 2015-09-29 11:38
Category: LGBTI, Nacional
Tags: Alejandro Lanz, Sebastian Lanz, Universidad de los Andes
Slug: amenazan-a-activista-lgbti-de-la-universidad-de-los-andes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Colombia-Diversa-9.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: sentido.com 

###### [29 Sep 2015]

Desde el pasado 28 de Agosto, **Sebastian Lanz**, activista LGBTI de la **Universidad de los Andes**, viene recibiendo amenazas de muerte y de quemarlo con ácido a través de mensajes que llegan en la red social Grindr, creada para integrantes de la comunidad LGBTI. **Según la denuncia una de las peores amenazas dice “Sebastian  Lanz,  tus  días  están  contados”**.

Una  de las características de  esta aplicación  para celulares, es que jerarquiza a las personas  de su  alrededor por  aproximación territorial, por lo que el mismo Lanz concluye que el agresor  podría  encontrarse en la  misma  universidad. El activista denunció esta situación ante las directivas de la universidad y teme por su integridad ya que en uno de los mensajes afirman ***“Si me  echan de la  universidad, lo mato. Su hermano y usted son unos hijos de puta”***.

Ante el agravamiento de la situación Lanz interpuso la  denuncia ante la Fiscalía y especifica que las  amenazas tienen relación directa  con  su labor como activista de  derechos humanos  y  **atacan  a su  hermano Alejandro  quien también es  activista LGBTI.** Hasta el momento el  caso no ha  sido asignado a un  investigador, según la propia denuncia de Lanz.
