Title: Con el asesinato de Jorge Macana, El Tambo pierde un pilar de su organización social
Date: 2020-03-09 12:06
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Cauca, El Tambo, lider, Sustitución de cultivos de uso ilícito
Slug: con-el-asesinato-de-jorge-macana-el-tambo-pierde-un-pilar-de-su-organizacion-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Líder-Jorge-Macana.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este fin de semana se denunció el asesinato del líder Jorge Macana en el corregimiento de Playa Rica, El Tambo (Cauca). Según las primeras informaciones, el hecho fue perpetrado por hombres armados que atentaron contra su vida cerca a su domicilio. (Le puede interesar: ["Dos nuevos casos de asesinato a líderes sociales enlutan al país"](https://archivo.contagioradio.com/dos-nuevos-casos-de-asesinato-a-lideres-sociales-enlutan-al-pais/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jorge Macana, un líder que desde hace años trabaja por el bien de su comunidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo al testimonio de habitantes de El Tambo, Jorge Macana era un líder reconocido desde hace muchos años y su trabajo se enfocaba en las labores del campo. Él había impulsado la creación del colegio en Plata Rica, y de igual forma, trabajó por la construcción de la vía para llegar a esta zona de El Tambo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el Acuerdo de Paz, el líder le apostó a la sustitución de cultivos de uso ilícito, y hacía parte de la Mesa Departamental encargada de tratar ese tema. Adicionalmente, actualmente trabajaba en la creación de proyectos productivos, que son la alternativa para que los campesinos obtengan sus ingresos de recursos distintos a la Coca.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Playa Rica, un lugar estratégico y de difícil acceso**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El lugar en el que vivía Jorge es una zona apartada de El Tambo, para llegar hasta el corregimiento se debe hacer un viaje de cerca de cuatro horas, porque la vía aún no está en condiciones para hacer el trayecto en vehículo. Es además, un lugar con valor estratégico porque está al sur del Cauca y conecta con municipios que tienen salida al mar pacífico como López de Micay.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los habitantes señalan que en Playa Rica ha hecho presencia de manera histórica el Ejército de Liberación Nacional (ELN), y recientemente otros actores armados, aunque no hay claridad sobre cuáles. Las autoridades dijeron estar investigando este hecho, para aclarar si estos actores están relacionados con el homicidio de líder Jorge Macana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Más de 1000 familias en riesgo por enfrentamientos en el Cañón del Micay](https://archivo.contagioradio.com/mas-de-1000-familias-en-riesgo-por-enfrentamientos-en-el-canon-del-micay/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
