Title: Conflictos en Parques Nacionales deben ser subsanados integralmente por el Estado: Julia Miranda
Date: 2020-12-24 15:31
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: Julia Miranda, Parques Nacionales Naturales
Slug: conflictos-en-parques-nacionales-deben-ser-subsanados-integralmente-por-el-estado-julia-miranda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Parques-Nacionales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Parques Nacionales

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El sorpresivo relevo de Julia Miranda, quien durante los últimos 16 años ´s se ha desempeñado como directora de Parques Nacionales Naturales de Colombia ha despertado nuevos cuestionamientos sobre la gestión del Gobierno de Iván Duque en términos ambientales. Dichas dudas se incrementaron tras el nombramiento de Orlando Molano - director del IDRD durante la administración de Enrique Peñalosa - como nueva cabeza de la entidad, lo que ha desatado la molestia de sectores ambientalistas que temen sobre el futuro de las áreas protegidas y el destino de las comunidades indígenas y campesinas que habitan en dichas áreas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante su gestión, Miranda se opuso a la intervención en territorios como el Parque Nacional Sierra Nevada de Santa Marta donde conglomerados como Daabon han mostrado su interés e incluso a través de la sociedad C.I. La Samaria SAS, ocuparon con un proyecto agrícola de banano una de sus zonas, lo que llevó a sancionar desde Parques Nacionales al grupo con 4.600 millones de pesos. Mientras algunos medios afirman que la salida de la funcionaria estuvo ligada a este hecho, la funcionaria se refirió a estos hechos, además hizo un balance de su gestión y de lo que significa la llegada de una nueva administración.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: Desde 2004 usted estuvo al frente de la dirección de Parques Nacionales Naturales trabajando en la preservación de los ecosistemas naturales en Colombia ¿Cómo evalúa su gestión? ¿En qué se equivocaron ustedes, en qué acertaron?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** Fue un privilegio para mí estar todo este tiempo, casi 17 años cumplo el 5 de enero de haber sido llamada e invitada para ser la directora de Parques Nacionales, por la ministra de entonces Sandra Suarez Pérez, ministra de Ambiente del primer gobierno del expresidente Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Haber tenido todos estos años ha hecho posible que [Parques](https://www.parquesnacionales.gov.co/portal/es/) Nacionales se haya consolidado como una entidad fuerte, técnica, con un equipo magnifico, ha mejorado su presupuesto, el número de personas que conforman el equipo, pudimos declarar 10 parque nacionales, ampliar 2 veces el Santuario de Malpelo y el Parque Nacional Chiribiquete y lograr con un trabajo arduo que se incluyeran ambos en la lista de Patrimonio de la humanidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Firmamos más de 40 acuerdos con comunidades locales, comunidades indígenas y afrocolombianas, para el manejo conjunto de las áreas protegidas, donde ellos tienen sus territorios ancestrales, logramos aumentar el presupuesto exponencialmente, de una manera importantísima, gracias al apoyo de los ministros de ambiente, de todos estos años pudimos aumentar un poquito cada año, al apoyo de la presidencia de la república, de los gobiernos que he tenido la oportunidad de participar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Nos dedicamos a cumplir los planes de desarrollo de dos periodos del expresidente Uribe, dos gobiernos del expresidente Santos, y estamos muy bien encaminados para cumplir con las metas del plan de desarrollo del presidente Duque, y yo estoy segura de que se va lograr.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Parque Nacionales de Colombia se ha posicionado a nivel internacional como entidad seria técnica, que da ejemplo en muchos de los temas en los que estamos trabajando, como efectividad de manejo, turismo bien manejado, relación con los pueblos originarios y muchos temas interesantes en los que la entidad ha crecido. La sistematización, la tecnología que estamos utilizando hoy para poder hacer vigilancia remota de los parques, hemos aprendido mucho y hemos logrado adquirir imágenes satelitales, tecnología para poder lograr esa vigilancia remota.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: Usted desarrollo su trabajo a lo largo de dos administraciones, dos gobiernos de Uribe y dos de Santos, ¿Qué hizo diferente la administración de Iván Duque cuando tiene una visión ambiental similar a las anteriores?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** Estoy convencida que el presidente Duque está totalmente comprometido con la conservación de los parques así lo ha demostrado, así está en sus metas del plan de desarrollo, creo que es normal que haya cambios en las entidades, uno es servidora publica y esta esencialmente de paso, uno siempre sabe, que pueden cambiarlo y que las decisiones las toman quienes tienen la decisión política de que énfasis quieren hacer con sus funcionarios y yo respeto profundamente la decisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ya son muchos años los que he estado acá y de ninguna manera soy indispensable, lo que sí estoy segura es que la entidad técnica y fortalecida sabe para dónde va y continuará la tarea, porque no es mi tarea, es la tarea marcada por el plan de desarrollo, por las metas del convenio de diversidad biológica que es la que hemos nosotros impulsado y cumplido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: Usted es abogada con una especialización en derecho ambiental, al cargo llega Orlando Molano, arquitecto con una especialización en Gerencia de Construcciones y exfuncionario de la administración de Enrique Peñalosa, estuvo tras la renovación d el del parque El Japón que involucró una numerosa tala de árboles ¿Considera que su modelo es compatible con el perfil que requiere ser director de parques naturales o por otro lado considera que el Gobierno está apostándole a otro modelo del cuidado de los parques?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** No creo, están muy claramente establecidas las normas que rigen el sistema de Parques, lo que se puede hacer y lo que no se puede hacer, yo he conocido al doctor Molano en estas dos reuniones de empalme, no lo conocía antes y lo que puedo destacar es que es un servidor público, eficiente, que indudablemente se enamorara de los parques y podrá cumplir una muy buena tarea.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Le estamos dando toda la información que él necesita, para ubicarse muy bien, para conocer el equipo, para conocer las áreas mientras puede llegar a ellas, con la información que le estamos brindando, yo le deseo grandes éxitos, lo mejor, porque llega a la entidad más linda que tiene el Estado y estoy segura de que él se comprometerá, como servidor público a sacar adelante las metas de conservación de las áreas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Parques Nacionales y el futuro de las comunidades indígenas, campesinas y los conflictos empresariales

<!-- /wp:heading -->

<!-- wp:paragraph -->

**CR: Si hablamos de las condiciones de las poblaciones que habitan los parques en particular comunidades indígenas, sus poblaciones crecen y han tenido que expandir su frontera agrícola, ¿Qué pueden esperar los campesinos e indígenas con su salida y con el cambio drástico de administración?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**JM:** Indudablemente en los parques está claramente establecido lo que se puede y lo que no se puede hacer. Las comunidades indígenas tienen un derecho ancestral del territorio, y las ampara la constitución y la ley para hacer uso de los recursos naturales según su cultura ancestral.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Existen comunidades campesinas que estaban en las áreas de parques desde antes de su declaratoria, otros que han ido llegando y que indudablemente con las actividades agrícolas y pecuarias que ellos realizan impactan el área protegida y no están permitidas ni por la constitución, ni por la ley, con ellos hemos estado trabajando para hacer acuerdos, trabajando con otras entidades del Estado que deben garantizar el bienestar de la población rural en Colombia del campesinado, para que se puedan dar alternativas de tierra, porque el campesino sin tierra no puede desarrollarse.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Son la Agencia Nacional de Tierras y otras entidades del sector de la agricultura que deben trabajar en este tema y lograr el bienestar de la comunidad campesina en nuestro país, creo que es algo en lo que Colombia debe trabajar muchísimo más.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El otro gran reto es el trabajo con las comunidades campesinas para brindarles alternativas productivas fuera del parque o aquellas que están permitidas dentro de los parques como el ecoturismo, la restauración, la investigación científica o el aviturismo; actividades que pueden servirles para su sustento si ellos quieren quedarse en las áreas o el trabajo coordinado con las entidades del estado competentes, para hacer la oferta institucional al campesinado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cuáles serían esas recomendaciones que le daría a la nueva administración sobre la situación de las comunidades indígenas y campesinas?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** Seguir fortaleciendo la relación con los indígenas donde tenemos eso acuerdos, ellos están viviendo según su tradición ancestral en los parques, bastante tranquilos salvo los temas de orden público, están trabajando muy bien con Parques Nacionales y respecto a la población campesina, que se logré continuar y haya una respuesta institucional de estas otras entidades del sector agricultura, implementar el Acuerdo de Paz en el tema de la ruralidad, del desarrollo rural integral, que es fundamental para que Colombia consiga la paz y el bienestar de los campesinos en los territorios rurales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR:** **Parques Nacionales iba a sancionar a la empresa La Samaria S.A.S. del grupo Daabon de la familia Dávila Abondano, - uno de los mayores activos de la campaña de Iván Duque en el Magdalena - por cerca de 7.200 millones de pesos, la multa sería por haber adelantado actividades agrícolas en el territorio de la Sierra Nevada. Muchos dicen que este debate con Daabon generó su salida ¿Qué opina? ¿Desde su percepción personal cuál pudo ser ese conflicto? ¿No es un hecho muy casual?       **  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** No, yo creo que esa suspicacia no hay que tenerla para nada, es un proceso administrativo sancionatorio que impuso una multa, ellos la recurrieron y ya está firmada la resolución de la confirmación de la multa, de tal manera que no hay que tener esa suspicacia, ellos están en proceso de notificación de la resolución, y seguramente si quieren podrán demandar esa decisión, están en su derecho, pero no eso no tiene nada que ver en absoluto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Estoy convencida de que los cambios son normales en la administración pública y que si consideraron que ya yo llevaba demasiado tiempo y quieren darle la oportunidad hacer un cambio pues es lo normal que ocurra a un servidor público, eso me lo he repetido todos estos años y he estado tratando de asimilar. [(Le recomendamos leer: Plan "Artemisa" habría arrancado con desplazamiento forzado en Chiribiquete)](https://archivo.contagioradio.com/plan-artemisa-habria-arrancado-con-desplazamiento-forzado-en-chiribiquete/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mira que es duro porque uno se enamora de este trabajo, a pesar de que me repetía todo el tiempo que este momento iba llegar es duro porque uno ama esto, y es casi que la conservación de la naturaleza, no es un trabajo sino un propósito en la vida y Parque Nacionales es la gran oportunidad para trabajar en esto. Era normal, y se respeta la decisión totalmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: Son 16 años de procesos que ha adelantado durante su gestión, ¿Existe cierto temor de dejar en otras manos su trabajo de tantos años?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** No, en absoluto. Él ha manifestado todo el interés de cuidar los parques, de cumplir la ley porque es que Colombia tiene esa ventaja, yo he podido trabajar, pero es que el marco de la ley colombiana es muy importante, blindaje constitucional, legal, de la jurisprudencia de todas las cortes, la corte constitucional, el Consejo de Estado, la Corte suprema de justicia, todos se han pronunciado, a lo largo de los años protegiendo los parques, entonces los funcionarios públicos lo que tienen es que acatar esas normas y desarrollar lo que allí está establecido, eso es lo que hace uno y pues quien siga tendrá que hacer lo mismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cuáles serían esos conflictos empresariales que asume el nuevo director y que se les debe hacer seguimiento y no dejarlos de lado?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** Yo no los llamaría conflictos empresariales, las actividades no permitidas en los parque son actividades ilegales, esta permitidos en los parques la conservación, la restauración, la investigación científica, el ecoturismo, todo lo demás no está permitido, de manera que actividades agrícolas, pecuarias, de infraestructura, puertos carreteras, no están permitidas, es un conflicto con la ilegalidad y con el reto que tiene Colombia de lograr la gobernabilidad de su territorio, que pueda intervenir, y mantener la ley y el orden de todas las esquinas del país y de todos los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esa es tarea no solamente es de Parques Nacionales, (...) la conservación de la naturaleza es una función del Estado, los guardaparques están allá, tratando de hacer su tarea de autoridad ambiental, de control y vigilancia, de investigación científica, de monitoreo de la riqueza natural, pero indudablemente todo lo que tiene que ver con cultivos de uso ilícitos, ganadería y obras ilegales le compete al Ministerio de Defensa, a la Fiscalía y a otras entidades. No puede tildarse esa responsabilidad a los guardaparques en ningún momento; ese conflicto sí existe, pero debe ser subsanado integralmente por las entidades del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: Son 59 áreas protegidas, de ellas  42 son parques nacionales, tiene 12 que son asignadas como santuarios de flora y fauna, y son muchos los conflictos que existen ¿Cuántos conflictos asume el director con otras empresas? qué pasa con otras empresas que hay en los parques?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** ¡Claro! Son 59 parques nacionales y 3 distritos nacionales de manejo integrado, un poco más de 20 millones de hectáreas que son nuestra responsabilidad, le dimos la información completa de todos los temas, presupuestales administrativos, técnicos, todo se le ha dado y habrá más reuniones de profundización de los diferentes temas, las reuniones las hicimos con los seis directores territoriales, con el equipo de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Yo creo que él está ya con bastante información, pero nuestro deber es darle la información completa, llevamos ya dos días de empalme y todavía quedan muchas reuniones más, Parques es inmenso, es muy complejo, y tomará un buen tiempo mientras él puede captar toda la dimensión de lo que tiene de trabajo por delante. [(Le puede interesar: Las pistas tras el asesinato de la ecóloga Nathalia Jiménez y su esposo Rodrigo Monsalve)](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR:** **Para 2019, sabíamos que eran al menos 12 los parques nacionales en los que sus empleados han recibido amenazas por parte de grupos armados. En la actualidad, ¿Cómo concluye ese tema?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**JM:** Sí, es un tema muy preocupante que viene siendo tratado con el presidente, el ministro de Defensa, los comandantes de las Fuerzas Militares y la Fiscalía, se ha entregado la información y se está manejando desde allí. En este momento hay varios sitios muy difíciles, en donde se ha recrudecido la situación de seguridad para los guardaparques, este año fue difícil desde el comienzo. A finales del año pasado incluso se dieron unos asesinatos de guardaparques y de líderes ambientales que trabajaban con nosotros en las áreas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

No hay duda de que se ha recrudecido la situación de seguridad en varios sitios, si bien en otros se ha mantenido la seguridad y por eso hemos podido abrir el ecoturismo en varios parques, hay otros como los del sur del Amazonas, como el Parque Catatumbo, Paramillo, Nukak, Puré, La Paya,  Yaigojé Apaporis, que son parques que todavía tienen muchos problemas. [(Lea también: Asesinan a Yamid Silva, funcionario de Parques Nacionales en El Cocuy)](https://archivo.contagioradio.com/asesinan-yamid-silva-funcionario-parques-nacionales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Miren ustedes lo que ocurrió en La Macarena recientemente, el director de la territorial de Cormacarena asesinado en el propio casco urbano del municipio en su oficina; aquí el Estado colombiano tiene un gran reto de recuperar los espacios de control territorial que se han ido perdiendo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
