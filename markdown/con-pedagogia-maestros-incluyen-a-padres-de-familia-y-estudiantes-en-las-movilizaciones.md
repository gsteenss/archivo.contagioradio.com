Title: Padres, madres y estudiantes respaldan el paro de docentes
Date: 2017-06-09 12:49
Category: Educación, Nacional
Tags: educacion, fecode, Paro de maestros
Slug: con-pedagogia-maestros-incluyen-a-padres-de-familia-y-estudiantes-en-las-movilizaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-8-800x5501.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Jun 2017]

Los maestros y maestras del país, han desarrollado actividades de pedagogía para mostrarle a los padres de familia y a los estudiantes que **el paro no se está haciendo sólo para pedir aumentos salariales**. Le han explicado a la comunidad que el Sistema General de Participación necesita más recursos para garantizar una educación de calidad.

<iframe src="http://co.ivoox.com/es/player_ek_19177494_2_1.html?data=kp6emZyYfZWhhpywj5WZaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaLgxt3O0MnJtoy70NPnh6iXaaKlzcrgh5enb9Tjw9fSjdfJt9HVzcncjcnJb9HVxdfS1ZDFsIzkwtfcjcmRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En asambleas locales, con clases abiertas, los padres y madres han tomado la palabra para defender los derechos a la educación de sus hijos. Según Alexander González, profesor de arte del Colegio Minuto Buenos Aires, en la localidad de Ciudad Bolívar, “**los padres han manifestado que entienden de que se trata la lucha** **de los profesores** y nos sentimos respaldados por ellos”. (Le puede interesar: ["Maestros y maestras ¿un paro de tradición?"](https://archivo.contagioradio.com/maestros-y-maestras-un-paro-de-tradicion/))

<iframe src="http://co.ivoox.com/es/player_ek_19177707_2_1.html?data=kp6emZybdJihhpywj5WaaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpCrpdPuhqigh6eXsoampJDg0cfWqYzhwsrg1tfTt4zl1sqYxdTSuMrihqigh6elpc-fxNTbjcbHuMrqjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De igual forma, los padres y madres, luego de casi 4 semanas de movilización, le han pedido al gobierno que **fomente las condiciones que les garantice a sus hijos desarrollar a plenitud su derecho a la educación.** Manuel Garzón, profesor del Colegio Eduardo Umaña en Usme afirmó que “hay una población pequeña, el 5%, en Bogotá de estudiantes y maestros que no han parado sus actividades porque están realizando labores artísticas y deportivas que hacen parte de diferentes proyectos”.

**Maestros reforzarán las movilizaciones**

<iframe src="http://co.ivoox.com/es/player_ek_19177605_2_1.html?data=kp6emZyadJahhpywj5WbaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncaXV187RjajJtsbm0IqfpZDXs8PmxpDfw8nNp8Lgyt_Oxc6Jh5SZo5jbjcnJsIzkwtfcjcnJb87Vxtjh1NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para David Cerero, profesor en Bogotá y miembro de la Asociación de Educadores ADE, “las declaraciones del gobierno y las amenazas de la Ministra de Educación, ha hecho que **radicalicemos la nuestra postura en las calles”**. Los docentes han dicho que atenderán el llamado de Fecode a endurecer la movilización para lograr que se inviertan recursos a la educación. (Le puede interesar: ["Gobierno está "repartiendo la pobreza" pero no aumenta presupuesto: Fecode"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/))

De igual forma, anunciaron que los padres de familia y los estudiantes acompañarán las movilizaciones y plantones que se realizarán el día de hoy en la Secretaria de Educación de Bogotá. Según Cerero, “**hoy hay invitados 20 colegios de la localidad de Kennedy para acompañar los desplazamientos por la avenida el Dorado”.**

Cerero destacó el hecho de que en el día de **ayer los senadores hayan discutido en el Congreso la situación del Sistema General de Participaciones**. Para los maestros, las declaraciones que se han hecho en los medios de información, no afectan al magisterio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
