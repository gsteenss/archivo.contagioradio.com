Title: Extractivismo, negociaciones y paz
Date: 2016-04-07 06:29
Category: CENSAT, Opinion
Tags: ELN, extractivismo, gran mineria, Plan Nacional de Desarrollo
Slug: extractivismo-negociaciones-y-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/extractivismo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 7 Abr 2016

La formalización de los diálogos de superación del conflicto armado entre el gobierno colombiano y el Ejército de Liberación Nacional -ELN- permite retrotraer y poner sobre la mesa algunas de las reflexiones frente a la cuestión ambiental y la paz en el país.

Varios son los temas que han estado en la preocupación del ambientalismo por la relación entre el proceso de negociaciones para el fin de la guerra y los conflictos socio – ambientales que se acentúan y amplían con la imposición del modelo de desarrollo actual basado en el extractivismo. No obstante el respaldo irrestricto que debe primar para que el conflicto armado, social y político sea solucionado a través del diálogo y la búsqueda de una salida política negociada, es necesario presentar los aspectos emergentes en las tensiones ambientales.

Primero. Asalta la preocupación de que en un posible pos-acuerdo se entregue a las trasnacionales grandes porciones del territorio del país que estuvieron bajo el control de las insurgencias, y se dé paso a la explotación agenciada vía megaproyectos de carácter extractivo.

Segundo. Las propuestas en términos ambientales que se han esbozado y han permeado incluso la construcción del Plan Nacional de Desarrollo (2015 - 2018), orientan la institucionalización de un modelo de economía verde que tiene como algunos de sus postulados: la concepción de la naturaleza como capital natural; supuestos esquemas de minería sostenible con la participación de las comunidades y los excombatientes; fortalecimiento institucional ambiental a partir de los ingresos obtenidos de Pagos por Servicios Ambientales -PSA- y Reducción de Emisiones por Deforestación y Degradación -REDD; y la estructuración de un modelo productivo sin sujetos campesinos y con proyecciones de proletarización del campo bajo gobernanzas de carácter corporativo.

Tercero. Mientras se avanza en la construcción de las condiciones de posibilidad para el cierre de las negociaciones con las Fuerzas Armadas Revolucionarias de Colombia -FARC-, y se instala oficialmente la mesa de negociación con el ELN, arrecia la violencia contra las y los defensores de la naturaleza. En los últimos días, una decena de defensores de derechos territoriales han sido asesinados, con lo que se hace manifiesto el riesgo para las poblaciones, organizaciones, procesos y movimientos que se oponen a la destrucción extractiva y defienden los territorios de agua y vida.

La cuestión ambiental que hasta el momento ha sido observada en las negociaciones de forma superficial y aleatoria, permitiendo que una visión mecantilista y una concepción de la naturaleza como capital natural prevalezca, debe ser abordada con la integralidad que le caracteriza y deben tomarse en cuenta las propuestas que diversos sectores ambientales han construido con las comunidades, llenando los vacíos intencionales que sobre el tema se han extendido desde el gobierno colombiano en los diálogos con las FARC.

El reconocimiento de la naturaleza como víctima del conflicto armado en Colombia, como condición necesaria para la reparación integral de las víctimas humanas de la guerra, y la implementación de una comisión ambiental de la verdad como garantía de reconstrucción de la memoria histórica de las afectaciones ambientales de la guerra, permitirían una aproximación ética a la posibilidad de reconciliación de la sociedad con la naturaleza.

Esperamos que las negociaciones que se avecinan incluyan estas perspectivas, y la participación social, proyectada también desde espacios como la Mesa Social para la Paz, se exprese en la decisión autónoma sobre el futuro de los territorios.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
