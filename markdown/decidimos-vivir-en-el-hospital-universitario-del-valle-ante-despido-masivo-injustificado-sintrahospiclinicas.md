Title: "Decidimos vivir en el Hospital Universitario del Valle ante despido masivo injustificado" Sintrahospiclinicas
Date: 2016-11-01 13:01
Category: Movilización, Nacional
Tags: Crisis en Hospital Universitario del Valle, Crisis hospitalaria, Trabajadores
Slug: decidimos-vivir-en-el-hospital-universitario-del-valle-ante-despido-masivo-injustificado-sintrahospiclinicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/hospital-del-valle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:UTL Víctor Correa 

###### [1 de Nov 2016] 

**Trabajadores del Hospital Universitario del Valle decidieron quedarse permanentemente en las instalaciones del lugar hasta que no se plantee una solución frente a los despidos masivos** y la masacre laboral que se está generando con la suspensión de 947 puestos de trabajo.  Además, aseguran que servicios médicos como ginecología, pediatría, entre otros, se están ofreciendo intermitentemente a la ciudadanía por falta de personal.

Jorge Rodríguez, presidente del Sindicato Sintrahospiclinicas, anunció que **desde el fin de semana pasado y hasta una próxima decisión asamblearia, se mantendrán al interior de las instalaciones**. Rodríguez denunció que ayer en horas de la noche la Policía intento desalojar a las personas que se encontraban en el lugar y **amenazó con sacar, con lista en mano, a los trabajadores que están en el Hospital.**

El funcionamiento del Hospital también se ha visto afectado con la crisis, **de 500 camas hospitalarias se pasaron a 400 y hasta anoche se re activo el ingreso de pacientes**. De acuerdo con Rodríguez áreas tan importantes como la **USI de Neurocirugía están cerradas, mientras que otras funcionan intermitentemente**. Además al cerrar áreas como lavandería o costurero, lo que vendrá son contratos con otras empresas para que presten el servicio en modalidad de tercerización laboral. Le puede interesar: ["Hospital Universitario del Valle se queda sin 947 puestos de trabajo"](https://archivo.contagioradio.com/hospital-universitario-del-valle-sin-947-empleos/)

“Esta paz construida por Dilian Francisa Toro, no le da oportunidad al trabajo, burla las garantías y las vulnera, **esta es una paz sin salud ni trabajo y de familias en una angustia total**” afirmó Jorge Rodríguez.

Los trabajadores expresaron que a pesar de que sigan recibiendo notificaciones de despido seguirán acudiendo al hospital a cumplir con sus labores. De igual forma Sintrahospitales señaló que **hay en trámite una tutela por violación a los derechos laborales que esperan solvente la situación de aproximadamente 547 trabajadores**. Estudiantes del a Universidad del Valle y del SENA, también han estado visitando a los trabajadores y expresando su solidaridad con la crisis hospitalaria del Valle del Cauca.

<iframe id="audio_13569680" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13569680_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
