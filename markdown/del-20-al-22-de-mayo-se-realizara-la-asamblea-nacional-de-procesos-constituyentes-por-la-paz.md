Title: Comunidades en Colombia le apuestan a la constituyente por la paz
Date: 2016-05-20 13:40
Category: Movilización, Nacional
Tags: Asamblea Nacional Consituyente, Movimiento social
Slug: del-20-al-22-de-mayo-se-realizara-la-asamblea-nacional-de-procesos-constituyentes-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/constituyentes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RadioMacondo] 

###### [20May 2016]

[Desde el 20 hasta el 22 de mayo se realizará en Bogotá, la **Asamblea  Nacional de Constituyentes por la Paz,** un escenario que pretende recoger las experiencias de construcción de mandatos y ley en todo Colombia, ]y que recibirá a más de **500 personas de todas las delegaciones del país.**

Este proceso comenzó hace 4 años y ha generado más de **70 ejercicios de constituyentes en 18 departamentos del país**, a nivel local, regional y sectorial, con la finalidad de construir mandatos que puedan servir como [iniciativas para la conformación de una nueva Constitución democrática y popular,](https://archivo.contagioradio.com/inician-las-cumbres-de-paz-por-el-territorio-colombiano/) en una Colombia que se desarrollará en el marco de la paz.

Esta primera Asamblea Nacional busca **recoger y evaluar todos los procesos que se han venido gestando para proyectar una ruta de trabajo más concreta**, además este escenario contará con la participación de diferentes sectores del movimiento social que permitirá intercambiar [lecturas para  analizar la coyuntura actual frente a posible escenario de constituyente](https://archivo.contagioradio.com/constituyente-propondra-una-nueva-politica-antidrogas/) Nacional. Posteriormente se tendrá un escenario de trabajo y con propuestas de cuales son las transformaciones claves para una Paz Sostenible y duradera en Colombia.

De acuerdo con Andrés Gil, vocero del proceso de Constituyentes Nacionales por la Paz, este es un momento en donde las personas del común están desde abajo **generando propuestas e iniciativas con un alto contenido de movilización** y por otro lado, se esta viviendo una falta de gobernabilidad en el Estado Colombiano, que permiten que escenarios como este sean de vital importancia para el fin del conflicto armado.

"Tenemos un momento más crítico que el que se tenía en la crisis que genero la constituyente del 91, estamos en un momento en donde las fuerzas militares y policiales están en un grado de corrupción, descontrol y permeadas hasta lo más profundo por la mafía y el crimen. Además tenemos una situación económica y social que da cuenta ca da vez más de la falta de presupuesto o recursos bien manejados, **esto ubica un momento de transformación**" afirma Andrés Gil.

Sin embargo, asegura que para que se blinde un proceso constituyente nacional es necesario que se genere una mayor participación popular y que a su vez existan condiciones como un momento político favorable para las transformaciones democráticas desde un pacto que incluya a todas las fuerzas políticas del país.

**Esta será la agenda de trabajo:**

<div>

<div>

<div>

**Viernes 20 de Mayo**
</p>
• 7:00 am a 4:00 pm Inscripciones de las/os delegadas/os en el lugar de la apertura de la Asamblea Nacional de Procesos Constituyentes.

• 9:00 am a 3:00 pm II Foro Dialoguemos por la Asamblea Nacional Constituyente • 4:00 pm a 4:30 pm Mística de apertura.  
• 4:30 pm a 6:30 pm Panel de apertura: Proceso Constituyente en Colombia.  
• 6:30 pm a 7:00 pm Actividad Cultural

**Sábado 21 de Mayo**  
• 8:00 am a 11:00 am Paneles sobre Legislaciones Alternativas y Populares construidas en los últimos años por parte iniciativas constituyentes.

Se desarrollarán cuatro (4) paneles simultáneos cada uno con los siguientes temas: Jus- ticia social; Problemáticas Rurales; Participación política y; Verdad, Justicia y Reparación donde distintas iniciativas constituyentes expondrán sus propuestas de Legislaciones Alternativas y Populares. Cualquier proceso, organización o iniciativa que participe en la ANPC puede presentar sus propuestas respecto a Legislaciones Alternativas y Populares en cualquier tema de los paneles de forma escrita, desarrollando las siguientes preguntas orientadoras: ¿Cuáles son los principios o fundamentos de su legislación alternativa o popular? y ¿Cómo se relacionan sus propuestas de legislación alternativa o popular frente a los acuerdos o salvedades de La Habana?

*•* 11:00 am a 1:00 pm Trabajo en grupos para la construcción de principios fundamentales de una constitución democrática para la paz con justicia social.

Se desarrollarán quince (15) grupos de trabajo simultáneos, en los que se construirán principios fundamentales de una Constitución democrática para la paz con justicia social partiendo de los siguientes temas: Nueva Democracia, Derechos y Garantías, y formas de Estado y Ordenamiento Territorial. Cualquier proceso, organización o iniciativa que participe en la ANPC puede presentar sus propuestas para la construcción de principios fundamentales de una constitución democrática para la paz con justicia social de forma escrita, teniendo en cuenta los temas anteriormente mencionados.

</div>

</div>

</div>

<div>

• 12:00 m a 2:00 pm almuerzo

• 2:00 pm a 5:00 pm Continuación del trabajo en grupos para la construcción de principios fundamentales de una constitución democrática para la paz con justicia social y ambiental.

**Domingo 22 de Mayo**

• 8:00 am a 1:00 pm Trabajo en grupos para la elaboración de lineamientos para una hoja de ruta que potencie el Proceso Constituyente Democrático y popular.

En los mismos quince (15) grupos de trabajo simultáneos del día anterior, se construirán propuestas para una hoja de ruta que potencie el Proceso Constituyente Democrático y Popular. Cualquier proceso, organización o iniciativa que participe en la ANPC puede presentar sus propuestas de forma escrita.

• 12:00 m a 2:00 Almuerzo  
• 2:00 pm a 5:00 pm Plenaria de conclusiones, mística y cierre.

<p>
<iframe src="http://co.ivoox.com/es/player_ej_11605030_2_1.html?data=kpajkpqUd5Ghhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLixdeSpZiJhZrnjKzWzpCRb6Tjz9jhy9nZvcbi1crgjbPFp8rjz8bZx9iPtNDmjNHOjbXFvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>

</div>
