Title: "Águilas Negras" amenazan a ambientalistas que rechazan actividades de Anglogold Ashanti
Date: 2015-10-05 16:36
Category: DDHH, Nacional
Tags: Aguilas Negras, Anglogold Ashanti, Asociación de Productores Agroecológicos de la Cuenca del Rio Anaime APACRA, Colectivo Socio-Ambiental Juvenil de Cajamarca, Comité Ambiental en Defensa de la Vida del Tolima, COSAJUCA, Juan Manuel Santos, La Colosa Cajamarca, La Corporación Conciencia Campesina, Red de Comités Ambientales del Tolima., Unión Campesina
Slug: aguilas-negras-amenazan-a-ambientalistas-que-rechazan-actividades-de-anglogold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.youtube.com 

###### [05 oct 2015] 

El movimiento ambientalista del  Departamento del Tolima que rechaza la presencia de la empresa Anglogold Ashanti, denunció que los ambientalistas han sido blanco de amenzas por parte del grupo paramilitar "**Las Águilas Negras".**

De acuerdo a la denuncia, el pasado 30 de septiembre de 2015 “las organizaciones **Comité Ambiental en Defensa de la Vida del Tolima, el Colectivo Socio-Ambiental Juvenil de Cajamarca COSAJUCA, La Unión Campesina, La Corporación Conciencia Campesina y la Asociación de Productores Agroecológicos de la Cuenca del Rio Anaime APACRA;** recibieron un correo firmado por las Águilas Negras donde amenazan la integridad de algunos de sus integrantes.”

Los ambientalistas tolimenses “rechazan que en Colombia, un estado social de derecho, se sigan privilegiando los derechos particulares de empresas mineras por encima de los derechos colectivos al ambiente sano, la vida, el agua y la participación ciudadana”.

Debido a ese panorama, **exigen el esclarecimiento de los hechos, que se otorguen las medidas de protección y respaldo** que garanticen el trabajo, la integridad y honra de los defensores y defensoras.
