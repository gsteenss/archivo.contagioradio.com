Title: Revista Kabai una apuesta desde los estudiantes para la construcción de paz
Date: 2017-10-13 11:41
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad Nacional sede Medellín
Slug: revista-kabai-una-apuesta-desde-los-estudiantes-para-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/IMG_28291.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Direkta] 

###### [13 Oct 2017] 

Estudiantes de la Uninversidad Nacional, sede Medellín, presentaron la versión 22 de la Revista Kabai, una iniciativa que surgió hace veinte años y que tiene por objeto presentar un análisis, desde la academia, de las diferentes reivindicaciones del movimiento social y las transformaciones que deben darse para **gestar una Colombia que posibilite un escenario de paz**.

De acuerdo con Iván Álvarez, integrante de la Revista, han existido muchas dificultades para poder continuar con este proyecto, una de ellas ha sido la falta de apoyo institucional, que cada vez se hace más difícil con la reducción de presupuesto y que, según el estudiante de maestría, **es una muestra del proceso de privatización que afronta la Universidad pública en todo el país.**

Para el lanzamiento de esta edición de la revista, contaron con la participación de ponencia de egresados de la universidad y docentes, como lo son Sara Manuela Graciano, autora del artículo “Conflictos territoriales en el escenario posacuerdo: el extractivismo minero como amenaza a la paz”; Bibiana Ramírez, periodista de Prensa Rural y la docente **Carolina Jiménez, que escribió el artículo Negociaciones de paz y luchas populares en Colombia**. (Le puede interesar: ["Colombia le cierra las puertas a la investigación y a las ciencias"](https://archivo.contagioradio.com/recorte-a-ciencia/))

De igual forma, Álvarez, señaló que para esta versión de la revista los estudiantes se enfocaron en plantear el debate sobre la paz y las dificultades que continúan presentándose en el país para que sea una realidad, **como el paramilitarismo que sigue manteniendo un control territorial en las comunas de Medellín** o los retrasos en el cumplimiento de la implementación del acuerdo de paz, en el Congreso de la República.

**Con 22 años de historia los estudiantes de la Universidad Nacional sede Medellín,** expresaron que seguirán en la construcción de este espacio que le ha permitido tener una memoria del movimiento social que necesita con urgencia de la financiación pública de la universidad. (Le puede interesar:["Así fue la movilización en defensa de la Educación"](https://archivo.contagioradio.com/estar-seran-las-rutas-de-movilizacion-de-la-marcha-por-la-educacion/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
