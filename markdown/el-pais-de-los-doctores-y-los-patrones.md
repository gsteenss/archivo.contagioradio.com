Title: El país de los doctores y los patrones
Date: 2016-04-20 12:11
Category: invitado, Opinion, superandianda
Tags: doctorado, Peñalosa
Slug: el-pais-de-los-doctores-y-los-patrones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/penalosa-y-el-doctorado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: primiciadiario 

#### **[Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

###### Abr 19 2016 

En las clases populares colombianas ser patrón es el máximo puesto, y en la clase privilegiada, ser doctor es la máxima distinción. Pero la figura del doctor y el patrón, a pesar de sus diferentes contextos sociales, comparten un papel similar, el de ser admirados, atendidos y dirigir; cada uno desde su campo, aparentemente.

[Entonces, somos el país de los doctores y los patrones, dos personajes que identifican nuestra cultura servil, donde los que tienen algún grado académico, sin padrino o patrocinio empresarial, no alcanzan un lugar en la repartida burocracia. En Colombia siempre gana el más fuerte, el mejor contactado, el que demuestre que su astucia o su palanca, pueden más que el pobre iluso que piensa que los puestos públicos se ganan con postulaciones  en concursos honestos. Así funciona Colombia, sin mucho talento y  con viveza, con ventaja, sabiéndose “meter en la colada”.]

[Básicamente, la figura del patrón surge de la clase humilde y maltratada, es el que demuestra a su círculo social que sí se puede llegar más lejos de manera fácil y rápida, solo es cuestión ganarse el respeto, de saber hacer las cosas, y de ser admirable para su fanaticada. En cambio, la figura del doctor, de la clase privilegiada, nace con estrella, con apellido y casta política, tiene por herencia las influencias y conexiones suficientes que le permiten no mostrar tanto papel, solo es necesario un discurso bien elaborado, que contenga respuestas y palabras que confirmen el nivel social al que pertenece, los viajes que ha hecho, las universidades que ha pisado -así no las haya pisado nunca-  demostrar que  sabe de todo así no sepa nada, alardear de su conocimiento para  que su palabra no sea puesta en duda; aunque algunos antisociales y terroristas, a veces, se atrevan a hacerlo.]

[Recientemente, el caso del doctorado de Enrique Peñalosa, es un claro ejemplo, que aunque un "doctor" colombiano mienta todo el tiempo, respecto a su título académico, aun así, su proceder y conocimiento en la administración pública, no se ponen en duda, su "mentirita" es justificable  y comprensible, dado que en este país macondiano, solo es cuestión pertenecer al  sector godo y doble moral para que sus equivocaciones sean minimizadas, hasta parezcan chistosas, y así, por debajo de cuerda, pasadas por alto. El carisma de clase no se improvisa, ni en el doctor ni en el patrón, personajes con una personalidad arrolladora, amados por el jet set y la clase popular.]

[Lo de los patrones tienen otro estilo, mientras a los doctores los redimen los medios de comunicación y las familias decentes, a los patrones los redime su gente, que ve en ellos unos héroes dignos de ser imitados. Patrones como los hermanos Castaño, Pablo Escobar o cualquier narco que hable con verraquera, ése hasta puede ser presidente y es capaz de callar de un solo fuetazo a quien le contradiga.  ]

[La historia de Colombia, es una repartición de tierra, de puestos, de política y de sociedad,] [entre los  patrones y los doctores, que empleando el miedo y el poder, han sabido manejar al país macondiano; país que se administra como finca y empresa privada. En definitiva, las  alianzas de las dos caras sociales, han sido fundamentales para que nuestro sistema “democrático” funcione: mientras unos hacen el trabajo sucio, otros lo legalizan.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
