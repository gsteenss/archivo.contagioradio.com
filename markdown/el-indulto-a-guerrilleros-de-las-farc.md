Title: Indultarían a 30 guerrilleros de las FARC-EP
Date: 2015-11-22 13:09
Category: Nacional, Paz
Tags: alto comisionado para la paz, FARC, indulto guerrilla de las farc, Juan Manuel Santos, Mesa de conversaciones de paz de la habana, proceso de paz
Slug: el-indulto-a-guerrilleros-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cese-unilaterla-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Josecardenas] 

###### [22 de Nov 2015] 

A través de un comunicado de la oficina del **Alto Comisionado para la paz** se conocieron nuevos avances en el proceso de paz, entre ellos se encuentra el i**ndulto a 30 guerrilleros de las FARC** que se encuentran  recluidos en diferentes cárceles del país.

Entre los guerrilleros que se les concederá el indulto, "*es decir, un perdón de la pena*" ninguno de ellos se encuentra pagando penas por delitos graves, su pena en prisión es por rebelión. Según el comunicado las personas que  se verán beneficiadas de estos gestos de confianza serán apoyadas con programas  psicosociales y un acompañamiento constante para la estabilización familiar.

Igualmente a través del comunicado se indica la creación de brigadas de salud que atenderán a 106  reclusos de las FARC y  la creación de patios especiales para los integrantes de este guerrilla donde será atendida su  situación jurídica, personal y familiar para una futura reintegración a la vida civil.

Este gesto de confianza se da al cumplirse 3 años de diálogo entre la guerrilla de las FARC y el Gobierno nacional,  y en un momento de [preocupación de la sociedad civil por el estancamiento del proceso de paz](http://bit.ly/1MXsnrT).

[Comunicado del Alto Comisionado para la Paz ](http://bit.ly/1lcJlYD)

 
