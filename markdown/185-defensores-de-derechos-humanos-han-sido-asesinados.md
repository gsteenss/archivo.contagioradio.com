Title: 185 defensores de derechos humanos han sido asesinados en el último año
Date: 2016-10-26 15:13
Category: DDHH, Nacional, Uncategorized
Tags: Asesinatos, Derechos Humanos, muertes, paz
Slug: 185-defensores-de-derechos-humanos-han-sido-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Defensores-Asesinados-e1477508845527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Nuevo Siglo 

###### 26 Oct de 2016 

**Al menos 185 defensores de derechos humanos alrededor del mundo perdieron la vida en 2015. De esta cifra, 122 estaban en Latinoamérica,** con estos datos OXFAM dio a conocer **un informe titulado** ***El riesgo de defender:** La agudización de las agresiones hacia activistas de derechos humanos, *en el cual entre otras cosas, expresan su profunda preocupación por el aumento de violencia contra defensores.

En América Latina, cientos de mujeres y hombres han optado **por defender los derechos humanos y el territorio**, sin embargo, en medio de esa opción de vida, deben enfrentar la violencia de grupos armados y de intereses empresariales, así como la represión que busca sistemáticamente eliminar su labor como defensores.

Aida Pesquera, directora de OXFAM en Colombia, manifestó que no existe ninguna semana en la cual no reciban una notificación de un ataque por la labor que defensores de derechos humanos realizan “**en este informe estamos denunciando que la violencia hacia defensores de derechos humanos en América Latina es lamentable. Colombia es uno de los países que infortunadamente puntea el ranking”. **Le puede interesar: [54 defensores de Derechos Humanos fueron asesinados en Colombia durante 2015](https://archivo.contagioradio.com/54-defensores-de-derechos-humanos-fueron-asesinados-en-colombia-durante-2015/)

De igual manera, en este informe OXFAM identificó que **son las mujeres que defienden el territorio contra multinacionales y las que acompañan a víctimas de violencia de género, quienes más se ven hostigadas y asesinadas** y expresó “esto es un fenómeno creciente, bien sea porque ahora denuncian más – las mujeres - y porque ellas han estado al frente de la defensa de sus territorios al ser cabezas de familia”.

Otro de los temas encontrados en el informe de OXFAM, es la estrecha relación entre intereses económicos y agresiones contra defensores. La organización asegura que, **los líderes y lideresas que defienden sus territorios de intereses económicos sufren constantemente afectaciones a su integridad física** “estos – las multinacionales - vienen detrás del petróleo, del oro, de la minería en general. Colombia desafortunadamente tiene ejemplos muy significativos” manifestó Pesquera.

**El secuestro de la democracia**

De esta manera, ha titulado OXFAM el capítulo del informe en el cual se interesaron por mostrar que el rol de los Estados en el mantenimiento de la violación a los derechos humanos y de los intereses particulares que se benefician de él, demuestran que su función primordial está siendo desvirtuada a favor de intereses económicos y políticos particulares, prácticas discriminadoras y excluyentes. Le puede interesar: [Saldo en rojo para los derechos civiles y políticos en Colombia](https://archivo.contagioradio.com/saldo-en-rojo-para-los-derechos-civiles-y-politicos-en-colombia/)

Con este informe OXFAM hizo un llamado a los Estados de América Latina, al sector privado y a la sociedad civil a reflexionar sobre la importante labor que realizan los defensores de derechos humanos y a actuar de manera efectiva contra este incremento de violencia y represión.

<iframe id="audio_13490355" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13490355_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
