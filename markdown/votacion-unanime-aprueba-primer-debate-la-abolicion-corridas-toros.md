Title: Votación unánime aprueba en primer debate la abolición de las corridas de toros
Date: 2017-05-30 17:57
Category: Animales, Nacional
Tags: corridas de toros
Slug: votacion-unanime-aprueba-primer-debate-la-abolicion-corridas-toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/no-a-las-corridas-de-toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Contagio Radio 

###### [30 May 2017]

[Este martes, la Comisión Séptima de la Cámara de Representantes, aprobó en su primer debate el proyecto de Ley que busca prohibir las corridas de toros. En la discusión **se incluyó el coleo, las riñas de gallos, las corralejas y la pesca deportiva**, excluidas en las leyes que penalizan el maltrato animal.]

[**Con una votación de 11 contra 0,** todos los congresistas que asistieron al debate aprobaron el proyecto. Mientras que la bancada del **Centro Democrático no asistió a votar** el proyecto de autoría del exministro del Interior, Juan Fernando Cristo y el senador Guillermo García Realpe, del Partido Liberal. [(Le puede interesar: Centro Democrático busca volver patrimonio cultural las corridas de toros)](https://archivo.contagioradio.com/centro_democratico_corridas_toros/)]

### [Deuda histórica con los animales] 

[Al debate asistieron diferentes organizaciones animalistas que están trabajando desde todos los frentes para lograr la abolición de las corridas de toros en el país. Una de ellas es Natalia Parra, directora de la Plataforma ALTO, quien expresa que el país tiene una deuda histórica con los animales, y por ello [ es necesario que haya una Ley que prohíba espectáculos donde hay maltrato animal, por ende]

<div class="text_exposed_show">

[Para el senador Guillermo García Realpe, se trata de un gran paso para que se acabe con ese tipo de espectáculos públicos. Asimismo, asegura que **esta vez se está avanzado de manera más consistente que en ocasiones anteriores,** cuando ya se había intentado debatir este tipo de propuestas.]

["El congreso ha hecho caso a las manifestaciones de los defensores de los animales. Hacemos un llamado a la ciudadanía a **que sumemos esfuerzos para que en Cámara y Senado se saque adelante la iniciativa",** indica el congresista, quien resalta que los taurinos no son minorías, y que la bancada animalista del Congreso trabaja para que esta Ley sea un hecho. ]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

</div>
