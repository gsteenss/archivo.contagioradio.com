Title: Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños
Date: 2019-11-20 15:41
Author: CtgAdm
Category: DDHH, Nacional
Tags: Chocó, colombia, Convención de Derechos del Niño, Derechos de los niños y niñas, Desplazamiento forzado, educación pública américa latina, Naya, Putumayo
Slug: comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-5.31.00-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-5.31.01-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-5.31.00-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado 20 de noviembre se celebró en múltiples países el **Día Mundial del Niño**, fecha en la que hace 30 años la ONU, aprobó la  Declaración de los Derechos del Niño **uno de los tratados más ratificados en la historia** (195 países); en Colombia existen territorios como la  Reserva Campesina de la Perla Amazónica, las comunidades chocoanas en Pichimá Quebrada y el territorio étnico del Naya en el Valle del Cauca que fomentan la protección de los niños y niñas y como la violencia a repercutido en su desarrollo.

**(Le puede interesar:[Por medio de dibujos y fotografías, niños Embera reconstruyen la memoria](https://archivo.contagioradio.com/por-medio-de-dibujos-y-fotografias-ninos-embera-reconstruyen-la-memoria/))  **

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-5.31.01-PM-1024x683.jpeg){.aligncenter .size-large .wp-image-76844 width="1024" height="683"}

### **El amor a las raíces campesinas**

Los niños, niñas  y adolescentes en la Zona de Reserva Campesina de la Perla Amazónica en Putumayo gozan del privilegio que solo ofrece el campo, una cercanía que les permite dar un valor real a los elementos que componen el ambiente y que les permiten una convivencia directa con los mismos,  según Rubén Pastrana, joven integrante de la reserva, "el campo se presta para que el niño se puedan recrear de una manera más sana y no este expuesto a tantas problemáticas que se ven en las ciudades". (Le puede interesar: [Con sus historias niños visibilizan las violencias que amenazan su libre desarrollo](https://archivo.contagioradio.com/con-sus-historias-ninos-visibilizan-las-violencias-que-amenazan-su-libre-desarrollo/))

Por otro lado, el entorno rural está expuesto a altos índices de empobrecimiento que hay en los campos y zonas de reserva campesina en consecuencia del abandono estatal de la ruralidad; así como peligros que enfrentan los niños con la presencia de grupos armados, "no sabemos exactamente qué grupos son, y esto llena de incertidumbre y miedo a los padres que toman decisiones para  sacarlos del campo y mandarlos a la ciudad", afirmó Pastrana.

Según el obispo de la Diócesis de Mocoa- Sibundoy, Luis Albeiro Maldonado,por lo menos 150 niños habrían sido reclutados por los grupos armados ilegales en Putumayo en los últimos 20 meses,  causando un aumento en el abandono de los campos por parte de la juventud, “difícilmente un niño que sale a estudiar a la ciudad regresa al campo o a las zonas de reserva campesina, entonces los pocos que se queden van a ver una reducción de profesores, y de recursos de manutención y sostenimiento de las instituciones educativas , perjudicando el derecho a la educación”, sostuvo Pastrana.

Las soluciones que se han trabajado desde la Zona de Reserva Campesina según Pastrana, son actividades que buscan fortalecer otros aspectos de la vida de las y los niños, en ese sentido se han buscado ocupaciones de carácter artístico con actividades como estampado, pintura, danza y avistamiento de aves. Asimismo, existe un trabajo para desarrollar la comprensión de sus derechos, el sentido de pertenencia por la comunidad, por su territorio, la organización y la reserva, “acciones puntuales que generen impacto y que los jóvenes reconozcan el valor de los territorios y abran la perspectiva de amor a las raíces campesinas”.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-20-at-5.31.00-PM-1-1024x683.jpeg){.aligncenter .size-large .wp-image-76845 width="1024" height="683"}

### Los niños en Pichimá Quebrada, Chocó reclaman su territorio

El desplazamiento forzado por causa de los frecuentes enfrentamientos entre grupos armados que se disputan el territorio o que operan bajo el mandato de empresas que  buscan apropiarse de los recursos de una región a costa del despojo o abandono de tierras es una de las grandes violaciones que afectan directamente al núcleo familiar , según el último informe de la Unidad de Manejo y Análisis de la Información en Colombia (Umaic), 25.512 personas fueron desplazadas de su territorio en el 2018, 6.674 afrodescendientes, 2.871  indígenas y  3.718 menores de edad. (Le puede interesar:[Por medio de dibujos y fotografías, niños Embera reconstruyen la memoria](https://archivo.contagioradio.com/por-medio-de-dibujos-y-fotografias-ninos-embera-reconstruyen-la-memoria/))

En el caso de la comunidad indígena de Pichimá Quebrada en Chocó, son más de 90 familias las que llevan 5 meses y 16 días fuera de su territorio. Según **Guillermo Peña, comunero y parte del acompañamiento juvenil e infantil de la comunidad**, las y los jóvenes son los que permanentemente insisten en retornar a sus hogares, “la comunidad se encuentra vulnerada, queremos retornar lo más pronto posible, y para eso requerimos apoyo de las instituciones estatales, generando planes de protección y adaptación"

Destacó que explicar a los niños y niñas esta situación de desplazamiento es difícil, y aún más ante la imposibilidad, por el momento de volver a sus lugares de arraigo, educación y entretenimiento como la escuela y la casa de la cultura, lugares que significa mucho para ellos, “tenemos problemas con temas del lenguaje y la adaptación al contenido académico, causando un rendimiento muy bajo, la juventud se siente preocupados porque quieren mucho su colegio, su comunidad y su territorio, ahí se sienten libres para practicar sus métodos de enseñanza, deporte y danza”.

Fruto de este desplazamiento, no solo se vulnera los derechos a la educación, el acceso a la salud,  a la libertad de expresión  y a una vivienda digna están directamente relacionados. [(Pichima Quebrada, dos veces desterrada por la guerra)](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

### El Naya, un territorio étnico que vela por los derechos de los niños y niñas

Según Hugo Mondragón, integrante de la Fundación Kiango en el Naya, Valle del Cauca,  la ausencia de garantías en la protección de la comunidad, luego de la entrega de armas del frente 30 de las FARC y la no continua ocupación de la Fuerza  Pública en estos territorios, permitió la proliferación de disidencias que permanecieron en la zona en función de negocios ligados al narcotráfico, causando una situación de riesgo relacionada a la utilización de los niños y niñas en el marco del conflicto vulnerando su derecho a la paz.

Mondragón también destacó que algunos derechos vulnerados a los menores son el derecho al registro pues "muchos niños por las condiciones del territorio no logran ser registrados a tiempos, a su vez este derecho a la identidad los cohíbe de tener derecho a salud"; también se quebranta el derecho a la educación , **"a pesar de haber instituciones, estas no cuentan a menudo con las condiciones adecuadas y generan riesgos de seguridad"**. (Le puede interesar: [Todas reunidas no vamos a olvidar, vamos a reconciliarnos”: Mujeres del Río Naya](https://archivo.contagioradio.com/todas-reunidas-no-vamos-a-olvidar-vamos-a-reconciliarnos-mujeres-del-rio-naya/))

Frente a esta vulneración, también existen alternativas y formas de resistencia por parte de las comunidades, el Naya un territorio étnico por excelencia y con presencia de resguardos indígenas e importantes organizaciones de mujeres y jóvenes, viene desarrollando temas de acompañamiento a los niños, "organizaciones que han trabajado por la sensibilizarnos alrededor de la cultura, orientado a los valores del pueblo negro, como hermandad unidad y poder, resinificando la visión de las comunidades de frente al territorio", afirmó Mondragón.

La violencia en estos territorios ha vulnerado por generaciones a los integrantes más pequeños de las comunidades, ahora y gracias a los esfuerzos de sus integrantes, es posible dar una solución a muchos problemas y garantizas cada vez más el respeto a sus derechos,  y las obligaciones que tienen para responder de manera autónoma ante padres, profesores, profesionales de la salud, investigadores y los propios niños y niñas.

(y la repsonsabilidad del estado)

<iframe id="audio_44570170" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44570170_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
