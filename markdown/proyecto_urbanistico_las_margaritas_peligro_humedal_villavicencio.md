Title: Proyecto urbanístico tiene en grave riesgo el humedal más grande de Villavicencio
Date: 2018-01-12 14:29
Category: Ambiente, Voces de la Tierra
Tags: Cormacarena, Humedal Kirpas Pinilla La Cuerera, humedales en riesgo, Las Margaritas, Villavicencio
Slug: proyecto_urbanistico_las_margaritas_peligro_humedal_villavicencio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Supensión_proyecto_en_Kirpas-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Informando] 

###### [12 Ene 2018] 

En grave peligro continúa el **Humedal Kirpas Pinilla La Cuerera,** ubicado a 6 kilómetros de la capital del Meta y paso obligado de aves migratorias y fauna de la región, debido a la permanencia de las obras ilegales de construcción del proyecto de urbanización denominado Las Margaritas, pese a que estas habían sido suspendidas por orden de Cormacarena.

Así lo denuncia un comunicado firmado por varias organizaciones y ambientalistas de esa zona del Meta, que buscan proteger este humedal, ubicado en el sector sur oriental de la ciudad de Villavicencio, a 6 kilómetros del centro de la ciudad, y que cuenta con una extensión aproximada de 293 hectáreas. Cormacarena, ha señalado que se trata del humedal más grande de la capital del Meta, y **cumple diversas funciones hidrológicas,  biológicas y ecosistémicas en el municipio.**

### **El avance de las obras pese al llamado de Cormacarena** 

Meses atrás, Cormacarena había hecho una visita ocular, en la que evidenció que dentro del área de conservación se encontraban realizando una intervención del suelo en 16,215 metros cuadrados, mediante la disposición de material de río para relleno, adecuación de cinco vías, instalación  de tubería en [PVC]{.caps}, cinco pozos o cámaras de inspección, de los cuales tres se hallaron fundidas para la disposición de aguas residuales de las viviendas que se pretendían construir en el proyecto.

A pesar de la suspensión formal de la obra por parte de la autoridad ambiental, lo que se han venido encontrado los habitantes de la zona es que el proceso de construcción en esa área no ha cesado. De acuerdo con la denuncia de las organizaciones ambientales, en el mes de diciembre **se transportó material de relleno al lote Las Margaritas, también hubo excavación hacia el caño La Cuerera y se ubicaron tubos transportadores de aguas negras hacia la fuente hídrica**. Asimismo, se observó la tala total de los arboles de la ronda del caño La Cuerera que se ubicaba sobre ese predio, y la toma de posesión con encerramiento sobre la ronda del caño La Cuerera.

### **La importancia del humedal** 

De acuerdo con estudio de la Corporación Universitaria de Meta, el Sistema de Humedales Kirpas - Pinilla La Cuerera es el hogar de diversidad de especies de flora nativa (**17 familias y 36 especies arbóreas) y fauna silvestre, como micos, 113 especies de aves, 15 de ellas exclusivas del sistema.** Además hay especies arbóreas tales como: cedros, cubarro, ceibas, floramarillo, diversas palmas como moriche y choapo.

El humedal también hace parte de la cuenca del río Ocoa y se incluye dentro de la microcuenca del caño La Cuerera. De manera que allí, los peces son uno de los grupos faunísticos del humedal Pinillas-Kirpas.

### **Amenazas a los defensores del humedal** 

Sumado a la situación ambiental, se viene denunciando que paralelamente representantes de la comunidad que defienden el Distrito de Conservación de Suelos han recibido amenazas a su integridad y vida.

"**Hacemos un llamado a las autoridades para que se ejerza el control pertinente sobre el área protegida para evitar estas infracciones y se proteja la vida de las personas** amenazadas y a la ciudadanía en general para que solicite por medios escritos, redes sociales y emisoras, la defensa del patrimonio ambiental público de nuestra ciudad", señalan en el comunicado.

\[caption id="attachment\_50477" align="aligncenter" width="548"\]![Imágenes de los adelantos de la construcción](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/humedal_villavicencio.png){.size-full .wp-image-50477 width="548" height="494"} Imágenes de los adelantos de la construcción\[/caption\]

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). {#reciba-toda-la-información-de-contagio-radio-ensu-correoo-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-porcontagio-radio. .x_MsoNormal}
