Title: JEP preservará los archivos del DAS
Date: 2018-11-06 16:47
Author: AdminContagio
Category: DDHH, Nacional
Tags: crímenes de estado, das, JEP, víctimas de crímenes de Estado
Slug: jep-preservara-los-archivos-del-das
Status: published

###### [Foto: Contagio Radio] 

###### [06 Nov 2018] 

En una reciente decisión**, la Jurisdicción Especial para la Paz señaló que conservará los archivos del DAS**,  al considerarlos como fundamentales para las tareas que desarrollarán la Comisión de Esclarecimiento de la Verdad, la Convivencia y la No Repetición, CEV y la Unidad de Búsqueda de Personas dadas por Desaparecidas, UBPD.

Determinación que ha sido respaldada por organizaciones defensoras de derechos humanos y víctimas, al considerar que esta acción responde a la necesidad de **"garantizar efectivamente los derechos de las víctimas a la verdad, a la justicia, a la reparación y a la no repetición"**, como lo hicieron saber a través de un comunicado de prensa. [(Le puede interesar: "JEP otorga medidas cauteles a los archivos del DAS"](https://archivo.contagioradio.com/jep-otorga-medidas-cauteles-a-archivos-de-inteligencia-del-das/))

De igual forma las organizaciones y víctimas manifestaron que la decisión también acata el llamado que hicieron frente al riesgo que corrían estos archivos de desaparecer o ser alterados y que fue constatado por la Sala de Reconocimiento de Verdad y Responsabilidad .

"En la actualidad no hay claridad sobre la gestión documental que se viene desarrollando frente al material contenido en los archivos; la falta de certeza sobre el material disponible, cuál ha sido depurado y en qué forma, así como el procesamiento de documentos antiguos, su conservación y consulta mediante la tecnología actual. Tampoco existe claridad sobre cuál es el material clasificado, y si se han aplicado protocolos de seguridad para el acceso y consulta. Todas estas falencias son las que pretenden corregirse con las medidas cautelares adoptadas por la JEP" aseguraron en el comunicado.

Asimismo, expresaron que la "sociedad colombiana debe conocer todos los archivos de inteligencia y contrainteligencia relacionados con graves violaciones a derechos humanos" y agregaron que con esta decisión se da un paso significativo por tratar de develar uno de los **"episodios de violencia sociopolítica cometida desde el máximo organismo de inteligencia y contrainteligencia del Estado en contra de amplios sectores sociales** y de oposición".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
