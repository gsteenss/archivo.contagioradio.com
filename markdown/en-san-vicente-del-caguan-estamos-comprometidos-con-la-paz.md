Title: "En San Vicente del Caguán estamos comprometidos con la paz"
Date: 2016-09-20 21:04
Category: Nacional
Tags: Acuerdos de paz en Colombia, Diálogos de La Habana, FARC-EP
Slug: en-san-vicente-del-caguan-estamos-comprometidos-con-la-paz
Status: published

###### [Fotos: Contagio Radio  ] 

###### [20 Sept 2016 ] 

El primer indicio de estar acercándose al sitio en el que se realiza la X Conferencia de las FARC-EP es un letrero con una flecha que apunta hacia El Diamante, solamente 3 horas después de salir del centro urbano de San Vicente del Cagúan. Allí se congregan desde el sábado y hasta el viernes, 69 guerrilleros y guerrilleras, y más de 200 visitantes.

[![x-conferencia-farc-3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-3.jpe){.alignnone .size-full .wp-image-29585 width="1280" height="855"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-3.jpe)

Por el camino, el poblado de Campo Hermoso recibe a los visitantes ocasionales con rostros de sencillez y algunos con atisbos de esperanza, la mirada de Alipio en la tienda "la mejor esquina" es de esas que dan la bienvenida, no solamente porque la visita indica una posible venta sino porque esa visita no es como cualquier otra, indica, para él, que algo bueno está pasando o está por pasar.

[![x-conferencia-farc-4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-4.jpe){.alignnone .size-full .wp-image-29584 width="1280" height="720"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-4.jpe)

Mientras visitábamos a Alipio y conversábamos con él acerca de sus esperanzas, se acercó la señora Ruth a pedirnos un campo para llegar hasta el Diamante. Ruth, con tono bajo y mirando de un lado a otro, nos dijo que al caserío de su sobrina ya llegaron amenazas de las Águilas Negras advirtiendo que cuando salieran las FARC-EP ellos iban a llegar a limpiar el terreno, "de todas maneras no tengo miedo, el día de la muerte es el día de la muerte y de ese no se escapa nadie, ni ellos mismos", refiriéndose a los paramilitares.

Dos horas después, el caserío Las Damas apareció anunciando que veníamos por buen camino. Allí Rodrigo, poblador de la zona, nos aseguró que venía la paz, y expresó que no solamente es una cuestión de Colombia sino que es el mundo entero y que nuestra paz también es para ellos. Mientras pelaba una de "las mejores piñas de la región" nos contó que lleva viviendo en San Vicente 47 años y que pase lo que pase, está por la paz y comprometido con ella. Lo difícil viene después "pero para eso también estamos listos" y agregó que el jugo de la piña recién pelada evita el cáncer de pulmón, y nos ofreció un sorbo que tomamos con gusto para continuar.

Casi dos horas después y luego de 4 flechas hacia El Diamante asomó una pancarta en la que se rememora a Alfonso Cano que murió en medio de los esfuerzos para iniciar el proceso de paz que ahora culmina. Pocos metros adelante está la pancarta oficial de la X Conferencia de las FARC-EP, la histórica, la última de las FARC-EP como guerrilla y la primera como movimiento político.

![cano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/cano-e1474479844431.jpg){.size-full .wp-image-29632 .aligncenter width="749" height="500"}

[![x-conferencia-farc-2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-2.jpe){.size-full .wp-image-29586 .aligncenter width="583" height="559"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-2.jpe)

La inscripción bajo el sol inclemente y luego la travesía entre una maraña de periodistas, estudiantes y visitantes para llegar hasta el campamento guerrillero. Algunos nos dijeron que ese era el "parque temático"  y seguramente así lo ven muchos y muchas. Sin embargo para Bernardo, oficial de servicio de uno de los campamentos guerrilleros e integrante del Frente XV de las FARC, es muchísimo mas que eso, "ésta es la oportunidad para que ustedes conozcan que somos mucho más humanos" asegura, y agrega que está dispuesto a asumir lo que su organización le exija y a hacer política como siempre ha querido.

"No queremos que se termine el trabajo de las tropas como hizo el M- 19, la idea es que ningún guerrillero quede desamparado, mientras queramos permanecer en el colectivo, vamos continuar trabajando juntos", dice.

[![x-conferencia-farc-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-1.jpe){.alignnone .size-full .wp-image-29587 width="1280" height="855"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-1.jpe)

<iframe id="audio_12999719" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12999719_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ya en campamento, cuando nos mezclamos periodistas con la guerrillerada, ese sueño de una Colombia en paz, se ve plasmado también en las palabras de las guerrilleras. Dani y Yimarly, ambas oriundas del departamento del Caquetá, reafirman las palabras de Bernardo. Ambas tienen quieren de empezar su carrera en sistemas, y desde ese ámbito esperan seguir aportando al ahora movimiento político de las FARC.

"Queremos estudiar y aportar en lo que más podamos. Queremos apoyar a las mujeres de la vida civil para que salgan adelante, y demostrarle a las mujeres que podemos y que somos valientes", dice una de ellas, y agregan que sueñan con que la paz sea una realidad y "le traiga cosas buenas al país".

![x-conferencia-farc-7](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-7-e1474481383849.jpe){.alignnone .size-full .wp-image-29588 width="889" height="500"}

Sin lugar a dudas,  el viaje de 6 horas hasta el campamento de bloque sur, valen la pena si se dimensiona que lo que aquí está pasando es un giro en la historia de nuestro país.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
