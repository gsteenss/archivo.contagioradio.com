Title: 600 toneladas de basura podrían convertirse en 600 mil Kilovatios
Date: 2016-05-22 08:00
Category: Ambiente, Entrevistas
Tags: energías alternativas, problema ambiental doña juana, Relleno sanitario Doña Juana
Slug: asi-podria-resolverse-el-lio-ambiental-de-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Relleno-Doña-Juana..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria ] 

###### [22 Mayo 2016]

Juan Fernando Gutierrez, experto en energía renovable y desarrollo sostenible, afirma que se continúa usando el relleno sanitario Doña Juana, pese a las problemáticas ambientales y sociales que ha originado, por **falta de voluntad política y gestión institucional para implementar las tecnologías de aprovechamiento de residuos sólidos **que actualmente funcionan en diversas partes del mundo.

La gasificación es una de estas técnicas, explica Gutierrez, a través de ésta **600 toneladas de plásticos, aceites y residuos con alto contenido de carbono pueden llegar a convertirse en 600 mil Kilovatios de energía**, en una planta que permanentemente estaría generando 25 Megavatios por hora al año y que puede costar US\$20 millones. Costo que podría pagarse con recursos públicos, aportes de los ciudadanos y dineros de financiación extranjera.

El tema del relleno es complejo y debe afrontarse integralmente con varias tecnologías; en principio, debe haber un proceso pedagógico que asegure que los residuos sean separados en la fuente, para garantizar que la aplicación de las técnicas sea exitosa, segura Gutierrez. La solución es paulatina y depende de la **voluntad política de las instituciones y de la inversión estatal para implementar un plan integral de manejo** en el que los residuos orgánicos y no orgánicos sean procesados, valorizados y aprovechados, y no enterrados, agrega.

Teniendo en cuenta que por ejemplo de 3 mil toneladas de basura, por costos, sólo pueden gasificarse 600, lo que habría que hacer con las restantes 2 mil 400 es llevarlas a cadenas de separación, reciclaje y reutilización. De implementarse un plan que contemple distintas tecnologías **se podría llegar al concepto de basura cero, no sería necesario usar rellenos sanitarios**, y se eliminarían los focos de las[ [enfermedades y plagas que a diario atacan a las comunidades](https://archivo.contagioradio.com/comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana/)], asevera el experto.

De acuerdo con Gutierrez, sí bien estas tecnologías pueden ser costosas, más se invierte en la atención de los impactos ambientales y sociales. Diariamente **en correctivos en la salud, impactos en el suelo, el agua y el aire se gastan sumas onerosas que no resuelven el problema**, porque la normatividad colombiana no está a la altura y no hay voluntad política. Lo que se propone entonces es implementar estas técnicas en las que las [[comunidades afectadas pueden juegan un papel central como socias](https://archivo.contagioradio.com/como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana/)].

<iframe src="http://co.ivoox.com/es/player_ej_11605258_2_1.html?data=kpajkpqWeZmhhpywj5aaaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYqMrWssLixdSYqdrYrYa3lIqum9fWqdufjpCy2tXJttXjjMrbjcrSqdPbhqigh6aopYzmxtPc2MbGsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
