Title: La carta de los ambientalistas a la CSIVI
Date: 2017-08-27 06:00
Category: Columnistas invitados, Opinion
Tags: ambientalistas, CSIVI
Slug: la-carta-de-los-ambientalistas-a-la-csivi
Status: published

###### 26 Ago 2017 

#### **Por: Valentina Camacho Montealegre** 

Organizaciones ambientalistas nacionales e internacionales se pronunciaron ante la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final (CSIVI), con respecto al “Proyecto de Ley Estatutaria de Participación Ciudadana” presentado por el Gobierno Nacional.

[Mediante una carta, alrededor de 100 organizaciones rechazaron las modificaciones que pretende realizar el Gobierno a los procesos de revocatoria de mandato y las consultas populares.]

[Aseguran que los condicionamientos que presenta el Gobierno Nacional para poder adelantar la revocatoria de mandato y las consultas populares son regresivos y desconocen lo dispuesto por la Corte Constitucional en la Sentencia T-445 de 2016, en términos de autonomía territorial y participación democrática.]

[Para el abogado Rodrigo Negrete, en el proyecto de ley se pretende establecer un  un obstáculo para llevar a cabo las consultas populares. Además, el jurista calificó las reformas planteadas como “una prohibición disfrazada bajo la formalidad de surtir un paso que no está previsto en la Constitución, porqué los mecanismos de participación son independientes y tienen finalidades diferentes".]

[Los ambientalistas critican que en el título III, denominado]*[Garantías para el ejercicio de los mecanismos de participación directa,]*[ “se imponen requisitos previos a la revocatoria de mandato y las consultas populares que obstaculizan el derecho fundamental a la participación y el ejercicio de la democracia”.]

[Se oponen a la exigencia que pretenden imponer a las comunidades que promueven las consultas populares y las revocatorias del mandato de presentar de manera integral, completa, veraz y verificable las razones que sustentan su solicitud para la inclusión de los Cabildos Abiertos Especiales de Deliberación en procesos de revocatoria.]

[Renzo Alexander García, vocero del Comité Ambiental en Defensa de la Vida, manifestó: “El Gobierno plantea exigir a las comunidades información veraz, verificable y las razones que sustentan su solicitud, tratando de desconocer la cosmovisión y los conocimientos tradicionales, que tienen un valor similar al conocimiento científico occidental”.]

[“Condicionando de esta forma la actuación del movimiento social al discurso academicista y de expertos, que en la mayoría de veces se encuentra cooptado por las empresas mineras y desconectado de las problemáticas sociales”, agregó.]

[Vale recordar que la CSIVI es la instancia que recibe y avala todos los proyectos de ley que se derivan de la implementación del acuerdo con la FARC, es así como, todos los proyectos del fast track pasan por allí antes de llegar al congreso.]

[“Es lamentable e incoherente que el gobierno nacional pretenda aprovecharse de los acuerdos de paz para tratar de generar condicionamientos y limitaciones a los mecanismos de participación ciudadana”, puntualizó Renzo García.  [Leer Carta a la CSIVI](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Carta-a-la-CSIVI.pdf)]
