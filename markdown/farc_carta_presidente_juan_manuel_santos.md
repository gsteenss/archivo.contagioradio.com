Title: Dura carta de las FARC al gobierno por asesinatos de integrantes de ese partido polítco
Date: 2017-10-22 17:09
Category: Nacional, Paz
Tags: acuerdo de paz, FARC, masacre en Tumaco, Tumaco
Slug: farc_carta_presidente_juan_manuel_santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/FARC-Partido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Rodrigo Londoño] 

###### [21 Oct 2017] 

En una dura carta de las FARC al gobierno La dirección política exigió respeto de los acuerdos sobre solución de conflictos, dado que en la última semana se han presentado dos ingresos de la fuerza pública a espacios territoriales de reincorporación y además han asesinado a varios integrantes de esa organización. Adicionalmente en redes sociales han denunciado que habría la intensión de capturar varios militantes con fines de extradición.

### **Fuerza Pública ha ingresado a dos espacios de normalización** 

Otro hecho contra los excombatientes de las FARC a manos de la policía se repite. Esta vez en Guaviare, puntualmente en el municipio de San José del Guaviare, vereda Colinas, en el Espacio Territorial de Capacitación y Reconciliación Jaime Pardo Leal, donde **unidades de la Fuerza Pública dispararon contra dos ex combatientes de las FARC-EP,** no obstante, no hubo heridos.

La denuncia la hace las FARC en una carta al presidente Juan Manuel Santos, en la que evidencian su preocupación ante los últimos hechos contra ellos y las comunidades. En esa misma línea, recuerdan que este viernes, **la fuerza pública, irrumpió en zona rural de Tumaco, en el Espacio Territorial de Capacitación y Reincorporación Ariel Aldana, para detener a Tito Aldemar Ruano Yandún,** "quien figura como miliciano en las listas entregadas para acreditación por parte de las FARC-EP para todos los efectos relacionados con los acuerdos firmados entre el Estado y la insurgencia", dice la carta.

### **Ex combatientes abandonaron la Zona Ariel Aldana en Nariño** 

Tras ese hecho, las FARC publicó fotos de la zona veredal donde se evidencia que **los excombatientes han preferido abandonar ese espacio de reincorporació**n teniendo en cuenta que están atemorizados ante las acciones de la fuerza pública. Respecto a ese caso, resaltan que se debió "respetar los mecanismos acordados para la solución de diferencias entre las partes para evitar que se pierda la confianza construida; lo que para nada cuestiona la potestad del Estado para hacer presencia en todo el territorio nacional". (Le puede interesar: [Asesinan a integrantes de las FARC en Nariño)](https://archivo.contagioradio.com/integrantes-de-las-farc-asesinados-en-narino-estaban-en-sus-casas/)

Asimismo, llaman la atención sobre los recientes hechos ocurridos en Tumaco, donde una masacre el pasado 5 de octubre acabó con la vida de 7 campesinos. También mencionan su preocupación por los asesinatos contra lideres sociales en todo el país. Todo ello, en el marco del **incumplimiento del punto 4 de los acuerdos de paz, referentes a los cultivos de uso ilícito.** (Le puede interesar: [Tumaco llora una masacre)](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/)

### **FARC llaman a respetar mecanismos para solución de conflictos** 

"Convoquemos todos los mecanismos, nacionales e internacionales, contemplados en los acuerdos para que, de manera objetiva y sosegada, nos sentemos a analizar las actuales dificultades, con la certeza que así encontraremos la forma más adecuada para fortalecer la construcción de la paz estable y duradera en nuestro país; y de paso, cerrarle el camino a quienes buscan beneficiarse de clima de incertidumbre que rodea la etapa de implementación", concluye el Consejo Político Nacional, Fuerza Alternativa Revolucionaria del Común.

<iframe id="doc_15052" class="scribd_iframe_embed" title="Carta Abierta Al Presidente de La República - Consejo Político Nacional-Fuerza Alternativa Revolucionaria Del Común" src="https://www.scribd.com/embeds/362306955/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-oMPy24QybG3mKE6OTYTa&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
