Title: ESMAD también arremetió contra manifestantes en el relleno Doña Juana
Date: 2017-08-15 11:44
Category: DDHH, Nacional
Tags: Alcaldía de Bogotá, Barrio el mochuelo, ESMAD, manifestación, Relleno sanitario Doña Juana
Slug: esmad-continua-arremetida-contra-manifestantes-en-el-relleno-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/esmad-mochuelo-e1502815467960.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Iván Cepeda] 

###### [15 Ago 2017] 

En medio de la protesta que continúa en el sur de Bogotá, el **ESMAD arremete contra los manifestantes, **5 personas han resultado heridas y 7 más fueron detenidas. La comunidad está protestando por las pésimas condiciones socio ambientales en las que viven debido al mal manejo de los desechos que llegan al relleno sanitario de Doña Juana.

El día de ayer falleció un menor de edad por un paro bronco respiratorio y los habitantes de inmediato llamaron a la ambulancia que se demoró, según ellos, 40 minutos en llegar. Sin embargo, han manifestado que la información que han dado los medios de información es falsa. Según Yurani Muñoz, habitante del sector, **"No es verdad que no hayamos dejado pasar la ambulancia, ella llegó tarde"**.

Muñoz afirmó, **“nos han tildado de asesinos cuando la verdad es que no enviaron ambulancia a tiempo**. Hay fotos y videos de la hora en que llegó la ambulancia y donde se ve que sí se le dio paso”. Igualmente manifestó que “el Distrito quiere desviar la atención de lo que está pasando para no resolver el problema”. (Le puede interesar: ["Habitantes de más de 100 barrios del sur de Bogotá protestan contra relleno Doña Juana"](https://archivo.contagioradio.com/habitantes-de-mas-de-100-barrios-del-sur-de-bogota-protestan-contra-relleno-sanitario-dona-juana/))

Ante esto, Muñoz indicó que **las afectaciones de salud de los habitantes, producto de los malos manejos del relleno, son preocupantes** y “nadie se ha comprometido a solucionar la deuda que tienen con nosotros por 30 años”.

**ESMAD continúa arremetiendo contra la población**

Desde que comenzaron las protestas el día de ayer, de acuerdo con los habitantes, 5 personas han sido heridas con balas de goma y **ellos han asegurado que las patrullas continúan llegando al lugar**. Yurani Muñoz indicó que la protesta ha sido de manera pacífica y le han pedido a las autoridades que retiren el ESMAD.

La situación es de tal gravedad que los manifestantes han dicho que **“las motos del ESMAD suben a atacar a la comunidad**, si le pasa algo a las personas el responsable es el Distrito. Necesitamos soluciones a este problema y es indignante que ataquen a una población que hace reclamos justos”. (Le puede interesar: ["Comunidades reclaman cierre definitivo de relleno Doña Juana"](https://archivo.contagioradio.com/comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana/))

Finalmente, Muñoz aseguró que el Distrito envió una comunicación en la que manifiesta que **los atenderá el día jueves y sólo a un grupo pequeño de personas**. Esto aumentó el descontento de los habitantes quienes reclaman la presencia de Enrique Peñalosa de manera urgente.

Hoy a las 12:00m **se realizará un plantón en la sede tecnológica de la Universidad Distrital en Ciudad Bolivar** en solidaridad con las personas que realizan la protesta.

<iframe id="audio_20348075" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20348075_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
