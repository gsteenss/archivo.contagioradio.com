Title: Seguiremos cumpliendo con nuestra función y mandato: JEP
Date: 2019-08-29 18:31
Author: CtgAdm
Category: Judicial, Paz
Tags: FARC, Iván Santrich, JEP, Patricia Linares
Slug: seguiremos-cumpliendo-con-nuestra-funcion-y-mandato-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jep1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[A raíz de las declaraciones de Iván Márquez, anunciando el rearme de algunos exmiembros de las FARC,  la Jurisdicción Especial para la Paz (JEP)  se expresó señalando que como parte del Sistema Integral de Justicia, le corresponde adoptar decisiones frente a los comparecientes que, de demostrarse su rearme, se les retirará todos sus beneficios, incluida su permanencia en el sistema.]

**"No podemos defraudar la confianza de las víctimas, la sociedad colombiana, la comunidad internacional y de los comparecientes**", expresó a[ través de un comunicado firmado por la directora de la entidad, Patricia Linares   [(También puede leer: «El Acuerdo es mucho más grande que unas personas que se apartan»)](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/)]

> ?|| El rearme es causa de exclusión en la JEP  
> Frente al rearme de exmiembros de las Farc "esta Jurisdicción actuará con celeridad, porque así lo dispone la norma": [\#PatriciaLinares](https://twitter.com/hashtag/PatriciaLinares?src=hash&ref_src=twsrc%5Etfw), presidenta de la [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw).  
> ➡️Leer nota: <https://t.co/4aqbAy7brV> [pic.twitter.com/iYOsVCb3GZ](https://t.co/iYOsVCb3GZ)
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [August 29, 2019](https://twitter.com/JEP_Colombia/status/1167166058505154567?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### El rearme es causa de exclusión de la JEP

Algunos de los jefes guerrilleros que aparecieron anunciando su rearme como **Iván Márquez, Jesús Santrich y Henry Castellanos Garzón alias 'Romaña'** tienen abiertos incidentes de verificación de cumplimiento en esa jurisdicción, mientras que en el caso de  alias 'El Paisa', la JEP decidió emitir una orden de captura en su contra aunque en aquel entonces no se determinó su expulsión del sistema de justicia transicional.

[En la actualidad la JEP administra a **11.986 personas** que incluyen a excombatientes de las FARC, Fuerza Pública, agentes del Estado y terceros que tuvieron participación directa o indirecta en el conflicto y que continán cumpliendo sus compromisos y condiciones impuestas por el sistema. ][(Lea también: La verdad de Mancuso que podría llegar a la JEP)](https://archivo.contagioradio.com/la-verdad-de-mancuso-que-podria-llegar-a-la-jep/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
