Title: La memoria histórica del país no se puede crear desde la ambigüedad de Darío Acevedo
Date: 2019-09-19 17:13
Author: CtgAdm
Category: DDHH, Memoria
Tags: Centro Nacional de Memoria Histórica, conflicto armado, Dario Acevedo, Iván Cepeda
Slug: la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Dario-Acevedo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Senado de la República/ CNMH] 

Iván Cepeda, senador del Polo Democrático, radicará ante la Comisión Segunda de Senado una citación de debate de control político a Dario Acevedo, con el fin de examinar la forma en que el director del Centro Nacional de Memoria Histórica (CNMH), ha descalificado el respaldo internacional al Acuerdo de Paz, la realidad sobre las ejecuciones extrajudiciales y en general el conflicto armado en Colombia.

Para el senador, desde que Acevedo fue nombrado en este cargo, "ha suscitado una gran polémica, no solo por su nombramiento sino por sus declaraciones y decisiones" que reflejan una posición ambigua con relación a si existe o no el conflicto armado, por lo que es necesario conocer de forma clara su posición con respecto a la memoria histórica, razón por la que se cita a este debate.

### ¿Cuál es la versión de Dario Acevedo del conflicto en Colombia?

"Ante ese tipo de ambigüedad es difícil que una institución tan importante que debe darle seguridad al país de que habrá una memoria y un relato democrático lo haga", manifestó el senador, quien señaló que también **es necesario conocer las acciones que emprenderá el CNMH para trabajar de forma articulada con la Comisión para el Esclarecimiento de la Verdad.**  
**  
**"Hemos conocido su decisión de hacer a un lado el trabajo que tomó seis años en el Centro de Memoria y que concluyó en el informe !Basta Ya!, como si se tratase de construir de cero el relato histórico y la memoria del país", argumenta Cepeda destacando la inversión de recursos y los valiosos relatos que hicieron parte del informe.

A propósito del informe ¡Basta Ya!, compilado de voces y testimonios, Acevedo tendrá que responder sobre la forma en que han sido descartados o congelados otros informes del Centro de Memoria, no publicados ni difundidos, además tendrá que dar a conocer cuál es su posición con respecto a las víctimas a las que según Cepeda discrimina, privilegiando los relatos o narrativas de las Fueras Militares y **minimizando la versión de las víctimas de crímenes de Estado.**

### Desconocer la responsabilidad de los falsos positivos

Otro de los puntos claves de este debate de control serán las ejecuciones extrajudiciales y la forma en que el director del CNMH ha señalado que hacen parte de una campaña para "enlodar y desprestigiar a las Fuerzas Militares", desconociendo así que fue una política de Estado y revigtimizando a los familiares de las víctimas de los mal llamados falsos positivos. [La ruptura de confianza entre la víctimas y el Centro Nacional de Memoria Histórica](https://archivo.contagioradio.com/victimas-memoria-historica/)

"Cómo es posible que el país haya pasado por proceso tan traumático, de intentar que haya justicia frente a hechos sistemáticamente perpetrados, que sean objeto de un examen preliminar en la Corte Pena Internacional y la JEP y que diga que esos crímenes son simplemente hechos que se le ocurrieron a algunos miembros de la Fuerza Pública menospreciando y pisoteando a las víctimas", se cuestiona Cepeda.

Finalmente señala que Colombia cuenta con diversos investigadores que han trabajado junto a víctimas e instituciones nacionales e internacionales y que si la función de Acevedo es la de "**criticar la memoria histórica o descalificar a víctimas"**, no debería estar frente a una institución que tiene precisamente funciones totalmente contrarias a las que ha ejercido.

<iframe id="audio_41721594" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41721594_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
