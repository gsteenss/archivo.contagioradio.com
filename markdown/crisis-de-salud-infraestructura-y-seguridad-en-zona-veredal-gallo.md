Title: Crisis de salud, infraestructura y seguridad en Zona Veredal Gallo
Date: 2017-03-31 18:16
Category: Nacional, Paz
Tags: implementación acuerdos de paz, ZVTN Gallo
Slug: crisis-de-salud-infraestructura-y-seguridad-en-zona-veredal-gallo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-31-at-4.42.35-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DIPAZ] 

###### [31 Mar 2017] 

[El comité Local de Tierralta de acompañamiento de la sociedad civil al MM&V expresó sus preocupaciones sobre el estado actual de la Zona Veredal del Gallo e insiste en el temor de las comunidades frente a la aparición de **grupos paramilitares y el peligro que esto representa para los integrantes de las FARC-EP que se encuentran en este punto de normalización.**]

### [**Logística e Infraestructura**] 

[Frente a las condiciones actuales en las que se encuentra la zona veredal, el comité pudo constatar que no hay avances **ni siquiera del 50% en logística, señalan que las aulas comunes están incompletas**, impidiendo que se inicie con las labores de pedagogía, además hay solamente dos baterías de baños instaladas que no funcionan correctamente.]

[El comité señala que esas malas condiciones son **“el reflejo del abandono estatal” del que han sido víctimas las comunidades** y otros lugares rurales del país al que hoy se suman las zonas veredales y añade que el punto de Gallo es una de las zonas que menos avances ha tenido, entre los otros 26 puntos. Le puede interesar: ["Abraza la paz, una iniciativa de los estudiantes de Colombia para la reconciliación"](https://archivo.contagioradio.com/abraza-la-paz-una-iniciativa-de-los-estudiantes-de-colombia-para-la-reconciliacion/)]

[Otra de las preocupaciones de la comisión tiene que ver con el estado de salud de algunos de los integrantes de las FARC-EP, con **20 casos de paludismo simultáneos, que al parecer no han sido atendido como corresponde** y como si el tratamiento que se estuviera suministrando no estuviese siendo efectivo. Enfermedades que se producen por la falta de tuberías, baños y zonas adecuadas para que estos brotes no se produzcan.]

[Además, hacen un llamado de atención sobre el avance del proceso de dejación de armas ya que **“a pesar de la voluntad de las FARC-EP”**, no tienen conocimiento de la implementación de medidas o elementos necesarios para la continuidad de este proceso.]

![Zona Veredal Gallo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-31-at-4.42.59-PM.jpeg){.alignnone .size-full .wp-image-38677 width="1280" height="960"}

### [**Protección y garantías para las comunidades**] 

[La Comisión indicó en su informe que hay una **“alarmante situación” que pone en riesgo a los miembros de la comunidad de Gallo y a los integrantes de las FARC-EP** que se encuentran en este lugar. Le puede interesar: ["Corte revisará constitucionalidad de modificaciones del congreso al Acuerdo Final de Paz"](https://archivo.contagioradio.com/corte-revisara-constitucionalidad-de-modificaciones-del-congreso-al-acuerdo-final-de-paz/)]

[Una de esas situaciones tiene que ver con la mala conducta y el abuso de bienes comunitarios por parte parte de miembros de la Fuerza Pública bajo el mando del Sargento Javier Pineda, ejemplo de ello es el hecho ocurrido el pasado 8 de marzo, cuando un agente de la Policía expresó, refiriéndose al presidente de la Junta de Acción Comunal de Gallo, **“si no fuera por este proceso, la actuación de nosotros sobre este desgraciado hubiera sido otra, pero lo tenemos en la mira”**.]

[De igual forma, en el informe la Comisión señaló que hay registro de acciones que demuestran el accionar de grupos criminales sucesores del paramilitarismo y que denuncian las comunidades, como lo es el avistamiento y la presencia de grupos autodenominados como **Autodefensas Gaitanistas de Colombia, que han incursionado en diferentes fechas al punto conocido como Caracolí.**]

[Finalmente indicaron que es preocupante la **detención del integrante de las FARC-EP Julio Enrique Lemos, cuando se encontraba en Medellín para recibir tratamiento médico por una hepatitis**, que se constituye en una violación al Acuerdo de Paz. Le puede interesar:["Familia del líder guerrillero de los años 50 ofrece sus tierras para la construcción de paz"](https://archivo.contagioradio.com/familia-aljure-dona-tierras-para-proyectos-productivos-de-las-farc/)]

######  Reciba toda la información de Contagio Radio en [[su correo]
