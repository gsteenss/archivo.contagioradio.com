Title: OtraMirada: realidad de campesinos durante pandemia
Date: 2020-06-26 16:12
Author: PracticasCR
Category: Nacional
Tags: campesinado, campesinas, campesinos, mercados campesinos
Slug: otramirada-realidad-de-campesinos-durante-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EXwUxBxWkAE2Rx-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Campesinos @JuventudesUPSP

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la pandemia mucho se ha hablado del impacto que el aislamiento provocará en la economía y se han tenido en cuenta los problemas de las grandes empresas, de la industria del petróleo, entre otras. Sin embargo no se ha considerado la situación de los campesinos y campesinas que por el confinamiento han visto afectadas sus actividades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los campesinos han enfrentado esta pandemia con solidaridad, han repartido mercados, han abierto nuevos canales de distribución intentando eliminar la intermediación, que muchas veces es la que más gana, pero sobre todo con la dignidad que los caracteriza. Este programa es una oportunidad de llegar a ellos y ellas, a su realidad. Nos abren las puertas de sus casas para que veamos esa otra realidad que pretende ser ocultada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

José Felipe Negrete, pescador artesanal en Lorica, Córdoba, fue nuestro primer invitado, quien explicó cómo desde los territorios se han acomodado y adaptado para hacer frente a la situación de pandemia por la COVID-19 que no solo ha afectado el trabajo en las ciudades sino también a campesinos y pescadores artesanales. Además, comparte su opinión frente a la manera en que el gobierno debe garantizar a los y las campesinas una vida digna.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lo que hay que hacer es restaurar nuestra ciénaga y ríos pues al tener en buen estado las ciénagas. 

<!-- /wp:quote -->

<!-- wp:paragraph -->

Otro de nuestros invitados, Absalon Arias, campesino defensor de DD.HH en Fresno, Tolima, describe la complicada situación que se vive en el campo con los bancos y lo difícil que ha sido mantenerse al día con sus obligaciones financieras debido a factores como el clima, los tratados de libre comercio y la poca comprensión y ayuda que les prestan tanto las entidades financieras como el propio gobierno. (Le puede interesar:  [Bancos son las prioridades de Iván Duque durante la pandemia: Coeuropa](https://archivo.contagioradio.com/ffmm-y-bancos-son-las-prioridades-de-ivan-duque-durante-la-pandemia-coeuropa/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Los bancos han iniciado un acoso en contra de los campesinos. 

<!-- /wp:quote -->

<!-- wp:paragraph -->

Desde el Valle del Cauca, Alfredo Añasco miembro de la Red de Mercado Agroecológicos Campesinos del Valle del Cauca, presenta y explica la propuesta y la iniciativa que se ha desarrollado desde el campo con uno de los mercados agroecológicos más grandes que tiene Colombia, con una variedad de productos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alfredo Añasco comenta que les ha tocado sostener la organización por su cuenta y explica su objetivo de ser capaces de producir sus propios alimentos sin el problema de los agroquímicos y sin depender de los insumos para en un futuro ser reconocidos, apoyados y elegidos por los ciudadanos de todo el país. 

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Queremos que haya una política pública para que tengamos oportunidades más justas para sacar nuestros productos y que las personas entiendan cuál es nuestra lucha.

<!-- /wp:quote -->

<!-- wp:heading -->

El modelo de desarrollo excluye a campesinos
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último, y para brindarnos un análisis sobre la situación a la luz de lo que pasa en los territorios estuvo Fabio Pachón, profesor de la facultad de ciencias agrarias de la Universidad Nacional. Según el profesor Pachón, hay un desconocimiento y aislamiento del campesinado y ha sido por las políticas públicas. Estas a la vez están relacionadas con un modelo de desarrollo que lo único que busca es que haya aumento de la productividad y en esto, por la opinión de muchos, no tienen cabida los campesinos que son reacios al cambio y al desarrollo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la vez, nos muestra cómo realmente deberíamos ver el día sin IVA y cómo este refleja el papel del campesinado para la sociedad colombiana. (Si quiere saber más: [Según la FAO el COVID-19 ha afectado al 87% del campesinado en Colombia)](https://archivo.contagioradio.com/segun-la-fao-el-covid-19-ha-afectado-al-87-del-campesinado-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Nosotros seguimos en ese círculo vicioso en el que no reconocemos al campesinado, no reconocemos la importancia que tiene el campesinado, no valoramos el alimento.
>
> (Si desea escuchar el programa del 18 de junio: <https://www.facebook.com/contagioradio/videos/3358259164253000/>

<!-- /wp:quote -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
