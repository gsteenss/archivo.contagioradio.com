Title: Asesinan a Jhon Jader Cayapú, cabildante indígena de Corinto
Date: 2019-04-16 15:17
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cauca, Corinto, lideres sociales
Slug: asesinan-jhon-jader-cayapu-corinto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lider-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado lunes 15 de abril cerca de las 11 a.m., fue encontrado el cuerpo sin vida de Jhon Jader Cayapú, cabildante indígena que habitaba en Corinto, Cauca. En cuanto a los motivos para asesinarlo, así como los perpetradores del hecho no se tiene información y tanto la Fiscalía como la Policía no se han pronunciado sobre el crimen. (Le puede interesar: ["Guardia campesino que que acudió a llamado de auxilio fue asesinado en Corinto, Cauca"](https://archivo.contagioradio.com/asesinan-guardia-campesina-corinto/))

Johana Dagua, integrante de la Guardia Indígena en Corinto y familiar de Cayapú, señaló que desconocen quienes causaron la muerte del comunero y las razones para hacerlo, pues él se dedicaba únicamente a la agricultura y al cuidado de su hijo. El pequeño pasará a custodia de su abuela, pues su mamá (compañera de Cayapú) también había sido asesinada. (Le puede interesar:["Ola de homicidios no se detiene en Argelia, Cauca"](https://archivo.contagioradio.com/ola-homicidios-argelia-cauca/))

### **Corinto bajo fuego cruzado** 

Dagua afirmó que en ese municipio del norte de Cauca, se han presentado desapariciones forzadas, masacres y asesinatos por cuenta de la presencia de grupos de hombres armados que no se acogieron al proceso de paz, y el EPL que se están disputando el territorio.  En ocasiones pasadas, Contagio Radio ha reportado el secuestro de líderes indígenas y el confinamiento de comunidades por cuenta de confrontaciones entre estos grupos.

Sobre el asesinato de Cayapú, Dagua sostuvo que no han tenido comunicación por parte de la Fiscalía o Policía hasta el momento, y esperan que las instituciones oficiales se pongan en contacto con la familia para acordar mecanismos de apoyo que los ayuden a sobrellevar esta difícil situación.  (Le puede interesar: ["Corinto bajo fuego cruzado de EPL y disidentes de las FARC"](https://archivo.contagioradio.com/conrito-cauca-bajo-el-fuego-cruzado-por-enfrentamientos-entre-disi/))

<iframe id="audio_34567036" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34567036_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
