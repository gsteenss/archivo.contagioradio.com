Title: Prisioneros políticos de FARC en condiciones inhumanas
Date: 2020-04-08 18:33
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: ex combatientes FARC, Expreso Libertad, FARC, prisioneros farc, prisioneros políticos, Prisioneros políticos FARC-EP
Slug: prisioneros-politicos-de-farc-en-condiciones-inhumanas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/los_presos_politicos_de_farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

José Zamora, ex prisionero político de **FARC**, junto a Jhon León, defensor de derechos humanos e integrante de la Corporación Solidaridad Jurídica, denunciaron en el **Expreso Libertad** la crítica situación que viven tres prisioneros políticos del partido Farc, luego de ser víctimas de un traslado arbitrario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin acceso a agua potable, alcantarillado o alimentación serían algunas de las vulneraciones que estarían viviendo estas personas, que de acuerdo con los entrevistados podría generar no solo afectaciones a la salud, sino también un intento acabar con las protestas que se vienen adelantado desde el movimiento cárcelario.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_59934663" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_59934663_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Le puede interesar: [Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
