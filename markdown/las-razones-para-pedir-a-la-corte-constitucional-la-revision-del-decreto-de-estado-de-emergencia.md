Title: Las razones para pedir a la Corte Constitucional la revisión del decreto de estado de emergencia
Date: 2020-04-23 21:28
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Corte Constitucional, Gobierno, Organizaciones sociales
Slug: las-razones-para-pedir-a-la-corte-constitucional-la-revision-del-decreto-de-estado-de-emergencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Corte-Constitucinoal-revise-estado-de-emergencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CConstitucional {#foto-cconstitucional .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tomando en cuenta las denuncias hechas por ciudadanos y congresistas, respecto a varios decretos promulgados por el presidente Iván Duque en el marco del estado de emergencia declarado en Colombia y que irían en contracorriente con los intereses ciudadanos, el Colectivo de Abogados José Alvear Restrepo y la Corporación Sisma Mujer presentaron una intervención ante la Corte Constitucional para revisar dichas medidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Específicamente las organizaciones defensoras de DD.HH. solicitan a la Corte revisar si el Decreto 417 de 2020 que dio origen a la declaración de "emergencia, social, económica y ecológica", se ajusta a la Constitución, y si los decretos legislativos amparados en esta declaratoria también tienen una adecuada orientación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las dos acciones que le solicitan a la Corte Constitucional**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Jomary Ortegón, abogada e integrante del Colectivo de Abogados explica que la Corte Constitucional realiza control automático a todas las decisiones que se tomen en el marco del estado de emergencia, la primera revisión será al decreto que lo declaró. Dicho estado está regulado por la Constitución y se deben cumplir algunos requisitos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En esa medida, Ortegón señala que las organizaciones consideran que era necesario decretar el estado de emergencia "porque los medios ordinarios no sirven para conjurar una crisis" como la que generó el Covid-19; pero le piden a la Corte que analice si todos los fundamentos del decreto 417 son necesariamente conexos a la situación de la pandemia y que evalué todos los decretos reglamentarios, que entre legislativos y ordinarios suman más de 90.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el primer caso, porque entre los considerados para decretar el estado de emergencia se acude al precio del petróleo o del dólar, que no necesariamente están relacionados con la pandemia, lo que para las organizaciones podría dar lugar a que se dicten medidas no encaminadas a conjurar el corazón de la crisis sanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En segundo lugar, la abogada recuerda que tanto las Naciones Unidas como el Sistema Interamericano de DD.HH. han llamado a que este tipo de medidas se ajusten a la protección de los derechos sociales, económicos y culturales. Sobre este punto, Ortegón aclara que en análisis de las organizaciones, algunos decretos no atienden a la población más vulnerable y contienen medidas disperas que se podrían cambiar por otras con enfoque de derechos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1252987088703754243","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1252987088703754243

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El papel del Congreso y los otros órganos de control en el estado de emergencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La Corte Constitucional se deberá referir sobre el decreto 417 que dio origen al estado de emergencia, y posteriormente tendrá que revisar los decretos expedidos, pero la abogada también instó al Congreso a que actúe tomando en cuenta que está instalado desde el 16 de marzo y ya inició sesiones formales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una solicitud que recuerda lo fundamenta de esta rama que del poder, que aún en estado de emergencia, mantiene sus facultades legislativas para hacer control político y modificar los decretos si lo considera necesario. De igual forma, Ortegón llamó a los órganos de control a que también actúen en el sentido de revisar las medidas.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El giro que requiere la política social

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Una situación evidente que se está viviendo en varias ciudades del país es la ausencia de garantías reales para que la población vulnerable económicamente cumpla con el aislamiento preventivo, lo que los ha empujado a salir a las calles reclamando ayudas estatales en términos de dinero y productos de canasta familiar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, Ortegón declara que respondiendo al principio de la dignidad humana se debería apoyar a las familias con la renta básica, "un salario mínimo por familia o medio salario para quienes no viven en núcleos familiares", para que puedan enfrentar esta situación. (Le puede interesar: ["De la renta básica, a la garantía total de derechos"](https://archivo.contagioradio.com/de-la-renta-basica-a-la-garantia-total-de-derechos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
