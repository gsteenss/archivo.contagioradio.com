Title: Tribunal de Ecuador debe embargar bienes de Chevron para reparar los daños en la Amazonía
Date: 2016-07-07 17:23
Category: DDHH, El mundo
Tags: Chevron, ecuador, Sucumbíos, Texaco
Slug: tribunal-de-ecuador-debe-embargar-bienes-de-chevron-para-reparar-los-danos-en-la-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/chevron-ecuador-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: taringa] 

###### [7 jul 2016]

Las comunidades indígenas afectadas por la Chevron - Texaco afirman que no hay excusa para demorar los trámites que podrían significar el **inicio de las labores de reparación de los daños a la Amazonía**. Desde 2013, un tribunal ecuatoriano ordenó al Estado embargar los bienes de la multinacional en Ecuador, sin embargo, la decisión no se ha hecho efectiva, pues no se han notificado a las entidades desde el tribunal de Sucumbíos que debe hacerla de manera inmediata.

La suma, **que asciende a 9 mil quinientos millones de dólares**, no se ha cancelado a pesar de los [fallos de los tribunales](https://archivo.contagioradio.com/corte-suprema-de-justicia-de-canada-falla-a-favor-de-afectados-por-chevron-en-ecuador/) porque no se ha hecho efectivo el embargo a los bienes de la empresa y porque hay un fallo del tribunal de Estados Unidos en que se obliga al Estado ecuatoriano el pago de una indemnización por los efectos del cese de actividades en la región selvática de Sucumbíos, al norte de ese país.

Desafortunadamente los daños en la selva amazónica y lugar de habitación de las comunidades indígenas no son menores. Chevron Texaco **contaminó todas las fuentes de agua ubicadas en cerca de 480 mil hectáreas de selva y construyó más de 800 piscinas**, sin ningún tipo de recubrimiento o protección, en las que se albergaron durante años residuos tóxicos que tienen efectos evidentes en la salud de los habitantes y en la biodiversidad.

De acuerdo con Garcés, muchos de los daños son irreparables, pero los que se pueden menguar **requieren de una intervención técnica especializada que debe disponer de recursos económicos y profesionales suficientes**, es decir, no son daños que puedan ser asumidos por las comunidades habitantes de la región, que además han sufrido daños en su estructura cultural y de creencias por el desplazamiento forzado del que fueron víctimas.

Aunque este caso se ha conocido internacionalmente por [representar la lucha de los pueblos originarios por su territorio](https://archivo.contagioradio.com/onu-discute-tratado-para-que-empresas-respeten-derechos-humanos/) y su calidad de vida contras las multinacionales, María Eugenia Garcés, comunicadora de las comunidades afectadas, afirma que el gobierno de **Ecuador no ha intervenido en las decisiones de la justicia como lo pretende afirmar Texaco** para intentar una defensa, pero tampoco ha intervenido a favor de las comunidades más allá de anunciar el daño producido a los recursos naturales y el patrimonio cultural de la región.

<iframe src="http://co.ivoox.com/es/player_ej_12153666_2_1.html?data=kpeel5iaepehhpywj5aZaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfptrUx9PNpYy7wtfQh6iXaaKt1IqfpZDHs87pz87Qw8nTtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
