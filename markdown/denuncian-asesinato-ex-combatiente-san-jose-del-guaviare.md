Title: Denuncian asesinato de ex-combatiente en San José del Guaviare
Date: 2019-01-03 16:51
Author: AdminContagio
Category: DDHH, Nacional
Tags: acuerdo final, asesinato de excombatientes, etcr, FARC, proceso de paz, reincorporación de las FARC, Zonas Veredales
Slug: denuncian-asesinato-ex-combatiente-san-jose-del-guaviare
Status: published

###### [Foto: Misión de Verificación de la ONU en Colombia] 

###### [03 de Ene 2019] 

[La comunidad del Espacio Territorial de Capacitación y Reincorporación (ETCR) “Jaime Pardo Leal”, ubicado en la vereda Colinas en el departamento de Guaviare rechazó el asesinato de Lidier Alexander Astros, ex-combatiente que se encontraba en el proceso de reincorporación, el pasado 31 de diciembre.]

Según el comunicado de la ETCR, a la 1 de la mañana, Astros salió de su casa a atender una llamada con una persona desconocida y a las 6:30 de la mañana, fue hallado sin vida, con dos impactos de bala en la cabeza, a un kilómetro de la comunidad.

Antes de que se diera a conocer los hechos, **no se había registrado asesinatos de habitantes en esta zona de reincorporación**, donde 351 excombatientes se preparan para regresar a la vida civil, ni en el departamento de Guaviare. La comunidad de reincorporados de Colinas manifestaron que el asesinato ha generado un ambiente de "[gran incertidumbre y zozobra" en donde antes se vivía tranquilamente.]

La dirección de Partido FARC, la junta administrativa y la comisión de derechos humanos del ETCR rechazaron el crimen y hicieron un llamado al Estado a esclarecer lo ocurrido. [Además, pidieron al Estado garantizar la seguridad de los 7.000 reincorporados **como quedó acordado en el punto 3.4 del Acorde Final**.]

(Le puede interesar: "[Consejo de Seguridad de Naciones Unidas preocupada por asesinato de líderes sociales en Colombia](https://archivo.contagioradio.com/onu-preocupada-por-el-asesinato-de-lideres-sociales-en-colombia/)")

[Sin embargo,** 85 reincorporados han sido víctimas de asesinatos desde la firma del acuerdo de paz en el 2016**, según el último informe del Consejo de Seguridad de las Naciones Unidas sobre la Misión de Verificación en Colombia. El partido político FARC también llamó a la atención en agosto que **el 20 % de excombatientes asesinados desde el 2016 vivían en zonas veredales o en un ETCR**.]

######  Reciba toda la información de Contagio Radio en [[su correo]
