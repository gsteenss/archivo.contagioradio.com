Title: Así va la Consulta Popular en Cajamarca
Date: 2017-03-26 10:26
Category: Ambiente
Tags: Cajamarca, consulta popular, Mineria
Slug: asi-va-la-consulta-popular-en-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-26-at-8.32.55-AM-2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de Cajamarca] 

###### [26 Mar. 2017] 

Pese a las lluvias y la decisión de la Registraduría de reducir el número de mesas , los habitantes de **Cajamarca acuden hoy a votar la Consulta Popular. **Le puede interesar: [Crece riesgo para la Consulta popular en Cajamarca](https://archivo.contagioradio.com/crece-riesgo-para-la-consulta-popular-en-cajamarca/)

Según información conocida por Contagio Radio **cada votante se está demorando hasta 4 minutos** para realizar su voto, lo que se traduce en que **solo alcanzarían a votar 2880 personas** hasta el momento que se cierren las mesas, **cabe recordar que el umbral mínimo para que la consulta sea válida es de 5500 votantes.**

Además algunos de los habitantes del municipio que ya han ejercido su derecho al voto aseguran que **los aparatos de detección de huellas presentan varias fallas** que también han demorado el proceso.

Por otra parte algunos habitantes de Cajamarca que se desplazan desde Ibagué y otros municipios cercanos para participar en la consulta, han sido **demorados por retenes policiales  ubicados en las afueras de Ibagué y las entradas a Cajamarca.**

La medida, que parece obedecer a la seguridad normal de un proceso electoral, ha sido calificada por integrantes del movimiento ambiental como una **demora más al proceso de votación** ya que todas las personas que acuden a Cajamarca hacen parte del censo electoral legal.

En su momento el Comité promotor le había manifestado a la Registraduría su **preocupación ante la reducción de mesas** y le había hecho un llamado a la entidad para que mantuviera el número inicial de 36 mesas, solicitud que no fue atendida.

**Para los cajamarcunos estas situaciones afectan la consulta minera.** Le puede interesar: [Denuncian nuevos obstáculos para la Consulta Popular en Cajamarca](https://archivo.contagioradio.com/nuevos-obstaculos-para-la-consulta-popular-en-cajamarca/)

### **Actualización** 

Según integrantes del Comité promotor y la comunidad de Cajamarca, **hasta las 11:30 a.m. habían logrado  registrar de 3 mil** votos en la Consulta popular.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

   
\
