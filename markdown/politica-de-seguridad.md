Title: Política de seguridad, un retroceso de 20 años en derechos humanos
Date: 2019-02-07 18:52
Author: AdminContagio
Category: DDHH, Política
Tags: Duque, guerra, politica, Seguridad Democrática
Slug: politica-de-seguridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-07-a-las-2.47.26-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanDuque] 

###### [7 Feb 2019] 

Este miércoles el presidente **Iván Duque presentó** en la base militar de Tolemaida, Tolima, **su Política de Defensa y Seguridad para la vigencia (2018-2022)**; entre los hechos que se resaltan está **el retorno a las redes de informantes que empleó el gobierno Uribe, el desconocimiento del conflicto armado,** el tratamiento del narcotráfico desde la óptica de la penalización y en general, el regreso de la conocida "política de seguridad democrática".

Para **Alberto Yepes, integrante de la Coordinación Colombia, Europa, Estados Unidos (COEUROPA),** la política lanzada por Duque este miércoles es muy similar a la de Seguridad democrática  presentada el 9 de agosto de 2002 por Álvaro Uribe Vélez en Valledupar.  El paralelismo se establece entre un conjunto de medidas que incluyen la creación de una red de informantes, premiados mediante recompensas por su información, sumado a la posibilidad de que civiles porten armas "para defenderse".

En esa medida, la nueva política de seguridad, implica un retroceso en los aprendizajes obtenidos a lo largo de casi dos décadas, en las que se había entendido que esas medidas en conjunto produjeron dos grandes fenómenos: el fortalecimiento del paramilitarismo y el desarrollo de las ejecuciones extra judiciales "Falsos Positivos". (Le puede interesara: ["Más de 70 mil armas han sido incautadas desde firma de decreto que prohíbe su porte"](https://archivo.contagioradio.com/70-mil-armas-decreto/))

Según el defensor de DDHH, la red de informantes y recompensas se facilitó que algunas personas señalaran a sus vecinos de ser colaboradores de la guerrilla, para una posterior identificación de las fuerzas militares, que procedían a asesinar a los señalados y presentarlos como bajas en combate; y en segundo lugar, permitir que civiles porten armas, sirvió para facilitar la expansión del paramilitarismo.

Aunque en la política de Duque las armas quedaron en poder del Estado, el Ministerio de Defensa está desarrollando criterios para que **personas "en riesgo" tengan acceso a estos elementos, situación que se asemeja a lo ocurrido en 2002**; adicionalmente varios grupos de ganaderos han pedido que se flexibilice aún más esa política, para que ellos se puedan "defender" cuando se sientan amenazados.

### **Usan eufemismos para llamar con otros nombres lo que ya conocemos** 

Yepes sostiene que se están usando otros términos para referirse a las mismas políticas de Uribe, por ejemplo, **en lugar de las redes de cooperantes hay redes de participación cívica, "no hay seguridad democrática sino legalidad, no hay posconflicto sino consolidación territorial",** términos con los el Gobierno espera tener el visto bueno por parte de la comunidad internacional.

Sin embargo, sí se añaden nuevos elementos como el uso de la tecnología para realizar inteligencia militar, lo que para el Abogado y defensor de derechos humanos podría representar una nueva forma para aumentar la interceptación a las comunicaciones de las personas, tal como ocurrió con el desaparecido DAS. A ello se suma una política de seguridad en las fronteras, que plantea nuevamente la confrontación trasfronteriza y "nos devuelve a la posibilidad de guerra con un país vecino como Venezuela".

Adicionalmente, se está creando un enemigo interno representado en el narcotráfico, cambiando la política con enfoque en salud pública para el consumo de psicoactivos, por una de tipo represivo que castiga al pequeño consumidor; pero que **no plantea soluciones estructurales para el sistema que permite la siembra, cosecha, producción, transporte y distribución de narcóticos ilegales**.

### **La oposición al Acuerdo de Paz ** 

Tras el atentado del ELN a la Escuela General Santander, Duque ha endurecido su tono amplificado por el eco de los medios empresariales, presentándolo con un nuevo aire para su Gobierno, y la lucha contra el terrorismo,, como su nuevo caballo de batalla. Cambios que, para Yepes, se traducen en la búsqueda de legitimidad para el plan de trabajo del Presidente, y **como una forma de adelantar su agenda económica que incluye la venta de acciones de ECOPETROL, el aumento en la edad de pensión y el apoyo al Fracking**.

El efecto negativo lo vivirían las comunidades rurales, porque con esta política se conduce al país nuevamente a una fase de la guerra de la "que los colombianos pensábamos que estábamos saliendo con el proceso de paz con las FARC y las conversaciones con el ELN". Todo ello mientras **se ignora la existencia de un conflicto armado, pues en ningún momento el documento se refiere en esos términos a lo que ha ocurrido en Colombia en los últimos 50 años**.

### **La privatización de la fuerza pública al servicio de la empresas** 

Otros temas que resaltan de la política de defensa y seguridad son la supuesta protección al ambiente y la financiación para la fuerza pública. Por una parte, porque el Presidente dijo que el plan de defensa contemplaba el cuidado de territorios de minería ilegal y la deforestación; pero ello significa, en análisis del activista, que **los territorios se van a proteger para hacer una extracción privada de los recursos**.

Por lo tanto, **la protección estaría enfocada en cuidar los grandes intereses económicos que generan dinero de la explotación de la naturaleza y no en resguardar el uso que hacen las comunidades de sus regiones**. Dicha acción la haría la fuerza pública, porque es su mandato, pero también porque se financiaría gracias a ello. (Le puede interesar: ["ANLA aprueba licencias ambientales en el único bosque de niebla del Tolima"](https://archivo.contagioradio.com/anla-licencia-bosque-niebla/))

La política de Duque reconoce que el gasto militar es alto, y el Estado no puede continuar aumentando la financiación del mismo, razón por la que se proponen alianzas público privadas para obtener ingresos. Sobre esta forma de generar recursos, el doctor Yepes recuerda que ya funciona, "por ejemplo, **empresas petroleras hacen aportes presupuestales a la fuerza pública para construir oleoductos**", a cambio de ser protegidos por los uniformados del país, **incluso, por encima del interés de las comunidades**.

### **Lo que se puede hacer frente a esa política** 

Para finalizar, el doctor Alberto Yepes propone algunas líneas que deberían seguirse para hacer frente a la Política de Seguridad y Defensa propuesta por Duque, la primera de ellas sería el Congreso, puesto que son los representantes a la cámara y senadores quienes encarnan las aspiraciones del pueblo colombiano. (Le puede interesar: ["Aumento del IVA: Única propuesta del Gobierno que puso de acuerdo al Congreso"](https://archivo.contagioradio.com/aumento-iva/))

Adicionalmente se podrían establecer demandas ante la Corte Constitucional, esperando que el tribunal se pronunciara sobre los hechos anteriormente mencionados; no obstante, **el espacio idóneo para manifestar la inconformidad por parte de la ciudadanía ha sido siempre la calle**, razón por la que organizaciones y movimientos ciudadanos deberían pronunciarse en tal escenario contra esta política y presionar para lograr su cambio.

<iframe id="audio_32372878" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32372878_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
