Title: Multiplicar las tierras y los peces veinte años después
Date: 2017-03-28 09:49
Category: Abilio, Opinion
Tags: cacarica, mapiripan, Multrifruits, Sikuane y Jiw
Slug: multiplicar-las-tierras-y-los-peces-veinte-anos-despues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/20-años.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 27 Mar 2017 

En este dos mil diecisiete pasan por la mente y el corazón los veinte años del horror militar y paramilitar que asesinó, desapareció, desplazó a mestizos , indígenas, afrocolombianos en San José de Apartadó,[Cacarica,](https://archivo.contagioradio.com/20-anos-renaciendo/) Curvaradó, Jiguamiandó, Dabeiba, Mapiripán, entre otros territorios de Colombia. En ese lapso también en cada uno de esos lugares, se ha hecho patente el por qué asesinan, desaparecen y desplazan.

En san José se sabe de una reserva minera, en Cacarica se montó Multrifruits aliada con Delmonte, extrajo madera Pizano S.A y se proyectó la transversal de las Américas. En Curvaradó y Jiguamiandó se deforestó y plantó palma aceitera por Urapalma con el concurso de los bananeros y con financiación del gobierno, también se inició la exploración minera por parte de la Muriel Mining Corporation en el cerro sagrado de los indígenas Embera, Cara de Perro. En Dabeiba se construyó la represa de Urrá I y se declaró como parque natural parte de las tierras de los campesinos desplazados. En Mapiripán todo quedó servido para los acaparadores de tierra, se instaló la empresa italiana Poligrow, sobre la que pesan investigaciones de orden penal y administrativo.

La memoria no es solo de dolor, es también de mucha esperanza. La fuerza de la dignidad irrumpió como respuesta de las víctimas que no se rindieron ante el desastre. En San José se declararon como Comunidad de Paz inspirados en la acción Noviolenta, convirtiéndose en signo de la resistencia civil no sólo para Colombia sino para el mundo. En Cacarica Constituyeron Zonas Humanitarias a donde regresaron para sacar de sus territorios a Pizano S.A; a la empresa Multifruits y a decirle a los de la Transversal que no podían construirla sin contar con ellos; en el Curvaradó y Jiguamiandó aprendieron del Cacarica y tras regresar a las zonas humanitarias cortaron palma aceitera para sembrar comida y construir vivienda, pero además constituyeron zonas de biodiversidad para la recuperación de las vidas amenazadas por los monocultivos. Las mujeres indígenas del Jiguamiandó se le atravesaron en la pista a un helicóptero de la transnacional minera y en una extraordinaria consulta de los pueblos decidieron decirle no a la exploración, obligándolos a retirarse de sus territorios. En Dabeiba se acercaron a sus tierras en una reubicación y ya este año, con el ayuda de los acuerdos de paz, sueñan con pisar Antazales a donde no regresan desde el desplazamiento.

La historia de Mapiripán ha sido distinta, pero la fuerza de la lengua Sikuane y Jiw, de su cultura, se resisten a ser arrinconados por la expansión palmera de Poligrow que, con el gobierno, le negaron los derechos a la consulta y han logrado decisiones de jueces que permiten esperar una ampliación de resguardo y adquisición de tierras. También un puñado de campesinos ha tenido el valor de vencer el miedo y denunciar el despojo del que han sido víctimas. Ya indígenas y campesinos han marchado a avizorar la laguna sagrada de las Toninas, donde el delfín rosado y la nutria gigante habitan, donde la empresa palmera pretende constituir una zona franca e instalar una planta extractora de gran capacidad, con las implicaciones ambientales que ello conlleva. Veinte años después de la masacre van a volver a sus tierras y a caminar de nuevo en busca de lo sagrado en las Toninas, también inspirados por sus hermanos/as de dignidad de las Zonas Humanitarias y la Comunidad de Paz.

Esa memoria de los que no se amilanan ante el poder, es la misma que ha inspirado a Jesús, por nombrar solo la tradición de sentido que nos es más cercana. Dominique Crossan en su libro “Cuando orad decid padre nuestro”, al interpretar aquel “dadnos hoy nuestro pan de cada día” del Padre Nuestro, retoma la multiplicación de los panes y los peces, que acontece al lado del lago de Galilea, que para el momento fue renombrado por Antipas el hijo de Herodes, como lago Tiberiades, y de cuya pesca producían una salsa que era exportada a la cede del imperio, pasando el manejo del lago de los pescadores tradicionales a los comerciantes.

En ese contexto aconteció el “milagro” de la multiplicación de los panes y los peces. Ante el hambre de la multitud que escuchaba a Jesús en ese momento y la insistencia de los discípulos de despacharlos a que buscaran comida en otro lugar, Jesús les pidió que fueran ellos, casi todos pescadores, quienes les dieran de comer. Hubo tantos panes y peces que comieron y sobró para llevar a sus casas. La multiplicación de los panes y los peces, es la recuperación del lago en poder de Tiberio, para la gente necesitada, para Dios.

Esa fuerza de la dignidad es la que permitirá en Colombia que acuerdos como el de Reforma Agraria Integral se cumplan, por la decisión de las comunidades y la acción Noviolenta de los exgerrilleros que hacen transito a la vida civil. Sin esa decisión difícilmente gozarán de los derechos pactados con el gobierno, por eso la invitación es a mirar esas metodologías de la acción Noviolenta utilizada por las comunidades, en esa memoria viva, cercana, que se revela desde ellas para contemplarla, asumirla y recreara, veinte años después.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
