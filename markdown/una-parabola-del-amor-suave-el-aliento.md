Title: Una parábola del amor:  ‘Suave El Aliento’
Date: 2015-10-22 12:49
Author: AdminContagio
Category: 24 Cuadros
Tags: Augusto Cesar Sandino, Bogota International Film Festival, Cine Colombiano, Gustavo Angarita Actor, Suave el aliento, Vicky Hernández actriz
Slug: una-parabola-del-amor-suave-el-aliento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/CR22_upUsAAvMsq-e1445537139993.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [22, Oct 2015] 

Una mirada sobre el amor en tres etapas distintas de la vida: adolescencia, adultez y vejez, es el eje sobre el que se desarrollan las historias que componen "**Suave el aliento**", la ópera prima del director y guionista bogotano Augusto César Sandino que se estrena este jueves en salas de cine a nivel nacional.

El jóven director describe su primer largometraje como una producción "muy personal, que hago a partir de mis observaciones y mis búsquedas, la recolección de experiencias familiares también, y todo eso trato de ponerlo en la película", en busqueda de encontrar la esencia de lo cotidiano de la forma más real posible.

<iframe src="http://www.ivoox.com/player_ek_9129774_2_1.html?data=mpafm5ybeI6ZmKiakpWJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmc%2FVjNXO1Iqnd4a1ksfczsaPqMbgjMba0deJd6Kfhqqfh52UaZqstNrO2MqPic2fotHWx9PYs4a5k4qlkoqdfY%2BfotrU19jYs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Somos individuos que sentimos, que sufrimos y que amamos. Durante todas las etapas de nuestra vida no somos ajenos a este fenómeno. Platón argumentaba que el amor es sabiduría; que se encuentra en la espiritualidad del otro y es allí en donde radica la verdadera belleza.]

En la película, se exponene una serie de personajes pertenecientes a un mismo núcleo familiar, quienes viven en una constante búsqueda de un amor, que para cada uno se materializa de distintas formas; así lo expone **Gustavo Angarita**, quien es "Vicente" protagonista de uno de los tres relatos.

<iframe src="http://www.ivoox.com/player_ek_9129867_2_1.html?data=mpafm52ae46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRmc%2FVjNXO1Iqnd4a1ksfczsaPqMbgjMba0deJd6Kfhqqfh52UaZqstNrO2MqPic2fotHWx9PYs4a5k4qlkoqdfYyhjKzi1dnFuo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La principal virtud de "**Suave El Aliento"** es precisamente la proximidad con la que se cuentan las tres historias, en donde el espectador podrá identificarse con ellas, o en su defecto, no sentirse distante a la premisa de la película. **Isabel Gaona** interpreta a "Esperanza" en la cinta y afirma que este es el aspecto que hace valiosa a la película.

<iframe src="http://www.ivoox.com/player_ek_9129931_2_1.html?data=mpafm56XdY6ZmKiakpWJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmc%2FVjNXO1Iqnd4a1ksfczsaPqMbgjMba0deJd6Kfhqqfh52UaZqstNrO2MqPic2fotHWx9PYs4a5k4qlkoqdfYyhqtjOxMrQb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En "Suave el aliento". el amor es una parábola que sin embargo desconocemos; porque es incierto saber en qué punto de nuestras vidas va a estar en el lugar más alto, para que finalmente permanezca en lo más recóndito de nuestros recuerdos.

[![11949481\_463042760536160\_8047304578139603371\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/11949481_463042760536160_8047304578139603371_n.jpg){.aligncenter .size-full .wp-image-16144 width="480" height="177"}](https://archivo.contagioradio.com/una-parabola-del-amor-suave-el-aliento/11949481_463042760536160_8047304578139603371_n/)

El largometraje es una producción de Schweizen Media Group, compañía fundada por Sandino, quién es además el director del Simposio Internacional Cine de Autor que se lleva a cabo cada dos años en Colombia. El reparto se complementa con las actuaciones de Vicky Hernández, Camila Ariza, Alberto Cardeño,  Shirley Martínez y Camilo Suárez.

**Sobre Augusto Cesar Sandino**.

Director y guionista bogotano, estudió en el Art Center College of Design en Los Ángeles y en EICTV en San Antonio de los Baños. Realizó los cortometrajes Visita (1998), Noche de lluvia (2000), Pelos (2003), Aniversario (2005), La cámara (2006), y Trata de ser amable con ella (2010) seleccionados para participar en festivales de cine como Guadalajara, Huesca, Palm Springs, La Habana, Cartagena, Auckland, Milano, New York, Manchester, y merecedores de algunos premios internacionales.

Realizó el especial documental para televisión El oficio del cineasta (2014) con Carlos Reygadas, Lisandro Alonso, Pedro Aguilera y Amat Escalante.

<iframe src="https://www.youtube.com/embed/ISaEavdDmrw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Sinopsis Oficial:  
**

Tres miembros de una familia viven el amor en diferentes etapas de sus vidas. Dolores (Vicky Hernández) una mujer mayor que durante más de 30 años fue amante de Vicente (Gustavo Angarita). Rafael (Alberto Cardeño) es un deportista retirado y venido a menos, quien a pesar de todos los intentos por consolidar una familia y establecer su vida no ha logrado darle sentido a su existencia. Una de sus hijas, Laura (Camila Ariza), es una quinceañera enamorada y enfrentada a una decisión que cambiará su destino. Ellos buscan que los una el afecto que a su vez los separa.
