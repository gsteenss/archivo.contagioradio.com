Title: Día nacional de la Afrocolombianidad: Retos para el decenio 2015-2025
Date: 2015-05-21 13:06
Author: CtgAdm
Category: DDHH, Nacional
Tags: Afrocolombianidad, ANAFRO, Autoridad Nacional Afrocolombiana ANAFRO, Comunidades Afrodescendientes Colombia, Día de la Afrocolombianidad, Discriminación afrodescendientes Colombia
Slug: dia-nacional-de-la-afrocolombianidad-retos-para-el-decenio-2015-2025
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Yumbo.gov.co 

###### **Entrevista con [Jimmy Viera], representante de ANAFRO, Autoridad Nacional Afrocolombiana:** 

<iframe src="http://www.ivoox.com/player_ek_4528518_2_1.html?data=lZqfmpqVfI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiIa3lIqupsaPqMafzcaYo8vWs8TjzdTaxM7FssrYwsmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Hoy 21 de Mayo de 2015 se celebra el **día de la Afrocolombianidad** en todo el país, **distintas expresiones culturales, lúdicas, pero también académicas** se pueden ver por las distintas regiones de Colombia donde hay presencia de la etnia.

Provenientes de **África, fueron esclavizados y traídos por los españoles** con el objetivo de trabajar en los cultivos, en servicios domésticos o en todo tipo de trabajos que requerían de una gran masa de mano de obra.

En **1819  Simón Bolívar, el Libertador, abolió la esclavitud**, pero no fue hasta **1851 con el presidente liberal José Hilario López cuando realmente fueron liberados** y terminó una de las peores etapas de los afrodescendientes en Colombia.

Actualmente son el **10,6% de la población colombiana**, como etnia fueron **reconocidos en 1991** y actualmente su presencia mayoritaria se encuentra en Cali, Cartagena de Indias, Buenaventura, Medellín y Tumaco, distribuidos principalmente en grandes municipios.

**Valle del Cauca, Cauca, Nariño, Chocó, Bolívar, atlántico y Magdalena son los departamentos más poblados por afrodescendientes**, en el Pacífico y la costa Caribeña las zonas donde la etnia tiene más presencia.

Su **cultura y su historia** proveniente de África han marcado el devenir de la **identidad Colombiana desde lo social, lo político y lo cultural,** influyendo y aportando a la creación de la Nación Colombiana.

Para **Jimmy Viera**, representante de la **Autoridad Nacional Afrodescendiente de Colombia,** este es un día muy especial, en el que se celebra la libertad de su pueblo y la conmemoración del reconocimiento en Colombia.

Distintos actos culturales han dado comienzo a la celebración como la Arboleda en la Plaza Bolívar de Bogotá, aunque la más importante son las **jornadas académicas que inician hoy en la Universidad Nacional con el objetivo de estudiar, analizar y programar las políticas públicas** en toda América para las comunidades afrodescendientes.

Este año comienza el **decenio (2015-2025) programado por la Asamblea de Naciones Unidas para las comunidades afrodescendientes de América**. Mireia Jano representante de la comisión Internacional de Afrodescendientes de la ONU, acudirá a los distintos conversatorios en la Universidad Nacional.

Es por ello que se van a estudiar los **retos integrales para la creación de políticas públicas para la etnia.** En las jornadas iniciadas en la Universidad Nacional se va a contar con presencia de representantes de afrodescendientes de distintos países como Perú, Venezuela o EEUU.

Para **Jimmy los retos van ligados a las necesidades de las comunidades y a la reparación y las consecuencias de la esclavitud.** El representante de ANAFRO, nos explica que en Colombia el racismo no es directo como en EEUU, sino institucionalizado, estructural y cultural.

Ejemplos como la **no presencia de ningún afrodescendiente en cargos directivos en el gobierno o en instituciones públicas como la Iglesia, los bancos o las empresas constituyen una forma de ninguneo de la etnia.**

También los afrodescendientes han sido duramente golpeados por el **conflicto armado Colombiano constituyendo el sector de la población con más desplazamiento**, ademas de reclutamiento forzado y deterioro de las condiciones de vida, debido a la presencia de actores armados en sus territorios.

Por otro lado Viera nos recuerda que en Colombia las **peores estadísticas de precariedad corresponden a los afrodescendientes** donde departamentos como Cauca o Valle del Cauca trabajan en situaciones similares a las de la **esclavitud**.

Igual sucede con la **educación,** por ejemplo en departamentos como el Chocó, y en general en todo el Pacífico, donde **apenas hay escolarización** entre los Afros.

Siendo la educación la lucha contra el racismo, la esclavitud y la precariedad laboral o las políticas de inclusión social y reforzamiento identitario los **principales retos para el decenio 2015-2015.**

   
   
   
 
