Title: Campesinos alertan sobre inminente ataque del ESMAD en Catatumbo
Date: 2017-10-29 14:59
Category: Movilización, Nacional
Tags: campesinos, Catatumbo, paro
Slug: campesinos-catatumbo-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DNPEkkHXcAIyFlj.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Ascamcat 

###### 29 Oct 2017 

Por medio de un comunicado campesinos del Catatumbo que se encuentran en manifestación pacífica, advierten sobre lo que catalogan como un "inminente ataque" por parte del Escuadrón Móvil Antidisturbios ESMAD, haciendo una llamado a las autoridades y organizaciones de Derechos Humanos a hacer presencia con el fin de preservar la vida y la integridad de los manifestantes.

En el texto los campesinos lamentan que a pesar de los llamados hechos al gobierno nacional durante meses no se haya podido reactivar la Mesa de interlocución y acuerdo del Catatumbo, dejando pocos caminos diferente a las vías de hecho para ser escuchados y responsabilizaron al gobierno por cualquier acción que pueda poner el peligro la vida e integridad de quienes se encuentran movilizados.

En la comunicación firmada por el Movimiento Marcha Patriótica Norte de Santander, ASCAMCAT y la COCCAM, aseguran además que los pobladores del corregimiento Y de Astilleros han salido a apoyar a los campesinos y campesinas y disuadir el ataque de la fuerza pública lo que ha aumentado la tensión en el lugar. Situación que demanda la presencia de una comisión del gobierno que pueda reactivar la Mesa de Interlocución.

Desde la semana anterior, campesinos que participan del Paro Nacional, vienen denunciando las [provocaciones por parte de la fuerza pública](https://archivo.contagioradio.com/denuncian-provocaciones-de-la-policia-a-campesinos-en-paro-nacional/),  a pesar de haberse acordado un pacto de no agresión.

![WhatsApp Image 2017-10-29 at 1.26.07 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-29-at-1.26.07-PM.jpeg){.alignnone .size-full .wp-image-48537 width="628" height="871"}
