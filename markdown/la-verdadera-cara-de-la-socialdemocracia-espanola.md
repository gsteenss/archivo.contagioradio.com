Title: La verdadera cara de la socialdemocracia española
Date: 2016-11-01 09:42
Category: Eleuterio, Opinion
Tags: elecciones, españa
Slug: la-verdadera-cara-de-la-socialdemocracia-espanola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/CtHIvdJXYAQRxbV.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pedripol 

#### [**Por [Eleuterio Gabón](https://archivo.contagioradio.com/eleuterio-gabon/)  **] 

Oficialmente se llama PSOE (Partido Socialista Obrero Español) aunque cada vez son más quienes creen que para que sus siglas se correspondan con su verdadera naturaleza, debería llamarse simplemente PE, ya que de socialista y menos de obrero, este partido tiene bien poco.

[Aunque tal vez los sucesos que han rodeado a este partido últimamente han sido definitivos para su desprestigio, sin embargo la pérdida de credibilidad tiene su historia. Decimos pérdida de credibilidad en un partido que comenzó renunciado al marxismo en el que se había fundado, en el momento preciso en que la democracia comenzaba a llegar a España. Este oportuno gesto le concedió una aureola de partido idóneo, alejado de postulados que se consideraran radicales o antiguos vinculados al marxismo y presentándose de manera renovadora y “moderna” acorde a los nuevos tiempos de la época que vivía España tras la dictadura franquista. Todo una puesta a punto, una operación de marketing político que sin embargo que quedaría pequeña ante la que tendría lugar años después a propósito del referéndum sobre la entrada del país en la Alianza del Tratado Atlántico Norte, OTAN. Con el lema “OTAN, de entrada no” el PSOE se postuló a rechazar la entrada de España en la alianza militar internacional. Poco después su líder y presidente del gobierno por aquel entonces, Felipe González, amenazó con dimitir si el referéndum no salía favorable al ingreso.]

[El carisma del líder socialista resultó fundamental para darle la vuelta a las encuestas y España entró a formar parte de la OTAN. Esto supuso la aceptación del modelo neoliberal por parte de la socialdemocracia española que se vería ratificado de manera progresiva en las siguientes década tanto en sus posiciones como gobierno como en sus planteamientos en la oposición. En todo este tiempo el PSOE no ha apostado por hacer suya ninguna estrategia, principio, ley o reforma de fuerza suficiente para poder llamarse de justicia social. Se ha mantenido fiel al sistema del bipartidismo que en España representa la monarquía heredada del franquismo. El único movimiento de reforma de la constitución del 78, fue un acuerdo con el Partido Popular para garantizar que el pago de la deuda a los banqueros del capital internacional estaría por encima de la soberanía del pueblo. Nunca fueron capaces de apostar por un federalismo ni mucho menos por recuperar la República y su negativa no ha hecho más que legitimar aún más la voluntad, de soberanía de las naciones catalana y vasca.]

[El propio Felipe González, cuya trayectoria desde la política a consejero millonario en una multinacional es un reflejo de las miras de este partido, ha dado la puntilla a la dignidad que podía quedarle a un partido que decía representar a la izquierda. Su boicot al secretario general del partido, el señor Sánchez, por su honesta posición contraria a permitir el gobierno de la derecha, ha dejado claro a todas luces la verdadera cara de este partido. Un partido que antes de tratar de formar una coalición de izquierdas con las diferentes fuerzas del país, ha preferido otorgar por omisión el gobierno a una de las derechas más reaccionarias y corruptas de Europa.]

[Este hecho ha sido acogido con indignación por una gran parte de la sociedad española cuando no como una traición por quienes se consideran socialistas. El país, el Estado Español, se encuentra ante una gran crisis de legitimidad que el bochornoso movimiento del PSOE no ha hecho más que poner totalmente al descubierto. Leer opiniones  ][**[Eleuterio Gabón](https://archivo.contagioradio.com/eleuterio-gabon/)  **]

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
