Title: Resguardo indígena gana pelea por la tierra al Cerrejón en La Guajira
Date: 2018-01-25 10:20
Category: Comunidad, Nacional
Tags: El Cerrejón, La Guajira
Slug: reguardo_nuevo_espinal_la_guajira_comunidades_el_cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/guajira-paro.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CENSAT AGUA VIVA] 

###### [24 Ene 2018] 

En un **nuevo espacio de 420 hectáreas, 65 familias, víctimas de desplazamiento forzado,** por cuenta las actividades minera de El Cerrejón, en La Guajira lograron constituir en el municipio de Barrancas**, el resguardo indígena Nuevo Espinal.**

"No fue nada fácil, pero fue gracias a Dios y a la perseverancia de la comunidad", señala  Álvaro Ipuana, líder de las comunidades indígenas. Quien explica cómo a través de una situación víctimizante hoy esas familias pudieron conformar su nuevo resguardo para acceder a diferentes tipos de programas de desarrollo del Gobierno Nacional y de las Organizaciones no Gubernamentales.

### **El desplazamiento de todo un resguardo** 

Según el dirigente indígena, **el primer desplazamiento forzado que sufrió la comunidad se dio en 1993** y lo produjo  la empresa minera que para ese entonces ya estaba establecida en ese territorio. "N[os llevaron a un corredor donde había presencia del frente 59 de las FARC". Cuatro años después, **en 1997 ocurrió el primer asesinato de una autoridad de la comunidad.** Se trató de un joven que fue raptado de su comunidad por parte del grupo paramilitar de las autodefensas y luego fue asesinado.]

“Las familias se dispersaron, no todas se quedaron, no sabíamos qué íbamos a hacer y mucha de la gente se fue a pasar trabajo a la ciudad, nosotros solo sabíamos trabajar la tierra y vivir de ella”, dice la autoridad tradicional indígena. Agrega que los señalaban como integrantes de las[ FARC y "un tercero quería apoderarse de nuestros territorio", dice Ipuana.]

[Las tierras finalmente fueron adquiridas, gracias a su constante exigencia de derechos ante las autoridades competentes de ese entonces, tales como el **Incora y la Agencia Nacional de Tierras, a manera de restitución de tierras que les quitó la empresa minera.**]

### **En lucha por un nuevo resguardo** 

Las Palmas, Nuevo Hato,  el Cerrito y el Nuevo Sincelejo; son los predios donde hoy se ubican las familias Ipuana, Uriana, Sijuana, Pushaina, Urariyúu. Pero además, ya se traba en la constitución de un sexto resguardo, en el municipio de Barrancas.

Con el resguardo el Nuevo Espinal**, ese municipio quedaría con 6 resguardos indígenas reconocidos por el Ministerio del Interior.** Ahora que ya se prepara con las autoridades Tradicionales para elegir Cabildo Gobernador. Y de acuerdo con el Gobierno Nacional, con la nueva Ley de urbanismo, se trataría del último resguardo indígena constituido legalmente, debido a que la nueva figura será la creación de comunidades étnicas reasentadas.

Según han informado medios locales, se espera que en los próximos días, el comité de Justicia Transicional, con la presencia de la Agencia Nacional de Tierra, ratifique y dé cumplimiento a través de una resolución, a  la legalización y posesión del Resguardo Indígena para que tenga seguridad jurídica, que permita garantizar el uso de tierra y su permanencia en el territorio.

<iframe id="audio_23346509" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23346509_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
