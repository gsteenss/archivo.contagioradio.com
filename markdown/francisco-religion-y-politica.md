Title: Francisco, Religión y Política
Date: 2015-10-01 15:13
Category: Cesar, Opinion
Tags: El discurso del Papa Francisco, Encíclica papal, Movimientos sociales a partir del catolicismo, Papa Francisco
Slug: francisco-religion-y-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/papa_francisco-copia-e1443730035340.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Cesar Torres del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) - [~~@~~logicoanalista2](https://twitter.com/logicoanalista2)

###### [1 de Oct 2015]

Y de nuevo la Iglesia Católica habla pública y críticamente sobre aquellos grandes problemas que agobian a sus fieles y a centenares de millones de seres humanos. Décadas completas han pasado desde el Concilio Vaticano Segundo y el movimiento de la Teología de la Liberación.

Francisco habla claro y en tono elevado; además denuncia. Las diferencias con Juan Pablo II y con Benedicto XVI son plenamente advertidas. Puede ser que ellas no lo lleven a la santidad pero aquí en el planeta son bienvenidas, y eso es lo que interesa a fin de cuentas ya que vivimos tiempos de imposiciones y de regulaciones; de ideologías triunfantes y de fetichismos; de mercancías y mercados; de individualismos y competencias desleales; de guerras y esclavitudes; tiempos, en fin, de Termidor.

Son bienvenidas sus palabras porque los movimientos eclesiales de base, integrados mayoritariamente por los marginados, por los pobres de la tierra, las interpretan como pasos adelante, como campos de trabajo político-religiosos en su permanente búsqueda de liberación. No significa, claro está, que haya habido una superación de la tradicional postura conservadora del episcopado y del papado; por eso hablamos de “campos de trabajo”, de “ideas-fuerza” que permiten una amplia discusión, incluso ecuménica, sobre una “iglesia pobre, para los pobres”, como dijo Francisco en marzo de 2013, que haga, también, de la religión un espacio específico para la política.

**¿De qué se trata?**

El discurso social del Papa jesuita no es improvisado. Interpreta el sentir de los movimientos sociales y busca la conexión espiritual que parcialmente restablezca los vínculos perdidos con los fieles debido a lo que podríamos denominar la “crisis vaticana” (escándalos financieros de corrupción, evidencia clara de hechos sexuales que afectaron la dignidad de niñ@s y jóvenes, apoyo a regímenes dictatoriales …) y a las posturas políticas conservadoras, retardatarias, plasmadas en los textos encíclicos y pastorales de Juan Pablo II y Benedicto XVI.

Recordemos entonces que para fines de octubre de 2014 el Papa convocó en Roma al Encuentro Mundial de Movimientos Populares; asistieron organizaciones de América Latina, Asia y África para discutir sobre los problemas álgidos de hoy, en particular sobre tierra, trabajo y vivienda. Allí aceptó que los movimientos sociales ya no tragan entero los discursos oficiales del “imperio del dinero” ni esperan pasivamente las ayudas condicionadas de las ONGs; habló del protagonismo activo de los pobres y de sus prácticas solidarias - lo que Michael Löwy, en el espíritu de Weber, denomina “religión comunitaria de salvación” -; y animando a los movimientos sociales a expresarse y luchar contra las causas “estructurales” de la pobreza (o sea, decimos, el capitalismo y sus diversas formas de explotación) los instó a continuar luchando “porque nos hace bien a todos”. Son palabras mayores.

La religión, pues, no es exclusivamente asunto teológico. 1959 y 1979, Cuba y Nicaragua, como Acontecimientos, como Hechos Históricos, se hicieron posibles por la praxis política, y profana, de millones de creyentes católicos y protestantes en comunidad fraterna, solidaria. La máxima “La religión es el opio del pueblo”, se demostró inapropiada, insostenible.

Lo que escuchamos de la voz papal en su periplo reciente por Cuba y Estados Unidos no fue el discurso almibarado hacia los círculos palaciegos, o, de otro lado, de regaño a los pastores que reivindican y esperan una Iglesia de, y para, los pobres (recuérdese a Juan Pablo II y al jesuita nicaragüense Ernesto Cardenal). Son, para repetirlo, “campos de trabajo” político-religioso que los movimientos sociales deben estudiar y aprehender en su cotidiana búsqueda de salvación y redención. ¿Hay algo más político que ello?
