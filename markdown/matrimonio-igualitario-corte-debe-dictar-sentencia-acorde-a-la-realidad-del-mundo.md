Title: Matrimonio igualitario: "Corte debe dictar sentencia acorde a la realidad del mundo"
Date: 2015-07-30 11:57
Category: DDHH, LGBTI
Tags: Alejandro Ordoñez, Corte Constitucional, el Defensor del Pueblo, el Ministro de Justicia, Fiscal General, Germán Rincón, la Fundación Marido y Mujer, la ONG Alliance Defending Freedom, LGBTI, matrimonio gay, matrimonio igualitario
Slug: matrimonio-igualitario-corte-debe-dictar-sentencia-acorde-a-la-realidad-del-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/LGBTI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_5589912_2_1.html?data=lpqlm56Vdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9DiyNfS1dSPqMafzcaYtMrUaaSnhqeuxNHNp8KfxcrPx5DQqcjd1NHO1JDXs8PmxpC6w9nWrc7jz86ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Germán Rincón, Abogado y defensor de la comunidad LGBTI] 

Hoy la Corte Constitucional realiza una **audiencia sobre el matrimonio igualitario para saber si se vulneran o no los derechos fundamentales** de parejas del mismo sexo.

“Estamos frente a una mega audiencia, vamos a estar todo el día, la Corte Constitucional escuchará partes a favor y en contra, **para que se pueda dictar una sentencia que ponga a la Corte acorde a la realidad del mundo**”, dice el abogado y defensor de la comunidad LGBTI, Germán Rincón.

Colombia Diversa y De Justicia, solicitaron esta audiencia debido a la ponencia del magistrado Jorge Pretelt que se opone al matrimonio igualitario argumentando que no existe la figura en la normatividad vigente.

El abogado explica que la Corte emitió una sentencia donde se se le daba dos años al Congreso de la República para que legislara sobre el matrimonio igualitario, pero **“el congreso fue insuficiente y no sacó la Ley".** Fue por eso que los notarios dijeron que no podían realizar matrimonios igualitarios y "se inventaron un documento de protocolización de las uniones de pareja del mismo sexo, que no da garantía a derechos, como afiliación a salud, pensión, divorcio, entre otras, lo que generó un hueco legal”.

Además, Rincón denuncia que el Procurador Alejandro Ordoñez ha amenazado a los notarios, y si estos llevan a cabo matrimonios entre parejas del mismo sexo, les puede quitar el cargo. Es por eso que los jueces hicieron un llamado a la Procuraduría, rechazando que ese órgano de control "**haya expedido una circular donde se les amenaza con investigarlos si realizan uniones solemnes"**.

Mientras se realiza un plantón frente a la Corte Constitucional, los jueces que han hablado durante la audiencia han asegurado que es **necesario que el Congreso de la República legisle sobre del matrimonio igualitario.**

En la sesión participa el Fiscal General, el Defensor del Pueblo, el Ministro de Justicia, la Fundación Marido y Mujer,  la ONG Alliance Defending Freedom y varias universidades, entre otros organismos, además de tres parejas del mismo sexo.
