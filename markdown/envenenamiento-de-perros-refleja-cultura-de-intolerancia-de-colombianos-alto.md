Title: Envenenamiento de perros refleja cultura de intolerancia de colombianos: Plataforma ALTO
Date: 2018-06-05 13:14
Category: Animales, Nacional
Tags: Diana Turbay, envenenamiento de perros, perros envenenados, Plataforma Alto
Slug: envenenamiento-de-perros-refleja-cultura-de-intolerancia-de-colombianos-alto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/perros-2-e1528222456787.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pxhere] 

###### [05 Jun 2018] 

Luego de que en el barrio Diana Turbay de Bogotá, fueron encontrados **30 perros muertos por envenenamiento**, la Plataforma ALTO, que defiende los derechos de los animales, manifestó que estos hechos continúan en la impunidad y hacen parte de la cultura de la intolerancia de los colombianos.

De acuerdo con Natalia Parra, este tipo de hechos se han presentado no sólo en Bogotá sino también en Norte de Santander, Huila, Nariño y Cauca. Para la Plataforma, el envenenamiento de animales “es **otra forma de intolerancia** que nos caracteriza como colombianos en donde lo que nos fastidia, lo matamos”.

### **Animales de compañía no son los únicos afectados** 

Parra afirmó que este tipo de acciones pone en riesgo no sólo la vida de los animales de compañía sino también **de las aves, de niños** y habitantes de calle que se movilizan por los sectores donde son dejados los elementos envenenados. Es decir que “la persona que incurre en esta conducta está cometiendo un delito contra el medio ambiente, los animales y tentativa de homicidio si un niño consume estos elementos”.

Frente a la labor de las autoridades, Parra recalcó que “este tipo de móvil es muy difícil de detectar”. Aseguró que “la persona que comete la conducta **va con disimulo** y a falta de cámaras de seguridad” es poco probable que se identifique a estas personas. (Le puede interesar:["Colombia carece de planes que garanticen la vida de la fauna cuando se desarrolla infraestructura": Plataforma Alto"](https://archivo.contagioradio.com/atropellamiento_carreteras_colombia-animales_silvestres/))

### **Ciudadanos deben estar alerta a comportamientos sospechosos** 

Sin embargo, insistió en que **se debe abrir las investigaciones** teniendo en cuenta las zonas donde han ocurrido estos hechos. De igual forma, desde ALTO consideran que hay un perfil para reconocer a estas personas que tiene que ver con el carácter conflictivo de las mismas “que chocan con los otros ciudadanos y tienen actitudes intolerantes”.

Finalmente, Parra recordó que debe haber una vigilancia más activa por parte de la ciudadanía para hacer seguimiento a estos casos. Afirmó que **se necesitan redadas** de vecinos para identificar a personas que pueden ser sospechosas, se requiere revisar el pasto de las zonas comunes y que los dueños de los perros hagan uso de la correa.

<iframe id="audio_26369962" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26369962_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
