Title: What are the main drivers of deforestation in Colombia?
Date: 2019-05-30 16:56
Author: CtgAdm
Category: English
Tags: Business, Deforestation
Slug: what-are-the-main-drivers-of-deforestation-in-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-62.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

According to Global Forest Watch's most recent report, Colombia is the fourth country in the world that suffered the most primary forest loss in 2018, with 176,977 hectacres deforested. For environmental organizations, these figures represent serious issues, such as the disappearance of flora and fauna and violations of international agreements on the conservation of the environment, while also prompting concern over the response of the government to contain the expansion of lands affected by deforestation.

**What causes deforestation?**

Juan Manuel Rengifo, researcher at the Environmental Conflicts Observatory at the National University, said that the main driver of deforestation in Colombia is the armed conflict. Violence enacted within the conflict, such as forced displacement, has caused the appropriation of lands which gives way to others drivers of deforestation, such as ranching, single-crop farming and mining.

Likewise, the environmentalist Veruska Nieto assured that deforestation in the country occurs not only in national parks, but also in areas eyed by private interests. She noted that in the Macarena region, which includes four parks, 9,300 hot spots or fires had been reported recently while 5,700 were found within licensed oil blocks, approved before the signing of the peace accords.

"For the past three years we have seen fires in the southern part of Meta, San José de Guaviare and in the northern part of Caquetá begin to ignite along the famous Route 65. This road crosses three countries and follows an area designated to oil extraction from the Putumayo-Caguan sedmentary basin," Nieto said.

**The Artemisa Plan doesn't address the main drivers of deforestation**

Recently, the federal government announced Plan Artemisa, which aims to protect natural parks from illegal activities that threaten the environment. For Andrea Echeverri, a researcher at Censat Agua Viva, the government uses this operation to evade responsibility for the increase in the rates of deforestation.

"This strategy is only a political ruse used to convince the public into thinking the government is doing something when in reality, they are keeping intact the socio-political policies that allow for deforestation, and the environmental crisis, to worsen," the researcher said.

Nieto added that there are several concerns surrounding this operation. The first is that this strategy appears to be similar to another one that was employed the Congolese military. Despite deploying 6,000 soldiers, this operation proved to be ineffective. After all, Congo leads the list of the countries most affected by deforestation.

Another concern has to do with the commander of the Colombian military, Gen. Nicacio Martínez, who has been questioned by the organization Human Rights Watch his alleged involvement in extrajudicial killings in the past.

### **The environmental "false positives" of Colombia** 

On April 25, 11 people that lived within the Chibiriquete National Park, among them four children, were detained by the police, which accused them of having committed environmental crimes. While the families denounced these arrests as "environmental false positives", the government claimed they were, in fact, part of the first phase of the Artemisa Plan.

The adults captured were subjected to an interrogation and according to a statement from the families, "pressured by the military to accept charges related to environmental crimes." The families were then given three days time to vacate the premises, but when they returned to their homes, the families found that the military had burnt their houses along with their work tools. A bridge built by the community was also detonated.

In Nieto’s opinion, this could be part of greater plan to forcibly displace peasant farmers that have lived in and protected these areas for many years. Once cleared, these lands could be sold to big landowners or multinacionals.

According to Echeverri, 72 peasants were arrested in 2018 in Guaviare for crimes related to deforestation as a result of a new iniciative known as the "deforestation bubble." However, peasants represent only .72% of those responsible of the country, she said, because "it's very difficult for a farmer, who barely has enough money to support his family, can stay three weeks in a row in a forest and raze 500 hectares of land, with a chainsaw, gasoline and remittance."

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F926915641033543%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
