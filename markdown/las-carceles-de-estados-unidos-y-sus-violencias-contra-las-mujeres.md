Title: Las cárceles de Estados Unidos y sus violencias contra las mujeres
Date: 2019-09-06 11:34
Author: CtgAdm
Category: Expreso Libertad
Tags: abolición, carceles, cárceles mujeres, Mujeres y cárceles
Slug: las-carceles-de-estados-unidos-y-sus-violencias-contra-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/esta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div dir="auto">

<div dir="ltr" style="text-align: justify;">

Las mujeres que estuvieron en cárceles en Estados Unidos, han venido denunciado las fuertes afectaciones del sistema penitenciario sobre sus vidas, desde fuertes dosis de medicamentos para mantenerlas controladas, hasta traslados impredecibles que les impide ver a sus familiares, son algunas de las situaciones que ellas deben afrontar.

</div>

<div dir="ltr" style="text-align: justify;">

 ([le puede interesar:Mujeres inician camino para abolir sistema carcelario](https://archivo.contagioradio.com/mujeres-inician-camino-para-abolir-sistema-carcelario/))

</div>

<div dir="ltr">

</div>

<div dir="ltr" style="text-align: justify;">

</div>

<div dir="ltr" style="text-align: justify;">

En este programa del Expreso Libertad, Claudia Cardona, integrante de la Corporación Humanas y Kathy  integrante del movimiento Abolicionista de Cárceles en Estados Unidos comentan sobre las dificultades de estos lugares, la imposibilidad de una resocialización y la falta de programas oportunos desde los países para prevenir estos círculos de violencia.

</div>

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F632224337184200%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
