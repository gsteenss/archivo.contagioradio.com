Title: Dos razones del bajo funcionamiento en la Comisión de Paz del Congreso
Date: 2019-03-14 14:23
Author: CtgAdm
Category: Nacional, Política
Tags: Comisión de la Paz, Congreso de la República
Slug: dos-razones-por-las-que-la-comision-de-paz-del-congreso-esta-dejando-de-ser-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1i6nGsXcAALe_g.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PCatatumbo\_Farc] 

###### [20 Mar 2019] 

Luego de las duras descalificaciones por parte de la senadora **Paloma Valencia del Centro Democrático** en contra de **Pablo Catatumbo del partido FARC** en una reciente sesión de la comisión de paz del Congreso, se abre la discusión en torno a este escenario, que debería propender por la paz, pero que por el contrario se ha convertido en un nuevo foco de polarización.

Entre las razones para que esté sucediendo esto hay dos posibles: la primera tiene que ver con la reciente inclusión de varios congresistas del Centro Democrático, avalados por el presidente del Congreso Ernesto Macías. Según lo afirma Iván Cepeda, congresista del Polo Democrático, el **senador Macías está promoviendo deliberadamente un desequilibrio a favor de las posturas de ultra derecha** que han impedido el avance de las discusiones en esa instancia.

Actualmente la composición de la Comisión de Paz del Senado está así: **por el Centro Democrático hay 12 senadores**, Partido de la U 4, Partido Verde 3, Polo Democrático 2, Partido Liberal 4, Lista de los Decentes 2, FARC 2, Partido Conservador 1, MAIS 2, MIRA 2 y Colombia Justa Libres 2, lo que evidencia un claro desbalance en las fuerzas que propenden por el respeto de los acuerdos y la continuidad del proceso de paz con el ELN; y aquellas que han afirmado se deben “hacer trizas los acuerdos”.

### **Actitud del presidente Duque contra la Comisión de Paz es "arrogante y displicente" ** 

Por otra parte, está **la negativa del presidente Duque para recibir a los integrantes de dicha comisión**, quienes llevan varios meses solicitando una audiencia, que no se ha podido dar por una actuación **“arrogante y displicente”** que está generando mayor polarización.  Según Cepeda esta reunión se ha solicitado desde que el ELN hizo explícita su intención de convocar a esa instancia para resolver los asuntos concernientes al proceso de paz que no ha tenido avances en este gobierno.

Para Cepeda, las recientes objeciones presentadas por el presidente Duque a la ley de procedimiento de la JEP han facilitado un ambiente de confrontación entre poderes que está siendo respaldado por congresistas del Centro Democrático en todas las instancias en que tienen participación, incluidas las Comisiones del Paz del Congreso.

**Seguirá la confrontación y seguramente se agudizará**

Para el senador del Polo Democrático, lastimosamente este tipo de situaciones que incluyen descalificaciones y otras acusaciones se van a seguir presentando, no solamente en el congreso sino en otras instancias y además se agudizarían, lo que evidencia un panorama muy difícil para lograr consensos, es más, para lograr avances en las discusiones que necesitan celeridad.

<iframe id="audio_33381055" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33381055_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
