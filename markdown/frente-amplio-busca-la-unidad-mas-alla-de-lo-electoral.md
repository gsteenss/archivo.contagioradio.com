Title: Frente Amplio busca la unidad más allá de lo electoral
Date: 2014-12-26 16:12
Author: CtgAdm
Category: Otra Mirada
Slug: frente-amplio-busca-la-unidad-mas-alla-de-lo-electoral
Status: published

Crédito foto onic  
[Audio](http://ctgaudio.megadatesystem.com/upload/0/fs91XDqO.mp3)

El viernes 14 y sábado 15 de noviembre se lleva a cabo en Bogotá la primera reunión nacional del Frente Amplio. Este espacio de encuentro de organizaciones como el MAIS, el Congreso de los Pueblos, Marcha Patriótica, Progresistas, Partido Verde, y Polo (entre otros), se lanzó en mayo del 2014 en el marco de las elecciones presidenciales para manifestar el **apoyo de estas organizaciones y partidos a la continuidad de los diálogos de paz y la solución política al conflicto social y armado colombiano.**

Desde ese momento, quienes conforman el Frente Amplio han acordado puntos de encuentro, y realizando giras por todas las regiones del país para para construir espacios unitarios por todo el país.

Esta primera reunión nacional se consolida como un punto de llegada del proceso de acercamiento de posiciones y apuestas, de dinámicas unitarias que se han dado en los últimos años para impulsar el proceso de paz.

El Frente Amplio se proyecta, en palabras de David Flórez, vocero de Marcha Patriótica, como un escenario que construirá unidad en diferentes campos, no sólo el electoral, mediante una agenda propia con una serie de iniciativas y escenarios de movilización. "Hay muchos temas que abordar. Este encuentro lo que va a demostrar son los acercamientos que se han dado en los últimos meses", señala Flórez.
