Title: MOVICE cumple 10 años de existencia
Date: 2015-01-29 22:34
Author: CtgAdm
Category: DDHH, Movilización
Tags: crímenes de estado, DDHH, MOVICE, proceso de paz, víctimas
Slug: el-movice-ha-logrado-visibilizar-que-existen-victimas-de-crimenes-de-estado
Status: published

##### Foto: [ojosparalapaz-colombia.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4015938_2_1.html?data=lZWel56XfI6ZmKiak5WJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zy8bbxtfTb7LpysjS0NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Quiceno] 

Del 29 al 31 de enero en Villavicencio (Meta), los integrantes del MOVICE (Movimiento de Víctimas de Crímenes de Estado), se reunirán con el objetivo de evaluar y planear las acciones del movimiento de acuerdo a la coyuntura del momento con el proceso de paz.

Este año el MOVICE cumple 10 años y Alejandro Quiceno, del área de Coordinación Área de Proyectos y Cooperación, resalta que uno de los grandes logros durante esta década, es que se ha logrado visibilizar, en la escena pública, que existen víctimas del crimen de Estado.

Quiceno, afirma que son 10 años de haberse consolidado para luchar contra la impunidad, recordando que el movimiento nace en el 2005, durante el gobierno de Álvaro Uribe Vélez, cuando la Ley de Justicia y Paz obligó a las víctimas de los paramilitares a congregarse y crear este colectivo, para enfrentar lo que sucedía con “la supuesta desmovilización de los grupos paramilitares”, señala el integrante del MOVICE.

Con esta reunión, de la mano de delegados de diferentes capítulos regionales, se analizará la posición del MOVICE, respecto al proceso de paz, y específicamente, lo que tiene que ver con justicia transicional, la comisión de la verdad y las refrendaciones.

Para el movimiento, los diálogos representan una oportunidad para que este colectivo posicione su agenda y estrategias en la lucha contra la impunidad como víctimas de crímenes de Estado.
