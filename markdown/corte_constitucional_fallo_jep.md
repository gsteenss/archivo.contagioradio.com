Title: "Corte Constitucional todavía puede corregir el fallo sobre la JEP": Alberto Yepes
Date: 2017-11-21 15:08
Category: Entrevistas, Paz
Tags: Corte Constitucional, JEP, Magistrados JEP
Slug: corte_constitucional_fallo_jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Nov 2017] 

Organizaciones sociales y de derechos humanos, que hacen parte de la Coordinación Colombia-Europa-Estados Unidos, aseguran que de mantenerse la Ley estatutaria de la Justicia Especial para la Paz como viene desde el Senado de la República interpondrán la debida demanda ante la Corte Constitucional, e incluso **llegarán hasta instancias internacionales para impedir que se acabe con el espíritu del acuerdo.**

No obstante, defensores de derechos humanos como Alberto Yepes, integrante de la Coordinación Colombia-Europa-Estados Unidos, aseguran que aún se tiene la esperanza en que en la Cámara de Representantes se reversen varias de las modificaciones aprobadas por los senadores, y de igual forma la Corte Constitucional corrija su fallo que atenta contra la posibilidad de conocer la verdad detrás de los múltiples crímenes y masacres en el marco del conflicto.

### **Terceros fuera de la JEP** 

Para Yepes, que la **Corte Constitucional exonere a los terceros o particulares de rendir cuentas ante la JEP, desconoce el espíritu de los acuerdos,** y sobre ello, considera que "es poco probable que el congreso pueda hacer alguna modificación". De manera que espera que el alto tribunal retroceda su fallo en ese sentido, ya que de no ser así se trataría de una **JEP muy parcializada y solo para uno de los actores de la guerra en el país.**

"La Corte podría corregir ese fallo y todavía tiene la oportunidad de hacerlo, si no realmente la JEP quedaría como un tribunal parcializado a interpretación de los propios magistrados", dice el defensor de DDHH. (Le puede interesar: [Corte bendijo la fiesta de la impunidad en Colombia)](https://archivo.contagioradio.com/corte_constitucional_impunidad_jep_enrique_santiago/)

### **Continúa la persecución contra los defensores de DDHH** 

Por otro lado, el abogado asegura que el hecho de que se impida que defensores de derechos humanos hagan parte de la JEP, es inconstitucional y "así debe declararlo la Corte", ya que se estaría avalando la continuidad de un proyecto de persecución contra estos defensores, "configurando una JEP que se priva de personas expertas en temas de derechos humanos". Algo que para Yepes es "**absolutmente contrario a un tribunal que debe tener como eje principal la defensa de los derechos de la víctimas**". (Le puede interesar: [Inhabilidad de magistrados es inconstitucional)](https://archivo.contagioradio.com/incostitucional-comite-escogencia-jep/)

### **La importancia de la movilización ciudadana** 

A las organizaciones sociales les preocupa que la cúpula a militar y terceros queden por fuera de la JEP, pero manifiestan que la movilización ciudadana y la incidencia de la comunidad internacional es clave para lograr que se corrija la Ley Estatutaria. Asimismo, esa misma actividad de **las organizaciones sociales, las regiones y las víctimas, será lo que legitime la Comisión de la Verdad,** aunque desde algunos sectores se describa como una "Comisión de la venganza". Para Yepes, eso demuestra que los terceros involucrados con paramilitarismo temen a decir la verdad sobre el cómo y por qué se desarrolló el conflicto colombiano.

Cabe recordar que sobre ese tema, el presidente Juan Manuel Santos dijo esta mañana en [su balance de la implementación del acuerdo de paz en las emisoras de la fuerza pública y la Radio Nacional de Colombia que “**Es una torpeza de los terceros y sus defensores insistir en dejarlos por fuera de la JEP,** porque si se descubre que cometieron algún delito de lesa humanidad, con solo contar la verdad se hace acreedor a los beneficios, si se va a la justicia ordinaria podría enfrentar penas de hasta 60 años”.]

<iframe id="audio_22207565" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22207565_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
