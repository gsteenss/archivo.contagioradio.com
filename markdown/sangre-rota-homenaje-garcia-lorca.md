Title: "La sangre rota", un homenaje teatral a García Lorca
Date: 2017-12-13 12:49
Category: Cultura
Tags: Bogotá, Cultura, García Lorca, teatro
Slug: sangre-rota-homenaje-garcia-lorca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/image001-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: **Circuito Liquen** 

###### 30 Ago 2017 

En el marco del ciclo Mirada Paralela, que rinde homenaje al escritor y dramaturgo Federico García Lorca, llega al escenario de la Maldita Vanidad el director y coreógrafo Saeed Pezeshki para presentar una pieza inspirada en “Suicidio en Alejandría” y “Viaje a la luna”: **La Sangre Rota (y las naranjas Verdes)** con la actuación de Alejo Díaz, Daniel Diaza e Isabella Santiago. En temporada del 6 al 16 de Diciembre.

Dos hombres se encuentran en un baño público. Los dos comparten un motivo para estar ahí. Hay poco espacio para la reconstrucción de los afectos, los vínculos de la memoria y sus efectos en el cuerpo. Un acercamiento a los pensamientos que solo tenemos cuando estamos solos, lo incorrecto. El error. El cansancio del cuerpo. Ese cuerpo que cuenta todo lo que aún no existe. Lo que está en la memoria más profunda (o lejana).

La creación de La Sangre Rota (y las naranjas Verdes) parte de sucesos orgánicos, en los cuales la acción/movimiento conducen a la escena, generando un espectáculo donde el lenguaje común es el cuerpo. También se construyó sobre los universos de los textos: “Viaje a la Luna” y “Suicidio en Alejandría”, pertenecientes al periodo "surrealista" que Lorca compartió con Salvador Dalí, y es en esos dos textos donde habitan los personajes de la obra, en ese lugar inconcluso que todos adivinamos y a los que es inevitable acudir.

“El proceso de creación inició con un primer acercamiento en soledad al texto. Experimentando con esa metodología numérica de construcción anecdótica que presentan los textos lorquianos. Llegando después a un dibujo más cercano de lo que será la dramaturgia de la obra. Luego, ya en la práctica con los actores ese material de la palabra fue tomando forma en el cuerpo y en el espacio. Integrándose en una estética común. Es la visión de una realidad paralela, en la que nuestras conductas más privadas son expuestas. Donde se escucha el pensamiento como a un narrador que no sabe de lo que habla” afirma el director.

Saeed Pezeshki es director y fundador de la compañía de artes escénicas Circuito Liquen de origen mexicano, con presencia permanente en este país, Argentina y ahora Colombia, donde actualmente reside. Ha participado en festivales, circuitos e intercambios internacionales en Europa y América. (Le puede interesar: [Los versos de Neruda convertidos en canción](https://archivo.contagioradio.com/neruda-musica-poesia/))

**Funciones:** viernes y sábado a las 8:00 p.m. Domingo a las 6:30 p.m.  
**Bono de apoyo:** \$30.000. \$20.000 estudiantes.  
**50% Membresía Maldita Vanidad.**  
**Lugar:** Cra 19 \# 45 A 17. Barrio Palermo, Bogotá.  
**Reservas:** 6055312 / 3192487560/ lamalditavanidadteatro@gmail.com
