Title: Fue asesinado Mario Jacanamijoy, líder indígena en Caquetá
Date: 2017-11-27 13:33
Category: DDHH, Nacional
Tags: Caquetá, Jacanamijoy, Lider social
Slug: fue-asesinado-mario-jacanamijoy-lider-indigena-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/el-tiemp.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [27 Nov 2017] 

El pasado 25 de noviembre **fue hallado el cuerpo sin vida de Mario Jacanamijoy, líder del pueblo Inga**, que había sido reportado como desaparecido el pasado 23 de noviembre. Jacanamijoy fue encontrado con signos de tortura en la vereda Buenos Aires, Caquetá. Junto a su cuerpo fue hallado también Dubier Prieto, asesinado al parecer con arma blanca.

Jacanamijoy era reconocido al interior de su comunidad por el trabajo y la lucha por la defensa de los derechos indígenas y que participó en diferentes procesos organizativos políticos en su territorio, de igual forma dentro de su organización Tandachiridu Inganokuna, ejerció diferentes cargos a nivel departamental, desde la coordinación de asuntos étnicos hasta ser actualmente **el Consejero Departamental de Salud de la Mesa de Concertación de los Pueblos Indígenas**.

La gobernación de Caquetá rechazó el homicidio y manifestó que este hecho **“enluta a los pueblos indígenas y al Caquetá, en época de Paz**”, además el gobernador Álvaro Álvarez afirmó que “con profundo dolor debemos reportar la pérdida de un gran líder y finqueros del sector en hechos que son materia de investigación”. (Le puede interesar: ["Asesinan a Luz Jenny Montaño, lideresa comunitaria en Tumaco"](https://archivo.contagioradio.com/asesinan-a-luz-jenny-montano-lideresa-comunidaria-en-tumaco/))

Junto con Jacanamijoy son tres los líderes asesinados durante este fin de semana y se suman a los 182 líderes indígenas y sociales que han sido víctimas de la violencia y estigmatización hacia la labor que realizan en el país.

###### Reciba toda la información de Contagio Radio en [[su correo]
