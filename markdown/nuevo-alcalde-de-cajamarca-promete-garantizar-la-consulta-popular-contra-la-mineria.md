Title: Nuevo alcalde de Cajamarca promete garantizar la consulta popular contra la minería
Date: 2017-03-13 18:00
Category: Ambiente, Nacional
Tags: Cajamarca, consulta popular, Mineria
Slug: nuevo-alcalde-de-cajamarca-promete-garantizar-la-consulta-popular-contra-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RDSColombia 

###### [13 Mar 2017]

**Con 4756 votos Pedro Pablo Marín, topógrafo egresado de la Universidad del Quindío,** es el nuevo alcalde de Cajamarca, tras la realización de unas elecciones atípicas por el fallecimiento del anterior alcalde, William Poveda, quien murió en diciembre del año pasado.

Estas elecciones representan un nuevo obstáculos para los defensores del ambiente en esta región del país, donde el proyecto minero, La Colosa, de la empresa Anglogold Ashanti, **busca imponerse pese al rechazo de la comunidad. El nuevo alcalde es señalado de estar “al servicio de la multinacional”,** como lo han denunciado desde el Comité Promotor de la Consulta Popular NO a la Minería.

Frente a este nuevo panorama, el Comité solicita al nuevo alcalde, garantizar la realización de la consulta popular el próximo 26 de marzo. Asimismo, piden que el resultado de la consulta sea respetado por la administración entrante. Ante esa petición, el alcalde electo respondió que será respetuoso de la Ley y **se comprometió a garantizar que los ciudadanos puedan decidir sobre el futuro de la minería.**

Pedro Pablo Marín, fue apoyado por el Partido de La U y el Partido Conservador. De acuerdo con el último boletín de la Registraduría con la totalidad de las 35 mesas escrutadas Julio Roberto Vargas del Polo Democrático y Partido Liberal, obtuvo, 4.451 votos, mientras que Pedro Pablo Marín tuvo 4.756. **Una diferencia de 305 votos.**

Ante esta situación, los ambientalistas continúan su campaña de NO a la minería en su territorio. Cabe recordar que tras varios problemas para que 5.500 ciudadanos puedan expresar si están de acuerdo o no con la realización de este tipo de proyectos, en enero del 2017, **el Tribunal Administrativo del Tolima propuso una nueva pregunta que cumple con las exigencias constitucionales y legales.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
