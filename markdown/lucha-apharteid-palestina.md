Title: Lucha contra el Apartheid de Israel a Palestina se toma Bogotá
Date: 2017-03-30 15:09
Category: Hablemos alguito
Tags: Apharteid, BDS Colombia, Palestina
Slug: lucha-apharteid-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Palestina-asentamientos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [30 Mar 2017] 

La Semana Contra el Apartheid Israelí sobre Palestina es uno de los esfuerzos internacionales de movimientos civiles, instituciones y universidades en las principales ciudades del mundo, que busca crear conciencia sobre las políticas de apartheid y los proyectos colonialistas Israelíes sobre el pueblo palestino. [Consulte aquí la programación](https://archivo.contagioradio.com/apartheid-palestina-colombia/).

Sin embargo, el Movimiento de Boicot, Desinversión y Sancioines va mucho más allá de la \#IsraelApharteidWeek. Según lo explican en nuestro programa Isabel Rikkers, Oscar Suarez y Cindy González, integrantes de BDS Colombia, este compromiso y esta solidaridad ha trascendido y ha logrado posicionar la causa Palestina hasta el punto de que la empresa G4S ha anunciado la venta de sus negocios en Israel.

En el caso de Colombia los logros no son menores, muchas empresas han retirado los logos de G4S de sus fachadas, un ejemplo concreto es Crepes & Waffles, que también revisaría el contrato con la empresa de seguridad y varios artistas que se han comprometido con no asistir o participar en eventos comprometidas con el Estado de Israel.

![BDS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/DSC05667.jpg){.size-full .wp-image-38635 .aligncenter width="2445" height="1617"}

Este Hablemos Alguito también nos acerca a las realidades de tres jóvenes que a pesar del individualismo que es preponderante en estos tiempos, han decidido trabajar, solidarizarse y extender el mensaje de que a pesar de la crudeza de la situación en Palestina y lo lejana que parezca, es posible cambiar las cosas y es posible recuperar, poco a poco pero con paso firme, la dignidad del pueblo Palestino.

<iframe id="audio_17868475" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17868475_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
