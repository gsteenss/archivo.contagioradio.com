Title: PazHaremos, evento para ponerse la camiseta por la paz
Date: 2019-06-28 12:43
Author: CtgAdm
Category: eventos, Memoria
Tags: acuerdo de paz, Evento, MOVICE, paz
Slug: pazharemos-ponerse-camiseta-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/paz-haremos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Contagio Radio  
] 

Este viernes desde las 5:30 de la tarde se desarrollará **PazHaremos, un evento con el que se buscará hacer conciencia sobre el tránsito que debe hacer la sociedad hacía a la paz.** La cita será en el costado oriental de la Carrera 7ma con Calle 16; y allí, las personas podrán encontrar 4 estaciones en las cuales evidenciarán las razones para hacer ese tránsito y 'ponerse la camiseta' por la paz de Colombia.

### **Para transitar se encontrarán 4 espacios diferentes**

**Claudia Girón, integrante del Movimiento de Víctimas de Crímenes de Estado (MOVICE)**,  explicó que PazHaremos es una inciativa que surgió para rodear las instituciones que permiten hacer un transito hacia una sociedad diferente; para hacer dicho recorrido, en esta ocasión se dispusieron 4 espacios diferentes que permitirán abrir la mente y el corazón. (Le puede interesar: ["Las tres correciones a Luis Almagro sobre la implementación de la Paz en Colombia"](https://archivo.contagioradio.com/correcciones-almagro-implementacion-paz/))

En el primer espacio,habrá una especie de viacrucis por la paz, en la que se mostrarán algunos de los moentos claves del Acuerdo de Paz, mostrándolos en clave de la tradición Cristiana que enseña el proceso vivido por Jesús antes de ser crucificado. Con ello, esperan mostara que a pesar de los golpes recibidos la paz no está acabada, y depende de nuestros actos que sea una realidad. (Le puede interesar: ["Siete obispos firman comunicado por la paz y contra el Glifosato"](https://archivo.contagioradio.com/siete-obispos-firman-comunicado-por-la-paz-y-en-contra-del-glifosato/))

Posteriormente, quienes participen del evento encontrarán un bosque de memoria compuesto por varias organizaciones en la que se rendirá tributo a las personas que aunque fueron asesinadas o desaparecidas, siguen vivas en sus ideas, que muchos aún promueven. Posteriormente, en el espacio del sendero transitarán difernetes voces, "representadas en repertorios musicales y acciones teatrales performativas que dan cuenta de la paz".

En este espacio estará la cantante **Beatriz Mora, la Corporación Colombiana de Teatro, la madre de Soacha y cantante Lucero Carmona; también participará una chirimía y también César López.** En el tercer espacio, se encontrará una mesa para estampar camisetas con dos letreros: **"Del odio no saldrá una mejor Colombia" y "La paz es un bien común"**; con ello, esperan que las personas conversen y abran sus corazones a escuchar lo que tienen otros por decir.

En conclusión, Girón afirmó que PazHaremos **será un espacio y una invitación para ponerse la camiseta de la paz, que es importante en este momento, y trabajar por el efectivo tránsito de esta sociedad hacía la democracia y el respeto por el otro.** (Le puede interesar: ["No podemos hablar de paz sin nuestra participación política: Víctimas del conflicto"](https://archivo.contagioradio.com/no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto/))

> El Movice Capítulo Bogotá está convocando al evento 'Paz Haremos: arte y ciudadanía transformando país'??  
> ¡Acompáñanos y pintemos la paz! ??‍?  
> Este viernes a las 5:30 en la Cra 7 con calle 26. [pic.twitter.com/pOcDrKDUfE](https://t.co/pOcDrKDUfE)
>
> — Movice (@Movicecol) [26 de junio de 2019](https://twitter.com/Movicecol/status/1143909797185708033?ref_src=twsrc%5Etfw)

<iframe id="audio_37724835" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37724835_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
