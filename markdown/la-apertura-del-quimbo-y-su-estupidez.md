Title: ¡Cuánta estupidez tenemos que transformar! El Quimbo
Date: 2016-01-12 09:19
Category: Camilo, Opinion
Tags: Agua, El Quimbo, Hidroelectrica
Slug: la-apertura-del-quimbo-y-su-estupidez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/APERTURA-DEL-QUIMBO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: defensaterritorios] 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas){.twitter-atreply .pretty-link}  **

###### 12 Ene 2016 

De tal apelación en un trino, a la que sin duda alguna, hicieron eco lo medios empresariales de información, pues se juega con el fantasma de un eventual racionamiento por culpa del cierre ordenado por el poder jurisdiccional, se revelan muchas cosas.

Entre el derecho público y lo privado, para Santos, lo privado. En febrero a través de una Acción Popular un Tribunal del Huila ordenó una medida cautelar para impedir el llenado de la represa [El Quimbo](https://archivo.contagioradio.com/?s=El+Quimbo) por los daños irreparables en lo social y ambiental, los que se estaban produciendo con la obra y se producirán a mayor escala. Esta Acción Popular no nació de la nada, sí de la tozudez y persistencia de pobladores y ambientalistas que desde el inicio de la obra, bajo la llamada Alianza Público Privada, APP, ha ido haciendo llamados y fortalecido sus denuncias y organización ante la gravedad de las actuaciones multinacionales contra derecho.

La orden del juez se desacató de facto por el propio Santos, en octubre pasado. Como ya es usual en el presidencialismo despótico, las limitaciones ambientales se obvian con Decretos y estrategias de publicidad verde. A través del decreto 1979, Santos ordenó el llenado de la represa. En la expedición, de la fácil salida, el gobierno expresó que Emgesa, multinacional española, había cumplido a la fecha con el 99 por ciento de la recolección de la biomasa o desechos forestales, se aseguraba una calidad del agua y una adecuada oxigenación de la misma, que no afectaría a los piscicultores de una represa contigua Betania.

En diciembre pasado, la Corte Constitucional ordenó de manera inmediata el cierre de El Quimbo, afortunadamente reaccionaron, pues un decreto presidencial no puede desconocer una decisión de un juez.  Hace pocas horas, hoy 8 de enero, como lo expresa Emgesa en su página, esta se notificó del fallo de Tutela emitido por el Juzgado Tercero Penal de Neiva, que ordenó de forma transitoria e inmediata a la Empresa la generación de energía con la Central Hidroeléctrica de El Quimbo [http://www.<wbr></wbr>proyectoelquimboemgesa.com.co/<wbr></wbr>site/Prensa/Comunicados/<wbr></wbr>ElQuimboreiniciasuoperaci%C3%<wbr></wbr>B3n.aspx\#sthash.bljNiZPf.dpuf](http://www.proyectoelquimboemgesa.com.co/site/Prensa/Comunicados/ElQuimboreiniciasuoperaci%C3%B3n.aspx#sthash.bljNiZPf.dpuf)

La decisión que es temporal, no resuelve el asunto de fondo y se convierte en un salvavidas para Emgesa y la dupla Santos Vargas Lleras, y muchos privados internacionales. Todo el Magdalena pretende convertirse en territorio para  las multinacionales. La expedición del 1979 además de la producción del 4 por ciento de energía para el país, y la cruzada Santista, lo que quiere asegurar es la inversión de 40 millones de dólares de Emgesa y el tránsito fluvial de las multinacionales petroleras.

Mentiras es lo que hay detrás del discurso oficial. El aumento del caudal, como lo señaló hace 10 meses, el diario Portafolio es para transportar fluvialmente el crudo del Putumayo, del Caquetá, Huila y Santander por esta subregión del Huila. El cierre de El Quimbo no produjo un nivel menor del estiaje durante este período, el menor nivel del caudal en este sector del río Magdalena es otra expresión del calentamiento global.

La apertura de El Quimbo no resuelve un eventual racionamiento nacional debido a políticas ineficaces de previsión, ni va evitar el aumento de las tarifas a los usuarios, las que ya se contemplaban desde mediados de 2015.

El Quimbo es la presentación internacional de las APP por eso la reacción de Santos. Poco importan los nativos, poco la soberanía y la justicia, esa justicia es solamente un poder real coyuntural. En realidad, la justicia cuenta con poca autonomía para amparar los derechos ambientales y sociales. Por encima del derecho está el poder del ejecutivo (político) y más arriba el poder multinacional que “legalmente” se asegura en Tribunales de Arbitramento en donde estos, gracias a los Tratados de Libre Comercio o Tratados Bilaterales se han amparado, es lo que llaman los tres huevitos de Uribe y que hoy Santos mantiene bien calienticos.

El clamor de Santos no es porque los colombianos vuelvan a padecer un racionamiento de energía o porque con El Quimbo se enfrente uno de los efectos del fenómeno de sequía y desertización que padecemos como una de las consecuencias del cambio climático mundial-

Si la democracia quiere sostener la paz, se requiere reiventar la justicia, más que la jurídica penal carcelaria, está debe ser, junto a la sancionatoria, para restaurar la ruptura a un nuevo Pacto Social o una lograda justicia social y ambiental- Esa democracia, solo podrá ser real sobre la base de rupturas de diverso tipo, entre ellas, las del basismo y el vanguardismo.

Las construcción de una democracia profunda es la transversalidad, la revolución sensitiva y perceptiva, ante la idiotez mediática empresarial; la ruptura frente a prácticas llamadas “populares” en las que se están destruyendo los ríos con la deforestación intensiva, los paseos de olla sin conservación y sin mitigación de los daños que se dejan en la juerga y el festejo, que también mercancía se volvió. Porque para muchos de los pobres, los ríos son para los desechos, para recrearse en medio de latas, de plásticos, del bulloso narco corridos, vallenatos o reguetón, que infantiliza sus conciencias, y que va convirtiendo todo en un basurero, en que han hecho trizas sus sueños, entre ellos, los de su sobrevivencia, día a día.

La transformación para ser soberana nos implica a todos, y en diversos niveles. Tan indignante es golpear a una mujer, a un animal, como hacer de los ríos, aún sin represas, estiércol, porque no el agua no es mercancía. La democracia real será tal si los ciudadanos todos y sus líderes, somos concientes de nuestros sentimientos y sueños para establecer unas relaciones de poder basadas en la justicia socio ambiental.
