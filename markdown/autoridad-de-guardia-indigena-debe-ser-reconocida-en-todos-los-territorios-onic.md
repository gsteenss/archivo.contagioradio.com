Title: Autoridad de Guardia Indígena debe ser reconocida en todos los territorios: ONIC
Date: 2018-01-29 17:34
Category: DDHH, Nacional
Tags: Ejército Nacional, guardia indígena, ONIC
Slug: autoridad-de-guardia-indigena-debe-ser-reconocida-en-todos-los-territorios-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/indigena_p.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [29 Ene 2018] 

El pasado 27 de enero fue asesinado **Eleazar Tequia, Guardia Mayor Indígena del pueblo Kankuamo**, en la vía Medellín-Chocó, de acuerdo con la Organización Nacional Indígena de Colombia, ONIC, el asesinato fue cometido por integrantes de la Fuerza Pública, cuando los indígenas, después de una movilización, se disponían a retornar a sus territorios.

Por ello la Organización Nacional Indígena, ONIC,  exigió que **la Fuerza Pública reconozca a la Guardia Indígena como órgano de control social** y legítima dentro de los pueblos indígenas y que se encuentren los responsables de la muerte del indígena.

De acuerdo son Cilsa Arias, vocera de comunicaciones de la ONIC, los hechos se presentaron en el kilómetro 18 de la vía que comunica a Quibdó con Medellín, cuando integrantes del **Ejército Nacional,** llegaron a la zona debido a que camioneros denunciaron que estaban siendo extorsionados. A su vez, en este lugar se encontraba Tequia con otro compañero indígena de la Guardia, asegurándose del retorno de los indígenas al territorio.

Arias, aseguró que ambos indígenas fueron llamados por miembros del Ejército para realizar una requisa y posteriormente fue encontrado el cuerpo de Tequia, sin vida. El Ejército Nacional aseguró que el hecho se produjo cuando un integrante de la Fuerza Pública estaba siendo desarmado por indígenas que evitaban que se le retirara el arma a una persona que estaba allí, y que en **“consecuencia de la acción criminal violenta” Tequia resultó sin vida**.

Sin embargo, Cilsa Arias aseveró que el cuerpo del indígena fue hallado con dos impactos de bala y que **los indígenas solo estaban armados con su bastón de mando** “es tan confusa la comunicación que genera el Ejército Nacional que dice que una sola persona desarmó a dos, ahí muestra las incoherencias y una intensión de desviar el verdadero hecho”.

### **La Guardia Indígena legítima en los pueblos ancestrales** 

La ONIC también hizo un llamado al gobierno y la Fuerza Pública en general, para que se respete el accionar de la Guardia Indígena y la legitimidad que tiene al interior de los pueblos ancestrales, en ese sentido Silsa Arias señaló que esta además es una guardia que ha propendido por la defensa de la paz, razón por la cual su única arma es el bastón y afirmó que fue por este motivo que **se decomisaron las armas y fueron retenidos los integrantes del Ejército que estuvieron en los confusos hechos del asesinato del Tequia**.

El día de ayer, tanto los soldados como las armas fueron entregadas a una comisión de la Cruz Roja Internacional. De igual forma, hoy se llevó a cabo una reunión con organizaciones internacionales defensoras de derechos humanos y altas instancias del gobierno nacional para esclarecer este acto de violencia y garantiza la vida de las comunidades indígenas. (Le puede interesar: ["Indígenas del Cauca logran acuerdos con Ministerio de Educación"](https://archivo.contagioradio.com/indigenas-del-cauca-logran-acuerdo-con-ministerio-de-educacion/))

<iframe id="audio_23429441" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23429441_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
