Title: ¡Ríos Libres para cosechar la Paz!
Date: 2019-03-30 06:00
Category: CENSAT, Opinion
Tags: CENSAT, represas, ríos
Slug: rios-libres-para-cosechar-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/censat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

###### 30 Mar 2019

##### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)**

Cada año el 14 de marzo se celebra el día Internacional de Acción contra las Represas y en Defensa de los Ríos, el Agua y la Vida el cual fue adoptado en marzo de 1997 en Curitiba (Brasil) en el primer encuentro internacional de afectados por las represas. Alrededor del mundo en esta jornada se realizan numerosas actividades que dan cuenta de las afectaciones de los proyectos hidroenergéticos y de las reivindicaciones de las comunidades afectadas para mejorar sus condiciones de vida.

[En el último período viene creciendo el cuestionamiento de este tipo de infraestructuras energéticas por situaciones que han puesto en evidencia las dimensiones y efectos negativos que pueden llegar a tener las externalidades no previstas de este tipo de proyectos. La represa Yacyretá al Sur del continente donde el proyecto ha estado rodeado de escándalos de corrupción y las comunidades temen por las fracturas del muro que han constatado y denunciado técnicos e investigadores; la ruptura de la represa de lodos tóxicos de la minería en Mariana y Bento Rodrígues donde se contaminó el  Río Doce en más de 600 kilómetros hasta su desembocadura en el mar o la ruptura de una presa similar que arrasó con Brumadinho y contaminó el río Paraopebas, ambos casos en Brasil, ambos casos en el Estado de Minas Gerais, ambos casos de la empresa Minera Vale y en ambos casos podrían haberse prevenido la catástrofes que dejan a la fecha a miles de personas enfermas, desaparecidas, muertas y un drama social y ambiental que el mundo debe atender. En estos casos como en el de Hidroituango en Colombia, las comunidades alertaron sobre las afectaciones que vendrían pero ningún Estado atendió sus voces, ningún Estado evitó las tragedias.]

[Frente a este escenario para la celebración del 14 de marzo en Colombia el Movimiento Ríos Vivos convocó a la]**IX Jornada Nacional en Defensa de los Territorios**[ que se extiende durante todo marzo en la que se contemplaron acciones de movilización, formación, artísticas y culturales para celebrar y defender la vida y las aguas. El mensaje de esta jornada es que para cosechar la paz,]**los ríos deben correr**[ l]**ibres**[, se entiende a su vez que los ríos están hablando por ellos mismos, que a pesar de su represamiento siguen luchando por vivir y correr libres, tal ese el caso de río Cauca que puso en jaque a Hidroituango, del Río Sinú que llama la atención de la entidades regionales y nacionales por la regulación del caudal de la represa Urrá I que ha puesto en riesgo a varias comunidades del Bajo Sinú o el Río Magdalena que sigue poniendo en aprietos a la represa El Quimbo, entre otros. La jornada nacional también contempló acciones comunitarias de reforestación de cuencas en la cuenca del río Sinú y afluentes del río Cauca, la implementación de proyectos de energía alternativa, articulación e integración social, formación propia, y actividades artísticas de calle entre otras.]

[En el oriente antioqueño las comunidades y organizaciones articuladas en el Movimiento por la Defensa de la Vida y el Territorio -MOVETE convocaron acciones enfocadas en el fortalecimiento organizativo y de incidencia para defender los ríos del oriente antioqueño que han sido represados y los que aún corren libres pero que están amenazados por la proyección de más de 60 pequeñas hidroeléctricas (PCH´s) y, cuando menos, tres grandes hidroeléctricas. Esta manera de generación de energía aumenta exponencialmente las afectaciones de comunidades en Latinoamérica; por tal motivo en Panamá, Costa rica y en el caso de Colombia en el departamento del Quindío también se adelantaron acciones enfocadas en la defensa de sus territorios frente a las incursiones de empresas que hacen caso omiso de las disposiciones de la legislación ambiental y la voluntad de las comunidades locales.]

[Es de recordar que las Pequeñas Centrales Hidroeléctricas y sus obras asociadas, bajo la lógica en que están siendo promovidas, no son una opción de generación limpia, sus impactos han cercenado las aguas de más de 70 quebradas en Tolima, secado más de 23 quebradas en Caldas generando presión sobre la producción de alimentos y la cultura campesina y en Panamá han hecho que muchos de sus ríos ya no lleguen con agua al mar.]

[Esta nueva jornada de acción avanza en llamar la atención sobre la necesidad urgente de replantear el modelo de gestión de la energía y de las aguas en el país. Replantear estos modelos implica avanzar en una transición energética justa que contribuya a la vida y libertad de los ríos, a desacelerar el calentamiento del planeta y a garantizar los derechos de todas las comunidades ribereñas del país.]

[¡Ríos Libres para Cosechar la paz!]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
