Title: FARC pide perdón a sus víctimas por las retenciones
Date: 2016-09-12 16:33
Category: Nacional, Paz
Tags: diputados del valle, Equipo de paz del gobierno, FARC, Juan Manuel Santos, masacre de la chinita, plebiscito por la paz
Slug: farc-piden-perdon-tras-reunion-con-familias-de-diputados-del-valle-y-victimas-de-la-chinita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/victimas-diputados-del-valle-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [12 Sep 2016] 

Este fin de semana las delegaciones de paz del gobierno y las FARC sostuvieron reuniones con las familias de los **11 diputados del Valle y las víctimas de la masacre de “La Chinita”** en la que se establecieron las bases de confianza y de los actos de perdón que las víctimas le exigieron a las FARC. Tras el encuentro Iván Márquez, jefe de la delegación de paz de las FARC pidió perdón generalizado por las retenciones realizadas por esa guerrilla.

Las **exigencias que hicieron los familiares de los diputados a las FARC** fueron 5 que hicieron explícitas en un comunicado público. En primer lugar exigen realizar en la ciudad de Cali un acto público de perdón y reivindicación por parte de las FARC-EP que se desarrollaría después del plebiscito y  con una declaración expresa de perdón al Ex-Diputado Sigifredo Lopez y su familia.

También exigieron que se haga una entrega formal de los restos y las pertenencias de sus familiares, que se cuente la verdad de los hechos en los que murieron los diputados, así como la manifestación pública de **quiénes fueron los responsables intelectuales y materiales tanto del secuestro como del asesinato**. Al gobierno colombiano le exigen la declaratoria de los diputados como “Heroes Nacionales de Paz” y la construcción de un monumento nacional en su memoria.

En cuanto a las víctimas de la masacre de “La Chinita” se espera un comunicado oficial  del mecanismo, pero se podría prever que el acto se asemeje al realizado hacia **las víctimas de la [masacre de Bojayá](https://archivo.contagioradio.com/sentido-acto-de-perdon-se-realizo-hoy-en-bojaya/)**, el cual fue una experiencia piloto de varias semanas de preparación en las que las víctimas tuvieron una participación abierta y directa.

Por su parte, Iván Márquez, luego del encuentro al que también asistió Monseñor Darío Monsalve, aseguró en el video publicado por NCN, que las retenciones nunca más se volverán a cometer y esperan **que el [Estado colombiano tome ejemplo y también pida perdón a sus víctimas.](https://archivo.contagioradio.com/bojaya-viejo-el-nacimiento-de-lo-nuevo/)**

[Comunicado Prensa Familiares de Diputados La Habana](https://www.scribd.com/document/323776852/Comunicado-Prensa-Familiares-de-Diputados-La-Habana#from_embed "View Comunicado Prensa Familiares de Diputados La Habana on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_62261" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/323776852/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-doBH4QEd04E963mEk4zt&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
\[embed\]https://www.youtube.com/watch?v=C1SIbU9Ba-c\[/embed\]
