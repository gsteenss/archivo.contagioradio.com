Title: "Hasta que yo tenga vida voy a seguir luchando por buscar la verdad" René Guarin
Date: 2015-10-21 16:39
Author: AdminContagio
Category: Entrevistas, Resistencias
Tags: Coronel Plazas Vega, Cristina del Pilar Guarín, das, Desaparición forzada en Colombia, F2, Holocausto Palacio de Justicia, Lucy Amparo Oviedo, Luz Mary Portela, Medicina Legal, René Guarín
Slug: hasta-que-yo-tenga-vida-voy-a-seguir-luchando-por-buscar-la-verdad-rene-guarin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Rene_Guarin_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [protestas-seryozem]

<iframe src="http://www.ivoox.com/player_ek_9116200_2_1.html?data=mpaemJeUdI6ZmKiak5qJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptjhw8nTb8TjzdTaxM7FstCfwoqwlYqmhc%2Bfz9TgjcnJpsafzcaY2MrWqMLYhqqfh52UaZq4hpewjbfJsoa3lIqum5CruY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rene Guarín, familiar desaparecida] 

###### [21 Oct 2015]

[Tras el hallazgo por parte del Instituto Colombiano de Medicina Legal de los restos mortales de Cristina del Pilar Guarín, desaparecida durante la retoma del Palacio de Justicia en noviembre de 1985, René Guarín hermano de la víctima, asegura que este hecho le genera alegría, tristeza y expectativa, pues **todavía no conoce las circunstancia de modo, tiempo y lugar en que fue asesinada su hermana**.  ]

Recibir el informe de Medicina Legal para René Guarín, fue “un momento demoledor… creo que lo que me golpeó mucho emocionalmente fue la bolsa que decía prendas de vestir… en esa bolsa estaba un **trozo de la falda con la cual vimos salir a Cristina viva del Palacio de Justicia en custodia de miembros del Ejército**”.

Guarín afirma también que es doloroso que sus padres no hayan podido recibir esta noticia, "murieron hace unos años", varios de sus hermanos también fallecieron y los que han quedado decidieron no asistir al acto de Medicina Legal ni participar de las acciones de búsqueda "por miedo", "no he tenido familia con quien compartir lo que siento en este momento".

“Hay un **sentimiento de alegría y hay un sentimiento de tristeza** puesto que los padres de Lucy Amparo Oviedo, Luz Mary Pórtela y Cristina Guarín no están, que son las personas que pensamos deberían haber recibido estos restos, nos tocó a nosotros los hermanos recibir este informe sobre los restos”, agrega Guarín.

“Falta un pedazo del rompecabezas y es conocer la verdad de lo que pasó con nuestros familiares…el **Estado colombiano aún nos debe la verdad**” y pese a que “hemos recibido maltrato de parte del Estado colombiano y de algunos medios de comunicación, creo que hay que levantarse con la frente en alto, con dignidad y seguir adelante en la lucha”, afirma Guarín.

Preguntas como “Qué buscó el **Estado colombiano alterando las escenas del crimen**, por qué concentraron los cadáveres en el primer piso del Palacio, por qué la Alcaldía Mayor de Bogotá ordenó apresuradamente lavar el Palacio de Justicia, por qué aquí el **Estado ha sido juez y parte durante estos 30 años** y los primeros 20 no investigó nada, por qué quedaron **por fuera de las investigaciones el F2, el DAS y la Policía Nacional**, qué llevo a la Fiscalía a hacer estas 16 exhumaciones 30 años después, coincidencialmente a pocos días del anuncio del acuerdo de La Habana entre el Gobierno y las FARC”, son las que les quedan a estos familiares.

[Este hallazgo, según Guarín, “es el resultado de **30 años de persistencia** de los familiares de los desaparecidos, quienes comenzamos una lucha con nuestros padres y madres… para **buscar a las personas que habíamos visto salir vivas del Palacio de Justicia y de las cuales el Estado no nos daba razón**”.  ]

[En relación con las últimas aseveraciones del **Coronel Plazas Vega** acerca de la veracidad de su tesis de que los cuerpos estaban extraviados y que estas 11 personas no están desaparecidas, Guarín indica que “él tiene unos líos jurídicos que debe solucionar y **no se puede montar sobre los restos de Lucy Amparo, de Luz Mary y de Cristina del Pilar para pedir su libertad**”.]

[“Es evidente que **personas salieron con vida del Palacio de Justicia**, fueron conducidas a la Casa del Florero… **fueron torturadas, asesinadas y desaparecidas**… y que desde hace 30 años se ha intentado tender un manto de impunidad alrededor de este crimen de Estado”, por eso “nosotros somos unos tercos buscadores de esperanza y verdad, **siempre seremos tercos buscadores de dignidad**”, concluye Guarín.]
