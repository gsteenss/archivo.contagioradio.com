Title: Maestros mexicanos anuncian que las movilizaciones continuarán
Date: 2016-07-11 13:38
Category: El mundo, Movilización
Tags: CNTE, Movilizaciones maestros en Mexico, Reforma educativa en México
Slug: maestros-mexicanos-anuncian-que-las-movilizaciones-continuaran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Maestros-México.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Entrevision ] 

###### [11 Julio 2016 ]

El pasado sábado, la 'Coordinadora Nacional de Trabajadores de la Educación' de México decidió continuar con las **movilizaciones en por lo menos dieciocho estados, para rechazar la reforma educativa** impulsada por Enrique Peña Nieto; mientras retoman los diálogos con las autoridades y buscan salidas negociadas al conflicto entre el gremio y el gobierno.

Miembros de la Coordinadora insistieron en que **estos diálogos deben resultar en la derogación de la reforma** y confían en que el encuentro de este lunes con Miguel Ángel Osorio, secretario de gobernación, resulte en soluciones efectivas, que incluyan la libertad para los maestros que han sido arrestados [[en el marco de las movilizaciones](https://archivo.contagioradio.com/a-8-asciende-la-cifra-de-asesinatos-tras-represion-policial-en-oaxaca/)].  
Desde hace tres años, el gremio magisterial viene adelantando movilizaciones contra esta reforma educativa; sin embargo, durante los últimos meses se han intensificado, sobre todo en estados como Guerrero, Chiapas y Oaxaca, en los que **se han presentado confrontaciones con la Policía**, como la ocurrida el pasado 19 de junio en la que murieron ocho personas, decenas resultaron heridas y detenidas.

Dentro de las próximas actividades de movilización se contemplan marchas y bloqueos en carreteras. Este lunes los maestros se sumarán a un mitin en apoyo a los familiares de los [[43 normalistas de Ayotzinapa](https://archivo.contagioradio.com/?s=ayotzinapa+)]; el próximo miércoles realizarán una megamarcha desde el Palacio de las Bellas Artes hasta el Senado; **el sábado harán una caravana motorizada desde Oaxaca, que culminará el martes 19 de julio en el Zócalo**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio] 
