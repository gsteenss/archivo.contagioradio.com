Title: Acuerdos de París no resuelven crisis del Cambio Climático
Date: 2016-11-10 17:17
Category: Ambiente, Entrevistas
Tags: Acuerdos COP 21, cambio climatico, COP 22
Slug: acuerdos-paris-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cambio_climático-e1478814999334.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lopezdoriga] 

###### [10 Nov de 2016] 

Varias organizaciones ambientalistas manifiestan que los acuerdos de París y los mecanismos que pretende crear la COP22 para implementarlos, **son insuficientes y no responden a las necesidades reales e inmediatas de las comunidades más afectadas por el cambio climático.**

Tom Kucharz integrante de Ecologistas en Acción, manifestó que “el mundo ha perdido mucho tiempo con las conferencias climáticas, pues están bastante **coptadas por grandes corporaciones y las empresas más contaminantes del mundo que tienen una enorme y muy peligrosa influencia en las cumbres”.**

Kucharz señala que para el año 2100 habría un aumento considerable en la cantidad de Gases Efecto Invernadero que elevaría la temperatura global hasta 3,7ºC, lo que ocasionaría la **muerte de millones de personas, desplazamientos forzados, la pérdida de fuentes de agua, fuentes de alimentación** e impactos muy severos en las costas del sureste asiático, e islas del pacifico que se verían **amenazadas por fuertes inundaciones y huracanes.**

En la actualidad mas de **300 mil personas mueren al año por impactos del cambio climático**, así que “no basta con que entre en vigor el acuerdo de París, **necesitamos mucha mayor ambición y voluntad política por parte de los Gobiernos,** quienes deben asumir mayores compromisos en la lucha contra el cambio climático” señala el ambientalista.

### **¿Cuáles son las alternativas de las comunidades?** 

Lo principal es hacer un verdadero cambio en el modelo energético, que debe basarse en energías renovables y no en el modelo neoliberal donde prima el uso de combustibles fósiles, Kucharz afirma que **“hay que eliminar radicalmente todas las subvenciones de dinero publico que se destinan a financiar las grandes empresas petroleras, de gas natural, carbón y deforestadoras”.**

Resalta que también debe descentralizarse el sistema energético **“para que las comunidades locales tengan el control de la producción y distribución de los recursos** (…) hemos visto que cuando esto sucede, las comunidades trabajan en sistemas mas baratos pero que además son los más limpios”. Le puede interesar: [Cambio climático afecta con mayor fuerza a la población vulnerable del mundo.](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/)

La agricultura familiar campesina es otra de las alternativas vitales para afrontar la crisis ambiental, “podría reducir enormemente los gastos energéticos y el uso de agrotóxicos (…) **se pondría freno a los impactos nocivos de grandes farmacéuticas e industrias agroalimentarias como Bayer y Monsanto”.**

Por ultimo llama la atención sobre el compromiso que deben asumir los Gobiernos de Colombia y otras países de América Latina, quienes han sido los más afectados y los que cuentan con mayor cantidad de bienes naturales, **“estos gobiernos deben cambiar las políticas de comercio exterior con potencias como China, EEUU y la Unión Europea, que son los países que fomentan los modelos extractivistas en América Latina.**

<iframe id="audio_13702586" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13702586_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
