Title: Se realizará la primera misión de verificación de DDHH del Pueblo Ziobain de Putumayo
Date: 2017-06-14 06:53
Category: DDHH, Nacional
Tags: Amerisur, Misión de Verificación, Putumayo
Slug: mision_verificacion_derechos_humanos_indigenas_putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/ziobain-e1497440490314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @PuebloZiobain 

###### [14 Jun 2017] 

Las reiteradas denuncias de las comunidades indígenas del Resguardo Siona de Buenavista, en Putumayo, que desde hace varios años viene asegurando que la subsistencia de su pueblo se encuentra en peligro por las constantes violaciones a los derechos humanos, lograron que **del próximo 17 al 24 de junio se desarrolle la I Misión interinstitucional de Verificación de Graves Violaciones a DDHH y DIH** ocurridas contra su población.

[[[Factores como el conflicto armado, el paramilitarismo, la falta de presencia estatal y la amenaza extractivista por parte de la empresa petrolera Amerisur han hecho que en pocos años la población de la comunidad ZioBain se haya reducido a la mitad. Hace unos]]] **[[[**10 años la comunidad contaba con cerca de 5000 integrantes,** **pero** **hoy solo** **cuenta con** **2400.**]]]**

[[[A finales del 2016, las autoridades indígenas del pueblo ZioBain ya se habían trasladado a Bogotá para]]]**[[[**denunciar la grave situación de vulnerabilidad en que se encuentran,** principalmente por la petrolera que **pretende adquirir el bloque Putumayo 12 para desarrollar extracción de** **crudo** **con sísmica**]]]**[[[, a pesar que en consulta previa del año 2014 la comunidad dio un rotundo no al proyecto. [(Le puede interesar: Ante el embajador británico el pueblo ZioBain denuncia situación de DDHH](http://pueblo%20ZioBain))]]]

### [[[La Misión]]] 

Teniendo en cuenta esa situación, la comunidad ya había empezado un proceso de exigibilidad de derechos, mediante la Minga de Pensamiento y Resistencia en el marco de la sesión extraordinaria de la Comisión de Derechos Humanos de Pueblos Indígenas celebrada el pasado 23 de marzo en la capital.

Desde esa fecha hasta hoy se han desarrollado tres reuniones a las cuales, **no asistió la Agencia Nacional de Hidrocarburos ni la Defensoría del Pueblo,** como lo había solicitado la comunidad. A las comunidades les preocupa especialmente la ausencia de la Defensoría ya que esta debía liderar la Misión, como ya se había comprometido en reuniones anteriores. [(Le puede interesar: Pueblos indígenas de Putumayo se niegan a actividades petroleras)](https://archivo.contagioradio.com/comunidad-indigena-en-putumayo-dijo-no-a-actividad-petrolera-de-amerisur/)

Para dicha Misión, las instituciones que han confirmado su asistencia son: el Ministerio del Interior, la Consejería Presidencial para los Derechos Humanos, el Ministerio del Ambiente, la Autoridad Nacional de Licenciamiento Ambiental, Corpoamazonia, la Unidad para la Atención y Reparación Integral a las Víctimas, DAICMA y la Procuraduría. Asimismo, contarán con el acompañamiento de la Secretaria técnica de la Comisión de DDHH de Pueblos Indígenas, OPIAC y otras, ONGs de DDHH.

Dicha misión dará como resultado un informe que date las violaciones a los derechos humanos de la comunidad, que se desarrollará mediante los testimonios de la comunidad y la constatación visual y técnica que realicen los funcionarios.

Un informe que llamará la atención a las autoridades para que se **tomen las medidas de prevención, protección y garantía inmediatas** de acuerdo a la situación que vive la comunidad.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
