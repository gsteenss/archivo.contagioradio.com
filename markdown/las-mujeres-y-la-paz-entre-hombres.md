Title: Las mujeres y la paz entre hombres
Date: 2016-09-25 21:12
Category: invitado, Itayosara, Opinion
Tags: conversaciones de paz, itayosara rojas, mujer, Opinión
Slug: las-mujeres-y-la-paz-entre-hombres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paz-e1472858979891.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Itayosara Rojas Herrera](https://archivo.contagioradio.com/category/itayosara/)** 

###### 25 Sep 2016 

*“Cada vez estoy más convencida de que es necesario expresar aquello que para mí es importante, es necesario verbalizarlo y compartirlo, aún a riesgo de que se interprete mal o se tergiverse. Creo que, por encima de todo hablar me beneficia”*

*Audre Lordé*

El 26 de septiembre será un día que pasará a la historia, los periódicos del mundo registrarán que en Colombia la guerra entre la insurgencia armada de las FARC-EP y el Estado colombiano ha llegado a su fin. Los titulares del mundo serán impactantes mensajes de esperanza, los gobiernos del mundo felicitaran al pueblo colombiano y a los HOMBRES que hicieron la paz, líderes del mundo llamarán a Santos y felicitarán a Timochenko, y la prensa acompañará a sus contundentes mensajes de esperanza con fotos en las que solo aparecen hombres estrechando la mano o firmando el acuerdo histórico.

Y sí el 26 de septiembre será un día histórico se pondrá fin a una guerra, y es imposible que con ello no salgan a la luz los mensajes esperanzadores y románticos  de paz que nos moralizan y permiten seguir peleando. Pero me incomoda profundamente que la vocería y la representación de las mujeres a lo largo de este largo proceso que termina el 26 de septiembre sea una representación mínima, una vocería restringida y que nosotras compañeras de distintos procesos que hemos luchado junto a hombres por la paz con justicia social debamos seguir guardando silencio.

Me incomoda profundamente no poder ver a una guerrillera en la dirección de las FARC, firmando la paz, me incomoda profundamente no encontrar un cubrimiento serio de prensa a las acciones que las mujeres de la sociedad civil  han adelantado durante años para trabajar en el fin último de la paz con justicia social. Me incomoda que el resultado de un proceso colectivo entre hombres y mujeres sea capitalizado solo por hombres. Me incomoda que no se visibilice a la mujer en esta lucha de paz con justicia social, me molestan los especiales de SOHO de guerrilleras sin ropa o maquillándose, me molestan que conviertan a las ex combatientes ahora en el nuevo fetiche sexual, me molesta que la figura de la mujer sea tomada como símbolo de dignidad inventado por hombres, sin reconocerle plenamente su participación política.

Por último me molesta profundamente que al reclamar legítimamente mi derecho, nuestro derecho de ser reconocidas y  para gozar de los mismo derechos de participación en los escenarios de construcción y reconocimiento que tienen los hombres, se me conteste con un “no es tan grave”  o “en los acuerdos se les dio pleno reconocimiento a las mujeres con el enfoque de género” o que en el peor de los casos se me exija silencio para no ventilar los problemas de la izquierda de forma pública.

Y es que creo que sí, que en efecto este momento abre puertas a debates que por miedo o censura no se me permitieron ni a mí, ni a compañeras, dar al interior de organizaciones de izquierda.   El enfoque de género que acompaña y atraviesa de manera transversal  el acuerdo final para la terminación del conflicto es un avance sustancial, es de algún modo el reconocimiento al papel de la mujer en esta larga lucha por la construcción de paz.

Pero es insuficiente sí al interior de las organizaciones de izquierda se mantienen las prácticas misóginas que atacan a las mujeres, es insuficiente sí los medios siguen mostrando a los hombres haciendo la paz, es insuficiente sí las revistas quieren retratar guerrilleras o ex guerrilleras sin ropa, es insuficiente sí los medios buscan entrevistar a los genios hombres de la política, es insuficiente a la luz de las prácticas machistas al interior de las instituciones del Estado y de las “instituciones” de izquierda y resulta aún más insuficiente al observar hombres que no quieren reconocer este problema y que nos piden silencio para seguir asumiendo su rol de vocero y representante a costa de la exclusión de las mujeres que han puesto cada ladrillo por la paz,  pero que no pueden y no se les permite salir en la foto.

Me niego a aceptar que se me desconozca, que se nos desconozca en los procesos de construcción colectiva que han llevado a la paz, me niego a creer que no haya ni una sola mujer en la dirección de la insurgencia de las FARC y me niego a aceptar un feminismo conveniente y complaciente con las prácticas misóginas de hombres al interior de los procesos de izquierda. Pero también con esperanza y coraje me sumaré con todos hombres y con todas mujeres este próximo 26 de septiembre a la plaza de Bolívar para celebrar conjuntamente con esos mismos rostros, con los que  de la mano hemos peleado por este momento y por la construcción de paz, de nuestra paz, con cambios y con más luchas, sin prácticas de discriminación ni opresión, sin misoginia. Me sumaré a la plaza con mis compañeras y camaradas exigiendo no solamente aparecer en el discurso de un hombre, sino también dando el discurso, firmando la paz y peleando en las calles.
