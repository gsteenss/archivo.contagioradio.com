Title: Vendedores Informales salieron a marchar exigiendo su derecho al trabajo
Date: 2015-11-20 17:55
Category: DDHH, Nacional
Tags: Alcaldía de Bogotá, Gustavo Petro, Hoy los vendedores informales salieron a marchar, San Victorino, Vendedores informales
Slug: vendedores-informales-salieron-a-marchar-exigiendo-su-derecho-al-trabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:youtube 

<iframe src="http://www.ivoox.com/player_ek_9459034_2_1.html?data=mpmim5WXeI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRmsbixcrR0dfJt4y9z8vc1NLFsMbnjNjOzs7JttDijMaYz8bWp8nV05DS2s7LrcbixdSY1dqPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jaime León] 

<iframe src="http://www.ivoox.com/player_ek_9459043_2_1.html?data=mpmim5WYd46ZmKiak5mJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmsbixcrR0dfJt4y9z8vc1NLFsMbnjNjOzs7JttDijMaYz8bWp8nV05DS2s7LrcbixdSY1dqPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Viviana Martínez] 

###### [20 nov 2015 ] 

Este viernes más de dos mil vendedores y vendedoras informales  salieron a las calles del centro de Bogotá desde las 9:00 de la mañana para marchar en rechazo al abuso por parte de la policía y exigiendo que se les respete su derecho al trabajo de los **más de 48 mil vendedores ambulantes que hay en la ciudad. **

Con esta movilización se busca lograr una mesa de concertación con la Alcaldía de Gustavo Petro, con el fin de que se garantice un lugar digno de trabajo donde sean re ubicados, de tal forma que los trabajadores informales puedan tener una estabilidad económica. Así mismo se exige que las mercancías decomisadas sean devueltas.

De acuerdo con Jaime León, integrante de la Corporación Clartiana Norman Pérez Bello, la mayoría de los vendedores informales son desplazados, personas de la tercera edad o madres cabezas de hogar como  es el caso de **Viviana  Martínez, madre de 4 hijos a quien desplazaron su lugar de trabajo y la mandaron al Parque Tercer Milenio,** donde le es difícil vender debido a la mala imagen que tiene esta zona de la capital, como lo cuenta Viviana. Para ella, este hecho ha generado que nuevamente se encuentre sin recursos para mantener a su familia.

**“Tenemos que dejar algo en concreto para que con la nueva alcaldía se continué en el proceso, queremos brindarle un mejor futuro mejor a nuestros hijos que se conviertan en profesionales, tenemos necesidades, además no hay posibilidades de tener un trabajo formal”,** es el mensaje que envía Martínez a la ciudadanía que muchas veces discrimina esta actividad por usar el espacio público para poder sostener a sus familias.
