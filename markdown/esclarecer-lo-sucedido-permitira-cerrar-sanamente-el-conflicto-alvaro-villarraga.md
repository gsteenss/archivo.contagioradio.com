Title: "Esclarecer lo sucedido permitirá cerrar sanamente el conflicto": Álvaro Villarraga
Date: 2016-08-25 13:32
Category: Nacional, Paz
Tags: Conversaciones de paz con las FARC, Diálogos de paz en Colombia, memoria, proceso de paz
Slug: esclarecer-lo-sucedido-permitira-cerrar-sanamente-el-conflicto-alvaro-villarraga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Memoria-histórica.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Centro Nacional de Memoria Histórica ] 

###### [25 Ago 2016] 

Después de 52 años podremos decir que sin duda éste es un excelente día para Colombia, no en vano el mundo entero tiene los ojos volcados hacia nuestro país, afirma el analista Álvaro Villarraga quien agrega que los acuerdos bien aplicados generaran las condiciones para finalizar la guerra y para lograr las **reformas políticas que favorecerán con la garantía de derechos a diversos sectores sociales**, quienes deberán asumir el reto que implica la superación de conflicto a través del trabajo en torno a la memoria histórica.

De acuerdo con Villarraga, el primer gran reto que se deberá asumir de cara al posconflicto es el de la conformación de una [[Comisión de Verdad y Esclarecimiento](https://archivo.contagioradio.com/comision-de-la-verdad-en-colombia-entre-lo-imaginable-y-lo-posible/)] que en tres años entregará un informe final para el que se pondrán en marcha audiencias públicas en diversas regiones, con el fin de nutrirse de todas las interpretaciones de las víctimas y de la sociedad civil en torno al conflicto armado, para **lograr legitimidad y consenso social en la narración de lo sucedido**.

Esta Comisión también permitirá que los directos responsables de los principales hechos cometidos en el marco del conflicto armado reconozcan su responsabilidad, y justamente ese es el segundo reto que la población deberá superar, lograr que la memoria histórica se constituya en una herramienta para la movilización ciudadana que permita conocer lo que sucedió en términos de la crisis en derechos humanos, como un patrimonio de los pueblos para **cerrar el conflicto sanamente y llegar a una verdadera reconciliación**.

Para el analista el país está ante la oportunidad de retomar las iniciativas legales y políticas públicas que quedaron pendientes y que se acordaron en anteriores procesos de paz, pero que no se han cumplido o se han cumplido parcialmente, como ocurrió con los acuerdos con las guerrillas del EPL, el M-19 o el Quintín Lame, algunos de los cuales aportaron a las transformaciones que favorecieron a los pueblos originarios tras la Constitución de 1991; sin embargo, se espera que este proceso de paz con la guerrilla de las FARC-EP permita el cumplimiento de todo lo que se ha pactado porque **"la sociedad no debe soportar que se cumplan pactos de paz a medias"**.

El investigador afirma que si bien la paz no es el silencio de los fusiles, para llegar a ella es obligatorio que cesen las hostilidades y se supere el levantamiento armado, hechos que son decisivos pero no suficientes, como lo prueba el hecho de que [[cinco de los seis puntos que fueron acordados](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] entre la guerrilla de las FARC-EP y el Gobierno, estén **dedicados a la solución de otros conflictos sociales**, lo que confirma que la sociedad civil tiene el papel protagónico en la construcción de paz.

<iframe src="http://co.ivoox.com/es/player_ej_12664457_2_1.html?data=kpejmJmYeZihhpywj5aaaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncaLg18bf0ZC6rc3gwtffw8zFaZO3jMbbw9HNt9XVjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
