Title: Continúan las amenazas a líderes en el Cauca
Date: 2017-02-07 17:51
Category: DDHH, Nacional
Tags: ACIN, Cauca, Cerro Tijeras, Juan Manuel Camayo Díaz, Leonardo Cano
Slug: continuan-las-amenazas-a-lideres-en-el-cauca
Status: published

###### [Foto: Pulzo] 

###### [7 de Feb 2017] 

A través de un comunicado las autoridades tradicionales del Norte del Cauca, denunciaron el asesinato del comunero Leonardo Cano habitante de Cerro Tijeras en Suárez y el intento de homicidio contra Juan Manuel Camayo Díaz, jurídico de Las Delicias y concejal en ejercicio de Buenos Aires.

La Asociación de Cabildos Indígenas del Norte del Cauca –ACIN–, aseguró que Camayo fue abordado por dos hombres en motocicleta, quienes le disparan pero no logran impactarle, **lo que le permitió “huir y esconderse en la vegetación”.**

La Asociación resalta que desde diciembre del año pasado “circulan panfletos de las Águilas Negras” que amenazan a líderes sociales, ambientalistas y organizaciones sindicales en la región del Cauca.

Respecto al caso del comunero Leonardo Cano, hasta el momento no hay un dictamen de las entidades investigativas, sin embargo Jairo Camayo uno de los consejeros de la ACIN delegados para el Cabildo de Cerro Tijeras, reveló que el pasado 5 de febrero, hombres desconocidos asesinaron a Leonardo en su propia casa.

El Consejero señaló que aún **no cuentan con la descripción ni el paradero de los responsables, pero que la Fiscalía ya adelanta las respectivas indagatorias** a habitantes de la zona.

Las comunidades de Las Delicias y Cerro Tijeras, han manifestado su rechazo total a estos actos de violencia “en medio de un proceso de paz” y exigen al Gobierno una **“pronta y efectiva implementación de los acuerdos”** y de los mecanismos de seguridad en los territorios.

[DENUNCIA Juan Manuel Camayo - Jurídico Vereda Las Delicias](https://www.scribd.com/document/338709718/DENUNCIA-Juan-Manuel-Camayo-Juridico-Vereda-Las-Delicias#from_embed "View DENUNCIA Juan Manuel Camayo - Jurídico Vereda Las Delicias on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_96060" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338709718/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ogKCkIFhSJBnSwn16Tzf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
