Title: Comunidades indígenas de Boyacá exigen respeto por su territorio
Date: 2015-10-27 16:30
Category: DDHH, Nacional
Tags: 11 militares muertos, Boyacá, elecciones regionales Colombia, ELN, emboscada del eLN, Güicán, indígenas del resguardo Bachira
Slug: 360-indigenas-se-verian-afectados-por-presunto-ataque-del-eln-en-guican-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ejercito_colombiano_getty_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  [www.pulzo.com] 

<iframe src="http://www.ivoox.com/player_ek_9183789_2_1.html?data=mpallZycfY6ZmKiak5eJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRd5ekjM7bxoqnd4a1pczS0MbXb9TZjNvS1Iqnd4a1pcbbjcbKqcTowsnc1ZDUs9Of0dfS1drSuNCfwtnO09rJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Vladimir Moreno Torres, ASO UWA] 

###### [27 Oct 2015 ] 

[La emboscada perpetrada en la mañana de ayer por actores armados, presumiblemente del ELN, en Cubará jurisdicción del municipio de Güicán, Boyacá, que dejó 12 militares muertos y la militarización del territorio ha **afectado cerca de 360 indígenas del resguardo Bachira** con los que la Asociación de Autoridades Tradicionales y Cabildos U’wa no se han podido comunicar para verificar la situación.]

[Vladimir Moreno Torres, presidente ASO UWA, asegura que han pedido al Ejército que les ayude a movilizar una comisión de verificación a Cubará y ante la no respuesta, han enviado en la mañana de hoy una **delegación de la Guardia Indígena** que se espera llegue mañana al resguardo **para poder verificar lo sucedido y la información que se afirma en **medios.]

[Las comunidades indígenas asumen este tipo de actos violentos como una violación a sus derechos fundamentales, y así mismo **rechazan** las medidas que estiman las autoridades de la **militarización** del territorio, puesto que las comunidades estarían en permanente peligro.]
