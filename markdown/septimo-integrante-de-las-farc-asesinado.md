Title: Dos integrantes de las FARC asesinados en menos de 24 horas
Date: 2017-08-14 12:30
Category: Nacional, Paz
Tags: Amnistia e Indulto, Asesinato de indultados, FARC
Slug: septimo-integrante-de-las-farc-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/farc-campamento-guerrilla-afp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [14 Ago. 2017] 

Este 13 de agosto en horas de la noche **fue asesinado Jesús Adán Mazo, conocido como “Andrés Molina”, integrante de las FARC** y que había sido beneficiado con la ley de amnistía e indulto. Los hechos ocurrieron muy cerca de la Zona Veredal Transitoria de Normalización Román Ruiz en Ituango, Antioquia.

Según vecinos de la zona, los hombres fuertemente armados, ingresaron al lugar donde se encontraba Andrés, de donde lo sacaron por la fuerza y le dispararon en tres ocasiones. Mazo ya había conocido de los planes para asesinarlo por lo que cambió su lugar de residencia y estaba acompañado de dos milicianos más en el lugar hasta el que llegaron los armados. Le puede interesar: [Denuncian asesinato de integrante de las FARC en Ituango](https://archivo.contagioradio.com/denuncian-asesinato-de-exguerrillero-de-las-farc-en-ituango/)

Este hecho se suma a la grave crisis por la que está pasando el municipio, en donde **se ha denunciado el control paramilitar, la desaparición y el asesinato** de integrantes de la comunidad y de esa guerrilla. Le puede interesar: [Guerrilleros indultados a la espera de más liberaciones](https://archivo.contagioradio.com/guerrilleros-indultados-a-la-espera-de-mas-liberaciones/)

**En el Tarra fue asesinado Norbey Téllez**

De igual modo, a través de una denuncia se manifiesta que este domingo fue asesinado Norbey Téllez conocido como "Duván Ríos" que pertenecía a la Zona Veredal Transitoria de Normalización de la Vereda el Tarra en el departamento de Norte de Santander.

"Hombres armados entraron a su casa y sin mediar palabras lo asesinaron. Lo más aberrante es que al trasladar el cuerpo del miliciano al hospital que queda cerca al puesto de Policía entran tres tipos encapuchados y con pistola en mano averiguan si Duvan Ríos continuaba con vida para rematarlo" dice la comunicación

### **Son 8 integrantes de las FARC asesinados desde el inicio de la implementación del acuerdo** 

Con este asesinato, **se completan 8 integrantes de las FARC asesinados** luego de la firma de los Acuerdos de Paz, así como de las amnistías otorgadas. Le puede interesar: [Atentan contra dos integrantes de las FARC en Cauca](https://archivo.contagioradio.com/44695/)

En un comunicado entregado por las FARC el 4 de agosto luego de un atentado en contra de dos integrantes de esa guerrilla que realizaban pedagogía para la paz, **instaron a garantizar las medidas para proteger la vida de las comunidades y de las personas que integran esa agrupación guerrillera** “conforme lo indican los protocolos de los acuerdos de paz”.

> Condenamos el asesinato de Jesús Adán Mazo a metros de ZVTN de Ituango. Hace dos dias salieron contenedores con armas, hoy el primer muerto. [pic.twitter.com/qYc6Mt0mlO](https://t.co/qYc6Mt0mlO)
>
> — Pastor Alape (@AlapePastorFARC) [14 de agosto de 2017](https://twitter.com/AlapePastorFARC/status/897121585852801025)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
