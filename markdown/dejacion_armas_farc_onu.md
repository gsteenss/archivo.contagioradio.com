Title: ONU extrajo últimos contenedores de armas de las FARC
Date: 2017-08-15 04:50
Category: Nacional, Paz
Tags: Dejación de armas, FARC, ONU
Slug: dejacion_armas_farc_onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dejacion-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  NC noticias 

###### [15 Ago 2017] 

[Este martes finalizó la dejación de los fusiles de las FARC con la extracción de los últimos contenedores con las armas de la guerrilla que tuvieron en poder más de 50 años. A su vez se da por finalizada la etapa de las 26 zonas veredales. Con ello se afianzan los pasos hacia la construcción de la paz en Colombia, y es así como este 15 de agosto se constituye el día "del último suspiro del conflicto", como lo expresó el presidente Juan Manuel Santos.]

[El acto se llevó a cabo en el **municipio de Fonseca, en La Guajira, donde operaba el Frente 59 de las FARC,  y ahora era la zona veredal de Pondores.** Allí estuvieron presentes los representantes de las Naciones Unidas, el jefe de Estado colombiano y los integrantes del Secretariado de las FARC.  Con este acto, se continúan evidenciando las acciones hacia la paz. Esta, **de la dejación de las armas, ha evitado la muerte de más de 2.500 personas.**]

Jean Arnult, jefe de la Misión Verificadora de la ONU, anunció que hasta el momento se han registrado un total de "**8112 armas, casi un millón trescientos mil cartuchos incinerados** y también se concluye la destrucción de todo el material inestable, incluyendo minas antipersonales, granadas, explosivos caseros, pólvora". Respecto a las caletas, 510 han sido ejecutadas de manera exitosa y como resultado se recolectó 795 armas, 293,803 municiones de diferentes calibres de armas ligeras y 22,077 kilogramos de explosivos diversos, entre otros.

Simultáneamente, mientras se llevaba a cabo el acto aplaudido por el mundo entero, en las demás zonas veredales también se empezó el traslado de las últimas armas recolectadas en los demás puntos del territorio nacional, que luego serán transformadas en monumentos en representación de la paz.

### Finaliza un ciclo y empieza otro 

[Se trata de un proceso que inició el pasado 23 de junio de 2016, fecha en la que las partes que negociaron durante 5 años en La Habana, acordaron el cese bilateral del fuego. Con la finalización de dicho ciclo, hasta hoy, funcionará el Mecanismo Tripartito de Monitoreo y Verificación, además, la función de la ONU ahora será vigilar la transición a la vida civil de los excombatientes y ser veedores de la seguridad en los puntos donde se desarrolló el conflicto armado.]

[Por su parte, la fuerza pública también jugará un nuevo papel. Los militares ya no combatirán a la insurgencia, sino que deberán garantizar la vida y la seguridad de los exguerrilleros. Una situación que sigue en la cuerda floja, ya que en los últimos días **han matado a dos excombatientes, y además otros tres líderes sociales han sido asesinados**, el último fue Fernando Asprilla, integrante de Marcha Patriótica en la Baja Bota Caucana.]

Ante tal panorama, desde las organizaciones sociales se ha pedido al gobierno nacional que cumpla con los compromisos y se avance en las garantías jurídicas, económicas y de seguridad  en la reincorporación a la sociedad civil de los integrantes de las FARC-EP, y de igual forma se brinden garantías para los líderes y lideresas sociales, teniendo en cuenta la permanencia y el accionar de los grupos paramilitares en diferentes puntos del país.

### Un reto para todo el país 

Para Camilo González, director de INDEPAZ, es un hecho político y el símbolo de la terminación de un ciclo histórico, que debe implicar un adiós a las armas en **"todo lo que ha sido la vida política  y los negocios de Colombia",** incluyendo el paramilitarismo.

González indica que ahora el gran reto para el país "**es el cambio de la mentalidad contrainsurgente", que implica un gran propósito nacional y con ello, "superar la conciencia perversa de la beligerancia y la consideración del otro como enemigo".** Es necesario entonces cambiar los discursos del odio por los de la reconciliación, que incluso, están siendo reconocidos desde el poder, pues "hay todo un replanteamiento institucional, que considera necesario cerrar el capítulo de la violencia", explica el director de INDEPAZ.

<iframe id="audio_20355533" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20355533_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
