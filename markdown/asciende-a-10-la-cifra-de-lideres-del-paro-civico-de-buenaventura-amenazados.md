Title: Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados
Date: 2019-06-13 18:39
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amenazas contra líderes sociales, buenaventura, Comité paro cívico Buenaventura
Slug: asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité Paró Cívico Buenaventura] 

El pasado 11 de junio, el Comité del Paro Cívico de Buenaventura rechazó y denunció las amenazas recibidas por **Yency Murillo Sarria y Mary Cruz Rentería Mina, lideresas sociales que integran la submesa de Mujeres del Paro Cívico** y quienes durante más de diez años han defendido los derechos de las mujeres y los niños del Distrito portuario.

Ante estas amedrantaciones, tal como indicó **Danelly Estupiñán, integrante de la veeduría y protección de derechos humanos del Comité del Paro Cívico de Buenaventura,** se convocó a una rueda de prensa para hacer públicas las amenazas no solo contra Yency y  Mary sino contra un total de 10 líderes del territorio que han venido sufriendo situaciones que ponen en riesgo su integridad física y emocional.

De igual forma se fortalecieron las medidas de autoprotección y fue establecida comunicación con la UNP porque algunos de los líderes y lideresas amenazados no cuentan con esquemas de seguridad, "algunos porque no habían tenido antecedentes de amenazas y otros porque fueron diagnosticados con medidas blandas de protección", explicó Danelly.

### En  Buenaventura se dice "dime qué denuncias y te diré por qué te amenazan" 

Ante los posibles motivos por los que habrían sido amenazadas,  Danelly hace alusión a la lucha que ha emprendido por la constitución de la Secretaria de la Mujer de Buenaventura, la que tal como denuncian las lideresas, quedaría sin recursos asignados para poder funcionar,  **"Lo único que las compañeras Yency y Mary Cruz han hecho es defender los derechos de las mujeres"**.

Danelly afirma que este podría ser el motivo de las amenazas pero que "por el patrón que se ha vivido históricamente, creemos que es importante que la Fiscalía tenga en cuenta esta hipótesis por los debates que han venido dando frente a este tema", puntualizó.  [(Le puede interesar: Si el Gobierno no cumple retomaremos el paro cívico: habitantes de Buenaventura)](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/)

<iframe id="audio_37113443" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37113443_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
