Title: Autoridades de Antioquia descalificaron llamado de atención de Europa sobre Hidroituango
Date: 2018-04-23 13:10
Category: DDHH, Nacional
Tags: Antioquia, Europarlamentarios, Hidroelectrica, Hidroituango, Parlamento Europeo
Slug: autoridades-de-antioquia-descalificaron-llamado-de-atencion-de-europa-sobre-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/hidroituango-e1516739322374.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Abr 2018] 

Integrantes del Parlamento Europeo enviaron una carta a las autoridades colombianas para expresar su preocupación por la llenada de la presa del proyecto hidroeléctrico Hidroituango en la medida en que esto “significaría la posibilidad de que **cientos de familiares de víctimas de desaparición forzada** en Colombia pierdan la oportunidad de hallar a sus seres queridos”. Por su parte, el alcalde de Medellín, **Federico Gutiérrez,** confirmó que el llenado de la presa se realizará el primero de Julio.

En la misiva, los eurodiputados afirmaron que son miles los campesinos los que dependen del río Cauca para **sus formas de vida** y que han sido muchas las familias quienes han sido despojadas de forma violenta para la realización del proyecto.

### **Llamado desde Europa evidencia intereses económicos y políticos en Hidroituango** 

Para Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, el llamado de atención que viene desde Europa “es muy importante y **ya está teniendo sus alcances**”. Afirmó que se ha mostrado que “hay intereses políticos y económicos por ocultar la verdad de lo que ha sucedido en un territorio afligido por el conflicto armado”. (Le puede interesar:["Continúa la violencia y los desalojos contra comunidades afectadas por Hidroituango en Antioquia"](https://archivo.contagioradio.com/continua-la-violencia-y-los-desalojos-contra-comunidades-afectadas-por-hidroituango-en-antioquia/))

Enfatizó en que las respuestas de las autoridades de Antioquia y Medellín, quienes aseguraron que la carta **no proviene del Parlamento Europeo enteramente** y que hay intereses de algunas organizaciones para detener la hidroeléctrica, dan cuenta de “actitudes arbitrarias donde se desconocen los derechos de las víctimas”.

Además, el alcalde de Medellín fue enfático en descalificar la carta argumentando que los diputados que firmaron están relacionados con **partidos de izquierda en Europa**. Ante esto, Zuleta manifestó que estas declaraciones sólo intentan quitarle valor al llamado de atención y que deben ser tenidas en cuenta.

Dijo que el Gobernador de Antioquia le ha estado **mintiendo al país** cuando en repetidas ocasiones ha manifestado que los cuerpos que se han encontrado en las zonas de influencia de Hidroituango, no fueron hallado en la rivera del río Cauca, por lo que las actitudes ante la carta reflejan “el despotismo que se vive en Antioquia”.

### **"Es importante que se sepa las arbitrariedades” de las autoridades en Antioquia** 

Si bien Zuleta reconoce que el llenado de la presa no se va a detener, asegura que es “importante que el país conozca la arbitrariedad de las actuaciones de **Empresas Públicas de Medellín**, el desconocimiento de los derechos de las víctimas y la arbitrariedad de las instituciones del Estado”. Aseguró que lo que está el Parlamento social es un llamado al diálogo social para evitar la re victimización y “más daños de los que ya han ocurrido”. (Le puede interesar: ["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

Finalmente, los eurodiputados pidieron que se garanticen los derechos de las personas que están buscando a los **desaparecidos en el río Cauca** y de las comunidades y campesinos que dependen del trabajo en el río. Aseguraron que es necesario que se detengan los desalojos violentos por parte del ESMAD y que EPM garantice la reparación integral de todos los afectados.

[Llamado URGENTE de Europarlamentarios sobre situación Hidroituango Colombia (1)](https://www.scribd.com/document/377159555/Llamado-URGENTE-de-Europarlamentarios-sobre-situacio-n-Hidroituango-Colombia-1#from_embed "View Llamado URGENTE de Europarlamentarios sobre situación Hidroituango Colombia (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_10392" class="scribd_iframe_embed" title="Llamado URGENTE de Europarlamentarios sobre situación Hidroituango Colombia (1)" src="https://www.scribd.com/embeds/377159555/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-VGo7HJnYzP1uQMxbetox&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_25575603" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575603_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
