Title: Ambientalistas exigen al Banco Mundial no financiar proyecto en Santurbán
Date: 2016-10-27 16:46
Category: Ambiente, Nacional
Slug: ambientalistas-exigen-al-banco-mundial-no-financiar-proyecto-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/santurban-e1477604527271.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vetas-Santander.org] 

###### [27 Oct 2016] 

Organizaciones de ambientalistas estuvieron en la sede del Grupo del Banco Mundial este 27 de Octubre, exigiendo que la Corporación Financiera Internacional, **retire su inversión del proyecto de extracción minera Angostura, propuesto para el páramo de Santurbán,** ecosistema de alta montaña que abastece de agua a más de dos millones de personas en Colombia.

La Oficina del Asesor en Cumplimiento *Ombudsman*, decidió entrevistar a funcionarios en Bucaramanga y Bogotá, para ampliar el panorama y emitir un informe de 55 páginas luego de la** **denuncia presentada en 2012** por el Comité por la Defensa del Agua y el Páramo de Santurbán y otras organizaciones ambientalistas** ante esa entidad. Le puede interesar: [Minería en páramo de Santurbán no contó con estudios sobre impactos socio-ambientales.](https://archivo.contagioradio.com/mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales/)

En dicho informe, la entidad señala que la Corporación Financiera Internacional, brazo del Banco Mundial que financia al sector privado, **invirtió en la empresa canadiense Eco Oro Minerals omitiendo aspectos claves que debería contemplar cualquier proyecto.** Por ejemplo omitió una juiciosa evaluación de impactos ambientales y una consulta previa con las comunidades del territorio.

La licencia ambiental para la mina Angostura, que pretende extraer de Santurbán 12,6 millones de onzas de oro, fue negada por la ANLA en 2011, y a pesar de ello, **actualmente Eco Oro Minerals prepara una demanda internacional contra el Estado colombiano.** Le puede interesar: [Empresa canadiense demandaría a Colombia por fallo que protege páramo de](https://archivo.contagioradio.com/empresa-canadiense-demandaria-a-colombia-por-fallo-que-protege-paramo-de-santurban/)[Santurbán.]

La empresa argumenta, que la reciente **sentencia de la Corte Constitucional que prohíbe la minería en páramos** y el recorte del título minero que realizó la Agencia Nacional de Minería basada en **la delimitación de Santurbán**, han hecho que, 23 años después del comienzo de las labores de exploración, el proyecto Angostura les siga generando pérdidas económicas.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
