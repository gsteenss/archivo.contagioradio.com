Title: 56 niños palestinos fueron asesinados en 2018 por el ejército Israelí
Date: 2019-02-21 14:29
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: Israel, niños, Palestina
Slug: ninos-palestinos-asesinados-israel-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Hazem Bader (Getty Images) 

###### 21 Feb 2019 

La organización Defense for Children International- Palestine, presentó un informe donde revela que durante el año 2018 **56 niños y adolescentes fueron asesinados por cuenta de la ocupación Israelí en Gaza**.

La organización documentó además **120 casos de menores detenidos por fuerzas israelíes en Cisjordania**, acciones que desde el gobierno se ha pretendido justificar asegurando que los menores utilizan piedras contra las autoridades cuando ocurren protestas.

El mismo grupo de Derechos Humanos había informado ya el año pasado que desde el año 2000**, cerca de ocho mil menores palestinos fueron detenidos y juzgados en tribunales militares israelíes**.

Otras organizaciones calculan que desde 1967, el Estado de Israel detuvo a un total de 750 mil palestinos, entre ellos 23 mil mujeres y **25 mil niños**, el equivalente al 20 por ciento de la población palestina total de los territorios ocupados.

#### **Los menores son blanco indiscriminado ** 

Este jueves 21, medios internacionales dieron cuenta de las afectaciones que provocaron **gases lacrimógenos lanzados contra la escuela Al- Khalil, en Hebron,** causando que al menos 30 estudiantes resultaran afectados por inhalación. Médicos locales informaron a la agencia palestina Wafa que uno de los menores se encuentra en estado crítico.

Las agencias dan cuenta de un **incremento de las acciones bélicas desde que el régimen de Tel Aviv expulsó a un grupo internacional de observadores** encargado de salvaguardar a los palestinos en Khalil.

#### **Van 7 niños muertos en lo corrido de 2019** 

La Organización de la Naciones Unidas (ONU) asegura que **el ejército israelí ejecutó a más de 295 palestinos, e hirió a más de 6.000 con armas de fuego** desde el inicio de las protestas por la Gran Marcha del Retorno desde el 30 de marzo de 2018, como oposición al bloque económico de Israel

La ONU asegura que **40 menores han fallecido por disparos de arma de fuego de soldados israelíes** en Gaza durante ese periodo de tiempo, y que sólo en lo corrido del presente mes de febrero ya **son tres los niños muertos en Gaza y dos más en Cisjordania**. En cuanto a las detenciones, estiman que el **75 por ciento de los jóvenes privados de libertad experimentó violencia física después del arresto**.

El coordinador de Naciones Unidas para el proceso de paz al Consejo de Seguridad, Nicolay Mladenov, aseguro que **mientras la situación humanitaria se deteriora, la ONU cada vez tiene menos fondos para responder con ayudas para los refugiados palestinos**, agregando que el llamamiento humanitario para 2018 sólo consiguió un 46 por ciento de los fondos solicitados.

En razón de esos recortes, el Programa Mundial de Alimentos se vio en necesidad de suspender la entrega de comida a 27 mil personas y reducir las raciones para otros 166 mil.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
