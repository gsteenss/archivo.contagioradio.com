Title: "Hammerdeath" thrash metal en "A la calle"
Date: 2015-06-29 13:05
Category: Sonidos Urbanos
Tags: Hammerdeath banda de Thrash metal
Slug: hammerdeath-thrash-metal-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10257272_632141106873158_9202451957937986297_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_4693432_2_1.html?data=lZumlZmXdo6ZmKiakpaJd6Kkkoqgo5mZcYarpJKfj4qbh46kjoqkpZKUcYarpJKSlJespc7hxtfRx8bYrIamk5DhytfFt8mfzsrhw9GPqc%2Bfhpefo5DQpYzXwtHZx4qWdo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Hammerdeath, thrash metal en "A la Calle"] 

[![unnamed (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/unnamed-1-e1435353152643.jpg){.aligncenter .size-full .wp-image-10562 width="1092" height="701"}](https://archivo.contagioradio.com/hammerdeath-thrash-metal-en-a-la-calle/unnamed-1-3/)

##### [Hamerdeath con Leonardo y Mateo de "A la Calle"] 

"**Hammerdeath**" es una banda de Thrash metal bogotano; las diferentes influencias musicales de cada uno de sus integrantes permiten una exploración por sonidos de naturaleza variada, evitando encasillar su música bajo un sub-género específico. Desde el poder del Metal, y los “guturales hechos con pasión”, envían un mensaje de libre interpretación para los espectadores de acuerdo con su percepción y experiencia de vida. "**Hammerdeath**" presenta en "A la Calle" el videoclip de su canción “**Green Babylon**” que hace parte del nuevo álbum.
