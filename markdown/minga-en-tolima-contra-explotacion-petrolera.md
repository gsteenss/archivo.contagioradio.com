Title: Minga en Tolima contra explotación Petrolera
Date: 2018-07-12 17:23
Category: DDHH, Nacional, Uncategorized
Tags: ESMAD, Expltoación Petrolera, Minga, Tolima
Slug: minga-en-tolima-contra-explotacion-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Hocol.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asuntos Legales] 

###### [12 Jul 2018] 

Desde el 8 de julio la comunidad indígena en Ortega, Tolima se encuentra en una minga para evitar la reapertura de los pozos 1, 4, 5 y 11 del campo Toldado, por parte de la empresa HOCOL S.A.; en razón de que la explotación petrolera, según denuncia la comunidad, ha generado problemas de salud y afectaciones al ambiente.

De acuerdo con una de las voceras de la minga en Ortega, la minga pretende evitar el ingreso de maquinaria para que se reactiven los pozos petroleros, porque **en la zona  se han visto afectadas las fuentes hídricas por vertimiento de material industrial,** han convivido con contaminación auditiva y han sufrido de problemas respiratorios pues la empresa se encuentra ubicada en medio de un caserío.

La vocera de las comunidades, señaló que la empresa no socializó con las comunidades la reapertura de los posos, razón por la cual, **no tienen conocimiento sobre los estudios ambientales y de riesgo de dicha explotación.** Por ello, piden que se entable un canal de comunicación que permita establecer un diálogo, y que se evalúen los riesgos de la reactivación de los pozos.

Según lo refiere la integrante de la Minga, hacia el medio día de este jueves la minga buscó un acercamiento con la empresa que inicialmente se mostró dispuesta al diálgoo, sin embargo el encuentro no fue posible por la intervención de la inspectora de policía de Ortega. **Se prevé que en las primeras horas del viernes el ESMAD haga presencia en la zona.**

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

######  
