Title: Polaricemos
Date: 2019-07-08 16:50
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Paramilitarismo, polarización, uribismo
Slug: polaricemos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/polarizacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**¿Cómo no polarizar entre la vida o la muerte?**

Rimbombante intelectual que no tiene nada que perder más que puntos en el cvlac viene a argumentar que no polaricemos, cuando lo que está en juego es la vida y la muerte para aquellos que no tienen tiempo de citar o de recibir un aplauso en ese foro que brilla por su dorada conciencia.

**¿Cómo no polarizar entre la vida o la muerte?**

Excéntrico pacifista que solo se dedica a atacar a los que emplean la violencia para defenderse, pero que no estaría dispuesto a nunca responderle al Esmad durante sus tomas en la madrugada, o a los abusos intensos de las autoridades que mortifican por estos días a los colombianos. ¡Chocante pacifista que no lucha pacíficamente! que no moriría nunca como el Dr. King, como Gandhi, como Abad, como Pardo Leal, como María del Pilar porque entonces ¿quién criticaría a los violentos?

**¿Cómo no polarizar entre la vida o la muerte?**

Si los líderes de la privada opinión pública aún no saben que el limbo no existe para la gente que está muriendo en el campo colombiano.  La equivocación y el delirio de quienes piden no polarizar, es tan grande como las ganas de estabilidad y confianza que necesita este régimen de la barbarie.

**¿Cómo no polarizar si la esperanza necesita un espacio para aterrizar luego de sus longevos vuelos generacionales?**

Polarizar sí, pero aceptar la tranquilidad y la paz de los opresores o la categorización de las tragedias como hacen los medios corporativos y la supuesta comunidad internacional ¡¡jamás!! Es así de difícil, porque en los laureles de esas victorias que consiguieron con mentiras y muerte, no puede reposar nuestra esperanza; su piel delicada se dañaría y luego los opresores la convertirían en un comodín que usarían a conveniencia.

Valientes monstruos de papel, los líderes de este régimen colombiano, que sacan los dientes cuando golpean por la espalda a sus contradictores, pero que se asustan como si estuvieran en un cementerio encantado por miles de almas a la media noche, cuando los miran a los ojos dispuestos a enfrentarlos.

¿Cómo no polarizar entre la vida y la muerte, si la muerte en ningún momento es natural, sino que forma parte de una matanza cruel que no le importa lo suficiente a la comunidad internacional y no moviliza a los concienzudos artistas de Miami a cantar para que el régimen uribista de Duque sea sancionado? ¿dónde está la pataleta de Miguel Bosé? ¿dónde están los mordaces comentarios de Vargas Llosa, viejo liberal lacayo de coronas? ¿dónde están todos los artistas militantes anti izquierda que no se mueven por las vidas humanas? ¿ahora sí son a-políticos? … bastardos del arte…

¿Cómo no polarizar entre la vida y la muerte si lo que buscan algunos es que hagamos buena cara ante el exterminio? quieren que cantemos al son de los youtubers en vez de promover esa fea rabia que agota los cuerpos y arruga la cara, quieren que no polaricemos porque supuestamente eso no ayuda ¿no ayuda a quién? ¿a la paz de régimen? ¿a la tranquilidad de los opresores que saben que con frasecitas clichés y mierdas bien citadas pasarán por alto la potencia que aguarda un pueblo indignado?

**Polaricemos, para ver de qué tamaño es esta democracia totalitaria. Una democracia que acepta al partido del odio y al partido de la paz, que acepta a gente amenazando con picar a otra en plena calle, que acepta al partido de los asesinos y que acepta al partido de los honestos… ¿de qué hablan? Esa no es ninguna democracia ¡es un espejismo! y para salir de él hay que polarizar, para saber la distancia tan grande que hay entre un espejismo y un pozo de agua fresca. **

Polaricemos, pero no entre estupideces que nos ponen en una encuesta virtual; polaricemos, pero no para elegir entre el partido del odio y el partido del odio disfrazado de ese verde liberal; polaricemos, pero no para bifurcar una tragedia como el exterminio de personas por causas políticas, en si eran líderes o lideresas cuando aquí están acabando con todo. Polaricemos, pero no para decidir por qué corrupto es “menos peor” votar; polaricemos, pero no para no para celebrar que se ha vencido a los polarizadores; Polaricemos, pero no para concluir que el mundo binario causa más daño que la paz de los opresores.

Polaricemos, porque aquí en Colombia no se está discutiendo si es blanco o negro, o si es más chévere o menos chévere la entrada de esos cínicos youtubers que luego de aparecer como grandes críticos de los poderosos, se sientan almorzar en el Nogal, en el Country, en los Lagartos, en el Jockey, en Serrezuela o en el Gun ¡¡¡con de ellos!!!

Polaricemos porque lo que se está viviendo en Colombia es una masacre de personas por razones políticas que importa menos que muchas cosas superfluas. Es una barbarie que no tiene puntos intermedios. O se acaba o continúa… y ya estamos viendo tristemente cuál es la respuesta.

Polaricemos para hacernos a un lado, así sea moralmente, de este crudo sistema que lo siguen dominando con la mentira, el robo, la corrupción, el asesinato, la infamia, la impunidad (solo para los pobres), la hipocresía y aun así exige que le llamen democracia, y aun así, exige que la institucionalidad sea respetada, ¿de dónde respeto a este régimen que gobierna Colombia? … ¿de dónde?

En las calles de una Bogotá callada pero adolorida, escuché este eco, que dejo aquí, para viva o muera, pues nunca, nunca hay un punto intermedio entre la vida o la muerte:

*Bogotá, mediados del año 19*

*No puede ser… la indiferencia y las notas positivas danzando y riendo en medio del exterminio de personas. Qué puedo decir, así de dialéctica es la existencia…. Y tú y yo, ahí, observándolo todo. Es una pesadilla de la que no podemos escapar… es la frontera entre la cordura y la melancolía, los muertos ya no importan, ya ni siquiera eso, ya ni siquiera quejarse, porque si te quejas, entonces no eres positivo, y eso no sirve… y si te quejas, entonces hay que hacerlo como el sistema pide, danzando y riendo en medio del exterminio de personas... Qué puedo decir, así de dialéctica es la existencia…. Y tú y yo, ahí, observándolo todo…es una pesadilla de la que no escaparemos hasta no hacerle frente ¿podemos escapar? … polaricemos; es su régimen que permite muerte, versus nuestras ansias incontenibles de dignidad y vida; aquí cabemos todos los que no dominan, esa conciencia heterogénea de dominados, inestable, difusa, diversa y popular será nuestra equivalencia. *

##### [Leer más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)
