Title: Noticias de las comunidades del Río Naya
Date: 2017-08-16 12:09
Category: Comunidad, Nacional
Tags: comunicadores, Comunicadores CONPAZ, Naya
Slug: noticias-comunitarias-del-rio-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/rio-naya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nidiria Ruíz 

###### 16 Ago 2017 

Lunes 14 de agosto, a las 6:15 am, en el Distrito de Buenaventura fue capturado por unidades de la policía antinarcóticos Francisco Mina Angulo, miembro del Consejo Comunitario del río Naya y motorista de trasporte fluvial público, bajo el presunto delito de tráfico de estupefacientes.

Domingo 13 de agosto, a las 6:14. Pm, en el Barrio Las Américas, comuna 10 del Distrito de Buenaventura, mediante una llamada del celular No 50936820586 fue amenazada la lideresa del Consejo comunitario del Río Naya, Amparo Zamora por un hombre que le advierte “ No te has dado cuenta que estoy mandando a matar a todos los hijueputas que me la hicieron, faltas vos”. Amparo es vicepresidenta de la organización Huellas del pacífico y lideresa de la organización marcha patriótica. La policía del cuadrante al conocer el caso manifestó que el número del celular del que realizaron la llamada amenazante corresponde a un teléfono de la república de Haití. Amparo es beneficiaria de medidas cautelares otorgadas por la Comisión interamericana de Derechos Humanos.

Sábado 12 de agosto, Corregimiento de Puerto Merizalde, miembros del Consejo Comunitario de Río Naya en compañía de la organización internacional Asokatios y nuestra Comisión de Justicia y Paz verificaron como unidades militares del Batallón de infantería de marina número 42 ocupaban viviendas de la población civil, en una clara infracción al Derecho Internacional Humanitario. La junta del consejo comunitario como autoridad étnico territorial exigió su retiro inmediato de las viviendas y el cumplimiento al control perimetral que deben asumir en el marco de las medidas cautelares que fueron otorgadas por la Comisión interamericana de Derechos Humanos de la OEA.  A esta misma hora y en presencia del consejo comunitario y las organizaciones defensoras de los Derechos Humanos, las unidades militares hicieron entrega a su propietario del bote de fibra de nombre Nayla con motor Yamaha 75 que había sido abandonado en cercanías a este lugar luego de que hiciera caso omiso al llamado de control solicitado.

Domingo 6 de agosto, a las 6 pm, en la comunidad de San José, sector de la playa, en el Consejo comunitario del río Naya, sobre el océano pacífico, unidades militares de la infantería de marina que   junto a la fuerza aérea realizaban operativos de persecución a una avioneta ocuparon 4 viviendas de donde saquearon bienes de la población civil como planta eléctrica, televisor, alimentos, estufa, dinero, además de intentar llevarse un bote familiar y retener a un poblador. Según los testimonios esto ocurrió luego de escuchar disparos y el sonido de helicópteros y de una avioneta que fue obligada a descender sobre la playa a orilla del mar, aeronave que luego fue incinerada por los militares. Los pobladores expresan preocupación por que se desconoce qué pasó con los tripulantes y la carga que seguramente transportaba, pues los uniformados no permitieron que la comunidad se acercara.

#### **Nidiria Ruíz Medina/Comunicadora CONPAZ Concejo comunitario Río Naya**
