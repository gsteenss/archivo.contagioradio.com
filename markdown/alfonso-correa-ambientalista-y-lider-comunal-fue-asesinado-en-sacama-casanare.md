Title: Alfonso Correa, ambientalista y líder comunal fue asesinado en Sácama, Casanare
Date: 2019-03-15 17:06
Category: Comunidad, DDHH
Tags: Arauca, asesinato de líderes sociales, Casanare
Slug: alfonso-correa-ambientalista-y-lider-comunal-fue-asesinado-en-sacama-casanare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 15 Mar 2019

El pasado 14 de marzo en horas de la mañana, fue hallado con numerosos impactos de bala, el cuerpo sin vida del líder comunal **Alfonso Correa**  en la vereda L**a Casirva, en Sácama, Casanare** donde se desempañaba como presidente de la Junta de Acción Comunal de la vereda La Cabuya desde hace tres años.

[Desde la tarde del miércoles, el líder comunal había desaparecido dejando su moto en la carretera que conduce a la finca en la que habitaba y solo fue encontrado hasta el día siguiente a un costado del camino, según información preliminar habría sido abordado por  tres hombres armados que le dispararon en más de 20 ocasiones causándole la muerte.]

Alfonso también era integrante de la Asociación Campesina y Ambiental "Manantiales Asocam", la cual trabaja por la defensa de la naturaleza, la soberanía alimentaria y los derechos humanos y que era representada por el líder en la parte **alta de Tame, los municipios de Sácama y La Salina en Casanare y Chita en Boyacá, **

Según la información de la comunidad, aunque el hecho se perpetró hace dos días y no se tiene información de los posibles móviles del asesinato, la Personería municipal de Tame, la Alcaldía de Sácama y agentes de la Fiscalía junto a integrantes de la fuerza pública, se han dirigido al lugar de los hechos para realizar una investigación. [(Le puede interesar En Arauca han sido asesinadas 32 personas en tan solo dos meses)](https://archivo.contagioradio.com/en-arauca-han-sido-asesinadas-32-personas-en-tan-solo-dos-meses/)

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
