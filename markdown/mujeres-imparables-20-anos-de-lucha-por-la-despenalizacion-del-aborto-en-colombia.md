Title: Mujeres imparables, 20 años de lucha por la despenalización del aborto en Colombia
Date: 2018-09-24 16:10
Author: AdminContagio
Category: Movilización, Mujer
Tags: Aborte IVE, Derechos sexuales y reproductivos, Mesa por la salud y la vida de las mujeres, mujeres
Slug: mujeres-imparables-20-anos-de-lucha-por-la-despenalizacion-del-aborto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto1-e1506634623556.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [24 Sept 2018] 

La Mesa por la Vida y la Salud de las Mujeres, en conmemoración de su trabajo en la defensa de los derechos reproductivos y sexuales de las mujeres, lanzarán la iniciativa **"Mujeres imparables, 20 años abriendo camino".** Una apuesta que a partir del arte, relatará los testimonios de mujeres y hombres que han vivido o acompañado experiencias de interrupción voluntario del embarazo (IVE).

Según Juliana Martínez, directora de esta organización, esta iniciativa tiene la finalidad de continuar en la lucha por el respeto y las garantías que se le deben dar a las mujeres que deciden ejercer el derecho sobre sus cuerpos del aborto y su despenalización en las tres causales.

"Los retos aún tienen que ver con la superación de las barreras y la realización de prácticas que contravienen los derechos sexuales y reproductivos de las mujeres. La razón de ser de la Mesa que es la despenalización del aborto sigue todavía vigente" afirmó Martínez.

### **Las barreras al aborto IVE** 

Recientemente, la columnista Piedad Bonnett, dio a conocer en una de sus columnas, uno de los testimonios de una mujer que abortó bajo una de las causales de la IVE, por malformaciones en el feto. En ella, relató cómo existió negligencia y malos tratos por los médicos, al igual que el riesgo al que fue expuesta.

Martínez, manifestó que esta es una de las barreras a las que se enfrentan las mujeres en Colombia a la hora de exigir sus derechos, "se interponen obstáculos que no están establecidos como por ejemplo **la aprobación en juntas médicas, la necesidad de autorizaciones de terceros o dilatar en el tiempo la atención a las mujeres**".

Otra de esas barreras tiene que ver con el desconocimiento del marco legal y de la sentencia C-355 y recordó que el país es legal el aborto cuando las mujeres son víctimas de violación, el feto viene con malformaciones o cuando este en riesgo la vida o salud de la mujer. (Le puede interesar:["A 12 años de la despenalización del aborto, Colombia aún tiene barreras"](https://archivo.contagioradio.com/a-12-anos-de-la-despenalizacion-del-aborto-colombia/))

Finalmente aseguró que la barrera más grande es la que se ha establecido socialmente, en ese sentido señaló que se debe entender que "esta es una experiencia en la vida de las mujeres y que en esa medidas las mujeres **no deben ser juzgadas ni estigmatizadas por acceder a un aborto legal".**

### **"El arte alza su voz por el aborto legal en Colombia"** 

Entre los diversos artistas que se han sumado a esta iniciativa, se encuentran Piedad Bonnett, Ricardo Silva, Gloria Susana Esquivel, Camila Brúges, Juliana Muñoz Toro y Luis Fernanado Afanador, además de ilustradores reconocidos por su trayectoria en el país como Tinta del Río, Mugre Diamante, Niebla Rosa, entre otros.

Las historias se compartirán en el portal www.mujeresimparables.co , en donde se contará un poco más a fondo de los avances en la despenalización del aborto, los antecedentes de esta lucha y la importancia de defender la aplicación de la sentencia.

<iframe id="audio_28824517" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28824517_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
