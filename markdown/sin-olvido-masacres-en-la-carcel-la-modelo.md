Title: Sin Olvido: Masacres en la Cárcel La Modelo
Date: 2019-09-06 11:54
Author: CtgAdm
Category: Expreso Libertad
Tags: carcel la modelo, carceles, cárceles colombia, Cárceles colombianas, masacre, prisioneros políticos, Prisioneros políticos en Colombia
Slug: sin-olvido-masacres-en-la-carcel-la-modelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div dir="auto">

<div dir="ltr" style="text-align: justify;">

Desde el año 1997 colectivos de prisioneros politicos, organizados en la Cárcel la Modelo en las diferentes mesas de diálogo denunciaron múltiples masacres, asesinatos y desapariciones forzadas comentadas en su gran mayoría por estructuras paramilitares en connivencia de la Fuerza Pública.

</div>

</div>

<div dir="ltr">

</div>

<div dir="auto">

<div dir="ltr" style="text-align: justify;">

</div>

<div dir="ltr" style="text-align: justify;">

En esta emisión del Expreso Libertad José Zamora, ex prisionero político que estuvo recluido en la Carcel La Modelo, relató los difíciles momentos que el movimiento carcelario tuvo que afrontar durante el conflicto armado y la impunidad que prevalece sobre los hechos de violencia cometidos al interior de las cárceles. ([le puede interesar:Prisioneros políticos denuncian violación de DD.HH desde directivas de La Picota](https://archivo.contagioradio.com/prisioneros-politicos-denuncian-violacion-de-dd-hh-desde-directivas-picota/))

</div>

<div dir="ltr" style="text-align: justify;">

De igual forma el abogado Uldarico Flores, relató el contexto jurídico de esos años y la importancia de escenarios como la Comisión de la Verdad para dar respuesta a las responsabilidades de estos crímenes.

</div>

</div>

<div dir="ltr" style="text-align: justify;">

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2502420643321116%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
