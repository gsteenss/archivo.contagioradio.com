Title: Cormacarena y Gobernación del Meta señalan riesgos por actividades de Ecopetrol en Guamal
Date: 2017-02-07 15:07
Category: Ambiente, Nacional
Tags: Ecopetrol, Guamal Meta, Petroleras
Slug: cormacarena-gobernacion-del-meta-senalan-riesgos-actividades-ecopetrol-guamal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Guamal_Humadea.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periodismo Especializado] 

###### [7 Feb 2017] 

Derrames de petróleo, escapes de sustancias químicas, alteraciones en la cobertura vegetal y en zonas de recarga hídrica, son algunas de las **amenazas que representan el Bloque petrolero CPO9 y la plataforma Lorito1 de Ecopetrol,** para los habitantes de la cuenca del río Humadea en Guamal Meta.

Contrario a las declaraciones dadas por Ecopetrol durante una rueda de prensa el pasado 31 de enero, el informe de la visita de control y seguimiento al proyecto Bloque CPO9 y la plataforma Lorito1, realizada por la Gerencia Ambiental de la Gobernación del Meta, se logró demostrar que los Estudios de Impacto Ambiental presentados no fueron apropiados y desde el inicio del proyecto **“se ha puesto en riesgo la vida de los usuarios de los acueductos de Castilla La Nueva y La Vereda Humadea”. **

\[caption id="attachment\_35925" align="aligncenter" width="519"\]![Gobernación del Meta - Visita Bloque petrolero CPO9 y plataforma Lorito1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Captura-de-pantalla-2017-02-07-a-las-13.16.55.png){.wp-image-35925 .size-full width="519" height="368"} "Plano de localización infraestructura vital del área de influencia del pozo. Nótese la presencia de dos acueductos en el área de influencia". Gobernación del Meta\[/caption\]

### Violaciones a derechos con proyecto petrolero 

Edgar Humberto Cruz, líder comunitario de Guamal, manifestó que además de este informe,  Cormacarena emitió un concepto en el que se precisa que es urgente “cambiar de ubicación la plataforma, pues **no es compatible su ubicación y actividad industrial, con el uso del agua para el consumo humano de la microcuenca”** lo que significa la vulneración de los derechos fundamentales a la salud, la vida y a un ambiente sano.

En reiteradas ocasiones las comunidades han expuesto que la misma entidad, Cormacarena, determinó que las áreas en donde se hacen las actividades extractivas** “son áreas protegidas, donde solamente está permitido el uso para la conservación, reforestación e investigación”.** (Le puede interesar: [Proyecto petrolero acabaría con el río Humadea en el Meta](https://archivo.contagioradio.com/proyecto-petrolero-acabaria-con-el-rio-humadea-en-el-meta/))

Además, el concepto de Cormacarena es claro en decir que la Gobernación del Meta, “debe tomar las medidas pertinentes para **obligar a ECOPETROL suspender el proceso constructivo de la plataforma de exploración petrolera LORITO 1** y su vía de acceso”.

### Ecopetrol y la Alcaldía 

Cruz manifestó que hasta el momento la empresa no ha hecho públicos los estudios técnicos “referentes al inventario de reservas subterráneas de agua dulce en la zona” y los estudios de impacto ambiental que corresponden a los bloques petroleros CPO-09 y Cubarral, los cuales tienen influencia en Acacias, Guamal, Castilla la Nueva, Cubarral, El Dorado, el Castillo, San Martín y Granada.

El líder comunitario reveló que el alcalde Cristóbal Lozano ha manifestado en varias ocasiones, que ese tipo de proyectos petroleros **“traen desarrollo al municipio” y que “los argumentos de las comunidades son infundados”**, pues afirma que ECOPETROL “tiene sus papeles en regla ante las autoridades ambientales”.

Cruz comenta que en los últimos meses integrantes de la comunidad y líderes defensores del territorio en Guamal, han sido objeto de estigmatización por parte de funcionarios de Ecopetrol y del Ministerio del Interior hostigamientos, llegando en algunas ocasiones a asegurar que "los defensores del territorio están dividiendo a la comunidad".

Por último, habitantes de la vereda Pio XII han denunciado que el 7 de febrero en horas de la mañana, llegó una "cuadrilla de trabajadores de Ecopetrol" y desde entonces **"están preparando el terreno donde estría ubicado el pozo Trogon". **

\[caption id="" align="aligncenter" width="589"\]![Captura de pantalla 2017-02-07 a la(s) 13.17.12](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Captura-de-pantalla-2017-02-07-a-las-13.17.12.png){.alignnone .size-full .wp-image-35926 width="589" height="435"} ![Captura de pantalla 2017-02-07 a la(s) 13.17.47](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Captura-de-pantalla-2017-02-07-a-las-13.17.47.png){.alignnone .size-full .wp-image-35927 width="615" height="334"} ![Captura de pantalla 2017-02-07 a la(s) 13.20.26](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Captura-de-pantalla-2017-02-07-a-las-13.20.26.png){.alignnone .size-full .wp-image-35928 width="601" height="372"}

###### Registro de la Visita de la Gerencia Ambiental del Meta al Bloque CPO9 y Plataforma Lorito 1

 

[CONCEPTO Gerencia Ambiental](https://www.scribd.com/document/338697162/CONCEPTO-Gerencia-Ambiental#from_embed "View CONCEPTO Gerencia Ambiental on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_49393" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338697162/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-iNNEwgn4YUt3plpJQDEt&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

[Concepto Cormacarena Lorito 1](https://www.scribd.com/document/338697161/Concepto-Cormacarena-Lorito-1#from_embed "View Concepto Cormacarena Lorito 1 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_29022" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338697161/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ZHqV23xZB9onbmoYEc4q&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7735849056603774"></iframe>  
\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]
