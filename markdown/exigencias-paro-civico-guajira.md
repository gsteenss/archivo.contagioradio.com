Title: Las cuatro exigencias del paro cívico en La Guajira
Date: 2019-06-27 18:43
Author: CtgAdm
Category: Movilización, Nacional
Tags: cesar, La Guajira, protesta, transporte
Slug: exigencias-paro-civico-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/La-Guajira.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @julioferrer13] 

**Desde el pasado miércoles 26 de junio se decretó la hora cero del paro indefinido en el departamento de La Guajira para exigir su acceso a derechos básicos como el agua potable**. Aunque la Mesa Departamental del Paro Cívico intentó dialogar con el Gobierno Nacional y Departamental, no fueron escuchados, razón por la que tuvieron que acudir al paro para que se responda a sus demandas.

> Por la dignidad del pueblo guajiro, apoyamos el paro cívico departamental indefinido, a esperas de que el gobierno de Duque resuelva las exigencias. Abro hilo:
>
> 1.Por el Derecho a la Educación Superior.  
> 2. Por el Transporte y alimentación escolar.
>
> — Martha Peralta Epieyú (@marthaperaltae) [26 de junio de 2019](https://twitter.com/marthaperaltae/status/1143927753789644800?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según **Alejandro Vergara, participante de la protesta,** el paro se está llevando a cabo porque los recursos del Departamento están siendo dilapidados al no tener derechos fundamentales como educación, salud, saneamiento básico o acceso a agua potable. (Le puede interesar: ["Duque, atienda primero la crisis humanitaria en el Caribe: Fuerza Mujeres Wayúu"](https://archivo.contagioradio.com/guajira/))

En primera medida, los manifestantes acudieron a Wilbert José Hernández Sierra, gobernador encargado de La Guajira, para buscar el arreglo de las vías de circulación, pero él les comunicó que se reuniría con el presidente Duque y tras el encuentro, no pasó nada. Posteriormente se comunicaron con el Instituto Nacional de Vías (INVIAS) para buscar una solución, pero allí tampoco fueron escuchados, y finalmente, se dirigieron **al Gobierno Nacional, que hizo oídos sordos ante sus peticiones.**

### **Las cuatro demandas de los manifestantes en La Guajira**

Según lo **explicó Carolina Díaz, integrante de la Mesa Departamental del Paro Cívico,** tiene 4 puntos básicos de exigencia de derechos que esperan alcanzar con el paro: **reparación de vías, inversión estatal, apoyo para atender el proceso de movilidad humana que llega desde Venezuela y asegurar el servicio de agua potable para el Departamento**. Sobre el primer punto, Vergara señaló que en la vía que comunica a La Guajira con el Cesar hay un peaje que genera 60 millones de pesos a diario (cerca de 2 mil millones mensuales), pero las autoridades afirmaron que utilizarían la mitad de esos recursos en las vías del Cesar.

Sobre el tema de inversión social, los manifestantes están pidiendo que se ponga en marcha un plan que asegure el acceso a educación en la región mediante el aumento de ingresos a la Universidad de La Guajira, recientemente calificada por el ranking GNC-Sapiens como una de las mejores del Caribe colombiano. Respecto al tercer punto, Díaz manifestó que era necesario apoyar con políticas públicas al Departamento, para así mismo prestar la atención adecuada a los migrantes y refugiados venezolanos que están llegando a la región.

Por último, tanto Díaz como Vergara afirmaron la preocupación ante la prestación del servicio de agua; pues aunque se construyó una represa que permitiría controlar los flujos de agua y suministrar el vital líquido a los 15 municipios que componen La Guajira, eso no ha ocurrido. A ello se suma el problema de desnutrición que sufren las y los niños en el Departamento, pues según cifras recopiladas por la Corte Constitucional, entre 2010 y 2018 se registró la muerte de 4.170 niños por desnutrición, asociada con causas estructurales.

### **"No somos rebeldes, somos un movimiento organizado para encontrar soluciones"**

> En La Guajira se está realizando un Paro Cívico Departamental exigiendo al Gobierno de [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) la garantía de los derechos fundamentales de quienes habitan una región donde la infancia se muere de hambre.
>
> ¡No a la represión! [@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [pic.twitter.com/bvHbRA0QRt](https://t.co/bvHbRA0QRt)
>
> — Feliciano Valencia (@FelicianoValen) [26 de junio de 2019](https://twitter.com/FelicianoValen/status/1143932536332201984?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La principal via del Departamento amaneció bloqueada desde el miércoles en la mañana, y rápidamente el Gobierno ordenó la intervención del Escuadron Móvil Anti Disturbios (ESMAD), que atacó a los manifestantes en varios puntos. De acuerdo a Vergara, en Fonseca los uniformados sacaron a los manifestantes de la ciudad y dispararon gases incluso contra las casas del lugar. (Le puede interesar: ["Comunidades del norte demandaron a Cerrejón y piden anular licencia ambiental"](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/))

Al paro se unieron también los sectores indigenas que habitan el extremo norte del país, y según la denuncia del Activista, "el ESMAD se metió a terrenos ancestrales (...) se metieron a las casas de los indígenas, les robaron los chinchorros" e incluso, orinaron las ollas que usan para cocinar. Pese a estos ataques, tanto Díaz como Vergara señalaron que se mantendrían en la vía y bloqueando el Departamento hasta que el Gobierno brinde alternativas; no obstante, para evitar la violencia en medio de la protesta Díaz manifestó que ellos no son rebeldes, "somos un movimiento organizado para encontrar soluciones".

> Apoyamos el paro cívico de la Guajira que se realiza en el sur del depto.El líder indígena de Fonseca, Crispiano Fragozo fue detenido e incomunicado; responsabilizamos a la [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) por su vida.Nuestros pueblos, guardia indígena y autoridades Wayúu siguen en pie de lucha. [pic.twitter.com/d8pdXiE3v5](https://t.co/d8pdXiE3v5)
>
> — Martha Peralta Epieyú (@marthaperaltae) [26 de junio de 2019](https://twitter.com/marthaperaltae/status/1143925974804979713?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<iframe id="audio_37718408" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37718408_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio TRadio]
