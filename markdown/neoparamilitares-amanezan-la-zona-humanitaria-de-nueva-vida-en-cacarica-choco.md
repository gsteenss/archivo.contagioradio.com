Title: Neoparamilitares amenazan la zona humanitaria de Nueva Vida en Cacarica, Chocó
Date: 2018-02-07 11:23
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, cacarica, Nueva Vida
Slug: neoparamilitares-amanezan-la-zona-humanitaria-de-nueva-vida-en-cacarica-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [07 Feb 2018] 

Los habitantes de la zona humanitaria Nueva Vida, en el territorio colectivo de Cacarica, denunciaron que un grupo de neoparamilitares, pertenecientes a las Autofensas Gaitanistas de Colombia, AGC, entraron a la comunidad, el pasado 3 de febrero, **hacia las 2:00pm y los amenazaron de muerte, tanto a los líderes sociales como a los habitantes del lugar sí informaban sobre esta incursión**.

Las personas de la zona humanitaria afirmaron que el grupo armado desconoció la exigencia de los pobladores de respetar su espacio de protección civil y **amenazaron en particular a los líderes afrocolombianos que han mantenido procesos de denuncia en el territorio**. (Le puede interesar: ["Líder social y comunidad del Bajo San Juan Litoral amenazadas por retornar a su territorio"](https://archivo.contagioradio.com/lider-social-y-comunidad-del-litoral-bajo-san-juan-amenazadas-por-retornar-a-su-territorio/))

"A los sapos no les pasamos una" (...) Les vamos a dar como a ratas.". (...)Aseveraron, " si no cambian las cosas, vamos a empezar a actuar", expresaron los hombres armados, de acuerdo con la denuncia de la Comisión de Justicia y Paz.

Además, los neoparamilitares agregaron que “no gustaban de violadores, ni marihuaneros, ni pandilleros". Posteriormente, de acuerdo con la comunidad, se movilizaron en un bote hacia el lugar conocido como La Tapa, sitio desde donde las autodemoninadas **AGC, controlan permanentemente el acceso y la movilidad de civiles hacia los ríos Peancho, Peranchito y Bocas del Limón**.

Así mismo, según la organización de derechos humanos, en este lugar las mujeres han sido intimidadas, los bienes de supervivencia han sido saqueados y desde **allí también se realiza un control a la movilidad de los líderes y lideresas del territorio** que hacen parte de la Asociación de Familias de los Consejos Comunitarios de Autodereminación, Vida y Dignidad, CAVIDA.

Este hecho se suma a otras denuncias que ya había realizado la comunidad en donde afirmaban que desde el año 2015 ha existido la presencia de este grupo neoparamilitar en el territorio, y que pese a las diferentes alertas que han hecho continúan sin medidas de protección por parte de las autoridades correspondientes. (Le puede interesar: ["Paramilitares asesinan a habitante en Cacarica, Chocó"](https://archivo.contagioradio.com/paramilitares-asesinan-a-lider-social-en-cacarica/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
