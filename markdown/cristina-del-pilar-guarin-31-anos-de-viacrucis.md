Title: Cristina del Pilar Guarín: 31 años de viacrucis
Date: 2016-09-08 17:44
Category: Judicial, Otra Mirada
Tags: Cristina del Pilar Guarín, Palacio de Justicia, plazas vega, René Guarín
Slug: cristina-del-pilar-guarin-31-anos-de-viacrucis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cristina-Guarín-e1473374652897.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Sept 2016 ] 

Hoy René Guarín se enfrenta al sabor agridulce que le deja recibir el informe de la Fiscalía General de la Nación junto a los nueve restos óseos de su hermana Cristina del Pilar Guarín. René lleva 31 años exigiendo justicia, buscando la verdad, enfrentando amenazas y persecuciones, y **11 meses esperando la entrega de los** r**estos óseos de su hermana asesinada, calcinada y luego desaparecida** en los hechos que rodearon la toma y retoma del Palacio de Justicia entre el 6 y 7 de noviembre de 1985.

"Es grato porque representa el resultado de la persistencia de **31 años en medio tanta barbarie, injusticia estatal, estigmatización y ausencia de verdad** (...) es el encuentro con la hermana, pero no es ni siquiera con la hermana muerta y completa, es el encuentro con nueve huesos, uno de los cuales permitió establecer que Cristina no fue víctima del homicidio del cuarto piso, como siempre sostuvo el Estado, sino que ella terminó siendo **asesinada y su cuerpo fue dejado en medio de las llamas** para que apareciera como muerta por el incendio", asegura René.

Para René "la lucha no ha sido fácil": hace 31 años quienes iniciaron la búsqueda fueron su mamá y su papá, y murieron sin hallar verdad, justicia y reparación. Hace tres décadas José Guarín Ortiz y Elsa Cortés, buscaron a Cristina del Pilar en oficinas de Gobierno, Medicina Legal, juzgados y hospitales, **"desde ese tiempo mis padres me dijeron: claudicar es peor que la muerte"**, afirma René.

Don José murió en 2001 y doña Elsa en 2011, cuando René vivía en el exilio. Hoy tres décadas después, él sabe que su familia "desde el más allá", está viendo cómo su persistencia, sonrisas, lágrimas y luchas, honran la memoria de Cristina del Pilar. "Persistencia por conocer la verdad de cómo fueron asesinados, una lucha que no comenzó ayer, ni va terminar mañana", agrega René e insiste en que **persistirán para que el Estado colombiano, que ha sido juez y parte, entregue los cuerpos completos en condiciones de dignidad y diga toda la verdad a todos los familiares.**

La historia no es fácil; **hace un año fueron encontrados algunos fragmentos óseos de Cristina, casi todo el cuerpo de Luz Mary Portela y una vertebra de Lucy Amparo Oviedo**. Fiscalía y Medicina legal lo anunciaron con bombos y platillos, pasaron los meses y no devolvían los restos. Cartas al Instituto de Medicina Legal, que en un primer momento aseguró que ya estaba lista la entrega para febrero, quién debía encargarse del trámite era la Fiscalía.

La Fiscalía respondió tres meses después, diciendo que no podían hacer la entrega porque no conocían el informe técnico de Medicina Legal. Tras conocer la falta de coordinación entre las instituciones, los familiares se dirigieron a la **Corte Interamericana de Derechos Humanos que le ordenó al Estado colombiano entregar un informe antes del 27 de Julio**, explicando la dilación. Solo ahí comenzaron a funcionar las entidades estatales.

### **Entrega de restos en el mismo sitio por el que entraron los tanques para la retoma** 

Este viernes el acto solemne, de honras fúnebres después de 31 años, iniciará a las 9 de la mañana intervendrán la Vicefiscal Paulina Riveros quien entregará los restos; la fiscal encargada del caso; Rene Guarín; los delegados de la Comisión Intereclesial de Justicia y Paz y el Colectivo de Abogados José Alvear Restrepo; el ex magistrado José Roberto Herrera, que hizo parte de la Comisión de Esclarecimiento de la toma y retoma del Palacio de Justicia; Camilo Umaña Hernandez, hijo del abogado José Eduardo Umaña, quien llevaba el caso hasta que fue asesinado en 1998; el profesor Miguel Ángel Beltrán, y el sacerdote Alberto Franco quien dará inicio a un viacrucis que desde el Palacio, pasará por la Casa del Florero y culminará en la capilla del Colegio de San Bartolomé, dónde después de una eucaristía se ihumarán los restos.

"En Colombia hay cerca de 45 mil personas desaparecidas, una cifra vergonzosa, yo creería que no hay que claudicar porque es peor que la muerte, hay que tener dignidad y exigir del Estado el cumplimiento de la **búsqueda de las personas desaparecidas y la entrega en condiciones dignas** (...) por la [[búsqueda de verdad](https://archivo.contagioradio.com/30-anos-3-hallazgos-y-cero-de-verdad-rene-guarin/)], justicia, reparación y garantías de no repetición", concluye René Guarín.

Otra de las apuestas de René y de las familias de las [[víctimas desaparecidas en el Palacio de Justicia](https://archivo.contagioradio.com/medicina-legal-confirma-que-victima-de-la-retoma-del-palacio-fue-asesinada/)] es la de "la verdad para las víctimas, porque por más dolorosa que sea, **es más sanadora que todo**".

<iframe src="https://co.ivoox.com/es/player_ej_12844574_2_1.html?data=kpellpmZe5Whhpywj5aYaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncbPZz8qYqdrFtoa3lIquptOJdqSfqdLb0ZCntsrn1c7bw5DIqc2fsc7Zw9ePi9bV04qwlYqliM-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
