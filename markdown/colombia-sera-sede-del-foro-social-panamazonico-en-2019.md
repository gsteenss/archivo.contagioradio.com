Title: Colombia será sede del Foro Social Panamazónico en 2019
Date: 2018-06-08 15:09
Category: Ambiente, Nacional
Tags: amazonas, Amazonía, defensa de la amazonía, Foro Social Amazónico
Slug: colombia-sera-sede-del-foro-social-panamazonico-en-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/La-Amazonía-colombiana.-Alejandro-Polling-WWF-e1528487950341.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alejandro Polling WWF] 

###### [08 Jun 2018] 

En aras de **defender la Amazonía** y posicionar los problemas que afronta en la agenda nacional e internacional, diferentes organizaciones que protegen los derechos del ambiente preparan el Foro Social Panamazónico. Un espacio en que se definirán los detalles para el evento a realizarse en 2019 en Colombia, que contará con la presencia de 1500 personas de los 9 países de la cuenca andinoamazónica.

Para perfilar las líneas de debate, más de **50 líderes sociales** que hacen parte de comunidades y procesos campesinos, indígenas, comunales y comunitarios del Putumayo y Caquetá, se reunieron en Mocoa el 6 de junio. Allí, reflexionaron sobre las problemáticas que afrontan como parte de la cuenca amazónica.

### **El noveno Foro se realizará en Colombia** 

De acuerdo con Javier Marín, integrante de la Asociación Minga, el Foro Social Panamazónico está inspirado en el Foro Social Mundial que surgió en la década pasada que se creó alrededor del **territorio de la Amazonía**.

Durante el octavo foro que se realizó en Perú, se decretó que la novena edición se llevara a cabo en Colombia para fortalecer los procesos de diferentes iniciativas que buscan desarrollar soluciones para defender el territorio.

Marín recuerda que en el evento se han construido procesos de articulación con las organizaciones territoriales “entendiendo que la Amazonía es un ecosistema socio cultural que abarca desde los **Andes hasta el Atlántico**”. En esa medida, lo que han buscado las organizaciones es posicionar la problemática del Amazonas en la agenda internacional y nacional.

### **Estas son las problemáticas que tiene la Amazonía** 

Recordó que en Colombia la política de intervención de las empresas transnacionales ha puesto en riesgo el Amazonas. La política **extractivista, la ganadería extensiva y la agroindustria,** son ejes fundamentales que amenazan los ecosistemas andinoamazónicos. (Le puede interesar:["El precedente que marca el fallo sobre la amazonía colombiana"](https://archivo.contagioradio.com/fallo_historico_corte_suprema_amazonas/))

Además, las políticas gubernamentales enfocadas a la explotación turística y “el capitalismo verde” requieren procesos de articulación de las organizaciones para hacerles frente.  De igual forma, Marín enfatizó en que el tema de la Amazonía **no ha tenido una mayor trascendencia** en diferentes países por lo que plataformas como el Foro Social Amazónico sirven como mecanismos para hacer conciencia de la importancia de este territorio para Sur América.

Con la definición de Mocoa como sede para el evento, se constituyó el Comité Local como una medida para organizar la agenda a tratar en el desarrollo del Foro. Se espera que los 9 países en donde tiene influencia la Amazonía, participen de forma activa del evento que tendrá lugar el año que viene.

Las problemáticas de ese territorio serán tratadas allí con la intención de generar dinámicas que ayuden a construir políticas y lazos entre las organizaciones para defender el territorio y la vida del Amazonas.

<iframe id="audio_26435042" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26435042_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
