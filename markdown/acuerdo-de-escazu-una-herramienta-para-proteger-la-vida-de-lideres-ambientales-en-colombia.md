Title: Acuerdo de Escazú una herramienta para proteger la vida de líderes ambientales en Colombia
Date: 2018-09-26 12:46
Author: AdminContagio
Category: Entrevistas, Movilización
Tags: Acuerdo de Escazú, Ambiente, Ambiente y Sociedad, colombia, lideres sociales
Slug: acuerdo-de-escazu-una-herramienta-para-proteger-la-vida-de-lideres-ambientales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/censat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat] 

###### [26 Sept 2018] 

Diversas organizaciones sociales le están pidiendo al gobierno de Colombia que se adhiera al acuerdo regional en asuntos Ambientales en América Latina y el Caribe, Escazú, que busca proteger la vida de los líderes y defensores de la naturaleza, debido a que en el país, desde la firma del Acuerdo de Paz, **se han asesinado 33 activistas que cumplían con esta labor en sus territorios.**

Camila Cristancho, abogada de la organización Ambiente y Sociedad, aseguró que lo innovador de este acuerdo es que especifica que para defender los recursos naturales y el ambiente es necesario proteger a quienes efectúan esta tarea, además de que"este es uno de **los primeros acuerdos internacionales que trae un apartado completo para garantizar la protección de los mismos**".

### **La situación de defensores del Ambiente en Colombia** 

Cristancho manifestó que una de las preocupaciones más grandes frente al contexto colombiano es el aumento del asesinato a líderes sociales, debido a que desde la firma del Acuerdo de Paz, se han registrado **300 casos, de los cuales 37 de ellos eran defensores del ambiente o impulsaban iniciativas de protección a los recursos naturales. **

De igual forma aseveró que debido a la falta de avances en las investigaciones de los asesinatos a los líderes ambientales, no se ha podido establecer las causas de estos hechos, ni quiénes son los responsables intelectuales. (Le puede interesar: ["77 defensores de DD.HH han sido asesinados en el primer semestre del 2018"](https://archivo.contagioradio.com/77-defensores-asesinados/))

En esa medida, Cristancho explicó que si Colombia se une a este acuerdo, entrará a formar parte de un pacto regional que creará un marco regulatorio e institucional, con aportes de recursos económicos, para garantizar la protección a los defensores del ambiente, mientras que a **nivel nacional sería una herramienta que actuaría como ente de vigilancia para estar al tanto de los avances del país en esa materia**.

El acuerdo se firmará en la sesión de la asamblea general de la **ONU del próximo 27 de septiembre**. Una vez el país se una a este tratado continuará el proceso de ratificación interna en el gobierno y posteriormente se dará una aprobación de constitucionalidad desde la Corte.

<iframe id="audio_28911935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28911935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
