Title: Colombia es uno de los 10 países más peligrosos para los sindicalistas
Date: 2016-07-15 13:56
Category: Movilización, Nacional
Tags: colombia, OCDE, Sindicalistas
Slug: colombia-es-uno-de-los-10-paises-mas-peligrosos-para-los-sindicalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-de-mayo-4-20161.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

###### [15 Jul 2016] 

Colombia es uno de los 10 países del mundo donde menos respeta los derechos de los trabajadores, así lo concluye en reciente estudio de la Confederación Sindical Internacional, CSI donde además se señala que las restricciones de la libertad de expresión y de reunión, incluyendo intensas campañas de represión en algunos países, se incrementaron **en un 22%, registrándose restricciones en 50 de los 141 países incluidos en el informe.**

De acuerdo con Carlos Julio Díaz, director de la Escuela Nacional Sindical, **“De cada 100 hechos de violencia contra sindicalistas en  el mundo**, la mitad ocurren en Colombia, donde el  derecho a la huelga está en vía de extinción”.

Este tipo de infracciones a los derechos de los trabajadores, se posibilita debido a que el Estado colombiano no efectúa un control y seguimiento oportuno a los marcos regulatorios que deben garantizar los derechos de los trabajadores, evidenciando que el Estado es uno de los grandes responsables de las violaciones a los derechos humanos por la incapacidad y falta de voluntad para actuar, explica Díaz.

**“El sector empresarial en Colombia tiene una lógica antisindical”**, explica el director de la Escuela Nacional Sindical , quien agrega que además el TLC con Corea que arrancó este viernes, imposibilita el desarrollo industrial colombiano, **“Seguimos siendo uno de los 10 países en los que es más difícil ser sindicalista**”.

Frente al panorama internacional, Sharan Burrow, Secretaria General de la CSI, afirma **“Estamos siendo testigos del cierre de espacios democráticos y un aumento de la inseguridad, el temor y la intimidación hacia los trabajadores y trabajadoras.** El ritmo al que se están intensificando los ataques contra los derechos, incluso en algunas democracias por ejemplo las propuestas del Gobierno en Finlandia o la nueva ley de sindicatos en el Reino Unido, indica una alarmante tendencia para los trabajadores/as y sus familias”.

Cabe resaltar que la Organización para la Cooperación y el Desarrollo Económico, OCDE, informó semanas atrás que uno de los países en donde más se trabaja al año en Colombia con **con 2.496 horas en promedio**. Belarús, China, Colombia, Camboya, Guatemala, India, Irán, Qatar, Turquía y los Emiratos Árabes Unidos,  son los diez peores países del mundo para los trabajadores y trabajadoras.

"El panorama es complicado pero los acuerdos de paz de la Habana se nos abren posibilidades democráticas", concluye Carlos Julio Díaz.

<iframe src="http://co.ivoox.com/es/player_ej_12236928_2_1.html?data=kpeflZuddpmhhpywj5aaaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCuuc3d0JCxh6iXaaK4wt-SlKiPidTX1srZw5CypcTd0NPOzpC3rc_YysjOzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
