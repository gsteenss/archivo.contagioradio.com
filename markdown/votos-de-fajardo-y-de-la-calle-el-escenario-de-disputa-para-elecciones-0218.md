Title: Votos de Fajardo y De la Calle el escenario de disputa para elecciones 2018
Date: 2018-05-28 16:29
Category: Paz, Política
Tags: De La Calle, Elecciones presidenciales 2018, Fajardo, Iván Duque, Petro, Vargas Lleras
Slug: votos-de-fajardo-y-de-la-calle-el-escenario-de-disputa-para-elecciones-0218
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/petro-y-duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: KienyKe, Dinero] 

###### [28 May 2018] 

Varias conclusiones salen de las elecciones de ayer en Colombia, de acuerdo con Fernando Giraldo, analista político. La primera de ellas es que los votos de Fajardo y De La Calle son la nueva disputa entre la derecha y la izquierda del país**. De igual forma, es la primera vez en la historia política colombiana, un candidato de izquierda llega a disputarse una segunda vuelta presidencial** y finalmente estaría la “parcial” derrota de las maquinarias políticas que no le alcanzaron a Germán Vargas Lleras.

### **Las sorpresas de las elecciones** 

Giraldo afirmó que la mayoría del electorado joven apoyó las candidaturas de Gustavo Petro y Fajardo, y que particularmente en esta ocasión, la participación se acercó a 20 millones de electores, comparada a la última elección en la que hubo un 60% de abstención. Este aumento, según Giraldo, se debe a que había nuevos electores y a que el proceso de **paz tuvo un alto impacto positivo sobre asegurar una democracia incluyente que disminuyera la violencia por pensar diferente**.

### **La quemada de Vargas Lleras** 

Frente al resultado de Vargas Lleras, que saco un **7,28% del escrutinio total, Giraldo afirmó que hay una derrota de las maquinarias políticas**, “buena parte del partido Liberal, del partido de la U, del partido Conservador, del partido Cambio Radical y del Centro Democrático, votaron por Iván Duque”.

Hecho que revela que el voto de opinión en Colombia sería más grande que el voto de maquinaria y que además está divido en las tendencias de Petro, Fajardo y De la Calle, que no contó con la maquinaria del partido Liberal. (Le puede interesar:["Defensores de DD.HH del Tolima, amenazados por intensión de voto a favor de Petro"](https://archivo.contagioradio.com/defensores-de-ddhh-del-tolima-amenazados-ante-intencion-de-voto-por-gustavo-petro/))

**La segunda vuelta**

Para Giraldo, la segunda vuelta estará bastante estrecha. De un lado se podrían sumar los votos de Vargas Lleras a la tendencia de Duque. No obstante, no hay tanta claridad frente al comportamiento de los votos de opinión. Además, **el analista advierte que podría darse un aumento en el voto en blanco producto de esa partición.**

“La gente no está buscando partidos sino personas, esto puede ser bueno o malo, es más riesgoso, pero es la realidad” afirmó Giraldo. Frente a las posibilidades que tiene Duque de llegar a la presidencia, el analista señaló que habrá una parte de la votación de Fajardo que irá a parar en el apoyo al candidato del Centro Democrático, pero que aún es apresurado darlo por hecho.Del otro lado, para que Gustavo Petro pueda ganar, **Giraldo enfatizó en que necesitará que los “Fajardistas”, en un 75% se decidan a votar por él**.

Giraldo manifestó que hay que esperar dos o tres días para analizar cómo se ha configurado el tablero político de las alianzas y se deje en firme los apoyos de Vargas Lleras, De la Calle y Fajardo, o la de los otros sectores que no hayan participado hasta el momento y se quieran sumar.

<iframe id="audio_26222889" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26222889_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
