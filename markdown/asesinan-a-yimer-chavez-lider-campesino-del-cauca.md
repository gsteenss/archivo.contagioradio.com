Title: Asesinan a Yimer Chávez, lider campesino del Cauca
Date: 2016-10-18 15:08
Category: DDHH, Nacional
Tags: acuerdos de paz ya, Cauca, Líderes campesinos asesinados
Slug: asesinan-a-yimer-chavez-lider-campesino-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/guardia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MINGA] 

###### [18 Oct 2016]

E**l líder campesino Yimer Chávez, fue asesinado el 16 de Octubre en horas de la tarde, por dos hombres quienes le propinaron 3 disparos.** Los hechos fueron denunciados, mediante un comunicado emitido por la Organización para el Desarrollo Urbano Campesino, ORDEURCA, quienes también exigieron al Gobierno garantías para líderes campesinos e indígenas defensores de Derechos Humanos.

Este líder campesino, tenía 31 años y era el coordinador de la Guardia Campesina y Popular en la vereda Frontino en el municipio de La Sierra, Walter Quiñonez, integrante de la organización señala que Chávez “era un inalcanzable defensor de derechos humanos, constructor de paz y trabajador de la tierra (…) **no queremos que este caso quede impune como otros que han ocurrido en el departamento**”. Le puede interesar: [En menos de un mes han sido asesinados 11 líderes sociales en colombia](https://archivo.contagioradio.com/en-menos-de-un-mes-han-sido-asesinados-11-lideres-sociales-en-colombia/)

Por otra parte, Walter revela que lo más preocupante es que “Yimer no había recibido ningún tipo de amenaza”, sin embargo, manifiesta que el asesinato del líder campesino **“constituye una agresión directa contra las propuestas organizativas de paz para el Cauca,** para toda nuestra población, y es una amenaza directa a la organización”.

Quiñonez resalta que además de la denuncia, se hace una exigencia al Gobierno para que **proteja a los dirigentes, y también para que la fiscalía haga la investigación correspondiente y entregue un informe a la comunidad,** “pues no estamos tranquilos y sabemos de la presencia de actores armados y delincuencia organizada en los territorios”. Le puede interesar: [Caravana de juristas constata 54 asesinatos en norte de santander y cauca en los últimos meses](https://archivo.contagioradio.com/continua-riesgo-para-defensores-de-derechos-humanos-y-victimas-en-colombia-caravana-de-juristas/)

**Los integrantes de ORDEURCA hacen una serie de exigencias**

<ol>
1.  Requerimos que las instituciones encargadas asumen la investigación respectiva de este hecho repudiable de violencia y violación del derecho fundamental a la vida.
2.  Exigimos al Estado garantías reales para los defensores y defensoras de los derechos humanos en el Cauca.
3.  Exigimos el reconocimiento y la protección de las Guardias Campesinas y Populares como figura de construcción de paz en los territorios.![Comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14712768_10153840252130812_6918386198790787535_o.jpg){.alignnone .size-full .wp-image-30840 width="826" height="1169"}

</ol>
<iframe id="audio_13374083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13374083_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-bit.ly1nvao4u-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio-bit.ly1icyhvu .p1}
