Title: Colombia por debajo el promedio mundial y regional en paridad política
Date: 2019-03-05 18:28
Author: CtgAdm
Category: Mujer, Política
Tags: Cámara de Representantes, Congreso, Informe, ONU Mujeres, paridad política, Registraduría Nacional, Senado
Slug: colombia-debajo-promedio-mundial-regional-paridad-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [05 Mar 2019] 

Un nuevo informe de la Registraduría Nacional y ONU Mujeres indica que el porcentaje de participación feminina en el Congreso colombiano está por debajo del promedio en la región de las Américas y en el mundo. Además, **disminuyó la representación parlamentaria para esta población del 2014 al 2018,** lo cual es "un llamado a la acción para hacer efectiva la plena participación de las mujeres en los puestos de representación".

Si bien el porcentaje de candidatas incrementó en las elecciones legislativas del 2018 al 34,5%, el porcentaje de mujeres elegidas solo alcanzó el 19,7%. Estos resultados distanciaron el país del promedio de representación en las Américas que se encuentra en 29,7% y el promedio mundial de 24,1%.

Estos resultados "muestran que alcanzar una representación paritaria de hombres y mujeres en las corporaciones públicas y demás cargos de elección popular sigue siendo un desafío para el Estado Colombiano", señaló el informe.

 

![Paridad Política](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Screen-Shot-2019-03-05-at-2.43.22-PM-585x337.png){.size-medium .wp-image-62719 width="585" height="337"}

El análisis presentado señala que la cuota del 30% de representación feminina en las listas de candidaturas, tal como lo plantea la ley 1475 de 2011, es una "medida necesaria temporal" que se debería seguir exigiendo. Sin embargo, también indica que los partidos políticos son los principales responsables de generar las condiciones de igualdad en la participación electoral y por tal razón, deberían superar lo mínimo requerido al selecionar candidatos.

Para 2018, solo cinco organizaciones políticas superaron el 30% de representación de mujeres. Todas ellas fueron minoritarias: la Coalición Lista de la Decencia, el Partido Alianza Verde, el Partido Mira, el Partido Opción Ciudadana y la G.S.C Colombia Humana. Y de estas, tres alcanzaron la paridad: la Mira, Opción Ciudadana y Colombia Humana.

Al respecto, el informe también resaltó que los principios de paridad en la conformación de las listas de candidaturas están incluidos en la Constitución Política, pero no han sido reglamentados. De hecho, un artículo en la Reforma Política del año pasado que **reglamentaba la paridad política fue hundida en la Cámara de Representantes**. (Le puede interesar: "[Mujeres seguirán luchando en Colombia por la paridad democrática](https://archivo.contagioradio.com/mujeres-seguiran-luchando-en-colombia-por-la-paridad-democratica/)")

En contraste, son seis países latinoamericanos que regulan la paridad política, Argentina, Bolivia, Costa Rica, Ecuador, México y Nicaragua, además en Honduras y Panamá, las reglas de paridad operan parcialmente.

### **Sí hay avances** 

Por primera vez, la Registraduría presentó información desagregada por sexo de los votantes en las elecciones del 11 de marzo de 2018. El 51,7% de los sufragantes para estos comicios fueron mujeres, lo cual "contrasta con la baja representación femenina obtenida para ambas cámaras".

Por el otro lado, el informe resalta que mujeres fueron elejidas por primera vez en cinco departamentos, **Amazonas, San Andrés, Chocó, La Guajira y Vaupés**. Este hecho constituye "un avance importante" dado que en estas circunscripciones territoriales nunca se había obtenido representación femenina desde la Constitución de 1991.

Finalmente, el informe evidencia un interés creciente por parte de las mujeres en consolidar una carrera política que se extiende más de un período electoral. En el caso de las senadoras existe un aumento de 13 puntos porcentuales entre las mujeres que conservaban una curul en el Congreso en 2014, el 43,4%, y las que lo conservan en 2018, el 56,5%. Para la Cámara de Representantes esta diferencia es de 19 puntos porcentuales.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
