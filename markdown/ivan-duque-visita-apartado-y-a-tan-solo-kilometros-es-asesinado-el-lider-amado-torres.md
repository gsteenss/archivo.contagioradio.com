Title: Iván Duque visita Apartadó y a tan solo kilómetros es asesinado el líder Amado Torres
Date: 2020-02-29 18:34
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Antioquia, asesinato de líderes sociales, Iván Duque, San josé de Apartadó
Slug: ivan-duque-visita-apartado-y-a-tan-solo-kilometros-es-asesinado-el-lider-amado-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Amado-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto/ Amado Torres: Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La mañana de este sábado 29 de febrero en San José de Apartadó, Antioquia fue asesinado el líder social y actual tesorero de la Junta de Acción Comunal de la vereda La Miranda, **Amado** **Torres** de 49 años. El suceso, que señalan fue perpetrado por accionar paramilitar ocurrió el mismo día que el presidente Iván Duque visitaba el corregimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relatan sus familiares, cerca de las 6:00 am, hombres fuertemente armados con prendas militares llegaron hasta la vivienda del líder y lo sacaron a la fuerza para después disparar contra Amado. [(Lea también: Comunidad responsabiliza al Ejército del asesinato y desaparición del líder Didian Agudelo)](https://archivo.contagioradio.com/comunidad-responsabiliza-al-ejercito-del-asesinato-y-desaparicion-del-lider-didian-agudelo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Construyendo país?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**El asesinato ocurre el mismo día en que el presidente Iván Duque llegó a la región como parte de su agenda 'Construyendo País**'. Aunque la visita del mandatario - cuyo esquema de seguridad fue reforzado en diciembre de 2019 - debería redoblar la protección del corregimiento; y la alta cantidad de pie de fuerza que existe en esta zona del país ser un apoyo, estas no han sido suficientes en veredas como La Miranda donde fue asesinado el líder social.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que durante su intervención en la región de Urabá, Duque hizo referencia a su compromiso con los habitantes y su respaldo a los agricultores y al sector rural, **los ataques contra líderes sociales en departamentos como Antioquia, donde hasta enero habían sido asesinados siete líderes sociales**, evidencian que son necesarias medidas integrales de protección en las zonas rurales. [(Lea también: Más medidas integrales y menos accionar policial y militar: ONU)](https://archivo.contagioradio.com/onu-mas-medidas-integrales-menos-accionar-policial-militar/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AbadColorado/status/1233867136872914944","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AbadColorado/status/1233867136872914944

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Denuncian además que [la familia de Amado Torres](https://twitter.com/AbadColorado/status/1233867136872914944) **tuvo que bajar el cuerpo de su ser querido desde su vereda en una hamaca debido a la negativa de la Sijin para realizar el levantamiento** y realizar la investigación pertinente, por lo que el trabajo tuvieron que realizarlo los mismos integrantes de la comunidad. Con el asesinato de Amado, la cifra de líderes asesinados en lo que va del 2020 llega a 43, según organizaciones defensoras de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
