Title: Festival de la montaña
Date: 2020-10-30 14:57
Author: AdminContagio
Category: eventos, yoreporto
Tags: Audiovisual, eventos, Festival, festival de la montaña
Slug: festival-de-la-montana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/festival.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/119828931_745714352670884_40066918360150421_o.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### **Por:** Alejandro Trujillo Moreno

<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->

#### [Un encuentro para celebrar](https://www.facebook.com/watch/?v=256126302484287), reconocer y valorar los saberes y prácticas de las comunidades rurales de El Carmen de Viboral, Antioquia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las prácticas y saberes de las comunidades rurales representan un entramado comunitario que logra tejer diferentes generaciones, son una clara representación de lo que significa la vida en comunidad y de la relación que se establece en el territorio, en las maneras de habitarlo y en los valores de identidad que se construyen de manera colectiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esto al referirnos a las prácticas y saberes de las comunidades rurales de **El Carmen de Viboral**, estamos preguntándonos al mismo tiempo por las narrativas del territorio que las comunidades han construido históricamente. Estos elementos no solo generan un reconocimiento, valoración y defensa del lugar que se habita, sino que al mismo tiempo permite dar continuidad a ese entramado cultural y natural que es el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es así como en el año 2015 inicia un proceso de formación musical con los niños de la escuela de la vereda El Porvenir, del cañón del Río Melcocho, que años más tarde daría un primer fruto de este proceso, alrededor de la investigación **“Entre el río y la montaña los hombres cantan para espantar la guerra: Caminando por las trochas de las músicas campesinas de El Carmen de Viboral, Antioquia”.** Este trabajo indagó por las funciones de la música en el Cañón del río Melcocho de El Carmen de Viboral, generando reconocimiento, valoración y revitalización de estas músicas de carácter rural como parte del patrimonio local y regional y en la que se expresa una cosmovisión del “ser campesino” de las montañas de Antioquia, entendiendo la música como una narrativa viva de la memoria colectiva de estos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así para el año 2018 como iniciativa de la Corporación cultural Nybram en alianza con la Junta de Acción Comunal de la vereda El Porvenir y los procesos de formación musical “Guitarras para educar”, nace “El festival de la montaña: Saberes y prácticas de las comunidades rurales de El Carmen de Viboral Antioquia”, un encuentro comunitario para el intercambio de saberes, el reconocimiento de las prácticas comunitarias como aspectos vitales del territorio y la celebración de la vida en comunidad desde la ruralidad. La música, la cocina, la agricultura, las festividades son algunas de las manifestaciones culturales que se quieren dinamizar desde el núcleo mismo de las comunidades, incluyendo dimensiones de formación, circulación, investigación y procesos de memoria, defensa y protección del territorio y el patrimonio natural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las dos primeras versiones del festival contó con la participación de comunidades rurales de las veredas La Cristalina, Santa Rita, El Roblal, El retiro, El Cocuyo, entre otras, a través del desarrollo de diferentes talleres y encuentros comunitarios en los que se tejieron narrativas comunitarias principalmente alrededor de la música y la cocina. Si bien el festival se concibe desde una sede principal en la Vereda El Porvenir Cañón del Río Melcocho, también se propone una actividad itinerante denominada “**A lomo de mula por otras veredas”,** que teja espacios para el intercambio de saberes, experiencias y un reconocimiento de la diversidad cultural que habita otros territorios rurales, creando vínculos comunitarios entre diferentes veredas de esta localidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta tercera versión que se realizará de manera virtual, presenta diferentes invitados que nos narran “Las trochas del maíz” desde el Cañón del río Melcocho, las veredas invitadas como la Palma, Camargo, Viboral, San José y algunas historias que habitan nuestras zonas urbanas pero cuya memoria es rural. Reconociendo en estos caminos la vocación agrícola de El Carmen de Viboral y su relación histórica con este alimento ancestral. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los contenidos audiovisuales son realizados por la Corporación cultural Nybram y otros por la Junta de acción comunal vereda El Porvenir, en un dialogo de saberes y una construcción colectiva presentada durante 5 días de emisión (26 al 30 de octubre) en los que nos acercaremos a los saberes y prácticas del maíz, maíz que se pila al ritmo de la música campesina y de sonoridades que cuentan la montaña y que podremos conocer  a través de las redes sociales del festival y el canal local Viboral tv. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este ecosistema de saberes “Las trochas del maíz” se presenta desde 5 componentes:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **1-Enjalmando la memoria:** Este componente reflexiona sobre aspectos de la memoria y la identidad campesina desde diferentes miradas. Acá se escuchará la voz de algunos invitados que compartirán su mirada, su experiencia y su saber. Desde este lugar se proyecta consolidar procesos de investigación desde la ruralidad.

<!-- /wp:list -->

<!-- wp:list -->

-   2-**Serenatas montañeras:** Las narrativas musicales son memoria viva de las comunidades rurales. Así en este espacio se presentan historias de vida de músicos campesinos, intervenciones musicales de grupos que habitan la ruralidad o que construyen su narrativa desde este lugar. Además de escuchar el cantar de los niños campesinos y sus nuevas formas de contar el territorio.

<!-- /wp:list -->

<!-- wp:list -->

-   3**-Sabores de la montaña:** La memoria de las comunidades rurales está en el alimento y en los conocimientos y saberes alrededor de la cocina campesina. La relación con la tierra parte de este principio vital que sustenta la vida. Este año conoceremos la memoria del maíz: sus preparaciones y sabores que son herencias antiguas de nuestro territorio.

<!-- /wp:list -->

<!-- wp:list -->

-   4**-Cultivando un saber:** Cultivar los saberes y prácticas rurales para que la memoria este viva en las nuevas generaciones. Aprender un oficio, distinguir un saber, recordar una práctica, es parte de lo que significa habitar un territorio rural. Esta versión estará nos conectará al tiple desde su fabricación, acercándonos a este ancestral instrumento que ha sonado en estas montañas durante más de un siglo.

<!-- /wp:list -->

<!-- wp:paragraph -->

En esta versión además, se incluirán narrativas del territorio rural desde las percepciones y vivencias de los niños en los que nos contarán ¿Qué significa el maíz?, ¿Qué recuerdos tienen del maíz en la vereda?, ¿Qué significado le da a la música campesina?, ¿Qué le gusta de vivir en el campo?

<!-- /wp:paragraph -->

<!-- wp:list -->

-   5-**Las palabras de la montaña:** La lectura en voz alta de poemas, mitos, cuentos es un ejercicio vital en la promoción de lectura y en la recuperación de la tradición oral. Encontrar las voces de la montaña, sus múltiples acentos e historias de la más antigua a la más reciente. Esta versión retoma aquellas poéticas del maíz para hacer memoria y agradecer su siembra.

<!-- /wp:list -->

<!-- wp:paragraph -->

Además de los contenidos anteriores el festival propone una actividad itinerante denominada “**A lomo de mula por otras veredas”,** que busca tejer espacios para el intercambio de saberes, experiencias y un reconocimiento de la diversidad cultural que habita en los territorios rurales, creando vínculos comunitarios entre diferentes veredas de esta localidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente y como parte de la interacción con otras miradas, se crea el primer concurso de fotografía “La siembra”, con este espacio queremos invitar a la comunidad a contar desde su mirada lo que significa sembrar, permitiendo crear así una historia contada por muchos que nos invite a conectar de nuevo con nuestras prácticas de agricultura y el cuidado de la naturaleza como fuente y madre del alimento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este festival 2020 fue ganador de la ***CONVOCATORIA DE APOYO A ENCUENTROS Y ESTIVALES ARTÍSTICOS Y CULTURALES DEL DEPARTAMENTO DE ANTIOQUIA 2020*** del Instituto de cultura y Patrimonio de Antioquia. Además es apoyado por la Administración municipal de El Carmen de Viboral y el Instituto de cultura de El Carmen de Viboral.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es un tejido comunitario apoyado además por diferentes instituciones públicas y privadas y diferentes colectivos y espacios del municipio que se suman a estos andares en[“Las trochas del maíz”](https://www.facebook.com/watch/?v=785733048920752): Viboral Tv, Carmelina parrilla, Carmelina Beer bar, Café teatro, Bohemia Café Bistro, La casa de Vero, Café Vivo Bar, El Barqueadero, Caffe Media Luna, Café Provincia, Corporación cultural Tespys,Estanquillo Licomás, Un desastre llamado Julia, Aleteos, Scuilo, Colectivo Antorcha, La píldora, Evolution, Hojarasca,  Soul Rebe, Licomás, Enfoque de Oriente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nuestra gratitud a todos los invitados que alimentan esta narrativa del maíz: familias participantes de la vereda El Porvenir Cañón del Río Melcocho, Casa Carrataplan Isaias Arcila, Granja los abuelos Doña Orfilia Velazquez, Raíces escuela Agrosocial, Jorge Gómez, María Judith Valencia, Ana Carmona, Fabián Giraldo, Nilton García, Buenaventura Granja Escuela, Israel Osorio, Granja Renacer Carlos Osorio, Empanadas bailables Reinaldo Álvarez, Casa Luthier Velazquez Obdulio Velásquez y Edwin Villa, Felix Trujillo, Robinson López, Niños de la vereda El Porvenir, Tradición Melcocheña y músicos Lucelida Martínez, Aldemar Hernández, Uriel Valencia, Odulio Patiño, Juan Gabriel Martinez, Arbey Martínez. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, hablamos[del Festival de la montaña como un encuentro comunitario](https://www.facebook.com/watch/?v=648249859213136)que invita a reconocemos desde las expresiones culturales de la ruralidad. Entendemos que la ruralidad no está separada de la vida urbana y que es justo en esta relación que podemos tejer nuevas posibilidades de vida. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reconocemos que la identidad está ligada a los procesos de territorialización y memoria y que es preciso crear nuevas perspectivas para nuestras comunidades rurales, advertir otras formas de habitar los campos desde una mirada ecosistémica en equilibrio con la naturaleza y recordar que ser campesino es una historia que debemos transitar para comprender lo que hemos sido, lo que somos y lo que seremos. Celebramos y sembramos la montaña, cultivamos los saberes que la habitan, agradecemos su alimento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Que canten los pájaros y que el río nos cuente sus historias, que la tierra florecida nos enseñe la bondad del alimento y que las manos del labriego nos recuerde el origen. La voz del labriego, la historia del monte, la palabra y el canto serán las huellas de otros tiempos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea mas sobre el festival](https://www.instagram.com/festival_delamontana/?hl=es-la)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[conozca mas sobre Contagio Radio](https://archivo.contagioradio.com/)

<!-- /wp:paragraph -->
