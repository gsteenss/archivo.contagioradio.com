Title: Claves del acuerdo del Sistema Integral de Verdad, Justicia, Reparación y No Repetición
Date: 2015-09-23 17:48
Category: Nacional, Paz
Tags: Acuerdo de justicia farc y gobierno, acuerdos de paz, Amnisitia, Cual es el acuerdo de justicia, FARC, Juan Manuel Santos en Cuba, Mesa de conversaciones de paz de la habana
Slug: sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Sistema-Integral-de-Verdad-Justicia-Reparación-y-No-Repetición.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [23 Sep 2015]

El acuerdo firmado hoy en La Habana, por la delegación de paz de las FARC y del gobierno nacional con la presencia de delegados nacionales e internacionales de los países veedores, garantes y EEUU tiene un significado histórico en Colombia dada la importancia de este acuerdo y que en ningún proceso de paz en el mundo se han contemplado temas tan completos y precisos como en el actual.

**Primera Clave: **Es un Sistema basado en la primacía de la verdad por encima de otros derechos de las víctimas. El mecanismo contempla que entre más acceso a la verdad de los hechos relacionados con el conflicto armado, más garantías judiciales tendrán las personas que decidan acogerse.

**Segunda Clave: **El sistema se contempla de manera integral y complementaria tanto para combatientes de las guerrillas y de las FFMM y de policía, como para empresarios y autoridades civiles que se hayan visto involucrados en hechos relacionados con el conflicto armado en Colombia. No hay trato diferente para uno u otro actor.

**Tercera Clave: **Se nombrará un tribunal Adhoc con jueces tanto nacionales, en su mayoría, como internacionales. Los jueces serán seleccionados por las partes en conversaciones como otros asesores externos y se tendrá en cuenta la opinión de las organizaciones de víctimas.  Una de las funciones principales del tribunal es garantizar la No Impunidad.

**Cuarta Clave: **El tribunal sesionará en siete salas entre las que se encuentran una para recibir los casos, otra para la revisión de sentencias, otra para establecer las responsabilidades individuales o colectivas y otra para establecer los montos de las sanciones a las personas que decidan acogerse al sistema.

**Quinta Clave: **Cualquier persona que esté involucrada en hechos violatorios en el escenario del conflicto armado en Colombia puede acogerse al Sistema Integral de Verdad, Justicia, Reparación y No Repetición con el compromiso de revelar la verdad acerca de sus propios crímenes o los de sus unidades y cadenas de mando.

**Sexta Clave: **En caso de que las personas que se acojan al sistema no revelen toda la verdad o mientan en sus declaraciones las penas privativas de la libertad irán hasta los 10 años o más según lo considere el tribunal.

**Séptima Clave:** Las penas que se establecerán en el tribunal adhoc serán penas restrictivas de las libertades y derechos civiles en establecimientos penitenciarios hasta por 20 años. Si las personas que se acojan al sistema revelan la verdad en cuanto a sus crímenes podrán hacerse acreedores a rebajas de penas o amnistias

**Octava Clave:** Se concederán amnistias o indultos más amplios posibles en los casos de los delitos políticos y conexos. Hay delitos que no son amnistiables en el sistema integral como los crímenes de Lesa Humanidad u otros crímenes contemplados en el estatuto de Roma como los delitos sexuales entre otros.

**Novena Clave:** Si Organizaciones de Derechos Humanos y de víctimas tienen documentados algunos casos en los que se establezca la responsabilidad de militares, policías, guerrilleros empresarios o cualquier otro actor podrán presentar casos ante el tribunal, haciendo las veces de una especie de fiscalía civil.

**Décima Clave:** La dejación de armas se realizaría por lo menos 60 días después de la firma del acuerdo final.
