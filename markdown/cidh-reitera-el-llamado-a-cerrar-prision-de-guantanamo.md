Title: CIDH reitera el llamado a cerrar prisión de Guantánamo.
Date: 2015-08-05 14:56
Category: El mundo, Otra Mirada
Tags: Felipe González relator de la CIDH para Estados Unidos, Informe CIDH cierre de Guantanamo, Presidenta de la CIDH, Rose-Marie Belle Antoine, Violación de Derechos Humanos en Guantanamo
Slug: cidh-reitera-el-llamado-a-cerrar-prision-de-guantanamo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/09est1f01-guantanamo-protesta-usa-838030.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Foto: Reuters 

###### [5/Agosto/2015] 

Con la publicación del **informe “Hacia el cierre de Guantánamo”**, la Comisión Interamericana de Derechos Humanos (CIDH) hace un nuevo **llamado a clausurar el centro de detención**, ante las inminentes y reiterativas **violaciones cometidas en contra de los prisioneros** desde su creación luego de los atentados del 11 de Septiembre de 2001.

El informe difundido este miércoles, analiza que el gobierno de Estados Unidos decidió abrir un centro de detención fuera de su territorio con el único propósito de **detener a extranjeros, musulmanes,  sospechosos de terrorismo,** diseñando un sistema especial de comisiones militares para juzgarlos, en **ausencia de las garantías procesales** disponibles ante las cortes federales.

De acuerdo con el informe , **las principales violaciones cometidas en contra de las personas detenidas por Estados Unidos, son** la práctica de tortura y otros tratos crueles, inhumanos o degradantes, la detención indefinida, el acceso limitado o nulo a la protección judicial, la falta de debido proceso, el régimen discriminatorio y la falta de defensa adecuada.

Ante los avances realizados por el gobierno Obama hacia el cierre del centro de detención, **Rose-Marie Belle Antoine, Presidenta de la CIDH**, asegura que "Saludamos los pasos dados por Estados Unidos en este sentido, a la vez que **urgimos a redoblar los esfuerzos para alcanzar el objetivo de cerrarlo de manera definitiva**”, alentando, desde las recomendaciones contenidas en el informe, a adoptar compromisos que garanticen el derecho internacional de los derechos humanos.

El comisionado **Felipe González, relator de la CIDH para Estados Unidos**, indicó que “las razones de seguridad pública no pueden servir como pretexto para la detención indefinida de personas sin presentar cargos en su contra o someterlas a juicio”, manifestando además que “**Estados Unidos debe liberar inmediatamente a todos los detenidos que no serán acusados ni juzgados**" y quienes están siendo procesados por comisiones militares "deben ser juzgados ante cortes federales ”.

En relación con los actos de tortura y malos tratos, "**Estados Unidos debe garantizar una reparación efectiva e integral**, y debe proceder a desclasificar toda prueba de tortura y malos tratos e investigar todos los abusos que hayan tenido lugar”, indicó el Relator .

“**Este informe es un paso adicional, y esperamos que final**, en el trabajo de monitoreo que la CIDH ha venido realizando desde que fueron trasladados los primeros detenidos a Guantánamo”, indicó la Presidenta Belle Antoine, destacando la importancia que "**Estados Unidos implemente las recomendaciones contenidas en este informe**, a fin de avanzar hacia el cierre de este centro de detención, que se ha convertido en un símbolo de abuso en todo el mundo”.

La CIDH reiteró su llamado a los **Estados Miembros de la OEA** a considerar **recibir a los detenidos de Guantánamo**, para lograr el objetivo de cerrar este centro de detención y reafirmar la larga tradición de asilo y protección de refugiados en la región.

##### Con información de la CIDH. 
