Title: Radicada reforma constitucional que avala el voto de la Fuerza Pública
Date: 2016-07-27 11:55
Category: Nacional
Tags: voto militares
Slug: se-radico-reforma-consitucional-para-que-fuerza-publica-pueda-votar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:medellin.gov.co] 

###### [27 de Jul] 

El senador Roy Barreras radicó una reforma constitucional en donde propone que **los miembros de la Fuerza Pública del país puedan ejercer el derecho al voto y participar electoralmente**. Propuesta que ha generado polémica debido a que se presenta en medio de un proceso de paz en el que la **Fuerza Pública ha estado involucrada con una posición fuerte desde su doctrina y se ha visto envuelta en crímenes como corrupción**.

De acuerdo con el Senador, **el derecho al voto solo se haría efectivo en un momento de posconflicto** y la iniciativa tendría como fin "reconocer los derechos políticos a los militares", sin embargo se mantendrían prohibiciones como que los militares opinen sobre política o puedan ser candidatos.

Para el senador Iván Cepeda "la iniciativa es improcedente e inoportuna, además en una historia en la que han ocurrido con una frecuencia sistemática violación a derechos humanos y [toda clase de acciones criminales que se han cometido desde el Estado](https://archivo.contagioradio.com/estamos-ante-una-privatizacion-de-las-ffmm-al-servicio-de-las-multinacionales/)". A su vez afirmo que estas fuerzas han tenido una doctrina "**que tiene una ideología muy clara al de considerar a movimientos sociales, fuerzas de oposición, grupos de izquierda enemigos internos**".

Estos factores son determinantes para el senador y considera que son fundamentales para comprender por qué la Fuerza Pública no esta en condiciones para participar electoralmente. De otro lado Cepeda expone que el papel de las **Fuerzas Militares en el posconflicto debe ser otro a partir de la convivencia y un rol compatible con la paz**, que se dará con un proceso paulatino contrario al del senador Barreras.

<iframe src="http://co.ivoox.com/es/player_ej_12356884_2_1.html?data=kpegl5ucfJWhhpywj5WaaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjKjO1dnWs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
