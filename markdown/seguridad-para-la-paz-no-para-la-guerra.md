Title: Seguridad para la paz, No para la guerra
Date: 2019-10-09 16:52
Author: Foro Opina
Category: Opinion
Tags: diálogos paz, paz, seguridad
Slug: seguridad-para-la-paz-no-para-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/paz-y-seguridad-1-728.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**\*Por la Fundación Foro Nacional por Colombia**

 

El llamado hecho por **Iván Márquez** el pasado 29 de agosto para volver a la guerra tuvo un incuestionable impacto político. Colocó de nuevo en el centro del debate la implementación del Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera. Este llamado ha favorecido a los enemigos del Acuerdo y de la solución política negociada de la guerra, pero también dio origen a una reacción positiva por parte de las fuerzas defensoras de la paz, que se cohesionaron en la condena a quienes pretenden persistir en la guerra como medio para transformar los conflictos de la sociedad.

[(le puede interesar: La paz en Colombia es paz con participación o no es paz](https://archivo.contagioradio.com/?s=Fundaci%C3%B3n+Foro+Nacional+por+Colombia))

Estamos en un momento difícil en relación con el proceso de paz. Dificultades debidas en buena medida, a la falta de un compromiso político del gobierno para la adecuada implementación del Acuerdo Final, agravadas por la crítica situación de seguridad que se manifiesta en el persistente asesinato de líderes sociales y excombatientes de las **FARC-EP**.

En las condiciones de Colombia, así sea una perogrullada decirlo, seguridad y construcción de paz son indisociables. La guerra interna ha sido favorecida y alimentada por factores que se manifiestan en la precariedad de nuestra democracia, en las inequitativas estructuras económicas y sociales, en los niveles de pobreza, entre muchos otros. Transformar esos factores es construir paz y crear condiciones para la convivencia. La seguridad se construye a partir del fortalecimiento de la ciudadanía participante con capacidad de incidencia y del pacto social asociado a la creación de condiciones de vida digna para los pobladores. Y con la construcción de un Estado que goce de legitimidad.

Desde luego no es posible ignorar que las expresiones de violencia política tienen que ver con la seguridad, pero ésta no se agota en ellas. Entendemos que los cambios experimentados por la guerra y su economía política en las últimas décadas tejieron articulaciones con economía ilegales, formas de ejercicio de la política y regulación del poder, relaciones perversas entre sectores del Estado y la ilegalidad y expresiones diversas de violencia social que inciden de manera significativa en la seguridad.

El gobierno formuló en febrero de 2019 su *“Política de defensa y seguridad para la legalidad, el emprendimiento y la equidad”*, centrada únicamente en la violencia asociada a las economías ilegales, desconociendo otros factores que afectan la seguridad ciudadana. No articuló su política a la construcción de paz y a la implementación del Acuerdo Final en lo que respecta a tiene que ver con transformaciones que se traducirían en modificar las condiciones de seguridad. Se planteó como objetivo hacer frente a la que considera la principal amenaza a la seguridad: la ausencia de presencia estatal en regiones en las que se desarrollan economías ilegales *-narcotráfico y minería ilegal-* y operan grupos armados organizados como el **ELN**, **el Clan del Golfo**, **Los Pelusos**, entre otros. Para esto se propuso restablecer el principio de autoridad y el control territorial para lo cual se definió cinco ejes, fundamentos de la transformación estratégica que se espera alcanzar en materia de seguridad.

**Destacamos tres de ellos:**

seguridad cooperativa, mediante una diplomacia para la defensa que garantice aliados estratégicos; disrupción del delito, mediante la desarticulación de economías ilegales para privar de “combustible” a los grupos armados organizados y, en tercer lugar, el tránsito de control militar al control institucional de los territorios. El gobierno entiende la seguridad como una cuestión fundamentalmente de control territorial militar y de legalidad y deja de lado las necesarias transformaciones de la sociedad.

Para avanzar en el control territorial, el gobierno definió Zonas Estratégicas de Intervención Integral **(ZEII)** caracterizadas por la ausencia de Estado, economías ilegales y presencia de grupos armados organizados, abundancia de *“recursos hídricos, biodiversidad o medio ambiente”* o bien zonas de frontera. En esas **ZEII** serán desplegadas las acciones de recuperación del control territorial, establecimiento del principio de autoridad e imperio de la legalidad para garantizar la seguridad.

Se trata de la reedición de las estrategias del Plan Consolidación aplicado en el gobierno de Uribe Vélez. Un Plan de seguridad del Estado, no de seguridad ciudadana, que está siendo aplicado por unas fuerzas militares dirigidas por una cúpula cuestionada por violaciones a los derechos humanos y al derecho internacional humanitario, sacudidas por fenómenos de corrupción y con sectores que, al parecer, persisten en sus alianzas perversas con narcotraficantes y paramilitares. Fuerzas Militares que han experimentado una grave regresión en este gobierno respecto de lo que habían ganado en medio de la compleja negociación por poner fin al conflicto armado: revisar la doctrina militar y proponerse la construcción de una fuerza militar para la paz y no para la guerra.

La actual política de seguridad persiste en una concepción de seguridad y democracia bajo la tutela militar, que poco tiene que ver con la seguridad basada en transformaciones estructurales de la sociedad para garantizar condiciones de vida digna de los asociados y el fortalecimiento de la participación ciudadana.

###### \*La Fundación Foro Nacional por Colombia es una organización civil no gubernamental sin ánimo de lucro, creada en 1982 por iniciativa de intelectuales colombianos comprometidos con el fortalecimiento de la democracia y la promoción de la justicia social, la paz y la convivencia. El propósito de nuestro trabajo es crear las condiciones para el ejercicio de una ciudadanía activa con capacidad de incidencia en los asuntos públicos. El pluralismo, la participación ciudadana, la concertación democrática, la corresponsabilidad y la solidaridad son la base para el desarrollo de nuestra misión, con un enfoque diferencial (de género, generación y etnia). Desde sus inicios, Foro rechazó la violencia como forma de acción política. Por ello cobijó la propuesta de una salida negociada al conflicto armado y del fomento de una cultura democrática. Luego de la firma del Acuerdo entre el Gobierno y las FARC, Foro ha orientado su quehacer hacia el objetivo estratégico de contribuir a la construcción de la paz y la convivencia en Colombia. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).  
Contáctenos: 316 697 8026 –   (1) 282 2550 
