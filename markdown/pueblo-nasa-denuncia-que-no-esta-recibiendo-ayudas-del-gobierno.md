Title: Pueblo Nasa denuncia que no está recibiendo ayudas del Gobierno en Mocoa
Date: 2017-04-09 12:54
Category: DDHH
Tags: Avalancha en Mocoa, Ayudas para Mocoa
Slug: pueblo-nasa-denuncia-que-no-esta-recibiendo-ayudas-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/cruz-roja.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cruz Roja] 

###### [09 Abr 2017] 

Los integrantes del pueblo Nasa, han denunciado que al no **encontrarse reunidos en los cuatro albergues dispuestos por el gobierno, no están recibiendo ningún tipo de ayuda**. Esta comunidad está concentrada en la sede de la Consejería del Pueblo Nasa en Mocoa y de acuerdo con el censo habría **500 personas afectadas**.

El pueblo Nasa es una de las comunidades que más se ha visto afectada por la avalancha en Mocoa,  debido a que **la gran mayoría de sus hogares estaban construidas en las laderas de los ríos, en total son 170 familias** las que lo han perdido todo, entre los damnificados se encuentran niños, madres cabeza de hogar y adultos mayores.

Cristina Ulcue, integrante de la Consejería del Pueblo Nasa, señaló que ellos han recibido ayudas humanitarias por parte de organizaciones defensoras de derechos humanos, “como pueblo Nasa el **gobierno Nacional no se ha pronunciado sobre nuestra situación, no sabemos si es un trato diferencial,** hemos escuchado rumores pero hasta el momento no se han comunicado con nosotros”. Le puede interear: ["Lo más pobres las principales víctimas de la avalancha en Mocoa, Putumayo"](https://archivo.contagioradio.com/a-2-dias-de-la-avalancha-la-situacion-de-mocoa-sigue-siendo-critica/)

El pueblo Nasa, **está buscando juntar en la Consejería al total de personas afectadas que hacen parte de su comunidad, hasta el momento hay 220 personas allí reunidas**. Ulcue expresó que es importante tener en cuenta que el proceso para que las familias se restablezcan es largo, razón por la cual hizo un llamado a la solidaridad de las y los colombianos.

“Este proceso no es ni de uno ni de dos días, este proceso va para dos, tres meses o más, recién está empezando, no hemos recibido ayuda del gobierno, ni su pronunciamiento, **necesitamos saber que vamos a hacer con el pueblo Nasa afectado por esta avalancha**”. Le puede interesar:["Habitantes de Mocoa que ayudas no estarían llegando a personas fuera de los albergues"](https://archivo.contagioradio.com/habitantes-de-mocoa-denuncian-que-ayudas-no-estarian-llegando-a-personas-fuera-de-los-albergues/)

El pasado 8 de abril los mejores bailarines y campeones de Tango de la capital, ofrecieron un espectáculo para recolectar ayudas y enviarlas al pueblo Nasa, en total se recogieron aportes por **\$4.000.000 de pesos y donaciones en alimentos no perecederos, ropa y medicamentos, que serán enviados a Mocoa** con la ayuda de defensores de derechos humanos del a Comisión Intereclesial de Justicia y Paz. Le puede interesar: ["Peligro de epidemias deben ser contrarrestadas con urgencia en Mocoa"](https://archivo.contagioradio.com/peligro-de-epidemias-deben-ser-contrarrestados-con-urgencia-en-mocoa/)

<iframe id="audio_18060123" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18060123_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
