Title: Cine en los barrios, el otro Festival en el Festival
Date: 2019-03-07 16:54
Category: 24 Cuadros, Cultura
Tags: Cartagena, Cine, Ficci
Slug: cine-en-los-barrios-el-otro-festival-en-el-festival
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-07-at-4.40.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

Con "El piedra", opera prima del cartagenero Rafael Martínez Moreno, se dio apertura a Cine en los barrios, programa social del 59 Festival Internacional de cine de Cartagena, que **busca llevar gratuitamente producciones audiovisuales a sectores vulnerables en la periferia de la ciudad**.

La polvorosa cancha del barrio Canapote se convirtió en una sala de cine en la que hombres, mujeres, niños y abuelos de la comunidad, compartieron mientras acompañados por palomitas y gaseosa observaban las imágenes de sus propias calles y casas en la gran pantalla.

Mientras avanza la historia de Reinaldo Salgado, un boxeador que sustenta su diario vivir manejando un mototaxi por la ciudad, en el final de sus mejores días como pujilista, se escuchaban los comentarios de quienes se reconocen e identifican a sus vecinos "el entrenaba boxeadores en realidad" murmuraban entre risas y expresiones de asombro.

La cotidianidad de **"El piedra"** se ve alterada cuando Breyder, un niño de 12 años aparece en su puerta asegurando ser su hijo no reconocido, desestabilizando su lucha individual por sobrevivir al día a día y confrontando su propia existencia.

\[caption id="attachment\_62854" align="alignnone" width="800"\]![Proyección El Piedra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-07-at-4.05.15-PM-800x449.jpeg){.size-medium .wp-image-62854 width="800" height="449"} Proyección El Piedra\[/caption\]

**Giovanny Bustos, del equipo del Ficci** asegura que la sección es importante porque es la única que se realiza durante todo el año, y porque va mas allá del centro histórico llegando a los rincones más recónditos de la ciudad y el departamento de Bolívar, acercando el Festival a quienes en muchas ocasiones no tienen la posibilidad de ir a una sala de cine.

Del programa hacen parte seis mega proyecciones y nueve talleres comunitarios de realización para jóvenes y niños, fruto de los cuales se realizan productos audiovisuales propios, visita de directores, la ruta 90 y show car de cine.

**La coordinadora Ángela Bueno**, afirma que la importancia de la sección radica en que impacta a las comunidades a todo nivel, **"es una puerta que se abre de cultura para las comunidades que lo necesitan, es como llevarlos a viajar a soñar, para que ellos vean algo mas que no ven en su entorno"**, y eso lleva a varias transformaciones en sus vidas.

Se trata de no dejar de ver la parte internacional del Festival, que es muy importante, pero tener un poco de contacto con la comunidad de Cartagena y cual es la realidad que muchas personas no conocen, una que va mas allá de la ciudad amurallada y los espacios turísticos frecuentados habitualmente
