Title: Más de 400 familias desplazadas por guerra entre grupos ilegales en Córdoba
Date: 2019-03-26 13:32
Category: Comunidad, DDHH
Tags: Clan del Golfo, cordoba, Desplazamiento, Desplazamiento HidroItuango, FARC
Slug: familias-desplazadas-ilegales-cordoba
Status: published

###### [Foto: Archivo Contagio Radio] 

###### [26 Mar 2019] 

Desde el viernes pasado se viene presentando una difícil situación en materia de derechos humanos en el sur de Córdoba, causada por el conflicto entre Clan del Golfo y el nuevo Frente 18 que hasta el momento ha dejado más de 400 familias desplazadas, la desaparición de 2 líderes de sustitución de cultivos y el asesinato de otro más. (Le puede interesar: ["Denuncian desplazamiento de por lo menos 200 familias al sur de Córdoba"](https://archivo.contagioradio.com/cuerpo-campesino-desaparecido-cordoba/))

Según Andrés Chica, integrante de la Fundación Cordoberxia, desde 2016 diferentes bandas se están disputando el territorio, han instalado minas por los caminos de herradura y han prohibido navegar los ríos. La razón de esta disputa es la estratégica ubicación de la zona que conecta con el sur de Bolivar, el Golfo de Urabá y el Bajo Cauca Antioqueño.

A ello se suma la presencia de cultivos de uso ilícito, la posibilidad de hacerse con otras rentas ilegales y la oportunidad para los armados de refugiarse en el Parque Nacional Natural Nudo del Paramillo. Estas razones han llevado a que el **Clan del Golfo se ubique en la llanura del sur de Córdoba, mientras el nuevo Frente 18 lo haga en la cordillera**.

Según Chica, **en la zona también hacen presencia Los Caparrapos o Bloque Vigilio Peralta Arenas (grupo creado por una división con el Clan del Golfo), Cartel de Sinaloa y un frente de Guerra del Ejército Liberación Nacional (ELN)**. (Le puede interesar: ["Desde 2016 hasta 2018, 28 líderes han sido asesinados en Córdoba"](https://archivo.contagioradio.com/lideres-asesinados-cordoba/))

### **Los desplazamientos y desapariciones que no ha atendido el Gobierno** 

La situación de violación a los derechos humanos que se ha presentando desde 2016 tuvo un nuevo pico de violencia desde el viernes 22 de marzo, cuando el **Frente 18 mandó a desocupar 7 veredas en la parte alta de las montañas: 2 de Ituango y 5 de Puerto Libertador**; generando un desplazamiento de cerca de 160 familias, unas "400 personas entre jóvenes, niños y adultos", explicó Chica.

A esta situación de desplazamiento hay que sumar **la desaparición forzada de 3 líderes en sustitución de cultivos de uso ilícito**, que fueron sacados de su casa presuntamente por integrantes del Clan del Golfo. El integrante de Cordoberxia indicó que **"el cuerpo de uno de ellos fue encontrado dos días después brutalmente asesinado"**. Sobre estos casos, Chica señaló que las familias de los líderes están atemorizadas, porque después de dar los nombres de los líderes, son amenazadas y hay represalias por parte de los armados.

### **Comunidad sigue en medio de actores armados** 

Posteriormente, **el domingo 24 de marzo se produjo un nuevo desplazamiento masivo de unas 300 familias por parte del nuevo Frente 18,** pero que el Clan del Golfo obligó a permanecer en la zona alta para desobedecer la orden dada por su contrincante; situación que obligó a la población a permanecer en el municipio de Río Sucio hasta el momento. (Le puede interesar: ["Comunidades de Córdoba marchan contra asesinato de campesino a manos de Ejército"](https://archivo.contagioradio.com/comunidades-de-cordoba-marchan-contra-asesinato-de-campesino-a-manos-del-ejercito/))

Todas estas situaciones se presentan en el marco de una movilización campesina que se realiza desde el 28 de febrero en Córdoba, con la que campesinos le exigen al Gobierno Nacional que cumpla lo pactado en el marco del Plan Nacional Integral de Sustitución de cultivos de uso ilícito; y según la denuncia de Chica, ante la pasividad de las fuerzas armadas y civiles del Estado que "brillan por su ausencia" en el territorio.

> los campesinos del sur de Córdoba siguen comprometidos con la sustitución de cultivos de uso ilícito [@EmilioJArchila](https://twitter.com/EmilioJArchila?ref_src=twsrc%5Etfw) [\#ARCHILACUMPLALEALSURDECORDOBA](https://twitter.com/hashtag/ARCHILACUMPLALEALSURDECORDOBA?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/voUToyncbw](https://t.co/voUToyncbw)
>
> — Jose Ortega (@JoseOrt00781405) [26 de marzo de 2019](https://twitter.com/JoseOrt00781405/status/1110544005807394817?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_33750952" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33750952_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
