Title: En el Día Internacional de los Pueblos Indígenas ordenan desalojo de un resguardo
Date: 2017-08-09 15:11
Category: Ambiente, Nacional
Tags: indígenas, liberación de la madre tierra, Resguardo Indígena
Slug: en-el-dia-internacional-de-los-pueblos-indigenas-ordenan-desalojo-de-un-resguardo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/San-Antonio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: A la Luz Pública] 

###### [09 Ago. 2017] 

Diversas Organizaciones sociales de Antioquia, Caldas, Risaralda, Tolima y Quindío rechazaron a través de un comunicado la orden de desalojo en **contra de las comunidades indígenas que habitan el resguardo de San Antonio de Calarma** en el municipio de San Antonio en el Tolima, comunidades que llevan varios años realizando proceso de liberación de la madre tierra en el predio conocido como Las Delicias Holanda.

En la comunicación instan a las instituciones encargadas a no llevar a cabo el desalojo, que a todas luces **atenta contra las reclamaciones que hacen las comunidades al derecho al territorio.**

“Solicitamos apersonarse de este caso y como medida cautelar suspendan el operativo de desalojo por el derecho fundamental que esta comunidad tiene al territorio ancestral y a la integridad cultural”. Le puede interesar: [Trabajadores de INCAUCA serían responsables del asesinato de comunero indígena](https://archivo.contagioradio.com/incauca-responsables-del-asesinato-de-comunero-indigena-del-cric/)

Añaden que se debe realizar **un llamado a la fuerza pública con el objetivo de evitar cualquier acción violatoria** contra las comunidades “como ha sucedido en diferentes movilizaciones del movimiento popular desde el Paro de Buenaventura hasta el Paro de Mineros en Segovia y Remedios, reprimidos brutalmente por el ESMAD”. Le puede interesar: [Indígenas de Cauca denuncian hostigamientos por combates entre el ELN y Policía](https://archivo.contagioradio.com/indigenas-de-cauca-denuncian-hostigamientos-por-combates-entre-el-eln-y-policia/)

Por último, hacen un llamado a toda la población del departamento de Tolima, a las organizaciones de derechos humanos, al movimiento campesino e indígena a **movilizarse y acompañar a las comunidades del territorio de San Antonio de Calarma,** evitando el desplazamiento de niños, niñas, mujeres y ancianos que permanecen en esta zona.

“Es nuestro deber velar no solo por la defensa de la biodiversidad y los ecosistemas, sino de las comunidades que en nuestros territorios viven”. Le puede interesar: [8 comuneros han sido asesinados en proceso de liberación de la madre tierra en Cauca](https://archivo.contagioradio.com/8-comuneros-han-sido-asesinados-en-proceso-de-liberacion-de-la-madre-tierra-en-cauca/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
