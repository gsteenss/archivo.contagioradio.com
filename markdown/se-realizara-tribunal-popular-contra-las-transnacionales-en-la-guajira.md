Title: El Cerrejón en la mira del Tribunal Popular contra las Transnacionales en la Guajira
Date: 2015-10-01 16:00
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Chevron, Comité Guajira Resiste, crisis en la Guajira, DDHH, El Cerrejón, falta de agua, PDVSA, río Ranchería, Rioacha, tribunal permanente de los pueblos, Tribunal Popular contra transnacionales por la Guajira, Tribunal Russel
Slug: se-realizara-tribunal-popular-contra-las-transnacionales-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imagen promocional 

<iframe src="http://www.ivoox.com/player_ek_8733390_2_1.html?data=mZyglZiddI6ZmKiak5qJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8af08rOzs7epdOZpJiSo5aPmNPdw9rbw9GPlNDk1tHO1JDHs8%2Fo08aYzsbXb7XmwtPg0MbHrdDiwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Lozano, FEU Guajira] 

###### [1 Oct 2015] 

Los días 3 y 4 de octubre en Rioacha, se llevará a cabo el **Tribunal Popular contra las Transnacionales en la Guajira**, en el marco de la crisis ambiental y humanitaria  presentada históricamente en este departamento de Colombia.

José Lozano, integrante del equipo coordinador de la audiencia asegura que el Tribunal se centrará en la presencia de la empresa extractora de carbón “**El Cerrejón”, que ha cometido varias violaciones a los derechos humanos** de las comunidades, sin embargo, también se tendrá en cuenta las denuncias que realicen las comunidades frente a otras compañías como **Chvron, empresa petrolera, y  PDVSA.**

El Cerrejón es la multinacional con mayor presencia en la región, con 159 kilometros. Esta empresa **“representa el verdugo de las comunidades**”, como expresa Lozano. Las poblaciones indígenas y afrocolombianas denuncian que por la actividad de la compañía, las personas han sufrido afectación pulmonar, han sido desalojadas pese a las acciones de resistencia de las comunidades y aunque han interpuesto varias tutelas en contra de la compañía, esta manipula las demandas gracias a que tiene influencia directa en la parte administrativa y judicial del departamento.

Es por eso que “el Tribunal Popular contra las Transnacionales tiene como fin **someter a juicio**  bajo los estándares de verdad, justicia y reparación integral para las víctimas, **a los responsables de los flagelos cometidos contra el Pueblo Guajiro y la naturaleza**", afirma el Comité Guajira Resiste en su convocatoria dirigida a organizaciones de derechos humanos, ambientalistas, y a toda la población en general.

Esta iniciativa de acción pública no gubernamental, **inspirada en el Tribunal Russel y en el Tribunal Permanente de los Pueblos**, contará con dos momentos; en el primero, se desarrollarán ***acciones previas****, *consistentes en la **verificación y acciones humanitarias ante la afectación de la empresa minera El Cerrejón**, y en el segundo, se sesionará el *Tribunal* *Popular *donde se enjuiciará a los responsables de la violación a los derechos humanos de los Guajiros y a la naturaleza, contando con magistrados, personalidades del mundo intelectual y los derechos humanos".

De acuerdo a este panorama, la crisis que se presenta en la Guajira no se debería al fenómeno de "El Niño", sino que, en su mayoría, se trataría de **consecuencia de las políticas minero-energéticas establecidas por el gobierno colombiano**, la precariedad en el suministro y administración de agua a los habitantes, y la alteración natural de acuíferos superficiales de la empresa El Cerrejón para la extracción de carbón natural, que **ha desviado el Río Rachería y actualmente haría lo mismo con el arroyo Bruno, señala José Lozano.**

##### Vea aquí la convocatoria completa: 

<iframe id="doc_7457" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/278447066/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-dNwS8VXOLLx64VszAQBN&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
