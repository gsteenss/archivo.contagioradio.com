Title: Juana Perea, líder ambientalista es asesinada en Chocó
Date: 2020-10-30 11:26
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asesinato de lideresa, Departamento del Chocó, Lideresa ambiental
Slug: juana-perea-lider-ambientalista-es-asesinada-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/juana-pe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado miércoles 28 de octubre **fue asesinada la líder ambientalista colombo española, Juana Perea Plata** en el corregimiento de Termales, municipio de Nuquí, departamento del Chocó.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1321976076202029057","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1321976076202029057

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El cuerpo de la reconocida lideresa fue hallado frente a la estación de policía de Nuquí, arrojado en la playa. **El alcalde del municipio, Yefer Gamboa confirmó el asesinato y señaló que habría muerto por un disparo de arma de fuego en la cabeza.** (Le puede interesar: [Atentan contra el senador Feliciano Valencia](https://archivo.contagioradio.com/atentan-contra-la-vida-del-senador-feliciano-valencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según [el Instituto de Estudios para el Desarrollo y la Paz -Indepaz](http://www.indepaz.org.co/) –, se desconocen los autores de este asesinato y los hechos aún están siendo investigados por las autoridades, pues la lideresa residía en Termales, sin embargo, el cuerpo fue hallado en la cabecera municipal del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el alcalde afirmó que la seguridad en el municipio es un tema muy preocupante, debido a que **hay presencia de grupos armados ilegales que se asientan en el territorio para el tráfico de drogas pero poca protección por parte del Estado; Gamboa resaltó que solo había 9 policías para una comunidad de más de 6.000 habitantes**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Kathy Sutton, presidenta de la Liga de Surf del Chocó compartió que «Juana era como la mamá de todos, era una luz para la comunidad de la zona. Dónde la necesitaban ahí estaba». (Le puede interesar: [Carlos Navia líder social de Asocomunal en Cauca, es asesinado](https://archivo.contagioradio.com/carlos-navia-lider-social-de-asocomunal-en-cauca-es-asesinado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Juana Perea era opositora de la construcción del puerto de Tribugá y actualmente estaba desarrollando la construcción de un hotel ecoturístico en el corregimiento de Termales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
