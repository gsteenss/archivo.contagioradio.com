Title: Los guiños de Duque para construir paz en Colombia
Date: 2018-08-08 14:45
Category: Paz, Política
Tags: ELN, Iván Cepeda, Iván Duque, paz
Slug: los-guinos-de-duque-para-construir-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz a la Calle] 

###### [08 Ago 2018] 

Cómo un hecho histórico calificó el senador del Polo Democrático, Iván Cepeda, la movilización del pasado 7 de agosto en más de 81 ciudades del país que hicieron un llamado al presidente Iván Duque, durante su posesión como primer mandatario, para que continué los esfuerzos por construir la paz en Colombia.

Algunas de las afirmaciones hechas por Duque, de acuerdo con Cepeda, podrían tener un visto bueno hacia extender las conversaciones de paz con la guerrilla del ELN, debido a que el presidente anunció que dará un **plazo de 30 días para que su equipo analice el estado actual de la mesa y defina si proseguir o no con la misma.**

Asimismo, señaló que Duque debe tener en cuenta no presionar con las propuestas de concentración de tropas o cese unilaterales del ELN, debido a que podría echar a tras los avances que se tienen hasta el momento. De igual forma, la mención de un pacto concertado con todas las fuerzas políticas, según Cepeda **“puede ser un espacio para abrir un diálogo, ojalá constructivo, sobre los temas esenciales del país”.**

Sin embargo, del otro lado está el temor hacia las modificaciones que ya ha anunciado el presidente y el Centro Democrático, harían al Acuerdo de Paz con la FARC, en temas puntuales como el narcotráfico. Sumado a ellos, Cepeda manifestó que también discierne por completo en las políticas económicas y educativas que pretende poner en marcha el gobierno de Duque. (Le puede interesar: ["Colombia y sus dos presidentes: análisis de Fernando Giraldo"](https://archivo.contagioradio.com/colombia-y-sus-dos-presidentes-analisis-de-fernando-giraldo/))

<iframe id="audio_27705954" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27705954_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
