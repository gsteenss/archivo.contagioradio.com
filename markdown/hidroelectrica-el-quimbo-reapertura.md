Title: Juez de Huila ordena reapertura de "El Quimbo"
Date: 2016-01-08 14:03
Category: Ambiente, Nacional
Tags: El Quimbo, Hidroeléctricas
Slug: hidroelectrica-el-quimbo-reapertura
Status: published

###### 8 Ene 2016 

[  
J]uez del Huila falla a favor del Ministerio de Minas y la Autoridad Nacional de Agricultura la tutela que habrían interpuesto contra la medida de cierre de la **hidroeléctrica ‘El Quimbo’** y ordena su reapertura inmediata y temporal, argumentándose en la necesidad de hacer frente a las condiciones de sequía y de desabastecimiento de energía por las que atraviesa el país, bajo el supuesto de que abrir la represa aumentaría el nivel del Río Magdalena y evitaría pérdidas ambientales y un eventual reajuste en las tarifas de energía.

Pero los argumentos del Gobierno nacional en cabeza de Juan Manuel Santos, contrastan con las denuncias emitidas por pobladores sobre los efectos tanto sociales como ambientales que resultan de la actividad de la hidroeléctrica, poniendo en tela de juicio el que reabrirla sea la solución más acertada para enfrentar el recrudecimiento del ‘Fenómeno del Niño’ en el país, que ha llevado entre otras, a que en los últimos días el Río Magdalena reporte 30 cms de profundidad en puntos donde los niveles eran de por lo menos 1.40 metros.

Pese a que el fallo judicial es transitorio y autoriza a Emgesa que reanude actividades en ‘El Quimbo’ hasta que el Tribunal Contencioso Administrativo del Huila decida sobre el levantamiento o no de la medida cautelar ya decretada, las comunidades afectadas continúan alertando sobre la contribución de estos megaproyectos hidroeléctricos en el cambio climático y en las afectaciones ambientales y económicas que recaen en las zonas en las que éstos son implementados.

\[embed\]https://www.youtube.com/watch?v=sggjDwRyWO8\[/embed\]

Vea también:[¿Realmente se necesita El Quimbo en pleno fenómeno del niño?](https://archivo.contagioradio.com/realmente-se-necesita-el-quimbo-con-pleno-fenomeno-de-el-nino/)
