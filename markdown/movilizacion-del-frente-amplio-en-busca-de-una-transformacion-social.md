Title: Movilización del Frente Amplio en busca de una transformación social
Date: 2015-09-29 08:36
Category: Comunidad, yoreporto
Tags: ADISPA, Frente Amplio por la PAz, marcha en Putumayo, Perla Amazónica, Putumayo
Slug: movilizacion-del-frente-amplio-en-busca-de-una-transformacion-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/adispa-movilizacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Puerto Asís, Putumayo, 27 de Septiembre de 2015 

###### **BOLETÍN INFORMATIVO No. 32** 

**Comunidad del departamento Putumayo se moviliza por el Frente amplio por la Unidad, la Democracia y la Paz con Justicia Social**

El Frente Amplio conformado por diferentes fuerzas políticas, movimientos sociales y populares lideró en Puerto Asís (Putumayo) una gran marcha de lanzamiento de las candidaturas a Gobernación, Asamblea, Alcaldías y Concejos Municipales, quienes participaran en las próximas contiendas electorales.

En una multitudinaria marcha por las principales calles de Puerto Asís, hicieron presencia comunidades campesinas, indígenas, y afrodescendientes, asi como diferentes fuerzas políticas, movimiento sociales y populares del departamento del Putumayo, entre ellos: la Mesa Regional de Organizaciones Sociales, Marcha Patriótica Putumayo, Mesa de víctimas, Mesa Departamental de Población Desplazada, Asociación de Educadores del Putumayo, los partidos políticos Alianza Verde, Movimiento alternativo indígena y Social, Partido Alianza Social Independiente, Sector Juvenil Regional, Pueblo Nasa, Federación de Comunidades Afrodescendientes del Putumayo, entre otros.

[![adispa 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/adispa-2.jpg){.aligncenter .size-full .wp-image-14898 width="698" height="390"}](https://archivo.contagioradio.com/movilizacion-del-frente-amplio-en-busca-de-una-transformacion-social/adispa-2/)La Zona de Reserva Campesina Perla Amazónica representada por la Asociación de Desarrollo Integral Sostenible Perla Amazónica (Adispa) hizo presencia con su comunidad y con su candidato al Concejo Municipal, el líder comunal Orlando Díaz.

[Al finalizar la marcha los participantes se concentraron en Instalaciones de Polideportivo del Barrio San Nicolás donde diferentes líderes del nivel regional y nacional se pronunciaron sobre la importancia del Frente Amplio en busca de una transformación social que beneficie a indígenas, campesinos, afrodescendientes y comunidad en general.]

Wilmar Madroñero, vocero del Frente Amplio manifestó que a través de este espacio, se busca que este 25 de octubre se materialice los sueños que en unidad se han forjado al calor de la lucha social por la defensa del territorio.

[Por su parte, Yule Anzueta, vocero tambien del frente amplio, destacó que en los últimos años Colombia se ha caracterizado por un ascenso de la movilización social y articulación de las dinámicas sociales y políticas, las cuales se han visto estimuladas por un clima favorable de los acontecimientos democráticos y progresistas de américa latina.]

De otra parte manifestó las razones por las cuales las organizaciones sociales y partidos alternativos del departamento del Putumayo acordaron participar en los escenarios políticos electorales, impulsadas principalmente por los resultados de las políticas ejecutadas durante varias décadas por los gobiernos de la dirigencia tradicional que han conducido al país a niveles extremos de dependencia, concentración de riqueza, injusticia social, narcotráfico, guerra, paramilitarismo, violencia, sometimiento político, represión y degradación de la naturaleza y el principal el cáncer de la corrupción.

[![adispa 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/adispa-3.jpg){.aligncenter .size-full .wp-image-14899 width="629" height="351"}](https://archivo.contagioradio.com/movilizacion-del-frente-amplio-en-busca-de-una-transformacion-social/adispa-3/)

[El objetivo de la jornada además de respaldar a los candidatos de Gobernación, Asamblea, Alcaldías y Concejos Municipales, fue el de firmar unos compromisos por parte de la Candidata a la Gobernación Sorrel Aroca y demás candidatos  para con la comunidad del departamento de Putumayo.  ]

Para ello se dio lectura al Acuerdo político en el cual se establece que quienes representan el frente amplio en la actual contienda electoral asumirán la responsabilidad y el compromiso bajo diferentes criterios, los cuales se enfoca en que el accionar político del frente amplio sea una lucha incanzable por lograr la paz con justicia social, la verdadera democracia y la movilización social.

**Se destacan compromisos como:**

-   [Defensa, promoción y protección de los derechos humanos y derecho internacional humanitario.]
-   [Promover la consecución de la paz y la solución política al conflicto social y armado]
-   [Impulsar y garantizar a las comunidades la participación en la construcción de la asamblea nacional constituyente.]
-   [Fomentar, garantizar y aplicar programas que construyan políticas públicas hacia una democracia participativa, popular y comunitaria.]
-   [Construir e impulsar planes de desarrollar respondan a los intereses del pueblo, en espacios amplios, públicos y asamblearios.]
-   [Apoyo a iniciativas y propuestas de las comunidades y organizaciones  para la sustitución gradual y concertada de los ingresos derivados del cultivo de la coca y se buscara su implementación como política pública.]

Sorrel Aroca, se comprometió con las comunidades para trabajar en unidad y con la representación de todo el departamento para transformar y mejorar las condiciones de vida de las comunidades, convencida que las políticas públicas deben ser construidas y concertadas en espacios asamblearios de participación de las comunidades y manifestó que mas que un documento firmado era algo que se llevaba en el corazón y hace un llamado a todas las fuerzas políticas, movimientos sociales y populares, que se identifiquen y quieran unirse a esta propuesta.

***Grupo de comunicaciones Adispa.   E-mail:adispa2009@hotmail.com - comunicadores CONPAZ***
