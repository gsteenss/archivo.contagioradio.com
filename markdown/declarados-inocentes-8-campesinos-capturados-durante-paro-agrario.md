Title: Declarados inocentes 8 campesinos capturados durante paro agrario
Date: 2015-05-15 16:05
Author: CtgAdm
Category: DDHH, Nacional
Tags: Alexander Pianda, José Harry Rendón, Juan Clímaco Vega, Luis Efrén Fajardo, montaje judicial, os campesinos Mario Vilora, Oswaldo Arcos, Paro Agrario, Putumayo, red de derechos humanos, Sabino Eduardo Castro, Wilmar Madroñero, Yuri Quintero
Slug: declarados-inocentes-8-campesinos-capturados-durante-paro-agrario
Status: published

###### Foto: Notifronteras.com 

<iframe src="http://www.ivoox.com/player_ek_4502426_2_1.html?data=lZqdlJmWeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiMbXzcbfw8nTt4zdz9TQx9PYqdSfmZDQw9LUqdTdz9TgjcjFtNXp08bR0diPqNbmwtPhx5DUpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yury Quintero, Red de Derechos Humanos del Putumayo] 

###### [15 may 2015] 

Los 8 campesinos miembros de la organización ASCAP habían sido capturados en el marco del Paro Agrario realizado en el año 2013, acusados de "rebelión". Sin embargo, la defensa a cargo del Comité Permanente por la Defensa de los Derechos Humanos y la Red de Derechos Humanos del Putumayo, logró demostrar que las acusaciones de la Fiscalía se basaban en montajes judiciales.

Los campesinos Mario Vilora, Juan Clímaco Vega, Alexander Pianda, Sabino Eduardo Castro, Luis Efrén Fajardo, José Harry Rendón, Oswaldo Arcos y Wilmar Madroñero, fueron puestos en libertad de manera inmediata.

<div>

"Estamos convencidos de que estas capturas hacen parte de una campaña por desestabilizar los proceso organizativos en Colombia, y que así como sucede en el Putumayo, sucede en otros departamentos del país", declara Yury Quintero, de la Red de Derechos Humanos.

Hay que recordar que según el informe presentado por la Coalición Larga Vida a las Mariposas, de los 10 mil prisioneros políticos que existen en Colombia, cerca del 80% son líderes y lideresas sociales que nunca han portado un arma.

Para las organizaciones defensoras de derechos humanos, el Estado colombiano habría cambiado sus tácticas criminales, reemplazando las ejecuciones extrajudiciales por capturas masivas que desestabilizan a las organizaciones en resistencia. En el departamento del Putumayo, los líderes judicializados hacen parte en su mayoría de FENSUAGRO y ANZORC.

El caso se repite una vez más con la captura de 5 campesinos el mismo jueves 14 de mayo en la Vereda Arabia, del Putumayo: 1 familia compuesta por padre, madre y sus dos hijos; y un campesino cabeza de familia, responsable de su esposa e hijos en condición de discapacidad.

Las comunidades han rodeado a las familias víctimas de los montajes judiciales, mientras la Red de Derechos Humanos se organiza para solicitar que la audiencia se realice lo más pronto posible, de ser el caso, se decrete prisión domiciliaria como uno de los beneficios jurídicos más inmediatos. "Son inocentes y vamos a demostrarlo, estoy convencida y meto las manos al fuego por estos compañeros", asegura Quintero.

</div>
