Title: Madres de la Plaza de Mayo conmemoran 40 años de lucha digna
Date: 2017-05-02 12:48
Category: El mundo, Movilización
Tags: Argentina, desaparecidos, dictadura militar, Madres de la Plaza de Mayo
Slug: madres-de-la-plaza-de-mayo-conmemoran-40-anos-de-lucha-digna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/madres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Facebook de Silva Bigna] 

###### [02 May. 2017] 

Hace 40 años, un 30 de abril de 1977, cerca de 14 madres decidieron unirse a Azucena Villaflor y a Mirta Acuña de Baravalle en la Plaza de Mayo de Argentina, para exigir que sus hijos e hijas detenidas y posteriormente desaparecidas regresaran con vida a sus hogares. **Han sido 40 años de caminar, de recordar, de exigir, de perseguir la justicia, de insistir.**

A propósito de esta fecha, las madres de la Plaza de Mayo – Línea Fundadora se reunieron en el monumento a Belgrano frente a la Casa Rosada, este domingo, en donde, acompañadas de muchos amigos, amigas, simpatizantes y personas parte de esta lucha que se convirtió en colectiva, **recordaron a los más de 30 mil desparecidos de la dictadura militar.**

**Mirta Acuña de Baravalle madre de la Plaza de Mayo - Línea Fundadora**, aseguró en Contagio Radio que este evento de conmemoración "fue un día de recuerdos de los más de 30 mil desaparecidos, fue un día muy emocionante, un día se puede decir, feliz, de recordar junto con todas las amigas y amigos”. Le puede interesar: [Madres de la Plaza de Mayo vuelven a ser víctimas del Sistema Judicial de Argentina](https://archivo.contagioradio.com/ordenan-captura-sobre-hebe-de-bonafini-titular-de-madres-de-plaza-de-mayo/)

En el evento también se realizó un mural con los rostros de los desaparecidos, para conmemorar así su memoria “era necesario rescatarlos, saber dónde estaban, por qué se los habían llevado, qué hicieron. **Siempre ha persistido la esperanza, nos concentramos y hasta el día de hoy, pensamos que los volveremos a ver” dice Mirta.**

Fue un día en el que siguieron recordando a sus hijos e hijas, arrebatados de la vida por la dictadura militar de la época. Con voz entrecortada Mirta dice que “uno no se imagina verlos correr de chiquitos y pensar que cuando sean más grandes alguien puede hacerles tanto daño (…) **nosotros seguimos pensando en nuestros hijos, en los horrores que vivieron, en mi caso, pienso en mi hija, embarazada, en cómo fue a dar a luz”.**

Daño que se ha convertido en fuerza para seguir manteniéndolos en la memoria y buscando sin cesar a los nietos que ahora andarán por todo el país, a los hijos, que fueron arrebatados de los vientres de su madre de maneras indescriptibles y dolorosas **"aún falta un integrante en mi familia, mi nieto, que nació en cautiverio" añade Mirta.** Le puede interesar: ["Son 30 mil" la consigna en el día de la memoria Argentina](https://archivo.contagioradio.com/argentina-desparecidos-memoria/)

Estas madres, que han luchado desde muy jóvenes y para quienes luego de tantos años siguen esperando verdad, justicia y castigo a los asesinos la voz se entrecorta al recordar **"son cuarenta años, pero para mí es como si fuera ayer** (…) tenemos que estar alerta que la historia de hace 40 años, no se vuelva a repetir"** **recalca Mirta.

Finalmente, Mirta manifestó que "**no queremos reconciliarnos con los responsables de tanto dolor**, los responsables de los asesinatos en Argentina (…) el que tiene que perdonar es el ausente, yo no podría perdonar a los causantes del dolor de mis hijos”.

<iframe id="audio_18463713" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18463713_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
