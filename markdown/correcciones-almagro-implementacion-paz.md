Title: Las tres correcciones a Luis Almagro sobre la implementación de la Paz en Colombia
Date: 2019-06-26 15:15
Author: CtgAdm
Category: Paz, Política
Tags: Almagro, Defendamos la Paz, Duque, OEA
Slug: correcciones-almagro-implementacion-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Luis-Almagro-y-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @Almagro\_OEA2015  
] 

Por medio de una carta, el movimiento ciudadano Defendamos la Paz pidió al **secretario general de la Organización de Estados Americanos (OEA) Luis Almagro,** que haga una "aproximación más objetiva y realista a la paz en Colombia", luego de que afirmara que el presidente Iván Duque está comprometido con la implementación del proceso de paz. En la Misiva, los firmantes presentan tres argumentos en los que rechazan la afirmación de Almagro, recordando las objeciones presidenciales a la JEP, la búsqueda por volver a usar glifosato en la fumigación de cultivos de uso ilícito y el asesinato de excombatientes de las FARC.

### **¿Duque ha hecho lo necesario para implementar el Acuerdo como dice Almagro?**

En su declaración "Colombia y Paz", emitida en el marco del 49° sesiones de la Asamblea General de la OEA que se realizará en Medellín, Almagro sostiene que está complacido con el **"compromiso del gobierno del presidente Iván Duque con la paz"**. No obstante, como lo referenció **el senador Iván Cepeda, integrante de Defendamos la Paz,** la declaración contradice los hechos, así como los propios documentos de su organización.

De acuerdo a Cepeda, Duque no ha querido implementar la totalidad del Acuerdo, por el contrario, sus actuaciones en ese tema han sido de carácter selectivo; de igual forma, tampoco ha destinado los recursos necesarios en la implementación, hecho que se denunció cuando se presentó el Plan Nacional de Desarrollo y cuyos recortes afectaron a la Comisión para el Esclarecimiento de la Verdad y la Unidad de Búsqueda de Personas dadas por Desaparecidas.

En la misiva, los firmantes señalan que **el Presidente no ha hecho "todo por profundizar la paz con justicia", pues hizo oposición férrea a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP),** desafiando así una decisión de la Corte Constitucional sobre este tema. También manifestaron que no es cierto que el Acuerdo de Paz haya habilitado el crecimiento de los cultivos de uso ilícito, haciendo insostenible la implementación del punto 4, sobre este tema.

En cambio, criticaron que el Gobierno haya suspendido el pago de los convenios alcanzados con las familias que se acogieron al Plan Nacional Integral de Sustitución (PNIS), y se esté pensando en volver a usar Glifosato para solucionar el aumento de cultivos de Coca. A ello se sumó la **crítica ante el aumento de asesinatos de excombatientes de las FARC que se acogieron al proceso de paz y de líderes sociales,** frente los que el Gobierno no está respondiendo como se esperaría.

### **"Esperamos una aproximación más objetiva y más realista a la paz en Colombia"**

Cepeda explicó que probablemente **las declaraciones de Almagro están en la vía de respaldar un Gobierno con el que se ha mostrado cercano en anteriores ocasiones**; sin embargo, resaltó el trabajo de Defendamos la Paz, en ser una propuesta social, comunicativa y diplomática que puede reaccionar rápido y con un peso importante ante este tipo de asuntos. (Le puede interesar: ["Defendamos la Paz: Tejiendo esperanza desde los territorios"](https://archivo.contagioradio.com/defendamos-la-paz/))

Para concluir, Cepeda resaltó que en el debate con el Gobierno sobre cómo se debe implementar el Acuerdo de Paz se debe acudir a todos los escenarios, especialmente los internacionales, razón por la que asistirán al encuentro de las OEA para pedir que se siga apoyando el proceso de paz en Colombia; al tiempo que desde Defendamos la Paz se pidió a Almagro que haga **"una aproximación más objetiva y más realista a la paz en Colombia".**

> Carta a la [@OEA\_oficial](https://twitter.com/OEA_oficial?ref_src=twsrc%5Etfw) | [@DefendamosPaz](https://twitter.com/DefendamosPaz?ref_src=twsrc%5Etfw) expresa desacuerdo a Luis Almagro ([@Almagro\_OEA2015](https://twitter.com/Almagro_OEA2015?ref_src=twsrc%5Etfw)) por su declaración “Colombia y la Paz”[\#OEAApoyeLaPaz](https://twitter.com/hashtag/OEAApoyeLaPaz?src=hash&ref_src=twsrc%5Etfw)
>
> Vea la Carta completa en: <https://t.co/gZeOf2SkRN><https://t.co/jhFMoAXQyW> … [pic.twitter.com/SfWzq9VuX4](https://t.co/SfWzq9VuX4)
>
> — DefendamosLaPazColombia (@DefendamosPaz) [26 de junio de 2019](https://twitter.com/DefendamosPaz/status/1143837708919418881?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37648359" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37648359_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
