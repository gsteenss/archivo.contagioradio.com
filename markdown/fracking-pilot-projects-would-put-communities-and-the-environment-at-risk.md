Title: Fracking pilot projects would "put communities and the environment at risk"
Date: 2019-09-26 15:23
Author: CtgAdm
Category: English
Tags: fracking, Magdalena Medio, pilot projects
Slug: fracking-pilot-projects-would-put-communities-and-the-environment-at-risk
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Archive] 

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[On Sept. 17, Colombia’s top administrative court announced that their previous decision to maintain a temporary suspension of regulations needed to move forward with natural gas extraction using the fracking technique does not impede the realization of pilot fracking projects. ]

[This clarification provoked reactions from groups such as the Alliance Colombia Free of Fracking, which said that “this ‘pilot’ action means to risk the environment and the health of the communities” while the Minister of Mines and Energy María Fernanda Suárez insisted that “saying no to the pilot projects is to say no to science.”]

[The pilot projects will start initially in the Magdalena Medio region, but could expand to regions such as the Cesar Basin, Catatumbo, Amagá Antioquia, Putumayo, the Cordillera Oriental and the Boyacá savannahs, located near the Santander province.]

[This pilot project consists of a 3-kilometer, vertical perforation of the ground to inject pressurized water with chemicals, used to create fragments in the bedrock. Oscar Sampayo, a member of the environmentla group Alliance Colombia Free of Fracking, assured that the pilot project is an experiment that will put the environment and the communities at risk. “This is what seems illogical to us, given that in the world many studies have showed that this \[technique\] causes terrible damage to the earth.”]

[These projects could take at least one to two years to be completed while the results will be available within three to four years. But Sampayo insisted that the damage to the regions would evident as soon as the first machine begins the exploration phase.]

[Still, Sampayo clarified that fracking in Colombia is still unauthorized and that the pilot projects could take one to two years to initiate. “The top administrative court says it is following the recommendations of the commission of experts, and we ask ourselves if they are also following the other, eight points that the commission said are necessary to implement prior to the launch of the pilot projects,” said Sampayo.]

[One of the recommendations is the creation of an economic baseline that includes the participation of the communities and their approval of the projects. "We want this decision to consider the voices of the communities and that this not be the imposition of two or three technocrats or businessmen, interested in the exploitation of the regions," Sampayo said.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
