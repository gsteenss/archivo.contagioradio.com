Title: Hoy se premia labor de Defensores y Defensoras de Derechos Humanos en Colombia
Date: 2015-09-09 10:50
Category: DDHH, Movilización
Tags: COSPAC, Cuarto premio defensores de derechos humanos, Diakonia, la Asociación Campesina del Valle del Río Cimitarra y el Proceso de Comunidades Negras, Recompas
Slug: hoy-se-premia-labor-de-defensores-y-defensoras-de-derechos-humanos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/colombiahumanrightsaward2013.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Diakonia.com 

###### [9 sep 2015]

En el marco de la conmemoración del día de los derechos Humanos en Colombia, la Fundación Diakonia entrega **el cuarto Premio Nacional a los Defensores y Defensoras de derechos humanos en el país,** evento que se desarrolla** en el **Centro de Memoria, Paz y Reconciliación.****

De acuerdo con Cesar Grajales, Director de Diakonia en Colombia, a pesar de ser entregados de manera individual los reconocimientos “**expresan el trabajo de regiones, de grupos humanos y la expresión de trabajos comunitarios**”.

Según el informe “Los nadies” del programa “Somos defensores” entre enero y junio de 2015, fueron **registradas 399 agresiones individuales contra defensores y defensoras de DDHH**, razón por la cual el premio “**es una acción pertinente para levantar la legitimidad política y el trabajo que realizan los defensores de derechos humanos**”, indica Grajales.

El evento transmitido en Directo por Canal Capital desde el Centro de Memoria, Paz y Reconciliación se desarrolla en un ambiente “emotivo y alegre”, y sirve como homenaje a las personas quienes más allá del premio consideran que “**han cumplido con un deber ético político y humano**”.

### **Categorías y nominados:** 

**Defensor o Defensora del año:** Los nominados en esta categoría son Francia Elena Márquez Mina, Monseñor Héctor Epalza Quintero, Presbítero Adriel José Ruíz Galván, José Alfredy Galvis Torres.

**Proceso Colectivo del Año:** Los nominados son Mujeres Caminando por la Verdad, Red de Consejos Comunitarios del Pacífico Sur –Recompas-, la Asociación Campesina del Valle del Río Cimitarra y el Proceso de Comunidades Negras – Palenque Buenaventura -.

**Proceso Colectivo a nivel ONG: **Los nominados son Colombia Diversa, Diócesis de Tumaco, Corporación Social para la ayuda y capacitación – COSPAC – y la Fundación Forjando Futuros

**Toda una Vida:** Los nominados son Fabiola Lalinde, Venus Carrillo, Benjamin Cardona y Emma Doris López.

**Toda una Vida en el nivel de Organizaciones**: Está nominado el Consejo Comunitario Mayor de la Asociación Campesina del Atrato –COCOMACIA-, Corporación Vamos Mujer, Comité Permanente por la Defensa de los Derechos Humanos y el Colectivo 16 de Mayo.

Ver también** **[En 2015 se han duplicado las agresiones contra Defensores de Derechos Humanos](https://archivo.contagioradio.com/ataques-contra-defensores-de-derechos-humanos-se-han-duplicado-en-2015/)
