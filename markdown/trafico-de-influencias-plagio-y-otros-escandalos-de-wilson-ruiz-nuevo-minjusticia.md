Title: Tráfico de influencias, plagio y otros escándalos de  Wilson Ruíz nuevo Minjusticia
Date: 2020-09-17 12:52
Author: AdminContagio
Category: Actualidad
Tags: Ministro de Justicia, Minjusticia, Wilson Ruíz
Slug: trafico-de-influencias-plagio-y-otros-escandalos-de-wilson-ruiz-nuevo-minjusticia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Wilson-Ruiz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este miércoles, **el presidente Iván Duque anunció el nombramiento del abogado Wilson Ruiz, como nuevo Ministro de Justicia, en reemplazo de Margarita Cabello**, quien fue recientemente elegida como nueva Procuradora General de la Nación. El nuevo Ministro ha sido seriamente cuestionado por diversas causas a lo largo de su carrera. (Lea también: [Las críticas tras la elección de Margarita Cabello en Procuraduría](https://archivo.contagioradio.com/las-criticas-tras-la-eleccion-de-margarita-cabello-en-procuraduria/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1306367235649396737","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1306367235649396737

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En el periodo del  exprocurador Alejandro Ordoñez, **Wilson Ruíz se desempeñó como procurador delegado ante el Consejo de Estado y se vio implicado en casos de tráfico de influencias para acceder a cargos en la Procuraduría.** En este caso, habría sido identificado en [interceptaciones telefónicas](https://www.youtube.com/watch?v=dYbCJeJd_CM), ofreciendo puestos a Carlos Fernández, alias “El Papi”, quien después fue sancionado por estos hechos. En ese mismo periodo **como procurador delegado, también se vio envuelto en un escándalo de presunta compra de fallos en el Consejo de Estado, órgano ante el cual ejercía sus funciones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Wilson Ruiz, aspiró a ser Defensor del Pueblo pero fracasó en ese intento; aspiró a ser también consejero de Estado pero fue acusado de plagio por haber tomado casi en su totalidad una tesis sobre responsabilidad médica estatal realizada por otra persona, lo cual le impidió acceder a esa magistratura. Según el columnista Ramiro Bejarano, esa no sería la única vez que Ruíz se vio implicado en un caso de plagio, porque también fue acusado por hechos similares en la Universidad Pompeu Fabra de Barcelona.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RamiroBejaranoG/status/1306365452004872193","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RamiroBejaranoG/status/1306365452004872193

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Todo esto deja serias dudas sobre el funcionario que está llamado a liderar la reforma estructural a la Justicia que propone el Gobierno en el Congreso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Wilson Ruíz llega a Minjusticia por pactos políticos

<!-- /wp:heading -->

<!-- wp:paragraph -->

La última aspiración de Wilson Ruíz fue a la Procuraduría General, donde no obtuvo ningún voto por parte del Senado en la elección en la que fue nombrada Margarita Cabello. Este nombramiento retrata lo que se ha denominado como «puerta giratoria» en los altos cargos del Estado, en especial si se tiene en cuenta que Ruíz había sido magistrado del Consejo Superior de la Judicatura en donde incluso llegó a ocupar la presidencia de ese Alto Tribunal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, su presencia en la terna por la Procuraduría también ha dejado entrever que tanto el nombramiento de Cabello como Procuradora, como el de Ruíz en el Ministerio de Justicia; hicieron parte de una **[componenda política](Demandan%20pérdida%20de%20investidura%20en%20contra%20de%20Arturo%20Char) entre los Partidos Conservador, Centro Democrático, Cambio Radical, la U, Liberal** y otros que han sido afines al actual Gobierno; lo que ha sido calificado por muchos sectores políticos y sociales como una estrategia de cooptación de todos los poderes del Estado por parte de este Gobierno.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
