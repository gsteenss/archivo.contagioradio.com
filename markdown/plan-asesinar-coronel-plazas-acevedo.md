Title: Amenazan a Coronel (r) Plazas Acevedo postulado a la JEP
Date: 2017-11-06 23:03
Category: DDHH, Entrevistas
Tags: Jaime Garzon, JEP, mapiripan, plazas acevedo, rito alejo del rio
Slug: plan-asesinar-coronel-plazas-acevedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/coronel-acevedo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [06 Nov 2017]

Según la denuncia, la hija del Coronel ® Plazas Acevedo fue interceptada hace pocos días por varios  hombres que aseguraron que en próximas fechas el Coronel sería trasladado de la cárcel “Picota” en Bogotá a la cárcel la Dorada en Caldas y que allí sería asesinado. Lo más preocupante para sus abogados es que e**ste 5 de Noviembre se ordenó el traslado del militar al establecimiento penitenciario que había sido anunciado por los desconocidos.**

En la denuncia que llegó a través Guillermo Rico a las redes sociales de nuestra emisora, el equipo de abogados del ex militar han hecho todo lo posible para evitar el traslado puesto que los procesos en los que colabora se llevan en Bogotá, además porque en las amenazas que se hacen contra Plazas Acevedo y su familia siempre reiteran que "**le harán pagar por la colaboración que está brindando"**

El coronel retirado ha afirmado su colaboración en casos como el del asesinato de Jaiime Garzón, la masacre de Mapiripán, el asesinato de los investigadores del CINEP Mario Calderón y Elsa Alvarado y el pasado 8 de Septiembre solicitó su inclusión a la secretaria ejecutiva de la Jurisdicción Especial del Paz. El mismo día en que se firmó y entregó el acta a la JEP se aseguró que la vida del militar corría alto riesgo. [Lea también: Coronel Plazas Acevedo será investigado por la masacre de Mapiripán ](https://archivo.contagioradio.com/coronel-r-plazas-acevedo-sera-procesado-por-presunta-responsabilidad-en-la-masacre-de-mapiripan/)

### **Las brigadas militares de las que hizo parte el Coronel Plazas Acevedo** 

Plazas Acevedo hizo parte de varias unidades militares involucradas en grandes escándalos, una de ellas la Brigada de Inteligencia XX con sede en Bogotá, vinculada a persecución sistemática contra defensores y defensoras de DDHH, así como magnicidios como el de Jaime Garzón. [Lea también: Don Berna vincula a 3 militares con las AUC](https://archivo.contagioradio.com/don-berna-vincula-a-3-militares-con-crimenes-y-funcionamiento-de-las-auc/)

Luego de ser parte de esa Brigada, Plazas Acevedo hizo parte de la Brigada XVII al mando del General ® Rito Alejo del Rio, condenado por el asesinato de Mario Córdoba y señalado de ser uno de los grandes impulsores del paramilitarismo en Urabá y **Antioquia, departamento gobernado en ese entonces por el actual senador Álvaro Uribe.**

###### Reciba toda la información de Contagio Radio en [[su correo]
