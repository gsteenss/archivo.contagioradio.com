Title: El tema ambiental y las conversaciones con el ELN
Date: 2017-02-05 09:47
Category: Ambiente, Nacional
Tags: CENSAT, conversaciones de paz con el ELN, Locomotora mineroenergética, Rios Vivos
Slug: tema-ambiental-las-conversaciones-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG-20170204-WA0080.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 Feb 2017] 

El tema minero energético, la conservación de la biodiversidad y las iniciativas ciudadanas como las consultas populares, son algunos de los puntos más álgidos que proponen organizaciones ambientalistas **para llevar a la mesa de conversaciones con el ELN y el Gobierno de Juan Manuel Santos.**

Respondiendo a la iniciativa de ‘100 Encuentros por la Paz’, de la Mesa Social por la Paz, se llevo a cabo en la capital la ‘Pedaleada Ambientalista’, la cual reunió distintas necesidades  manifestadas por **las comunidades en distintas regiones del país, y que no pudieron ser abordadas en los diálogos de La Habana.** ([Le puede interesar: “Para negociar es necesario abrir debates y para eso se requiere a la sociedad”](https://archivo.contagioradio.com/para-negociar-es-necesario-abrir-debates-y-para-eso-se-requiere-a-la-socieda/))

### La sociedad civil y las preocupaciones ambientales 

Para las organizaciones participantes de la jornada como CENSAT, el Colectivo de Abogados José Alvear Restrepo, el Colectivo Soberanía y Naturaleza, Ríos Vivos y ciudadanos independientes, es fundamental que el tema del grave impacto ambiental de las **actividades extractivas sobre los ecosistemas y las negligencias del Estado frente a las iniciativas de Consulta Popular** en los territorios, sean llevados el próximo 8 de febrero a Quito.

Teniendo en cuenta que el primer punto de la agenda es la participación de la sociedad civil, durante la actividad se propusieron actividades para informar y hacer participes a las comunidades urbanas y rurales del preocupante panorama ambiental y de la nueva mesa de conversaciones, **jornadas en distintos barrios de la ciudad para recoger inquietudes con murales, ollas comunitarias, música y movilizaciones carnaval,** fueron algunas de ellas.

Las organizaciones señalaron que en el transcurso de esta semana, se elaborará un **documento que articule lo construido** en los demás encuentros por la paz, realizados en San Andrés Isla, Cartagena, Cali, Ibagué, Pereira, Manizales, Cúcuta, Barrancabermeja, Medellín, Neiva, Pasto, Popayán, Sincelejo, Valledupar, Puerto Asís y **sea enviado a la delegación de paz del ELN.**

Por último, las organizaciones participantes de la pedaleada por la paz, invitaron a participar en la próxima **'Marcha Carnaval por el Agua' que será el próximo 2 de junio en la ciudad de Ibagué**, para fortalecer el proceso de Consulta Popular y además, manifestaron que durante todo el mes de febrero se estarán realizando más actividades que reúnan las propuestas de la sociedad civil y fortalezcan los lazos entre las comunidades urbanas y rurales. ([Le puede interesar: Consulta popular minera en Ibagué por fin tiene vía libre](https://archivo.contagioradio.com/consulta-popular-minera-ibague-fin-via-libre/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
