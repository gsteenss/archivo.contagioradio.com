Title: Continúa la huelga de hambre de presos palestinos
Date: 2017-05-30 10:07
Category: El mundo, Onda Palestina
Tags: Huelga, Onda palestina, Palestina, presos
Slug: presos-palestinos-huelga-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/huelga2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imágen: Archivo 

###### 24 May 2017 

Más de mil seiscientos presos palestinos, entre los más de seis mil quinientos detenidos en cárceles de Israel, completan cerca de 40 días de huelga de hambre por la Libertad y la Dignidad. Durante este tiempo sólo han ingerido agua con sal tratando de preservar su salud. Reclaman que las autoridades de Tel Aviv pongan fin a la negación de las visitas familiares y de abogados, el derecho a la educación superior, al tratamiento médico adecuado, y se terminen los aislamientos y las detenciones administrativas, encarcelamientos sin cargos ni juicio indefinidamente renovable.

Autoridades de Israel han optado por transferirlos y/o llevarlos a aislamiento, esto último hecho entre otros a los líderes: Marwan Barghouthi parlamentario detenido de Fatah, Ahmad Sa´adat, el secretario general del Frente Popular para la Liberación de Palestina, el líder del FPLP Ahed Abu Ghoulmeh, los líderes en prisión de Hamas Hassan Salameh y Abbas Sayyed, el preso palestino más antiguo Nael Barghouthi y el periodista Mohammed al-Qeeq.

Aunque un tribunal israelí señaló que deben permitirse las visitas de los abogados de los huelguistas, se sigue saboteando las visitas de familiares y abogados de los presos en huelga. Sólo una pequeña fracción de los presos han podido ser visitados por abogados, y como resultado de estas visitas se ha logrado obtener información sobre el peligroso deterioro de la salud de los huelguistas.

Se ha informado sobre la preocupante pérdida de peso, algunos han perdido más de 20kg, casos de desmayos, arritmias, visión borrosa, vómito u orina con sangre; en general gran debilidad. Hay información de presos que han tenido que ser internados en hospitales de campaña o militares, lo que genera preocupación entre familiares, pues allí se supedita la atención médica a que los detenidos abandonen la huelga de hambre, o incluso se amenaza con la alimentación forzada, la cual es considerada un método de tortura.

En solidaridad se han generado masivas movilizaciones en Palestina, duramente reprimidas. En medio de las movilizaciones se han presentado ataques por parte de colonos y soldados israelíes, que han dejado como saldo el asesinato de Moataz Hussein Bani Shamsa, de 23 años y el de Saba Obeid, de 22 años, y varias personas heridas, entre esas el periodista palestino: Majdi Eshtayyeh.

La represión contra el pueblo palestino que apoya la huelga ha incluido la persecución y detención masiva de líderes de organizaciones sociales y defensores de los DDHH en Palestina En la noche del domingo 21 de mayo, por lo menos 9 líderes fueron capturados, entre ellos: Nasser Abu Khdeir, Eteraf Rimawi -Director Ejecutivo del Centro Bisan de Investigación y Desarrollo-, y Abdul Razeq Farraj, escritor, periodista y Director administrativo y financiero de la Unión de Comités de Trabajo Agrícola.

La semana pasada los palestinos hicieron una huelga general y hubo movilizaciones en diferentes ciudades, no solo en apoyo a la huelga sino además en rechazo de la visita del presidente de los Estados Unidos, Donald Trump, quien fue recibido por el primer ministro de Israel, Benjamín Netanyahu,. Estas también fueron duramente reprimidas, y fuentes como El País y EFE, informaban de por lo menos 13 palestinos heridos con disparos de fusil.

El presidente de la Autoridad Nacional Palestina, Mahmoud Abbas, se reunió con Trump en Belén. El New York Times calificó las palabras de Trump luego de la reunión con Abbas como “esperanzadoras pero vagas”. Le puede interesar: [Las consecuencias ambientales de la ocupación israelí sobre Palestina](https://archivo.contagioradio.com/las-consecuencias-ambientales-en-territorio-palestino-ocupado/).

El Middle East Eye informó que según una fuente cercana a la OLP, Abbas en esta reunión presentaría una propuesta de entregar por parte de los palestinos un 6,5% de sus tierras a Israel. Esta propuesta por supuesto no cuenta con el apoyo del pueblo palestino.

El Comité Nacional de Huelga ha emitido varios comunicados pidiendo a la comunidad internacional que continúe apoyando esta huelga, con acciones de visibilización, solicitando a sus gobiernos y a entidades como la ONU y el CICR que cumplan con su papel de garantes de los ddhh, e insistiendo a Israel a que cumpla con la legislación internacional. Se invita también a que las personas solidarias con Palestina se sumen a la Campaña por el Boicot, las Desinversiones y las Sanciones.

<iframe id="audio_18979063" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18979063_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más noticias sobre Palestina en Onda Palestina, todos los martes de 7 a 8 Pm 
