Title: Estos son los ganadores del premio Nacional a la Defensa de los Derechos Humanos
Date: 2016-09-09 17:53
Category: DDHH, Nacional
Tags: colombia, defensores de derechos humanos, Premio a la defensa de los derechos humanos
Slug: estos-son-los-ganadores-del-premio-nacional-a-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/GANADORES-DEL-PREMIO-DE-DEREHOS-HUMANOS-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 de sept de 2016] 

[La quinta versión del premio Nacional de los Derechos Humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-4to-premio-nacional-a-la-defensa-de-los-derechos-humanos/)que se llevó a cabo el pasado 9 de septiembre, ya tiene sus cuatro ganadores:  El coordinador del Comité de Derechos Humanos de SINTRAUNICOL, José Milciades Sánchez Ortiz, el Foro Interétnico Solidaridad Chocó-FISCH, la Corporación Colectivo de Abogados Luis Carlos Pérez y María Ruth Sanabria.

En la categoría a **Defensor o Defensora del año, el ganador fue José Milciades Sánchez Ortiz,** coordinador del Comité de Derechos Humanos de SINTRAUNICOL y miembro del comité de derechos humanos de la Universidad del Valle. En su ardua lucha se ha dedicado a la defensa de lo público, reivindicando la protesta y la movilización social.

En la **categoría a experiencia o proceso colectivo del año, el reconocimiento fue otorgado a el Foro Interétnico Solidaridad Chocó-FISCH**, una organización de base, creada en un contexto de crisis humanitaria en Chocó y Atrato medio Antioqueño. A su vez este escenario se ha convertido en un lugar de convergencia de muchos procesos con el fin de visibilizar las problemáticas sociales, políticas y económicas que afrontan las comunidades.

En la **categoría a experiencia o proceso colectivo del año, nivel ONG acompañantes, el ganador fue la Corporación Colectivo de Abogado Luis Carlos Pérez,** una organización que acompaña desde hace 15 años a la población víctima de graves violaciones de derechos humanos en zonas como el Catatumbo, Sur de Bolívar y Nordeste Antioqueño.

Para finalizar en la **categoría a reconocimiento a "Toda una vida", la ganadora fue la defensora de derechos humanos María Ruth Sanabria,** que inició su labor en el año 1978 en acompañamiento a la población de San Alberto César. Actualmente acompaña procesos de restitución de tierras y asume la representación de algunas víctimas del genocidio de la Unión Patriótica.

[En total se nominaron 48 procesos o personas](https://archivo.contagioradio.com/los-finalistas-del-premio-defensores-derechos-humanos/) que ejercen la defensa de los derechos humanos en Colombia, cabe resaltar que la finalidad de este reconocimiento es visibilizar la labor que realizan las y los defensores de derechos humanos y los riesgos que aún implican este compromiso con las comunidades y los derechos.

\[embed\]https://www.youtube.com/watch?v=Jhki3Pm-HV8\[/embed\]
