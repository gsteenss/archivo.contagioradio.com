Title: Amnistía Internacional, nueva víctima del espionaje
Date: 2015-07-03 17:21
Category: DDHH, El mundo
Tags: Amnistía Internacional, DDHH, espionaje, gobierno británico, Inglaterra, Salil Shetty
Slug: amnistia-internacional-es-otra-victima-de-espionaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/reino-unido-amnistia-internacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio 

###### [3 Jul 2015] 

El 1 de julio, el Tribunal británico reveló que **Amnistía Internacional** habría sido víctima de espionaje mediante el acceso y almacenamiento de sus comunicaciones por parte del gobierno británico. Tras 18 meses de litigio se confirmó la vigilancia masiva a este movimiento global de derechos humanos.

Según una comunicación del **Tribunal sobre competencias indagatorias**,  Amnistía Internacional y el **Centro de Recursos Jurídicos de Sudáfrica**, habrían sido víctimas del mismo.

“*La confirmación de que el gobierno del Reino Unido ha estado espiando a Amnistía Internacional no hace sino poner aún más de manifiesto las graves deficiencias de la legislación británica en materia de vigilancia. De no haber almacenado nuestras comunicaciones durante más tiempo del permitido por sus directrices internas, jamás nos habríamos enterado. Y lo que es peor, se podría haber considerado perfectamente legal*”. Manifestó [**Salil Shetty, secretario general de Amnistía Internacional**.]

La organización de defensa de los derechos humanos ha manifestado su preocupación ante el riesgo que corre la confidencialidad de la información entre ellos y las víctimas de abusos o violaciones de derechos, puesto que dicha información está ahora en poder de los organismos de inteligencia del gobierno **Inglés**.

"Nos escandaliza que unas prácticas que habitualmente se nos presentan como propias de mandatarios despóticos hayan estado produciéndose en territorio británico, a cargo del propio gobierno británico”, manifestó Shetty.

Hasta el momento no se tiene conocimiento de las razones por las que el movimiento a sido espiado. Están en espera de respuestas gubernamentales. Lo cierto es que una actividad como la de la organización, está en riesgo, y el espionaje no obedece a ninguna actividad en el marco de alguna ley de seguridad, como se ha develado en otros países de Europa como **Francia con la "ley de inteligencia" y España con la "Ley Mordaza".**
