Title: ¿Qué implicó para el Putumayo la aplicación del Plan Colombia?
Date: 2016-04-29 14:51
Category: eventos, Política
Tags: feria del libro, plan Colombia, Winifred Tate
Slug: que-implico-para-el-putumayo-la-aplicacion-del-plan-colombia
Status: published

###### [Foto: Notiamerica ] 

###### [29 Abril 2016 ]

Desde diversas disciplinas se ha evaluado la implementación del 'Plan Colombia', en esta oportunidad la profesora Winifred Tate comparte la mirada antropológica a través de la que evaluó esta política pública que fue pensada en Washington, prestando poca atención a la realidad de Colombia y a los impactos que su aplicación representarían en las zonas más afectadas por el conflicto armado.

Tate asegura que el 'Plan Colombia' fue **completamente dominado por una lógica militarista que lo llevó al fracaso**, a no ser capaz de frenar la expansión de cultivos de coca, no brindar alternativas reales para el campesinado cultivador, no impedir que las drogas llegaran a los Estados Unidos, no lograr una reforma militar real y no disminuir los altos niveles de impunidad.

Cómo se formuló en Washington y los **impactos que generó su aplicación en comunidades del Putumayo**, son ámbitos poco conocidos del [['Plan Colombia'](https://archivo.contagioradio.com/?s=plan+colombia+)], y justamente la intención de Tate es que a través de su publicación 'Drogas, bandidos y Diplomáticos', la opinión pública conozca del tema pero con una visión mucho más amplia frente a la formulación de políticas públicas.

"Estamos en un momento propicio para mirar hacia atrás y ver cómo se afronta el futuro" asevera Tate, en relación con el rol que Estados Unidos debe asumir en el nuevo momento político de Colombia y en el que debe hacer un **recuento de sus responsabilidades para poder aportar** en la construcción de un [['Plan Paz Colombia'](https://archivo.contagioradio.com/los-verdaderos-alcances-del-plan-paz-colombia/)] que corresponda con una lógica de paz y no militarista.

La presentación de este texto se llevará a cabo este viernes de 3 a 4 de la tarde en Corferias, en el salón múltiple 7, pabellón 20 al 23, en el marco de esta 29° versión de la Feria Internacional del Libro de Bogotá.

<iframe src="http://co.ivoox.com/es/player_ej_11356807_2_1.html?data=kpagl5ucdJihhpywj5WaaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncbjdz87T1MrIb7XV1cqYj5ClstXm0NWSpZiJhpTg0Mzcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[![Libro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Libro.png){.aligncenter .size-full .wp-image-23335 width="940" height="788"}](https://archivo.contagioradio.com/que-implico-para-el-putumayo-la-aplicacion-del-plan-colombia/libro/)

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
