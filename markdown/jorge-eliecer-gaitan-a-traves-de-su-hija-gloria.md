Title: Jorge Eliécer Gaitán a través de su hija  Gloria
Date: 2018-04-09 08:42
Category: Abilio, Opinion
Tags: dia de la memoria, Gaitán, Gaitanistas, Gloria Gaitán
Slug: jorge-eliecer-gaitan-a-traves-de-su-hija-gloria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/jorge-eliecer-gaitan-y-gloria-gaitan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[Gloria Gaitán en brazos de su padre Jorge Eliecer Gaitán. Archivo de la familia.] 

###### 9 Abr 2018

#### **La fuerza de la memoria** 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

[El primer encuentro del  grupo de católicos promotor de las 1000 firmas para el perdón con Gloria Gaitán  no fue fácil. La historia de dolores vividos por Gaitán, en su momento, por el movimiento Gaitanista y por la familia  afloraron de entrada con franqueza de tal modo que fue posible escuchar, reconocer las fundadas responsabilidades de la iglesia en la ambientación del genocidio y el magnicidio, así como también hacer las aclaraciones y precisiones de los múltiples desencuentros, sobre la base de que quienes se acercaban a proponer el  perdón no lo hacían a nombre de las autoridades representativas de la institución eclesiástica.]

[Buena parte de los diálogos francos se centraron en aclarar la percepción generalizada de que la persecución se centró en los liberales. Gloria en intervenciones vehementes que se extendieron a  escritos, se esforzó en mostrar que si bien Gaitán fue en la última etapa de su vida el director del partido liberal, él representaba un movimiento multitudinario del pueblo liberal y conservador,    que tomó distancia de las “oligarquías liberal y conservadora”, como las nombraba el mismo Gaitán, hasta configurar una fuerte movilización social que lo iba a convertir en seguro presidente de Colombia, bajo el comprendido de que,  como ha recordado Gloria: “mi papá no se sentía él como vocero del pueblo sino como empujado por el pueblo.”]

[De la mano de ella aprendimos a dilucidar que luego del crimen atroz de centenares de militantes del gaitanismo y del propio magnicidio de su padre, se  empezó a configurar la muerte de la memoria del movimiento, como también de la estatura política, social, jurídica, ética que ha pretendido ser desmoronada hasta extinguirla en lo que se puede denominar “memoricidio”.]

[Este neologismo  de la década de los 90,  ha sido definido por la ONU como “la destrucción intencional de bienes culturales que no se puede justificar por la necesidad militar”.]

[Hechos abundan de  intentos por arruinar las expresiones tangibles de esa memoria de Gaitán, ligadas directamente a la familia. (Ver: [La vida de Jorge Eliecer Gaitán narrada por su hija](https://www.youtube.com/watch?v=nurK8T3UisI))]

[La expropiación de la casa familiar  para convertirla en museo sin la debida custodia de archivos  y objetos de relevancia histórica incalculable, permitiendo el extravío de muchos de ellos. También la extracción de tres vértebras del propio cuerpo del líder dentro del proceso judicial, sin que hayan sido devueltos a la tumba.]

[Siguió el crimen de Eduardo Umaña Mendoza, abogado de la familia para el caso, quien pocos días antes del atentado  le había dicho a Gloria que había hallazgos del caso que podían provocar su asesinato.]

[La expresión  más nítida del “memoricidio” es la construida por el  siquiatra y posterior Alto Consejero de paz del gobierno de Álvaro Uribe Luis Carlos Restrepo, quien manifestó que era necesario enterrar de manera definitiva la memoria de Gaitán.]

[Al poco tiempo un panfleto amenazante  de paramilitares citaba las palabras del psiquiatra anunciando que  si Gloria no renunciaba a la dirección de la casa Gaitán “trinchera de subversión”  sería ésta dinamitada, la tumba destruida y ella asesinada. Meses después sobrevino, mediante resolución    la disolución de la institución que amparaba la existencia de la casa museo y la cesión de la administración a la Universidad Nacional, por orden del presidente Álvaro Uribe. Más adelante en esa precipitación de hechos se desencadenaron  un feroz montaje judicial contra la heredera más fiel de ésta memoria, que le robó casi todas sus energías en defenderse, quedando absuelta de todo cargo.]

[Y como si fuera poco  el perverso uso del nombre de Gaitán.   La reingeniería paramilitar propiciada por Uribe, con el  falso desmonte de éstas estructuras a cargo del mismo psiquiatra  que proclamó la muerte de su memoria, propició que paramilitares  mal se denominaran “Gaitanistas” invirtiendo del todo los valores de la defensa de la vida, de la paz,  de la organización social popular, de las reivindicación del derecho a la tierra, de la nacionalización de la industria, de la afirmación de la autonomía frente a potencia extranjera,  defendidos por Jorge Eliécer Gaitán.]

[Hoy 70 años después renace la esperanza en que la historia de la que es portadora Gloria y su familia  pueda ser recuperada por la Comisión de Esclarecimiento de la verdad que, justo en éste 9 de abril, sostendrá sesión informal  con los deudos directos del líder, abriendo la posibilidad de devolución de la casa a la administración de la familia y a personas cercanas al Gaitanismo  y de la culminación de la construcción del exploratorio conjunto para el uso de los ciudadanos.]

[También un grupo de significativo de iglesias celebrará ceremonia ecuménica en memoria del líder inmolado, resistiendo así, de la mano de Gloria y su familia,  al pretendido “memoricidio”, demostrando así que el olvido y la banalización del recuerdo, no son las últimas palabra de ésta relevante historia de Colombia.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
