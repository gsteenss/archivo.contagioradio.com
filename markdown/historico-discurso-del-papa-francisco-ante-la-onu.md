Title: Histórico discurso del Papa Francisco ante la ONU
Date: 2015-09-26 08:17
Category: Ambiente, El mundo, Política
Tags: Ambiente, Conferencia de París sobre cambio climático, Cumbre Mundial, DDHH, excluidos, ONU, Papa Francisco
Slug: historico-discurso-del-papa-francisco-ante-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/onu-papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: tn.com 

###### [25 sep 2015] 

Ante la asamblea general  de la ONU, el Papa Francisco llamó la atención a los gobiernos de todo el mundo a  participar de forma equitativa y justa en la toma de decisiones, exigió respeto por el derecho del ambiente, reprochó los malos ejercicios del poder, y rechazó de manera rotunda las armas nucleares exigiendo su **"total prohibición"**.

Frente a representantes de 193 Estados miembros, el sumo pontífice aseguró que en el panorama mundial se presenta un mal ejercicio del poder por parte de *“**preponderantes relaciones políticas y económicas que excluyen** a sectores indefensos y violan la protección al ambiente”*.

Así mismo, resaltó el derecho al ambiente mencionando la innegable relación entre este y el ser humano, quien, aunque tenga grandes capacidades que trascienden lo biológico y físico, es una “porción de ese ambiente”, afirmando que ***"cualquier daño al ambiente es un daño a la humanidad"**.*

El papa de 78 años de edad hizo, además, una crítica a la **“cultura del descarte”** como resultado de los fenómenos que se presentan a los más empobrecidos. “*Los más pobres son los que más sufren* (...) *son descartados por la sociedad, son al mismo tiempo obligados a vivir del descarte y deben sufrir injustamente las consecuencias del abuso del ambiente*.”

Así mismo, resaltó el valor que tienen las víctimas de la violencia y el conflicto, iguálandolos a los gobernantes**.** Afirma que el ejercicio político sirve cuando es prudente, guiado por la justicia y tiene en cuenta que "***hay mujeres y hombres concretos, iguales a los gobernantes,** que viven, luchan y sufren, y que muchas veces se ven obligados a vivir miserablemente, privados de cualquier derecho".*

Tras  hacer un llamado de conciencia a quienes están a cargo de los asuntos internacionales en el conflicto del Medio Oriente, **asumió su "grave responsabilidad" ante los eventos de exclusión e inequidad que se han presentado en el mundo **y dijo "alzo mi voz, junto a la de todos aquellos que anhelan soluciones urgentes y efectivas."

Como solución urgente y efectiva, denominó como esperanzadora la adopción de la agenda 2030 para el Desarrollo Sostenible en la Cumbre mundial, que iniciará el  día de hoy, así como en la Conferencia de París sobre cambio climático  para lograr acuerdos “fundamentales y eficaces”.

Discurso completo

[Discurso Papa ONU](https://es.scribd.com/doc/282760424/Discurso-Papa-ONU "View Discurso Papa ONU on Scribd")

<iframe id="doc_16523" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/282760424/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
