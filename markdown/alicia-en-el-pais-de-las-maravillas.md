Title: Alicia en el país de las maravillas
Date: 2020-07-02 13:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Nacional, Opinion
Tags: colombia, Política colombiana, Política y paz en Colombia
Slug: alicia-en-el-pais-de-las-maravillas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/nota-johan.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/GAB3249.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:audio {"id":86137} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/nota-johan.mp3">
</audio>
  

<figcaption>
Escuche aquí la nota completa

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:heading {"level":5} -->

##### La cosa está barro.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Unas veces más que otras, otras veces mas escarlatas o de rumores más o menos injustos, pero sin duda: la cosa en Colombia está barro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia de género con los abusos y asesinatos a mujeres que no cesa; la violencia política con decapitaciones y fusilamientos a hombres no cesa; la violencia estructural donde mujeres y hombres son arrastrados a las fosas de la opresión por múltiples causas no cesa. Y la violencia cultural que mantiene todo el mierdero bajo la pregunta-frase: *¿qué podemos hacer? Esto es Colombia,* sigue intacta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asumir por separado la batalla afianzados en convencer con nuestros 5 pensamientos que es solo un tipo de violencia la que oprime, puede resultar en la atomización de las causas y por ende, una tranquilidad (aunque parezca increíble) de carácter democrático para el régimen en el que vivimos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se ha confundido la diversidad de nuestra sociedad, con la razón estructural y pragmática que produce toda la infamia. No se puede negar la diversidad, pero en la diversidad debería emerger la equivalencia que permita avanzar, no para ser parte de la oposición, sino para ser la oposición.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Si la cosa no avanza, si entre oprimidas y oprimidos solo surgen, la división, los reparos y hasta los improperios, entonces paila, la estamos cagando.  

<!-- /wp:heading -->

<!-- wp:paragraph -->

El orden barbárico y cínico que se mantiene en Colombia lo mantienen dos cosas fundamentalmente: la primera es el **poder armado**, que hace de las suyas en el campo y en las calles de Colombia. La segunda es el **poder cultural** que tiene el régimen y su triunfo más brillante: ofrecer a los oprimidos las formas y las vías por las que supuestamente las cosas se van a transformar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No me detengo en la primera, porque no estoy en la guerra, y hablar de lejos es facilísimo. Prefiero no juzgar, pero no sin antes pensar que quienes deciden luchar de frente contra esta porquería y arriesgar sus vidas, sean amigos o enemigos, merecen respeto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Me detengo en la segunda, porque allí es donde la mayoría podríamos hacer algo definitivamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La cultura del régimen** está compuesta por la publicidad y la diseminación de las ideas sobre cómo observar la realidad, sea de la forma más intelectual, de la forma más estúpida o sencilla. Estas ideas viajan en forma de “educación para el trabajo”, en forma de “educación popular o trabajo comunitario pero direccionado por las fundaciones de las multinacionales”, en forma de una moral que adoctrina sobre lo que es bueno y lo que es malo, dejando en lo malo, todo aquello que le perjudique; en forma de ideas sobre el cambio social amparadas en una institucionalidad que está podrida y que no puede avanzar porque fue consumida por el virus del clientelismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En forma de democracia, que permite votar sobre lo que ya está decidido para luego alejarnos de toda decisión real. En forma simbólica de expresión verbal y escénica, imponiendo la condición de que para gobernar o hacer política hay que hablar y verse como indica el estereotipo occidental burgués o clase alta, con sus respectivos pensamientos y sus respectivas ambiciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Y no es para más.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La gente se ríe de la dicción de un tipo como Juan Daniel Oviedo, pero lo acepta porque es un eco normal de la simbología del poder. La gente no se reiría de a mucho y desaprobaría si de director de un departamento nacional pusieran a un individuo con acento campesino, **sencillamente porque no haría eco de la simbología del poder.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El poder de la cultura hegemónica del régimen en Colombia es una realidad. La masa, ese eterno cuerpo maleable y sentimental, tan alejado aparentemente de la política y haciendo uso práctico y social de su escenificación en la realidad cotidiana, actúa y piensa en la intimidad como el régimen se lo indica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Sí, hasta los pensamientos están oprimidos, por eso insisto: la cosa está barro.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Muchas y muchos quieren un cambio en Colombia; pero no saben o más bien, no quieren saber cómo. Porque saber cómo significa cambio. Es decir: **cambiarlo todo.** Y la cultura hegemónica se manifiesta en todo acto cotidiano (esa cosita que parece tan nuestra) y hace muy bien su labor: cagarnos de miedo… perdón, sugerir la revolución de las cosas pequeñas, tranquilizadoras y bellas, que dan sosiego y dejan dormir en paz… claro, mientras siguen matando mujeres, decapitando hombres, fusilando niños… pero dejan dormir en paz. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El régimen que gobierna Colombia no tiene contendor cultural. Adopta a toda la fauna de revoltosos y revoltosas porque absolutamente todos tienen en común que utilizan el método educativo del régimen, el lenguaje del régimen y las instituciones sociales y políticas del régimen.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En Colombia la desobediencia es un producto más del mercado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hasta las multinacionales hablan de transformación, la guerra cultural que viene ganando el régimen de la barbarie que obvio no está instalado solo en Colombia, se manifiesta en cosas como la nueva imagen de Calvin Klein; es que a los revoltosos y revoltosas hay que empaquetarlos y venderlos, porque mientras no hagan daño y sean solo ganas, ¡también se venden!

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Me pondré un poquito Maquiavélico. La frontera entre ser consumidor de rebeldía y ser rebelde es el poder definitorio entre la violencia y la capacidad para repelerla o derrotarla con otra violencia. Y no se me angustien aquellos que sé que se me angustian cuando suena la palabra violencia; creo que es un debate que sería interesante darlo, pero con una condición: sin la estructura educativa del régimen como determinadora de la consciencia sobre la paz y sobre la guerra. Ese debate sería hermoso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuando un revoltoso quiere cruzar la frontera, si no está preparado, puede someter su cuerpo al sufrimiento ¡que lo digan los de las innumerables cabezas rotas que mientras gritaban “sin violencia” “sin violencia” no vieron venir la violencia en las protestas del 2019!   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Además, Habermas aquí no opera, aquí ni con argumentos ni con hijueputazos caerá un régimen tan mezquino, tan corrupto, tan infame, tan injusto como el que tiene Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Duele, pero es real. Lo lamento por los utópicos de la teoría comunicativa, pero por favor, están Colombia y se corre el riesgo de pasar como vendedores de humo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La guerra cultural comienza por entender que un país gobernado por una clase dominante, como por ejemplo la clase que actualmente domina Colombia, es una clase o un grupo de poder (conceptualícelo como quiera, pero no pierda de vista lo que es) que no entregará el poder por la vía democrática, pues la democracia que tiene montada favorece el sistema clientelar que lo sostiene eternizado en el poder. La eternización en el poder, no la hace de manera estúpida dejando a un mismo tipo durante décadas, sino haciendo cambios cada cuatro años en los rostros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es decir, funciona estructuralmente como dictadura, pero su imagen varía y para muchos superfluos, mientras la imagen cambie, pues no hay dictadura. Con ese jueguito, suben a los amigos, a los familiares, a los hijos, los nietos, los yernos, etc. etc. etc.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Para afectar al régimen que gobierna Colombia, se debe declarar una guerra a muerte en el campo cultural.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se necesita de cierta centralidad democrática o lo que el más preocupado liberal podría llamar “autoritarismo”, para retar la dictadura civil que tienen montada con esos símbolos democráticos muy claros como son los votos, el cambio de presidente cada 4 años; símbolos que al final se mantienen sobre los pilares de poder terrateniente, empresarial, banquero, multinacional, narcotraficante y armado bastante fuertes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿En qué mundo de las maravillas pensamos cuando pensamos en que Colombia debe cambiar? ¿se hubiese ido Hitler del poder si un niño judío le hubiera pedido el favor de que se fuera? Pura analogía del país de las maravillas, pero vea; leer a Marx sin leer a Lenin, conformar primeras líneas sin leer a Clausewitz, leer a Foucault sin saber la historia de la comuna de París no sirve de mucho para esa izquierda sin carácter que anda jugando a la noche de antifaces, muy crítica claro (sobre todo con los errores de la izquierda), muy “vamos a cambiar la sociedad”, pero sin capacidad para hacer daño al que la domina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es esa izquierda sin duda, una versión abstracta de Alicia en el país de las maravillas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya se sabe desde Le Bon, que una mentira repetida mil veces se convierte en una verdad. Vamos entonces a mentir en el país de las maravillas para ver si vuelven a su tamaño normal algunos muchos:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **La vía institucional para transformar la barbarie que existe en Colombia no sirve.** Podemos insistir para hacer de nuestra vida menos miserable, o entregarnos al placer hegemónico como si no existiera nada más y ganar la corona de los cretinos.

<!-- /wp:list -->

<!-- wp:list -->

-   **No hay revoluciones perfectas.** No existen tales cosas. Solo en la mente de intelectuales que impulsan los pensamientos de los jóvenes, pero que se cagan de miedo ante las sombras de su propia sospecha cuando se van volviendo viejos y por eso resguardan su ego desprestigiando todo lo que no fueron capaces de hacer.  

<!-- /wp:list -->

<!-- wp:list -->

-   **Las cosas no son tan lindas cuando las sociedades oprimidas deciden bajar del poder a asesinos, a violadores, a corruptos que roban a la gente.** ¿por qué no son lindas? ¿por qué no se pueden ir y dejar en paz a este país con toda tu belleza y toda su riqueza? Para ellos es simple, es instintivo, tienen que asegurar a sus nuevas generaciones fortunas más grandes que las que ellos mismos robaron. **Por eso nada se detendrá de manera linda.**

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### ¿o acaso quién dejaría por la buenas un privilegio que, aunque ha costado la sangre a miles de inocentes no deja de ser privilegio?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Podríamos dejar ya tanta cosa por una vez en la existencia de los pensamientos que nos recorren una bendita tarde y aceptar que la clase que domina está compuesta por hombres y mujeres que se unen para joder a otros hombres y a otras mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La distinción es la clase.** Ya no tan clara como en otros siglos, pero no por eso caduca; claro, no son solo un par de familias, tampoco son oligarquías, porque ya se casaron y se cuadraron con más gente, se revolvieron luego de una farra en Andrés. No obstante, lo que los une es lo mismo que nos separa de ellos: su poder en el campo económico y militar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Triste es escribir que Cabal (una mujer) es capaz de crear la mentira respecto a la violación de una niña.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ella simboliza junto con Paloma, con Uribe y con Londoño el poder político de Colombia, ¿y todavía pensamos que el poder radica en un concepto o una abstracción teórica de la dominación? No, las cosas que parecen tan complejas son en realidad sencillas de entender solo si contenemos por ciertos instantes ese país de las maravillas, y estamos dispuestos a convertimos en Alicia…simplemente Alicia.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea mas columnas de](https://archivo.contagioradio.com/johan-mendoza-torres/)[J](https://archivo.contagioradio.com/johan-mendoza-torres/)[ohan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)

<!-- /wp:paragraph -->
