Title: Segovia y Remedios siguen esperando respuestas a crisis laboral y social
Date: 2017-08-10 13:09
Category: Movilización, Nacional
Tags: ESMAD, Mineria, Movilizaciones, paro minero, Remedios y Segovia
Slug: segovia-y-remedios-siguen-esperando-respuestas-a-crisis-laboral-y-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/segovia-y-remedios-e1502388737744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPC] 

###### [10 Ago 2017] 

En los municipios de Segovia y Remedios continúa el paro minero. Así mismo**, continúa la represión por parte del ESMAD** y los habitantes manifiestan que, tanto el gobierno nacional como la multinacional Grand Colombia Gold, no tienen voluntad política para resolver la crisis laboral y social que hay en estas poblaciones.

Jaime Gallego, miembro de la mesa minera, manifestó que ayer, en el corregimiento La Cruzada, hacia las 4:00 am, el Escuadrón Móvil Anti Disturbios ESMAD, **“desbarató las carpas y la cocina de los manifestantes”**. Esto, según Mongo, “lo hicieron para provocar a los manifestantes”. (Le puede interesar: ["Continúa paro de mineros en Remedios y Segovia pese a la represión"](https://archivo.contagioradio.com/continua-paro-de-mineros-en-remedios-y-segovia-pese-a-la-represion/))

Ante la represión, los remedianos y segovianos le han pedido al Gobierno Nacional y a la Fuerza Pública **que retire las unidades del ESMAD del territorio para garantizar la seguridad de las personas**. “Los padres no están mandando a sus hijos a estudiar porque no hay garantías de seguridad, el comercio dice que la economía está caída y el bloqueo de la comercialización del oro los ha afectado”.

### **Van 26 heridos graves por arremetidas del ESMAD** 

Gallego manifestó que, durante de los disturbios, **en Remedios hubo 17 personas agredidas y 9 heridos de bala**. Por su parte, en Segovia fueron 11 los heridos, 4 de ellos con impacto de bala. Pese a los reclamos de la población, el representante de la mesa minera denunció la llegada de 2 buses más del ESMAD durante la noche del 9 de agosto.

Frente a las arremetidas de los uniformados y la falta de diálogos con el Gobierno y la multinacional, Gallego indicó que **“quieren que la población se desgaste y la presionan para que se levante y así poder justificar el aparato represivo”**. Sin embargo, los habitantes han manifestado en varias ocasiones que continuarán con las movilizaciones pacíficas. (Le puede interesar: ["Toque de queda y militarización no detendrán paro" Mineros de Segovia y Remedios"](https://archivo.contagioradio.com/toque-de-queda-y-militarizacion-no-detendra-paro-mineros-de-segovia-y-remedios/))

### **Mineros continúan sin respuesta del gobierno y de la multinacional** 

Los mineros tradicionales de estos departamentos, han pedido que se **legalice su trabajo para que la comercialización del oro beneficie a los trabajadores** de la cadena productiva. Alegan que la multinacional Grand Gold Colombia se lleva todas las ganancias y las condiciones laborales para los trabajadores son lamentables.

Habiendo realizado el paro, le han pedido al Gobierno Nacional que **disponga de mecanismos para dialogar con la multinacional y la mesa minera**. Sin embrago, no ha habido presencia de las autoridades en los municipios y gremios como el de los comerciantes y habitantes de Medellín se han unido a la protesta social. (Le puede interesar: ["Mineros artesanales de Marmato le ganan el pulso a la Grand Gold Colombia"](https://archivo.contagioradio.com/mineros-artesanales-de-marmato-le-ganan-el-pulso-a-la-gran-colombia-gold/))

Según Jaime Gallego, **“el comercio lleva cerrado varios días y los mismos comerciantes han decidido mantenerlo cerrado”**. De igual manera, Mongo dijo que “la economía en la región está caída, hay mucho oro, pero no tenemos como comercializarlo”. Esta falta de oportunidades de comercialización del mineral se da por el reconocimiento de ilegalidad de los mineros artesanales, condición que está en las demandas de los mineros.

Finalmente, los habitantes de Remedios y Segovia propusieron realizar una marcha y una caravana hacia Medellín. **Esperan que el Gobierno y la Multinacional se pronuncien para poder solucionar los problemas**.

<iframe id="audio_20272808" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20272808_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
