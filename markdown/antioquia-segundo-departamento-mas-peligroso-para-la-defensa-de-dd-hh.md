Title: Antioquia, segundo departamento más peligroso para la defensa de DD.HH.
Date: 2019-09-18 12:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales
Slug: antioquia-segundo-departamento-mas-peligroso-para-la-defensa-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El Centro de Investigación y Educación Popular (CINEP) a través de su más reciente informe: Antioquia: la guerra en desarrollo documenta la grave situación de violencia que se vive en este departamento, provocando que ocupe el cuarto lugar en donde se registra un mayor número de asesinatos de líderes sociales, con un total de 105 casos en la región.

La información recolectada a través de testimonios de víctimas, prensa local, regional y nacional e informes de organizaciones de base en los territorios revela que de **643 casos de violencia, 105 casos se presentaron en Antioquia**, de los que 39 fueron responsabilidad de grupos paramilitares, mientras que 67 obedecen a violencia política de los que aún no se ha podido establecer su autoría, otros 7 son infracciones cometidas por la Policía, Ejército y el CTI.

De igual forma la violencia estructural se mantiene en el departamento, muestra de ello son las 4.018 amenazas contra defensores de derechos humanos, 23 delitos contra la libertad y la integridad sexual, 15 desapariciones forzadas; 21.789 víctimas de desplazamiento forzado; 207 homicidios; 49 lesionados; 19 casos de víctimas de explosión de artefactos explosivos;  1 caso de secuestro, 4 casos de tortura y 17 casos de reclutamiento forzado de menores que se presentaron en 2018.

Estas cifras se pueden contrastar con los datos obtenidos en lo que va corrido del 2019 que revelan 1.184 casos de amenazas; 3 delitos contra la libertad y la integridad sexual, 2 desapariciones forzadas; 4.162 casos de desplazamiento forzado; 12 homicidios; 5 lesionados; 1 víctima de artefactos explosivos, y un evidente aumento en las agresiones contra mujeres lideresas permiten señalar que la situación no ha mejorado en el departamento.

### Alerta sobre los líderes que promueven la sustitución en Antioquia 

Entre 2017 y 2018 , han sido asesinados 16 líderes que pertenecen al Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS) en este departamento, mientras otros 40 han sido desplazados, esto debido al interés de grupos paramilitares como l**os Caparrapos, las Autodefensas Gaitanistas de Colombia, AGC** por perpetuar y mantener el control de la cadena de producción de cocaína en el departamento. [(Lea también: Al Bajo Cauca no llega la sustitución pero si la extorsión y el desplazamiento)](https://archivo.contagioradio.com/al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento/)

### ¿Qué documentó el CINEP en el resto del país?

A nivel nacional, Según el CINEP, Durante el primer semestre de 2019 se documentaron 643 nuevos casos de violencia, 446 representan violaciones a los DD. HH.; 150 a infracciones al Derecho Internacional Humanitario (DIH), y 630 a la violencia político social en el país.

De acuerdo con la documentación, a los 105 hechos que se documentaron en Antioquia, se suman, los 314 casos de Cauca, que se ubica como** **el departamento más afectado, ** seguido de Norte de Santander con 147, Nariño con 108 y Valle del Cauca con 100.** [(Le puede interesar: Violencia, un mecanismo de competencia entre candidatos)](https://archivo.contagioradio.com/violencia-un-mecanismo-de-competencia-entre-candidatos/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
