Title: Colombia debe respetar a sus Magistrados y sus decisiones
Date: 2020-08-27 12:37
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Corte Constitucional, Corte Suprema de Justicia, Fiscalía, Procuraduría General de la Nación
Slug: colombia-debe-respetar-a-sus-magistrados-y-sus-decisiones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/19055754_1511587105602024_618100866967551602_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Magistrados de Corte Suprema de Justicia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la presión que se ha ejercido por parte de partidos como el Centro Democrático y el mismo presidente Duque y funcionarios de gobierno contra la Corte Suprema de Justicia tras la medida de aseguramiento contra el expresidente y exsenador Álvaro Uribe, antiguos integrantes de las altas cortes hacen un llamado a respetar la independencia de las instituciones y entes de control, muchos de los cuales están en la mira del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a la polarización que ha generado la situación política que vive el país, para **Jaime Arrubla, jurista y exmagistrado de la Corte Suprema de Justicia**, lo esencial es que la opinión pública y la ciudadanía comprendan que el país requiere de un aparato judicial independiente y autónomo "que sea capaz de tomar sus propias decisiones, respete los derechos de las personas, que se les aplique su debido derecho y tengan su derecho a la defensa".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Magistrados no debe ceder ante la presión

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a la presión que ha existido hacia la Corte Suprema dada la decisión de ordenar una medida de aseguramiento contra el expresidente Uribe, el jurista es enfático en que este tipo de situaciones no debe afectar su juicio **"a un magistrado no lo presiona nadie, un magistrado actúa según la Constitución y diga lo que diga el mundo, él va a tomar su decisión".** [(Lea también: Absolución de Iván Cepeda en Procuraduría pone más peso al proceso contra Álvaro Uribe)](https://archivo.contagioradio.com/absolucion-de-ivan-cepeda-en-procuraduria-pone-mas-peso-al-proceso-contra-alvaro-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez resalta que el exsenador Uribe podía renunciar a su cargo, lo que implicaría a lo mejor un cambio de competencia, pues si cesa en el cargo, se cesa en el fuero, sin embargo añade que es decisión de la Corte Suprema de Justicia definir y demostrar que el cargo y sus funciones tienen una relación directa con los hechos investigados, "eso es algo que a la Corte le corresponde, dejemos que el sistema actué, eso es lo que pasa en las democracias y no podemos cambiar eso por sucesos o por personajes".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El día en que uno como magistrado se siente presionado lo que tiene que hacer es renunciar"
>
> <cite>Exmagistrado Jaime Arrubla</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Para **José Gregorio Hernández, jurista, doce y exmagistrado de la Corte Constitucional** es innegable que ha existido una presión en el caso desde ambas partes, por lo que apoya la postura del exmagistrado Arrubla exaltando que los magistrados no deben dejarse presionar pues han actuado "dentro de la órbita de sus competencias" y considera lo han demostrado a través del Auto, el expediente y sus elementos de juicio por lo que debe respetarse su decisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que los magistrados únicamente deben responder a la Constitución y al ámbito de sus competencias, ni siquiera a los partidos políticos a los que pertenecen, "tenemos que acogernos al derecho, la decisión la debe asumir la Corte".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El constitucionalista a su vez rechaza los sucesos que ha derivado de tal decisión tal como las sindicaciones que han existido contra los magistrados, los pronunciamientos del presidente Duque con relación al caso y la forma en que funcionarios de otros gobiernos extranjeros, como el vicepresidente de EE. UU., Mike Pence ha buscado presionar por la liberación de Uribe Vélez.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La importancia del fuero político y la reserva del sumario

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Jaime Arrubla el caso del hoy exsenador del Centro Democrático es el mejor laboratorio para comprender la importancia del fuero político y la reserva del sumario en procesos en los que "precisamente para evitar que las decisiones las tomen en el micrófono, deben ser reservadas para proteger el caso" y donde el tribunal mayor, como lo establece el artículo 235 de la Constitución debe tomar este tipo de decisiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Gustavo Gallón, director de la Comisión Colombiana de Juristas (CCJ)** expresó que si bien no se conocen los elementos suficientes al respecto, llama la atención cómo el exsenador busca cambiar de juez "cuando hace dos años dijo que no lo haría", por lo que considera existe una incoherencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez el director de la CCJ señala que debe respetarse el pronunciamiento de la [Procuraduría,](https://twitter.com/PGN_COL) órgano de control que a su juicio considera que la Corte Suprema perdió la competencia y debe remitir a la Fiscalía el proceso contra el exsenador Álvaro Uribe, sin embargo también resaltó que la alta corte y sus magistrados pueden apartarse de ese concepto si así lo consideran.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿ Y si el caso de Álvaro Uribe por falsos testigos llega a la Fiscalía?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Gallón, la prisa que tienen los abogados del exsenador para que el caso pase a la Fiscalía es sospechoso, pues resulta evidente que "es mucho más fácil tener la capacidad de impresionar con otros métodos a un solo funcionario sea el fiscal, sea el juez a que a cinco magistrados", expresa el director de la CCJ, resaltando que si bien al interior del ente investigador también existen funcionarios honorables, otros pueden ser susceptibles a ser intimidados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De llegar a la Fiscalía el caso, Gallón resalta que es temprano para saberlo, pero es firme en que debe tenerse en cuenta la conveniencia de que el fiscal general, Francisco Barbosa se aparte del caso y un fiscal ad hoc sea elegido, **"todos sabemos que hay vínculos estrechos entre el presidente Duque y Barbosa y todos sabemos que el jefe del presidente es Álvaro Uribe, por lo que que mantener la presencia del fiscal Barbosa no da garantías"**, argumentos por los que señala debería considerarse impedido. [(Lea también: Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad)](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Hérnandez inquiere ante un escenario hipotético en el que el caso del expresidente regrese a la justicia ordinaria si eso significaría que el proceso tendría que comenzar de nuevo o si sería archivado y la detención del exmandatario sería revocada, "me parece muy difícil que un fiscal individualcontrovertir una decisión proferida por los magistrados o que tengamos que regresar al inicio del proceso".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo hace la salvedad en que **tarde o temprano, incluso si el caso regresara a la Fiscalía, este volvería a la Corte** a través de recursos de carácter extraordinario como casación: (por infracción de ley y por quebrantamiento de forma) o de revisión de las sentencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras la competencia del caso se define, la Corte Suprema de Justicia también llamó al expresidente y exsenador Álvaro Uribe Vélez a rendir versión libre el próximo 16 de septiembre por los hechos de la masacre de El Aro ocurridos en 1997, llamado que para **José Gregorio Hernández,** es anterior a la renuncia, pese a que su notificación se dio un día después de dejar su curul.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el caso debería ser de competencia de la justicia ordinaria, considera que si bien el expediente iba dirigido a la Fiscalía, esto ocurrió cuando el exgobernador de Antioquia fue nombrado senador, por lo que se decidió que todo fuera remitido a la Corte Suprema de Justicia, y la Fiscalía nunca llegó a acusar al expresidente ni siquiera cuando estaba al frente de la institución Eduardo Montealegre quien ha hecho una serie de denuncias contra el expresidente.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La puerta giratoria desde el Ejecutivo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la crisis que vive el país, también se ha alertado sobre la pérdida de equilibrio de poderes que se está dando en los organismos de control como la Fiscalía, Defensoría del Pueblo Contraloria y Procuraduría General a la que también llegaría funcionarios simpatizantes del Gobierno, al respecto el exconstitucionalista alerta sobre la ausencia de "un valor fundamental para un Estado Social de Derecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La cercanía a la Casa de Nariño por parte de varios funcionarios es evidente en los nombramientos de Francisco Barbosa, actual fiscal general, reconocido por su amistad de larga data con el mandatario, Carlos Camargo, defensor del Pueblo y compañero de Iván Duque en la Universidad Sergio Arboleda y de pronosticarse su llegada a la Margarita Cabello se convertiría en la nueva procuradora general, después de fungir como ministra de justicia del actual gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Se ha perdido independencia por parte de quienes deberían tenerla (...) no podemos pretender que todas las corporaciones estén en una misma tendencia del Gobierno", por lo que señala que en el futuro quienes ocupen este tipo de cargos "no pueden ser seleccionados según el querer del presidente" pues al hacerlo se rompe con el sistema de mutuo control; por el contrario deben ser seleccionados sin recomendaciones políticas y con base en su hoja de vida. [(Lea también:Carlos Camargo será la voz del gobierno en la Defensoría del Pueblo)](https://archivo.contagioradio.com/cercania-del-nuevo-defensor-carlos-camargo-al-gobierno-es-un-dano-a-la-democracia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
