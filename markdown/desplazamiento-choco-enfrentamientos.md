Title: Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó
Date: 2017-04-18 13:50
Category: Nacional
Tags: Chocó, Desplazamiento, ELN, Enfrentamientos, paramilitares
Slug: desplazamiento-choco-enfrentamientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/desplazados-Choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### 18 Abr 2017 

Más de **300 familias** de las comunidades de las **Pavas, Truandó Medio, Dos Bocas y Clavellino** han tenido que salir huyendo de sus territorios en el Bajo Atrato Chocoano, por cuenta de los **fuertes enfrentamientos** del fin de semana **entre paramilitares de las denominadas “Autodefensas Gaitanistas” e integrantes del ELN.**

Según la denuncia de las comunidades, además de los enfrentamientos que también se han presentado en otros sectores del departamento, **existe el riesgo de las “minas antipersona”** que en los últimos días han dejado 3 víctimas: un indígena menor de edad, un afrodescendiente y un integrante de las Fuerzas Militares que cayeron en campos minados.

Ante esas situaciones, cerca de 300 familias decidieron abandonar sus territorios para **resguardarse en el casco urbano de Rio Sucio y buscar allí refugio y atención**. Sin embargo la ayuda que han podido proporcionar las autoridades locales ha sido insuficiente y **no hay respuestas por parte del gobierno nacional** que debería mirar de cerca el problema que afrontan indígenas y afrodescendientes.

Uno de los pobladores relata que aunque hay presencia de la Defensoría del Pueblo, es necesario que el gobierno nacional atienda la situación porque **la alcaldía no da abasto con las urgencias de las personas desplazadas** “el gobierno nacional debería mirar la situación” exige uno de los líderes de las comunidades quien también está amenazado por lo que pidió no revelar su identidad.

Además **las víctimas desconocen si se están realizando o no acciones militares por parte del Estado** para intentar tomar el control de las zonas y posibilitar que las comunidades retornen a sus territorios. Opción que, hasta el momento, no es considerada por lo delicada de la situación y la necesidad urgente de la ayuda humanitaria de emergencia.

Para **Luis Murillo, defensor regional del Chocó**, en el departamento hay 2**0 municipios en riesgo de desplazamiento** **y que sufren confinamiento o desabastecimiento** dada la presencia de estructuras armadas “con las mismas características de los paramilitares” y debido a la persistencia de las acciones de control de las rutas del narcotráfico podrían generarse nuevas víctimas.

De manera extraoficial y aún sin confirmación, el pasado Domingo se hablaba de 50 víctimas fatales, al parecer pertenecientes a las AGC y al ELN, sin embargo, la comunidad no confirma o descarta esta información y hasta el momento se desconocen partes de resultados operacionales por parte del ELN o de los paramilitares. Le puede interesar: [Paramilitares controlan veredas de San José de Apartadó](https://archivo.contagioradio.com/paramilitares-controlan-veredas-de-san-jose-de-apartado/).

<iframe id="audio_18210886" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18210886_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_18211709" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18211709_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
