Title: Todo lo que necesita sobre el avance de la Marcha por la Dignidad
Date: 2020-07-04 19:17
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cauca, Gobierno Duque, Líderes Sociales, Movilización social, Suroccidente colombiano
Slug: a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Marcha-por-la-dignidad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Dignidad.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/106928646_194939145315745_5383901862467760421_n.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/106117668_2042094472594610_6617169945615244422_n.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-07-at-1.10.44-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-07-at-1.10.52-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Macha por la Dignidad/ PCN

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un total de 590 kilómetros será la distancia que tendrán que recorrer los y las caminantes de la Marcha Por la Dignidad, iniciativa que partió el pasado jueves 25 de junio **desde Popayán, Cauca con rumbo a Bogotá con el fin de exigirle al Gobierno medidas contra las problemáticas que se viven en el suroccidente** colombiano y sobre todo denunciar la violencia de género y el asesinato de líderes sociales en todo el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La marcha que comenzó su recorrido con 15 personas ha ido sumando participantes con cada paso avanzado, incluidos defensores y defensoras de DD.HH, estudiantes de diversas universidades y líderes de comunidades indígenas, afro y campesinas que buscan llegar a la capital y pedir además ante la comunidad internacional y las embajadas para que intercedan ante la crisis humanitaria que viven las zonas rurales del país.

<!-- /wp:paragraph -->

<!-- wp:image {"id":86315,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Dignidad.jpg){.wp-image-86315}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad internacional, incluidos integrantes de Parlamento Europeao ya han destacado ante el Gobierno de Iván Duque, la importancia de tomar medidas para evitar que siga en aumento el ataque a líderes sociales y excombatientes, además de redoblar los esfuerzos para proteger regiones como el **Chocó, Pacífico, Cauca, Bajo Cauca, Sur de Córdoba y el Catatumbo** donde el conflicto armado se ha derivado en nuevos episodios de desplazamiento forzado, asesinatos selectivos, masacres, presuntas ejecuciones extrajudiciales y violencia sexual. [(Le recomendamos leer: Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH)](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":86317,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/106928646_194939145315745_5383901862467760421_n.jpg){.wp-image-86317}  

<figcaption>
Paso de la marcha por el barrio Siloé en Cali, foto: Tejido Social Xamundi

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que Cauca, departamento del que partió la marcha ha sido uno de los departamentos que más ha sufrido la violencia generada en los territorios y que fue en ascenso desde el inicio de la cuarentena. Para inicios de junio, según las cifras del Sistema de Información sobre Agresiones contra Personas Defensoras de Derechos Humanos en Colombia (SIADDHH), **Cauca registró el asesinato de 9 asesinatos a lo largo del 2020**. [(Lea también: 167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz)](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/RedAlternaPopayan/videos/261559051806570/?__xts__[0]=68.ARCRlr9eTsisVWFgPQ6ZAYYBL7r1CTfsO63UejZm2Rv89BEPiB8CdelNcK66DlW_mf3qG1VmceGDiTYYndcnfa2aoaH330oSHhVhXyZU5T3MSkRvsaz13xAWtq4VAQkvYALBN3RvDBWfFDOstwNV213W51h-_uEDMStmLQ51tWbq5dSPE2LguKw4YvC_0djIY_RZ7_w08PD7vcX-vW6ZBBctsTkZZV_f5lWkI9gf1e9aPtb87ZosmJR-V-5e5woGuxv4zKX3FFzbkyK23Noh-cnQrimhkabBwIY_ozdiAifReflLxkd-CRo1VBeSG3xo45ov_QLJNHJ4S2-jG-6X2wAqJ00DhdiAjQo\u0026__tn__=-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/RedAlternaPopayan/videos/261559051806570/?\_\_xts\_\_\[0\]=68.ARCRlr9eTsisVWFgPQ6ZAYYBL7r1CTfsO63UejZm2Rv89BEPiB8CdelNcK66DlW\_mf3qG1VmceGDiTYYndcnfa2aoaH330oSHhVhXyZU5T3MSkRvsaz13xAWtq4VAQkvYALBN3RvDBWfFDOstwNV213W51h-\_uEDMStmLQ51tWbq5dSPE2LguKw4YvC\_0djIY\_RZ7\_w08PD7vcX-vW6ZBBctsTkZZV\_f5lWkI9gf1e9aPtb87ZosmJR-V-5e5woGuxv4zKX3FFzbkyK23Noh-cnQrimhkabBwIY\_ozdiAifReflLxkd-CRo1VBeSG3xo45ov\_QLJNHJ4S2-jG-6X2wAqJ00DhdiAjQo&\_\_tn\_\_=-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph {"align":"justify"} -->

**"Es una jornada de reivindación y memoria por nuestros líderes que han sido asesinados",** afirma Daniel Gallego, defensor de DD.HH. y un de los estudiantes de la Universidad del Cauca que desde el primer día se sumó a la movilización que ya ha recorrido municipios como Santander de Quilichao, Buga, Tuluá, Jamundí, Palmira, Cali y que durante su noveno día de marcha han cruzado de Armenia a Pereira.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante su paso por Pereira afirman, se busca realizar un plantón en frente del Batallón San Mateo, en rechazo a la violación de la que fue objeto una menor indígena Embera Chamí por parte de soldados de esta guarnición. [(Le puede interesar: Retiran del Ejército a sargento que denunció el abuso de niña Embera)](https://archivo.contagioradio.com/retiran-del-ejercito-a-sargento-que-denuncio-el-abuso-de-nina-embera/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":86320,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/106117668_2042094472594610_6617169945615244422_n.jpg){.wp-image-86320}  

<figcaption>
/ Foto: En Red Podemos

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de su recorrido y en el transito por el Valle del Cauca, según la denuncia de medios como [Red Alterna Popayán](https://www.facebook.com/RedAlternaPopayan/), el pasado 2 de julio, integrantes de la Policía intentaron impedir su avance y su derecho a la protesta pacífica que incluía todas las medidas de bioprotección. Agregan que desde entonces la Policía Nacional ha grabado de manera oculta a participantes de la marcha y a integrantes de organizaciones sociales que han acompañado por tramos a los caminantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A raíz de ello, organizaciones, defensores de DD.HH. e incluso congresistas han solicitado a entes como la Procuraduría General de la Nación para que acompañen el ejercicio de la Marcha por la Dignidad y garanticen los derechos de los caminantes que se aproximan a Bogotá con el fin de resaltar la defensa de las comunidades. [(Lea también: Caminar para revivir las memorias de El Castillo, Meta)](https://archivo.contagioradio.com/caminar-para-revivir-las-memorias-de-el-castillo-meta/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/PCNcolombia/videos/2666673330274513/?__cft__[0]=AZVgCDP-rDyCi0qvccJ3O_u9xJ4dCzgXgTUsCfM8rudzTgj2SKr8gNYvNXkCiXq83rxfCavjOGrj8BfTQNy7_QM9bkl0TvDFc1iuZbJwfTJRF-EJe3XQIRDApHLkoNPSlAsv1DnyCjlmRv1wwmOutFKqm1a-SokJLcil3LpPwxuJIQ\u0026__tn__=%2CO%2CP-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/PCNcolombia/videos/2666673330274513/?\_\_cft\_\_\[0\]=AZVgCDP-rDyCi0qvccJ3O\_u9xJ4dCzgXgTUsCfM8rudzTgj2SKr8gNYvNXkCiXq83rxfCavjOGrj8BfTQNy7\_QM9bkl0TvDFc1iuZbJwfTJRF-EJe3XQIRDApHLkoNPSlAsv1DnyCjlmRv1wwmOutFKqm1a-SokJLcil3LpPwxuJIQ&\_\_tn\_\_=%2CO%2CP-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph {"align":"justify"} -->

En la ciudad de Ibagué realizaron una marcha en el centro de la ciudad donde se reunieron con excombatientes y estudiantes de la Universidad de Tolima. Señalan que ya han tenido pequeños espacios de interlocución con algunas de las embajadas y la expectativa está a a llegada de Bogotá donde por medio de las Naciones Unidas se está adelantando dicha gestión de diálogo.

<!-- /wp:paragraph -->

<!-- wp:image {"id":86473,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-07-at-1.10.52-PM-1024x768.jpeg){.wp-image-86473}  

<figcaption>
Excombatientes en la ciudad de Ibagué/ Red Alterna Popayán

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Luego de cruzar más de 522 kilometros del territorio nacional a lo largo de 13 días de movilización pacífica la Marcha por la Dignidad, llegó en la mañana de este martes a Melgar, Tolima, distanciados únicamente por 98 kilómetros de la capital, su punto de llegada.

<!-- /wp:paragraph -->

<!-- wp:image {"id":86471,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-07-at-1.10.44-PM-1024x768.jpeg){.wp-image-86471}

</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
