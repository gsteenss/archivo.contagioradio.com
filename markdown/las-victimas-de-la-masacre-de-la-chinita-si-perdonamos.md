Title: "Las víctimas de la masacre de La Chinita sí perdonamos"
Date: 2016-09-21 15:31
Category: Nacional, Paz
Tags: Acuerdo sobre víctimas, Diálogos de paz Colombia, Paramilitarismo en San José de Apartadó
Slug: las-victimas-de-la-masacre-de-la-chinita-si-perdonamos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Masacre-de-La-Chinita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana ] 

###### [21 Sept 2016 ] 

El pasado 12 de septiembre, la delegación de paz de las FARC-EP anunció que por petición de las víctimas llevará a cabo un acto de **reconocimiento público de su responsabilidad en la masacre ocurrida el 23 de enero de 1994**, en el barrio obrero La Chinita, del municipio de Apartadó, Antioquia. El acto se llevará a cabo el próximo viernes 30 de septiembre y contará con la presencia de delegados del Gobierno, la guerrilla, víctimas y familiares.

"Las víctimas de la masacre La Chinita sí perdonamos", asegura la líder Silvia Berrocal, quien recuerda que el día anterior a la masacre, **un grupo de desmovilizados del Ejército Popular de Liberación habían convocado una reunión** para apoyar la candidatura de uno de sus integrantes al Senado; gran cantidad de pobladores asistieron al encuentro al que llegaron también miembros del quinto frente de las FARC-EP. "Habían llegado muchas amenazas de muerte (...) vivíamos aterrorizados (...) decían que iban a matarnos", agrega.

34 hombres y una mujer fueron asesinados, 17 personas resultaron heridas en la cruenta masacre de la que Diana Hurtado recuerda que "tenía 6 años (...) llegaron a tocar la puerta a las 5 de la mañana, mi papá había salido un día antes, a la reunión que había sido convocada, él no quería ir pero mi mamá le insistió mucho (...) mi mamá abrió la ventana y a sangre fría le dijeron que mi papá estaba muerto (...) **me prendí de la pijama de mi mamá y salimos corriendo saltando muertos, eran muchos**".

Como Diana y Silvia, todas las víctimas y los familiares tienen esperanza en que este acto de reconocimiento sirva para que sean aclarados los hechos en los que sucedió la masacre, como parte de una verdadera reparación, en la que también **esperan que se implementen proyectos como un centro social comunitario, una universidad y viviendas**. "Esperamos que las demás víctimas del municipio de Apartadó sean reparadas y sean esclarecidos los hechos que rodearon otras masacres ocurridas", agrega Diana.

En el acto de reconocimiento, delegados de paz, víctimas y demás participantes harán un recorrido por la calle en la que ocurrió la masacre y la renombrarán. Durante el recorrido usarán camisetas blancas y llevarán en sus manos claveles, recibirán atención psicosocial pues como asegura Silvia, "revivir el momento va a ser muy duro porque **hay personas que no hemos elaborado ese duelo**". En el colegio San Pedro Claver, los participantes podrán ver el acto a través de una pantalla.

Vea también: [[Comunidad de San José de Apartadó atemorizada por presencia paramilitar](https://archivo.contagioradio.com/comunidad-de-san-jose-de-apartado-atemorizada-por-presencia-de-grupo-paramilitar/) ]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

<div class="itemIntroText">

</div>
