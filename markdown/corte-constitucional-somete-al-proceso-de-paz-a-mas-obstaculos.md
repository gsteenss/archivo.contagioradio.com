Title: Corte Constitucional "somete al proceso de paz a más obstáculos"
Date: 2017-05-19 13:04
Category: Nacional, Paz
Tags: acuerdos de paz, Corte Contitucional
Slug: corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corte-e1495109388589.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Voces de Paz] 

###### [19 May 2017] 

A través de una rueda de prensa, los voceros de Voces de Paz, expresaron que la decisión de la Corte Constitucional sitúa al proceso de paz en uno de los momentos más difíciles que este ha afrontado, debido a las implicaciones que tiene la decisión en los trámites de los proyectos de ley y **porque lo somete a más obstáculos y saboteos por parte de quienes no quieren la paz.**

Voces de Paz señaló que “llama la atención la Corte Constitucional que se está conformando al interior del país” y que podría evidenciar la unión “**retrograda y conservadora, no comprensiva del momento histórico del país**” que deja en la incertidumbre y que pone en riesgo el desarrollo normativo de los Acuerdos de Paz y su implementación. Le puede interesar

De igual forma afirmaron que si lo que se busca es una democracia real, con debates y deliberación, el Centro Democrático tendría que estar dispuesto a participar y no debería retirarse de los debates después de plantear sus posturas “sí es cierto que la oposición de ultraderecha que hay en el Congreso de la República está dispuesta al debate democrático queremos que **participe en las discusiones y que se quede a la hora de las votaciones, para que el país sepa cómo se generan las mayorías**”.

Además, hicieron un llamado al presidente Santos para que evidencie su liderazgo y se comprometieron a aportar para que los desarrollos normativos de los acuerdos ocurran en los términos que están previstos en el texto final del Teatro Colón. Le puede interesar: ["Corte Constitucional congela Acuerdos de Paz con las FARC: Enrique Santiago"](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

### **Las FARC-EP seguirán cumpliendo lo pactado** 

Voces de Paz, en permanente comunicación con las FARC-EP, expresó que la guerrilla es enfática en asegurar que quiere continuar cumpliendo con los acuerdos tal y como lo ha venido haciendo hasta el momento y **continuar en su empeño en el tránsito a la vida civil, la construcción de su partido político**.

Sin embargo, consideran que la preocupación se debe a que la decisión de la Corte Constitucional se da a 13 días de que llegue el día D+180 en donde terminará la dejación de armas, **obligando a amabas parte a redefinir los tiempos** en los que se había establecido el calendario para cumplir con los protocolos y proyectos de ley. Le puede interesar: ["Con movilización y y pactos políticos haremos frente a la decisión de la Corte: Cepeda"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

###### Reciba toda la información de Contagio Radio en [[su correo]
