Title: Colombia recibió más de 200 recomendaciones frente a situación de derechos humanos
Date: 2018-05-10 18:35
Category: DDHH, Política
Tags: Derechos Humanos, examen periodico universal, Naciones Unidas, Organizaciones sociales
Slug: colombia-recibio-mas-de-200-recomendaciones-frente-a-situacion-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/examen-periodico-e1525972007951.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 May 2018] 

En la madrugada del 10 de mayo, se llevó a cabo el **tercer Examen Periódico Universal** donde Colombia fue examinada en el Consejo de Derechos Humanos de las Naciones Unidas en Ginebra Suiza. 86 países intervinieron y realizaron más de 200 recomendaciones al Estado colombiano. Las organizaciones sociales que participaron en la creación de insumos para este examen concluyeron que, pese al Acuerdo de Paz, Colombia se raja en derechos humanos.

Como parte del examen los Estados que intervinieron destacaron como positivo la firma del Acuerdo de Paz referenciándolo como un **avance en el cumplimiento de los derechos humanos en el país.** Sin embargo, para los Estados, aún persisten las violaciones a los derechos humanos especialmente de las mujeres, los niños, defensores de derechos humanos y poblaciones étnicas.

**Estados saludaron el Acuerdo de Paz pero, alertaron que las violaciones a los DDHH persisten**

En lo que tiene que ver con la implementación del acuerdo de paz, se realizaron alrededor de **18 recomendaciones** relacionadas con el cumplimiento al pie de la letra de lo acordado. Recomendaron que haya sostenibilidad y autonomía del Sistema Integral de Verdad, Justicia, Reparación y No Repetición.  De igual forma, cuatro países recomendaron dar continuidad a los esfuerzos para lograr un acuerdo con el ELN.

De acuerdo con Luz Estela Aponte, integrante de la organización Reiniciar, las recomendaciones hechas a Colombia significan que **“la comunidad internacional tiene los ojos puestos sobre el país”.** Afirmó que, a pesar de que resaltaron el Acuerdo de Paz, “hay problemas que persisten en el país por lo que alertó sobre situaciones que se han agravado como, por ejemplo, los ataques a las personas que defienden los derechos humanos”. (Le puede interesar:["Indígenas Sikuani víctimas de despojo de tierras en el Meta"](https://archivo.contagioradio.com/indigenas-sikuani-victimas-de-despojo-de-tierras-en-el-porvenir-meta/))

### **Estados recomendaron fortalecer la institucionalidad para proteger a defensores de derechos humanos** 

De igual manera, las organizaciones sociales destacaron que “hubo una alarma de cerca de 27 países, en donde en más del **30% de las recomendaciones** se pidió la protección de los defensores de derechos humanos”. Dijo que lo novedoso de las recomendaciones “es que los países formularon la recomendación general de fortalecer la institucionalidad para el reconocimiento de la labor de las personas defensoras e insistieron en la necesidad de adoptar medidas efectivas para la prevención, protección y judicialización de las personas responsables de los ataques”.

La alerta se dio teniendo en cuenta el aumento de los crímenes contra defensores en los territorios rurales, por lo que “no es suficiente con que existan instituciones como la Unidad Nacional de Protección, **si no se tienen medidas efectivas de prevención**”. A esta alerta se suma la preocupación de 15 países que consideraron que el tema de la impunidad es un tema que debe generar acciones concretas para garantizar el acceso a la justicia, las investigaciones efectivas y las sanciones correspondientes para evitar que la impunidad continúe creciendo y se desligue en una mayor violencia para las comunidades.

### **Colombia está peor en la protección a los derechos humanos** 

En este periodo, el número de recomendaciones que le hicieron a Colombia pasa el número de 200. Esto, para las organizaciones significa que en el país ha habido un **retroceso en materia de garantizar los derechos humanos** de las personas con la salvedad de que la firma del Acuerdo de Paz “ha sido un gran logro”. (Le puede interesar:["Misión humanitaria verificará situación de derechos humanos en el Naya"](https://archivo.contagioradio.com/mision-humanitaria-verificara-crisis-de-derechos-humanos-en-el-naya/))

Aponte indicó que haber suscrito el Acuerdo de Paz con las FARC significa **“ahorrar vidas perdidas en el conflicto armado”**. Sin embargo, en el momento que vive Colombia “se han ido evidenciando nuevos problemas de derechos humanos que estaban escondidos en tanto que la relevancia noticiosa eran las situaciones relacionadas con el conflicto armado”.

Para las organizaciones, el Acuerdo de Paz ha significado nuevos retos para el Estado y en las Naciones Unidas manifestaron que “no son suficientes los mecanismos que se aprobaron dentro del Sistema Integral de Justicia y Reparación”. Por esto, desde las organizaciones han planteado que **se deben incluir medidas efectivas** para que los mecanismos funcionen y se garantice el acceso de las víctimas a este sistema.

### **Recomendaciones no se ajustan en la totalidad a las preocupaciones de las organizaciones** 

Desde Reiniciar consideran que no hubo una relevancia significativa frente a preocupaciones como **“la ausencia de visibilizar el respeto de los derechos humanos en los proyectos económicos”**. Esto quiere decir que las recomendaciones no se hicieron considerando la responsabilidad que tiene el Estado y las empresas en materia de derechos humanos.

Dijo que este tema no se tocó en el Examen que se desarrolló a pesar de que las organizaciones realizaron un arduo trabajo por **visibilizar esta problemática**. Por esto les preocupa que “a futuro, a medida que el Estado otorga mayores beneficios a la inversión extranjera, los problemas donde la producción arrasa con los derechos humanos se van a incrementar”.

Finalmente, Aponte indicó que el paso a seguir es esperar a ver si el Estado colombiano adopta las recomendaciones que se realizaron. Por parte de las organizaciones esperan que Colombia **recoja la totalidad de las recomendaciones** y a partir de allí, empieza un proceso de seguimiento y verificación de la adopción e implementación de aquellas recomendaciones que se enfocaron en la violencia contra las mujeres, los defensores de derechos humanos, las personas en condición de discapacidad, lo pueblos étnicos y la población LGBTI.

<iframe id="audio_25902586" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25902586_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
