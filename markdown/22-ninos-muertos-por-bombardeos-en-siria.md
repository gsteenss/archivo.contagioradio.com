Title: 22 niños muertos por bombardeos en Siria
Date: 2016-10-27 07:44
Category: El mundo, Política
Tags: bombardeos en Siria, Estado Islámico, Rusia
Slug: 22-ninos-muertos-por-bombardeos-en-siria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/siria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [27 Oct 2016] 

El pasado miércoles, se registraron bombardeos en una escuela de la aldea de Has en la provincia de Idlib, que dejaron como saldo **veintidós niños y seis maestros muertos.** Anthony Lake, director ejecutivo de la UNICEF, anunció que presuntamente fueron lanzados por el ejército ruso. Le puede interesar: [Los niños y niñas sirias.](https://archivo.contagioradio.com/los-ninos-y-ninas-sirias/)

Además, Lake manifestó que estos ataques se pueden tomar como un “crimen de guerra (…) **es el ataque más mortífero contra una escuela desde el inicio de la guerra que inició hace cinco años y  medio".** Hasta el momento, la Unicef no ha dado detalles sobre los autores del ataque ni de las sanciones respectivas.

Según esta entidad, en días anteriores se habían registrado unos seis ataques aéreos en cercanías a esta escuela. En una alocución el mismo día, el viceministro ruso de Relaciones Exteriores, Serguei Riabkov, informó que **“pese el fin del cese al fuego, la aviación rusa no ha lanzado ataques los últimos días ni en esa zona**, ni en cercanías a Alepo”.

El ministro de defensa ruso, Igor Konashenkov, reiteró anunció que su gobierno está en la disposición de ordenar una nueva "pausa humanitaria en Alepo”, para garantizar que **organizaciones internacionales realicen la evacuación de enfermos, heridos y civiles.** Le puede interesar: [Estado Islámico avanza y logra controlar la mitad de Siria.](https://archivo.contagioradio.com/estado-islamico-avanza-y-logra-controlar-la-mitad-de-siria/)

Por otra parte, la organización defensora de derechos humanos Human Rights Watch, denunció que tanto el Estado Islámico como los ejércitos de intervención **están faltando al Derecho Internacional, y están perpetrando crímenes de lesa humanidad.** “Han convertido varias casas abandonadas en trampas mortales con artefactos explosivos caseros”. Según esta organización, sólo en la ciudad de Manbij **han muerto por bombas trampa 69 civiles y alertan sobre el aumento de estas cifras.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
