Title: Asesinan al gestor cultural y audiovisual, Mauricio Lezama en Arauca
Date: 2019-05-10 11:26
Author: CtgAdm
Category: DDHH, Nacional
Tags: Arauca, Arauquita, Mauricio Lezama
Slug: asesinan-al-gestor-cultural-y-audiovisual-mauricio-lezama-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Mau2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-09-at-9.38.48-PM.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

En horas de la tarde del pasado 9 de mayo fue asesinado el gestor cultural, **Mauricio Lezama**, mientras se encontraba en el sector de La Esmeralda en el municipio de Arauquita, Arauca. El comunicador fue abordado por hombres armados que se movilizaban en motocicleta que dispararon en su contra. Mauricio se encontraba en el departamento realizando diferentes entrevistas a víctimas del conflicto armado para un trabajo documental.

Según la F**undación para la Libertad de Prensa (FLIP**), los hechos ocurrieron mientras  Lezama se encontraba en el comercio local junto al camarógrafo Ricardo Llain, "tras el ataque, Mauricio perdió la vida y Llain resultó herido". Adicionalmente al trabajo que venía realizando, Mauricio avanzaba en la selección del reparto para su producción 'Mayo'.

### El documental 'Mayo'

A través de Proimágenes Colombia y el Fondo para el Desarrollo Cinematográfico, Mauricio se encaminaba a narrar las historias del conflicto armado en Arauca, un relato que comenzaría con el testimonio de  la lideresa Mayo Villareal, sobreviviente y víctima del genocidio de la Unión Patriótica.

El asesinato de Mauricio Lezama, quien hacía parte del Consejo Departamental de Cine ha sido rechazado por la comunidad de Arauquita, comunidades del séptimo arte y organizaciones como la ONU quienes destacaron a Mauricio como un gestor cultural y de paz , exigiendo celeridad en la investigación sobre el homicidio. (Lea también:[En Arauca han sido asesinadas 32 personas en tan solo dos meses](https://archivo.contagioradio.com/en-arauca-han-sido-asesinadas-32-personas-en-tan-solo-dos-meses/))

[\#Arauca](https://twitter.com/hashtag/Arauca?src=hash&ref_src=twsrc%5Etfw) Condenamos homicidio hoy en Arauquita del cineasta Mauricio Lezama, consejero de cine del departamento de Arauca y promotor de derechos de la juventud, la cultura y la paz [\#NiUnoMenos](https://twitter.com/hashtag/NiUnoMenos?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/DFkAfg8biO](https://t.co/DFkAfg8biO)

> — ONU Derechos Humanos Colombia (@ONUHumanRights) [10 de mayo de 2019](https://twitter.com/ONUHumanRights/status/1126653545657315329?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
