Title: 438 niñas y niños palestinos son prisioneros del régimen israelí
Date: 2016-04-26 18:10
Category: DDHH, El mundo
Tags: Dima al Wawi, Israel, Palestina, Unicef
Slug: ninos-ninas-presos-regimen-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/dima2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Activestlls 

###### [26 abr 2016] 

Gracias a la fuerte presión internacional, Deema al Wawi, niña palestina de 12 años de edad y la prisionera más joven que ha tenido el régimen israelí, fue liberada este martes. Sin embargo aún siguen prisioneros 438 niños palestinos, de los cuales **98 tienen menos de 16 años**, según los registros de la organización de defensa de los derechos de los presos palestinos Addameer.

Dima al Wawi, fue detenida el pasado 9 de febrero de 2016 en la entrada de un asentamiento judío cerca de Hebrón, **por llevar un cuchillo en su bolso, según indican algunas versiones.** Al encontrarle ese objeto el tribunal militar la acusó de intento de homicidio por lo que fue condenada a cuatro meses y medio de prisión. Su salida se debe a que su familia, con el apoyo de la comunidad internacional, logró pagar una fianza de 2.100 dólares.

Este caso ha puesto en el ojo público la detención y encarcelamiento de menores de edad palestinos de los territorios ocupados, que ha aumentado aceleradamente desde septiembre pasado. **De acuerdo a la Unicef Israel es la única zona donde se establece la edad de responsabilidad criminal a los 12 años.**

Además, se denuncia que como muchos otros prisioneros palestinos, **la pequeña Dima estuvo en una prisión fuera de los territorios ocupados, lo que constituye una violación de la ley internacional.** Um Rashid, madre de la menor expresó “Quizás tuviera un cuchillo cuando la detuvieron pero solo es una niña, ¿qué podría haber hecho? En Israel no tratan a sus hijos igual que a nuestros hijos”.

Según el portal web Palestina Libre, la niña ha afirmado en entrevistas a medios locales que en la cárcel conoció a niños de 13 y 14 años, también **relató que le dieron patadas durante su detención** y que fue trasladada al tribunal militar de Ofer en un autobús, sentada en un asiento de metal, sin chaqueta y **esposada y con cadenas en los tobillos.** Fue interrogada “por cinco hombres a la vez”. “Fue muy confuso me gritaban enfadados y se reían de mí”, contó.

La Unicef y la organización Defense for Children Internactional-Palestine, DCIP, ha denunciado los malos tratos a los cuales son sometidos los menores de edad que se encuentran en las cárceles israelíes, **del 97% de los 429 casos investigados, los menores fueron interrogados sin la presencia de un abogado o de sus familiares** como exige la ley, en un **88% los niños no fueron informados del motivo de su detención y más del 75% afirmó haber testificado bajo presión psicológica**. De acuerdo con el DCIP desde el año 2000 han sido detenidos, juzgados y encarcelados aproximadamente **8.000 niñas y niños palestinos**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
