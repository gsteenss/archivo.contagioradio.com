Title: "Asco" y otros recomendados del 32 Festival de cine de Bogota
Date: 2015-10-20 08:01
Author: AdminContagio
Category: 24 Cuadros
Tags: 32 Festival de Cine de Bogotá, Asco Película, Carlos Palau, El río de las Tumbas película colombiana, Henry Laguado, Julio Luzardo, La odisea del Gardel muerto
Slug: asco-y-otros-recomendados-del-32-festival-de-cine-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/asco_Featured.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [20 Oct 2015] 

Con la interpretación de recordadas bandas sonoras cinematográficas por parte de la Orquesta Sinfónica Juvenil "Batuta", se dará inicio a la edición 32 del Festival de Cine de Bogotá, el más antiguo de la capital y uno de los más tradicionales del país; con una programación académica y en salas que se extenderá hasta el 28 de Octubre.

La presentación musical, que tendrá lugar en el Teatro "Bogotá", recuperado por la Universidad Central, servirá como antesala de la película inaugural "Asco" del director brasileño Alexandre Paschoalini, escogida por el comité organizador por utilizar "nuevas narrativas que tienen su raíces en el uso del digital contemporáneo".

[![Bogocine](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Bogocine.gif){.wp-image-15974 .aligncenter width="232" height="361"}](https://archivo.contagioradio.com/asco-y-otros-recomendados-del-32-festival-de-cine-de-bogota/bogocine/)

Teatros del centro de la capital, la Cinemateca Distrital, Universidades, el Jardín Botánico, son algunos de los espacios en los que se desarrollaran actividades como la categoría documental sobre el Medio Ambiente, que en su apertura número 13 tendrá un "Anem", cantos sagrados indígenas de la Nación Shuar de la Amazonía colombiana.

El sábado 24 en la Cinemateca Distrital, se presentara el documental "Esclavas" de Alejandro Chaparro y Frida Spiwak, donde se aborda la explotación sexual y el tráfico de personas con fines de prostitución en Colombia y en algunos otros lugares del mundo, cerrando con un conversatorio con una víctima de trata, una abogada defensora, el productor y el director de la cinta.

[**Películas destacadas**]

**Asco.**

Dentro de una excelente y novedosa imagen en blanco y negro neo impresionista, con situaciones no antes presentadas en el cine de largometraje que bordean lo erótico y retratan comportamientos psicóticos, "Asco" de Ale Paschoalini, es un viaje al corazón de un joven enamorado al que no le importan las consecuencias.

Película de Inauguración. Teatro de Bogotá, 20 de octubre 7 p.m.

**El Rio de las Tumbas **

Filmada hace mas de 50 años a escasos kilómetros de Marquetalia, "El Río de las Tumbas" de Julio Luzardo es una excelente película que retrata la vida de un pueblo colombiano con su alcalde corrupto, su policía holgazán, su reinado local y el bobo del pueblo enfrentados al problema de encontrar un muerto en la rivera del río.

Sábado 24 de octubre en la Sala Fundadores, 2 p.m.

**La Odisea de Gardel muerto**

Basada en el libro de Fernando Cruz Kronfly, Carlos Palau hace una road movie colombiana por los caminos de los años treinta que de Medellín conducen a Buenaventura con el cadáver de Carlos Gardel. Salpicada de tangos y anécdotas,

Jueves 22 de Octubre, Aula Máxima de la Universidad Tadeo Lozano, 2 p.m. con presencia del director quien hará un foro al final de la misma.

**"Leche" materna**

"Milk" de Noemí Weis es un documental sobre la comercialización y las políticas que afectan la alimentación natural del infante: la leche materna y reitera como, a través de entrevistas en distintos lugares del mundo, el amamantar el tiempo necesario protege no solo la salud del hijo, sino la de la madre.

**"Minas" al desnudo**

"Minas" de Emily C. Cohen Ibañez es un mosaico basado en investigaciones etnográficas de quienes de una u otra manera han tenido que ver con el problema de los minas en los campos en Colombia: soldados, civiles, cirujanos, terapistas, representantes de corporaciones y activistas. Un llamado a la reflexión desde un punto de vista canadiense.

**"Cordillera" en extinción**

La presentación del documental sobre el Medio Ambiente "Cordillera del Cóndor Paraíso Amenazado" de Beth Wald y Lupita De Heredia, permitirá oír a Dominga Antun, mujer indígena de la nacionalidad Shuar del sur de la Amazonía ecuatoriana.  
Ella cantará un Anem que son cantos sagrados para ellos que interpretan para diferentes ocasiones, que alejan los males, protegen de mordedura de culebra, consiguen buen yerno, piden permiso a los dioses para sembrar.

Jueves 22, Jardín Botánico de Bogotá, 6 p.m
