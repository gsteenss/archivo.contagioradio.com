Title: Samurái, el poeta de las líricas del Rap
Date: 2018-01-16 15:20
Category: Cultura, Nacional
Tags: Hip Hop, Rap, Samurái
Slug: samurai-el-poeta-de-las-liricas-del-rap
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/samuari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Samurái] 

###### [16 Ene 2018] 

Héctor Everzon Hernández, conocido en la escena del Hip Hop y el Rap como Samurái, será recordado para quienes lo conocieron como uno de los artistas más entregados a la comunidad, al movimiento musical y crear letras que reflejaran la realidad dura de la cotidianidad en una ciudad como Bogotá. De acuerdo con Diana Avella, rapera, **se tiene conocimiento de amenazas que le habrían llegado al artista, sin embargo, aún no se tiene información sobre los responsables del asesinato.**

Avella, afirmó que Samurái era un **“poeta, un trabajador y una gran persona”**, además de ser el mejor letrista de Colombia, es decir uno de los raperos que más se formó en la creación de rimas a partir del estudio de la estructura lírica y la composición. Además, expresó que, entre las grandes corrientes de la poesía, Samurái encontraba afinidad con el nadaísmo y los poetas malditos.

**“Samurái era un tipo adicto a la poesía, todo el tiempo estaba leyendo e investigando las figuras, las formas literarias**, las posibilidades de escritura y eso lo plasma perfectamente en sus canciones”. Recientemente Samurái estuvo dictando un taller de composición en el marco del Festival Hip Hop al Parque. (Le puede interesar: ["Nace Independencia Records una productora para la paz"](https://archivo.contagioradio.com/independencia-records-paz/))

Los inicios del artista fueron en el grupo de rap Octavo Imperio, después continuaría su carrera como solista. Otra de las grandes habilidades que tenía Samurái, era acompasar a la perfección sus letras con los bits en sus canciones.

### **Su trabajo con la comunidad** 

Diana Avella señaló que uno de los trabajos “más bonitos” que desarrollaba Samurái tenía que ver con fomentar la cultura del rap pacífico y crítico en los barrios periféricos de la capital y sobretodo con las y los jóvenes **“Samurái era de calle, vino de una familia humilde y era brillante”** afirmó la artista y añadió que sin importar el avance en su carrera y las tarimas que recorrió de talla internacional, siempre estuvo a la disposición de los festivales barriales.

<iframe id="audio_23183411" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23183411_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
