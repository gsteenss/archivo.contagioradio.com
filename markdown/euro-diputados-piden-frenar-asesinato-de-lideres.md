Title: Euro diputados piden intervención inmediata para frenar asesinato de líderes sociales
Date: 2018-07-13 12:58
Category: DDHH, Nacional
Tags: Euro diputados, Federica Mogherini, lideres sociales, paz
Slug: euro-diputados-piden-frenar-asesinato-de-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Jul 2018] 

Un grupo de Euro parlamentarios solicitaron a la Alta representante de la Unión Europea para Asuntos exteriores y política de Seguridad, Federica Mogherini, **intervenir de forma urgente e inmediata en Colombia para frenar el asesinato contra líderes sociales.**

A través de una carta, los eurodiputados manifestaron que siguen de cerca la situación de Derechos Humanos en Colombia y su preocupación por el sistemático (y ahora acelerado) asesinato de líderes sociales. Que son en su mayoría defensores de los DDHH, del ambiente y personas que apoyan partidos de oposición.

Para los 38 parlamentarios firmantes, la **f**r**ecuencia con la que ocurren los asesinatos, así como las diversas zonas en las que tienen lugar, revelan el carácter sistemático del fenómeno,** respaldando su afirmación al señalar que desde el primero de junio del presente año "32 personas han sido asesinadas, muchas otras han sido amenazadas", y en algunos casos exiliadas.

Adicionalmente, indican que las regiones más afectadas son la costa pacífica, en especial los puertos de Tumaco y Buenaventura, por intereses económicos; así como la región de Ituango en Antioquia, donde han asesinado muchos líderes por oponerse al proyecto hidroeléctrico que allí se construye, en co-financiación con bancos europeos.

**Estos asesinatos se deben, según los diputados,  a la "reactivación de grupos armados, y en particular  a la reestructuración de grupos paramilitares"**; en concordancia con ello, los firmantes de la carta pidieron que se implemente el Acuerdo de Paz con las FARC y se pongan en marcha las instituciones creadas en dicho pacto, encargadas de velar por la seguridad de los líderes sociales.

[Euro Parlamentarios se manifiestan sobre asesinato de líderes sociales en Colombia](https://www.scribd.com/document/383796580/Euro-Parlamentarios-se-manifiestan-sobre-asesinato-de-lideres-sociales-en-Colombia#from_embed "View Euro Parlamentarios se manifiestan sobre asesinato de líderes sociales en Colombia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_31911" class="scribd_iframe_embed" title="Euro Parlamentarios se manifiestan sobre asesinato de líderes sociales en Colombia" src="https://www.scribd.com/embeds/383796580/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-OIDGINyKRG2Uq2TB3RR0&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###  

###### **[Reciba toda la información de Contagio Radio en] [[su correo]** [[Contagio Radio]
