Title: Las tres medidas urgentes para implementar el Acuerdo de Paz luego del Fast Track
Date: 2016-12-13 16:17
Category: Nacional, Paz
Tags: Amnistia, Fast Track, guerrilla, Ley de Amnistia, paz
Slug: las-tres-medidas-urgentes-para-implementar-el-acuerdo-de-paz-luego-del-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/colombia-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CNN] 

###### [13 Dic. 2014] 

**Con 8 votos a favor y 1 en contra, este 13 de diciembre la Corte Constitucional le dio un sí a la vía rápida o** ***“fast track”**,* procedimiento que permitirá tramitar las reformas legales y constitucionales que se deriven de los acuerdos, además de otorgarle al presidente Juan Manuel Santos facultades extraordinarias para expedir decretos con fuerza de ley, exclusivamente relacionados con el desarrollo del acuerdo de paz.

Luego de esta aprobación queda la pregunta abierta **¿Cuáles son las tres medidas urgentes que deberían implementarse primero luego de la aprobación del *“Fast Track”?*** para entre otras cosas dar esas garantías a la guerrilla de las Farc.

Ya en su momento, el Ministerio del Interior había manifestado que mientras se aprobaba el “*Fast Track”* terminaría de preparar un paquete de normas que las ha denominado como “de alta prioridad” y que tienen relación precisamente con aspectos fundamentales de los acuerdos de paz.

### **Ley de Amnistía** 

El primero de ellos es la **ley de amnistía, que es aquella que genera indultos a los guerrilleros que hayan cometido delitos** y por ende podría comenzarse de manera efectiva el traslado de los guerrilleros a las zonas de concentración pactadas.

Cabe resaltar que para que pueda concederse **dicha amnistía es necesario que sean incluidos otros delitos comunes que sean conexos, trámite que es aprobado por el Congreso.**  De igual modo debe crearse el Tribunal de paz, instancia que servirá para dar amnistías en casos específicos.

**La  Ley de Amnistía permitirá conceder indultos generales por  delitos políticos y es aprobada por dos tercios de los votos tanto en Cámara como en Senado.** Solo puede ser expedida por el congreso y se hace “por graves motivos de conveniencia pública y tiene la característica de cuando es amnistía recibir el olvido por parte del Estado sobre los delitos políticos cometidos por aquellos a quienes se les otorga”.

### **Garantías de Seguridad** 

Otro de los puntos importantes y en los que se espera mucho trabajo, es el tema de garantías de seguridad y lucha contra las organizaciones criminales responsables de homicidios y masacres o que atentan contra defensores de derechos humanos, movimientos sociales o políticos.

Frente a este, **el gobierno se comprometió a garantizar la implementación de las medidas necesarias para intensificar las acciones contra las organizaciones criminales, asegurar la protección de las comunidades en los territorios y a que se rompa cualquier tipo de nexo entre política y uso de las armas.**

Sin embargo, el panorama es desalentador dado que **en lo que va corrido del año han sido más de 70 asesinatos a líderes sociales y a cerca de 127 integrantes del movimiento político y social Marcha Patriótica en lo que va corrido del año.**

En este punto las FARC-EP también tiene un arduo trabajo dado que aseguraron en contribuir “de manera efectiva a la construcción y consolidación de la paz, en todo lo que resulte dentro de sus capacidades…”.

### **Comisión de Seguimiento, Impulso y Verificación** 

Una vez aprobada y aplicada la Ley de Amnistía, **la Comisión de Seguimiento, Impulso y Verificación del Acuerdo de Paz y el Consejo Nacional de Reincorporación** podrá comenzar a trabajar.

Dicha Comisión se encargará de acompañar de manera constante el avance de la implementación de los acuerdos y estará conformada por quienes lideraron las conversaciones en La Habana.

Quienes integraran dicha comisión por parte de las FARC-EP ‘Iván Márquez’, ‘Victoria Sandino’ y ‘Jesús Santrich’. Y en representación del Gobierno estarán Sergio Jaramillo, el ministro del Interior, Juan Fernando Cristo, y el alto consejero para el Posconflicto, Rafael Pardo.

Uno de los primeros trabajos de los integrantes, será evaluar cómo se encuentra la implementación del Acuerdo que fue frenado luego de los resultados del plebiscito del pasado 2 de Octubre.

Por su parte, en **el Consejo Nacional de Reincorporación estarán ‘Pastor Alape’ y ‘Jairo Quintero’, pero hasta el momento el Gobierno no ha dicho quienes estarán ahí.**

Para este proceso, en el que se implementan cada uno de los puntos del acuerdo, como ha sucedido hasta ahora, existirá un acompañamiento internacional de varios países y de entidades.

A grandes rasgos, **lo que le espera en materia de trabajo para esta fase de implementación de los acuerdos de paz al país es una ardua labor. **Le puede interesar: [Se están activando mecanismos para la implementación de Acuerdo de Paz](https://archivo.contagioradio.com/activados-mecanismos-para-la-implementacion-de-acuerdo-de-paz/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
