Title: Habitantes del Tamarindo serán desalojados de sus viviendas
Date: 2015-12-06 08:46
Category: DDHH, Nacional
Tags: Asotracampo, Barranquilla, conflicto armado, Desalojo, Juan Martinez, Norma Balduvino, Reubicación, Tamarindo
Slug: habitantes-del-tamarindo-seran-desalojados-de-sus-viviendas
Status: published

###### [Foto: Contagioradio.com] 

###### [6 Dic 2015] 

Para el próximo Lunes el ESMAD tiene la orden de desalojar a las familias que residen en la población de Tamarindo en Barranquilla, un territorio que limita con la zona franca ([Intereses económicos detrás del desalojo en Tamarindo](https://archivo.contagioradio.com/alcaldia-de-barranquilla-quiere-desalojar-a-57-familias-del-tamarindo/)) de la ciudad atlántica, por lo cual, el precio de sus tierras se ha disparado en los últimos años. Sin embargo, la comunidad busca que se frene este proceso mediante acciones legales, jurídicas y políticas elaboradas con el apoyo de organizaciones de Derechos Humanos.

El representante legal de la Asociación de Desplazados y Campesinos del Tamarindo Asotracampo y reclamante de tierras Juan Martínez, afirmó que durante los 14 años que llevan las más de 134 familias en el lugar, han sido objeto de persecuciones y se les han vulnerado sus derechos, revictimizando a la comunidad que es víctima del conflicto armado en Colombia.

Los pobladores del Tamarindo han solicitado reiteradamente un proceso de reubicación en donde puedan continuar desarrollando sus proyectos productivos. Norma Balduvino, habitante de Tamarindo y secretaria de Asotracampo expone que "la alcaldía ofreció 24 viviendas para todas las familias. Además de que esos espacios planteados son insuficientes, nosotros buscamos continuar realizando actividades agrícolas en un entorno rural".

Juan Martínez reitera que las familias que ocupan esos terrenos, están allí "por causas ajenas a nuestra voluntad. Las personas de Tamarindo tuvimos que marcharnos de nuestras tierrras y nos instalamos aquí, con el propósito de habitar un territorio, trabajar la tierra y rehacer nuestras vidas de una manera digna".

De igual manera, Martínez afirmó que los instigamientos por parte de la alcaldía de Barranquilla, los propietarios de las tierras y el accionar del ESMAD, ha sido frecuente sobre todo en la etapa de la construcción de las zonas francas en La Cayena, Barranquillita y Galapa, que limitan con la población de Tamarindo que con sus 120 hectáreas de extensión, se convierte en un punto de interés para negocios e inversiones inmobiliarias y comerciales.

Según el informe de Alfredo Molano sobre la pobreza y la migración de las poblaciones del Atlántico por medio del conflicto armado, "Tamarindo está dividido en cuatro parcelas: El Mirador y Natacha, reclamados por Julio Muvdi; Beitjala, de Invermas S. A., del que hace parte Henry Char Muvdi, y Granja Catalina, de Negocios Abdala Tarud S. A" y entran en conflicto con las familias que de buena fe consideraban esos territorios como baldíos.
