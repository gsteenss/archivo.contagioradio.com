Title: Trump no es el problema…
Date: 2017-01-31 11:10
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Donald Trump, Estados Unidos
Slug: trump-no-es-el-problema
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Trump-no-es-el-problema.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gage Skidmore 

#### [**Por:[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

**Hoy en día, el mundo necesita desgraciados como Trump**... él es un tipo de esos que no tiene miedo a defender lo que significa Estado Unidos en términos de su criminal política exterior. Un tipo que ayuda a visualizar que las potencias son la causa y el malestar de todos los problemas de la periferia; es un tipo que con sus explosivas declaraciones desenmascara a esos liberales hipócritas que andan hablando de libertad, de democracia, de libre competencia, que hablan de privatización, de excluir al Estado de todo, pero que pujan en partidos de golf y excelentes comidas, para que el dinero público o un acto legal siempre salve todos sus fracasos y toda su incoherencia ideológica.

Veaaaa pueeess…. la gente misteriosamente hasta ahora se percata de que un presidente norteamericano es un completo cretino… ¿acaso los anteriores eran hombres de Estado?... aquí lo que se está imponiendo es el símbolo, es la tergiversación de la realidad y no la realidad, un viejo problema que Hegel y Marx explican magistralmente.

Mejor un desgraciado como Trump y no un demócrata como Obama, como Clinton, como Hollande, como Merkel, que aparecen simbolizados como grandes gurús de la retórica, la política y el estilo, pero que al final terminan bombardeando países, financiado grupos terroristas, vendiendo armas al que las pague, sacando ventaja con TLC, saliendo aplaudidos, admirados de no sé dónde, y hasta con premio nobel.   La ventaja de un desgraciado como Trump, es que permite ver claramente a qué se dedica Estados Unidos en términos de política exterior. Qué misteriosa efervescencia negativa contra un presidente norteamericano como lo es Trump, pero que beneplácito con un nobel como Obama, quien también como muchos presidentes norteamericanos, tiene manchadas las manos de sangre inocente.

**La gente ya odia a Trump**, lo odia exclusivamente por sus discursos, lo odia hoy y ahora, porque como el odio sin comprensión real de por qué se odia resulta en algo tan efímero, imagino que no odian a Bush, pues ya él “pasó de moda”. Odian a Trump porque es un bocón, pero no “odian” a Obama por lo acontecido en medio oriente… ¿Quiere pruebas? mire cómo está Afganistán, Irak, Libia, Túnez, Siria, Gaza, Yemen.... y no vengan con que esos países eran nidos de terroristas pues dichos países estaban en paz. Tampoco vengan convenientemente a decir que tenían armas de destrucción masiva, pues eso solo fue un cuento gringo. Y mucho menos digan que Obama mandó bombardear países en el nombre de la democracia, porque los gringos adoran al califa quita cabezas de Arabia Saudita, e incluso le dieron mundial a los monarcas de Qatar y yo que sepa a ellos misteriosamente nadie les critica su tiranía, ni nadie aboga por “participación política” en sus reinos.

¡No me vengan con el tema de los derechos humanos, si estos al final valen más para unos que para otros! ¡no me vengan con que Estados Unidos bombardea en nombre de la libertad si por libertad comprendemos una estatua con firmes intereses económicos! ¡no me vengan con que odian a Trump, pero aman a Obama o no se acuerdan qué hizo Bush, qué hizo Clinton, qué hizo Reagan!

Al final, mejor un desgraciado que se identifique, para tener motivos más claros para enfrentarlo y no un hipócrita neoliberal, que mientras te obliga a decir "viva la libertad" te va clavando el puñal en toda la espalda, que mientras grita “viva la democracia” bombardea al que no sea lacayo, que mientras grita “viva el libre mercado” imprime los billetes que se reparten a través de créditos bancarios para que sean pagados con el trabajo de los ciudadanos y así seguir enseñando que el socialismo ha fracasado.

**¿Quiere más pruebas?** mire las marchas contra Trump... son multitudinarias.  Pero son multitudinarias porque son ¡contra Trump! no contra el verdadero problema, es decir, la política exterior criminal de los Estado Unidos.  La gente individualiza los problemas porque ante la negación simbólica que imponen desde los medios para evitar verlos claramente, ahora muchos creen que "como por arte de magia" Estados Unidos "quedó en malas manos" y ahora si será "un peligro para el mundo".

Peligro ya lo era, desde hace mucho... su política exterior, criminal e invasiva lo demuestran. Es tan indignante que hasta ahora surjan indignados contra un presidente norteamericano… parece que quedó en el aire la idea de que Obama era un buen tipo... ¿Acaso quién podría dudar de un nobel de paz? ¿Acaso quién podría dudar de un tipo que la cnn siempre mostró tan caballeroso? Tal vez, sí sé quién… ¡¡los afganos, los sirios, los iraquíes, los palestinos, los libios, los osetios!!

¿Acaso a Hitler no era un tipazo decentísimo en su entorno social? ¿Acaso antes que los nazis, no fueron todos esos distinguidos señores ingleses, los que fundaron los primeros campos de concentración en el mundo? ¡¡a claro, pero como fue en Sudáfrica!! ¿¿entonces fueron más malos los nazis???

¡¡reaccionada colombiano!!! todo es una batalla simbólica que el sujeto debe enfrentar, y solo el sujeto asociado con otros, podrá comprender el tamaño del engaño...

Estamos perdidos si creemos que podemos individualizar los problemas políticos... creer que un problema político y social es culpa "de una persona" es un error orquestado, creer que ahora sí, como subió Trump entonces Estados Unidos "se ve como peligro y malo" es un absurdo juego de dominio inconsciente a través del símbolo, de los medios, que están repletos de periodistas, pero de poquísimos comunicólogos.

Es un juego de control inconsciente, donde se desplaza la razón verdadera de las problemáticas y se reemplaza por modas de “amar u odiar”, modas en las que la gente se mete para considerarse justa o indignada, no porque realmente lo haya deducido, sino porque sencillamente lo "vio" "lo consumió".

Hoy hay muchos que odian con el alma a Trump, y nunca “odiaron” con el alma a Obama, o no recuerdan ni quién carajos era Bush o lo que hizo, hoy esos muchos, son los que se pueden denominar: idiotas útiles. Sujetos que consumen información, pero no deducen a partir de la comprensión de los problemas. Cómo dirían las mamás, “no saben ni dónde están parados”.

#### **[Ver más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
