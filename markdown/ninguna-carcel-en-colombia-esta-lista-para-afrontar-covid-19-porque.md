Title: Ninguna Cárcel en Colombia está lista para afrontar Covid 19
Date: 2020-03-20 23:05
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Mvimiento carcelario, vida, virus
Slug: ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde el pasado 15 de marzo el Movimiento Carcelario ha emitido comunicados de prensa exigiendo medidas urgentes para garantizar la vida de la población carcelaria ante Covid-19. Las y los internos y sus familiares denuncian que ninguna Cárcel en Colombia está lista para afrontar Covid 19 porque INPEC no toma medidas de protección.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel El Buen Pastor-Bogotá
----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 17 de marzo, las mujeres reclusas en la Cárcel El Buen Pastor, en Bogotá, manifestaron que fueron víctimas de acciones desmedidas por parte del INPEC, al evitar su ingreso a los patios, debido a que los guardias no están utilizando ni tapabocas, ni se encuentran acuartelados, hecho que los expone diariamente a contraer el virus y luego transmitirlo a las prisioneras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumado a este hecho, según las reclusas, **la directiva de este centro penitenciario no permitió el ingresó de encomiendas,** con las que los y las familiares intentaban aprovisionarlas con elementos para su protección.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la gestora de derechos humanos, 15 reclusas estarían heridas producto de la intervención del ESMAD que lanzó gases lacrimógenos, sin respetar protocolos en contra de las mujeres.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Cómbita-Boyacá
------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La población reclusa de esta cárcel denunció que desde el pasado fin de semana, las visitas fueron restringidas. Sin embargo, el cuerpo de custodia y vigilancia, administrativos, personal médico, contratistas del suministro de alimentos y contratistas de obras de mantenimiento, **siguen entrando y saliendo de la cárcel**, hecho que implica exponer a los prisioneros a un alto riesgo de contagio de Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la Guardia del INPEC tampoco estaría utilizando ningún tipo de protección como tapabocas o guantes para evitar propagar el virus. (Le pude interesar: ["Sin desinfectantes ni tapabocas. Población carcelaria en alto riesgo por COVID 19"](https://archivo.contagioradio.com/sin-desinfectantes-ni-tapabocas-poblacion-carcelaria-en-alto-riesgo-por-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel Erón Picota-Bogotá
-------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los prisioneros de esta Cárcel denunciaron que el 19 de marzo, la directiva de esta cárcel permitió el ingreso de familiares para realizar la compra de vales y así proveer a las personas de alimentos dentro del centro reclusorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, en las afueras de la cárcel, no se tomaron las medidas suficientes para salvaguardar la vida de las personas que están tanto dentro de la cárcel, como quienes fueron a visitarles. (Le puede interesar:["Fuerte incendio luego de intervención del INPEC en cárcel La Picota"](https://archivo.contagioradio.com/fuerte-incendio-luego-de-intervencion-de-inpec-en-carcel-la-picota/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel Picaleña-Ibague
----------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los internos de este centro de reclusión denuncian que **solo han tenido agua una hora al día,** hecho que dificulta que puedan adelantar jornadas de aseo, o incluso cumplir con el protocolo de defensa del Covid 19 de lavarse al menos cada tres horas las manos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, aseguran que no se ha tomado ningún tipo de precaución en tanto a la protección de los reclusos, la atención en salud sigue siendo precaria, mientras que la guardia del INPEC no aplica ninguna medida de acuartelamiento o que evite la propagación del virus.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Valledupar o Tramacúa
-------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los internos de esta cárcel están solicitando que, producto del hacinamiento al que son sometidos, **las puertas entre las celdas y los patios queden abiertas para permitir la movilidad** de las personas y así evitar contagios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este mismo motivo, el movimiento carcelario le está solicitando al Ministerio de Salud que de forma urgente entregue dotación a los centros de reclusión de elementos como tapabocas, jabón antibacterial y desinfectantes que permitan mantener estos lugares de forma higiénica y resguardando la vida de las personas dentro de las cárceles.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Ninguna Cárcel en Colombia está lista para afrontar virus
---------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado Uldarico Flórez, integrante de la Brigada Jurídica Eduardo Umaña, afirma que **ninguna cárcel en Colombia está lista para afrontar este virus**, razón por la cual asegura que deben tomarse acciones de forma inmediata desde la presidencia con Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma asegura, que si el director del INPEC "**no asume esta crisis, debería también ser retirado del cargo** y reemplazado por alguien que tenga las capacidades para evitar este riesgo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente aseguró que entre las medidas de protección a la población reclusa, deben estar las **brigadas médicas** que realicen visitas para notificar que no existan contagios, al igual que modificar las comidas que actualmente reciben las personas, por dietas que ayuden al sistema inmunológico. (Le pude interesar:["Seguimiento coronavirus Colombia"](https://coronaviruscolombia.gov.co/Covid19/index.html))

<!-- /wp:paragraph -->
