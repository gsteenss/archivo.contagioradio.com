Title: Episodio de las Brigadas militares estadounidenses es ejemplo de la política exterior colombiana
Date: 2020-07-07 23:28
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Brigadas de Estados Unidos, Congreso de la República, Fuerzas militares, Presidente Ivan Duque, Tribunal Administrativo de Cundinamarca
Slug: episodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Brigadas-militares-estadounidenses.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Anoche se cumplió el término de la orden que emitió el Tribunal Administrativo de Cundinamarca **al presidente Iván Duque para que suspendiera la autorización que había concedido a Brigadas de militares estadounidenses para realizar actividades en el territorio nacional**, enviando un mensaje al Gobierno de no saltar los conductos institucionales y las competencias de control legislativo que tiene el Senado de la República en relación con estos temas, en virtud del [artículo 173-4](https://www.constitucioncolombia.com/titulo-6/capitulo-4/articulo-173) de la Constitución. (Lea también: [Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La decisión del Tribunal se [notificó formalmente](https://consultaprocesos.ramajudicial.gov.co/Procesos/NumeroRadicacion#DetalleProceso) al Gobierno el día 2 de julio y teniendo en cuenta que **el Tribunal concedió 2 días hábiles al presidente para cumplir el fallo**, ayer lunes **las tropas tuvieron que haberse retirado para dar cumplimiento al mismo.  **(Le puede interesar: ["Brigadas de EE.UU. en Colombia tiene potestad para asesorar grupos irregulares"](https://archivo.contagioradio.com/brigadas-de-ee-uu-que-llegaron-a-colombia-tienen-potestad-para-asesorar-grupos-irregulares/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Iván Cepeda, principal accionante en la tutela, [señaló ante los micrófonos de Contagio Radio](https://www.facebook.com/watch/live/?v=282066349783725) que el ingreso de estas Brigadas, **denota el nivel de sumisión de Colombia frente a la política estadounidense.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Cepeda señaló que esta sería la primera vez en la historia contemporánea del Senado que se discuta si tropas extranjeras pueden transitar o permanecer en territorio colombiano pese a que este ingreso se ha dado previamente en varias ocasiones.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El Gobierno colombiano tiene una actitud absolutamente sumisa cuando se trata de ofrecer nuestro territorio como plataforma militar \[…\] pero cuando se trata de acoger las normas internacionales de DD.HH. \[…\] o el escrutinio internacional sobre el asesinato de líderes sociales y el cumplimiento del Acuerdo de Paz, entonces ahí sí aparece una falsa soberanía y una actitud soberbia y agresiva»
>
> <cite>Iván Cepeda, Senador de la República y accionante en la tutela.</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Pérdida de control legislativo con la virtualidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

Varios sectores y congresistas se han opuesto a esta nueva forma de sesionar en el Congreso. Señalan por una parte que esto es **inconstitucional** en la medida en que la Ley 5° del Congreso no contempla la virtualidad como forma para deliberar y que las sesiones se iniciaron solo después del Decreto 491 emitido por el presidente Duque cuyo texto establece: «*los órganos, corporaciones (…) de todas las ramas del Poder Público podrán realizar sesiones no presenciales cuando por cualquier medio sus miembros puedan deliberar y decidir*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto **según el expresidente de la Corte Constitucional, Alfredo Beltrán, supeditó la labor del Congreso al Poder Ejecutivo representado por el Gobierno** «*No se puede dejar que quede a expensas de lo que diga u ordene el presidente*», expresó el exmagistrado. Quien añadió que no es potestad del presidente modificar leyes orgánicas como la ley 5° de 1992. Por otra parte, varios miembros del legislativo han señalado que esta forma de sesionar ha favorecido el hecho de que las mayorías silencien a las voces disidentes en las dos cámaras.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1255613306917851137","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1255613306917851137

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1255604163242864646?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1255604163242864646%7Ctwgr%5E\u0026amp;ref_url=https%3A%2F%2Fcolombianoindignado.com%2Fsilencian-microfonos-y-chats-denuncian-supuesta-censura-en-sesiones-virtuales-del-senado%2F","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1255604163242864646?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1255604163242864646%7Ctwgr%5E&ref\_url=https%3A%2F%2Fcolombianoindignado.com%2Fsilencian-microfonos-y-chats-denuncian-supuesta-censura-en-sesiones-virtuales-del-senado%2F

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Ingreso de tropas extranjeras en plena crisis de las Fuerzas Militares

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los recientes hechos delictivos en los que se han visto inmersos integrantes de las Fuerzas Armadas como la violación de dos niñas indígenas en Risaralda y Guaviare, los seguimientos e interceptaciones ilegales, el bombardeo de niños y el resurgimiento de una directiva militar similar a la que propició en su momento los casos de ejecuciones extrajudiciales; los cuales, se han revelado en lo que va corrido del periodo de Duque en la Presidencia, han llevado a las Fuerzas Militares a una crisis de credibilidad y legitimidad, y al cuestionamiento de varios sectores sobre el poco control que tiene el Gobierno sobre las mismas. (Lea también: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto también invita a pensar que si el Gobierno, con la ocurrencia de todos estos hechos,  no ha mostrado tener control sobre las Fuerzas Armadas que están bajo su propia dirección; cuál podría ser el manejo que se tenga sobre unidades militares extranjeras que atienden a una línea de mando diferente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumado a lo anterior, están las denuncias que recayeron sobre militares estadounidenses en Colombia por parte de la Comisión Histórica del Conflicto y sus Víctimas donde se aseguraba que al menos 53 menores habían sido violadas por parte de soldados de dicha nacionalidad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
