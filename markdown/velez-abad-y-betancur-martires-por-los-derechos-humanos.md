Title: Vélez, Abad y Betancur: Mártires por los derechos humanos
Date: 2018-08-24 22:43
Category: Sin Olvido
Tags: Carta a una sombra, Héctor Abad Gómez, marcha de claveles rojos
Slug: velez-abad-y-betancur-martires-por-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-24-at-5.30.51-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### 25 Ago 2018 

Hace 31 años, el 25 de agosto de 1987 quedó escrita en la historia como una de las fechas más oscuras y devastadoras para los defensores de derechos humanos en Colombia. En el centro de Medellín sicarios paramilitares acabaron en un lapso de 10 horas con la vida de los docentes y defensores de la vida Luis Felipe Vélez, Héctor Abad Gómez y Leonardo Betancur Taborda.

<iframe id="audio_28076173" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28076173_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El primer asesinado fue Luis Felipe, quien a sus 33 años de edad, ya era Presidente de la Asociación de Institutores de Antioquia (ADIDA), Directivo de la Federación Colombiana de Educadores FECODE, y reconocido luchador por los derechos sindicales de su gremio. Ese martes, a las 7:30 de la mañana, miembros de la organización paramilitar 'Amor por Medellín', lo acribillaron con ráfaga de ametralladora frente a la sede de la organización que presidia. Paradójicamente, algunos días antes en un discurso había pronunciado una frase casi premonitoria “¡A la vida por fin daremos todo, a la muerte jamás daremos nada!”.

Del asesinato de Luis Felipe fue informado el médico Héctor Abad Gómez, un hombre que dedicó su vida a la docencia, la medicina y los derechos humanos. Sus 66 años de vida le alcanzaron para ser profesor de la facultad de medicina de la Universidad de Antioquia y catedrático en salud pública. Como periodista fundó el periódico universitario U-235 y fue columnista en El Tiempo, El Mundo y El Colombiano, participando también en espacios de radio, donde expresaba su opinión, cuestionaba y realizaba denuncias de la violencia en Colombia, las desapariciones forzadas, los secuestros cometidos por las guerrillas y también los delitos cometidos por los paramilitares, entre ellos, crímenes selectivos a líderes sociales y a miembros o simpatizantes de la Unión Patriótica (UP); convirtiéndose en un blanco para los grupos a quienes les incomodaban sus posturas.

Abad Gómez fue un hombre que defendió la vida, denunciaba las condiciones básicas que propagaban la desigualdad en Colombia y siempre se opuso a los crímenes cometidos contra los inocentes, responsabilizando al Estado y al gobierno, como actor principal en la promoción de la pobreza, la injusticia y la violencia del país, indicando que el poder y las prioridades del mismo debían cambiar. Después de retirarse como docente se dedicó al cultivo de rosas y al trabajo en el Comité de Derechos Humanos de Antioquia, del cual fue presidente hasta su muerte.

Antes de ser asesinado, Abad Gómez lideró una manifestación recordada como 'La marcha por los claveles rojos' donde tres mil personas protestaron contra la violencia sistemática contra estudiantes y profesores de la Universidad de Antioquia simpatizantes de ideas de izquierda. A su lado caminaron Vélez y Leonardo Betancourt Taborda de 41 años, quien también empeño su vida a la salud y la educación, estuvo dedicado principalmente a la lucha de los docentes, y era miembro y militante activo de la UP. Denunciando diferentes formas de violencia entre 1970 a 1987 como consecuencia principalmente de la corrupción y el narcotráfico.

Alertados por la noticia de la muerte de Vélez, Abad y Betancur llegaron a la sede de ADIDA, allí fueron baleados aproximadamente a las 5 de la tarde, los tres el mismo día, en el mismo lugar; con el claro objetivo de “anular cerebros” por parte de los paramilitares. Después de su muerte se decretó un paro de 72 horas por parte del magisterio antioqueño y se elaboró un pliego donde se manifestaban su apoyo al derecho a la vida, las libertades políticas y democráticas. Luego de la muerte de Abad, el reemplazo de la presidencia del comité de derechos humanos fue Jesús María Valle, quien sería asesinado en 1998.

**El proceso jurídico**

Después de cerrado el caso, el 13 de febrero de 2012, la Fiscalía General de la Nación reabrió la investigación tras las declaraciones aportadas por el jefe paramilitar Diego Fernando Murillo alias “Don Berna”, quien aseguró que el responsable de los crímenes fue Carlos Castaño, quien consideraba a Héctor Abad como un guerrillero del Ejército Popular de Liberación (EPL).

Para 2014, la misma entidad declaró estos crímenes como de lesa humanidad, tras comprobar la sistematicidad en los tres casos y como parte del genocidio de la UP, cometido en coordinación de entidades estatales, narcotraficantes y paramilitares. En lo corrido de 1987, fueron asesinados 15 docentes más de ADIDA, y según la organización, desde el año de los asesinatos hasta 2008, 334 miembros de la misma corrieron con la misma suerte.

**Legado**

Cada 25 de agosto se recuerda a estos anunciadores de verdades y quienes han evidenciado que su lucha es dura y peligrosa dentro de un juego político y económico. Cada víctima ha evocado una memoria diferente, ninguna de ellas ha sido olvidada, el doctor Betancur es recordado por sus amigos, familiares y algunos de sus alumnos de la Universidad de Antioquia; Vélez, es memorable por su labor con los docentes al dejar el Fondo Solidario por Muerte a Educador, de igual forma un grupo de docentes que lo admira creo el Equipo Magisterial Luis Felipe Vélez que recoge las banderas de este líder.

De los tres, es tal vez Abad el más conmemorado. Su hijo Héctor Abad Faciolince, le dedicó su libro 'El Olvido que seremos', título que toma de las letras manchadas con sangre encontradas en el bolsillo de su padre con el verso de Borges. Un libro que inspiraría a su nieta Daniela Abad para realizar junto con Miguel Salazar el documental “Carta a una sombra” estrenado en el año 2015. Su [marcha de claveles rojos](https://archivo.contagioradio.com/marcha-claveles-rojos/) se sigue realizando en Medellín y cada 25 de agosto la facultad de salud pública de la Universidad de Antioquia celebra el día nacional del salubrista, así como la realización de la cátedra de formación ciudadana Héctor Abad Gómez. El año pasado la Fundación para la Libertad de Prensa (FLIP), publicó una serie de podcast en conmemoración de 30 años de su asesinato.

Vélez, Abad y Betancur... en la memoria

Vélez, Abad y Betancur... Sin Olvido
