Title: En Septiembre se conocerán los nombres de magistrados del Tribunal de Paz
Date: 2017-07-04 16:12
Category: Judicial, Nacional, Paz
Tags: acuerdos de paz, Tribunal de Paz
Slug: en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Público] 

###### [04 Jul 2017] 

**El Comité de Escogencia público las características con las que tendrán que cumplir los 51 magistrados** que van a conformar el Tribunal de Paz, el director de la Unidad de Investigación y Acusación y el Director de la Unidad de Búsqueda de Personas Desaparecidas y el 26 de septiembre se tendrán los resultados de los seleccionados.

Los aspirantes deberán estar altamente calificados y tendrán que **tener conocimiento en del Derecho Internacional Humanitario o resolución de conflictos** y podrán tener cualquier especialidad en el Derecho. De igual forma el Comité de Escogencia señaló que tendrá en cuenta los estándares de independencia judicial de los postulados y sus altas calidades morales. (Le puede interesar: ["ECOMÚN la primera cooperativa de las FARC aún no tiene los recursos prometidos"](https://archivo.contagioradio.com/ecomun-la-primera-cooperativa-de-las-farc-aun-no-tiene-los-recursos-prometidos/))

De igual forma el Comité manifestó que e**l proceso también estará guiado por los principios de participación equitativa en términos de género y respeto a la diversidad étnica, territorial y cultural**.  Los aspirantes no tendrán que ser funcionarios de carrera y no se aplicará ninguna limitación en edad como requisito de selección, sin embargo, si deberán tener perfecto dominio del idioma español.

### **Requisitos de los Magistrados del Tribunal de Paz ** 

El Tribunal estará conformado por 20 Magistrados del Tribunal para la Paz que deberán cumplir con los siguientes requisitos:

1.Ser colombiano (a) de nacimiento y ciudadano (a) en ejercicio.

2.Ser abogado (a)

3.No haber sido condenado por sentencia judicial a pena privativa de la libertad, excepto por delitos públicos o culposos.

4.Haber desempeñado, durante 15 años un cargo en la rama Judicial o en el Ministerio Público, o haberse desempeñado como abogado o la cátedra universitaria en disciplinas jurídicas en establecimientos reconocidos oficialmente.

### **Requisitos para los aspirantes a Magistrados de Sala** 

Estará conformado por 18 magistrados que deberán cumplir con las siguientes condiciones:

1. Ser colombiano (a) de nacimiento y ciudadano (a) en ejercicio y estar en pleno goce de sus derechos civiles.

2\. Tener título de abogado (a) expedido o revalidado conforme a la ley.

3.No estar incriminado en causal de inhabilidad o incompatibilidad.

4.Tener experiencia profesional por un lapso no menor a 8 años. La expedriencia deberá ser adquirida con posterioridad a la obtención del título de abogado (a), en actividades jurídicas ya sea de manera independiente o en cargos públicos o privados o en el ejercicio de la función judicial.

Este Tribunal de Paz tendrá como funciones juzgar e imponer sanciones a los responsables en los delitos en el marco del conflicto armado, específicamente en los crímenes de lesa humanidad, genocidios, graves crímenes de guerra, entre otros.

### **Requisitos para la postulación ** 

Las personas interesadas en postularse para estos cargos deben presentar:

1.Cédula de Ciudadanía

2.Tarjeta Profesional

3.Declaración de las motivaciones para aspirar al cargo.

4.Certificaciones laborales, en dado caso de tener publicaciones, material audiovisual o textos relacionados con las funciones del cargo, es necesario referenciarlas y deben hacer una declaración bajo juramento de ser ciertos y verídicos todos los datos aprobados.

 

[Convocatoria Magistrados(as) JEP](https://www.scribd.com/document/352923888/Convocatoria-Magistrados-as-JEP#from_embed "View Convocatoria Magistrados(as) JEP on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_28180" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/352923888/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Xw9Zs03wr9R9XJ1rBOHp&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
