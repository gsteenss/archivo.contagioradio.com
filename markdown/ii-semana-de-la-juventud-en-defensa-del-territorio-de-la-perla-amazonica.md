Title: II Semana de la Juventud en defensa del territorio de la Perla Amazónica
Date: 2020-11-04 10:07
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Putumayo, Semana de la Juventud, Zona de Reserva Campesina Perla Amazónica
Slug: ii-semana-de-la-juventud-en-defensa-del-territorio-de-la-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_5059-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_5058.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_5417.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0034-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9901-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9894-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0137-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0211-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0375-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0307-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9520-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9465-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4822-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0456-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9842-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9890-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0237-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4742-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4742-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/IMG_9877-1-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Fotos: Zona de Reserva Campesina Perla Amazónica: Contagio Radio / Adispa

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el marco de la segunda edición de la Semana de la Juventud, ad portas de la conmemoración de los 20 años de construcción de identidad campesina de la **Zona de Reserva Campesina Perla Amazónica (ZRCPA) en Putumayo,** más de 60 jóvenes acudieron al encuentro del 22 al 25 de octubre donde compartieron experiencias, saberes y aprendizajes para reconocer y cuidar la Amazonía como territorio de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con la gestión de la **Asociación de Desarrollo Integral Sostenible (ADISPA**), organización comunitaria responsable del proceso de reactivación de la Perla Amazónica, desde el 22 hasta el 25 de octubre, y bajo todas las medidas de seguridad en medio de la pandemia, se busco la integración de niños, niñas y jóvenes de todas las edades como una forma de fortalecer el tejido social y el sentido de pertenencia de los habitantes de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La zona de reserva que está conformada por las 23 veredas de **Agualongo, Alea, Angostura, Bajo Cuembí, Bajo Mansoyá, Bajo Lorenzó, Baldío, Belén, Bocana del Cuembí, Buen Samaritano, Camios, Comandante, Chufiyá, Guadalupe, Juvenil, La Española, La Frontera, La Piña, La Rosa, Puerto Playa, San Salvador, Sevilla y Toayá** cuenta con una extensa y rica variedad de flora y fauna que permitió que sus habitantes pudieran explorar los cielos en búsqueda de aves en ejercicios de avistamiento, cabe señalar que Colombia es el país número uno en diversidad de aves en el mundo, con cerca de 1.876 especies.

<!-- /wp:paragraph -->

<!-- wp:jetpack/slideshow {"ids":[92034,92036,92070,92122,92126],"sizeSlug":"full"} -->

<div class="wp-block-jetpack-slideshow aligncenter" data-effect="slide">

<div class="wp-block-jetpack-slideshow_container swiper-container">

-   <figure>
    ![ZRCPA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_5059-1.jpg){.wp-block-jetpack-slideshow_image .wp-image-92034}  
   <figcaption class="wp-block-jetpack-slideshow_caption gallery-caption">
    Foto: ADISPA
    </figcaption>
    </figure>
-   <figure>
    ![ZRCPA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_5058.jpg){.wp-block-jetpack-slideshow_image .wp-image-92036}  
   <figcaption class="wp-block-jetpack-slideshow_caption gallery-caption">
    Foto: ADISPA
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9520-1-scaled.jpg){.wp-block-jetpack-slideshow_image .wp-image-92070}  
   <figcaption class="wp-block-jetpack-slideshow_caption gallery-caption">
    Foto: Contagio Radio
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9465-1-scaled.jpg){.wp-block-jetpack-slideshow_image .wp-image-92122}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4822-scaled.jpg){.wp-block-jetpack-slideshow_image .wp-image-92126}
    </figure>

[]{.wp-block-jetpack-slideshow_button-prev .swiper-button-prev .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-next .swiper-button-next .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-pause}

<div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white">

</div>

</div>

</div>

<!-- /wp:jetpack/slideshow -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, se incentivó a los y las jóvenes en actividades como la astronomía a través de la creación de cohete de agua caseros y cajas diseñadas para ver eclipses solares, con ello no solo se les motivó a conocer su territorio sino también a explorar aquellos espacios que permanecen ocultos a nuestros ojos. [(Lea también: La apuesta por la vida de comunidades campesinas en Putumayo)](https://archivo.contagioradio.com/la-apuesta-por-la-vida-de-comunidades-campesinas-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La observación durante las noches, permitió no solo la identificación de satélites y constelaciones, a su vez se logró observar los planetas de Marte, Júpiter y Saturno, además de resaltar los avances que han existido para la astronomía en el país y en general el mundo en la continúa búsqueda de darle un sentido a los misterios que aguardan en la galaxia.

<!-- /wp:paragraph -->

<!-- wp:jetpack/slideshow {"ids":[92048,92050,92056,92140],"sizeSlug":"full"} -->

<div class="wp-block-jetpack-slideshow aligncenter" data-effect="slide">

<div class="wp-block-jetpack-slideshow_container swiper-container">

-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0034-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92048}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9901-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92050}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9894-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92056}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9890-1-scaled.jpg){.wp-block-jetpack-slideshow_image .wp-image-92140}
    </figure>

[]{.wp-block-jetpack-slideshow_button-prev .swiper-button-prev .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-next .swiper-button-next .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-pause}

<div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white">

</div>

</div>

</div>

<!-- /wp:jetpack/slideshow -->

<!-- wp:heading {"level":3} -->

### El rol de la memoria en la Perla Amazónica

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La apuesta desde la Zona de Reserva Campesina Perla Amazónica, es invitar a las nuevas generaciones a apropiarse de su territorio, no solo a través del avistamiento de aves o la examinación de invertebrados, sino en sí mismo la importancia de la protección y cuidado de su región, reconociéndose como integrantes de un mismo espacio. [(Lea también: Por daños ambientales en Putumayo congelan millonaria cifra a petrolera Amerisur)](https://archivo.contagioradio.com/por-danos-ambientales-en-putumayo-congelan-millonaria-cifra-a-petrolera-amerisur/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De la mano de discusiones y talleres alrededor de la agricultura biodinámica también se buscó que las nuevas generaciones consideren formas de abordar el campo basándose en las relaciones que se establecen entre los suelos, las plantas, semillas y los mismos individuos, todo como un sistema en equilibrio.

<!-- /wp:paragraph -->

<!-- wp:jetpack/slideshow {"ids":[92138,92139,92143,92146,92237],"sizeSlug":"large"} -->

<div class="wp-block-jetpack-slideshow aligncenter" data-effect="slide">

<div class="wp-block-jetpack-slideshow_container swiper-container">

-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0456-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92138}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_9842-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92139}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0237-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92143}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4742-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92146}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/IMG_9877-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92237}
    </figure>

[]{.wp-block-jetpack-slideshow_button-prev .swiper-button-prev .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-next .swiper-button-next .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-pause}

<div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white">

</div>

</div>

</div>

<!-- /wp:jetpack/slideshow -->

<!-- wp:paragraph {"align":"justify"} -->

Esta misma premisa se llevó al campo de la fotografía, la memoria como herramienta y fuente que nutre el discurso histórico y la memoria de cualquier persona y a la vez de reconocerse dentro de una comunidad para así reconocer con quiénes comparte una misma comunidad y reconocer la palabra y la imagen que se proyecta y comprender que la historia futura se debe construir sobre el saber de lo que se fue. Dicho objetivo también se logró junto a la Fundación Inty Grillos, que de la mano de la música y el muralismo llenaron de color los días del festival. [(Lea también: Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños)](https://archivo.contagioradio.com/comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos/)

<!-- /wp:paragraph -->

<!-- wp:jetpack/slideshow {"ids":[92065,92066,92067,92069,92145],"sizeSlug":"large"} -->

<div class="wp-block-jetpack-slideshow aligncenter" data-effect="slide">

<div class="wp-block-jetpack-slideshow_container swiper-container">

-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0137-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92065}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0211-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92066}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0375-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92067}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_0307-1-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92069}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/IMG_4742-1024x683.jpg){.wp-block-jetpack-slideshow_image .wp-image-92145}
    </figure>

[]{.wp-block-jetpack-slideshow_button-prev .swiper-button-prev .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-next .swiper-button-next .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-pause}

<div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white">

</div>

</div>

</div>

<!-- /wp:jetpack/slideshow -->

<!-- wp:paragraph {"align":"justify"} -->

La culminación de la Semana contó además con la participación y apoyo de Mi Nombre es Mujer Perla Amazónica” (MEMPA), grupo de mujeres que componen el Comité Central de Mujeres de ADISPA y su campaña 'Sembremos Vida, Sembremos Futuro' promoviendo la siembra y adopción de árboles en el territorio.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
