Title: “Un Fiscal General que tiene una agenda política es un peligro” Bejarano
Date: 2017-02-16 16:04
Category: Judicial, Nacional
Tags: acuerdos de paz, FARC, Fiscal General, Odebrecht
Slug: un-fiscal-general-que-tiene-una-agenda-politica-es-un-peligro-para-la-nacion-bejarano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ramiro-bejarano-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: semana] 

###### [16 Feb 2017]

Según Ramiro Bejarano, las acusaciones del Fiscal General contra el gobierno Santos estaban dirigidas a torpedear los acuerdos de paz y representaron un ataque directo al trámite de la Jurisdicción Especial de Paz. Sin embargo, las declaraciones de Otto Bula negando una acusación contra la campaña Santos, **le restaron credibilidad y le significó pérdida de fuerza propia y de Cambio Radical** para oponerse a la implementación de la Jurisdicción Especial de Paz.

Para el abogado y profesor universitario hubo dos situaciones en la actuación del fiscal Nestor Humberto Martínez. Por una parte quiso debilitar el proceso de paz y atacar directamente la Jurisdicción Especial de Paz a la que se ha opuesto y por otra parte quería fortalecer a Cambio Radical que ha señalado que el fiscal hace parte de ese partido. ([Lea También: Odebrecht la punta del Iceberg de la corrupción en Colombia](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/))

### **Fiscal general actuó irresponsablemente en acusación a Santos** 

Para Bejarano la actuación del Fiscal General ha sido por lo menos “irresponsable” dado que no se debió acusar a la campaña de Santos con una interpretación de las actuaciones criminales de Otto Bula. Además la carta del exsenador, que se conoció este martes, deja claro que se hizo una acusación sin ningún tipo de prueba, ni siquiera testimonial, en un momento crucial para el gobierno y para los acuerdos de paz.

Sin embargo el fiscal general “no calculó” que el escándalo fuese a trascender más allá de los partidos y no tuvo en cuenta que se está hablando de una serie de crímenes transnacionales que está cometiendo la empresa Odebrecht y que también han alcanzado a ex presidentes que ya tienen órdenes de captura como en el caso de Alejandro Toledo de Perú.

Además Martínez tampoco contó con que los contratos relacionados con el préstamo del Banco Agrario a Odebrecht y el “otro si” para la construcción de uno de los tramos de la Ruta del Sol II iban a terminar por enredarlo a él mismo y a su círculo cercano. En ese sentido el jurista señala que el fiscal debería apartarse de la investigación. ([Le puede interesar: Martínez debe decalrarse impedido para investigar caso Odebrecht](https://archivo.contagioradio.com/fiscal-martinez-declararse-impedido-investigar-caso-odebrecht/))

### **Ya hicimos la paz y los políticos tienen que entenderlo** 

Bejarano señala también que ya no habría espacio a la oposición al acuerdo de paz y que la intención del fiscal de restar fuerza al gobierno debería redirigirse. “Espero que políticos entiendan que no pueden fallarle al país, nosotros ya hicimos la paz, no podemos volver a la guerra”. ([Lea también: Fiscalía obvia crímenes del Estado para la JEP](https://archivo.contagioradio.com/informe-de-la-fiscalia-omite-crimenes-cometidos-por-agentes-del-estado/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
