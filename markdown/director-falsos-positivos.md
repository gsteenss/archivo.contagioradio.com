Title: General(r) acusado de falsos positivos dirige plan que protege a líderes sociales
Date: 2019-01-29 16:40
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Comité de Solidaridad con los Presos Políticos, Ejecuciones Extrajudiciales, falsos positivos, Fiscalía General de la Nación, jurisdicción especial para la paz
Slug: director-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [29 ene 2019] 

Organizaciones de derechos humanos rechazaron el nombramiento del General (r) Leonardo Barrero como director del Plan de Acción Oportuna (PAO) para la protección de defensores de DD.HH. y periodistas, dado su presunto implicación en casos de ejecuciones extrajudiciales, obstrucción a la justicia y estigmatización de líderes sociales durante su tiempo en el Ejército.

En un comunicado al público, las plataformas de derechos humanos, junto con el Movimiento de Víctimas de Crímenes de Estado, la Marcha Patriótica y la Cumbre Agraria, Campesina, Étnica y Popular manifestaron que el General retirado "es una persona impedida ética y moralmente para ejercer el cargo" de director del PAO, el cual pretende detener el sistemático asesinato de líderes sociales.

El documento destacó graves denuncias en contra del nuevo director, incluso, su presunta implicación en las ejecuciones extrajudiciales de tres campesinos mientras se desempeñaba como comandante de la Brigada XVI, con jurisdicción en Casanare y Boyacá, entre enero de 2004 y diciembre de 2005.

### **Denuncias por ejecuciones extrajudiciales** 

El pasado 15 de agosto, el Comité de Solidaridad de Presos Políticos, junto con las víctimas, presentaron ante la Jurisdicción Especial para la Paz (JEP) un informe titulado "Ni delincuentes, ni combatientes", sobre 12 casos de ejecuciones extrajudiciales, que incluye a 20 víctimas, presuntamente cometidos por la Brigada XVI en Casanare, entre el 2005 y el 2007. El General estaría implicado en uno de estos casos.

Según el informe presentado a la JEP, el entonces comandante firmó una orden de operaciones el 14 de junio de 2005, que terminó con el homicidio de tres campesinos, incluyendo una mujer en estado de embarazo, en Pisba, Boyacá. De acuerdo a Harold Vargas, abogado del Comité, al ordenar esta operación, el general retirado se encuentra directamente vinculado con el caso.

<iframe src="https://co.ivoox.com/es/player_ek_31985764_2_1.html?data=lJammpqbepWhhpywj5WcaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncanV09TZxpC6pdPbwtiSlKiPpcPjyMbR0ZCns87d1YqwlYqlfYzYxpDA0dHNqMLmysnOxpDIqYzE08rg0diRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Vargas afirmó que la Fiscalía General de la Nación abrió un proceso en contra de Barrero por estos crímenes, que sigue abierto. Sin embargo, en el proceso "no ha sido posible vincular al general" con los hechos y por esta razón, las víctimas han pedido a la justicia transicional llamar al general para dar su versión voluntaria sobre estos hechos.

Ante estas denuncias, la ministra del interior Nancy Patricia Gutiérrez defendió al general retirado, diciendo que él "no tiene ningún proceso abierto" y que al contrario, "la hoja de vida del general Leonardo Barrero no tiene tacha."

Al respeto, Vargas afirmó que si bien el general no tiene investigaciones abiertas o condenas en su contra, Barrero tiene un proceso en curso y posiblemente será llamado por la JEP por el informe que se entregó. "Que en este momento no tenga una condena en firme no significa que sea la persona más calificada para este cargo," dijo Vargas.

### **Denuncias por obstrucción a la justicia y estigmatización de líderes sociales** 

Las organizaciones de derechos humanos también resaltaron otras denuncias en contra de Barrero que para ellos hacen que el general no posea "la idoneidad requerida para ocupar ningún cargo dentro de la institucionalidad." Una de ellas se trata de una conversación que Barrero tuvo con el Teniente Coronel Robinson González del Río, implicado en por lo menos 46 casos de ejecuciones extrajudiciales.

En las grabaciones de esta conversación, Barrero manifiesta la necesidad de crear "una mafia para denunciar fiscales" que investigan casos de falsos positivos. Cabe recordar, que ante estos hechos Barrero fue retirado de su puesto como comandante de las Fuerzas Militares en el 2014 tras solo servir seis meses en este cargo.

Además, en 2013, el movimiento popular Marcha Patriótica acusó al alto funcionario de señalar a los campesinos de pertenecer a la guerrilla e impedir la captura de presuntos milicianos.  "Estas acusaciones demuestran que existe un sesgo de su parte, y que no posee una postura ecuánime ante el grupo poblacional al que ahora, desde la dirección del PAO, debe brindar garantías," escribieron los autores del documento.

Según Diana Sanchez, directora de la Asociación Minga, las comunidades que hacen parte de las plataformas de derechos humanos han expresado dudas sobre la voluntad del General para garantizar la seguridad de los líderes sociales en las regiones. Frente a estos hechos, las organizaciones manifestaron que no trabajarán junto al director del PAO ni asistirán a reuniones programada con este alto funcionario.

<iframe src="https://co.ivoox.com/es/player_ek_31985878_2_1.html?data=lJammpqce5mhhpywj5WcaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncaXdwtPOjbiJh5SZopbbxc3JvoampJCu1dTHrcLXyoqwlYqmd8-frs7bycaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
