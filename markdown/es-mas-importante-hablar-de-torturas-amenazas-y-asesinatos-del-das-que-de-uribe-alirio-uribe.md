Title: “Son más importantes los crímenes del DAS que los trinos de Uribe” Alirio Uribe
Date: 2017-09-14 15:48
Category: DDHH, Entrevistas
Tags: alirio uribe, Alvaro Uribe, chuzadas, das, interceptaciones ilegales
Slug: es-mas-importante-hablar-de-torturas-amenazas-y-asesinatos-del-das-que-de-uribe-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Beligerarte] 

###### [14 Sept. 2017] 

“Más importante que contestarle judicialmente los trinos al senador Álvaro Uribe Vélez es que el país sepa lo que pasó con el DAS, recordemos que no solo fueron chuzadas sino también torturas, amenazas, asesinatos. **Todo un aparato criminal al servicio de narcotraficantes y paramilitares**”, así lo dijo el representante a la Cámara Alirio Uribe Muñoz ante las acusaciones hechas por el senador a través de su cuenta de Twitter.

Dice Uribe Muñoz que teniendo en cuenta el escenario en el que se encuentra el país, en el que se avecina la creación y puesta en marcha de la comisión de la verdad, que podrá develar el rol de la inteligencia en Colombia, **una reacción de ese tipo es natural porque se intenta tender una cortina de humo con acusaciones** para que se oculte la verdad. Le puede interesar: [Actividades criminales del DAS a punto de prescribir](https://archivo.contagioradio.com/a-juicio-exdectectives-del-das-35421/)

“Estamos hablado de la columna vertebral, de la médula más terrible de la guerra sucia en Colombia y por eso el movimiento de derechos humanos y nosotros desde el Congreso hemos insistido en la necesidad de que **haya una reforma a la inteligencia, al manejo de la inteligencia y a la doctrina militar** que consideraban que cualquier opositor del Gobierno era un terrorista o era un cómplice”.

### **Víctimas del DAS esperan que investigaciones avancen en la CPI** 

Manifiesta el representante que fue bastante llamativo que el fallo de compulsa de copias en contra del senador del Centro Democrático para investigarlo por el caso del DAS, saliera en el momento de la visita de la fiscal de la CPI, Fatou Bensouda. Le puede interesar: [Condena a Jorge Noguera es tardía pero ratifica la existencia de los crímenes del DAS](https://archivo.contagioradio.com/victimas-del-das-no-esperan-mucho-de-la-comision-de-acusacion-de-la-camara/)

“Si ustedes miran es en el marco de esa visita que se produce esta sentencia y nosotros (el Colectivo de Abogados José Alvear Restrepo) **ya habíamos denunciado en diversos informes el tema de los “falsos positivos”,** que fue por uno de los hechos por los que más nos atacó el ex presidente Uribe a los defensores de DD.HH. porque denunciábamos esos hechos”.

Si bien a la fecha van 20 sentencias contra algunos implicados del caso DAS, las víctimas de estas operaciones ilegales esperan que algún día los máximos responsables respondan, sin embargo, no ven con mucha esperanza que eso suceda “mientras exista la Comisión de Acusaciones” asevera Uribe Muñoz “creo que eso es lo que va a llevar que se tarde mucho tiempo en avanzar, pero es importante la presencia de la CPI en el país”.

### **“Se debería mirar los militares juzgados por ejecuciones extrajudiciales y no el show”** 

Este miércoles, **el ministro de Defensa notificó al Fiscal de la CPI que 1400 militares han sido condenados por la práctica de asesinar a civiles** y hacerlos pasar como guerrilleros muertos en combate lo que para Uribe Muñoz debería poner a reflexionar al país y a los medios de comunicación.

“Aquí siempre se mira más el escándalo, los insultos y las cosas que realmente los temas de fondo y **creo que el ex presidente Uribe Vélez hoy senador es muy hábil en tender cortinas de humo,** en desviar los temas que son realmente esenciales de la discusión, para hacer show y no darle explicaciones al país de sus responsabilidades”.

Cabe recordar que dos generales que eran jefes de seguridad durante el mandato de Álvaro Uribe están condenados, dentro de los cuales se encuentra Mauricio Santoyo, condenado por paramilitarismo y narcotráfico. Le puede interesar: ['ChuzaDAS' confirmaría "cacería criminal" durante gobierno Uribe](https://archivo.contagioradio.com/chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe/)

<iframe id="audio_20885467" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20885467_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<article id="post-20976" class="post__holder post-20976 post type-post status-publish format-standard has-post-thumbnail hentry category-ddhh category-entrevistas cat-13-id cat-15-id">
<div class="post_content">

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="clear">

</div>

</div>

</article>

