Title: La JEP necesita a la ciudadanía en movilización para defenderla
Date: 2017-09-28 15:13
Category: Nacional, Paz
Tags: Cambio Radical, Jurisdicción Especial de Paz
Slug: la-jep-necesita-a-la-ciudadania-en-movilizacion-para-defenderla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Sept 2017] 

Ante la decisión del partido político Cambio Radical de no apoyar ni votar, la discusión de la ley estatutaria de la Jurisdicción Especial de Paz, el panorama para la implementación de los Acuerdos de Paz se complejiza. Sin embargo, existen algunas alternativas que sectores de la Unidad Nacional y de la oposición están planteando, **así como la necesidad de la movilización social. **

<iframe src="https://co.ivoox.com/es/player_ek_21158484_2_1.html?data=k5ael52YfJWhhpywj5aXaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjNjcxNfJb83VjMnSxc7XrYa3lIqvldOPqMafpMbaxM7Tb9PVxc7Qw9GPstCfwtXcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Iván Cepeda, la implementación de la JEP, las circunscripciones de paz, las reformas rurales e incluso la reforma política, están en riesgo, no solamente por la decisión de Cambio Radical, que ya se esperaba, **sino porque hay división en sectores de la Unidad Nacional, con algunos congresistas**. (Le puede interesar: ["Circunscripciones de paz deben respetar liderazgos de mujeres , jóvenes y afros"](https://archivo.contagioradio.com/circusncripciones-de-paz-deben-respetar-liderazgos-de-jovenes-mujeres-y-afros/))

No obstante según el senador se están haciendo grandes esfuerzos para superar esta problemática en lo que concierne a las coaliciones con sectores del partido Conservador. **Liberal, la Alianza Verde y otros minoritarios para aprobar el articulado**. (Le puede interesar: ["Cambio Radical y Centro Democrático se le atraviesan al Fast Track"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-atraviesan-al-fast-track/))

<iframe src="https://co.ivoox.com/es/player_ek_21158521_2_1.html?data=k5ael52ZdpKhhpywj5aZaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncavVytfcjbfNusbmwpDg0cfWqYzgwpDbx8jJt8rYwsmYxsqPtsbVzc7nw9ePsdDqytHW3MbHrdChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte Jairo Rivera, integrante de Voces de Paz, aseguró que es necesario que expresiones de la sociedad como las que se manifestaron los días después del plebiscito, y que cumplen un año de movilización, **vuelvan a manifestar de manera contundente su apoyo y presionen nuevamente al Congreso para que actúe a favor de la paz**.

Una de las convocatorias que van en esa línea de retomar el debate en la sociedad sobre los Acuerdos de Paz, es el encuentro ¿A dónde vamos con esta vuelta? un espacio que pretender reunir a diferentes sectores de la ciudadanía para hablar de paz, el evento se llevará a cabo el próximo **3 de octubre, en Casa Ensamble, a las 5:00pm para posteriormente convocar a una gran movilización por la paz, el 5 de octubre**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
