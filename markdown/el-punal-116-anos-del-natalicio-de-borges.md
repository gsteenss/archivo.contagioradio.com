Title: "El puñal" - 116 años del natalicio de Borges
Date: 2015-08-24 10:56
Category: Cultura, Viaje Literario
Tags: 116 años de Borges, Cuentos cortos de Borges, El puñal, Jorge Luis Borges, Literatura Latinoamericana
Slug: el-punal-116-anos-del-natalicio-de-borges
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/puñal-e1440418628876.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_7498900_2_1.html?data=mJmmmp6UdI6ZmKiakpiJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptGY0tqJh5SZo5bOzoqWdoy%2B0NfUx5CwucrnjKfc1MzJt4ylmZ6mj5adfJehhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [24, ago,2015]

Jorge Luis Borges, nació el 24 de Agosto de 1899 en Buenos Aires (Argentina). Su obra, que incluye poesía, ensayo y narrativa, es de alto contenido metafísico, fantástico y subjetivo. No es de fácil comprensión para el lector, ya que sus escritos reflejan, según el propio Borges, "su propia confusión y el respetado sistema de confusiones que llamamos filosofía, en forma de literatura".

En sus últimos años, en los que no podía valerse por sí mismo a causa de sus problemas generales de salud sumados a una ceguera progresiva que lo afectó desde su juventud, estuvo con su segunda esposa Marìa Kodama. Murió en Ginebra (Suiza), el 14 de junio de 1986, a causa de cáncer hepático.
