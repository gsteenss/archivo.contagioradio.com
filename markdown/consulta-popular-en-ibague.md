Title: Por el agua y por la vida, vamos todos a exigir: Consulta popular ya!!!
Date: 2016-11-03 06:00
Category: CENSAT, Opinion
Tags: consulta popular, Ibagué, Mineria
Slug: consulta-popular-en-ibague
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Consulta-popular-ya.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 3 Nov 2016 

#### [**Ibagué es Colombia**] 

En este país, después del tal plebiscito, el ambiente (no las ambientalistas) está enrarecido a causa de sus resultados: “el embrujo Uribista triunfó en las urnas”, tras una sórdida campaña que conspira contra el triunfo de la paz. Cuál paz?

[Hoy, después de firmados los acuerdos de la Habana con bombos y platillos, estamos frente a la incertidumbre política de cómo salvaguardar los acuerdos logrados, y entonces se tejen toda suerte de artimañas jurídicas para intentarlo y en este campo derrotar la PARAfernalia Uribista que reclamando su triunfo democrático impone las reglas: La paz impuesta!!!]

[Así las cosas, desde la otra orilla, la respuesta fue esclarecedora e inmediata…”la voluntad popular” -llamémosla provisionalmente de esta manera- se viene expresando contundentemente: masivas manifestaciones y marchas salieron a las calles: indígenas, campesinos, negritudes (primeras víctimas del conflicto), acompañados por estudiantes, trabajadores y una numerosa presencia popular, hicieron sentir su enérgica demanda: Acuerdo Ya! Sí a la Paz, no a la guerra!]

[Ahora bien, este es en general el panorama actual que dibujan los medios de comunicación… el que se oculta es otro, por ejemplo, el que nos ocupa el día a día de nuestras luchas, por un ambiente distinto para la paz.]

[El 2 de octubre del presente año, sorteando una y mil dificultades jurídico políticas, se convocó en Ibagué a una Consulta Popular, al igual que en otras localidades del país. El asunto es que los municipios y las regiones reclaman más autonomía para decidir si sus territorios aprueban o no la intervención de agentes o empresas nacionales o extranjeras que vienen a explotar o ya están explotando nuestros bienes naturales (agua, oro, carbón, petróleo, entre otros), todo ello en contravía a la vocación agrícola tradicional de nuestras regiones. Estas actividades extractivas afectan el agua, bosques, montañas, flora y fauna, alimentos y la agricultura campesina, y en general transformando o destruyendo todo el entorno social, cultural y ambiental de los territorios y sus pobladores, a lo largo y ancho del país.]

[El 2 de octubre se nos vino la paz encima!!! Se convocó el plebiscito para esa fecha, trasladando la Consulta Popular en Ibagué para el 30 del mismo mes (primero lo primero, que extraña y calculada casualidad). Luego nos enteramos que por artimañas de promotores de la minería y las empresas se interpone una tutela y la sala cuarta del Consejo de Estado suspende de manera temporal la Consulta Popular de Ibagué del día 30. Ver:][[http://www.las2orillas.co/consejo-estado-suspende-consulta-popular-minera-ibague/]](http://www.las2orillas.co/consejo-estado-suspende-consulta-popular-minera-ibague/)[ Ya la Corte Constitucional había aprobado que las Consultas sobre esta temática ambiental eran y son legítimas y se les debe dar curso.]

[Ahora bien coincidencialmente con la suspensión de la consulta, el uribismo incluye como cláusula para cambiar los acuerdos de paz, que se desconozca el fallo de la Corte sobre el tema de las Consultas Populares. Ver:][[http://www.semana.com/nacion/articulo/propuestas-de-alvaro-uribe-para-renegociar-el-acuerdo-final-con-las-farc/498453]](http://www.semana.com/nacion/articulo/propuestas-de-alvaro-uribe-para-renegociar-el-acuerdo-final-con-las-farc/498453)[ Qué tal, que tan siniestro el personaje al ocultar en su discurso de la paz que persigue los más endemoniados propósitos de entregar los bienes comunes de la patria que dice defender. No nos sorprende…]

[Sorprendámonos para bien, del tamaño y la magnitud del reto que tenemos al frente de nosotros, respaldando la movilización popular que la población ibaguereña viene, día a día y de tiempo atrás, impulsando con sus multitudinarias manifestaciones, alegres y carnavaleras en donde todas las edades, de niños, mujeres y hombres nos invitan y señalan el camino que todos debemos seguir. La defensa de nuestros territorios para la vida digna.]

[Todas las personas que defendemos la vida y toda Colombia debemos y tenemos que volcarnos sobre Ibagué- para que con todas nuestras mejores energías descarrilemos la locomotora minera energética que se nos vino encima y que se empeña “desde arriba” aplastar a “los de abajo” en nuestras aspiraciones generales por una paz con justicia ambiental.]

[Todas a Ibagué!!!]

**Vamos por la consulta popular ya!!!**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
