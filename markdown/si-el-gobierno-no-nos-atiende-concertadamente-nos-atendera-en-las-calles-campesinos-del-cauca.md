Title: “Si el Gobierno no nos atiende concertadamente, lo hará en las calles” campesinos del Cauca
Date: 2017-03-15 12:47
Category: DDHH, Entrevistas
Tags: campesinos, Cauca, indígenas, Min Interior, Movilización
Slug: si-el-gobierno-no-nos-atiende-concertadamente-nos-atendera-en-las-calles-campesinos-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-negras-norte-del-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Mar. 2017] 

**Las organizaciones sociales del Cauca le dieron 15 días al Ministerio del Interior para que se siente a concertar soluciones** debido a la grave situación de derechos humanos del departamento o de lo contrario los campesinos han asegurado realizarán una gran movilización.

Esta decisión se toma ante **la falta de voluntad del Gobierno frente al asesinato de 117 líderes sociales en 2016, y 30 más en lo que va corrido del 2017**, las amenazas en contra de la vida e integridad y los riesgos en el territorio; razón por la que se han declarado en Asamblea Permanente por la vida, la defensa de los territorios y los derechos. Le puede interesar: [Lideres indígenas, blanco de asesinatos y amenazas](https://archivo.contagioradio.com/lider-indigena-del-cauca-amenzado-por-los-urabenos/)

Las organizaciones articuladas en la “mesa por los derechos humanos, para la defensa de la vida y los territorios en el departamento del Cauca”, buscan con esta asamblea que **el Gobierno Nacional cumpla las citas que ha pactado con ellos y a las cuáles, sin razones aparentes no han asistido**, para que de esta manera puedan articular propuestas y dar respuesta a la situación a la crisis que se vive en el país.

**Nelson Lis Marín,** de la Asociación Nacional De Usuarios Campesinos De Colombia – **ANUC**, dijo que se declaran en asamblea “porque **no vemos acciones contundentes que impliquen salvaguardar la vida de los dirigentes”.**

Pese a que el Ministerio del Interior y la Oficina de Derechos Humanos de la Presidencia han conocido de voz de las comunidades las situaciones que vienen ocurriendo en el territorio de Cauca, **hasta el momento no se han tomado las medidas pertinentes para salvaguardar la vida de los líderes. **Le puede interesar: [Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

“Vemos que hay toda la tarea de buscar salidas a esta situación, pero las acciones no son rápidas, ni contundentes y eso nos lleva a que la vida de nuestros dirigentes corra riesgos. **Hoy varios de nuestros líderes de la ANUC han tenido que salir del territorio por las amenazas y hace 20 días mataron al presidente de la Junta de Acción Comunal**, quien era de la ANUC” agregó Lis Marín.

Serán **15 días de plazo máximo al Ministerio del Interior para que se pueda conjuntamente “buscar una salida por la defensa del territorio y frente a los actores paramilitares** que hoy están haciendo asesinatos a nuestros dirigentes” contó Lis Marín. Le puede interesar: [Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016](https://archivo.contagioradio.com/colombia-reporto-85-asesinatos-contra-defensores-de-ddhh-en-el-2016/)

Sin embargo, cuenta Lis Marín que **el panorama es complejo, dado que ya le han insistido varias veces a Paula Gaviria**, Alta Consejera de los Derechos Humanos de la Presidencia que realice un acompañamiento a las comunidades y que se trabaje con ellas una agenda de soluciones, **invitación que no ha sido respondida.**

De igual modo, **el Ministerio del Interior ha sido increpado por las comunidades a reunirse y concertar unos puntos a seguir, pero tampoco hubo respuesta** “hoy teníamos una reunión y se nos llamó ayer a cancelarla diciéndonos que por agenda del Ministro. La crítica que hacemos al Ministerio es que sigue vigente la situación de violación a los derechos humanos en el Cauca y que no vemos voluntad política de la entidad para atender las necesidades de seguridad” añadió Lis Marín.

Esta no es la primera vez que estos hechos ocurren, según lo denuncia Lis Marín, el **Ministerio del Interior los ha dejado esperando en varias ocasiones, por lo que han catalogado la actitud de Ministro como “carente de voluntad política”** y le han lanzado la pregunta “¿será que para el Ministro del Interior los campesinos, no existimos?”. Le puede interesar: [Asesinado Eder Cuetia Conda, líder defensor de derechos humanos de Cauca](https://archivo.contagioradio.com/asesinado-eider-cuetia-lider-de-derechos-humanos-36907/)

En el marco de esta asamblea, las organizaciones que son parte de “mesa por los derechos humanos, para la defensa de la vida y los territorios en el departamento del Cauca” continuarán llamando la atención del Gobierno nacional a través de asambleas municipales, en las cuales se estará trabajando los puntos claves para una gran movilización en el país.

**“Si el Gobierno no nos atiende por las vías de concertación pues nos tendrá que atender en la vía**, en una gran movilización que haremos próximamente” concluyó Lis Marín. Le puede interesar: [Continúan las amenazas a líderes en el Cauca](https://archivo.contagioradio.com/continuan-las-amenazas-a-lideres-en-el-cauca/)

[Comunicado Público](https://www.scribd.com/document/341983731/Comunicado-Publico#from_embed "View Comunicado Público on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17567402" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17567402_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_34807" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/341983731/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ZSXcPTeqkw56tCaudW4r&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
