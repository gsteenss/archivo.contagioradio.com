Title: Los prisioneros políticos y las modificaciones a la JEP
Date: 2018-07-10 14:43
Category: Expreso Libertad
Tags: JEP, prisioneros políticos
Slug: prisioneros-politicos-modificaciones-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/jep-carcel.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 3 Jul 2018 

Las modificaciones hechas en la última plenaria extraordinaria del Senado a la Jurisdicción Especial para la paz podrían generar grandes afectaciones de ser aprobadas por la Corte Constitucional, entre ellas el papel de ese organismo a la hora de analizar las solicitudes en extradición de quienes se acogieron al mecanismo de justicia transicional.

José Vega, abogado de la Corporación Solidaridad Jurídica, asegura que todos los cambios que planteó el Centro Democrático a la JEP, están llenos de vacíos jurídicos que no solo atentan contra la Constitución del país, sino con la misma esencia con la que fueron pactados los Acuerdos de paz de La Habana.

<iframe id="audio_27083512" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27083512_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
