Title: Amenazan de muerte a líderes sociales en el Putumayo
Date: 2019-01-20 13:07
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Aguilas Negras, AUC, lideres sociales, Putumayo
Slug: amenazan-de-muerte-a-lideres-sociales-en-el-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Ene 2019] 

Diversas organizaciones sociales en el Putumayo, denunciaron las recientes amenazas que llegaron a los teléfonos celulares de líderes sociales en el departamento, enviadas por un grupo que se auto identifica como Bloque Sur Putumayo AUC Águilas Negras, advirtiéndoles que **regresará la limpieza social al territorio y sentenciando de muerte a los defensores de derechos humanos.**

Los líderes a quienes les llegó este mensaje son Yuri Quintero, actual diputada de la Asamblea General del Putumayo, Julian Medina, Gonzalo Portilla, Nixon Piaguaje, Robinson López, Euler Guerrero y Laura Montoya. En el mensaje también amedrentan a periodistas, organizaciones internacionales y disidentes de FARC, además, **señalan que "ya han venido asesinando a varios líderes"**.

Frente a estos hechos, las organizaciones sociales rechazaron cualquier acto de intimidación, zozobra y acto "que quiera debilitar el tejido social" y la estigmatización que se está generando en contra de la labor que realizar líderes sociales y defensores de derechos humanos en el territorio. (Le puede interesar: ["Águilas Negras amenazan de muerte a Héctor Carabalí líder social en Buenos Aires, Cauca"](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-en-buenos-aires-cauca/))

Asimismo, las organizaciones le exigieron al gobierno y las autoridades estatales, medias urgentes y garantías para la vida digna de líderes sociales en el departamento y a la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos prestar toda su gestión para que las actuaciones del Estado Colombiano se apeguen a las Normas internas y externas que se ha comprometido a respetar y que se inicien las investigaciones a que haya lugar por el desconocimiento de estas.

![WhatsApp Image 2019-01-19 at 9.36.02 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-19-at-9.36.02-AM-614x800.jpeg){.alignnone .size-medium .wp-image-60155 width="614" height="800"}

###### Reciba toda la información de Contagio Radio en [[su correo]
