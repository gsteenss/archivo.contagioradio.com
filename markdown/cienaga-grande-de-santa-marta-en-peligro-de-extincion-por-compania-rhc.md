Title: Compañía RHC tiene en peligro de extinción la Ciénaga Grande de Santa Marta
Date: 2015-03-27 23:08
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Ciénaga Grande de Santa MArtha, Desalojos, ganadería bufalina, Humedales, Jorge 40., paramiliatares, Rodrigo Tovar Pupo, Salvatore Mancuso, Santa Marta
Slug: cienaga-grande-de-santa-marta-en-peligro-de-extincion-por-compania-rhc
Status: published

##### Foto: El Tiempo 

<iframe src="http://www.ivoox.com/player_ek_4274825_2_1.html?data=lZeklp2WeY6ZmKiak5aJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMKZpJiSpJbTt4zVzsfWx9PYpc3Z1JDS0JCnrYa3lIqum9PFq8KfqNfO0MnJb8XZjLjO0NnFb67V09nOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sandra Viraldy,  profesora de la Universidad de Magdalena] 

La Ciénaga Grande de Santamarta, la  más importante de Colombia, **que cuenta con más de 4.400 Kilómetros cuadrados,** está en peligro de extinción por cuenta de los daños causados por la compañía agropecuaria RHC, la mayor responsable de esta situación. Pero eso no es todo, esta catástrofe ambiental está acompañada por el desalojo de campesinos por parte de paramilitares.

De acuerdo a la Unidad de Restitución de Tierras, **se trata de 150 campesinos que vivían hace 15 años en las tierras de la Ciénaga, pero fueron víctimas de desplazamiento forzado por parte de Salvatore Mancuso y Rodrigo Tovar Pupo, alias Jorge 40**. Más tarde esas mismas tierras despojadas, fueron adquiridas por la empresa Agropecuaria RHC, de Rafael Hoyos Cañavera y filial de Serinco de Córdova S.A., que actualmente construye diques, terraplenes, realiza actividades agropecuarias y de ganadería que están acabando con ese ecosistema.

La compañía ha realizado obras de **infraestructura vial y ha venido desarrollando actividades portuarias, agricultura y palma africana**, las cuales han generado afectaciones como incendios forestales en diferentes sectores de la ciénaga, poniendo en riesgo el régimen hídrico, produciendo distintas transformaciones a nivel ecológico y generando un secamiento de la misma.

Pese a que se han realizado todas las denuncias, persisten las actividades de la empresa, entre ellas, también está la **ganadería bufalina,** que afecta “de manera muy importante a los humedales, ya que al ser ganado pesado, afectan la fluctuación superficial del agua en los suelos de los humedales”, afirma Sandra Viraldy, doctora en ecología y profesora de la Universidad de Magdalena, quien resalta que esta especie de animales fue introducida y no hace parte de la Ciénaga.

Viraldy, asegura que **después del año 2000, la restauración de la ciénaga ha sido mínima,** según ella, el  gobierno colombiano no ha presentado atención a este caso que lleva más de dos años, teniendo en cuenta que ese ecosistema, fue declarado **Reserva del Hombre y Biósfera por la Unesco hace 15 años.**

La comunidad espera que las investigaciones que se han abierto contra la empresa den resultados, y haya multas y sanciones. Así mismo esperan que la Fiscalía, la Procuraduría y la Contraloría hagan parte del caso, “porque hay omisiones importantes y delitos contra el medio ambiente”, denuncia la profesora de la Universidad de Magdalena.
