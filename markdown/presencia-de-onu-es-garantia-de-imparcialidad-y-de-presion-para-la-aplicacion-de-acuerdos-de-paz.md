Title: Presencia de ONU es garantía de imparcialidad y de presión para la aplicación de acuerdos de paz
Date: 2016-01-26 13:12
Category: Nacional, Paz
Tags: CELAC, ELN, FARC, Jose Gregorio Hernandez, ONU, plebiscito por la paz, Proceso de conversaciones de paz
Slug: presencia-de-onu-es-garantia-de-imparcialidad-y-de-presion-para-la-aplicacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/jose-gregorio-hernadez-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: jurisprudenciahoy.] 

<iframe src="http://www.ivoox.com/player_ek_10207345_2_1.html?data=kpWfkpyXeJahhpywj5aUaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncbHmxtjS0MjNpYzYxpC8sLqPqdSfyMbfw9PYaaSnhqaxw5DIqYzdztXO1MjNpc3dxcbRjd6PqMaf0dfSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Gregorio Hernández, U. del Sinú] 

###### [26 Ene 2016.] 

La presencia de la ONU y el respaldo político al proceso de paz en Colombia que se da a través de la resolución aprobada este 25 de Enero, **es garantía de imparcialidad y de presión política para que se haga efectiva la aplicación de los acuerdos de paz** luego de la firma del documento final. Así lo señala José Gregorio Hernandez, experto jurista y rector de la Universidad del Sinú.

La imparcialidad de la ONU, es además una señal de tranquilidad para las partes en conversaciones y **puede significar un respaldo también a las organizaciones sociales que se han mostrado preocupadas por las señales contradictorias del gobierno** nacional frente a las exigencias que el acuerdo contiene y que hablan de reformas en el agro colombiano, la protección de los recursos del Estado y las garantías de vida digna, que se han convertido en necesidades urgentes.

Además, para el jurista la ONU reivindica el mandato por el que fue creada que radica en contribuir a la construcción de la paz y evitar las guerras alrededor del mundo. Otro de los aspectos es que **Colombia demuestra que hay un esquema serio en la búsqueda de la paz y por ello se da un respaldo casi que inmediato y al pie de la letra**, demostrado en el acuerdo conjunto de las partes en conversaciones.

### **El plebiscito no es el mecanismo “más adecuado”** 

Por otro lado, frente al recién aprobado plebiscito por la paz, Hernández afirma que no es muy clara la posibilidad de que la Corte Constitucional avale esta iniciativa y que no está muy de acuerdo por las condiciones con las que se ha fijado. Que el umbral se haya definido en el 15% del padrón electoral estaría abriendo la puerta para que sea una minoría la que define si respalda o no los acuerdos de paz. **Para el constitucionalista se podría explorar la posibilidad de una asamblea constituyente.**

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
