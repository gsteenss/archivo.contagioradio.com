Title: CIDH y organizaciones de la sociedad civil condenan asesinato de Bernardo Cuero
Date: 2017-06-16 13:25
Category: DDHH, Nacional
Tags: Bernardo Cuero, CIDH, DDHH, Defensores Derechos Humanos, Organizaciones sociedad civil
Slug: cidh-y-organizaciones-de-la-sociedad-civil-condenan-asesinato-de-bernardo-cuero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Bernardo-Cuero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Jun 2017] 

Organizaciones de la sociedad civil, al igual que la Comisión Interamericana de Derechos Humanos, **condenaron y rechazaron el asesinato del líder de derechos humanos Bernardo Cuero** ocurrido el pasado 7 de junio. Manifestaron que, según la Defensoría del Pueblo,  entre el 1 de marzo de 2016 hasta el 1 de marzo de 2017 han sido asesinados 156 líderes y lideresas en el país.

**Bernardo Cuero fue asesinado en su casa en Malambo, Atlántico.** Cuero era fiscal de la Asociación Nacional de Afrocolombianos Desplazados AFRODES, miembro de la Mesa de Víctimas del Atlántico y defensor de los derechos humanos de la población afrodescendiente. En varias ocasiones y después de haber sido víctima del desplazamiento forzado, Cuero fue objeto de dos atentados contra su vida y la protección brindada por la UNP le había sido retirada por tratarse según ellos de un riesgo “ordinario”. (Le puede interesar: ["Defensores de derechos humanos "contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/))

Las organizaciones defensoras de los derechos humanos exhortaron al gobierno nacional a que "**adopte medidas de protección necesarias para garantizar la vida e integridad** de líderes y lideresas que trabajan por la paz”. De igual forma le pidieron al Ministro del Interior que" implemente los mecanismos de protección frente al incremento de los asesinatos de defensores de derechos humanos".

Le hicieron un llamado a la Fiscalía General de la Nación para que "avance con prontitud en las investigaciones de los asesinatos y amenazas contra los líderes y las lideresas y así garantizar que no haya impunidad". Recordaron así mismo, las palabras del Procurador General de la Nación Fernando Carrillo quien manifestó que “la protección de la vida de los y las líderes sociales y de los defensores de derechos humanos es una **prueba de fuego para el proceso de paz**”. (Le puede interesar: "[Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/))

Por su parte la CIDH manifestó que Bernardo Cuero ya había denunciado en varias oportunidades la **situación de riesgo y hostigamiento que viven los líderes y lideresas afrodescendientes** en el país por parte de los paramilitares. La Comisión aseguró que “se ha señalado en repetidas ocasiones la especial gravedad de los efectos del conflicto armado sobre la población afrocolombiana”.

Ante esto le recordó al Estado la “obligación que tiene de **prevenir cualquier atentado contra la vida e integridad física** de las defensoras y los defensores de derechos humanos y de garantizar que en todo momento puedan realizar sus actividades sin temor a que puedan tomar represalias o restricciones contra ellos”.

[Comunicado Asesinato Bernardo Cuero (1)](https://www.scribd.com/document/351491902/Comunicado-Asesinato-Bernardo-Cuero-1#from_embed "View Comunicado Asesinato Bernardo Cuero (1) on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_15546" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/351491902/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-r4UJML5fjigzFZKCRwrI&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
