Title: Afectaciones del derrame de petróleo en Santander podrían durar 30 años
Date: 2018-03-22 14:23
Category: Ambiente, Nacional
Tags: comunidades de santander, derrame de petóleo, Ecopetrol, emergencia ambiental, petroleo, Santander
Slug: afectaciones-del-derrame-de-petroleo-en-santander-podrian-durar-30-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/DYvHSVwX0AAj8hP1-e1521740843192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Jairo Cala] 

###### [22 Mar 2018] 

[Veinte días completa la emergencia ambiental por el derrame de crudo del pozo Lizama de Ecopetrol en los afluentes hídricos de Santander que está afectando a comunidades y diferentes especies animales. La contaminación **ha recorrido 15 km** y podría llegar al río Magdalena donde las afectaciones pueden durar hasta 30 años. El Ministerio de Ambiente anunció que investigará a la empresa y podría haber sanciones.]

[De acuerdo con Óscar Sampayo, integrante de la Alianza contra el Fracking y habitante de Barrancabermeja en Santander, lo que se está viviendo **“es catastrófico y de magnitudes inigualables”**. El derrame ya afectó municipios como San Vicente de Chucurí,  Puerto Wilches y Sabana de Torres.]

[El ambientalista afirmó que el hecho se presentó desde el 3 de marzo cuando **“hubo una emergencia en el pozo Lizama 158 de Ecopetrol”**. La empresa ese día acordonó el lugar y “no permitió la entrada de ningún poblador, a partir de allí tenemos información nula y luego del aguacero del 12 de marzo nos dimos cuenta de la magnitud de la tragedia”.]

### **Fuertes lluvias dieron cuenta de la tragedia ambiental** 

[Efectivamente, tras la lluvia las comunidades evidenciaron que se trataba de una gran contaminación que baja por la quebrada la Lizama que se encuentra en inmediaciones del pozo por lo que **se empezaron a poner las alarmas desde las comunidades.** Para el 13 de marzo “la situación se vuelve caótica porque empieza a salir del suelo la sustancia con petróleo y lodo”.]

[Según Sampayo el presidente de Ecopetrol hizo referencia a que “están saliendo 1500 barriles de petróleo **pero no sabemos en cuánto tiempo están saliendo**”. Dijo que la mancha se está extendiendo por el río Sogamosos y es muy probable que no tarde en llegar al río Magdalena y podría llegar al mar Caribe. (Le puede interesar:["En San Martín se contaminó el agua luego de actividades de Fracking"](https://archivo.contagioradio.com/en-san-martin-se-contamino-el-agua-luego-del-inicio-del-fracking/))]

### **Acciones de Ecopetrol no han sido suficientes** 

[Si bien la empresa se encuentra realizando las labores de mitigación de la emergencia, las comunidades consideran que **sus acciones no son suficientes.** Creen que ante las afectaciones de los cuerpo hídricos, no sólamente la empresa debe dar respuestas sino que también lo debe hacer las instituciones del Estado.]

[Dicen que en la historia de la explotación de hidrocarburos **no se había presentado una** **situación igual** y que, ante la magnitud, ha sido muy difícil cuantificar las afectaciones. Sampayo argumentó que no se le ha dado la atención suficiente y ya han muerto decenas de iguanas, tortugas y nutrias a la vez que miles de peces y aves.]

[A esto se suma que, los pobladores creen que las afectaciones a las fuentes hídricas pueden tardar hasta 30 años y “la responsabilidad **no puede recaer solamente en Ecopetrol”**. Esto teniendo de presente que, si bien la empresa es la operadora del campo, “aquí hay Estado e instituciones responsables, parece que viviéramos en tierra de nadie”.]

[Finalmente, Sampayo afirmó que también ha habido **afectaciones a la salud de las personas** en la medida en que el derrame de hidrocarburos contiene gas que se está movilizando por el agua. Cientos de familias han sido evacuadas y otras se han quedado sin acceso al agua. Por eso las comunidades le han pedido a la empresa que tenga claridad sobre lo que está sucediendo y han pedido celeridad para que la tragedia ambiental sea controlada.]

<iframe id="audio_24746378" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24746378_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
