Title: Comunidades buscan llegar a acuerdo sobre adjudicación de tierras en Putumayo
Date: 2017-08-14 16:26
Category: Nacional, Paz
Tags: Comunidades campesinas, comunidades indígenas
Slug: comunidades-buscan-llegar-a-acuerdo-sobre-adjudicacion-de-tierras-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Imagen1ttt-e1501885667449.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Bien Común] 

###### [14 Ago 2017] 

Comunidades campesinas e Indígenas, del corregimiento de la Castellana, en el municipio de Villa Garzón en Putumayo, denunciaron los intereses por de una familia perteneciente al pueblo indígena Inga, de intentar adjudicarse más de 30 mil hectáreas de tierra, asegurando que ellos serían quienes ancestralmente habrían ocupado el territorio, sin embargo, **las comunidades argumentan que llevan habitando allí desde hace más de 60 años.**

De acuerdo con Iván Ruíz, presidente de Asojuntas del municipio de Villa Garzón, lo que tanto campesinos como indígenas buscan es que se geste un escenario de diálogo en donde se pueda llegar a un acuerdo con los indígenas Inga, en pro de la permanencia de todos los habitantes en el territorio y de esta forma realizar una repartición equitativa de la tierra, entendiendo **que no se puede desconocer los derechos territoriales de quienes llevan allí más de 60 años**.

De igual forma Ruíz expresó que detrás de esta adjudicación de predios habría otros intereses, como los de la multinacional Gran Tierra Energy, y las concesiones de la Agencia Nacional Minera para que se realice extracción de oro y otros minerales. (Le puede interesar: ["Puerto Leguízamo, Putumayo, arranca proceso de Consulta Popular")](https://archivo.contagioradio.com/puerto-leguizamo-putumayo-arranca-proceso-de-consulta-popular/)

“Lo que pretendemos es que el territorio no se adjudique a un grupo étnico, cuando no sabemos cuál es el interés de él, porque si lo vamos a proteger, lo podemos hacer entre todos, ellos no son las únicas personas que viven en el territorio**, acá hay más comunidades étnicas, está el pueblo Awa, los Embera Chamí, los Nasa, con los que los campesinos ya hemos llegado a acuerdos de protección del territorio**” afirmó Ruíz.

Esta zona, además es reconocida como un corredor biológico que conecta con la Amazonía, en donde se ha demostrado la presencia de especies importantes, que están en vía de extinción como la danta, el venado, el jaguar, entre otros. (Le puede interesar: ["Comunidades del Putumayo denuncia persecución judicial"](https://archivo.contagioradio.com/comunidades-del-putumayo-denuncian-persecucion-judicial/))

Actualmente los campesinos e indígenas de la Comunidad Nasa, le están solicitando a la ANLA y la Agencia de Tierras que verifique los mapas. **Otra de las iniciativas que planea adelantar la comunidad es realizar una consulta popular minera**, para definir el uso del suelo “creemos que ninguna multinacional debería ingresar al territorio porque sería fatal y catastrófico el daño a las fuentes hídricas” expresó Ruíz.

<iframe id="audio_20328591" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20328591_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
