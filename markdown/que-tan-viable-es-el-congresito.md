Title: ¿Qué tan viable es el "Congresito"?
Date: 2015-08-12 15:14
Category: Entrevistas, Judicial, Paz
Tags: acuerdos de paz, Asamblea Constituyente, congresito, Congreso de la República, constitución de 1991, FARC, Juan Manuel Santos, proceso de paz
Slug: que-tan-viable-es-el-congresito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/santos-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: sincuento.com 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Rodolfo-Arango.mp3"\]\[/audio\]

##### [Rodolfo Arango, analista] 

Las declaraciones del Presidente Juan Manuel Santos en la revista Semana, sobre la propuesta de  creación de un “Congresito” que permita validar parte de los acuerdos de paz que se logren en la Habana, han generado polémica en los últimos días sobre qué tan viable es esta iniciativa.

El jurista Rodolfo Arango, asegura que para llevar a cabo la propuesta de Santos, **sería necesario recurrir a una reforma constitucional para concebir la figura del “Congresito”,** debido a que actualmente en la constitución “no es posible pensar en esa figura”, dice el analista.

Arango explica que esa posibilidad se origina en 1991 cuando se necesitaba un órgano legislativo que desarrollara determinadas partes de la Constitución de 91.

Esta célula legislativa habría que crearla a través de un acto legislativo que diga  que “el presidente podrá regular e implementar los acuerdos a los que se llegue a la Habana, y el Congresito podría improbar esos decretos que presente el ejecutivo en el desarrollo de los acuerdos del proceso de paz en la Habana”, expone el jurista, quien agrega que se trata sólo de una idea que “hay que discutir”.

Según Arango, “claramente la reacción política debe ser adversa, porque la idea del "**Congresito" es desplazar al Congreso de la República y su soberanía en un tema neurálgico”,** de manera que ningún congresista aceptaría que el congreso pierda sus competencias.

Respecto al tema jurídico, el presidente tiene la facultad de presentar actos legislativos, sin embargo queda la duda, sobre cuál sería el apoyo que recibiría de los parlamentarios.

Por otro lado, sobre la posibilidad de realizar una **Asamblea Nacional Constituyente** en el marco de los acuerdos del proceso de paz, Rodolfo Arango, asegura que pese a **tratatarse de una idea  atractiva para los demócratas, podría ser “un espejismo”.**
