Title: Comunidades afrodescendientes buscan impulsar la minería ancestral
Date: 2015-04-17 16:34
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Aguas de Barcelona, Anglo Gold Ashanti, Calle, Cauca, COSUDE, Embera de Riosucio, minería ancestral, Minería ilegal, Norte del Cauca, Palenque Alto Cauca, Santander de Quilichao
Slug: comunidades-afrodescendientes-buscan-impulsar-la-mineria-ancestral
Status: published

##### Foto: [altamanzana.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4370717_2_1.html?data=lZikkpyVe46ZmKiakpqJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfosvf0cnJt8TZz8nWx9PYqdSfw9rgxcbSb8rh0drZ1cbWb83VjNLW0MrWaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Armando Caracas, Palenque Alto Cauca] 

Comunidades Afrodescendientes del Alto  Cauca y Embera de Riosucio, Caldas se reunirán los días  **16 y 17 de abril en Santander de Quilichao,** en el evento ** “Diálogos Interculturales sobre Minería en Colombia",** con el objetivo de desarrollar una propuesta para impulsar la minería ancestral.

“Nosotros como comunidades negras, hace 4 años llevamos a cabo un proceso interétnico e intercultural, **buscando que las comunidades sigan ejerciendo la minería ancestral y no caiga en la minería criminal”,** expresa Armando Caracas¸ quien hace parte del Palenque Alto Cauca.

Durante la jornada, **36 consejos comunitarios de 10 municipios del norte del Cauca y 2 municipios del sur Valle**, han realizado un recorrido de 12 kilómetros donde se muestra la catástrofe ambiental, y se presenta una propuesta al gobierno nacional para poder ejercer la minería ancestral, que a suele volverse ilegal, debido a los requerimientos que deben cumplir los mineros artesanales muchas veces son imposibles de cumplir, “el poder lo tienen las multinacionales como **Anglo Gold Ashanti y Aguas de Barcelona** que vienen arrasando con los territorios”, asegura Caracas, quien agrega que, “**desde 1536 se realiza la minería ancestral, sin máquinas que acaben con la fauna y la flora**”.

​Es por eso, que a partir de este evento intercultural, se impulsan acciones concretas hacia el reconocimiento estatal del aprovechamiento ancestral del recurso minero, la protección de los territorios colectivos, y el respeto a las Autoridades Étnicas, ya que de acuerdo a la gobernación del Cauca, en el departamento **hay aproximadamente 2000 retroexcavadores haciendo minería ilegal.**

El encuentro que se realizará en el Hotel Los Samanes, Cr 13 \# 12-51, y está organizado por el Palenque Alto Cauca en alianza con el Resguardo Indígena Cañamomo Lomaprieta (Riosucio y Supía, Caldas), el Programa de los Pueblos de los Bosques (Forest Peoples Programme) del Reino Unido, y cuenta con el apoyo de Organismos de Cooperación Internacional de Suiza, COSUDE.
