Title: Aumentan a 7 las ofensivas militares en el Cauca que ponen en riesgo el cese al fuego unilateral
Date: 2015-03-19 18:16
Author: CtgAdm
Category: Nacional, Paz
Tags: 6 frente, Cauca, Corinto, ejercito, FARC, rio negro
Slug: aumentan-a-7-las-ofensivas-militares-en-el-cauca-que-ponen-en-riesgo-el-cese-al-fuego-unilateral
Status: published

###### Foto: Occidente 

<iframe src="http://www.ivoox.com/player_ek_4237794_2_1.html?data=lZegmZydeI6ZmKiakpuJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdbhxtPhw5DFb5ifxtGY0Iqnd4a2otLS1NSPqMaf0MvS0NjNusLnjNLWzs7YpdPZ1JDS0JDJsIy3wtqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Lizeth Montero, Red de DDHH Francisco Isaías Montero] 

Un desembarco de 8 helicópteros y al rededor de 60 policías y militares en el municipio de Corinto, corregimiento de Rio Negro, Cauca, el miércoles 18 de marzo volvió a poner en riesgo el cese al fuego unilateral decretado por las FARC desde el 20 de diciembre pasado.

La acción conjunta ofensiva del Ejercito y la policía contra el VI Frente de las FARC ocasionó una confrontación entre las 3:30 y las 6:00 de la tarde. Según denuncia la comunidad y la Red de Derechos Humanos Francisco Isaías Cifuentes, el desembarco del Ejercito que desencadenó la confrontación se dio en zona poblada, afectando de manera directa a la población civil, como es el caso del joven campesino Jhon Maro Velasco Chate, quien hace parte de la organización pro constitución de la Zona de Reserva Campesina de Corinto, y recibió un impacto de bala en su hombro izquierdo.

El jueves 19 de marzo, la Red de Derechos Humanos Francisco Isaías Cifuentes realizará una misión de verificación en Rio Negro, para determinar los daños materiales y las afectaciones a civiles producto de las acciones del Ejercito, e intentará recopilar la narración de los hechos por parte de la población civil. Según indica la abogada Lizeth Montero, la población tiene zozobra y miedo por la posible reanudación de la confrontación, en tanto los helicópteros del Ejercito continúan sobre-volando  la zona.

La Asociación de Cabildos Indígenas del Norte del Cauca, ACIN, y las organizaciones campesinas de la región, insisten en la necesidad de acordar un cese al fuego bilateral que ponga fin a las acciones militares que afectan directa e indirectamente a la población civil. Según indican, con la ofensiva militar del miércoles 18 de marzo ya son 7 las acciones del Ejercito que ponen en riesgo el cese al fuego unilateral. Además, registran que por acciones similares, el domingo 15 de marzo se cometieron dos ejecuciones extrajudiciales de los campesinos de la región, Homero Flor Vidal y Nelly Camayo Sánchez.
