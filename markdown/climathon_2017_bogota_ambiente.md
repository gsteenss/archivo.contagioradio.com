Title: Con Climathon 2017 ciudadanía bogotana le apuesta a la movilidad verde
Date: 2017-10-26 16:29
Category: Ambiente, Voces de la Tierra
Tags: cambio climatico, Climathon 2017
Slug: climathon_2017_bogota_ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Grupo-rental-e1509052924930.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bogotravel Tours] 

###### [26 Oct 2017]

En el marco de las diferentes acciones que se vienen gestando desde las organizaciones ambientalistas, para generar conciencia sobre el cambio climático, este 26, 27 y 28 de octubre en Bogotá se realiza el evento **Climathon Bogotá 2017, que este año se enfoca en la Movilidad verde.**

Climathon es una iniciativa de la Comunidad Económica Europea, a través de la cual abre a organizaciones sociales del planeta entero, la posibilidad de realizar eventos alrededor de la solución de problemáticas ambientales.

De acuerdo con Andrés Acuña, integrante de la Corporación Colectivo CreAcción, se trata de un evento para que la ciudadanía pueda tener herramientas al rededor de temas sobre la movilidad verde, a partir de diferentes **actividades lúdicas y culturales, con talleres, charlas, mesas de trabajo y conferencias. **

Se trata de un evento que busca promover las propuestas de la ciudadanía para enfrentar el cambio climático y por ello el sábado las personas que hagan parte de las actividades, tendrán la oportunidad sus propuestas alrededor de la temática de este año en el evento y esta misma será financiada y puesta en marcha.

No obstante, señala el abogado Acuña, este tipo de propuestas deben ir acompañadas de una política clara del gobierno que busque tomar medidas realmente efectivas para la adaptación y mitigación del cambio climático. **"Sabemos del doble discurso que maneja la política colombiana sobre el tema, se suscriben unos compromisos en las COP, y leyes en favor de la conservación de los ecosistemas**", de allí, que en este evento también están invitados funcionarios públicos que se comprometan a enfrentar la crisis ambiental actual.

El evento está liderado por FUNCENER -Fundación Centro de Entrenamiento para la Adaptación y Mitigación al Cambio Climático-, entidad sin ánimo de lucro dedicada a la pedagogía frente al cambio climático desde la agroecología y las energías renovables.

<iframe id="audio_21714860" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21714860_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
