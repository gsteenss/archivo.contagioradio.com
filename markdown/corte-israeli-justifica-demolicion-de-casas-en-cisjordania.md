Title: Israel inicia demolición de casas de palestinos
Date: 2015-11-13 17:07
Category: El mundo
Tags: Cisjordania, Corte israelí justifica demolición de casas en Cisjordania, Demolición casas en Cisjordania
Slug: corte-israeli-justifica-demolicion-de-casas-en-cisjordania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Demolición-casas-Cisjordania.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre ] 

###### [13 Nov 2015 ] 

[La Corte Suprema israelí, en cabeza de su presidenta Miriam Naor, desatendiendo las apelaciones de distintas organizaciones no gubernamentales **justifica la demolición de las casas pertenecientes a aquellos palestinos que presumiblemente perpetraron los recientes ataques** contra israelíes y que cobraron las vidas de 12 de ellos.]

[Pese a que Naor admite el **daño que esta medida causa a las familias sin responsabilidad** directa en la comisión de los delitos mencionados, asegura que "en ciertas circunstancias al Estado no le queda otra opción". Sólo ha habido una excepción, relacionada con la solicitud para evitar la demolición de un edificio de 8 pisos de propiedad de un tercero en el que vive la familia de uno de los atacantes, mientras que **varias de las casas situadas en Ramalá Nablus y Kalandia, Cisjordania serán derruidas**.  ]
