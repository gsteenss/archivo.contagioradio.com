Title: FECODE está dispuesto a dar espacio a los estudiantes en reunión con el Gobierno
Date: 2018-10-23 13:14
Author: AdminContagio
Category: Educación, Entrevistas
Tags: estudiantes, fecode, Gobierno, Mesa de Concertación, profesores
Slug: fecode-reunion-gobierno
Status: published

###### [Foto: FECODE] 

###### [19 Oct 2018] 

El próximo miércoles, la **Federación Colombiana de Trabajadores de la Educación (FECODE)** sostendrá un encuentro con la ministra de educación **Maria Victoria Angulo,** para continuar con las conversaciones entre el gremio docente y el Gobierno que se vienen adelantado desde inicios de mes, donde los docentes estarían dispuestos a ceder espacios para que los estudiantes universitarios puedan participar de la discusión.

**Miguel Ángel Pardo,** secretario de asuntos técnicos, científicos y pedagógicos de FECODE; desestimó a quienes califican como oportunista la [movilización](https://archivo.contagioradio.com/profesores-paro-por-24-horas/) que están llevando a cabo los docentes, recordando que la jornada de paro se realiza en apoyo a los estudiantes de educación superior y como seguimiento a los acuerdos alcanzados con el Gobierno tras el paro del año pasado.

Dichos acuerdos incluían una reforma constitucional que devolviera los **73 billones** que la educación pública dejó de recibir por parte del gobierno, tras 16 años de reformas constitucionales que **redujeron la participación del presupuesto nacional a los entes territoriales de 46 a 24%**; y también incluían la mejora en el sistema de salud del magisterio, pero que hasta el momento, no han sido cumplidos.

Adicionalmente, la movilización se presenta en medio de dos hechos actuales: La huelga de hambre que tras 7 días, decidió finalizar el profesor **Adolfo Atehortúa**, pero que continúan los docentes **Luis Aguirre**, de la UIS,  **Luis Marín**, de la Universidad del Quindío, y **Juan Yepes** de la Universidad de Caldas; así como el asesinato en Cauca, del profesor **José Domingo Ulcué**. (Le puede interesar: ["Asesinan a docente indígena en Santader de Quilicaho, Cauca"](https://archivo.contagioradio.com/asesinan-profesor-santander-de-quilichao/))

Pardo sostuvo que en la movilización, esperan que confluyan organizaciones estudiantiles, docentes de educación básica y superior, y los rectores que integran el **Sistema Universitario Estatal (SUE)** para coordinar nuevas acciones en favor de la educación pública desde la primaria hasta la universidad. (Le puede interesar: ["Generación E: ¿El reencauche de Ser Pilo Paga?"](https://archivo.contagioradio.com/generacion-e/))

El Secretario de FECODE concluyó que el martes tendrán una nueva reunión con la ministra de educación, siguiendo la mesa de concertación instalada el 3 de octubre, y allí, **estarían dispuestos a cederle puestos a los estudiantes para que lleven sus reclamos al Gobierno. **(Le puede interesar: ["Las 10 exigencias del movimiento estudiantil al Gobierno Nacional"](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

<iframe id="audio_29548681" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29548681_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
