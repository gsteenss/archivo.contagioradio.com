Title: En pleno proceso de paz se recrudece la violencia en el departamento de Putumayo
Date: 2016-03-11 17:46
Category: Nacional, yoreporto
Tags: Paramilitarismo, Puerto Asís, Putumayo
Slug: putumayo-se-recrudece-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [Claudia Lancheros Fajardo - Yo Reporto      ] 

###### 11 Mar 2016 

En la segunda semana del mes de Febrero, las comunidades del Bajo Putumayo empezaron a manifestar su preocupación por la aparición de panfletos intimidatorios, a través de los cuales se anunciaba la llegada del “Comando de limpieza social, escuadrón de justicia y muerte” y su presencia a partir de la fecha, en varios municipios del departamento.

El  Consejero para los Derechos Humanos  rechazó en nombre del  Gobierno Nacional estos panfletos, solicitando a las autoridades competentes, que inicien las investigaciones correspondientes para establecer el origen de las amenazas y su autoría intelectual. A su vez varias organizaciones defensoras de DD.HH., y la ciudadanía del común han manifestado una creciente preocupación por la aparición de estructuras criminales que pretenden imponer un control ilegal y de corte autoritario sobre las formas de ser, estar y habitar el territorio.

En ese sentido declaran como objetivo militar en uno de sus panfletos a “Personas cuyas conductas morales no sean aceptadas en una población sana de buenos principios y una buena convivencia” exhortando a padres de familia y empleadores a no permitir que se transite a altas horas de la noche ya que “no queremos lamentar la trágica muerte de seres queridos o personas inocentes”  Días después la incertidumbre se ha tornado mayor, al constatar alrededor de ocho (8) homicidios cometidos en la modalidad de sicariato, en los municipios de Puerto Asís, Valle del Guamuez, Villa Garzón, Puerto Caicedo y Orito. Incluso se registró un hecho violento que culminó en la muerte de un hombre de mediana edad justo a la hora de salida de los niños y niñas a un colegio de la Hormiga, generando zozobra y temor en la población civil.

La implementación de los acuerdos de Paz en territorios tan sensibles por su pasado reciente y por las graves violaciones al DIH cometidas en el marco del conflicto armado, requieren de una mirada más profunda del Gobierno Nacional, para que se tomen las medidas necesarias para evitar estos hechos que sólo traen incertidumbre y zozobra a una población civil agotada por décadas de guerra, justo cuando el país transita más firme que nunca a un acuerdo de paz con las FARC.   La confianza en el proceso de Paz en un territorio que guarda profundos dolores por los daños causados  a la Vida y la Dignidad Humana y  por las secuelas de crímenes que llevaron a límites inexplorados  las dimensiones de la existencia humana, es un asunto de Estado, porque aquí en el territorio del Putumayo se vive o no, la real posibilidad de una paz estable y duradera.
