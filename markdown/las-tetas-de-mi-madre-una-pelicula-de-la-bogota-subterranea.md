Title: ´Las tetas de mi madre´ una película de la Bogotá subterranea
Date: 2015-11-17 11:39
Author: AdminContagio
Category: 24 Cuadros
Tags: Carlos Zapata, Crack Family, Estreno Bogotá, Las tetas de mi madre, Película COlombiana
Slug: las-tetas-de-mi-madre-una-pelicula-de-la-bogota-subterranea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Radionacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:radionacional.com 

<iframe src="http://www.ivoox.com/player_ek_9417225_2_1.html?data=mpmemZeWeY6ZmKiak5eJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqehrsbXb9XZ1cbgjcnJb87djNLOxtfJaaSmhqehjdrSpYzkxtGSpZiJhaXX1tHOjcnJb83VjKfcydTYaaSnhqaejdiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Zapata, Director de cine] 

###### 17 Nov 2015 

En **"Las tetas de mi madre"** la **nueva película de Carlos Zapata** ("Pequeños vagos"), cuenta la historia de Martín, un niño de 10 años, que trabaja junto a su madre como repartidor de pizza, **él quiere llevarla a Disneylandia, sin embargo a medida que se desarrolla la trama esta ilusión se hace más compleja de cumplir**, la violencia y un ambiente feroz van mostrando una realidad de muchos niños y familias de Bogotá.

Martin está obligado a madurar de forma precoz ante la ausencia de su madre, la amistad con u**n chico de su escuela que lo introduce a la oscuridad del negocio de la droga** mostrándole las **irrevocables consecuencias en la figura de la progenitora de este chico**, además el descubrimiento del oficio nocturno de la madre de Martín trastornan su existencia.

Esta película **muestra la cara menos bella de esta ciudad, una Bogotá vista desde los ojos de un niño que se adueña de ella día a día “rodando” en su bicicleta**, una ciudad que conduce al protagonista a una serie de situaciones particulares que van robando su inocencia, pero el espectador desde su silla solo podrá observar, al salir a la calle puede tomar una decisión y hacer algo.

'Las tetas de mi madre' es protagonizada por Paula Matura (María), Billy Heins (Martín), Santiago Heins (Martin) y Alejandro Aguilar(Álvaro) un drama sobre la familia, el amor y lo cotidiano, además cabe resaltar que la **propuesta estética de Zapata muestra riesgos que se atrevió a tomar a la hora de dirigir y realizar esta película**.

**Este filme en el año 2014 recibió el premio Work In Progress Latinoamericano en la décima edición del Festival Internacional de Cine en Chile**. Recibió también un premio del jurado en la primera edición del Mercado Audiovisual Latinoamericano del Flicc en México. Un premio a Mejor Película en el Festival de Cine Colombiano de Nueva York.

**También participó** este año en el Festival Internacional de Cine en Guadalajara, (Ficg); el Festival de Cine de Málaga; el Festival Internacional de Cine de Cali y el Festival Internacional de Cine de Viña del Mar.

Carlos Zapata, el director, recibió el pasado mes de agosto, el premio a Mejor Director en el Festival Internacional de Cine de Santander (Fics). La **película está musicalizada por la agrupación de rap Crack Family,** banda sonora que se ha publicado como un álbum que acompañó el lanzamiento de la cinta**.**

https://www.youtube.com/watch?v=NZw564sdwvg
