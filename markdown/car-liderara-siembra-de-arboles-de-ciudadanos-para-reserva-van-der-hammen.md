Title: CAR liderará siembra de árboles de ciudadanos para reserva Van Der Hammen
Date: 2017-08-11 12:50
Category: Ambiente, Voces de la Tierra
Tags: Corporación Autónoma Regional, Reserva Forestal Thomas Van der Hammen
Slug: car-liderara-siembra-de-arboles-de-ciudadanos-para-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/árboles-Reserva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [11 Ago 2017]

El director de la Corporación Autónoma Regional de Cundinamarca (CAR), Néstor Franco manifestó que luego de un encuentro con los grupos ciudadanos promotores del regalo de los árboles para la reserva Van Der Hammen, **se coordinará el proceso para recibir, sembrar y mantener los árboles donados por los ciudadanos el pasado fin de semana** y que tienen por objetivo ampliar la foresta de la reserva.

Néstor Franco, aseguró que solamente hasta el pasado 10 de Agosto recibieron la solicitud formal de un encuentro para coordinar el proceso de siembra, sin embargo, aseguró que están dispuestos a llevar a cabo el encuentro y poder coordinar todas las acciones para que los cerca de **3000 árboles sean evaluados, sembrados y sometidos también a un plan de sostenimiento.**

El director de la CAR agregó que “nosotros hemos señalado que este tipo de actividades cívicas son de mucha importancia para el beneficio **de los intereses ambientales de la ciudad, pero se debe hacer coordinación y que se cumpla con los procedimientos**”.

"No solamente se trata de hacer el hueco y poner la mata, sino que se debe realizar un proceso de evaluación fitosanitaria, de ubicación geográfica y a un plan de sostenimiento que garantice que los árboles puedan subsistir y no afecten la fauna y la flrora presente en la zona de reserva", aseguó Franco.

**El próximo paso es que se pueda gestar la reunión entre la CAR y las organizaciones ambientales para coordinar la entrega**, el día en que se llevará acabo la siembra y el plan de mantenimiento.

Frente a la posibilidad de que la Reserva cambie de estatus, Franco aseguró que **hasta el momento la CAR no ha recibido ningún tipo de solicitud** en donde se haga esa petición. (Le puede interesar:["Alcaldía de Bogotá no recibió 2339 árboles para Reserva Thomas Van Der Hammen"](https://archivo.contagioradio.com/alcaldia-de-bogota-no-recibio-donacion-de-arboles-para-reforestar-reserva-van-der-hammen/))

### **Este domingo habrá una primera jornada de siembra** 

Por su parte Sabina Rodríguez, vocera de la organización en defensa de la Reserva Thomas Van Der Hammen, **manifestó que volverán a hacer una radicación a la Alcaldía Distrital para hacer el balance de la actividad,** pese a que se negaron a recibir los árboles. (Le puede interesar: ["Alcaldía de Bogotá se negó a firmar pacto para proteger Cerros Orientales"](https://archivo.contagioradio.com/se-crea-pacto-para-proteger-los-cerros-orientales/))

“**Nosotros como compromiso con la ciudadanía estamos invitando a este domingo a una primera siembra, en la misma carta que se le radicó a la CAR**, también los invitamos para que sea un primer gesto y empecemos a trabajar de manera conjunta, la primera siembra se realizará en la Quebrada la Salitrosa” afirmó Sabina Rodríguez.

<iframe id="audio_20288405" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20288405_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
