Title: Cabildos Abiertos podrían ser propuesta desde la Mesa Social para la Paz
Date: 2016-10-26 17:54
Category: Nacional, Paz
Tags: ELN, Mesa Social para la paz., Quito
Slug: cabildos-abiertos-propuesta-mesa-social
Status: published

###### [Foto: Contagio Radio] 

###### [26 de Oct 2016] 

Desde que se conoció la agenda de conversaciones de los diálogos en Quito, **la Mesa Social para la Paz ha venido gestando un escenario de participación**. Esta mesa intentará recoger las propuestas y acumulados de diferentes sectores de la sociedad y podría funcionar con un mecanismo de Cabildo Abierto.

Para el profesor y analista Víctor de Currea, la sociedad civil está un poco “viche” para acompañar a la mesa Gobierno-ELN, sin embargo, considera que el **movimiento social y en general la sociedad civil, han ido acumulando durante un buen tiempo agendas que permiten develar los temas que irían a Quito**. No obstante aclara que el reto más grande es que la institución acepte esta forma de participación desde la ciudadanía. Le puede interesar:["Ya están conformados equipos de ELN y Gobierno para diálogos en Quito"](https://archivo.contagioradio.com/conformados-equipos-de-eln-y-gobierno-para-dialogos-en-quito/)

**Los cabildos podrían ser la herramienta que permita la participación social en la mesa ELN-Gobierno**  y abrir un espacio mucho más democrático, de esta manera se generaría una serie de flujos entre los territorios locales, regionales y nacionales.  Esta propuesta posibilitaría que regiones como **el Sarare, el Uraba, el Magdalena Medio, Montes de María o Catatumbo, que tienen una configuración identitaria especial, puedan  generar sus propios cabildos e interactuar** desde sus problemáticas.

De Currea considera que la Mesa Social para la Paz podría desarrollarse en dos vías: la primera es establecer tareas inmediatas que son la **implementación de los derechos constitucionales**, como lo son la Salud o Educación. Por otro lado está el reto de las **políticas públicas** y el diálogo con el orden nacional e institucional que impactan a la sociedad. Le podría interesar: ["Mesa Social para la Paz se dispone para diálogos con el ELN"](https://archivo.contagioradio.com/mesa-social-para-la-paz-se-dispone-para-dialogos-eln/)

 “La mesa social para la paz es una propuesta política y social en donde se han venido sumando diferentes fuerzas como el **movimiento campesino, indígena, negritudes y mujeres, que conforman estas agendas**” afirmó De Currea.

<iframe id="audio_13490856" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13490856_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>
