Title: johan post
Date: 2020-01-19 21:39
Author: CtgAdm
Slug: johan-post
Status: published

[![Uber: nadie te está echando](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/uber_risk-1200x800-e1507148819593-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/uber-nadie-te-esta-echando/)  

#### [Uber: nadie te está echando](https://archivo.contagioradio.com/uber-nadie-te-esta-echando/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2020-01-15T13:52:13-05:00" title="2020-01-15T13:52:13-05:00">enero 15, 2020</time>](https://archivo.contagioradio.com/2020/01/15/)¿Cómo es posible que el Estado Colombiano saque a Uber? No. Yo creo que está mal planteada la pregunta, que…[Leer más](https://archivo.contagioradio.com/uber-nadie-te-esta-echando/)  
[![El salto ideológico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/74802282_1249874088532329_945839746624520192_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-salto-ideologico/)  

#### [El salto ideológico](https://archivo.contagioradio.com/el-salto-ideologico/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-12-16T16:13:23-05:00" title="2019-12-16T16:13:23-05:00">diciembre 16, 2019</time>](https://archivo.contagioradio.com/2019/12/16/)¿Qué hace falta para que las cosas cambien? Parece que tenemos todo; tenemos al pueblo en las calles, tenemos a…[Leer más](https://archivo.contagioradio.com/el-salto-ideologico/)  
[![¿Qué sigue? soluciones al 21N](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/76705221_1249873755199029_7211690620484583424_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/)  

#### [¿Qué sigue? soluciones al 21N](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-11-25T16:30:18-05:00" title="2019-11-25T16:30:18-05:00">noviembre 25, 2019</time>](https://archivo.contagioradio.com/2019/11/25/)Tenemos el poder popular para modificar las agendas del gobierno, fuimos más de 2 millones solo en las calles de…[Leer más](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/)  
[![21 de noviembre: por la dignidad  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  

#### [21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-11-06T12:32:42-05:00" title="2019-11-06T12:32:42-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)Si el mundo cambia o no cambia con una marcha, es la pregunta que el fatalista quiere poner, como piedra…[Leer más](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  
[![La contra violencia ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/protestas-ecuador-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-contra-violencia-en-ecuador/)  

#### [La contra violencia ](https://archivo.contagioradio.com/la-contra-violencia-en-ecuador/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-10-17T15:30:15-05:00" title="2019-10-17T15:30:15-05:00">octubre 17, 2019</time>](https://archivo.contagioradio.com/2019/10/17/)El destino de la protesta social en América Latina se jugaba en Ecuador. No solo se trataba de una pugna…[Leer más](https://archivo.contagioradio.com/la-contra-violencia-en-ecuador/)  
[![Triza a la vista  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/curvarado-e1473650667277-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/triza-a-la-vista/)  

#### [Triza a la vista  ](https://archivo.contagioradio.com/triza-a-la-vista/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-10-05T22:44:18-05:00" title="2019-10-05T22:44:18-05:00">octubre 5, 2019</time>](https://archivo.contagioradio.com/2019/10/05/)Tal parece que la maldad camina alegre por estos días en Colombia. Alegre porque avanza con una dosis de anestesia…[Leer más](https://archivo.contagioradio.com/triza-a-la-vista/)  
[![El culpable del uribismo soy yo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/rain-2538429_1280-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-culpable-del-uribismo-soy-yo/)  

#### [El culpable del uribismo soy yo](https://archivo.contagioradio.com/el-culpable-del-uribismo-soy-yo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-08-30T10:42:08-05:00" title="2019-08-30T10:42:08-05:00">agosto 30, 2019</time>](https://archivo.contagioradio.com/2019/08/30/)Qué dirían nuestras lágrimas si tuvieran voz para cantarnos ¡culpable el Uribismo y su corona de trizas! ¡culpable la guerrilla…[Leer más](https://archivo.contagioradio.com/el-culpable-del-uribismo-soy-yo/)  
[![A Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/colombia-desde-el-espacio-nasa-mapa-suramerica-tierra-123rf-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/a-colombia/)  

#### [A Colombia](https://archivo.contagioradio.com/a-colombia/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-08-07T15:22:44-05:00" title="2019-08-07T15:22:44-05:00">agosto 7, 2019</time>](https://archivo.contagioradio.com/2019/08/07/)Apreciada Colombia, sabes que no tenemos estaciones, aquí pasamos en un día del verano al invierno, de la euforia a…[Leer más](https://archivo.contagioradio.com/a-colombia/)  
[![Nos arrastran a la guerra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/6f6e9d14-congreso-de-colombia-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/nos-arrastran-a-la-guerra-uribismo/)  

#### [Nos arrastran a la guerra](https://archivo.contagioradio.com/nos-arrastran-a-la-guerra-uribismo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-07-26T09:06:19-05:00" title="2019-07-26T09:06:19-05:00">julio 26, 2019</time>](https://archivo.contagioradio.com/2019/07/26/)El descaro del uribismo, lo ha convertido en un régimen que se burla en la cara de todos los colombianos…[Leer más](https://archivo.contagioradio.com/nos-arrastran-a-la-guerra-uribismo/)  
[![Polaricemos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/polarizacion-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/polaricemos/)  

#### [Polaricemos](https://archivo.contagioradio.com/polaricemos/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-07-08T16:50:57-05:00" title="2019-07-08T16:50:57-05:00">julio 8, 2019</time>](https://archivo.contagioradio.com/2019/07/08/)Polaricemos, pero no entre estupideces que nos ponen en una encuesta virtual; polaricemos, pero no para elegir entre el partido…[Leer más](https://archivo.contagioradio.com/polaricemos/)  
[![“Políticamente incorrecto”: un cliché más](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/POLIITCAMENTE-INCORRECTO-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/politicamente-incorrecto-un-cliche-mas/)  

#### [“Políticamente incorrecto”: un cliché más](https://archivo.contagioradio.com/politicamente-incorrecto-un-cliche-mas/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-06-17T18:07:31-05:00" title="2019-06-17T18:07:31-05:00">junio 17, 2019</time>](https://archivo.contagioradio.com/2019/06/17/)Herido de muerte, el neoliberalismo observa sus tesis pisoteadas en el suelo. Ni libre mercado, ni menos Estado, son realidades…[Leer más](https://archivo.contagioradio.com/politicamente-incorrecto-un-cliche-mas/)  
[![Periodismo en riesgo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/periodismo-en-riesgo/)  

#### [Periodismo en riesgo](https://archivo.contagioradio.com/periodismo-en-riesgo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-06-09T08:40:40-05:00" title="2019-06-09T08:40:40-05:00">junio 9, 2019</time>](https://archivo.contagioradio.com/2019/06/09/)No pocos años permanecerá trinando un expresidente sin darse cuenta el daño que causa, y el impulso tan serio que…[Leer más](https://archivo.contagioradio.com/periodismo-en-riesgo/)  
[![La quiebra de la ideología neutral](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-24-at-16.07.10-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)  

#### [La quiebra de la ideología neutral](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-25T07:30:36-05:00" title="2019-05-25T07:30:36-05:00">mayo 25, 2019</time>](https://archivo.contagioradio.com/2019/05/25/)Ya pronto serán 30 años desde que el mundo occidental daba cantos de victoria ante el fin de la guerra…[Leer más](https://archivo.contagioradio.com/quiebra-ideologia-neutral/)  
[![El fascismo del siglo XXI](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1-1-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)  

#### [El fascismo del siglo XXI](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-14T10:03:27-05:00" title="2019-05-14T10:03:27-05:00">mayo 14, 2019</time>](https://archivo.contagioradio.com/2019/05/14/)No pocos años permanecerá trinando un expresidente sin darse cuenta el daño que causa, y el impulso tan serio que…[Leer más](https://archivo.contagioradio.com/el-fascismo-del-siglo-xxi/)  
[![Se quemó Guaidó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D5kHMpUWkAAi0Vu-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/guaido-se-quemo/)  

#### [Se quemó Guaidó](https://archivo.contagioradio.com/guaido-se-quemo/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-05-02T12:45:20-05:00" title="2019-05-02T12:45:20-05:00">mayo 2, 2019</time>](https://archivo.contagioradio.com/2019/05/02/)Guaidó se quemó, nada que hacer, es un mamarracho. Y esto no es un agravio, es una literatura de nuestra…[Leer más](https://archivo.contagioradio.com/guaido-se-quemo/)  
[![La ideología de Daniel Coronell](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/La-ideología-de-Daniel-Coronell-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)  

#### [La ideología de Daniel Coronell](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-04-22T09:59:04-05:00" title="2019-04-22T09:59:04-05:00">abril 22, 2019</time>](https://archivo.contagioradio.com/2019/04/22/)Daniel Coronell es un excelente profesional, pero obedece con claridad a esa naturaleza del periodismo, que cuando se manifiesta de…[Leer más](https://archivo.contagioradio.com/la-ideologia-de-daniel-coronell/)  
[![Cauca: ¿ponderar derechos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56232221_1676961222406256_7904919769650823168_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/cauca-ponderar-derechos/)  

#### [Cauca: ¿ponderar derechos?](https://archivo.contagioradio.com/cauca-ponderar-derechos/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-04-05T16:09:55-05:00" title="2019-04-05T16:09:55-05:00">abril 5, 2019</time>](https://archivo.contagioradio.com/2019/04/05/)La protesta no es el problema, es solo el síntoma. El problema no es el gobierno, este es solo un…[Leer más](https://archivo.contagioradio.com/cauca-ponderar-derechos/)  
[![¿Adversarios o enemigos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/adversarios-o-enemigos/)  

#### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-22T06:00:16-05:00" title="2019-02-22T06:00:16-05:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)Quisimos ser adversarios, pero nos equipararon con bestias salvajes, mientras éramos nosotros los que llorábamos los muertos.[Leer más](https://archivo.contagioradio.com/adversarios-o-enemigos/)  
[![Arias: no lloraremos por ti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/61229-2/)  

#### [Arias: no lloraremos por ti](https://archivo.contagioradio.com/61229-2/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-07T06:00:43-05:00" title="2019-02-07T06:00:43-05:00">febrero 7, 2019</time>](https://archivo.contagioradio.com/2019/02/07/)or más de que los medios corporativos de información anden en una campaña casi melodramática para recomponer la imagen de…[Leer más](https://archivo.contagioradio.com/61229-2/)  
[![¿Polarizar o no polarizar Dr. Wasserman?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  

#### [¿Polarizar o no polarizar Dr. Wasserman?](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-01-19T07:24:22-05:00" title="2019-01-19T07:24:22-05:00">enero 19, 2019</time>](https://archivo.contagioradio.com/2019/01/19/)16 Ene 2019El movimiento a favor de la polarización no es tan furioso como lo presentan, pero sí es un…[Leer más](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  
[![Mensaje de navidad para un Estado secuestrado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  

#### [Mensaje de navidad para un Estado secuestrado](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-12-19T06:00:46-05:00" title="2018-12-19T06:00:46-05:00">diciembre 19, 2018</time>](https://archivo.contagioradio.com/2018/12/19/)A esa gente, que tiene secuestrado al Estado Colombiano y que participa de los mecanismos de ese secuestro, un solo…[Leer más](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  
[![De la onírica a la política](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/De-la-onírica-a-la-politica-770x400-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)  

#### [De la onírica a la política](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-28T09:44:04-05:00" title="2018-11-28T09:44:04-05:00">noviembre 28, 2018</time>](https://archivo.contagioradio.com/2018/11/28/)Lo que hoy hacen por ejemplo Uribe, Carrasquilla y Martínez es la prueba tangible del que llamo yo, el secuestro…[Leer más](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)  
[![Desde el sótano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/desde-el-sotano/)  

#### [Desde el sótano](https://archivo.contagioradio.com/desde-el-sotano/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-09T15:33:08-05:00" title="2018-11-09T15:33:08-05:00">noviembre 9, 2018</time>](https://archivo.contagioradio.com/2018/11/09/)Según Schopenhauer toda verdad pasa por tres etapas. En la primera es ridiculizada. En la segunda es criticada y en…[Leer más](https://archivo.contagioradio.com/desde-el-sotano/)  
[![La protesta sumisa   ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-protesta-sumisa/)  

#### [La protesta sumisa   ](https://archivo.contagioradio.com/la-protesta-sumisa/)

Posted by [Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-10-25T09:55:22-05:00" title="2018-10-25T09:55:22-05:00">octubre 25, 2018</time>](https://archivo.contagioradio.com/2018/10/25/)Lograron ofender a los manifestantes más por un grafiti que por las razones de una movilización; lograron preocupar más a…[Leer más](https://archivo.contagioradio.com/la-protesta-sumisa/)
