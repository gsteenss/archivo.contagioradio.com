Title: Con homenaje a la UP, Congreso conmemora 23 años del asesinato del Senador Manuel Cepeda
Date: 2017-08-09 13:08
Category: DDHH, Nacional, Sin Olvido
Tags: Iván Cepeda, Manuel Cepeda, Partido Comunista, Unión Patriótica, UP
Slug: con-homenaje-a-la-up-congreso-conmemora-23-anos-del-asesinato-del-senador-manuel-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Manuel-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Heidy\_UP] 

###### [09 Ago. 2017]

*“Hay que hacer algo, golpear un tarro en solitarias calles, hay que ir a cine, hay que leer un libro en compañía, hay que escribir la vida con mil manos. En ese bus pueden ir la esperanza y la muerte. En cualquier lado puede encontrarse el fin”* era lo que decía **Manuel Cepeda Vargas, quien un 9 de agosto de 1994, hace 23 años fue asesinado** por agentes del Estado en connivencia con paramilitares.

Perdió su vida como cientos más que pertenecían a la Unión Patriótica (UP) y para quienes **hoy se realiza un homenaje en el Congreso de la República.** Manuel Cepeda hizo que en tiempos de paz y hoy más que siempre, su voz es recordada. Le puede interesar: [Una exposición busca dignificar la memoria de los líderes de la UP](https://archivo.contagioradio.com/memoria-up-exposicion/)

Un evento acompañado por congresistas, integrantes de la UP y del Partido Comunista en memoria de su vida y **su trabajo político, que fue silenciado por convertirse en “incómodo” para el Estado y los paramilitares** de la época.

### **Manuel Cepeda un luchador por una Colombia con Justicia Social** 

**Manuel Cepeda nace en Armenia un 13 de abril de 1930, cursó estudios de derecho** y para su época estudiantil fue cuándo decidió vincularse al Partido Comunista, siendo delegado en 1958 como la persona que se encargaría de reconstruir la Juventud Comunista Colombina (JUCO).

De su matrimonio con Yira Castro, quien también era parte de la JUCO, nacieron **María e Iván Cepeda, quienes luego de tantos años han continuado con el trabajo** y el legado que sus padres les enseñaron, buscar una Colombia con justicia Social.

“Son 23 años del asesinato del senador Manuel Cepeda y hemos logrado después de grandes discusiones que se votara de manera unánime una proposición para rendir homenaje a la bancada de la UP que fue además literalmente exterminada” cuenta Iván Cepeda.

Manuel Cepeda, sería el último congresista de esa bancada en ser asesinado, de un total de nueve. **Para 1992 el genocidio contra la UP ya completaba 5 años** y los muertos y desaparecidos sumaban más de tres mil víctimas. Le puede interesar:[ Manuel Cepeda y la memoria en sus poemas](https://archivo.contagioradio.com/manuel-cepeda-y-la-memoria-en-sus-poemas/)

Un año antes de su asesinato, Cepeda se atrevió a denunciar el plan que tenían para terminar de exterminar a todos los integrantes de la UP, se conoció como “Golpe de Gracia”, del cual fue víctima.

### **El Estado es responsable del asesinato de Cepeda y demás integrantes de la UP** 

Los Suboficiales del ejército para esa época, Hernando Medina Camacho y Justo Gilberto Zúñiga, con integrantes de la estructura paramilitar de Carlos Castaño han sido vinculados por participar en el crimen del Senador.

**Debido a la lentitud del proceso jurídico otros responsables han fallecido** como el General Rodolfo Herrera Luna, quien había sido señalado de ser uno de los autores intelectuales y murió de un infarto.

Cinco años después del asesinato del Senador Cepeda un juez penal de Bogotá condenó a 43 años, a los Suboficiales Justo Gil Zúñiga y a Hernando Medina, quienes no cumplieron la pena pues se les adjudicó otro delito, el asesinato del teniente José Simón Talero.

Para junio de **2010 la Corte Interamericana de Derechos Humanos (CIDH) halló responsable al Estado de Colombia** por el asesinato de Manuel Cepeda. Le puede interesar: [En 22 años no se han determinado los máximos responsables del crimen de Manuel Cepeda](https://archivo.contagioradio.com/en-22-anos-no-se-han-determinado-los-maximos-responsables-del-crimen-de-manuel-cepeda/)

### **Sigue sin devalarse toda la verdad del exterminio de la UP** 

Ni en el caso del asesinato del Senador Manuel Cepeda, ni el de los demás integrantes de la bancada y del movimiento UP, se han podido develar los autores intelectuales, la cadena de mando. **No se sabe quiénes pagaron y ordenaron a los sicarios que cometieran esta masacre.**

Por esta razón, casos como el de Cepeda han sido declarados de lesa humanidad por la Fiscalía General de la Nación, para que así no prescriba y de tal modo no se asegure la impunidad.

### **Homenaje a la memoria colectiva que representa Cepeda y cientos de militantes de la UP** 

Con una ofrenda floral que será ubicada en el Cementerio Central a las 12 p.m., un homenaje a la bancada de la UP en el Congreso de la República que incluye una placa, con los nombres de los integrantes de la bancada de parlamentarios de la UP y el PCC víctimas del genocidio contra ese movimiento y con un conversatorio a las 6 p.m. en el que se hablará de la vida y lucha del senador Manuel Cepeda, se hará un homenaje a la memoria.

**“Es un acto de mínimo reconocimiento y de homenaje a quienes ofrendaron su vida por la paz y la democracia en Colombia”** recalcó Iván Cepeda.

<iframe id="audio_20256131" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20256131_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
