Title: Indígenas del Cauca denuncian que hay 110 comuneros heridos y siguen ataques del ESMAD
Date: 2015-03-12 23:05
Author: CtgAdm
Category: Movilización, Nacional
Tags: Indigenas Nasa, liberación de la madre tierra, Norte del Cauca
Slug: indigenas-del-cauca-denuncian-que-hay-110-comuneros-heridos-y-siguen-ataques-del-esmad
Status: published

###### Foto: Pulzo.com 

Este jueves 12 de marzo arriba al municipio de Corinto, una misión conformada por la **ONU, Defensoría del Pueblo y la Iglesia**, representada en Francisco de Roux. La idea es que esta comisión pueda servir de **mediadora entre el gobierno y las comunidades** indígenas ante la ausencia de respuestas pero la persistencia de los ataques del ESMAD, que según Hector Fabio, consejero Nasa de la ACIN deja un saldo de 110 heridos en las casi 3 semanas del proceso de liberación de la madre tierra en el Norte del Cauca.

Las comunidades indígenas se mantienen en su postura de continuar con la presión para que el gobierno entregue las 6400 hectáreas de tierras en **Corinto, Miranda y Santader de Quilichao** como parte de la reparación por la Masacre el Nilo y afirma que esperan que la comisión de mediación pueda interceder por el cese de la represión pero también por el cumplimiento en la entrega de tierras.

Desde el pasado 23 de Febrero, comunidades indígenas del Norte del Cauca realizan lo que han llamado "**proceso de liberación de la madre tierra"** para hacer valer las promesas incumplidas del gobierno en cuanto a la entrega de tierras como parte de la reparación colectiva por la **masacre del Nilo** que contempla 30000 hectáreas.
