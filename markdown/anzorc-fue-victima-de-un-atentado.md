Title: Atentan con disparos contra sede de ANZORC en Bogotá
Date: 2020-05-24 08:18
Author: CtgAdm
Category: Actualidad, Nacional
Tags: ANZORC, Atentado, campesinos
Slug: anzorc-fue-victima-de-un-atentado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EYvt49kXkAAIPQv.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EYvcJgWXkAMcI3J.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: @ANZORC\_OFICIAL*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Asociación Nacional de Zonas de Reserva Campesina, ANZORC, denunció este 23 de mayo un atentado con arma de fuego contra sus instalaciones en Bogotá. Según la denuncia fueron varios impactos de bala contra vidrios y mobiliario.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/catatumbol281/status/1264365690036854786","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/catatumbol281/status/1264365690036854786

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se dio sobre el mediodía en la sede que tiene la Asociación en la calle 17 \# 5- 21 en un tercer piso en la capital colombiana , " la guardia del edificio escucha un estruendo minutos después recibió aviso por parte del personal de seguridad de un parqueadero quines le informan de un disparo contra una de las oficinas", señala el comunicado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al hacer la verificación la seguridad del edificio verifica que se trata de la sede de ANZORC al poco tiempo se comunican con el equipo quienes al hacer la verificación del lugar encuentran vidrios rotos, una ventana con un visible impacto redondo y restos de un proyectil.

<!-- /wp:paragraph -->

<!-- wp:image {"id":84693,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EYvcJgWXkAMcI3J-768x1024.jpg){.wp-image-84693}  

<figcaption>
*"**Rechazamos cualquier acto de violencia amenaza hostigamiento y atentado contra la vida e integridad de los líderes y lideresas agrarios** defensores de derechos humanos y organizaciones campesinas que desde los territorios busca la garantía de los derechos para las comunidades"*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Ante los hechos ocurridos, la organización señaló que debido a la situación actual modificada por la cuarentena no había nadie en la oficina a esa hora, sin embargo agregan que por lo general los fines de semana al mediodía se realizan actividades y se da movilidad de personal en la oficina, la cual además se encuentra a unas pocas cuadras de la Procuraduría General de la Nación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ANZORC una organización que busca la paz en las zonas campesinos

<!-- /wp:heading -->

<!-- wp:paragraph -->

La **ANZORC es un proceso que articula a 67 organizaciones campesinas en 21 departamentos de Colombia,** en un proceso que sólo busca en medio del conflicto armado promover la paz a través de las zonas de reserva campesina, trabajan *"**en la búsqueda del reconocimiento del campesinado como sujeto político** derechos la protección de los Derechos Humanos el acceso a la Tierra y la soberanía alimentaria entre otras garantías en defensa de la vida y la permanencia en el territorio".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que su labor es en busca de la paz, **esta no es la primera vez que la organización ha sido víctima de diferentes hostigamientos atentados y homicidios**, por ello en el comunicado enlistan 8 hechos violentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primero de ellos en el 2011 en Miranda,Cauca, en donde **hombres armados abordaron a un integrante de la asociación el cual fue agredido con arma de fuego y a pesar de ello sobrevivió.** (Le puede interesar: <https://archivo.contagioradio.com/feminismo-campesino-trabajan-mujeres/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

2014, 2016 y 2018, en territorios como César, Putumayo, Meta, en donde hace presencia la Asociación registraron acciones violentas contra diferentes integrantes de ésta; panfletos, desplazamientos del territorio y agresiones directas de las cuales varias de las víctimas lograron escapar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**[Jani Silva](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/) quién es fundadora de ANZOR e integrante de la comisión política ha sufrido varios hostigamientos desde hace año**s lo que conlleva a su desplazamiento en los últimos meses ha sido hostigada por hombres armados alrededor de su casa en Puerto Asís putumayo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**En el año 2016 se presentaron 116 homicidios de líderes sociales y defensores de Derechos Humanos en Colombia de los cuales 14 eran integrantes de la asociación,** en el 2017 191, 12 de la Asociación y en 2018- 255, 24 de las organizaciones "*, agregan que en el 2020 han reportado 37 violaciones a Derechos Humanos a líderes, afiliados y habitantes de las zonas de reserva campesina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eso sumado a la conocida lista de ***oposición*** del ejército en twitter revelada el 11 de mayo del 2020, en donde aparecía también ANZORC . (Le puede interesar: <https://www.justiciaypazcolombia.com/organizaciones-de-la-sociedad-civil-internacional-alertan-sobre-el-grave-riesgo-que-representa-para-la-paz-y-la-democracia-en-colombia-el-uso-indebido-de-los-sistemas-de-inteligencia-estatal/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente exigen al Estado y a instituciones como Defensoría del Pueblo, **implementar de manera inmediata acciones para investigar identificar, juzgar y sancionar a los responsables de los hechos;** así como también las garantías para que se respeten los derechos y la vida de las personas que conforman la asociación tanto en Bogotá como en los territorios.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
