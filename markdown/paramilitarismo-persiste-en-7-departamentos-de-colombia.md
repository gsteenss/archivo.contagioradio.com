Title: Delegación de Paz de FARC denuncia Paramilitarismo en 7 departamentos de Colombia
Date: 2015-07-10 16:44
Category: Nacional, Paz, Política
Tags: Aguilas Negras, buenaventura, DDHH, Delegados de paz de las FARC, Empresa, Gaitanistas, Neoparamilitarismo, Paramilitarismo, Rastrojos, Urabeños, violación a derechos humanos
Slug: paramilitarismo-persiste-en-7-departamentos-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/paramilitarismo-en-colombia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **[10 Jul 2015]** 

##### Foto: original tomada de internet

El primer informe sobre la **presencia neo paramilitar en Colombia**, publicado por la delegación de paz de las FARC EP en la Habana, Cuba,  da cuenta de la presencia actual de **8 estructuras paramilitares posicionadas en 7 departamentos del país** desde el 2008 hasta la fecha, que estarían al servicio del **narcotráfico y el sicariato. **En este se asegura, además, que la **Policía colombiana y fuerzas militares** serían **presuntos cómplices de sus negocios con sectores comerciantes.**

El informe, fechado el 9 de julio, subraya que algunos pobladores de diversas regiones del país han dado testimonio de la **presencia de grupos que rondan por las zonas,** estableciendo **control social territorial para el tráfico de droga **en los departamentos** del Putumayo, Meta, Chocó, Valle, Cauca y Costa Atlántica.**

La Delegación de paz, señala  que las acciones de los nuevos grupos paramilitares van desde el amedrentar** a las comunidades y líderes** comunitarios, **quemar propiedades** de habitantes que retornan luego de desplazamiento, **reclutar jóvenes** para vincularlos con el expendio de drogas, hasta el desmembramiento y la tortura de los y las habitantes.

Grupos como los **Rastrojos, Urabeños, Águilas Negras, Paisas y  Ejército Revolucionario Popular Anticomunista de Colombia (ERPAC),** continuaron operando después de la desmovilización paramilitar del año 2006, según la Delegación de paz e informes de organizaciones de derechos humanos.

En el mismo se acentúa en varios párrafos la actividad paramilitar en el Pacífico colombiano, donde **Buenaventura** es una de las zonas del país con una **fuerte presencia neoparamilitar**. *"Las cifras de **violaciones a derechos humanos y desplazamientos forzados** es el **más alto de todo el país**"*. *“En todo el Pacífico la **desmovilización paramilitar del Bloque Calima fue parte de un montaje** para legitimar el proceso de la ley 975, pero el territorio quedó y se mantiene hasta la fecha bajo el **control de las estructuras** que asumieron la fachada de **Águilas Negras, Rastrojos, Empresa, Gaitanistas o Urabeños**, pero mantienen la misma lógica de actuación al lado de la fuerza pública para garantizar el despojo desde el desplazamiento forzado(...)".*

La delegación de paz de las FARC concluye en su comunicación que el propósito neoparamilitar es el control social territorial para los negocios de actores privados.
