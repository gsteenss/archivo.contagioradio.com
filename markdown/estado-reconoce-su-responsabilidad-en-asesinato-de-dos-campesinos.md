Title: Estado reconoce su responsabilidad en asesinato de dos campesinos
Date: 2016-04-18 16:59
Category: DDHH, Entrevistas
Slug: estado-reconoce-su-responsabilidad-en-asesinato-de-dos-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Acto-de-perdón-por-asesinato-de-campesinos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CCJ 

###### [18 Abr 2016] 

Luego de **28 años de impunidad en el asesinato de los campesinos Valentín Basto Calderón y Pedro Vicente Camargo,** el Estado colombiano reconoció su responsabilidad por medio de un acto público en el Parque Principal del municipio del Cerrito, departamento de Santander, por recomendación de la Comisión Interamericana de Derechos Humanos, que además ordenó medidas de reparación y satisfacción para los familiares de las víctimas, entre ellos de Carmenza Camargo, quien resultó herida en los hechos y para ese entonces apenas tenía 8 años.

**El doble asesinato sucedió el 21 de febrero de 1988 en horas de la mañana a 20 metros de la estación de policía del Cerrito**. Tres hombres armados pertenecientes al grupo paramilitar Muerte a Subversivos y Colaboradores (MASC) en complicidad con miembros de la Policía y efectivos de la base militar de Servitá adscrita a la V Brigada en el barrio Calicanto de Cerrito (Santander) arremetieron contra los dos campesinos, y además hirieron a la pequeña de 8 años hija de Pedro Camargo y a Eliza Maldonado.

El Sargento Espitia Díaz, quien se desempeñaba como Comandante de la estación de Policía de Cerrito, no adelantó ningún tipo de acción para detener a los asesinos. Además, según denuncian los familiares de las víctimas, dos días después durante el sepelio, los asistentes fueron hostigados y hombres armados hicieron disparos al aire e intentaron torpedear la realización de los actos fúnebres.

Valentín Basto Calderón era un líder campesino y defensor de derechos humanos que ejercía como Presidente de la Asociación Nacional de Usuarios Campesinos (ANUC) de la provincia de García Rovira en Santander, por otra parte,  Pedro Vicente Camargo era un trabajador del campo en el corregimiento de Servitá.

**El caso se encuentra en la “absoluta impunidad”,** asegura la abogada Lucía Hernández de la Comisión Colombiana de Juristas, quien indica que nadie ha sido judicializado y además hubo una serie de omisiones y antecedentes por parte de agentes estatales que comprometen la responsabilidad del Estado colombiano.

**“No se dio ninguna explicación clara de por qué ese día se había levantado el retén militar en la carretera que del Cerrito conduce a Málaga por donde huyeron los victimarios**. Por otro lado, el comandante de la estación de policía fue hallado responsable pero la sanción fue suspenderlo del cargo  por 20 días”, señala la abogada, quien agrega que ya se cumplen 7 años en los que no se produce ninguna actuación por parte de la Fiscalía.

Este acto de perdón es apenas una de las medidas simbólicas contenidas en las recomendaciones de la CIDH, entre las que también se encuentran la continuación de la investigación para lo cual se  suscribió un acuerdo de cumplimiento en el que el Estado se  comprometió a fortalecer ese aspecto. Además, se inició el trámite para indemnizar a los familiares por perjuicios materiales y morales.

<iframe src="http://co.ivoox.com/es/player_ej_11210999_2_1.html?data=kpafk5WdfZqhhpywj5aUaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5ynca3pxM7Oja3Jts%2BZpJiSo5bSqMbuhpewjainjo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="https://www.youtube.com/embed/f7ea2odJ55E" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
