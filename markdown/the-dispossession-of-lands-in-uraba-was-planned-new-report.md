Title: "The dispossession of lands in Urabá was planned": New report
Date: 2019-12-04 10:40
Author: CtgAdm
Category: English
Tags: Bajo Atrato, paramilitaries, Truth Commission, uraba
Slug: the-dispossession-of-lands-in-uraba-was-planned-new-report
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Despojo-de-tierras-copia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @CorpoJuridicaLi] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Communities of the northwestern Urabá region turned in two reports to the Truth Commission that describe the violence they lived within the context of the armed conflict. During the gathering, victims called on the Commission to identify the third parties involved in these crimes as part of an effort to end more than 30 years of impunity. ]

[The reports, written by the Interchurch Justice and Peace Commission, the Corporation for Judicial Freedom, the Foundation Forging Futures and the Popular Institute of Training, reveal 166,000 hectares of land, located in 11 municipalities in Urabá, were stolen between 1995 and 1997 by a diverse set of armed actors that pretended to appropriate and redistribute the land. This affected various ethnic and peasant communities in this region of Colombia.]

### **“The dispossession of lands in the Bajo Atrato was planned”**

[Martha Peña, of the Popular Institute of Training, said that the organizations that authored the report have accompanied the communities for years and collected first-hand testimonies that provided a base for the investigation. The report documented almost 500,000 cases of forced displacement, 1,200 forced disappearances and more than 120 victims of massacres, all committed between 1996 and 1997. ]

[As communities fled their lands in search of safety, companies that belonged to the corporate agriculture, infrastructure, palm oil and banana industry took ownership of the territory as part of what human rights organizations have said was an alliance between the private industry and paramilitary groups.]

[“When they left the Urabá region without leadership and silenced, they organized another plan to clear the territory,” said Peña, who added that impunity has reigned in these cases due to the permissiveness of the state. “We are not speaking of an issue that is unknown to the public but rather an issue that is talked about in terms of negation and impunity.”]

### **"Since ‘97, we need to know the truth, but 22 years have gone by without an answer”**

The reports were turned in to the Truth Commission only days after nine land rights activists from Turbo, Antioquia were captured by the police in a case that some have called a judicial "false positive." Ayineth Pérez, of the Earth and Peace organizations, told the Truth Commission that those captured are "peasants that have the court decisions in their hands and despite this, their rights are being disrespected."

[Ana del Carmen Martínez, social leader of the organization Cavida, added that she wants "to know who are the ones responsible for their \[first\] dispossession and our new forced displacement because we know that behind this, there are interests of third parties at play."]

The truth commissioner Ángela Salazar said that the usurpation of lands in the Urabá and the Bajo Atrato region has been systematic, as evidenced by the two reports. The truth commissioner Alejandro Valencia added that the investigation into crimes committed in this region of Colombia will be central to the work of the Truth Commission.

A delegate of the Special Peace Jurisdiction said that the court has made important advances in Case 004, which focuses on the Urabá region, as it identifies the responsibility of civilians, ex-combatants and state agents, such as members of the Armed Forces, in crimes committed in the last 30 years.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
