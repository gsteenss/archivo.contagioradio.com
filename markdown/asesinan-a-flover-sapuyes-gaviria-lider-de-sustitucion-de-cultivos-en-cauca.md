Title: Asesinan a Flover Sapuyes Gaviria líder de sustitución de cultivos en Cauca
Date: 2018-02-24 19:27
Category: DDHH, Nacional, Uncategorized
Slug: asesinan-a-flover-sapuyes-gaviria-lider-de-sustitucion-de-cultivos-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Flover-Sapuyes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RedFIC] 

###### [24 Feb 2018]

La información divulgada por la Red de Derechos Humanos Francisco Isaías Cifuentes, es que el líder de sustitución de cultivos de uso ilícito **Flover Sapuyes Gaviria** fue asesinado este 23 de Febrero cerca de su casa, en el municipio de Balboa, hacia las 4:30 de la tarde. Según testigos un hombre se bajó de una motocicleta y disparó contra el joven que intentó correr para salvaguardar su vida.

Flover era integrante de la **Coordinadora Nacional de Cultivadores de Coca, Marihuana y Amapola, COCCAM** del municipio y se desempeñaba como tesorero en la vereda La Esperanza, además era integrante de la Asociación Campesina de Trabajadores de Balboa, ASCATBAL que a su vez es parte del movimiento Marcha Patriótica.

### **Violentos se ensañan contra la sustitución de cultivos de uso ilícito** 

Recientemente la Fundación Ideas para la Paz publicó un informe en que da cuenta de la violencia que ha afectado a quienes, en los municipios, impulsan la sustitución cultivos de uso ilícito. Según la organización hay una relación directa entre los sitios con mayor número de hectáreas de coca y con mayor número de asesinatos.

“En los municipios con cultivos de coca la t**asa de homicidios aumentó 11% y en aquellos donde la sustitución comenzó, el alza fue del 33%**. En 36 municipios en los que la sustitución avanzó, la tasa pasó de 41.1 a 54.7 por cada cien mil habitantes” señaló la organización en su informe. Además concluye que “En la mayoría de los casos el aumento de la violencia letal estuvo vinculada con las disputas y reacomodamientos de los grupos armados al margen de la ley.”

\[caption id="attachment\_51750" align="aligncenter" width="525"\]![Ideas para la paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/sustitucion-y-asesinatos-525x600.jpg){.size-large .wp-image-51750 width="525" height="600"} Ideas para la paz\[/caption\]

Sin embargo, organizaciones como la propia COCCAM han señalado que la ausencia de presencia estatal y la incoherencia de la política de sustitución en algunos departamento en que se insiste con la erradicación forzada de cultivos está generando incertidumbre y no permite que los planes de sustitución  avancen con paso firme, generando focos de violencia que en muchos casos termina con el asesinato de líderes.

Así las cosas, los retos para intentar afianzar los PNIS en estos últimos meses de gobierno no son menores. Hay que resaltar que en muchas regiones los campesinos que han sobrevivido de la siembra de Coca, exigen que los planes de sustitución de cultivos sea integral, con adecuación de vías terciarias y garantías de comercialización. En este sentido el Estado no ha encontrado una ruta eficaz y por el contrario se siguen afianzando los poderes que controlan el mercado.
