Title: Desalojo en Tamarindo, un desastre humanitario
Date: 2015-12-11 19:07
Category: Nacional
Tags: alcaldía de Barranquilla, Barranquilla, conflicto armado, desalojo en Tamarindo, El tamarindo, ESMAD, Inspección General de Policía, Norma Baldovino, Policía de Barranquilla, Policía Nacional, Tamarindo, Unidad de Víctimas
Slug: desalojo-en-tamarindo-un-desastre-humanitario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/desalojo-tamarindo-51.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Justicia y Paz. 

<iframe src="http://www.ivoox.com/player_ek_9678854_2_1.html?data=mpukmp2ZeI6ZmKiakpyJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnwtHczNSPqc%2Bftcbaw9fNssXjhpewjdrSb8XZ1Mbg1tfJb8npzsbby9nFtsrjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Norma Balduvino] 

###### [11 Dic 2015] 

Después de que el pasado 9 de diciembre, cuatro camiones del ESMAD, en compañía de civiles armados con armas blancas y vestidos con camisetas rojas, desalojaron a las familias del Tamarindo y destruyeron cultivos, el acueducto y 3 casas de la zona, las autoridades continúan expulsando a la comunidad de las 120 hectáreas que han ocupado durante más de 20 años.

En palabras de Norma Balduvino, secretaria de la Asociación de Desplazados y Campesinos del Tamarindo, Asotracampo, la población califica este hecho como “lamentable y un desastre humanitario”, puesto que se les están despojando sus pertenencias y como víctimas del conflicto armado en Colombia, las entidades oficiales no les han garantizado sus derechos fundamentales.

Balduvino afirma que ni la alcaldía de Barranquilla ni la Unidad de Víctimas han sido garantes de de los derechos de la comunidad. Ella dice que con la única entidad pública con la que se ha llegado a un acuerdo es con la Inspección de Policía, en donde el organismo pospondrá la diligencia de desalojo hasta el próximo 18 de diciembre, sin  embargo las familias aseguran no tener a donde ir.

“Todos estamos con nuestros cultivos y animales; en este momento no hay donde llevarlos”, expone. No obstante, la entrevistada aclaró que una vivienda más será destruida antes de ese plazo. Menciona además que para las personas que tienen ganado como caballos, burros, pollos, cerdos, entre otros, es difícil trasladarse a otro lugar con tal cantidad de animales.

\[embed\]https://www.youtube.com/watch?v=1ophWBNAK2Q\[/embed\]

Por su parte, la alcaldía señala que a nivel de su jurisdicción tiene competencia con cuestiones urbanas y no con proyectos rurales, a pesar de que El Tamarindo se encuentra en la zona metropolitana de Barranquilla. La secretaria de Asotracampo expone que “ni los órganos distritales ni los departamentales han respondido por nosotros”.

La líder comunitaria lamenta que los representantes de la Unidad de Víctimas que estaban presentes durante el desalojo no hubieran apoyado a la comunidad. “Ellos estuvieron allí, pero no sirvieron como garantes”. Adicionalmente, dijo que “ellos no hicieron absolutamente nada por las personas que se quedaron sin sus pertenencias”. Cabe señalar que quienes han perdido sus ranchos, han dormido junto con las personas a las que la Inspección de Policía les otorgó el plazo.

Frente a la participación de la banda delincuencial de ‘Los Coletos’ en el proceso de desalojo, la comunidad de El Tamarindo se ha mostrado consternada, puesto que les preocupa el tema de la inseguridad. El día de la diligencia había 60 de ellos. Los pobladores denuncian que varios enceres y láminas de zinc han sido robadas en los días posteriores.

Sin embargo, este 11 de diciembre se tiene prevista una reunión con una comisionada de la Alta Consejería para la Presidencia, miembros de la Unidad de Víctimas y una delegada de la Iglesia Presbiteriana de Colombia, en la cual se denunciará el caso y se espera que se dé una solución al problema de tierras.
