Title: No hay una política clara de registro, documentación e identificación de desaparecidos en Colombia
Date: 2015-08-31 15:11
Author: AdminContagio
Category: DDHH, El mundo, Movilización, Resistencias
Tags: ASFADDES, Desaparición forzada Colombia, Diálogos de paz en la Habana, La escombrera, Medellin, MOVICE
Slug: no-hay-una-politica-clara-de-registro-documentacion-e-identificacion-de-desaparecidos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Medellín-desaparición.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: De La Urbe] 

<iframe src="http://www.ivoox.com/player_ek_7718625_2_1.html?data=mJyempuWeY6ZmKiak5eJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktCfycbmjdrSpYzk0NGSpZiJhaXoysjOjcjQpdPVjMnSjdfJq8rn1dfch5enb8XjxNrax9PYpcTdhqigh6eXsoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Adriana Arboleda, Corporación Jurídica Libertad] 

###### [31 Ago 2015] 

[El pasado lunes 27 de julio se llevó a cabo la diligencia ordenada por la Fiscalía, para iniciar el proceso de búsqueda en el sector de la Arenera, de La Escombrera en Medellín, y dar con los **restos de personas desaparecidas durante la realización de la Operación Orión en la Comuna 13, en el año 2002**.]

[De esta primera fase de remoción de escombros, aún no se tienen resultados, **se estima que la próxima semana el equipo de Arqueología Forense de la Fiscalía General de la Nación presente su primer informe**.]

[Adriana Arboleda, abogada de la Corporación Jurídica Libertad, asegura que **la desaparición forzada a nivel nacional ha sido un crimen invisibilizado** y pese a que desde el año 2000 se cuenta con una Ley sobre este delito, actualmente **no existe claridad en la cifra de los casos**.]

[Según el último informe de la Fiscalía se registran 45.000 casos, de los que, de acuerdo al registro de la Unidad de Desaparición Forzada desde 2010 a la fecha, **10.000 corresponden a Medellín, que ocupa el primer lugar en Colombia frente a este flagelo**.]

[El proceso de registro, documentación e identificación de personas desaparecidas forzadamente en Colombia ocurre en el marco de una **política poco clara para la investigación de los hechos**. Sí bien la legislación actual representa un avance en la materia y tiene lugar gracias a la lucha de las victimas desde hace cerca de 30 años, **las acciones concretas de Comisión Nacional de Búsqueda resultan poco operantes y se da la necesidad de reformularla**.]

[Organizaciones sociales como el MOVICE o ASFADDES, lideran una propuesta para que desde la Mesa de Diálogos de La Habana, el Estado construya una política efectiva en relación con la desaparición forzada en Colombia, que contemple el establecimiento de mecanismos de búsqueda verificación y entrega, junto con una Subcomisión de Verdad en el marco de la Comisión de la Verdad.]

[Por lo que adelantaron el día de ayer movilizaciones en la ciudad de Medellín para llamar la atención de la sociedad civil colombiana y el Gobierno para que se avance en el combate de este crimen, a través de acciones como la **consolidación de mecanismos efectivos de búsqueda, verificación y entrega**, en los que los miembros del ejército, la policía y entes estatales deben asumir su responsabilidad y cooperar efectivamente.]
