Title: Diálogo entre antiguos actores armados abrió la puerta a la verdad y la reconciliación
Date: 2019-11-15 17:44
Author: CtgAdm
Category: Nacional, Paz
Tags: AUC, comision de la verdad, ELN, EPL, excombatientes, FARC, Justicia y Paz, M19, Verdad y no repetición
Slug: dialogo-entre-antiguos-actores-armados-abrio-la-puerta-a-la-verdad-y-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EXcom.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ComisiónVerdadC] 

A lo largo de ocho meses y con el acompañamiento de la Comisión de la Verdad, el Centro Internacional de Justicia Transicional (ICTJ) y la gestión de ABCPAZ, 30 hombres y mujeres que alguna vez pertenecieron a actores armados como **Fuerzas Armadas Revolucionarias de Colombia - Ejército del Pueblo (FARC-EP), Ejército de Liberación Nacional (ELN), Autodefensas Unidas de Colombia (AUC), Movimiento 19 de abril (M19), Ejército Popular de Liberación (EPL), entre otros**, realizaron por primera vez en la historia del país, un trabajo en conjunto que permitió la entrega de un documento con consideraciones y balances sobre el Acuerdo de Paz que contribuirán a las garantías de convivencia y no repetición.

Los integrantes de esta mesa se concentraron en resolver interrogantes sobre a la guerra, los contextos de violencia y el impacto producido en las comunidades y territorios, con el fin de llegar  a conclusiones, compartir experiencias y demostrar cómo a través de diversas herramientas se puede hacer un tránsito a la paz, "despejados de las armas, entendimos la posibilidad  histórica que se nos presentaba con este empeño, que estamos seguros puede tributar con creces al logro de la paz y la reconciliación".

Juan Carlos  Villamizar ,integrante del Centro Internacional de Justicia Transicional, destacó la importancia de la capacidad de escucha que se desarrolló a lo largo de esos ocho meses para descubrir poco a poco, en medio de las diferencias,  las lógicas,  circunstancias y esfuerzos y encontrar al final un horizonte en común: el de decirle nunca más a la guerra.  [(Le puede interesar: Comisión de la Verdad expuso sus avances tras un año de trabajo) ](https://archivo.contagioradio.com/comision-de-la-verdad-expuso-sus-avances-tras-un-ano-de-trabajo/)

José Eleazar Moreno exintegrante de las AUC afirmó que se trata de **"construir con las víctimas lo que permite romper el espiral de la violencia" mientras que Arlex Arango, también excombatiente de las autodefensas, apuntó que pese a los abismos ideológicos, todos están en búsqueda de la paz y la reconciliación"**.

Por su parte, la comisionada de la verdad, Lucía González señaló que lo más alentador de este encuentro es "encontrar a un grupo de seres humanos que fue a la guerra y que ahora es capaz de hacer una reflexión ética y reconocer sus errores; y si ellos son capaces el resto del país también puede".

### Las consideraciones para aportar a la verdad después de la guerra 

Por medio de su declaración, los integrantes de la mesa resaltaron  la importancia de respaldar al **Sistema Integral de Verdad, Justicia, Reparación y No Repetició**n y  de orientar los avances del Acuerdo hacia las víctimas del conflicto. [(Lea también: Con Mesa Técnica, FARC se compromete con el esclarecimiento de la verdad)](https://archivo.contagioradio.com/con-mesa-tecnica-farc-se-compromete-con-el-esclarecimiento-de-la-verdad/)

A su vez rechazaron la posición asumida por "los sectores que siempre han trabajado por hacer trizas el Acuerdo de Paz", y resaltaron que aunque el narcotráfico sigue siendo una de las causales de violencia en Colombia, la guerra contra las drogas  no es el camino para combatir dicha problemática por lo que es necesario apoyar planes territoriales como los Programas de Desarrollo con Enfoque Territorial (PDET) y el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS), además, condenaron el asesinato sistemático de líderes y lideresas sociales, excombatientes y defensores de DD.HH,

"Nos apartamos de quienes insisten en persistir alzados en armas y de quienes tomaron la decisión de volver a ellas" agregó el comunicado. [(Lea también:La desaparición de Santrich no puede ser un impedimento para buscar la paz)](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/)

El padre Francisco de Roux se refirió a la entrega de la declaración y manifestó que "es posible ponerse más allá de nosotros mismos, de nuestros grupos, de nuestras organizaciones, de nuestros partidos políticos, porque la causa es más grande que nosotros mismos, **la posibilidad que en Colombia podamos vivir como lo merecemos, aceptándonos en nuestras diferencias**".

Estos son los firmantes e integrantes de la Mesa de reconciliación:

Álvaro Villaraga  
Carlos Arturo Velandia  
Luis Eduardo Celis  
Rodrigo Londoño  
Luz Amparo Jiménez  
Óscar Leonardo Montealegre  
Fabio Mariño  
Griselda Lobo  
Iván Roberto Duque  
Freddy Rendón  
Nodier Giraldo  
José Matías Ortiz  
Medardo Correa  
Ildefonso Henao  
José Eleazar Moreno  
Manuel Pirabán  
Rodrigo Pérez Alzate  
Fernando Luis del Río Beltrán  
Gabriel Barrios  
Alonso Ojeda  
Gabriel Ángel  
María Buendía  
Arlex Arango  
Fernando Hernández  
Vera Grabe  
Edwar Cobos,  
Raquel Vergara  
Óscar José Ospina  
Gloria Quiceno,  
Jorge  Iván Laverde

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
