Title: El historial de investigaciones contra Álvaro Uribe Vélez
Date: 2018-06-25 12:46
Category: Judicial, Nacional
Tags: alvaro uribe velez, Centro Democrático, Iván Cepeda
Slug: el-historial-de-investigaciones-contra-alvaro-uribe-velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Alvaro-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [25 Jun 2018] 

Según la senadora del Centro Democrático Paloma Valencia, la Corte Suprema de Justicia estaría preparando la orden de captura en contra del también senador, y líder de la colectividad, Álvaro Uribe Vélez. La Senadora, afirmó que esta orden de captura representa una venganza por la victoria de Iván Duque, y su propuesta de unificar las altas cortes.

![Captura de pantalla 2018-06-25 a la(s) 10.51.16 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-25-a-las-10.51.16-a.m.-624x483.png){.alignnone .size-medium .wp-image-54178 width="624" height="483"}

###### [Foto: @PalomaValenciaL] 

Aunque la Corte Suprema no ha hecho pública la existencia de dicha orden, no resultaría extraño que se diera en el futuro próximo, pues Uribe Vélez tiene investigaciones en curso por diferentes delitos.

### **Falsos Testigos** 

La más reciente (y sonada) investigación en contra de Álvaro Uribe Vélez tiene que ver con la telaraña de falsos testigos que se tejió tanto para favorecer al expresidente, como para culpar al Senador Iván Cepeda por la supuesta compra de testigos.

En este proceso y de acuerdo con las denuncias del senador Cepeda, Uribe habría buscado que miembros de grupos paramilitares denunciarán haber recibido dineros por parte del Senador del Polo Democrático, para declarar en su contra. (Le puede interesar: [“Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda”](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

En este caso, la corte determinó que las visitas que realizó Iván Cepeda a los testigos del caso Uribe fueron lícitas y ocurrieron en el marco de las funciones legislativas que tiene Cepeda como senador. Adicionalmente, la Corte encontró similitudes en los testimonios que acusan a Cepeda y tratan de exculpar a Álvaro Uribe de su presunta participación en la creación de “Los doce apóstoles”.

### **Los Doce Apóstoles** 

El grupo paramilitar Los 12 Apóstoles fue creado en la década del 90, conformado en la región de Yarumal, Antioquia y fue el culpable de desatar toda una ola de violencia en el territorio que dejó cerca de unos 570 asesinatos. De hecho, el Concejo de Estado ya ha emitido sentencias que responsabilizan a miembros de la Policía Nacional y el Ejército por colaborar o apoyar directamente esta estructura violenta.

Además, se ha investigado al hermano menor de Álvaro Uribe, Santiago Uribe Vélez por estar aparentemente relacionado con el grupo paramilitar gracias a los testimonios del exjefe paramilitar Diego Fernando Murillo, alias “Don Berna”; Olwan de Jesús Agudelo Betancurt, miembro de Los Doce Apóstoles y el mayor (r) de la Policía, Juan Carlos Meneses.

En declaraciones rendidas ante la fiscalía 10 de la Corte Suprema de Justicia en el proceso 14044 contra Alberto Osorio, alias “El mono de los llanos”, trabajadores de la hacienda “La Carolina” confirmaron que los hermanos Uribe Vélez eran dueños de esa finca, y que todas las semanas se realizaban reuniones en las cuales participaban tanto Osorio como Santiago Uribe. (Le puede interesar:[“Extrabajadores de “la Carolina” confirman nexos de los Uribe Vélez con el paramilitarismo”](https://archivo.contagioradio.com/tres-extrabajadores-de-la-carolina-propiedad-de-los-uribe-velez-confirmaron-paramilitarismo/))

### **Masacres de El Aro y La Granja ** 

El tribunal superior de Medellín compulsó copias contra el expresidente y hoy senador por su presunta participación en las masacres de El Aro y La Granja, así como en el asesinato del defensor de Derechos Humanos, Jesús María Valle. Hechos que ocurrieron mientras Uribe Vélez fue gobernador de Antioquía.

En declaraciones del asesinado paramilitar, Francisco Barreto, el exgobernador Uribe Vélez habría usado un helicóptero de la Gobernación de Antioquia en medio de la masacre de El Aro que tuvo lugar el 22 de octubre de 1997 y la habría corroborado personalmente.

Por su parte, el abogado de Derechos Humanos Jesús María del Valle, fue asesinado el 27 de febrero de 1998 en Medellín. El abogado había pedido en 1996 al gobernador Uribe que se protegiera la vida de la población civil de la Granja, pues el lugar vivía una grave crisis humanitaria. Posteriormente, el defensor de DDHH había denunciado la colaboración de la IV Brigada en las Masacres de El Aro y La Granja (ocurrida el 11 de junio de 1996).

En relación a estos hechos, el Tribunal Superior de Medellín señaló que: “El solo hecho de que las Fuerzas Militares permitieran su funcionamiento (de los paramilitares), de no combatirlos, como era su deber deber legal y constitucional, es indicador de su compromiso con esas organizaciones ilegales, y por supuesto de los actos y conductas punibles cometidas, igual ocurre con la gobernación de Antioquia y sus funcionarios de más alto rango”. (Le puede interesar: [“Compulsan copias contra Álvaro Uribe Vélez por dos masacres en Antioquia”](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/))

En razón de la participación por acción u, omisión en los hechos de la Granja, El Aro y el asesinato de Jesús María del Valle por parte de actores del Estado; así como porque en dichas acciones criminales hay características de sistematicidad, brutalidad e intención de aniquilar personas por un interés particular, estos delitos fueron declarados como crímenes de lesa humanidad por parte de la Corte Suprema de Justicia.

###### [Reciba toda la información de Contagio Radio en [[su correo] 
