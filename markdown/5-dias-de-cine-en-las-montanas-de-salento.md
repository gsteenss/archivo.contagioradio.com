Title: 5 días de cine en las montañas de Salento
Date: 2015-06-05 10:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine en Salento Quindío, Festival Internacional de Cine en las Montañas
Slug: 5-dias-de-cine-en-las-montanas-de-salento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/mdc-hotel-boutique-salento-vista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Desde hoy y hasta el próximo lunes 8 de junio, **Salento**, población ubicada al norte de la ciudad de Armenia, acogerá la primera edición del **Festival Internacional de Cine en las Montañas. **5 días continuos de exhibición, discusión y formación entorno a lo audiovisual.

Con Francia como país invitado de honor, la [programación](http://www.festivalinternacionalcineenlasmontanas.com/) está compuesta por una **selección de cine contemporáneo nacional e internacional**, incluyendo estrenos, trabajos de realizadores universitarios, y producciones resultantes de los talleres realizados en algunas veredas del municipio, **proyectados de manera simultánea en tres escenarios del mismo**.

[![10393710\_643950805734284\_6515422464114517005\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10393710_643950805734284_6515422464114517005_n-e1433457987442.jpg){.aligncenter .wp-image-9727 .size-full width="568" height="842"}](https://archivo.contagioradio.com/?attachment_id=9727)

La temática central del evento es **la concienciación sobre la naturaleza y el medio ambiente, el cuidado de los recursos y en particular con la relación con el paisaje natural cafetero**, utilizando como medio las diferentes producciones, documentales, argumentales, en formato de corto y largometraje, que integran la parrilla de programación.

Complementando la exhibición, la [programación](http://festivalinternacionalcineenlasmontanas.com/EL-FESTIVAL.php) académica incluye **los talleres, "Flora y fauna; Cine en las Alturas; Filminutos de realismo natural"**, que se lleva a cabo durante los cuatro días posteriores al Festival, donde expertos en el tema y Productoras Audiovisuales de la Región darán las pautas para realizar tomas de video y fotografía en la alta montaña, con el fin de realizar filminutos de naturaleza.

El **Taller de realización Cine hecho a mano,** es diseñado especialmente para los estudiantes de los últimos grados de bachillerato de las 17 veredas que conforman el Municipio de Salento, enfocado en la apreciación y realización audiovisual, siendo un acercamiento al lenguaje cinematográfico. Una propuesta que busca, al finalizar, la realización de 2 cortometrajes por cada semestre del año en las instituciones educativas participantes.

**El laboratorio de Videomapping,** se selecciona un grupo de máximo 8 personas que reciben la formación para proyectar sobre superficies arquitectónicas y principios de animación 3d, quienes serán los encargados del cierre del Festival, proyectando sobre las terrazas de las casas típicas de la aldea de artesanos.

Dentro de la lista de **50 invitados**, nacionales e internacionales, se destaca la presencia del i**nvestigador, docente y critico de cine Juan Guillermo Ramírez**, quien realizará un recuento por los 20 Cuadernos del Cine en Colombia publicados por la Cinemateca Distrital y se invitará un escritor de alguno de los tomos.

También se destaca la participación de **Augusto Bernal Jimenes**, **critico, investigador y director de la Escuela de Cine Black María, **y **Alvaro Concha Henao, investigador, columnista, y profesor universitario de cine**; en el lanzamiento del Segundo Tomo del libro "**Historia Social del Cine en Colombia**", escrito por los dos invitados.

El primer Festival Internacional de Cine en las montañas, cuenta con el apoyo de la **Alianza Francesa, Parques Nacionales Naturales de Colombia, Corporación Autónoma Regional del Quindío, La escuela de Cine Black María, La Universidad del Quindío y la Cinemateca Distrital de Bogotá,** entre otros.
