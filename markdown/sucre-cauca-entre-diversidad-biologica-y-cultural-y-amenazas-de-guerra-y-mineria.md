Title: Especial Sucre Cauca, diversidad biológica y cultural
Date: 2015-05-19 19:51
Author: CtgAdm
Category: Comunidad, Otra Mirada
Tags: agricultura, Ambiente, Cauca, Cerro de Lerma, Cerro negro, cultivos del macizo colombiano, macizo colombiano, riqueza natural Cauca, Sucre, turismo en Sucre
Slug: sucre-cauca-entre-diversidad-biologica-y-cultural-y-amenazas-de-guerra-y-mineria
Status: published

##### Foto: Flicker.com 

<iframe src="http://www.ivoox.com/player_ek_4519486_2_1.html?data=lZqem5mceo6ZmKiakpqJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl9DW08qYx9GPh8bm09SYsMrLttCf2pCwx9fWs4zYxpC5x9fRpYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://www.ivoox.com/player_ek_4519472_2_1.html?data=lZqem5mbdo6ZmKiakpuJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbiysjW0s7Tb8XZjLjixdfJaZO3jKjO18jFb9bijNHiycbWb83gxtPcjcnJb9Pd0trS3MbXb8%2FVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Naslid Muñoz, ingeniera ambiental] 

El municipio de Sucre, ubicado en el departamento del Cauca, cuenta con gran biodiversidad, gracias a que está situado en el macizo colombiano, es por ello que cuenta con importantes **fuentes hídricas, variedad climática y agrícola**,  además de **atractivos turísticos naturales.**

Sucre, hace parte de la cuenca del río Patía, desde donde nace el río Mazamorras  y la quebrada Los Huevos, dos fuentes hídricas que abastecen de agua aproximadamente **8 mil habitantes del municipio y de varios municipios aledaños.**

Naslid Muñoz, ingeniera ambiental, en compañía de su equipo ambiental, vienen desarrollando **dos inventarios basados en la fauna y flora** con la que cuentan los dos afluentes.

“Por la variedad de fauna y flora, para conservar las riquezas, en la comunidad se adelantan campañas y proyectos de reforestación para cuidar esa riqueza”, señala la ingeniera, quien resalta que la respuesta de los pobladores ha sido positiva, porque las personas entienden que su  principal riqueza son los ríos y toda la naturaleza, por lo tanto "**son consientes que deben de preservar los recursos”.**

Aunque según sus habitantes, el municipio de Sucre y en general el Cauca se caracteriza a través de los medios de información, como una zona peligrosa por el conflicto armado, lo cierto es que esta parte del Cauca cuenta zonas turísticas que solo se encuentran en esta región  como la** Quebrada buena vista**, que es un balnearios que atrae a personas de áreas cercanas. También posee riquezas naturales tales como el **Cerro de Lerma** que tiene gran importancia ecológica, ya que en este último se encuentran los monos maiceros y se puede observar la panorámica del sur de Colombia, según dice Naslid Muñoz.

Así mismo, a tres horas del municipio se encuentra ubicado el **Cerro Negro**, donde nacen las dos quebradas del río Mazamorras, que abastece gran parte del municipio y posee una parte boscosa que provee el oxígeno a los habitantes de Sucre.

Otra de las riquezas es la variedad de temperaturas, por lo que allí se tienen **los tres climas,** de manera que la agricultura pasa por el **café, hasta la caña de azúcar y la mora**. Sin embargo, la ingeniera ambiental asegura que hay una falencia ya que pese a la diversidad en climas, los campesino no cultivan papa ni cebolla, aunque se está trabajando en ese tema con la comunidad, afirma Naslid.

El motor cultural de este municipio son los jóvenes y los niños que diariamente se reúnen en torno a diferentes actividades culturales como las artesanías y la construcción de obras de arte que los mismos jóvenes enseñan a producir.

Sin embargo, a pesar de la riqueza y la permanente actividad, en el edificio que se construyó como Casa de la Cultura se encuentra la estación de policía, en medio del casco urbano, lo que en el contexto de guerra pone en riesgo a los habitantes de Sucre y se constituye en una violación al DIH.

Tanto la propia comunidad, como las autoridades locales están de acuerdo en la necesidad de restablecer el espacio para la actividad artística y cultural esperando que la estación de policía se construya en el terreno que debe ser asignado con celeridad.
