Title: Más de 10.000 familias en Putumayo afectadas por incumplimiento del Gobierno en acuerdo de sustitución
Date: 2020-03-12 17:36
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: ESMAD, PNIS, Putumayo
Slug: mas-de-10-000-familias-en-putumayo-afectadas-por-incumplimiento-del-gobierno-en-acuerdo-de-sustitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Campesinos-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/yury-quintero-se-continua-estigmatizando-protesta-en_md_48880806_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Yuri Quintero, Red de Derechos Humanos del Putumayo

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Putumayo / Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cinco municipios del Putumayo completan un mes en medio de movilizaciones campesinas que exigen el cumplimiento del Programa Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS) por parte del Gobierno que ha dilatado los diálogos y en su lugar han acudido a la erradicación forzada y al uso desmedido de la fuerza por parte del Escuadrón Movil Antidisturbios que continúa estigmatizando la protesta y a las poblaciones campesinas. [(Lea también: Erradicación forzada provoca múltiples violaciones de DD.HH. en Teteyé, Putumayo)](https://archivo.contagioradio.com/erradicacion-forzada-violaciones-ddhh-teteye/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se continúa estigmatizando la protesta en Putumayo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con el fin de ser escuchados, el campesinado se ha movilizado e impedido el paso del transporte vinculado a proyectos petroleros en lugares como el corredor Puerto Vega, Teteyé en Puerto Asís o el corredor vial del corredor San Pedro- Arizona de Puerto Caicedo lo que ha derivado en choques entre la población y el ESMAD.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 12 de marzo, en el marco de las movilizaciones, las comunidades denuncian que cinco campesinos fueron heridos por parte del ESMAD con rifles calibre 12 causando heridas en su cuerpo y rostro, aunque el campesinado ha demostrado voluntad de diálogo y han existido acercamientos para el diálogo, aún continúan que el Gobierno ofrezca soluciones viables para la población. [(Le puede interesar: Comunidades campesinas movilizadas en Putumayo son agredidas por la fuerza pública)](https://archivo.contagioradio.com/comunidades-campesinas-movilizadas-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explica [Quintero](https://twitter.com/Contagioradio1/status/1238163354201530368), En 2017, antes de la firma del acuerdo regional del PNIS, el Gobierno realizó una pre inscripción en la que participaron cerca de 27.000 familias que buscaban hacer parte del programa, su vinculación dependía de una etapa de socialización, capacitación y protocolos de seguridad que fue dilatada por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Para el momento de la firma, las 27.000 familias pasaron a ser cerca de 21.000 que son las que hoy está en el sistema, de ellas hay cerca de 11.000 atendidas, mientras el resto han sido suspendidas y aguardan a que cumpla con lo pactado**, "estamos hablando de un porcentaje bastante alto que no ha sido atendido, mientras que las que han sido atendidas se cumplió con el sostenimiento alimentario y aunque esa etapa ya se terminó de forma paralela debía hacerse una asistencia técnica, una de proyectos familiares y un proyecto macro para la comunidad y eso tampoco se ha hecho". [(Le puede interesar: Aspersión aérea con glifosato, una estrategia fallida que se repite)](https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde el Gobierno afirman que están buscando los recursos suficientes para continuar con el programa, la defensora de DD.HH. cuestiona que sí existe dinero para la erradicación forzada, la fumigación y el transporte del ESMAD, incluso a las regiones más apartadas del departamento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El afán de la Policía es el de mostrar resultados"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Yury Quintero, integrante de la Red de Derechos Humanos del Putumayo explica que a su vez, la Policía Nacional lanzó un comunicado a través del que afirma haber judicializado a tres personas vinculadas a la "obstrucción de vías" en el marco de las movilizaciones. Según, la defensora, dicha situación ha generado una crisis en la intención de mantener un diálogo con el Gobierno, representado en la región por la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las personas mencionadas por el comunicado: Paola Franco, Jhorman Rivera Reyes y a Nelson David Romero desmintieron haber sido capturados o tener imputación de cargos, sí fue detenido un joven de nacionalidad ecuatoriana llamado Jefferson Gustavo Moreno que habita en Puerto Asís a quien la Policía acusa de participar en la protesta pese a que este afirma no haber hecho parte de la movilización. "Lo que la Fuerza Pública busca es dilatar la movilización y generar terror para que las personas no se movilicen", afirma Yury Quintero.

<!-- /wp:paragraph -->
