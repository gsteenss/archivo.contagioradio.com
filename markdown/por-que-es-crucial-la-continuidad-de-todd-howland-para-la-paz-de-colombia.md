Title: ¿Por qué para ONGS es crucial el mandato de la OACNUDH en Colombia?
Date: 2017-03-24 18:04
Category: DDHH, Nacional
Tags: Alto Comisionado de la ONU, todd howland
Slug: por-que-es-crucial-la-continuidad-de-todd-howland-para-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Todd.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [24 Mar 2017] 

Todd Howland, se ha convertido en una de las **figuras más representativas en el país a la hora de mediar por la paz de Colombia y los derechos humano**s. Desde hace 5 años, en las Naciones Unidas, fue nombraron como Alto Comisionado para los derechos humanos. A su vez, ha tenido uno de los papeles más importantes durante el proceso de paz que se gestó con las FARC, velar por los derechos de las víctimas, sin embargo, Howland podría ser retirado de su cargo.

Por este motivo, más de 280 organizaciones sociales y un sin número de usuarios de redes sociales, le expresaron al secretario de las Naciones Unidas, Antonio Manuel de Oliveira Guterres, a través de una carta, que replanteara esta decisión y permitiera que **Howland continué en el apoyo a Colombia, en el proceso de implementación de los Acuerdos de Paz y como Alto Comisionado para los derechos humanos.**

En la misiva, las organizaciones señalan que el compromiso que Howland ha tenido con las poblaciones más vulnerables del país, la constante mediación por el diálogo entre el Estado Colombiano y diversos sectores sociales y populares, y sus aportes a l**os procesos de paz con el ELN y el EPL, son indiscutibles y sumamente importantes en el escenario de implementación que se viene para el país**.

De igual forma señalan que los diferentes informes que han salido desde la oficina del Alto Comisionado sobre el estado de los derechos humanos en Colombia, han sido “**acertados en sus recomendaciones”** frente a las adversas situaciones que aún debe superar el país. Le puede interesar: ["Impunidad, la mayor preocupación de las organizaciones de DDHH con la JEP"](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/)

Minutos después del fuerte movimiento que se vivió en redes sociales, Antonio Manuel de Oliveira Guterres, respondió a través de su cuenta de Twitter que **"Muchas gracias por vuestra información. Muy importante para nosotros. Seguramente voy a consultar con mi colega el Alto Comisionado de Derechos Humanos. Un saludo muy cordial".**

### *Carta de las organizaciones* 

*Respetado Secretario General, Respetado Príncipe Zeid Al Hussein:*  
*Cordial saludo.*

*Colombia atraviesa una coyuntura muy especial, en la medida en que los procesos de paz que se adelantan están partiendo su historia en dos grandes momentos: el pasado, atravesado por una permanente tragedia humana producto de un prolongado conflicto armado interno; y el presente: la construcción de una paz definitiva. Todo en medio de una complejidad que expresa lo difícil de este tránsito, donde algunos actores se resisten a los cambios, pero donde muchos otros creemos en la esperanza de un país tranquilo y próspero.*

*Para lograr este segundo objetivo, múltiples actores locales, nacionales e internacionales vienen jugando un papel determinante, cuyas tendencias y aportes son cada vez más evidentes e importantes. Uno de ellos es la Oficina del Alto Comisionado de Nacionales Unidas para los Derechos Humanos y su representante en Colombia, señor Todd Howland. Su solidez en el enfoque de derechos humanos, su compromiso con los sectores más vulnerables del país, la constante mediación para el diálogo entre el Estado colombiano con diversos sectores sociales y populares, son indiscutibles; su permanente presencia en los más remotos lugares donde se vive de verdad el conflicto, así como el aporte sustancial a las negociaciones de paz realizadas entre el Gobierno nacional y las dos insurgencias de las FARC-EP y el ELN.*

*Capítulo especial merecen los informes anuales donde se muestran los avances en materia de derechos humanos, pero también sus recomendaciones acertadas sobre las situaciones que aún deben ser superadas. El reciente informe publicado el jueves 16 de marzo es un indicador del análisis de nuestra realidad; de ahí la extraordinaria afluencia de público a la presentación del mismo, y el cubrimiento de los medios de comunicación.*

*Hoy por hoy, el señor Howland se ha convertido en un actor clave y muy positivo para el avance del proceso.  No es extraño el gran respeto, admiración y agradecimiento que un amplio espectro político y social del país muestra por él, entre ellos el movimiento social (indígenas, campesinos, afrodescendientes, mujeres, víctimas, sindicalistas, ambientalistas, defensores de derechos humanos, periodistas, partidos políticos, entre otros) y estamos seguros que igualmente algunas entidades y actores institucionales.*

*Hoy la esperanza de la construcción de paz pasa, en primera medida, por la implementación cierta de los Acuerdos entre el Gobierno nacional y las FARC, y en segunda, por el avance en las conversaciones con la insurgencia del ELN. Por ello, la relación entre la Oficina en Colombia del Alto Comisionado de Nacionales Unidas para los Derechos Humanos (OACNUDH)  y la Misión 2 de verificación de la ONU es sustancial, para lo cual, tanto la OACNUDH como su representante actual, son una garantía del logro que se podrá obtener.*

*Por lo anterior, quienes firmamos esta comunicación solicitamos a Ustedes, con un énfasis muy especial, que el señor Todd Howland permanezca en nuestro país para que pueda darle continuidad a la tarea encomendada a él por el Sistema de Naciones Unidas hace cinco (5) años. Los excelentes resultados de la gestión del señor Howland nos permiten afirmar que su continuidad en Colombia es garantía del cumplimiento cabal del mandato de la OACNUDH en el país, que hoy requiere una conducción altamente cualificada por los desafíos que vive la transición a la democracia.*
