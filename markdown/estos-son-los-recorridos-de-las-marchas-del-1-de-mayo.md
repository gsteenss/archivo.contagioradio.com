Title: Estos son los recorridos de las marchas del 1 de Mayo
Date: 2019-05-01 08:43
Author: CtgAdm
Category: Movilización, Nacional
Tags: Día del Trabajador
Slug: estos-son-los-recorridos-de-las-marchas-del-1-de-mayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-01-at-8.25.09-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

En el día del primero de mayo, trabajadores de todo el país saldrán a las calles para exigir sus derechos. Estos serán los recorridos y puntos de encuentro de las marchas de hoy en Bogotá.

Puntos de encuentro y los recorridos:

**Centro:**  
1. Saldrán desde el Parque Nacional, el Planetario Distrital, la Torre Colpatria a la Plaza de Bolívar. Estas personas se movilizarán por la carrera Séptima, calle 32, carrera 13, carrera 10 y avenida Jiménez hasta llegar a la Plaza de Bolívar.

2\. Una segunda movilización saldrá de la Plazoleta de Las Nieves, tomará la carrera Octava, avenida Jiménez, carrera Séptima y llegará a la Plaza de Bolívar.

**Sur:**  
El punto de encuentro de esta movilización será contiguo al Polideportivo de Molinos. A este punto llegarán movilizaciones procedentes de las localidades de Ciudad Bolívar, Usme, Bosa, Kennedy y Rafael Uribe Uribe. Estas se movilizarán de la siguiente manera:

**Usme:**  
Saldrán desde el barrio Tenerife. Su recorrido continuará por el barrio Yomasa y, posteriormente, se desplazarán por la avenida Caracas hasta el Centro Comercial Caracas (Calle 52 sur con avenida Caracas).

**Bosa:**  
El punto de concentración será en la Plaza Fundacional, en Bosa Centro; tomarán por el sector de Tropezón y luego hacia Bosa Estación; continuarán hacia la autopista Sur hasta la Av. Villavicencio. En este punto se unen las movilizaciones procedentes de las localidades de Kennedy y Ciudad Bolívar.

**Kennedy:**  
Parte del parque contiguo a la Alcaldía Local y tomará la Av. Primero de Mayo para salir a la avenida Villavicencio. Esta movilización se dirigirá hacia la Casa de la Cultura (Ciudad Bolívar).

Allí se reunirán las movilizaciones de Kennedy y Ciudad Bolívar, y realizarán un recorrido interno dentro de la localidad. Saldrán al puente Meissen y, posteriormente, tomarán toda la avenida Caracas con dirección al Centro Comercial Caracas.

Una vez reunidas estas cuatro movilizaciones, saldrán hacia la bahía contigua al Polideportivo Los Molinos, en la localidad de Rafael Uribe Uribe.

**San Cristóbal:**  
Se hará una movilización interna que partirá desde el barrio Juan Rey y llegará al parque Columnas.

**Norte:**  
En la Calle 85 con Carrera 15 habrá una concentración de población venezolana. CIAPS

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-01-at-8.25.09-AM-300x243.jpeg){.aligncenter .size-medium .wp-image-66133 width="300" height="243"}

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
