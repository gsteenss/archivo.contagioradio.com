Title: Dabeiba
Date: 2020-01-14 18:20
Author: Antonio Jose Garcia
Category: Opinion
Tags: conflicto armado, Dabeiba, memoria
Slug: dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/dabeiba-e1477516905457.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-49-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-50-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-18-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-18-1-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-51-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-18-2-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-22-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

El Imaginario colectivo de los habitantes del occidente antioqueño está cundido de violencias dolorosamente enraizadas en sus mentes por años tras años de  guerras. Desde el genocidio contra sus primeros habitantes, los indígenas Emberas, cuyos pocos descendientes aun se resisten a desaparecer definitivamente, reducidos a algunos territorios dentro del vasto continente que poseían desde la prehistoria. De ahí en adelante, todas las guerras pasaron por allí. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

También  han pasado los dolorosos procesos de  las violencias políticas del siglo 20 y comienzos del 21  que se han reflejado sin compasión contra los habitantes de estas regiones, que aun se sienten y  que generan no solo unas memorias de incertidumbre y terror, que hacen a los habitantes mirar con desconfianza cargada con la pesadumbre del pasado, el futuro que les ofrece el indolente establecimiento Colombiano. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hace ya varias décadas, en Medellín, cuando era  pre adolescente y recién estrenando el cerebro con el afán de leer cualquier cosa que fuera leíble y estuviera a mi alcance,  revolviendo y desempolvando libros en la biblioteca familiar encontré lecturas que devoraba ávidamente, encontrando interés en casi cualquier tema. Así  leí por ejemplo, las mil y una noches; muchos apartes que me interesaron de un diccionario que ampliaba los significados con historias y gráficas relacionadas; los capítulos históricos de “el libro de las narraciones interesantes” que contenía  la enciclopedia, “el tesoro de la juventud”; “historia de roma y de los romanos”; “la isla del tesoro”; “ Robinson Crusoe” ; “ La guerra y la paz”; “ Los primitivos”; “Historia de colombia”; “Antioquia en las guerras de independencia” etc., Pasaba de una lectura a otra  hasta encontrar en alguno de esos días, un libro que hablaba de regiones que en principio no me decían nada, pero que hojeándolo vi unas fotos horrorizantes que me impactaron y me hicieron perseverar en la lectura mas angustiante que había tenido en mi corta vida. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El libro se llama “lo que el cielo no perdona”, escrito por  un cura liberal que tuvo que huir despavorido iniciando la década de los 50, hacia Medellín dejando descubierta  la parroquia y la feligresía que le había asignado el obispo de Santa Fe de Antioquia. Con esa lectura comencé a recrear en la mente una imagen de las montañas y los personajes de Cañasgordas,  Peque, Urama, Uramita, Juntas de Uramita, Antasales, Dabeiba, y otra cantidad de lugares y personas que a través de la versión del cura imaginaba en blanco y negro, escenarios oscuros, personajes también oscuros en medio de un ambiente de frío  permanente siempre a punto de lluvia y una luz mortecina de un crepúsculo invernal que volvía el paisaje lleno de rincones atemorizantes; caminos sinuosos bordeando bosques donde posiblemente se toparía después de la próxima curva uno o varios cadáveres humanos con crueles signos de violencia  inhumana, expuestos grotescamente para intimidar quienes pasaran por allí o cruzando el siguiente río, una mortal emboscada de enemigos armados a quienes nunca conoció.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la realidad lo que entonces imaginaba es una vasta región de montañas encumbradas en todos los pisos términos, sobre la cara occidental de la cordillera de dicho nombre, continente de vastos territorios entre el páramo de Frontino, el nudo de Paramillo y ríos afluentes de la cuenca del río Atrato, hacia donde vierte la mayoría de sus aguas, recolectadas  en gran extensión por el Riosucio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las escenas terribles que apenas vislumbraba mi imaginación, no se aproximaban un poco siquiera a lo que ocurrió durante muchos años, días y noches muchísimos niños, niñas, adolescentes, hombres, mujeres, ancianos y ancianas que vieron, vivieron, sufrieron,  huyeron y en muchos casos murieron por el odio que asoló en esa geografía.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

No me interesó ni entendí la tendencia política del cura Blandón que evidentemente reflejaba el escrito. Seguramente me salté esos capítulos. No entendía el ataque e inculpación emergente en cada oportunidad a un sujeto de nombre Laureano Gomez y su gobierno a quien denominaba “el Basilisco”,  tampoco la denuncia esporádica a un tal “Monseñor Builes”, su eclesial jefe incendiario, ni la exaltación permanente que hacia el autor de un personaje llamado “teniente coronel Gustavo Rojas Pinilla” a quien hacia ver como el redentor de Colombia, poniendo al mismo nivel heroico de Colón y de Bolivar, así como a su loado “Ejercito Nacional”.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lo que ocurrió en esos lares, relata en el libro, fue una guerra sin precedentes y sin justificación. Ocurrió y seguramente fue mucho peor por que muy poco da cuenta de lo que hacían los guerrilleros liberales que también se armaron para protegerse de la “chulavita” o policía política del régimen conservador y sus paramilitares, llamados “los pájaros” que eran los victimarios según el relato del cura.  Masacres, homicidios selectivos, torturas, violencia sexual, desplazamiento forzado, se hicieron los medios de expresión, eran el lenguaje mediante el cual se expresaba la inconformidad política, de unos y otros y en la mitad, como siempre el campesinado y los indígenas completamente inermes y desprotegidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha violencia se aplacó durante algunos años  por las políticas de paz del gobierno que conllevó la concesión de amplias amnistías e indultos a los guerreros y la implantación de el llamado “Frente Nacional” que  tranquilizó las ansias burocráticas de liberales y conservadores pero exacerbó las de quienes quedaron excluidos de acceder al poder. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Muchos años después, ya adulto, viajé por la carretera que de Medellín conduce a Urabá por primera vez y tuve oportunidad de conocer esa región y algunas de esas poblaciones, que inmediatamente me devolvieron a la mente el libro, sus escritos y sus  escabrosas fotografías.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Posteriormente pasaría por allí varias veces, no solo con la sensación de miedo de la memoria del libro, sino con la de terror que se vivía en esos momentos y que aun se siente, por la nueva guerra que entonces llegó con mas fuerza  con nuevos actores políticos y armados, legales e ilegales con mejores armas, más tecnología, más odio y más violencia. Esta nueva guerra aparentemente se terminó por acuerdos de paz con los actores, en este caso las AUC y posteriormente las FARC. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hace poco regresé,  justo antes de conocerse la existencia de fosas comunes con múltiples víctimas de “falsos positivos” en el cementerio de Dabeiba. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Conocí de primera mano la tragedia humanitaria que se vivió allí recientemente  y recordé el libro de el cura Blandón que da cuenta de la violencia de hace 60 años. Pensé en la amnistía que puso fin a la violencia de entonces que dejó, desplazados, despojados, excluidos, viudas, viudos, huérfanos y  cientos de miles de muertos en los campos y ciudades de colombia, (120.000 hasta mas de 300.000 según investigadores) y contrastó con las cifras del actual conflicto que se desactiva poco a poco mediante acuerdos de paz. Luego conocí y admiré  los esfuerzos que hacen las víctimas de esos grupos por sobreponerse a su tragedia, por ser resistentes y también resilientes, por reconstruir desde adentro, partiendo del perdón, el tejido social dañado por la guerra. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ahora  encuentro que en esa misma región, los mismos nombres y accidentes geográficos  que a través de un mecanismo de justicia transnacional se han encontrado decenas de fosas comunes con cuerpos de los llamados “falsos positivos” que en el fondo son desaparecidas por “Razones de Estado”, o sea el aterrador  “body Count” que no es mas que dar resultados a cualquier costo dentro de la tácita política gubernamental de “todo vale”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Releo el libro del cura Blandón y encuentro que nada cambió, que la violencia de hace 50 años sigue, escalada, terrible y cada vez mas escabrosa, que la violencia de hace 30 años sigue generando víctimas  día a día, se renueva con otros actores pero con el mismo absurdo transversal de “todo vale” y que los acuerdos de paz, destinados a finalizar esto, se diluyen en el sinsentido de las políticas de gobierno. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuál puede ser la diferencia?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

¿Qué hizo falta para que el conflicto político liberal-conservador no se repitiera como se repitió, pues aterradoramente se recicló algunos años después en el conflicto político del establecimiento contra los “comunistas” que se escaló a formas impensables y ha dejado 8 millones de víctimas, mas de 200 mil víctimas de desaparición forzada mas de 300 mil muertos, mas de 5 millones de personas desplazadas y mas de 5 millones de hectáreas despojadas en los últimos 30 años en todo el país? Y entonces me pregunto: ¿que hay que hacer para que este proceso de paz actual tenga éxito, los procesos anteriores se afiancen y la violencia no se recicle nuevamente?

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Me pregunto  también, ¿qué hizo Colombia tan mal, que la vida humana adquirió el valor de mercancía siempre mal remunerada?  ¿Por qué algunos colombianos han bajado o suprimido los limites morales para ser capaces de matar un ser humano para ganarse un premio o  simplemente cumplir la meta de muertos que se les impone?  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

¿Cómo haremos los colombianos para  que esas memorias, necesarias para dar la debida connotación  que merecen las víctimas, que a pesar del perdón que conceden,  siempre llevarán la carga del dolor que se les causó, en vez de odio resalte la resiliencia, la des- victimización,  la restauración de la dignidad de la persona que deja de ser víctima y se sobrepone a la violencia recobrando su condición de persona  integra que mira el futuro sin ataduras de animadversión y resentimiento?. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

¿Como hacerlo en medio del conflicto, de la victimización que continúa ahora, como siempre, por la indolencia del estado colombiano?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y entonces me llega a la mente la pregunta y me adhiero a la respuesta del poeta y filosofo Gonzalo Arango:   

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Yo pregunto sobre su tumba cavada en la montaña: ¿no habrá manera de que Colombia, en vez de matar a sus hijos, los haga dignos de vivir?*
>
> <cite> Gonzalo Arango </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

*Si Colombia no puede responder a esta pregunta, entonces profetizo una desgracia: Desquite resucitará, y la tierra se volverá a regar de sangre, dolor y lágrimas.”*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**ANTONIO J. GARCÍA**

<!-- /wp:paragraph -->
