Title: Boletín informativo Junio 4
Date: 2015-06-04 17:00
Category: datos
Tags: Asamblea Parlamentaria Eurolatinoamericana, comision de la verdad, INCODER recuperación de la tierra, Ingrid Vergara, MOVICE, Rodolfo Benítez Verson, Senador Ivan Cepeda, Yessica Hoyos
Slug: boletin-informativo-junio-4
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4596573_2_1.html?data=lZqmmJqbd46ZmKiakpmJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZmRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Rodolfo Benítez Verson**, delegado de Cuba para el proceso de conversaciones de paz, **anunció hoy desde la Habana que se creará una Comisión de la Verdad**, **una vez se hayan firmado los acuerdos de paz entre el gobierno colombiano y la guerrilla de las FARC.** Esta Comisión de la Verdad tendrá un tiempo limitado de duración, con participación de las víctimas y la información que se recoja servirá para el reconocimiento de las mismas.

-Este viernes 5 de junio, se realizará la audiencia pública: **Análisis del cumplimiento de las obligaciones del Incoder frente al acceso y recuperación de la tierra**, con el objetivo de los resultados en materia de acceso a tierras de los campesinos por parte del Incoder. Habla **Iván Cepeda**, senador del **Polo Democrático Alternativo**.  
-Este miércoles en la **Asamblea Parlamentaria Eurolatinoamericana**, se iba a escuchar la voz de **Yessica Hoyos**, como **víctima y vocera de las Plataformas de derechos humanos en Colombianas**, sin embargo, **la abogada fue censurada, estigmatizada y tildada como representante de la FARC-EP**, argumento que usaron 16 parlamentarios para votar en contra de su intervención.  
-En el departamento de Sucre, **en la Villa de San Benito de Abad Ciénaga de Sispataca**, el **Movimiento de Víctimas de Crímenes de Estado**, denunció que el pasado 21 de mayo, **cuatro hombres armados ingresaron a ese lugar, rompieron la cerca que protegía los cultivos y los destruyeron**. Habla **Ingrid Vergara** del **Movimiento de Víctimas de Crimenes de Estado** MOVICE.
