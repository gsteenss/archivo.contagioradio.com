Title: Acción comunitaria en defensa del humedal "La Conejera"
Date: 2015-03-28 18:28
Author: CtgAdm
Category: Ambiente, Hablemos alguito
Tags: Acción comunitaria humedal "La Conejera", Campamento en defensa del humedal la "Conejera", Recuperación humedales Colombia, Resistencia humedal "La Conejera""
Slug: accion-comunitaria-en-defensa-del-humedal-la-conejera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/descarga-8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Angie Ortiz Twitter 

###### **Entrevista con [activistas] del Campamento en defensa del humedal la "Conejera":** 

<iframe src="http://www.ivoox.com/player_ek_4275826_2_1.html?data=lZekl52Weo6ZmKiakpaJd6Kkl4qgo5WUcYarpJKfj4qbh46kjoqkpZKUcYarpJKww9LUpc7Zz9ncjcrSb8XZx8rb1caPqMbgjM3iz8rIpc2frcaYpdTSqcvZ08aYx9OPl9bWwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Los **Humedales** tienen un gran **valor por ser ecosistemas con gran riqueza biológica,** social y cultural, aproximadamente el 20 % de la superficie de Colombia está cubierto por humedales y el **87% de la población vive alrededor de ellos**. Son parte del sistema de áreas protegidas que según la legislación ambiental de Colombia se entienden como  “el conjunto de espacios con valores singulares para el patrimonio natural”.

Según la tradición **Muisca los humedales o Chupkuas en su lengua Muyskkubun eran el lugar donde las mujeres iban a dar a luz** pues sus aguas frías ayudaban en el proceso de parto y los niños que nacían eran entregados a la diosa de agua dulce Zie.

Entre las tareas que tiene un humedal están que son **productoras de oxígeno,** regulan el ciclo hídrico previenen inundaciones, recogen sedimentos, funcionan como reservorios de agua, equilibran la temperatura ambiental, son refugio de especies locales y migratorias; el aporte socio cultural de estos ecosistemas responde a una interdependencia entre la naturaleza y el ser humano intrínseca en el desarrollo social y  para la conservación de las especies del planeta.

En la **localidad 11 de Suba en Bogotá se encuentran los humedales de Cordoba, Guaymal y la Conejera**. Este Ultimo en la actualidad se encuentra amenazado desde hace un poco más de un año debido a que se dio inicio a la **construcción de la  Reserva Fontanar, un proyecto  de vivienda multifamiliar**, esta propuesta que se quiere hacer sobre la ronda del humedal,  afecta e impacta negativamente a todo el ecosistema natural y el desarrollo socio cultural alrededor del humedal.

Debido a lo anterior un **grupo de vecinos del sector se organizan  y en la actualidad se conocen como Acción Comunitaria la Conejera, un movimiento pacífico y pacifista**, que invita a la sensatez, al que se puede incluir cualquier persona, grupo u organización  interesado en sumar fuerzas por aportar en el proceso. Desde la llegada de la constructora se levanta un campamento permanente en el humedal  en defensa de la conservación, promoción y cuidado de este santuario natural y se adelantan iniciativas como el aula ecológica y otras actividades de carácter educativo, pedagógico, artístico y cultural. Este grupo se nutre por personas y grupos que se encuentran entorno de un interés y un propósito común; el respeto por  su territorio y las propuestas que lo afectan. Pero en este proceso sean encontrado con múltiples irregularidades y delitos ambientales como: La **demolición de la hacienda Fontanar del Río,  contradicciones legales como que la curadurías**  siendo entes privados  otorgan permisos de construcción sobre espacios públicos y patrimonio natural invaluable,  indicios de la invasión de un 4% (238 m2) de la obra sobre la zona de manejo y preservación ambiental es decir construir sobre la ronda del humedal, entre otras graves denuncias.

El grupo y el espacio son sujetos de presión por parte de la fuerza pública y de la constructora que con diferentes acciones sobre el campamento han generado provocaciones a las personas que lo habitan.

En la actualidad existen medidas cautelares y una conciliación programada para el 14 de abril.

Para el día de hoy 28 de marzo está programada una movilización desde el humedal de la conejera a las 8am hasta la alcaldía local en donde se hará un plantón.

En esta edición **entrevistamos a activistas del campamento en defensa de la ¨Conejera¨**, para que nos informen de primera mano sobre todo el trabajo de resistencia y de integración ambiental con la comunidad que se está realizando in situ.

Contactos: <https://www.facebook.com/humedalconejeraaccioncomunitaria?fref=ts>

http://campamentohumedalconejera.blogspot.com
