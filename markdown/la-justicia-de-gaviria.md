Title: La justicia de Gaviria
Date: 2015-02-20 12:42
Author: CtgAdm
Category: Nicole, Opinion
Tags: ex presidente gaviria, justicia transicional en colombia
Slug: la-justicia-de-gaviria
Status: published

###### Foto: La Prensa 

Por [**[Nicole Jullian ]**](https://archivo.contagioradio.com/nicole-jullian/)

Lunes 16 de febrero, 8:30 de la mañana, y yo tenía cita con mi médico. Yo llegué puntual y el atrasado. Yo llegué en Transmilenio y el en un Porsche descapotable. Él sabe que trabajo como defensora de derechos humanos y debido a su verdadero interés por la situación que atraviesa su país él siempre busca algunos minutos y me pregunta por lo que me ocupa. Esta vez me abordó con la siguiente pregunta: ¿qué le pareció la propuesta de Gaviria que salió ayer en El Tiempo? Algo medio a la pasada había escuchado sobre lo de Gaviria, pero en realidad no sabía nada. Terminó la cita y yo salí en dirección a mi oficina con la misión de leer la propuesta de Gaviria, feliz de que un colombiano que maneja un Porsche se interese de verdad por lo que pasa en su país.

La propuesta del ex presidente Gaviria plantea la aplicación de un tipo de mecanismo de justicia llamado *justicia transicional* para todos los partícipes del conflicto. Al decir “todos” Gaviria plantea que éste mecanismo de justicia abarque también a los actores no combatientes, es decir a aquellos *“que de alguna manera fueron financiadores, auxiliadores o pactaron compromisos con grupos paramilitares o guerrilleros por beneficios electorales o por simple intimidación y con el fin de adelantar su tarea proselitista”.*

A raíz de la columna de Gaviria el programa de Radio Caracol “Hora 20” preparó para el lunes una edición especial. La directora del programa, Diana Calderón, se dirigió a La Habana y como invitados estaban Iván Márquez, Pablo Catatumbo y Joaquín Gómez. Apoyada por los panelistas Luz María Sierra, Héctor Riveros y Hassan Nassar, la meta de este programa era, en palabras de Calderón, “hacer un debate de altura, ilustrativo y con franqueza”. Después de haber escuchado la hora y media de programa la sensación que me queda es que con ese nivel de periodismo Colombia está lejos de poder construir un debate de altura. El ping-pong acusativo se centró en lo mismo de siempre: si el obstáculo para la paz es el paramilitarismo o la guerrilla. Pero nadie de los participantes de ese programa reparó a cabalidad en lo central, en el significado de justicia transicional.

La justicia transicional es un desafío sumamente delicado, pues pone valores tan nobles como la justicia y la paz en una inevitable contraposición. Este mecanismo propone buscar un equilibrio razonable, más nunca justo, entre el deber de castigar a los que cometieron graves crímenes y el deber de poner fin a años de conflicto. No se trata de que los crímenes queden en la impunidad. Se trata más bien de buscar el camino para poder dar vuelta la página, para iniciar un consenso sobre cuál es el ajuste de cuenta que iniciará una sociedad con un pasado impregnado de dolor, sangre, injusticia e impunidad.

Las víctimas del conflicto serán a fin de cuenta las dueñas de la palabra en todo esto. Pues el derecho legítimo de cada una a la justicia se verá lamentablemente restringido para poder alcanzar un propósito prioritario como lo es el de la paz. Y la pregunta aquí es cómo poder honrar de todos modos a cada una de las víctimas de este conflicto. Tengo la sensación de lo que propone Gaviria es un indulto que cubra desde Uribe hasta Simón Trinidad. Y si ha de ser el indulto el camino que elige la sociedad colombiana para alcanzar la paz, la moneda de cambio deberá ser una Comisión de la Verdad que retribuya con creces la restricción del derecho de las víctimas a la justicia.
