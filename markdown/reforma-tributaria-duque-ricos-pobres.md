Title: La reforma tributaria de Duque: Los ricos más ricos y los pobres más pobres
Date: 2018-08-17 17:13
Category: Economía, Política
Tags: Alberto Carrasquilla, economia, Iván Duque, Reforma tributaria
Slug: reforma-tributaria-duque-ricos-pobres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/duque-y-redorma-tributaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MILLACOUTURE] 

###### [17 Ago 2018] 

En el marco del tercer **Congreso Empresarial Colombiano (CEC)** organizado por la **Asociación Nacional de Empresarios (ANDI)** en Cartagena, Bolivar, Alberto Carrasquilla, Ministro de Hacienda y Crédito Público, formuló ideas sobre lo que sería la reforma tributaria del Gobierno Duque. Sin embargo, las ideas de Carrasquilla ya son ampliamente cuestionadas y calificadas como inequitativas por diferentes analistas.

**Eduardo Sarmiento, analista y profesor de economía,** señaló que su impresión sobre las reformas tributarias es que todas son improvisadas, puesto que cada una busca compensar a su predecesora; y sobre los planteamientos de Carrasquilla, el experto afirmó que **"está hechando globos",** porque obedece a una seria de reflexiones que no han sido plasmadas en propuestas específicas.

Sin embargo, para Sarmiento,  los anuncios de Carrasquilla responden a lo hecho por el ex-ministro Mauricio Cárdenas al final de la administración Santos, que representa por una parte la sustitución de los impuestos al capital y la renta por impuestos indirectos y regresivos; y de otra parte,  bajar los impuestos al capital pero aumentar el tributo al trabajo.

### **Impuestos al capital vs impuestos indirectos y regresivos ** 

En el CEC, el Ministro de economía señaló que las empresas estaban pagando muchos impuestos, que son aquellos gravámenes al capital, y por ende **deberían trasladarse estas cargas tributarias a las personas,** una de las formas de hacerlo, será vía impuestos regresivos como el IVA. (Le puede interesar:["Ni conflicto ni paz forman parte del lenguaje del Gobierno Duque"](https://archivo.contagioradio.com/ni-conflicto-ni-paz-forman-parte-del-lenguaje-del-gobierno-duque-antonio-madariaga/))

Aunque Carrasquilla no ha hablado abiertamente de ello, para Sarmiento es claro que **"lo que aquí se está cocinando es una nueva elevación del IVA extendida a los alimentos"**, y también, busca que las personas tributen más de sus ingresos para sostener la carga fiscal, que las compañías estarían dejando de aportar al Estado.

### **Aumentar el Tributo al Trabajo** 

El analista señaló que otra parte del dinero que dejaría de percibir el Estado por la reducción de impuestos al capital sería subsanada por un Tributo al trabajo. Es decir, ampliando la base tributaria (personas que tienen que declarar renta) de forma tal **que declaren aquellos que ganan más de 1.9 millones al mes y no 3.5 como estaba establecido** hasta el momento.

Para el profesor de Economía, Carrasquilla propone bajar los gravámenes a las empresas sobre la idea de que pagan muchos impuestos, hecho que es relativo, porque esta medida se ha ido ajustando a la baja. Adicionalmente, porque **los impuestos altos de las empresas son transferidos en buena parte al consumidor, **quien finalmente los asume.

Otra razón del Ministro de hacienda para reducir los impuestos a las compañías, es que aumentará el empleo y se engrosará la clase media. Sin embargo, Sarmiento señala que "eso no es cierto en las circunstancias actuales de la economía", porque el mercado nacional presenta un problema en la demanda y no en la oferta. Dicho de otra manera, "la economía crece al 2% no porque las empresas no tengan capacidad de producción" sino porque las personas no tienen capacidad de compra.

Para Sarmiento, con la reducción de los impuestos al capital generada por Cárdenas, y profundizada por Carrasquilla, lo único que logra aumentar son las ganancias netas de las empresas.

### **Los posibles efectos de esta reforma** 

Eduardo Sarmiento concluye que con el alza en los impuestos al trabajo y al IVA, se verá afectada la demanda. Entre tanto, la rentabilidad de las empresas será ahora más alta, por consiguiente, el experto calificó una posible reforma tributaria de este tipo como "altamente inequitativa", **porque grava a los más pobres en contraprestación con los más ricos.**

<iframe id="audio_27907248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27907248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
