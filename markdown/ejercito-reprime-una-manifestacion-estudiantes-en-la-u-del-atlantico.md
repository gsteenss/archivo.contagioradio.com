Title: Es la primera vez que el Ejército reprime una manifestación: Estudiante del Atlántico
Date: 2019-10-02 16:21
Author: CtgAdm
Category: Educación, Movilización
Tags: Disparos, ESMAD, Protesta estudiantil, Universidad del Atlántico
Slug: ejercito-reprime-una-manifestacion-estudiantes-en-la-u-del-atlantico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado lunes 30 de septiembre, estudiantes de la Universidad del Atlántico denunciaron el uso de armas de fuego para reprimir la jornada de protesta que se desarrollaba en apoyo a los estudiantes de Bogotá. Los jóvenes señalaron que tras la realización del plantón, hicieron presencia para reprimir la protesta integrantes del **Escuadrón Móvil Antidisturbios (ESMAD), el Grupo de Operativos Especiales de Seguridad (GOES) de la policía y del Ejército, armados con fusiles de largo alcance.**

### **"Es la primera vez que el Ejército reprime una manifestación"** 

Fabian Salcedo, estudiante de la Universidad del Atlántico, recordó que los jóvenes estaban realizando un plantón pacífico en solidaridad con los estudiantes de Bogotá que sufrieron la semana pasada violencia por parte del ESMAD y reclamando la autonomía universitaria de las Instituciones de Educación Superior.  El plantón inició sobre las 10 de la mañana, con actividades musicales y culturales, pero cerca de las 2 de la tarde "la fuerza pública inició la intervención, cuando logró dispersar los manifestantes, el Ejército apoya la acción del ESMAD haciendo tiros de fusil", referenció Salcedo.

En un comunicado emitido por la Unión Nacional de Estudiantes de Educación Superior (UNEES), los jóvenes señalaron el ingreso irregular de la fuerza pública al campus de la Universidad y denunciaron que se había presentado un herido por arma de fuego; sin embargo, Salcedo desmintió la información referente a los heridos al señalar que "no hay heridos por parte de los estudiantes". (Le puede interesar:["Marcha estudiantil del viernes termina en nuevas agresiones por parte del ESMAD"](https://archivo.contagioradio.com/marcha-estudiantil-agresiones-esmad/))

Salcedo expresó su preocupación ante la actuación de la fuerza pública, específicamente por parte del Ejército, porque no es la institución encargada de atender las eventualidades en el marco de la protesta social, como tampoco lo es el GOES. "En anteriores ocasiones ha habido excesos de la fuerza pública, pero prácticamente es la primera vez que el Ejército reprime una manifestación", aseguró el estudiante, al tiempo que manifestó que era necesario saber quién dió la orden para que esto ocurriera, "porque el Ejército dijo que pasaban por ahí de casualidad pero no es así, seguramente alguien les tuvo que dar la orden".

### **En contexto: La U. del Atlántico no es ajena a la violencia, ni a la corrupción del país** 

Al igual que en la Universidad Distrital Francisco José de Caldas, los estudiantes de la Universidad del Atlántico se manifestaron contra la dirección de la Institución, a cargo de Carlos Prasca. En reiteradas ocasiones los estudiantes han denunciado a Prasca por presuntos actos de acoso sexual contra integrantes de la comunidad universitaria; asimismo, se han mostrado contra la forma de proceder del rector frente a las protestas en la Institución, porque no se sienten respaldados en la exigencia de sus derechos. (Le puede interesar:["¿Por qué pidió un descanso no remunerado Carlos Prasca, rector de la Universidad del A.?"](https://archivo.contagioradio.com/carlos-prasca-rector/))

Por su parte, académicos presentaron el informe "Universidades Públicas Bajo S.O.Specha: Represión estatal a estudiantes, profesorado y sindicalistas en Colombia (2000 – 2019)" ante la **Comisión para el Esclarecimiento de la Verdad (CEV) y la Jurisdicción Especial para la Paz (JEP),** en el que se resumen algunos hechos de violencia ocurridos en el marco del conflicto en la Universidad del Atlántico. También allí se denunciaba que esta institución fue un bastión político, dada la cantidad de recursos que llegaba a manejar. (Le puede interesar:["Universidad pública: Otro escenario donde se vive el conflicto armado"](https://archivo.contagioradio.com/universidad-publica-conflicto-armado/))

Lejos de ser hechos del pasado, vale la pena recordar la aparición de **panfletos de las Águilas Negras** en el campus universitario, y la posterior alerta de bomba que terminó siendo detonada de forma controlada en julio de este año. Después de esta situación, los estudiantes dijeron que se presentó un incendio en el mismo edificio donde explotó la bomba. (Le puede interesar:["Universidad del A. de nuevo bajo la mira del terror"](https://archivo.contagioradio.com/universidad-del-atlantico-mira-terror/))

> La plataforma [@UneesCol](https://twitter.com/UneesCol?ref_src=twsrc%5Etfw) denuncia uso desmedido de la fuerza en la Universidad del Atlántico. Según el comité de derechos humanos de esta organización, se presentaron disparos con arma de fuego, un herido y la violación a la autonomía universitaria. [pic.twitter.com/ACR9IH1dQD](https://t.co/ACR9IH1dQD)
>
> — Contagio Radio (@Contagioradio1) [September 30, 2019](https://twitter.com/Contagioradio1/status/1178766574397865990?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
