Title: ¿Cómo interpretar los acuerdos de la Oposición en Honduras?
Date: 2015-02-25 20:05
Author: CtgAdm
Category: Opinion
Tags: honduras, Partido Anticorrupción, Unidad Social Demócrata
Slug: como-interpretar-los-acuerdos-de-la-oposicion-en-honduras
Status: published

###### Foto:[laprensa.com.ni]

#### Por **[[Gilberto Ríos Munguía](https://archivo.contagioradio.com/gilberto-rios/)]** (•) 

Muchas expectativas se han suscitado en la población hondureña a partir de los acuerdos firmados públicamente en los eventos del Aeropuerto Toncontín en diciembre y los de hoy en en las instalaciones del Colegio Médico, en el que ha comenzado a manifestarse una postura anti neoliberal cuando todas estas fuerzas políticas de oposición rechazan la clara intención de privatizar todos los servicios públicos de salud en el país.

Es aplaudible que un sector importante del actual Partido Liberal de Honduras se manifieste en la coyuntura de manera progresista. Muchos especulan de sus cálculos, pero el hecho se repite en el Partido Anticorrupción -Oficialmente la tercera fuerza política del país- y de proclamada posición de centro, como en el Partido Innovación y Unidad Social Demócrata, cuya dirección muestra mucho entendimiento y capacidad en la coyuntura.

Como es evidente, la resolución de las expresiones políticas que representan a más de 70% de la población políticamente activa -«que al menos vota una vez cada cuatro años"- demuestra la insoslayable tarea de trabajar por la permanente unidad del Pueblo entorno a sus derechos democráticos y humanos, como el principal que es el Derecho a la Vida; Honduras es el país de mayor número de asesinatos en todo el mundo, del que más de 90% quedan en la impunidad, según reconocen instituciones del mismo Estado.

La extrema violencia política y social heredada en buena medida del reciente Golpe de Estado Militar, el cinismo de los códigos de comunicación social que hablan abiertamente de prosperidad en el momento de mayor miseria nacional, la imposición permanente de nuevos impuestos a la población, la amenaza de la muerte cotidiana a la oposición, empuja a la creación inmediata de alternativas.

La situación social del país está manifestando muchos síntomas de madurez política, en el que la mayoría -política y social- comprende cómo se comporta el Poder, es decir, hay cambio de calidad en la población expectante, aunque esto solo sea un ensayo que no debería exacerbar nuestras esperanzas o llenarnos de triunfalismos.

Podemos demostrar que se puede frenar la impunidad del Partido gobernante, podemos diseñar una hoja de ruta democrática entre todas las facciones políticas que se han manifestado positivamente en esta coyuntura, pero no podremos avanzar hasta que se diseñe conjuntamente un trabajo de bases, de orientación y organización política.

El ingrediente principal, el que causa temor a la oligarquía, son las expresiones de masas. Fortaleza del Partido Libertad y Refundación que proviniendo de las experiencia de la Resistencia Popular de calle y de demostraciones importantes de nuestra mayoría, creamos el sentimiento y la convicción de que es posible derrotar al Régimen.

Por lo pronto se viene a la cabeza la necesidad de la Movilización Popular, sin embargo hay una infinidad de maneras de lograr expresiones populares, pero esas dependen justamente de la capacidad de lucha que se inculque en las bases.

(•) Miembro de la Dirección Nacional del Partido Libertad y Refundación, Libre.
