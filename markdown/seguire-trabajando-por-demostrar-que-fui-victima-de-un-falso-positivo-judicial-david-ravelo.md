Title: "Seguiré trabajando por demostrar que fui víctima de un falso positivo judicial": David Ravelo
Date: 2017-06-21 12:08
Category: DDHH, Entrevistas
Tags: David Ravelo, DDHH, Defensor derechos humanos, Derechos Humanos, JEP
Slug: seguire-trabajando-por-demostrar-que-fui-victima-de-un-falso-positivo-judicial-david-ravelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/David-ravelo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Reiniciar] 

###### [21 Jun 2017]

Luego de haber sido dejado en libertad condicional, el defensor de derechos humanos **David Ravelo, manifestó que se re integró a sus funciones como líder social**. En la cárcel la Picota fue representante de los presos en la Comisión de Derechos Humanos, poeta y cuentero. Afirma que nunca dejó de ser defensor y que ya en libertad seguirá viviendo su pasión por los demás.

Ravelo salió de prisión el día de ayer y se trasladó de inmediato para Barrancabermeja donde se reunió con su familia y sus amigos. A partir de ese momento y como él lo afirma, **“estoy ya al tanto de mis actividades porque voy a seguir luchando por la defensa de los derechos humanos”.**

Ravelo manifestó además que **va a comenzar a visitar varios sectores de Santander para cumplir con las labores que tiene pendientes**; comenzó con una rueda de prensa en Barrancabermeja en horas de la mañana. (Le puede interesar: ["Sabemos que mi papá es inocente": David Ravelo, hijo de defensor de DDHH"](https://archivo.contagioradio.com/sabemos-que-mi-papa-es-inocente-david-ravelo-hijo-de-defensor-de-ddhh/))

**La vida de Ravelo en la cárcel**

David Ravelo fue condenado a 18 años de prisión en 2010 por el homicidio de del ex candidato a la alcaldía de Barrancabermeja David Nuñez Cala en 1991. **A lo largo de 7 años ha intentado demostrar que no cometió dicho crimen** y manifiesta que con la salida de la cárcel La Picota “queda clara mi inocencia ante la opinión pública. Seguiré trabajando por demostrar que fui víctima de un falso positivo judicial”.

Según Ravelo, su tiempo en prisión significó para él “dolor y sufrimiento”. Sin embargo, afirmó que “cuando uno llega a la cárcel tiene sentimientos negativos. Uno escoge si se detiene o continua con su labor de trabajar por los demás”. En La Picota lo nombraron **representante de los derechos humanos de los internos** y encontró en la escritura la posibilidad de hacerle frente a lo que él llama, “sentimientos negativos”. (Le puede interesar: ["Defensor de DDHH David Ravelo en libertad tras siete años de prisión"](https://archivo.contagioradio.com/david-rabelo-es-dejado-en-libertad-luego-de-siete-anos-de-prision/))

Ravelo escribió varios cuentos que ya han sido publicados. Según él pudo resistir el cautiverio y las injusticias a través del arte donde **“pude dar a conocer mis vivencias, mis sensaciones y fui muy creativo a pesar del dolor y del sufrimiento”.** El defensor manifestó que hizo poemas dedicados a las estrellas y la luna porque en la cárcel “nos encerraban a las 4:30 pm y no podíamos ver el firmamento”.

**El defensor se acogerá a la JEP**

David Ravelo fue dejado en libertad en la medida que se acogió a los procesos que se llevarán a cabo en la Justicia Especial para la Paz. En repetidas ocasiones el defensor ha manifestado que se presenta en condición de inocente **“porque en mi proceso hubo múltiples irregularidades como los falsos testigos”**. Espera ser tenido en cuenta en la Ley de Amnistía que se pactó en la Habana donde el artículo 35, establece que se aplicaría la amnistía a las personas que fueron condenadas injustamente.

<iframe id="audio_19397917" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19397917_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
