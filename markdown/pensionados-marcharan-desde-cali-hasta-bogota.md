Title: Pensionados marchan desde Cali hasta Bogotá
Date: 2015-06-07 22:16
Category: Movilización, Nacional
Tags: Alianza Interinstitucional de Jubilados y Pensionados de Emcali, Alianza Nacional de Pensionados, Derechos Humanos, Emcali, Pensionados
Slug: pensionados-marcharan-desde-cali-hasta-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/29marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

###### [8 Jun 2015] 

[Desde Cali, saldrá este lunes 8 de Junio, la primera Marcha por la Unidad y Dignidad del Pensionado Colombiano, que se espera llegue a Bogotá el próximo 18 de junio para realizar la primera Cumbre Nacional de Pensionados, que tiene como propósito visibilizar la crítica situación por la que atraviesa este sector, así como la de los adultos mayores del país, ante el desamparo y olvido  por parte del Estado Colombiano.]{lang="ES"}

Organizados en la **Alianza Interinstitucional de Jubilados y Pensionados de Emcali** y la **Alianza Nacional de Pensionados**, los adultos mayores, reclaman del gobierno, que se resuelva de fondo la situación del pasivo pensional del Estado y el cumplimiento de los acuerdos firmados ante la promesa de campaña del presidente Juan Manuel Santos, quien se comprometió a rebajar los aportes de Salud del 12% al 4%, entre otras cosas.

En la actualidad cursa en el Congreso de la República, el Proyecto de Ley 183 de 2015, que precisamente pretende que la cotización mensual al régimen contributivo de salud de los pensionados sea del 4% del ingreso de la respectiva mesada pensional.

Los pensionados también reclaman el incremento anual de las mesadas con el porcentaje más favorable, entre el incremento del IPC, y el incremento del salario mínimo, la no aplicación de cuotas moderadoras a los beneficiarios, pero sobre todo que se instale una mesa de concertación con el gobierno nacional, en la que se discutan estos y otros temas que los afectan en su cotidianidad.

Serán por ahora 35 marchantes, que saldrán del  monumento a la solidaridad en Cali (Av 3 Norte con Calle 34) a las 7:30 de la mañana, con destino al municipio de Buga, donde pernoctarán la noche del lunes 8 de junio, para continuar su recorrido hasta Bogotá.

Para \#YoReporto de UTL Alirio Uribe.
