Title: Los elementos de la crisis de asesinatos de los AWA en Nariño
Date: 2017-12-20 08:00
Category: DDHH, Nacional
Tags: Estructuras Armadas, indígenas Awá, nariño, paramilitares
Slug: indigenas-awa-denuncian-asesinatos-y-hostigamientos-de-estructuras-armadas-contra-su-comunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [19 Dic 2018] 

Indígenas del Pueblo Awá están denunciando la grave crisis de seguridad que están afrontando, en el departamento de Nariño. El pasado 14 de diciembre fue **asesinada Mary Elsi Angulo, indígena perteneciente al resguardo indígena Awá de Watsalpí**, su cuerpo apareció con signos de ahorcamiento, a estos hechos se suman la presencia de grupos armados, el cobro de vacunas extorsivas y la falta de presencia de autoridades.

Mary Elsi Angulo fue encontrada en la carretera que de Junín conduce a Barbacoas, en horas de la mañana, junto a su hijo de dos años que al parecer abría presenciado el asesinato de su madre. De acuerdo con Reiver Pai, representante de la Unidad Indígena Awá (UNIPA), Mary era una mujer, que pesé a no vivir dentro del resguardo sino en una vereda muy cercana a él, **estaba al pendiente de lo que pasaba con las demás personas de su comunidad**.

La comisión de verificación de la UNIPA, estableció que uno de los posibles móviles para el asesinato de **Mary pudo haber sido el oponerse al pago de las vacunas** que se están cobrando en los territorios por parte de los grupos armados que hacen presencia en el mismo. (Le puede interesar: ["Continúan las amenazas a defensores de Derechos Humanos en el país"](https://archivo.contagioradio.com/continuan-las-amenazas-a-defensores-de-derechos-humanos-en-el-pais/))

### **El Resguardo Awá de Watsalpí, en medio de la guerra** 

Pai aseguró que en diversas ocasiones le han dado a conocer al gobierno nacional, a la Fuerza Pública y las instituciones el riesgo que enfrentan en el territorio, debido a la presencia de grupos paramilitares, grupos disidentes de la FARC y del ELN, **que se están disputando las rutas del narcotráfico y el control del territorio**.

En ese sentido Pai señaló que se han pedido medidas de protección tanto colectivas como individuales, sin embargo, hasta el momento no ha existido ningún tipo de respuesta por parte de las autoridades. **De igual forma están solicitando la constitución legal y la ampliación de algunos resguardos que hay en el territorio**.

Además, la minería también es otra de las problemáticas que ponen en riesgo la comunidad, esto según Pai, debido a que hay minería ilegal desde hace más de 9 en el territorio, y aunque los indígenas le han solicitado al gobierno que los retire, porque están en medio del resguardo, **que cuenta con medidas cautelares, las actividades cautelares continúan ejerciéndose arriesgando a los indígenas que se oponen a ellas**.

<iframe id="audio_22744222" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22744222_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
