Title: Cinep exige al Gobierno garantizar el derecho a la vida de colombianos
Date: 2016-12-01 13:05
Category: DDHH, Paz
Tags: CINEP, Derechoa la vida, marcha patriotica
Slug: cinep-exige-a-gobierno-garantizar-la-vida-33167
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/marcha-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Dic. 2016] 

A través de un comunicado el Centro de Investigación y Educación Popular / Programa por la Paz (**Cinep/PPP) instó al presidente de Colombia, Juan Manuel Santos a garantizar el derecho a la vida de todos los colombianos y colombianas** y de manera imperante de los líderes sociales quienes están siendo asesinados de manera continua en las regiones del país.

En la misiva, el Cinep pone de ejemplo el caso de la señora **Marcelina Canacué de 60 años de edad, reconocida líder integrante del movimiento político Marcha Patriótica y adscrita a la Junta de Acción Comunal de la vereda Versalles, jurisdicción del municipio de Palermo, Huila, quien fue asesinada el pasado 25 de noviembre, Día de la no violencia contra las mujeres.**

“Advertimos que este tipo de acciones repetidas buscan generar un ambiente de inestabilidad y hostilidad para la implementación de los acuerdos de paz con las FARC” dice la comunicación.

Según cifras del Cinep, tan **solo en el tercer trimestre de este año se han registrado 19 homicidios y 37 amenazas a mujeres y hombres integrantes de organizaciones y movimientos sociales** que según la organización “amedrentan la participación ciudadana”.

De igual manera, reiteraron su **preocupación por las víctimas y por las personas que trabajan en las regiones en diversos procesos sociales,** a quienes han manifestado su solidaridad y respaldo “en este momento de la historia que esperamos no deje más dolor ni muerte en la memoria de nuestro país”.

Además, el Cinep aprovecha la comunicación para **hacer un llamado de urgencia al presidente Juan Manuel Santos para que sean implementadas las herramientas y protocolos para “combatir el paramilitarismo y los grupos armados** que se han organizado tras su desmovilización” así como brindar protección a los líderes sociales y defensores de derechos humanos, y garantizar su derecho a la vida.

Y concluyen diciendo “(…) **el nuevo acuerdo firmado el 24 de noviembre de 2016, debe ser entendido como el punto de partida de un proceso largo que busca, en medio de las diferencias, contradicciones y acuerdos, la construcción de la paz sin la mediación de la violencia, para  hacer de Colombia un país más justo, sostenible y en paz”. **Le puede interesar: [“Es fantasioso” asegurar que el paramilitarismo no sigue vigente](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
