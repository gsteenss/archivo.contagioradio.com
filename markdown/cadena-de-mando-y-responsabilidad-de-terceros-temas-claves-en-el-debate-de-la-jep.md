Title: Cadena de mando y responsabilidad de terceros, claves en debate de la JEP
Date: 2017-03-07 18:18
Category: Otra Mirada, Paz
Tags: Congreso, JEP, Jurisdicción Especial de Paz
Slug: cadena-de-mando-y-responsabilidad-de-terceros-temas-claves-en-el-debate-de-la-jep
Status: published

###### Foto: Sin Olvido 

###### 7 Mar 2017 

El último debate de la  **Jurisdicción Especial del Paz en el Congreso de la República**, radica en el capítulo séptimo que fue incluido en el articulado y que pretende salvar la responsabilidad de la cadena de mando de integrantes de la Fuerza Pública, que debería ser regulado por el estatuto de Roma en su artículo 28, además busca también salvar la responsabilidad de terceros en la guerra.

Según **Jairo Estrada, vocero de la agrupación Voces de Paz**, este capítulo séptimo, no debería estar incluido en este acto legislativo sino que debería tramitarse aparte ya que no hace parte del Acuerdo de Paz de la Habana, sino que fue incluido en Diciembre, cuando el gobierno presentó la propuesta para que se juzgue a militares de manera equitativa. Estrada asegura que este punto debería tramitarse en una ley estatutaria y no ahora en medio del Fast Track. (Ver: "[Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/))

Sin embargo, las motivaciones son clara, afirma el vocero de la agrupación ciudadana, por una parte se busca que los militares de altos rangos no tengan que responder por crímenes en el marco de la guerra por la cadena de mando operacional que deben tener sobre su subalternos. Así se propone en el capítulo séptimo que se realicen cambios sustanciales que tengan en cuenta la jurisdicción sobre la cual se aplica el conocimiento de las operaciones.

Por ejemplo, en el caso de Mapiripán y con lo propuesto en el capítulo en mención, no se podría responsabilizar al comandante de las brigadas de las cuales salieron los paramilitares en Urabá, puesto que la jurisdicción recaería solamente sobre las brigadas con responsabilidad en el departamento del meta.

Por otro lado se pretende que los terceros involucrados en la guerra, que podrían ascender a 15 mil si se tienen en cuenta las confesiones de comandantes paramilitares en el marco de la Ley 975, solamente tengan que comparecer ante la JEP, de manera voluntaria y casi que en calidad de víctimas, lo cual salvaría su responsabilidad en la financiación o complicidad con la comisión de violaciones de DDHH.

Además, estas dos medidas estarían violando el principio del acuerdo en el que las víctimas son el centro y se prima su derecho a la Verdad, dado que en ese orden de cosas no se conocerían los móviles de muchos crímenes ni los responsables directos o indirectos.

Según Estrada, este es uno de los puntos cruciales del actual debate en plenaria del Senado, pero también tendrá que ser analizado por la Corte Constitucional en cuanto se incluye un capítulo completo no contenido en el acuerdo, además porque tendría que revisarse la constitucionalidad de las medidas incluidas y la complementariedad con el Estatuto de Roma, ya que la Corte Penal Internacional también ha realizado recomendaciones en ese sentido.

------------------------------------------------------------------------

###### Reciba toda la información de Contagio Radio en [[su correo]
