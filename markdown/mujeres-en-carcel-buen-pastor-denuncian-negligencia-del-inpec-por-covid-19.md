Title: Mujeres en Cárcel Buen Pastor denuncian negligencia del INPEC por Covid-19
Date: 2020-06-29 12:24
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: #Cárcel #covid19 #IvánDuque #contagio #Crisis carcelaria, carcel de mujeres, carcel el buen pastor, carceles de colombia, Covid-19, INPEC
Slug: mujeres-en-carcel-buen-pastor-denuncian-negligencia-del-inpec-por-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Mujeres-en-la-Cácel-El-Buen-Pastor-Colprensa-Referencia-Mauricio-Alvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Mauricio Alvarado {#foto-de-mauricio-alvarado .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad,** Claudia Cardona integrante de la [Corporación Humanas](https://www.humanas.org.co/alfa/index.php)y Martha Franco, integrante del colectivo Mujeres Libres, denuncian las violaciones de derechos humanos a las que están sometidas 12 mujeres en aislamiento por posible contagio de Covid 19 en el centro de reclusión El Buen Pastor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Franco, hasta el momento solo se ha confirmado el contagio de una mujer reclusa, producto del contacto con guardias del **INPEC** que dieron positivo. Sin embargo, hay otras 11 mujeres, que se encuentran con la reclusa, por sospecha de tener el virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Claudia Cardona, este hecho no solo rompe el protocolo del cerco epidemiológico que tendría que establecer tanto las directivas de la cárcel como del INPEC, sino que ha puesto en máximo riesgo a las demás reclusas a quienes no se les ha practicado la prueba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, otros hechos como tener a las mujeres en un lugar no apto para el aislamiento, vulnerado sus derechos a la salud y genera temor en el resto de reclusas, debido a que apenas las separa una reja.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/1055377928197980/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/1055377928197980/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Vea mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
