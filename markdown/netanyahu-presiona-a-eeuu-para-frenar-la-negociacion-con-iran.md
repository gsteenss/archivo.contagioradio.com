Title: Netanyahu presiona a EEUU para frenar la negociación con Irán
Date: 2015-03-03 19:34
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Netanyahu no respeta a Obama, Netanyahu presiona a EEUU a no negociar con Irán, Netanyahu reta a Obama a no negociar con Irán
Slug: netanyahu-presiona-a-eeuu-para-frenar-la-negociacion-con-iran
Status: published

###### Foto:Periodismoalternativo.com 

A dos semanas de las elecciones israelíes y sin contar con la Casa Blanca, **Netanyahu se ha dirigido al congreso de EEUU para rechazar las negociaciones con Irán sobre el programa nuclear**.

El presidente de **EEUU junto con Alemania, Gran Bretaña, Francia,  y Rusia están negociando un acuerdo para el programa nuclear iraní**. El acuerdo congela las bases del actual e impediría la construcción de una bomba nuclear.

Pero para el presidente de **Israel** dicho acuerdo permitiría en unos años crear la bomba atómica poniendo en riesgo sus propios interés. Según sus propias palabras"... pondría en peligro la estabilidad de toda la región...".

Es habitual que presidentes de otros países se dirijan ante el congreso de EEUU para presionar o realizar discursos. Pero este no ha sido el caso de **Netanyahu, ya que no ha contado con la Casa Blanca y a espaldas de Obama, ha sido invitado por un congresista republicano Boehner**.

Las reacciones no se han hecho esperar ante el anuncio de la aparición del presidente israelí ante el congreso este martes. La semana pasada la secretaria de seguridad nacional de Obama, Susan Rice afirmó que la visita era **"destructiva" para las relaciones entre ambos países.**

Netanyahu tuvo que decir "... que su intención no era faltar al respeto al presidente Barack Obama..", ya que las relaciones entre ambas administraciones llevan 6 años de desconfianzas y tensión. El presidente israelí indicó a los periodistas que, "... **es su obligación moral de advertir de los peligros de un acuerdo nuclear con el régimen de los ayatolás..."**.

Parte de la comunidad judía y 50 representantes entre senadores y congresistas demócratas no han a acudido al discurso en protesta por las formas en las que ha actuado Netanyahu.
