Title: "Violencia contra líderes sociales se concentró en 29 de los 32 departamentos"
Date: 2020-10-11 11:54
Author: AdminContagio
Category: Actualidad
Tags: CCJ, Informe, informe de derechos humanos, Lider social
Slug: violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj-3.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 7 de octubre la Comisión Colombiana de Juristas presentó el informe, *["**El riesgo de defender y liderar**](https://www.coljuristas.org/nuestro_quehacer/item.php?id=388): Pautas comunes y afectaciones diferenciales en las violaciones de los derechos humanos de las personas defensoras en Colombia";*  en el cual se abordan las violaciones a sus derechos, documentadas por 20 organizaciones.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Coljuristas/status/1314316218413785090","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Coljuristas/status/1314316218413785090

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este informe que evidencia los riesgos de las personas, organizaciones y comunidades que ejercen la labor de defender los derechos y revindicar las causas sociales en Colombia, **recopilando cientos de casos entre el 1° de agosto de 2018 y el 31 de julio de 2020.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**No es este uniforme más sobre las violaciones a los derechos humanos de personas defensora**s (….). Es la continuación de informes que le precedieron y contribuye un esfuerzo necesario para seguir documentando la situación".*

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### *"La historia no oficial del conflicto social y armado en Colombia persiste en las voces y en los silencios"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

El informe se divide en cinco partes, la primera de ellas analiza el contexto general de **la situación de personas defensoras líderes y lideresas sociales sus comunidades y organizaciones, y las violaciones a sus Derechos Humanos** en su labor de defensa de la vida, el territorio y el ambiente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este contexto dan cuenta de que condiciones de exclusión, desigualdad y vulnerabilidad de **mujeres, personas LGBT, pueblos indígenas y comunidades afrodescendientes y campesinas, son las principales denunciadas** y acciones de visibilización por parte de las personas defensoras líderes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto es evidenciado en el resumen que hacen entre el primero de enero y el 31 de julio de 2020, por medio de el cruce de información en una la base de datos de violencia sociopolítica en donde se documentan **184 casos de violaciones de los derechos humanos de líderes, lideresas y personas defensoras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**De estos 184 casos, 183 fueron asesinadas y una persona fue víctima de desaparición forzada**, a esto se suma que como elemento común se pretendió rastreas si previo al homicidio se habían identificado amenazas. ([En Tumaco el terror se apoderó de las comunidades](https://archivo.contagioradio.com/en-tumaco-el-terror-se-apodero-de-las-comunidades-indigenas/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Identificando que algunas de estas amenazas previas eran generalizadas contra determinado sector o comunidad, mientras otras se realizaban de manera selectivas, ***"con nombre y apellido, haciendo alusión a la labor de los líderes, lideresas o personas defensoras".*** [Denuncian nuevo plan para atentar contra lideresa Jani Silva en el Putumayo](https://archivo.contagioradio.com/denuncian-plan-en-contra-de-la-vida-de-la-lideresa-jani-silva-en-el-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Según la CIDH, en Colombia las amenazas son el tipo de agresión más frecuente contra las personas defensoras,** y en el contexto actual del país estarían íntimamente ligadas al proceso de paz y a la implementación del Acuerdo"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

El informe también da cuenta que los patrones mas comunes de agresiones en contra de los líderes se suma la **estigmatización, el desplazamiento forzado, la violencia sexual basada en el género y las desapariciones forzadas**, *"Este conjunto de violaciones de los derechos permite afirmar que persisten altos niveles de violencia, cuyos impactos continúan, dando cuenta de violencias acumuladas y focalizadas*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En resumen las tres primeras partes del informe identifican, describe y **analiza los elementos comunes en materia de violación de Derechos Humanos de los líderes, lideresas y defensores, cruzando información consolidada en bases de datos de, CINEP, Indepaz, Marcha Patriótica, Colombia Diversa, Sisma Mujer, ONIC, la CCJ**, entre otras organizaciones, así como el reporte de diferentes medios de comunicación y la misma comunidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **18 municipios concentran la mayor parte de los casos de violaciones contra líderes sociales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La cuarta parte del informe analiza el panorama regional y la situación que viven los líderes y defensores en departamentos como Antioquia, Cauca, Norte de Santander y Córdoba. ([No cesa la barbarie: 55 masacres en 2020](https://archivo.contagioradio.com/no-cesa-la-barbarie-55-masacres-en-2020/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":91261,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj.jpg){.wp-image-91261}  

<figcaption>
Fuente: Base de datos de homicidios a líderes, lideresas y personas de DDHH  
de la Comisión Colombiana de Juristas (2020)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El gráfico presentado por la CCJ da cuenta de los 184 casos de violaciones del derecho a la vida de las personas defensoras y de líderes sociales, las cuales se concentran 100 municipios, de estos 18 concentran la mayor parte de los casos de violaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de estos se encuentran **encabezado la lista Puerto Guzmán y Caloto, 8 casos cada uno; seguido por Corinto y El Tambo, siete cada uno**; Toribío (6); Algeciras, Barbacoas y Tarazá, (5) ; Buenos Aires, Miranda, Morales, Puerto Asís, Puerto Libertador, Santander de Quilichao y Tibú (4); Belén de los Andaquíes, Cáceres y Tumaco (3).

<!-- /wp:paragraph -->

<!-- wp:image {"id":91263,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj-2.jpg){.wp-image-91263}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Este registro construido a la luz de el trabajo de registro y acompañamiento de múltiples organizaciones, dio como resultado la consolidada de 396 violaciones de los derechos de líderes y lideresas sociales. *"Del total de estos hechos, 3**92 casos corresponden a homicidios, el 98,99 % de las víctimas; y 4 corresponden a desaparición forzada, equivalentes al 1,01 % del total de víctimas**"*. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional este recuento del periodo entre el 1° de enero y el 31 de julio de 2020, registra **23 casos en los cuales según el informe no fue posible identificar con claridad la organización de la cual eran miembros**, mientras que en 161 de estos casos sí fue posible precisar esa información.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91264,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-ccj-3.jpg){.wp-image-91264}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Develado así que **una de las organizaciones más afectadas durante el periodo de investigación fueron las Juntas de Acción Comunal (JAC), esto debido a** que *"41 de las 184 víctimas pertenecían a la JAC de algún municipio"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estas les siguen las personas pertenecientes a organizaciones indígenas, con 30 casos, luego los movimientos políticos, con 17 casos y los resguardos indígenas con 15 casos. [Cierre del mes de Agosto fue fatídico para líderes sociales en Colombia](https://archivo.contagioradio.com/cierre-del-mes-de-agosto-fue-fatidico-para-lideres-sociales-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y por último presentando un balance sobre las respuestas a los contextos diferenciales y específicos en la persistencia en material violación de los derechos de líderes y lideresas sociales seguido de de recomendaciones construidas de manera conjunta por las organizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas de estas recomendaciones corresponden a ***"dar cumplimiento de manera concertada, eficaz e integral al enfoque diferencial étnico, territorial y de género del AFP"***, haciendo un especial énfasis en las medidas y los mecanismos para la prevención de riesgos y la protección para personas defensoras, líderes y lideresas sociales con políticas adaptadas a sus contextos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=iAtCFmLft4w","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=iAtCFmLft4w

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
