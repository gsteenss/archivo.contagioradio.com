Title: Ambientalistas piden respuestas a Minambiente ante resolución para realinderar reservas forestales
Date: 2018-03-12 15:01
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Ministerio de Ambiente, Reserva Thomas Van der Hammen
Slug: ambientalistas_respuesta_minambiente_reserva_thomas_van_der_hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/reserva-thomas-van-der-hammen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Uniandes] 

###### [9 Mar 2018] 

Una cadena de trinos de Daniel Bernal, vocero de Humedales Bogotá evidencia que a pesar de las reuniones que tuvieron los ambientalistas de Bogotá, el  Ministerio de Ambiente aprobó una resolución para la recategorización, integración, y** realinderación de las reservas forestales del en el país, como la Reserva Thomas Van der Hammen**, lo que supone una nueva amenaza para lo que se espera, sea el otro pulmón de la ciudad.

"Nefasto ejemplo dá ~~@~~**MinAmbienteCo** en participación ciudadana. **Desde 22 feb aprobaron resolución para realinderar reservas como la van der Hammen y en la mesa de trabajo acordada con ellos no sabíamos nada ¿Pura "demagogia ambiental" ~~@~~LuisGMurillo?"**, es el trino de Bernal, que va acompañado de una imagen con la resolución aprobada, y con otra foto de la reunión de los ambientalistas con el ministro.

Precisamente este era uno de los temores de los defensores de la reserva, como lo había afirmado anteriormente Sabina Rodríguez, pues  "la alcaldía podía estar esperando para continuar con su proyecto de expandir el norte de Bogotá", acabando con la conexión ambiental del borde note de la capital.

Ante tal situación, Daniel Bernal, manifiesta que "las reuniones e intenciones de trabajar conjuntamente sólo fueron un saludo a la bandera. Mientras esperábamos acuerdos y trabajo, ellos lo firmaron sin siquiera tener la decencia de comunicarlo. ~~\#~~**MinistroAcabóLasReservas**".

### **La respuesta de Peñalosa** 

"Nuestra propuesta es el triunfo para los proponentes de la reserva Van Der Hammen: pasaría de estar en el papel, **sin posibilidades de hacerse, a ser una formidable realidad: 1700 hectáreas!**", respondió en su Twitter el alcalde, a lo cual las respuestas indignadas de los ambientalistas no se hicieron esperar.

"Esta afirmación demagógica es el fruto de las verdades a medias, la arrogancia, las mentiras, y sobre todo, de los grandes intereses económicos que son los únicos triunfadores de lo que sería un atropello contra el bien colectivo: la urbanización de la ~~@~~ReservaVDHammen", trinó Manuel Rodríguez Becerra, director del Fondo Nacional Ambiental.

Por su parte, María Mercedes Maldonado, le pidió a Peñalosa no ofendiera a quienes llevan años defendiendo la reserva. "**Destruir la ReservaTVDH no es el triunfo de los ambientalistas, es el triunfo de Cambio Radical y de Camacol", constructores del proyecto Lagos de Torca.**

### **Así reaccionaron otras instituciones** 

Cabe recordar que tras la reunión del gobierno con los ambientalistas, desde el Twitter del viceministro del Interior, Luis Ernesto Gómez, se había anunciado la reunión como un paso para lograr concertaciones frente al tema, junto con las autoridades ambientales, "Gracias a la voluntad de ~~@~~**LuisGMurillo** y a la ~~\#~~**CausaCiudadana** ~~\#~~**ReservaVanDerHammen** se acordó suspender expedición de la resolución para reservas hasta realizar mesas técnicas que recojan propuestas de las organizaciones ambientalistas", no obstante todo parece haberse quedado en tuits.

De hecho, el viceministro expresó que está  la resolución del Ministerio de Ambiente no puede ir en contravía de la protección ambiental. "Cuidar nuestra riqueza natural es el primer compromiso de todos", en referencia a un mensaje publicado por el ministro del posconflicto, Rafael Pardo quien manifestó que "Se requiere claridad. **El legado del presidente Santos en el tema ambiental es muy importante para que esta resolución del minambiente borre con el codo todo lo construido".**

###### Reciba toda la información de Contagio Radio en [[su correo]
