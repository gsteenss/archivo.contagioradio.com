Title: Mujeres víctimas de violencia sexual en Montes de María rompen el silencio ante la JEP
Date: 2020-05-22 15:28
Author: CtgAdm
Category: Actualidad, DDHH
Tags: JEP, montes de maría, mujeres
Slug: mujeres-victimas-de-violencia-sexual-en-montes-de-maria-rompen-el-silencio-ante-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: @HumanasColombia*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 22 de mayo 39 mujeres de los Montes de María, respaldadas por la **[Corporación Humanas Colombia](https://archivo.contagioradio.com/mujeres-detenidas-en-carcel-picalena-exigen-protocolos-contra-covid-19/)** y el Colectivo de Abogadas Helenita González, le entregaron a la Jurisdicción Especial para la Paz (JEP), un informe que denuncia 47 casos de violencia sexual cometidos en contra de 39 mujeres.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/zZNRj0TuEI0","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/zZNRj0TuEI0

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph -->

El informe, que inicialmente se iba a entregar en el mes de marzo en las oficinas de la JEP, en compañía de las mujeres víctimas de violencia sexual, fue presentado de forma virtual con un audio que recopila las voces de las protagonistas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1263841966435024896","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1263841966435024896

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A la entrega se sumaron representantes del Fondo Multidonante de las Naciones Unidas para el Sostenimiento de la Paz en Colombia, Procuraduría General de la Nación, Defensoría del Pueblo y Comisión de la Verdad, delegación encabezada por **Catalina Díaz**, **presidenta de la Sala de Reconocimiento de la** [**JEP**.](https://archivo.contagioradio.com/victimas-del-palacio-de-justicia-temen-que-jep-sea-escenario-de-impunidad-para-arias-cabrales/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La justicia para las mujeres de Montes de María no puede seguir llegando tarde"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Adriana Benjumea** Directora de la Corporación *Humanas* Colombia, reiteró que son **47 casos de violencia sexual ocurridos a 39 mujeres, *"esto quiere decir que muchas de esas mujeres sufrieron más de una victimizació*n".**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Son la mujeres víctimas y sobrevivientes de los Montes de María las que hacen posible este espacio, son ellas las que mueven el engranaje de justicia, verdad y reparación y exigen su derecho a la verdad"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

El informe que inició su construcción desde 2018, tuvo varías etapas; entre ellas un tejido de lazos de confianza, recuperación emocional con con las [mujeres](https://www.justiciaypazcolombia.com/honor-a-nuestra-colombianidad/), el conocimiento de los hechos en sus vidas y las dinámicas de la guerra en la reproducción de múltiples estereotipos, manifestó Benjumea.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este trabajo de largo aliento, fue posible **gracias a múltiples organizaciones que recopilaron las situaciones ocurridas entre 1983 y 2014**, en 15 municipios de [Montes de María](https://archivo.contagioradio.com/lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla/), y en Andrés de Sotavento, en el departamento de Córdoba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Los hechos se centran en 8 municipios de los Montes de María: 1 de Sotavento, 17 en Sucre, 16 en Ovejas , 1 en Morroa; 15 casos en San Juan de Nepuceno, 3 en San Brano, 3 en Carmen de Bolívar, 3 en Córdoba, 4 en San Jacinto y 1 en María la Baja".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la organización señaló que uno de los pasos fundamental en el proceso fue el **autoreconocimiento étnico territorial** de las víctimas, en donde 11 se identifican como mestizas, 10 como afrocolombianas, 8 como indígenas, 3 como integrantes del pueblo Zenú y las mujeres restantes no se identifican en ninguna.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Benjumea al momento de realizar las entrevistas el promedio de edad de las víctimas era de 44 años, por lo tanto la edad de las mujeres **durante los hechos era de: 7 víctimas entre los 5 y 13 años; 10 entre los 14 y 17 años; 11 entre los 18 y 25 años; 3 entre los 26 y 30; 13 entre 31 y 40 años; y 3 entre 41 y 45 años.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma aseguró que estos casos son responsabilidad de diferentes actores armados legales e ilegales, que han participado en el conflicto armado, ***"van a encontrar 37 casos cometidos por las Farc (79% del informe) , 9 por el Ejercito Nacional (19%) y 1 por un miembro de la Policía Nacional (2%)".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente agregó que uno de los picos más importante de agresiones cometidas por la Fuerza Pública se da en el periodo de 2002 a 2009, cuando se da inicio a las políticas de seguridad democrática, del entonces presidente Álvaro Uribe Vélez.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Cuando Montes de María fue militarizado por orden del Estado, fue el periodo más peligroso para las mujeres y las niñas, aumentando la inseguridad producida por miembros del Ejército".***

<!-- /wp:quote -->

<!-- wp:paragraph -->

De igual manera, señaló que el el índice más alto de incidencia de FARC, se dio entre 1998 y 2002, con algunos agravantes, *"las FARCr es responsable del 67% de los casos cometidos contra niñas y mujeres jóvenes en esta región".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los patrones de violencia el informe evidenció *" patrones para castigar a las víctimas, dominarlas, regular sus relaciones sexoafectivas, para compensar a la tropa a través de los cuerpos de las mujeres, para desplazar a las mujeres víctimas y reforzar la jerarquía del grupo"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Mientras el Ejército se dedicó a castigar las mujeres del territorio por sus supuestos vínculos con la guerrilla, la guerrilla en su estructura utilizó los cuerpos de las mujeres para dejar claro quién tenia las jerarquías** en el grupo y pasar como moneda de cambio los cuerpos y las vidas de las mujeres"*

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La justicia ahora esta en manos de la JEP

<!-- /wp:heading -->

<!-- wp:paragraph -->

Catalina Gómez Diaz, **presidenta de la Sala de Reconocimiento de la JEP**, señaló, *"les agradecemos el voto de confianza y por recordar y compartir esa vivencias tan dolorosas, y tener la fuerza para narrarlas* (...), *toda esta información para nosotros es central para cumplir nuestra función"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y reafirmó su compromiso en este proceso, *"reiteramos nuestro deber y mayor determinación para cumplir con nuestro deber, para cumplir con la razón de ser por la cual fuimos escogidos y es que se haga justicia, para que estos hechos no queden en la impunidad, y que el país pueda conocer su verdad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

*Escuche aquí el audio de la entrega del informe.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

....

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
