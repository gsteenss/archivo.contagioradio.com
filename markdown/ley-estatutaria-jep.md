Title: ¿Firmará Duque la Ley Estatutaria de la JEP?
Date: 2019-02-11 16:03
Author: AdminContagio
Category: Paz, Política
Tags: Iván Duque, JEP, Ley, Objetar
Slug: ley-estatutaria-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título1-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 Feb 2019] 

Luego de superar diferentes obstáculos en su aprobación, **la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) tendría que enfrentarse a uno nuevo: la posible negativa del presidente Duque para aprobar el documento**. La nueva traba la habría anunciado la ministra de Interior Nancy Patricia Gutiérrez, al afirmar que el Gobierno podría objetar la Ley, pero esa decisión se tomaría tras hacer el estudio del documento.

La Ley Estatutaria de la JEP es el documento en el que se [consignan los procedimientos](https://archivo.contagioradio.com/que-cambia-en-la-jep-con-la-aprobacion-de-la-ley-estatutaria/) que debe seguir esta institución en el desarrollo de sus funciones; y como lo explica el **Abogado Gustavo Gallón, director de la Comisión Colombiana de Juristas (CCJ)**, sin dicha normatividad, "es difícil que el mecanismo funcione" en tanto se producirían controversias sobre la legalidad de algunos procedimientos. Aunque la JEP tiene un reglamento interno que ha sido importante para avanzar en algunos procedimientos, el doctor Gallón no lo considera equiparable a la Ley Estatutaria.

### **El difícil proceso legislativo de la Ley Estatutaria** 

No es un secreto que todo el proceso de Implementación y legislación del Proceso de Paz ha sido bastante difícil, la Ley Estatutaria de la JEP no ha sido la excepción, pues como lo recuerda Gallón, durante el anterior Gobierno se formuló un Proyecto de Acto Legislativo que fue considerado "largo" y tuvo que ser reformulado, posteriormente, en 2017 fue aprobado en el Congreso pero por demoras del mismo organismo al enviar información incompleta a la Corte Constitucional, **esta Corte emitió su concepto de favorabilidad sobre el Proyecto hasta diciembre de 2018**.

Posteriormente, se generaron dudas sobre el proceso de aprobación, porque se supone que la Ley debía haber sido sancionada a principios de año por los presidentes de Senado y Cámara de Representantes, pero en realidad no había sido así porque Ernesto Macías, presidente del Senado aún no firmaba el documento. Después de firmarlo, varios congresistas denunciaron que se habían modificado irregularmente partes del mismo, **a lo que Macías respondió que se trataban de errores en la fotocopia**.

Y ahora que por fin el documento puede ser sancionado por el Presidente, su Ministra de Interior ha afirmado que el Proyecto está en estudio, y que podría ser objetado por el presidente, lo que significaría volver al Congreso para su revisión y aprobación. De acuerdo a Gallón, el Proyecto no podría ser objetado por inconstitucionalidad, pues ya la Corte dio su visto bueno sobre ese punto, pero **podría tratarse de objeciones a determinados artículos o la totalidad del Proyecto por considerarla "inconveniente"**.

Para el Abogado, resulta contradictorio que muchos de quienes critican a la JEP y dicen que no ha producido resultados, a pesar de los resultados de [un año en operación](https://archivo.contagioradio.com/estos-son-los-aciertos-y-desafios-de-la-jep-tras-su-primer-ano-de-funcionamiento/) que cumplió recientemente, ahora se oponen a que tenga una norma de procedimiento como está; y añadió que lo único que se puede hacer en este momento es "generar toda la dinámica posible que el Presidente apruebe la Ley", y en caso de que la objetara, esperar que solo lo hiciera sobre artículos y no de la totalidad de la misma, para poder resolver las inconveniencias que él invoque de manera rápida.

### **El Plan Nacional de Desarrollo atenta contra la paz** 

En días recientes, diferentes sectores han criticado que el Plan Nacional de Desarrollo no destine los recursos específicos para la implementación del proceso de paz, a lo que el doctor Gallón suma la introducción de un artículo en dicho plan, que prevería la creación de unas **facultades extraordinarias para que el Gobierno modifique la estructura de la administración pública, situación que sería "inconveniente y peligrosa"**.

A propósito de estas facultades, el Director de la CCJ recordó que siempre han causado temor, y por eso la Constitución de 1991 dejó en claro que no debía abusarse de esta figura, sin embargo, cuando se han otorgado facultades extraordinarias para modificar la administración pública se ha hecho "con facultades precisas en temas determinados", no de forma difusa como se está intentado ahora. (Le puede interesar: ["¿Plan de Desarrollo Nacional de Duque se olvidó de la paz?"](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/))

En ese sentido, Gallón señala que las facultades que se intentan aprobar son amplias, no tienen directriz y la preocupación se genera tanto para la administración en general del Estado, como para las instituciones creadas en el marco del proceso de paz, como las que forman el Sistema Integral de Verdad, Justicia, Reparación y No Repetición.** **(Le puede interesar:["Política de Duque, un retroceso de 20 años en derechos humanos"](https://archivo.contagioradio.com/politica-de-seguridad/))

<iframe id="audio_32447939" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32447939_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
