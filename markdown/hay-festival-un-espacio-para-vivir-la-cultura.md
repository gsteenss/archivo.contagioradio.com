Title: Hay Festival, un espacio para vivir la cultura
Date: 2016-01-28 17:12
Category: Cultura, eventos
Tags: Cultura, literatura
Slug: hay-festival-un-espacio-para-vivir-la-cultura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Cartagena-cabezote-4M.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 28 Ene 2016 

###### <iframe src="http://www.ivoox.com/player_ek_10233856_2_1.html?data=kpWflZiceZehhpywj5WWaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5ynca3VjNXO3JDYrcbixpDi0JDQucjV05DS0JDJsIy8wt6YqMrXuMrqwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Cristina Fuentes, Director Hay Festival**] 

Durante 4 días la ciudad de Cartagena abre sus puertas a una de las citas más importantes con la cultura en el país, el Hay Festival, evento que nace en Colombia en el año 2005 con el objetivo de fortalecer la cultura y generar puntos de encuentro en temas de Literatura, artes visuales, cine, música,  periodismo, medioambiente, economía entre muchos temas más.

La edición número 11 del Festival, contará con la participación reconocidos expertos como el premio Nobel de economía **Joseph Stiglitz, **escritores ****Hanif Kureishi, **Lionel Shiriver, Johann Hari, Piedad Bonett, Mario Mendoza;  directores y actores de cine, poetas y músicos** estarán presentes en las diferentes charlas que se presentarán en diferentes escenarios de la ciudad**.**

Cristina Fuentes, directora del evento, asegura que el sello de Hay Festival es el ser "ecléptico" por la cantidad de temas que se abordan en cada edición y al talante de los invitados nacionales e internacionales que asisten, destacando la presencia en esta ocasión de Sergio Jaramillo y Humberto de la Calle, interactuando con David Bojanini sobre la economía de la paz en Colombia.

La [programación del festival](https://www.hayfestival.com/cartagena/documents/2016/Hay-Festival-Cartagena-2016_Programa.pdf) se extiende a otra lugares del país como Medellín y Riohacha e igualmente  tiene un espacio dedicado a los niños con  el Hay Festivalito, lugar donde los más jóvenes podrán compartir con periodistas, escritores y artistas en diferentes talleres y charlas.

En cuanto a la música, uno de los homenajes en esta edición, será el que Fruko, Chelito de Castro y Checo Acosta rendirán al cantautor colombiano[Joe Arroyo](https://archivo.contagioradio.com/el-legado-inmortal-del-joe-para-la-salsa/) , además como invitados especiales se encuentran el cantante Fonseca y el portorriqueño Robi Draco Rosa.

Días de cultura vivirá la ciudad amurallada y durante este tiempo Contagio Radio estará informando sobre las diferentes actividades que se vivirán en la edición número 11 del festival.
