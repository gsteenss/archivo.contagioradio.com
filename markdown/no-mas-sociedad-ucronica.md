Title: No más sociedad ucrónica
Date: 2017-03-29 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: historia, politica
Slug: no-mas-sociedad-ucronica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/isuniverse-tumblr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: isuniverse-tumblr 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 29 Mar 2017 

[Hace tiempo, Marcuse aseguraba que la historia es el reino de la posibilidad en el reino de la necesidad… hoy los que ejercemos el papel de educadores, nos damos cuenta de algo innegable: la historia, como un cúmulo de acontecimientos objetivos o inter subjetivos, directa o indirectamente relacionados, parece que no tiene mucho que ver con lo que amplias capas de la sociedad reconocen como “historia”.]

[Yo no sé si atribuir a la “responsabilidad individual” el aprendizaje mismo de la historia, puesto que en Colombia los más “responsables” en ocasiones son los más peligrosos; prefiero por eso mencionar que saber de historia, es algo que está lejos de ser una característica intelectual, yo creo, con algo de certeza, que es más bien una NBI (necesidad básica insatisfecha).]

[Bueno, y ¿para qué saber historia? Es sencillo: para comprender (como mínimo) que la causa y la solución de los problemas sociales del país, no residen en una única razón o mucho menos en una sola persona, sirve para comprender que cuando un ciudadano cualquiera siente rabia por algo que hace el Estado o un grupo privado empresarial que roba al Estado, ese “madrazo” natural, lícito y sincero, podría emerger coherentemente más allá de la rabia efímera y durar tanto, como para accionar políticamente.  ]

[Hago la aclaración, porque hoy en Colombia algunos despistados muy emprendedores con su típico discurso individualucho, creen que el problema de la pobreza son los pobres, o que el problema de Colombia es que no se parece a]*[X o Y]*[ país, son los mismos que hablan de lo que Colombia debería hacer o ser, sin conocer más allá que los pueblitos aburridos por los que pasan de camino a la finca; los mismos que creen que la solución a los problemas sociales es exclusivamente un tema de actitud ganadora, o son esos mismos que ponen ejemplos incoherentes al mejor estilo de Juan Carlos Echeverry y los extraterrestres de google cuando su posición “muy emprendedora y ganadora” es desnudada por vacía y absurda… en fin, como cerraba aquel antiguo programa ¡¡Dejémonos de vainas!! Saber de historia incomoda a los poderosos, que la gente ignore la historia le conveniente a los poderosos; eliminar la historia como materia es un penoso intento fallido de los poderosos, por tanto, la solución para esos poderosos ha sido configurar poco a poco, una sociedad ucrónica.  ]

[La sociedad ucrónica es esa sociedad que sabe más de Pablo Escobar que de Antonio Nariño, sencillamente porque “El capo 1, 2, 3” “sin tetas no hay paraíso”,  “Narcos” “El Patrón” y toda esa manada de monumentos al traquetismo tuvieron mayor eco que una clase de historia, por obra y gracia de los medios masivos de comunicación, eso sí, curándose en salud de no atentar contra la “moral social colombiana” cuando luego de pasarnos su apología a los paracos o a los narcos, nos dejaban “El defensor del televidente” para quejarnos, o a un guionista estúpido explicando que siempre sacaban novelas sobre traquetos para “enseñar a la sociedad a tomar conciencia sobre esos malos personajes de la historia”… gran mentira…  lo peor es que al principio, muchos incautos creyeron, luego, podemos decir que definitivamente hoy, muchísimas capas de la sociedad estamos mamados del cine, la telenovela, la serie y en general alguna cosa que ensalce a narcos, paracos y todos sus combos…]

[La sociedad ucrónica es esa sociedad que enseñó poco a poco en el marco de las dinámicas mercantilistas que en vez de reflexionar sobre la historia, en vez de tratar de unificar un relato a partir de las inter subjetividades, resultaba “más sencillo” que Caracol o RCN contaran su versión alternativa sobre la historia… total, a los profes de barrio a las profes de pueblo, les habían bajado tanto el sueldo, los habían debilitado tanto como fuerza social, los jodieron a tal punto de fastidiarlos con su profesión, que al final les daba lo mismo si los estudiantes aprendían o no... ¿cómo juzgarlos?  ]

[La sociedad ucrónica es esa sociedad que está inmersa en el penoso y fracasado intento de fomentar líderes a partir de ideologías liberales, neoliberales o neoreencauchadas como los yuppies del]*[movimiento libertario]*[ que siguen dando lata, ¡¡perdón!! o sea “cátedra” en los mejores escenarios académicos, es decir en los que se halla esa clase media-alta que cree que el mundo comenzó y termina con ella. Es el mismo combito que ignorando la historia grita “¡¡libre mercado o muerte!!” son más libertaros que Bakunin o Kropotkin juntos, pero se olvidan que las condiciones para que el libre mercado se desarrolle, no es solo que no exista el Estado, sino que correlativamente no existan capitalistas, y eso si OOOO SEEAA GAS, de eso mejor ni hablemos.]

[La sociedad ucrónica es esa que grita “viva la democracia” … pero la griega… es decir no la de esos socialistas que se quieren salir de Unión Europea, sino la de la Grecia Clásica… esa bella utopía sostenida por el esclavismo como modelo fundamental de la economía, porque, sin ofender o si acaso insinuar demérito en la filosofía griega, pero definitivamente uno tiene que tener a alguien que le lave los pies, que le sirva y le ponga las uvas en la boca para escribir semejantes maravillas.  ]

[La sociedad ucrónica es esa que aún tiene gente que enseña a votar creyendo que la solución se puede lograr aplicando de manera adecuada un manual que lleva 200 años de fracaso…]

[La sociedad ucrónica es esa que produce neoliberales que aman el legado de Jaime Garzón.]

[La sociedad ucrónica es esa que elogia a los políticos de derecha que quieren convencer al país de que el problema es la corrupción y no el sistema económico y político que la promueve. Por eso algunos bien aviones ya hacen campaña con ese slogan, para quitarse el rótulo de derecha y ponerse una túnica de “buenos ciudadanos” los mismos que se jactan diciendo, qué conviene y qué no conviene “al país”, es decir, a su país, ese país donde no conviene que exista por ejemplo la inconveniente izquierda.]

[La sociedad ucrónica es un problema serio. Muchos están sacando provecho de ello, desde los grandes canales que ofrecen entretenimiento, pasando por esos políticos de derecha que se hacen pasar como neutrales, hasta los neoreencauchados… todos al final, adornados con la misma característica: estar muy alineaditos con el poder establecido en Colombia.]

[Al igual que muchas personas en el campo y en la ciudad, me niego a creerles sencillamente porque quieren dejar de lado la historia, ¡y es la historia la posibilidad en el reino de la necesidad!, por tanto, no favoreceré el absurdo.]

[Por eso, ante una sociedad ucrónica, mejor una sociedad que conoce su historia. Poco a poco, paso a paso, escribiendo, hablando, estudiando, deliberando, investigando, mirándolos siempre muy de cerca podremos recordarles a los ucrónicos que el mundo no comenzó con ellos, solo allí, podremos establecer una sociedad que conoce su historia, sin duda: una sana defensa contra los poderosos. ]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
