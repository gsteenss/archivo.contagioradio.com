Title: La Cadena Perpetua desde la óptica constitucional
Date: 2020-06-24 11:19
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Asesinos de niños, Cadena perpetua, Congreso de la República, Corte Constitucional, Violadores de niños
Slug: la-cadena-perpetua-desde-la-optica-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/ALFREDO-BELTRÁN-SIERRA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Alfredo Beltrán Expresidente de la Corte Constitucional / Cumbre por la Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes habló para los micrófonos de Contagio Radio, el doctor en Derecho, Alfredo Beltrán, expresidente de la Corte Constitucional, a propósito de **la aprobación en el Congreso de la República de la cadena perpetua para violadores y asesinos de niños.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exmagistrado de la Corte Constitucional señala que la cadena perpetua está expresamente prohibida en el artículo 34 de la Constitución Política por antecedentes históricos que datan de más de 100 años, cuando en 1910 se introdujo una reforma a la Constitución de 1886 para prohibir dicha pena por considerarla una pena «infamante, cruel y de trato inhumano». Manifestó también, que por ese motivo, **el Constituyente del año 1991 decidió establecer la prohibición expresa de dicha pena.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El jurista Beltrán también declaró que aunque la violación a menores es uno de los delitos más graves tipificados en el Código Penal, aun así, había que partir de la base de que la pena tiene que tener atada a la función de protección de la ciudadanía, una función de resocialización del victimario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El expresidente de la Corte explicó que **no puede caerse en una legislación penal que opere como «la antigua Ley del Talión: “Ojo por ojo, diente por diente”»**. Y agregó que la cadena perpetua es una pena que contraria los DD.HH. pues no le permite al infractor reincorporarse a la sociedad y lo somete a perpetuidad al encierro. (Le puede interesar: [¿La justicia restaurativa puede contrarrestar la violencia de género?](https://archivo.contagioradio.com/justicia-restaurativa-puede-contrarrestar-violencia-genero/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, expresó que esta decisión del Legislador no cumple la función preventiva, es decir, la de no comisión del delito, pues el potencial delincuente no se va a ver disuadido de incurrir en la conducta por la mera tipificación de la prisión perpetua como castigo; **«está demostrado que donde opera la cadena perpetua e incluso la pena de muerte, los delitos no dejan de cometerse»**, señaló Beltrán. (Lea también: [La Cadena perpetua no reduce la criminalidad ni repara los daños a las víctimas](https://archivo.contagioradio.com/la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teniendo en cuenta lo anterior, añadió que como termina operando entonces, la cadena perpetua, es más «con una función de venganza que cualquier otra cosa», lo que contraría el principio fundamental de dignidad humana establecido en la Constitución Política.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### Caminos sinuosos que se pueden recorrer con la aprobación de la cadena perpetua {#caminos-sinuosos-que-se-pueden-recorrer-con-la-aprobación-de-la-cadena-perpetua .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El constitucionalista sostuvo que con este proceder, el Congreso le abre la puerta a permitir que una vez aprobada la cadena perpetua, a futuro, esta pena se pueda emplear como castigo para otro tipo de conductas. También, que **por ese camino «podríamos regresar por ejemplo a que desaparezca la prohibición de la tortura, lo que conllevaría a recaer en métodos ya superados por la humanidad».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Ese no es el camino. Ese es el camino que no quiso que se recorriera la Constitución con la prohibición expresa de estas penas»
>
> <cite>Alfredo Beltrán, expresidente de la Corte Constitucional</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Beltrán, concluyó diciendo que **el Acto Legislativo expedido por el Congreso correría la suerte de ser declarado inexequible en el control constitucional de la Corte pues según él «no sobreviviría un juicio de sustitución»**, ya que, modifica preceptos esenciales del ordenamiento jurídico nacional, lo que llevaría a incurrir en el riesgo de «retribuir con recursos de la Nación a los verdugos que se vieran condenados con esta pena» una vez que se declarara inexequible.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https:\/\/www.facebook.com\/contagioradio\/videos\/773458143394748","type":"video","providerNameSlug":"facebook","align":"center","className":""} -->

<figure class="wp-block-embed-facebook aligncenter wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/773458143394748

</div>

<figcaption>
Otra Mirada: Cadena perpetua en Colombia

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
