Title: Contra la corrupción, hay que pasar del discurso a los resultados
Date: 2017-03-15 15:02
Category: Economía, Nacional
Tags: corrupción, Odebrecht, Presidente Juan Manuel Santos
Slug: contra-la-corrupcion-hay-que-pasar-del-discurso-a-los-resultados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/corrupcion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Somos] 

###### [15 Mar 2017] 

Condenar a los responsables de sobornos y financiación de campañas políticas, recuperar el dinero usado en dichos actos de corrupción e impedir que los responsables sigan ejerciendo cargos públicos, son algunas de los retos que plantea Andrés Hernández director de Transparencia por Colombia, **frente a los recientes hallazgos de la financiación de la campaña de Juan Manuel Santos 2010-2014 por parte de Odebrecht.**

Los nexos de empresarios y personajes de la política con la brasilera Odebrecht, continúan saliendo a la luz, luego de conocerse las declaraciones de Roberto Prieto, quien para 2010 era el coordinador operativo de la campaña presidencial, **el magistrado Felipe García anunció que la competencia del Consejo Nacional Electoral es verificar dicha información y sancionar**, sin embargo, advirtió que en derecho penal, después de 3 años vencen los términos.

Por otra parte, el magistrado Armando Novoa, dijo que **“el artículo 21 de la Ley de Garantías le da una competencia al CNE para que en cualquier momento** haga una revisión de los informes de ingresos de las campañas”. ([Le puede interesar: La corrupción, un obstáculo para la paz en Colombia](https://archivo.contagioradio.com/la-corrupcion-un-obstaculo-para-la-paz-en-colombia/))

Andrés Hernández, señaló que se hace urgente frente a este caso y los demás de corrupción, **“hacer un salto del discurso a los resultados”**, explicó que si bien ha habido avances en las investigaciones “tienen que haber resultados más contundentes desde el derecho penal y el control del Consejo Nacional Electoral”.

Hernández manifiesta que los órganos de control “tienen las herramientas para generar sanciones efectivas”, pero que muchas veces los conflictos de interés políticos y económicos, **“entorpecen los resultados finales de las investigaciones”. Puntualizó que las medidas que se tomen “deben ser lo más ágil y abiertas posible”,** sin que se entorpezca el debido proceso legal “pero sin que toque esperar por años el dictamen”.

Resaltó que la sociedad colombiana tiene que ser cada vez más exigente, ser veedora del cumplimiento de la Ley 1757 y recibir estas “alertas para las elecciones que vienen”. Mencionó que se debe buscar que **con el voto y estrategias democráticas se “exija de manera constante que las instituciones cumplan sus tareas”.**

Por último, aseguró que los medios de comunicación “juegan un papel fundamental como mecanismos de control”, afirmó que **“son claves para hacer seguimiento a las investigaciones que se han abierto”**, y promover en la ciudadanía el apoyo a estas labores de seguimiento, “para que los escándalos no se queden en titulares y se puedan lograr niveles de exigencia aún más altos”.

<iframe id="audio_17569443" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17569443_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
