Title: Delegación de paz de las FARC celebra inicio de fase pública con ELN
Date: 2016-03-30 14:32
Category: Nacional, Paz
Tags: Caracas, ELN, FARC, La Habana, proceso de paz
Slug: delegacion-de-paz-de-las-farc-celebra-inicio-de-fase-publica-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Ivan-Marquez-e1459366203225.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Primicidiario 

###### [30 Mar 2016] 

La Delegación de Paz de las FARC - EP, emitió un comunicado desde La Habana, saludando el anuncio que se realizó este miércoles desde Caracas, Venezuela, donde se dio [[inicio a la fase pública de las conversaciones de paz](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)] entre la guerrilla del ELN y el gobierno nacional. Allí se reitera, que ambas guerrillas continúan luchando en la búsqueda de la paz de Colombia **"en términos de dos mesas un solo proceso",** cómo lo sostuvo en el comunicado Iván Márquez, Jefe de la delegación guerrillera.

### Aquí el Comunicado 

La Delegación de Paz de las FARC - EP celebra el inicio de la etapa pública de diálogos, entre el Ejército de Liberación Nacional, ELN y el gobierno de Colombia.

Se consolidan así los cambios hacia la construcción de una paz verdadera y completa. Aspiración histórica de nuestra guerrilla y de la organización hermana del ELN, como se consignó en la declaración conjunta de nuestros comandantes, Timoleón Jiménez y Nicolás Rodríguez Bautista, al concebir un proceso de diálogos y negociación con el Estado colombiano y en términos de dos mesas un solo proceso, dando continuidad a los esfuerzos de trasformaciones políticas y sociales, en búsqueda de la reconciliación de los colombianos por la que entregaron sus vidas nuestros comandantes históricos, Manuel Marulanda Vélez, Manuel Pérez Martínez y miles de guerrilleros y guerrilleras.

Estamos seguros que la agenda pactada entre el gobierno y el ELN contribuirá, a avanzar en la materialización de las aspiraciones del pueblo colombiano por una sociedad sustentada en la democracia verdadera y la justicia social, atendiendo su trayectoria y reconocida especificada como organización revolucionaria alzada en armas.

El inicio de la etapa pública de los diálogos con el ELN, evidencia el anacronismo de aquellas fuerzas políticas y económicas en decadencia que todavía persisten en la idea de la prolongación indebida de la guerra.

La perspectiva de dos mesas, un solo proceso, es un hecho histórico, no solo por lo que ellos representan para la terminación de la expresión armada del conflicto, sino por las condiciones que contribuye abrir para avanzar hacia la verdadera democratización política, económica, social y cultural del país, superando la desigualdad, la miseria, y la exclusión política.

La fase pública expresa, además, el compromiso de las naciones hermanas de nuestra América, en especial de Cuba, Venezuela, Ecuador, Chile y Brasil, así como del gobierno de Noruega con la paz del continente. Nuestra América ha de ser zona de paz.

*Delegación de paz de las FARC*

<iframe src="http://co.ivoox.com/es/player_ej_10984518_2_1.html?data=kpWmmpmZdZmhhpywj5WWaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JCzo7encabEjNjcxNfJb8fV1MqY0oqnd4a2osfZy8jFb8XZzZDd1NTHqdTjjMjc0JDJsIy5rbOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
