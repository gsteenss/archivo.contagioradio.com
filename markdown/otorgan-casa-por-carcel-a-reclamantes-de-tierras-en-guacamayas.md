Title: Otorgan casa por cárcel a reclamantes de tierras en Guacamayas
Date: 2020-02-07 17:50
Author: CtgAdm
Category: Comunidad, Judicial
Tags: falso positivo judicial, reclamantes de tierras
Slug: otorgan-casa-por-carcel-a-reclamantes-de-tierras-en-guacamayas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Guacamayas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Guacamayas IPC

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/ruth-esther-david-sobre-casa-carcel-a_md_47539816_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Ruth Ester David | Familiar de reclamante de tierras

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Después de dos meses de detención, este jueves 6 de febrero, el Juzgado Promiscuo de Apartadó, Antioquia concedió la casa por cárcel a los 9 reclamantes de tierras de la vereda Guacamayas capturados el 27 y 28 de noviembre de 2019 en medio de lo que sería un falso positivo judicial orquestado por empresarios y administradores de las fincas restituidas, en respuesta a la sentencia que favoreció a los campesinos, dueños legítimos de **las cerca de 1.542 hectáreas** en disputa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ruth Esther David, hija de [Tito David](https://twitter.com/IPCoficial/status/1225782204514668547) uno de los reclamantes de tierras que fue privado de su libertad, y quien fue acusado de desplazamiento forzado y concierto para delinquir afirma que esta noticia ha resultado gratificante para las familias de los líderes sociales, pese a que no se ha dado finalizado el caso en el que se les declare su inocencia, resulta un avance su regreso a casa, pues muchos de ellos son de avanzada edad y presentan diversos problemas de salud. [(Lea también: Denuncian captura de ocho líderes reclamantes de tierras en Guacamayas en el Urabá)](https://archivo.contagioradio.com/denuncian-captura-de-ocho-lideres-reclamantes-de-tierras-en-guacamayas-en-el-uraba/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Han sido bastantes quebrantados de salud, varios han tenido que acudir a las enfermerías del INPEC por problemas respiratorios y de próstata", relata la hija del líder sobre los últimos tres meses que han transcurrido. [(Lea también: Corte Constitucional ordena restitución de tierras a 27 familias en Guacamayas)](https://archivo.contagioradio.com/corte-constitucional-ordena-restitucion-de-tierras-a-27-campesinos-en-guacamayas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto a su retorno, aún se espera que el traslado se realice junto a la Unidad Nacional de Protección, pues aunque les haya sido otorgado esta condición de casa por cárcel, la situación en la vereda Guacamayas es de precaución; como relatan otros habitantes de la zona, los líderes han sido anteriormente declarados objetivo militar en medio de un fortalecimiento del paramilitarismo en la zona, lo que ha llevado a que muchos de los pobladores se sientan prisioneros en sus propias tierras. [(Le puede interesar: En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras)](https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Sabemos que no podemos regresar a los lugares con nuestras familias así no más sabiendo que hemos sido atropellados, pero si esperamos que la UNP y las entidades nos acompañen, es lo que estamos pidiendo.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Ruth agrega que a lo largo de este proceso de despojo, restitución y lucha por sus tierras durante cerca de 20 años, nuevamente **"da nostalgia y miedo sufrir ese desplazamiento para volver a trabajar"**, sin embargo señala que "no es justo que dejen lo que han construido en manos de personas que quieren obtener las cosas de manera fácil" por lo que van a a seguir defendiendo sus derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la audiencia que definiría si los nueve reclamantes de tierras podrían acceder al beneficio de arresto domiciliario estaba programada para el 21 de febrero, el proceso fue adelantado por los abogados defensores quienes continúan trabajando para esclarecer la situación de los **legítimos dueños de los predios** quienes se espera, puedan regresar a sus viviendas con garantías de seguridad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
