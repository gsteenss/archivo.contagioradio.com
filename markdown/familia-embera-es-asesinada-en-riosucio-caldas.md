Title: Familia embera es asesinada en Ríosucio, Caldas
Date: 2018-11-26 15:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asesinato de indígenas, lideres sociales
Slug: familia-embera-es-asesinada-en-riosucio-caldas
Status: published

###### [Foto: Contagio Radio] 

###### [26 Nov 2018] 

Las Autoridades del Consejo Regional Indígena de Caldas (CRIDEC) denunciaron el asesinato del líder comunitario Serafín Díaz, su esposa María Gabriela Tapasco y su hijo César Augusto Díaz, ocurrido el pasado 23 de noviembre en el **Centro Poblado del Resguardo de San Lorenzo** en Riosucio, población que ha sido constantemente blanco del conflicto armado.

Según la denuncia, los hechos sucedieron cerca de las 10:00 p.m. cuando cuatro encapuchados armados irrumpieron en la casa de la familia embera Tapasco Díaz y dispararon a **Serafín,** líder comunitario, músico y agricultor de 63 años , su esposa de 52 años y a su hijo de 32 años y coordinador académico de la Institución Educativa San Lorenzo.

Al interior del hogar también se encontraba la hija de Serafín, quien logró defender su vida y escapar; según **Higinio Obispo, secretario general de la ONIC,** la sobreviviente "ya se encuentra bajo la protección del cabildo y que fue gracias a ella que las autoridades del resguardo indígena se enteraron de lo ocurrido".

Adicionalmente, Obispo indicó que desde el **CRIDEC** se está exigiendo de inmediato que el Gobierno **"disponga de equipos que lleven a cabo la investigación y dar con el paradero de los autores materiales e intelectuales"** y así esclarecer la verdad de lo acontecido.[(Le puede interesar: Denuncian masacre de una familia indígena en Mesetas, Meta)](https://archivo.contagioradio.com/asesinan_familia_indigena_mesetas_meta_farc/)

También advirtió que aún no hay un pronunciamiento oficial, pero que desde los medios se buscó desviar la atención del tema al decir que se trató de un situación de robo, "nos parece vergonzoso que antes de llegar a una instancia de investigación real se hagan ese tipo de anuncios desde las autoridades del departamento o desde los medios de comunicación, es una situación condenable" apuntó.

Por su parte, las autoridades anunciaron la creación de un equipo de trabajo entre Fiscalía, Policía Judicial e Inteligencia, Gaula, CTI y un fiscal que asumirá la investigación para determinar las causas del homicidio y encontrar a los responsables. [(Le puede interesar: Desde firma del acuerdo de paz reportan 40 indígenas asesinados: OPIAC)](https://archivo.contagioradio.com/indigenas-asesinados-acuerdo-paz/)

Finalmente, Obispo señaló que estos hechos de violencia se suman a los ocurridos en marzo 2015,  cuando también fueron asesinados  Gustavo y Edwin Bañol, padre y hermano de Norman Bañol, actual consejero mayor del CRIDEC, "**a los medios se les olvidan esos hechos, es una situación de violencia sistemática  en la región y en los resguardos indígenas pertenecientes al departamento de Caldas"** concluye el secretario.

<iframe id="audio_30334533" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30334533_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
