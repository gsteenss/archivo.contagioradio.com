Title: Mauricio Macri pretende “amordazar” la ley de medios
Date: 2015-12-16 13:23
Category: DDHH, El mundo
Tags: Argentina, Ley de Medios, Mauricio Macri
Slug: ley-de-medios-amordazada-en-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/ley-de-medios-argentina-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hispantv 

<iframe src="http://www.ivoox.com/player_ek_9746598_2_1.html?data=mpyhmJqdfI6ZmKiak5aJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLp087Qy9SPkcLX086Y0tfJuMbixcqYh6qWaZmkhp6ww9LTtsXV28bfh6qWaZmkhp6xjdHFb83Z2pDRx5DRqcXd0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luciana Lavila] 

###### [16 Dic 2015]

Miles de personas se siguen manifestando en contra del decreto del presidente Mauricio Macri, en que se delegan las funciones de la Autoridad Federal de Tecnologías de la Información y Comunicaciones a un nuevo Ministerio de Comunicaciones. Según los manifestantes, **este ministerio “amordazaría” la ley de Medios por la cual velaba la AFTIC**.

Luciana Lavila, integrante del colectivo **Barricada TV, afirma que con este ministerio y las declaraciones de Oscar Aguad, cabeza de la nueva cartera, en las que resalta que “la ley no va a subsistir”**, se pondrían en peligro 10 años de avances y muchos pasos dados desde la promulgación de la “ley de medios” en 2012, en la que se establece la obligatoriedad del uso del 33% del espectro radiofónico para medios de comunicación sin fines de lucro.

Para ejemplificar la difícil situación de la Ley de Medio, Lavila relata que Barricada TV lleva 6 años en funcionamiento, desde la primera fábrica recuperada de Argentina, pero a partir de la promulgación de la ley han podido avanzar en el objetivo de compartir la información con mucha más gente ya que cuentan **con canales públicos a través de la Televisión Digital Abierta, lo cual sería imposible sin la actual legislación.**

Sin embargo, a lo largo de esta semana siguen las movilizaciones y para el próximo jueves está citado un plantón en frente a las instalaciones del Congreso Nacional, organismo que no ha sido consultado para la emisión de los decretos del presidente Macri. Lea también [Las cuentas pendientes de Maurico Macri ante la justicia de Argentina.](https://archivo.contagioradio.com/las-cuentas-pedientes-de-mauricio-macri-ante-la-justicia-argentina/)

Video de Barricada TV

\[embed\]https://www.youtube.com/watch?v=OnHel5lLNDA\[/embed\]
