Title: Víctimas piden al Papa Francisco que interceda para que se cumpla lo pactado y sean reparadas
Date: 2017-09-08 15:44
Category: DDHH, Nacional
Tags: víctimas del conflicto armado, Visita Papa Francisco
Slug: victimas-piden-al-papa-francisco-que-interceda-para-que-cesen-asesinatos-a-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_171.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Sept 2017] 

La visita del Papa Francisco a Colombia también se ha convertido en un escenario para que las víctimas exijan el cumplimiento de los Acuerdos de Paz, y junto con ello sus derechos a la verdad, justicia y reparación, en ese sentido, el sumo pontífice se reunirá con más de 6 mil víctimas del conflicto armado para **conversar con ellas de lo que ha sido el proceso de reconciliación.**

Las víctimas reunidas en la Red de Comunidades Contruyendo Paz en los Territorios (CONPAZ), le piden al Papa, en una carta, que interceda y de su bendición a los Acuerdos de Paz de la Habana, su ánimo para que "por fin algún gobierno cumpla lo pactado" y para que avancen los diálogos de paz entre el ELN y el gobierno Nacional.

De acuerdo con Luz Marina Cuchumbe, vocera de CONPAZ, lo que las víctimas le pedirán al Papa es que sea el intermediario ante el presidente Santos y todos los ministros, para que no sigan asesinando a líderes campesinos, indígenas y afros, **en los territorios, además le pedirán que eleve sus oraciones por las víctimas del país, para que continúen con fuerzas luchando en la construcción de un país diferente**. (Le puede interesar:["El Papa nos está devolviendo a lo central del Evangelio"](https://archivo.contagioradio.com/46343/))

Además, las víctimas lograron llevarle al Santo Padre, cartas escritas desde sus comunidades en donde le relatan cómo ha sido vivir el conflicto armado en Colombia y continuar en la defensa de sus derechos a partir del perdón, “**quiero decirle que yo como víctima ya perdone a los victimarios que asesinaron a mi hija**, solo le pido al gobierno que me dé la oportunidad de mirar a los 30 militares que estuvieron en el asesinato de mi hija y que los puedo mirar con perdón” afirmó Luz Marina.

### **Carta de un niño** 

Una de las cartas que se le entregará al Papa, es la de Jhon Jairo Tinja Cuchumbe que le pide al máximo jerarca de la Iglesia Católica, **que tenga presente en sus oraciones la salud de su familia y apoyo para que logre pasar el año escolar**. (Le puede interesar: ["COALICO insta a ELN y Gobierno a incluir protección de menores en agenda de diálogo"](https://archivo.contagioradio.com/coalico-insta-a-eln-y-gobierno-a-incluir-proteccion-de-menores-de-edad-en-agenda-de-dialogo/))

“Padre es para pedirle oración, porque en el colegio me ha ido un poco mal en estos días, mi madre muy preocupada quiere que ore a Dios para que me de sabiduría, espero que usted con sus oraciones me de esperanza para que esto cambie algún día” este es uno de los apartados de la carta de **Jhon y agrega que han hecho muchas acciones para que la paz retorne a su territorio**.

Luego de este encuentro el Papa Francisco participará en la caravana de “Millones de pasos por la paz” que lo estará acompañando hasta su llegada al barrio **2.000 en Villavicencio para posteriormente retornar a la capital**. (Le puede interesar: ["Víctimas se entierran para exigir búsqueda de los desaparecidos"](https://archivo.contagioradio.com/victimas-se-enterraran-en-bogota-para-exigir-la-busqueda-de-los-desaparecidos/))

[Comunicado Papa Francisco](https://www.scribd.com/document/358387894/Comunicado-Papa-Francisco#from_embed "View Comunicado Papa Francisco on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_76555" class="scribd_iframe_embed" title="Comunicado Papa Francisco" src="https://www.scribd.com/embeds/358387894/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-g1VSnoabNCPZQqCLf8PF&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<iframe id="audio_20771653" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20771653_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
