Title: Ni descuido ni manzanas podridas, el informe sobre 156 casos de ejecuciones extra judiciales
Date: 2019-05-27 18:51
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Ejecuciones Extra Judiciales, falsos positivos, Informe, JEP
Slug: ni-descuido-ni-manzanas-podridas-informe-ejecuciones-extra-judiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Ejecuciones-Extra-Judiciales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PBIColombia] 

Este lunes fue presentado en Medellín el informe **"Ni descuido ni manzanas podridas", con el que diferentes organizaciones defensoras de derechos humanos presentarán 156 casos de ejecuciones** extrajudiciales ante la Comisión para el Esclarecimiento de la Verdad (CEV), la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) y la Jurisdicción Especial para la Paz (JEP).

**Byron Góngora, abogado de la Corporación Jurídica Libertad**, explicó que en la elaboración del informe participaron además de su organización la **Corporación Jurídica Humanidad Vigente, la Corporación Yira Castro**; en el que lograron registrar 156 casos de ejecuciones extrajudiciales cometidas por tropas comandadas por el teniente coronel **Carlos Medina Jurado mientras integraba el Batallón de Artillería N° 4, y cuando fue comandante de la Brigada XIV.**

El abogado indicó que serían más de 400 las víctimas de estos delitos, a manos de hombres que estaban bajo el mando de Medina Jurado, como de una estructura jerárquica en la que estaban incluidos el **general (r) Mario Montoya Uribe y el general (r) Óscar González Peña**; aunque ambos generales no son mencionados en el informe, las organizaciones esperan que se establezcan responsabilidades según la línea de mando, para que las víctimas puedan acceder a si derecho a la verdad, justicia, reparación y garantías de no repetición.

### **El oriente de Antioquia fue uno de los primeros lugares donde se denunciaron los 'falsos positivos'**

Góngora aclaró que en el informe se recogieron casos de ejecuciones extrajudiciales en el Oriente antioqueño y Magdalena medio, zonas en las que se presume hay más víctimas de este delito, siendo "esta región del oriente una de las primeras donde se empezaron a reportar que campesinos era retenidos y luego desaparecidos". Según lo estudiado por el abogado, se trata de hechos que se posibilitaron por la presencia de guerrillas en el territorio, la doctrina militar que asumió a los campesinos como colaboradores de la guerrilla y las órdenes superiores de aumentar el número de bajas en la región.

En esa medida, el integrante de la Corporación Juridica Libertad explicó que la intención de presentar este caso ante el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR) es que sea tomado en cuenta por la JEP, y sea priorizado en el marco del **caso 003, abierto en julio de 2018, sobre "muertes ilegítimamente presentadas como bajas en combate por agentes del Estado"**, para determinar responsabilidades "de todas las personas que hacían parte de la comandancia".

### **El informe es un llamado para que estos hechos no se vuelvan a repetir**

Por último, Góngora afirmó que la presentación de este informe es vital por la situación que ha denunció el New York Times, pues resulta relevante "la opinión pública conozca toda la estructura que dio lugar a las ejecuciones extra judiciales", y que se conozca el dolor y las versiones de las víctimas; como concluyó el abogado, **"es un llamado para que se advierta que esto no se puede repetir".**

"Ni descuido ni manzanas podridas" tendrá una versión de acceso al público al que se podrá acceder a través de las organizaciones que lo realizaron, sin embargo, el documento entregado a la JEP tendrá información de carácter reservado por sus implicaciones y solo cuando la Jurisdicción adelante el trámite del caso y las personas en él mencionadas, podrá ser consultado de forma abierta.

<iframe id="audio_36376673" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36376673_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
