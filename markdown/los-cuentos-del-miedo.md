Title: Los cuentos  del miedo
Date: 2015-04-28 12:25
Author: CtgAdm
Category: Eleuterio, Opinion
Tags: españa, Francisco Quevedo, Franquismo, ley mordaza
Slug: los-cuentos-del-miedo
Status: published

###### Foto: Podemos 

#### Por [**Eleuterio Gabón** ](https://archivo.contagioradio.com/eleuterio-gabon/)

Durante muchos, muchos años España fue un lugar tranquilo donde reinaba la paz social. No existía la disidencia, todo el mundo trabajaba y el orden imperaba en todos los aspectos de la vida. Cada uno sabía bien lo que tenía que hacer y se cuidaba de cumplir con sus obligaciones. Una rutina, atada y bien atada, se cumplía en cada rincón del país.

Los domingos invariablemente, las iglesias se llenaban de devotos prestos a comulgar y atender los sermones de la santa iglesia. En los bares y las tabernas no asomaban debates sobre política, las únicas discrepancias que uno podía escuchar eran sobre fútbol[ ]{.Apple-converted-space}o sobre matadores de toros. Todo lo que un ciudadano de bien debía conocer sobre su país, venía bien explicado en los partes televisados de la noche. La misma noche era un mundo sereno dedicado al sueño y el descanso, nadie salía a deshora a pasear por las calles. Las calles no eran sitio de reunión ni de encuentro para más de dos personas. Tampoco era lugar de manifestaciones amorosas o de cariño, esas intimidades quedaban para el ámbito del hogar. El hogar era el hábitat de la mujer decente, nunca los bares.

Como decíamos, en aquellos años no había problemas. No existían los divorcios, ni el aborto, ni el maltrato, no había homosexuales, ni sindicatos, tampoco manifestaciones de protesta y la libertad de expresión era un cuento de extranjeros. Tampoco había extranjeros, sólo turistas. Si aparecía algún renegado se aplicaba la ley para vagos y maleantes; para posibles rebeldes se tenía la pena de muerte. La policía era incuestionable, el ejército el garante de la unidad de todos.

Cuentos como estos o muy similares, son los que los llamados “nostálgicos del régimen” contaban a sus niños y cuentan a sus nietos. Ellos cuentan que el franquismo salvó del caos al país y trajo una realidad armoniosa en la que ser español era algo maravilloso. No hablaremos aquí de los crímenes franquistas porque si no jamás acabaríamos este artículo; queda pues pendiente. El traer hoy aquí los recuerdos de la dictadura viene al caso por la reciente aprobación de la **ley mordaza** que tantos fantasmas del pasado parece traer consigo. Una ley redactada sin duda por los hijos y los nietos a los que contaban las historias idealizadas que antes hemos referido.

Estos hijos y estos nietos tienen hoy miedo. Sabido es que un sistema autoritario se basa en infligir miedo a los demás y que sin miedo no funciona. Ante las movilizaciones ciudadanas de los últimos años, los miles de manifestaciones y protestas por el estado de las cosas, la toma de conciencia ante el engaño, las valientes denuncias contra la represión, la creación de nuevos discursos, la renovación de las propuestas y de las preguntas, el miedo cambió de bando. No hay duda de esto. La puesta en vigor de la ley mordaza, una ley que protege y acrecienta el aparato represor, que criminaliza la protesta y la libre expresión, es una muestra de ese miedo. De miedo al cambio, a la pérdida de los privilegios, a perder la autoridad, el negocio, la impunidad con la que gobiernan los herederos de aquellos cuentos.

Siempre nos contaron sus cuentos, afortunadamente siempre hubo, siempre hay, quien los desmiente. Francisco Quevedo ya se lo advirtió a la autoridad competente de su época cuando dijo aquello de:

*“No he de callar por más que con el dedo*

*Silencio avises o amenaces miedo…”*

El mensaje sigue vigente.
