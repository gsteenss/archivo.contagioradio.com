Title: Declaran inconstitucionales las modificaciones del Centro Democrático a la JEP
Date: 2019-03-13 21:40
Author: CtgAdm
Category: Judicial, Paz
Tags: JEP, Paz. Corte Constitucional
Slug: declaran-inconstitucionales-las-modificaciones-del-centro-democratico-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/03-02-2015justice_gavel-e1527700435872.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UNOCD] 

###### [13 Mar 2019]

Tras el estudio de la demanda interpuesta contra varios artículos de la ley de procedimiento de la JEP, que habían sido incluidos por el Centro Democrático, la Corte Constitucional falló a favor de los demandantes y aseguró que la jurisdicción Especial para la Paz **puede practicar pruebas en caso de solicitudes de extradición y rechazó la creación de un tribunal diferente para integrantes de las Fuerzas Militares.**

La demanda que había sido interpuesta por la Comisión Colombiana de Juristas, De Justicia y con el apoyo de la organización Human Rights Watch, recalcaba el riesgo de que estos artículos no se apegaran al derecho internacional y legislaban en contra de **asuntos que son competencia de tribunales internacionales como la Corte Penal Internacional.** Le puede interesar: [las víctimas rechazan objeciones del gobierno a la JEP](https://archivo.contagioradio.com/razones-para-aprobar-la-ley-estatutaria/)

En cuanto a la creación de salas especiales para las Fuerzas Militares, el magistrado José Reyes Cuartas aseguró que ese punto, incluido por el partido de gobierno, “podía impedir que se lleven a cabo investigaciones y procesos penales por crímenes de competencia de la Corte Penal Internacional cometidos por miembros de las Fuerzas Armadas”.  Lea también: [Víctimas respaldan a quienes se han acogido a la JEP](https://archivo.contagioradio.com/dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep/)

Según la investigadora de **Dejusticia, Diana Isabel Güiza**, la noticia fue recibida de forma positiva pues la Corte aclara finalmente las dudas que existían sobre la competencia de la JEP en dos temas de gran importancia como lo son los procesos de la Fuerza Pública y en segundo lugar la extradición de excombatientes de Farc.

La investigadora explica que la demanda estaba dirigida hacia tres artículos: dos agregados por el Centro Democrático relacionados a los procesos mencionados anteriormente y otro sobre el derecho al buen nombre, **"pues la norma establece que la JEP debe preservar el derecho al buen nombre de ser incluidos en algún informe"**, un tema al que la Corte no se refirió ni tomó ninguna decisión y que desde Dejusticia han decidido respetar.

En materia de extradición y en específico de la sección de revisión de la JEP se estableció que todos los jueces tienen la facultad de pedir pruebas y que no pueden tomar criterios autónomos si no existe un material probatorio amplio, además agrega **Güiza **que la función de la sección de revisión únicamente debe evaluar "si los hechos cometido e el exterior" ocurrieron antes o después del 1 de diciembre de 2016, fecha en que se firmó el acuerdo.

Finalmente resalta que la decisión final de extradición debe tener en cuenta los derechos de las víctimas y la estabilidad del proceso, una decisión que aplica a todos los excombatientes que se hayan acogido al Acuerdo de Paz, lo que incluye casos como el del dirigente de Farc **Jesús Santrich.  **

<iframe id="audio_33381215" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33381215_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.] 
