Title: ACIN denuncia aparición de nuevo panfleto que amenaza a la guardia indígena
Date: 2019-08-15 10:48
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amenaza, Cauca, guardia indígena, indígenas
Slug: acin-panfleto-guardia-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Amenazas-a-ACIN.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[Según la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN), desde esta semana está circulando un panfleto en la región, en el que se declara objetivo militar a la guardia indígena en el Cauca y en todo el territorio nacional. El panfleto, firmado por quienes se hacen llamar Columna Móvil Dagoberto Ramos, circula luego del asesinato de tres comuneros en una emboscada en Caloto. (Le puede interesar: ["José Eduardo Tumbo líder campesino asesinado en Caloto"](https://archivo.contagioradio.com/jose-eduardo-tumbo-lider-campesino-asesinado-en-caloto/))]

[Esta amenaza señala en concreto al coordinador de la zona norte Oveimar Tenorio y el coordinador del resguardo de Huellas Caloto, Héctor Casamachín, quienes son reconocidos líderes de la Guardia Indígena del Cauca y coincide con la realización de la Comisión de Paz del Senado que abordó el tema de la creciente ola de asesinatos de indígenas que ascendió a 98 este 14 de Agosto. (Le puede interesar: ["Asesinan a Gersain Yatacue, coordinador y guardia indígena de Toribío, Cauca"](https://archivo.contagioradio.com/asesinan-a-gersain-yatacue-coordinador-y-guardia-indigena-de-toribio-cauca/))]

[La ACIN junto a las organizaciones indígenas presentes en el Cauca declararon la semana pasada el estado de emergencia territorial, dado que las amenazas y los asesinatos son recurrentes en la región. (Le puede interesar: ["Durante gobierno Duque han sido asesinados 97 comuneros indígenas: ONIC"](https://archivo.contagioradio.com/onic-gobierno-duque-97-indigenas-asesinados/))]

[Según un comunicado de la ACIN que se emitió luego del panfleto, los intereses económicos de grupos armados en el territorio es lo que está motivando este tipo de acciones en contra de la guardia indígena que ha ejercido control territorial y en varios municipios han evitado la instalación o presencia de actores armados. (Le puede interesar: ["Guardia indígena es atacada en la vía Caloto-Toribío, Cauca"](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/))]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
