Title: Memoria ambiental: el desafío de enunciar la vida
Date: 2017-10-11 06:00
Category: CENSAT, Opinion
Tags: Agua, Ambiente, consultas populares, derecho al agua, Rios Vivos
Slug: memoria-ambiental-el-desafio-de-enunciar-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/censat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 22 Sep 2017

Colombia vive un momento coyuntural, un tiempo en el que se están reorganizando varias instituciones del estado para garantizar la implementación de los acuerdos negociados entre el gobierno nacional y la ex guerrilla de las Farc.  Al mismo tiempo se reconstruyen las verdades del conflicto, con el propósito de dar voz a todos y todas y de reconocer esas voces en los espacios de construcción de políticas públicas y en las narraciones de país. Todo está por discutirse, construirse e implementarse, todo, menos el modelo de desarrollo y las locomotoras que lo impulsan.

[Uno de los ejercicios que garantiza que las verdades de las comunidades y de los sectores más afectados por la guerra tengan un eco, es el de reconstrucción de memoria; un ejercicio que debe privilegiar los sentidos que las comunidades preservan  sobre los acontecimientos, los  significados de la barbarie y las heridas individuales, las cuales, en la medida en que son reconocidas de manera colectiva, trazan un mapa de los daños sistemáticos y de los repertorios violentos que se convirtieron en estrategia de guerra y despojo.  Lo paradójico de estas narraciones es que siempre albergan un contenido ambiental y una evocación territorial sobre las transformaciones, todas ellas asociadas a las dinámicas de la guerra y al arribo de proyectos minero energéticos, pero este contenido parece menor al no encontrar eco en los espacios de discusión ni en las nuevas estructuras gubernamentales que preparan al país para la paz.  Sin embargo, la tierra hace presencia en los escenarios de poder en sus dimensiones más empresariales e instrumentales, lo que facilita la introducción de capitales que homogenizan el paisaje o lo desaparecen.]

[La memoria no es un mero ejercicio para anquilosarse nostálgicamente al pasado, si no que se convierte en un instrumento de proyección para habitar en la tierra y de protección contra los mecanismos que ocasionaron los daños.   ]

[Narrar y reconocer la memoria del agua, de los afluentes, de las técnicas ancestrales y los ejercicios colectivos con que las comunidades dialogan y habitan los territorios, y además narrarla desde la voz de los y las pescadoras, de los y las campesinas, permitiría ampliar la magnitud de los daños y con esto las reparaciones y las garantías de no repetición.  No narrar esta memoria ausenta a las víctimas por megaproyectos de ese país que se construye actualmente.  Esta otra narración no es admisible en la configuración de un Estado que ha decidido soportar su economía sobre la expoliación de los bienes naturales.  Sin embargo, el rumor del agua y la tierra viene envuelto en las narraciones de pescadores y campesinos afectados por la guerra, porque para nombrar el territorio hay que enunciar la vida.]

[Desde el espacio de discusión alentado por Censat Agua viva y el Movimiento Ríos Vivos, junto a otras organizaciones sociales regionales y nacionales, nos hemos encontrado con los ejercicios de memoria histórica ambiental que los procesos locales han venido realizando desde años atrás como un mecanismo para permanecer en el territorio y como una estrategia que les ha permitido blindarse de las estigmatizaciones políticas y armadas.  La memoria histórica ambiental no hace parte hoy día de los grandes relatos de país, pero sí de las memorias locales, que son en buena medida las que deben construir una narración diversa de la Nación. Para los procesos y colectivos que han realizado ejercicios de memoria ambiental, la guerra y los megaproyectos minero-energéticos conllevan la misma amenaza: el despojo y el desplazamiento.  El ejercicio de hacer memoria se convierte entonces en resistencia, como la que llevan a cabo las mujeres y los hombres que hacen parte del movimiento Ríos Vivos y que han sido afectados por la represa de hidrosogamoso en Santander, las cuales recuerdan a medida que van bordando y tejiendo, a través de la técnica de la arpillería, la desaparición del río, la presencia del narcotráfico, el tránsito de grupos armados y las victimizaciones sufridas.  ]

[Cuando se cuenta desde la otra orilla del río se pone en evidencia otras verdades y se iluminan otras realidades.  Narrar la desaparición de ecosistemas, de aguas y formas de vida, así como las propuestas y ejercicios que han convivido con los territorios, abre la posibilidad de pensarnos un país donde lo local sea puesto en el centro, así como lo propone la política de Desarrollo Agrario Integral, que es el primer punto de los acuerdos de paz, encaminado hacia una reforma rural integral, incluida la memoria biocultural de los campesinos y las campesinas.  En esta medida, la mayor garantía de no repetición apuntaría a garantizar la permanencia en los territorios desde las formas locales de habitarlos.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
