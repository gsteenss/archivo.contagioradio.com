Title: La movilización social es legal, lo ilegal es la represión: F Castañeda
Date: 2020-06-16 22:48
Author: AdminContagio
Category: Nacional
Slug: lo-ilegal-en-colombia-no-la-movilizacion-social-lo-ilegal-es-la-represion-desmedida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.23.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ante las movilizaciones realizadas este 15 de junio por diferentes sectores sociales, en ciudades como Cali, Bogotá y Medellín como **medida para exigir una cuarentena digna, un alto a la violencia y cumplimiento del Gobierno,** se registraron nuevamente acciones desmedidas por parte de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diferentes organizaciones defensoras de Derechos Humanos, medios de comunicación y miles de usuarios de redes sociales fueron testigos del uso desmedio de la Fuerza Pública en todo el país, a pesar del escaso número de manifestantes que salieron a las calles a ejercer su derecho a la protesta.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DefenderLiberta/status/1272878649851752449","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefenderLiberta/status/1272878649851752449

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la **Campaña Defender la Libertad un asunto de Todas** en su informe sobre el [balance del día](https://defenderlalibertad.com/boletin-informativo-15dejunio/), fueron **118 personas las detenidas; 45 en Medellín, 73 en Bogotá; y 3 con diagnóstico de lesiones serias en su cuerpo** producto de la intervención de la Policía y el Esmad. (Le puede interesar leer: [Más de 100 detenidos y 20 heridos deja la violencia policial contra movilización del 15 de Junio)](https://archivo.contagioradio.com/violencia-policial-15-junio/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Flankin Castañeda, presidente de la Fundación Comité de Solidaridad con los Presos Políticos (CSPP), organización que hace parte de la Campaña y quienes acompañaron la movilización señaló que *"**es inconcebible que en las principales ciudades no se puede ejercer el derecho a la protesta social** y aún más ante el manejo de doble discurso de lo Gobiernos Distritales".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ClaudiaLopez/status/1272852088993845248","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ClaudiaLopez/status/1272852088993845248

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Destacando las medidas adoptadas por los Gobiernos de las ciudades, *"por un lado permiten que las personas salgan a las calles a comprar a las grandes cadenas comerciales, quienes tienen la probación del Gobierno, pero **restringir el derecho a la protesta social y son permisivos ante las arbitrariedades hechas por la Policía**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Y destacó los pronunciamientos en redes sociales de los mandatarios de Bogotá y Medellín, *"lo que vemos son alcaldes que cada vez se comportan de forma más similar a la derecha y a la política tradicional en el país; **alcaldes llamados alternativos que apoyan la protesta cuando beneficia sin intereses, pero cataloga de vándalos cuando cuestiona su gobernanza**"*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/QuinteroCalle/status/1272664473229295617","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/QuinteroCalle/status/1272664473229295617

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante estos discursos señaló que *"**quienes están poniendo en riesgo a las personas son ellos, con sus medidas pensadas más en la economía y no en la gente"***, y recordó que en Colombia la cuarentena se esta acabando es porque el Gobierno tanto nacional como local no supo adoptar medidas adecuadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**La gente ya no puede seguir con esta cuarentena, no puede soportar mas desempleo, hambre, quiebras y agresiones**, ni tampoco de una política de gestión que pretende fortalecer a los ricos a costa de la salud y los sectores mas pobres"*, resaltó Castañeda y agregó que esto ocurre en medio de un contexto que ya venía mal.

<!-- /wp:paragraph -->

<!-- wp:quote -->

>   
> *"Es inconcebible que tengamos la posibilidad de adquirir bienes suntuarios, pero que no podamos salir a ejercer nuestro derecho a la libertad de expresión , protesta social y derecho a la reunión"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Para Castañeda no se puede dejar de lado la serie de movilizaciones en contra de la gestión del Gobierno que se venía evidenciando desde noviembre del año pasado, *"**cada vez más gente se va a atrever a movilizarse por mejores servicios, en contra de las amenazas a una lista de reformas absurdas y ahora por las libertades y garantías** en contra del racismo y porofobia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el defensor de DDHH el olvido y las agresiones siempre se dan hacia las comunidades pobres y que muchas veces son afros, indígenas o campesinas y LGBTI, por lo tanto es normal que haya ese tipo de levantamientos en Colombia, ***"lo anormal es que se ordene a los policías agredir a la sociedad a partir de la represión extrema".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Colombia se facilita todo menos la movilización social

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el presidente del CSPP es inconcebible que ante claros protocolos de respeto a la protesta social, señalados en Resoluciones como la 1190, dada en medio de los diálogos de La Habana, hoy estos se encuentren *"engavetados por parte de las autoridades nacionales, a pesar de estar vigentes"*, y que por el contrario se flexibilicen 43 excepciones en la pandemia, menos la movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y recordó que con la llegada de Claudia López, también se habló de nuevos protocolos para la movilización un protocolo de la mano de las madres de quienes salen a marchar, ***"esta era la construcción de un modelo a partir de la imagen de los padres para ejercer más represión y de regañar y no en el ejercicio de la protección de los Derechos Humanos".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La agresión es cada vez peor y no pasa nada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante el aumento de las agresiones por parte de funcionarios de la Fuerza Pública, las cuales han dejado como saldo múltiples fracturas, lesiones, [amputación de órganos](https://archivo.contagioradio.com/perdio-su-ojo-por-el-esmad-pero-no-la-fuerza-para-luchar-cristian-rodriguez/) e incluso la [muerte](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/), es fácil pensar que lo que prima en este Gobierno es la impunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que Castañeda señaló como un patrón que se viene y se vendrá repitiendo entorno a una cadena de favores, ***"esto que ocurre en la movilización es lo mismo que pasa con los líderes y defensores, callar para beneficiar, y archivar antes que investigar la verdad"***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Como todos los crímenes en Colombia, en la impunidad, la Fiscalía y los órganos de control, colocan una buena parte de la cuota para que esta realidad no cambia en el transcurso del tiempo"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

En consecuencia para Castañeda, *"un policía tiene la certeza de que puede torturar, reprimir e incluso asesinar personas en el marco de la protesta social sin que nada le pase"*; y recordó el caso del policía responsabilizado y registrado en el asesinato de Dylan Cruz, el cual solo recibió una suspensión de carácter humanitario, y hoy sigue ejerciendo su labor.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**La única forma de acabar con la impunidad en Colombia va a ser con cambios políticos profundos**, eso quiere decir que la labor de defensores de Derechos Humanos es muy importante para denunciar y construir Gobierno basados en la verdad y la justicia"*  

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por último destacó que el movimiento social está despertando a pesar de situación atípica que se vive por pandemia, *"sé que antes de esta pandemia cientos de miles de personas se manifestaron en todo el país, y es imposible pensar que las medidas adoptadas en la pandemia fueron suficientes para olvidar el porqué se salía a las calles"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, "necesariamente esa movilización social va a subir por qué es falso que en ese proceso de "reinversión del capitalismo *"se vayan a tomar medidas para favorecer a la gente, al contrario cuando esta olla a presión explote la única opción que nos va a quedar es salir a protestar".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**No me da miedo asumir que en Colombia necesitamos más protesta social** porque es la única alternativa que vamos a tener para que el gobierno colombiano de una mirada a una política diferente".*

<!-- /wp:quote -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray"} -->

*Le puede interesar ver Otra Mirada: "De esto te hablamos viejo ¡Se activa la movilización social!.*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F656309261620475%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" style="border:none;overflow:hidden" scrolling="no" allowtransparency="true" allow="encrypted-media" allowfullscreen="true" width="734" height="413" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
