Title: La quiebra de la ideología neutral
Date: 2019-05-25 07:30
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: ideologias, pensamiento crítico, polarización, uribismo
Slug: quiebra-ideologia-neutral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-24-at-16.07.10.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Ya pronto serán 30 años desde que el mundo occidental daba cantos de victoria ante el fin de la guerra fría. Por supuesto no podía cantar tan fuerte porque aún sabiendo que había ganado, proteger la victoria en el plano ideológico, eran muy necesario para consolidar una hegemonía absoluta.

Emergieron y florecieron en sótanos con calor y luces artificiales, esos discursos que hablaban sobre “el fin de las ideologías” “el fin de la historia” como si toda la realidad mundial, pudiera pasar por el ojo de occidente. Por allá a finales de los noventa mientras los victoriosos liberales (que se les reconocía como neoliberales) dictaban la última palabra sobre el supuesto fin de las ideologías, Latinoamérica levantó la mano y dijo: oye chamo un momento.

La ideología neutral viajó desde la política institucionalista, hasta la academia. Políticos e intelectuales comenzaban sus discursos con una frase sin mucha sorna “*no pretendo ideologizar mis palabras*” y acto seguido, despotricaban contra su fantasma eterno: el socialismo.

La hegemonía fue tan grande que en las aulas universitarias muchos docentes profesaron a sus estudiantes “*si vinieron a cambiar el mundo, aquí no es el lugar*”, otros fueron capaces de decir “*esos autores ya están pasados de moda, entremos en materia*”, incluso, los más fundamentalistas de esa ideología neutral exhortaban a estudiantes de sociología a que “*no busquen problemas en el mundo sino resalten la cosas que están bien*” como si los problemas fueran algo de actitud; el caso es que así fueron consolidando la hegemonía del vencedor con tal de gastar su frustración transformada en actitud positiva, en un aliento artificial, que hizo el cambiazo de la esperanza por el deseo de consumir.

Mientras el siglo XXI avanzó, con él, el discurso escueto del “fin de las ideologías” quedó acallado, sentadito y juicioso, mientras observaba el giro a la izquierda de una región. Hoy que el giro a la derecha es más que evidente, volver a implementar el trasnochado “fin de las ideologías” tenía cabida en la lingüística contemporánea, siempre y cuando cayera Venezuela o por lo menos no saliera Ordoñez con esas ventoleras en la OEA.

Si hace un tiempo había emergido con suficiente fuerza emocional el discurso de “*viva la paz*”  mientras a la par esperaban con ojos de sangre la nunca acontecida caída de Venezuela, sumado al exterminio de personas que se vive en el país, con la probabilidad del resurgimiento de falsos positivos, con más de 3.000 personas entrenando en las selvas del Guaviare, con el conflicto ardiendo de nuevo en el Cauca, en Chocó, en Catatumbo, en Arauca y avanzando de la mano del narcotráfico colombo-mexicano; esa fuerza emocional se la llevó el diablo, dejando solo al cielo estrellado y el silencio, un binomio perfecto de color fúnebre esperando a ser interrumpido por una ráfaga de fusil.

Queda en el ambiente la tranquilidad de una ciudad que vive feliz en su agradable pobreza, que es libre de masticar su miseria administrativa y que no tiene metro elevado o subterráneo ¡pero que afortunadamente! respirando ese mal necesario humo de Transmilenio, nunca, nunca, nunca sufrirá la guerra.

Queda una derecha que celebra el regreso de la guerra con vestigios de ideología neutral que ahora llaman “pacto nacional” y una izquierda liberalizada que supera la reacción que constituyó las Farc-ep, en forma de Rodrigo Londoño quien ahora usa términos del fiscal para acusar al que fuera su camarada.

Si hace un tiempo había emergido con fuerza emocional suficiente el discurso de “no polaricemos” como aquel lindo corbatín que usa la ideología neutral cuando quiere desautorizar aquello que no entiende; hoy, la polarización, el “no polaricemos”, descubrió que la neutralidad presentada como imparcial, se convertía en un polo más, uno bastante agresivo de hecho por creer que tenía la razón poderosa de desautorizar la naturaleza dialéctica de la política, y que ignoraba a quiénes escribían la última palabra de la historia llamada “objetividad”… ¡fue allí!  ¡Allí quedó tendida sobre el sueño la neutralidad!

Aguerridos y dóciles defensores de la no-polarización han surgido desde la literatura, la política, los medios informativos y las universidades. Negaron las contradicciones aún sabiendo que negándolas no habría posibilidad de que dejaran de existir. Desautorizaron las contradicciones, suponiendo un estado de superación de las mismas, sin más propuesta que la desautorización de las contradicciones, es decir, se creyeron el papel de falsos mesías.

**La verdad muere ante la pretensión de la neutralidad política que hoy sentimentalmente muchos defienden.**

Por supuesto no se trata de sindicar de “resentido” o “violento” al que defiende la polarización. Pero tampoco se trata de sindicar como justo al que pretende la neutralidad con argumentos que la realidad misma se encarga de superar.

¿Hay una forma en que la polarización sea vista positivamente? No podemos confundir el sentido político y social de la polarización, con la propaganda vulgar del uribismo que siempre somete a la ciudadanía a un sí o a un no, tal como en las urnas virtuales que dan los resultados en la emisión de las 7 de la noche. Eso no es polarización, porque siendo francos esa masa que cae en semejante engaño, no sabe por qué sí, ni sabe por qué no. Esa masa, es la masa de cobardes que alimentan los millones de comentarios en Twitter, Facebook, etc.

Dejando a un lado el desprestigio de la polarización, creo que esta tiene un aspecto positivo:  podemos polarizar para afirmarnos, es el primer paso. Luego, dejar de afirmarnos para escuchar al otro sería el segundo paso. Por último, volver a afirmarnos con el legado del otro interiorizado sería un tercer paso. En la polarización reposa el reconocimiento de la autenticidad, de intereses colectivos que son irremplazables, pero allí también, quizá oculto por el velo de la imposibilidad que muchos tienen para dialogar sin ofenderse porque no les dicen lo que quieren oír, se halla la equivalencia entre tanta diferencia.

La ideología neutral está quebrada, ya no es una “empresa” seria, porque delimitó su razón social, a una pretensión de verdad que la realidad siempre le negó. Por eso se dedicó fielmente a negar a sus contradictores, con tal de prevalecer como el arma favorita de la derecha cuando gana la contienda en un momento histórico.

La ideología neutral quebró, porque convirtió al Ni Ni es su tótem. Negando y negando, el ni ni se creyó justo sin serlo, porque lo justo sería reconocer la existencia de aquello que niega y deliberar con él, preguntarle, aunque sea de manera reprimida y rabiosa “¡por qué no dejas de existir!”.

La ideología neutral quebró, porque le queda muy complicado darse cuenta que es ella, el arma consentida del poder, cuando este vence (claro). Y este, por lo menos en Colombia, no ha dejado de vencer.
