Title: "Para negociar es necesario abrir debates y para eso se requiere a la sociedad" ELN
Date: 2017-01-26 13:31
Category: Entrevistas, Paz
Tags: Cese al fuego bilateral, conversaciones de paz con el ELN, Diálogos ELN
Slug: para-negociar-es-necesario-abrir-debates-y-para-eso-se-requiere-a-la-socieda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/sILVANA-GUERERO-ELN-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ELN Paz 

###### 26 Ene 2017 

En entrevista con Contagio Radio**, Silvana Guerrero, comandante del ELN e integrante de la delegación de paz** de esa guerrilla afirmó que una de las mayores preocupaciones que tiene esa delegación es que “en algún momento el gobierno no tenga la voluntad política para continuar con el proceso", esto debido a los debates que se darán en la mesa.

Para Guerrero un paso importante es que se hayan resuelto los “atranques” que impidieron que el proceso iniciará, y para ello se necesitó de mucha voluntad política desde las dos partes en conversaciones, por ejemplo acordar que la liberación de Odín Sánchez y los indultos de los integrantes de esa guerrilla se dieran  de manera simultánea según el acuerdo.

La integrante del ELN afirmó que por parte de esa guerrilla ya se inició el operativo de liberación del político que tendrá su finalización el 2 de Febrero. Igualmente la comandante de esta guerrilla retomó las palabras de Pablo Beltrán,  jefe negociador del ELN donde afirmaba  que el próximo 28 de enero se entregarán las coordenadas para que se realice dicha operación humanitaria, según los acuerdos que se han tenido en la mesa de diálogo.

### [Cese al fuego bilateral] 

Frente a un posible cese bilateral al fuego o un desescalamiento de las acciones bélicas, la integrante del ELN resaltó que sobre ese tema ya hay un acuerdo planteado en la agenda de conversaciones en el punto que corresponde a acciones y dinámicas humanitarias, e igualmente este punto se irá trabajando mientras se va generando un “clima de paz”. Sin embargo también recordó que ha sido el gobierno el que se ha negado a conversar en medio de un cese bilateral.

Para la integrante del equipo de paz los medios de información no están mostrando las realidades que viven las mayorías del pueblo colombiano y solo hacen visibles las acciones de guerra por parte de esa guerrilla y no las denuncias. Un ejemplo de ello es la invisibilidad en torno al asesinato de líderes sociales y defensores de DDHH que se cometen en el país, para Guerrero “eso nos pone en desventaja ante el pueblo colombiano”.

### [Participación social debe ser masiva y vinculante] 

“Para negociar es necesario dialogar y abrir debates y eso es lo que prima para nosotros en estos momentos” afirma Silvana Guerrero, y resalta que la participación de la sociedad debe ser masiva y debe ser vinculante, es decir, que lo que se acuerde con los sectores sociales con los que se abran debates debería estar incluido en la batería de acuerdos que se logren en la mesa. “En base a esos diálogos se acordarán las negociaciones” resaltó.

También recordó que los mecanismos y metodologías a trabajar se estarán acordando con diversos sectores sociales. En ese sentido no se puede saber cómo será la participación porque decir algo en este momento sería “imponer” un estilo y esa no es la idea. Los mecanismos “serán parte de los acuerdos y de la discusión que se dará con el gobierno nacional” [Le puede interesar: ["Participación Social será el corazón del proceso de paz ELN-Gobierno" Carlos Velandia](https://archivo.contagioradio.com/participacion-social-sera-el-corazon-del-proceso-de-paz-eln-gobierno-carlos-velandia/)]

### [Preocupaciones, cotidianidad y diversión hacen parte de la condición humana en las conversaciones de paz] 

Como una cotidianidad en la que prima el respeto en medio de álgidas discusiones describió Silvana Guerrero el día a día de las conversaciones de paz. También señaló que en la delegación del ELN prima el optimismo y recordó que la lucha armada que ellos y ellas escogieron fue también porque están buscando la paz con condiciones dignas para todos y todas. Todo ello en medio de situaciones divertidas que prefiere compartir una vez se instale la mesa de conversaciones.

------------------------------------------------------------------------

<iframe id="audio_16670843" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16670843_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### Reciba toda la información de Contagio Radio en [[su correo]
