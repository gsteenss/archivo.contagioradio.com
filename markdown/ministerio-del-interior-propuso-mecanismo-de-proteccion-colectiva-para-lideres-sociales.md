Title: Ministerio del Interior propuso mecanismo de protección colectiva para líderes sociales
Date: 2018-04-23 12:49
Category: DDHH, Política
Tags: defensores de derechos humanos, lideres sociales, mecanismos de protección, Ministerio del Interior, protección de líderes sociales, violación a los derechos humanos
Slug: ministerio-del-interior-propuso-mecanismo-de-proteccion-colectiva-para-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Abr 2018] 

El Ministerio del Interior emitió el decreto 660 de 2018 que busca crear nuevas medidas para la protección de los líderes sociales y las comunidades en los territorios. Estas medidas hacen énfasis **en la protección colectiva** y crean un promotor de paz en los territorios para responder al aumento de la violencia contra los defensores de los derechos humanos y del territorio en el país.

En el decreto quiere crear e implementar el **Programa Integral de Seguridad y Protección para las Comunidades y Organizaciones en los Territorios** y definir las medidas necesarias que protejan de manera integral a las comunidades. Esto incluye a los líderes, dirigentes, representantes, activistas, mujeres, indígenas, integrantes de la comunidad LGBTI y defensores de derechos humanos.

### **Medidas se coordinarán con las comunidades** 

Estas medidas integrales “tienen como propósito la prevención de violaciones, protección, respeto y **garantía de los derechos humanos** a la vida, la integridad, la libertad y la seguridad de comunidades y organizaciones en los territorios”. Éstas se realizarán en coordinación con las comunidades y se articularán con el Sistema de Prevención y Alerta para la Reacción Rápida. (Le pude interesar: ["Asesinatos de líderes sociales son prácticas sistemáticas: Somos Defensores"](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/))

De acuerdo con el Ministerio, las medidas están orientas a identificar los **factores de riesgo de violaciones a los derechos humanos** contra las comunidades y las organizaciones que funcionan en los territorios. De igual formar, la adopción de las mismas estarán encaminadas a mitigar los efectos de los riesgos que además promuevan “la reconciliación, la convivencia pacífica y democrática en los territorios, para la construcción de confianza entre entidades públicas y comunidades, a través de la articulación local y nacional”.

### **Promotores de paz y apoyo a la denuncia medidas que propone el Gobierno** 

En específico, el decreto busca que se creen medidas integrales de prevención, seguridad y protección a partir del fortalecimiento de lo que denomina el ministerio público como **“promotores comunitarios de paz y convivencia”.** Además, se creará un protocolo de protección para territorios rurales así como un apoyo a la actividad de la denuncia.

Ahora, quienes deberán formular y elaborar los Planes Integrales de Prevención serán las **gobernaciones y las alcaldías** en conjunto con la Fuerza Pública que tenga jurisdicción en los territorios, toda vez que no se pueden reemplazar las medidas constitucionales donde el Estado es quien debe garantizar la seguridad de las personas. (Le puede interesar:["Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/))

Además, será la **Fuerza Pública** la que funcione como un puente de comunicación con las comunicaciones y deberán realizar “reuniones periódicas de seguimiento sobre la pertinencia de las medidas adoptadas”. El papel del Ministerio del Interior será hacer seguimiento al programa atendiendo a la información que se recoja de los territorios.

### **Mecanismos de protección a líderes sociales aún son débiles en el país** 

En repetidas ocasiones, organizaciones de la sociedad civil, han manifestado que las medidas de protección para los líderes sociales **aún no son suficientes** y no responden a las necesidades de los territorios. A esto se suma el reiterativo desconocimiento de la sistematicidad de los homicidios por parte de la Fiscalía General de Nación y la descalificación de los mismos por parte del Ministerio de Defensa.

Estos hechos han puesto en riesgo a las comunidades que además advierten de la presencia de **grupos paramilitares** en sus territorios, afirmaciones que desconoce el Gobierno Nacional. Adicionalmente, las medidas que se han implementado siempre involucran la presencia de la Fuerza Pública la cual, según los líderes y defensores, no garantiza su protección.

Esto teniendo en cuenta que han sido varios los asesinatos que han sido denunciados que ocurrieron a **pocos metros de las bases de las Fuerzas Armadas** sin que estas hagan algo para proteger la vida de los defensores. El decreto pasará al Congreso de la República para ser debatido allí donde se espera sea aprobado.

[D-660-18-Programa Integral de Seguridad y Protección para Comunidades y Organizaciones en los Territorios...](https://www.scribd.com/document/377151322/D-660-18-Programa-Integral-de-Seguridad-y-Proteccio-n-para-Comunidades-y-Organizaciones-en-los-Territorios-1#from_embed "View D-660-18-Programa Integral de Seguridad y Protección para Comunidades y Organizaciones en los Territorios (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_85829" class="scribd_iframe_embed" title="D-660-18-Programa Integral de Seguridad y Protección para Comunidades y Organizaciones en los Territorios (1)" src="https://www.scribd.com/embeds/377151322/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-GQK8pqxQV7lPSRHPs45C&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6569555717407137"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="osd-sms-wrapper">

</div>

   
 
