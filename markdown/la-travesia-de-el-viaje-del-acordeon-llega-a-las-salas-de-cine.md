Title: La travesía de "El viaje del Acordeón" llega a las salas de cine.
Date: 2015-05-14 15:42
Author: CtgAdm
Category: 24 Cuadros
Tags: Andrew Tucker, Aurora Film und TV, Cine Colombia, Ciudad Lunar Producciones, Documental colombo-alemán, El viaje del Acordeón Documental, Festival Internacional de Cine de Cartagena, Fondo Cinematográfico de Hamburgo / Schleswig-Holstein, Fondo Cinematográfico de Rheinland-Pflaz, Limelight Producciones, Manuel Vega juglar vallenato, Orquesta de acordeones de la Hohner, Rey Sagbini, Universidad del Magdalena
Slug: la-travesia-de-el-viaje-del-acordeon-llega-a-las-salas-de-cine
Status: published

*El documental se estrena en Colombia luego de trasegar por diferentes festivales alrededor del mundo.*

**Rey Sagbini** (Colombia) y **Andrew Tucker** (Alemania) son los directores de "**El viaje del Acordeón**" un documental inspirado en el instrumento musical que llegó a nuestro país de manera accidental a finales del siglo XIX para convertirse en símbolo representativo y parte fundamental del vallenato, uno de los géneros musicales representativos del país.

Durante cuatro años de trabajo continuo, que representó múltiples viajes desde Europa a nuestro país, Sagbini y Tucker trabajaron en el proyecto que une dos culturas diferentes y a dos amigos que se conocieron durante sus días como estudiantes en **Hamburgo, Alemania**.

La coproducción colombo-alemana que se estrena hoy en **salas de Cine Colombia** de todo el páís, tuvo su estreno oficial durante la edición 43 del **Festival Internacional de Cine de Cartagena FICCI** en el año 2013, donde fue reconocida con el premio del público, fue presentada además en 17 festivales de cine en todo el mundo aproximadamente.

"**El viaje del Acordeón**" es la historia de **Manuel Vega**, un juglar vallenato que junto a **Dionisio Bertel** y **Jairo Suárez** se han presentado durante 16 años en el **Festival de la Leyenda Vallenata**. La vida de estos hombres se transforma cuando les llega una invitación para tocar junto a la legendaria **Orquesta de acordeones de la Hohner**, en la S**elva Negra alemana**, emprendiendo un viaje épico al otro lado del mundo.

<iframe src="https://www.youtube.com/embed/1OB38DkM04g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Las productoras **Limelight Producciones** Colombia, **Ciudad Lunar** y **Aurora Film und TV**, son las responsables de llevar a los espectadores el documental que  cuenta con el apoyo de la **Universidad del Magdalena**, el **Fondo Cinematográfico de Hamburgo/Schleswig-Holstein**, la secretaria de cultura de la ciudad de Hamburgo y el **Fondo Cinematográfico de Rheinland-Pflaz**.

Compartimos con ustedes la **sinopsis** del audiovisual:

Este documental enlaza dos mundos por medio de un sonido y su instrumento, el acordeón, básico en la música vallenata. Traído desde Alemania rumbo a Buenos Aires, llegó por accidente a Colombia, o al menos así lo cuenta la leyenda. Los directores a través del músico Manuel Vega y su grupo, realizan un viaje desde su fallida participación en el concurso del Festival de la Leyenda Vallenata, hasta su presentación con la Orquesta de acordeones Hohner en la ciudad de Trossingen, Alemania a donde fueron invitados como representantes del vallenato, enlazando los orígenes y sonidos de este instrumento, los sueños y sus transformaciones en un homenaje a la vocación, la música y la alegría.
