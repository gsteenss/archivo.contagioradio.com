Title: El Festival de Cine por los Derechos Humanos viaja por Colombia
Date: 2016-08-29 15:56
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colmbiano, Festival cine por los derechos humanos, Festivales de cine colombia
Slug: el-festival-de-cine-por-los-derechos-humanos-viaja-por-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ddhh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Impulsos Films 

###### [29 Agos 2016] 

Desde este lunes y hasta el 10 de diciembre, el **3er Festival de Cine por los Derechos Humanos de Bogotá**, inicia su etapa de circulación nacional. Una travesía que llevara por diferentes ciudades del país los trabajos más destacado de la más[reciente edición.](https://archivo.contagioradio.com/asi-luce-la-3ra-edicion-del-festival-internacional-de-cine-por-los-ddhh/)

Las [90 películas](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/)de 27 países que conformaron la selección oficial del Festival, llegarán a salas, auditorios y aulas de clase entre otras, de ciudades como **Barranquilla, Buenaventura, Cúcuta e Ibagué**; llevando al público, de manera gratuita, excelentes producciones que abordan la temática de derechos humanos, su promoción, defensa y protección.

Como valor agregado, las proyecciones estarán acompañadas por espacios académicos como cineforos y talleres, que tienen como objetivo promover el cine con enfoque de derechos humanos en las ciudades, para la construcción, en conjunto, de una **cultura de paz**.

Con esta iniciativa el Festival de Cine por los Derechos Humanos, busca aportar a la formación de públicos en las regiones y **contribuir desde el audiovisual a la paz y al desarrollo social de las comunidades**.

La circulación del Festival, organizado por **Impulsos Films** con el apoyo de la **Fundación Konrad Adenauer,** es posible gracias a la colaboración en las regiones del Museo Norte de Santander y ciudad de Cúcuta, Sueño Pacífico, Arte y cultura en Buenaventura, Universidad del Atlántico, Alianza Francesa, sede Barranquilla, Conservatorio Audiovisual del Tolima y Festival de Cine de Ibagué,

**Programación Cúcuta**

Del 29 de agosto al 3 de septiembre en el la 12ª Fiesta del Libro, organizada por la Corporación Cultural Biblioteca Pública Julio Pérez Ferrero.

**Buenaventura**

Del 2 al 5 de septiembre en el marco del proyecto “Sueño Pacífico”, ganador de la categoría Prácticas Culturales de la convocatoria RecOn.

**Territorio nacional**

Del 4 al 11 de septiembre, durante la Semana por la Paz

**Barranquilla**

20, 21, 22 de septiembre y 1 de octubre. Muestra en la Universidad del Atlántico y la Alianza Francesa.

**Ibagué**

Noviembre. En el marco del Festival de Cine de Ibagué en alianza con el Conservatorio Audiovisual del Tolima.
