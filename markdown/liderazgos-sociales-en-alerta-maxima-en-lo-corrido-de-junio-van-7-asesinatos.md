Title: Liderazgos sociales en alerta máxima. En lo corrido de Junio van 7 asesinatos
Date: 2020-06-08 23:56
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Cauca
Slug: liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/asesinato-de-lider-indigena-en-tacueyó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Liderazgos sociales/ CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras organizaciones como el Instituto de Estudios para el Desarrollo y la Paz (Indepaz) a la fecha contabilizan un total de [121 líderes asesinados](http://www.indepaz.org.co/paz-al-liderazgo-social/) solo en el presente año, las ataques contra defensores de DD.HH. continúan elevándose, apenas **en el primer día de junio, tres de ellos fueron asesinados** y en lo recorrido del mes esta cifra ha ascendido a siete homicidios en los departamentos de Córdoba, Antioquia y Cauca en medio de un aislamiento preventivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La primera víctima fue **Hermes Loaiza quien fue asesinado en zona rural de Florida, Valle, frontera con Caloto, norte del Cauca.** Hermes se desempeñaba como secretario de la Junta de Acción Comunal (JAC) de dicho municipio y aún se investiga si el hecho se habría perpetrado por su labor al interior de dicha organización. ([Lea también: Hermes Loaiza, secretario JAC de Pueblo Nuevo, fue asesinado en Florida, Valle](https://archivo.contagioradio.com/hermes-loaiza-secretario-jac-de-pueblo-nuevo-fue-asesinado-en-florida-valle/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El mismo día fueron asesinados Arcángel Pantoja y Omar Agudelo en Puerto Libertador, Córdoba;** los dos eran miembros fundadores de la Asociación de Campesinos del Sur de Córdoba. Pantoja por su parte, también pertenecía a la Junta de Acción Comunal (JAC) de la vereda Río Sucio. **El secretario del Interior y Participación Ciudadana de Córdoba, Camilo Berrocal, señaló que el Clan del Golfo estaría detrás de los homicidios en este caso.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tan solo dos días después, el **dirigente comunitario** **Julio Humberto Moreno Arce fue asesinado con arma de fuego** el 3 de junio cuando se dirigía en una motocicleta por la vía que conduce de La Balsa al municipio de Santander de Quilichao, Cauca; en sus documentos fue hallado un carné que lo acreditaba como defensor de DD.HH. Según versiones, Moreno había denunciado amenazas en su contra en el mes de diciembre de 2019. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El caso más reciente fue el de **Óscar Dicto Domicó, líder indígena** quien se desempeñaba como coordinador de la guardia indígena del resguardo Embera Katio, en el Alto Sinú, quien fue asesinado con arma blanca el pasado 3 de junio en zona rural de Tierralta, Córdoba. A esto hecho se suman las recientes intimidaciones hechas contra la defensora de DD.HH. del pueblo nasa Aida Quilcué y su familia en el departamento de Cauca y de las que han advertido la Organización Nacional Indígena de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A ellos se suma el asesinato de dos menores de edad familiares de personas en proceso de reincorporación, víctimas de las amenazas contra habitantes del ETCR ubicado en Ituango, Antioquia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La tendencia de agresiones contra liderazgos podría aumentar en 2020

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Organizaciones sociales advierten que la persecución que ha incrementando en épocas de aislamiento se trata de un ataque sistemático** del que están siendo víctimas los líderes y lideresas sociales y todas aquellas personas que abogan por la defensa de los DD.HH. en el país. ([Lea también: Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Así lo confirman también registros como los del programa Somos Defensores que recoge las cifras del **Sistema de Información sobre Agresiones contra Personas Defensoras de Derechos Humanos en Colombia (SIADDHH)** que estimó el **aumento en los homicidios en contra de líderes y defensores en un 88%** en el primer semestre del año respecto a las cifras del 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Su actual coordinadora, Lourdes Castro **afirma que el aumento es significativo, incluso en tiempos de pandemia** lo que "demuestra que la vulnerabilidad en tiempos de Covid-19 es mayor" señalando que hay un vínculo directo entre los liderazgos sociales y su derecho a reclamar cambios y transformaciones, tal y como se vio en el paro nacional del 21 de noviembre de 2019.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
