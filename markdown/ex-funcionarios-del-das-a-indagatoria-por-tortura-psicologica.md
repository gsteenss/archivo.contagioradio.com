Title: Ex-funcionarios del DAS a indagatoria por "tortura psicológica"
Date: 2015-06-24 15:01
Category: Judicial, Nacional
Tags: Claudia Julieta Duque, cpdh, das, diego martinez, enrique ariza, giancarlo auque, José Miguel Narváez, Maria del Pilar Hurtado, tortura
Slug: ex-funcionarios-del-das-a-indagatoria-por-tortura-psicologica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Sede-del-DAS-en-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo de Abogados José Alvear Restrepo 

<iframe src="http://www.ivoox.com/player_ek_4683467_2_1.html?data=lZullZmae46ZmKiak5uJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidmhx9rbxc7TssLmytTgjcnJsIy4oriY1MrSqMrmhqigh6aVsozdz8nOycbYs9PdwpDhw9LGrYa3lIqum9OPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diego Martinez, CPDH] 

#####  

######  

###### [24 jun 2015] 

Este miércoles 24 de junio inició el proceso de juicio en contra de los ex-funcionarios del Departamento Administrativo de Seguridad, DAS, José Miguel Narváez, Giancarlo Auqué y Enrique Ariza, por la tortura psicológica de la periodista Claudia Julieta Duque.

Aunque suele relacionarse al DAS únicamente con las interceptaciones ilegales, más conocidas como "chuzadas", estos no son los únicos crímenes que se cometían desde la institución estatal.

La Fiscalía considera que los ejercicios de tortura psicológica no eran aislados o autónomos por parte de algunos funcionarios, sino que el Departamento Administrativo de Seguridad contaba con manuales de tortura que dan cuenta de la realización de estos crímenes de manera sistemática.

Como señala el abogado Diego Martinez, del Comité Permanente por la Defensa de los Derechos Humanos, este tipo de manuales son herencia de la formación de militares en la Escuela de las Américas, y en este sentido, "el DAS era nada más y nada menos que la policía política, el instrumento de los gobiernos de turno para cometer violaciones a los Derechos Humanos".

El abogado agregó que muchas veces suele pensarse "que la tortura se infringe únicamente de manera física, pero aquí lo que se ha logrado comprobar es que el Estado, a través de su policía secreta, infringía psicológicamente tratos crueles, degradantes e inhumanos".

Para el CPDH, es positivo que se inicie este proceso, en tanto se reconoce que el DAS es la punta de la cadena de muchos crímenes, arrojando las primeras condenas por un tipo especial de crimen, como la tortura, cometido por una institución estatal. Sin embargo, reiteran que el caso de la periodista Claudia Julieta Duque llegó a su etapa judicial como resultado de una juiciosa tarea de búsqueda y recopilación de fuertes componentes probatorios por parte de las víctimas, y no por un trabajo riguroso por parte de la Fiscalía.

En las organizaciones defensoras de Derechos Humanos existe el temor, porque la negligencia del ente investigador no adelante la descalcificación pertinente de archivos del extinto DAS, permitiendo que estos finalmente se pierdan, máxime en el marco de un proceso de paz en el que se plantea como imperativa el establecimiento de la verdad.
