Title: Organizaciones ecologistas impulsan la creación de una Comisión de la Verdad ambiental
Date: 2017-03-11 11:43
Category: Ambiente, Entrevistas
Tags: comision de la verdad, Hidroeléctricas, Mineria, paz, ríos
Slug: organizaciones-ecologistas-impulsan-la-creacion-una-comision-la-verdad-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Movimiento Rios Vivos 

###### [11 Mar 2017]

Este viernes inició la jornada en defensa de los territorios, el agua y la vida. La séptima jornada nacional que organiza el Movimiento Ríos Vivos, se enfoca en la **relación que existe entre los afluentes y la construcción de paz**, pues si los territorios se ven como mercancías y los ríos son represados no se puede hablar de paz, así lo explica Isabel Cristina Zuleta, vocera de la organización.

Zuleta, señala que se busca visibilizar la propuesta desde esa organización y el Centro de Memoria Histórica, frente a la necesidad de establecer una Comisión de la Verdad frente a los crímenes ambientales. De esa manera se busca reivindicar a la naturaleza como víctima de los más de 50 años de conflicto armado colombiano.

En Santander, Antioquia, Bogotá, Cauca, Córdoba, Huila, y otras ciudades del mundo se realizarán diversas actividades en torno a lo que significa la defensa de los ríos, frente a los intereses económicos mundiales y el desarrollo de proyectos hidroeléctricos.

"La mayoría de los ríos han sido los depositarios de personas asesinadas y de la sangre (...) La ciudad está enfermando el campo colombiano". dice Zuleta, quien agrega que de eso se trata la violencia ambiental, ya que el conflicto colombiano ha favorecido los intereses económicos de empresas que construyen represas y otros **proyectos extractivistas, que a su vez, desplazan forzosamente a las comunidades**, como actualmente está sucediendo con las poblaciones afectadas por Hidroituango en Antioquia.

Teniendo en cuenta esa relación entre los ríos y la paz, las actividades de esta séptima jornada, se realizarán en zonas veredales de concentración, como lo son Briceño, Toledo e Ituango. El objetivo es fortalecer los procesos comunitarios desde el enfoque ambientalista, para que los pobladores sepan defender sus territorios de las empresas.

Por ejemplo, según explica Isabel Cristina, se ha conocido que **compañías como Continental Gold, ya está socializando un proyecto minero en esas zonas de Antioquia**, por ello, es importante capacitar a los pobladores para que no permitan que las empresas ingresen a los territorios.

Finalmente la vocera de Ríos Vivos, añade que las actividades de la jornada buscan impulsar la solicitud a la ANLA, Agencia Nacional de Licencias Ambientales, de una audiencia pública de seguimiento a Hidrotituango que contemple las afectaciones ambientales por parte de EPM. Especialmente los daños del bosque seco tropical, los animales y la contaminación del agua.

![Movimiento Ríos Vivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/C6j5GrZWgAE5fd4.jpg){.wp-image-37598 .aligncenter width="693" height="312"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
