Title: Inglaterra polarizada tras elección de Theresa May como primer ministra
Date: 2017-06-09 18:04
Category: El mundo
Tags: Corbyn, Theresa May
Slug: inglaterra-polarizada-tras-eleccion-de-theresa-may-como-primer-ministra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/theresa-may-14-e1497049433821.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Poniente] 

###### [09 Jun 2017]

En los comicios electorales del pasado jueves en el Reino Unido dejaron como resultado que el Partido Conservador, del que hacer parte May, perdió la mayoría absoluta, debido a que tan solo **obtuvieron 318 escaños en el Parlamento, el partido Laborista obtuvo 261 y el Partido Liberal Demócratas obtuvo 12**. Es decir, no hay mayoría suficiente para gobernar.

Según algunos primeros análisis, jos comicios celebrados en Inglaterra revelaron la polarización que actualmente afronta el país. Theresa May retorna a su cargo de primer ministra con apenas **50 votos de diferencia con su mayor oposito Jeremy Corbyn, del Partido Laborista**.

Con esta división en el Parlamento, **May aseguró que buscará las alternativas para formar coaliciones en este escenario y ser poder** “Lo que el país necesita más que nunca es certeza. Y habiendo obtenido el mayor número de votos y el mayor número de escaños en las elecciones generales, es evidente que solo el partido Conservador y Unionista tiene la legitimidad y la capacidad de proporcionar esa certeza por mando de una mayoría en la Cámara de los Comunes”.

Del otro lado Corby, ha señalado que la mejor decisión que podría tomar May en estos momentos, es dimitir de su cargo al no conseguir la mayoría de los votos, sumado a ello con la cantidad de escaños que tiene el Partido Laborista, **las propuestas de Corby podrían tener mayor alcance en las votaciones del parlamento.**

Esta polarización en Inglaterra es producto de la crisis económica en la que actualmente se encuentra tras su salida de la Unión Europea, decisión que afecto la votación de May, del otro lado Corby y su agenda política, fue la que logró que aumentaran los escaños de su partido, propuestas enfocadas en los problemas económicos y sociales, como la jubilación y subir el salario mínimo, le garantizan ahora posibilidades reales de transformaciones para Inglaterra.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
