Title: Cuatro modificaciones a la JEP que preocupan a la Corte Penal Internacional
Date: 2017-10-20 17:42
Category: Judicial, Otra Mirada
Tags: acuerdo de paz, Corte Penal Internacional, cpi, Fatou Bensouda, JEP
Slug: cuatro-modificaciones-a-la-jep-que-preocupan-a-la-corte-penal-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/fatou-bensouda-new2-e1508539282211.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Journalists for Justice] 

###### [20 Oct 2017] 

En un documento de 22 páginas, la Corte Penal Internacional, envió un recurso de *Amicus Curiae* a la Corte Constitucional colombiana sobre la Jurisdicción Especial de Paz, el que señala **serias preocupaciones sobre 4 aspectos** que se habrían modificado en el acuerdo de fin del conflicto con las FARC. Esto dejaría abierta la posibilidad de que la CPI intervenga puesto que habría violaciones del Estatuto de Roma.

Los cuatro puntos sobre los que enfatiza la CPI están en el marco de la JEP que fue regulado por el acto legislativo 01 del 4 de abril de 2017 y la ley 1820 del 30 de diciembre de 2016 y son **la responsabilidad de mando, la definición de graves crímenes de guerra, la determinación de la participación activa o determinante en los crímenes y la restricción efectiva de libertades** y derechos.

### **La responsabilidad de mando** 

Sobre la responsabilidad de mando, el documento con fecha de 18 de octubre, señala que un tribunal que quiera aplicar el acto legislativo 01 “podría verse impotente para hacer cumplir el derecho internacional consuetudinario **frente a los jefes militares con autoridad de *facto*** pero no de *jure* si solamente puede aceptar como prueba del grado de mando exigido un nombramiento formal”.

Es decir, que se pondría en duda si esas investigaciones o procesos **“estarían viciados por la incapacidad o falta de disposición de llevarlos realmente a cabo”**. Además, agrega que la doctrina de responsabilidad de mando, está fundamentada en la noción de mando responsable “los jefes son responsables por sus subordinados y las acciones de éstos”.

En consecuencia, sobre este aspecto, la CPI señala que, la definición de responsabilidad de mando incluida en el acto legislativo 01, **se aparta del acto consuetudinario** y podría frustrar los esfuerzos de Colombia en el juzgamiento de los crímenes internacionales.

### **Graves crímenes de guerra** 

La Corte Penal Internacional observa que, el requisito de que una conducta sea sistemática, **abriría la posibilidad de que se den amnistías, indultos o renuncias a la persecución penal** a personas responsables por crímenes de guerra bajo la jurisdicción de esa Corte que no sean sistemáticos.

En ese aspecto, afirma que, hay responsabilidad sobre graves crímenes de guerra **aún sin sistematicidad** incluso hay responsabilidad cuando las conductas en esos crímenes “hayan sido de manera indirecta o a través de una omisión culpable”.

### **Participación determinante en los crímenes** 

En este aspecto, la Corte observa una **ambigüedad en la definición de los terceros determinantes** que podría significar una “interpretación excesivamente amplia de los criterios” lo cual sería incompatible con el derecho internacional consuetudinario y favorecer una amnistía generalizada para terceros responsables de crímenes de su jurisdicción “incluyendo individuos que podrían haber cumplido un rol decisivo por omisión”.

Las preocupaciones señaladas en cuanto a la participación determinante estarían en que los terceros, para ser juzgado, **debieron haber participado a través de acción y no de omisión** dado que la omisión, para la CPI, puede ser igualmente grave “esta limitación parecería irrazonable puesto que podría llevar a resultados no deseables”.

Además, cuestiona que se limita el carácter de determinante a una contribución eficaz y decisiva y es importante definir de manera correcta esas interpretaciones, porque para el derecho internacional **“basta con que una persona brinde asistencia práctica**, aliento o apoyo moral que tenga un efecto sustancial en la perpetración de los crímenes (...) basta con que se contribuya sustancialmente a los crímenes en forma directa o indirecta”.

Incluso el documento hace énfasis en este punto y ejemplifica, “si una empresa privada financia a un grupo armado involucrado en la comisión de los crímenes, **es irrelevante si el apoyo económico** estaba específicamente dirigido a la comisión de los crímenes”.

### **Restricción de libertades** 

Según la Corte, para que se entienda como válida una pena contemplada en la JEP **va a depender de la naturaleza** y los alcances de las medidas que combinadas podrían conformar una sanción. Sin embargo, es necesaria una implementación efectiva, un “sistema riguroso de verificación” y de que las actividades que no hacen parte de la sanción no frustren el objeto de la pena.

En conclusión, Fatou Beonsouda, asegura que la JEP, debe permitir que los individuos responsables por conductas que constituyan crímenes del Estatuto de Roma, **sean llamados a responder ante la justicia.**

Aquí la carta completa

[Amicus Curiae de CPI a Corte Constitucional](https://www.scribd.com/document/362162472/Amicus-Curiae-de-CPI-a-Corte-Constitucional#from_embed "View Amicus Curiae de CPI a Corte Constitucional on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_36249" class="scribd_iframe_embed" title="Amicus Curiae de CPI a Corte Constitucional" src="https://www.scribd.com/embeds/362162472/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-3aixt5WRqyvBDxqE6i5C&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
