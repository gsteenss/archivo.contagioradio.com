Title: Multinacionales habrían desplazado a productores artesanales de sal de la Guajira
Date: 2019-07-19 15:54
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Guajira, manaure, productores sal, Wayuu
Slug: multinacionales-habrian-desplazado-productores-artesanales-de-sal-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Guayuu-manaure-plaza-de-Bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto:@ONIC] 

Las comunidades de Patarroyo y Neiman de La Guajira se desplazaron a Bogotá y protestaron en la Plaza de Bolívar a araíz de las agresiones a las que fueron sometidos y que los y las obligaron a desplazarse de las charcas de las que obtienen la sal (Le puede interesar ["Parques eólicos ¿una nueva amenaza para los territorios Wayúu?"](https://archivo.contagioradio.com/parques-eolicos-una-nueva-amenaza-territorios-wayuu/))

Según José Luis Martínez, vocero de la comunidad, este producto es el sustento de dos mil familias y "la violencia perpetrada la semana pasada, contra los habitantes de la región entre los que se encontraban alrededor de  veinte adultos mayores, quince mujeres y veinte niños, fue ejercida por parte de la Fuerza Pública y por la Alcaldía Municipal de Manaure".

Según Martínez hasta el momento, la respuesta de la vicepresidencia, por medio de su vocero Ricardo Ariza, es aparentemente positiva, pues el funcionario recibió toda la información referente al caso. Aunque la comunidad ya manifestó sus exigencias aún no hay una respuesta efectiva por parte del Gobierno, por ello se mantendrán en Bogotá hasta que sus reclamos sean atendidos y sus derechos respetados.

Según el vocero indígena, una de las exigencias de las comunidades es la realización de un proceso de especialización y de concentración sobre la actividad de extracción, pues los habitantes de la región se sienten ajenos a la actividad de la empresa.

El vocero de la comunidad indígena expresó que "la Agencia Nacional de Minería permitió que las comunidades fueran retiradas de sus tierras y de las charcas". (Le puede interesar ["«Duque, atienda primero la crisis humanitaria en la Guajira» mujeres Wayúu"](https://archivo.contagioradio.com/guajira/))

La producción de sal en el territorio está en manos de Sama LTDA, desde el 2002, empresa que cedió los derechos de explotación a Big Group Salinas SAS, quienes no han cumplido con lo pactado en inversión e infraestructura para las comunidades.

"La comunidad está en completa vulnerabilidad, pues el ente territorial es socio de la concesión con un 24% en participación accionaria, además no entendemos cómo el alcalde al ver la calidad de vida de su pueblo permite esta clase de atropellos" afirma el vocero Wayúu.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

<iframe id="audio_38663180" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38663180_4_1.html?c1=ff6600"></iframe>
