Title: Beato Monseñor Romero dignifica los mártires en América Latina
Date: 2015-05-24 20:37
Category: El mundo, Movilización
Tags: Monseñor Romero, Movimientos Romero de América Latina, Oscar Arnulfo Romero, sicsal
Slug: beato-monsenor-romero-dignifica-los-martires-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Monseñor-romero-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: presidencia.gov.sv 

Este 23 de Mayo, El Salvador fue epicentro de la beatificación del Monseñor Oscar Arnulfo Romero, obispo del mismo país asesinado en el año 1980 por la estrategia paramilitar de las fuerzas armadas en ese momento. Su beatificación se llevó a cabo desde las 10:00 am, hora de El Salvador, la cual contará con la presencia desde Roma delegada por el Papa Francisco, del cardenal Angelo Amato.

Su beatificación, en un inicio pensada desde el año 1992, fue frenada por el Papa Juan Pablo II y en el año 2013 desbloqueada por el papa Francisco. En ese contexto y en el marco de la visita de Romero a una audiencia del Papa, durante la más cruenta represión a los campesinos y campesinas y los movimientos sociales, con el fin de buscar su apoyo y solidaridad el Papa anunció a Monseñor que se negaba a escucharlo. "¡No exagere, señor arzobispo!"(11 de mayo de 1979) (Testimonio de María López Vigil, San Salvador 1993).

Por consecuencia de su asesinato, miles de personas en el mundo se identificaron con el martirio de Monseñor Romero, convirtiéndolo en un  símbolo del compromiso que la Iglesia debe asumir con el pueblo empobrecido, siguiendo así los lineamientos de la teología de la liberación que promulga que es compromiso de la Iglesia estar al lado de los oprimidos y oprimidas.

Según diversos teólogos y estudiosos de las religiones en América Latina, esta beatificación representa  el triunfo de miles de personas que consideran el legado de Monseñor como histórico y la reivindicación de la memoria de miles de mártires en el continente.
