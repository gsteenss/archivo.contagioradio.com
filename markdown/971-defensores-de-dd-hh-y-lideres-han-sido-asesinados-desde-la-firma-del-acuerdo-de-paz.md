Title: 971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz
Date: 2020-07-17 16:03
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Agresiones contra defensores de DDHH, Asesinato de indígenas
Slug: 971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Indepaz-informen_mezcla.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/indepaz.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Captura-de-Pantalla-2020-07-16-a-las-4.11.37-p.-m..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Captura-de-Pantalla-2020-07-16-a-las-6.38.39-p.-m..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Captura-de-Pantalla-2020-07-16-a-las-6.46.50-p.-m..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Informe-Especial-asesinato-líderes-2016-2020-Leo.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El **Instituto de Estudios Para el Desarrollo de la Paz (Indepaz)** , junto con **Marcha Patriótica**, y la **Cumbre Agraria Campesina, Etnica y Popular** y presentaron, el informe, sobre las agresiones a defensores y líderes sociales desde la firma del Acuerdo de Paz. Este informe especial, reúne d**atos desde el 24 de noviembre del 2016 hasta el 15 de julio del 2020**, en una investigación basada en noticias, comunicados, y denuncias públicas, presentada por organizaciones, grupos sociales y personas defensoras de Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La primera cifra que presentan son las **971 personas líderes sociales y defensoras de Derechos Humanos asesinadas desde 2016**, y de las cuales 21 se registraron en el 2016; 208 en el 2017; 287 en el 2018; 253 en 2019 y 53 en el primer semestre del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregan, que durante el **Gobierno de [Iván Duque](https://archivo.contagioradio.com/colombia-acumula-mas-de-10-solicitudes-sin-aceptar-de-relatores-de-la-onu/), 553 personas defensoras y líderes han sido asesinados en Colombia**, mientras que durante lo que restaba de la presidencia de **Juan Manuel** **Santos**, de julio del 2016 al 7 de agosto del 2018, se registraron **459 asesinatos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respecto al registro del **2020, 166 homicidios** se han informado, destacando a enero como el mes con más casos, seguido de febrero, mayo y junio. En lo corrido del mes de julio ya se registran 9 casos .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al hacer un análisis en los territorios, señalan que de los **32 departamentos en 29 se reportaron casos**, y de los **1.123 municipios en 125 hay registros**. Sin duda el número más alto está en el departamento del **[Cauca](https://archivo.contagioradio.com/los-castigos-brutales-con-los-que-grupos-armados-instauran-su-propio-regimen-contra-el-covid-19/)**, en el que desde el 2016 se han presentado **226 casos**, con la cifra más alta en 2019, con 72. A este departamento le preceden [Antioquia](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/) (133), [Nariño](https://archivo.contagioradio.com/la-saga-del-elefante/) (84), [Valle del Cauca](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/) (74) y [Putumayo](https://www.justiciaypazcolombia.com/plantar-arboles-como-una-accion-politica-de-amor-por-el-territorio/) (60).

<!-- /wp:paragraph -->

<!-- wp:image {"id":86894,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Captura-de-Pantalla-2020-07-16-a-las-6.38.39-p.-m.-1024x509.png){.wp-image-86894}  

<figcaption>
En el estudio refleja que existe un patrón de agresiones en los mismos departamentos Durante los años estudiados en los que cauca y Antioquia siempre encabezan esta lista.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading -->

Los líderes y defensores más asesinados son los campesinos con 342 casos
------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las organizaciones también hicieron un estudio sobre el patrón de casos al interior de los diferentes sectores sociales, en los que indican que el número más alto de homicidios se encuentra entre el sector campesino, con 342 casos; **seguido por los indígenas con 250, organizaciones cívicas (124), y afrodescendientes (71).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, l**a tasa más alta de homicidios se encuentra en el sector rural, con 261** casos desde el 2016, con un pico en el 2018 (209 casos), mientras que en las zonas urbanas se registraron 290, con una similitud de 89 casos en el 2018 y 2019.

<!-- /wp:paragraph -->

<!-- wp:image {"id":86895,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Captura-de-Pantalla-2020-07-16-a-las-6.46.50-p.-m.-1024x422.png){.wp-image-86895}  

<figcaption>
Casos registrados hacia sectores sociales, en donde el más alto es hacia las comunidades indigenas.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

También señalaron que l**os presuntos responsables de estos homicidios hacia los defensores y líderes, son autores desconocidos o sicarios**, seguido por narcoparamilitares, disidencias de las FARC y el ELN, resaltando que el 10% en el 2017 y el 4% de los casos de 2019, son responsabilidad de integrantes de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 211 asesinatos en 21 departamentos de excombatientes de Farc

<!-- /wp:heading -->

<!-- wp:paragraph -->

El informe reconoce el aumento de agresiones hacia los y las firmantes de paz, en el que se registra desde la **firma del Acuerdo de Paz un número de 211 excombatientes asesinados**, con las cifras más altas en los departamento del Cauca (37); Nariño (25); y Antioquia (24). A estas cifras añaden que **16 exguerrilleros han sido desaparecidos forzadamente en Colombia**, una cifra que se registrada hasta el 21 de febrero del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma la violencia sociopolítica d**urante el gobierno de Duque, con 11 homicidios** en medio de enfrentamientos, 12 desapariciones forzadas a excombatientes o sus familiares; 45 tentativas de homicidios, y un total de **138 casos fatidicos corresponden a firmantes de paz** y 20 de algún miembro de su núcleo familiar.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Hay que ir más allá de las cifras
---------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Leonardo González, coordinador de proyectos de Indepaz,** y autor de este informe, señaló que antes de ver el documento, se debe tener en cuenta el contexto social que vive en los territorios, hechos que no se registran simplemente en los últimos meses, sino que corresponden a a**ctos de violencia históricos en el país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Cuando se lee un informe sobre asesinatos de líderes sociales debe preguntarse **¿qué está pasando?, ¿por qué los están asesinando?,** ¿cuáles son los intereses?, ¿cuál es la historia de la comunidad que vive ahí?*, señaló González, y agregó que este trabajo ayuda ver un contexto, junto a soluciones y recomendaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recomendaciones que el analista dividió en dos, corto y largo plazo. Iniciando con el fortalecimiento de los **sistemas de autoprotección, reflejado en la Guardia Indígena,Cimarrona y Campesina,** *"presentes en diferentes territorios, esto sumado al cumplimiento de Acuerdo de Paz en torno a los pactos de no violencia".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Asesinan porque es mucho más fácil acabar con la vida de una persona que, discutir con ella, tratar de convencer o negociar, acción a la que se suma la estigmatización en muchos casos".*
>
> <cite>**Leonardo González |Coordinador de proyectos de Indepaz**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por último, señaló que se debe hacer un análisis más profundo de lo que es el narcoparamilitarismo, con sus diferentes componentes políticos y económicos, no solamente desde el enfoque armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***Entrevista: Leonardo González , Coordinador de proyectos Indepaz***

<!-- /wp:paragraph -->

<!-- wp:audio {"id":86869} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Indepaz-informen_mezcla.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:file {"id":86896,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Informe-Especial-asesinato-líderes-2016-2020-Leo.pdf"} -->

<div class="wp-block-file">

[Informe-Especial-asesinato-líderes-2016-2020-Leo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Informe-Especial-asesinato-líderes-2016-2020-Leo.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Informe-Especial-asesinato-líderes-2016-2020-Leo.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:block {"ref":78955} /-->
