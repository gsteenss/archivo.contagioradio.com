Title: Fue asesinado Mario Chilhueso, líder campesino del Naya
Date: 2020-04-19 17:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: asesinato, Cauca, lide, Lider social, Mario Chilhueso
Slug: fue-asesinado-mario-chilhueso-lider-campesino-del-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/naya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: JusticiayPaz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 19 de abril cerca de las 5:00 de la mañana, fue asesinado el líder campesino de 45 años, Mario Chilhueso, cuando iba saliendo de su casa en la vereda Los Robles de Buenos Aires , Cauca

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Mario Chilhueso** Cruz, nació el 20 de julio de 1975 en Miranda cauca, y se desempeñaba como Presidente de la asociación de trabajadores y pequeños productores de Naya (ASTCAP).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento se desconoce la razón y los responsables de su asesinato; sin embargo la **Comisión Colombiana de Jurista**s (CCJ), rechazó exigió la **protección de los líderes y las comunidades en medio de la cuarentena**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Coljuristas/status/1251928689446707207","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Coljuristas/status/1251928689446707207

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El líder campesino **fue víctima de la [masacre de El Naya](https://www.justiciaypazcolombia.com/masacre-del-naya/) en abril de 2001**, la cual ocurrió del 9 al 14 de abril ; y en la que se registró el ingreso al territorio de 60 paramilitares de las AUC bloque Calima con ayuda de miembros de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el Naya, según la comunidad fueron masacrando más de 46 campesinos, indígenas y afrodescendientes, mientras que en la versión entregada por el ex jefe paramilitar alias HH, quién lideró esta incursión fueron 24, por su parte el Ejército solo reconoció 20 víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con Marío Chilhueso, este fin de semana suma un lamentable número de tres líderes sociales y una persona en proceso de reincorporación, asesinados en medio de un Estado de Emergencia y cuarentena obligatoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: <https://archivo.contagioradio.com/mision-humanitaria-verificara-crisis-de-derechos-humanos-en-el-naya/>)

<!-- /wp:paragraph -->
