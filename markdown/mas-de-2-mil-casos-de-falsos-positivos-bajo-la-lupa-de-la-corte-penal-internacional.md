Title: Corte Penal Internacional tiene información sobre 2.047civiles y empresarios vinculados al paramilitarismo
Date: 2019-12-06 12:36
Author: CtgAdm
Category: DDHH, Judicial
Tags: Corte Penal Internacional, falsos positivos, JEP, paramilitares
Slug: mas-de-2-mil-casos-de-falsos-positivos-bajo-la-lupa-de-la-corte-penal-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cour-Penale-Internacionale-e1575589878337.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Foto: @IntlCrimCourt] 

 

En su informe preliminar sobre la situación de Colombia para el año 2019, la **Corte Penal Internacional (CPI)** señaló que las autoridades colombianas parecen haber progresado en el sentido de cumplir con su deber de investigar y juzgar las conductas que constituyen crímenes de guerra y crímenes de lesa humanidad según el Estatuto de Roma. La Corte hizo especial énfasis en la investigación sobre la creación y promoción de los grupos paramilitares, así como en lo que ha sido, en general, el trabajo de la Jurisdicción Especial para la Paz (JEP).

### **Grupos paramilitares y el trabajo de la JEP** 

La CPI inició el examen preliminar sobre la situación de Colombia desde junio de 2004, y en este informe se estudia lo ocurrido en el país en materia de justicia, investigación y avance de casos de violación a los derechos humanos, entre diciembre de 2018 y el 30 de noviembre el presente año. A lo largo del informe, la Corte hace un recuento con especial énfasis en el trabajo de la JEP, así como el avance en casos de los, mal llamados, 'falsos positivos' y respecto a la acción de grupos paramilitares.

Sobre la Jurisdicción, el informe señala que hasta noviembre de 2019, **9.713 miembros de las FARC, 2.291 de las Fuerzas Armadas y 63 'terceros', o no agentes del Estado, han firmado actas de sometimiento.** Asimismo, que este mecanismo de justicia abrió 7 macro casos referidos a delitos representativos ocurridos en el marco del conflicto armado; en ese sentido, destacó los casos 002, 004 y 005 sobre la violencia ocurrida en Nariño, Urabá y Cauca, respectivamente.

De igual forma, el informe rescata los sometimientos de **David Char Navas y Álvaro Ashton ante la Jurisdicción,** para saber la verdad sobre la relación de políticos con grupos paramilitares en la zona norte de Colombia. (Le puede interesar: ["Sí hay voluntad de los terceros para decir la verdad sobre la guerra"](https://archivo.contagioradio.com/voluntad-terceros-verdad-guerra/))

Respecto a los procesos por paramilitarismo que lleva la justicia ordinaria, la Corte señaló que hasta octubre de este año, la Fiscalía General de la Nación lleva un total de 1.253 casos contra civiles o empresarios, y 794 contra agentes de Estado, por crímenes relacionados con la promoción, apoyo o financiación de estos grupos. (Le puede interesar: ["¿Impunidad para los terceros civiles que participaron en la guerra?"](https://archivo.contagioradio.com/impunidad-para-los-terceros-civiles-que-participaron-en-la-guerra/))

En el apartado sobre avance de procesos, se menciona el proceso contra exejecutivos y empleados de la multinacional **Chiquita Brands,** acusados en 2018 por concierto para delinquir por la financiación del Frente Arlex Hurtado. También hizo referencia a los procesos relacionados con el Bloque Calima, por el que se pidió orden de prisión preventiva para 2 personas, e indagatoría contra otras 3.(Le puede interesar: ["La relación entre Chiquita Brands, Álvaro Uribe y las Convivir"](https://archivo.contagioradio.com/la-relacion-entre-chiquita-brands-alvaro-uribe-y-las-convivir/))

### **Corte Penal Internacional tiene conocimiento de más de 10mil  personas investigadas por falsos positivos**

Sobre los casos de falsos positivos, el mecanismo internacional referenció que la justicia ordinaria investiga **5 casos potenciales:** De la Primera (brigada 10), Segunda (Brigada 30 y 15 móvil), Cuarta (brigadas 7, 16 y 28), Quinta (brigada 9) y Séptima (brigadas 4, 11 y 14)  división del Ejército. A pesar de que organizaciones defensoras de derechos humanos señalan que podrían haber hasta 10 mil víctimas de ejecuciones presentadas como bajas en combate, la Fiscalía reportó a la Corte 2.268 casos activos que involucran a 3.876 víctimas.

Adicionalmente, según la información entregada a la Corte Penal, 10.742 personas han sido investigadas por los hechos, mientras que solo 1.740 han sido condenadas. Respecto a los casos de las 5 divisiones mencionadas con casos ocurridos en los departamentos de Cesar, Norte de Santander, Magdalena, Meta, Casanare, Vichada, Huila, Antioquia y Córdoba, la CPI informó que **un total de 20 generales, 182 coroneles y 143 comandantes están siendo investigados.**

### **"Las autoridades colombianas parecen haber progresado hacia el cumplimiento de su deber de investigar y juzgar"** 

En la conclusión, el Tribunal Internacional relató que las "autoridades colombianas parecen haber progresado hacia el cumplimieento de su deber de investigar y juzgar conductas que constituyen crímenes de guerra y crímenes contra la humanidad según el Estatuto de Roma". No obstante, también señaló que continuará evaluando los procesos que se llevan en las tres jurisdicciones: La justicia ordinaria, la Ley de Justicia y Paz y la JEP; de igual forma, la oficina adelantó que el próximo año adelantará la revisión de puntos de referencia relevantes que podrían permitirle culminar su evaluación preliminar sobre Colombia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
