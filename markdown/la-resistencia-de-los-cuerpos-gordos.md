Title: La resistencia de los cuerpos gordos
Date: 2017-08-04 16:58
Category: Libertades Sonoras, Mujer
Tags: feminismo, Feminismo gordo, Gordos, Gordura, resistencia
Slug: la-resistencia-de-los-cuerpos-gordos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Gordas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Faebook Gordas Sin Chaqueta] 

###### [02 Ago. 2017] 

En los últimos años y en diversas partes del mundo, mujeres gordas agrupadas en colectivas, desafían al sistema desde una postura política del cuerpo. **El activismo y la resistencia de los cuerpos gordos es una puerta para empezar a pensar la corporalidad** disidente, cuestionar la patologización de los cuerpos, reflexionar sobre qué es lo que define lo “normal” y eliminar los estereotipos e imaginarios que sustentan un régimen corporal que discrimina y violenta las corporalidades que se salen de la norma y el estándar.

Escucha en este **\#LibertadesSonoras a **[ **Marcela Salas, Lu Robles y Ángela Quiceno (Yela Quim), integrantes de la Colectiva Gordas Sin Chaqueta** que reivindica la gordura como un acto político, libremente elegido y construido, que resiste a un sistema machista y patriarcal.]

Además podrás escuchar cómo** Lucrecia Masson - Feminista, activista, antiracista,** sudaka, migrante en movimiente, habla como gorda de lo convencida de la diversidad corporal como apuesta política.

¿Qué opinas de la gordura?¿Crees que es posible reivindicar la gordura y así resistir al sistema?¿Sabes qué es la gordofobia?¿Crees que la gordura es un problema de salud pública?. Danos tu opinión en Facebook (Contagio Radio) o en Twitter @contagioadio1.

<iframe id="audio_20174054" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20174054_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
