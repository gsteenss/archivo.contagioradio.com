Title: El drama de la desaparición forzada desde las Tablas
Date: 2016-08-10 12:15
Category: Cultura, eventos
Tags: De ausencias, Desaparición forzada Colombia, teatro en bogota, Teatro Quimera
Slug: el-drama-de-la-desaparicion-forzada-desde-las-tablas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/681b02d4-b6b4-4852-8f12-b689c93a0155-e1470849090861.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Angela Pinzón 

###### 10 Agos 2016 

La desaparición forzada en Colombia parte de una realidad que toca sin distinción a todos los sectores de la población nacional, un tema que ha sido abordado desde diferentes perspectivas incluyendo la artística; ejemplo de ello es "**De Ausencias**" una puesta en escena del **Teatro Quimera**, que se presenta en temporada del 11 al 27 de Agosto.

La carga dramática que supone vivir la experiencia de la desaparición, aporta a la construcción del relato, al que se integran temas como la violencia, la descomposición social y la miseria, que parten de agentes al margen y en la ley, paramilitarismo, guerrilla, violencia de estado, limpieza social, narcotráfico y el secuestro como empresa rentable.

[![Teatro Quimera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/e674bbf1-ab51-4a84-b6f6-f1d6cf9da123-e1470848048506.jpg){.aligncenter .wp-image-27668 width="590" height="331"}](https://archivo.contagioradio.com/el-drama-de-la-desaparicion-forzada-desde-las-tablas/e674bbf1-ab51-4a84-b6f6-f1d6cf9da123/)

El montaje de la obra se desarrolla en dos planos: el “real” presente en cada momento; y un plano “posible”. El primero alude a quienes viven alguna desaparición; quienes deben afrontar la perdida, esperar o buscar en “los ausentes”; y el segundo, el de los ausentes, los desaparecidos, los muertos o los posibles muertos, por lo que se nos convierte en una suerte de espacio mental, fantasmagórico, en algunos casos metafórico; onírico y de evasión, tal vez un refugio. Esta división es el eje de la puesta en escena, a ella obedecen el ordenamiento y la disposición formal de la escena, lo espacial, lo lumínico, lo sonoro y los personajes.

"De Ausencias" dirigida por el maestro **Fernando Ospina**, se presentará en funciones de jueves a domingo a las 7:30 de la noche, en el Teatro Quimera, Sala Concerada IDARTES, ubicada en la calle **70A N° 19-40.** Para más información y reservas al 2179240 y 3158298723.
