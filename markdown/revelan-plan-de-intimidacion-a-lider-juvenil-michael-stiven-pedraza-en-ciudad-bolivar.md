Title: Revelan plan de intimidación a líder juvenil Michael Stiven Pedraza en Ciudad Bolívar
Date: 2019-11-08 18:46
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: amenaza, Bogotá, ciudad bolivar, congreso de los pueblos
Slug: revelan-plan-de-intimidacion-a-lider-juvenil-michael-stiven-pedraza-en-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ciudad-bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  
] 

El Congreso de los Pueblos y Ciudad en Movimiento denunciaron que el pasado sábado 2 de noviembre, **Michael Stiven Pedraza, integrante de ambas colectividades,** fue abordado por dos sujetos que dijeron ser integrantes de la Fiscalía y lo señalaron de participar en actividades ilegales en Ciudad Bolívar. Los hombres le mostraron fotografías de seguimientos a su familia, y le dijeron que solo querían ayudarlo porque había una supuesta orden de captura, adicionalmente, las organizaciones denunciaron que los entes encargados de investigar el caso no activaron los mecanismos necesarios para protegerlo de la amenaza.

### **Seguimientos al líder Stiven Pedraza en Ciudad Bolívar  
** 

En entrevista con Contagio Radio, Michael Pedraza explicó que es profesor de la escuela de fútbol popular "Montañeros" en el barrio Perdomo. El pasado sábado 2 de noviembre, luego de salir de su casa en la localidad de Ciudad Bolívar para la escuela, un hombre lo llamó por su nombre, y pensando que se trataba de un papá de la Escuela decidió atenderlo, pero el hombre le dijo que lo dejaría con un funcionario de la Fiscalía, que **se presentó como Edwin pero no aportó ninguna identificación.**

Edwin le dijo que quería ayudarle porque sabía que Stiven es un buen muchacho y le afirma que está implicado en un caso, que hay una orden de captura en su contra y **luego le enseña fotos de él saliendo de su casa, de sus familiares y del proceso de la Escuela de Fútbol.** Ante el desconcierto de Stiven, le dice que alguien lo quiere culpar por participar en casos de atentados y le muestra algunas fotos, de las que el profesor de la escuela solo reconoció algunas de la Escuela General Santander.

Ante el temor de Stiven, que decide moverse del lugar en el que fue abordado, el hombre le dice que le enviará un mensaje por WhatsApp para que sigan hablando, y se vuelvan a encontrar. Stiven dijo que ha recibido llamadas de números desconocidos e invitaciones por mensajes para otros encuentros, **pero ha decido no asistir previendo riesgos a su seguridad.** (Le puede interesar:["Con panfletos amenazantes advierten «toque de queda» en Ciudad Bolívar"](https://archivo.contagioradio.com/panfletos-amenazantes-ciudad-bolivar/))

### **Los organismos de investigación le recomendaron 'seguirle el juego' a quienes lo amedrentaron** 

Ante el temor por la situación, Stiven Pedraza acudió a Ciudad en Movimiento y Congreso de los Pueblos, quienes le recomendaron acudir a las autoridades para denunciar el caso, ellos lo acompañaron al cuerpo élite de la Policía, pero el profesor sostuvo que el subintendente que tomó nota del caso **le dijo que aceptara la propuesta de encontrarse de nuevo y que llamara a la Policía si algo nuevo llegase a ocurrir.** (Le puede interesar: ["Águilas Negras amenazan a líderes sociales en Ciudad Bolívar"](https://archivo.contagioradio.com/aguilas-negras-amenaza-ciudad-bolivar/))

Ante la respuesta, tanto el Congreso de los Pueblos como Ciudad en Movimiento cuestionaron que la respuesta oficial sea pedir a Stiven que se exponga, desconociendo el contexto violento que vive el país para los y las líderes sociales. En ese sentido, pidieron que se protegiera la vida del Profesor, así como que se tomen acciones preventivas para cuidar la vida de las personas que habitan el territorio colombiano. (Le puede interesar:["Vida de jóvenes de Ciudad Bolívar amenazada por control de bandas criminales"](https://archivo.contagioradio.com/vida-de-jovenes-de-ciudad-bolivar-amenazada-por-control-de-bandas-criminales/))

[Denuncia de amenazas a líder juvenil en Ciudad Bolívar](https://www.scribd.com/document/434081744/Denuncia-de-amenazas-a-lider-juvenil-en-Ciudad-Bolivar#from_embed "View Denuncia de amenazas a líder juvenil en Ciudad Bolívar on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_29519" class="scribd_iframe_embed" title="Denuncia de amenazas a líder juvenil en Ciudad Bolívar" src="https://es.scribd.com/embeds/434081744/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-uuU9UrZmEycc9bNn5ZHZ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44213725" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44213725_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
