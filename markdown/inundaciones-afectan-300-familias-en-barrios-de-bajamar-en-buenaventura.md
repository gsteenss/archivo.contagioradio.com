Title: Inundaciones afectan 300 familias en barrios de bajamar en Buenaventura
Date: 2015-10-02 14:01
Category: Ambiente, Nacional
Tags: Aumento de Marea Buenaventura, buenaventura, Gobierno Nacional, Pescadores, puente nayero, Zona humanitaria
Slug: inundaciones-afectan-300-familias-en-barrios-de-bajamar-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/puerto-buenaventura-colombia-15c1d797-c3a4-4f31-ae0e-1db28969d52c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:las2orillas.com 

###### [1 oct 2015]

En Buenaventura y en el espacio humanitario de Puente Nayero, las comunidades denuncian que debido al aumento de la marea se han visto afectadas **alrededor de 300 familias y  diez mil pescadores**, que además  han visto afectadas sus viviendas y su forma de trabajo y hasta ahora el gobierno municipal, ni departamental, ni nacional han hecho presencia en la zona.

Hilario Reina, pescador de Puente Nayero, indicó que allí son 20 las familias afectadas con el **aumento de la marea de 5.5 mts**, lo cual “*nunca se había visto en Buenaventura*”, por lo cual "*ninguna de las familias estaba preparada para afrontar esta situación*", por esto la comunidad **ha sufrido pérdidas y daños en sus enceres**, como neveras, estufas, televisores, muebles, además de implementos de trabajo de los pescadores que han sido arrastrado por las corrientes.

Reina, indica que los únicos que se han presentado en Puente Nayero son los bomberos y la defensa civil quienes solo “*alertan al pueblo*” de que esta situación continuará, Hilario asegura que “*la administración municipal nunca se ha hecho presente en el espacio humanitario*” y que después que **este fenómeno afecte a las comunidades de Buenaventura no habrá “***quien nos responda a nosotros***”**.

Las comunidades de Buenaventura y Puente Nayero, exigen al gobierno nacional que “*haya una entidad que ayude a los perjudicados*”, ya que situaciones como esta *“es una cosa que no se espera, que viene desde muchos años".*
