Title: Municipio de Tabio marcha este domingo en defensa de reserva forestal
Date: 2015-09-12 13:30
Category: Ambiente, DDHH
Tags: Alexander Fonseca, ANLA, CAR, Chivor II, DDHH, Empresa de Energía de Bogotá, medio ambiente, Minambiente, Subachoque, Tabio, Zipaquirá
Slug: municipio-de-tabio-marchara-este-domingo-en-defensa-de-reserva-forestal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/foto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: noticiascolombianas.com 

<iframe src="http://www.ivoox.com/player_ek_8617938_2_1.html?data=mZuemZ6XfI6ZmKiak5yJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMLWytSY1cqPsdDqytHW3MaPtMLmwpDRx8vJssXZ05Dg15DYqdPmytnc1M7TcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [11 sep 2015] 

Comunidades de los municipios de Tabio, Zipaquirá y Subachoque convocan a una **marcha pacífica este domingo 13 de septiembre en rechazo al proyecto de energía Chivor II**, que sería ejecutado sobre la reserva forestal de Tabio, Cundinamarca. Esta reserva está compuesta por especies endémicas de flora y fauna; algunas de ellas se hallarían en vía de extinción.

El proyecto Chivor II pertenece a la Empresa de Energía de Bogotá, EEB, y consistiría en **construir 260 torres de energía en un perímetro de 8 kilómetros del municipio de Tabio**; cada torre a 30 metros de distancia. Vea también: [Empresa de energía planea construir tendido de torres en plena reserva forestal de Tabio](https://archivo.contagioradio.com/empresa-de-energia-planea-construir-tendido-de-torres-en-plena-reserva-forestal-de-tabio/)

La marcha en la que participarán habitantes de los tres municipio se llevará a cabo este **13 de septiembre, a partir de las 9;00 a.m en el Foro municipal** como punto de encuentro, y se dirigirán hacia el Alto de Canica.  "Será una marcha pacífica, por lo que no obstruirá el fluo vehicular", señala Alexander Fonseca, quien agrega que ésta sería la **última esperanza para que el gobierno departamental los escuche** y preste suma atención a las denuncias de los pobladores sobre los **daños socio ambientales que podría ocasionar la construcción** de las líneas de alta tensión en esta región del país.

La presencia de EEB en la reserva forestal, además de causar impactos económicos como la disminución del turismo, la subida de costos del agua y la migración de los habitantes a otros sectores, **perjudicaría la permanencia de los acueductos** **que abastecen de agua al municipio**, **transformaría el paisaje, y afectaría fuertemente la salud de sus habitantes.** Vea también: [Comunidad de Tabio se moviliza para salvar su reserva forestal](https://archivo.contagioradio.com/comunidad-de-tabio-se-moviliza-para-defender-su-reserva-forestal/).
