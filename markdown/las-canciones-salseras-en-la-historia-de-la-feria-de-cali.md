Title: Las canciones salseras en la historia de la Feria de Cali
Date: 2016-07-15 15:49
Category: En clave de son
Tags: Feria de Cali, Salsa colombiana
Slug: las-canciones-salseras-en-la-historia-de-la-feria-de-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/feria-cali.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Desde 1957, la Feria de Cali se ha consolidado como uno de los eventos populares más importantes de la agenda colombiana. Tan solo un año después de su creación se instauró una de las tradiciones que, hasta la fecha captura la atención de los asistentes y de la industria musical: La elección del disco de la Feria.

La alternancia entre canciones tropicales, el popular "chucu-chucu", y composiciones de orquestas salseras, dominó los primeros años de la Feria con artistas como Los Corraleros del Majagual, La Billos Caracas Boys, Fruko y sus Tesos, Grupo Niche y Guayacán orquesta entre muchos otros.

Durante los últimos años, las tendencias musicales que se imponen en el mercado han influenciado fuertemente la elección de los discos de cada edición, por lo que se han incluído propuestas que fusionan ritmos urbanos con los sonidos tradicionales, convirtiéndose en un tema controversial entre diferentes generaciones.

En este especial de 'En Clave de son' hacemos un repaso por los éxitos que pusieron a bailar a los fanáticos durante las festividades de la caña, particularmente en aquellas canciones salseras que se llevaron el reconocimiento del público y que aun retumban en las pistas de baile.

<iframe src="http://co.ivoox.com/es/player_ej_12237392_2_1.html?data=kpeflZyXfZOhhpywj5WWaZS1k5uSlaaVeo6ZmKialJKJe6ShkZKSmaiRdI6ZmKiartTXb8Xd1Mjc1ZDXpc3nxtfc1ZDIqYzgwpCzx9fNpYzYxpCww9HNcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
