Title: 4.876 indígenas han sido víctimas de violación de DDHH en 2015
Date: 2015-12-11 19:43
Category: DDHH, Nacional
Tags: Asesinatos de indígenas en Colombia, ONIC, Violación de DDHH a indígenas en Colombia, Violación de DDHH durante 2015
Slug: 4-876-indigenas-ha-sido-victimas-de-violacion-de-ddhh-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Violación-DDHH-a-indígenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

###### [11 Dic 2015 ]

[De acuerdo con el último informe de la Organización Nacional Indígena de Colombia ONIC, a 4.876 indígenas les han sido vulnerados sistemáticamente sus derechos humanos, en lo que va corrido de 2015.]

[Entre las acciones violatorias se registran **asesinatos, amenazas colectivas, actos de persecución y desaparición forzada** padecidos por los 122 pueblos originarios existentes actualmente en Colombia.]

[La situación es compleja en todo el territorio nacional, pero se agrava principalmente en los departamentos de Chocó, Valle del Cauca, Nariño y Cauca en los que la dinámica del conflicto se ha agudizado por el **reagrupamiento de bandas criminales y estructuras paramilitares**. 3.481 [indígenas](https://archivo.contagioradio.com/?s=indigenas) han sido víctimas de desplazamiento forzado y en muchas de las regiones que habitan continúa el despojo de tierras. ]

[Esta situación de violación de derechos humanos sumada a un **histórico abandono estatal, pone en riesgo a las comunidades indígenas** quienes exigen al Gobierno que se preocupe por revisar el conflicto armado no sólo en las ciudades, sino que haga una verificación en las poblaciones en las que **persisten sus reclamaciones de justicia y de mecanismos que garanticen su superveniencia**.   ]

 
