Title: Lo que esta en juego con las elecciones presidenciales 2018
Date: 2018-06-15 16:45
Category: Nacional, Política
Tags: acuerdos de paz, elecciones 2018, ELN, Gustavo Petro, Iván Duque
Slug: lo-que-esta-en-juego-con-las-elecciones-presidenciales-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Elecciones-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [15 Jun 2018]

Finaliza la última semana de campaña electoral a la presidencia de Colombia, y para el director de la organización Podion, Jaime Díaz, **la decisión que tomen los colombianos no solo será crucial, sino que también definiría el futuro de temas tan importantes** como la implementación de los Acuerdos de paz, la continuidad del proceso de diálogo con el ELN, la reforma agraria, entre otros.

### **El retorno de la guerra**

Uno de los mayores temores que ha evidenciado esta carrera electoral, tiene que ver con el retorno del conflicto armado en Colombia, que ha tenido una fuerte disminución en las cifras de las víctimas luego de la firma de los Acuerdos de paz. (Le puede interesar: ["Quieren limitar los poderes de la JEP: Iván Cepeda"](https://archivo.contagioradio.com/quieren-limitar-los-poderes-de-la-jep-ivan-cepeda/))

En ese sentido para Díaz si a Colombia llega un “presidente guerrerista” pueden generarse escenarios adversos para la paz. **Uno de ellos es que integrantes de FARC, regresen a engrosar las filas del ELN** o que el proceso de paz que se adelanta con la guerrilla se frené. Iván Duque candidato a la presidencia, ya manifestó su interés por levantar el proceso de diálogos que se adelanta en La Habana, de ganar la presidencia de Colombia.

Asimismo, el analista señaló que otro de los puntos relacionados con el conflicto armado que están en vilo, es la implementación de los Acuerdos de ellos, específicamente en el tema de tierras, en donde de acuerdo con Díaz, Duque propone **“que aquellos que se tomaron las tierras de los campesinos y desplazados, les sean devueltas** porque son poseedores de buena fe”, luego haberla comprado a testaferros o adquirirlas a través de “manos dolosas”.

La Jurisdicción Especial para la paz y la Comisión para Búsqueda de la verdad también estarían en riesgo, según Díaz, es necesario que están continúen gane quien gane las elecciones debido a que **“las víctimas tienen derecho a saber qué fue lo que pasó en el país y por qué la guerra”** y en esa misma vía conocer quiénes la financiaron y estuvieron detrás de ella.

### **La mesa de conversaciones gobierno-ELN** 

Referente a la mesa de diálogos en La Habana, entre el ELN y el gobierno Nacional, Díaz resaltó que ha existido la posibilidad en este proceso de que las comunidades y víctimas participen en lo que se está debatiendo actualmente, razón por la cual manifestó que sería **“lamentable” acabar con este proceso y regresar a la guerra.**

En esa vía expresó que si bien Duque no ha afirmado que acabara con las negociaciones, las condiciones que colocaría a este espacio de llegar a ser presidente si provocaría una disolución de la mesa. (Le puede interesar: ["Cese bilateral será el tema central del sexto ciclo de conversaciones"](https://archivo.contagioradio.com/cese-bilateral-sera-tema-central-en-el-sexto-ciclo-de-conversaciones/))

### **El ambiente y la tierra en alerta roja**

De acuerdo con el analista, las múltiples afectaciones a los ecosistemas y el afán por adquirir tierra en el país, son elementos que deben revisarse en las propuestas de ambos candidatos, “las propuestas de Duque, que estuvo hace 10 días en Leticia, prometió carreteras por el Amazonas. **Sería la conquista de la Amazonía para la colonización y la posterior entrega de esa tierra a los grandes terratenientes**”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
