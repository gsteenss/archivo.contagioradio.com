Title: Crónica de una tragedia anunciada: joven es víctima de mina en Ituango
Date: 2019-04-26 17:52
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Antioquia, cordoba, desplazados, Mina
Slug: joven-victima-mina-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/minas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: imperiocrucial.com] 

La Asociación Campesina del Sur de Córdoba denunció este viernes que **un joven de 16 años pisó una mina antipersonal y tiene en riesgo de perder una de sus piernas cerca de la quebrada Zabaleta de la vereda Flechas**, municipio de Ituango (Antioquia); sobre este territorio diferentes organizaciones sociales han advertido la presencia de este tipo de artefactos, así como de actores armados que operan sin control alguno de la Fuerza Pública. Adicionalmente,  la Gobernadora de Córdoba y el Comandante  de la Séptima División del Ejército, tendrían parte de responsabilidad en el incidente.

**Andrés Chica, integrante de la Fundación Social Cordoberxia**, recordó que a finales de marzo la familia del joven fue desplazada de Las Flechas por Los Caparrapos, grupo armado que hace presencia en la parte alta del Nudo de Paramillo; en su momento se registró el desplazamiento forzado de ocho veredas de Ituango hacía al corregimiento de Juan José, municipio de Puerto Libertador (Córdoba). Posteriormente, la gobernadora Sandra Devia Ruiz y el general Juan Carlos Ramírez fueron a dicho corregimiento, prometieron ayudas y aseguraron que el territorio cuenta con garantías de seguridad para el retorno.

### **Crónica de una tragedia anunciada** 

Desde finales de 2017 y durante 2018, por lo menos 3 estructuras armadas ilegales se disputan el control del Parque Nacional Natural Paramillo; en consecuencia, se han producido desplazamientos, confinamiento de comunidades y siembra de minas como forma de control al territorio. Según Chica, del corregimiento de Santa Lucía y el Cedral de Ituango, se han desplazado más de 200 familias desde finales de 2018 producto de las actividades ilegales en la zona, así como por la erradicación forzada de cultivos. (Le puede interesar: ["Se libra una guerra por tomar el control del Nudo de Paramillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

En la última semana de marzo, y como parte de la guerra que se libra por la zona entre Los Caparrapos y las Autodefensas Gaitanistas de Colombia (AGC), 9 veredas de Ituango fueron desplazadas forzosamente hacía Córdoba; produciendo un éxodo de cerca de 700 familias en total. Posteriormente, de acuerdo a Chica, el 6 de abril visitaron el territorio la gobernadora de Córdoba Sandra Devia y el comandante de la Séptima División del Ejército, general Juan Carlos Ramírez, y dijeron a las familias que ya habían condiciones para su retorno.

Pero tales afirmaciones fueron falsas, y algunas familias que intentaron regresar a sus hogares fueron golpeados por los actores armados que permanecían en la zona, otra de las familias que confió en estas palabras fue la del joven que pisó la mina en la vereda Flechas. Incluso, Chica recalcó que la Gobernadora dijo que los desplazados en Juan José buscaban únicamente recibir las ayudas -que nunca envió la Gobernación- para atender a las más de 2 mil personas que tuvieron que huir de sus hogares.

### **El último llamado a la atención sobre el territorio se emitió el pasado 7 de abril** 

Para el integrante de Cordoberxia es inexplicable que no se hayan tomado medidas para solucionar la situación de un territorio sobre el que la Defensoría del Pueblo ha emitido diferentes alertas tempranas, la última de ellas el pasado 7 de abril; y en la que hay una desazón frente a la implementación del Acuerdo Final, una esperanza de una reforma rural integral que nunca llegó y la presencia de grupos armados que operan en el territorio sin que la fuerza pública actúe generando las condiciones de seguridad necesarias para los habitantes. (Le puede interesar: ["Al Bajo Cauca no llega la sustitución pero sí la extorsión y el desplazamiento"](https://archivo.contagioradio.com/al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento/))

[Denuncia Publica 084 Joven Desplazado Retornado Cae en Mina Antipersonal -](https://www.scribd.com/document/407752642/Denuncia-Publica-084-Joven-Desplazado-Retornado-Cae-en-Mina-Antipersonal#from_embed "View Denuncia Publica 084 Joven Desplazado Retornado Cae en Mina Antipersonal - on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_51555" class="scribd_iframe_embed" title="Denuncia Publica 084 Joven Desplazado Retornado Cae en Mina Antipersonal -" src="https://es.scribd.com/embeds/407752642/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pMsACqEAzv2gKElFBr5G&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.707221350078493"></iframe>

<iframe id="audio_34998701" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34998701_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
