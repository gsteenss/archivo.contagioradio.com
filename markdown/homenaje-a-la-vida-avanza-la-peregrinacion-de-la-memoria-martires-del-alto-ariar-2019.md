Title: Homenaje a la vida, avanza la Peregrinación de la Memoria: mártires del alto Ariari, 2019
Date: 2019-02-03 08:22
Author: AdminContagio
Category: Memoria, Sin Olvido
Tags: Castillo Meta, Jornada de Peregtinación
Slug: homenaje-a-la-vida-avanza-la-peregrinacion-de-la-memoria-martires-del-alto-ariar-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-02-at-10.25.04-AM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 2 Feb 2019 

Desde 2 al 7 de febrero, el Comité de memoria y veeduría a las acciones de Reparación Integral de las víctimas del municipio El Castillo, Meta junto a caminantes de todo el país se congregarán para recorrer más de 70 kilómetros de las carreteras y rutas del Meta para honrar la memoria de las víctimas de  [una región que durante más de cinco décadas ha sido escenario del conflicto armado  y social que ha vivido el país.]

[En particular los habitante de El Castillo, **han sido enfáticos en el  realizar procesos de Memoria, incluso en medio de la guerra, prueba de ello son las conmemoraciones, vigilias, peregrinaciones, marchas, que se han realizado desde el año 2000**  en las veredas de la Esmeralda, Caño Sibao, en el centro poblado de Medellín del Ariari, y Puerto Esperanza.]

[Una de estas tradiciones es la Peregrinación de la Memoria, la  cual se viene celebrando desde el 2018 y es apoyada entre otros por la **Revista Noche y Niebla, el Cinep Programa por la Paz, la Corporación Claretiana Norman Perez Bello, el Movice y la Comisión Interclesial de Justicia y Paz,** organizaciones como la Comisión de la Verdad también se han unido al recorrido.]

\

### **Jornadas de Peregrinación** 

[Durante el día 1 todo el grupo se desplazará desde Caño Silbao en el municipio de Granada recorriendo 20,3 kilómetros, el día 2 el grupo se dividirá en dos, el primero tomará la ruta vereda Malabar a la vereda Caño Claro mientras el grupo B recorrerá 12, 4 kilómetros al tomar la ruta Medellín del Ariari hasta la vereda La Cumbre.]

[El día 3, el grupo A y B continuarán su camino por separado hasta reunirse en la vereda Puerto Esperanza, para al día siguiente recorrer el tramo de la vereda de Puerto Esperanza hasta la Esmeralda recorriendo 12,2, kilómetros para finalmente el día 5 realizar un último recorrido a pie desde la vereda La Esmeralda a Miravalles. El dia 6 el grupo llegará hasta la vereda Miravalles y El Parque de la Memoria de El Castillo, recorriendo 21 kilómetros, es opcional ir en vehículo o recorrer la distancia a pie.]

<iframe src="https://www.google.com/maps/d/embed?mid=1jmdATpiPbkPPnC0IA5CmPQMjZv8Scze5" width="640" height="480"></iframe>

 

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
