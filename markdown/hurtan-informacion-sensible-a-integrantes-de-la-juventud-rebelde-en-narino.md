Title: Hurtan información sensible a integrantes de la Juventud Rebelde en Nariño
Date: 2018-01-30 10:07
Category: DDHH, Nacional
Tags: Hurto, juventud rebelde, nariño
Slug: hurtan-informacion-sensible-a-integrantes-de-la-juventud-rebelde-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Juventud-rebelde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Juventud Rebelde] 

###### [30 Ene 2018] 

La organización Juventud Rebelde denunció que dos de sus integrantes fueron víctimas de hostigamiento y hurto, el pasado 28 de enero, cuando hombres **desconocidos entraron a sus viviendas y robaron computadores con información sensible, documentos y agendas**, las víctimas son María José Villota Guacas y Francisco Marín Gutiérrez, ambos integrantes de esta plataforma en el departamento de Nariño.

De acuerdo con la denuncia de la plataforma, en horas del mediodía, sujetos desconocidos ingresaron a la vivienda de María José Villota y Francisco Marín. Cuando los familiares de Villota regresaron a la casa, observaron que **dos de las habitaciones del hogar habían sido revisadas “minuciosamente”** y que los perros que viven con ellos, habían sido encerrados en un cuarto.

Entre los elementos robados se encuentran **dos discos duros, agendas y documentos con información del trabajo político organizativo, un televisor y una alcancía.** En ese sentido la plataforma manifestó que en la vivienda se encontraban objetos de mayor valor que fueron ignorados y que para la organización descarta que el hecho pueda ser identificado como hurto. (Le puede interesar: ["Hurtan información sensible a tres organizaciones defensoras de derechos humanos en Bogotá"](https://archivo.contagioradio.com/robo_informacion_organizaciones_derechos_humanos/))

Ambos integrantes de la Juventud Rebelde ya habían sido víctimas de amenazas por parte de grupos paramilitares debido a su actividad política y la labor que desarrollan en torno a la defensa de los derechos humanos. En ese sentido la plataforma de carácter nacional, le **exige al gobierno y a las entidades correspondientes que realicen las investigaciones correspondientes y determinen el origen y responsables por estos hechos**.

Además, pidieron que se garantice en el país el derecho y la labor que realizan los líderes sociales y defensores de derechos políticos en sus territorios, **“sin ser objetos de amenazas y hostigamientos por esta razón”** y le solicitaron a organizaciones internacionales como la ONU, insistirle al gobierno colombiano en las recomendaciones hechas, para que de manera efectiva proteja la vida de los líderes juveniles, sociales y defensores de derechos humanos.

[DENUNCIA PÚBLICA Juventud Rebelde](https://www.scribd.com/document/370340498/DENUNCIA-PU-BLICA-Juventud-Rebelde#from_embed "View DENUNCIA PÚBLICA Juventud Rebelde on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19015" class="scribd_iframe_embed" title="DENUNCIA PÚBLICA Juventud Rebelde" src="https://www.scribd.com/embeds/370340498/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-72LN1GEMinK0m3itqOH7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
