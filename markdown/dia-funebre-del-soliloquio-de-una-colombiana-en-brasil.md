Title: Día fúnebre: del soliloquio de una colombiana en Brasil
Date: 2016-10-05 09:55
Category: Opinion
Tags: acuerdo de paz, Plebiscito
Slug: dia-funebre-del-soliloquio-de-una-colombiana-en-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/gano-el-no-y-solo-queda-esperar-que-las-farc-siga-adelante-para-renegociar-otro-acuerdo-_860_573_1413364.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

#### [**Por Claudia Gordillo - Opinión **] 

###### 5 Oct 2016

Hace poco invité un amigo a un encuentro de colombianos en Campinas (Brasil) con el objetivo de discutir el plebiscito y me dice: ¡ya tengo suficiente con los dos colombianos que vivo!. Lo miré esperando una ampliación o corrección de su respuesta. Pero llegó el silencio, ese vacío impenetrable que nos configura, que nos llena las ausencias, que nos permite sobrevivir. Silencios que explotaron ayer. El día de votar el plebiscito por la paz fueron montañas de silencios los que aparecieron: los que dejaron de ir a votar por pereza, porque no entendían el acuerdo, porque tenían cosas divertidas para hacer, porque tenían que trabajar, porque no les llegó información, porque Colombia siempre ha estado así. Y que más da, un voto menos no hace diferencia.

Pues sí, hizo diferencia, y qué diferencia. Cambiamos un día triunfal para inaugurar una nueva semántica del afecto en torno al lenguaje de la paz para mantener el de la guerra, el del odio, el del exterminio del otro. ¿Qué más deberíamos esperar sí no nos gustamos? si cuando encontramos un colombiano en el exterior queremos estar lo más lejos posible de él. Si crecimos con el “no se dejé”, “sea avispado papito”, “hágale como pueda”, expresiones que nos inscribieron en una cultura revanchista, arribista, polarizada y excluyente. Nos enseñaron a sobrevivir, a mantener la vida con vida, a luchar el lugar que creemos que nos corresponde, a aruñar cada pedazo de vida del otro que nos sea útil para mantenernos. Ese es el nivel desde donde debemos pensar el último acontecimiento político.

Que haya ganado el NO, no significa que no queremos la paz; significa, sobretodo, que no entendemos qué es la paz, para qué la paz, cómo se hace la paz, dónde se fundamenta la paz y, lo más importante, con quien se hace la paz. Pensemos esto a la luz de cuatro movimientos: primero, el gobierno durante las negociaciones con las FARC instauró un uso lingüístico del postconflicto que inundó de manera esperanzadora y poco racional varios sectores, a pesar del vigor de la guerra. Segundo, los medios de comunicación usaron el nuevo discurso del gobierno con las viejas rutinas semánticas y visuales de la guerra, creando mayor esquizofrenia informativa enraizada en la polarización del SI/NO. Tercero, el gobierno delegó una buena parte de la votación a los televidentes de la guerra, a aquellos que asisten cotidianamente el espectáculo que las mediaciones informativas hacen de ella. Y cuarto, se filtró, desde hace tiempo, un discurso empobrecido de “todos somos víctimas”, que nos hace creer que somos cuerpos en trauma y estamos sentimentalmente atravesados por la pérdida.

La sumatoria de esos movimientos construyó una red de significados con nuevos ordenes: sin contexto fijo (¿conflicto o postconflicto?), sin sujeto (¿cuáles víctimas?: las directas, las de la ley, todos los colombianos) y sin democracia (o lo que es peor, sumergidos en el simulacro de una democracia reducida a un Sí a favor de Juan Manuel Santos y un No a favor de Álvaro Uribe Vélez). En conclusión: sin cultura política, sin transiciones, con abstracciones, sin saber qué hacer. Y así, ¿quién entiende qué es la paz?

Los resultados de las votaciones evidencian que los que saben estas respuestas son las víctimas, los que han sido cruzados, marcados, acribillados por la constancia temible de la guerra. Sus cuerpos adoloridos se han levantado de las cenizas, de las tristezas, de las múltiples perdidas para enseñarnos que significa la paz y el perdón mediante estrategias comunitarias de reconciliación y memoria. Trabajos que reflejan la entereza de la humanidad, de la posibilidad del amor, de la constitución de nuevos cuerpos y otras sentimentalidades.

Pero aún así, la grandeza de sus corazones ha sido poco para nosotros, los que cómodamente vamos al trabajo, al estudio, al mercado, al centro comercial. Porque no entendimos, porque nos perdimos como siempre entre la ignorancia, la apatía y el egoísmo que solo nos permite construir un “yo” y no múltiples “yos”. Nos da miedo perder lo que creemos que “ganamos” o “tenemos”, nos aterroriza la idea de no tener “otro” para lanzar nuestro odio, por eso el inmaculado silencio de 62% de abstencionistas. Entonces, que siga la exclusión, que sigan cayendo los subalternos, que se siga reproduciendo el odio y la indiferencia. Al final, nadie se quiere sentir responsable, lo que nos hace títeres sin afectos.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
