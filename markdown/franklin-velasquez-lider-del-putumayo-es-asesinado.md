Title: Franklin Velásquez, líder del Putumayo es asesinado
Date: 2020-09-02 10:00
Author: AdminContagio
Category: Actualidad, Líderes sociales
Slug: franklin-velasquez-lider-del-putumayo-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-02-at-9.25.14-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Cortesia Indepaz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 1 de septiembre se reportó el asesinato de **Franklin Velásquez, líder social y exintegrante del partido Polo Democrático** en el municipio de San Miguel, departamento del [Putumayo](https://archivo.contagioradio.com/comunidades-le-recuerdan-al-gobierno-que-acudir-a-aspersion-aerea-seria-ilegal/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según lo reportó el Instituto Nacional de Desarrollo para la Paz (Indepaz), el hecho se registró sobre el medía día, **cuando ingresaron hombres armados a la vivienda de Velasquez y le propinaron cinco impactos en su cuerpo con arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1300960102799810560","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1300960102799810560

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Indepaz también señaló que el líder **vivía en el barrio Central a orillas de la quebrada La Dorada, en el municipio de San Miguel**, lugar en donde perdió la vida a causa de las graves heridas provocadas por el hecho armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Franklin Velásquez, había dedicado su vida al trabajo social, labor que lo llevó a hacer parte en años atrás del partido Polo Democrático Alternativo, **también en Red Unidos y en el en el Instituto de Bienestar Familiar (ICBF).**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que se desconocen los autores intelectuales y materiales del crimen, **este hecho se suma a los más de cinco líderes social asesinados desde el último fin de semana de agosto y a los 203 líderes sociales y defensores de Derechos Humanos que han sido víctimas de homicidio en el 2020**; según el reporte mas reciente de [Indepaz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El asesinato de Franklin Velásquez se da en un contexto en que las acciones de erradicación forzada de cultivos de uso ilícitos en el departamento del Putumayo se siguen presentando ante la falta de garantías para la sustitución voluntaria por parte del Gobierno y en medio del riesgo que presenta para las comunidades la presencia y acciona de grupos ilegales. ([Otra Mirada: La aspersión aérea no es la respuesta](https://archivo.contagioradio.com/otra-mirada-la-aspersion-aerea-no-es-la-respuesta/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
