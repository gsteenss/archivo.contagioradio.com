Title: CNE inició persecución política contra FARC: Carlos Lozada
Date: 2018-05-04 18:02
Category: Paz, Política
Tags: CNE, FARC, Jaun Manuel Santos, santrich
Slug: cne-inicio-persecucion-politica-contra-farc-carlos-lozada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farc-2-e1518197319974.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 May 2018]

Frente a la orden que dio el Consejo Nacional Electoral de abrir una investigación sobre el manejo de los 5. 500 millones que están en la cuenta personal del director de la campaña política de la FARC, Carlos Lozada integrante de este partido, aseguró que este hecho hace parte de la **"persecución política"** y afirmó que el gobierno ha estado al tanto de todos los movimientos bancarios del naciente partido.

### **¿Qué pasó con el dinero de la FARC?**

La razón de que los dineros de la FARC, estuvieses en esta cuenta y no en una del partido, como lo exige la ley, de acuerdo con Lozada, radica en que para la realización de la campaña electoral de este partido, se hizo el desembolso de 5 mil quinientos millones, faltando tan solo 5 días para las elecciones a Cámara y Senado del Congreso.

"Eso provocó que en un primer momento, esos dineros que se depositaron en la cuenta del partido del Banco Agrario, solo podíamos disponer de una parte, **algo así como 3 mil millones de pesos, para pagar cuentas que se habían ejecutado**" afirmó Lozada.

Agregó que los cinco mil millones restantes, fueron retirados de esta cuenta 3 días antes de la finalización de la campaña, por recomendación de un funcionario del Estado, que les habría manifestado que si el dinero se quedaba en la cuenta, sería congelado. **No obstante, el integrante de la FARC, reiteró que este movimiento se hizo con conocimiento tanto del Banco Agrario como del Estado**.

Posteriormente, Lozada señaló, que el director de la camapaña, que fue quien recibió el dinero en su cuenta, estuvo intentando abrir en diferentes entidades bancarias una cuenta para el partido, "sorpresivamente ninguna entidad bancaria quiso hacerlo" y vuelve a consignarlo a su cuenta personal.  (Le puede interesar: ["FARC teme nuevas capturas y allanamientos en los próximos días"](https://archivo.contagioradio.com/farc_teme_nuevos_allanamientos_capturas/))

Ahora por orden del Consejo Nacional Electoral los dineros deben regresar a  la cuenta del Banco Agrario, mientras que se investigan los movimiento financieros del mismo. Situación que impediría que la FARC **pague las deudas que tiene y que para Lozada se convierte en una "persecución"**.

### **Parlamentario Británicos hacen moción a favor de Santrich** 

De igual forma, parlamentarios británicos, en una sesión, realizaron una moción en la que instaron al gobierno de Colombia a garantizar que se respete el debido proceso a Jesús Santrich, integrante del partido político FARC, y que se tenga una permanente vigilancia sobre el estado de su salud, producto de la huelga de **hambre que inició desde el pasado 9 de abril cuando fue capturado por el supuesto delito de narcotráfico**.

### **¿Otras formas de acabar con la FARC?** 

Estas situaciones para Carlos Lozada, se deben denunciar nacional e internacionalmente **"que existe un verdadero acoso político y jurídico contra el naciente partido"**, al que se le suman los incumplimientos del Acuerdo de Paz, en términos de implementación de los mismos.

El próximo lunes a las 10 am, en el Hospital el Tunal, se realizará una visita hospitalaria a Jesús Santrich y a su vez, se hará una cadena humana de solidaridad, "entendiendo que lo que sucede con Santrich no afecta solamente a su persona, o al partido, sino al conjunto de la sociedad colombiana en la medida que se esta afectando los Acuerdos de Paz".

###### Reciba toda la información de Contagio Radio en [[su correo]
