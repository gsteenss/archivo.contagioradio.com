Title: Romero, Roy y Raúl en San Salvador
Date: 2015-04-01 11:00
Author: CtgAdm
Category: Abilio, Opinion
Tags: Monseñor Oscar Arnulfo Romero, Raúl Vera, Roy Bourgeois, San Salvador, sicsal, SOAW
Slug: romero-roy-y-raul-en-san-salvador
Status: published

#### Por [[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - @abiliopena ] 

Estos tres defensores de derechos humanos, de los pueblos y seguidores de Jesús, se encontraron en San Salvador con ocasión del aniversario numero treinta y cinco del martirio y resurrección de **Monseñor Oscar Arnulfo Romero**.

**Roy Bourgeois** intervino en un auditorio repleto de la Universidad Centroamericana “José Simeón Cañas UCA”, de la Compañía de Jesús, sobre el **Movimiento por el Cierre de la Escuela de las Américas** (SOAW) que él preside, buscando clausurar esa escuela de adiestramiento militar en la que se instruyeron los oficiales  que asesinaron a Romero y  a Elba, Celina, y a los jesuitas Ellacuría, Baró, Montes, Joaquin López, Antonio López y Moreno. Esa academia militar instruyó, también al general colombiano Rito Alejo del Río, responsable del crimen del afrodescendiente del Cacarica, Marino López y hoy sigue instruyendo militares latinoamericanos siendo Honduras y Colombia los países que más estudiantes envían.

Al final de su presentación Roy habló de la injusta exclusión de la iglesia  Católica a mujeres a las que se les niega el sacerdocio y de la discriminación a la comunidad LGTBI. Compartió el dolor por su excomunión de la iglesia católica y, también de su comunidad religiosa de  Maryknoll por apoyar el sacerdocio femenino del que no quiso retractarse, condición pedida para evitar la severa medida.

Por su parte don **Raúl Vera obispo católico** de la diócesis del Saltillo en México, presidente del Sicsal -Oscar Romero- comprometido con la causa de los empobrecidos,  participó como ponente en en el mismo Congreso de la Uca pero no en el mismo panel, con igual afluencia de público.

Coincidieron, en la distancia, cuando don Raúl presidió la eucaristía en la pequeña capilla del Hospitalito en donde treinta y cinco años atrás, asesinaron a Monseñor Romero de un tiro de gracia. Roy, una vez pasada la comunión debió salir de prisa a reunirse con el presidente de El Salvador para pedirle que no enviara más militares de su país, a esa escuela de asesinos, por eso el sacerdote excomulgado y el obispo, no se pudieron encontrar personalmente.

La oportunidad se dio al día siguiente en la rueda de prensa convocada por SoaW -por sus siglas en ingles- en el parque Cuscaclán, donde se construyó el monumento de homenaje a las víctimas civiles de la guerra. Hablaron en un acto de reconocimiento a Roy, propiciado por Sicsal,  con la intensión de paliar, en algo, el dolor de su expulsión de la iglesia y de su comunidad religiosa. Don Raúl,  le dijo a Roy el significado evangélico de su lucha por la justicia, que como estudiantes de teología, por tantos años, sabían que la decisión de su excomunión podría ser revertida y que él,  con otros, haría lo posible por enviar mensajes que pudieran reconsiderar esa injusta decisión.

Roy por su parte   manifestó que  bastaría sólo un día de teología para entender que todas y todos somos iguales y que tenía esperanza en que un día la Iglesia  Católica se abriera al sacerdocio de las mujeres y al reconocimiento de los derechos de la comunidad LGTBI.

Un abrazo entre los dos selló ese profundo momento. Roy, excomulgado sólo por reclamar la igualdad en la iglesia y Raúl  un obispo comprometido con la justicia, solidario con su dolor, que se encuentran en las acciones contra el militarismo.

Romero, a sus 35 años de martirio y resurrección, próximo a ser beatificado por la Iglesia Católica, hizo lo suyo. Por él llegaron Roy y Raul a San Salvador, en él se inspiró el Movimiento por el Cierre de a Escuela de las América, en el se inspira el Sicsal- Oscar Romero-, del que es co-presidente don Raúl. Los nombres y apellido por los que se les reconoce inician con “R”. Muchas gracias a los tres por hacer posible este encuentro, altamente reparador para quienes tuvimos el privilegio de presenciarlo.
