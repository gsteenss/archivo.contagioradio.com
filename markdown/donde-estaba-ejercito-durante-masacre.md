Title: ¿Dónde estaba el Ejército durante la masacre de El Tarra?
Date: 2018-08-01 18:22
Category: DDHH, Nacional
Tags: ASCAMCAT, masacre, Paramilitarismo, Tarra
Slug: donde-estaba-ejercito-durante-masacre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-01-a-las-6.07.55-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CiscaCatatumbo] 

###### [1 Ago 2018] 

Esa es la pregunta que se formulan organizaciones sociales tras la masacre de 9 personas el 30 de julio, en el Municipio de Tarra, Norte de Santander; uno de los municipios más militarizado de los 11 que componen la región del Catatumbo. (Le puede interesar: ["Líder Social Frederman Quintero se encuentra entre las víctimas de la masacre en el Tarra"](https://archivo.contagioradio.com/lider-social-frederman-quintero-se-encuentran-entre-las-victimas-de-la-masacre-en-el-tarra/))

Para **Juan Carlos Quintero**, vocero de la **Asociación Campesina del Catatumbo (ASCAMCAT)**, entre la población de El Tarra hay incertidumbre por cuenta de la masacre que recuerda escenarios de violencia ocurridos 10 años atrás durante la incursión del Paramilitarismo en la zona. Adicionalmente, porque los hechos ocurrieron a plena luz del día, en el casco urbano del Municipio y en el lugar con mayor presencia de fuerza pública del Departamento.

El Catatumbo tiene 13 mil unidades de soldados y policías, específicamente en el Tarra tiene presencia La **Brigada Móvil Nº 23** y el pasado 20 de julio se inauguraron otras dos, hecho que para Quintero demuestra que puede haber un grado de omisión de las autoridades en el Municipio, sin embargo, la crisis humanitaria en Norte de Santander no es un tema nuevo.

### **Por guerra en el Catatumbo la ONU ha reportado más de 154 mil afectados** 

La militarización del Catatumbo no es un hecho novedoso, como tampoco lo es la delicada situación de derechos humanos en la zona, pues según cifras de la ONU, **154 mil personas se han visto afectadas** desde el 14 de Marzo en razón de la guerra por el control territorial entre el ELN y el EPL.

Según Quintero, unas 10 mil personas han sufrido el desplazamiento a principio de mayo y se establecieron en 32 refugios humanitarios, situación que puede verse agravada, pues se ha conocido que tanto el ELN como el EPL han rechazado la autoría de la masacre del Tarra, **hecho que significaría la presencia de otro actor armado en la región.**

Sobre esta situación, el vocero de ASCAMCAT concluyó que **la militarización del territorio ya ha demostrado no ser una salida a la violencia,** y que en lugar de ello, se deben revisar las causas profundas que son generadoras de desigualdad y terminan configurando los escenarios de violencia.

<iframe id="audio_27499831" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27499831_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
