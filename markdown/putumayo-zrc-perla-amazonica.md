Title: Zona de reserva campesina en Putumayo celebró 17 años de procesos organizativos
Date: 2017-12-13 11:54
Category: Comunidad, Nacional
Tags: ANOZRC, Perla Amazónica, Putumayo
Slug: putumayo-zrc-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 13 Dic 2017 

Las comunidades de la perla amazónica en Putumayo celebraron **17 años** de la creación de la zona de reserva campesina. Ellos y ellas **conmemoran los procesos de organización que han creado para auto gestionar su desarrollo y proteger sus derechos y el territorio**. Con muchas dificultades, los campesinos y campesinas han logrado fortalecer la agricultura y los procesos políticos en lugares que, por tradición, han sido olvidados por el Estado colombiano.

De acuerdo con Yaneth Silva, integrante de las Comunidades Construyendo Paz (CONPAZ) y lideresa de ADISPA, la celebración se realizó como forma de seguir fortaleciendo los procesos políticos que desarrollan las comunidades. Sin embargo, debido a la grave situación de seguridad que están viviendo las personas en Putumayo, **las personas celebraron durante el día realizando diferentes actividades, pero no lo hicieron en la noche**.

**Retos a futuro del proceso organizativo campesino**

Silva enfatizó en que los jóvenes se han interesado cada vez más en los diferentes aspectos de la vida política en los espacios de integración, pero esto constituye uno de los grandes retos del proceso organizativo a futuro. “**El reto más grande es que los jóvenes se vayan empoderando de los procesos políticos porque ya los adultos vamos de salida**”, recalcó.

Además, manifestó que hay un reto muy fuerte en el tema productivo de las comunidades campesinas en la medida en que “**podamos tener gobiernos propios que se comprometan con los jóvenes de la región**”. Informó que en la celebración las instituciones locales no se vincularon “los únicos que participaron fueron la Comisión Intereclesial de Justicia y Paz y ACNUR”.

**Mujeres deben empoderar los procesos políticos campesinos**

Silva enfatizó en que la representación de estos territorios, es muy importante que participen las mujeres. Indicó que **las mujeres campesinas han tenido siempre una vocación por defender los territorios pero necesitan que se creen espacios para poder crear estructuras de empoderamiento** como lo son las Circunscripciones Especiales de Paz.

Por esto, “**en las circunscripciones debe haber una representación diferente a los politiqueros de siempre**”. Silva además argumentó que, en ocasiones, en los territorios se ha visto que algunas personas buscan fortalecer sus proyectos personales y no el de las comunidades en general, por esto “vamos a seguir trabajando para seguir apoyando los procesos y lograr que estas curules queden en manos de quienes van a representar a las comunidades”.

Finalmente, recordó que esta celebración en la zona de reserva campesina en la perla amazónica, sirve como **espacio para fortalecer los procesos que se han ya construido e impulsa a la creación de nuevos caminos** que buscan proteger el territorio teniendo de presente que el Estado y sus instituciones no lo han hecho.

\[aesop\_gallery id="49959" revealfx="fromleft" overlay\_revealfx="fromleft"\]  
   
 

<iframe id="audio_22632770" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22632770_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="si" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
