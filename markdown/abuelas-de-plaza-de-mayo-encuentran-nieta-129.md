Title: Abuelas de Plaza de Mayo encuentran nieta 129
Date: 2019-04-09 22:21
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Abuelas plaza de mayo, Dictadura argentina, nietos desaparecidos
Slug: abuelas-de-plaza-de-mayo-encuentran-nieta-129
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/carlos-solsona-presidenta-las-abuelas-plaza-mayo-estela-carlotto-rueda-prensa-que-han-anunciado-hallazgo-1554879560999.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3v63S6WAAA-qmx.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

La organización argentina Abuelas de Plaza de Mayo anuncio que **otra hija de desaparecidos recuperó su verdadera identidad.** La nieta 129 es hija de los ex militantes del Partido Revolucionario de los Trabajadores-Ejército Revolucionario del Pueblo (PRT -ERP)  **Carlos Alberto Solsona y Norma Síntora**, ésta última detenida y desaparecida hace 42 años cuando tenía 8 meses de embarazo, mientras que Carlos estuvo presente durante el anuncio realizado por la organización y ahora espera encontrarse finalmente con su hija.

\[caption id="attachment\_64629" align="aligncenter" width="300"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3v63S6WAAA-qmx-300x200.jpg){.wp-image-64629 .size-medium width="300" height="200"} Carlos Alberto Solsona Foto: @PACOURONDO\[/caption\]

La nieta recuperada vive en el exterior y pasó varios años sin querer hacerse el examen de ADN, hasta la semana pasada cuando lo hizo voluntariamente. “Hace dos semanas la nueva nieta ingresó al país y, por una notificación de Migraciones, se presentó a la Justicia el miércoles 3 de abril. Allí, con intervención del equipo interdisciplinario de la Comisión Nacional por el Derecho a la Identidad (CONADI), **aceptó realizarse voluntariamente el análisis en el Banco Nacional de Datos Genéticos (BNDG), que arrojó que es hija de Norma Síntora y Carlos Alberto Solsona**” contó Estela de Carlotto, presidenta de Abuelas en conferencia de prensa.

> <https://t.co/rWWNAy4MBG>
>
> — Abuelas Plaza Mayo (@abuelasdifusion) [9 de abril de 2019](https://twitter.com/abuelasdifusion/status/1115713436846055424?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
“Abuelas notificó a su papá y a sus hermanos sobre el encuentro, quienes viajaron hasta aquí para conocer todos los detalles del caso. **Su papá Carlos, sus hermanos Marcos y Martín y sus sobrinos la esperan para abrazarla y reconstruir más de 40 años de historia arrebatados por el terrorismo de Estado**” añadió de Carlotto.

Las Abuelas destacaron la importancia del aporte de datos de la sociedad en el caso de esta nueva nieta recuperada y reiteraron el llamado a romper el silencio para poder encontrar a los nietos y nietas que faltan.

###### Con información de [FARCO](http://agencia.farco.org.ar/noticias/nieta-129-otra-hija-de-desaparecidos-recupero-su-verdadera-identidad/?fbclid=IwAR3ckVDPNnlMS0rlORCLeJNAo61HHjcBHWzSKbvJD0HGv1dZVtrEj7qsrh8)
