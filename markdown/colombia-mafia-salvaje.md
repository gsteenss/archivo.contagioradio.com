Title: Colombia Mafia Salvaje
Date: 2016-01-21 10:52
Category: Camilo Alvarez, Opinion
Tags: El Quimbo, fracking, Magia Salvaje, Mineria
Slug: colombia-mafia-salvaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/MAFIA-SALVAJE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB).** 

###### [(]21 Ene 2016[)] 

Nos Parecemos a un documental, Con alrededor de 46 millones de espectadores que se estrenó desde finales de 2015, somos un caleidoscopio que mira atónito en el espejo las calidades de los especímenes en vía de extinción que nos rodean, esa buena cantidad de reptiles, lagartos, micos, ratas y anfibios disputándose la naturaleza en la era del calentamiento global. Mafia Salvaje es el sentido de la realidad donde el principal mensaje es “El menor Riesgo es quedarse”

La espectacular mirada sobre la Sierra Nevada, La Amazonia, Chiribiquete, La Macarena, El Bio-pacifico zonas esenciales de la definición de nuestras próximas décadas, se nos venden como la puerta del Cielo, el pulmón del mundo, el corazón andino, La glándula pituitaria del continente, nombres que nos hacen sentirlos nuestros y que a su vez denotan el mercado de órganos al que nos enfrentamos.

El mensaje para hacernos más identitarios con una idea de nación inconclusa, y con ello sentirnos como pobladores o consumidores del proyecto turístico -tabla de salvación de nuestros campos- que nos convertirá en emprendedores empresarios y renovados guías de deportes extremos bajo cursos subsidiados del Sena mientras el “ser pillo paga” campea en forma de narcotráfico y economías ilegales.

Y es que el ángulo de la Colombia profunda -que son sus gentes- sigue perdido en la sala de producción, ¿cómo podrá compaginar la Magia Salvaje con la deforestación acelerada, con la minería exacerbada, con la cacería indiscriminada y con la pobreza absoluta? En que ríos se hará El Torrentismo, El Rafting, El Trekking, Hijuepuerking!!

Tal vez no importe y así como se venden tours para conocer laboratorios de coca con práctica incluida, se podrán vender planes para conocer las coloridas playas de manchas de carbón en la Bahía de Santa Marta, retroexcavar en el Rio Naya y hasta desalojar nativos con técnicas ESMAD. Lógicas que pasan en silencio y rápidamente al olvido, Nadie habla ya de los convenios establecidos en parques naturales con Aviatur y como el señor Bessudo con recursos del estado, sobre áreas protegidas implementó un piloto para su enriquecimiento licito.

Pero este 2016 es más obtuso de su parte, con la sequía más grave de los últimos años  (previsible y científicamente medible) se vende nuestra soberanía energética -que es AGUA- mientras se realiza una campaña contra el derroche. El Uberrimo oportunista no menciona todo lo vendible de su corazón firme, ni la comisión que le corresponde a sus secuaces por tal venta.

 La expresión “llora como una Magdalena” pierde más vigencia en el rio descendiendo a índices insospechados en el Magdalena Medio, ese mismo Rio que vendían “Navegable” en respuesta a la crisis invernal de hace tres años, mientras en el Alto Magdalena los campesinos echan Quimba desplazados por El Quimbo y así entre ciclo y ciclo la emergencia ambiental que siempre avanza mientras se enriquecen con las ayudas y subsidios de los verdaderos damnificados.

Al final volvemos la mirada a los puntos “positivos” de estos días mientras suenan los hermanos lebrón de cortinilla en los noticieros /La temperatura sube y sube/ y Se marcan nuevas tendencias de moda, que si la chancla, que si el chingue, que Bogotá esta tan linda que parece Medellín, que Medellín esta tan caliente que parece Barrancabermeja y que Barranca se evapora mientras aumenta el consumo de cerveza.

Si esta grave sequía no es aun contundente para enfrentar la cruda –hoy hervida- realidad, salgámonos de ella; vamos en búsqueda del Mohan y la Madremonte y todos los viejos mitos que cuidaban la naturaleza, personajes que tal vez invocando nos hagan una propia versión de Avatar. No sé si exista un desfibrilador cultural, uno que reanime y nos despierte de los bronceados y el turismo de las películas del Paseo. Un algo que nos devuelva el sentido de la Madre Tierra y que grite a esos especímenes de la Mafia Salvaje que nos llevan a la extinción.

Por favor, Me colabora con la salida!!!!
