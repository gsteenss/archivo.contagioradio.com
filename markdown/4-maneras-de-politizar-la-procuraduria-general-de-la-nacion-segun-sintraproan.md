Title: 4 maneras de politizar la Procuraduría General de la Nación según SINTRAPROAN
Date: 2016-07-05 15:44
Category: Judicial, Nacional
Tags: corrupción, Procurador Alejandro Ordoñez
Slug: 4-maneras-de-politizar-la-procuraduria-general-de-la-nacion-segun-sintraproan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/procu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: zonacero.com] 

###### [5 Julio 2016] 

El Sindicato de Trabajadores de la Procuraduría General de la Nación SINTRAPROAN, denuncia que se están presentando por lo menos cuatro irregularidades  al interior de la institución: **el desfile de integrantes del Congreso, la disminución de sanciones y procesos, la permanente presencia de militares y policías y presuntos pagos por procesos**.

De acuerdo con William Millan presidente de SINTRAPROAN "estas problemáticas han generado al interior de la entidad un declive de la Procuraduría para mal de la democracia y un incumplimiento de la función principal de esta institución".

**Desfile de integrantes del Congreso**

Eduardo Enríquez Maya, María Fernanda Cabal, entre otros, hacen parte de los congresistas y senadores que según Millan, visitan permanentemente las instalaciones de la Procuraduría General de la Nación,  con el fin de obtener recomendaciones del tipo político para que sus recomendados sean nombrados en diferentes cargos al interior de la entidad, evidenciando cómo prima el iteres particular que posteriormente podría significar "catapultar al Procurador Alejandro Ordoñez a una aspiración de tipo político a la presidencia de la Nación".

Millan índica que en la actualidad, podrían existir **500 cargos ocupados** a partir de estas recomendaciones políticas dentro de la institución de los 4.000 puestos que existen en la Procuraduría.

**Presencia de Militares y Policías **

Según Millan, la presencia de miembros de las Fuerzas Militares y de la Policía es cada vez más constante en la Procuraduria General de la Nación desde la reelección de Alejandro Ordoñez, hecho que visibiliza diferentes anomalías como que el personal de la policía haya venido desplazando a la seguridad privada de la institución, el nombramiento de dos coroneles de la policía al interior de la entidad que va en [contravía del artículo 86, numeral cuarto del decreto 262 del 2ooo](https://archivo.contagioradio.com/el-procurador-viola-abiertamente-la-constitucion-ivan-cepeda/) por el cual se regula la estructura de la Procuraduría General de la Nación, a su vez se conoció que uno de los soldados que hacía parte de la seguridad de la entidad, fue encontrado en la calle del Bronx portando sustancias ilícitas.

**Presuntos Pagos por procesos**

Pese a que aún Sintraproan no tiene las pruebas reunidas para demostrar esta irregularidad, William Millan advierte que  en las diferentes regiones a las que ha viajado, las personas le han indicado que se sancionan procesos en regiones para posteriormente arreglarlos en Bogotá o viceversa, a cambio de dineros. De otro lado señala que se están formulando cargos que generen causal de nulidad y  cobrando por este hecho.

**Disminución de procesos y sanciones**

El sindicato ha recalcado que desde el último periodo de Alejandro Ordoñez como procurador han menguado los procesos y ha disminuido las sanciones. De acuerdo con Millan, hay un 50% menos de sanciones a funcionarios  comparadas con la anterior administración distrital, dejando que proliferen las denuncias interpuestas por la ciudadanía a un control interno disciplinario o archivando los procesos. "[Significa que se condena o se sanciona a los contrarios de un determinado grupo político y se absuelve a los amigos".](https://archivo.contagioradio.com/palacio-de-justicia-procurador-ordonez-pretende-ignorar-fallo-de-la-cidh/)

Esta crisis, conforme lo afirma Millan, afecta la función de la procuraduría y pone en tela de juicio su accionar ante la sociedad como es el caso del [debilitamiento de las investigaciones de violación a derechos humanos](https://archivo.contagioradio.com/palacio-de-justicia-procurador-ordonez-pretende-ignorar-fallo-de-la-cidh/) "aquí hay alrededor de 60 investigaciones por falsos positivos que se quedaron quietas pese a que iban bastante adelantadas" asevero el presidente de Sintraproan.

<iframe src="http://co.ivoox.com/es/player_ej_12129202_2_1.html?data=kpeelJ6WdJOhhpywj5aaaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncbjdzdHWw9KPkcrgzcbbh5enb7S9r7m_o7W2k6LCjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]
