Title: Espacio Humanitario de Puente Nayero celebra 3 años construyendo paz
Date: 2017-04-10 15:54
Category: DDHH, Nacional
Tags: Conpaz, puente nayero
Slug: puente-nayero-3-anos-construyendo-paz-y-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-10-at-10.15.19-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [10 Abr 2017] 

El pasado domingo 9 de abril se llevó a cabo el tercer aniversario de la creación del Espacio Humanitario de Puente Nayero, los integrantes de esta comunidad celebraron entre juego ancestrales, baile, teatro y música del Pacífico, **la vida de un escenario que les ha permitido retornar a sus raíces, construir posibilidades de paz y alejar la violencia del paramilitarismo.**

**Los habitantes del espacio humanitario, después de tres años, cambiaron lágrimas por sonrisas, ya que el paramilitarismo se había tomado esa zona**, realizando torturas y masacres en casas de piques. Después de haber   vivido tantos momentos de dolor y haber recompuesto el tejido social y la familia, hoy los niños no le temen estar en las calles.

En el evento se realizó **en Buenaventura, en el barrio La Playita, allí participaron diferentes organizaciones defensoras de derechos humanos como CONPAZ y la Comisión Intereclesial de Justicia y Paz**, además diferentes líderes y lideresas que desde un principio han estado en presentes en la conformación de este espacio. Le puede interesar: ["Amenazan de muerte a líder social de CONPAZ en Buenaventura"](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-de-conpaz-en-buenaventura/)

Además, se contó con la participación de Monseñor Héctor Epalza Quintero, obispo de Buenaventura, quien ofició misa, al igual que en la constitución del Espacio Humanitario tres años atrás. **De esta forma se celebró la memoria, la vida, la dignificación de las víctimas y los procesos construidos por la comunidad.** Le puede interesar: ["Paramilitares asesinaros a jóvenes que se negaron a ser reclutados"](https://archivo.contagioradio.com/paramilitares-asesinaron-a-jovenes-que-se-negaron-a-ser-reclutados/)

\

Comunicadora de CONPAZ Nidiria Ruíz, Valle del Cauca

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
