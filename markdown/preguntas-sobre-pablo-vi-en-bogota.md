Title: Preguntas sobre Pablo VI en Bogotá (1968), Camilo Torres y los campesinos
Date: 2017-08-28 09:00
Category: Columnistas invitados, Opinion
Tags: Teologia, Visita papa Colombia
Slug: preguntas-sobre-pablo-vi-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Pablo-VI-en-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: uniminutoradio 

**<u>Primera entrega</u>**

#### ** Por David Martínez Osorio / Escritor, investigador y consultor** 

Acaban de cumplirse cuarenta y nueve años de la visita de Pablo VI a Bogotá (22 a 24 de agosto de 1968). ¿Por qué Bogotá fue el primer destino de un pontífice romano? ¿Es apenas una anécdota litúrgica que haya celebrado una misa campesina en Mosquera (Cundinamarca)? ¿Cuál es el significado que encierran las palabras de Gabo en *Vivir para contarla*: “*Entre las amistades que me quedaron de la universidad, la de Camilo Torres no sólo fue de las menos olvidables, sino la más dramática de nuestra juventud*”.

Entre 1962 y 1965, se adelantó la última gran reforma histórica de la iglesia católica: el Concilio Vaticano II (CV II). Juan XXIII quería que nuevos aires del mundo le devolvieran vitalidad a las desgastadas estructuras eclesiales. En la Europa de la Guerra Fría, el proceso de secularización se profundizaba, luego de que el Vaticano mantuviera relaciones ruidosas con el fascismo. Fernando Vallejo las documentó de manera detallada en su diatriba *La puta de Babilonia*[\[i\]](#_edn1){#_ednref1} (2007), y con base en los datos recaudados se refirió a la iglesia católica como “*la* *solapadora de Mussolini y de Hitler*”. Por estos lares, mientras tanto, la persecución estadounidense de todo lo que oliera a comunismo, iniciada a nivel doméstico –*Me casé con un comunista* (1998) de Philip Roth es una novela insuperable, que recrea el ambiente anticomunista gringo durante la segunda mitad de los años cuarenta y la primera de los cincuenta–, irrumpía en América Latina y el Caribe, y se tornó obsesiva después del triunfo de la revolución cubana. El Plan Marshall (desde 1947), allá; la Alianza para el Progreso (desde 1961), acá.

¿Y en Colombia? Rodolfo Ramón de Roux llama la atención sobre los efectos del CV II: «Los cambios más ‘externos’, como la reforma litúrgica y el abandono de la sotana, fueron los que más impactaron, al menos en Colombia, al creyente ‘ordinario’». Hasta ese momento, la misa era en latín y el cura la decía de espaldas a los feligreses, por ejemplo. Quien no lo tuviera presente, no hubiese podido descifrar todo el significado que tiene un pasaje de *Vivir para contarla* de Gabo. Él y Mercedes, su esposa, habían decidido bautizar a su primogénito, Rodrigo (nacido el 24 de agosto de 1959, nueve años cerrados antes de que llegara Pablo VI a Bogotá). Escogieron a Camilo Torres Restrepo para que administrara el rito, y a Plinio Apuleyo Mendoza como padrino. El primero no estuvo muy conforme con la segunda elección, cuenta Gabo, y dice algo más:

(continuará)

###### [\[i\]](#_ednref1)
