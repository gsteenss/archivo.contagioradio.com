Title: "Al San Juan de Dios hay que recuperarlo entre todos y todas"
Date: 2015-02-13 22:57
Author: CtgAdm
Category: DDHH, Nacional
Tags: Derecho a la salud, hospital, Ley 100, mario hernandez, Privatización de la salud, Salud, san juan de dios
Slug: al-san-juan-de-dios-hay-que-recuperarlo-entre-todos-y-todas
Status: published

###### [Foto: Universidad Sergio Arboleda] 

<iframe src="http://www.ivoox.com/player_ek_4081195_2_1.html?data=lZWlk5adeY6ZmKiak5WJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmotGYtcbSb6vpwtOYxsqPiMrj1JDVw96PtdbZjNfSxdrUqdPV09HcjcrSuNPZjNncxtTXb9qfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Hernández, Médico y Doctor en Historia] 

El miércoles 11 de febrero se llevó a cabo un acto público en las instalaciones de **Hospital San Juan de Dios,** que contó con la participación de la administración distrital, el gobierno nacional y las directivas de la Universidad Nacional de Colombia, para dar inicio al ambicioso proyecto que busca **devolverle la vida a este emblemático centro**.

Para el médico y Doctor en Historia, **Mario Hernández**, "**el San Juan de Dios es la historia de la nación colombiana**, allí se sintetiza mucho de lo que somos, por eso es muy importante recuperarlo entre todos". El cierre se dio en el proceso de privatización del sistema de salud, iniciado en 1978, cuando el gobierno de Turbay convirtió al hospital en una Fundación privada, en manos del Ministro de Salud, y finalizó al implementarse la Ley 100 de 1993, que privatizó el derecho a la salud.

Para Mario Hernández, **el San Juan de Dios "se cerró porque no logró entrar en esta lógica de competencia de la Ley 100**. Se dejó morir allí porque no tenía como sobrevivir."

Los retos para logran llevar a cabo la reapertura del Hospital en los términos planteados por el proyecto de la Universidad Nacional, requieren **coherencia por parte del Estado colombiano, y su financiación a través de un Plan Nacional de Desarrollo** que no profundice el aseguramiento y la intermediación financiera como lógica de bien estar social.

Según Hernandez, **el proyecto también requiere que la sociedad colombiana en su conjunto se piense y re-apropie de las instituciones que son patrimonio** de las y los colombianos, y constituyen elementos fundamentales de la identidad.

**El San Juan de Dios tiene más de 450 años**, y en su largo trasegar -señala Hernandez- ha visto nacer y crecer a Colombia-. Debería darse una valoración muy fuerte de este hecho, para que "cualquiera no lo pueda cerrar".

El proyecto de reapertura también busca vincular nuevamente la salud y la educación a través del hospital, generando también **espacios para el arte y la memoria**.
