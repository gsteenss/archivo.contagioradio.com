Title: Participa en el Festival de Mujeres en escena por la paz
Date: 2017-02-17 09:58
Category: Cultura, eventos
Tags: colombia, mujeres, paz, teatro
Slug: 36424-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Mujeres-en-escena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación colombiana de Teatro 

###### 17 Feb 2017 

Este año el Festival de Mujeres en Escena por la Paz, abre convocatoria para todos los colectivos teatrales, organizaciones sociales, bailarines o artistas del performance, cuyos trabajos traten sobre los temas de g**énero, Derechos Humanos, nuevas masculinidades, paz y mujeres.**

En la edición XXVI del Festival, que **se realizará entre el 11 y el 21 de agosto**, se busca visibilizar **el trabajo que desarrollan las mujeres y el movimiento social desde el teatro, la danza y la música,** en función de mostrar a los públicos una reflexión a través de tales manifestaciones y las mujeres que exploran sus talentos desde estos campos de arte.

Para participar en las convocatorias, que culminan el próximo 28 de febrero, **los participantes deben presentar sus propuestas con un video de diez minutos**, obras dirigidas preferiblemente escritas o dirigidas por mujeres, con experiencia mínima de 5 años y diligenciar el formulario que podrá llenar [aquí](https://docs.google.com/a/contagioradio.com/forms/d/e/1FAIpQLSf1Yu9mKVh0Mp8_aX0LeuIdgfkc_CGOnCA2a7Z3HkUpg3D64w/viewform?c=0&w=1).  
El indispensable que todos los trabajos a participar, estén enmarcadas dentro de las temáticas descritas anteriormente. Para encontrar más información visite la página oficial de la [Corporación Colombiana de Teatro](http://www.corporacioncolombianadeteatro.com/). El Festival es un espacio para que tanto el público como el movimiento cultural nacional y las delegadas nacionales e internacionales puedan también hacer un balance del estado del arte del trabajo que realizan las mujeres en el teatro.
