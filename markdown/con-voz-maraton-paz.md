Title: La paz es una apuesta de fe y de unidad: Primera etapa de “ Con voz tejiendo país ”
Date: 2017-12-13 08:15
Category: Nacional
Tags: maraton, paz, radio
Slug: con-voz-maraton-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/WhatsApp-Image-2017-12-13-at-8.29.04-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio radio 

##### 13 Dic 2017 

En una maratón radial por la paz de 16 horas, **se han unido tres emisoras, una red de radio comunitaria, un espacio radial, dos universidades y siete organizaciones**. " Con voz tejiendo país" es un espacio que recoge diferentes voces para entender la forma como se está construyendo la paz en el país desde la sociedad civil y las organizaciones.

El primer bloque de esta maratón, está enfocado en la **reflexión espiritual de la construcción de la reconciliación y la paz**. Para esto, se ha hablado de la formación y la educación para la paz desde un enfoque religioso y espiritual. Además, se ha hizo énfasis en la reconciliación como vehículo para traducir la paz en la vida, una reconciliación que sirva como punto de encuentro de las diferencias.

Teniendo en cuenta la necesidad de evidenciar las acciones educativas y formativas desde las regiones, monseñor Omar Sánchez de la diócesis de Tibú en Norte Santander, enfatizó en la necesidad de explorar la reconciliación teniendo en cuenta que “**hay reconciliación cuando hay transformación y las partes se acercan y se dan cuenta que hay que afrontar el pasado con un futuro común**”.

Dijo además que en Colombia la reconciliación va a tomar tiempo debido a que “la cultura del país está dada para el desquite, para ajustar al otro y hoy, fácilmente, se retroalimentan más las diferencias que los puntos de encuentro”. Con esto, monseñor Omar Sánchez, culminó con una reflexión que llama a la paz y la reconciliación, no como palabras vacías, si no como entendimientos con contenidos teóricos que justifiquen lo que significa la reconciliación.

**Las personas deben exigir la paz**

Continuando con el tema de la formación y la educación para la paz, Monseñor Darío Monsalve, enfatizó en la dificultad por la que está atravesando la coyuntura política colombiana en donde los acuerdos de paz, firmados entre el Gobierno Nacional y las FARC, se encuentran en “riesgo”. Esto en la medida que “**la institucionalidad no ha respondido con honradez**”.

Para monseñor Monsalve, “**haber traído los Acuerdos al esquema institucional ha sido fatal, hay un riesgo latente de que se acaben**”. Por esto, indicó que hay una necesidad de” mirar al pueblo como el autor de la paz, allí se da la paz y son ellos y ellas los que exigen la paz”. Ante esto, manifiesto que los Acuerdos de Paz deben fortalecer la voluntad del pueblo para que la paz sea el conector de las zonas rurales.

**Resistencia de las comunidades desde la fe, el amor y la unidad**

**Ledis Tuirán y Eustaquio Polo**, víctimas del conflicto armado y ahora líderes sociales del territorio de Pedeguita y Mancilla y Curvaradó en el Chocó, fueron desplazados de su territorio en 1997. Hoy, a través de la fe, han emprendido una lucha por la conservación del territorio y la defensa de los derechos humanos.

Eustaquio Polo, indicó que **la fortaleza espiritual los ha llevado a construir procesos de perdón y reconciliación en donde han podido empezar su vida de nuevo** buscando conservar ese territorio que una vez les quitaron. Sin embargo, estas comunidades han tenido que afrontar la presión de grupos armados que, en los últimos 12 días han asesinado a dos líderes sociales de este territorio.

Finalmente, los líderes recordaron que para alcanzar la paz en los territorios “**hay que abrir los corazones para recibir la fortaleza de Dios**”. Indicaron que la lucha por el país se puede sacar adelante si hay redes de esperanza y unidad en la esperanza de un futuro mejor.

Además, y como elemento fundamental de la lucha por la defensa de la vida y el territorio se manifestaron como defensores ambientales, que a través de la **construcción de Planes de Ordenamiento Territorial individuales y colectivos propenden por la conservación de las fuentes de vida a pesar de las amenazas y acciones de empresas de plátano, ganaderas, palmeras y madereras que vienen actuando en el territorio** en connivencia con estructuras criminales que ya han asesinado a varios de los líderes, entre ellos Mario Castaño y Hernán Bedoya.

Como cierre de la intervención los líderes comunitarios y defensores ambientales cantaron la alabanza “**ajuntemos los tisones**” que es una parábola en la que mencionan que para prender un fogón de leña es necesario que los carbones con calor estén juntos. Ellos y ellas han encontrado la fuerza en la unidad y por ello este canto tiene especial relevancia para su proceso.

###### Reciba toda la información de Contagio Radio en [[su correo]
