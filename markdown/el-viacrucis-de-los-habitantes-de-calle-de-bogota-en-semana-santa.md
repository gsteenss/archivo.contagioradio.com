Title: El viacrucis de los habitantes de calle de Bogotá en Semana Santa
Date: 2018-03-28 13:46
Category: DDHH, Nacional
Tags: derechos de los habitantes de calle, Fray Gabriel, Habitantes de calle, Semana Santa, viacrusis callejero
Slug: el-viacrucis-de-los-habitantes-de-calle-de-bogota-en-semana-santa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/viscrusis-callejero-1-e1522262762833.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fray Gabriel Gutiérrez] 

###### [28 Mar 2018] 

Con motivo del inicio de la Semana Santa, los defensores de los derechos humanos de los habitantes de calle, realizaron un viacrucis callejero en Bogotá. El objetivo es hacer un llamado a la ciudadanía de **bondad y misericordia** por la población olvidada e invisibilizada en el país.

Quien ha coordinado este viacrucis es Fray Gabriel Gutiérrez, uno de los defensores de los derechos humanos de esta población y quien ha querido que con el viacrucis callejero se **humanicen las calles** mientras que se “visibiliza al Cristo sufriente en los ciudadanos habitantes de calle”. Afirmó que junto con la Fundación Callejeros de la Misericordia han acompañado a los más de 16 mil habitantes de calle de Bogotá para compartir la Cruz de Jesucristo.

### **Así fue el viacrucis callejero** 

El viacrucis callejero se ha hecho durante tres días y estuvo dividido en tres sectores. Fray Gabriel indicó que durante el lunes Santo visitaron lugares como el **antiguo Bronx**, el parque Santander y la Plaza España. En ese trayecto encontraron a más de 150 personas “tiradas en el piso con hambre”, en total abandono estatal.

Afirmó que cuando se encuentran con estas personas **los escuchan y atienden** sus preocupaciones. Acompañados de un grupo de jóvenes “le vamos llevando el mensaje de paz y de misericordia”. Durante el viacrucis acompañaron a las trabajadoras sexuales de la estatua de la mariposa en el centro de la ciudad y “le presentamos la Cruz como un signo de solidaridad”. (Le puede interesar:["Censo de habitantes de calle "debe verse como un drama y no como una cifra": Fray Gabriel Gutiérrez"](https://archivo.contagioradio.com/censo-de-habitantes-de-calle-debe-verse-como-un-drama-y-no-una-cifra-fray-gabriel-gutierrez/))

Durante el martes Santo, acudieron al parque el Tercer Milenio **“que nos recuerda la historia de lo que era el cartucho”**. Allí rezaron el santo viacrusis con los habitantes de calle “como un signo de que el señor está con ellos y no los ha abandonado”. El miércoles santo irán a la calle 19 y al barrio Santa Fe donde acompañarán “a miles de seres humanos invisibilizados”.

### **Hace falta apoyo institucional para atender a los habitantes de calle** 

Fray Gabriel recordó que el viacrucis callejero se realizó como **iniciativa ciudadana**. Sin embargo, no se logró contar con el apoyo de la Policía para visitar sectores como el caño de la sexta donde viven cerca de 400 habitantes de calle. Indicó que las autoridades ponen muchas “trabas” para este tipo de procesos y no se tiene en cuenta que “la solidaridad es un derecho de la Constitución”. (Le puede interesar:["Piden declara emergencia humanitaria por asesinatos de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/policia-realizo-operativo-contra-habitantes-de-calle-en-bogota/))

Finalmente, insistió en que es necesario que en Bogotá se declare una **emergencia humanitaria** para lograr atender a la población que vive en la calle. Por esto, invitó a la ciudadanía a hacer un ejercicio de reconocimiento del otro e hizo un llamado para que la ciudadanía apoye el trabajo que realizan los defensores de los derechos de los habitantes de calle quienes están en busca de una sede para acoger a esta población.

<iframe id="audio_24929201" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24929201_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>

<div class="osd-sms-wrapper">

</div>
