Title: Ejército no llegó a acto de perdón por asesinato del comunero indígena Eduin Legarda
Date: 2018-02-23 16:33
Category: DDHH
Tags: Aida Quilcue, CRIC, Eduin Legarda, Ejército Nacional, Fuerza Pública
Slug: ejercito-reconocio-y-pidio-perdon-por-asesinato-del-comunero-indigena-eduin-legarda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Edwin_Legarda_CRIC-e1519421459692.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPS Noticias] 

###### [23 Feb 2018] 

Este viernes se tenia que realizar el acto de desagravio y perdón público del Ministerio de Defensa en representación del Estado Colombiano, **por el asesinato de Eduin Legarda, el 16 de diciembre de 2008 cuando se dirigía a recoger a su compañera Aida Quilcue**, vocera del CRIC y líder indígena y a una delegación de indígenas en el sector de San Pedro del Bosque, en el territorio del Pueblo Totoroez, Cauca.

Para Aida Quilcue este acto de perdón es un hecho agridulce, debido a que por un lado está la esperanza de haber encontrado justicia y por el otro, **el hecho de que se de 3 años después de que un fallo del Tribunal contencioso Administrativo del Cauca**, responsabiliza a la Fuerza Pública por el asesinato de Legarda y le ordenará al Estado como medida de reparación, llevar a cabo el acto de perdón.

Quilcue señala que hoy tienen “la esperanza, de que después de una **revictimización de tantos años, hoy nos devuelven la buena imagen, no solo a mi sino también a mi familia**” y recordó que Eduin Legarda, dejó un legado dentro de la comunidad en por impulsar proyectos productivos y microempresariales. (Le puede interesar:["Dos integrantes de la ACIN han sido víctimas de atentados durante este mes](https://archivo.contagioradio.com/2-integrantes-de-la-acin-han-sido-victimas-de-atentados-este-mes/)")

### **La responsabilidad de la Fuerza Pública** 

El asesinato de Legarda se cometió el 16 de diciembre de 2008 cuando se dirigía a la ciudad de Popayán a recoger a su compañera Aida Quilcue. De acuerdo con la vocera del CRIC, Las primeras versiones de la Fuerza Pública afirmaban que los habían asesinado porque Legarda acompañaba a una jefe guerrillera, posteriormente, **sindicaron a Aida de estar detrás del asesinato**.

Sin embargo, la defensa de Quilcue logró demostrar que el asesinato se produjo en el marco de una emboscada del Ejército Colombiano. De igual forma, Quilcue recordó que durante ese diciembre, el asesinato de su esposo no fue el único hecho violento que tuvo que soportar la comunidad y su familia, también se registró el atentado contra su hija, que en ese entonces tenía 12 años, e**l desplazamiento forzado de cientos de familias y la judicialización de los líderes indígenas que protestaron en el marco de la Minga**.

Por este hecho hay  5 soldados condenados a 40 años de presión y dos suboficiales que en segunda instancia quedaron absueltos, todos señalados de ser autores materiales de los hechos, no obstante, no hay ninguna captura o investigación que señale a autores intelectuales. (Le puede interesar:["Así fue el acto de perdón del Ejército por el asesinato del líder indígena Eleazar Tequia"](https://archivo.contagioradio.com/perdon_ejercito_asesinato_lider_indigena/))

Así mismo Aida señaló que este proceso en gran medida logró llegar hasta el reconocimiento del Estado de su culpabilidad, por el apoyo y el acompañamiento de los indígenas en el Norte del Cauca.

**Actualización:** Ni el gobierno, ni los integrantes del ejercito se presentaron al lugar donde la comunidad indígena había decidido hacer la ceremonia en memoria de **Eduin Legarda, **

<iframe id="audio_24002243" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24002243_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
