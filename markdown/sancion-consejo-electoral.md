Title: Sanciones del Consejo electoral a la UP limitan participación política
Date: 2017-04-28 14:26
Category: Nacional, Política
Tags: Consejo electoral, Nacional, paz, Unión Patriótica
Slug: sancion-consejo-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/union_patriotica-22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las dos orillas] 

###### [27 Abr 2017] 

El **Consejo Electoral sancionó al partido político la Unión Patriótica** por inscribir candidatos de manera irregular en las pasadas elecciones regionales del 2015, sin embargo, de acuerdo con el vocero de este partido, Jaime Caycedo, **esta decisión del Consejo es viciada contra los partidos de oposición** que se enmarcan en la izquierda, en vez de investigar los grandes partidos como Cambio Radical.

Caycedo señala que el Consejo está conformado “por los partidos tradicionales” que han demostrado **una “actitud” por ensañarse hacia los pequeños partidos y fuerzas políticas** y no hacia los que han tenido grandes casos de corrupción como Cambio Radical o el Partido de la U, para quienes hasta el momento no ha existido alguna sanción de inhabilidad.

“Mientras las fuerzas de la oligarquía tienen presos a sus exgobernadores, que han tenido inculpaciones incluso de asesinato y están pagando cárcel por eso, **a la izquierda se le aplica de manera abierta, pública y antes de hacer alguna reclamación** este tipo de sanciones”. Le puede interesar: [Avanzar en lo nuevo, no hay mas](https://archivo.contagioradio.com/avanzar-en-lo-nuevo-no-hay-mas/)

**Jesús Santrich**, integrante del Secretariado de las FARC-EP indica que sanciones como esta, impartidas por el Consejo Electoral, **demuestran la necesidad de que se den transformaciones en este órgano** que permita una participación no solo de los nuevos partidos que surjan, sino también de los que han intentado hacerse espacio entre los grandes partidos políticos.

En el caso de la Unión Patriótica, no podría presentar candidato en 8 municipios Francisco Pizarro, Pasto y Samaniego (Nariño); Málaga y Bucaramanga (Santander); Ubaté y Soacha (Cundinamarca); Buga (Valle); Florencia (Caquetá); Purificación (Tolima); Fortul (Arauca); Barranquilla (Atlántico), y Bogotá.

El integrante de la Unión Patriótica también expresa, que, si bien es cierto que hay inhabilidades, por casos como juicios de alimentos, h**asta el momento no se ha encontrado ninguna inhabilidad por corrupción o crímenes**. Sin embargo, afirma que se tomaran medidas al respecto como reforzar los mecanismos de control previo a la presentación de candidatos.

<iframe id="audio_18399668" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18399668_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
