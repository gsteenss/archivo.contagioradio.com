Title: Dos integrantes de la FARC han sido asesinados esta semana
Date: 2018-04-05 15:51
Category: DDHH, Nacional
Tags: Antioquia, asesinatos de excombatientes, Caquetá, Cauca, FARC
Slug: farc_integrantes_asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Teleantioquia] 

###### [5 Abr 2018] 

Continúan los asesinatos de integrantes de la FARC. Esta semana dos han sido las víctimas. **Se trata de  Nelson Andrés Zapata Urrego, asesinado en Remedios, Antioquia y  Darwin Londoño Bohórquez,  quien fue ultimado el miércoles en Piamonte, Cauca.**

El primer homicidio se ocurrió sobre las 6:00 pm cuando hombres encapuchados dispararon en repetidas ocasiones contra el reincorporado de las Farc de 32 años. Este se encontraba desplazándose entre la vereda Lejanías y el casco urbano del municipio de Remedios.

Lucía del Socorro Carvajal, la alcaldesa de este municipio, confirmó el asesinato del exguerrillero  e informó que se trataba del alias de “Willinton” y era oriundo de Yolombó.  Tanto la comunidad como la FARC se encuentran a la espera de la investigación de los responsables del hecho. ([Le puede interesar: Asesinan a líder campesino del Catatumbo)](https://archivo.contagioradio.com/asesinado-campesino-catatumbo/)

Además, en la denuncia publicada por la Corporación Cahucopana reiteró las exigencias al Estado sobre las garantías para los territorios en el Nordeste Antioqueño ante los asesinatos que se han venido presentando en los últimos meses, y con el alto riesgo que se presenta en las zonas donde se está realizando procesos de reincorporación.

Teniendo en cuenta que **en menos de un mes han sido asesinadas cuatro personas en la zona rural de este municipio,** esta semana se desarrolló un consejo de seguridad en el que se solicitó presencia de la Fuerza Pública alrededor del Espacio Territorial de Capacitación y Reintegración, ETCR.

**Otra víctima en Cauca**

La otra víctima que se presentó esta semana es Darwin Londoño Bohórquez, de 34 años de edad *y *oriundo de PuertoRico, Caquetá, fue asesinado este miércoles en el municipio de Piamonte, Cauca. **El hombre pertenecía a la ETCR de Agua Bonita en La Montañita, residía en Zabaleta,** área general de San José del Fragua, y se encontraba en el vecino departamento hospedado en una residencia llamada 'El Viajero', según pudo establecer el medio local 'Caquetá al día'.

"Según lo que se ha podido establecer es que  llegaron a la residencia dos sujetos a preguntar por él, al parecer lo venían siguiendo. Tipo 8:00 de la noche el hombre sale a cenar al restaurante 'Doña Juana' y ahí lo interceptan las dos personas quienes el disparan varias veces", dice ese mismo medio. [(Le puede interesar: FARC denuncia asesinato y abuso de madre de excombatiente)](https://archivo.contagioradio.com/farc-denuncia-asesinato-de-madre-de-un-ex-combatiente/)

José Joaquín Ramos, alcalde de Piamonte, informó que el excombatiente se encontraba solo en un restaurante, hasta donde llegó un sujeto desconocido que le propinó seis disparos de arma de fuego. También, las autoridades conocieron que uno de los victimarios sería oriundo de Zabaleta y llevaría presuntamente varios años dedicado al sicariato.

**Según la Fiscalía, entre enero de 2017 y marzo de 2018, han sido asesinados 52 integrantes de Farc,** además de familiares de estos. Ante la situación, el hoy movimiento político continúa exigiendo garantías a la vida de los excombatientes, y asimismo, esta semana tuvieron una reunión en Quito con la delegación de paz del ELN, donde abordaron estas situaciones en las que integrantes de la FARC han aparecido muertos a manos de exguerilleros del ELN, frente a lo cual acordaron desarrollar actividades de clarificación.

<div class="js-tweet-text-container">

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>
