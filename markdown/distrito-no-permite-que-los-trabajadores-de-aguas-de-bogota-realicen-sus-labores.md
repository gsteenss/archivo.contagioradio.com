Title: Distrito no permite que los trabajadores de Aguas de Bogotá realicen sus labores
Date: 2018-02-05 13:28
Category: Movilización
Tags: Aguas de Bogotá, basuras Bogotá, Bogotá, emergencia sanitaria Bogotá, Enrique Peñalosa
Slug: distrito-no-permite-que-los-trabajadores-de-aguas-de-bogota-realicen-sus-labores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/basuras1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [05 Feb 2018] 

En medio de la emergencia sanitaria que por estos días vive la ciudad, los trabajadores de Aguas de Bogotá continúan en cese de actividades y denuncian que su situación laboral no ha sido atendida. Asimismo,  aseguran que en ellos no recae frente a los tumultos de basuras en diferentes puntos del país, ya que, según ellos, son las autoridades policiales quienes  **no les han permitido el ingreso a las instalaciones** de la empresa para que se pueda prestar el servicio de recolección.

De acuerdo con Orlando Quiroga, trabajador de Aguas de Bogotá, “desde que el 19 de diciembre sacaron a los trabajadores de la empresa, y **no se les ha permitido el ingreso para que se realice el trabajo**”. Afirman que desde la Alcaldía no ha habido ningún pronunciamiento y nadie está presto a dialogar con ellos.

Por otro lado, frente a la situación que se presentó con la protesta de los trabajadores, han aclarado que “la manifestación duró dos horas hasta que llegó el ESMAD y esas unidades están todavía a la salida de la empresa y no han dejado que los trabajadores entren”. Por esto afirman que los trabajadores **están dispuestos a normalizar las actividades** de recolección “pero no ha sido posible”.

### **Flota de vehículos ha sido deteriorada** 

Al problema de la recolección de basuras se suma la denuncia de los trabajadores que afirman que hay más de **90 carros en malas condiciones** y “les han retirado piezas para colocárselos a otros”. Esta situación no es nueva, desde hace más de 3 meses los trabajadores han venido manifestando que la condición de los carros de recolección se ha deteriorado y “no estaban saliendo más de 70 vehículos cuando deben operar cerca de 120”. (Le puede interesar: ["Peñalosa no da respuesta a futuro laboral de más de 2000 trabajadores de Aguas de Bogotá"](https://archivo.contagioradio.com/penalosa-no-da-respuesta-a-futuro-laboral-de-mas-de-2000-trabajadores-de-aguas-de-bogota/))

**Denuncian que a algunos trabajadores no les pagarán la indemnización**

Quiroga enfatizó en que **no hubo ninguna planificación para hacer el cierre de la empresa** y “el gerente de manera irresponsable le ha dicho a las personas que no va a haber liquidación ni indemnización para los más de mil trabajadores en condición de discapacidad que han sufrido enfermedades por del trabajo”, y agrega que los **despidos de esos trabajadores han sido masivas ,** aunque** **el Ministerio de Trabajo ha dicho que no se pude ser así.

En esa empresa, hay más de **500 personas en condición de discapacidad** que prestan sus servicios para la recolección de basuras y “son explotados de manera indiscriminada”. Por esto han responsabilizado al Estado de lo que sucede en la ciudad pues “desde hace más de un mes se había advertido lo que podía ocurrir y hay un abandono de los trabajadores a quienes no les avisaron que los iban a despedir”.

### **Procuraduría ha pedido que se dialogue con los trabajadores** 

La Procuraduría le ha solicitado al alcalde Enrique Peñalosa que informe acerca de las acciones adoptadas frente al problema de la recolección de basuras. Así las cosas, el alcalde deberá decir que ha hecho para evitar que la ciudad se vea inmersa en una **problemática social y ambiental** producto de la no recolección de las basuras en 6 días. (Le puede interesar: ["Corrupción e incumplimientos, factor común de empresas que controlarán el negocio de la basura en Bogotá"](https://archivo.contagioradio.com/corrupcion-e-incumplimientos-factor-comun-de-empresas-que-controlaran-el-negocio-de-la-basura-en-bogota/))

Además, el ente de control le exigió a la administración que entable un diálogo con los trabajadores de Aguas de Bogotá para que se atiendan los reclamos de **vulneración a los derechos laborales**, la protección del trabajo y el mínimo vital. Como no han recibido respuesta, han argumentado que “es culpa de la administración local quien prácticamente cerró la empresa antes del 12 de febrero que empiezan a operar los privados”.

### **Trabajadores no están dispuestos a mezclar la situación con intereses políticos** 

Frente a los señalamientos que han hecho algunos medios de comunicación, quienes afirman que la responsabilidad de la emergencia sanitaria recae en el ex alcalde Gustavo Petro, los trabajadores han señalado que **no van a caer en la coyuntura electoral** para tratar este problema.

Quiroga reitera que “han llegado candidatos de todos los partidos a explotar este caldo de cultivo”. Afirmó que los candidatos del nuevo partido FARC, que hicieron presencia en las instalaciones de la empresa, **“nos han provocado un problema** porque hemos recibido amenazas por parte de grupos al margen de la ley”. Por esto, pidieron que sean únicamente las autoridades quienes se encarguen de entablar un diálogo con ellos para resolver la situación.

<iframe id="audio_23559906" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23559906_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
