Title: Colombia es el país con más asesinatos de ambientalistas en el mundo
Date: 2020-07-29 19:27
Author: PracticasCR
Category: Actualidad, DDHH
Tags: Defensores ambiente, Global Witness, indígenas, Informe Global Witness
Slug: colombia-es-el-pais-con-mas-asesinatos-de-ambientalistas-en-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Global-Witness-defensores-del-medio-ambiente-asesinados-en-2019-en-el-mundo.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Defensores-del-medio-ambiente-Global-Witness.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Defensores-del-medio-ambiente-Global-Witness-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Ilustración: Global Witness

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de las múltiples violencias que se viven en los territorios que son aquejados por un sinnúmero de actores y factores; la ONG Global Witness dio a conocer su más reciente informe titulado «*[Defending Tomorrow](https://www.globalwitness.org/en/campaigns/environmental-activists/defending-tomorrow/)»* en el que aborda la crisis climática y las amenazas contra defensores del medio ambiente y el territorio en el año 2019. (Lea también: [Catatumbo sufre seis tipos de pandemias](https://archivo.contagioradio.com/catatumbo-sufre-seis-tipos-de-pandemias/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe, **el 2019 fue el año más violento en la historia**, del que se tiene registro, pues **se alcanzó la cifra record de 212 asesinatos en contra de** **ecologistas** que luchan, entre otras, cosas contra los efectos de la minería, los proyectos agroindustriales y la deforestación. **Esto implica un alarmante promedio de más de 4 personas asesinadas por semana.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A nivel mundial, Colombia ocupó el primer lugar con el mayor número de asesinatos de ambientalistas registrados, siendo **64 los defensores del medioambiente que perdieron la vida en desarrollo de su labor**, acercándose **casi al tercio del total de asesinatos en todo el mundo** con un 30% y con una considerable diferencia respecto a Filipinas que es el segundo país con un 20%. (Lea también: [971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De hecho, **solo Colombia (64) y Filipinas (43) completan más de la mitad de los asesinatos en todo el mundo** contra estos defensores entre los que se encuentran líderes indígenas, guardabosques, activistas medioambientales  y campesinos.

<!-- /wp:paragraph -->

<!-- wp:image {"id":87538,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Global-Witness-defensores-del-medio-ambiente-asesinados-en-2019-en-el-mundo.png){.wp-image-87538}  

<figcaption>
Gráfico: Informe *[Defending Tomorrow](https://www.globalwitness.org/en/campaigns/environmental-activists/defending-tomorrow/)* - Global Witness

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Adicionalmente, con base en el informe se puede señalar que el desalentador panorama se extiende también a **América Latina donde se registraron dos tercios de los asesinatos de todo el mundo.** Según Global Witness esta es la región históricamente más afectada por las violencias desde que la ONG comenzó a publicar sus informes en 2012.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En 2019, solo la región amazónica registró 33 muertes. Casi el 90% de los asesinatos en Brasil fueron en la Amazonía y en Honduras los asesinatos aumentaron de 4 que se registraron en 2018 a 14 el año pasado, lo que lo convierte en el país con más muertes violentas por número de habitantes en 2019 seguido por Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, el informe arrojó que «***la minería fue el sector más mortífero, con 50 defensores asesinados en 2019****. Los agronegocios continúan causando destrucción, con 34 defensores asesinados \[…\] y **la tala fue el sector con el mayor aumento de asesinatos a nivel mundial respecto a 2018 con un 85%** más de ataques registrados contra defensores que se oponen a esa industria y 24 defensores asesinados en 2019*».

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=FM7X1tnT4Sc","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-4-3 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=FM7X1tnT4Sc

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading {"level":3} -->

### Capítulo Colombia: Las violencias contra defensores del medio ambiente y los territorios

<!-- /wp:heading -->

<!-- wp:paragraph -->

Debido a la situación especialmente preocupante, **Global Witness destinó en su informe un apartado especial solo para referirse al caso Colombia y estableció que el aumento de asesinatos en el país representaba  más de un 150% respecto a los registros del año 2018.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, documentó que los grupos indígenas estaban particularmente en riesgo, representando la mitad de los asesinatos registrados, a pesar de que la población indígena constituye apenas un 4,4% del total nacional. (Le puede interesar: [Pueblos indígenas hacen llamado urgente para proteger la Línea Negra](https://archivo.contagioradio.com/pueblos-indigenas-hacen-llamado-urgente-para-proteger-la-linea-negra/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para explicar, estas preocupantes cifras Global Witness, citó a la Oficina de Derechos Humanos de las Naciones Unidas, la cual señala que algunas de las razones que explican esta creciente ola de violencia en los territorios, son las dificultades en la implementación del Acuerdo de Paz, la reforma agraria que este prevé y los programas de sustitución de cultivos de uso ilícito. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
