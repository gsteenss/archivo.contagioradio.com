Title: La obra de Cortázar llega al Teatro Quimera
Date: 2016-11-01 13:30
Category: Cultura, Otra Mirada
Tags: Bogotá, Julio Cortázar, Quimera, teatro
Slug: cortazar-llega-al-teatro-quimera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/unnamed-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro Quimera 

##### 2 Nov 2016 

Como parte de la celebración por sus **30 años** de existencia, el **Teatro Quimera de Bogotá** presenta "**Mancuspias y otros juegos**", una propuesta escénica construída a partir de textos escritos en prosa y en verso que hacen parte de la obra del genio de las letras latinoamericanas **Julio Cortázar**.

La propuesta del grupo artístico, está estructurada como un **conjunto de fragmentos sobre el exilio, el amor y la muerte**, donde se incluyen **apartes de Rayuela**, una de las obras cumbres del escritor, presentadas en **15 viñetas o escenas** que apelan a la sensibilidad de los espectadores para que estos armen su propia obra. Le puede interesar: [Cortázar y la defensa de los Derechos Humanos](https://archivo.contagioradio.com/cortazar-y-la-defensa-de-los-derechos-humanos/).

Aplicando el "**salto cortaziano**", que presenta los enlaces o conectores como saltos al vacío y no como la unión de situaciones semejantes, característica definitiva de la obra de Cortázar, la compañía pone en escena una serie de acciones y personajes que parecen moverse en un continuo absoluto, ajenos a toda historicidad con cierto repentismo, como fantasmas de un escritor que huye constantemente del orangután de lo idéntico.

![Teatro quimera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/unnamed-4.jpg){.alignnone .size-full .wp-image-31568 width="1388" height="925"}

### Estructura al estilo Cortázar 

Las 15  viñetas o escenas que Te desarrollan durante los 90 minutos que dura "Mancuspias y otros juegos" son: 1. Maitre 2. Volver 3. Toco tu boca. 4. Sueños 5. Bebé Rocamadeur/Rayuela. 6. El río 7. Cartas(1) 8. Llama el teléfono Delia 9. La puerta 10. Encubridor 11. Lukas y su patriotismo 12. Patria de lejos 13.. Cartas 14. Casa Tomada y la 15. Te quiero país.

La obra, dirigida por **Jorge Prada Prada** y la co-dirección de **Sandra Ortega**, es  una creación escénica de Mercedes Burgos, Rosario Montaña, Sandra Ortega, Fernando Ospina, Gabriel Pérez, Ángela Piñeros, Alberto Salamanca, Karen Villegas, Diego Zamora.

La temporada tendrá lugar del **jueves 3 al Sábado 12 de Noviembre** a partir de las **7:30 de la noche** en el Teatro Quimera, Calle 70a Nº 19- 40. Costo de la entrada **estudiante: \$10.000 general: \$15.000.**
