Title: Avanza la Cumbre de los Pueblos en Panamá
Date: 2015-04-11 10:16
Author: CtgAdm
Category: El mundo, Movilización
Tags: Cumbre de los Pueblos Panamá, Evo Morales, Movimientos sociales Ámerica Latina, Rafael Correa, Silvio rodríguez Cumbre de los Pueblos Panamá
Slug: avanza-la-cumbre-de-los-pueblos-en-panama
Status: published

###### Foto:Kaosenlared.net 

###### **Entrevista con [Olmedo Carrasquillas] de los colectivos Radio Temblor y Voces ecológicas de Panamá:** 

<iframe src="http://www.ivoox.com/player_ek_4336448_2_1.html?data=lZigmJmYfI6ZmKiakpyJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bhw9fSjcnJb83j1JC918rGsNDnjMnSjbXFssLhhqigh6aVcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
La **Cumbre de los Pueblos** que se está celebrando en la Universidad de Panamá, constituye una **alternativa a la VII Cumbre de las Américas de Panamá**, que reagrupa a líderes de todos los países del continente americano, con el objetivo de proyectar las políticas económicas para la región.

Con el lema **“América Latina, una patria para todos, en paz, solidaria y con justicia social”** y con una marcha desde el Parque Belisario Porras, cerca de la Embajada de Cuba , hasta la Universidad de Panamá, donde los asistentes pudieron escuchar un emotivo **concierto de Silvio Rodríguez** inició la cumbre .

Según **Olmedo Carrasquillas**, miembro de los colectivos Radio Temblor y Voces ecológicas, a la cita han acudido más de **3.000 delegados** de movimientos sociales de toda América Latina.

En la cumbre se pretende que cada delegación exponga sus motivos y propuestas para poder extraer conclusiones con el objetivo no solo de protestar ante las injusticias, sino de crear un **modelo alternativo de desarrollo**, al impuesto por las políticas neoliberales del norte de América.

Olmedo afirma que esta cumbre es **antineoliberal, antiimperialista, antitrasnacional y de carácter progresista.**

**A 10 años de la derrota del ALCA**, en la agenda se van a discutir temas como el feminismo, la educación, el sindicalismo, la soberanía territorial, el extractivismo y el papel de las multinacionales y  los DDHH.

Pero para el reportero Panameño los temas más importantes son las **relaciones entre EEUU y Cuba**, las declaraciones de **EEUU sobre la amenaza para su seguridad de Venezuela**, y el conflicto de Las **Islas Malvinas en Argentina**.

Entre los asistentes se espera la llegada de **Evo Morales, Cristina Kirchner o Rafael Correa**, y delegados de sindicatos y organizaciones sociales para impartir conferencias sobre la soberanía de los pueblos y participar de los debates entre los movimientos sociales asistentes.

Por último, el viernes 10 de Abril se realizó la conferencia de Evo Morales sobre los pueblos originarios, para más tarde realizar un **torneo de fútbol entre el equipo de Evo y otros equipos de pueblos indígenas.**
