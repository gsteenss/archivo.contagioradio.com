Title: Nicaragua elige por cuarta vez a Daniel Ortega
Date: 2016-11-09 16:57
Category: El mundo, Otra Mirada
Tags: Daniel Ortega, elecciones, Nicaragua, Sandinista
Slug: daniel-ortega-nicaragua-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/15305954_xl.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:HispanTV 

##### 9 Nov 2016 

**Con más del 72% de la votación, el Frente Sandinista de Liberación Nacional, se impuso en las urnas** para lo que será el cuarto mandato de Daniel Ortega, tercero consecutivo, como presidente de Nicaragua, **alcanzando además la mayoría de las curules en la Asamblea Nacional **del pais centroamericano.

Con el 99,8 % de las mesas electorales escrutadas, los resultados indican que **el 68% de los ciudadanos ejerció su derecho constitucional al voto**, apoyando en su mayoría la propuesta de Ortega, quien para esta oportunidad se presentó junto a su esposa Rosario Murillo, como fórmula vicepresidencial.

Un total de **1.803.944 votos** le otorgaron la victoria al comandante Sandinista contra los **373.230** alcanzados por su principal contendor **Máximo Rodríguez**, candidato del Partido Liberal Constitucionalista (PLC) y ex guerrillero de la “Contra” que enfrentó al sandinismo durante los años 80.

Los comicios, estuvieron acompañados por seis expresidentes latinoamericanos: Fernando Lugo de Paraguay, Manuel Zelaya de Honduras (2006-2009), el salvadoreño Mauricio Funes (2009-2014), Vinicio Cerezo (1986-1991) y Álvaro Colom (2008-2012) de Guatemala.

### **Las políticas sociales y económicas de Ortega.**

Analistas internacionales aseguran que la nueva reelección de Ortega, **es el resultado del proceso de reconstrucción socioeconómica y modernización** del pais iniciado desde su segundo mandato en 2007, que sirvió para **posicionar a Nicaragua en el segundo lugar de crecimiento económico en la región** por detrás de Panamá.

Los impactos de la economía social de mercado, han permitido el **establecimiento de políticas que ayuden a reducir los niveles de pobreza y fomenten la generación de empleo**, estrategias para las cuales se han establecido alianzas con la empresa privada, lo que ha facilitado una mayor **inversión en obras públicas de infraestructura** y fortalecimiento de los programas de atención social en salud, educación, emprendimiento y acceso a la comunicación, donde la figura de la primera dama, ahora vicepresidenta, ha cobrado mayor relevancia.

**Oposición no reconoce el triunfo.**

Los opositores al gobierno Ortega, aglutinados en el **Frente Amplio por la Democracia (FAD)**, aseguran que la abstención en todo el pais estuvo sobre el 70%, con lo que a su parecer resta cualquier tipo de legitimidad al proceso electoral, sumado al hecho de no contar con observadores electorales nacionales o internacionales de la Organización de Estados Americanos (OEA) ni la Unión Europea (UE),  que acompañaran el proceso.

La oposición quedó fuera de la contienda a causa de **un fallo judicial que los despojó de la representación legal de su partido** cuatro meses antes de las elecciones, en una maniobra que atribuyeron los afectados a Ortega. Algunos de los militantes, **anunciaron que no aceptarán los resultados electorales y exigieron convocar nuevas elecciones**, decisión que requeriría la aprobación de dos tercios del Congreso.
