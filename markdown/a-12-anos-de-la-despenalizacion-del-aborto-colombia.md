Title: A 12 años de la despenalización del aborto, Colombia aún tiene barreras
Date: 2018-05-10 14:29
Category: Mujer, Nacional
Tags: aborto, legalización del aborto, mujeres
Slug: a-12-anos-de-la-despenalizacion-del-aborto-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 May 2018] 

Se cumplen 12 años desde que entró en vigencia la Sentecia c-355 del 2006, por la cual se despenaliza el aborto en 3 causales en Colombia. De acuerdo con Juliana Martínez, coordinadora de la Mesa por la vida y las mujeres, ha existido un avance social en defensa del aborto como derecho, sin embargo, afirmó que aún existen muchas barreras que dificultan que en la práctica las mujeres puedan acceder a abortos seguros y oportunos.

### **Las barreras al aborto seguro y oportuno en Colombia** 

La principal barrera que se encuentran las mujeres en Colombia a la hora de ejercer su derecho a un aborto tiene que ver con la ruta de atención, desde el mismo instante en que lo solicita, muchas de ellas han denunciado malos tratos, cuestionamientos a sus decisiones o incluso ser expuestas a juntas de médicos para que se tome la decisión sobre sus cuerpos.

El conocimiento sobre la sentencia c- 355 de 2016 ha sido una de las principales labores que han adoptado las organizaciones defensoras de los derechos de las mujeres en Colombia. No obstante, de acurdo con la Corporación Humanas, **tan solo el 11% de las mujeres que no tienen educación superior conocen la sentencia**, situación a la que se suma el desconocimiento en los métodos de planificación que tienen las mujeres.

Otro de los obstáculos de acuerdo con Juliana Martínez, tiene que ver con la capacitación a médicos en el país que presten el servicio de aborto, sintiéndose seguros y con la experticia técnica que se requiere. La dilación injustificada, que es básicamente dilatar la prestación del servicio, es uno del os riesgos más grandes que afrontan las mujeres en Colombia que solicitan el aborto. (Le puede interesar: ["ABC de la despenalización del aborto en Colombia](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)")

### **Los avances de la legalización del aborto** 

“Hemos logrado que más personas tengan claro que el aborto es un derecho fundamental de las mujeres en el país y que está permitido en 3 circunstancias manifestó Martínez. Las tres causales aprobadas en Colombia para el aborto son: por malformaciones del feto, cuando hay un riesgo para la salud o la vida de la mujer y cuando el embarazo es fruto de violencia sexual.

El año pasado, en la cuesta que desarrolló la Mesa por la vida y las mujeres, se logró establecer que un **65% de las personas encuestadas expresaron estar a favor del aborto en algunas circunstancias**. De igual forma, se constató que hay aceptación por las causales despenalizadas y que hay un rechazo a que se penalice a las mujeres por abortar.

### **Los retos para que Colombia garantice un aborto seguro y oportuno** 

El principal reto que debe asumir Colombia para que exista un aborto seguro y oportuno, de acuerdo con Martínez, es que se eliminen por completo las barreras para ejercer este derecho. Asimismo, la divulgación de la sentencia y **la pedagogía sobre la misma, también deben ampliarse en el país para garantizar el acceso a la misma**.

"Este es un derecho para todas las mujeres del país, independientemente del lugar en donde se encuentren, de su edad, sus condiciones particulares, es decir que es universal, que una mujer que esta en Botogá, en el Chocó o en Leticia, pueda acceder con calidad a este servicio, que se cumpla su voluntad y no que tenga que ser sometida a malos tratos en la ruta de atención" afirmó Martínez. (Le puede interesar:["Las nuevas visiones de la sociedad colombiana sobre el aborto"](https://archivo.contagioradio.com/encuesta_aborto_colombia/))

<iframe id="audio_25903512" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25903512_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
