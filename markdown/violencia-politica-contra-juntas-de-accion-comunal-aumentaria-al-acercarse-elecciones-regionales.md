Title: Violencia política contra Juntas de Acción Comunal aumentaría al acercarse elecciones regionales
Date: 2019-06-14 18:26
Author: CtgAdm
Category: Comunidad, Nacional
Tags: cerac, juntas de acción comunal, Violencia Política
Slug: violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Juntas-de-Acción.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **[Foto: cahucopana]** 

**El Centro de Recursos para el Análisis de Conflictos(CERAC)** reveló un incremento del 86% durante el mes de mayo en las muerte vinculadas a violencia política. Agresiones en las que los principales objetivos han sido dirigentes de Juntas de Acción Comunal (JAC) y activistas de tipo político a nivel regional, un hallazgo de cara a los comicios locales y regionales de octubre próximo.

**Andrés Palencia, investigador del CERAC** indicó que este aumento se dio después de cuatro meses de caída, y frente a un escenario de prospectiva, para la entidad es altamente probable que este riesgo aumente en el marco de elecciones locales y regionales.

Según los datos recopilados, un balance mensual revela que los primeros cinco meses del 2019 en promedio murieron 11 personas, lo que representa una reducción del 15% si es comparado con la misma fecha del 2018 que corresponde a 13 muertes en promedio. A pesar de ello, si se habla del promedio mensual, este aumento en 58%  comparándolo con el 2017%  correspondiente a 7 muertes.

Tales eventos se registraron según Palencia, con mayor intensidad en **Antioquia, Tolima, Cauca,  Arauca y Valle del Cauca,** zonas en las que el CERAC identificó una mayor disputa entre grupos armados por el territorio, el control del narcotráfico, la extorsión y la minería ilegal, revelando una mayor letalidad en regiones donde el ELN tiene presencia.

Para el investigador, se trata de una forma de agresión centrada en "personas que ejercen actividades de representación, ampliación y construcción de poder al interior de las comunidades, lo que podría coincidir con el proceso electoral que se va a desarrollar en el mes de octubre. [(Lea también: No hay garantías para Juntas de Acción Comunal, asesinan al líder Benedicto Valencia)](https://archivo.contagioradio.com/no-hay-garantias-para-juntas-de-accion-comunal-asesinan-al-lider-benedicto-valencia/)

El informe del CERAC coincide con la información presentada en el informe ‘Violencia Camuflada, la base social en riesgo del **CINEP**, un estudio que denota un incremento en el 2018 de los ataques a integrantes y líderes de movimientos sociales de base, como las juntas de acción comunal, registrando 648 asesinatos que según la investigación serían consecuencia de la violencia política. [(Le puede interesar: según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales)](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/)

<iframe id="audio_37113642" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37113642_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
