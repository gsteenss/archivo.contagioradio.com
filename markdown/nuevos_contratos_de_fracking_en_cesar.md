Title: Hay dos nuevos contratos para fracking en el departamento del Cesar
Date: 2017-05-02 14:47
Category: Ambiente, Voces de la Tierra
Tags: fracking, san martin
Slug: nuevos_contratos_de_fracking_en_cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/No-al-Fracking1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gert Steenssens 

###### [2 May 2017] 

La alerta por aumento de concesiones destinadas a fracking en Colombia aumenta. Una nueva denuncia de CORDATEC, evidencia que **ya son cinco los municipios del departamento del Cesar** los que se ven amenazados por esta técnica para extraer petróleo y gas.

El Personero de Gamarra, Fredy Martínez, recibió este domingo de la ANH una respuesta mediante la cual se confirma que además del contrato para Fracking del bloque VMM3 que hoy está en exploración y que afecta San Alberto, San Martín y Aguachica, está en firma un nuevo contrato del bloque VMM2 que afectaría además a Gamarra y Rio de Oro. **Contratos que están a nombre de las empresas ConocoPhillips y Canacol Energy.**

En el marco de esa serie de denuncias, **este martes y miércoles 3 y 4 de mayo, se lleva a cabo en el Congreso de la República, un debate de control político sobre el tema.** Un espacio en el que se piensa evidenciar las actividades ilegales que se llevan a cabo en esos cinco territorios del Cesar, donde las comunidades no han sido consultadas.

“La ANH y la ANLA (**Agencia Nacional de Hidrocarburos y la Agencia Nacional de Licencias Ambientales) y también lo ha sido el gobierno nacional**”, asegura Carlos Andrés Santiago, vocero de CORDATEC. Con el debate la comunidad espera que se confronte a las autoridades sobre los planes que se tienen en Colombia con esta técnica, que tiene en riesgo, incluso [el páramo de Sumapaz que dejaría en grave riesgo el abastecimiento de agua en de la capital del país. ](https://archivo.contagioradio.com/fracking-amenaza-abastecimiento-agua-bogota/)

Frente a esos posibles efectos del fracking, **se espera que el debate promueva la necesidad de declarar la moratoria contra el fracturamiento hidráulico**, con miras a prohibirlo, según explica Carlos Andrés Santiago. [(Le pude interesar: Gobierno prepara dos nuevos bloques para fracking)](https://archivo.contagioradio.com/gobierno_prepara_fracking_colombia/)

Al debate se encuentran citados los Ministros de Minas y Energía, Germán Arce, de Ambiente y Desarrollo Sostenible, Luis Gilberto Murillo, entre otros altos funcionarios del Gobierno, para discutir sobre normatividad, condiciones y términos de referencia para la exploración y producción en Yacimientos No Convencionales usando Fracking.

Los Senadores Maritza Martínez del Partido de la U y Jorge Robledo del Polo Democrático; impulsan el debate del martes, en la Comisión Quinta **del Senado, y el 4 3 de mayo desde las 9:00 am en la Comisión V de Cámara la iniciativa ha sido por iniciativa de los representantes** Luciano Grisales, Angel María Gaitán, Flora Perdomo y Crisanto Pizo del Partido Liberal, Ruben Darío Molano del Centro Democrático, y Nicolás Albeiro Echeverry del Partido Conservador.

<iframe id="audio_18464438" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18464438_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
