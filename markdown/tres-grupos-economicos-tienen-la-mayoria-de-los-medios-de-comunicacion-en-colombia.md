Title: Tres grupos económicos tienen la mayoría de los medios de comunicación en Colombia
Date: 2015-10-15 16:00
Category: Entrevistas, Política
Tags: Ardila Lule, FECOLPER, FLIP, Grupo Prisa, Grupo Santodomingo, Medios de información, Monopolio de los medios de información en Colombia, Reporteros Sin Frnteras, Sarmiento Angulo
Slug: tres-grupos-economicos-tienen-la-mayoria-de-los-medios-de-comunicacion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/MOM_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio 

###### [15 Oct 2015] 

Los resultados del Monitoreo de la Propiedad de los Medios en Colombia MOM (Media Ownership Monitor) realizado por Reporteros Sin fronteras y Fecolper con reveló que la concentración de los medios en Colombia está en manos de **8 grupos económicos de los cuales 3 tienen el 57% del control en el mercado en la prensa,** **la radio y la televisión lo cual impacta el derecho a la libertad de prensa.**

El balance de este proyecto realizado en medios tradicionales y en la web reveló que la tradición política en los medios está presente y en la medida que en las mesas directivas de estos medios existe participación de personas que tienen una **“rotación permanente en cargos públicos”, además que estos grupos que manejan los medios participan en la financiación de algunas campañas**, presentando así “un impacto a la libertad de prensa y a la independencia periodística”, como indicó Johana Silva, coordinadora del proyecto.

### **Prensa:** 

En materia de prensa el monitoreo de propiedad de los medios reveló que el Q´hubo, el ADN, El Tiempo y Al día son los periódicos más leídos, cubriendo aproximadamente un 70% de lectores en el país y además evidenciando un aumento “*en el consumo en la prensa popular y gratuita*”, como indicó Silva.

Llamó la atención que ColPrensa es la única agencia de noticias colombiana y que cuenta con presencia de grupos económicos como los Lloreda, vinculados al  diario el País de Cali, Los Galvis, vinculados a el periódico Vanguardia en Santander y los Gómez-Hernández vinculados al Colombiano en Antioquia.

### **Televisión:** 

En cuánto al sector de la televisión se evidenció que los grupos **Ardilla Lulle y Santo Domingo concentran el 74% de la audiencia de la televisión colombiana**, con los canales RCN y Caracol, *“los canales regionales y locales no tienen mayor relevancia considerando la cuota de audiencia”*, indica el informe.

Pese a que RCN y Caracol son los canales que tienen un mayor consumo en el país, el consumo de canales internacionales se ha hecho mayor, además según indica la coordinadora del proyecto Johana Silva a pesar de que **la Televisión Digital Terrestre llegó a Colombia “no ha transformado la concentración de los medios**”.

### **Radio:** 

El panorama que se presenta en la radio no es muy diferente a los anteriores, existe una **concentración del 73% en los grupos Ardila Lulle, Grupo Prisa y la Cadena Radial Olímpica**. Esta concentración ha provocado una ausencia en la pluralidad informativa, pero presenta una característica y es que la audiencias tiene una preferencia por las emisoras con programación musical, que de tipo informativo.

### **Digital:** 

Para evaluar la transparencia en la propiedad de estos medios se presentaron dificultades y anomalías principalmente en las regiones ya que existe una mayor información sobre canales, emisoras y periódicos pertenecientes a las grandes cadenas que a los de regiones. Nina Ludewing indica que “la dificultad para nosotros fue la falta de información pública disponible con los datos del mercado, la cual es de dominio público en muchos países, pero no aquí en Colombia”. Los realizadores del informe resaltaron la colaboración en este aspecto de los medios públicos e independientes.

Al final de la presentación el MOM resalta 4 puntos claves para continuar con esta labor: 1 Que la sociedad civil investigue más sobre el contenido de estos medios y quién brinda la información. 2 Que en base a los datos presentados en este informe se genere mayor transparencia. 3 Mejorar la aplicación a la ley de acceso a la información. 4 Que se realicen ajustes para una nueva ley de acceso a la información.
