Title: Implementación de los Acuerdos requiere movilización social: Iván Cepeda
Date: 2017-03-13 13:29
Category: Entrevistas, Paz
Tags: Fast Track, Implementación de los Acuerdos de paz
Slug: implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Mar 2017] 

La Jurisdicción Especial para la Paz y su implementación, tendrá su debate final hoy en el Congreso, organizaciones de víctimas y de veeduría ciudadana estarán haciendo presencia en la plenaria con la finalidad de **evitar la inasistencia de senadores y modificaciones al proyecto de ley en puntos como la responsabilidad de terceros en el conflicto armado y la cadena de mando de las Fuerzas Militares**, que de ser alteradas darían paso a la impunidad.

De acuerdo con el senador Iván Cepeda la función que tendrán hoy las bancadas en defensa de los acuerdos será defender el sistema de la Jurisdicción Especial para la Paz y de los organismos que componen ese sistema como lo son **la Comisión de búsqueda de la Verdad, la unidad para la Búsqueda de las personas desaparecidas y el conjunto de mecanismo de no repetición. **Le puede interesar: ["Del Fast Track al Slow Track"](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/)

“Vamos a defender el sistema de quienes quisieran que en Colombia el proceso de paz no tuviera que ver con una rendición de cuentas de militares, **agentes estatales y particulares que han tenido activa participación en el conflicto armado y que tienen responsabilidad en la comisión de crímenes de lesa humanidad**” enfatizó Cepeda y agregó que estas personas deben asumir su responsabilidad y ser sancionados por la misma.

Referente a la bancada del Centro Democrático en el Congreso, Cepeda señala que son ambiguos **“por una parte hablan de que la justicia colombiana ha perseguido al Uribismo y por otra cuando se crea la JEP dicen que es preferible la justicia ordinaría”**, además señaló que esta contradicción viene del “temor” a que exista una justicia independiente que llame a rendir cuentas.

### **Operación Tortuga en el Fast Track** 

Las demoras e incumplimientos por parte de los congresistas a la hora de asistir a los debates del Fast Track, han retrasado el avance de la implementación de los acuerdos, por este motivo la ciudadanía hará presencia durante el último debate de la JEP, para presionar a los congresistas en lo que de acuerdo con Carlos Monetegro, integrante de Ojo a la paz, **“es su deber”. **Le puede interesar: ["Crece el riesgo para los acuerdos de paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

Iván Cepeda, señalo que el papel para la sociedad en estos momentos es fundamental, **“no hay que engañarnos, la implementación requiere de la movilización social”** a su vez el senador hizo un llamado a la sociedad para no ser simples espectadores en este proceso.

### ** La veeduría de Ojo a la paz** 

Desde las dos de la tarde a ciudadanía y organizaciones del movimiento social, estarán a las afueras del Congreso, en la Plaza de Bolívar al tanto de qué congresistas asistan al debate y permanecen en el, **para votar finalmente en el debate de la Jurisdicción Especial para la Paz.** Le puede interesar: ["Senadores pretenden aplicar operación tortuga a Jurisdicción de Paz"](https://archivo.contagioradio.com/fast-track-operacion-tortuga-jep/)

El ejercicio consistirá en una carrera de tortugas, se diseñarán unos carriles y cada partido político tendrá uno, con su respectivo logo, las tortugas tendrán a su vez los nombres de los congresistas que participan en el debate, **los que se retrasen o no lleguen estarán desde el inicio de la carrera.**

Carlos Montenegro, integrante de Ojo a la paz, afirmó que estos ejercicios empoderan a la ciudadanía y le hacen comprender el roll de la democracia “**no solo se trata de exigir nuestros derechos, sino también de ejercer nuestros deberes**, sobretodo en el trámite de leyes y más en esta coyuntura”, indicó Montenegro. Le puede interesar: ["Duro reclamo de las FARC al gobierno por incumplimientos al acuerdos"](https://archivo.contagioradio.com/duro-reclamo-de-las-farc-al-gobierno-por-incumplimientos-al-acuerdo/)  
<iframe id="audio_17514158" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17514158_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
