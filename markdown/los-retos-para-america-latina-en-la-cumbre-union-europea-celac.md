Title: Los retos para América Latina en la Cumbre Unión Europea-CELAC
Date: 2015-06-09 13:01
Category: Economía, El mundo
Tags: CELAC, cumbre, Evo Morales, Rafael Correa, tribunal permanente de los pueblos, Unión europea
Slug: los-retos-para-america-latina-en-la-cumbre-union-europea-celac
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CELAC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Universal 

<iframe src="http://www.ivoox.com/player_ek_4616405_2_1.html?data=lZuemJmUeY6ZmKiakp6Jd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjNfS1tTXb9HV08aYo9KJh5SZop7fy8jFb63V1c7bw5DJsozgwpCw19LGtsafttPWh6iXaaOnz5Cy19fTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Aguiar, Campaña Global para Desmantelar el Poder de las Trasnacionales] 

######  

###### [9 jun 2015] 

En el marco de la Cumbre Unión Europea - CELAC que se realiza en Bruselas del 8 al 10 de junio, más de 50 organizaciones y plataformas sociales de Europa y América Latina ha convocado a una serie de actividades de discusión y de movilización para denunciar la arquitectura económica trasnacional que se desarrolla en este tipo de encuentros, y que viola la autodeterminación de los territorios.

Para Diana Aguiar, de la Campaña Global para Desmantelar el Poder de las Trasnacionales, en la Cumbre UE-CELAC se negocian tratados de libre comercio y beneficios a empresas trasnacionales, principalmente europeas, que avanzan sobre América Latina. El objetivo de las actividades de los "Días de movilización", comenta Diana, es "denunciar esta agenda, pero por otro lado mostrar la resistencia de los pueblos europeos sobre las políticas de austeridad, y enseñar que los pueblos resisten e intercambiar estrategias".

Habría entonces un reto para los gobierno progresistas latinoamericanos que hacen parte de la CELAC, que participan en la Cumbre pero que vienen de los movimientos sociales, como el del presidente Evo Morales en Bolivia, y Rafael Correa en Ecuador: "Es una dicotomía que estos nuevos gobiernos que han estado más al lado de los pueblos en muchos términos, que vienen de las luchas de los pueblos, por otro lado, cada vez más promuevan el capital privado y el capital de Estado".

<div>

<div>

"El comunicado muy claro a los gobiernos es que rechacen, y terminen las negociaciones de Libre Comercio. Que no es posible que 10 años después de la victoria contra el ALCA tengamos que estar todavía luchando contra nuevas generaciones de Acuerdo, todavía más fuertes de las que se presentaron en el pasado", declara Aguiar.

Las organizaciones sociales también demandan es que se logre concretar en la ONU la firma de un tratado histórico vinculante en términos de Derechos Humanos, promovido por Bolivia, Venezuela y Cuba. "Que los gobiernos paren la hipocresía de decir que están a favor de los DDHH, cuando no incluyen normas vinculantes sobre las empresas trasnacionales en términos de Derechos Humanos". Según afirma Aguiar, EEUU, Canadá y Australia se han opuesto en bloque a la discusión y firma de un tratado en ese sentido, porque son los países con las mayores empresas trasnacionales.

La agenda de los "Días de movilización", del 8 al 10 de junio, incluyen paneles de discusión como "Reclamando la Soberanía de los Pueblos contra la Arquitectura de Comercio e Inversiones promovida por las transnacionales", "Nueva Generación de Tratados de Libre Comercio y sus impactos en América Latina y Europa" y "Reclamando la Soberanía de los Pueblos por el Acceso a la Justicia".

<p>
\[caption id="attachment\_9881" align="aligncenter" width="400"\][![Bruselas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Bruselas.png){.size-full .wp-image-9881 width="400" height="195"}](https://archivo.contagioradio.com/los-retos-para-america-latina-en-la-cumbre-union-europea-celac/bruselas/) Bruselas\[/caption\]

</div>

</div>
