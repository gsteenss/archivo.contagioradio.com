Title: Informe de Amnistía Internacional 2014/2015
Date: 2015-02-25 23:16
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Amnistía Internacional, Informe DDHH Amnistía Internacional 2014, Violaciones DDHH Amnistía internacional
Slug: informe-de-amnistia-internacional-20142015
Status: published

###### Foto:E5ln.blogspot.com 

Amnistía Internacional ha presentado su **informe anual 2014/2015 sobre violaciones de derechos humanos en 160 países**. A pesar de que en determinados casos ha habido una mejora, **las estadísticas muestran un empeoramiento general en los 160 países donde la ONG ha podido realizar investigaciones**.

En total en **18 países** se han cometido **crímenes de guerra** o de las leyes internacionales sobre la guerra o el estatuto de Roma.

En **35** de los países investigados **hubo grupos armados** que cometieron violaciones de los DDHH y del derecho internacional, en total un 20% del conjunto del informe.

Tampoco mejoró la situación de los refugiados y desplazados, con mención especial para la guerra de **Siria donde de los 4 millones de refugiados el  95% huyeron a países vecinos**.

Según AI la situación entre las **fronteras de Europa y África y Asia en concreto en el mediterráneo se agravó durante 2014, con una cifra de 3.400 personas ahogadas en el mar.**

Otra parte del informe hace referencia a la **libertad de expresión, en total 119 de los 160 países la restringieron junto con ataques a la libertad periodística**.

En el **58% de los países realizaron prácticas injustas en el ejercicio de la justicia, así como 62 del total juzgaron y encarcelaron a personas por delitos relacionados con la objeción de conciencia**, o sea personas que cumplían sus derechos y libertades. En el transcurso de las detenciones el **82% de los países cometieron torturas.**

Por último **28 de los países tienen leyes que prohíben el aborto aún en casos de peligro de la vida humana o de violación. Del total 78 países tienen leyes que discriminas las relaciones o el sexo entre homosexuales o personas del mismo sexo.**

**Informe completo**: http://bit.ly/1Dq8sMb
