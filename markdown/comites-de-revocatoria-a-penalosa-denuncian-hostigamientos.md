Title: Comités de revocatoria a Peñalosa denuncian hostigamientos
Date: 2017-01-23 15:04
Category: Movilización, Nacional
Tags: Alcaldía de Bogotá, Peñalosa, Unidos revocaremos a Peñalosa
Slug: comites-de-revocatoria-a-penalosa-denuncian-hostigamientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/unidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidos revocaremos a Peñalosa] 

###### [23 Ene 2017] 

En rueda de prensa adelantada en el Consejo de Bogotá, los comités que buscan la revocatoria del alcalde Enrique Peñalosa **denunciaron el hostigamiento y persecución del que están siendo víctimas los voluntarios** que recogen las firmas. De acuerdo con las vocerías, miembros de la fuerza pública destruyeron algunas planillas de los voluntarios, mientras que otros han sido detenidos en el proceso de recolección.

En la exposición del pliego de motivos para exigir la revocatoria, los voceros señalaron que esta iniciativa surge de la “**preocupación por la profundización de un modelo de ciudad, que en vez de concentrarse en el beneficio colectivo, se transformó en un negocio** que deja ganancias para las minorías y pérdidas para las mayorías”. Le puede interesar: ["Listo cronograma para recolección de firmas de la revocatoria a Peñalosa"](https://archivo.contagioradio.com/listo-cronograma-para-recolectar-firmas-de-la-revocatoria-a-penalosa/)

Según lo expresado por los comites, la inciativa se sustenta en hechos como el **desalojo del Bronx sin un acompañamiento por la defensa de los derechos humanos**, la disminución en presupuesto o cancelación de proyectos que fomentaban la cultura en la capital como los Centros Locales de Arte para la niñez y la juventud (Clanes), la apuesta por construir sobre la reserva Van Der Hammen, la venta de la empresa pública ETB, situaciones que han **generado un rechazo mayoritario al interior de la ciudadanía**  y una afectación a la población de escasos recursos.

Finalmente anunciaron la unión de los comités “**Recuperemos a Bogotá” y “Unidos revocaremos a Peñalosa**” para tener mayor éxito en el trabajo de recolección de firmas y concluyeron señalando que la máxima finalidad de esta actividad que “pone en **ejercicio el papel de la ciudadanía**” es que todas y todos se apropien de las problemáticas de la ciudad. Le puede interesar: ["Estas son las razones para revocar a Peñalosa"](https://archivo.contagioradio.com/estas-las-razones-la-revocatoria-enrique-penalosa/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
