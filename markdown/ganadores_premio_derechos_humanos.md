Title: Estos son las y los ganadores del Premio Nacional a la Defensa de los Derechos Humanos
Date: 2017-09-19 11:46
Category: DDHH, Nacional
Tags: Diakonia, Premio Nacional a la Defensa de los Derechos HUmanos
Slug: ganadores_premio_derechos_humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/prensa-rural.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Rural 

###### [19 Sep 2017] 

Luego de la recepción de 71 historias de 18 departamentos de Colombia, este martes se llevó a cabo la ceremonia de **la sexta versión del Premio Nacional a la Defensa de los Derechos Humanos.** El premio otorga un reconocimiento público en 4 categorías que buscan visibilizar el aporte de los y las defensoras a la construcción de democracia y paz en Colombia.

### **Categoría 1: "Defensor o defensora del año"** 

**Enrique Chimonja Coy**- Integrante de la Comisión Intereclesial de Justicia y Paz, defensor de los derechos humanos en Buenaventura y el Valle del Cauca. Él realiza un acompañamiento a las víctimas de violaciones de los derechos humanos y violaciones al Derecho Internacional Humanitario.

### **Categoría 2A: "Experiencia o proceso colectivo del año (proceso social comunitario)"** 

**Asociación Sutsuin Jiyeyu Wayuu, Fuerza de Mujeres Wayuu, SJW-FMW-** Esta organización visibiliza las violaciones a los derechos humanos y los derechos étnicos de la Guajira. Han denunciado los mega proyectos mineros, el desplazamiento forzado y la situación de vulneración de derechos de las mujeres indígenas.

### **Categoría 2B: "Experiencia o proceso colectivo del año (modalidad de ONG)"** 

**Corporación Regional para la Defensa de los Derechos Humanos (CREDHOS)-** Promueve la defensa de los derechos humanos, la democracia y el DIH. Busca además favorecer a los sectores vulneraBles y victimizados de Barrancabermeja y el Magdalena Medio.

### **Categoría 3: Reconocimiento a “Toda una vida”** 

**Socorro Aceros Bautista-** Lideresa que ha trabajado por la promoción y la defensa de los derechos humanos acompañando a víctimas del paramilitarismo en el municipio de Tame en Arauca.

El Programa Colombia de Diakonia, y este año en conjunto con la Iglesia Sueca, ha otorgado este premio desde el año 2012 **como un mecanismo de respaldo al trabajo que realizan los defensores de derechos humanos y del territorio en el país**. Como en años pasados, el reconocimiento a los hombres, mujeres y organizaciones que contribuyen a la democracia y la paz se hará como parte de un mecanismo de protección ante la alarmante situación de riesgo en la que viven estas personas en Colombia. (Le puede interesar: ["Denuncia 335 agresiones a defensores de DDHH en 2017 "Agúzate"](https://archivo.contagioradio.com/somos-defensores-agresiones-en-2017-aguzate/))

###### Reciba toda la información de Contagio Radio en [[su correo]
