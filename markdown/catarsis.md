Title: Catarsis
Date: 2017-12-23 08:55
Category: Columnistas invitados, Opinion
Slug: catarsis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/catarsis.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-13-at-14.46.20.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: [Revistas Excelencias]

#### **[Por Angela Galvis Ardila]** 

###### 23 Dic 2017

Diciembre es sinónimo de sentimientos bonitos, de paz, de reencuentros, de reconciliación, de familia, de amigos, de entrega, pero también de nuevos propósitos, de metas por cumplir, de páginas por pasar, de hacer catarsis. Y por eso, hoy como parte de ese proceso, hago mi lista de cosas que logran agobiarme y que, como es época también de compartir, comparto hoy pidiendo que el año que viene logre superarlas y lleguen a no importarme , o al menos, a que me importen menos.

-   [Detesto a los que creen que a alguien le importa lo que les agobia.]
-   [Detesto todo cuanto tenga que ver con la estética y cultura mafiosa.]
-   [Detesto a esos políticos ladrones y corruptos que no les sacia el estómago ni una montaña de dinero.]
-   [Detesto a esos niños con ínfulas de adultos.]
-   [Detesto a los viejos que se creen adolescentes y van por la vida haciendo el ridículo.]
-   [Detesto a quienes creen que sus gustos personales son mejores que los de los demás.]
-   [Detesto a esas mujeres que creen que haber parido un hijo casi que las santifica y tienen por ese solo hecho más derechos que los papás.]
-   [Detesto a quienes viajan a algún país o a alguna región y vuelven hablando con más acento que los propios del lugar.]
-   [Detesto a quienes van a comer a cine como puercos.]
-   [Detesto al ateo militante que va gritando por el mundo que lo es como si eso le diera un plus.]
-   [Detesto al creyente fanático.]
-   [Detesto al vegetariano y al vegano que se encarga de que todos se enteren de lo bien que come.]
-   [Detesto al que le molesta el humo del cigarrillo y hace el gran escándalo cuando apenas lo huele.]
-   [Detesto al abstemio que mira con cara de horror un trago y a quien se lo toma.   ]
-   [Detesto a todo aquél que se autodenomina de algo ya sea bueno o malo.]
-   [Detesto el absolutismo.]
-   [Detesto a todo aquél que frente a alguien distinto y que no encaja dentro de su moral y sus buenas costumbres se persigna.]
-   [Detesto a esa gente que dice media frase en español y media en inglés.]
-   [Detesto a los de servicio al cliente que con el mismo tono de voz siempre entienden el problema pero jamás lo solucionan.]
-   [Detesto a quienes creen que no decir malas palabras es ser decente.]
-   [Detesto a esos que van pregonando su felicidad en redes sociales pero en el fondo uno sabe que solo es una tristeza disfrazada.]
-   [Detesto a los resentidos de clase.]
-   [Detesto a esos que opinan creyendo que le hacen un favor a la humanidad con sus elucubraciones.  ]
-   [Detesto a quienes se ríen con un hahaha como si se rieran en inglés.]
-   [Detesto a quienes al hablar de arte (cualquiera) cambian hasta el tono de voz y ponen pose como si se subieran a un pedestal.]
-   [Detesto a quienes creen que madrugar es una virtud.]
-   [Detesto a los que desprecian el ocio.]
-   [Detesto a esos que siempre quieren mostrarse ocupados y que, por lo general, nunca lo están.]
-   [Detesto esas conversaciones donde el tema es el colegio en donde estudian los hijos y luego siguen con la empleada del servicio y lo inservible que es.]
-   [Detesto el uso del verbo colocar porque dizque quienes ponen son las gallinas.]
-   [Detesto a quienes se burlan del feminismo y creen que se reduce a buscar un lenguaje incluyente.]
-   [Detesto a las feministas infumables.]
-   [Detesto la palabra infumable.]
-   [Detesto los chistes machistas y misóginos.]
-   [Detesto a los hombres maltratadores.]
-   [Detesto a las mujeres que creen tener una patente de corso y tratan mal a los hombres y ponen el grito en el cielo cuando les responden igual.]
-   [Detesto a la gente que se cree muy buena y habla por lo general en nombre de la bondad como si le hubiera sido adjudicada por algún mandato.  ]
-   [Y, finalmente, detesto las listas hechas con alguna excusa para esconder las amarguras de quien las hace.  ]

[Feliz Navidad o para decirlo como ahora parece ser la costumbre en este país donde hablamos español: Merry Christmas.]
