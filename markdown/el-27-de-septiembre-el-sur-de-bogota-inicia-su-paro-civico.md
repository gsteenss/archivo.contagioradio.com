Title: El Sur de Bogotá inicia paro cívico
Date: 2017-09-13 15:41
Category: Movilización, Nacional
Tags: Basurero doña juana, Paro 27 septiembre
Slug: el-27-de-septiembre-el-sur-de-bogota-inicia-su-paro-civico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Relleno-Doña-Juana-1200x630-e1503006517453.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo Particular] 

###### [13 Sept 2017] 

Los habitantes de diferentes barrios del sur de Bogotá, están convocando a la gran movilización del paro cívico Tunjuelo “Sur pone Norte”, el próximo 27 de septiembre, con la finalidad de hacer un llamado no solo a la Alcaldía de Bogotá, bajo la administración de Enrique Peñalosa, sino a la ciudadanía en general de la capital, **frente a las precarias condiciones en las que tienen que vivir,** soportando los malos olores del basurero Doña Juana, las enfermedades y l falta de medidas urgentes.

Desde el pasado mes de agosto los habitantes de barrios como Mochuelos, y 130 más que colindan con el basurero Doña Juana, salieron a bloquear las vías del botadero para evidenciar la crítica situación que atraviesan con la reproducción **descontrolada de moscas y zancudos que se encentran en sus casas producto del mal manejo de las basuras.**

A esta situación se le suman las denuncias sobre grietas que estarían abriéndose en las basuras que son cubiertas por telas. Además, muy cerca de allí está la cuenca del río Tunjuelo, completamente afectada por los lixiviados que son arrojados allí, según denuncian los habitantes. (Le puede interesar:["Habitantes de más de 100 barrios del sur protestan contra Relleno Doña Juana"](https://archivo.contagioradio.com/habitantes-de-mas-de-100-barrios-del-sur-de-bogota-protestan-contra-relleno-sanitario-dona-juana/))

De otro lado, las comunidades señalaron que podrían ser más de 3 millones de personas las que todos los días se ven afectadas por el basurero y con mayor frecuencia las afectaciones se reflejan en la salud de los menores de edad y **los adultos mayores, que padecen de diferentes enfermedades entre las que se resaltan las respiratorias.**

Sin embargo, frente a estas problemáticas, los habitantes expresaron que las medidas tomadas por el alcalde Peñalosa son insuficientes y no solucionan "el problema de fondo", además el **Distrito habría anunciado la ampliación hasta el 2070 de la existencia del relleno Doña Juan**a. (Le puede interesar: ["Distrito ampliaría hasta 2070 la existencia del Relleno Sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

Se espera que para el próximo 27 de septiembre, hora cero del inicio del paro, los habitantes de los más de 130 barrios afectados por el Basurero Doña Juana, las comunidades campesinas que habitan en cercanías a este lugar y los municipios que colindan se junten a esta actividad para exigir que se tomen medidas urgentes y se declare en estado de emergencia al basurero.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
