Title: Estado no ha indemnizado a dos familias por ejecución extrajudicial de sus hijos
Date: 2017-08-15 16:38
Category: DDHH, Judicial
Tags: CIDH, Comisión Colombiana de Juristas, Ejecucion extrajudicial, estado
Slug: estado-no-ha-indemnizado-a-familias-por-desaparicion-forzada-de-sus-hijos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Ago. 2017] 

Luego de siete años de que la Corte Interamericana de Derechos Humanos **(CIDH) declarara al Estado colombiano responsable de la desaparición forzada** y posterior ejecución extrajudicial de dos personas, a manos de agentes del F-2 de la Policía Nacional en Manizales, Caldas y ordenara pagar una indemnización, las familias siguen esperando que se cumpla con este fallo.

Los hechos sucedieron el 22 de marzo de 1988, cuando **James Zapata Valencia y José Heriberto Ramírez fueron retenidos por hombres que se identificaron como integrantes del F-2**, luego de ese suceso sus familias no volvieron a tener conocimiento de ellos.

Al activar los mecanismos de búsqueda, varios días después sus familiares supieron de ellos gracias a una publicación en un medio de comunicación, en el que eran presentados como "N.N.". Le puede interesar: [Luego de 30 años familiares del Palacio de Justicia siguen sin reparación integral](https://archivo.contagioradio.com/luego-de-30-anos-familiares-del-palacio-de-justicia-siguen-sin-reparacion-integral/)

**James y José fueron hallados con signos de tortura y baleados en una finca conocida como “Taparcal”,** en un municipio cercano de Caldas. Por tal motivo, la familia emprendió un proceso con el Estado en 1991 para hallar justicia, verdad, reparación y garantías de no repetición.

### **Todo se hizo pero el Estado no ha pagado** 

Según la Comisión Colombiana de Juristas que lleva el proceso jurídico de las familias, en 2012 se inició un proceso de conciliación para lograr el pago de la indemnización. Le puede interesar: [Estado no garantiza acceso a salud y atención psicosocial a víctimas del conflicto](https://archivo.contagioradio.com/estado-no-garantiza-el-acceso-a-la-salud-de-victimas-del-conflicto/)

Luego de “múltiples dificultades con las entidades estatales se aprobó la conciliación, pero cuando todo parecía estar listo para el desembolso, el **DAPRE asegura haber encontrado un error de \$100 mil pesos en la cuenta de cobro** y solicitó al Tribunal Administrativo de Cundinamarca corregir el documento”.

Pese a la solicitud hecha por el DAPRE hasta el momento no se ha realizado la corrección y dicha entidad manifiesta que no realizará el pago hasta que no se cambie el documento.

### **La situación de las familias** 

Mientras se continúa esperando en medio de la "tramitología" el pago de la indemnización, **los padres de José Heriberto Ramírez fallecieron, sin alcanzar justicia por la ejecución extrajudicial** y sin recibir la atención del Estado.

Por su parte, **Mariscela Valencia de Zapata, madre de James, se encuentra en un grave estado de salud** “por lo que el pago no solo ayudaría a disminuir la carga económica que genera su atención médica, sino que permitiría ver asomo de justicia casi 30 años de sucedidos los hechos”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
