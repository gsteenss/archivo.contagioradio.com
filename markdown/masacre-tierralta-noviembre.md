Title: Masacre en Tierraalta Córdoba, asesinan a 3 personas
Date: 2020-11-15 15:00
Author: AdminContagio
Category: Actualidad, DDHH
Tags: cordoba, cordobexia, Tierralta
Slug: masacre-tierralta-noviembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/tierraltasantaferalito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: La silla vacia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este viernes 12 de noviembre, en las veredas Lorenzo y Baquito en la zona rural del municipio de Tierralta, Córdoba, se presentó el asesinato de tres personas de una misma familia, este caso fue denunciado por la Fundación Cordoberxia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordoberxia, aseguró que los hechos ocurrieron en el municipio de Tierralta en donde fueron asesinados un hombre, su esposa y la hija de ambos, aunque la hija fue abordada en un lugar diferente al de los padres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas fueron los esposos Tomás Correa y Unilda Díaz quienes fueron asesinados en Lorenzo y su hija, Luisa Fernanda Correa Díaz, quien fue asesinada en El Banquito. Cordoberxia explicó que los asesinados pertenecían al Programa Nacional Integral de Sustitución de Cultivos Ilícitos (PNIS), creado en 2017 como componente del acuerdo de paz firmado un año antes con la guerrilla de las Farc. Lea también: [Desde la firma del acuerdo han sido asesinados 7 líderes del PNIS en Córdoba](https://archivo.contagioradio.com/liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La población de Tierralta, municipio del sur de Córdoba, está en riesgo permanente por la presencia de diversos grupos armados ilegales como las Autodefensas Gaitanistas de Colombia (AGC), herederas de los grupos paramilitares y disidentes de la antigua guerrilla de las Farc. Lea también: [Alerta máxima para líderes sociales](https://archivo.contagioradio.com/liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varias organizaciones defensoras de derechos humanos como la Asociación Campesina Para El Alto Sinú -ASODECAS- rechazaron y denunciaron ante la comunidad nacional e internacional los actos de violencia que se han venido generando en los territorios. Así mismo exigen al gobierno en cabeza de Iván Duque, para que combata las bandas criminales que operan en la región, y les brinde garantías de seguridad a los campesinos y líderes sociales.

<!-- /wp:paragraph -->
