Title: Hoy se vence el plazo para que Fiscalía entregue pruebas en caso Jesús Santrich
Date: 2018-10-02 12:22
Author: AdminContagio
Category: Judicial, Paz
Tags: Fiscalía, Jesús Santrich, Néstor Umberto Martínez
Slug: hoy-se-vence-el-plazo-para-que-fiscalia-entregue-pruebas-en-caso-jesus-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Jesus-Santrich-en-entrevista-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [2 Oct 2018]

Aunque desde el momento de la captura del líder del partido político FARC, conocido como Jesús Santrich, el Fiscal General de la Nación anunció que había pruebas contundentes de su responsabilidad en el delito de narcotráfico, **hasta la fecha no se han entregado dichas evidencias a la Jurisdicción Especial de Paz** (JEP), que debe dar su concepto en torno a si los crímenes se cometieron antes o después de la firma del acuerdo.

El abogado Gustavo Gallardo, cabeza del equipo de defensa del integrante de FARC, aseguró que el plazo establecido para que la JEP reciba las pruebas es este 2 de Octubre y que hasta el momento, a pesar de los anuncios del Fiscal Martínez, **ningún material probatorio ha sido allegado al despacho que lo solicita**. En caso de no entregarse las pruebas ese tribunal podría resolver que no procedería el trámite de extradición. (Le puede interesar: ["Captura de Jesús Santrich es un montaje: Iván Marquez"](https://archivo.contagioradio.com/captura-de-jesus-santrich-es-un-montaje-ivan-marquez/))

### **Fiscalía no ha entregado pruebas a la JEP a pesar de haberlas anunciado** 

Gallardo también hizo énfasis en que la carta de la Fiscalía, conocida la semana pasada en la que se afirma que no hay pruebas contra Seuxis Paucias Hernández Solarte, sino solamente contra Marlon Marín, fue una respuesta a una orden de la Corte Constitucional, y una respuesta a una exigencia de la JEP, que por quinta vez solicitaba las pruebas de manera oficial. (Le puede interesar: ["Las razones de la JEP para detener la extradicción de Jesús Santrich"](https://archivo.contagioradio.com/las-razones-de-la-jep-para-detener-la-extradicion-de-jesus-santrich/))

El jurista también resaltó que, aunque este 1 de octubre el fiscal anunció la entrega de 12 audios, estos no han sido entregados de manera formal y por tanto la defensa tampoco ha tenido acceso a estos. Si la Fiscalía entrega las pruebas, la JEP entraría en un periodo de análisis en el que determinaría si hay o **no competencia en el marco del acuerdo de paz firmado por las FARC y el Gobierno Nacional**.

### **Así ha sido el caso de Jesús Santrich desde su captura** 

El líder del partido político y antes guerrilla FARC, fue capturado el pasado 9 de abril en su lugar de residencia en la ciudad de Bogotá, en medio de un operativo, que habría sido ordenado por la DEA, en el marco de un proceso que se seguiría en la Corte del Distrito Sur de Nueva York, con cargos por tráfico de estupefacientes, en una operación que se habría realizado en Colombia.

Una vez conocidos los cargos, . (Le puede interesar: ["Jesús Santrich en cuidados intensivos luego de 18 días de huelga de hambre"](https://archivo.contagioradio.com/jesus-santrich-cuidados-intensivos-luego-18-dias-huelga-hambre/))

El 2 de Octubre se cierra el plazo para que la JEP conozca las pruebas y determine si los delitos se cometieron antes o después de la firma del acuerdo, es decir con posterioridad o anterioridad a diciembre de 2016. Hasta el 31 de octubre la JEP tendrá plazo de pronunciarse y definir si el caso se asume en la JEP o si por el contrario podría proseguir el trámite de extradición.

<iframe id="audio_29075878" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29075878_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
