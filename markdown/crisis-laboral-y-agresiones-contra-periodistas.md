Title: Crisis laboral y agresiones contra periodistas
Date: 2017-02-09 11:28
Category: Carolina, Opinion
Tags: agresiones a periodistas, periodismo, Periodistas
Slug: crisis-laboral-y-agresiones-contra-periodistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/periodistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

**Por: [Carolina Garzón Díaz\*](https://archivo.contagioradio.com/carolina-garzon-diaz/)**

[Desde el año 2004 en Colombia se celebra el 9 de febrero como el **Día del Periodista y el comunicador.** Esta es la excusa perfecta para felicitar a quienes se dedican a la labor, conocer los informes sobre el estado de la libertad de prensa del país, hacer un poco de crítica (seguramente un poco menos de auto-crítica) y reflexionar sobre este oficio que despierta tantas pasiones: amor, odio, incomodidad, vergüenza, esperanza, frustración, desdén y pasión. Porque siendo sinceros, en el mundo del periodismo y la comunicación hay de todo un poco.]

[Pero con motivo de este día bien valdría la pena revisar dos situaciones que atraviesan la vida de la mayoría de las mujeres y los hombres que se dedican de lleno a la profesión de periodismo: en primer lugar, la situación laboral y sus condiciones de trabajo; y en segundo, las agresiones de las que son víctimas por ejercer su labor.]

[Según el Ministerio de Educación “anualmente se gradúan de las universidades colombianas cerca de 4.500 comunicadores sociales y periodistas, pese a que el mercado laboral para estos profesionales cada día es más escaso y competido.”. Está claro. Lamentablemente esta sobrepoblación de periodistas y comunicadores en vez de impulsar un ejercicio fuerte y diverso de la prensa, limita las posibilidades de empleos formales para los profesionales y viene la pauperización. Los recién graduados y hasta los más experimentados se ven obligados a regalar su trabajo o bajar sus aspiraciones salariales, lo que repercute en empleos cada vez más miserables.]

[Sumemos a la sobreoferta de profesionales el estereotipo que nos sigue a los periodistas y comunicadores: cualquiera puede hablar, cualquiera puede escribir, así que cualquiera puede hacerlo. Parece que nuestros cinco años de pregrado, más las especializaciones y maestrías que podamos tener no pesan lo suficiente.]

[En cuanto a las agresiones allí el panorama también es complicado. Con motivo del Día del periodista la Fundación para la Libertad de Prensa –FLIP- acaba de publicar su informe anual titulado “][[Silencioff, ¿las regiones tomarán la palabra?]](https://issuu.com/comunicaflip/docs/informe_flip_2016)[” el cual revela que, por primera vez en los últimos siete años, en el 2016 no se registró ni un solo asesinato contra periodistas por razón de su oficio en Colombia. Una excelente noticia. Sin embargo, hubo 216 violaciones a la libertad de prensa que afectaron a 262 víctimas. Entre estas agresiones se cuentan 5 secuestros.]

[Según este y otros informes presentados anteriormente, quienes más sufren estas agresiones y en dónde más se ven sus impactos son las regiones. Allí una amenaza o cualquier otra agresión significan callar un medio de comunicación, una voz, una investigación. Es dejar en silencio una población completa.]

[¿Qué ha pasado con las investigaciones por los crímenes ocurridos anteriormente? ¿Por qué siguen asesinando y amenazando periodistas? Es porque la impunidad reina en estos casos y la comisión de nuevas agresiones. Según la FLIP, de los 153 periodistas asesinados desde 1977 a hoy, sólo se han condenado cuatro autores intelectuales y 27 autores materiales; y no hay condenas por las amenazas u otras agresiones. Es decir, la impunidad en los casos de crímenes contra periodistas es del 99,36%.]

[No desconozco las responsabilidades que caen sobre nosotros los periodistas, siempre hay que ser crítico del propio oficio y de los colegas. Pero enfrentar consientemente estos dos escenarios expuestos anteriormente fortalecería el periodismo en Colombia, permitiría ejercer el derecho a la información por parte de todos y tener una mayor veeduría del Estado y los privados, aumentaría la participación ciudadana y, sin duda alguna, robustecería nuestra democracia. Una necesidad en estos tiempos de tránsito hacia la paz.]

###### [\*Comunicadora social y periodista con profundización en periodismo digital y sistemas convergentes. Columnista. Defensora de derechos humanos. Twitter: @E\_Vinna] 

#### [**Leer más columnas de opinión de  Carolina Garzón**](https://archivo.contagioradio.com/carolina-garzon-diaz/)

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
