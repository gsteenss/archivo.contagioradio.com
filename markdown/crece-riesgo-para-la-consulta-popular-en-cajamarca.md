Title: Crece riesgo para la Consulta popular en Cajamarca
Date: 2017-03-26 09:38
Category: Ambiente
Tags: Cajamarca, Mineria
Slug: crece-riesgo-para-la-consulta-popular-en-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2Orillas] 

###### [23 Mar 2017] 

**Con 18 mesas y un potencial electoral de 16.312 personas**, sin conocer los lugares de votación, con una desventaja por no contar con los recursos necesarios para realizar una consulta informada y votantes que tendrán que ocupar menos de un minuto, transcurrirá la consulta popular en Cajamarca, jornada electoral que tendría que durar cerca de **11 horas para que todos puedan ejercer su derecho al voto**.

Así la cosas, los riesgos para que se realice una consulta libre e informada y con garantías plenas para los electores el próximo 26 de Marzo, son cada vez más crecientes y cercanos. La **Misión de Observación Electoral, expresó su preocupación por las posibles congestiones en los puntos de votación.** Le puede interesar: ["Denuncian nuevos obstáculos para la Consulta Popular"](https://archivo.contagioradio.com/nuevos-obstaculos-para-la-consulta-popular-en-cajamarca/)

De acuerdo con Cesar Bocanegra, Delegado Departamental de la Registraduría Nacional, esta reducción de mesas para votación fue una determinación que tomo la entidad con base en las últimas elecciones atípicas para escoger alcalde en Cajamarca y resultados de otras votaciones en municipios como Cabrera, **que determinó que el potencial de votación solo era de 980 personas por mesa**.

Sin embargo, para las elecciones de alcalde había un potencial de sufragantes de 16.312 y 35 mesas de votación, **que para la consulta popular minera se reducirán a 18, 16 ubicadas en el caso urbano y 2 en veredas aledañas. **Le puede interesar: ["Nuevo Alcalde de Cajamarca promete garantizar Consulta Popular en Cajamarca"](https://archivo.contagioradio.com/nuevo-alcalde-de-cajamarca-promete-garantizar-la-consulta-popular-contra-la-mineria/)

### **Garantías para la Consulta Popular** 

La MOE, señaló que con el número de votantes en cada mesa, **se necesitaría aproximadamente una jornada de 11 horas de elección, que actualmente es de 8**, si la votación la realizaran las 980 personas por mesa y cada persona se demorara tan solo un minuto en realizar el proceso.

Cristian Martínez, representante regional de la MOE, indicó que es importante, pese a la reducción de las mesas, que se tengan **planes alternos en la votación para evitar las congestiones y que finalmente toda la población pueda votar**.

“Consideramos que uno es importante que se **tengan computadores de "infovotantes" adicionales, con personas para agilizar este trámite** y dos que se coloquen dos cubículos por mesa, o que las personas guías ayuden al elector a ubicarse” indicó Martinez. Le puede interesar: ["Llega el Festival Campesino por la defensa de la Consulta Popular"](https://archivo.contagioradio.com/listo-festival-campesino-por-la-defensa-de-la-consulta-popular/)

De igual forma la Registraduría informó que ampliará el personal que estará presente en los puntos de votación y los puestos de "infovotantes" para que las personas ubiquen mucho más rápido su lugar de votación. Frente al tiempo en el que se conocerán los resultados de votación, Bocanegra infiere que el dato del **100% del escrutinio se podrá conocer sobre las 4:45 pm** y agregó que a partir de mañana se publicarán las listas de los puntos de votación para informar a la comunidad.

### La pregunta: 

¿Está de acuerdo sí o no con que en el municipio de Cajamarca se ejecuten proyectos y actividades mineras?  
<iframe id="audio_17732807" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17732807_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_17732768" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17732768_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
