Title: Otra Mirada: Colombia y sus relaciones internacionales
Date: 2020-07-09 21:33
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Iván Cepeda, Políticas internacionales, Relaciones Internacionales
Slug: otra-mirada-colombia-y-sus-relaciones-internacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia - WikiCommons

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la pandemia, se está cuestionando el proceder del país con relación a sus relaciones internacionales debido a múltiples hechos como la intervención de entes internacionales ante el aumento en los asesinatos a líderes y defensores de derechos humanos, así como la negación a relatores de la ONU para entrar al país y la llegada de tropas de EE.UU. al territorio poniendo en tensión las relaciones con Venezuela.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, Gustavo Gallón, director Comisión Colombiana de Juristas, Diana Sánchez directora Asociación Minga, Laura Gil, politóloga internacionalista y directora de la Línea del Medio e Iván Zepeda, senador de la República de Colombia nos informaron cómo desde aspectos jurídicos y de derechos humanos, nos miran en la comunidad internacional. (Le puede interesar: [Congreso de EE.UU. pide que Trump presione a Duque para proteger a líderes sociales](https://archivo.contagioradio.com/congreso-de-ee-uu-pide-que-trump-presione-a-duque-para-proteger-a-lideres-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, nuestro panel de invitados respondió a preguntas tocantes a la evaluación que hacen de las relaciones internacionales actuales, qué consideran, se pueden aportar desde lo internacional a las organizaciones de derechos humanos en un momento en el que no pueden acceder al apoyo nacional y cómo pareciera que el gobierno ve sus obligaciones internacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respecto a esto, según Diana Sánchez, los tratados internacionales son de obligatorio cumplimiento y están dentro del ordenamiento jurídico, social y político del país; sin embargo, comenta que tenemos un Gobierno alérgico a la comunidad internacional en general. Tanto al sistema universal de Naciones Unidas-ONU como al regional del Sistema Interamericano de Derechos Humanos-CIDH.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Este Gobierno y su partido son como una logia cerrada, que solo se mira hacia dentro y no tiene capacidad de relacionamiento con el resto de sectores sociales y políticos nacionales ni internacionales».

<!-- /wp:quote -->

<!-- wp:heading {"customTextColor":"#056a95"} -->

¿En qué se debe enfocar la comunidad internacional? 
---------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

También explica que la nueva representante de la Oficina del Alto Comisionado de las Naciones Unidas en Colombia, Juliette de Rivero tendrá que asumir retos tales como la observación de la violencia y violación de los DD.HH y la implementación del Acuerdo de Paz; la situación de agresiones constantes a líderes y lideresas sociales y personas defensoras de DD.HH y finalmente los temas de pobreza, cobertura de salud y demás problemáticas sociales que afectan a los sectores más vulnerables de la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, el panel explica dónde se deben enfocar los esfuerzos y cómo seguir avanzando para defender una agenda que no es del gobierno. (Si desea escuchar el programa del 6 de julio: [Hablemos de la Fiscalía](https://www.facebook.com/contagioradio/videos/908498579669234))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/282066349783725","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/282066349783725

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
