Title: Más de 6 mil personas rechazan nombramiento de Vicente Torrijos como director del CNMH
Date: 2018-11-29 17:33
Author: AdminContagio
Category: DDHH, Nacional
Tags: Centro Nacional de Memoria Histórica, memoria, Vicente Torrijos
Slug: rechazan-vicente-torrijos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-29-a-las-5.18.02-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PosgradosPonal] 

###### [29 nov 2018] 

Más de 6 mil personas firman carta de rechazo a la designación de Vicente Torrijos, como nuevo director del **Centro Nacional de Memoria Histórica (CNMH)** por parte del presidente Iván Duque; en la misiva se argumenta que Torrijos no cumple con los requisitos para ocupar el cargo y por el contrario, dada su trayectoria personal y profesional, **se observa un sesgo de su parte hacía una versión de la historia del conflicto.**

En la comunicación, personas cercanas a instituciones de memoria nacionales y de américa latina recuerdan que el pasado 6 de noviembre listaron algunas condiciones que debería cumplir la persona designada como director del CNMH, entre ellas: que tenga una trayectoria de vida profesional y personal intachable, **que tenga altos méritos académicos comprobables y de calidad, y que posea una visión objetiva e imparcial sobre el conflicto colombiano**.

Pero según los firmantes, “Torrijos no reúne los requisitos de idoneidad y legitimidad necesarios para ocupar este cargo”, en tanto su trayectoria como asesor del Ministerio de Defensa y Consultor de las Fuerzas Militares puede afectar la visión imparcial sobre el conflicto; **a ello se suma la imprecisión sobre el grado de sus estudios doctorales, que parece no haber finalizado**.

En el documento se señala, además, **que son las víctimas, sus organizaciones y la sociedad civil en general quienes deben reconstruir la historia,** “a través de los mecanismos institucionales creados para garantizar sus derechos a la memoria y la verdad”, razón por la que es necesario que sean tomados en cuenta en la designación de los cargos directivos de estas instituciones. (Le puede interesar: ["Diecisiete años de memoria y resistencia: Páramo de la Sarna"](https://archivo.contagioradio.com/diecisiete-anos-memoria-paramo-la-sarna/))

Por estas razones, en la carta se expresa la oposición a este nombramiento, y concluyeron que este momento de la historia del país requiere compromiso y voluntad para construir un nuevo futuro, “en el que el Estado colombiano se abra a las víctimas y la sociedad, para promover una participación e inclusión real, necesarias para fortalecer la reconciliación nacional”, y por esta vía, garantizar la no repetición de los hechos.

[Rechazamos La Nueva Direcci...](https://www.scribd.com/document/394243983/Rechazamos-La-Nueva-Direccion-Del-CNMH#from_embed "View Rechazamos La Nueva Dirección Del CNMH on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Rechazamos La Nueva Dirección Del CNMH" src="https://www.scribd.com/embeds/394243983/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;access_key=key-N0v2TRCNNVUk8QfpSzBE" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
