Title: ¿A quién beneficia la liberación de tarifas de parquaderos en Bogotá?
Date: 2017-08-11 18:05
Category: Economía, Nacional
Tags: Alcalde Peñalosa, Enrique Peñalosa, Parqueaderos, Peñalosa
Slug: a-quien-le-sirve-la-liberacion-de-tarifas-de-parquaderos-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Parqueaderos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo] 

###### [11 Ago. 2017] 

Luego de conocerse que el Alcalde Enrique Peñalosa firmaría un decreto en el que liberaría las tarifas de los parqueaderos en Bogotá, la bancada del Polo Democrático en el Concejo de la Capital asegura que se opone a dicha medida dado que **los grandes afectados serían los propietarios de vehículos de estratos 1, 2, 3 y 4 **y además se favorecerían negocios de familiares, incluso del mandatario colombiano Juan Manuel Santos.

“Con esta política lo que haría es que se dispararían los precios de los parqueaderos haciendo más costosa la vida para los bogotanos. **El Alcalde Peñalosa ha venido dando una serie de razones que son otras de sus mentiras** para justificar este tipo de despropósitos” manifestó Manuel Sarmiento, Concejal del Polo Democrático Alternativo.

### **Anuncios del Alcalde son improvisados** 

Sarmiento se refiere a dos argumentos que han sido entregados por Peñalosa para poder sustentar ese decreto, uno de ellos es que **la mayoría de los que usan carro son los grandes magnates** y el segundo es que permitirá que haya libre competencia en el mercado. Le puede interesar:[ "Las tres razones por las que se cayó el proyecto de valorización de Peñalosa"](https://archivo.contagioradio.com/las-3-razones-por-las-que-se-cayo-proyecto-de-valorizacion-de-penalosa/)

**“La Secretaria de Movilidad desmienten ese tipo de afirmaciones al señalar que el 65% de los vehículos privados de Bogotá pertenecen a los estratos 1,2 y 3** y además el 80% de los viajes que se hacen cada día en vehículo particular los hacen personas de los estratos 1,2,3 y 4 (…) Este es otro de los anuncios del Alcalde que además de improvisado apuntan a mejorar el negocio a los parqueaderos”.

### **¿Quiénes estarían detrás de los negocios de los parqueaderos?** 

Existen algunas grandes firmas de los parqueaderos que podrían verse beneficiados con este tipo de decretos “yo he estado detrás de 3 alianzas público-privadas a los que el Alcalde Peñalosa ya les dio la factibilidad. **Ahí están los consuegros del presidente Juan Manuel Santos, un señor que se llama Mario Fernando Pinzón** y una persona que se llama Alicia Naranjo que son padres del novio o esposo de la hija de Santos”.

Además, denuncia el cabildante que Mario Fernando Pinzón le donó a la campaña del Alcalde \$50 millones de pesos y Alicia Naranjo trabajo en el primer gobierno del Alcalde Peñalosa como subdirectora del IDU para asuntos de espacio público. Le puede interesar: [Denuncian irregularidades en contratación con financiadoras de campaña del Alcalde Peñalosa](https://archivo.contagioradio.com/denucian-irregularidades-en-contratacion-con-financiadoras-de-campana-del-alcalde-penalosa/)

### **Parqueaderos serían en espacio público** 

Uno de esos parqueaderos según la denuncia de Sarmiento estaría ubicado en la Calle 100 con avenida 19 **“lo que haría el Distrito es que les entrega a estos señores esas plazoletas y ellos construyen** una serie de parqueaderos subterráneos y encima ponen locales comerciales. Es la privatización del espacio público”.

### **Acciones conjuntas para rechazar estas acciones del Alcalde** 

Eso es lo que propone Sarmiento pues asegura que, si bien ya han citado a un debate de control político e interpuesto una tutela en contra de este proyecto, la comunidad debe unirse para rechazar esta iniciativa.

**“Ojalá los vecinos envíen cartas, salgan a protestar si es posible.** Porque este es otro de los argumentos para entrar a trabajar en la revocatoria del Alcalde Peñalosa, que en realidad es un Alcalde que no tiene en una situación extrema con distintas medidas”.

Lo que ha manifestado el Alcalde es que para desestimular el uso del vehículo hay que subir las tarifas de los parqueaderos de modo que más personas utilicen el servicio público y la bicicleta. Le puede interesar: [Nuevo intento de Peñalosa por vender la ETB no sería aprobado por el Concejo](https://archivo.contagioradio.com/nuevo-intento-de-penalosa-por-vender-la-etb-no-seria-aprobado-por-el-concejo/)

Sin embargo, el Concejal manifiesta que este tipo de medidas no funcionan porque la única forma desestimular el uso del vehículo particular es teniendo un sistema de servicio público eficiente “y **ya todos sabemos que Transmilenio fracasó como eje estructurador del sistema de movilidad** y el Alcalde Peñalosa en lugar de corregir el error hace 20 años quiere profundizarlo”.

### **No hay plata para el Metro, pero si para Transmilenio** 

La Contraloría Distrital decidió abrir un proceso contra el Alcalde Peñalosa y el gerente del Metro para que se puedan explicar las razones por las que el actual mandatario decidió no usar los estudios contratados por la administración de Gustavo Petro y que ascendieron a \$146 mil millones. Le puede interesar: [En menos de 3 meses bogotanos podrán decidir futuro de Peñalosa](https://archivo.contagioradio.com/en-menos-de-3-meses-bogotanos-podran-decidir-futuro-de-penalosa/)

“El Alcalde Peñalosa está diciendo otra mentira y es que con esos estudios no se podía hacer el metro porque era muy costoso y ha dicho que costaría 18 billones. Yo estoy diciendo **cómo va a haber plata para gastarse 28 billones de pesos en Transmilenio y no va a haber plata para gastarse en 18 en el Metro** que es lo que necesita Bogotá”.

### **Pagar por no tener pico y placa es “elitista”** 

Si bien la bancada del Polo no votó a favor esta propuesta en el Plan de Desarrollo en Bogotá, si fue aprobada por votación unánime en el Concejo y que es vista dice Sarmiento como una medida “elitista, que le permite a quien tiene plata saltarse la fila, pero es una medida reaccionaria porque quien no tenga ingresos pues tiene que someterse”.

Todo parece indicar que **esta medida se convertirá próximamente en realidad, pese a las herramientas utilizadas en contra** de esta como los derechos de petición enviados a la Secretaria de Movilidad. Se calcula que la suma podría estar entre los 4 o 5 millones de pesos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
