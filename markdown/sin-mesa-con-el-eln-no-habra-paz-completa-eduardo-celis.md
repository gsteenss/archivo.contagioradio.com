Title: "Sin mesa con el ELN no habrá paz completa" Eduardo Celis
Date: 2016-12-02 13:29
Category: Paz, Política
Tags: Mesa en Quito, proceso de paz eln
Slug: sin-mesa-con-el-eln-no-habra-paz-completa-eduardo-celis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [2 Dic 2016] 

El último comunicado emitido por el ELN, ha dejado claro que la postergación del inicio de las conversaciones y la instalación de la mesa en Quito, se deben a los **inamovibles generados por el gobierno Nacional** y en específico a la liberación de Odín Sánchez de Oca. Además expresaron que el hecho ocurrido en el Choco contra la población de Birudó, **no fue realizado por ninguna de sus tropas.**

En el comunidado el ELN afirma que “la delegación del presidente Santos fijo posiciones inamovibles, con las que cerraron las posibilidades de diálogo, d**ejando a la mesa en un punto de estancamiento**”. Motivo que lleva a la guerrilla a realizar consultas y debates frente a cómo continuar en las conversaciones, para retornar hasta el 10 de enero con los diálogos. Le puede interesar:["El 10 de enero se retomarán diálogos con el ELN"](https://archivo.contagioradio.com/10-enero-se-retomaran-dialogos-eln/)

De acuerdo con el analista Luis Eduardo Celis, estas 5 semanas que se tomaran ambas partes deben servir para que se reconsidere la estrategia de actuación.  Por un lado está el ELN que debe decidir y escoger entre **“si mantenerse en la raya de no ceder en la entrega de Odín Sánchez o la apertura de una mesa en donde pueda generar un proceso de participación”** y por el otro lado está el gobierno que para Celis no se moverá de su exigencia de la liberación de Sánchez para continuar con las conversaciones.

No obstante estas 5 semanas se desarrollarán sin un cese al fuego bilateral, que da paso a que se sigan presentando acciones en medio de la guerra, dejando en la mitad a la sociedad civil.  Celis agrega que para estos momentos la labor que puede encarnar la sociedad y la **Mesa Social para la paz, es seguir presionando para que se instale la mesa en Quito** lo más pronto posible, sin embargo advierte que de no continuarse con el diálogo y no establecer una mesa con el ELN **“no habrá una paz completa, habrá un pulso militar y por supuesto se tendrán consecuencias humanitarias”.**

El comunicado de prensa cierra expresando que tanto el ELN como el gobierno, están de acuerdo en agilizar la puesta en marcha de la liberación de Odín Sánchez y que “pese a los obstáculos que existen para avanzar en una solución política al conflicto, tiene la firme convicción de que la paz ese es el mejor camino para Colombia”. Le puede interesar: ["Listos mecanismos de participación para la Mesa Social para la Paz"](https://archivo.contagioradio.com/listos-mecanismos-de-participacion-de-mesa-social-para-la-paz/)

<iframe id="audio_14447371" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14447371_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
