Title: Prohibición del paramilitarismo sería un paso necesario para la reconciliación
Date: 2017-08-23 15:08
Category: DDHH, Nacional, Política
Tags: Andi, Camilo González Posso, Empresarios, Fedegan, Gremios, INDEPAZ, paramilitares
Slug: prohibicion-del-paramilitarismo-seria-un-paso-necesario-para-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [23 Ago. 2017] 

A través de una carta, el Consejo Gremial Nacional que agrupa a diversas entidades como la Andi, Fenalco, Asobancaria, Fedegan, entre otras, le pidió a la Cámara de Representantes **no tramite el acto legislativo contemplado en el Acuerdo de Paz que busca prohibir el paramilitarismo** en Colombia.

Postura que según el **analista y director de Instituto de Estudios para el desarrollo y la paz – Indepaz – Camilo Gonzalez Posso,** es improcedente, dado que éste sería un paso importante a propósito del contexto de paz que está comenzando a trabajar el país y que exige garantías para las víctimas.

“Frente a eso el Senado de la República hizo una discusión seria y aprobaron un articulado bastante avanzado. Yo creo que es importante que en Colombia exista no solamente una prohibición constitucional de la conformación de estos grupos sino también un tipo penal, porque no existe”.

### **¿Si se aprueba este proyecto se estaría lesionando al Estado?** 

Contrario a lo manifestado por los gremios y algunos sectores empresariales frente al daño que podría significar este tipo de actos legislativos, juristas y constitucionalistas como Rodrigo Uprimny y Gustavo Gallón han dicho que cabe recordar otros ejemplos como que en la constitución están proscritos la tortura, la desaparición, el desplazamiento forzado y con eso se está haciendo un pacto de sociedad.

“También se establecen muchos preceptos sobre libertades, desde el propio enunciado de la Carta Política y en muchas constituciones del mundo por ejemplo se prohíbe la esclavitud y eso no ya existe hace muchos años, entonces **yo creo que ese argumento no tiene seriedad. Mal hacen los gremios en hacer sentencias anticipadas”.**

La preocupación que debería existir, dice Posso es que el artículo se quede corto frente a las situaciones que tiene que proscribirse porque no se trata solamente de prohibir la formación de grupos armados ilegales, sino de otras prácticas como **utilizar grupos armados legales para uso de particulares, haciendo referencia a apropiación de territorios** o recursos.

“Debe entenderse que para que este país cierre un periodo de guerra, de violencia sistemática es muy importante que las normas constitucionales consagren un pacto de adiós a las armas en la política y en los negocios”. Le puede interesar: [Si Estado no juzga empresarios y mandos militares "que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

### **Empresarios tienen temor con la aprobación de la ley que prohíbe el paramilitarismo** 

Dice el director de Indepaz que debido a innumerables presiones **los empresarios tienen un temor de que se reabra un proceso de recriminaciones**, acusaciones por eventos, situaciones y prácticas que se han dado en el pasado y que eso implique una fuerza constitucional.

“Yo pienso que no va a haber retaliaciones contra empresarios sino es para poder mirar para adelante y caminar hacia un país que no repita las historias de guerras y confrontaciones armadas”. Le puede interesar: ["Política de miedo" de paramilitarismo debe ser afrontada con movilización social](https://archivo.contagioradio.com/paramilitarismo-es-una-politica-del-miedo-impuesto/)

### **¿Cómo complementar este acto legislativo que prohíbe el paramilitarismo?** 

Muchas cosas, manifiesta el analista, son las que se podrían hacer para tener un acto legislativo más sólido, como tipificar en el código penal, de modo que sea mucho más contundente la prohibición y trabajar por la reconciliación en la sociedad.

**“En estas décadas que siguen hay que cambiar de mentalidad, hay que superar todos los discursos de estigmatización y de odio.** Verdaderamente es un trabajo cultural, institucional, de democracia que tiene que hacerse para que no exista repetición. Hay que enunciar las cosas, para dar rutas a la sociedad y ese es el meollo del asunto”. Le puede interesar: [Si es necesario prohibir el paramilitarismo en Colombia: Iván Cepeda](https://archivo.contagioradio.com/si-es-necesario-prohibir-el-paramilitarismo-en-colombia/)

<iframe id="audio_20506104" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20506104_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
