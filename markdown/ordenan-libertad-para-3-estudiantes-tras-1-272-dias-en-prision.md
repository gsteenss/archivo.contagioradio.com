Title: Ordenan libertad para 3 estudiantes tras 1.272 días en prisión
Date: 2015-03-30 19:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: carlos lugo, feu, jega, jorge eliecer, omar marin, presos, prisioneros
Slug: ordenan-libertad-para-3-estudiantes-tras-1-272-dias-en-prision
Status: published

###### Foto: Solidaridad y Defensa 

<iframe src="http://www.ivoox.com/player_ek_4285527_2_1.html?data=lZell5qWe46ZmKiakpqJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9PYxtPO0JDQrcPZ09nOxpDUpdPVjJiYx9jYucXdwtPhx9iPuNPV1JCekJebdozYhqigh6aopdSfxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jesus David Sánchez, Colectivo Gorgona] 

**Omar Marín, Jorge Eliecer Gaitán y Carlos Lugo** vivieron 1.272 días en prisión, antes de que el viernes 27 de marzo se ordenara su libertad por vencimiento de términos.

Los jóvenes universitarios habían sido capturados el 2 de octubre del 2011 en desarrollo de la **Operación Alejandría**, realizada por la Fiscalía en el marco de las movilizaciones que realizara la **Mesa Amplia Nacional Estudiantil** -MANE- contra la reforma a la **Ley 30**, y del 3er Congreso de la **Federación de Estudiantes Universitarios**. En dicha operación fueron capturados además, 5 líderes campesinos y sociales del departamento del Huila, sindicados de Rebelión.

Según denunció la FEU a lo largo de 3 años y medio, el proceso sufría **incongruencias, falta de pruebas y dilataciones por parte del INPEC** (quien no trasladaba a los estudiantes a sus audiencias), y de la Fiscalía (quien solicitaba aplazamientos injustificados reiteradamente), propia de una "lógica de tortura y de silenciamiento", según señala Jesus David Sánchez, del Colectivo Gorgona por la libertad de los prisioneros políticos de la FEU.

La orden de libertad de los 8 líderes sociales se da como respuesta a una de las 10 solicitudes por vencimiento de términos presentada por la defensa de los estudiantes. Según señala Sánchez, sus compañeros estudiantes "están bastante felices", a la espera de poder reunirse con sus familias.
