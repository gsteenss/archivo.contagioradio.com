Title: Querido Papa Francisco - Cartas desde el Closet
Date: 2017-11-15 09:10
Category: Cartas desde el Closet, Opinion
Tags: LGBTI, LGBTI Colombia
Slug: querido-papa-francisco-cartas-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/papa-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Presidencia México 

[Noviembre, 2017]

#### **Parche por la vida** 

Desde mi condición de cristiana católica y homosexual, quiero agradecerle por darnos a millones de personas la oportunidad de vivir un profundo momento de espiritualidad en su visita a nuestro país. Fue un momento de encuentro con el evangelio, de reconocimiento de la palabra de Dios y de comprensión de lo que significa seguir a Cristo en este tiempo.

En la Eucaristía del parque Simón Bolivar en Bogotá, me conmovió la fe de tantas personas emocionadas con su presencia, llorando de alegría, cuando hemos llorado de dolor por tanto sufrimiento y tanta muerte. Yo también lo viví así y sentí la fuerza del espíritu de Dios en su rostro, en sus palabras y en todas las personas que estábamos allí reunidos.

Gracias por su mensaje al país. Fue una invitación al amor, a la reconciliación, al perdón y a una fe reflexiva y comprometida con los pobres, con el ambiente, con la construcción de la paz sobre la base de la verdad, la justicia y la inclusión.

Dolorosamente y según lo he vivido en Colombia, ser una persona de fe es ser una persona irreflexiva, sin autonomía y sin libertad pensamiento, que vive más en el pasado que en el presente; lo cual se ha agravado porque la fe se ha mezclado con una politiquería cerrada e irracional que ha convertido a las iglesias en púlpitos; desde los cuales se hacen discursos distantes del mensaje del evangelio y que buscan proteger intereses ocultos. Con frecuencia se ha reemplazado el mensaje de amor de Jesucristo por el de odio contra el opositor y se ha manipulado la fe, identificando la fidelidad al mensaje cristiano con ideas en contra de las diferencias sexuales, étnicas y culturales.

Siempre me ha resultado molesto que los no creyentes crean que la fe sea una especie de locura, de insensatez, o que sea el resultado inmediato de la falta de acceso a la educación. Pero debo decir que, ante las posiciones de muchos cristianos en temas como el proceso de paz o el reconocimiento de los derechos de los homosexuales, a veces me he quedado sin argumentos para defender a mis hermanos y hermanas de fe.

Estimado Papa, en cada uno de sus mensajes me pareció escuchar un llamado al pensamiento crítico, a no dejarnos orientar por quienes no tiene autoridad para hacerlo y a mirar con detenimiento en manos de quien ponemos nuestra fe, llamado que nos hacía mucha falta a las y los creyentes, segados por el miedo generado por los “cuentos” que han difundido quienes venden “baratijas de fe” afirmando que al país se lo va a tomar los homosexuales, el “Castrochavismo”, la “ideología de género” …

Su invitación a no dejarnos robar la alegría, la esperanza y la paz de quienes siembran la cizaña, la maravillosa forma como usted trasmite el mensaje de Dios, me ha dado de nuevo valor y argumentos para sostener ambas cosas: mi amor infinito a Dios y mi capacidad de construir mis propias opiniones sobre el mundo, desde mi condición sexual.

Por ser un Papa cercano a las personas y conectado  a la realidad humana, un millón de gracias.

#### **Parche por la vida 10 - Ver más [Cartas desde el Closet](https://archivo.contagioradio.com/cartas-desde-closet/)** 
