Title: mega-item-172
Date: 2018-05-17 08:24
Author: AdminContagio
Slug: mega-item-172
Status: published

##### Categories Layouts

[Listing](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=default&posts_per_page=6)  
[Listing 2<small>NEW</small>](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=default-small-img&posts_per_page=12)  
[Grid](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=grid&posts_per_page=12)  
[Grid 2](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=grid-2&posts_per_page=16&blog_read_more_btn=0&blog_posts_content=none&blog_post_share_buttons=0&blog_post_tags=0)  
[Masonry](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=masonry&posts_per_page=12)  
[Timeline<small>HOT</small>](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=timeline&blog_post_publish_date=0&posts_per_page=12)  
[Justify](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=vertical-justify&sidebar_position=fullwidth&posts_per_page=7&blog_posts_content=none&blog_post_tags=0)  
[Pagination default](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_pagination_type=default)  
[Pagination Load More<small>SPECIAL</small>](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_pagination_type=load-more)

##### Post Formats

[Standard](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/how-do-political-parties-oppose-their-own-presidents-2-2/)  
[Gallery](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/japan-holds-next-business-forum-on-azerbaijan/)  
[Image](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/battle-laptop-vs-tablet/)  
[Quote](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/mohandas-gandhi-facts-summary/)  
[Audio](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/discussion-of-the-best-hits-of-the-90s-selection/)  
[Video](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/tributes-continue-for-popular-swimming-coach-paul-johnson/)  
[Link](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/high-quality-templates/)

##### Post Modules

[Smart List](#)  
[Smart List 1](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-1/)  
[Smart List 2](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-2/)  
[Smart List 3](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-3/)  
[Smart List 4](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-4/)  
[Smart List 5](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-5/)  
[Smart List 6](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-6/)  
[Smart List 7](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-7/)  
[Smart List 8](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-list-8/)  
[Smart Tiles](#)  
[Smart Tiles 1](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-1/)  
[Smart Tiles 2](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-2/)  
[Smart Tiles 3](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-3/)  
[Smart Tiles 4](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-4/)  
[Smart Tiles 5](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-5/)  
[Smart Tiles 6](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-6/)  
[Smart Tiles 7](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-7/)  
[Smart Tiles 8](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-8/)  
[Smart Tiles 9](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/smart-tiles-9/)  
[Post Single Layouts](#)  
[Post Layout \#1](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/north-of-england-cities-get-15m-cultural-powerhouse-boost/)  
[Post Layout \#2](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/15-thoughtful-and-useful-gifts-for-the-diy-enthusiast-in-your-life/)  
[Post Layout \#3](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/inequality-robs-individuals-and-society/)  
[Post Layout \#4](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/culture-wellbeing-and-the-creative-society/)  
[Post Layout \#5](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/political-base-bigger-stronger-than-ever-before/)  
[Post Layout \#6](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/the-incredible-story-of-an-american-travel-blogger-2/)  
[Post Layout \#7](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/photo-report-from-southern-africa/)  
[Post Layout \#8](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/the-5-street-style-trends-to-try-from-new-york-fashion-week/)  
[Post Layout \#9](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/is-it-art-or-just-awful-beijing-grannys-portrait-sketches-draw-a-mixed-response/)  
[Post Layout \#10](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/the-new-flagship-from-the-worlds-largest-tablet-manufacturer-2/)  
[Reviews](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/reviews/)  
[Text Ticker](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/text-ticker/)  
[Video Playlist](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/video-playlist/)  
[AMP Sample<small>NEW</small>](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/how-economics-became-a-religion/amp/)

##### Site Layouts

[Boxed](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&page_layout_style=boxed)  
[Framed](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&page_layout_style=framed)  
[Fullwidth](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&page_layout_style=fullwidth)

##### Sidebar Layouts​

[Left Sidebar](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=grid&sidebar_position=one-left-sidebar&posts_per_page=12)  
[Right Sidebar](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&blog_layout_type=grid&sidebar_position=one-right-sidebar&posts_per_page=12)  
[No Sidebar](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog/?ld&sidebar_position=fullwidth)

##### Pages

[Typography](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/typography/)  
[Documentation](#)
