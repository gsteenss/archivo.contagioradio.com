Title: 25 abril paro nacional por la paz y contra el Plan Nacional de Desarrollo
Date: 2019-04-02 17:47
Author: ambiente y sociedad
Category: Movilización, Nacional
Tags: Minga, Paro Nacional, PND, Trabajadores
Slug: 25-abril-paro-nacional-paz-pnd
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/CUT-Paro-Nacional-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cutcolombia] 

###### [2 Abr 2019] 

**El próximo 25 de abril se realizará un gran paro nacional en el que participarán sindicatos, movimientos políticos, sociales y campesinos**; manifestación que tendrá como banderas el cumplimiento de los acuerdos pactados (e incumplidos) anteriormente con los distintos sectores, la defensa de la paz y el ajuste del Plan Nacional de Desarrollo (PND) que responda a las necesidades de la mayoría de los colombianos.

Como explicó el **fiscal de la Central Unitaria de Trabajadores (CUT) Fabio Arias**, todos los sectores que se han unido a esta convocatoria entienden el sistema de negociación que tiene este Gobierno y están unidos entorno a la defensa de la paz. En ese sentido, una de las banderas de la lucha será evitar modificaciones a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), y el rechazo a las objeciones presidenciales.

La segunda reivindicación está relacionada con el cumplimiento de los acuerdos pactados con el Estado colombiano por parte de sectores populares, reclamo similar que motivó la Minga adelantada por los Indígenas del Cauca y otros departamentos hace más de dos semanas. **En ese sentido, la lucha de las centrales sindicales estará enfocada en la garantías de respeto a lo pactado con profesores, estudiantes, pensionados, trabajadores, transportadores y campesinos**.

Por último, el paro buscará modificar el Plan Nacional de Desarrollo para **que se eliminen todos los artículos y disposiciones encontradas en el Plan Plurianual de Inversiones que son contrarias al interés de la mayoría de colombianos**. (Le puede interesar: ["El Plan Nacional de Desarrollo, un respaldo al sector minero-energético"](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/))

Pese a que las reivindicaciones parecen bastante difíciles de ser atendidas por el gobierno Duque, que tras una Minga de 22 días aún no asiste al territorio para dialogar, Arias afirmó que están convencidos de la fuerza de la movilización del jueves 25 de abril, a la que ya se han sumado sectores campesinos agrupados en la Cumbre Agraria, el Congreso de los Pueblos, y que espera la llegada de estudiantes, transportadores, "pequeños y medianos empresarios, caficultores (sumidos en una profunda crisis por el precio del grano) y, obviamente, con todos los sectores ciudadanos".

<iframe id="audio_34096623" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096623_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [English Version](https://archivo.contagioradio.com/april-25th-national-strike-for-peace-against-the-national-development-plan/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
