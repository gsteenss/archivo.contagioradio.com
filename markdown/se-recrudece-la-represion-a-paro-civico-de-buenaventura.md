Title: Hasta los niños son víctimas del ESMAD en Buenaventura
Date: 2017-05-26 13:32
Category: Movilización, Nacional
Tags: buenaventura, ESMAD, Paro de Buenaventura
Slug: se-recrudece-la-represion-a-paro-civico-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/ESMAD-e1495823544560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [26 may 2017] 

En la madrugada de hoy el Esmad, en una situación repetitiva del exceso del uso de la fuerza, lanzó gases lacrimógenos contra las casas del casco urbano de Buenaventura. Dos bebes resultaron afectados por los gases y tuvieron que ser trasladados a centros de atención médica.

Javier Torres, integrante del Comité del Paro Cívico, denunció que “**el Esmad está arremetiendo contra la población en una situación de provocación** para que cuando la gente se canse y arremeta contra la fuerza pública, ellos puedan justificar sus actos de violencia”. Le puede interesar: ["Piden suspender toque de queda en Buenaventura"](https://archivo.contagioradio.com/piden-suspender-toque-de-queda-en-buenaventura/)

### **ESMAD disparó gases en la madrugada contra personas en medio de un velorio** 

Además el líder manifestó que “en medio de un velorio, los agentes del Esmad, en un acto de exceso de la fuerza, arremetió contra la población presente”. La fuerza pública se instaló en el barrio Isla de Paz donde los habitantes dormían y **se encontraban en completa indefensión.**

Igualmente, ha sido posible establecer que las fuerzas militares han llegado a la región en lo que Torres denominó “**un desfile de militares que nunca se vio cuando había casa de pique, nos asesinaban y nos amenazaban”.** Los habitantes de Buenaventura han tratado de dialogar con los miembros de la Policía para que cese la represión, pero según ellos, no ha sido posible. Le puede interesar: ["Camioneros se unen al paro de Buenaventura"](https://archivo.contagioradio.com/camioneros-se-unen-al-paro-de-buenaventura/)

**Los bonaverenses siguen esperando que el gobierno haga presencia en Buenaventura** para exigirles que cese la represión contra el pueblo. Torres afirmó que “en la mesa de diálogo vamos a exigir que haya respuestas a la situación en Buenaventura, pero lo primero que exigimos es que cese la represión”.

La provocación de las fuerzas armadas sólo se puede traducir en más violencia y eso es algo que la población quiere evitar. Para ellos y ellas es **“indignante que se le de este trato a una población que se está movilizando por algo justo”**. Hoy a las 10:00 am comenzó la reunión con el gobierno en donde los líderes de la movilización le exigieron, como garantía para dialogar, detener la represión contra la población.

### **A Buenaventura no le sirven las actas, exigen decretos** 

Ante la negativa de las instituciones del gobierno de decretar la emergencia social, económica y ambiental en Buenaventura, los pobladores le han pedido al gobierno la creación de un mecanismo alternativo que garantice el cumplimiento de los acuerdos retrasados en mejoras de infraestructura, agua y salud. Según Torres, **“no vamos a permitir que otra vez las soluciones se queden en papel”.** Le puede interesar: ["¿Dónde estaba la Policía de Buenaventura o el Esmad durante saqueos"](https://archivo.contagioradio.com/confirman-que-arremetida-del-esmad-provoco-disturbios-en-buenaventura/)

Igualmente, ellos y ellas han manifestado que tienen los insumos para que se decrete la emergencia social y económica, mecanismo que garantizaría la implementación de soluciones oportunas a la situación crítica que se vive allí. En los diálogos con el gobierno, los bonaverenses **estarán atentos ante la propuesta de un mecanismo alternativo que se pueda aplicar en Buenaventura**.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154477419905812%2F&amp;show_text=0&amp;width=400" width="400" height="400" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_18920629" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18920629_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
