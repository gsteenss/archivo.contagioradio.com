Title: FECODE marcha este jueves en defensa de la educación pública
Date: 2018-09-13 11:20
Author: AdminContagio
Category: Educación, Nacional
Tags: fecode, maestros, marcha, profesores, Proyecto de Ley
Slug: fecode-marcha-defensa-educacion
Status: published

###### [Foto: ADE] 

###### [13 Sept 2018] 

Los maestros saldrán este jueves a marchar en defensa de la educación pública, y contra los **Proyectos de Ley 14 y 057,** que van en detrimento de las garantías para educadores y estudiantes de la educación básica pública, movilización nacional convocada por **FECODE** que iniciará a las 10:30 am, y en Bogotá saldrá desde la Gobernación de Cundinamarca y la Secretaría de Educación distrital hasta el Ministerio de Educación Nacional.

**Carlos Rivas, presidente de FECODE,** sostuvo que la jornada nacional de protesta busca pedirle al Gobierno que retire los Proyectos de Ley 14 y 57. El primero busca convertir la educación en un servicio público esencial, mientras el segundo "atenta contra el presupuesto del sistema general de participación, y desde luego contra la salud, el agua potable, el saneamiento básico y la educación".

### **Proyecto de Ley 14: “Por medio de la cual se regula el derecho fundamental a la educación”** 

Según Rivas, este Proyecto impulsado por congresistas del Partido Colombia Justa Libres, busca que la educación sea un servicio público esencial **para que en caso de que los maestros entren en paro, su protesta se vea deslegitimada y declarada ilegal,** como ocurrió con el caso de los pilotos de ACDAC. (Le puede interesar: ["Fallo sobre ilegalidad de huelga de pilotos golpea la libertad sindical"](https://archivo.contagioradio.com/fallo-sobre-ilegalidad-de-huelga-de-pilotos-golpea-la-libertad-sindical/))

Rivas recordó que cuando se plantea que la educación es un servicio público esencial, se reconocería que en caso de protesta, se afecta la vida, la salud, la seguridad o la constitución; pero cuando los maestros hacen paro, ninguna de estos elementos se ven afectados. Adicionalmente, sostuvo que la Organización Internacional del Trabajo (OIT), ha dicho que la educación es un derecho fundamental, que tiene una función de carácter social pero no es un servicio público.

Por lo tanto, lo que se está buscando con el Proyecto es limitar el derecho fundamental a la protesta, un "recorte democrático" que los maestros no permitirán. (Le puede interesar: ["El vía crucis de los acuerdos entre maestros y Ministerio de Educación"](https://archivo.contagioradio.com/salud-y-financiacion-a-la-educacion-las-exigencias-de-los-docentes-al-ministerio-de-educacion/))

### **Proyecto de Ley 057: Sobre el Sistema General de Participaciones** 

Para el profesor, el Proyecto que hace su curso legislativo vía Cámara de Representantes, **pretende establecer unas responsabilidades a los entes territoriales sobre el tema general de participaciones como "la alimentación escolar, el transporte y la jornada única",** obligando a los municipios a generar nuevos recursos mediante estampillas, para financiar estas modificaciones.

Adicionalmente, la iniciativa atenta contra la estabilidad laboral de los profesores, y **"pone a los padres de familia a financiar la educación"**, porque deberán pagar matriculas, cuando la Constitución señala que la educación básica debe ser gratuita. Sin embargo, Rivas ve difícil el tramite de ambos proyectos en el Congreso. (Le puede interesar: ["Gobierno no resuelve exigencias sobre déficit presupuestal de educación: ADE"](https://archivo.contagioradio.com/gobierno-no-resuelve-exigencias-sobre-deficit-presupuestal-de-educacion-ade/))

A pesar de que "el Gobierno tiene apenas 54 senadores, y no tiene músculo en el legislativo para aprobar ambas iniciativas", los maestros saldrán a protestar este jueves exigiendo que los Proyectos no pasen al debate, y así evitar que las escuelas se cierren por falta de dinero, y se recorte el derecho a la protesta de los maestros.

<iframe id="audio_28587856" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28587856_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

<div class="osd-sms-wrapper">

</div>
