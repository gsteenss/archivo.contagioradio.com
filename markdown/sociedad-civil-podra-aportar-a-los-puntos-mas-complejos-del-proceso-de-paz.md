Title: Sociedad civil podrá aportar a los puntos más complejos del proceso de paz
Date: 2016-02-04 16:23
Category: Nacional, Paz
Tags: Foro ONU y universidad nacional
Slug: sociedad-civil-podra-aportar-a-los-puntos-mas-complejos-del-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Foro-UNal-ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Emisora Atlántico ] 

###### [4 Feb 2016. ]

Cerca de **700 personas entre miembros de organizaciones sociales, de victimas, afro, campesinas e indígenas** participaran entre el 8 y el 10 de febrero del foro organizado por la ONU y la Universidad Nacional que busca discutir propuestas en torno a los puntos que restan ser acordados por el Gobierno nacional y la guerrilla de las FARC-EP en la mesa de diálogos de La Habana.

En rueda de prensa Fabrizio Hochschild, coordinador residente y humanitario de la ONU en Colombia, y Alejo Vargas, integrante del Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional, afirmaron que en este foro **distintos sectores de la sociedad civil podrán participar activa y democráticamente** en la construcción de propuestas para el fin del conflicto y la refrendación, implementación y verificación de los acuerdos de paz.

En este foro la participación de hombres y mujeres será equitativa, **se organizarán mesas de trabajo integradas cada una por 50 personas** quienes discutirán aspectos concernientes al fin del conflicto, el cese al fuego bilateral y definitivo, la dejación de armas, y la refrendación, implementación y verificación de los acuerdos. Propuestas que servirán de insumos para las sesiones de trabajo de las delegaciones de paz.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
