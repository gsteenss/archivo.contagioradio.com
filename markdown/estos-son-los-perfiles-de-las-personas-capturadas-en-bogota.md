Title: Estos son los perfiles de jóvenes capturados en Bogotá
Date: 2015-07-09 09:08
Category: Movilización, Nacional
Tags: congreso de los pueblos, ELN, Mesa Amplia Nacional Estudiantil
Slug: estos-son-los-perfiles-de-las-personas-capturadas-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/estudiantes-capturados-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [cambiototalrevista.blogspot.com]

###### [9 Jul 2015] 

Cerca de medio centenar de organizaciones firmaron un documento en el que respaldan a las personas capturadas y además confirman que todos hacen parte de organizaciones sociales y estudiantiles, todos con amplia trayectoria de trabajo social y popular. Las organizaciones llamaron a la “Fiscalía General de la Nación a actuar en coherencia con el principio de independencia judicial haciendo una valoración razonable y técnica de las supuestas pruebas recaudadas”.

A continuación presentamos los perfiles de las personas capturadas y a las cuales las organizaciones de DDHH dan su respaldo.  ([Leer [Capturas a líderes del movimiento social serían "falsos positivos judiciales](https://archivo.contagioradio.com/capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales/)")]

[**PAOLA ANDREA SALGADO PIEDRAHITA**]

Mujer, feminista, joven abogada de la Universidad Nacional de Colombia, comprometida con la defensa de los Derechos Humanos y derechos de las Mujeres. Con un gran reconocimiento en el movimiento feminista, en donde su actuar es ampliamente conocido por su litigio e investigación sobre violaciones a los Derechos Civiles y Políticos, infracciones al DIH y Violencia contra las mujeres. Ha hecho parte de la Mesa por la Vida y la Salud de las Mujeres, de la Red Nacional de Mujeres, de la Confluencia de Mujeres del Congreso de los Pueblos. Su opinión ha sido consultada por los medios de comunicación. Ha participado en diferentes programas de televisión, eventos académicos, foros seminarios entre otros espacios.

Su compromiso y destreza en la defensa de los derechos de las mujeres la llevó a desempeñarse como coordinadora del Programa Servicios amigables en salud sexual y salud reproductiva para mujeres de la Secretaria Distrital de Salud de Bogotá – Hospital de Suba II Nivel ESE. A Paola la reconocemos como una amiga, amante de los animales, comprometida con la vida y constructora de sueños.

[**SERGIO ESTEBAN SEGURA GUIZA**]

Tiene 27 años es Comunicador Social de la Universidad Cooperativa de Colombia y candidato a magister en Ciencias Sociales en la Universidad Pedagógica Nacional, se ha desempeñado como periodista y comunicador en la Agencia de Comunicaciones de los Pueblos Colombia Informa. Actualmente trabajando como gestor de ciudadanía en la Secretaria de Educación en el marco del proyecto de Educación para la Ciudadanía y la Convivencia en la localidad de Ciudad Bolívar.

Se destaca también por su liderazgo en los procesos de Objeción de Conciencia en la ciudad de Bogotá.

[**STEFANY LORENA ROMO MUÑOZ**]

Lorena Romo Muñoz de 23 años, es politóloga de la Universidad Nacional de Colombia y esta matriculada en la Universidad Externado de Colombia, para iniciar la especialización en Políticas Públicas en el mes de agosto del año en curso.

Es activista del movimiento estudiantil desde el año 2010. Fue parte de la dirección del Proceso Nacional Identidad Estudiantil, en Bogotá y desde allí, miembro de la Comisión Académica de la Mesa Amplia Nacional Estudiantil (MANE). Lorena en lo que va corrido del año 2015 ha trabajado como Gestora Social del Distrito, desde la Secretaría de Educación, con un desempeño intachable.

Es lideresa barrial de las localidades de Teusaquillo y Chapinero. Durante su activismo social ha participado en diferentes espacios que han promovido la construcción de paz desde la sociedad civil tales como; Frente Amplio por la paz, Clamor Social por la paz e impulsó el desarrollo del pre congreso educativo por la paz, realizado en Cali en el año 2014.

[**HEILER LAMPREA**]

Heiler Lamprea de 25 años, es Representante al Consejo Superior Universitario de la Universidad Pedagógica Nacional desde el año 2013. Es estudiante de último semestre de Licenciatura en Filosofía. Como parte de su activismo social, ha sido destacado líder del Frente Amplio por la Educación, la Paz y los Derechos Humanos. Es parte del Proceso Nacional Identidad Estudiantil y del Congreso de los Pueblos desde el año 2010.

Ha desarrollado trabajo de acompañamiento barrial-comunitario en colegios de Suba, promoviendo la participación política, la defensa y promoción de los derechos, la construcción de paz, la participación política desde las comunidades, la prevención al consumo de drogas, promoviendo el acceso gratuito a la educación superior pública y de calidad.

Desde su papel como Representante Estudiantil, ha promovido el espacio institucional de Diálogos UPN, que tienen como objetivo la discusión sobre el papel de la universidad en la construcción de paz.

[**VÍCTOR ORLANDO ARIZA GUTIERREZ**]

Víctor Orlando Ariza de 21 años, ha sido parte de movimientos sociales de defensa de la educación pública en Colombia desde que era estudiante de secundaria, motivado por este hecho, continúo impulsando la organización estudiantil secundarista al momento de ingresar a la Universidad.

Es estudiante de Geografía y Representante Estudiantil al Comité de Resolución del Conflicto de la Facultad de Humanas de la Universidad Nacional de Colombia. Hace parte del Proceso Nacional Identidad Estudiantil desde el año 2011, organización que se ha destacado por promover la educación pública, gratuita y de calidad, desde el espacio de la Mesa Amplia Nacional de Estudiantil (MANE).

[**DANIEL EDUARDO HERNÁNDEZ MUÑOZ**]

Daniel Hernández de 23 años, hace parte del Proceso Nacional Identidad Estudiantil y del Congreso de los Pueblos desde el año 2010. Es estudiante de séptimo semestre de la Licenciatura de Filosofía de la Universidad Pedagógica de Colombia.

Realiza trabajo barrial-comunitario en colegios de Suba, promoviendo la organización de los estudiantes de secundaria para la defensa de la educación pública. Su activismo social lo realiza desde el enfoque de educación popular y desde las enseñanzas del sociólogo Orlando Fals Borda, de la Investigación Acción Participativa.

[**LUIS DANIEL JIMENEZ CALDERON**]

Tiene 34 años, es Ingeniero Agrónomo de la Universidad Nacional. Actualmente es el representante de la Corporación Arando en donde se desempeña como líder en procesos barriales y procesos campesinos en Usme y Tunjuelito. Es miembro del Coordinador Nacional Agrario (CNA) y del Congreso de los Pueblos. También hace parte de la Red ambiental Bakata y líder del Proceso de Asociación de Familias Agroecológicas.

Durante su periodo estudiantil fue Representante del Consejo de Facultad de Agronomía de la Universidad Nacional y recibió amenazas por sus actividades. Hoy día es candidato a Maestría en Desarrollo Rural de Pontificia Universidad Javeriana.

[**ANDRES FELIPE RODRIGUEZ PARRA**]

Tiene 23 años es Filósofo de la Universidad Nacional de Colombia. Fue activista estudiantil, en el marco del proceso de la Mesa Amplia Nacional Estudiantil -MANE-. Actualmente trabaja en la Fundación Paz y Reconciliación. Actualmente hace parte de congreso de los pueblos como líder estudiantil.

[**GERSON ALEXANDER YACUMAL RUIZ**]

Tiene 27 años es estudiante de último semestre de educación comunitaria con énfasis en derechos humanos de la universidad pedagógica Nacional, ha sido profesor de niños y jóvenes en colegios de ciudad Bolívar y Tunjuelito. Actualmente se desempeñaba como tesorero de la organización TEJUNTAS, líder juvenil comunitario en ciudad bolívar y Usme y defensor de los derechos humanos del sector juvenil en la búsqueda de alternativas productivas de los jóvenes.

[**LICETH JOHANA ACOSTA**]

Tiene 21 años es estudiante de la Universidad Pedagógica de IV semestre licenciatura en sociales, hace parte de la Identidad Estudiantil y en su momento estuvo impulsando la Mesa Amplia Nacional Estudiantil (MANE) y es miembro activa del Congreso de los Pueblos. En 2010 se desempeñaba como defensora de los derechos de las mujeres y actualmente estaba dedicada a la construcción de consejos estudiantil en la UPN.

[**JHON FERNANDO ACOSTA**]

19 años estudiante de la Universidad Pedagógica de la licenciatura en Artes escénicas, trabaja en defensa delos derechos de las genero y las mujeres, en su trabajo como líder trabajaba desde su profesión en el respeto hacia las mujeres y el cuerpo como territorio de construcción de paz. Está desarrollando una propuesta académica que tiene como objetivo vincular las artes como herramienta para la construcción de paz. Hacen parte de identidad estudiantil

[**FELIX MAURICIO AUGUSTO GUTIERREZ DIAZ**]

Tiene 25 años es estudiante de Licenciatura en Filosofía de la universidad pedagógica nacional. Fue miembro activo del colectivo Acción Maestra que realizaba trabajo comunitario con niños y niñas desde el arte y el deporte, con intención de mantener a la población juvenil fuera del conflicto y de la posible vinculación a la drogadicción y al delito. Hizo parte del proyecto Lectores Ciudadanos de la Alcaldía Mayor de Bogotá para impulsar la lectura en familias y comunidades vulneradas. Actualmente se desempeñaba como profesor en colegios urbanos y semiurbanos de Cundinamarca para preparar estudiantes a las pruebas Saber, con la Corporación Educativa ASED.

Ha sido un defensor de los derechos humanos en el país, reivindicando la educación como derecho fundamental para las transformaciones que requiere el país. Es miembro del Congreso de los Pueblos.
