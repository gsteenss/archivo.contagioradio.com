Title: Nuevas amenazas contra ambientalistas de Tolima
Date: 2016-07-11 15:37
Category: Ambiente, Nacional
Tags: Agua, Fiscalía, Ibagué, Marcha carnaval
Slug: siguen-las-amenazas-de-muerte-en-contra-de-ambientalistas-de-ibague
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/amenaza-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: COSAJUCA 

###### [11 Jul 2016] 

Continúan las amenazas en contra de los integrantes de las organizaciones ambientalistas, que el pasado 3 de junio organizaron la marcha carnaval por el agua y por la vida, en Ibagué Tolima, donde participaron cerca de 120 mil personas. Esta vez, por medio de recortes de revista llegó un panfleto amenazante sin ninguna firma.

**“Los niños juiciosos se duermen temprano, los demás los acostamos nosotros”,** dice el papel que llegó el pasado 8 de julio a la organización defensora de los derechos humanos y el territorio que pertenece a la red Comités ambientales del Tolima, Colectivo Socio-Ambiental Juvenil de Cajamarca, **COSAJUCA, que adelanta acciones  políticas y de movilización en contra del proyecto minero La Colosa. **

Esta **se trata de la quinta amenaza que reciben los ambientalistas. [La anterior fue hace un mes y era firmada por el grupo paramilitar ‘Aguilas Negras](https://archivo.contagioradio.com/paramilitares-de-las-aguilas-negras-amenazan-a-organizaciones-que-impulsaron-carnaval-por-el-agua/)**’, donde se señalaba a varias organizaciones, e incluso al alcalde de Ibagué, Alfonso Jaramillo,  como colaboradores de la guerrilla de las FARC, y se les culpaba de **  “desinformar a la gente solo para poner tropiezo a las buenas ideologías y en los proyectos de desarrollo en el Tolima y en nuestra patria”.**

Robinson Mejía, integrante de COSAJUCA asegura que en el año 2015 se recibieron tres amenazas contra las mismas organizaciones y entre los años 2013 y 2014 sucedieron las muertes de los integrantes de este movimiento Daniel Sánchez, Camilo Pinto, Cesar García y José Ramírez.

Las organizaciones y colectivos amenazados no solo exigen al gobierno nacional que se implementen las medidas necesarias para la seguridad, sino también haya garantías políticas para la defensa del territorio. **Es por ello que este lunes presentaron una denuncia penal para que Fiscalía General esclarezca las responsabilidades.**

[![amenaza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/amenaza.jpg){.aligncenter .wp-image-26295 width="507" height="755"}](https://archivo.contagioradio.com/siguen-las-amenazas-de-muerte-en-contra-de-ambientalistas-de-ibague/amenaza/)

<iframe src="http://co.ivoox.com/es/player_ej_12189843_2_1.html?data=kpeemp6ceJShhpywj5WaaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncbPjw87b1dTSb67Zy87Oh5enb6TDtKa3t6ilcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
