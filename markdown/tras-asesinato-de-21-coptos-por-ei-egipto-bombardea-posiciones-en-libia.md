Title: Tras asesinato de 21 coptos por "EI" Egipto bombardea posiciones en Libia
Date: 2015-02-16 20:35
Author: CtgAdm
Category: DDHH, El mundo
Tags: Egipto ataca estado islámico, Estado Islámico, Estado islámico asesina coptos egipcios, Libia ataca Estado Islámico, Libia y Egipto bombardea Estado Islámico
Slug: tras-asesinato-de-21-coptos-por-ei-egipto-bombardea-posiciones-en-libia
Status: published

###### Foto:Huffingtonpost.com 

**Egipto bombardeó esta madrugada posiciones del Estado Islámico en Libia, con entre 40 y 50 milicianos del EI muertos como resultado**. El bombardeo se dio solo una hora después de que **miembros de la milicia islamista publicaran un video decapitando a 21 coptos egipcios.**

Las **víctimas habían sido secuestradas en la ciudad de Sirte al norte de Libia**, zona fronteriza con Egipto y de fuerte presencia del islamismo radical. Después de un mes el EI, difundió este domingo un video con la decapitación de los ciudadanos egipcios.

Este es el **primer bombardeo que declara Egipto en territorio libio** y en contra de campamentos, centros de reclutamiento, arsenales de armas y en general contra la propia estructura de la milicia en suelo ajeno.

La acción fue coordinada con fuerzas aéreas libias y valorada positivamente por el Estado libio. Esta no es la primera vez que el ejército egipcio actúa en contra del islamismo radical, en coordinación con fuerzas de **Emiratos Árabes Unidos o con Arabia Saudí**.

El brutal asesinato ha sido considerado por el estado como una agresión sin precedentes, a la que **Egipto se reserva el derecho de actuar "cuando y como quiera, dentro y fuera de sus propias fronteras".**

También ha reabierto el debate sobre la **necesidad de una operación terrestre en el país vecino con el fin de frenar el islamismo radical**, e impedir que se contagie al país.

Las fuerzas armadas han anunciado que los bombardeos van a continuar durante toda la semana, y el gobierno ha decretado un duelo en todo el país de 7 días.
