Title: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes
Date: 2020-05-25 16:28
Author: CtgAdm
Category: Actualidad, DDHH
Slug: partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/CIDH-NOTA-DE-PRENSA-FINAL.pdf" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En un encuentro virtual que se realizará este 26 de Mayo, **integrantes del Partido FARC, solicitarán medidas cautelares de la Comisión Interamericana de Derechos Humanos, ([CIDH](https://archivo.contagioradio.com/jueces-fallan-a-favor-de-poblacion-reclusa-en-3-carceles/))**, a favor de las personas que firmaron el Acuerdo de Paz y que **se enfrentan, según ellos, a un extermino sistemático, que ya ha cobrado de la vida de 197 de ellos y ellas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado difundido por el partido FARC, esta solicitud se hará formal en una reunión virtual, en la que explicarán las razones por las cuales consideran necesario que, a través del organismo internacional, **el Estado colombiano adopte medidas para proteger a los más de 12.000 [firmantes](https://www.justiciaypazcolombia.com/el-impacto-del-covid-19-en-el-proceso-de-reincorporacion-de-las-farc-en-colombia/) del acuerdo que ahora se encuentran en riesgo de muerte** debido a las múltiples violencias presentes en los territorios en los que se desarrolla su proceso de reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que la cifras de los asesinatos son preocupantes. **Tan solo en 2020, 20 exintegrantes de las Farc, que se acogieron al Acuerdo de Paz, fueron asesinados, según un estudio del INDEPAZ**, organización que se dedica a investigar el conflicto armado en Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, según el propio partido FARC, desde la firma del Acuerdo **han sido asesinados 197 firmantes de la paz y 41 personas, integrantes de sus núcleos familiares** también han sido víctima de los violentos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La impunidad sigue siendo la reina

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además del alto número de personas asesinadas o amenazadas, **una de las problemáticas que afianzan los hechos violentos es la falta de esclarecimiento de los crímenes cometidos**. Aunque no hay cifras oficiales del avance de las investigaciones, integrantes del partido FARC aseguran que **la impunidad alcanza la escandalosa cifra del 90%**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la Fiscalía General de la Nación, ha asegurado que **la responsabilidad sobre estos crímenes recae en las bandas criminales que aparecieron después de la firma del Acuerdo** en los lugares en que las FARC ejercía control, entre ellas las **Águilas Negras, Los Rastrojos y otros,** sin embargo no entregan resultados en torno a capturas o judicializaciones, aseguró Victoria Sandino, senadora del Partido FARC, a finales de 2019.

<!-- /wp:paragraph -->
