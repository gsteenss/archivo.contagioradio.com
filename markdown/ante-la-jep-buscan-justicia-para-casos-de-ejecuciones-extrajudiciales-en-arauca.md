Title: Ante la JEP buscan justicia para casos de ejecuciones extrajudiciales en Arauca
Date: 2019-10-23 15:22
Author: CtgAdm
Category: DDHH, Judicial
Tags: Arauca, Ejecuciones Extrajudiciales, Saravena, Tame
Slug: ante-la-jep-buscan-justicia-para-casos-de-ejecuciones-extrajudiciales-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/JEP-.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Jepm.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cjyiracastro] 

Organizaciones de derechos humanos hicieron entrega a la Jurisdicción Especial para la Paz (JEP), del informe 'De Arauca Somos y Resistimos", que compila información sobre posibles **ejecuciones extrajudiciales perpetradas por el Ejército en este departamento entre los años 2002 y 2008**. Esta entrega busca priorizar y orientar el trabajo que esta entidad viene realizando en el caso 003 “muertes ilegítimamente presentadas como bajas en combate”.

El abogado Sebastián Bojacá, de la Comisión Colombia de Juristas, precisa que se han recopilado 79 casos de 116 víctimas de ejecuciones extrajudiciales perpetradas en **Tame, Saravena, Arauquita, Fortul, Puerto Rondón y Arauca**, cometidos por al menos dos brigadas de la segunda división del Ejército:  la brigada n18 y la brigada quinta móvil. Pese a estos datos recopilados, resalta que aún existe un número más grande de víctimas que no ha podido ser documentado.

En el informe se señala como responsables a los entonces **generales Ovidio Saavedra Saenz y José Joaquín Cortés y al comandante Juan Pablo Rodríguez Barragán** quien estaría vinculado a otros casos como los acontecidos en la Costa en el Batallón La Popa entre 2003 y 2004.

\[caption id="attachment\_75462" align="aligncenter" width="1000"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Jepm.jpg){.size-full .wp-image-75462 width="1000" height="719"} Foto: @cjyiracastro\[/caption\]

### Los patrones que orientarían a la JEP

Dentro de los patrones hallados en este informe, resaltan que se atacó a la población civil de campesinos de este departamento, ejecutando a las víctimas en frente de sus familiares para luego llevarlos a los batallones "donde posiblemente eran manipulados los cuerpos para evidenciar un combate ilegítimo".

También se conoció que en su mayoría, las víctimas eran parte de la población indígena, sindicalistas, defensores de DD.HH. o líderes sociales,  además se destaca un encubrimiento por parte de personas desmovilizadas de algunas guerrillas quienes afirman que estas personas eran parte de organizaciones ilegales. [(Lea también: Ante la JEP buscan reconocimiento de los desaparecidos en Hidroituango)](https://archivo.contagioradio.com/audiencia-ante-la-jep-por-desaparecidos-hidroituango/)

### Impunidad en Arauca 

El abogado señala la dificultad que existió a la hora de recopilar información proveniente de la Fiscalía,  entidad que no tiene suficientes elementos materia de prueba; lo que en relación con otros casos de falsos positivos como los ocurridos en Soacha o en la Costa Atlántica, los casos de Arauca no cuentan con mucha información que dé con los máximos responsables.

Este alto nivel de impunidad se ve reflejado en la justicia ordinaria, puesto que tan solo en 4 de estos 79 casos se han dictado sentencias, limitadas únicamente a los autores materiales, mientras **los otros 75 continúan en indagaciones preliminares.**

El informe, que recopiló información a lo largo de un año y cuatro meses,  representa una exigencia de las víctimas para que se juzguen y sancionen las violencias cometidas contra los derechos humanos y el derecho internacional humanitario.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43539505" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43539505_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
