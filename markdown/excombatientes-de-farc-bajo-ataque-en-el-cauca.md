Title: Excombatientes de Farc bajo ataque en el Cauca
Date: 2020-01-08 15:34
Author: CtgAdm
Category: Nacional
Tags: ataques, Cauca, excombatientes, FARC, violencia
Slug: excombatientes-de-farc-bajo-ataque-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ultima-marcha-de-la-guerrillas-de-las-farc-8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:NC Noticias] 

Este 8 de enero en Santander de Quilichao al norte del departamento del Cauca, se registro el ataque al excombatiente de Farc, Viyarmil Serna Andrade de 35 años, quien fue agredido por hombres desconocidos con un arma de fuego y posteriormente trasladado al hospital local donde fue atendido.

Serna se encontraba en el sector conocido como “El Parque de los Cholados”, en un local de jugos, cuando fue impactado tres veces por hombres armados que luego de disparar se dieron a la fuga. Según fuentes locales el hecho se atribuye a álias ´Mayimbú´, jefe de la estructura Jaime Martínez, disidencia de las Farc, quien habría amenazado de muerte al excombatiente.

Serna se habría desplazado desde el corregimiento de La Elvira, Buenos Aires, hacia Santander de Quilichao, como mecanismo de protección luego de las amenazas. (Le puede interesar: [66 excombatientes que apostaron a la paz han sido asesinados en 2019](https://archivo.contagioradio.com/66-excombatientes-que-apostaron-a-la-paz-han-sido-asesinados-en-2019/))

Ante este hecho la Fiscalía General de la Nación y la Policía Nacional abrieron una investigación que permita esclarecer el caso y ubicar a los responsables; hasta el momento se desconoce el estado de salud del excombatiente. (Le puede interesar: [¿Existen garantías de seguridad para excombatientes que habitan los ETCR?)](https://archivo.contagioradio.com/existen-garantias-de-seguridad-para-excombatientes-que-habitan-los-etcr/)

### Excombatientes de Farc se desplazan de su territorio por falta de garantías

Adicional el movimiento Fuerza Alternativa Revolucionaria del Común (FARC) denunció que excombatientes fueron desplazados por grupos armados en el sector de La Elvira, municipio de Miranda, norte del Cauca; según Antonio Pardo, encargado de reincorporación en este departamento, *“se desplazaron desde la última semana de diciembre, algunos a Popayán, otros a Santander de Quilichao, y estamos adecuando un espacio, una casa de paso, mientras encuentran otro lugar para seguir su proyecto de vida”*, dijo el vocero, aún se desconoce el la ubicación y el estado de los excombatientes.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
