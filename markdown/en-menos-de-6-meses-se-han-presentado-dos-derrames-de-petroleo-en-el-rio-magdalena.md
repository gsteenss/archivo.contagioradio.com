Title: En menos de 6 meses se han presentado dos derrames de petróleo en el río Magdalena
Date: 2018-06-14 12:41
Category: Ambiente, Nacional
Tags: Ecopetrol, petroleo, Río Magdalena, Santander
Slug: en-menos-de-6-meses-se-han-presentado-dos-derrames-de-petroleo-en-el-rio-magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Rio-Magdalena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Viva las Noticias] 

###### [14 Jun 2018] 

Un nuevo derrame de petróleo se produjo ayer entre las localidades de Canta Gallo, en el departamento de Bolívar, y Puerto Wilches, en Santander, que se ha extendido en una macha de 20 kilómetros por el río Magdalena. Los habitantes denuncian que ya hay graves afectaciones en el ecosistema porque el petróleo habría llegado a ciénagas y humedales. **En menos de 6 meses este es el segundo derrame de crudo que se presenta en el país.**

La fuga se produjo en una de las líneas de Ecopetrol que va desde el sector conocido como “Isla 8” hasta el sector de la “Curutuma”. La alerta la encendieron los pescadores de la región, que, hacia las dos de la mañana del miércoles 13 de junio, se percataron de la mancha que estaba sobre el río Magdalena.

Posteriormente hacia las 9 de la mañana Ecopetrol cerró la línea e inició las labores de contingencia. Sin embargo, de acuerdo con Oscar Sampayo, integrante del Centro de Estudios Sociales, Extractivos y Ambientales del Magdalena Medio-GEAM, desde que se dio la alerta hasta que Ecopetrol reaccionó hubo un tiempo de **7 horas en donde la mancha logró expandirse hasta el sector del bajo río Magdalena** y posteriormente a la desembocadura del río Sogamoso, justo uno de los puntos más caudalosos del Magdalena.

### ** Los impactos de la mancha de crudo por el Magdalena** 

Sampayo manifestó que dentro de las afectaciones más grandes y difíciles de revertir se encuentran los daños que provoca el petróleo a los ecosistemas que ya ha llegado a las ciénagas y humedales que se encuentran en las laderas del río desde donde se provocó el derrame hasta la desembocadura del río Sogamoso.

“**Hoy las playas y las riberas del río Magdalena, tanto para los lados del margen derecho que es Santander y César**, como los del izquierdo que son los departamentos de Antioquia, Bolívar y Magdalena, indudablemente tienen impactos y la situación va a ser bien compleja” afirmó Sampayo.

Frente a esta situación, Sampayo dijo que temen que no haya medidas sobre las acciones de Ecopetrol, como sucedió en el caso del derrame de petróleo en la Lizama, en dónde aún no hay sanciones a ninguna de las instituciones y autoridades por uno de los daños ambientales más grandes que ha tenido que afrontar el país. (Le puede interesar: ["Amenazan a ambientalista que exige medidas sobre desastre de petróleo en la Lizama"](https://archivo.contagioradio.com/amenazan-a-ambientalista-que-exige-medidas-sobre-desastre-de-petroleo/))

<iframe id="audio_26538565" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26538565_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
