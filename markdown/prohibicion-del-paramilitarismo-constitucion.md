Title: Prohibición del paramilitarismo tendrá rango constitucional
Date: 2017-08-29 17:09
Category: DDHH, Nacional
Tags: Congreso de la República, Paramilitarismo
Slug: prohibicion-del-paramilitarismo-constitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/DIbQFxJVYAAF8eF.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Angela Robledo] 

###### [29 Ago 2017]

**Con una votación de 99 a favor y 10 en contra, la plenaria de la Cámara de Representantes aprobó la prohibición constitucional del paramilitarismo.** Quedó aprobado el Proyecto de Acto Legislativo 015/2017 Cámara – 004/2017 Senado “Por medio del cual se dictan disposiciones para asegurar el monopolio legítimo de la fuerza y del uso de las armas por parte del Estado”

Aunque luego de la votación hace falta la conciliación entre los textos de senado y cámara, los cambios son mínimos y seguramente los congresistas apoyarían la versión de Senado que se apega a la propuesta inicial.

Les compartimos algunos de los argumentos jurídicos y políticos que hoy ganaron este debate:

**Una de las razones la da el senador Iván Cepeda “el paramilitarismo es un flagelo que ha sacudido la historia del país** desde hace décadas como una política del desdoblamiento del Estado en organizaciones que han sembrado el terror” y señala que “en el código penal no hay una disposición formal que diga que formar grupos paramilitares está prohibido”.

Otro de los argumentos lo expone Camilo Gonzales Posso, director de INDEPAZ, el también ex comisionado de paz indica que “Debe entenderse que para que este país cierre un periodo de guerra, **de violencia sistemática es muy importante que las normas constitucionales consagren un pacto de adiós a las armas en la política y en los negocios**”. (Le puede interesar:["Paramilitares asesinan a dos integrantes de la comunidad indígena Wounnan"](https://archivo.contagioradio.com/paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan/))

Otra razón es considerada por Rodrigo Uprimny, director de De Justicia, una de ellas es que si el paramilitarismo hubiese estado prohibido de manera tácita en la constitución se hubiera podido evitar la conformación de las Convivir**, célebres por haber sido una de las organizaciones que abrió la posibilidad de conformar grupos paramilitares en Colombia**.

Gustavo Gallón, director de la Comisión Colombiana de Juristas, responde al argumento de algunos detractores de la propuesta que aseguran que prohibir el paramilitarismo sería un reconocimiento de responsabilidad por parte del Estado. A ello Gallón responde que a **ningún Estado lo condenan porque prohíba en su Constitución un hecho atroz que ha ocurrido en su territorio**.

Por último, cerca de 70 organizaciones de Derechos Humanos aseguran que el paramilitarismo se convirtió en una práctica sistemática que ha dejado miles de víctimas en su accionar por el territorio nacional, además de ser una amenaza latente y actual dado el número de defensores de **DDHH asesinados y las evidencias de presencia de estos grupos en bastas regiones de la geografía nacional**. (Le puede interesar:["De enero hasta agosto de 2017 han sido asesinados 101 líderes sociales"](https://archivo.contagioradio.com/entre-enero-y-agosto-han-sido-asesinados-ciento-un-lideres-sociales/))

Por ello esas organizaciones aseguran que prohibir esta práctica e incluir esta decisión en la constitución es materia prima para construir una política estatal sólida de enfrentamiento a esos grupos. **“Por lo que su prohibición es indispensable para estimular el desarrollo de una sólida política estatal en la materia”**.

También señalan que “Este fenómeno ha tenido origen y ha sido prolongado por el Estado como instrumento de discriminación por las opiniones políticas y las acciones de reivindicación de derechos; y su impacto, a lo largo de varias décadas, **ha afectado el país en términos humanitarios, económicos y políticos, deteriorando los bienes jurídicos pilares de una sociedad democrática.**”

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
