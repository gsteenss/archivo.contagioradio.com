Title: Video| El rol de la inteligencia militar en los crímenes de Estado
Date: 2020-02-11 16:59
Author: AdminContagio
Category: DDHH, Video
Tags: Binci, Brigada XX, Inteligencia Militar, JEP
Slug: video-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/rol-de-la-inteligencia-militar.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/inteligencia-militar.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 7 de febrero, el informe «BINCI y Brigada XX: El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno(1977-1998)», fue entregado a la Jurisdicción Especial para la Paz, con la intención de revelar las conductas criminales cometidas por integrantes de esas instituciones castrenses, a partir de la Doctrina del enemigo interno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La investigación, elaborada por la Comisión de Justicia y Paz, el Colectivo de Abogados José Alvear Restrepo y la Coordinación Colombia Europa Estados Unidos, recopila 35 casos de tortura, 51 casos de ejecuciones extrajudiciales y más de 70 casos de desaparición forzada, como un conjunto de prácticas de la inteligencia militar que tenían como finalidad acabar con el «enemigo interno», que se traducía en una persecución a integrantes de movimientos sociales, partidos políticos de oposición, defensores de derechos humanos y periodistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Leer: ["Los crímenes de la inteligencia militar llegaron a la JEP"](https://archivo.contagioradio.com/los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep/)

<!-- /wp:paragraph -->
