Title: "El otro yo" de Mario Benedetti
Date: 2018-09-14 09:15
Author: AdminContagio
Category: Viaje Literario
Tags: El otro yo Benedetti, Mario Benedetti, Poesía uruguaya
Slug: el-otro-yo-de-mario-benedetti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/mario.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [<iframe src="http://www.ivoox.com/player_ek_8378430_2_1.html?data=mZikmpmXdI6ZmKiakpmJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmytSYpMrSqcXZ1dnWj5CpsIzj1dfcjd7TcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

###### 

###### [Mario Benedetti, "El otro yo"] 

###### [14 Sep 2018] 

[Mario Benedetti nació en paso de los toros, Uruguay el 14 de septiembre de 1920 , fue un escritor, poeta y dramaturgo integrante de la generación del 45, a la que también pertenece Juan Carlos Onnetti. ]

[Tuvo una prolífica producción literaria que incluyó más de 80 libros, Entre los que se incluyen · La muerte y otras sorpresas (1968), Con y sin nostalgia, "El tiempo que no llegó", La tregua,  Gracias por el fuego, Poemas de la oficina, Rincón de Haikus (1999), Canciones del que no canta (2006), Testigo de uno mismo (2008). Algunos de estos libros fueron traducidos a más de 20 idiomas. ]

[Mario Bennedetti murió en Montevideo el 17 de mayo de 2009.]

[En recuerdo por la fecha de su nacimiento, compartimos con ustedes su relato "El otro yo"].
