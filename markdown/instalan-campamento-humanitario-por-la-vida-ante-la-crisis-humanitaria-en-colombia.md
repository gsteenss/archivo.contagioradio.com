Title: Instalan Campamento humanitario por la vida ante la crisis humanitaria en Colombia
Date: 2020-09-07 17:02
Author: CtgAdm
Category: yoreporto
Slug: instalan-campamento-humanitario-por-la-vida-ante-la-crisis-humanitaria-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.38-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.39-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.39-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.38-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.39-PM-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.39-PM-1-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### Bogotá, Septiembre 7 de 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde tempranas horas de la mañana cerca de 30 personas han decidido instalar un campamento humanitario frente a una de las sedes de la Cruz Roja Internacional en Bogotá, buscando que esta organización pueda habilitar canales de comunicación oficiales con el Gobierno Nacional ante la crisis humanitaria que vive el país para que se tomen medidas inmediatas para frenar el asesinato de líderes sociales, desarticular el paramilitarismo y frenar definitivamente las masacres.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89451,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.22.38-PM-1.jpeg){.wp-image-89451}  

<figcaption>
**“Campamento Humanitario por la vida”**

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Según Indepaz,[tan solo en el 2020 se han presentado más de 50 masacres en Colombia y el asesinato de más de 573 líderes y lideresas sociales desde la posesión de Iván Duque](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/), hechos que se concentran principalmente donde persiste la presencia de bandas paramilitares y narcotraficantes. Ante esta situación, las organizaciones concentradas en el barrio del norte de la ciudad la Soledad, denuncian que el Gobierno no ofrece ningún tipo de medidas eficaces e integrales que den solución a este drama humanitario, y por el contrario, insiste en medidas militares y el retorno de la aspersión con glifosato como única solución a un problema que ha venido creciendo en su mandato.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es por ello que desde diferentes organizaciones sociales han decidido instalar el denominado **“Campamento humanitario por la vida”** en frente a la Cruz Roja Internacional., quienes reclaman entre otras exigir el cumplimiento de disposiciones existentes en el ordenamiento jurídico, la implementación de los programas y el funcionamiento de mecanismos tendientes a ofrecer las garantías democráticas, y de paz en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las medidas que solicitan está la implementación de la **resolución 1190 de 2018** que busca la garantía del derecho a la protesta, exigir la instalación de la Mesa de Seguimiento al respeto y garantía de la protesta pacífica a nivel nacional y en las ciudades principales del país donde no haya sido instalada, también buscan que los planes formulados en la **Comisión Nacional de Garantías de Seguridad (CNGS)** desde el 2018 para el desmantelamiento de organizaciones o conductas criminales que amenazan los derechos a la vida, la integridad y la libertad de los líderes sociales y las comunidades afectadas por las violencias, sean implementados y haya una ruta concreta y viable para su implementación; retornando a un enfoque social en la formulación de estos planes, dando vigencia a la **CNGS** por encima de la **Comisión del Plan de Acción Oportuna (PAO).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el **“Campamento Humanitario por la vida”** llaman a la nueva líder de la Cruz Roja Internacional en Colombia, Mulan Giovannini, a que sostenga una reunión de urgencia con las organizaciones sociales para que pueda atender el pliego de peticiones y habilitar un diálogo directo con el alto Gobierno. Afirman que no se retirarán del lugar hasta poder obtener alguna respuesta de parte de la organización internacional y por ello convocan a una gran velatón por la vida para hoy, **lunes 7 de septiembre en frente de sus sedes a nivel nacional e internacional desde las 6pm.**

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Mas columnas de Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### Te invitamos a participar  enviándonos tus notas, reportajes, noticias y fotografías de temas relacionados con derechos humanos en Colombia y el mundo.

<!-- /wp:heading -->
