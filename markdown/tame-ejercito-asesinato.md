Title: Denuncian que Ejército habría asesinado campesino en Tame Arauca
Date: 2017-10-29 18:50
Category: DDHH, Nacional
Tags: Arauca, asesinato, ejercito, Tame
Slug: tame-ejercito-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/muerto-tapado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo 

##### 29 Oct 2017

La Fundación de Derechos Humanos Joel Sierra emitió una denuncia donde dan cuenta del **ataque por parte de militares a dos hombres y un niño de 8 años en Tame, Arauca**, resultado del cual uno murió, el otro resulto herido y se ocasionaron afectaciones psicológicas en el menor de edad.

De acuerdo con la comunicación, **Alejandro Tirado y Lizander Valencia Rodríguez, acompañados por Harvin Valencia Peñaranda**, hijo del segundo, transportaban gasolina a las 3:20 de la mañana cuando fueron requeridos por los uniformados en el punto de control ubicado en el centro poblado de Betoyes municipio de Tame.

Según lo reportado, tras estacionar el vehículo a escasos metros de la garita y de los campesinos dar la espalda y caminar unos pasos en dirección a una caseta civil, **los militares dispararon a quema ropa asesinando a Tirado, dejando herido de gravedad a Lizander Valencia, y con afectaciones psicológicas al menor de edad**.

Tras el ataque, como reporta la Fundación, Valencia recibió atención por sus heridas en el Hospital de Sarare, para ser remitido en avión ambulancia a un centro de salud de tercer nivel, mientras que **el niño fue puesto a disposición del Instituto Colombiano de Bienestar Familiar** (Le puede interesar: [CIDH admite demanda por tortura a 7 mujeres en Putumayo](https://archivo.contagioradio.com/cidh-admite-demanda-por-tortura-a-7-mujeres-en-putumayo/))

Desde la organización envían un llamado al Estado colombiano y al Ejército Nacional para que "**ponga a disposición de la justicia ordinara a los militares responsables del asesinato**, las heridas y daños psicológicos causados contra los campesinos esta madrugada en Betoyes, Tame".

Adicionalmente solicitan que se entreguen a la Fiscalía las armas con las que se cometió el hecho y retirar a los autores materiales y determinadores del crimen, así como **adelantar una investigación seria y ágil para dar con los responsables y garantizar la reparación integral a las víctimas**.

En el comunicado se hace énfasis en que **muchos labriegos de tradición deben buscar alternativas de subsistencia como la comercialización de combustibles** traídos desde Venezuela **por la crisis del agro** generada por las imposiciones de medidas a los agricultores mientras se acelera el despojo para fortalecer proyectos minero-energéticos.
