Title: Cajamarca no está arrepentida de la consulta popular
Date: 2018-02-26 13:11
Category: Nacional
Tags: Anglo Gold Ashanti, Cajamarca, consulta popular en cajamarca, Mineria
Slug: cajamarca-no-esta-arrepentida-de-la-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CENSAT] 

###### [26 Feb 2018] 

Tras la divulgación del artículo periodístico de Portafolio, en donde argumentan que luego de la consulta popular en Cajamarca el municipio se encuentra en quiebra, quienes promovieron este proceso de consulta indicaron que **esto no es cierto**. Afirmaron que el artículo se hizo haciendo alusión a los intereses de la empresa minera Anglo Gold Ashanti y que los cajamarcunos no están arrepentidos de la decisión que tomaron.

Hace un año, 6165 habitantes de ese municipio en el Tolima dijeron **no en las urnas** a las actividades mineras en su territorio. A partir de allí, han trabajado por el desarrollo de acciones alternativas de economía donde prime la agricultura y el eco turismo. Para algunos ambientalistas, esta consulta popular sentó un precedente en el país para la realización de actividades extractivas en el país.

### **Declaraciones del alcalde de Cajamarca son “indignantes”** 

De acuerdo con Robinson Mejía, integrante del Comité Ambiental y Campesino de Cajamarca y Anaime, hay una sorpresa en el municipio ante las declaraciones del alcalde Pedro Pablo Marín a Portafolio. Dijo que **“no ha tenido ni la precaución ni tampoco la rigurosidad** de revelar las cifras y hoy podemos decir que el alcalde le miente al país”.

Las cifras a las que hace alusión el ambientalista se refieren a que, en Portafolio, el alcalde de Cajamarca manifestó que al municipio dejaron de entrar cerca de **2 mil millones de pesos al mes** desde que la empresa minera salió del municipio. Además, indicó que el presupuesto de 2018 para el municipio se redujo debido a que no se recaudarán cerca de 100 millones.

Ante esto, Mejía afirmó que el presupuesto del municipio ha crecido “**cerca de 2 mil y 3 mil millones desde el año pasado** y hoy se está discutiendo en el concejo municipal cuál va a ser la reducción en el pago del predial que tienen que pagar los cajamarcunos”. Además, afirmó que “es indignante que se diga que Cajamarca está en jaque porque la Anglo Gold Ashanti dejó de pagarle 20 millones por impuesto reteica al municipio”.

Esto, para quienes defendieron la consulta popular “**es una falta de respeto** y no se ha entendido el mandato de los cajamarcunos”. Frente al artículo periodístico, Mejía afirmó que “a quienes reseñan son cuatro personas que fueron contratistas de la empresa que hoy, obviamente, no están teniendo sus contratos porque la empresa debió salir del territorio y están respondiendo a intereses mezquinos”. (Le puede interesar:["Desde las entrañas de la tierra y junto a los gritos del agua Cajamarca dijo no a la minería"](https://archivo.contagioradio.com/cajamarca-consulta-minera/))

### **Empresa ha hecho un ejercicio publicitario para “mover un imaginario” equivocado** 

Mejía enfatizó en que desde la empresa minera se ha tratado de tergiversar lo que pasó con la consulta popular pues “el artículo sólo tiene dos fuentes, los contratistas de Anglo Gold y la a**dministración municipal** que financió la campaña del alcalde de Cajamarca”. Por esto, reafirmó que “la gente está en la disposición de poder hacer el trabajo que se viene haciendo para impedir que la empresa regrese”.

Manifestó que han trabajado con los campesinos para potenciar el trabajo del campo pues “gracias a la consulta popular hoy empresas y restaurantes **le están comprando comida a los cajamarcunos**”. También han podido avanzar en el desarrollo de iniciativas de agro turismo y esto confirma el trabajo sostenido de quienes defendieron la consulta popular.

### **Muchos trabajadores del proyecto minero La Colosa están trabajando en el campo** 

El ambientalista indicó que cuando Anglo Gold estaba en el territorio, “había 72 trabajadores de Cajamarca que ahora trabajan en proyectos como la doble calzada hasta Ibagué y también en el campo”. Enfatizó en que esta actividad genera diariamente **10 mil empleos** y “hoy hay campesinos que se quejan porque no consiguen mano de obra para poder seguir cultivando y cosechando la comida”.

Además, dijo que, con el boom de la minería, el municipio tuvo una **población flotante de 2 mil habitantes** “que vino a buscar trabajo con la empresa y no lo logró”. Luego de la consulta popular se tuvieron que ir y las casas quedaron vacías. Sin embargo, “esto no quiere decir que la gente esté pasando hambre si no que la economía se está dinamizando de otra forma”. (Le puede interesar: ["¡Sí se pudo! Cajamarca dijo no a la minería en su territorio"](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/))

Finalmente, El Olfato, medio de comunicación de Ibagué, indicó que el alcalde de Cajamarca, para su elección, contó con el apoyo de los políticos que simpatizaban con la empresa Anglo Gold Ashanti como el representante a la Cámara por el partido de la U, Carlos Edward Osorio, el representante conservador Miguel Barreto y el diputado del Centro Democrático Milton Restrepo. Recuerda el medio de comunicación que **“Álvaro Uribe autorizó la exploración minera en zona rural de Cajamarca”.**

<iframe id="audio_24080739" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24080739_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
