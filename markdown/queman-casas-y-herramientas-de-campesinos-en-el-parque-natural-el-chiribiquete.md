Title: Queman casas y herramientas de campesinos en el Parque Natural el Chiribiquete
Date: 2019-07-10 17:37
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Chiribiquete, Guaviare, Plan Artemisa, PNN
Slug: queman-casas-y-herramientas-de-campesinos-en-el-parque-natural-el-chiribiquete
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/PLAN-ARTEMISA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Mongabay] 

Los ataques contra la población campesina en el PNN (Parque Nacional Natural) el Chiribiquete ubicado en San José del Guaviare no cesan. Según la denuncia de los campesinos el pasado 26 de junio, efectivos del Ejército quemaron las casas y destruyeron las herramientas de trabajo de dos familias.

Los campesinos fueron obligados a desplazarse  con el pretexto de que se encontraban en una zona de ampliación del PNN del Chiribiquete, que además colinda con la  reserva campesina del Guaviare. (Le puede interesar ["Plan "Artemisa" habría arrancado con desplazamiento forzado en Chiribiquete"](https://archivo.contagioradio.com/plan-artemisa-habria-arrancado-con-desplazamiento-forzado-en-chiribiquete/))

La Fiscalía y la Procuraduría no se han pronunciado ni han tomado medidas respecto a  los deforestadores, pues“lo que vemos es que hay un tratamiento diferencial entre campesinos pobres y grandes acaparadores ubicados en el  Guaviare” expone Sammy Sánchez, profesional asesora de organizaciones campesinas que ocupan los PNN.

### **Condición de los campesinos ** 

En cuanto a los campesinos desplazados en el mes de abril, Sánchez explica que “se les ha realizado acompañamiento jurídico, además se espera que el Gobierno repare y reubique a estas personas, que también fueron obligados  a firmar documentos donde se les prohíbe regresar a la zona”.

Respecto a los eventos del pasado mes de junio fueron dos acciones que afectaron a los campesinos y campesinas, habitantes de las veredas de  Manabil y Choapal en el Corregimiento El Capricho, en San José del Guaviare. Ellos y ellas también fueron desplazados, obligados a firmar documentos para no retornar a la zona protegida, además de ser señalados de cometer delitos ambientales.

Los campesinos tienen iniciativas que pueden generar ingresos, y gestionar la protección de los territorios, pero hace falta una política social, comunitaria y acompañamiento del Gobierno.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38274233" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38274233_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
