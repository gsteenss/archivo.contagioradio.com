Title: Estos son los finalistas al Premio Nacional a la Defensa de los Derechos Humanos
Date: 2016-09-02 13:21
Category: DDHH, Nacional
Tags: Defensores Derechos Humanos, Diakonia Colombia, Premio Derechos Humanos
Slug: los-finalistas-del-premio-defensores-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/13770335_1131408900246331_8791992980120098101_n-e1472840372314.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diakonia Colombia 

###### [04 Agos 2016] 

Luego de seleccionar entre **49 nominados**, fueron presentados los **16 finalistas** para la quinta edición del **Premio Nacional a la Defensa de los Derechos Humanos en Colombia**, otorgado por la ONG sueca **Diakonia**, como respaldo a la labor de hombres, mujeres y organizaciones que trabajan por su garantía en ciudades y campos del país.

La lista de pre-seleccionados en las **3 categorías** reconocidas con el galardón, fue presentada en la Universidad Javeriana de Bogotá, ante representantes de las organizaciones postulantes, mismas que han trabajado por décadas en la defensa de los Derechos Humanos.

Para esta versión, el premio estará enmarcado por diferentes actividades que hacen parte de la "**Semana por la paz**", que se desarrollará entre el **4 y el 11 de septiembre** y por los diálogos de paz que se adelantan en la Habana entre el gobierno colombiano y la guerrilla de las FARC EP.

**Los finalistas.**

**Categoría 1 : Defensor o Defensora del Año **

**Angélica Ortiz**, organización Fuerza de Mujeres Wayuu (FMW); **Julio Cesar Rivera Cantor**, Corporación Orinoquía Tierra Mágica; **Briands David Harnache Moreno**, Asociación Nacional de Ayudas Solidarias, ANDAS;  **José Milciades Sánchez Ortiz**, Sindicato Nacional de Trabajadores y Empleados Universitarios de Colombia; **María Lía Soto**, Fundación para la Defensa de los Derechos Humanos, FUDEHU; **Luz Ángela Uriana Epiayu**, Proceso de Defensa de los Derechos de los Niños afectados por la minería en La Guajira; **Jorge Freytter Florian**, Asociación Jorge Adolfo Freytter Romero, Estudios sobre Violencia Política en Latinoamérica; **Daría Cristina González Arias**, Lideresa Organización Trans del Sur.

**Categoría 2A : Experiencia o Proceso Colectivo del Año  **

Grupo de Familiares Víctimas de la Vereda La Esperanza, Asociación de Vecinos Las Vegas, Venecia; Gestores Humanitarios de San Andrés de Tello, Corredor Norte del Huila-Sumapaz; Comité de Impulso del Sujeto de Reparación Colectiva Libertad, Municipio de San Onofre; Consejo Comunitario Ancestral del Caserío de Roche, Consejo Comunitario de la Comunidad Negra del Rio Naya, Asociación de Cabildos Indígenas de Toribio, Tacueyo y San Francisco, ‘’Proyecto Nasa’’; Organismo de Acción Comunal; Organización de Campesinos de los Montes de María, Veredas Altas sur de Bolívar; Madres de Soacha, madres víctimas de ejecuciones extrajudiciales en un proceso cultural y artístico de visibilización y denuncia; Junta Social Pro Reubicación de Tabaco, municipio de Hatonuevo, La Guajira; Jóvenes por Amor Arte LGTBHP del Cauca.

**Categoría 2B : Experiencia o Proceso Colectivo del Año – Nivel ONG Acompañantes**

Corporación Jurídica Yira Castro, Plataforma 5 Claves; Parces ONG, Pares en Acción-Reacción contra la Exclusión Social; Humanidad Vigente, Corporación Jurídica; Fundación Funpaz; Fundación Luker, Transformación Educacional en Manizales; Corporación Ágora Club, Organización de Derechos Humanos; Escuela de Formación Artística y Promoción Cultural Ciudad Norte; Cuerpos Gramaticales; Corporación Cultural Horizonte Ciudadela Educativa; Colectivo Sociojurídico Orlando Fals Borda, Corporación Colectivo de Abogados Luis Carlos Pérez, Corporación Autónoma para el Desarrollo Integral y Sostenible de García Rovira, CADISGAR; Asociación de Hermandades Agroecológicas y Mineras de Guamocó-AHERAMIGUIA.

**Categoría 3 : Reconocimiento a ‘Toda Una Vida’ **

**Tito Epifanio Garzón Gómez**, Líder comunal, social y ambientalista del municipio de Vista Hermosa y Reserva La Macarena, Meta; **María Tila Uribe de Trujillo**, Centro de estudios e Investigaciones del Trabajo, CESTRA; **María Ruth Sanabria Rueda**, Comité Permanente por la Defensa de los Derechos Humanos-CPDH, Arauca; **Rosa Emilia Cadavid Carmona**, Misionera Laurita de la Fundación Obra Social Madre Laura; **Gerardo Fajardo Morales**, Movimiento Humanista Fundación Futuro Humano; **Eduardo Carreño Wilches**, Abogado miembro fundador de la Corporación de Abogados José Alvear Restrepo; **Carmen Alicia Chávez Agreda**, Fundación Social Colombiana CRESER; **Padre Pedro Elías Joya Aponte**, sacerdote misionero derechos humanos y alternativas de paz con justicia social y ambientalista; **Alfonso Mora León**, Defensor de derechos humanos, campesino, salubrista.

**Los ganadores **

De los 16 finalistas, el comité organizador deberá seleccionar a los **cuatro ganadores**, elección que se llevará a cabo el **9 de septiembre**, día de los Derechos Humanos en Colombia en **homenaje a San Pedro Claver**, primer defensor de derechos humanos que hubo en Colombia, a las 8:30 am en el auditorio Felix Restrepo de la Universidad Javeriana.
