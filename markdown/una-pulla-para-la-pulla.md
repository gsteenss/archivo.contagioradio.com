Title: Una pulla para la pulla
Date: 2019-10-24 11:18
Author: Sasha Yumbila Paz
Category: Opinion
Tags: Arauca, coca, ELN, FARC, fumigaciones, pulla
Slug: una-pulla-para-la-pulla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/pulla-para-la-pulla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

> [“La inteligencia humana no admite que le digan mentiras”]

**Javier Dario Restrepo**

*[“Arauca fue el segundo departamento después de la firma de los acuerdos con las FARC en erradicar todos, todos los cultivos de coca que había en la región”]*[dice el rimbombante vídeo entregado el 21 de octubre del presente año por **La Pulla** \[1\], utilizando mentiras refinadas para atribuirle el éxito de erradicación de los cultivos de coca a los protagonistas de los acuerdos que se firmaron con las FARC.]

### **Un poco de historia.** 

[**Los hechos no coinciden con el palabrerío de La Pulla**. A mediados de los 80 llega el Boom petrólero junto de la mano de publicidad engañosa en el departamento de Arauca, el cual prometía inversión social, garantías de vida a las comunidades, pero las políticas de gobiernos, ni brindó la inversión social, ni la protección a comunidades; lo que sí causó fue un desastre ambiental y étnico.]

[El incumplimiento al deber de llevar inversión social e infraestructura con las regalías que dejaba la explotación de petróleo, proporcionó durante los mismos años la producción de la siembra de cultivos de uso ilícito, como modo de subsistencia de las comunidades.]

[Ante el incremento de los cultivos, el Gobierno respondió con la militarización del territorio y la política de  fumigación con glifosato, la que se fortaleció en los mandatos de Uribe (2002-2010), sumándose la proriferación del paramilitarismo en la región. ]

[El i**nforme de Monitoreo de la Oficina de las Naciones Unidas contra las Drogas 2007** concluye que hubo desminución de los cultivos de uso ilicíto en Arauca de un 80,4 %, en el año 2003 pero la fracasada política antidrogas de fumigación conllevó a que en un año incrementara de nuevo los cultivos de uso ilicíto en un 188% (Cultivos de coca por departamento en Colombia 2000-2006, hectáreas \[2\].)]

[Sin expectativas de subsistencia, un inevitable desastre ambiental a causa de las fumigaciones, con el territorio militarizado y un tejido social debilitado por la cultura del Narcoparamilitarismo que se estaba generando en la zona, la comunidad tomó la decisión de sustituir voluntariamente los cultivos de uso ilicíto. ]

### **Una verdad criminalmente silenciada.** 

[El movimiento campesino, agrupado en diferentes organizaciones sociales, como la]*[“Asociación Departamental de Usuarios Campesinos de Arauca -ADUC-, Juntas de Acción Comunal y la Cooperativa Agropecuaria del Sarare -COOAGROSARARE-]*[\[3\]”, entre otras organizaciones, iniciaron en el año 2007 la lucha política para contrarrestar la violencia estatal, el impacto social, económico, ambiental y de vulneración de Derechos Humanos. Es así como se pasa a la acción organizada de erradicar y sustituir de manera voluntaria y permanente los cultivos de uso ilicíto.]

[En comunicaciones del 2012 de la Cámara de Representantes al General Antinarcóticos Álvaro Caro Meléndez, se confirma que un solo municipio, Fortul, en Arauca, “]*[Efectivamente se ha desarrollado un importante programa de erradicación manual, el cual ha permitido erradicar cerca del 95% de los cultivos ilícitos existentes e implementar una política de sustitución por cultivos de yuca, plátano, cacao, maíz y pastos]*[” \[4\]. Para ese mismo año la organización campesina afirma que de manera voluntaria pasaron de 1.038 a 80 hectáreas de cultivos de coca, y que continuarían con la sustitución voluntaria en Saravena, Fortul, Arauquita y Tame.]

[Así mismo, los informes de la UNODC datan que para  los años 2006-2012, **hubo disminución de las hectárias cultivadas con hoja de coca. En seis años pasaron de 1.306 hectárias a 81 hectarias**\[5\],  información que concuerda con las afirmaciones de la comunidad campesina.]

[Para nadie es un secreto que el Frente Domingo Laín Sáenz del ELN, respaldó, respetó y sigue respetando la decisión de las comunidades de la erradicación y sustitución voluntaria de los cultivos de uso ilicíto; y también para nadie es un secreto que las FARC-EP, por el contrario, no respetaron la decisión del campesinado, porque en sus planes estaba seguir financiándose del narcotráfico. Esto en el marco de un conflicto que comenzó a finales de 2005 y que se extendió hasta el 2009,]*[“entre las FARC Y el ELN, porque FARC apoyaba la mafia, mientras que los Elenos no, entonces comenzaron a darse plomo parejo y nosotros los campesinos en el medio, hubo familias afectadas, desplazamiento forzado y muertes por la actividad mafiosa”]*[\[6\].]

[En resumidas cuentas, queda en evidencia que no es gracias a los programas del Estado, ni  del proceso de negociación con las FARC que Arauca sea un territorio libre de coca, como lo presenta La Pulla.]

### **Verdades a medias con fines de propaganda electoral.** 

[A la Pulla se le olvida mencionar en el mismo video cuatro pequeños detalles sobre **Julio Acosta Bernal, exgobernador de Arauca.**]

1.  [Está vinculado penalmente por su responsabilidad en el asesinato del Delegado de la Registraduría Juan Plazas Lomónaco, el 10 de julio del 2003. ]
2.  [Su amistad con los paramilitares la empezaría desde el año 1997.]
3.  [Julio Acosta Bernal fue uno de los principales aliados de Álvaro Uribe Vélez en su primer Gobierno.]
4.  Y el más áspero: se le olvidó escribir o quizás omitió en el libreto que el candidato por el Centro Democrático (partido de Uribe) a la gobernación de Arauca, Luis Emilio Tovar Bello, construyó su capital político de la mano de Julio Acosta Bernal, es decir con los paramilitares y tiene procesos en la Procuraduría y la Contraloría por corrupción durante su cargo como alcalde de Arauca \[7\].

[Otra perla que se omite en el libreto de La Pulla, es que Julio Acosta Bernal, consiguió que el bloque paramilitar vencedores de Arauca - BVA, le financiara su campaña por la gobernación de Arauca en el año 2003. Así lo confirma la Corte Suprema de Justicia: ]

> *[“derivados de su connivencia… y en concreto, con la Casa Castaño desde el año 2001, que le permitieron reunirse a finales del año 2002 con miembros de la organización criminal, para planear la muerte de personas presuntamente vinculadas con la guerrilla y que pudieran representar un obstáculo a sus aspiraciones políticas, tal como se refiere que sucedió con la víctima Plazas Lomónaco, el 10 de julio de 2003, y , así mismo, recibir apoyo económico de dicha organización para la financiación de su campaña política a la Gobernación de Arauca el 26 de octubre de 2003”]*[ \[8\].]

[La Pulla también omite en su periodismo visceral, que así el Centro Democrático pose de independiente en Arauca, sus candidatos tienen en sus genes la escuela de Julio Acosta Bernal. ]

### **Reflexión de un maestro.** 

[Tomándonos un café con unos amigos y el precursor del periodismo ético, Javier Darío Restrepo, al preguntarle por el formato de La Pulla, nos hizo está reflexión. ]** **

*[“Un lenguaje muy ágil, muy imaginativo, pero está concebida como una forma de entretenimiento y no como un imperativo de cambio. Se debe utilizar un lenguaje que atrape la atención, que facilite la comprensión de las cosas, para finalmente compartir con los demás; un pensamiento serio sobre la realidad social. Pero qué pasa con eso? …hay quien se queda en la primera parte”.]*

[Es decir, La Pulla se ufana en llamar la atención, dice verdades a medias y desconoce el trabajo de años de las Organizaciones Sociales y Campesinas en referencia a la sustitución de cultivos de uso ilicíto en el departamento de Arauca.]

#### **Sasha Yumbila Paz - @yumbila**

1.  [\[1\]][[https://www.youtube.com/watch?v=1ttK4PmUlfE]](https://www.youtube.com/watch?v=1ttK4PmUlfE)
2.  [\[2\]][[https://www.unodc.org/pdf/research/icmp/colombia\_2006\_sp\_web.pdf]](https://www.unodc.org/pdf/research/icmp/colombia_2006_sp_web.pdf)
3.  [\[3\]][[https://notiagen.wordpress.com/2012/04/17/en-el-departamento-de-arauca-solo-quedan-unas-ochenta-hectareas-de-coca-organizaciones-sociales/]](https://notiagen.wordpress.com/2012/04/17/en-el-departamento-de-arauca-solo-quedan-unas-ochenta-hectareas-de-coca-organizaciones-sociales/)
4.  [\[4\]][[https://trochandosinfronteras.info/nosotros-la-erradicamos-cambiamos-la-coca-por-la-vida/]](https://trochandosinfronteras.info/nosotros-la-erradicamos-cambiamos-la-coca-por-la-vida/)
5.  [\[5\]][[https://www.unodc.org/documents/crop-monitoring/Colombia/Colombia\_Monitoreo\_de\_Cultivos\_de\_Coca\_2012\_web.pdf]](https://www.unodc.org/documents/crop-monitoring/Colombia/Colombia_Monitoreo_de_Cultivos_de_Coca_2012_web.pdf)
6.  [\[6\]][[https://kaosenlared.net/colombia-santos-miente-cuando-desconoce-a-las-organizaciones-sociales-de-arauca-como-promotoras-de-la-erradicacion-de-coca/]](https://kaosenlared.net/colombia-santos-miente-cuando-desconoce-a-las-organizaciones-sociales-de-arauca-como-promotoras-de-la-erradicacion-de-coca/)
7.  [\[7\]][[https://pares.com.co/wp-content/uploads/2019/08/Informe-Candidatos-Cuestionados-2019-1.pdf]](https://pares.com.co/wp-content/uploads/2019/08/Informe-Candidatos-Cuestionados-2019-1.pdf)
8.  [\[8\]][[https://www.colectivodeabogados.org/?Julio-Acosta-Bernal-sigue-vinculado-penalmente-por-homicidio-de-registrador-y]](https://www.colectivodeabogados.org/?Julio-Acosta-Bernal-sigue-vinculado-penalmente-por-homicidio-de-registrador-y)

