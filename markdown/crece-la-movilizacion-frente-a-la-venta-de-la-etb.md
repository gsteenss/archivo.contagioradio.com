Title: Crece la movilización frente a la venta de la ETB
Date: 2016-05-23 14:06
Category: Movilización, Uncategorized
Tags: Consejo de Bogotá, Venta de la ETB
Slug: crece-la-movilizacion-frente-a-la-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/13244030_1604630046531452_8321476486452042950_o-e1464029894459.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ETBnosevende 

###### [23 May 2016] 

Este fin de semana se realizó la exposición , en el Concejo de Bogotá, de la diferentes ponencias sobre el Plan de Desarrollo de la capital en donde se encuentra la venta de la empresa pública ETB,  el día de hoy se llevará a cabo la votación por comisión y el próximo fin de semana se realizará la votación en plenaria.

Durante el fin de semana **diferentes movilizaciones se hicieron presente a las afueras del Concejo de Bogotá** en rechazo a la venta de la ETB, al mismo tiempo se realizó la exposición de los concejales sobre el articulado del plan de desarrollo; el paso siguiente será realizar la votación el día de hoy, de cada uno de los puntos que conforma el articulado del Plan de Desarrollo por parte de la comisión que se constituye por 15 integrantes de consejo. Posteriormente se realizará **la votación en plenaria donde participan todos los concejales, esta votación se realizará el próximo fin de semana.**  Sin embargo los articulados que más debate han generado [son el 128 y 129 que tienen que ver con la enajenación de acciones de la ETB](https://archivo.contagioradio.com/asi-fue-el-cacerolazo-no-a-la-venta-de-la-etb/).

De acuerdo con Alejandra Wilches, presidenta de la Asociación Nacional de Técnicos en Telefonía y Comunicaciones a fines de la ETB (ATELCO) "**Pareciera que el plan de desarrollo se estuviera discutiendo detrás del salón y no en el consejo**, hay demasiados movimientos extraños que seguramente se notaran en la votación de hoy y la votación de plenaria, nosotros lo que le hemos reiterado a nuestros trabajadores y a la ciudadanía que ha estado firme acompañándonos, es que independiente de la votación de hoy y la de plenaria, la lucha no acaba acá y esa no será la última decisión sobre la ETB".

A su vez, reitera que **los intereses económicos detrás de la ETB son altos** y que pese a los cuestionamientos que se han hecho en el Consejo, tanto al alcalde de la capital como al presidente de la ETB,que no han sido respondidos, se tiene información de presuntos acercamientos a multinacionales con respecto a la empresa.

[Para el día de hoy se programo un plantón frente a las instalaciones del Consejo de Bogotá y se espera que se continúe con una movilización](https://archivo.contagioradio.com/con-cacerolazo-bailable-ciudadanos-se-oponen-a-la-venta-de-la-etb/) permanente frente a la exigencia de la no venta de la empresa ETB.

<iframe src="http://co.ivoox.com/es/player_ej_11635355_2_1.html?data=kpajlZqXeZahhpywj5WdaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYzLytHQysrXb46fotjcxc7Fp8qZpJiSpJjSb6%2FVxM7c0MbQb8XZjLmSpZiJhZrXz87Q0diPqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
