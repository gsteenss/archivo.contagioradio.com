Title: Duque: ¡La economía se revienta! Propuestas desde nuestra curul
Date: 2020-09-16 21:44
Author: CtgAdm
Category: Opinion
Slug: duque-la-economia-se-revienta-propuestas-desde-nuestra-curul
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/coins-money-colombia-weights-currency-saving-business-investment-banking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*Por: Ana Erazo | Concejala de Cali*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Necesitamos tomar medidas urgentes para evitar el desplome de la economía y que millones de colombianos de los estratos populares y medios caigan en la pobreza. **Según el Dane, en lo corrido del año la economía ha decrecido un 7.4% frente a 2019. Panorama preocupante que se viene agravando con el paso del tiempo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto se ha reflejado en el incremento del índice de desempleo, que a nivel nacional bordea el 20 %, y en Cali alcanza la preocupante cifra de 27.7%. A esto se suman las presiones derivadas de las crisis a nivel global:  menores precios de exportación de las materias primas (como el petróleo), que afectan los términos de intercambio para los países productores, a lo que se añade la caída de las remesas de trabajadores. Se han observado también salidas de capitales y un deterioro de las condiciones financieras internacionales que dificultan acceder a nuevo endeudamiento en el exterior.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La respuesta del gobierno ante este dramático cuadro de la actividad económica ha sido privilegiar al gran capital ignorando a los hogares, trabajadores y pequeña empresa.  Más allá de anuncios vacíos en el show presidencial de las 6 de la tarde, **no se han implementado medidas económicas que protejan efectivamente la fuerza laboral e industrial del país.** Se han redireccionado recursos desde los entes territoriales a la banca (Decreto 444) y a pesar de la disminución del encaje bancario para que los bancos dieran crédito, no lo han hecho. En el proyecto de presupuesto para el próximo año, presentado el pasado 29 de julio**, se prevé obtener \$ 12 billones de pesos por la venta de empresas públicas. Adicionalmente, el gobierno se propone salir a buscar con sus buenos amigos de la banca \$ 56.2 billones para financiar autopistas 5G como supuesto motor de reactivación.** Por si fuera poco, y con el beneplácito de los grandes gremios, se alista una reforma tributaria que tiene como gran solución la receta neoliberal de siempre: expandir la base de los declarantes de renta y el cobro del Iva a todos los productos hasta ahora exentos, incluyendo la canasta familiar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recurrir al desarrollo de grandes autopistas es una medida equivocada ante el colapso masivo de las unidades productivas intensivas en mano de obra. Tan solo en Cali, según Acodrés, han cerrado más de 480 restaurantes durante la crisis. Empresas con tradición que creaban puestos de empleo de carácter permanente y no transitorio como los de obras de infraestructura. Este tipo de proyectos demandan -para hacerlos bien- unos estudios y licitación que trascienden un horizonte de tiempo razonable para las soluciones inmediatas que requiere nuestra debilitada economía, y que en ningún momento se reflejaría en los indicadores del próximo año como pretende vender el gobierno. El país conoce, además, los **megaescándalos de corrupción que se han producido en la historia reciente alrededor de este tipo de proyectos** por los contubernios entre la clase política, firmas de contratistas transnacionales y la banca privada. Es insólito que se piense en endeudamientos de esta magnitud cuando según varios analistas, el hueco fiscal va a sobrepasar el 10% del PIB por la pandemia. Todavía peor, es pensar en la privatización de empresas públicas grandes y que por su rentabilidad generan ingresos importantes para el fisco nacional, a la vez que generan condiciones laborales dignas para sus trabajadores (como Ecopetrol).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Colombia no resiste la agudización del modelo neoliberal. Es inaceptable que al amparo del gobierno se aprovechen circunstancias tan difíciles para beneficio de los hiper ricos de este país.** Creemos que, en lugar de favorecer todavía más a la banca con todo este festival de regalos, proyectos de ley como el de Renta Básica resultan indispensables.  El país reclama una reforma tributaria progresiva, que recorte las exenciones desmedidas que disfrutan empresas transnacionales de corte extractivista cuya operación genera importantes costos ambientales y que tienen apenas un impacto marginal sobre el empleo. **Necesitamos gravar las rentas de capital, y no las del trabajo, ni mucho menos pesar en poner impuestos a bienes básicos** que representan el mayor porcentaje del gasto para las familias más afectadas por la crisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En nuestra ciudad, y ante la deuda multimillonaria de 554 mil millones que dejó Armitage ¿porqué no permitir que los entes territoriales emitan bonos de deuda pública comprados por el Banco de la República tal como ha propuesto el alcalde? **Si queremos sobrevivir a la crisis,** **tenemos que recurrir a alternativas de política económica ajenas a la insensibilidad de los tecnócratas.**

<!-- /wp:paragraph -->
