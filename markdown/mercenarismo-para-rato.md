Title: Mercenarismo para rato
Date: 2015-05-11 16:55
Author: CtgAdm
Category: Camilo, Opinion
Tags: Agrícola SAS, Energy Colombia, Glifosato, Organización Mundial de la Salud, Ssntos, Uniban
Slug: mercenarismo-para-rato
Status: published

###### Foto:Runrun.es 

**Camilo De Las Casas -[~~@~~CamilodCasas](https://twitter.com/CamilodCasas){.twitter-atreply .pretty-link} **

###### 10 de mayo de 2015 

Más allá de la sin razón de Uribe, de Ordoñez, y de Pinzón en  su obstinada defensa del uso del glifosato que solo podría explicarse por su vanidad personal en un país deseoso de autoristarismo, con una clase media arribista e ignorante que vive del complejo de pirámide. Eso les da créditos en un país donde el sentimiento de colonia es una grata perversión.

El análisis científico de la Organización Mundial de la Salud, OMS, concluye, que el uso del herbicida para atacar la siembra de hoja de coca tiene efectos cancerígenos sobre la salud de los habitantes rurales, y se da por descontado afecta las siembras de pan coger,  el agua, animales y bosques. Para esa terna, que expresa el pensamiento de un sector de la godorria colombiana, les da lo mismo que los mercenarios que contrata la DynCorp, para la aspersión desde el aire en avionetas traídas del Norte apliquen el terrible producto de Monsanto, algunos de ellos vengan a hacer turismo sexual, y a hacer lo que les venga en gana, pues en su cabeza tienen inoculado el pensamiento de que el mal somos los colombianos, que la adición de sus ciudadanos es nuestra responsabilidad.  
Es lo que tan lucidamente Germán Castro tituló en su libro: "Guerra Ajenas", una actitud mental y política de neoesclavitud, de dependencia y ausencia de autonomía de clase política y empresarial de los Estados Unidos.

Desde esa posición la terna pretende mantenerse leal a la mentalidad Bush contra las corrientes un poco más liberales de Obama frente al asunto de la droga. La expresión de la terna es la misma de la sumisión al designio empresarial de unos rentistas de la guerra o los especuladores de la guerra y sus operadores frente a los cuales no opera la justicia, ellos tienen asegurada la impunidad jurídica, la política y la social.

Las empresas y sus gatilleros del aire están inmunes a ser juzgados por tribunales nacionales independientemente de que cometan delitos de cualquier índole. Y para completar en la legislación interna y los procesos administrativos ningún daño ambiental ni el desplazamiento de personas por el uso de glifosato es sancionado.

La terna les asegura la impunidad política como cierre al uso del glifosato, esa que viene confeccionada desde Andrés Pastrana, reflejo de una dirigencia  carente de voluntad para hacer respetar a sus ciudadanos y sus territorios. A los pobladores  que buscan salidas a la exclusión social en la siembras de coca se les reprime, son habitantes sin derecho o derecho a sobrevivir indignamente en el campo, así se les ahoga, para que tarde o temprano, abandonen el territorio rural.

Esos territorios  pomposamente  llamados hoy los de la "paz territorial", en realidad son parte de la cartografía del mercado mundial como se deduce del más de medio centenar de tratados de libre comercio firmados y como se consigna en el escabroso Plan Nacional de Desarrollo. El PND es consiste con el extractivismo y los agronegocios haciendo innecesaria la aspersión viene otra moda. Si bien descansaran los habitantes rurales del glifosato se aceleraran otros desarrollos de la guerra económica.

En ese escenario ni Monsanto ni Dyncorp pierden, tendrán nuevas tareas, se ampliara la carpeta de oferentes de la represión y sus mecanismos. Santos  al cesar  la aspersión aérea con glifosato le hará un guiño a los cultuvadores rurales, a las FARC EP que se quedarán poco a poco sin esa fuente de ingreso que hará dificil su participación en la política (negocio), y como buen calculador acelerara las operaciones en el subsuelo, en donde ya se ha originado otra multiplicidad de conflictos, que no son parte de su pax neoliberal.No hay que llamarse a engaños seguirá Monsanto en los agronegocios y vendrán nuevos mercenarios de la guerra al estilo de Dyncorp para asegurar las operaciones privadas. Ya se ve con Poligrow, Vetra Amerisur, Energy Colombia, Uniban, Agrícola SAS,  entre otras, y eso también lo sabe Uribe, Ordoñez y Pinzón, los bufones de una despedida por la puerta ancha para Monsanto y Dyncorp en esta fase de guerra.

Estamos transitando aún en el camino de la servidumbre, en esta semanas, tres de sus amanuenses y actores lo han hecho por vanidad. Los tres han sabido trasgredir las normas, el derecho y las libertades de alguna manera y por supuesto Santos que ya prepara los campos para llevar los borregos lógicos a otro redil, los unos despiden y el otro inauguran.
