Title: "Propuestas de Uribe apuntan a la Impunidad" Carlos Lozano
Date: 2016-10-10 17:02
Category: Nacional, Paz
Tags: Acuerdos de La Habana, proceso de paz
Slug: propuestas-de-uribe-apuntan-a-la-impunidad-carlos-lozano
Status: published

###### [Foto: Contagio Radio] 

###### [10 de Oct 2016] 

Las propuestas presentadas por el senador Álvaro Uribe Vélez, pretenden **dilatar y torpedear el proceso de paz y la implementación de los Acuerdos de la Habana,** sin tener la mínima intención de corregir o darle mejores bases a la paz de Colombia, según el director del Semanario Voz, Carlos Lozano.

Para el periodista y docente, [Uribe busca clausurar el acuerdo de la Habana y llevar a la guerrilla de las FARC-EP  a un sometimiento de la justicia](https://archivo.contagioradio.com/no-podemos-darle-espacio-a-quienes-desde-ya-tratan-de-sabotear-los-acuerdos-carlos-lozano/), con la idea de ganar las elecciones del 2018 para finalizar los proceso de negociación y retomar la seguridad democrática en el país.

Lozano indica que lo más peligroso de las propuestas es que elimina toda posibilidad de la justicia transicional y restaurativa, debido a que **deja en total impunidad a quienes desde el establecimiento favorecieron el paramilitarismo** y tienen que responder por sus delitos. “Ni siquiera es una especie de canje de impunidades, sino es sencillamente la impunidad para unos y el castigo para otros” afirmo el periodista.

Frente a las movilizaciones sociales que se han venido gestando en los últimos días, Lozano afirma que esas manifestaciones justamente pretenden evitar que se dilaten los acuerdos y que a su vez sirven como escenarios para reclamar y exigir que se continúe con la construcción de paz [“es la lucha popular la que puede darle otro rumbo a esto”.](https://archivo.contagioradio.com/paz-a-la-calle-se-sigue-movilizando-en-toda-colombia/)

Con este panorama Lozano expone que hay tres caminos jurídicos para el país, el primero es considerar a **los acuerdos con el peso de acuerdo especial que se le otorgo ante el Consejo Federal Suizo**, otro podría ser la posibilidad de implementar los acuerdos a través de las leyes ordinarias o actor legislativos y la última es la iniciativa de realizar otro plebiscito.

<iframe id="audio_13261826" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13261826_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
