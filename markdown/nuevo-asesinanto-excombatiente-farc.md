Title: Continúan los asesinatos a excombatientes de las Farc
Date: 2019-09-28 19:00
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, FARC, Norte de Santander
Slug: nuevo-asesinanto-excombatiente-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto:Teleantioquia] 

Este viernes  al rededor de las 10:oo pm,  autoridades del corregimiento de La Gabarra, Tibú, Norte de Santander  confirmaron el asesinato de Fernando Antonio Castro García, excombatiente de las Farc  y  quien se había acogió en el 2017 al Proceso de paz, bajo la acreditación de la resolución 11 de 2017. Con la muerte de Castro son más de 150 excombatientes víctimas de hechos de violencia desde la firma del Acuerdo de Paz.

Fernando Castro era oriundo de Mesetas en el departamento del Meta, y se encontraba en La Gabarra por asuntos de trabajo. Según información reciente, los investigadores aún buscan los detalles del homicidio.(Le puede interesar:[Tres líderes sociales han sido asesinados en Cauca esta semana](https://archivo.contagioradio.com/tres-lideres-sociales-han-sido-asesinados-en-cauca-esta-semana/))

Con este  crimen se completan cinco ataques  que han cobrado las vidas de excombatientes de FARC en lo corrido de este mes. Cabe resaltar que en el caso de Carlos Celio, se había hecho la solicitud de un esquema de protección a la Unidad Nacional de Protección, sin embargo en los demás casos no se conocían amenazas en contra de las vidas de las víctimas.

Los  allegados de Fernando Castro piden colaboración para trasladar el cuerpo hasta el municipio de Mesetas y así realizar la despedida de su cuerpo; de igual forma solicitan a las autoridades del Norte de Santander investigar a los perpetradores de este homicidio y así evitar que que se sigan presentado mas casos de muerte a excombatientes. (Le puede interesar:[No avanzan investigaciones sobre asesinatos de excombatientes](https://archivo.contagioradio.com/no-avanzan-investigaciones-sobre-asesinatos-de-excombatientes/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
