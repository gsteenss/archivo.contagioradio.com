Title: Víctimas rechazan candidatura de Alto mando militar vinculado con ejecuciones extrajudiciales
Date: 2018-12-21 17:28
Author: AdminContagio
Category: DDHH, Movilización
Tags: Brigadier general Marcos Pinto, Ejecuciones Extrajudiciales, Ejército Nacional, falsos positivos
Slug: victimas-rechazan-candidatura-de-alto-mando-militar-vinculado-en-ejecuciones-extrajudiciales
Status: published

###### [Foto: Twitter @Ccajar] 

###### [21 Dic 2018] 

Las familias de las víctimas de ejecuciones extrajudiciales junto a organizaciones defensoras de derechos humanos, rechazaron la candidatura del Brigadier general Marcos Evangelista Pinto Lizarazo, para ser el Comandante de la IV Brigada del Ejército, debido a que en su contra **hay una denuncia por más de 30 casos de los mal llamados "falsos positivos". **

Estos hechos se habrían cometido durante el tiempo en que el Brigadier general Pinto fue comandante de los batallones Atanasio Girardot, adscrito a la Cuarta brigada del Ejército y el batallón 27 Magdalena, adscrito a la IXX Brigada del Ejército.

De acuerdo con el abogado Sebastian Escobar, del Colectivo de Abogados José Alvear Restrepo y defensor de las víctimas, la IV Brigada es una de las más importantes del país y desde hace un tiempo, quienes se han desempeñado como comandantes del Ejército han asumido ese cargo.

"Lo que estamos diciendo, frente a Marcos Evangelista Pinto Lizarazo es que es un personaje que no obstante, sus subordinados han participado en numerosos casos de falsos positivos y al interior de la Fuerza Pública y de las fuerzas Militares lo están perfilando **como un posible candidato para Comandante del Ejército en el futuro y eso nos parece peligroso**" afirmó Escobar. (Le pude interesar:["Demandan a Brigadier general Pinto Lizarazo por ejecuciones extrajudiciales"](https://archivo.contagioradio.com/demandan-a-brigadier-general-pinto-lizarazo-por-ejecuciones-extrajudiciales/))

### **Las denuncias por ejecuciones extrajudiciales en contra de Pinto**

El abogado señaló que cuando Marcos Pinto Lizarazo se desempeñó como comandante del Batallón Atanasio Girardot, adcrito a la Cuarta Brigada se cometieron numerosos casos de ejecuciones extrajudiciales y expresó que además, este es uno de lo batallones más grandes del país, que de acuerdo con la Human Right Watch, **entre el 2002 al 2008 estuvo relacionado con al menos 412 casos. **

Posteriormente cuando Pinto fue trasladado al Batallón 27 Magdalena, se mantuvo esta práctica y por ahora se tiene un **registro de 20 ejecuciones extrajudiciales durante su comandancia,** asimismo la organización  Human Right Watch, reveló que entre 2004 al 2008, este batallón estuvo implicado en 119 ejecuciones.

**No está garantizada la no repetición de los "Falsos Positivos"**

Para Escobar, es particularmente grave esta posible candidatura debido a que se firmó un Acuerdo de Paz en el que se habla de garantías de no repetición y con este hecho "el presidente Duque y las Fuerza Militares estarían **enviando un mensaje muy equivocado frente a la no repetición de falsos positivos" **

<iframe id="audio_30965165" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30965165_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
