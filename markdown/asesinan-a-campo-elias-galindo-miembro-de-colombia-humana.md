Title: Asesinan a Campo Elías Galindo miembro de Colombia Humana
Date: 2020-10-01 19:35
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Campo Elías Galindo, Colombia Humana, Gustavo Petro
Slug: asesinan-a-campo-elias-galindo-miembro-de-colombia-humana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Campo-Elias-Galindo-.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la tarde de este jueves se dio a conocer **el asesinato de Campo Elías Galindo, un reconocido historiador y docente de Medellín, quien también se desempeñaba como miembro de la Coordinadora de Colombia Humana en esa ciudad.** (Le puede interesar: [Jorge Quintero, líder social de Risaralda fue asesinado](https://archivo.contagioradio.com/jorge-quintero-lider-social-de-risaralda-fue-asesinado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El cuerpo  de Campo Elías, fue hallado en su apartamento del barrio Los Pinos, ubicado entre las comunas La América y Laureles, en Medellín. Según información preliminar de las autoridades, su hija fue quien lo encontró y de inmediato dio aviso a la línea de emergencias 123.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información que se ha dado por algunas personas cercanas a Campo Elías, **el crimen se habría cometido con excesiva brutalidad**, pues señalan que el cuerpo del docente fue encontrado, por agentes de la Policía, rodeado de sangre y con un libro quemado sobre su pecho. (Le puede interesar: [Se instaló el mural «¿Quién dio la orden? 2.0»](https://archivo.contagioradio.com/se-instalo-el-mural-quien-dio-la-orden-2-0/))

<!-- /wp:paragraph -->

<!-- wp:html -->

> Mataron y torturaron a Campo Elías, volvemos a la época donde acaban y matan por tener una posición distinta a la del gobierno de turno, no solo hay masacres, asesinatos a líderes sociales sino que exterminan a quien piense distinto. [@NoticiasUno](https://twitter.com/NoticiasUno?ref_src=twsrc%5Etfw) [@elespectador](https://twitter.com/elespectador?ref_src=twsrc%5Etfw) [@Teleantioquia](https://twitter.com/Teleantioquia?ref_src=twsrc%5Etfw) [pic.twitter.com/dkrkNkBFoW](https://t.co/dkrkNkBFoW)
>
> — León Fredy Muñoz?? (@LeonFredyM) [October 1, 2020](https://twitter.com/LeonFredyM/status/1311755140848857090?ref_src=twsrc%5Etfw)

<!-- /wp:html -->

<!-- wp:paragraph -->

Campo Elías tenía 69 años de edad, era investigador, historiador y magíster en Planeación Urbana de la Universidad Nacional de Colombia donde también se había desempeñado como docente. **Además de ser miembro de la Colombia Humana, también era integrante del Frente Amplio por la Paz, la Democracia y la Justicia Social.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El alcalde de Medellín, **Daniel Quintero**, repudió el hecho y anunció la intervención de la Fiscalía y la Policía en la investigación; al tiempo que **ofreció una recompensa de hasta 20 millones de pesos** para quienes pudieran suministrar información sobre los responsables de crimen.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Dolor por el asesinato en su casa del profesor y activista Campo Elias Galindo. Habrá justicia: Policía y Fiscalía trabajan para capturar a responsables. Alcaldía ha dispuesto hasta 20 millones para quien nos ayude en tareas de identificación. [\#LaVidaEsSagrada](https://twitter.com/hashtag/LaVidaEsSagrada?src=hash&ref_src=twsrc%5Etfw)
>
> — Daniel Quintero Calle (@QuinteroCalle) [October 1, 2020](https://twitter.com/QuinteroCalle/status/1311763866615984130?ref_src=twsrc%5Etfw)

<!-- /wp:html -->

<!-- wp:paragraph -->

El senador Gustavo Petro, líder de Colombia Humana, junto a otras varias figuras políticas del país, condenaron el asesinato del profesor Galindo y lo calificaron no solo como un ataque a su persona sino en general a la oposición en el país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1311755021084758017","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1311755021084758017

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:html -->

> A esta hora reunion urgente de fuerzas progresistas de Antioquia sobre el asesinato politico contra Campo Elias Galindo, lider de [@ColombiaHumana](https://twitter.com/colombiahumana?ref_src=twsrc%5Etfw).Se lanza una alerta a las fuerzas progresistas de Colombia y una exigencia a [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) y [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) para esclarecer crimen.
>
> — Ángela María Robledo (@angelamrobledo) [October 1, 2020](https://twitter.com/angelamrobledo/status/1311772383238291456?ref_src=twsrc%5Etfw)

<!-- /wp:html -->

<!-- wp:paragraph -->

La presidenta nacional del Movimiento MAIS, Martha Peralta Epieyú, **pidió a su vez, el pronunciamiento de la Comunidad Internacional frente al «*asesinato de la oposición en Colombia»*.**

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw)  
>   
> En la ciudad de Medellín, en su propia casa, fue asesinado Campo Elias, profesor universitario pensionado y miembro de la [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw)  
>   
> Están asesinando a la oposición en Colombia  
> ¿Van a seguir en silencio [@mbachelet](https://twitter.com/mbachelet?ref_src=twsrc%5Etfw) [@Almagro\_OEA2015](https://twitter.com/Almagro_OEA2015?ref_src=twsrc%5Etfw)?  
> [\#NosEstanMatando](https://twitter.com/hashtag/NosEstanMatando?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/l8Z0NUvTAX](https://t.co/l8Z0NUvTAX)
>
> — Martha Peralta Epieyú (@marthaperaltae) [October 1, 2020](https://twitter.com/marthaperaltae/status/1311755462208036867?ref_src=twsrc%5Etfw)

<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
