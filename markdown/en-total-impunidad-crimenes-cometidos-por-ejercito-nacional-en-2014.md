Title: En total impunidad crímenes cometidos por Ejército Nacional en 2004
Date: 2016-09-21 10:30
Category: DDHH, Nacional
Tags: Abuso de fuerza Policía, Fuerzas Militares de Colombia, violaciones sexuales Colombia, Violencia contra las mujeres en Colombia
Slug: en-total-impunidad-crimenes-cometidos-por-ejercito-nacional-en-2014
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Ejército.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Taringa ] 

###### [21 Sept 2016] 

Durante la última semana de octubre de 2004, **siete mujeres campesinas fueron víctimas de violencia sexual, ataques físicos y psicológicos, torturas y amenazas**, perpetrados por una compañía del Ejército del municipio de Puerto Caicedo, Putumayo, al mando del Sargento Segundo Juan Pablo Sierra Daza. Pese a la gravedad de los crímenes y la abundancia de material probatorio, sólo hasta 2010 la Fiscalía dispuso indagación preliminar. Hoy, 12 años después el organismo no ha proferido ninguna decisión sancionatoria contra los victimarios.

Las acciones violatorias ocurrieron en la vereda El Picudo, en dónde los efectivos retuvieron a las siete campesinas para luego someterlas a torturas, violaciones sexuales, agresiones físicas, lesiones con armas corto punzantes, amenazas e incluso, disparos con armas de fuego, como ocurrió con la señora Rosalía Benavides Franco, quien **tras ser asesinada, fue desmembrada y lanzada a un aljibe de su comunidad**. Tres de las mujeres fueron retenidas en una residencia durante tres días en los que las sometieron a torturas y abusos sexuales.

Según denuncia la Corporación Justicia y Dignidad, fue hasta el 8 de octubre de 2010 que la Fiscalía Segunda Especializada de Mocoa, Putumayo, dispuso la indagación preliminar de los hechos. Luego el caso fue asignado a la Fiscalía 70 Especializada de la Unidad de Derechos Humanos de Cali, donde su delegada, pese a conocer **minutas de guardia, declaraciones y documentos escritos que prueban la responsabilidad de los militares** en lo sucedido, no ha resuelto la situación jurídica ni siquiera del Sargento Sierra Daza, quien en octubre de 2015 reconoció su participación.

En mayo de 2015 las víctimas fueron valoradas psicológicamente por el Instituto de Medicina Legal, que constató que efectivamente las mujeres fueron agredidas y determinó que debido a las graves afectaciones psicológicas, debían recibir atención psicoterapeuta; sin embargo, el Estado ha desatendido esta disposición, y por el contrario, a través de la Fiscalía, **ha incumplido sus obligaciones constitucionales de establecer verdad, justicia y reparación** para estas campesinas que fueron agredidas por agentes estatales.

Vea también: [[Condenan a militares por abuso sexual de una mujer indígena](https://archivo.contagioradio.com/condenan-a-militares-por-abuso-sexual-de-una-mujer-indigena/)]

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
