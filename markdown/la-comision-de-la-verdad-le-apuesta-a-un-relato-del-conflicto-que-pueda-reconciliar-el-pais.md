Title: La Comisión de la Verdad le apuesta a un relato del conflicto que pueda reconciliar al país: Leyner Palacios
Date: 2020-12-03 22:09
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Comisión de la Verdad, Leyner Palacios, verdad
Slug: la-comision-de-la-verdad-le-apuesta-a-un-relato-del-conflicto-que-pueda-reconciliar-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Leyner-Palacios-Comision-de-la-Verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El comisionado Leyner Palacios recientemente elegido para el cargo por la Comisión para el Esclarecimiento de la Verdad –CEV-, habló en exclusiva para Contagio Radio sobre su nombramiento, el reconocimiento que se le hizo como “Defensor del año”, la realidad de los líderes(as) y defensores(as) de derechos humanos en el país y los retos de la Comisión de la Verdad. (Lea también: [Los desafíos de la Comisión de la Verdad al entrar en su tercer año](https://archivo.contagioradio.com/los-desafios-de-la-comision-de-la-verdad-al-entrar-en-su-tercer-ano/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de su cargo como comisionado, Leyner ha sido un consagrado defensor de derechos humanos, víctima de la masacre de Bojayá en el año 2002 hoy representa al colectivo de víctimas de dicho ataque. Adicionalmente ha trabajado directamente con las comunidades afrodescendientes víctimas del conflicto en el departamento del Chocó desde su trabajo con la Pastoral Social y la[Comisión Interétnica por la verdad del Pacífico.](https://verdadpacifico.org/)(Le puede interesar: [Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### CR ¿Qué significa recibir el **premio como Defensor del Año en la **IX edición del Premio Nacional de Derechos Humanos en Colombia 2020?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

LP: Es una alegría compartir con ustedes este momento. Este premio es un reconocimiento para la sociedad colombiana y las comunidades; el cual también dedico a los líderes y lideresas que siguen defendiendo la vida. Este premio nos llama a hacer memoria de cuántos líderes han sido asesinados y amenazados después de la firma del Acuerdo de Paz. (Lea también: [Premio Nacional a la defensa DD.HH. 2020: ganadores y ganadoras](https://archivo.contagioradio.com/premio-nacional-a-la-defensa-dd-hh-2020-ganadores-y-ganadoras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: Usted y su equipo de protección han sido víctimas de ataques a su seguridad, de hecho uno de sus escoltas fue asesinado. ¿Cómo ha sido este periodo de cuarentena que inició con esa situación crítica para su seguridad y siguió hacia el nombramiento como comisionado de la Verdad?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

LP: Este 2020 ha sido un año bastante complicado para mí. El 3 de diciembre del año pasado recibí amenazas para abandonar mi territorio, eso me llevó a radicarme en Cali y allí perdí a uno de mis escoltas. Fue muy difícil superar esa intimidación, no podía dormir, tenía que trasladarme constantemente de un lugar a otro,  escondido como si fuera un delincuente. Yo he sido un defensor de derechos humanos de cara al país, la ciudadanía me conoce abierto y sincero. Por eso me sentía muy mal estando en esas afugias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

No obstante, en medio de todo eso, siempre recibí mucho apoyo y solidaridad. Creo que para que yo esté vivo intervinieron dos cosas: Primero mi diosito, que nos protegió; las oraciones de las comunidades y los rezos ancestrales. Segundo, la misma ciudadanía; es impresionante cómo Colombia se movilizó y se preocupó por mi situación de seguridad, ejerciendo presión a las autoridades para que no me dejaran matar. Yo valoro mucho esa protección colectiva que ejerció la ciudadanía colombiana, estoy muy agradecido por eso, mi vida hoy es posible por esa gran presión y esa movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muchos colombianos queremos la paz, la reconciliación y la verdad, y en ese sentido nos identificamos y por eso es tan importante esa labor de defensa  y protección que muchos y muchas han ejercido en mi caso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### CR: **¿Cómo se traduce esa solidaridad ciudadana, que usted destaca, en el avance de los procesos comunitarios particularmente en el caso de Bojayá y los procesos que lidera la Comisión Interétnica del Pacifico?  **

<!-- /wp:heading -->

<!-- wp:paragraph -->

LP: Cuando yo salí de Bojayá por las amenazas, experimenté el nivel de miedo que viven los líderes; hoy en Bojayá nadie se atreve a denunciar lo que allí está pasando. Bojayá hoy está peor que en el 2002, existe una avanzada paramilitar muy fuerte en ese territorio, una cooptación y un control sobre la población civil tremendamente grave e incluso amenazas a defensores y líderes de la iglesia lo cual nunca antes había ocurrido. Ocurren intimidaciones a organismos de control y a organismos internacionales y eso está pasando a lo largo y ancho del Río Atrato, lo cual es muy triste porque  he sentido que el proceso organizativo se ha debilitado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente las organizaciones de esta región están más silenciadas y es evidente que han disminuido las denuncias por vulneraciones a los derechos humanos, no porque no estén ocurriendo, sino porque hay un silenciamiento a la labor de los defensores. Los líderes de allá, muchos de ellos jóvenes, me dicen que no quieren denunciar por miedo a que les pase lo mismo que a mí.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque mi salida también me permitió estar en otros territorios del Pacífico y continuar el trabajo organizativo ya desde un nivel regional; ahí constituimos la Comisión Interétnica por la Verdad del Pacífico y avanzamos en la consolidación de una propuesta metodológica de investigación para esclarecer lo ocurrido en el conflicto armado desde la perspectiva del daño al territorio. Esta metodología permitirá evidenciar al país que estas comunidades del Pacífico, no solo han sido víctimas del conflicto, sino que han sido sometidas a una especie de genocidio, especialmente las comunidades afrodescendientes e indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Comisión Interétnica surgió como una propuesta de la sociedad civil en la que se congregan más de 35 organizaciones como plataforma donde convergen objetivos distintos pero con un objetivo común, la cual ha sido rodeada por la Comunidad Internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, está mi llegada a la Comisión de la Verdad, los desafíos que hay para construir verdad en Colombia son bastante grandes. No nos digamos mentiras, es muy difícil construir verdad en los territorios étnicos donde el conflicto armado se ha exacerbado de manera terrible, en donde la pandemia ha tenido unos impactos muy grandes, donde existen desafíos de movilidad y acercamiento a las comunidades porque no se puede llegar de manera virtual, pues estos territorios no cuentan con Internet por ende se dificulta establecer ese contacto con las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a esto la CEV ha hecho grandes esfuerzos, se han escuchado alrededor de 12.000 testimonios, lo que, dado el contexto en el que se ha trabajado ha sido una gran apuesta, sobretodo por la riqueza de esos testimonios en los que se encuentran las voces de los responsables, de las víctimas y también de empresarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A futuro la Comisión pretende establecer un relato que esclarezca el conflicto armado, pero lo más importante, un relato que permita la reconciliación, pues una verdad que termine por polarizar y desangrar más el país no tiene sentido. Eso no quiere decir que se van a esconder verdades, la verdad tendrá que aflorar pero el desafío es que aflore para reconciliar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### **¿Cómo hacer que un informe que recogerá verdades que seguramente van a levantar ampollas no se convierta en un foco de odios y polarización?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como Comisión de la Verdad, no podemos engañar al país. Nosotros venimos viviendo una violencia por más de 54 años, una violencia cíclica que tiene unos patrones que se repiten en la historia de dicha violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si no tocamos esos patrones de persistencia del conflicto armado vamos a condenar al país a que repitamos un modelo de violencia y a que vivamos un nuevo ciclo. Por eso, ahí está el desafío de tocar esos patrones para lograr transformarlos. En ese sentido, más allá de la descripción de las dinámicas del conflicto el gran desafío es proponerle al país una serie de recomendaciones para que la sociedad las entienda y asuma, y para que las autoridades se comprometan con ellas para precisamente, evitar que se repita el modelo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo la creación del paramilitarismo en Colombia de antaño, surge de una lógica de alianza con miembros de la Fuerza Pública, con sectores políticos, esto es un hecho que la Comisión no se va a inventar ahora, eso está claramente dicho. Lo más grave es que ese fenómeno se repite. No hay manera de que la Comisión no aborde ese tipo de situaciones; pero se hace no con la intención de hacer señalamientos sobre si la Fuerza Pública es mala o es buena, a nosotros no nos corresponde calificar eso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que se quiere hacer es llamar a la reflexión al país, porque si esos temas no se abordan y no se proponen medidas para que por ejemplo las Fuerzas Militares implementen cambios, vamos a seguir viviendo hechos de violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Cuántas masacres llevamos este año? Más de 70. Es decir la violencia se sigue manteniendo a pesar de que estamos en un proceso de consolidación y de construcción de paz y todo porque no estamos aún en la capacidad de aceptar los errores que hemos cometido en ese pasado terriblemente doloroso. Por eso tenemos que prepararnos para afrontar ese horror del pasado y para a partir de eso, ponernos de acuerdo en cómo vamos a reconstruir el país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### **¿Que han pensado desde la Comisión para que ese relato de esclarecimiento logre en efecto reconciliar?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Nosotros estamos trabajando desde la lógica de la escucha plural, hemos recibido más de 12.000 testimonios incluidas las voces de miembros de las Fuerzas Militares, la guerrilla, las víctimas… En los testimonios que hemos recopilado van más de 8.000 testimonios de las víctimas y eso es un capital muy importante para descifrar el entramado de la guerra, de igual forma hemos escuchado empresarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yo lo que quiero es hacer un llamado para que más gente llegue y nos dé su testimonio porque nuestro desafío es construir un relato equilibrado sin tendencias para ningún lado; un relato que permita el esclarecimiento y sobre todo la comprensión de las causas del conflicto para que nos comprometamos con esa transformación que necesita el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el modelo de investigación que ha implementado la Comisión estamos convencidos que se emitirá un informe equilibrado. Por ejemplo yo que soy víctima de Bojayá, ya en mi rol de comisionado tengo que escuchar a otras víctimas, pero también a los victimarios de todos los sectores y a partir de esa escucha tenemos que hacer un análisis lo más desprendido de apasionamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso es grave que muchos estén criticando a la Comisión y al Sistema Integral en lugar de acercarse a dar su testimonio porque con eso se le hace un daño al país, su testimonio es necesario para la construcción de un relato equilibrado; entonces no depende solo de la Comisión o del Sistema, depende también de que muchas voces se acerquen y puedan dar su perspectiva de los acontecido en el marco del conflicto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### **¿Cómo ha sido ese proceso de convertirse, como comisionado de la verdad, en una especie de representante del Estado cuando al mismo tiempo se tiene un rol de representación de las víctimas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

No es fácil, es un ejercicio en el que incluso a veces uno se cuestiona su lugar y si tenía que venir acá o más bien seguir representando a las víctimas en otros escenarios. Pero creo que ha sido importante llegar a la Comisión por el momento en el que se encuentra y por esa disputa por la verdad que existe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Creo que la Comisión requería esta voz y esta perspectiva desde las víctimas y mi aporte desde la lógica de la visión étnica y territorial de la paz. Es también un deber como ciudadano de participar porque el derecho a la paz, si bien es un derecho, también es una obligación en la contribución para llegar a su consolidación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siendo parte del Estado es cuando podemos mostrar cómo es que este debe relacionarse con los territorios desde la experiencia que tenemos; eso permite enriquecer las lógicas de funcionamiento del Estado.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/174002577761120","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/174002577761120

</div>

<figcaption>
Vea la entrevista completa

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
