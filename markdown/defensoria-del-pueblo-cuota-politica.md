Title: Defensoría del Pueblo será repartida como cuota política del gobierno Duque
Date: 2020-08-07 09:50
Author: AdminContagio
Category: Nacional
Slug: defensoria-del-pueblo-cuota-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Defensoría-del-Pueblo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A pesar de las preocupaciones y los llamados de organizaciones sociales, campesinas y de diversas índole, el presidente Duque presentó su terna para que el congreso elija a quién se encargará de la Defensoría del Pueblo. Una entidad creada por la constitución de 1991, fruto de un acuerdo de paz que al sentir de muchos y muchas ha perdido su norte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta terna está conformada por personas como **Carlos Camargo, Elizabeth Martínez y Myriam Martinez**, todas integrantes de partidos que ahora respaldan al gobierno y que al parecer no van a significar ningún tipo de oposición o reclamo por la grave crisis de DDHH que afronta Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta terna y en caso de que el congreso elija entre una de ellas al nuevo Defensor del Pueblo, se estaría configurando un control del mismo gobierno en los entes de control al lado de la fiscalía, ocupada hasta el momento por Francisco Barbosa, también compañero de universidad del presidente Duque y quién ha sido calificado como un "fiscal de bolsillo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta carrera queda la Procuraduría General de la Nación que está próxima también a una etapa de elecciones.

<!-- /wp:paragraph -->

<!-- wp:core-embed/wordpress {"url":"https://archivo.contagioradio.com/otra-mirada-defensoria-del-pueblo-una-ong-mas/","type":"wp-embed","providerNameSlug":"contagio-radio","className":""} -->

<figure class="wp-block-embed-wordpress wp-block-embed is-type-wp-embed is-provider-contagio-radio">
<div class="wp-block-embed__wrapper">

https://archivo.contagioradio.com/otra-mirada-defensoria-del-pueblo-una-ong-mas/

</div>

</figure>
<!-- /wp:core-embed/wordpress -->

<!-- wp:heading {"level":3} -->

### Carlos Camargo Assis la cuota del partido Conservador para la Defensoría del Pueblo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Director de la Federación Nacional de Departamentos, se ha caracterizado por ser una persona muy cercana al gobierno de Iván Duque, ha defendido desde su cargo la iniciativa del Dia Sin Iva que fue una de las medidas más criticadas por haberse llevado a cabo en pleno pico de la pandemia del COVID.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Camargo fue también compañero de Iván Duque en la Universidad Sergio Arboleda y no cuenta con experiencia en la defensa de los DDHH, además no ha referenciado ningún tipo de acercamiento con sectores del Movimiento Social que son determinantes en el trabajo de asa entidad que tiene grandes retos por la crisis de DDHH que afronta el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente Camargo defendió a su copartidaria Marta Lucía Ramírez cuando se le cuestionó por el caso de su hermano quien fue detenido en EEUU por narcotráfico y al cuál la vicepresidenta le pagó una fianza para dejarlo en libertad. **Assis sería la cuota del partido conservador en la terna presentada por el presidente Iván Duque.**

<!-- /wp:paragraph -->

<!-- wp:html -->

> La doctora [@mluciaramirez](https://twitter.com/mluciaramirez?ref_src=twsrc%5Etfw) ha dedicado su vida al servicio abnegado, comprometido y transparente por el país en una trayectoria sin una sola tacha. Mezquino es querer juzgarla por un problema de su hermano de hace 23 años. Toda nuestra solidaridad y respaldo con la Vicepresidenta
>
> — Carlos Camargo Assis (@carloscamargoa) [June 12, 2020](https://twitter.com/carloscamargoa/status/1271281549120585729?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:heading {"level":3} -->

### Elizabeth Martínez Barrera la cuota del partido de la U para la defensoría

<!-- /wp:heading -->

<!-- wp:paragraph -->

Abogada de la Universidad Sergio Arboleda, es decir compañera del presidente Ivan Duque. Además tiene una especialización en Derecho Público, ciencias y sociología política. Es la actual secretaria de la Comisión Tercera de la Cámara de Representantes y cuota política del Partido de la U.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según fuentes cercanas al congreso Martínez ha trabajado con los excongresistas Bernardo el Ñoño Elías, Plinio Olano y Ángel Custodio Cabrera condenados por corrupción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con muy poca o nula experiencia en materia de Derechos Humanos, pero cercana al gobierno del Centro Democrático y a sus posturas en torno al conflicto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Myriam Martínez cuota uribista para la Defensoría del Pueblo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Martínez, quien es la actual directora de la Agencia Nacional de Tierras, abogada de la Universidad Sergio Arboleda, es fuertemente cuestionada pues su trabajo en esa entidad ha sido prácticamente nulo y no ha sido en favor de los intereses de los campesinos despojados de sus tierras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fue también delegada de Fedepalma para que liderara proyectos a nivel social. Allí trabajó en la generación de modelos y políticas sectoriales en materia laboral y de empleo y en lo concerniente a la propiedad, acceso a la tierra y desarrollo rural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tampoco cuenta con experiencia en el trabajo en defensa de los Derechos Humanos ni acercamientos con el sector pues ha desarrollado sus trabajos justamente en las entidades que no han facilitado la restitución de tierras y están inmersas en denuncias por violaciones de los DDHH como es la agremiación de palmeros en el Bajo Atrato Chocoano y el Urabá como lo han denunciado organizaciones como la [Comisón de Justicia y Paz](https://www.justiciaypazcolombia.com/) y La [Fundación Forjando Futuros](https://www.forjandofuturos.org/) entre otras

<!-- /wp:paragraph -->
