Title: Mientras Wall Street ya cotiza el agua del futuro, comunidades luchan contra su privatización
Date: 2020-12-21 11:06
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: colombia, privatización del agua, Wall Street
Slug: mientras-wall-street-ya-cotiza-el-agua-del-futuro-comunidades-luchan-contra-su-privatizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Agua-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: consumo de agua /sostenible.cat

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con la llegada del agua al mercado de la bolsa de valores de Wall Street, cotizando en los mercados futuros y con empresarios apostando sobre su escasez o abundancia en el planeta durante los próximos años, se han prendido las alarmas sobre lo que que podría convertirse en un paradigma que pondría un precio y privatizaría un recurso que es un derecho universal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la medida únicamente tendrá validez para regular su precio o cotización en California en Estados Unidos, **su primera cotización se estableció que la cantidad de 1.233.000 litros de agua tuviera un costo de 486 dólares basándose en un valor promedio** de los contratos que existen de derechos de agua en las regiones más grandes y comerciales de dicho estado, lugar que fue devastado por incendios forestales en septiembre y que pese a ser reconocido como el mercado agrícola más grande de Estados Unidos, salía de una sequía de ocho años.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entiéndanse los derechos del agua como aprovechar el agua subterránea, ríos, lagos, estanques y manantiales naturales, tecnologías de purificación y tratamiento del agua, perforación de pozos, saneamiento, mantenimiento, distribución y construcción de infraestructura, la producción, operación y venta de agua embotellada, camiones de agua y camiones cisterna.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para la profesora **Cecilia Roa, especialista en gestión del agua, acueductos comunitarios, democratización ambiental**, lo que refleja el hecho de que se empiece a cotizar el agua es una señal de la creciente preocupación sobre su escasez, su demanda y la preocupación de sectores económicos para abastecerse de agua en el futuro, "es un derivativo que se ha usado en el mercado de valores desde hace años, de las mercancías," lo que señala, obedece a las presiones que existen sobre el agua en la actualidad y los conflictos cada vez más creciente alrededor de este recurso.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte para **Juanita de los Ángeles Ariza, codirectora de la Red Nacional del Agua** resulta alarmante pues no es que se esté "cotizando el agua materialmente, sino los derechos de su uso, es como si las licencias ambientales se pusieran en oferta y se generara un comercio entorno a eso y quien tiene el dinero puede abarcar el agua del mundo", advierte la ambientalista.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Diego Martínez, coordinador del área de Agua en [Censat Agua Viva](https://censat.org/)** advierte que lo que intentan hacer desde la bolsa de valores es construir un modelo para generar un paradigma en la financiación del agua y sus derechos, un modelo que probablemente será emulado por otros mercados como el que surgió en California, **"se está vendiendo el agua del futuro y el principal problema es que se está especulando, se está vendiendo y comprando agua que nadie sabe si se va a tener o no en el planeta".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La privatización del agua, un peligro latente

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La profesora Roa señala que el agua a diferencia de otros bienes a lo largo de la historia ha sido valorada por las sociedades como un recurso, rechazando la idea de convertirla en una mercancia, muestra de ello es que en todas las legislaciones de América Latina, el único país que lo ha permitido es Chile donde en la actualidad transan los derechos del agua, una política que afecta principalmente a campesinos y pequeños agricultores que no tienen servicios de agua potable. [(Le puede interesar: Reducir gases de efecto invernadero en un 51% ¿otra promesa del Gobierno en el aire?)](https://archivo.contagioradio.com/reducir-gases-de-efecto-invernadero-en-un-51-otra-promesa-del-gobierno-en-el-aire/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si bien tras la dictadura de Augusto Pinochet, en Chile se ha intentado reformar la privatización del agua en al menos ocho ocasiones, es una iniciativa que ha sido rechazada por sectores como la Sociedad Nacional de Agricultura que agrupa a empresarios agrícola del país austral. Aunque se ha buscado fijar el líquido como un derecho y un bien público y dar fin a las concesiones, el gobierno de Sebastián Piñera ha expresado su deseo de mantener la entrega de estos derechos a los privados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el mundo, según **Market Oracle y Global Research** en un artículo publicado originalmente en 2012, bancos de Wall Street y multimillonarios, están consolidando sus compras de acuíferos y derechos de agua en todo el planeta. Tal es el caso de inversionistas como **Goldman Sachs, JP Morgan Chase, Credit Suisse, Blackstone Group, Citigroup, UBS, Deutsche Bank, Macquarie Bank, Barclays Bank, Allianz y HSBC Bank.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para hacerse una idea del valor que tiene este líquido en la actualidad, el mismo artículo titulado '**Los Barones del Agua'** describe cómo, mientras multimillonarios acumulan acuíferos, a pobladores de regiones como Oregon en Estados Unidos, se les criminalizó por recolectar el agua lluvia que caía en su propiedad, condenándoles por nueve cargos a 30 días de prisión.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se debe prestar atención al futuro de los recursos hídricos en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En el contexto nacional, la profesora Roa expresa que la historia de manejo hídrico del país deja mucho qué desear, si bien Colombia es uno de los países más ricos a nivel mundial en cuanto a su inventario natural hídrico, **en el 56% de los departamentos el coeficiente de Gini del agua es incluso más alto que el de la tierra**, lo que muestra que su asignación está incluso más concentrada. Esto en contraste con datos aportados por expertos en economía ambiental, como Gustavo Correa Assumus, que aseveran que la pobreza tiene implicaciones negativas en el acceso al agua segura y de igual forma la falta de acceso al agua segura es un elemento que sustenta y reproduce la pobreza.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega Roa que esta no está llegando a las comunidades que protegen las fuentes hídricas y es algo que debe tenerse en cuentea, **"los campesinos no tienen concesiones de agua y muchas veces se oponen a aplicar un permiso para consumirla o usarla, por considerarla natural en consecuencia no se debe pedir permiso a nadie, esa es una concepción que hay que respetar".** [(Lea también: Desplazamiento y acaparamiento: el precio del aguacate en Caldas)](https://archivo.contagioradio.com/__trashed-5/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraparte, al evaluar los acueductos en los territorios, estos son en sí mismos una expresión de la gestión comunitaria que **realizan las población pues entregan alrededor del 45% del agua en la ruralidad, convirtiéndose "en una pieza fundamental de decisión y participación sobre los usos** y manejos que tienen las poblaciones sobre sus tierras", afirma Diego Martínez quien advierte que a dichos acueductos se les someten a toda clase de exigencias administrativas y técnicas en contra de esta forma de gestión.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Acabar con un acueducto comunitario es acabar con procesos organizativos y procesos políticos
>
> <cite>Diego Martínez - coordinador del área de Censat</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, casos como el de **Cerrejón en La Guajira,** donde se ha desviado el río Ranchería ocurren en un contexto en que en promedio las comunidades Wayúu tienen que caminar cerca de siete kilómetros para acceder a fuentes hídricas y donde el ICBF reportó para septiembre de este año, 784 casos de menores con desnutrición aguda, lo que deja al descubierto la pérdida del patrimonio cultural de los pueblos originarios y de la soberanía hídrica nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Diego Martínez, la privatización del agua es un fenómeno que ya se viene viviendo y que en Colombia data de la década de los noventa expresándose en diferentes formas, desde un acueducto que pasa por manos público-privadas hasta el acaparamiento de mineras y petroleras del agua, privando a las comunidades de este derecho vital, en ese sentido, resalta que **el manejo por parte del Estado del agua se ha hecho en el país de forma privada**, "históricamente ha priorizado la empresas privadas, los entes económicos y ha entregados esos derechos a multinacionales, ese es el problema en el país", esto pese a que en Colombia existe el derecho fundamental al agua en conexidad con los derechos fundamentales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para concluir y con respecto a estos peligros que se ciernen sobre los recursos hídricos en el país, Juanita Ariza afirma que es fundamental que las organizaciones ambientales realicen un proceso de defensa integral del agua y de declaración como patrimonio público puesto que el país representa el 72% de los nacimientos de los páramos en el mundo y que se sumen resalta el integrante de Censat a procesos participativos de las comunidades como las consultas populares.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
