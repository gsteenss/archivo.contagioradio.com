Title: Se agota el tiempo en la mesa ELN-Gobierno para lograr un cese al fuego
Date: 2018-07-26 18:42
Category: Movilización, Paz
Tags: cese bilateral, dialogos de paz, ELN, Miguel Ceballos
Slug: se-agota-el-tiempo-para-lograr-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eln-cuba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Jul 2018] 

Frente al sexto ciclo de conversaciones que se adelanta en La Habana, Cuba, entre el Gobierno colombiano y negociadores de paz del ELN, y la posibilidad de acordar un nuevo y mejor cese bilateral al fuego, el analista en conflicto armado, Luis Eduardo Celis, afirma sentirse escéptico y ve difícil su consecución.

Aunque Celis cree que los mensajes emitidos por Pablo Beltrán, jefe del equipo negociador son positivos, y que todas las opciones que permitan la distención del conflicto y eviten la muerte de colombianos es una buena noticia; apunta que las condiciones en la mesa de negociaciones son difíciles porque Gobierno y Guerrilla asumen de forma diferente el cese bilateral.

La razón principal de su argumento, es que el ELN quiere un cese cualitativamente mejor, lo que para ellos significa la inclusión de garantías para proteger a los líderes sociales. En ese sentido, Celis afirma que hay pactos humanitarios concretos que dependen de ambas partes como el desminado humanitario y el no afectar la población civil.

Pero hay otras cosas que no están en manos de ambas partes, en tanto "el tema de los líderes es de tal complejidad que ponerlo en el marco de un cese al fuego bilateral genera dificultades". En consencuencia, todos los días se debe exigir que se proteja a los líderes, pero no en el  contexto de un cese bilateral.

### **Con el Gobierno entrante las conversaciones se ven difíciles**

El analista señaló que en el gobierno de Iván Duque las conversaciones se ven complicadas, pues observa que el ELN y el presidente electo están muy distanciados. De una parte, porque no comparte la formula del cese bilateral, y por otra porque desde el Centro Democrático se ha buscado más el sometimiento que la negociación.

Adicionalmente, Celis cree que entre el nuevo mandatario y el ELN no hay un canal de comunicación abierto, de tal forma que habrá incertidumbre hasta el 7 de agosto, "porque de no haber una señal hasta ese día, formalmente esa mesa estaría acabada". (Le puede interesar:["Grandes retos para mesa ELN-Gobierno en sexto ciclo de conversaciones"](https://archivo.contagioradio.com/grandes-retos-para-mesa-eln-gobierno-en-sexto-ciclo-de-conversaciones/))

Sin embargo, la disposición de la guerrilla muestra intenciones de optar por una fase de acercamiento, y el nombramiento de Miguel Ceballos como nuevo Comisionado de Paz durante la administración de Duque es una buena noticia, porque eso indicaría que hay una persona responsable de buscar una salida negociada al conflicto.

### **"El ELN cree que no hay condiciones para la paz"** 

A los inconvenientes mencionados anteriormente, se suma la lectura pesimista que para el analista hace el Comando Central del ELN de la implementación del Acuerdo de Paz con las FARC, pues "tienen una mirada demasiado negativa", lo que los hace dudar sobre una posible salida negociada al conflicto, y termina reforzando la idea de que no hay condiciones para la paz.

También es evidente para el país que el proceso no ha avanzado, particularmente en la participación de la ciudadanía, eje central de las conversaciones, que no se ha dado como el ELN lo esperaba. No obstante, Luis Celis afirma que es una responsabilidad tanto del Gobierno que no ha permitido la participación, como de la guerrilla que no ha renunciado a la práctica del secuestro.

Pese a todas las dificultades, el analista señaló que se debe continuar buscando una salida negociada al conflicto, y que la ciudadanía será un actor fundamental para lograr continuar con el proceso, pues en la participación de la sociedad, esta la clave para avanzar. (Le puede interesar: ["Futuro de la negociación con el ELN dependerá del nuevo presidente: Víctor de Currea"](https://archivo.contagioradio.com/futuro-de-la-negociacion-con-el-eln-dependera-del-nuevo-presidente-victor-de-currea/))

<iframe src="https://www.youtube.com/embed/lm7en3ASrXw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
