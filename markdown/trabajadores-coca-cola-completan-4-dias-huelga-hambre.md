Title: Trabajadores de Coca Cola completan 4 días en huelga de hambre
Date: 2016-11-03 13:50
Category: Movilización, Nacional
Tags: Huelga de hambre, Sinaltrainal, tercerización laboral
Slug: trabajadores-coca-cola-completan-4-dias-huelga-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cocacola.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombianos en el Exilio] 

###### [3 Nov de 2016] 

Los trabajadores denuncian que se ha **agudizado la persecución laboral, el aumento de la tercerización laboral y amenazas de grupos paramilitares a integrantes de diversos sindicatos. **En Bucaramanga los trabajadores de la Empresa de Servicios Públicos se encuentran en huelga de hambre frente a las instalaciones de la alcaldía y en Medellín, frente a las instalaciones de la fábrica de Coca Cola.

Javier Correa, secretario de Derechos Humanos de Sinaltrainal, explica que hay persecución contra trabajadores enfermos para que renuncien “tenemos un trabajador que tuvo una cirugía de corazón abierto y **ha recibido visitas en el hospital de otros funcionarios de la empresa y le dicen que renuncie”**, según Correa, “pagar la incapacidad es más problemático para la empresa y por eso están sobrepasando las fronteras de una relación laboral”. Le puede interesar: [Sinaltrainal denuncia persecución contra trabajadores de Saceites.](https://archivo.contagioradio.com/sinaltrainal-denuncia-persecucion-laboral-contra-trabajadores-de-saceites/)

El dirigente sindical denuncia que quienes hacen parte del Sinaltrainal **“son considerados objetivos militares por parte de grupos paramilitares que ven en su actividad una amenaza para el funcionamiento de la empresa”**, y resalta que no se tienen respuestas por parte de entes investigadores como la Fiscalía, para impedir daños a la integridad física de los sindicalistas amenazados y sus familias. Le puede interesar: [Se reabre caso por asesinato de sindicalista que trabajaba en Coca Cola.](https://archivo.contagioradio.com/se-reabre-caso-del-asesinato-de-sindicalista-afiliado-a-cocacola/)

Otra de las situaciones es la **tercerización laboral de más del 78% del total de empleados, Coca Cola usa a empresas de su misma firma como OXXO, Imbera, Mecanicatea, Nuevas Gaseosas Colombia**, entre otras. “El procedimiento es despedir a trabajadores directos de la embotelladora para que su contrato lo firmen las otras empresas sin asociaciones sindicales y en condiciones mucho peores que las que pueden tener siendo empleados directos y no solamente contratistas”.

### **Huelga de hambre en Bucaramanga** 

De acuerdo con las denuncias de los trabajadores de la Empresa de Servicios Públicos de Santander, los integrantes sindicales, vienen siendo objeto desde hace varios años de "persecuciones, amenazas de muerte, judicialización, obstrucción de su actividad sindical, desplazamientos forzados de sus puestos de trabajo, negación a la atención médica de sus familiares, sobrecargas laborales por falta de personal, **violaciones a las Convenciones Colectivas de Trabajo y Acuerdos Colectivos, así como intentos de cancelación de las personerías jurídicas de los sindicatos**".

Este conjunto de acciones, evidencia una **sistemática violación  del Derecho de Asociación, de la Libertad Sindical, y del Derecho al Trabajo.** El Concejo de esa ciudad, respaldó la huelga de hambre suspendiendo las sesiones de trabajo hasta que el alcalde Rodolfo Hernández, atienda las exigencias de los trabajadores.

### **¿Cuándo se levanta la huelga de hambre?** 

Según Correa, coca cola ya incumplió a los trabajadores que realizaron una huelga en Bogotá, citando a una serie de encuentros en otras ciudades, sin embargo se espera que con el respaldo del Ministerio de Trabajo y el informe que se presentó por parte del sindicato en Julio pasado, tanto Coca Cola como la empresa de servicios públicos de Bucaramanga respondan de manera pronta y eficaz.

<iframe id="audio_13600697" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13600697_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
