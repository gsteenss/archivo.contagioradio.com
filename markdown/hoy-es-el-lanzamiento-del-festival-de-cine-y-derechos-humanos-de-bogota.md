Title: Bogotá inaugura el Festival de Cine y Derechos Humanos
Date: 2015-04-07 15:56
Author: CtgAdm
Category: 24 Cuadros, Nacional
Tags: Cine alternativo, Cine Migrante, Diana Arias, Festival de Cine y Derechos Humanos, Impulsos Films
Slug: hoy-es-el-lanzamiento-del-festival-de-cine-y-derechos-humanos-de-bogota
Status: published

<iframe src="http://www.ivoox.com/player_ek_4320766_2_1.html?data=lZifkpyaeo6ZmKiak5qJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fxM7bx5DYpc7WyoqwlYqlfc%2BfxtiY19PFb8nZ09fOz87JstXVjMnSjcnJstbixM7Oj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Arias, Directora del Festival] 

Para** Diana Arias, directora del Festival de Cine y Derechos Humanos**, este es un espacio público de diálogo y reflexión que genera una posición crítica del contexto colombiano y del panorama universal en el tema de la defensa de los derechos humanos. “*Nos hemos dado cuenta de la preocupación y la iniciativa para contar las realidades*” es necesario que  “*le gente conozca esas realidades y que mejor que hacerlo a través del cine que es una herramienta fabulosa*” señala Arias.

Según Arias en la primera entrega del festival, en 2014, acogieron la convocatoria 480 documentales y cortometrajes de 35 países, en la segunda versión llegaron **1365 películas, documentales y cortometrajes, “lo cual quiere decir que el festival está tomando nombre dentro de la escena de los realizadores y productores del todo el mundo”**

El festival presentará la selección oficial con **80 producciones de 23 países**, en las que los temas principales serán la construcción y defensa de los derechos humanos en Colombia  y el mundo.

Este festival ha contado con el apoyo de la **“Biblio red”, la Alcaldía de Bogotá** y un equipo humano que ha venido trabajando durante varios meses para tener este festival al aire. También está el comité curador que hizo la selección final que está compuesto por cerca de 20 personas, por su parte, el jurado escogerá las producciones ganadoras que se darán a conocer el próximo 11 de Abril.

Dado el éxito de las convocatorias y las propuestas se está pensando en que el Festival de Cine y Derechos Humanos pueda estar en otras ciudades colombianas como Pereira, Cartagena, entre otras, sino también en escenarios internacionales como Buenos Aires y Paris en donde ya se compartieron algunas muestras.

**La inauguración del festival será el martes 7 de abril** a las 5:00 pm con el estreno de la película colombiana ‘La Sargento Matacho’ en el Auditorio Huitaca de la Alcaldía Mayor de Bogotá

Este miércoles, 8 de Abril, en 24 Cuadros, programa de Cine de Contagio Radio, contaremos con la presencia de Diana Arias para ampliar la información y profundizar en la apuesta de la construcción de propuestas y la denuncia desde el Cine.

[Consulte acá la programación del festival.](http://https://archivo.contagioradio.com/llega-el-2do-festival-internacional-de-cine-por-los-derechos-humanos/)
