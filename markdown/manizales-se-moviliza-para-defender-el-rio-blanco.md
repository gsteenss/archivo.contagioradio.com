Title: Manizales está de "Carnaval" en defensa del Rio Blanco
Date: 2017-06-01 13:41
Category: Ambiente, Movilización
Tags: Agua, Movilización, reserva forestal, Río Blanco
Slug: manizales-se-moviliza-para-defender-el-rio-blanco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/RIO-BLANCO6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Poleka Kasue] 

###### [01 Jun. 2017] 

A propósito de las movilizaciones que se están realizando en todo el país y que se darán este jueves y viernes en defensa del agua, en Manizales las comunidades se movilizarán en el **Carnaval para defender la Reserva Forestal Protectora Rio Blanco**, ubicada en Kumanday, Manizales, una de las zonas más biodiversas del mundo y amenazada por en un proyecto urbanístico de 2200 viviendas.

Las **firmas Vélez Uribe Ingeniería y CFC Asociados son las dos constructoras que planean construir una urbanización para 10 mil personas** en La Aurora, una hacienda contigua a la Reserva Forestal Protectora Río Blanco. Le puede interesar: [Constructoras ponen en riesgo la "Reserva Forestal Río Blanco"](https://archivo.contagioradio.com/conctructoras-ponen-en-riego-la-reserva-forestal-rio-blanco/)

**Daniel Hassam, integrante de la organización Todos Somos Río Blanco** afirma que saldrán a las 6 de la tarde desde la Torre del Cable hacia el Parque de la Mujer para mostrar su inconformismo ante este proyecto. Le puede interesar: [Micro-hidroeléctricas en Caldas han secado 19 fuentes de agua](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/)

“Será una marcha carnaval, cultural, donde **la ciudadanía se va a expresar en contra de este proyecto y de otras formas de destrucción del ecosistema** y de daño ambiental que se están presentando” recalcó Hassam.

### **¿En qué va el proyecto de urbanización?** 

Para las comunidades el proyecto ya está muy avanzado, porque si bien no han comenzado aún las construcciones si **se han realizado modificaciones al Plan de Ordenamiento Territorial POT de Manizales en el año 2003**, dado que en un principio la Reserva solo tenía una vocación de suelo rural y no se podían realizar construcciones.

“En el 2003 pasó a ser un suelo de tradición urbana y **de allí para acá pues ya vinieron todos los proyectos urbanísticos que se empezaron a plantear sobre el área”** añadió Hassam.

En la actualidad, ya se encuentran firmadas las autorizaciones de tipo administrativo y ambiental para que el primer urbanizador empiece un proyecto que va a contemplar la construcción de aproximadamente 2200 viviendas. Le puede interesar: [Hidroeléctrica el Edén acaba con el agua de Bolivia, Caldas](https://archivo.contagioradio.com/sin-agua-corregimiento-en-caldas-por-cuenta-de-la-hidroelectrica-el-eden/)

### **No hay estudios ambientales para construir en la Reserva** 

Asegura Hassam que hasta el momento **no han conocido de estudios ambientales, científicos o técnicos** realizados por parte de las constructoras o las autoridades que garanticen la viabilidad en términos ambientales de llevar a cabo esta urbanización.

“Siempre se han pedido e incluso es lo que legalmente se ha insistido, porque ese tipo de intervenciones urbanísticas deben estar precedidas de estudios técnicos que garanticen la viabilidad para llevar a cabo este proyecto de tanto impacto” concluye el líder. Le puede interesar: [En Montebonito, Caldas, no quieren proyectos hidroeléctricos](https://archivo.contagioradio.com/habitantes-monte-bonito-caldas-esperan-nieguen-licencia-proyecto-hidroelectrico/)

<iframe id="audio_19026324" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19026324_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
