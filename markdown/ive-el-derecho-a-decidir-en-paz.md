Title: IVE: el derecho a decidir en paz
Date: 2020-02-17 14:19
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Aborte IVE, aborto, Aborto en Colombia, Aborto legal, Aborto Legal en Colombia, mujeres
Slug: ive-el-derecho-a-decidir-en-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/800_x5x.jpeg_862388873.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Por: Renata Cabrales**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En colombia, por estos días, los medios de comunicación y las redes sociales  exponen en la palestra pública a una joven de 22 años, por defender su derecho a la Interrupción Voluntaria del Embarazo, IVE, pues es sabido que la sentencia **C355** de 2006 de la Corte Constitucional, lo admite.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Las tres causales que contempla la Corte para hacer el procedimiento de aborto son:

<!-- /wp:heading -->

<!-- wp:paragraph -->

malformación del feto, violación o que la vida de la madre esté en riesgo por el embarazo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero, Según la expareja de la joven víctima, quien pretende denunciarla por supuesto homicidio, pues el feto tenía siete meses de gestación; esta no cumplía con ninguna causal para exigir su derecho al aborto. Como si él tuviera la potestad para definir en qué condiciones está la salud mental de la joven, ¡faltaba más!

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La directora de Profamilia *(entidad que realizó el procedimiento)*, Marta Elena Arroyo, explicó que se llevó a cabo tras una evaluación médica que comprobó que cumplía con una de las tres causales de la Sentencia. El caso fue certificado por un profesional que concluyó que la joven sí tenía problemas mentales, esto es, de depresión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay que aclarar también que la interrupción se hace en cualquier momento de la gestación y que solo la mujer puede decidir.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, para Juan Carlos Vargas, asesor científico de Profamilia, “Lo más importante es decir que el aborto es un derecho que tienen las mujeres, y que no podemos ver a las mujeres como simplemente un útero o una máquina que sirven para gestar embarazos no más”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con todo esto, nos queda claro que somos una sociedad hipócrita que persigue a una mujer al mejor estilo de la Santa Inquisición, por tomar una decisión que no es para nada fácil, y porque su salud mental merece importancia. Pero volteamos la cara ante casos verdaderamente graves, como los niños y las niñas que mueren de hambre en el país, por culpa de gobiernos corruptos; ni qué decir de los niños y las niñas víctimas de violencia sexual por sus propios familiares. Más aberrante aún, víctimas de abusos y violencia sexual en manos de sacerdotes, esos mismos que se atreven a decir que la pedofilia es menos grave que el aborto, y a quienes, además, se les cubre la espalda ante sus actos aberrantes, porque somos un país de doble moral religiosa; pero también están los niños que son bombardeados en medio de la guerra eterna, “porque eligieron ser guerrilleros” y porque hay vidas que importan y otras no.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vea mas [columnas de opinión](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
