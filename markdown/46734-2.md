Title: Agencia Nacional de Tierras, no puede excluir a comunidades indígenas ni campesinos de Villa Garzón
Date: 2017-09-16 09:00
Category: DDHH, Nacional
Tags: comunidades indígenas, Putumayo
Slug: 46734-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Sept 2017] 

Las comunidades situadas en el municipio de Villagarzón, Putumayo, a través de un comunicado, le están **exigiendo a la Agencia Nacional de Tierras, que suspenda inmediatamente el proceso de declaratoria del resguardo Inga**, mientras concluye el proceso de ordenamiento ambiental, que decidirá cuántas hectáreas del municipio son declaradas como área de protección, su plan de manejo estratégico y posteriormente responder a las solicitudes hechas por otras comunidades indígenas.

Las comunidades le han expresado a la ANT, que si bien es cierto que en el territorio existen comunidades pertenecientes al resguardo Inga, **el 98% de la población es proveniente ancestralmente de otros grupos indígenas o campesinos**.

Razón por la cual, declarar el territorio como resguardo indígena de la comunidad Inga afectaría al resto de los habitantes del territorio que llevan habitándolo históricamente, como es el caso de los campesinos que allí se encuentran. (Le puede interesar: ["Comunidades buscan llegar a acuerdo sobre adjudicación de Tierras en el Putumayo"](https://archivo.contagioradio.com/comunidades-buscan-llegar-a-acuerdo-sobre-adjudicacion-de-tierras-en-putumayo/))

De igual forma los habitantes han manifestado que no están interesados en tener ningún tipo de confrontación con la comunidad Inga, sino que se llegue a un acuerdo en donde se establezca, en primera medida la protección del territorio y en segundo que respete la convivencia historia **de comunidades como la Awa, Nasa, Embera Chamí, afrodecendientes, entre otros**.

Además, en el texto expresaron que responsabilizan a las instituciones “**de lo que se pueda presentar a futuro de no atenderse a tiempo esta solicitud**”, sin embargo, reiteraron su ánimo por el diálogo como herramienta para resolver esta problemática.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
