Title: A 7 años de la independencia de Kosovo continua la impunidad
Date: 2015-02-17 22:50
Author: CtgAdm
Category: El mundo, Política
Tags: Independencia de Kosovo, Kosovo, Mitrovica, Serbia, TPI Kosovo, Yugoslavia
Slug: a-7-anos-de-la-independencia-de-kosovo-continua-la-impunidad
Status: published

###### **Foto:Wikipedia.org** 

Hoy se cumplen **7 años de la independencia de Kosovo, apoyada por EEUU**, y más tarde por otros miembros de la Unión Europea, actualmente esta **reconocida por 108 países de los 198 que conforman las Naciones Unidas**, países como China, Rusia, España o Serbia no la reconocen por distintas causas todas relacionadas con la geopolítica.

Después del **conflicto armado** que asoló el país entre 1996 y 1999, aunque siendo este una guerra abierta con Serbia entre **1998 y 1999.** La contienda comienza en el marco de la desmembración de las repúblicas de la antigua ex-Yugoslavia, mediante un proceso de reconversión de los distintos ejércitos de comunistas a nacionalistas.

Es en este contexto donde afloran las históricas reivindicaciones de autonomía de Kosovo, fuertemente reprimidas por las autoridades serbias de quien eran una autonomía. Es entonces cuando se conforma el UCK, conocido como Ejercito de Liberación de Kosovo, enfrentándose a fuerzas serbias comandadas por el ex-presidente serbio Milosevic y grupos de paramilitares afines al régimen serbio.

Después de los sucedido en las anteriores guerras, como la de Croacia o Bosnia, la OTAN decide intervenir, restando protagonismo a la ONU y la UE, y retornando a posturas cercanas a la guerra fría. EEUU bombardea la capital serbia, Belgrado, hasta que las fuerzas serbias se retiran, entonces es cuando pasa a **ser administrado primero por un protectorado internacional y luego por el gobierno Kosovar y por la fuerza de protección de Naciones Unidas conocida como UNMIK.**

En la guerra se cometieron limpiezas étnicas por ambos lados, siendo los serbios los más afectados. Es importante remarcar la labor de ACNUR en el retorno de casi un 70% de la población albanokosovar, aunque no sucedió lo mismo con la serbokosovar que huyó en gran parte hacia Belgrado.

**Serbia continua considerando a Kosovo como una provincia autónoma suya y aún no ha reconocido su independencia**, por otro lado la provincia es la cuna de la historia del pueblo serbio, en referencia a la batalla de Kosovo Polje.

La composición étnica de **Kosovo es de un 88% de albanokosovares y un 8% de serbios**, estos en la actualidad viven en el sur y al norte cerca de la frontera con Serbia donde tienen su comunidad mas grande en la ciudad de **Mitrovica.**

La ciudad ha quedado dividida por el puente (como se puede observar en la fotografía) en la parte serbia y la Kosovar. Éstos viven fuertemente escoltados por fuerzas de las Naciones Unidas, no reconocen la independencia y tienen un gobierno autónomo. **Su situación es precaria y similar a la de un ghetto, aunque gozan de beneficios del gobierno serbio.**

Los distintos gobiernos kosovares han ido cayendo debido a los numerosos casos de corrupción y a las peticiones de detención por crímenes de guerra, ya que muchos de ellos eran antiguos guerrilleros. La** fiscal del TPI Carla Delmonte** elaboró un informe acusando a los distintos gobiernos de **haber financiado tanto la resistencia como el propio Estado con el tráfico de órganos.**

La realidad es que el país vive una actual crisis económica con más del 44% de la población desempleada y con una **dependencia económica basada en el sector servicios que prestan a** **las fuerzas armadas de las distintas misiones humanitarias.**

La principal problemática proviene del estancamiento de las negociaciones con Serbia que hasta el miércoles de la semana pasada no iniciaron.En la mesa están las distintas propiedades somo las industrias del metal o la autonomía de los serbokosovares, así como el propio status quo del país con respecto a su vecino.

Actualmente se ha creado un **movimiento conocido como "Autonomía" que ha roto con la tradicional esquema de partidos nacionalistas** alineados con el antiguo conflicto armado y que gobierna la capital Prístina. Este movimiento es proclive a normalizar las relaciones con el vecino y es **muy crítico con la intervención internacional y con el papel de EEUU, reclamando su salida del país.**

Por último es importante destacar que **aún no se ha puesto en funcionamiento una comisión de la verdad que esclarezca los crímenes de guerra** y de lesa humanidad cometidos por ambos bandos, continuando muchos de los **victimarios en total impunidad.**
