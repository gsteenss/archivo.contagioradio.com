Title: El legado humanista de Javier Darío Restrepo al periodismo
Date: 2019-10-07 17:25
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: Ética Periodística, Javier Darío Restrepo, periodismo en colombia
Slug: el-legado-humanista-que-dejo-javier-dario-restrepo-en-el-periodismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/javier-dario-restrepo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Este 6 de octubre, a la edad de 87 años falleció  el periodista, catedrático y columnista Javier Darío Restrepo, quien dedicó más de medio siglo a este oficio, convirtiéndose en un referente de la ética periodística, hoy, el maestro deja un legado y una serie de enseñanzas a seguir, para conocer qué rumbo ha de tomar el periodismo en Colombia.

Durante una de sus últimas conversaciones con Contagio Radio, el maestro oriundo de Jericó, Antioquia, se refirió al rol de los medios de comunicación y la necesidad de crear un ambiente propicio para la reconciliación y el perdón, siendo como periodistas, "partidarios del pueblo colombiano" **para así estar a su servicio e interpretar sus sentimientos y expectativas.** [Lea también: Javier Darío Restrepo: El periodista y los medios de información en tiempos de paz](https://archivo.contagioradio.com/javier-dario-restrepo-el-periodista-y-los-medios-en-tiempos-de-paz/))

Ignacio Gómez, subdirector de Noticias Uno, señala que incluso en la parte final de su carrera como consultor de la Fundación Gabriel García Márquez para el Nuevo Periodismo Iberoamericano, actualmente Fundación Gabo,Javier Darío se mantuvo como un referente que planteó las bases teóricas para un periodismo de análisis en Colombia y en general América Latina.

### Javier Darío al servicio del pueblo

Gómez, señala además, que como periodista, Javier Darío introdujo una aproximación humana a la noticia quien se encargó determinar cómo los hechos afectaban a la gente en su cotidianidad y lo explicaba a sus audiencias de una manera sencilla, aproximando la noticia al lenguaje popular, "es poder entender que los hechos no suceden frente a una atierra despoblada, y que los hechos son hechos por que afectan a la gente", afirma.

Para el subdirector de Noticias Uno, Restrepo no se ceñía a una línea tradicional de periodismo, pues lo recuerda como un "ser contestatario en el sentido periodístico, racional y emocional con la gente", el periodista  se enfocaban en relatar cómo los hechos afectaban a la gente.  [(Le puede interesar: La importancia de la ética en medios públicos, una lección de Javier Darío Restrepo)](https://archivo.contagioradio.com/la-importancia-de-la-etica-en-los-medios-publicos-una-leccion-de-javier-dario-restrepo/)

"La academia te enseña a hacer cosas pero no te enseña a ser persona, y eso requiere formación, para poder después enseñar técnicas", afirmó Javier Darío en una ocasión, destacando la importancia de realizar un periodismo enfocado a la  inteligencia, revelando nuevas propuestas de contar un suceso, reflexiones que resultan de gran valor para el futuro del periodismo en el país.

<iframe id="audio_42763154" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42763154_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
