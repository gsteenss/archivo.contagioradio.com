Title: El trabajo de las mujeres en Colombia
Date: 2016-11-24 21:05
Category: Mujer, Nacional
Tags: 25 de noviembre día de la no violencia contra la mujer, Defensa de derechos de las mujeres, derechos laborales de las mujeres, Trabajo de las mujeres en Colombia
Slug: trabajo-las-mujeres-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mujer-chontaduro112416.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2016] 

De acuerdo con el resumen ejecutivo presentado en 2016 por la Organización Internacional del Trabajo sobre el trabajo de las Mujeres, revela que **entre 1995 y 2015 la tasa de participación de las mujeres en la fuerza de trabajo cayó de un 52,4% paso a ser del 49.6%.**

El informe de la OIT deja en evidencia la **brecha entre hombres y mujeres y las probabilidades de participación de las mismas en el mercado laboral** con una diferencia de 27 puntos porcentuales menos, las posibilidades de que las mujeres en el mundo accedan a un trabajo son mucho menores, **lo que socavando su capacidad de tener ingresos, autonomía y estabilidad económica.**

### **¿Cómo esta la situación del trabajo para las mujeres en Colombia?** 

**En Colombia a 2016 la brecha salarial entre hombre y mujeres es aproximadamente del 28%,** hecho al que se le suma una mayor participación de las mismas en trabajos no reconocidos como tal y no remunerados, como es el del **cuidado del hogar, los menores y la tercera edad.**

La Corporación Viva la Ciudadanía y el Colectivo de Abogados José Alvear Restrepo presentaron en 2016 uno de los resultados de la Encuesta realizada por el Departamento Administrativo Nacional de Estadística (DANE) sobre los Usos del Tiempo, en el que se evidenció la distribución social del cuidado inequitativa en Colombia; **las mujeres dedican 3 veces más el tiempo en la dichas labores que los hombres, promedio que aumenta a 4 en las zonas rurales.  **

Así mismo, según el informe *Brecha Salarial entre Hombres y Mujeres en Colombia: Un problema Descuidado,* presentado en 2016 por Viva La Ciudadanía, **alrededor de 5 millones de mujeres en Colombia se dedican al trabajo doméstico no remunerado** quedando de tal forma excluidas del mercado laboral y la protección social, sumado a ello existe** una tasa de informalidad femenina del 52%.**

Sobre este panorama vale la pena señalar también **la situación de las mujeres en materia del acoso laboral y sexual que viven a diario.** Según un estudio realizado en 2014 por el Grupo de Equidad Laboral adscrito al Ministerio del Trabajo y presentado por el Observatorio de Género, Equidad y Justicia **un 82% manifestó haber recibido solicitudes o presión para tener sexo y/o actos sexuales no consentidos,** o no aceptados, y en el **79% de los casos hubo un intento u ocurrencia del acto sexual, la mayoría en contra de mujeres. **Le puede interesar: [La violencia contra las mujeres en Colombia no cesa.](https://archivo.contagioradio.com/la-violencia-contra-las-mujeresen-colombia-no-cesa/)

###### Reciba toda la información de Contagio Radio en [[su correo]
