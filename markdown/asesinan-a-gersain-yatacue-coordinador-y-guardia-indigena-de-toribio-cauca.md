Title: Asesinan a Gersain Yatacue, coordinador y guardia indígena de Toribio, Cauca
Date: 2019-08-01 17:16
Author: CtgAdm
Category: Comunidad, Líderes sociales
Tags: Amenazas contra indígenas, Caloto, Cauca, elecciones, Paez
Slug: asesinan-a-gersain-yatacue-coordinador-y-guardia-indigena-de-toribio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Guardia_Indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

Las autoridades indígenas del norte del Cauca denunciaron este 1 de agosto el asesinato del coordinador de la guardia de la vereda San Julián de Toribio, Gersain Yatacue. Los sucesos ocurrieron cuando fue abordado por hombres desconocidos que dispararon contra el guardia que se encontraba en el sector de Pajarito, resguardo de Huellas Caloto.

"Hoy estamos siendo objetivo militar de los grupos que quieren causar desarmonías en los territorios, por eso hacemos un llamado a la comunidad para continuar con este ejercicio” expresó uno de los coordinadores de zona de la guardia indígena del norte quien atribuye este ataque a represalias contra el control territorial.

### Amenazas a candidatos en época electoral

De igual forma, el Consejo Regional Indígena del Cauca (CRIC) alertó sobre dos nuevas amenazas en contra de John Fredy Víquez, candidato a la Alcaldía del municipio de Páez y al candidato al concejo de dicho municipio a través de un panfleto firmado por las autodenominadas Autodefensa Gaitanístas de Colombia (AGC).

Para Joe Sauca, coordinador de derechos humanos del CRIC, las amenazas están ligadas directamente a los propuestas de los candidatos, enfocadas a los planes de vida de los territorios indígenas y campesinos, que equivalen a "propuestas alternativas que buscan cambiar la dinámica del sistema" y que vienen directamente desde las comunidades.

"Se habían demorado, por lo general en épocas electorales, el Cauca sufre este tipo de consecuencias" mencionó Sauca quien atribuye estas amenazas a grupos como las Águilas Negras, disidencias de las FARC o las AGC. [(Lea también: Amenazan a líderes del CRIC y ACIN que participan en Minga indígena)](https://archivo.contagioradio.com/amenazan-a-lideres-del-cric-y-acin-que-participan-en-minga-indigena/)

Sauca reitera que se trata de un territorio donde hay fuerte presencia de industrias y monocultivos y estos candidatos vienen desde las comunidades no gustan porque respaldan el control territorial de las guardias indígenas y eso implica que haya retaliaciones.[(Le puede interesar: Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca)](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)

### Guardia indígena protege a los candidatos

"Sabemos perfectamente que el Estado no da garantías frente a estos hechos, pero el mandato de la comunidad es hacer el esfuerzo de protección con la guardia indígena" afirma el coordinador quien asegura que los candidatos no van a desistir en su carrera electoral frente a las amenazas.

Según Sauca, a tan solo tres meses de las elecciones, en el Cauca se respira un ambiente en el que a raíz del Acuerdo de paz, las personas buscan otras alternativas en sus candidatos, algo que según el integrante del CRIC debe verse reflejado en las urnas, "la idea es que la gente con criterio dé su voto y que no venda más su conciencia por un tamal o por 50.000 sino empezar a darle cambio a este sistema".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39460350" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39460350_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
