Title: Los pasos a seguir para que la Consulta Antitaurina sea un hecho
Date: 2015-07-29 12:35
Category: Animales, Voces de la Tierra
Tags: Concejo de Bogotá, Consulta Antitaurina, consulta popular, corridas de toros en Bogotá, Maltrato animal, participación ciudadana, Plataforma Alto, tortura animal
Slug: los-pasos-a-seguir-para-que-la-consulta-antitaurina-sea-un-hecho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/MG_980511.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_5514653_2_1.html?data=lpqelpuZd46ZmKiakp6Jd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjNXO1dTXb8Kf1MrU187Wb9HV08aY09rJb83VjKjc0NjZsNXVjKbb1s7YpdbmytPOjdjJpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jonathan Ramírez, abogado Plataforma ALTO] 

La ciudadanía reunida en el movimiento **“Bogotá Sin Toreo”, celebra la decisión del Concejo de Bogotá que ayer le dio el sí a la consulta popular** para que los bogotanos y bogotanas sean quienes decidan sobre el futuro de las corridas de toros en la capital.

“Alegría absoluta, este ha sido un paso en una serie de pasos que llevamos, aun queda un largo camino por recorrer, que **concluye el próximo 25 de octubre cuando salgamos más de 1´600.000 habitantes a votar para que no hayan más corridas en Bogotá**”, expresa el abogado de la Plataforma ALTO, Jonathan Ramírez.

**Con 29 votos a favor y 6 en contra,** el Concejo definió dar paso a que se demuestre si existe o no arraigo cultural mayoritario sobre las corridas de toros y novilladas. El **siguiente paso continúa en el Tribunal Administrativo de Cundinamarca,** donde se tiene un periodo de 15 días hábiles para definir si la consulta es constitucional. Finalmente, se tiene un plazo de 10 días para que los ciudadanos expresen si están o no de acuerdo con esta iniciativa.

La Ley 134 de 1994 establece que el alcalde es el competente para presentar una solicitud de consulta popular, sin embargo, el abogado aclara que no es una solicitud de un partido político y mucho menos del alcalde, **“es la ciudadanía que ya no quiere más espectáculos violentos en la ciudad,** la que pide que se verifique que la mayoría de la población no quiere corridas de toros”.

La sentencia 666 del 2010, dice que no se pueden realizar corridas de toros en Bogotá si no se reúnen los requisitos, uno de ellos es sobre el arraigo cultural mayoritario, lo que se evidenciaría con este mecanismo de participación ciudadana, explica el abogado de la Plataforma ALTO.

Desde “Bogotá Sin Toreo” se extiende la invitación para que la ciudadanía esté atenta a las actividades programas para impulsar la consulta popular y finalmente se pueda demostrar que **“Los bogotanos no somos violentos, ni queremos espectáculos violentos”,** concluye Ramírez.
