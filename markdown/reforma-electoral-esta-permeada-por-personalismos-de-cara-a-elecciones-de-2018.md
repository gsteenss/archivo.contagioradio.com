Title: Lo que queda de la Reforma Política inicia trámite en el Congreso
Date: 2017-08-08 13:21
Category: Nacional, Política
Tags: control electoral, elecciones, MOE, partidos politicos, Reforma Electoral
Slug: reforma-electoral-esta-permeada-por-personalismos-de-cara-a-elecciones-de-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [08 Ago. 2017] 

“De una gran reforma política se ha ido disminuyendo, quitando pedazos, sin embargo, **lo más importante es lucha por mantener los aspectos esenciales** de lo que es una reforma electoral” así lo manifestó **Camilo Mancera, integrante de la Misión de Observación Electoral – MOE -,** luego de las modificaciones al proyecto de reforma presentado ante el Congreso por la MOE en compañía de otras organizaciones y que será debatido en los próximos días.

Con esta propuesta la MOE estaba **planteando cambiar toda la estructura electoral que permitiría un mayor control a la financiación** y la apertura democrática que Colombia podría necesitar luego de la firma del Acuerdo de Paz entre las FARC y el Gobierno. Le puede interesar: [Sin Corte Electoral ni listas cerradas la Reforma Electoral va a Fast Track](https://archivo.contagioradio.com/sin-corte-electoral-ni-listas-cerradas-la-reforma-electoral-va-a-fast-track/)

### **¿Qué quedó del texto inicial propuesto por la MOE?** 

-   #### **Mecanismo de control electoral que no sea partidista**

Una de las mayores “luchas” dice Mancera, que han tenido desde la MOE, es el tema de la conformación del mecanismo de control electoral, puesto que en la actualidad esta organización ha estado “peleando contra **un organismo que es partidista y que no tiene la posibilidad de garantizar la independencia** a la que aspirarían todos los actores políticos”.

Por eso la MOE insiste y seguirá trabajando porque este mecanismo no tenga origen partidista, puesto que al serlo existirán conflicto de intereses frente a una organización que tendría la labor de sancionar candidatos y partidos **“que mejor garantía que quitarle este origen político”**. Le puede interesar: [Estas son las recomendaciones de la Misión Electoral para la reforma política](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/)

-   #### **Presencia territorial del mecanismo de control**

Asegura Mancera que es indispensable que este mecanismo tenga presencia y control en los territorios “de esta manera se puede hacer veeduría para ver si se están cumpliendo con las garantías electorales, se puede además vigilar la financiación que es un tema tan delicado y del que hemos escuchado muchos escándalos”.

-   #### **Registro de Afiliados**

Este mecanismo a través del cual se están fortaleciendo las organizaciones políticas, permitirá que a largo plazo haya una mayor seriedad de los partidos, porque la ciudadanía estará allí gracias a una mayor afinidad con estas organizaciones políticas.

“No es solo que se generen más organizaciones políticas, lo importante es que la ciudadanía tenga un contacto real con las organizaciones políticas. En la actualidad operamos a través de firmas y en Colombia casi no se niega una firma y lo que busca la reforma es que más allá de una firma se hable de **un régimen de afiliados donde el ciudadano decida a qué partido quiere hacer parte,** lo que hará que los partidos busquen a los ciudadanos, van a querer atraerlos”.

Cabe recordar que en este momento los ciudadanos se encuentran habilitados para firmar por más de un candidato puesto que no existe ninguna restricción frente al tema.

### **¿A quién le favorece que no haya voto electrónico en Colombia?** 

El voto electrónico en la actualidad ya se encuentra dentro de la constitución y en la ley 892 de 2004, en la cual se establecieron los nuevos mecanismos de votación e inscripción para garantizar el ejercicio de ese derecho, sin embargo a la fecha no ha sucedido nada.

“Lo que parece es que va a tocar volver a meterlo en la constitución para que pueda implementarse, lo cual no tiene ningún sentido. **El mandato está solo se necesita de la voluntad política para sacar adelante esta propuesta”**. Le puede interesar: [Hay sectores que no les interesa una reforma electoral: MOE](https://archivo.contagioradio.com/hay-sectores-que-no-les-interesa-reforma-electoral-moe/)

### **Cambio Radical propuso retirar varios temas de la reforma electoral** 

La personería jurídica para movimientos ciudadanos, reelección de congresistas y listas cerradas son los puntos que el partido político Cambio Radical propone sean quitados, argumentando que no favorecen a la democracia ni a la Reforma Política, los cuales para Mancera son los puntos más difíciles y los que más afectan en el debate que se avecina.

“Hay un gran problema cuando uno presenta una reforma electoral ad portas de un proceso electoral, es decir, ya se vienen las elecciones a Congreso y Presidencia. Es un gran riesgo que los Congresistas en vez de gestionar por un bienestar general empiezan es a verse cada uno en lo que van a ser las elecciones que se acercan. **Esos personalismos terminan afectando profundamente el objetivo de una reforma”.**

Concluye diciendo que ojalá los Congresistas se pongan la mano en el corazón y digan que esta es la reforma que necesita el país y que puede generar mayor transparencia en los procesos electorales y no pensar que es la reforma que les permitirá mantener la curul “Ese cambio de conciencia es indispensable para el momento en el que nos encontramos”.

### **“Reforma que llega al Congreso no contiene los aspectos esenciales” Iván Cepeda** 

Para el senador del Polo Democrático, **la Reforma que será debatida en el Congreso, podría significar un retroceso en algunos puntos** como el diseño de mecanismos de financiación transparentes y favorables de manera equitativa o el desarrollo de coaliciones políticas. Le puede interesar: [Partidos minoritarios logran acuerdo para impulsar reforma política](https://archivo.contagioradio.com/partidos-minoritarios-buscan-fortalecer-su-incidencia-en-la-contienda-politica/)

“Cómo hacer para que el voto cerrado no se convierta en una camisa de fuerza para que las listas puedan ser listas de coalición, hoy hay varios temas que son objeto de una gran discusión. Pero lo esencial es **que han quedado por fuera temas sustanciales como la reforma del poder electoral**, la cuestión de la financiación que sigue siendo poco transparente, las reglas del juego democrático y el sistema electoral”.

<iframe id="audio_20229751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20229751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_20256212" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20256212_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
