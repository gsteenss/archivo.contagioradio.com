Title: Denuncian condiciones indignantes en Zona Veredal de Puerto Asís
Date: 2017-02-08 17:05
Category: DDHH, Nacional
Tags: FARC, Implementación de los Acuerdos de paz, La Carmelita Puerto Asís, Zonas Veredales Transitorias de Normalización
Slug: denuncian-condiciones-indignantes-en-zona-veredal-de-puerto-asis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-08-at-5.01.16-PM-e1486591460330.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Feb 2017] 

Infecciones urinarias, brotes cutáneos, diarreas, riesgo de epidemias, cambuches improvisados, alimentos en mal estado, madres gestantes y 6 recién nacidos sin atención médica, son algunas de las situaciones que enfrentan los integrantes del Bloque Sur de las FARC, en la Zona Veredal La Carmelita en Puerto Asís. Durante la visita del mecanismo de monitoreo, se corroboró que sólo hay 30 personas por parte del Gobierno adelantando las obras de construcción de unos 600 alojamientos.

Carlos Fernández, defensor de derechos humanos e integrante de la Comisión de Verificación, aseguró que las anteriores, son “condiciones lamentables e indignantes”, señaló que la mayor preocupación es el agua, pues “tienen que consumir y utilizar para sus labores agua contaminada con hidrocarburos y mercurio” producto de las actividades extractivas que se han llevado a cabo en esta y otras zonas del Putumayo.

Fernández comentó que el agua “no es apta para el consumo humano” y representa un incumplimiento, pues se había acordado “un suministro permanente de agua por parte de autoridades y Gobierno Nacional”. ([Le puede interesar: Bloque Sur denuncia incumplimientos en](https://archivo.contagioradio.com/bloque-sur-denuncia-incumplimientos-en-zona-veredal-de-puerto-asis/) Zona Veredal de Puerto Asís)

### Ni servicios de salud ni alojamientos 

Respecto a la situación de las guerrilleras, Fernández aseguro que en el momento hay alrededor de 10 mujeres en estado de gestación y 6 bebés entre los dos y 10 meses, ninguno ha recibido la atención médica que requiere y se teme que debido a ello, se hagan presentes problemas de salud en las madres y los bebés.

Ramiro Gareca, integrante del Comité Político del Bloque Sur, denunció que funcionarios del mecanismo de monitoreo pidieron a los integrantes de las FARC, nombres y números de cédula, pues prometieron "hacer una brigada de salud", la cual hasta la fecha no se ha realizado.

### Ni 1 de 600 alojamientos 

Fernández, reveló que “no existe infraestructura básica para atender ni albergar a los integrantes del Bloque Sur”, sólo se ha avanzado en la construcción de una casa modelo, pero no hay avances para los 600 alojamientos que deben construirse. Advirtió que los guerrilleros han instalado carpas con plásticos, lo que ha hecho “insoportable la situación” pues hay días en que “hay temperaturas de más de 40º” y eso “afecta a los adultos, pero hace que los niños o puedan estar tranquilos”.

Comentó que varios integrantes del Bloque Sur han hecho turnos en las noches y madrugadas “para apoyar el avance de las obras”, pero no es suficiente, pues sólo hay una cuadrilla de 30 hombres destinados por el Gobierno y el compromiso era que al tener la infraestructura de los alojamientos, las FARC continuaba con las adecuaciones.

Por último, Fernández mencionó que el Mecanismo de Monitoreo y Verificación ya cuenta con la infraestructura del alojamiento, pero “no están allí, están a 800 mts de la Zona Veredal”. Además, hizo un llamado a entes gubernamentales y organizaciones garantes, para que no pierdan de vista la preocupante situación que se vive no sólo en esta, sino en las demás Zonas Veredales del país.

<iframe id="audio_16908388" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16908388_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
