Title: Estos son los ganadores del 4to Premio Nacional a la Defensa de los Derechos Humanos
Date: 2015-09-09 16:44
Category: DDHH, Nacional
Tags: Cocomacia, COS-PACC, Diakonia Colombia, Fabiola Lalinde, Francia Elena Márquez, Mujeres Caminando por la Verdad, remio Nacional a la Defensa de los Derechos Humanos
Slug: estos-son-los-ganadores-del-4to-premio-nacional-a-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/premio_DDHH_contagioradio1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [9 Sept 2015] 

En la mañana de este miércoles se conocieron los ganadores y ganadoras del Premio Nacional a la Defensa de los Derechos Humanos, que en su cuarta edición reconoce el trabajo de personas y organizaciones que han dedicado su vida a la causa de los derechos humanos en Colombia.

Los galardones, entregados en cinco categorías, reconocen el trabajo como **"Defensor o defensora del año**, a Francia Elena Márquez, lideresa afro de la vereda Yolombo en el municipio de Buenos Aires en Cauca, por su trabajo en la defensa a los territorios afros.

<iframe src="http://www.ivoox.com/player_ek_8250249_2_1.html?data=mZeikpeYfY6ZmKiakpiJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPVz8jWw5CpsMbiwpC6w9fVucbujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**"Experiencia o proceso colectivo del año**, **nivel comunitario"**, entregado a Mujeres Caminando por la Verdad, colectivo de mujeres víctimas de la operación Orión, trabajadoras en busca de justicia, verdad y contra la  impunidad en la acción cometida por el ejercito colombiano en el año 2002 en la comuna 13 de Medellín.

<iframe src="http://www.ivoox.com/player_ek_8250254_2_1.html?data=mZeikpeZeI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkdbextfS1ZCRh8LhytPO0MnTb9Hj05DZw5DUpduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la  categoría "**Experiencia o proceso colectivo del año**" Nivel ONG, la organización galardona fue  COS-PACC, sobrevivientes del exterminio de la Asociación Departamental de Usuarios Campesinos de Casanare en la década de los años 90, por su trabajo constante con las  comunidades y organizaciones campesinas, víctimas de crímenes de estado.

<iframe src="http://www.ivoox.com/player_ek_8250272_2_1.html?data=mZeikpebdo6ZmKiakpeJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DnjtXOxciPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la categoría  "**Toda una Vida**" **nivel personas**: la galardonada  fue Fabiola Lalinde, madre de Fernando Lalinde, desaparecido en el año 1984 en la ciudad de Medellín, su labor  en la búsqueda de su hijo la ha convertido en una referente en contra de la impunidad y la desaparición forzada en Colombia.

<iframe src="http://www.ivoox.com/player_ek_8250277_2_1.html?data=mZeikpebe46ZmKiakpeJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisLWytTZw5Cwpc3dz8nSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La última categoría "**Toda una vida, a** **nivel organizaciones"**,  fue para Cocomacia, organización que trabaja por visibilizar, denunciar y acompañar a las comunidades del Medio Atrato  en la defensa de los derechos humanos. Ha  acompañado a las comunidades en el retorno a sus territorios y ha impulsado planes de  etno-desarrollo

Dos máscaras y tres tejidos originales del pueblo indígena Camêntsá del Putumayo, fueron los galardones entregados a defensores y defensoras de derechos humanos en el reconocimiento internacional, liderado por Diakonia Programa Colombia.
