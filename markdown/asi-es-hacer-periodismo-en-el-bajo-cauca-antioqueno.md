Title: Asi es hacer periodismo en el Bajo Cauca Antioqueño
Date: 2020-04-21 17:56
Author: AdminContagio
Category: Actualidad, DDHH
Tags: bajo cauca antioqueño, Caparrapos, Eder Narváez
Slug: asi-es-hacer-periodismo-en-el-bajo-cauca-antioqueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Periodismo-en-el-Bajo-Cauca-Antioquieño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @ReporteroJ {#foto-reporteroj .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según datos de la Fundación para la Libertad de Prensa (FLIP), en Colombia han sido asesinados 160 periodistas por razones de su oficio entre 1977 y 2019, muchos de ellos trabajaban en regiones como Norte de Santander, Arauca o Antioquia. La crifra es solo una muestra de lo que significa informar sobre lo que ocurre en regiones donde el conflicto armado tiene una intensidad alta.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FLIP_org/status/1212865618816458753","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FLIP\_org/status/1212865618816458753

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Aún hoy, el riesgo para quienes amparados en el artículo 20 de la Constitución ejercen el oficio de periodistas está vigente en regiones como el Bajo Cauca Antioqueño, una zona en la que los actores armados ilegales se reorganizaron tras la firma del Acuerdo de Paz y mantienen una disputa por dominar el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Eder Narváez, periodista amenazado durante semana santa en Caucasia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según relata[Eder Narváez](https://flip.org.co/index.php/es/informacion/pronunciamientos/item/2501-periodista-de-caucasia-se-desplazo-forzadamente-luego-de-recibir-dos-amenazas-en-una-semana), director de NP Noticias y corresponsal de Teleantioquia en el Bajo Cauca Antioqueño, el 13 de abril llegó a su WhatsApp un mensaje de una persona que se identificó como Manuel 'el negro', comandante de los Caparrapos. El texto decía que tenía que dejar de estar hablando o lo "acuesto", y que las personas que mataron el día anterior era solo el principio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las personas a las que se refería alias 'el negro' en el mensaje fue un doble homicidio que ocurrió en el barrio Loma Fresca de Caucasia sobre el que informó Eder en NP Noticias. Él explica que ese medio siempre hace cubrimiento al tema de orden público en la región porque es un tema que a otros les da miedo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Esta es una situación que nos causa tristeza porque lo vemos como un ataque a la libertad de prensa.
>
> <cite>Eder Valencia</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esta no es la primera amenaza que recibe por el mismo medio: Eder tiene un chaleco, celular y botón de pánico de la Unidad Nacional de Protección (UNP) por una amenaza similar en la que lo trataban de sapo. Sin embargo, señala que sus medidas de protección son insuficientes porque "si los actores armados son crueles con la Fuerza Pública, que tiene cómo defenderse, cómo será con un periodista".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hacer periodismo en el Bajo Cauca Antioqueño

<!-- /wp:heading -->

<!-- wp:paragraph -->

Narváez sostiene que cuando un actor armado hace una amenaza tiene el propósito de silenciar al medio, generar temr y miedo. En el caso del equipo de NP Noticias, "esta amenaza surtió efecto" y ocasionó que dejaran de informar sobre lo que está ocurriendo en el territorio desde hace una semana por temor a sus vidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Narváez asegura que la labor en una región como el Bajo Cauca es difícil porque los comunicadores tienen prohibido visitar las zonas rurales, "si yo quiero ir a Caucacia o El Bagre tengo que hablar con los presidentes de las Juntas de Acción Comunal para que pidan permiso a los actores armados para incresar sin inconveniente, porque hacerlo sin pedir permiso es firmar una sentencia de muerte", afirma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, el periodista resalta que la solidaridad entre colegas y medios debe prevalecer para unir las causas, rodear a los más vulnerables y seguir cumpliendo con el deber de informar de manera verás y objetiva a la población sobre lo que ocurre en las regiones. (Le puede interesar: ["Se libra una guerra por tomar el control del Nudo de Paramillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
