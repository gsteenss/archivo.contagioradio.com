Title: 100 fuentes de agua están en riesgo en Monte Bonito, Caldas
Date: 2016-02-26 13:54
Category: Ambiente, Nacional
Tags: Caldas, Micro Hidroeléctricas, Monte Bonito, Pensilvania
Slug: 100-fuentes-de-agua-estan-en-riesgo-en-monte-bonito-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Montebonito-caldas-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: historiayregion] 

<iframe src="http://co.ivoox.com/es/player_ek_10587983_2_1.html?data=kpWimpydfJShhpywj5aWaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5yncZKkkZDT18rSuMbnjMnSjcbLucKfxtjhh6iXaaKlz5DS0JDWrcbnyNSYx9OPkdDi1cqYpNTSrdXjhpewjaiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ariel Quintero, habitante Monte Bonito] 

###### [26 Feb 2016] 

La empresa Latinco, que **ya tiene las licencias otorgadas por la Corporación Autónoma Regional de Caldas, CORPOCALDAS**, pretende desarrollar un proyecto de Micro Hidroeléctricas que pondría en riesgo de secarse a 100 fuentes de agua, según denuncian los habitantes de la zona, que ya han visto como esos proyectos afectan a otras comunidades **como en el caso de [Bolivia, Caldas y la UT GC CHOC](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/).**

Ariel Quintero, habitante de Monte Bonito afirma que **la empresa LATINCO los ha dejado plantados en el proceso de socialización del proyecto**, en el que los pobladores piensan exponer sus reparos a este proyecto, el viernes 19 de Febrero, el representante de la empresa no se hizo presente. **Tampoco los ha atendido CORPOCALDAS** a quien le correspondió entregar la licencia sin ningún tipo de consulta a la comunidad.

Sobre esta situación, afirma el poblador que “Si uno quiere cortar una mata de guadua para hacer una casa le ponen mil problemas, pero a la empresa si le dan el permiso”, **el problema es que la empresa afirma que la comunidad puede hacer lo que quiera porque la empresa LATINCO ya tiene la licencia.**

Según lo que explica Quintero, la construcción de túneles para las hidroeléctricas, hace que el agua que nace en las montañas se profundice y las quebradas abandonan su cauce natural para correr por los túneles que luego la conducen a sitos diferentes, generando sequía para los campesinos y campesinas.

Quintero afirmó que en la interacción con las comunidades se han podido establecer el daño que provocan este tipo de proyectos y así han visto que, por ejemplo en San Daniel, la gente de manera pacífica, ha parado las operaciones de la empresa y ese es el camino que podría tomarse en Monte Bonito puesto que todas las demás puertas se les han cerrado.

Diversas empresas tienen  la vista puesta en el departamento y pretenden la construcción de **19 Micro Hidroeléctricas**. Sin embargo los impactos ambientales que podrían generar este tipo de intervenciones, podrían ser devastadores para la economía de una región que ha vivido de la agricultura y la ganadería, dependientes del agua.

[![caldas\_la\_miel\_proyectos2-page-001](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/caldas_la_miel_proyectos2-page-001.jpg){.aligncenter .wp-image-20759 width="544" height="288"}](https://archivo.contagioradio.com/100-fuentes-de-agua-estan-en-riesgo-en-monte-bonito-caldas/caldas_la_miel_proyectos2-page-001/)
