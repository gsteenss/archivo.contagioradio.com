Title: Parlamento de Alemania aprueba moción sobre la paz en Colombia
Date: 2016-07-15 14:10
Category: Nacional, Paz
Tags: Diálogos de paz en Colombia, parlamento alemán, proceso de paz Colombia
Slug: parlamento-aleman-expresa-su-apoyo-al-proceso-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Parlamento-Aleman.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Arsenal ] 

###### [15 Julio 2016 ]

En Alemania fue creada una comisión parlamentaria que plantea una moción en apoyo al proceso de paz en Colombia y que busca hacer recomendaciones al Gobierno federal. De acuerdo con Carlos Hainsfurth, el principal llamado que hacen está dirigido a autoridades nacionales regionales, locales, así como sectores económicos y políticos para que pongan en marcha **acciones efectivas de lucha contra el paramilitarismo**, que incluyan el compromiso de quienes lo han apoyado y financiado, para que se desmonte y puedan implementarse los acuerdos de paz.

Otra de las recomendaciones que emiten es que se tenga en cuenta la responsabilidad del Estado colombiano, en cabeza del Ejército Nacional en violaciones a los derechos humanos de la población civil. Así mismo insisten en la necesidad de que sean reparadas todas las víctimas del conflicto armado y que el **fomento económico del Gobierno alemán cumpla con los estándares de respeto a los derechos humanos**. La Comisión también recomienda al Gobierno apoyar de forma crítica la implementación de la Justicia Especial para la Paz.

El grupo de parlamentarios insiste en que tanto el Gobierno alemán como el colombiano deben **apoyar a las organizaciones sociales de base para fortalecer su participación** en la concreción de una paz estable y duradera, a través de la financiación de propuestas como la construcción de una [[universidad para la paz](https://archivo.contagioradio.com/en-el-naya-se-inaugura-la-primera-sede-de-la-universidad-de-la-paz/)].[ ]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
