Title: Jorge Quintero, líder social de Risaralda fue asesinado
Date: 2020-09-29 18:16
Author: AdminContagio
Category: Nacional
Tags: Dosquebradas, Líder Social Asesinado, risaralda
Slug: jorge-quintero-lider-social-de-risaralda-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Líderes-y-excombatientes-asesinados.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La noche de este lunes 28 de septiembre se informó que fue asesinado Jorge Luis Quintero Villada, líder social y presidente de la Junta de Acción Comunal de la vereda Sabanitas del municipio de Dosquebradas, departamento de Risaralda. Jorge Luis también trabajaba como guardabosques y era contratista de la Alcaldía. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información, el líder iba en su vehículo de regreso a casa y cuando fue dos individuos le dispararon repetidas veces con arma de fuego; de inmediato lo trasladaron al Hospital Santa Mónica donde finalmente, falleció. (Le puede interesar: [ONU extiende Misión de Verificación del Acuerdo de Paz en Colombia](https://archivo.contagioradio.com/onu-extiende-mision-de-verificacion-del-acuerdo-de-paz-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho fue confirmado por  Eisenhower Zapata, miembro de la Mesa Nacional de Víctimas quien hace un llamado **“al alcalde del municipio y al gobernador de Risaralda para mirar cuál es la estrategia real que se tiene con todos los líderes amenazados"**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/pereiraenvivo/status/1310789259192827906","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/pereiraenvivo/status/1310789259192827906

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según las autoridades, Jorge Quintero no tenía amenazas. Sin embargo, **había denunciado anteriormente, la destrucción de zonas de protección ambiental por lo que se investiga si esto tiene relación con su asesinato.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace unos días, la Misión de Observación Electoral, señaló que en los primeros 6 meses de este 2020, se ha incrementado la violencia contra líderes sociales y comunales con ocho casos registrados en el departamento de Risaraldas. ([Le puede interesar: Asesinatos contra líderes sociales se incrementaron un 85%: MOE](https://archivo.contagioradio.com/asesinatos-contra-lideres-sociales-se-incrementaron-un-85-moe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, el hecho se da pese a que la Defensoría del Pueblo había emitido este año dos alertas tempranas sobre el riesgo que corrían los líderes sociales en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, según el [Instituto de estudios para el desarrollo y la paz](http://www.indepaz.org.co/) - Indepaz- ya son 216 los líderes sociales y defensores de derechos humanos asesinados en lo que va del año en todo el país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
