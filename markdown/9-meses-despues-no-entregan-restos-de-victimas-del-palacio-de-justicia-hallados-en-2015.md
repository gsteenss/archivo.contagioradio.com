Title: No entregan restos de víctimas del Palacio de Justicia hallados en 2015
Date: 2016-07-08 11:59
Category: Judicial, Nacional
Tags: Entrega restos víctimas Palacio Justicia, Retoma Palacio de Justicia, Victimas palacio de justicia
Slug: 9-meses-despues-no-entregan-restos-de-victimas-del-palacio-de-justicia-hallados-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [8 jul 2016] 

El pasado 20 de octubre de 2015, los familiares de Luz Mary Portela, Cristina del Pilar Guarín y Lucy Amparo Oviedo, víctimas de la toma y retoma del Palacio de Justicia, recibieron la noticia de que les serían entregados los restos mortales de sus seres queridos; sin embargo, hoy, nueve meses después la **Fiscalía asegura que no puede entregar los restos sin recibir un informe de Medicina Legal**, mientras que la entidad asevera que la demora ha sido responsabilidad del ente acusador.

Rene Guarín asegura que en vista de que pasaban los meses y no se realizaba la entrega, se comunicó con Jairo Vivas, director científico de Medicina Legal, quien el 29 de febrero de este año respondió que **por parte del Instituto ya estaba todo resuelto y que la fecha dependía de la Fiscalía**. [[El tiempo siguió pasando y no hubo respuesta concreta](https://archivo.contagioradio.com/fiscalia-no-ha-entregado-los-restos-hallados-de-desaparecidos-del-palacio-de-justicia/)], así que el pasado mes de abril los familiares enviaron un derecho de petición a la Fiscalía que fue contestado hasta este miércoles y en el que aseguraron que Medicina Legal era quién debía establecer la fecha.

"Nos tienen en un peloteo entre una entidad y otra, y lo curioso es que nosotros pedimos que sea retirado de cualquier diligencia judicial, relacionada con entrega de restos, el funcionario Miguel Ángel Mora Clavijo, porque ha revictimizado fundamentalmente a la familia de Luz Mary Portela, **diciéndole a Milena que reciba los nueve huesos; que nueve huesos no dan mayor verdad ni mentira sobre lo que ocurrió con su hermana**"; sin embargo, es él quien responde el derecho de petición enviado a la Fiscalía, agrega Guarín.

Este viernes los familiares enviaron una nueva comunicación a las dos entidades para que se esclarezca de quién es la responsabilidad en la entrega de restos, pues este hecho es una nueva revictimización y las familias se preguntan qué intereses hay detrás de esta demora, pues la falta de coordinación institucional es evidente, "es una grosería que el Estado colombiano 31 años después, y después de la muerte de nuestros papás y mamás que eran quienes en primera instancia debían recibir los restos, nos encontremos con que **le está haciendo conejo a la sentencia de la Corte Interamericana de Derechos Humanos sobre el caso del Palacio de Justicia**", concluye Guarín.

<iframe src="http://co.ivoox.com/es/player_ej_12162697_2_1.html?data=kpeemJeafZihhpywj5WdaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbPZz4qwlYqlfYy71sbfh6iXaaK4z4qfpZCqpc7dzc7O1MrXb7HVzcbQy9SPqMafy9rg1s7HrcKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 
