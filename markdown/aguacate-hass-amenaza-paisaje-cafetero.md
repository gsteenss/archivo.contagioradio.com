Title: Aguacate hass ¿una amenaza para el paisaje cafetero?
Date: 2019-02-05 17:29
Author: AdminContagio
Category: Voces de la Tierra
Tags: aguacate hass, Monocultivos, Quindío
Slug: aguacate-hass-amenaza-paisaje-cafetero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La perla órganicos 

###### 31 Ene 2019 

En Estados Unidos y Europa **viene creciendo la demanda de aguacate Hass**, un fruto rico en grasas vegetales, que se da en países como México, Chile, Perú o Colombia donde se le conoce también como palta. Sin embargo, **tras la popularidad del aguacate pocos conocen el fuerte impacto ambiental que puede ocasionar**.

En este programa de **Voces de la tierra**, nos acompañó **Luis Carlos Serna**. Biólogo, docente y y representante de los ambientalistas ante la Car del Quindío, departamento donde el monocultivo de Hass viene amenazando con destruir el paisaje cafetero y su ecosistema, fenómeno que se estaría replicando en lugares como Caldas y el Norte del Valle.

<iframe id="audio_32296668" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32296668_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
