Title: Una píldora para la memoria: Diego Felipe Becerra
Date: 2015-04-22 11:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: “Trípido”, Diego Felipe Becerra, falso positivo, Gustavo Trejos, Hell Caps, Liliana Lizarazo, Policía Nacional
Slug: una-pildora-para-la-memoria-diego-felipe-becerra
Status: published

###### Foto: eltiempo.com 

El colectivo Hell Caps dedica su capítulo “Justicia” al joven grafitero.

La noche del 19 de Agosto de 2011 fue asesinado **Diego Felipe Becerra Lizarazo** **“Trípido”** mientras realizaba un grafiti en la calle 116 con Avenida Boyacá en el norte de Bogotá, un caso considerado por la fiscalía como un **“falso positivo urbano”** de la **Policía Nacional.**

La lucha de **Gustavo Trejos y Liliana Lizarazo** por limpiar la memoria de su hijo comenzó el mismo día de los hechos en los que, de acuerdo a las investigaciones realizadas hasta la fecha, existió manipulación de la escena del crimen por parte de los oficiales, algunos de alto rango, involucrados en el caso.

Son ellos quienes en su propia voz cuentan su versión de la historia a través del audiovisual producido por el colectivo **Hell Caps** y como es la realidad de vivir con la ausencia de su hijo y la necesidad de reivindicar, contra la corriente, la imagen de Diego Felipe, son los muros que hablan y claman por justicia.

“Cuando una esposa pierde a su ser amado, se le llama VIUDA, cuando un hijo pierde a sus padres, se le llama HUÉRFANO, pero cuando unos padres pierden a un hijo, eso no tiene nombre”.

<iframe src="https://www.youtube.com/embed/V6g5tjbfT30" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
