Title: El fantasma en el espejo
Date: 2017-06-21 14:54
Category: invitado, Opinion
Tags: feminismo, mujeres
Slug: el-fantasma-en-el-espejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/espejo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Malena en el espejo 

###### 21 Jun 2017 

#### [**Por: Ángela Galvis Ardila.**] 

No se siente así, no se vista de esa forma, no desee, no sienta, no diga, no sea, y hasta un no se mire tanto al espejo que se le puede aparecer un fantasma, fueron advertencias que escuché de niña, entre muchas otras, sin saber que eran solo el abrebocas de un universo de prohibiciones e imposiciones que me esperaban en la vida y que me irían calando tanto al punto que serían mi código invisible del deber ser, excepto la del espejo, esa me causaba risa y hasta rebeldía, pues no pocas veces esperé, aunque con mucho miedo con la cobardía que me ha caracterizado, que apareciera el fantasma.

Era tanto el tiempo que pasaba en mi infancia frente a un espejo que fue uno de mis hermanos mayores quien, creo que por agotamiento, me lanzó dicha sentencia paranormal, la que a pesar de ser tan contundente no fue suficiente para que dejara de hacerlo.  Recuerdo ver en cada espejo en el que tenía la oportunidad de mirarme, a una niña pequeña, delgada, de ojos muy grandes y expresivos, bailando, cantando, sintiéndose en el más enorme escenario lleno de espectadores que la ovacionaban por ser una verdadera estrella, pero que luego volvía a la realidad con el corazón entristecido, porque si tiene puntualidad la vida es en enseñarnos que si hay algo más difícil que volar es aterrizar.

Los espejos se fueron convirtiendo con el tiempo en compañeros cada vez más lejanos pero no porque no me gustara lo que veía, no, tampoco porque tuviera el síndrome de ese pececillo que ve en su propio reflejo a un rival. Simple y llanamente porque las hormonas se fueron calmando y hacerlo me hacía sentir no en un escenario iluminado sino en uno en el que no me sentía cómoda; si no estoy equivocada en el concepto, creo que eso se llama pudor, pero no ese pudor complaciente con los prejuicios de los demás sino uno menos hipócrita y más genuino: el del pudor de no hacer el ridículo frente a uno mismo.

Ahora cuando me miro al espejo ya no imagino a la artista que nunca fui sino que, aunque la vanidad se resienta,  me detengo a ver cómo mi mirada ha cambiado, cómo mi piel va perdiendo firmeza, cómo mi expresión se endurece y también cómo reconozco en mis gestos el asomo de algunos gestos que una vez vi en la cara de mi madre. Y no, aunque parezca no estoy renegando de haberme hecho adulta, por el contrario, siento algo parecido a la tranquilidad cuando me reconozco en esa mujer en la que los años me fueron convirtiendo, no sé si mejor o peor que antes, solo sé que distinta, como también sé que un día, más rápido de lo que puedo creer, me levantaré y al mirarme al espejo veré a una mujer tan lejana a esa niña menuda que pasaba horas y horas frente al espejo y la advertencia de mi hermano que me causaba risa se habrá hecho realidad: veré a un fantasma.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
