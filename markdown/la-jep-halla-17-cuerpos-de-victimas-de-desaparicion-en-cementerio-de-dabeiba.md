Title: La JEP halla 17 cuerpos de víctimas de desaparición en cementerio de Dabeiba
Date: 2020-11-18 18:21
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Antioquia, Cementerio de Dabeiba, JEP, Víctimas de desaparición forzada
Slug: la-jep-halla-17-cuerpos-de-victimas-de-desaparicion-en-cementerio-de-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cementerio-de-Dabeiba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @JEP\_Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes 17 de noviembre la[Jurisdicción Especial para la Paz (JEP) anunció](https://twitter.com/JEP_Colombia/status/1328846320048730112/photo/1) que se hallaron 17 nuevos cuerpos de posibles víctimas de desaparición en Dabeiba, Antioquia, en la tercera jornada de exhumación llevada a cabo en el cementerio de Las Mercedes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Unidad de Investigación y Acusación (UIA) de la JEP reportó estos 17 hallazgos forenses tras la ubicación de seis fosas comunes en el cementerio. (Le puede interesar: [UBPD recuperó 24 cuerpos en acción humanitaria en Caldas](https://archivo.contagioradio.com/ubpd-recupero-24-cuerpos-en-accion-humanitaria-en-caldas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según lo señaló la JEP, los cuerpos fueron hallados **dentro de bolsas negras, desnudos, con heridas de arma de fuego en la cabeza, totalmente fragmentados, unos inhumados con prendas militares y otros amarrados en las manos, pies y el cuello, en estado de total indefensión.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras esta jornada también se encontraron indicios que indicaban la realización de necropsias de forma previa. De acuerdo con la JEP, no es común esta exploración en personas enterradas ilegalmente, «como tampoco es usual la disposición de los cuerpos en los lugares en que fueron hallados, que no estuvieran en ataúdes, encontrados sin ropa, boca abajo, o dentro de bolsas negras**»**. (Le puede interesar: [Uniformados en la JEP coinciden en que habrían más víctimas de desaparición en Dabeiba](https://archivo.contagioradio.com/uniformados-en-la-jep-coinciden-en-que-habrian-mas-victimas-de-desaparicion-en-dabeiba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con estos nuevos hallazgos, la Jurisdicción reporta 71 hallazgos forenses en el cementerio desde el 2019 de personas inhumadas de forma ilegal y que corresponderían a víctimas de ejecuciones extrajudiciales y desapariciones forzadas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La JEP adelanta labores de toma de pruebas de ADN
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además de las jornadas de exhumación, 150 personas, incluidos indígenas de la comunidad embera, participaron de la segunda jornada de toma de muestras de ADN que se realizó entre el 8 y el 14 de noviembre. Número de familiares que superó a las 121 personas que se habían convocado inicialmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, hasta la fecha, la JEP ha hecho entrega a familiares de los restos de 5 personas víctimas de desaparición forzada o ejecuciones extrajudiciales. La primera entrega se realizó el 17 de febrero de 2020 con la entrega de los restos de Edison Lexánder Lezcano Hurtado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el pasado 10 de noviembre fueron entregados a sus familiares los cuerpos de Yulieth Andrea Tuberquia, Nelson Antonio Góez Manco, Alveiro Úsuga Uribe y Eliécer de Jesús Manco Úsuga. (Le puede interesar: [JEP hace entrega de restos de personas desaparecidas en Dabeiba](https://archivo.contagioradio.com/jep-hace-entrega-restos-de-personas-desaparecidas-en-dabeiba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas[ jornadas](https://twitter.com/JEP_Colombia/status/1321442628815200257) se están llevando a cabo después de conocer las versiones coincidentes rendidas por 14 miembros de la fuerza pública y en el marco de las medidas cautelares que buscan proteger a las víctimas de desaparición forzada.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
