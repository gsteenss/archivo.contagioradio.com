Title: Ex-integrantes del M-19 llamados a declarar por Palacio de Justicia
Date: 2015-11-09 13:26
Category: DDHH, Nacional
Tags: 30 años palacio de justica, Everth Bustamante, Ex ministra de comunicaciones Noemí Sanín, ex ministro de Gobierno Jaime Castro, exministro de Justicia Enrique Parejo, expresidente Belisario Betancur, Otty Patiño, Palacio de Justicia, Vera Grabe
Slug: tres-ex-integrantes-del-m-19-llamados-a-declarar-por-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/m-19-a-declaracion-palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### [9 nov 2015] 

Luego del anuncio realizado por la Fiscalía General de la Nación sobre el llamado a brindar testimonio a los ex-ministros del gobierno de **Belisario Betancur, Noemí Sanín, Enrique Parejo y Jaime Castro**, la entidad solicitó la presencia en condición de declarantes de 3 ex-integrantes del Movimiento M-19, por los hechos del Palacio de Justicia.

Se trata de **Vera Grabe, Otti Patiño, y Everth Bustamante**, quienes hacian parte del grupo insurgente que llevó a cabo la operación "Antonio Nariño por los derechos del hombre" ejecutada los días 6 y 7 de noviembre de 1985, de la que se cumplieron 30 años el pasado fin de semana.

A través de su cuenta en Twitter el ente acusador manifestó que "Por [[\#]{.PrettyLink-prefix}[PalacioDeJusticia]{.PrettyLink-value}](https://twitter.com/hashtag/PalacioDeJusticia?src=hash){.PrettyLink .hashtag .customisable} se adelantará línea investigación que apunta no solo a militares que estuvieron en retoma sino a altos mandos del M-19" señalando su intención de "profundizar en eventuales responsabilidades históricas" de los actores involucrados durante los acontecimientos.

Los exmilitantes se presentarán para ser escuchados, inicialmente no podrían ser investigados al respecto, por los indultos recibidos durante los procesos de paz mediante los cuales el M-19 dejó las armas y se convirtió en movimiento político. Sin embargo, la Fìscalía anunció que abrirá una nueva linea de investigación para revisar las amnistías e indultos otorgados a los desmovilizados, con lo que se contemplaría la posibilidad de apelarlos.

**Exministros a declarar**

El primero en presentarse será el exministro de Justicia Enrique Parejo, citado para el próximo 26 de noviembre , seguido por la exministra de comunicaciones Noemí Sanín el 1ro de diciembre, y por último el exministro de Gobierno Jaime Castro el día 3 del mismo mes.

La Fiscalía escuchará a los exintegrantes del gabinete ministerial, para así establecer su responsabilidad por sus acciones u omisiones, en las decisiones tomadas en consejo de ministros, aclarando que no se trata de un proceso de investigación o interrogatorio por parte de la entidad. (Lea tambien [Familiares exigen se rompa "pacto de silencio" y se conozca la verdad](https://archivo.contagioradio.com/familiares-exigen-que-se-rompa-el-pacto-de-silencio-y-se-conozca-la-verdad/)).
