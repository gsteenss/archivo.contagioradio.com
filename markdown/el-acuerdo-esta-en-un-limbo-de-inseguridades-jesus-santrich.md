Title: "El Acuerdo está en un limbo de inseguridades" Jesús Santrich
Date: 2017-11-17 12:43
Category: Nacional, Paz
Tags: Congreso de la República, FARC, Implementación de acuerdos de paz, JEP, Ley Estatutarua de JEP
Slug: el-acuerdo-esta-en-un-limbo-de-inseguridades-jesus-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Laprensa.hn] 

###### [17 Nov 2017] 

Tras las decisiones que se han venido tomando el Congreso de la República y la Corte Constitucional con respecto al trámite para la implementación del Acuerdo de paz y la Jurisdicción Especial de Paz, el partido político FARC indicó que **lo que se ha aprobado no es “ni la sombra”** de lo que se acordó en la Habana. Ahora, la JEP pasa a manos de los representantes a la Cámara donde su presidente, Rodrigo Lara, dijo que votarán artículo por artículo.

<iframe src="https://co.ivoox.com/es/player_ek_22142383_2_1.html?data=k5eelpeXfJShhpywj5aVaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncavZ1IqwlYqmhdSftMbb1tfNp8mf1NTP1MqPsMLnjNLcxs7KrcTVxM7c0MrXb8KfzcaYrKq0b8bijMrZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Jesús Santrich, integrante del partido político FARC, “el acuerdo de la Habana **está en una especie de limbo de inseguridades** que van desde lo jurídico hasta lo personal”. Por esto, dijo que la ruta que se está tomando en el país no es la adecuada por lo que le han pedido al Gobierno Nacional que “dé una vuelta de timón y ponga el proceso de implementación conforme a lo que se pactó en la Habana”.

Santrich manifestó también que, el presidente de la República **debió haber tomado las riendas desde el principio** “sin repartir tanto la posibilidad de decisiones en el conjunto de la constitucional”. Recordó que el Gobierno Nacional negoció en la Habana a nombre del Estado y por lo tanto el jefe de Estado es quien debe generar las condiciones para que se cumpla lo que se pactó. (Le puede interesar:["Las preocupaciones que deja el fallo de la Corte sobre la Jurisdicción Especial de Paz"](https://archivo.contagioradio.com/49091/))

### **Lo que ocurrió con la Corte y en el Congreso “da vergüenza”** 

En reiteradas ocasiones los miembros de ese partido han argumentado que **hay una actitud de chantaje en estas instituciones** “para sacar prebendas particulares”. Además, frente a la decisión de la Corte Constitucional, que introdujo cambios a para la aprobación de la ley estatutaria, Santrich manifestó que va en contra de sus previas decisiones porque “no respeta” el espíritu de los acuerdos sobre el cual ya había fallado.

Ahora, con respecto a las garantías de la reincorporación de los integrantes de la desarmada guerrilla de las FARC, afirmó que se encuentra enmarcado en la **crisis que está atravesando la implementación** de los acuerdos. Dijo que la reincorporación “ha sido saboteada y aún quedan más de mil presos políticos en las cárceles de Colombia”.

### **Tramite en Cámara de Representantes podrá tomar más tiempo que tomó en Senado** 

Ahora que el Senado hizo la aprobación de la ley estatuaria de la JEP, **es momento de que la Cámara de Representantes haga lo mismo**. Sin embargo, ya ha habido declaraciones del presidente de la misma, Rodrigo Lara, donde ha advertido que allí debatirán artículo por artículo, lo que demoraría aún más el trámite y el tiempo del Fast Track está por acabar. (Le puede interesar: ["Víctimas interponen tutela contra el Congreso para implementar el Acuerdo de Paz"](https://archivo.contagioradio.com/victimas-y-organizaciones-interpusieron-tutela-para-que-congreso-implemente-acuerdo-de-paz/))

<iframe src="https://co.ivoox.com/es/player_ek_22142434_2_1.html?data=k5eelpeYd5Whhpywj5WUaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpDg0cfWqYzZzZDh1Iqnd4a1ktLW1sqPtdbZjNHSjdbZqcXVjMrbjaiJh5SZopbaw9fFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según el representante por Bogotá, Alirio Uribe, **“el trámite en la Cámara puede ser más lento de lo que fue en el Senado**, si no se ordena el debate, podemos terminar como con la reforma política que nos demoramos más de un mes cuando tenía solo 21 artículos y la JEP tiene 163”.

Ante este panorama, lo que ha sugerido el representante es que **“se aplique una metodología que permita tomar decisiones más rápido**, pero eso va a depender del volumen de proposiciones que tengamos”. Además, afirmó también crearán proposiciones para hacerle frente a las modificaciones que se hicieron en el Senado como, por ejemplo, las inhabilidades a los magistrados de la JEP.

Finalmente, Uribe indicó que la ponencia **será radicada el día lunes y se anunciarán los debates para el martes**. Dijo que en este momento debe haber voluntad política de los representantes a la cámara para respetar los tiempos y así poder sacar todos los proyectos y conciliaciones que aún tiene pendiente el legislativo y que están relacionados con la implementación de los Acuerdos de Paz.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
