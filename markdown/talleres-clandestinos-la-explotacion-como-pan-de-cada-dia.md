Title: Talleres clandestinos: La explotación como pan de cada día
Date: 2015-05-11 10:54
Author: CtgAdm
Category: Capote, Opinion
Tags: Argentina, Flores, Maquilas, textil clandestino
Slug: talleres-clandestinos-la-explotacion-como-pan-de-cada-dia
Status: published

###### Foto tomada de agenciapacourondo 

[Laura Capote](https://archivo.contagioradio.com/laura-capote/) - [~~@~~lauracapout](https://twitter.com/lauracapout){.twitter-atreply .pretty-link}

Hace dos semanas, en el barrio porteño de Flores, un **taller textil clandestino** que funcionaba en la calle Páez tuvo un incendio en el que dos menores fallecieron.

En la ciudad de Buenos Aires funcionan centenares de talleres textiles donde se producen las prendas de las marcas mas reconocidas de la moda local e inclusive de algunas marcas multinacionales que pueden encontrarse fácilmente en cualquier Shopping de la ciudad. Dichos talleres funcionan en la clandestinidad, ya que las condiciones de funcionamiento son totalmente ilegales y atentan contra la dignidad y los derechos básicos de los y las trabajadoras: Locales que tienen licencia para cuatro o cinco máquinas, que albergan en realidad mas de treinta o cuarenta generando condiciones de hacinamiento, turnos laborales de mas de catorce o dieciséis horas, condiciones de precarización laboral extrema, entre otros hacen parte de los problemas por los que atraviesan los trabajadores de estos talleres textiles.

En su mayoría migrantes sin procesos de sindicalización,  hombres y  mujeres se emplean en estos lugares que con el paso del tiempo, se van configurando como lugares de reunión y construcción de tejido social de las mismas comunidades migrantes: una misma cultura, gastronomía, música e inclusive lenguaje, van haciendo de estos espacios lugares en los que los y las trabajadoras se encuentran con otros que están en las mismas condiciones, convirtiéndolos en parte natural de su vida a pesar de todo lo que esto conlleva.

El tema medular de la discusión sobre las causas y consecuencias de dicho acontecimiento creo yo que está en pensar de qué manera el modo de producción en el que nos desarrollamos como sociedad está íntimamente ligado a nuestras formas de relacionamiento como hombres y mujeres, y a la forma de relacionarnos como clase con los dueños de dichos talleres y de las grandes marcas de la moda. Plantear la real y plena explotación laboral a la que son sometidos dichos trabajadores como una consecuencia directa de unas formas de vida donde, por ejemplo en el caso de los migrantes, los cuerpos se comercializan como cuerpos, como mano de obra, y no se relacionan como seres humanos.

Es necesario pensar de qué manera se ha instaurado en el sentido común argentino la criminalización de esos cuerpos, de las y los extranjeros latinoamericanos e inclusive de los migrantes internos, y se ha creado todo un imaginario de una masa de la sociedad que es justificable marginar del resto con argumentos racistas, xenófobos y por supuesto, clasistas.

El debate sobre si se deben cerrar o no dichos talleres, que se presentan por un lado como un escenario de explotación cotidiano, pero también como una fuente de trabajo para la población migrante, es, después de noticias como la del taller de Páez aun mas difícil de resolver. En el 2006 sucedió lo mismo en otro taller de la calle Luis Viale, ocasionando por un lado, un allanamiento masivo de distintos talleres que se cerraron por tales condiciones, y por otro, una traslación de esos talleres que funcionaban en Capital Federal a lugares fuera de la misma ubicados en Provincia, con las mismas condiciones de explotación intactas.

Entre la creciente derechización de la sociedad, por lo menos porteña (valga subrayar que este último taller incendiado confeccionaba ropa de la marca “Cheeky”, propiedad de Juliana Awada, esposa del jefe de gobierno de la ciudad Mauricio Macri, quien culpó del incendio a la “migración ilegal y la falta de trabajo”), y los sentimientos moralistas mesiánicos y lastimeros que sitúan siempre a los migrantes desde la condición de víctimas que necesitan salvación (reforzando los estereotipos de personas carentes de procesos racionales, menospreciadas y necesitadas de caridad), las y los trabajadores de los talleres textiles clandestinos siguen luchando por construir la posibilidad de acceder a trabajos dignos en los que la explotación no sea el pan de cada día, y en los que se los reconozca como sujetos activos en la constitución de su quehacer laboral y no como personas propensas a delinquir y a una vida de pobreza e inhumanidad.
