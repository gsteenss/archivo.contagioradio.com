Title: Una invitación a deponer los odios tras atentado en CC Andino
Date: 2017-06-19 13:27
Category: DDHH, Nacional
Tags: Bogotá, Centro Andino, Organizaciones sociales, paz
Slug: andinoatentadopaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/622d7fd6-87f4-4caa-a658-b41c183ae63a-1-600x450-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La FM 

###### 19 Jun 2017 

Un grupo de **organizaciones sociales y personas redactaron un comunicado** en solidaridad y condolencia por los hechos ocurridos en sábado 17 de Junio en el Centro Comercial Andino de Bogotá, en el que murieron 3 personas y 8 más resultaron heridas, **invitando a los colombiano a deponer los odios y abrazar la paz**.

En la comunicación, los firmantes calificaron lo ocurrido como "**un nuevo intento para entorpecer los irreversibles pasos dados en los últimos días hacia el final del conflicto** armado entre las FARC-EP y el estado Colombiano; así como los notorios avances en la mesa de negociaciones de paz entre el gobierno nacional y el ELN, en Quito, Ecuador". Le puede interesar: [Rechazan politización de explosión en Centro Comercial Andino](https://archivo.contagioradio.com/andinoexplosioneln/)

Dentro de las preocupaciones listadas en el documento, se menciona el que se haya puesto un explosivo en un sitio como el baño de mujeres lo cual "**evidenciaría la pretensión perversa de hacer daño** no solo a estas sino también y, eventualmente, a las niñas y niños que generalmente son acompañadas/os por sus madres a estos sitios" , particularmente tratándose de una fecha concurrida como la conmemoración del día del padre.

Sin embargo, los firmantes son enfáticos en reafirmar su disposición de **seguir participando activamente en los procesos de implementación de los acuerdos y en el trámite de la agenda de conversaciones con el ELN** en Quito. Adicionalmente, reiteraron el llamado a las autoridades para dar con los responsables, intelectuales y materiales del crimen.

[Pronunciamiento Atentado Andino](https://www.scribd.com/document/351725821/Pronunciamiento-Atentado-Andino#from_embed "View Pronunciamiento Atentado Andino on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19715" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/351725821/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-psqLwP89f3OqlA4gV2AZ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
