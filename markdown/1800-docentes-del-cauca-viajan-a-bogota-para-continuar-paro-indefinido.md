Title: 1800 Docentes del Cauca viajan a Bogotá para continuar Paro Indefinido
Date: 2016-08-23 13:46
Category: Educación, Nacional
Tags: Docentes del Cauca, educacion, Movilización, Paro Indefinido, protestas, Salud
Slug: 1800-docentes-del-cauca-viajan-a-bogota-para-continuar-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/asoinca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOINCA] 

###### [23 Ago 2016] 

Desde hace 3 semanas los **profesores y profesoras de ASOINCA en el departamento del Cauca**, se encuentran en paro indefinido debido al precario servicio de salud que los cobija. Además, en lugar de respuestas a sus exigencias han sido víctimas de la represión del ESMAD que deja, hasta el momento 35 personas heridas en diferentes movilizaciones.

Los maestros y maestras que decidieron escalar su movilización hasta Bogotá, llegarán con la propuesta de administrar los recursos de la salud desde un sector de ASOINCA, que actualmente tiene el intermediario **Magisalud 2**, esto según los docentes, para mejorar la calidad y atención de la red, y de esta forma se acaben las esperas de hasta 15 días para obtener una cita general o de 2 a 3 meses para tener una cita con especialistas.

El lugar al que llegarán más de 1.800 docentes, será las instalaciones de FECODE para exigir al comité ejecutivo de este sindicato que apruebe y respalde la propuesta para, posteriormente, llevarla al gobierno Nacional.

De acuerdo con Víctor Jiménez miembro de la **junta directiva departamental de ASOINCA**, "muchos han muerto a la espera de tratamientos como la quimioterapia". El último caso que se reportó fue el de una maestra que se encuentra en cuidados intensivos por un resultado equivocado del laboratorio de l**a prestadora del servicio de salud que la diagnostico con anemia y luego de un tratamiento inadecuado se estableció que padece leucemia**.

Esta situación, entre muchas otras, ha generado la suspensión de clases para más de 250 mil niños y niñas de todo el departamento, sin embargo tanto padres de familia como estudiantes apoyan las movilizaciones. Por otro lado, la organización reveló que en lo corrido del Paro Indefinido hay un saldo de 35 personas heridas por parte de la represión de Policía Nacional en cabeza del ESMAD.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
