Title: Día de la mujer en la ZRC "Perla Amazónica" en Putumayo
Date: 2015-03-11 00:40
Author: CtgAdm
Category: Comunidad, Mujer
Tags: 8 de marzo, Día de la mujer, mujer, zonas de reserva campesina
Slug: dia-de-la-mujer-en-la-zrc-perla-amazonica-en-putumayo
Status: published

##### Foto:PuertoLeguizamo.com 

#### **Comunicado ADISPA (Asociación de desarrollo integral sostenible Perla Amazónica):** 

**Mujeres**, campesinas, indígenas, afrodescendientes, profesionales, niñas, jóvenes, adultas, ancianas, todas ellas caracterizadas por ser luchadoras, perseverantes, responsables, inteligentes e identificadas por un sinnúmero de valores importantes en la construcción de un mundo justo, que crea condiciones de vida digna para la humanidad.

En el día internacional de la mujer, queremos destacar el papel de la mujer campesina, como abuela, madre, esposa, hija, tía, amiga, que ha liderado procesos en defensa del territorio, la vida y la biodiversidad, con el propósito de que todos esos seres de los que se ven rodeadas tengan **derecho a una vida digna**.

Resaltar, que la constitución de la **Zona de Reserva Campesina (ZRC) Perla Amazónica** fue liderada por un grupo de mujeres, recordando a María Silva, Elvia Silva y Graciela Viáfara, que sembraron esa semilla de amor por el territorio, la alimentación sana, la cultura, el rescate de las semillas nativas y custodia de las fuentes hídricas.

Semilla, que ha permitido, después de luchas, movilizaciones y trabajos organizativos, que la ZRC Perla Amazónica cuente con **lideresas que participan en el mejoramiento de la calidad de vida de sus familias** a través de sus propuestas sociales, económicas y ambientales construidas desde la realidad del campo y desde el pensamiento que sólo una mujer, como abuela, madre, esposa, hija, tía, amiga puede aportarle a la sociedad.

Queremos que su voz se siga escuchando, y conmemoramos este día por la reivindicación de sus derechos.  **Levantamos nuestra voz sobre toda muestra de violencia sobre ella, y recordamos a todas aquellas que han padecido maltrato, violación, asesinato, secuestro, desplazamiento**, porque su voz no se ha apagado, sigue viva en todas las mujeres que día a día luchan por un mundo mejor.

A TODAS LAS MUJERES DEL MUNDO, ESPECIALMENTE A TODAS AQUELLAS QUE HABITAN EN LA ZONA DE RESERVA CAMPESINA PERLA AMAZÓNICA……¡¡
