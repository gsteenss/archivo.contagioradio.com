Title: Águilas Negras amenazan a líderes y reclamantes de tierra en el Magdalena Medio
Date: 2020-10-02 18:48
Author: AdminContagio
Category: Actualidad
Tags: Águilas Negras, Panfletos, reclamantes de tierras
Slug: aguilas-negras-amenazan-a-lideres-y-reclamantes-de-tierra-en-el-magdalena-medio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Aguilas-Negras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Líderes reclamantes de tierras denunciaron haber **recibido este 29 de septiembre, un panfleto amenazante del bloque Magdalena Medio de las Águilas Negras,** en donde les daban 24 horas para salir del territorio antes de ser declarados como objetivo militar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En el panfleto se señala que el grupo no responderá si en las acciones violentas «*muere gente inocente*»;** al tiempo que estigmatizan a los líderes sociales, reseñándolos como «*guerrilleros*», «*vendedores de vicio*» y «*comunistas agazapados*». Adicionalmente, el grupo paramilitar advirtió con «*dar de baja*» a líderes sociales que según ellos «*ponen trabas al desarrollo de la región*». (Le puede interesar: [Los intereses tras las Águilas Negras en Colombia](https://archivo.contagioradio.com/los-intereses-tras-las-aguilas-negras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La región del **Magdalena Medio es uno de los territorios más afectados por el despojo de tierras** que trajo la violencia, el cual **fue aprovechado por muchas empresas y conglomerados económicos para adquirir predios a muy bajo precio,** en perjuicio de los campesinos propietarios y poseedores, que habitaban las tierras antes de ser desplazados forzosamente por los grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto la Fundación [Forjando Futuros](https://www.forjandofuturos.org/), creó una plataforma que permite evidenciar con base en las sentencias sobre restitución de tierras dictadas por los tribunales de justicia, **cuáles fueron las empresas realmente beneficiadas con la usurpación de predios; y particularmente previó un [acápite](https://www.forjandofuturos.org/wp-content/uploads/2020/09/Bolet%C3%ADn-BancosYDespojo-5-1.pdf) en el que evalúa el papel de los Bancos como auxiliadores del despojo de tierras «*a través de conductas sistemáticas y recurrentes*».** El Banco Agrario, el Banco BBVA y Davivienda son las entidades financieras que más figuran en este estudio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Más panfletos de Águilas Negras

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace apenas unas semanas, también se habían dado a conocer panfletos amenazantes también atribuidos al grupo paramilitar de las Águilas Negras en los que **se declaraba objetivo militar a no menos de 16 personas entre las que se encontraban líderes sociales, ediles y concejales del municipio de  Soacha, Cundinamarca.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos panfletos, firmados por el Bloque Capital de ese grupo anunciaba una «*limpieza en el territorio*» y se dirigían particularmente contra varios militantes del Movimiento Político Colombia Humana. (Le puede interesar: [Panfleto de Águilas Negras amenaza a integrantes de la Colombia Humana en Suacha](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-a-integrantes-de-la-colombia-humana-en-suacha/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
