Title: La Cumbre que no fue mundial, ni contra el terrorismo
Date: 2020-01-21 10:51
Author: CtgAdm
Category: Nacional, Política
Tags: Hezbolá, Juan Guaidó, Mike Pompeo, Venezuela
Slug: la-cumbre-que-no-fue-mundial-ni-contra-el-terrorismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cumbre-contra-el-Terrorismo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @infopresidencia {#foto-infopresidencia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de la llamada 'Cumbre Mundial contra el Terrorismo' que se realizó en la ciudad de Bogotá con la presencia de Juan Guaidó y el secretario de EE.UU. Mike Pompeo, han sido muchas las críticas contra el gobierno del presidente Duque. (Le puede interesar: ["¿Coordinaron Gobierno colombiano y Rastrojos ingreso de Juan Guaidó a la frontera?"](https://archivo.contagioradio.com/coordinaron-gobierno-colombiano-y-rastrojos-ingreso-de-juan-guaido-a-la-frontera/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de ellas, la hace el analísta político internacional Victor de Currea Lugo, que afirma que este encuentro no fue una cumbre, no fue mundial y tampoco contra el terrorismo. En primer lugar, porque no fue una cumbre mundial, y en segundo, porque no hay una definición consensuada a nivel internacional de lo que significa el 'terrorismo'.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un ejemplo de ello es el doble racero con el que se miden las acciones violentas: Mientras que se señala al gobierno de Maduro como una dictadura violadora de los DD.HH., se guarda silencio frente al terrorismo que ejerce el Estado y las graves acciones de grupos paramilitares como Los Rastrojos, que gestionaron el ingreso ilegal de Guaidó a Colombia en los primeros meses de 2019.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **No importa si lo que se dice es la verdad, importa cómo se dice**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otra de las conclusiones señaladas por el analista es que la afirmación acerca de que Hezbolá es una amenaza global, incluso en América Latina, no corresponde con una realidad política internacional dado que en el Libano, Hezbolá es un partido legal como lo es en Colombia el Centro Democrático o el Partido Liberal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, frente a las declaraciones del autoproclamado Guaidó en contra del senador Gustavo Petro, señaló que dichas acusaciones son la repetición del discurso de la guerra contra el terror, y que carece de argumentos más allá de la estigmatización. A este respecto, Gustavo Petro aseguró que emprenderá acciones legales en contra de Guaidó, por injuria y calumnia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Dinámicas de frontera no deberían afectar salida negociada al conflicto con el ELN**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El analista señaló que lo que se dice en torno a las operaciones militares de la guerrilla del Ejército de Liberación Nacional (ELN) en territorio de frontera no se pueden juzgar como una dinámica de acción violenta internacional, sino que se deben analizar en contexto de frontera. (Le puede interesar: ["Delegación de paz del ELN rechaza captura de Juan Carlos Cuellar, Gestor de paz"](https://archivo.contagioradio.com/delegacion-de-paz-del-eln-rechaza-captura-de-juan-carlos-cuellar-gestor-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este sentido, explicó que las fronteras entre Colombia y Venezuela tienen la característica de la porosidad y de una población fluctuante por la falta de presencia del Estado, tanto Venezolano como Colombiano. Ello obliga a que los habitantes de estos territorios asuman una dinámica de sobreviviencia que no conoce de [fronteras](https://aler.org/redmigracion/entre-parceros-y-panas); lo cual a su vez, no imposibilita la búsqueda de una salida negociada al conflicto con el ELN.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46798595" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46798595_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
