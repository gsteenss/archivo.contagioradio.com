Title: Diego Rodríguez 5to defensor de DD.HH asesinado en Cauca
Date: 2017-04-28 13:33
Category: DDHH, Nacional
Tags: Asesinato a líderes, Cauca, Mercaderes
Slug: diego-rodriguez-5to-defensor-de-dd-hh-asesinado-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/diego.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [28 Abr 2017] 

Las comunidades pertenecientes a los Consejos Comunitarios y Organizaciones de la Cuenca Alta del Río Patía, CORPOAFRO y el Colectivo AFROPATIANIDAD, **rechazaron el asesinato de Diego Rodríguez**, representante legal del Consejo Comunitario Ganaplata Canei del Municipio de Mercaderes e integrante del Palenque La Torre.

El cuerpo del líder social fue encontrado el 27 de abril, en el municipio de Mercaderes, en Cauca, con heridas de arma blanca. Amarildo Correa, líder de la región, expresó que no tenían conocimiento de amenazas hacía Rodríguez, y que **el joven se dedicaba a las actividades relacionadas con el Consejo Mayor de Comunidades Negras del Cauca** y al a defensa de los derechos de las mujeres y el ambiente.

Sin embargo, este hecho es el quinto homicidio que se presenta en el departamento durante este año, razón por la cual, las comunidades le están exigiendo al presidente Santos que se adelanten las investigaciones pertinentes y que la justicia actúe de forma pronta y efectiva con todos los asesinatos, además, **le reclamaron al Estado la protección urgente de las y los líderes que actúan en los territorios, promoviendo la paz**.

De igual forma, Correa expresó que hay grupos armados que están llegando a las zonas dejadas por las FARC-EP y que serían los autores de estos hechos y que esperan que “**el gobierno central atienda los vacíos que ha dejado esta guerrilla**” para que no retorne el conflicto armado a sus territorios. Le puede interesar:["En el transcuros del 2017 han asesinado 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/)

Pese a la grave situación de derechos humanos, las comunidades afirmaron que continuaran con el “**firme compromiso por la defensa de la vida, el territorio y la herencia ancestral**” y enviaron un mensaje de solidaridad y apoyo hacia la familia de Diego Rodríguez. Le puede interesar: ["En Bajo Atrato paramilitares reclutan a jóvenes para control social"](https://archivo.contagioradio.com/paramilitares-con-armas-cortas-controlan-territorios-del-choco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
