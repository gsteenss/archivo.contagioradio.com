Title: Pegasus: el sofisticado espionaje de gobiernos contra defensores de DDHH y periodistas
Date: 2017-06-21 17:17
Category: DDHH, El mundo
Tags: Enrique Peña Nieto, espionaje, mexico
Slug: pegasus-espionaje-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/espionaje_cibernetico-pegasus.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: alto nivel] 

###### [21 Jun 2017] 

Un nuevo escándalo de espionaje ilegal de comunicaciones se destapó en México por cuenta de una investigación del New York Times, que revela que el gobierno de ese país estaría utilizando el programa de fabricación Israelí conocido como “ Pegasus ” para espiar las comunicaciones de periodistas y defensores de Derechos Humanos sin orden judicial.

### **¿Qué es Pegasus?** 

Pegasus es un programa diseñado por las fuerzas de seguridad de Israel que se vende a gobiernos en todo el mundo que estarían interesados en realizar labores de espionaje de equipos electrónicos. Ese sistema tiene la capacidad de copiar información, usar cámara y micrófono de un teléfono inteligente, y que además es manejado por una oficina que puede centralizar la información que se recopile de los aparatos infectados.

El programa, que no funciona automáticamente, envía mensajes personalizados que dan cuenta de actividades de seguimiento o escuchas telefónicas previas, puesto que los mensajes con el Link adjunto indican actividades que ya se han venido realizando o que estarían próximas a desarrollarse.

### **Ya había sospechas de espionaje por parte del gobierno de Peña Nieto** 

Según Alejandro Cerezo, integrante del Comité Cerezo, ya había sospechas de espionaje de comunicaciones contra defensores de Derechos Humanos, pero no se tenía la confirmación. Sin embargo con la investigación periodística se ha probado que el gobierno de Peña Nieto si desarrolla este tipo de actividades ilegales en contra de esos grupos de personas.

Cerezo relata que hay muchos defensores a los que les llegan mensajes que inyectarían el programa en los teléfonos. Directamente a su celular llegó un mensaje con un link adjunto que contiene el virus. Además los mensajes son personalizados porque en su caso concreto ofrecía los datos de sepelio de uno de sus familiares.

En el caso de Francisco Cerezo, su hermano y también defensor, llegó un mensaje con los datos de una reunión que sostendrían con una entidad del gobierno, indicando que en el link infectado podría encontrar los datos del lugar y la hora de la reunión. [Lea también: La cacería criminal de las "chuzadas" en el gobierno de Uribe](https://archivo.contagioradio.com/chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe/)

Aunque desde organizaciones de Derechos Humanos de ese país ya han presentado denuncias ante las autoridades, Cerezo asegura que todo está en la impunidad, y por tanto, no hay esperanza de justicia en los entes estatales. Incluso se han presentado denuncias a nivel internacional que, aunque han tenido un trámite interno, tampoco han logrado respuesta por parte del gobierno de Peña Nieto.

### **Pegasus en México sería el tercer caso de espionaje probado en América Latina** 

Ya se han comprobado casos de espionaje gubernamental en América Latina,. Uno de ellos es el caso del Departamento Administrativo de Seguridad, DAS, en Colombia, que durante varios años habría seguido y escuchado conversaciones telefónicas y contactos de varios defensores de Derechos Humanos o políticos de oposición, tanto al interior del país como en el exterior. [Lea también: Amnistía Internacional víctima de espionaje del gobierno británico](https://archivo.contagioradio.com/amnistia-internacional-es-otra-victima-de-espionaje/)

Esas labores de espionaje trascendieron las fronteras y llegaron al mismo México y Europa con la llamada “Operación Europa” que realizó seguimientos a defensores y defensoras en el viejo continente. Aunque toda esa información ya hace parte de diversos procesos judiciales, muchos de ellos están a punto de caducar por vencimiento de términos y no todos los responsables han sido enjuiciados o condenados, ya que hay indicios de que la orden de desarrollar esas actividades habría salido de la misma oficina de la presidencia.

Otro de los casos de los que hay conocimiento es el de Panamá. Se denuncia a Ricardo Martinelli, por la compra y uso equipos de espionaje contra defensores de Derechos Humanos. De hecho algunas versiones indican que también se habría espiado a empresarios que tenían la capacidad de competir por contratos estatales en sectores de la economía que pensaban adjudicarse a empresas cercanas al gobierno.

Martinelli fue denunciado por ese caso, junto a otras acusaciones por corrupción. Aunque la INTERPOL emitió una orden de captura internacional y la semana pasada fue detenido en Estados Unidos, aún se desconoce si tendrá que rendir cuentas ante la justicia por el espionaje ilegal. [Le puede interesar: Panamá pide extradición de Martinelli por caso de espionaje](https://archivo.contagioradio.com/panama-pidio-extradicion-de-ricardo-martinelli-por-delito-de-espionaje/)

<iframe id="audio_19400828" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19400828_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
