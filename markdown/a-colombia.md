Title: A Colombia
Date: 2019-08-07 15:22
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, naturaleza, paisajes
Slug: a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/banderacolombia_dt-3-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/colombia-desde-el-espacio-nasa-mapa-suramerica-tierra-123rf.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-05-at-10.24.13-AM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Foto: Canal Uno**

[Húmedo como el Chocó, como la Buenaventura se mantiene el aire que trata de respirar más allá de la puesta del sol. ]

[Llanos orientales inmensos que compiten con el firmamento del mar. Llanos orientales eternos, que atrapan todos los suspiros enamorados o resignados que vienen de la cordillera. ]

[Cordilleras repletas de agua, de silencio perfecto que naufraga en el aire fresco antes de la lluvia. ]

[Roca fuerte del macizo colombiano, vientos autoritarios nos recuerdan que es allí donde nacen las tres cordilleras, donde las montañas son celosas de su cielo, donde las montañas someten a los ríos.  ]

[Páramo taciturno pero lleno de vida, enseña que el silencio jamás correrá la suerte de la nada. Monumento al agua, tesoro de cristal calma la sed de nuestra historia. ]

[La trocha es una oportunidad para quemar motores, pero también para escuchar nuestros pasos. La trocha bombea la paciencia de ver crecer un cultivo por el que pedirán rebaja, aquellos que por los espejismos pagan. ]

[Trocha sin progreso, pero cargada de sueños, trocha que no permite ir a velocidades altas, pero permite pensar detenidamente. ]

[Ciudades alegres como el más pequeño de los corregimientos; allí por donde corre cemento, también corren todos los destinos. ]

[Ciudades gigantes que te convierten en un ente insospechado, te abrazan en el susurro que te evapora hasta ser una partícula insignificante con significado. ]

[Cielo de luces lejanas, alimentan los sueños de los enamorados, los deseos de religiosos, los planes de los revolucionarios. ]

[Cielo de luces artificiales, componen la estética de un degradé de oscuros tonos que permiten sonreír aún en medio de tantas toneladas de concreto. ]

[Caliente como el valle del río Magdalena, calor espeso al fondo de las cordilleras central y oriental. Nido del fósil negro, ventisca caliente que pone a galopar los cuerpos al ritmo de cervezas heladas que a duras penas calman la sed. ]

[Majestuoso país que se halla secuestrado por aquellos que no saben dónde terminan sus fincas sin escrituras.  ]

[Majestuoso país que merece una justicia limpia, como las aguas del Río Bogotá por Guacheneque. ]

[Majestuoso país que contiene los deseos de celebrar algo más que un triunfo deportivo, merece toda la pena y toda la gloria; merece una que otra vez, alguna palabra que murió atorada. ]

[Relatos entre estatuas que cada vez más pocos saben para qué fueron erigidas, convierten la verdad en el mito, y el mito en la mentira que los arrastra hacia convicciones de papel. En Vano tratan de luchar contra columnas de oro, en las que se ve el reflejo del olvido. ]

[Exuberante matiz de verde, ocre, terracota y azul alimentando la vista de cualquier colombiano que busca la amatista en el fondo de la cordillera occidental, por allá en Piedra de Moler. ]

[Tumaco y Yotojohoin se esfuerzan por ser la cara de Colombia, pero El Poblado y la Zona T brillan más fuerte aparentemente, brillan, aunque solo sea racismo frotado con brilla metal. ]

[Majestuoso país del refresco, de la arepa que matiza del blanco al amarillo, del sonido del viento masajeando guaduales, de la fuerza del viento que exhala eucalipto, chaparrales, ocobos y borracheros; de la fuerza de la lluvia que inunda el Casanare, de la palma que baila lujuriosa y llena de arena a la vista de los mares. ]

[Mares de todos los colores, playas con mares de todos los recuerdos. ]

[Majestuoso país que aguarda el misterio infinito de la dignidad, la razón perpetua del por qué no nos vamos, o de por qué quisiéramos regresar. No hay lamentos entre los que se van o los que se quedan. Son las circunstancias, no obstante, los que se van se van, y los que se quedan luchan. ]

[País de personas que deberían poder gobernar sin problema, jugando el juego infantil de las tres comidas y un trabajo digno. ]

[País del dolor reprimido, se gasta tres generaciones en extraer una sonrisa franca de un mal pasaje, pero dos segundos en recordar que un delirio consciente entre la bendición y el infortunio no nos quita nada, pero sí nos pone a pensar ¿por qué si tenemos tanta, pero tanta riqueza, hemos abandonado la estética?]

[Selva profunda que porta el virus de los bosques, un virus que produce la enfermedad de la esperanza, esa que pronto infestará a los corazones que ya no creen en nada. ]

[Tan solo con una mirada eliminamos la frontera entre un bosque ceibal y un manglar, entre un bosque seco y uno de tumbes, entre un bosque andino o uno de selva alta y de selva baja, ellos allí, calmos y sabios, siguen regalándonos el susurro. ]

[Apreciada Colombia, sabes que no tenemos estaciones, aquí pasamos en un día del verano al invierno, de la euforia a la soledad, de una victoria a un velorio; parimos sobre mausoleos, morimos sobre el lecho de esperanzas inagotables, nos levantamos todos los días a batallar con la realidad para luego cortejarla por las tardes. ]

[¿Resistencia? ¿lucha? No la regalan los años, no se compra en una esquina de baratijas plusvaliosas; la resistencia, se observa todos los días en Colombia, país de la infamia y la virtud, de lo real maravilloso y la tragicomedia, de pocos que son los dueños, y de muchos que sabemos lo que están haciendo. ]

##### [Leer más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/) 
