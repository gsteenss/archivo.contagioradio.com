Title: Juez exige cumplimiento inmediato de medidas de reparación a comunidades en Cacarica, Chocó
Date: 2019-06-19 16:10
Author: CtgAdm
Category: Comunidad, Judicial
Tags: cacarica, Chocó, CIDH
Slug: incidente-de-desacato-obliga-al-estado-a-cumplirle-a-las-comunidades-de-cavida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CACARICA-FESTIVAL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El juzgado noveno penal del circuito de conocimiento de Bogotá emitió un incidente de desacato interpuesto por la Comisión de Justicia y paz contra la Unidad de Restitución de Tierras, Fiscalía, Unidad de Víctimas y los Ministerios de Salud, Relaciones Exteriores, Interior, Salud y Defensa, quienes deben velar por la reparación integral a las **Comunidades de Autodeterminación, Vida y Dignidad del Cacarica (CAVIDA).**

Las entidades mencionadas en el radicado, fechado este 13 de Junio de 2019, tienen "un plazo máximo de dos meses que proceda a establecer un plan de trabajo para llevar a cabo las acciones concretas que conforme a las competencias y funciones de cada organismo, estén directamente encaminadas a dar cumplimiento a a las ordenes impartidas por la Corte IDH", el no cumplimiento de esta orden implicaría multas o arresto.

### ¿Qué dicen las comunidades de Cacarica?

Bernardo Vivas, uno de los líderes de CAVIDA indicó que más allá de ser el Estado, "pues el Estado somos todos", han sido las instituciones encabezadas por los gobiernos de turno los que no han cumplido porque **"por encima de todo está siempre el poder".**

Señaló que pese a que la Corte ordenó al Estado reconocer su culpabilidad en los sucesos de la Operación Génesis ocurrida en 1997 y restaurar a las personas afectadas además de brindarles garantías de salud, educación y protección, "desafortunadamente en nuestro país, las instituciones no responden a los fallos de instancias internacionales".

El líder social explicó que del total de reparación  no se ha avanzado más allá de un 3% indicando que para 2021, año en el que se cumplen los 10 años de la ley 1448 de 2011, - **la cual dicta medidas de atención, asistencia y reparación integral a las víctimas del conflicto armado -** aún quedará mucho por hacer y reparar, lo que dificulta que se cumpla con lo estipulado por la Corte IDH.

Como deber del Estado de indemnizar a los afectados por el conflicto, la abogada **Diana Marcela Muriel** indicó que En cuanto a la reparación colectiva, debido a la vigencia de la norma, la que caduca en 2021, "la desarticulación de oferta institucional así como del poco presupuesto con el que cuenta la Unidad de Víctimas se requiere la creación de un fondo específico que se puede implementar de forma colectivas".

Aunque la comunidad permanece escéptica, Bernardo consideró que este incidente de desacato "alimenta la esperanza de la comunidad que sigue a la espera" de una respuesta de las instituciones. [(Lea también: JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica)](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/)

### El proceso para llegar al incidente de desacato 

El 20 de noviembre de 2013 la Corte Interamericana de Derechos Humanos falló a favor de las personas víctimas de la Operación Génesis ocurrida en 1997 en territorios colectivos de comunidades negras del Cacarica, en especial a favor de los y las integrantes de las Comunidades de Autodeterminación Vida y Dignidad - CAVIDA.

En vista del incumplimiento por parte de las instituciones del Estado los peticionarios de CAVIDA, es decir, la Comisión de Justicia y Paz, presentó una acción de tutela por el incumplimiento al fallo de la Corte IDH, que fue  emitida por la Sala Penal del Tribunal Superior de Bogotá el 14 de diciembre de 2018. [(Le puede interesar: “Para nosotros la justicia debe ser restauradora" Comunidades de Cacarica)](https://archivo.contagioradio.com/asi-seria-condena-de-rito-alejo-del-rio-en-las-comunidades-victimas/)

Después de casi seis meses, las instituciones del Estado, solamente han cumplido un 3% de lo fallado que además no cumple los requisitos exigidos por la Corte IDH dado que no ha sido una reparación concertada, sino que se ha apegado a la ley 1448 y las indemnizaciones han estado muy por debajo del estándar,  razón por la que se interpuso el incidente de desacato que fue fallado este 13 de Junio.

<iframe id="audio_37332279" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37332279_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
