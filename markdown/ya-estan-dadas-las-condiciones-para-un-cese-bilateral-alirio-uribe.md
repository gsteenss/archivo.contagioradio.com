Title: Ya están dadas las condiciones para un Cese Bilateral: Alirio Uribe
Date: 2015-10-07 15:37
Category: Nacional, Paz
Tags: Asesinatos defensores DDHH, Cese Bilateral de Fuego, Cese unilateral de, conflicto armado en Colombia, Criminalización de la protesta, Desescalamiento del conflicto en Colombia, Detenciones arbitrarias en Colombia, Diálogos de paz en Colombia, diálogos en la Habana, FARC-EP, Radio de derechos humanos
Slug: ya-estan-dadas-las-condiciones-para-un-cese-bilateral-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Frente-Amplio-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: asociacionminga] 

###### [07 Oct 2015] 

[El Frente Amplio por la paz presentó esta mañana un cuarto informe sobre los resultados del cese unilateral por parte de las FARC. Alirio Uribe, Representante por el Polo Democrático e integrante del Frente Amplio por la paz, asegura que “Hemos estado pasando por los meses más pacíficos de la historia del país desde el año 64 hasta hoy” lo que muestra el **compromiso de esta guerrilla con la construcción de la paz**.]

[El Representante anota que la **suspensión de actividades militares de las FARC debería implicar reciprocidad por parte del gobierno nacional y de las Fuerzas Militares**, que debe traducirse en el desescalamiento de operaciones militares dirigidas a esta guerrilla, así como en la reivindicación de derechos para organizaciones sociales y defensores que han sido atacados, asesinados, amenazados y detenidos arbitrariamente.  ]

["Creemos que es importante el cese unilateral de hostilidades, creemos que ya se dan las condiciones para el cese bilateral, pero a la vez **nos preocupa que el ataque se dirija hacia otros sectores** sobre todo en temas como la criminalización de la protesta o en ataques que se producen en amenazas asesinatos y detenciones arbitrarias”, concluye. ]
