Title: ConocoPhillips suspende actividades para fracking en San Martín, Cesar
Date: 2016-09-22 12:31
Author: AdminContagio
Category: Ambiente, Resistencias
Tags: Ambiente, empresas petroleras, fracking, Movilización, san martin
Slug: conocophillips-suspende-actividades-para-fracking-en-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/SAN-MARTIN-e1474564357431.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilon 

###### [22 Sep 2016]

ConocoPhillips decidió detener las actividades exploratorias en el bloque VMM-3 ubicado en **San Martín, Aguachica y Rionegro,** además solicitó a Agencia Nacional de Hidrocarburos, ANH, suspender de manera temporal el contrato para hacer Fracking en esa zona del país.** **

La comunidad conoció el anuncio por medio de una respuesta de la ANH: “**Dada la conflictividad de tipo social** que se ha presentado en el área del bloque VMM-3 con las autoridades locales y las comunidades que hacen parte del área de influencia del proyecto, la compañía ConocoPhillips, como operadora del bloque, ha decidido para las actividades exploratorias y ha solicitado a la ANH con base en las cláusulas contractuales, **se declare la medida suspensiva sobre el contrato** con el fin de encontrar alternativas y dar tratamiento a la problemática que actualmente se presenta”, dice la respuesta.

“No aguantaron la presión que les generamos y tuvieron que salir. Claramente, van a replegarse y redefinir su estrategia, volverán en semanas o meses pero es una victoria que tenemos que ratificar este domingo en la marcha”, dice Carlos Andrés Santiago, vocero de Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC, quien agrega a que desde ya la maquinaria de la empresa se fue del lugar.

Desde hace semanas, la empresa  petrolera **intentaba ingresar a la fuerza al pozo Picoplata 1 para iniciar los trabajos de [exploración para Fracking  junto al Ejército y la Policía,](https://archivo.contagioradio.com/en-san-martin-conocophillips-intenta-realizar-fraking-en-compania-de-policia-y-ffmm/)** pese a la resistencia de los habitantes. Sin embargo, tras la intensa movilización de la comunidad acompañada por CORDATEC logró su cometido.

Por el momento, la comunidad se prepara para la tercera movilización en San Martín este 25 de septiembre, donde protestarán para que se declare la moratoria para el fracking no solo en San Martín, sino en todo el país.

**Las actividades de la jornada **

Con el **Foro Internacional "El Fracking en las Américas y Colombia: Impactos, luchas y resistencias"**, y la** 3ra Marcha Carnaval en defensa del Agua y de la Vida**, actividades a llevarse a cabo en San Martín, Cesar, se da cierre a la II Jornada Nacional frente al Fracking que además tiene actividades en Ibagué, Bogotá, Puerto Boyacá, Sogamoso, Barrancabermeja y Bucaramanga.

Con la presencia de **Hector Colio**, fundador de la Alianza Mexicana frente al Fracking, **Alberto Acosta**, Presidente de la Asamblea Constituyente de Ecuador (2008 – 2009) y académico de FLACSO, **Jorge Aguilar**, Director para la Región Sur y responsable de las campañas en el sureste de EE.UU de Food & Water Watch, y **Hernan Escandizo**, del Observatorio Petrolero del Sur - OPSur de Argentina y la Alianza Latinoamericana frente al Fracking, entre otros invitados, se desarrollará en San Martín el **FORO INTERNACIONAL** el día sábado 24 de septiembre a las 2:00 pm en el Polideportivo Carlos Toledo Franco.

[Así mismo, y con participación de delegaciones de diferentes municipios del Magdalena Medio y de algunas regiones de Colombia, se realizará el domingo 25 desde la 1:30 pm (saliendo desde Los Mangos, frente al Estadio), la **3ra MARCHA CARNAVAL** que busca superar las 6 mil personas que participaron en la 2da realizada el pasado 17 de abril en el municipio.]

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
