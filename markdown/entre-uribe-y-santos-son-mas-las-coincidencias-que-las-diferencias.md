Title: Entre Uribe y Santos "son más las coincidencias que las diferencias"
Date: 2015-04-30 16:30
Author: CtgAdm
Category: Entrevistas, Política
Tags: Alvaro Uribe, Jorge Enrique Robledo, Juan Manuel Santos, Nestor Humberto Martínez, Polo Democrático Alternativo
Slug: entre-uribe-y-santos-son-mas-las-coincidencias-que-las-diferencias
Status: published

##### Foto: eluniversal.com 

<iframe src="http://www.ivoox.com/player_ek_4431854_2_1.html?data=lZmgk52ZeI6ZmKiakpeJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9Hdz86SpZiJhpTijK%2Fc1MzJb7Pjw9HSxtSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jorge Robledo, senador del Polo Democrático Alternativo] 

El origen de Uribe y Santos es el mismo, ambos vienen de la tradición liberal – conservadora, son sus ancestros políticos, construyeron juntos el partido de la U, en el que militaron varios años juntos. Santos ocupó el cargo del Ministerio de Defensa del gobierno de Uribe y en los temas cruciales de la vida de país coinciden, ambos apoyan los TLC y otras políticas, incluso **la diferencia en el tema de la paz radica en cómo desarmar a las FARC, en todo lo demás coinciden** señala Jorge Robledo, senador del Polo Democrático Alternativo.

La reciente reunión entre **Nestor Humberto Martínez**, Super Ministro de la presidencia y el senador **Álvaro Uribe**, se realizó a puerta cerrada el pasado jueves 29 de Abril, lo único que se supo del contenido de la reunión fue lo que indicó Martínez a la salida de la oficina del Senador Uribe “es un patriota” afirmó y dijo que no hay ningún enemigo de la paz.

**Jorge Robledo señala que hay que esperar las consecuencias de dicho encuentro** pero afirma que de cualquier manera no es positivo para las fuerzas democráticas del país, puesto que la unidad de la derecha fortalece a todos sus actores, lo cual les permite actuar de manera más coordinada.
