Title: Misión de la ONU pide remover obstáculos de las instituciones a la JEP
Date: 2018-06-26 12:09
Category: Paz, Política
Tags: Comunicado de Prensa, JEP, Misión de Verificación, Naciones Unidas, ONU
Slug: mision-de-las-naciones-unidas-pidio-remover-obstaculos-de-las-instituciones-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/dsc_0161.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Misión de Verificación de la ONU en Colombia] 

###### [26 Jun 2018] 

A través de un comunicado, la **Misión de las Naciones Unidas** para el acompañamiento del proceso de paz y su implementación en Colombia pide que se remuevan los obstáculos para que las víctimas tengan acceso a la justicia, la verdad y la reparación contempladas en el Sistema Integral acordado entre el gobierno y las FARC en el acuerdo de paz de la Habana.

En la misiva la ONU **“hace un llamado a las instituciones del Estado competentes y a las fuerzas políticas para que remuevan los obstáculos que siguen impidiendo que el proceso de paz en Colombia cumpla su compromiso con la justicia y el derecho de las víctimas”**. Así mismo recuerda el apoyo que la comunidad internacional ha manifestado a la Jurisdicción de paz y el avance de la Unidad de Búsqueda de Personas Desaparecidas.

A continuación el comunicado completo.

![Captura de pantalla 2018-06-26 a la(s) 11.12.10 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-26-a-las-11.12.10-a.m.-489x609.png){.size-medium .wp-image-54230 .aligncenter width="489" height="609"}

###### [Foto: ONU] 

###### [Reciba toda la información de Contagio Radio en [[su correo] 
