Title: Nuevas capturas contra líderes sociales en el oriente del país
Date: 2018-10-01 20:11
Author: AdminContagio
Category: DDHH, Nacional
Tags: Arauca, Casanare, congreso de los pueblos, delito, lideres sociales
Slug: captura-lideres-sociales-oriente
Status: published

###### [Foto: @Daniel\_castroga] 

###### [1 Oct 2018] 

Organizaciones defensoras de DD.HH de Casanare, denunciaron la captura de dos líderes sociales en menos de 24 horas, se trata de **Hermes José Burgos y Alix Miriam Aguilar,** integrantes del Movimiento Político de Mesas Social y Popular del Centro Oriente de Colombia. Según denuncias del Movimiento, como de Congreso de los Pueblos, **estas capturas están encaminadas a "romper la capacidad organizativa y resquebrajar el tejido social de la población".**

### **Hermes José Burgos, capturado el 30 de septiembre** 

El Movimiento del Centro Oriente de Colombia denunció que Burgos fue capturado a las 2:30 de la tarde del domingo, **en el municipio de Arauquita, Arauca.** El líder fue capturado por una orden proveniente de la Fiscalía General de la Nación por los delitos de Concierto para Delinquir y Rebelión. (Le puede interesar:["Fiscal General niega impunidad en asesinatos de líderes sociales"](https://archivo.contagioradio.com/fiscal-general-niega-impunidad-en-asesinatos-a-lideres-sociales/))

Sin embargo, organizaciones sociales sostienen que Burgos es un notable líder social, **defensor de los derechos campesinos** y vocero, en repetidas ocasiones, de las comunidades en las mesas de interlocución con el Estado; además, es directivo de la Asociación Nacional Campesina José Antonio Galán Zorro (ASONALCA), miembro del Movimiento Político de Mesa Social y Popular del Centro Oriente de Colombia, y del Congreso de los Pueblos.

### **Alix Miriam Aguilar, capturada el 1 de octubre** 

De igual forma, el Movimiento denunció que este lunes en horas de la madrugada, fue capturada **Miriam Aguilar en Yopal, Casanare**. Miembros del Gaula de la Policía realizaron el allanamiento de la propiedad de la líderesa y posteriormente, procedieron a su captura; con está, sería la segunda vez desde 2015 que el hogar de la mujer es registrado sin encontrar material probatorio o ilegal.

Aguilar es una reconocida líder de la ciudad, es **presidenta del comité municipal de la Asociación de mujeres Unidas por Casanare,** y apoya acciones para impulsar los movimientos pro vivienda digna, e igualmente ha entablado acciones legales en defensa de los derechos de las clases populares. (Le puede interesar: ["El próximo 5 de octubre 'pasarán al tablero' autoridades que protegen líderes"](https://archivo.contagioradio.com/5-octubre-lideres-cepeda/))

### **Ser Líder Social no es un delito ** 

La organizaciones sociales reclaman **que no se estigmatice a quienes forman parte de movilizaciones sociales en exigencia de sus derechos,** señalandolos de pertenecer a grupos armados al margen de la ley o ser financiados por los mismos. También recordaron que en desde 2016, según cifras recogidas por el movimiento social, han sido asesinados 460 líderes sociales, y 60 más han sido judicializados.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
