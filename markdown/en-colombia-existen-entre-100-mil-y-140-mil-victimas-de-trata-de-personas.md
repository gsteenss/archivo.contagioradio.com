Title: En Colombia existen entre 100 mil y 140 mil víctimas de trata de personas
Date: 2015-07-30 16:22
Category: DDHH, El mundo
Tags: 30 de julio, colombia, Corporación Anne Frank, Día mundial contra la trata de personas, Naciones Unidas, Oficina de las Naciones Unidas Contra la Droga y el Delito, tráfico de personas, Trata de personas
Slug: en-colombia-existen-entre-100-mil-y-140-mil-victimas-de-trata-de-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/chains-19176_1280.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.aipadh.org]

<iframe src="http://www.ivoox.com/player_ek_5595208_2_1.html?data=lpqml5eUfI6ZmKiakpqJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiIa3lIqupsaPsdbixc7OzpDHs8%2Fo08aYzsaPuNPV1caYxsqPtMbm1NTbw9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Quintero, corporación Anne Frank] 

###### [30 de Julio 2015]

Con el fin de combatir el tráfico de personas en el 2010 la Asamblea General de las Naciones Unidas, adoptó un Plan de Acción contra la trata de personas, es por eso que hoy 30 de julio se conmemora esta la lucha que se viene realizando en contra de este flagelo que puede afectar a todo tipo de personas.

La **captación traslado y explotación** sexual, laboral, mendicidad ajena, matrimonio servil, entre otros, son las modalidades de trata de personas, aunque los más frecuentes son la trata con fines sexuales o laborales.

En Colombia, el Comité Interinstitucional para la lucha contra la trata de personas, recibe cada año al menos **60 casos** de personas que han sido víctimas de este delito, y aunque cualquier individuo puede ser víctima de un explotador o un captador, **una de cada tres víctimas del flagelo es menor de 18 años y dos de cada tres menores afectadas son niñas**, así mismo la gran mayoría de personas afectadas son mujeres, según la Oficina de las Naciones Unidas Contra la Droga y el Delito.

De acuerdo con Claudia Quintero de la Corporación Anne Frank, desde donde también se combate delito, **en Colombia no se ha hecho un sistema nacional de información de las víctimas,** sin embargo, desde las diversas organizaciones que manejan este tema se dice que existen entre **100 y 140 mil esclavizados en el país,** “una cifra alarmante, donde se reúnen todas las modalidades de la tarta de personas”, señala Claudia.

En conmemoración a este día, la Corporación Anne Frank presentará su informe sobre la red de trata que funciona en el Bronx, centro de Bogotá, donde muchas jóvenes caen y sobre todo niñas caen en manos de los explotadores, **“sucede muy en nuestra narices”,** dice Quintero, resaltando que la trata de personas no solo se da hacia fuera del país sino en al interior de las ciudades.

**“No todas las propuestas son lo que son”,** advierte a la audiencia Claudia Quintero en entrevista con Contagio Radio y finaliza diciendo “con la trata no hay trato”.
