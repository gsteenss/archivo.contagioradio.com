Title: Primeras medidas cautelares étnicas de la JEP cobijan a comunidades del Bajo Atrato
Date: 2019-09-18 16:17
Author: CtgAdm
Category: Comunidad, Judicial
Tags: Bajo Atrato, JEP, medidas cautelares
Slug: primeras-medidas-cautelares-etnicas-de-la-jep-cobijan-a-comunidades-del-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Medidas-Cautaelares-Jep.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/JEP-MEDIDAS-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

[La Jurisdicción Especial para la Paz (JEP) a través de la Comisión Mixta de Verificación, instaló las primeras medidas cautelares para la protección de la vida y los territorios de comunidades étnicas organizadas en Zonas Humanitarias y de Biodiversidad en el Bajo Atrato, en particular para los procesos comunitarios de **Curbaradó, Jiguamiandó y el resguardo Camerú en Chocó**, dando prioridad a sus principios territoriales, étnicos y de participación que deberán ser garantizados por una presencia de la institucionalidad. ]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Esta medida, brindada por el mecanismo de justicia transicional, se da luego de la presentación del informe "Van por nuestras tierras a sangre y fuego' de diciembre de 2018 y hace parte del caso 004 que prioriza la situación territorial  del Bajo Atrato chocoano con hechos sucedidos desde el 1 de enero de 1985 hasta diciembre de 2016.[(Lea también: JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica)](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/)

Su importancia radica en que son las primeras medidas, tanto étnicas como colectivas instauradas por la JEP y que pretenden fortalecer mecanismos de autoprotección de las comunidades. **"Para nosotros es muy significativo e histórico lo que acaba de pasar y esperamos que las medidas puedan ejercerse en su totalidad y que sea para nuestro beneficio"** expresó uno de los habitantes de una de las zonas humanitarias del Bajo Atrato.

Cada una de las instituciones que hacen parte de la Comisión Mixta de Verificación, como la **Defensoría del Pueblo, La Unidad de Investigación y Acusación, Ministerio de Defensa, del Interior, la Unidad Nacional de Protección y Procuraduría** tienen tareas y compromisos individuales al que tendrán que cumplir, de otro modo serán sancionadas.

### Son las primeras medidas de la JEP de esta naturaleza 

"Hablamos no solo de la vulnerabilidad de sus derechos, sino de sus territorios, de sus comunidades, del ambiente" lo que es un componente esencial para su vida" afirma la abogada Diana Marcela Muriel quien agregó que se trata de una gran oportunidad para construir justicia restaurativa.

"Es un llamado a quienes estuvieron comprometidos en vulneraciones a las comunidades, para que comparezcan y se comprometan con la verdad, pero sobre todo garanticen la no repetición y puedan otorgar una reparación integral", agregó la abogada representante  de las zonas beneficiadas. [(Le puede interesar: Informes de violencia contra comunidades afro son entregados a la JEP)](https://archivo.contagioradio.com/informes-sobre-violencia-contra-comunidades-afro-llega-a-jep/)

<div class="css-1dbjc4n r-xoduu5">

La Comisión de Justicia y Paz, como organización acompañante de este proceso, ha solicitado que esta ampliación cobije a comunidades como las de Cacarica, La Balsita en Dabeiba y las zonas humanitarias de Pedeguita y Mancilla y La Larga y Tumaradó en Chocó, para que fortalezcan estas iniciativas de autoprotección.

**Síguenos en Facebook:**

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
