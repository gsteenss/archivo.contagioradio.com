Title: 2do día de protestas de trabajadores de Gaseosas Lux y Postobón en Bogotá
Date: 2015-07-23 15:34
Category: Economía, Nacional
Tags: ardila, lule, lux, postobon, sindicato, tercerizacion, Trabajadores, trabajar
Slug: 2do-dia-de-protestas-de-trabajadores-de-gaseosas-lux-y-postobon-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/PostobonPIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sindicato 

#####  

<iframe src="http://www.ivoox.com/player_ek_5224324_2_1.html?data=lpeflpiWeI6ZmKiak5WJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPVw8bXw8nTtsbnjMnSjazFt8bj1MbgjbHZvIztjLXc1dnTpoa3lIqvldOPtNPj1crg1sbSb8bijKeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Edwin Londoño, trabajador] 

######  

###### [23 jul 2015] 

 

Trabajadores de Gaseosas Lux, filial de Postobón S.A., cumplen su segundo día de protestas por lo que consideran, son políticas de la empresa para evitar la sindicalización de sus empleados.

Durante el miércoles 22 y jueves 23 de abril, cerca de 120 trabajadores se han manifestado pacíficamente con pancartas en la Avenida de las Américas con Cr 50 en Bogotá. Según explica Edwin Londoño, uno de los manifetantes, a pesar de que Postobón firmó un pacto global en el que asegura que respetan a las organizaciones sindicales, la empresa precariza los términos de contratación para evitar que los empleados organizados permanezcan en Postobón.

Aseguran que en años anteriores, el Sindicato de Trabajadores de Gaseosas Lux se extinguió por la no contratación de los afiliados, y que desde el 29 de mayo del 2015 en que se los trabajadores fundaron un nuevo sindicato, la Unión Sindical de Trabajadores de Gaseosas Lux, la empresa anunció la no renovación ni prorroga de contrato a algunos de sus fundadores.

Los abogados de la empresa habrían manifestado a los trabajadores sindicalizados que la contratación de término fijo y a través de cooperativas de tercerización laboral es una política de Gaseosas Lux, sin embargo, los trabajadores no se explican como personas que cumplen 4 años de labores no pueden acceder a contratos a término indefinido en la empresa del grupo Ardila Lüle.

\[KGVID width="640" height="480"\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/postobon.mp4\[/KGVID\]
