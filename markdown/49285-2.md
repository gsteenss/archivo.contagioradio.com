Title: En un mes se sabría como será la participación en la Mesa en Quito
Date: 2017-11-20 12:47
Category: Nacional, Paz
Tags: ELN, Mesa de diálogos Quito, Participación social
Slug: 49285-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [20 Nov 2017] 

Finalizaron las Audiencias preparatorias para la participación de la sociedad en la mesa de diálogos en Quito, en las que confluyeron aproximadamente 200 organizaciones, entidades y colectivos de todos los sectores y que permitió construir un mapa **para la participación que se podría dar a conocer dentro de un mes**.

De acuerdo con Luis Eduardo Celis, de las audiencias salieron varias conclusiones, una de ellas es la diversidad que hay en el país y la relevancia que debe dársele a las regiones **“debe haber una pluralidad de voces y un ejercicio de concertación entre diferentes**”, razón por la cual este componente tendrá gran importancia a la hora de diseñar el mecanismo de participación.

Otro de los puntos que rescató Celis de estos espacios, fue la participación de Ecopetrol, que expresó su voluntad por promover diálogos con el movimiento social, la academia y la ciudadanía sobre el aporte de la empresa petrolera a la economía del país y también las dudas y las propuestas de las comunidades en torno a ese renglón.

### **El tiempo de la participación es apremiante** 

Además, aseveró que debido al poco tiempo que le queda al gobierno Santos, **las primeras concertaciones sobre la participación deberían estarce generando a mediados de enero hasta mayo**.

“En lo que resta de gobierno es posible hacer unas dinámicas de participación y concertación, lo que lleva al tema de lo vinculante, de ser capaces de concertar entre los diferentes y los que han chocado en el territorio” afirmó Celis. (Le puede interesar:["ELN debe generar credibilidad y reforzar su unidad interna: Carlos Velandía"](https://archivo.contagioradio.com/eln-asesinato-indigena/))

Sobre la relevancia de este proceso en los medios de información, el analista político respondió que “el cubrimiento ha sido muy precario” como parte de un acuerdo entre el gobierno y el ELN, que ayuda a que no se dé un mal manejo de la comunicación y de lo que pasa en Quito, sin embargo Celis considera que lo que más ayudaría al proceso es que la sociedad esté informada de lo que pasa y no alejada del proceso de paz.

<iframe id="audio_22180683" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22180683_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
