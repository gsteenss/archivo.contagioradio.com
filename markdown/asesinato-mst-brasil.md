Title: Asesinan a militante histórico del MST en Brasil
Date: 2017-03-21 14:54
Category: DDHH, El mundo
Tags: asesinato, Brasil, MST, Waldomiro Costa Pereira
Slug: asesinato-mst-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/costa-pereira-800x445.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Libre red 

###### 21 Mar 2017 

**Waldomiro Costa Pereira**, militante del Movimiento Sin Tierra MST y del Partido de los trabajadores en Brasil, **fue asesinado en la madrugada de este lunes** por 5 hombres armados y encapuchados que lo remataron en el hospital donde se encontraba internado luego de haber sobrevivido a un atentado que había sufrido durante el fin de semana.

Según algunos testigos, los desconocidos que se transportaban en una moto **ingresaron al centro hospitalario a la fuerza y sometieron a los guardias de seguridad** para luego ejecutar a Costa Pereira, quien se recuperaba luego de haber sido intervenido quirurjicamente. Las acciones habrían ocurrido en un lapso de 3 minutos.

Por encontrarse alejado de la directiva del MST desde hace dos años, estos desconocen si Costa Pereira habia sido objeto de algún tipo de amenazas contra su vida o si  en su trabajo  había tenido algún tipo de contacto con personas peligrosas. A través de un comunicado la Organización repudió el homicidio afirmando que **"este es otro asesinato de un trabajador en el estado de Pará, del cual el gobierno es responsable por su incompetencia en garantizar la seguridad de la población y por la negligencia del estado en investigar y castigar los crímenes de esta naturaleza"**.

Waldomiro Costa Pereira era un **sobreviviente de la matanza de Eldorado dos Carajás** ocurrida en 1996 en el municipio de Pará, por el cual fueron únicamente condenados dos oficiales que estaban dirigiendo la operación. En ese momento algunos integrantes del MST ocupaban un establecimiento rural cuando la policía intentó dispersarlos disparando **asesinando a 19 de ellos**.

A manera de compensación, el gobierno entregó a los sobrevivientes lotes de tierra destinados en un asentamiento que fue bautizado 17 de Abril, en conmemoración a la fecha de la masacre. Costa, resultó beneficiario de la compensación, y **se dedicó enteramente a la producción agrícola**, llegando a ser designado asesor en esos temas para la alcaldía de Parauapebas, municipio vecino a Eldorado, en el que residía de lunes a viernes. Le puede interesar: 2[0 ciudades de Brasil se movilizan contra las reformas de Michel Temer](https://archivo.contagioradio.com/23-ciudades-de-brasil-se-movilizan-contra-reformas-de-michel-temer/).
