Title: Asesinan al líder social José Antonio Navas en el Catatumbo
Date: 2018-11-30 11:45
Author: AdminContagio
Category: DDHH, Nacional
Tags: Catatumbo, Lider social
Slug: asesinan-lider-social-jose-antonio-navas-catatumbo
Status: published

###### Foto: AscamcatOficial[[‏[ ]

###### 30 Nov 2018 

El pasado 29 de noviembre se registró un nuevo asesinato en el municipio de **Tibú, Norte de Santander**, se trata del líder social **José Antonio Navas**, integrante de La Asociación Campesina del Catatumbo (ASCAMCAT). Los hechos se presentaron en la misma vereda en donde hace tan solo 20 días asesinaron a Luis Tarazona, líder del mismo proceso.

Según la información recolectada, Navas, quien además era directivo de la **Junta de Acción Comunal de la Vereda El Libano** **e integrante de la Guardia Campesina y Marcha Patriótica**, se encontraba en la vereda Miramonte en las horas de la mañana, pero solo fue hasta el mediodía que se informó de su asesinato, al hallar su cuerpo con heridas de armas de fuego. Sin embargo, no se conoce que Antonio estuviera en alguna situación que pusiera en riesgo su vida.

**Continúa la problemática en el Catatumbo**

Olga Quintero vocera de ASCAMCAT denunció que en lo que va corrido del año, los miembros de la organización han sobrevivido a dos atentados, mientras que otros **han sido declarados objetivo militar**, sin que exista alguna "acción por parte del Estado para investigar y capturar a quienes cometen estos actos". [(Le puede interesar: Asesinan a Luis Tarazona, líder social en Tibú, Norte de Santander)](https://archivo.contagioradio.com/asesinan-a-luis-tarazona-lider-social-en-tibu-santander/)

La integrante de ASCAMCAT  agregó que en este momento un grupo de campesinos es el que está a cargo de hacer las debidas pesquisas sobre el homicidio e hizo un llamado al Gobierno pues **han sucedido 26 hechos de desplazamiento que han dejado a 387 familias desplazadas, han ocurrido 157 asesinatos en toda la región** de los cuales cinco fueron cometidos contra líderes sociales, tres de ellos sucedidos en los últimos tres meses.

**Jose Antonio líder social de la Zona de Reserva Campesina**

Quienes lo conocían afirman que se trataba de “un hombre alegre, sonriente y humilde que buscaba cambios reales para su región” y que tras su muerte deja una familia de seis menores edad y un vacío en el tejido de la comunidad de ASCAMCAT, “no se pueden apagar las ilusiones, las vidas y las esperanzas de paz en este país, esto tiene que parar” afirmó Quintero.

<iframe id="audio_30509314" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30509314_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
