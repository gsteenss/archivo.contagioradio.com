Title: Uribismo impulsa modificación de Ley para emitir ordenes de capturar contra ex-integrantes de las FARC
Date: 2018-12-12 12:14
Author: AdminContagio
Category: Nacional, Política
Tags: Congreso, FARC, Ley de Orden Público, Ratones
Slug: uribismo-orden-de-captura
Status: published

###### [Foto: Contagio Radio] 

###### [12 Dic 2018] 

Este martes, durante debate en el Congreso la Ley de Orden público, el representante a la cámara por el Centro Democrático **Alvaro Prada, incluyo una modificación a la normativa con la que se reactivarían las ordenes de captura contra ex-integrantes de las FARC**; la modificación, que no fue aprobada gracias a la presión ciudadana, tendría un efecto negativo para quienes están cumpliendo con lo acordado, mientras daría beneficios a quienes no han seguido el proceso.

> ¿Por qué es tan grave el mico del Centro Democrático ayer?
>
> Porque permite capturar a quienes sí están cumpliendo con el acuerdo de paz y en cambio beneficiar a disidencias que lo incumplieron.
>
> Hoy a las 10am twitteratón para reabrir la discusión del artículo. [\#NoEnredenLaPaz](https://twitter.com/hashtag/NoEnredenLaPaz?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/PW7Tu7NMEr](https://t.co/PW7Tu7NMEr)
>
> — Juanita Goebertus (@JuanitaGoe) [12 de diciembre de 2018](https://twitter.com/JuanitaGoe/status/1072830112175964160?ref_src=twsrc%5Etfw)

La Ley 418 de 1997, o Ley de Orden Público sirve de marco general para que el Presidente de la república pueda establecer procesos de diálogo, negociación y acuerdo con grupos armados al margen de la Ley. Dicho ordenamiento debe ser renovado periódicamente en función de las nuevas necesidades del país, no obstante durante su discusión este martes en plenaria de la Cámara de Representantes, el congresista Prada incluyó una modificación, que reactivaría ordenes de captura contra ex-integrantes de las FARC.

Congresistas de la bancada de oposición denunciaron que luego de ser radicada tal proposición, **la plenaria fue levantada sin que se diera discusión sobre la misma**; razón por la que algunos de ellos optaron por abandonarla. Miembros del partido de la U, Partido Liberal, e incluso, Cambio Radical, decidieron abandonar el recinto. (Le puede interesar: ["Organizaciones de DD.HH. y la Corte Penal Internacional temen impunidad por modificaciones a la JEP"](https://archivo.contagioradio.com/organizaciones-de-dd-hh-y-la-corte-penal-internacional-temen-impunidad-por-modificaciones-a-la-jep/))

Sobre esta modificación, **la senadora de la Fuerza Alternativa Revolucionaria del Común (FARC), Victoria Sandino**, manifestó que era inconstitucional, en tanto es una violación al Acuerdo de Paz, que fue adicionado a la carta constitucional de Colombia. No obstante, Sandino resaltó que **la acción de Prada, es solo una maniobra más del Centro Democrático cuya estrategia es hacer trizas efectivamente el Acuerdo**.

La Senadora de la FARC, sostuvo que esperan que en la discusión de este miércoles, el debate sobre la modificación se pueda realizar, de tal forma que se evite esta nueva estrategia "de intimidación", y de pánico contra quienes firmaron y han trabajado por la paz. (Le puede interesar: ["Implementación del Acuerdo de Paz no se está asumiendo como un compromiso de Estado"](https://archivo.contagioradio.com/implementacion-del-acuerdo-de-paz-no-se-esta-asumiendo-como-un-compromiso-de-estado/))

> Colombia: esta proposición incluida en la ley de orden público pone en riesgo el proceso paz y da argumentos a los bandidos de las disidencias de las FARC. Grave e irresponsable.  
> Pedimos que se elimine. [pic.twitter.com/KZ3GETtS8m](https://t.co/KZ3GETtS8m)
>
> — José Miguel Vivanco (@JMVivancoHRW) [12 de diciembre de 2018](https://twitter.com/JMVivancoHRW/status/1072869232071700484?ref_src=twsrc%5Etfw)

### **¡Con los ratones querían sabotear plenaria del Senado!** 

Otro hecho relevante ocurrió el martes en el Congreso: mientras la plenaria del Senado se disponía para iniciar el juicio contra el Magistrado Gustavo Malo, fueron arrojados desde las barras del público unos ratones sobre el lugar en el que habitualmente se sienta el Centro Democrático (a la derecha del recinto). **La plenaria tuvo que ser suspendida, y en análisis de la senadora Sandino, este precisamente era el objetivo del acto**.

> En medio de la puesta en escena de "reconciliación" de las Farc, arrojaron una bolsa con papeles y ratones a la bancada del [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw). Ahí es cuando uno dice, todos dan de lo que tienen. Rechazamos este acto de gaminería y esperamos saber pronto quien ingresó a los vándalos. [pic.twitter.com/g0NuBcQ9eD](https://t.co/g0NuBcQ9eD)
>
> — Senador Carlos Felipe Mejía (@CARLOSFMEJIA) [12 de diciembre de 2018](https://twitter.com/CARLOSFMEJIA/status/1072643427416854530?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
La Senadora manifestó que tan pronto como ocurrieron los hechos, algunos integrantes del Centro Democrático inmediatamente los acusaron como responsables de esta acción como una forma de distraer la atención. En su momento, el senador por el Centro Democrático Carlos Felipe Mejía, pidió que se investigará a quien había permitido el ingreso a barras de las personas que arrojaron los animales.

Gracias a información obtenida por funcionarios del Congreso, se supo que **las personas sindicadas ingresaron al recinto gracias a la autorización del también senador por el Centro Democrático, Ciro Guerra**. Aunque Guerra se defendió diciendo que había autorizado su ingreso sin conocer a las personas, Sandino cuestionó que esta situación ocurra, y pidió que asuma la responsabilidad de los hechos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
