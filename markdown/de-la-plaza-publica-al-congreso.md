Title: De la Plaza Pública al Congreso: La Agenda Legislativa de la Oposición
Date: 2018-07-19 13:22
Category: Paz, Política
Tags: Angela Robledo, Gustavo Petro, oposición, Proyectos de Paz
Slug: de-la-plaza-publica-al-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-19-a-las-1.11.29-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @petrogustavo] 

###### [19 Jul 2018] 

Los miembros de la "Bancada por la paz, la vida, la democracia y los derechos humanos", presentarán este jueves los proyectos de Ley que impulsarán en la posesión del nuevo Congreso el próximo 20 de julio, entre las cuales estarán las iniciativas de paz archivadas por la anterior legislatura, una reforma política y medidas con las que buscan apoyar a estudiantes, trabajadores y pensionados.

La bancada, liderada por Gustavo Petro y Angela Robledo, e integrada por congresistas de la lista de la decencia, miembros del Partido Verde, Maís y Polo, tendrá entre sus  proyectos más destacados el acto legislativo que busca reconocer al campesinado como sujeto de derecho y crear una reglamentación a la Consulta Popular; incluye la disminución del aporte al sistema de salud de los pensionados; y pretende revivir las 16 circunscripciones de paz.

Adicionalmente, propondrán nuevos beneficios para deudores morosos de créditos con el ICETEX;  modificar el Código Sustantivo del Trabajo para conciliar el derecho a la huelga con los Convenios sobre libertad sindical de la Organización Internacional del Trabajo; y radicarán un proyecto de Ley para lograr la atención integral de la primera infancia.

Con estos 14 proyectos de Ley, los miembros de la Bancada de oposición esperan pasar el programa de Gobierno que logró sumar más de 8 millones de votos, gracias a la Campaña de Plaza Pública al Legislativo. (Le puede interesar: ["Aún se puede defender el espíritu de la JEP"](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/))

[Proyectos de la Bancada por la paz](https://www.scribd.com/document/384256922/Proyectos-de-la-Bancada-por-la-paz#from_embed "View Proyectos de la Bancada por la paz on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_34273" class="scribd_iframe_embed" title="Proyectos de la Bancada por la paz" src="https://www.scribd.com/embeds/384256922/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-uRZTB3AjQcjzX5mSNzNN&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
