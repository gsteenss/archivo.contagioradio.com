Title: Universidad de Cartagena en paro hasta que el rector firme compromiso
Date: 2015-09-17 13:35
Category: Educación, Movilización, Nacional
Tags: Comuna Universitaria Cartagena, Derecho a la educación, Paro Estudiantil, Universidad de Cartagena
Slug: universidad-de-cartagena-en-paro-hasta-que-el-rector-firme-compromiso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cartagena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: S.O.SUniversidaddeCartagena 

###### 17 sep 2015 

La Universidad de Cartagena completa dos semanas de cese de actividades , con participación de todas las facultades, estudiantes y profesores, **debido a la fuerte crisis de infraestructura, falta de docentes de planta y calidad educativa** que se vive al interior del centro educativo.

Pese a que se presentó un pliego de peticiones ante decanos y el rector, donde firmaron representantes de las distintas facultades buscando un compromiso para solucionar esta situación, aún **no se ha logrado llegar a ningún acuerdo, debido a que el rector solicita que se reactiven las actividades, sin dar soluciones previas**.

Así mismo el rector ha dado **declaraciones en donde condena y criminaliza la protesta, y además ha indicado que hay intereses políticos al interior de estas manifestaciones**, sin embargo, Rodrigo Paredes, miembro de Comuna Universitaria Cartagena, indica que estas declaraciones son falsas ya que “no se ha permitido el desarrollo de ningún ejercicio electoral dentro de las actividades”.

Los estudiantes han indicado que **para levantar el paro y retomar actividades el rector debe comprometerse mediante un documento legal** a mejorar los problemas de infraestructura y contratación de docentes.

### **Emisora y Canal de T.V cerrados** 

En un comunicado de la emisora de la Universidad de Cartagena UdeC Radio, se indicó que los estudiantes no permitieron el ingreso a las instalaciones y por tanto, **mientras la comunidad se encuentre en asamblea permanente, se suspenderán las actividades radiales**.

Por esta situación también el canal UdeC T.V de la universidad, acogería las mismas medidas.
