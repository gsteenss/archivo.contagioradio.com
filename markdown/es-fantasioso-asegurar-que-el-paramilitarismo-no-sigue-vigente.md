Title: "Es fantasioso" asegurar que el paramilitarismo no sigue vigente
Date: 2016-04-13 19:32
Category: DDHH, Entrevistas
Tags: CINEP, Informe CINEP 2015, Paramilitarismo
Slug: es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/CINEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CINEP] 

###### [13 Abril 2016] 

El paramilitarismo es un fenómeno que no se ha superado, se le ha dado otro nombre para hacer ver la violencia política como violencia común, aseguró el sacerdote Javier Giraldo, durante la presentación del más reciente informe del CINEP sobre la situación de derechos humanos en Colombia durante 2015, en el que se evidencia que las 'BACRIM' cuentan con la **solidaridad y tolerancia de multinacionales, instituciones estatales y la fuerza pública**, tal y como ha sucedido con las estructuras paramilitares durante los últimos años.

Según se reporta en el informe, las principales violaciones a los derechos humanos fueron cometidas por paramilitares, **1064 hechos en total**, entre ellos 873 amenazas, 2 atentados, 99 ejecuciones extrajudiciales, 71 heridas, 18 torturas y 1 violación sexual. La mayoría de los casos se presentaron en Bogotá, Cauca, Santander y Valle del Cauca.

En relación con infracciones al DIH, entre ellas amenazas, heridas a personas protegidas y homicidios intencionales en persona protegida, los **paramilitares aparecen como los mayores infractores** con 838 hechos victimizantes, ocurridos en Bogotá , Cauca, Valle del Cauca, Santander y Antioquia, principalmente.  **  **

De acuerdo con el CINEP, quienes siguen en orden de responsabilidad en la comisión de violaciones a los derechos humanos, son la Policía Nacional con 572 hechos, el Ejército con 292 y la Fuerza Pública con 24. Frente al DIH el **Ejército cometió 169 infracciones, la Policía 161 y las FARC-EP 46**.

Como grave calificó el CINEP la actual situación de los defensores de DDHH en Colombia, pues continúan siendo uno de los sectores más afectados por acciones violentas cometidas, en su mayoría, por grupos paramilitares quienes durante el año pasado amenazaron a **191 defensores en Magdalena, Bogotá, Atlántico y Sucre**, principalmente.

Según Luis Guillermo Guerrero, actual director general del CINEP, este estudio detallado permite concluir que el cese de hostilidades ha funcionado para garantizar la integridad de los combatientes pero no la de la población civil, líderes sociales y defensores de DDHH principalmente, quienes **de recibir 421 amenazas de paramilitares durante 2012, pasaron a 873 en 2015**, y de enfrentar 71 detenciones arbitrarias por parte del Ejército durante 2012 y 128 por parte de la Policía, fueron víctimas de 115 y 160, respectivamente durante 2015.

Pese a que en la actual fase del paramilitarismo se evidencian cambios en los métodos de aplicación de la violencia y en el uso del lenguaje, de acuerdo con Guerrero el argumento usado por MinDefensa **para encubrir el accionar paramilitar** afirmando que lo que ocurrió en el marco del [[paro armado](https://archivo.contagioradio.com/comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas/)] de las Autodefensas Gaitanistas "fue la comisión de hechos criminales menores en número y en gravedad, pero que con el uso masivo del Facebook, Twitter e Instagram atemorizaron a la gente en lugares donde la violencia ha dominado muchos años", es "fantasioso", pues la realidad en las comunidades evidencia que los [[paramilitares siguen implementando planes de control territorial](https://archivo.contagioradio.com/ante-la-cidh-se-exige-al-gobierno-enfrentar-fenomeno-paramilitar/)].

El sacerdote Giraldo, concluyó la presentación de este informe insistiendo en la necesidad de consolidar una política de desmonte efectivo del paramilitarismo, con el fin de evitar el [[exterminio de quienes dejen las armas](https://archivo.contagioradio.com/el-paramilitarismo-tiene-una-fuerza-tan-grande-que-yo-creo-que-se-va-a-repetir-el-caso-de-la-union-patriotica/)] y conformen un partido político, por lo que recomienda a la Mesa de Diálogos que contemple entre las medidas de desmonte paramilitar, el romper los **vínculos que existen entre los paramilitares y la fuerza pública**, pues este elemento les ha permitido desarrollarse, consolidarse y cumplir con sus objetivos.

[Informe 2015 sobre la situación de derechos humanos y DIH en Colombia](https://es.scribd.com/doc/308466489/Informe-2015-sobre-la-situacion-de-derechos-humanos-y-DIH-en-Colombia "View Informe 2015 sobre la situación de derechos humanos y DIH en Colombia on Scribd")

<iframe id="doc_69665" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/308466489/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
