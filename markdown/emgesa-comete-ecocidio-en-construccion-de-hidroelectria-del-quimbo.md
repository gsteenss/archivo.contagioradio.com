Title: EMGESA comete "ecocidio" en construcción de hidroeléctria del Quimbo
Date: 2015-04-16 17:28
Author: CtgAdm
Category: Animales, Nacional
Tags: Animales, animales en vías de extinción, deforestación, EMGESA, Hidroelectrica, Quimbo, reforestación
Slug: emgesa-comete-ecocidio-en-construccion-de-hidroelectria-del-quimbo
Status: published

##### Foto: [polinizaciones.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4365251_2_1.html?data=lZijl5eZdY6ZmKiakp2Jd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FdzsbZx9iPt8af18rbjcbKqcTowsnc1ZDUs9Of0dfc28rHuNCfyc7R1NTJsIa3lIqum8jYtsrX0JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dusán, Asoquimbo] 

La fauna silvestre que habita a las orillas del río Magdalena se encuentra en peligro, debido a que le empresa **EMGESA,** que está construyendo la hidroeléctrica el Quimbo, **empezó su plan de deforestación para continuar con el proyecto.** Debido a la tala de árboles, muchos animales han tenido que salir de su hábitat y se encuentran en una grave situación de vulneración.

De acuerdo a Miller Dusán, integrante del Asoquimbo, **EMGESA se había comprometió a caracterizar todas las especies de flora y fauna** que habitan en esa zona, luego debían capturar a los animales y finalmente, reubicarlos en su hábitat natural. Sin embargo, ese compromiso no lo cumplió, y según las palabras de Dusán, **EMGESA está cometiendo un ecocidio**, es decir, un crimen contra el ambiente, definido como el deterioro al mismo y los recursos naturales como consecuencia de la acción directa del ser humano sobre los ecosistemas.

Pese a que la comunidad ha pedido que se actúe frente a esta problemática, lo cierto, es que “**a EMGESA le sale más barato pagar una multa así destruya la flora, la fauna y la seguridad alimentaria”,** asegura el integrante del Asoquimbo. Por su parte, el Ministerio de Ambiente, había anunciado que se había abierto un proceso sancionatorio contra la empresa, “pero eso continúa proceso”, afirma Miller Dusán.

“En otro países se hubiera expulsado a la empresa del país, debido a los efectos que estos actos causan en los equilibrios de los ecosistema”, dice Dusán, quien agrega que **EMGESA es una corporación “criminal”** y que el gobierno está en favor de ella.

Por su parte, los habitantes se encuentran preocupados porque los animales empezaron a salir de su hábitat y están siendo blanco de tráfico de especies. **Monos, culebras, venados, tigrillos, y otras especies que se encuentran en vías de extinción** están siendo víctimas de cazadores, y muchos están siendo vendidos a orillas de la carretera.

Mientras tanto, los pobladores como Miller, se siguen preguntando el por qué no actúan las autoridades ambientales y municipales, como **la Corporación Autónoma Regional del Río Grande de la Magdalena, Cormagdalena,** que según Dusán, “se lavó las manos diciendo que no puede intervenir en esa problemática”.
