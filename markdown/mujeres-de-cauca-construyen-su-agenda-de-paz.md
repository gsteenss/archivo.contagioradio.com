Title: Mujeres de Cauca construyen su agenda de Paz
Date: 2014-12-24 03:49
Author: CtgAdm
Category: Paz, Resistencias
Tags: mujeres, paz
Slug: mujeres-de-cauca-construyen-su-agenda-de-paz
Status: published

###### Foto: eluniversal.com.co 

Un grupo bastante diverso de organizaciones de mujeres del departamento del Cauca, presentaron su agenda “**Unidas pactamos Paz y exigimos justicia social**” un esfuerzo de varios meses que ha tenido como resultado un ejercicio de construcción de acuerdos entre los que se destaca la necesidad de que se desmilitaricen los territorios, los cuerpos de las mujeres y la vida de sus comunidades, este como un primer paso y a la vez logro de la **Agenda de Paz de las Mujeres del Cauca**.

Paola Díaz, integrante de la Corporación **COMUNITAR**, explica que esta agenda tiene 4 ejes Eje uno.  La **desmilitarización de los territorios, la vida y el cuerpo de las mujeres**, visibilización de las violencias contra las mujeres, desde todos los ámbitos, como contribución al ejercicio de los derechos, la disminución en las limitaciones para el acceso, uso y goce de la tierra para las mujeres en condiciones de equidad y avances en el reconocimiento y garantías para la representación y participación política de las mujeres en las organizaciones, procesos sociales e instancias de decisión.
