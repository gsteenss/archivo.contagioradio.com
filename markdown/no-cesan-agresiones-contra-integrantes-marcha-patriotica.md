Title: No cesan las agresiones contra integrantes de Marcha Patriótica
Date: 2016-11-28 13:18
Category: DDHH, Nacional
Tags: Amenazas, Lideres, marcha patriotica
Slug: no-cesan-agresiones-contra-integrantes-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha-patriotica1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La voz de Cinaruco] 

###### [28 Nov. 2016] 

**Este fin de semana se conocieron nuevos hechos intimidantes en contra de líderes y lideresas de Marcha Patriótica en diversos lugares del país.** Esta vez, las amenazas se presentaron en el Magdalena Medio y en Bogotá. **Los líderes que recibieron la afectación de manera directa fueron Miguel Cifuentes en Magdalena Medio y Ángela Tuta en Bogotá. **Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

En el primer caso además de Miguel Cifuentes, también han recibido amenazas Eladio Morales y Melkin Castrillón, todos integrantes de la Asociación Campesina del Valle del río Cimitarra – ACVC.

Miguel Cifuentes, en entrevista para Contagio Radio, aseguró que la amenaza llegó a su lugar de residencia **“llegué a mi casa y por debajo de la puerta habían echado un panfleto en el que pusieron precio a nuestras cabezas, 2 millones de pesos. Dicen que nos van a dar por donde más nos duele y creemos que es a nuestras familias. Tenemos que ver qué vamos a hacer".**

Por su parte en Bogotá la afectada directa fue la docente **Ángela Tuta, a quien le fue asaltado su apartamento por parte de sujetos que vulnerando la seguridad de su residencia de la que le robaron 3 computadores.**

“Cuando entré a mi casa vi que habían violado las chapas, se llevaron mis 3 computadores, una memoria y dinero. El CTI estuvo buscando huellas en mi casa, resulta que no habían, la persona que ingresó lo hizo con guantes" contó Ángela.

Ambos líderes aseguraron que han interpuesto las respectivas denuncias y que esperan haya una pronta respuesta y agregaron que “**el Estado debe responsabilizarse por lo que pasa con Marcha Patriótica porque somos quienes estamos poniendo los muertos". **Le puede interesar: [5 propuestas de Marcha Patriótica para frenar oleada de violencia.](https://archivo.contagioradio.com/5-propuestas-marcha-patriotica-frenar-oleada-violencia/)

<iframe id="audio_14192460" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14192460_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_14197988" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14197988_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
