Title: Comunidades dicen no a hidroeléctrica en Tolima
Date: 2019-02-06 14:37
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Comité Ambiental en Defensa de la Vida del Tolima, Hidrototare, Río Totare, sequías, Tolima
Slug: tolima-no-hidroelectrica-rio-totare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/tolima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [4 Feb 2019] 

Comunidades de seis municipios en el Tolima manifestaron su inconformidad con la construcción de una central hidroeléctrica sobre el Río Totare, en Anzoátegui, dado que este proyecto agudizará** la crisis de sequías de la cual ya padecen los pobladores en el noreste del departamento.**

Durante una audiencia pública convocada por la Corporación Autónoma Regional del Tolima (Cortolima), ciudadanos, organizaciones ambientales, funcionarios y representantes del proyecto presentaron argumentos al favor y en contra de Hidrototare, una hidroeléctrica a filo de agua que desviaría siete kilómetros del afluente para generar energía eléctrica. Actualmente, la empresa Hidrogeneradora Pijao S.A.S espera respuesta de Cortolima ante su solicitud de licencia ambiental.

Según Jaime Tocora, integrante del Comité Ambiental en Defensa de la Vida del Tolima, la principal preocupación de los ciudadanos es las sequías que el proyecto hidroeléctrico generaría en los municipios vecinos de Venadillo, Anzoátegui, Alvarado, Piedras, Santa Isabel e Ibagué dado que estos afectos se han registrado en relación a otros proyectos similares en el departamento.

En el municipio de Chaparral, ubicado en el sudoeste del Tolima, las comunidades cuentan que la Central Hidroeléctrica del Río Amoyá - La Esperanza, ha **ocasionando el desabastecimiento de agua en unas 40 veredas en los alrededores de la central** desde que se inauguró en el 2013. Según Tocora, este tipo de proyecto, los pequeños centros hidroeléctricos, suelen generar el drenaje de fuentes hídricas adyacentes.

Tocora agregó que en la región donde se planea construir Hidrototare, un sector de vocación agrícola, ya se registra desabastecimiento de agua en época de sequía por efectos causados por el cambio climático. Incluso, en recientes años, los caudales de los principales ríos del Tolima se han visto reducidos en un 50 %, causando racionamientos de agua en varios municipios. "[Si a eso le empezamos a sumar elementos negativos como este tipo de centrales eléctricas pues la gente tendrá mucho más difícil acceso al agua," aseguró Tocora. ]

Por su parte, Daniel Rubio, el Procurador Judicial, Agrario y Ambiental, le solicitó a Cortolima durante la audiencia de que se despache como desfavorable la solicitud de licencia ambiental a la hidroeléctrica. Cortolima tiene plazo de 60 días para decidir sobre el ortogamiento de la licencia ambiental.

<iframe id="audio_32240226" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_32240226_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
