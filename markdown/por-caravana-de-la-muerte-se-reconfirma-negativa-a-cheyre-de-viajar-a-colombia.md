Title: Por "Caravana de la muerte" se reconfirma negativa a Cheyre de viajar a Colombia
Date: 2016-09-22 12:13
Category: El mundo
Tags: Cheyre, colombia, Dictadura chilena, paz, pinochet
Slug: por-caravana-de-la-muerte-se-reconfirma-negativa-a-cheyre-de-viajar-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/3d8d757e773715cf1391177d616525fe_XL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### 22 Sep 2016

El juez chileno **Mario Carroza**, reabrió el caso por la denominada "**Caravana de la muerte**" que investiga el asesinato de casi militantes políticos durante la dictadura de Augusto Pinochet, acción que reafirmaría la negativa para el permiso concedido al General (r) **Juan Emilio Cheyre** de viajar a Colombia en calidad de observador al proceso de paz.

La decisión del magistrado tendría sustento en los primeros informes sobre "**El libro novedades de guardia**" de la cárcel de la Serena, donde se registra la salida de almenos 50 prisioneros, bajo la orden del general **Sergio Arellano Stark** de ser trasladados al regimiento de la ciudad en octubre de 1973.

En la publicación de 369 páginas, aparece una lista de nombres que Carroza ordenó a la **Policia de Investigaciones (PDI)**, ordenar e interrogar para conseguir nuevas informaciones sobre el caso. El documento había sido entregado, bajo reserva, por una mujer al Museo de la Memoria, de donde provino la copia que llegó a manos del magistrado.

En relación con la situación del general (r) Cheyre, el abogado del caso Cristian Cruz, indico que este primer informe "ancla, fortifica, robustece aún más los medios de pruebas que hay en contra del general  y **echa por tierra su defensa de que nunca hizo nada**", agregando que la decisión de trasladar los prisioneros del regimiento de Arica fue de Cheyre, por su rango superior al de los suboficiales que ejecutaron las acciones.

A partir de las decisiones que puedan llegar con la apertura del expediente de la Caravana de la muerte, nuevas peticiones de procesamiento que vinculan a uno o más oficiales del centro penitenciario, y pone en consideración de la **Corte de Apelaciones de Santiago** el permiso otorgado al General en retiro para viajar a Colombia.
