Title: En Arauca han sido asesinadas 32 personas en tan solo dos meses
Date: 2019-03-06 16:55
Author: CtgAdm
Category: DDHH, Nacional
Tags: Arauca, Disidencias de las FARC, ELN
Slug: en-arauca-han-sido-asesinadas-32-personas-en-tan-solo-dos-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 6 Mar 2019

**El Comité Permanente para la Defensa de los Derechos Humanos (CPDH)** y diversas organizaciones sociales denuncian que en lo que va corrido del 2019 han sido asesinadas 32 personas, en el departamento de Arauca, uno de los territorios más militarizados en Colombia.

**Guillermo Díaz, integrante del CPDH** de este departamento señala que aunque, en el territorio araucano siempre ha existido conflicto, los asesinatos selectivos y las amenazas que se han registrado, evidencian cómo se ha agudizado la violencia en en el pie de monte araucano, en las zonas rurales y en la capital.

**En municipios como Saravena, Arauca y la vía que del Centro Poblado de Santo Domingo conduce a la cabecera municipal de Tame**, desde el 20 de febrero se registraron los asesinatos de Villaer Mora de 28 años, Eliecer Andrés Meneses de 37 y José Jeferson Nuñez de 22,  Adrián Suárez Fernández de 43 años,  el exgobernador de la comunidad Cuiloto en Tame, Demetrio Barrera Fuentes; además de las muertes de otras tres personas que continúan sin identificar.

Historicamente, en Arauca ha hecho presencia la guerrilla del ELN, al que se suman la presencia de hombres encapuchados que pertenecerían a las disidencias del décimo frente de las FARC, quienes han puesto en circulación panfletos amenazantes en contra de la población civil, que según el defensor de DD.HH se ha convertido en una acción reiterativa **no solo en el municipio de Arauquita sino en Tame y Fortul.**

Aunque se han hecho las respectivas denuncias y se han activado alertas tempranas para que el Estado cumpla su función, Díaz señala que no es suficiente con el que Gobierno militarice la zona, "es necesario que el Gobierno implemente políticas públicas que detengan esta violencia", señala. [(Lea también 9 personas han sido asesinadas en Arauca tras fin de cese al fuego con ELN) ](https://archivo.contagioradio.com/tras-fin-de-cese-al-fuego-han-sido-asesinadas-a-9-personas-en-arauca/)

Asímismo, reitera que como defensores de derechos humanos de la región continúan realizando esfuerzos para que el "**Gobierno pueda continuar con los diálogos con el ELN, se trata de que las partes puedan dialogar y parar el conflicto, necesitamos que llegue la paz".**

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
