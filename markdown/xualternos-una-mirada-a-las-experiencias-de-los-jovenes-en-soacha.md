Title: "Xualternos" una mirada a las experiencias de los jóvenes en Soacha
Date: 2018-05-23 13:28
Category: Cultura
Tags: Documental, jóvenes de soacha, soacha, xualternos
Slug: xualternos-una-mirada-a-las-experiencias-de-los-jovenes-en-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/documental.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Xualternos] 

###### [23 May 2018]

Por medio del documental “Xualternos” un grupo de jóvenes que habita en el municipio de Soacha busca retratar **las experiencias juveniles** enfocadas a la superación de ciertos problemas que afectan a diferentes comunidades. El documental se ha presentado en México y será lanzado el 25 de mayo de 2018 en ese municipio.

De acuerdo con Cristian Cuellar, realizador del documental, la historia que buscan relatar, se enfoca en una necesidad de “dar a conocer **las luchas de las personas jóvenes** que han sido invisbilizadas o han pasado desapercibidas”.  Afirmó que el documental hace parte de una iniciativa de la comunicación popular que quiere “retratar a unos sujetos que se preocupan por el municipio”.

### **Documental retrata historias de jóvenes que trabajan por el territorio** 

Frente a las luchas que busca retratar el documental Cuellar se refirió a las que han estado enfocadas a generar transformaciones en temas **territoriales, de participación política,** el ambiente y la convivencia. Por ejemplo, dijo que en Soacha hay problemas ambientales relacionados con la minería ilegal de los cuales “las personas no se han enterado”.

Por esto, aprovecharon el uso de la entrevista para retratar historias individuales que han aportado al **desarrollo y buen vivir del municipio.** Los documentalistas creen que la juventud de Soacha se ha esforzado por comprender las dinámicas de su territorio teniendo como objetivo la transformación social. (Le puede interesar:["Mi hijo para mi es un gran héroe": María Sanabria, una de las madres de Soacha"](https://archivo.contagioradio.com/maria_sanabria_madres_soacha_falsos_positivos/))

Cuellar, indicó que las personas que aparecen en el documental han logrado “generar cambios en el municipio, pero también en las personas que habitan allí”. Dijo que la apuesta del documental es resolver la pregunta de **¿qué es ser joven?** y ¿cuál es la posición que se asume desde la juventud?

Finalmente, recordó que la presentación de documental se realizará el día **viernes 25 de mayo** en el teatro público de Soacha. Allí, desde las 5:30 de la tarde se hará el lanzamiento de documental y habrá un espacio de socialización con personas como Luz Marina Bernal, madre de uno de los jóvenes desaparecidos y asesinados en Soacha y los protagonistas del documental.

<iframe id="audio_26141088" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26141088_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
