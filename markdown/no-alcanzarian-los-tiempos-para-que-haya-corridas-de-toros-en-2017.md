Title: No alcanzarían los tiempos para que en Bogotá haya corridas de toros en 2017
Date: 2016-06-13 17:10
Category: Animales, Nacional
Tags: alcladia de bogotá, corridas de toros, Corte Constitucional, Maltrato animal
Slug: no-alcanzarian-los-tiempos-para-que-haya-corridas-de-toros-en-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-20-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [13 Jun 2016] 

Desde la Alcaldía de Bogotá se anunció que pese a su rechazo a las corridas de toros, la administración de Enrique Peñalosa piensa acatar el fallo de la Corte Constitucional por medio del cual se ordena a la ciudad iniciar los trabajos para adecuar la Plaza de la Santamaría  para las actividades taurinas.

Frente a ese hecho, Clara Sandoval, gerente de Protección y Bienestar Animal del Distrito aseguró “no vamos a hacer ninguna maniobra dilatoria para impedir que esta sentencia se de porque somos respetuosos de la norma y no queremos sabotear esta decisión”. Sin embargo, también expresó que **teniendo en cuenta que la única opción que existe para acabar con las corridas de toros es a través de un proyecto de Ley, desde el distrito se defenderá esa iniciativa pero además se promoverá.**

Por su parte, Natalia Parra Osorio, directora de la Plataforma ALTO, (Animales Libres de Tortura), expresó que “como animalistas no nos podemos oponer a los fallos de la Corte Constitucional, pero moralmente son decisiones desatinadas, se trata de **un fallo bastante cuestionable”.**

Así mismo, señala que deben tener en cuenta los procesos y tiempos licitatorios, pues solo cuando se termine el reforzamiento estructural podrá iniciarse ese trámite, y **si lo que se quiere es acatar al pie de la letra los fallos de la Corte, lo cierto es que los tiempos nos alcanzarían para que haya temporada taurina en el 2017 en Bogotá**, pues la Ley tiene claros los pasos de la contratación pública. Primero se requieren unos pre-pliegos, luego unos pliegos y solo a mediados de enero se empezaría el proceso licitatorio.

“Si se está hablando de cumplir los mandatos de la Corte, entonces deben cumplirse todos en términos de la entrega de la plaza y también deberá cumplirse con la sentencia 666 que ordena que los animales no sean víctimas de dolor y sufrimiento en estos espectáculos”, expresa Parra.

Frente a esta situación, desde ya la mayor parte de la ciudadanía se manifiesta a través de las redes sociales con  [**\#BogotáMejorSinToreo**](https://twitter.com/hashtag/Bogot%C3%A1MejorSinToreo?src=tren), expresando su rechazo a este tipo de actividades crueles con animales, sobre todo si se está hablando de un escenario de posconflicto, como lo afirmó Natalia Parra.

<iframe src="http://co.ivoox.com/es/player_ej_11886870_2_1.html?data=kpalmpuce5Ghhpywj5abaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca_V1cbZy8aPlMLm08aYj5ClkLXDjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
