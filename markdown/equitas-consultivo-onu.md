Title: Equitas Colombia alcanza nivel consultivo ante la ONU
Date: 2018-09-26 16:12
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: colombia, EQUITAS, Naciones Unidas, ONU
Slug: equitas-consultivo-onu
Status: published

###### [Foto: @EQUITASColombia] 

###### [26 Sept 2018] 

Tras 3 años de un proceso administrativo en la Organización de las Naciones Unidas (ONU), Equitas Colombia recibió el estatus de consultor ante esta Organización; hecho que para Diana Arango, directora de la corporación, es un reconocimiento a las organizaciones de víctimas que trabajan con ellos.

Arango explicó que este estatus permite que los organismos que integran la ONU, es decir, comites, secretarías y relatores de las diferentes oficinas, puedan pedir consultas externas a Equitas en diferentes temas; también implica que la ONG pueda presentar informes sobre los asuntos que investigan. Los consultores de la Organización también pueden participar directamente en algunos de los instrumentos que tiene el sistema de las Naciones Unidas.

La directora de Equitas dijo que el nuevo estatus, es una distinción para las organizaciones de víctimas con las cuales trabajan, y con ello esperan que las recomendaciones hechas a los comités de la ONU sobre el tema de personas desaparecidas, se reflejen en llamados a las autoridades colombianas encargadas de este tema.

### **Nos preocupa que hagan recortes presupuestarios a la implementación de la paz** 

La directora de la ONG sostuvo que aunque el nuevo Gobierno declaró durante elecciones su oposición al proceso de paz, debe garantizar el funcionamiento e implementación de los mecanismos creados por el acuerdo. Dicha implementación, incluye que se asignen los presupuestos para que los mecanismos del Sistema de Verdad, Justicia, Reparación y No Repetición operen (SVJRNR) y cumplan con su labor.

Por esta razón, Arango manifestó su preocupación ante las declaraciones del presidente Iván Duque, según las cuales no habría recursos económicos para la implementación del Acuerdo de Paz, lo que sería un "golpe muy duro para la sostenibilidad de la paz en Colombia"; pero sostuvo que la ONU es un nuevo aliado muy importante para hacer una llamado al Gobierno colombiano para garantizar los recursos para el SVJRNR, y especialmente para la Unidad de Personas Dadas por Desaparecidas.

Equitas es una organización científica forense, enfocada en investigación criminal con enfoque de derechos humanos que ha hecho acompañamiento integral para quienes han sufrido violaciones graves como la desaparición forzada. Por el impacto de ese trabajo en Colombia y la región, el Consejo Económico y Social de Naciones Unidas inició en 2015 el proceso que terminó con el estatus consultivo ante esa Organización.

<iframe id="audio_28914921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28914921_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
