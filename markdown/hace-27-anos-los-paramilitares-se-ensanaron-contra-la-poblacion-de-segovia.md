Title: Hace 27 años los paramilitares se ensañaron contra la población de Segovia
Date: 2015-12-21 13:35
Category: Nacional, Sin Olvido
Tags: masacre de Segovia, paramilitares, Unión Patriótica
Slug: hace-27-anos-los-paramilitares-se-ensanaron-contra-la-poblacion-de-segovia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Jueves_7_3_2013@@masacre_gra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elmundo.com 

<iframe src="http://www.ivoox.com/player_ek_9804043_2_1.html?data=mp2dlpWYd46ZmKiakpuJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLXxpCfmZDFaaSnhqee0diPsNDnjNXO1MbRrc3d1cbfx9iPt8afxtPgw4qnd4a2ksbf0dOPp9Di1dfOjdHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela Castellanos, Cahucopana] 

###### [21 Dic 2015]

Después de 27 años y por orden de la Corte Interamericana de Derechos humanos, el Estado colombiano reconoció su responsabilidad en los hechos que rodearon la masacre de Segovia, en Antioquia que **dejó 43 personas asesinadas a manos de paramilitares y con el permiso de la fuerza pública.**

Por las afectaciones a la vida, a la salud, el daño moral y el perjuicio material, en el municipio de Segovia, el Estado aceptó su responsabilidad en un acto donde participó la Unidad de Reparación de Victimas y Guillermo Rivera, consejero presidencial para los Derechos Humanos, quienes emitieron esta solicitud de perdón ante la delegación de víctimas que **ese día recordó cómo integrantes de la Policía y del Ejército contemplaron inmóviles la comisión de la masacre, facilitando la entrada de los victimarios, quitando puestos de control y suspendiendo el patrullaje.**

"Poco antes de las 7 de la noche del 11 de noviembre de 1988 tres camperos irrumpieron en las calles de Segovia" rememora una columna ubicada en la plaza central. Allí, llegó la** marcha que había empezado en el cementerio **donde se desarrolló una actividad simbólica y la proyección de un documental en homenaje de las víctimas.

Al medio día del domingo la población de Segovia recordó cómo un grupoo de paramilitares del Magdalena medio y del Urabá al mando de Alonso de Jesús Baquero alias "Negro Vladimir", con lista en mano irrumpieron en las casas de los pobladores, particularmente líderes sociales y dirigentes políticos miembros de la Unión Patriótica, asesinando 43 personas de las 60 que aparecían consignadas.

Ángela Castellanos, representante legal de Cahucopana, asegura que ningún acto de perdón repara a las víctimas, **"falta contarle a la gente la verdad y el por qué, debe haber una reparación integral",** pues señala que tras 27 años, este acto es lo único que han visto materializado pese a la sentencia de la Corte Interamericana.

Desde el año de los fatídicos sucesos, la justicia viene dando lentos pasos hacia el esclarecimiento de las motivaciones y los responsables de la masacre. El primero en recibir condena fue el expresidente de la Cámara y exdiputado de Antioquia César Pérez García, reconocido cacique político de la región, que  fue sentenciado a 30 años de prisión por la Corte Suprema de Justicia en 2013.

El horror paramilitar fue calificado por la Corte Suprema de Justicia como retaliación, en contra de una población que en la primera elección popular de alcaldes eligió a Rita Ivonne Tobón, de la Unión Patriótica, y se alejó del cacicazgo liberal que representaba Pérez García.

En 2000 la CIDH recibió denuncia interpuesta por Javier Villegas Abogados, en la que se alegaba la responsabilidad internacional del Estado, en favor de 34 personas muertas, de 4 heridas y de sus familias, por la violación de los derechos a la vida, a la integridad personal, a la libertad personal, a las garantías judiciales, a la indemnización, a la protección de la honra y la dignidad, a la protección de la familia, a la circulación y residencia y a la protección judicial.

"Hubo hechos de ensañamiento contra la población. Históricamente, el nordeste antioqueño ha sido una zona donde el auge del paramilitarismo estuvo y está presente", dice la integrante de Cahucopana, quien añade que **la única forma en que la población podría sentir el verdadero arrepentimiento del Estado, sería con el desmonte real y pronto del paramilitarismo.**
