Title: Las preocupaciones de restringir manifestaciones en periodos electorales
Date: 2019-07-17 17:12
Author: CtgAdm
Category: Movilización, Política
Tags: Decreto, derecho a la protesta, Ministerio del Interior, protesta
Slug: restringir-manifestaciones-electorales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/manifestacion-en-plaza-de-bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Santa Fé  
] 

Recientemente se hizo público el **borrador del decreto promovido por el Ministerio del Interior, que buscaría eliminar la posibilidad de realizar manifestaciones públicas en determinadas fechas del calendario electoral.** La medida fue cuestionada porque se podrían presentar sesgos en el momento de permitir -o negar- la realización de encuentros en espacios públicos; al tiempo que se suborinarían estas decisiones a las órdenes del poder nacional.

El **actual magistrado del Consejo Nacional Electoral (CNE) Luis Guillermo Pérez** dijo que no conocía la motivación del Gobierno para tomar esta decisión, pues hace poco tiempo participó de una reunión  en la que se presentó lo que sería el actual proceso electoral con el presidente Iván Duque, sus ministros y la Policía; pero "allí no se mencionó esta iniciativa". (Le puede interesar: ["Buenaventura volverá a marchar ante incumplimientos del Gobierno"](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/))

Pérez recordó que son las autoridades municipales son las encargadas del orden público, en esa medida, **el decreto sería "invadir la esfera de la autoridad municipal y las democracias locales"**. Adicionalmente, el Magistrado sostuvo que el Gobierno debería reconsiderar el Decreto, porque lo ideal es avanzar en garantizar derechos políticos, y en su lugar, **con esta medida "podrían presentarse sesgos para prohibir actividades políticas"**.

Por último, el Integrante del CNE recordó que **en momentos pasados fue necesario que el Gobierno Nacional interviniera ante las autoridades locales para proteger los derechos políticos de la oposición,** pero no para actuar contra ella; en ese sentido, señaló que un deber pendiente en aras de garantizar el derecho a la protesta es implementar lo contenido en el punto dos del Acuerdo de Paz. (Le puede interesar: ["Estudiantes vuelven a las calles por el desmonte del ESMAD"](https://archivo.contagioradio.com/desmonte-del-esmad/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
