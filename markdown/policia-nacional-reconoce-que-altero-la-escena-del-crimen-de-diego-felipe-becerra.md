Title: Policías reconocen que alteraron la escena del crimen de Diego Felipe
Date: 2016-04-12 16:18
Category: DDHH, Entrevistas
Tags: “Trípido”, asesinato joven grafitero, Diego Felipe Becerra
Slug: policia-nacional-reconoce-que-altero-la-escena-del-crimen-de-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/tripido-diego-felipe-becerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Beligerando Con Arte] 

###### [12 Abril 2016 ]

Este lunes, luego de años de dilación en el proceso, la Jueza 19 de conocimiento de Bogotá avaló el 'Principio de Oportunidad' aprobado por la Fiscalía a los patrulleros involucrados en el [[asesinato del joven grafitero Diego Felipe Becerra](https://archivo.contagioradio.com/una-pildora-para-la-memoria-diego-felipe-becerra/)]. El Principio de Oportunidad fue concedido a los patrulleros al aceptar que altos mandos de la Policía los obligaron a alterar la escena del crimen. Con esta aprobación se profirió un **fallo condenatorio a Freddy Navarrete y Nelson Rodríguez, de 48 y 54 meses**, respectivamente.

Entretanto el patrullero Nelson Giovanny Tovar espera ser cobijado con la medida, teniendo en cuenta que quiere cooperar con la justicia y contar la verdad, pero los altos mandos de la Policía involucrados en el crimen, han entrado a su lugar de residencia, han tomado fotografías de sus familiares y lo han [[amenazado para que no revele los nombres](https://archivo.contagioradio.com/dilaciones-y-amenazas-caracterizan-el-proceso-por-el-asesinato-de-diego-felipe-becerra/)]de los **Coroneles y Generales que tendrían responsabilidad en la alteración de la escena del homicidio**.

De acuerdo con Gustavo Trejos, padre de Diego Felipe, el patrullero Tovar no ha asistido a las audiencias del proceso por temor a que se cumplan las amenazas que los altos mandos de la Policía le han hecho llegar junto al ofrecimiento de dinero, **para que no involucre a Coroneles y Generales en sus declaraciones**. Trejos asegura que este patrullero fue quien llevó el arma al lugar del crimen y que uno de los últimos mensajes que le llegó es "que sí quería plata o plomo". Se espera que esta denuncia sea incluida en el proceso judicial y que sea presentada ante la CIDH, para proteger la integridad del patrullero.

"Se logró el objetivo" asevera Trejos y agrega que "con estas dos condenas se ratifica que la Policía Nacional incurrió en delitos para encubrir la muerte de Diego Felipe", y se obliga a Navarrete y a Rodríguez a continuar rindiendo declaraciones por el homicidio del joven y por la alteración de la escena del crimen. **De retractarse o mentir, los patrulleros podrían perder el beneficio del 'Principio de Oportunidad'** y se verían obligados a pagar condenas de entre 25 y 32 años, conforme lo harán los altos mandos involucrados.

"Es una lucha de mucha paciencia", afirma Trejos, calculando que la condena de 33 años de cárcel para el patrullero Wilmer Alarcón, por asesinar a Diego Felipe, puede estar lista para agosto o septiembre de este año. En el proceso por **alteración de la escena del crimen están involucradas 11 personas, 8 de la Policía y 3 civiles**, y de acuerdo con Trejos, llevan 37 meses esperando que finalice la audiencia preparatoria, por las dilaciones injustificadas de los abogados de los Coroneles, para buscar nulidades y vicios, para que en determinado momento el proceso vuelva a empezar.

Lo que sigue en el proceso es "algo muy complicado" asegura Gustavo Trejos, pues de los 11 abogados, "9 trabajan para buscar nulidad" y cada apelación representa 2 o 3 meses pérdidos; **"yo calculo que si nos va bien en el 2018 se tendrían condenas"**, y en los próximos meses nuevas capturas, entre ellas la de "un coronel, un mayor, una teniente y asesores de la Policía Nacional".

"Es muy triste que el ente que fue creado para salvaguardar la vida, honra y bienes de los ciudadanos, para proteger la constitución y la ley, esté permeado por la delincuencia y que esos malos elementos que están dentro de la Policía Nacional, actúen de una forma y otra para hacerle daño a sus mismos compañeros o a las personas que tienen la valentía de denunciar los **actos delincuenciales de la alta cúpula corrupta de la Policía Nacional que actúo con un poder desmedido del uso de la fuerza**", asevera Trejos.

"Es un peldaño que confirma que hemos tenido la razón en estos cinco años, que confirma que hay culpabilidad de altos mandos de la Policía Nacional. Iremos hasta el final para buscar esas condenas, y respiraremos con tranquilidad porque se limpiará el nombre de Diego Felipe, **se confirmará lo que hemos dicho durante estos cinco años y sentaremos un precedente para que esto no le vuelva a suceder a ningún joven**", concluye el padre del joven.

<iframe src="http://co.ivoox.com/es/player_ej_11141326_2_1.html?data=kpaelpaXdpehhpywj5aZaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPmNPZy9Tgh5enb9HVxdfSjanNqcjjjKvSzs7UqYy2xsjS1NfFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
