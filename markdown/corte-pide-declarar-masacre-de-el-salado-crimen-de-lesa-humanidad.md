Title: Corte pide declarar Masacre de El Salado crimen de lesa humanidad
Date: 2018-07-07 07:00
Category: Nacional
Tags: corte suprema, El Salado
Slug: corte-pide-declarar-masacre-de-el-salado-crimen-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/El-salado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Felipe Ariza - Min Interior 

###### 6 Jul 2018 

Por medio de la sentencia 52557 del 4 de julio de 2018 La **Corte Suprema de Justicia**  dejó en firme la condena contra el **capitán Héctor Martín Pita Vásquez**, por su omisión en el año 2000 para detener la masacre que cometían grupos paramilitares en El Salado, cuando se despeñaba como comandante de la Compañía Orca.

Según la sentencia el capitán y los oficiales de la primera brigada de infantería de marina “permitieron el accionar violento de los grupos paramilitares que se tomaron durante varios días la población de El Salado y sus zonas aledañas, prestando de manera omisiva su concurso para ese cometido criminal, dejando de actuar conforme al mandato constitucional que les imponía contrarrestar las acciones lesivas que fueron ejecutadas durante ese tiempo”.

Igualmente la Corte ratificó la sentencia de 13 años de prisión contra el capitán **Pita,** atribuida por el **Tribunal Superior y el Juzgado Único Penal del Circuito** Especializado de Cartagena por la complicidad en el homicidio agravado.

Así mismo la Alta Instancia judicial  revalido el testimonio  del **Infante de Marina Alfonso Enrique Benítez Espitia** quien manifestó la connivencia entre militares y el grupo paramilitar, a tal punto que a la salida del corregimiento las partes dialogaron sobre  la coordinación de sus acciones.

### **Crimen de lesa humanidad:** 

En los hechos ocurridos en El Salado entre el 16 y el 21 de febrero del año 2000 en el que fueron asesinadas 66 personas, la procuraduria delegada ante la Corte Suprema demanda que la investigación por los delitos cometidos sea reconocida como imprescriptible y pide a **la Fiscalía** y a los funcionarios judiciales declarar a la masacre de El Salado como un crimen de lesa humanidad.

Para una de las familiares de las víctimas de la masacre esta sentencia significa un avance, y considera que "estas noticias tan importantes deben ser compartidas con las victimas y que ojala esto ayude a que se haga justicia y haya paz y no vuelva a suceder eso tan terrible".

------------------------------------------------------------------------

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
