Title: En fotos | Con música y danza, líderes sociales dan apertura al Refugio Humanitario
Date: 2019-04-29 11:48
Author: CtgAdm
Category: Fotografia
Tags: lideres sociales, Refugio Humanitario por la vida
Slug: en-fotos-refugio-humanitario-lideres-sociales-se-toman-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1-e1556904603356.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/2.1-e1556904584854.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/2-e1556904569115.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.1-e1556904361569.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.2-e1556904335375.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.3-e1556904542245.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.4-e1556904527667.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3-e1556904478498.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/4-e1556904459789.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/5-1-e1556904448574.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/6-e1556904435770.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/7-e1556904421940.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/8-e1556904407217.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/9-e1556904383577.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/10-e1556904243610.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El día de ayer cerca de 2.000 líderes de la Costa Pacífica, Nariño, Antioquia Arauca y Casanare y en general de todo el país fueron recibidos de forma acogedora al llegar a “La Plaza de la Vida’ nombre que ha adoptado la Plaza de Toros La Santa María para esta celebración de la vida.

<figure>
![Foto: Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1-770x540.jpg)

</figure>
<figure>
![2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/2-770x540.jpg)

</figure>
<figure>
![2.1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/2.1-770x540.jpg)

</figure>
<figure>
![3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3-770x540.jpg)

</figure>
<figure>
![3.3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.3-770x540.jpg)

</figure>
<figure>
![3.1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.1-770x540.jpg)

</figure>
<figure>
![3.2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.2-770x540.jpg)

</figure>
<figure>
![Foto: Andres Zea /Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3.4-770x540.jpg)

</figure>
<figure>
![4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/4-770x540.jpg)

</figure>
<figure>
![5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/5-1-770x540.jpg)

</figure>
<figure>
![6](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/6-770x540.jpg)

</figure>
<figure>
![7](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/7-770x540.jpg)

</figure>
<figure>
![Foto: Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/8-770x540.jpg)

</figure>
<figure>
![9](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/9-770x540.png)

</figure>
<figure>
![10](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/10-770x540.jpg)

</figure>

