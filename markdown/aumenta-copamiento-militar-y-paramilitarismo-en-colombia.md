Title: Aumenta copamiento militar y paramilitarismo en Colombia
Date: 2015-11-12 14:10
Category: Nacional, Paz
Tags: Comisión Intereclesial de Justicia y Paz, dialogos de paz, DIPAZ, FARC-EP, Frente Amplio por la PAz, Gobierno Nacional, Ivan Cepede, Mesa de diálogos en la Habana, Octavo informe cese al fuego FARC-EP y desescalamiento del conflicto, Proceso de paz en la Habana
Slug: aumenta-copamiento-militar-y-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Veeduría-cese-al-fuego-unilateral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [12 Nov 2015]

[[Este jueves se presentó el informe de veeduría del cese al fuego unilateral por parte de las FARC-EP y al desescalamiento del conflicto por parte del Gobierno Nacional. Allí** **se evidenció que el proceso de paz se encuentra en riesgo teniendo en cuenta que el Ejército **continua realizando operaciones militares y el paramilitarismo sigue aumentando en niveles similares a los de la época entre 199**]

[El informe denominado "**Se necesita cese bilateral al fuego y garantías reales de no repetición**" fue presentado por DIPaz, el Frente Amplio Por la Paz, Clamor Social por la Paz y la ONIC. De acuerdo con el documento, entre el 20 de septiembre y el 20 de octubre se registró un **hecho de bombardeo por parte de la fuerza pública a un campamento de las FARC, y otros cuatro ametrallamientos** de las fuerzas militares, mientras que las FARC cumplieron en su totalidad con el cese al fuego unilateral.]

[En este octavo informe se muestra que durante el último mes se registraron **14 agresiones paramilitares,** **7 personas asesinadas, 3 desaparecidas y 41 judicializadas** entre los que se encuentran dirigentes sociales, políticos y defensores de derechos humanos. Así mismo, hubo **un desplazamiento por presencia paramilitar de 150  familias en el río Truandó**, en el departamento del Chocó lo que pone en riesgo este "prolongado y fructífero alto al fuego" como lo expresó el senador e integrante del Frente Amplio por la Paz, Iván Cepeda.]

[También se reveló la presencia paramilitar de grupos que se identifican como "Gaitansitas" y "AUC" (Autodefensas Unidas de Colombia) que en regiones como Chocó, Meta, Norte de Santander, Antioquia (Bajo Cauca Antioqueño), Tumaco (Nariño) y Cauca, se han fortalecido y han conformado bloques armados con la omisión de la fuerza pública y en algunos casos se han identificado "intereses empresariales" que han posibilitado el aumento del paramilitarismo en el país. ]

[Con base en esos datos, las organizaciones sociales hicieron un **llamado al Gobierno Nacional a reconocer el problema del paramilitarismo, y llamarlo por su nombre para poder enfrentarlo.** El mensaje también fue dirigido a los sectores empresariales que continúan financiando este tipo de estructuras.]

[Con el objetivo de salvaguardar el proceso de paz la  mesa veedora propuso la implementación de un **"Teléfono Rojo" para sortear cualquier situación que pueda colocar en riesgo las conversaciones de paz.** Con esta iniciativa se busca tener un canal de comunicación directo entre las  FARC y al Gobierno ante cualquier eventualidad de tipo militar. Así mismo se propuso desarrollar un diálogo territorial en el que las FARC-EP y la fuerza pública tomen medidas para desescalar el conflicto y las afectaciones a la población civil.]

**Medidas y recomendaciones para el proceso**

[1- Que el Gobierno Nacional implemente políticas y acciones concretas que lleven al desmote del paramilitarismo, incluyendo investigación y sanciones a funcionarios públicos y miembros de la fuerza pública que estén apoyando y colaborando con estos grupos.]

[2- Que el Gobierno Nacional cumpla con el compromiso de promover el desescalamiento del conflicto armado y se avance en la conformación  de la Subcomisión Técnica designada para acordar los mecanismos del cese al fuego, cese de hostilidades bilateral, definitivos y a la dejación de armas.]

[3- Que cesen los bombardeos y ametrallamientos indiscriminados en los territorios por parte de la fuerza pública como una forma de continuar en el camino al cese bilateral al fuego.]

[4- Que existan garantías reales para la oposición en Colombia, lo que a su vez implica  que haya sanción e investigación de los responsables que se han denunciado, además de los medios de comunicación y sectores políticos.]

[5- Que el derecho a la protesta sea respetado en las calles y en las cárceles del país.]

[6- Que se garantice la protección de defensores y defensoras de derechos humanos y trabajadores y trabajadoras por la paz.]

[7- Que la comunidad internacional mantenga y fortalezca su papel de observación en estos momentos de tensión del proceso de paz.  ]

[ ]{lang="es"}[8 Informe de Veeduria Cese Unilateral](https://es.scribd.com/doc/289481343/8-Informe-de-Veeduria-Cese-Unilateral-Se-Necesita-Cese-Bilateral-Al-Fuego-y-Garantias-Reales-de-No-Repeticion "View 8 Informe de Veeduria Cese Unilateral Se Necesita Cese Bilateral Al Fuego y Garantias Reales de No Repeticion on Scribd")

<iframe id="doc_79147" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/289481343/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="624" height="624" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
