Title: Más medidas integrales y menos accionar policial y militar: ONU
Date: 2020-02-26 17:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: ACNUDH, Desmonte del ESMAD, implementación acuerdos de paz, Militarización, ONU
Slug: onu-mas-medidas-integrales-menos-accionar-policial-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ONU.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ONU-militar.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ONU / Ascamcat

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe de DDHH 2019, realizado por la Oficina del [Alto Comisionado de las Naciones Unidas para los Derechos Humanos (ACNUDH)](https://twitter.com/ONUHumanRights) evaluó la situación de DD.HH. en Colombia entre el 1 de enero y el 31 de diciembre de 2019 expresando su preocupación sobre las agresiones contra defensores de DD.HH, pueblos indígenas, manifestantes y menores de edad en medio del conflicto en especial en zonas rurales pese a un incremento de la presencia militar en las regiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Evidente crisis humanitaria

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El ACNUDH manifiesta su preocupación ante el alto número de asesinatos de indígenas en el Cauca que en 2019, según Medicina Legal, **registró un incremento de casi el 52% en los homicidios de indígenas comparado con 2018,** registrando el asesinato de 66 miembros del pueblo indígena Nasa en el norte del Cauca, incluidas 16 autoridades indígenas que revelan la necesidad de establecer medidas diferenciales adoptadas manconudamente con las autoridades indígenas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Se registraron 36 masacres que implicaron la muerte de 133 personas, la cifra más alta registrada por el ACNUDH desde 2014 afectando especialmente a los departamentos de Antioquia, Cauca y Norte de Santander.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma alerta sobre las continuas agresiones por parte del ELN en **Antioquia, Arauca, Cauca, Chocó, Norte de Santander y Nariño,** el Ejército Popular de Liberación (EPL), en Cauca y Norte de Santander, y grupos conformados por disidencias de las FARC-EP en Caquetá, Meta y Guaviare además de las dinámicas de disputa territorial de otros grupos organizados como las Autodefensas Gaitanistas de Colombia o los Caparros que han puesto en peligro a la población civil en Antioquia, Cauca, Chocó, Córdoba, Guainía, Huila, La Guajira, Magdalena, Nariño, Norte de Santander, Putumayo y Valle del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, alertó sobre un incremento en la utilización de niñas y niños por actores armados y otros grupos violentos, así como su reclutamiento forzado. Además hizo énfasis en el asesinato de tres niñas y cuatro niños como consecuencia de los bombardeos militares contra otros grupos violentos en Caquetá y Guaviare el 30 de agosto.

<!-- /wp:paragraph -->

<!-- wp:image {"id":81300,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![ONU](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ONU.jpg){.wp-image-81300}  

<figcaption>
Foto: @ONUHumanRights  

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Asesinatos contra defensores de DD.HH. se incrementaron cerca del 50%: ONU

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el ACNUDH, se documentaron en el 2019 un total de 108 asesinatos de personas defensoras de derechos humanos, incluyendo 15 mujeres y 2 integrantes de la población LGBTI. Del total de homicidios, un 75% ocurrió en zonas rurales y el 86% en zonas donde la pobreza supera la media nacional, por lo que advierten, no basta con reducir el análisis a una cifra, siendo necesario comprender las causas estructurales de la violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante ello la ONU resalta la necesidad de implementar medidas colectivas de protección y de convocar regularmente la Comisión Nacional de Garantías de Seguridad direccionada al desmantelamiento de grupos criminales sucesores de organizaciones paramilitares, a menudo responsables de los asesinatos de liderazgos sociales e instó al Gobierno a responder **"de manera rápida y efectiva a recomendaciones contenidas en las Alertas Tempranas de Defensoría del Pueblo".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Son suficientes los esfuerzos de la Fiscalía?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el informe de la ONU, se han documentado avances en la investigación en el 55% de los casos ocurridos entre 2016 y 2019. Sin embargo señalan que aún no hay avances para establecer la autoría intelectual de las agresiones. [(Lea también: implementación del acuerdo de paz)](https://archivo.contagioradio.com/voces-difieren-de-las-declaraciones-de-onu-sobre-implementacion-del-acuerdo-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Advierten que aunque la Fiscalía tiene presencia en casi la mitad de los municipios del país aún hay problemas para llegar a zonas rurales lo que afecta directamente su capacidad de garantizar un acceso a la justicia para la población, esta situación se ve reflejada en otras autoridades civiles, no así la presencia militar que fue en aumento durante el 2019. [(Lea también: Postura de la Fiscalía sobre asesinato de líderes distorsiona la realidad de los territorios)](https://archivo.contagioradio.com/fiscalia-asesinato-lideres-distorsiona-realidad-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para agosto de 2019, la Fiscalía afirmaba que había resuelto el 58% de los asesinatos de líderes sociales denunciados, es decir, de 302 casos reportados se habrían resuelto 177, las cifras contrastan con las de la ONU que habla de un 55% de los cuales el 16% tiene sentencia condenatoria, el 20% está en fase de juicio, el 7% con imputación de cargos y el 11% con órdenes de captura vigentes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ONU evalúa primer año del Sistema Integral de Justicia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El ACNUDH que renovó el acuerdo de estadía en Colombia hasta el 31 de octubre de 2022, destacó los avances que ha hecho la Jurisdicción Especial para la Paz como en la recuperación de cuerpos de víctimas de ejecuciones extrajudiciales y desapariciones forzadas en el cementerio de Las Mercedes en Dabeiba, Antioquia en cooperación con la Unidad de Búsqueda de Personas dadas por Desaparecidas, así mismo resaltó la contribución que está haciendo la Comisión de la Verdad al recolectar cerca de 5.500 testimonios de víctimas y sus familiares para una mayor comprensión del conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Restringir el uso de la militarización

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

También se observó un incremento en la respuesta militar a situaciones de violencia e inseguridad, pese a que existen protocolos que regulan la participación éstos no fueron aplicados completamente por lo que insta al Estado a restringir, en la mayor medida posible el uso del Ejército en situaciones relacionadas con la seguridad ciudadana, incluida la protesta social.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Episodios como el de Dimar Torres, excombatiente asesinado por miembros del Ejército se suman al total de 15 casos de presuntas privaciones arbitrarias de la vida en 10 departamentos, la cifra más alta registrada por este organismo desde 2016. **En 13 de estos casos, las muertes habrían sido causadas por el uso innecesario y/o desproporcionado de la fuerz**a, la mayoría en contextos de operaciones militares relacionadas con seguridad ciudadana. Enfatiza además que estos casos deben mantenerse bajo la competencia de la justicia ordinaria y no de la justicia penal militar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ONU pide transformar al ESMAD

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, en el marco del Paro Nacional, la ONU se refirió a la evidente y desproporcionada manera de la Fuerza Pública de "resolver incidentes aislados de violencia" **incumpliendo con estándares internacionales relacionados con el uso de la fuerza**, hecho que escaló con el asesinato de Dylan Cruz, cometido por un agente del ESMAD armado con un rifle calibre 12 de munición beanbag. [(Lea también: En veinte años el ESMAD ha asesinado a 34 personas)](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto y teniendo en cuenta otros actos contra manifestantes que podrían llegar a constituir tortura, tales como desnudez forzada, golpizas, "amenazas de muerte con matices racistas", la ONU recomienda iniciar investigaciones exhaustivas,relacionadas al uso excesivo de la fuerza por parte del ESMAD durante las recientes protestas sociales y a iniciar una transformación que permita una revisión de sus protocolos sobre el uso de la fuerza y de las armas y municiones menos letales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Familias si han cumplido pero el Estado no

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en 2018 se formularon 16 Programas de Desarrollo con Enfoque Territorial (PDET) que contaron con altos niveles de participación de las comunidades, para el 2019, la ONU observó pocos avances en este y otros planes como la Reforma Rural Integral, el Plan de Reparación Colectiva y el Programa Nacional Integral para la Sustitución de Cultivos de Uso Ilícito (PNIS).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contravía al PNIS, el Gobierno publicó un proyecto el 30 de diciembre para reanudar las aspersiones aéreas con glifosato a fin de erradicar los cultivos de uso ilícito, ante la preocupación de organizaciones como el Comité de Derechos Económicos Sociales y Culturales y de las comunidades quienes han alertado sobre las consecuencias negativas que traerían de regreso las fumigaciones aéreas en la seguridad alimentaria y la salud, el informe **destaca que el 95% de las familias que participan en el PNIS han cumplido mediante la erradicación voluntaria y sólo el 0.4% retornó a los cultivos de uso ilícito, evidenciando la funcionalidad de esta primera etapa de sustitución.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
