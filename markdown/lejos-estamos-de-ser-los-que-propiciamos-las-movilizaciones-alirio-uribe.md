Title: ''Lejos estamos de ser los que propiciamos las movilizaciones'': Alirio Uribe
Date: 2016-03-01 14:52
Category: Nacional, Política
Tags: alirio uribe, manifestación vendedores Bogotá
Slug: lejos-estamos-de-ser-los-que-propiciamos-las-movilizaciones-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Vendedores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo Radicales Libre ] 

<iframe src="http://co.ivoox.com/es/player_ek_10633674_2_1.html?data=kpWjlZiae5Whhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYammIqfmbHJrtDnjMrg1sbRs9SfxcqY1crWb83j1JDe18qPtNPj0c7Qy8bRs9SfzcbgjdLTusrgyt%2FOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alirio Uribe, Polo Democrático] 

###### [1 Mar 2016 ] 

Como "un juego de parte de los medios de comunicación al alcalde Peñalosa'' calificó el representante Alirio Uribe las afirmaciones de Semana sobre la responsabilidad del Polo Democrático, el Partido Verde y el Progresismo en las [[movilizaciones de los vendedores informales](https://archivo.contagioradio.com/vendedores-informales-exigen-ser-incluidos-en-el-plan-de-desarrollo-de-bogota/)]. “**Es tendencioso que Semana y otros medios de comunicación pretendan decir que no hay descontento frente a las políticas de Peñalosa**, sino que lo que hay son personas malintencionadas. Hay tergiversación y lo que se quiere es desviar el problema''.

''No negamos que nosotros hemos hecho audiencias, hemos hechos denuncias, hemos solicitado que se abra una mesa de interlocución con los vendedores ambulantes, pero lejos estamos de ser los que propiciamos las movilizaciones'' asegura Uribe, e insiste en que las **manifestaciones son concertadas por las organizaciones de vendedores informales que se ven agredidas por las políticas de la actual alcaldía**.

De acuerdo con Alirio Uribe resulta "absurdo'' no reconocer que los miles de vendedores informales representan el 71% de la tasa de empleo en Bogotá, una ciudad con altos indices de desempleo y ''en la que la gente sale a buscarse la vida de manera honesta y digna'', por lo que **se hace necesaria ''una mesa de concertación para que se articulen dos derechos, el del espacio público y el que tiene la gente a trabajar''**, teniendo en cuenta, que la mayoría de vendedores ambulantes hacen parte de la población desplazada proveniente de distintas regiones del país.

''Cuando el aparato productivo de la ciudad genere empleos dignos seguramente la gente no tendrá que rebuscarse la vida en las calles de la ciudad'', asegura Uribe quien insiste en que esta problemática no puede ser atendida con represión ni ''tampoco diciendo que hay políticos detrás de las movilizaciones''. **Lo que se requiere es que la administración establezca la magnitud del problema, escuche a los vendedores y consolide un censo**. ''El espacio público es de todos y obviamente hay que racionalizarlo, pero estamos ante un drama social evidente en todas las localidades''.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)]. ] 
