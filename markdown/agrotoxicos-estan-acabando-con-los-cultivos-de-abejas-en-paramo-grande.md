Title: Agrotóxicos están acabando con los cultivos de abejas en Páramo Grande
Date: 2015-12-16 17:45
Category: Voces de la Tierra
Tags: abejas, agroquímicos, animales en peligro de extinción, páramos
Slug: agrotoxicos-estan-acabando-con-los-cultivos-de-abejas-en-paramo-grande
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Abejas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.semabelhasemalimento.com.br]

<iframe src="http://www.ivoox.com/player_ek_9749231_2_1.html?data=mpyhm5eXdY6ZmKiak5yJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw5CmudPVyNfcjc3Fb8LXwsfOxtSPp9DijJaikpDHs83hxtPO1ZDIqYzVw8rXw9iPqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [16 Dic 2015] 

La comunidad de apicultores del municipio de Guasca, en el departamento de Cundinamarca, denuncia que las** fumigaciones indiscriminadas** con fuertes químicos tóxicos a los cultivos de papa en el páramo grande, están acabado con la vida de más de 150 colmenas de abejas.

Apicultores de la zona, aseguran el problema inició cuando se empezó a realizar las fumigaciones a los cultivos de papa y zanahoria en el mes de agosto de 2014, luego repitieron esta acción en marzo de 2015 y la última vez que lo hicieron fue en el mes de octubre. Así mismo, el lider campesino indica que esta situación se repite en Guatavita, Facatativá, Nemocón, Tocancipá, y en los departamentos de Tolima, Huila y Santander.

Además de acabar con la vida de las abejas, el páramo en general, las mariposas y demás insectos, las frutas, los bosques, las fuentes hídricas, los pájaros e incluso las vacas han resultado contaminados por este tipo de químicos, según afirman los campesinos de la zona y la Red Tejiendo Páramos.

Pese a que se han realizado las denuncias pertinentes a las autoridades ambientales, tanto el **ICA (Instituto Colombiano Agropecuario), la CAR (Corporación Autónoma Regional de Cundinamarca), como Corpoguavio,** no han atendido la situación, argumentado que se trata de un fenómeno producto del cambio climático, sin embargo, algunos campesinos han decidido tomar muestras de laboratorio de las abejas muertas y de las vacas afectadas, y han encontrado que los animales tienen químicos altamente tóxicos en sus cuerpos, razón por la que la personería ya hizo el llamado de alerta a las empresas.

Estas extensas  nubes de veneno estarían matando abejas que se encuentran a 5 kilómetros de distancia, lo cual para ellos es “catastrófico (…) esto se va a convertir en un desierto, **estamos encontrando miles de abejas muertas, a veces a algunas las encontramos temblando moribundas”.**

Johana Gonzáles, integrante de la Red Tejiendo Páramos, hace un llamado de atención reflexionando sobre a la importancia de las abejas para la existencia del mundo, pues como ella lo explica, este tipo de animales son “**polinizadores de la vida, a nivel mundial las abejas están en peligro de extinción si ellas se extinguen se acaba la vida del planeta,** pues son un proceso viviente de restauración ecológica de bosques”, concluye la ambientalista.

https://www.youtube.com/watch?v=gVYcDGRIGg4
