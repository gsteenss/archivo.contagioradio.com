Title: Denuncian impunidad en caso de Feminicidio en Marinilla, Antioquia
Date: 2019-05-06 11:51
Author: CtgAdm
Category: Mujer, Nacional
Tags: Antioquia, Claudia Soto, feminicidios, impunidad, Marinilla
Slug: denuncian-impunidad-en-caso-de-feminicidio-en-marinilla-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/59337378_522581854938130_7221355529556197376_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo Memoria Femenina 

###### o6 May 2019 

El pasado  6 de febrero, Claudia Milena Soto Gómez fue víctima de feminicidio a manos de Héctor Alexander Giraldo, un hombre que vivía en su vereda en el municipio de Marinilla, y que desde hace varios años la acosaba sexualmente. De acuerdo con familiares de Soto, la mujer ya había acudido en varias ocasiones a la Fiscalía para denunciar al hombre, sin embargo, nunca se tomaron medidas en contra de él. **Además, el agresor tampoco sería procesado por feminicidio.**

Blanca Rivera, sobrina de Claudia, relata que existen tres documentos que evidencian las denuncias interpuestas en contra de Giraldo, y asegura que pese a haber acudido a varias entidades, **en todas les manifestaron que no existían las pruebas suficientes para tomar acciones. [(](https://archivo.contagioradio.com/violencia-de-genero-seguira-sino-hay-compromiso/)**Le puede interesar: "Violencia de género seguirá sino hay compromiso institucional")

"Él hacía varios años se había obsesionado sexualmente con mi tía. El empezó desde que ella era muy joven a acosarla, le decía que estaba muy buena, que para hacerle muchas maldades. La perseguía en la carretera cuando ella salía a trabajar, la esperaba en el camino, le decía muchísimas cosas" afirma Rivera.

De igual forma, Rivera expresa que Giraldo ya había comenzado a acosar sexualmente a la hija de Claudia, "él le decía a ella, incluso a la misma niña, que a ella ya le cabía también" afirma y agrega que en otras ocasiones Giraldo señaló que **"violaría y mataría"** a Claudia y su hija.

### **La impunidad sobre el feminicidio ** 

La familia de Claudia denunció que en el escrito de acusación, formulado por la Fiscal, se le estaría imputando a Giraldo cargos por homicidio agravado y no por feminicidio agravado. "Él hizo eso contra mi tía por el hecho de ser mujer, **porque se creía superior a ella, porque se creyó dueño del cuerpo de ella"**.

Asimismo, temen que Giraldo logre salir de la cárcel y tome represalias en contra de la hija de Claudia Soto, acabando también con la vida de la menor de edad. Por esta razón, tanto la familia como diversos colectivos y organizaciones defensoras de derechos humanos realizaron un plantón hoy 6 de mayo a las 11 de la mañana, en frente las instalaciones de la Fiscalía en Marinilla, para exigir una nueva decisión por parte de la Fiscal y evitar la impunidad sobre este crimen.

<iframe src="https://www.youtube.com/embed/vuubkd62hrw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
