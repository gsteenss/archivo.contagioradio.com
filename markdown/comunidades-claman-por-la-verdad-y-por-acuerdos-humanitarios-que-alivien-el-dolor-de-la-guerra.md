Title: Comunidades claman por la verdad y por acuerdos humanitarios que alivien el dolor de la guerra
Date: 2020-07-22 10:08
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Comisión de la Verdad, ELN, Empresarios, JEP
Slug: comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/festival-de-las-mermorias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comunidades / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luego de otras siete comunicaciones y a propósito de la conmemoración del Día de la Independencia, más de 110 comunidades y procesos de diferentes regiones del país expresaron a través de una [carta](https://www.justiciaypazcolombia.com/carta-abierta-8-otro-grito-por-la-independencia-en-la-paz-con-justicia-socio-ambiental/) un mensaje que clama por reforzar los esfuerzos por la consecución de la paz en los territorios y a todos los sectores que tienen un compromiso con esa meta, incluidos la Jurisdicción Especial para la Paz, la Comisión de la verdad, militares, empresarios, bancada de paz, ELN y otros grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La carta destaca la importancia de escuchar un clamor que, pese al asesinato de al menos 971 defensores de DD.HH. y líderes sociales después de la firma del Acuerdo, aún se escucha a través de su memoria, la vida de los más de 200 excombatientes asesinados, los "más de 80 mil desaparecidos en esta última guerra de más de 60 años" y en la voz de quienes aún defienden su territorio en medio del recrudecimiento del conflicto. [(Lea también: 971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz)](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El objetivo de destruir el tejido de un sueño integral y transversal de paz está ahí desde antes y en la firma del Acuerdo con las FARC, en la mesa con el ELN, y en el acogimiento fracasado de las AGC. (...) Se sigue alentando la guerra y dada la situación de pandemia está se ha ido cualificando

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### CEV y JEP deben persistir en enunciar sus verdades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La carta expresa a su vez su respaldo a la Jurisdicción Especial de Paz (JEP) y al trabajo que vienen realizando, aunque reconoce que su labor seguirá siendo cuestionada por algunos sectores políticos, empresariales y de la sociedad "que están lejos de comprender y aceptar su labor por miedos e intereses mezquinos", esta debe continuar de forma diligente dando prioridad a las víctimas y a que tanto estas como el país, conozcan la verdad del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, advierten que hay decisiones de la JEP "realmente incomprensibles para nosotros las víctimas. Las expectativas de conocer verdades se ven frustradas para nosotros cuando vemos que muchos solicitantes con voluntad cierta de enunciar verdades son rechazados". [(Lea también: La JEP le cierra la puerta a Salvatore Mancuso)](https://archivo.contagioradio.com/la-jep-le-cierra-la-puerta-a-salvatore-mancuso/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, la Comisión de Esclarecimiento de la Verdad (CEV), fue alentada a concentrar sus esfuerzos territoriales en convivencia, garantías de no repetición, pidiendoles que en lo posible, amplien el período de su mandato por lo menos un año más, pues "la virtualidad está lejos de suplir el contacto directo que requieren nuestros territorios". [(Le puede interesar: Comunidades que han vivido la guerra en sus territorios respaldan la voz de Dario Monsalve)](https://archivo.contagioradio.com/comunidades-que-han-vivido-la-guerra-en-sus-territorios-respaldan-la-voz-de-dario-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Los firmantes reconocieron los esfuerzos de la bancada de paz del Congreso señalando la importancia de proyectos de ley como la Renta Básica y otros** ligados a la protección ambiental, "sabemos que seguirán ejerciendo el control político y les llamamos que adopten decisiones para que estén en nuestros territorios. Estamos silenciados, padecemos la crisis de la paz con nuevas violencias y necesitamos que nos escuchen en nuestros territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un mensaje de las comunidades a los actores armados, legales e ilegales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades firmantes se dirigieron a la Fuerza Pública pidiéndoles "actuar con el respeto de los DD.HH., el derecho humanitario y el derecho a la paz", con respecto a las continuos señalamientos y acciones por parte del Ejército y la Policía en contra de los procesos comunitarios, en particular de los operativos de erradicación forzada y las más recientes denuncias hechas por parte de comunidades indígenas que señalan a militares de la institución de abusar sexualmente de menores de edad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A la guerrilla del ELN se le invitó a persistir en la paz y a evaluar todas las "practicas que están en contravía de sus enunciaciones" y a que respeten la la autonomía de las comunidades y se conozcan sus iniciativas de paz territorial. [(Le puede interesar: ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibilidad)](https://archivo.contagioradio.com/wp-admin/post.php?post=86497&action=edit)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El mensaje también se extendió a quienes se identifican como FARC Segunda Marquetalia para que persistan "en la búsqueda de la paz y del diálogo para construir paz y mientras se alcanza la ventana a un diálogo, centren sus esfuerzos en concertar Acuerdos Humanitarios, reconocer las iniciativas de las comunidades y abstenerse de acudir al reclutamiento de menores de edad, un mensaje similar se hizo a grupos como las autodenominadas Autodefensas Gaitanistas de Colombia y otras expresiones de violencia rural y urbana se hizo un llamado a "respetar la autonomía de los procesos locales y regionales, a respetar la vida e integridad de todos los civiles".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
