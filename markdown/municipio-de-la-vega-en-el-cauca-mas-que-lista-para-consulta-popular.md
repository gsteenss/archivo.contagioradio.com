Title: La Vega en Cauca lista para empezar proceso de consulta popular
Date: 2017-07-26 13:42
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, Cauca, consulta popular, la vega, macizo colombiano, Mineria
Slug: municipio-de-la-vega-en-el-cauca-mas-que-lista-para-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/vega-cauca-e1501094518847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Youtube] 

###### [26 Jul 2017] 

Las comunidades del municipio de La Vega, ubicado en el Macizo colombiano en el departamento del Cauca, **adelantan los procesos necesarios para realizar la consulta popular contra los proyectos de megaminería**. Según los habitantes de La Vega, el 80% del territorio del macizo está en proceso de concesión para obtener licencias ambientales de explotación de diferentes minerales.

De acuerdo con Óscar Salazar, miembro del Proceso Campesino Popular de La Vega, desde 2003 han empezado a construir un movimiento social que les ha permitido **“contener las multinacionales que han querido hacer exploraciones mineras”**. También afirmó que “el caso de la exploración en el macizo colombiano no ha tenido eco y sus efectos son devastadores”.

### **¿Qué es el macizo colombiano?** 

Para Salazar, **el macizo colombiano se encuentra ubicado en el sur oriente del país y comprende 6 municipios**. Allí, se divide la cordillera de los Andes en la Oriental y la Central. De igual manera, es un territorio por el cual pasan los ríos de mayor importancia para Colombia como los son el Magdalena, el río Cauca y el río Patía. (Le puede interesar: ["Morelia Caquetá busca consulta popular contra hidrocarburos")](https://archivo.contagioradio.com/morelia-caqueta-alista-proceso-para-consulta-popular-contra-hidrocarburos/)

Salazar manifestó que, con la explotación minera allí, se verían afectados estos ríos y en particular el Patía que es reserva hídrica para estas comunidades y **hace parte de la reserva biosfera de la humanidad establecida por la Unesco**. Ante esto, el ambientalista indicó que “queremos proteger el agua que sirve para los proyectos agrícolas y el consumo humano, es claro que sin agua no puede haber minería”.

### **Empresas de megaminería en el territorio** 

Salazar manifestó que las empresas que tienen títulos mineros para realizar exploraciones y explotación en el territorio del macizo colombiano son **la Anglo Gold Ashanti, Carbo Andes y Continental Gold.** Estas empresas, según Salazar, tienen títulos desde 2013 y han pasado de tener 33 a 8 títulos, sin cambio alguno en el área dispuesta para la exploración de minerales. (Le puede interesar: ["Sibaté prepara consulta popular contra la minería"](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/))

Además indicó que “hay un proyecto de Continental Gold que comprende **26 mil hectáreas del territorio, al lado Anglo Gold Ashanti tiene 36 mil héctareas** y Carbo Andes dispone de 3.400 hectáreas para realizar proyectos mineros”.

Además, afirmó que la comunidad **no ha podido hacer los controles y seguimientos pertinentes a estas empresas** porque a medida que cambian los títulos, las placas que los identifica también han cambiado y en cola hay 60 solicitudes de concesión minera.

### **Procesos de pedagogía de apropiación del territorio en La Vega** 

Para que las comunidades comprendan las afectaciones que ocasionan estos proyectos, **ellos mismos han desarrollado procesos de pedagogía como el estudio cartográfico.** Salazar manifestó que “por medio de mapas las personas pueden saber cuál es la ubicación de los proyectos de minería con respecto a los ríos que sirven para surtir agua”. (Le puede interesar: ["Tres municipios del Tolima se sumarían a consultas populares"](https://archivo.contagioradio.com/tres-municipios-del-tolima-se-sumarian-a-consultas-populares/))

Así mismo, aseguró que estos procesos pedagógicos le han servido a la comunidad para generar **“conciencia y apropiación por el territorio”**. Ellos han desarrollado el Plan Aurora “que es una política popular de protección colectiva del territorio donde la gente se apropia del agua y son capaces de evidenciar las amenazas que significan los títulos mineros”.

Finalmente, la comunidad de la Vega está decidiendo **los términos de la pregunta de la consulta popular para empezar el proceso de recolección de firmas**. En conjunto con la Cumbre Agraria, revisarán los títulos de minería para estudiar los procedimientos bajo los cuales se entregaron los títulos mineros. Esperan que a finales de este año puedan realizar la consulta.

<iframe id="audio_20014702" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20014702_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
