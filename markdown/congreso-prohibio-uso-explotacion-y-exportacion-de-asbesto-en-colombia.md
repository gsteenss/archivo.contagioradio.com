Title: Congreso prohibió uso, explotación y exportación de asbesto en Colombia
Date: 2019-06-12 14:58
Author: CtgAdm
Category: Ambiente, Política
Tags: asbesto, ley ana cecilia niño
Slug: congreso-prohibio-uso-explotacion-y-exportacion-de-asbesto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/D8zcp4NXYAADa-C.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Greenpeace Colombia] 

Después de 11 años de esfuerzos y intentos fallidos, el Congreso de la República aprobó el proyecto de ley "Ana Cecilia Niño", que busca prohibir el uso, explotación y exportación del asbesto en Colombia. Ahora, la iniciativa tendrá que pasar por conciliación entre las dos cámaras de esta corporación y recibir sanción presidencial para quedar como ley.

"Esto es un gran homenaje a las víctimas que desafortunadamente tuvieron que dejar su vida para que ahora entendamos la importancia de prohibir ese tipo de sustancias", expresó Silvia Gomez, directora de Greenpeace Colombia, en referencia a la decisión de la Cámara de Representantes de aprobar el proyecto ayer.

Según el Instituto Nacional de Cancerologia, la inhalación o manipulación del asbesto causa **cáncer de pulmón, laringe, ovario y mesotelioma, así como asbestosis**. Mientras tanto, un estudio de la Contraloría afirmó  que **este material produce 540 casos de cáncer de pulmón** en el país cada año. (Le puede interesar: "[Histórico respaldo en Congreso a prohibición del asbesto](https://archivo.contagioradio.com/historico-respaldo-en-congreso-a-eliminacion-de-asbesto/)")

El proyecto no afecta a los materiales de asbesto ya instalados sino prohíbe el uso, comercialización, importación y distribución de este material a partir del 1 de enero de 2021. Además, impide la explotación minera y la exportación del asbesto a pesar de esfuerzos de preservar estas industrias.

<iframe id="audio_37031530" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37031530_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
