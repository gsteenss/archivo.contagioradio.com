Title: 33 Eurodiputados piden que inicie fase pública entre ELN y gobierno
Date: 2016-07-14 16:34
Category: Otra Mirada, Paz
Tags: ELN, Gobierno, Parlamento Europeo, paz
Slug: 33-eurodiputados-piden-que-inicie-fase-publica-entre-eln-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/dialogos-Eln-y-gobierno-e1468532020926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Somos la Revista 

###### [14 Jul 2016]

Un grupo de 33 eurodiputados, envíaron una carta al gobierno del presidente Juan Manuel Santos y al Jefe de la delegación de Paz del ELN, Antonio García, con el objetivo, de promover el inicio de la fase pública de negociación de paz que ya había sido anunciada el pasado 30 de marzo.

**La carta reitera el** respaldo del parlamento europeo a este escenario entre el gobierno y el ELN en Ecuador, asegurando que los parlamentarios celebran estos pasos que se vienen dando para la construcción de la paz de Colombia.

Los eurodiputados aseguran en la carta que los diálogos con el ELN contribuirán a superar el conflicto armado, permitiendo “ganar confianza a la sociedad colombiana en el proceso y a las partes entre sí”. Así mismo, resaltan la importante labor de los defensores de derechos humanos, como reiteramos pieza clave para fortalecer la democracia en Colombia.

[![carta 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/carta-1.jpg){.aligncenter .size-full .wp-image-26479 width="1131" height="1599"}](https://archivo.contagioradio.com/33-eurodiputados-piden-que-inicie-fase-publica-entre-eln-y-gobierno/carta-1/) [![carta 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/carta-2.jpg){.aligncenter .size-full .wp-image-26480 width="1131" height="1599"}](https://archivo.contagioradio.com/33-eurodiputados-piden-que-inicie-fase-publica-entre-eln-y-gobierno/carta-2-2/)
