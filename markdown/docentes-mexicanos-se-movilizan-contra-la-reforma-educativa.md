Title: Docentes mexicanos se movilizan contra la reforma educativa
Date: 2015-10-19 15:19
Category: Educación, El mundo
Tags: Ayotzinapa, Coordinadora Nacional de trabajadores de la Educación, Marcha docentes en México, Movilziación contra gobierno de Peña Nieto, Reforma educativa en México
Slug: docentes-mexicanos-se-movilizan-contra-la-reforma-educativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-en-Mexico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:chiapasparalelo.com 

###### [19 oct 2015]

Frente a la **reforma educativa propuesta por el gobierno mexicano**, la Coordinadora Nacional de Trabajadores de la Educación (CNTE) continúa en movilizaciones desde el pasado 12 de octubre, con las cuales **se busca impedir la evaluación docente** que realizará la Secretaría de Educación Pública, que es contraria a las acciones para mejorar la educación y limita la calidad de vida de maestros.

Los maestros de la sección 22 del Sindicato Nacional de Trabajadores de la Educación (SNTE) proponen **bloquear los centros comerciales** durante 48 horas en el estado de Oaxaca, donde estaría el INEE (Instituto Nacional para la evaluación de la educación en México), los días 14 y 15 de noviembre.

Las protestas continúan pese a que la **Secretaría de Educación Pública (SEP) informó que sancionará** a 85 mil 296 docentes que han protagonizado una jornada de protesta el pasado lunes, la SEP, adelanto que les **descontará el día a los educadores** que suspendieron labores. Oaxaca es el estado con mayor número de profesores participantes en el paro, con 53 mil 978, y le siguen Michoacán, 16 mil 400; Guerrero, 5 mil 368, y Chiapas, 4 mil 602.

De esta forma la Coordinadora Nacional de trabajadores de la Educación **acordó marchas regionales** como parte de su apoyo por las jornadas de Acción Global por Ayotzinapa y la continuación de la búsqueda de los 43 desaparecidos de Iguala, causa que ha sido emblemática en las distintas movilizaciones contra el gobierno de Peña Nieto.
