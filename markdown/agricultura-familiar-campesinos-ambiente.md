Title: Agricultura familiar por la dignidad del campo y la protección ambiental
Date: 2019-03-18 17:50
Category: Voces de la Tierra
Tags: agricultura campesina, Ambiente, soberanía alimentaria
Slug: agricultura-familiar-campesinos-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Jobs-saved-at-fruit-and-vegetable-distributor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Foodmanufacture.co 

###### 14 Mar 2019 

La agricultura familiar es una propuesta alternativa para producir alimentos en entornos urbanos **¿Qué es y cómo podría ayudar a recuperar la dignidad de nuestros campesinos y proteger los bienes naturales?** ¿Cómo aporta a la soberanía alimentaria y a la integración del contexto rural campesino y la ciudad?.

Nos acompañaron en Voces de la tierra **Arlex Angarita**, campesino, psicólogo social comunitario, agroecologo, integrante de la Red Nacional de Agricultura Familiar (RENAF); **Fernando Castrillón**, ingeniero agrónomo, experto en economía solidaria y formación. Vienen con ellos a contar sus experiencias **Pedro Vicente González**, de la Asociación Red Agroecologica ARAC de Subachoque; **Andrea Murillo**, hace de un modelo de empresa familiar de Guasca, Cundinamarca y **Dana Avila**, productora e integrante de una organización campesina de Viota, Cundinamarca.

<iframe id="audio_33483077" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33483077_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 
