Title: Continúa dilación en caso del defensor David Ravelo Crespo
Date: 2016-11-29 16:33
Category: DDHH, Nacional
Slug: continuan-las-dilaciones-en-caso-de-david-ravelo-crespo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/DavidRavelo2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Barrancabermeja virtual] 

###### [29 Nov 2016] 

La Fiscalía Tercera Especializada de la Unidad contra los falsos testigos, demostró que Mario Jaimes Mejía, alias “El Panadero” **mintió al acusar al defensor de derechos humanos David Ravelo Crespo, quien fue condenado el 8 de octubre de 2013 a 18 años de cárcel**, por su presunta participación en el asesinato de David Núñez Cala, ex secretario de Obras Públicas de Barrancabermeja.

El Colectivo de Abogados José Alvear Restrepo, que ha venido acompañando este caso, denunció mediante un comunicado que Ravelo “es víctima de un montaje criminal orquestado por quienes antes pretendieron asesinarlo, **los paramilitares y el terrorismo de Estado, mediante el cual lo condenaron por un homicidio que no cometió”.**

En la misiva el CAJAR explica que los lazos paramilitares presentes en las entidades estatales han dilatado la solución del caso Ravelo debido a que en su momento el defensor denunció **“las atrocidades cometidas por los paramilitares y una reunión secreta entre el presidente Álvaro Uribe Vélez con paramilitares de Barrancabermeja,** en el municipio de Puerto Berrío, Antioquia”.

El 26 de mayo de 2015, el Juzgado Noveno Penal del Circuito de Bucaramanga realizó una audiencia preparatoria para el juicio contra alias “El Panadero” por los cargos de **falso testimonio en el caso Ravelo y “mentirle al país” sobre el caso de la periodista Jineth Bedoya,** sin embargo aún después de que el Tribunal de Justicia y Paz le expulsara de la ley 975, el paramilitar sigue gozando de privilegios en el patio 6 de la cárcel Modelo.

### **La ley del embudo ** 

Desde entonces el circuito penal de Barrancabermeja en cabeza de distintos jueces, han aplazado en seis ocasiones el juicio de Mario Jaimes “El Panadero”, la última ocasión siendo el pasado 27 de octubre, pues según informó el director de la cárcel Modelo de Bucaramanga, Teniente Henry Mayorga al Juez Noveno Penal del Circuito de Bucaramanga **“no lo llevé a la audiencia porque ‘El Panadero’ no quiso salir”.**

Por otra parte el Colectivo manifiesta que “el director del centro de reclusión estaba en la obligación de formalizar la respuesta del detenido a través de un escrito para que de este modo el juez correspondiente pudiera realizar la audiencia sin la presencia de Jaimes”. Aclaran en el comunicado que **“el debido proceso no se afecta cuando se realizan audiencias con procesados ausentes, cuando estos han manifestado expresamente su voluntad de no comparecer”.**

Exigen al INPEC que emita una directiva que “obligue a los directores de las cárceles a formalizar por escrito las razones por las cuales un preso no es trasladado a una audiencia a la que es citado, y **que no se considere suficiente el informe verbal”.**

Por último instan a la Procuraduría General de la Nación a evitar la continuidad de las dilaciones en el caso del defensor de derechos humanos, a que se investigue si el director de la cárcel modelo de Bucaramanga ha incurrido en falta disciplinaria por los hechos ocurridos y hacen un llamado a la comunidad nacional e internacional de protección y defensa de los derechos de David Ravelo **“quien lleva más de seis años injustamente condenado, víctima de un montaje criminal aún con las pruebas demuestran su absoluta inocencia”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
