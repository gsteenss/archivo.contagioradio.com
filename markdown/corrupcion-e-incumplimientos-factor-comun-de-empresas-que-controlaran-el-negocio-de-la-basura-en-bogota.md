Title: Corrupción e incumplimientos, factor común de empresas que controlarán el negocio de la basura en Bogotá
Date: 2018-01-10 14:41
Category: Nacional
Tags: Alcaldía de Bogotá, basuras Bogotá
Slug: corrupcion-e-incumplimientos-factor-comun-de-empresas-que-controlaran-el-negocio-de-la-basura-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/basuras1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [9 Ene 2018] 

Por un contrato de 4.8 billones de pesos y a 5 años, son cinco las empresas privadas que se encargarán de manejar las basuras de Bogotá. Se trata de tres empresas extranjeras y dos nacionales escogidas por el Comité Evaluador de la Unidad Especial de Servicios Públicos. Todas ellas han tenido algún tipo de sanción y han tenido que pagar millonarias sumas de dinero

**Promoambiental Distrito,  Lime, Ciudad Limpia, Promesa de ESP Futura Bogotá Limpia yPSF Área Limpia, algunas de ellas cuentan con un historial de corrupción.**

La licitación contempla, además, una modernización de la flota de camiones compactadores; el retiro de pendones, avisos y pasacalles ilegales; la remoción de grafitis; limpieza del mobiliario urbano y la renovación de canecas para atender desechos de fumadores y propietarios de mascotas. Sin embargo no especifica la labor de reciclaje y la vinculación de la población recicladora a ese modelo.

Aquí le hacemos una breve descripción de cada empresa.

### **1. Promoambiental Distrito** 

Fue sancionada por la Superintendencia de Servicios Públicos en julio de 2014 por los dobles cobros de aseo a usuarios en Cali, la multa de \$50 millones a Promoambiental, por facturar el componente de barrido a suscriptores con los cuales no tiene contrato del servicio de aseo. Por esta situación por 4 años, un total de 2200 usuarios se vieron afectados y habrían pagado alrededor de \$580 millones de más por el servicio de aseo en esa ciudad.

Es un consorcio colombo-chileno, constituido por la empresa colombiana Aseo Regional S. A. E.S.P., y las chilenas Empresa Nacional de Servicios de Aseo (Enasa) y Dimensión Sociedad Anónima. Su representante legal es el ex superintendente de Servicios Públicos Diego Humberto Caicedo.

Estará encargada del servicio de aseo de la zona 1: **San Cristóbal, Usaquén, Chapinero, Candelaria, Usme y Sumapaz. **De acuerdo con una investigación desarrollada por Noticias UNO, Promoambiental hará un aporte voluntario a la administración distrital por 35 mil millones de pesos.

### **2. Empresa Metropolitana LIME ** 

Para agosto del 2017, fue noticia que la Contraloría General condenó fiscalmente a seis exdirectivos de la Unidad Administrativa Especial de Servicios Públicos (UAESP), los Consesionarios, entre los que se encontraba LIME y a la Sociedad Fiduciaria Bancolombia, ya que se les había encontrado como responsables dentro de dos procesos de responsabilidad fiscal por una cuantía de \$59.700 millones en más de 400 contratos que no tenían relación con la prestación del servicio de aseo.

LIME es una de las empresas extranjeras de dueños argentinos. Ha trabajado especialmente en Bogotá. Se trata de una compañía que hace parte del conglomerado argentino, Pescarmona que en 2016, cesó pagos en Argentina. Había entrado en Default en 2014, por una deuda de casi US\$1.400 millones.

Por otra parte, de acuerdo con un artículo de las '2 Orillas' del 2013, desde principios de los años noventa esta empresa comenzó a concesionar millonarios contratos con el Distrito.  En el año 2003, ganaron una licitación para prestar el servicio en siete localidades: Suba, Usme, Rafael Uribe, Antonio Nariño, San Cristóbal y Usaquén, una cobertura superior al 45% del total de la ciudad, pero tras la llegada de Aguas Bogotá,  LIME bajó sus ingresos, pasando de recibir utilidades por más de \$60.000 millones a obtener \$46.800 millones.

Al haber ganado la reciente licitación, esta vez dicho operador estará encargado de manejar las basuras de las **localidad de Ciudad Bolívar, Puente Aranda, Tunjuelito, Antonio Nariño, Mártires, Teusaquillo y Bosa;** además, según Noticias UNO, se comprometió a consignar a la Alcladía de Bogotá, una suma de \$59.577 millones.

### **3. Ciudad Limpia** 

Este operador colombiano hace parte también de las compañías pertenecientes de la familia vallecaucana Losada, propietarios de uno de los más poderosos conglomerados en Colombia, Fanalca, cuyo presidente es Joaquín Losada.

El operador que estará **a cargo del aseo de las localidades de Kennedy y Fontibón,** también estuvo inmerso en la condena que profirió la Contraloría a seis exdirectores y cinco funcionarios técnicos de la (Uaesp), cuatro operadores de aseo y una empresa fiduciaria que tuvieron que responder por detrimento patrimonial en el manejo de las tarifas de aseo de Bogotá.

Asimismo, esta empresa se ha comprometido con la administración distrital con una contribución voluntaria por \$34.000 millones de pesos.

### **4. Bogotá Limpia ** 

La compañía que se hará cargo de las basuras de la zona 4 compuesta por las localidades de** Engativá y Barrios Unidos, hace parte de la sociedad financiera ecuatoriana Hidalgo & Hidalgo**, investigada por corrupción en Panamá por presunto pago de coimas a funcionarios con dineros de anticipos de la obra del valle de Tonosí.

Tras la investigación sobre ese caso, Marco Alfredo Albán Crespo,  representante legal, fue enviado a la cárcel en mayo del 2015 por delitos contra la administración pública. Esta misma persona  trabajaba hace 20 años en Hidalgo & Hidlago y se encarga de la operación en toda Centroamérica, además de otros proyectos de desarrollo en Colombia y Perú.

En ese sentido, esa misma empresa tiene un contrato en conjunto con el grupo Solarte para llevar a cabo la obra de la vía entre Santander de Quilichao y Popayán, pero esta cuenta con un retraso de dos años, según las palabras del ministro de transporte, Germán Cardona en 6 am de Caracol Radio.

Bogotá Limpia promete dar un aporte al distrito por una suma de 18.720 millones de pesos.

### **5. Área Limpia** 

**La zona 5** **integrada por Localidad de Suba** estará a cargo desde el próximo 12 de febrero de la empresa PSF Área Limpia, integrada por la empresa mexicana Sinergias Ecológicas que ha operado en México y España, propuso aportar a la alcaldía una suma de \$27.000 millones.

De acuerdo con la investigación de Noticias UNO, esta empresa está conformada por la mexicana, Sinergias Ecológicas y la compañía española Valoriza, que a su vez hace parte de Sacyr. Esta última, es española y por incumplimiento en las obras de la Ruta del Sol debió pagar 77 millones de dólares al Invías.

### **Los trabajadores de Aguas de Bogotá que se quedó por fuera de la licitación** 

La empresa distrital Aguas de Bogotá cuenta con una planta de trabajadores de 3200 personas que durante el mes de diciembre protagonizaron varias protesta dado que la nueva licitación excluyó a esta empresa pública. Sin embargo las empresas que ganaron la licitación afirmaron que incluirán en su planta a estas personas.

### **Recicladores con futuro incierto** 

El gran problema, aunque no menor del que afrontan los habitantes de las veredas aledañas al llamado "relleno sanitario de Doña Juana", es el que tienen que afrontar las familias de recicladores que se asociaron y que vieron en una decisión de la Corte Constitucional la posibilidad de sobrevivir buscando entre las basuras para separar el material reciclable.

Para algunos de los voceros de esta población, la mejor posibilidad era que las mismas empresas incluyeran el trabajo del reciclador, sin embargo el modelo Peñalosa paga el costo de la recolección de basuras por peso, es decir, entre más peso tenga el material recogido mayor será el valor cobrado por la empresa. Es por eso que para las empresas sería mucho mejor, en terminos monetarios, no facilitar el trabajo de los recicladores.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
