Title: video en vivo
Date: 2019-11-06 12:55
Author: CtgAdm
Slug: video-en-vivo
Status: published

DD.HH.  
</i>","nextArrow":"","autoplay":true,"autoplaySpeed":10000}' dir="ltr" data-typing="0"&gt;  
[Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  
[Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)  
[Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)  
[Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)  
[Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado](https://archivo.contagioradio.com/informe-revela-como-las-comunidades-de-fe-fueron-victimas-del-conflicto-armado/)  
</i>","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
<time datetime="2019-11-06T11:06:53-05:00" title="2019-11-06T11:06:53-05:00">noviembre 6, 2019</time> Botero debe asumir responsabilidad política y penal por bombardeo a niños  
[](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  
<time datetime="2019-11-06T10:52:09-05:00" title="2019-11-06T10:52:09-05:00">noviembre 6, 2019</time> Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe  
[](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)  
<time datetime="2019-11-05T17:13:19-05:00" title="2019-11-05T17:13:19-05:00">noviembre 5, 2019</time> Jericó en emergencia y calamidad pública tras avalancha  
[](https://archivo.contagioradio.com/se-declara-urgencia-manifiesta-en-jerico/)  
<time datetime="2019-11-05T17:08:03-05:00" title="2019-11-05T17:08:03-05:00">noviembre 5, 2019</time> Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino  
[](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)  
<time datetime="2019-11-05T13:05:49-05:00" title="2019-11-05T13:05:49-05:00">noviembre 5, 2019</time> Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional  
[](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)  
<time datetime="2019-11-04T14:12:23-05:00" title="2019-11-04T14:12:23-05:00">noviembre 4, 2019</time> Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca  
[](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)  
<time datetime="2019-11-03T16:20:50-05:00" title="2019-11-03T16:20:50-05:00">noviembre 3, 2019</time> Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca  
[](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)  
<time datetime="2019-11-01T16:54:20-05:00" title="2019-11-01T16:54:20-05:00">noviembre 1, 2019</time> Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado  
[](https://archivo.contagioradio.com/informe-revela-como-las-comunidades-de-fe-fueron-victimas-del-conflicto-armado/)

##### EN VIVO: Especial elecciones  

Sintonízate con la paz  
  
-----------------------

https://youtu.be/307\_X4q52YM

#### Nacional

[![Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-340x240.jpg "Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe"){width="340" height="240" sizes="(max-width: 340px) 100vw, 340px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-340x240.jpg 340w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-240x170.jpg 240w"}](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)  

##### [Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

[<time datetime="2019-11-06T10:52:09-05:00" title="2019-11-06T10:52:09-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)[0](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/#respond)Este miércoles incian los alegatos finales contra Santiago Uribe en las que se resolverá su presunta participación en la creación de los 12 apóstoles  
[![Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-370x260.jpg "Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional"){width="370" height="260" sizes="(max-width: 370px) 100vw, 370px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-340x240.jpg 340w"}](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)  

###### [Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)

[<time datetime="2019-11-05T13:05:49-05:00" title="2019-11-05T13:05:49-05:00">noviembre 5, 2019</time>](https://archivo.contagioradio.com/2019/11/05/)  
[![Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-370x260.jpg "Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico"){width="370" height="260" sizes="(max-width: 370px) 100vw, 370px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-770x538.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-340x240.jpg 340w"}](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)  

###### [Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)

[<time datetime="2019-11-01T17:28:46-05:00" title="2019-11-01T17:28:46-05:00">noviembre 1, 2019</time>](https://archivo.contagioradio.com/2019/11/01/)  
[![Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--370x260.jpg "Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado"){width="370" height="260" sizes="(max-width: 370px) 100vw, 370px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio--340x240.jpg 340w"}](https://archivo.contagioradio.com/informe-revela-como-las-comunidades-de-fe-fueron-victimas-del-conflicto-armado/)  

###### [Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado](https://archivo.contagioradio.com/informe-revela-como-las-comunidades-de-fe-fueron-victimas-del-conflicto-armado/)

[<time datetime="2019-11-01T16:54:20-05:00" title="2019-11-01T16:54:20-05:00">noviembre 1, 2019</time>](https://archivo.contagioradio.com/2019/11/01/)

#### Nacional

[![Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)  

#### [Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

Este miércoles incian los alegatos finales contra Santiago Uribe en las que se resolverá su presunta participación en la creación…  
[![Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)  

#### [Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)

Más allá de la moción de censura al ministro Guillermo Botero, senadores señalan que se debe modificar la política de…  
[![No se detiene el exterminio hacia los pueblos indígenas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ONIC-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas/)  

#### [No se detiene el exterminio hacia los pueblos indígenas](https://archivo.contagioradio.com/no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas/)

Otros cuatro integrantes de los pueblos indígenas fueron asesinados el pasado jueves 31 de octubre en Cauca, Putumayo y Cesar  
[![Pagar para contaminar, el decreto de Peñalosa con el pico y placa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/)  

#### [Pagar para contaminar, el decreto de Peñalosa con el pico y placa](https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/)

Foto:U.Andes Sectores ambientalistas rechazaron el  decreto de la  Secretaría de Movilidad que permite  el levantamiento del pico y placa en…  
[![Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Cauca-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/)  

#### [Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/)

Integrantes de derechos humanos de la ACIN, calificaron como mediática la propuesta de militarizar el territorio para eliminar la violencia

#### Ambiente

[Ambiente](#)[Animales](#)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)[<time datetime="2019-11-05T17:13:19-05:00" title="2019-11-05T17:13:19-05:00">noviembre 5, 2019</time>  
](https://archivo.contagioradio.com/se-declara-urgencia-manifiesta-en-jerico/)

##### Jericó en emergencia y calamidad pública tras avalancha

</a>  
[![Pagar para contaminar, el decreto de Peñalosa con el pico y placa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-110x78.jpg "Pagar para contaminar, el decreto de Peñalosa con el pico y placa"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n-340x240.jpg 340w"}](https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/)  

###### [Pagar para contaminar, el decreto de Peñalosa con el pico y placa](https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/)

[<time datetime="2019-10-31T17:39:21-05:00" title="2019-10-31T17:39:21-05:00">octubre 31, 2019</time>](https://archivo.contagioradio.com/2019/10/31/)  
[![Lo que debe saber sobre la resolución que aprueba la caza de tiburones en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-110x78.jpg "Lo que debe saber sobre la resolución que aprueba la caza de tiburones en Colombia"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium-340x240.jpg 340w"}](https://archivo.contagioradio.com/resolucion-aprueba-caza-de-tiburones/)  

###### [Lo que debe saber sobre la resolución que aprueba la caza de tiburones en Colombia](https://archivo.contagioradio.com/resolucion-aprueba-caza-de-tiburones/)

[<time datetime="2019-10-30T18:20:11-05:00" title="2019-10-30T18:20:11-05:00">octubre 30, 2019</time>](https://archivo.contagioradio.com/2019/10/30/)  
[![Con este concurso se busca proteger el Bosque Bavaria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-110x78.jpg "Con este concurso se busca proteger el Bosque Bavaria"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki-340x240.jpg 340w"}](https://archivo.contagioradio.com/con-este-concurso-se-busca-proteger-el-bosque-bavaria/)  

###### [Con este concurso se busca proteger el Bosque Bavaria](https://archivo.contagioradio.com/con-este-concurso-se-busca-proteger-el-bosque-bavaria/)

[<time datetime="2019-10-29T12:27:41-05:00" title="2019-10-29T12:27:41-05:00">octubre 29, 2019</time>](https://archivo.contagioradio.com/2019/10/29/)  
[![Articulación institucional: Clave para respetar la Amazonía como sujeto de derechos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-110x78.jpg "Articulación institucional: Clave para respetar la Amazonía como sujeto de derechos"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1-340x240.jpg 340w"}](https://archivo.contagioradio.com/articulacion-institucional-clave-para-respetar-la-amazonia-como-sujeto-de-derechos/)  

###### [Articulación institucional: Clave para respetar la Amazonía como sujeto de derechos](https://archivo.contagioradio.com/articulacion-institucional-clave-para-respetar-la-amazonia-como-sujeto-de-derechos/)

[<time datetime="2019-10-22T19:33:20-05:00" title="2019-10-22T19:33:20-05:00">octubre 22, 2019</time>](https://archivo.contagioradio.com/2019/10/22/)

#### Derechos Humanos

[DDHH](#)[Líderes sociales](#)[Movilización](#)  
[![Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-340x240.png "Botero debe asumir responsabilidad política y penal por bombardeo a niños"){width="340" height="240" sizes="(max-width: 340px) 100vw, 340px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-340x240.png 340w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-275x195.png 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-216x152.png 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-240x170.png 240w"}](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  

##### [Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)

[Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-11-06T11:06:53-05:00" title="2019-11-06T11:06:53-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)Según el personero Herner Carreño, el Gobierno conocía que había reclutamiento de menores, previo al ataque denunciado en la moción contra Guillermo Botero  
[![Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-110x78.png "Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-275x195.png 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-770x540.png 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-216x152.png 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-240x170.png 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-340x240.png 340w"}](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)  

###### [Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)

[<time datetime="2019-11-04T14:12:23-05:00" title="2019-11-04T14:12:23-05:00">noviembre 4, 2019</time>](https://archivo.contagioradio.com/2019/11/04/)  
[![Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-110x78.png "Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-275x195.png 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-770x540.png 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-216x152.png 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-240x170.png 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-340x240.png 340w"}](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)  

###### [Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)

[<time datetime="2019-11-03T16:20:50-05:00" title="2019-11-03T16:20:50-05:00">noviembre 3, 2019</time>](https://archivo.contagioradio.com/2019/11/03/)  
</i>","nextArrow":"","autoplay":true,"autoplaySpeed":5000}' dir="ltr" data-typing="1"&gt;  
12:41  
[En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)  
![21 de noviembre: por la dignidad  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-150x150.jpg "21 de noviembre: por la dignidad  "){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-360x360.jpg 360w"} 12:42  
[21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  
![Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-150x150.png "Botero debe asumir responsabilidad política y penal por bombardeo a niños"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-512x512.png 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-360x360.png 360w"} 11:53  
[Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  
![Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-150x150.jpg "Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-360x360.jpg 360w"} 10:09  
[Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

#### Derechos Humanos

[![Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  

#### [Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)

Según el personero Herner Carreño, el Gobierno conocía que había reclutamiento de menores, previo al ataque denunciado en la moción…  
[![Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)  

#### [Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)

El pasado domingo el CRIC denunció el asesinato del comunero y defensor de DD.HH. Eduardo Mestizo, así como el ataque…  
[![Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)  

#### [Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)

Alex Vitonás es la más reciente víctima de la violencia contra los pueblos indígenas en Cauca. Su asesinato se denunció…  
[![Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)  

#### [Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)

Durante masiva movilización en Barranquilla, el rector de la Universidad del Atlántico Carlos Prasca presentó su renuncia

### Programas

[Expreso Libertad](https://archivo.contagioradio.com/categoria/programas/expreso-libertad/)[  
](https://archivo.contagioradio.com/el-lujo-de-sobrevivir-en-una-prision-de-colombia/)

##### El lujo de sobrevivir en una prisión de Colombia

<time datetime="2019-10-31T08:31:43-05:00" title="2019-10-31T08:31:43-05:00">octubre 31, 2019</time></a>  
[![Radionovela «Furia Violeta»](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-150x150.png "Radionovela «Furia Violeta»"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-512x512.png 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-360x360.png 360w"}](https://archivo.contagioradio.com/radionovela-furiavioleta/)  

###### [Radionovela «Furia Violeta»](https://archivo.contagioradio.com/radionovela-furiavioleta/)

[![MIDBO 2019, nuevas formas de experimentar las narrativas documentales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO-150x150.png "MIDBO 2019, nuevas formas de experimentar las narrativas documentales"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO-360x360.png 360w"}](https://archivo.contagioradio.com/midbo-2019-nuevas-formas-de-experimentar-las-narrativas-documentales/)  

###### [MIDBO 2019, nuevas formas de experimentar las narrativas documentales](https://archivo.contagioradio.com/midbo-2019-nuevas-formas-de-experimentar-las-narrativas-documentales/)

[![INPEC violenta a las mujeres visitantes en centros de reclusión](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-150x150.jpg "INPEC violenta a las mujeres visitantes en centros de reclusión"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-360x360.jpg 360w"}](https://archivo.contagioradio.com/inpec-violenta-a-las-mujeres-visitantes-en-centros-de-reclusion/)  

###### [INPEC violenta a las mujeres visitantes en centros de reclusión](https://archivo.contagioradio.com/inpec-violenta-a-las-mujeres-visitantes-en-centros-de-reclusion/)

[![Universidad Nacional estaría negando el derecho a la educación de presos políticos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-150x150.jpg "Universidad Nacional estaría negando el derecho a la educación de presos políticos"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-360x360.jpg 360w"}](https://archivo.contagioradio.com/universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos/)  

###### [Universidad Nacional estaría negando el derecho a la educación de presos políticos](https://archivo.contagioradio.com/universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos/)

#### Entrevista del día

[![Tenga en cuenta estas recomendaciones para ejercer su derecho a la protesta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-340x240.jpeg "Tenga en cuenta estas recomendaciones para ejercer su derecho a la protesta"){width="340" height="240" sizes="(max-width: 340px) 100vw, 340px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-340x240.jpeg 340w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-300x211.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-768x540.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-1024x720.jpeg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-370x260.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-275x195.jpeg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-110x78.jpeg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-216x152.jpeg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM-240x170.jpeg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM.jpeg 1280w"}](https://archivo.contagioradio.com/tenga-en-cuenta-estas-recomendaciones-para-ejercer-su-derecho-a-la-protesta/)  

##### [Tenga en cuenta estas recomendaciones para ejercer su derecho a la protesta](https://archivo.contagioradio.com/tenga-en-cuenta-estas-recomendaciones-para-ejercer-su-derecho-a-la-protesta/)

[<time datetime="2019-10-31T15:00:07-05:00" title="2019-10-31T15:00:07-05:00">octubre 31, 2019</time>](https://archivo.contagioradio.com/2019/10/31/)En la entrevista, una defensora de derechos humanos señala algunas recomendaciones para ejercer el derecho a la protesta sin miedo

#### Memoria

[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)[![Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-770x540.jpg "Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino"){width="770" height="540" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Darío-Acevedo-1-216x152.jpg 216w"}](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)  

##### [Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)

[Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-11-05T17:08:03-05:00" title="2019-11-05T17:08:03-05:00">noviembre 5, 2019</time>](https://archivo.contagioradio.com/2019/11/05/)Este martes se desarrolló el debate de control político a Darío Acevedo, por la forma en que está dirigiendo el Centro Nacional de Memoria Histórica  
[![Exhumación de Franco: Un paso por la dignidad de las víctimas de la dictadura](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-110x78.jpg "Exhumación de Franco: Un paso por la dignidad de las víctimas de la dictadura"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1-340x240.jpg 340w"}](https://archivo.contagioradio.com/exhumacion-de-franco-un-paso-por-la-dignidad-de-las-victimas-de-la-dictadura/)  

###### [Exhumación de Franco: Un paso por la dignidad de las víctimas de la dictadura](https://archivo.contagioradio.com/exhumacion-de-franco-un-paso-por-la-dignidad-de-las-victimas-de-la-dictadura/)

[<time datetime="2019-10-27T12:14:16-05:00" title="2019-10-27T12:14:16-05:00">octubre 27, 2019</time>](https://archivo.contagioradio.com/2019/10/27/)  
[![Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-110x78.jpg "Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion-340x240.jpg 340w"}](https://archivo.contagioradio.com/victimas-operacion-orion/)  

###### [Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad](https://archivo.contagioradio.com/victimas-operacion-orion/)

[<time datetime="2019-10-16T13:15:29-05:00" title="2019-10-16T13:15:29-05:00">octubre 16, 2019</time>](https://archivo.contagioradio.com/2019/10/16/)  
[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/al-aire.gif){width="1540" height="180"}](http://ld-wp.template-help.com/wordpress_blog_multipurpose/sample/storycle/blog)

#### Opinión

[Johan Mendoza](https://archivo.contagioradio.com/categoria/opinion/johan/)[![21 de noviembre: por la dignidad  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-340x240.jpg "21 de noviembre: por la dignidad  "){width="340" height="240" sizes="(max-width: 340px) 100vw, 340px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-340x240.jpg 340w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-240x170.jpg 240w"}](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  

##### [21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-11-06T12:32:42-05:00" title="2019-11-06T12:32:42-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)Si el mundo cambia o no cambia con una marcha, es la pregunta que el fatalista quiere poner, como piedra en el zapato, al ímpetu que exhala la dignidad golpeada.  
</i>","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
[Cultura](https://archivo.contagioradio.com/categoria/cultura/)  
<time datetime="2019-10-30T18:06:53-05:00" title="2019-10-30T18:06:53-05:00">octubre 30, 2019</time> ¡Uy que miedo! el festival que desmiente las drogas en Colombia  
[](https://archivo.contagioradio.com/uy-que-miedo-el-festival-que-desmiente-las-drogas-en-colombia/)  
[24 Cuadros](https://archivo.contagioradio.com/categoria/programas/24-cuadros/)  
<time datetime="2019-10-25T16:09:44-05:00" title="2019-10-25T16:09:44-05:00">octubre 25, 2019</time> MIDBO 2019, nuevas formas de experimentar las narrativas documentales  
[](https://archivo.contagioradio.com/midbo-2019-nuevas-formas-de-experimentar-las-narrativas-documentales/)  
[24 Cuadros](https://archivo.contagioradio.com/categoria/programas/24-cuadros/)  
<time datetime="2019-09-20T13:04:20-05:00" title="2019-09-20T13:04:20-05:00">septiembre 20, 2019</time> ‘Bojayá entre fuegos cruzados’, el documental que reconstruye la verdad de un pueblo  
[](https://archivo.contagioradio.com/bojaya-entre-fuegos-cruzados-el-documental-que-reconstruye-la-verdad-de-un-pueblo/)  
Última Hora  
</i>","nextArrow":"","autoplay":true,"autoplaySpeed":5000}' dir="ltr" data-typing="1"&gt;  
noviembre 6, 2019  
[En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)  
noviembre 6, 2019  
[21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  
noviembre 6, 2019  
[Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  
[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PERIODISMO-DE-VERDAD.jpg){width="740" height="620" sizes="(max-width: 740px) 100vw, 740px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PERIODISMO-DE-VERDAD.jpg 740w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PERIODISMO-DE-VERDAD-300x251.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PERIODISMO-DE-VERDAD-370x310.jpg 370w"}](https://bit.ly/2ZqjDFp)

#### Visita nuestras redes:

[  
Facebook  
](https://www.facebook.com/contagioradio)  
[  
Twitter  
](https://twitter.com/contagioradio1)  
[  
Youtube  
](https://www.youtube.com/user/ContagioRadio)  
[  
Instagram  
](https://www.instagram.com/contagioradio/)  
[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/facebook-contagio-radio-.gif){width="370" height="596"}](https://www.facebook.com/contagioradio/)

#### Entrevistas

<iframe style="border: 1px solid #D7D7D7;" src="https://co.ivoox.com/es/player_es_podcast_168710_1.html" width="100%" height="440" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### ["Lo llamado alternativo está hecho de muchos matices y aprendizajes de convivencia, coexistencia temporal, a veces efímera de sectores democráticos..."](https://archivo.contagioradio.com/la-ultima-piedra-en-el-zapato/)

<figure>
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo.jpg){width="800" height="530" sizes="(max-width: 800px) 100vw, 800px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo.jpg 800w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo-300x199.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo-768x509.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo-370x245.jpg 370w"}](https://archivo.contagioradio.com/lo-que-nos-deja-el-27-un-respiro/)

</figure>
###### [Camilo de las Casas](https://archivo.contagioradio.com/lo-que-nos-deja-el-27-un-respiro/)

Opinión:Lo que nos deja el 27, un respiro

#### Videos

</iframe>"' data-height='500' data-video\_index='1'&gt;  
1  
![La Roja: Cerveza de la Paz](https://i.ytimg.com/vi/xxvu2N_2wxU/hqdefault.jpg "La Roja: Cerveza de la Paz")  
La Roja: Cerveza de la Paz</iframe>"' data-height='500' data-video\_index='2'&gt;  
2  
![Hay que enfocarse en las víctimas, ellas siguen aquí mientras los políticos vienen y van: David Crane](https://i.ytimg.com/vi/B9q1S5JFRGU/hqdefault.jpg "Hay que enfocarse en las víctimas, ellas siguen aquí mientras los políticos vienen y van: David Crane")  
Hay que enfocarse en las víctimas, ellas siguen aquí mientras los políticos vienen y van: David Crane</iframe>"' data-height='500' data-video\_index='3'&gt;  
3  
![Alter Eddie, más que caricaturista me considero un pedagogo](https://i.ytimg.com/vi/3tFBcfmDTGg/hqdefault.jpg "Alter Eddie, más que caricaturista me considero un pedagogo")  
Alter Eddie, más que caricaturista me considero un pedagogo</iframe>"' data-height='500' data-video\_index='4'&gt;  
4  
![Samuel David fruto de la paz, víctima de la guerra](https://i.ytimg.com/vi/TXajQCROOWk/hqdefault.jpg "Samuel David fruto de la paz, víctima de la guerra")  
Samuel David fruto de la paz, víctima de la guerra</iframe>"' data-height='500' data-video\_index='5'&gt;  
5  
![¿Qué hay detrás de las objeciones de Duque a la JEP?](https://i.ytimg.com/vi/N-0K98lhwL4/hqdefault.jpg "¿Qué hay detrás de las objeciones de Duque a la JEP?")  
¿Qué hay detrás de las objeciones de Duque a la JEP?</iframe>"' data-height='500' data-video\_index='6'&gt;  
6  
![Canción del Colegio AFLICOC](https://i.ytimg.com/vi/CGmhYhQTo_c/hqdefault.jpg "Canción del Colegio AFLICOC")  
Canción del Colegio AFLICOC</iframe>"' data-height='500' data-video\_index='7'&gt;  
7  
![Lida Urrego, nos cuenta su historia de vida en la cárcel](https://i.ytimg.com/vi/MuN2lZHKqJ4/hqdefault.jpg "Lida Urrego, nos cuenta su historia de vida en la cárcel")  
Lida Urrego, nos cuenta su historia de vida en la cárcel</iframe>"' data-height='500' data-video\_index='8'&gt;  
8  
![Adela Pérez, narra la situación de las mujeres en las cárceles](https://i.ytimg.com/vi/ht-K8Q3OjyA/hqdefault.jpg "Adela Pérez, narra la situación de las mujeres en las cárceles")  
Adela Pérez, narra la situación de las mujeres en las cárceles

#### Política

[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)[<time datetime="2019-11-05T13:05:49-05:00" title="2019-11-05T13:05:49-05:00">noviembre 5, 2019</time>  
](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)

##### Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional

</a>

###### [Pagar para contaminar, el decreto de Peñalosa con el pico y placa](https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/)

[<time datetime="2019-10-31T17:39:21-05:00" title="2019-10-31T17:39:21-05:00">octubre 31, 2019</time>](https://archivo.contagioradio.com/2019/10/31/)

###### [Del paro cívico a gobernar Buenaventura, los retos que asume Hugo Vidal](https://archivo.contagioradio.com/del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal/)

[<time datetime="2019-10-29T20:54:00-05:00" title="2019-10-29T20:54:00-05:00">octubre 29, 2019</time>](https://archivo.contagioradio.com/2019/10/29/)

###### [Los compromisos de Claudia López con la Universidad Distrital](https://archivo.contagioradio.com/los-compromisos-de-claudia-lopez-con-la-universidad-distrital/)

[<time datetime="2019-10-28T17:58:49-05:00" title="2019-10-28T17:58:49-05:00">octubre 28, 2019</time>](https://archivo.contagioradio.com/2019/10/28/)

#### DDHH

[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)[  
](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)

##### Botero debe asumir responsabilidad política y penal por bombardeo a niños

</a>

###### [Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca](https://archivo.contagioradio.com/asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca/)

[<time datetime="2019-11-04T14:12:23-05:00" title="2019-11-04T14:12:23-05:00">noviembre 4, 2019</time>](https://archivo.contagioradio.com/2019/11/04/)

###### [Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/)

[<time datetime="2019-11-03T16:20:50-05:00" title="2019-11-03T16:20:50-05:00">noviembre 3, 2019</time>](https://archivo.contagioradio.com/2019/11/03/)

###### [Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico](https://archivo.contagioradio.com/movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico/)

[<time datetime="2019-11-01T17:28:46-05:00" title="2019-11-01T17:28:46-05:00">noviembre 1, 2019</time>](https://archivo.contagioradio.com/2019/11/01/)

### YO REPORTO

Te invitamos a participar  enviándonos tus notas, reportajes, noticias y fotografías de temas relacionados con derechos humanos en Colombia y el mundo.

<form action="/wp-admin/admin-ajax.php#wpcf7-f60171-o1" method="post" enctype="multipart/form-data" novalidate="novalidate">
<input type="hidden" name="_wpcf7" value="60171"></input>  
<input type="hidden" name="_wpcf7_version" value="5.1.4"></input>  
<input type="hidden" name="_wpcf7_locale" value="es_ES"></input>  
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f60171-o1"></input>  
<input type="hidden" name="_wpcf7_container_post" value="0"></input>  
<input type="hidden" name="g-recaptcha-response" value></input>

</p>
<input type="text" name="your-name" value size="40" aria-required="true" aria-invalid="false" placeholder="Nombre*"></input>

<input type="email" name="your-email" value size="40" aria-required="true" aria-invalid="false" placeholder="e-mail*"></input>

<p style="color:#FFF; width: 97%;">
<textarea name="your-message" cols="40" rows="10" aria-invalid="false" placeholder="Reporte o Noticia">
</textarea>
</p>
Subir archivo  
<input type="file" name="file-931" size="40" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv" aria-invalid="false"></input>

Al enviar estoy aceptando que estoy de acuerdo con los [Términos de uso](https://archivo.contagioradio.com/terminos)

<input type="submit" value="Enviar"></input>

</form>
#### Publicaciones

RecientesPopulares

<style>.elementor-1509 .elementor-element.elementor-element-0769cdc > .elementor-container{max-width:800px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post-thumbnail.post-thumbnail-simple.post-thumbnail-simple{max-width:36%;flex:0 0 36%;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing{margin:0px -10px 0px -10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured{margin:10px 10px 10px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__posts{margin:0px 10px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__title{padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter > .jet-smart-listing__filter-item > a{margin:0px 0px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter-more > i{margin:0px 0px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__meta{text-align:left;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__meta{text-align:left;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-title-fields__item-label{margin-right:5px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-content-fields__item-label{margin-right:5px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-processing{opacity:0.5;}.elementor-1509 .elementor-element.elementor-element-59a8b78 > .tippy-popper .tippy-tooltip .tippy-content{text-align:center;}</style>
<div class="section" data-id="0769cdc" data-element_type="section">

###### [En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

[<time datetime="2019-11-06T12:46:41-05:00" title="2019-11-06T12:46:41-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![21 de noviembre: por la dignidad  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-110x78.jpg "21 de noviembre: por la dignidad  "){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-340x240.jpg 340w"}](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  

###### [21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)

[<time datetime="2019-11-06T12:32:42-05:00" title="2019-11-06T12:32:42-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-110x78.png "Botero debe asumir responsabilidad política y penal por bombardeo a niños"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-275x195.png 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-770x540.png 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-216x152.png 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-240x170.png 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-340x240.png 340w"}](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  

###### [Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)

[<time datetime="2019-11-06T11:06:53-05:00" title="2019-11-06T11:06:53-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-110x78.jpg "Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-340x240.jpg 340w"}](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)  

###### [Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

<p>
[<time datetime="2019-11-06T10:52:09-05:00" title="2019-11-06T10:52:09-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  

</div>

<style>.elementor-1529 .elementor-element.elementor-element-0769cdc > .elementor-container{max-width:800px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post-thumbnail.post-thumbnail-simple.post-thumbnail-simple{max-width:36%;flex:0 0 36%;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing{margin:0px -10px 0px -10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured{margin:10px 10px 10px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__posts{margin:0px 10px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__title{padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter > .jet-smart-listing__filter-item > a{margin:0px 0px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter-more > i{margin:0px 0px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__meta{text-align:left;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__meta{text-align:left;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-title-fields__item-label{margin-right:5px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-content-fields__item-label{margin-right:5px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-processing{opacity:0.5;}.elementor-1529 .elementor-element.elementor-element-59a8b78 > .tippy-popper .tippy-tooltip .tippy-content{text-align:center;}</style>
<div class="section" data-id="0769cdc" data-element_type="section">

###### [En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

[<time datetime="2019-11-06T12:46:41-05:00" title="2019-11-06T12:46:41-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![21 de noviembre: por la dignidad  ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-110x78.jpg "21 de noviembre: por la dignidad  "){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/72600431_1230226617163743_6504635164138668032_o-340x240.jpg 340w"}](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)  

###### [21 de noviembre: por la dignidad  ](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/)

[<time datetime="2019-11-06T12:32:42-05:00" title="2019-11-06T12:32:42-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-110x78.png "Botero debe asumir responsabilidad política y penal por bombardeo a niños"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-275x195.png 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-770x540.png 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-216x152.png 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-240x170.png 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5-340x240.png 340w"}](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)  

###### [Botero debe asumir responsabilidad política y penal por bombardeo a niños](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/)

[<time datetime="2019-11-06T11:06:53-05:00" title="2019-11-06T11:06:53-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  
[![Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-110x78.jpg "Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe-340x240.jpg 340w"}](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)  

###### [Inician las audiencias finales contra los 12 apóstoles y Santiago Uribe](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

<p>
[<time datetime="2019-11-06T10:52:09-05:00" title="2019-11-06T10:52:09-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)  

</div>

#### Newsletter

Suscríbete para obtener noticias

<form method="POST" action="#">
<input type="email" name="email" placeholder="E-mail" data-instance-data="[]">  
[Suscribirme](#)  

</form>

