Title: Cárcel La Picota retira dieta vital a persona con enfermedad terminal
Date: 2018-09-20 16:15
Author: AdminContagio
Category: DDHH, Nacional
Tags: Cárcel La Picota, Dieta, enfermedad terminal, preso
Slug: carcel-la-picota-retira-dieta-vital-a-persona-con-enfermedad-terminal
Status: published

###### [Foto: Alimentación del prisionero] 

###### [20 Sept 2018]

José Ángel, prisionero político de FARC que actualmente se encuentra recluido en la Cárcel La Picota, denunció que a pesar de padecer una enfermedad terminal y tener recomendaciones prioritarias para su alimentación, **el centro penitenciario no solo las incumple sino que lo condena a una muerte lenta, quebrantando su salud**.

Desde hace más de 10 años Ángel ha tenido que enfrentarse al sistema penitenciario a las múltiples violaciones a los derechos humanos, debido a que en un principio se le negó la posibilidad de atención a su salud, hecho que lo llevo a instaurar tutelas que tuvieron que llegar a instancias más importantes para que **se le dictaminaran medidas cautelares que protegieran su vida**.  (Le puede interesar: ["Internos de la Tramacúa vuelven a Huelga por sus derechos"](https://archivo.contagioradio.com/internos-tramacua-vuelven-a-huelga-de-hambre/))

En esas medidas se establece la necesidad de darle una dieta especial para su tratamiento contra la Leucemia, que contiene para el desayuno 4 frutas  que no sean ácidas, ni banano o guayaba, 2 cereales y 2 proteinas, para el almuerzo y la cena debe comer: 2 proteinas, 2 cereales, una sopa, una ensalada, una papa y postre.

![comida2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/comida2-450x800.jpeg){.alignnone .size-medium .wp-image-56889 width="450" height="800"}

"Me suspendieron la comida, no me han hecho ninguna valoración médica, están enviado todo lo contrario, la dieta la quitaron y me da miedo que la enfermedad empiece a molestar. **Esto debido a que el medicamento que consumo para el cáncer es demasiado fuerte"** afirmó José Ángel.

![comida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/comida-450x800.jpeg){.alignnone .size-medium .wp-image-56888 width="450" height="800"}

###### Reciba toda la información de Contagio Radio en [[su correo]
