Title: Transportadores a la espera de una reunión con el Presidente Santos
Date: 2016-07-18 13:57
Category: Movilización, Nacional
Tags: Audiencia pública paro camionero, Federación Nacional de Camioneros, Paro camionero, plebiscito por la paz
Slug: transportadores-a-la-espera-de-una-reunion-con-el-presidente-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Conversaciones-Paro-Camionero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [18 Julio 2016]

El pasado sábado la Comisión Sexta del Senado llevó a cabo una audiencia pública sobre el paro camionero que ya completa 42 días. Al debate estaban citados los voceros del gremio y el Ministro de Transportes; sin embargo, el funcionario no asistió. Pese a la negativa del Ministerio y al altercado que se presentó entre Pedro Aguilar y uno de los asistentes, los transportadores lograron que la Comisión se comprometiera a gestionar **para este lunes una reunión entre el Presidente Juan Manuel Santos y el gremio**.

Para Jorge García, presidente de la Federación Nacional de Camioneros, **el que el Ministro de Transporte no asistiera a la audiencia muestra la falta de voluntad política** del Gobierno para negociar el pliego de peticiones presentado por los transportadores, quienes además continúan enfrentando el [[hostigamiento y la persecución por parte de la Fuerza Pública](https://archivo.contagioradio.com/durante-paro-camionero-van-40-denuncias-de-abuso-de-la-fuerza-por-parte-del-esmad/)] en algunos de los puntos de concentración, en los que permanecen estacionados los vehículos sin bloquear las vías.

Frente a las afirmaciones del Ministro Jorge Rojas, García asegura que "ha confundido a la opinión pública y ha sido muy ágil en decir que pedimos un 40% del alza en los fletes, nosotros lo que hemos dicho es que **en los últimos 10 años hemos perdido un 40%, dado el incremento en los costos de operación**; nosotros lo que hemos dicho es que por ejemplo un flete de Bogotá a Buenaventura cuesta \$98 mil sí llegamos al nivel que nosotros estamos solicitando quedaría en \$118 mil, lo que significaría que 1 kilo subiría sólo \$2, pero lo malinterpretan".

"No tenemos problemas penales, ni jurídicos, **sí es que es cierto que existe algo en contra de nosotros que venga la Fiscalía o la entidad competente y nos aprehenda** porque sí somos delincuentes deberíamos estar en una cárcel", asevera García en relación con las acusaciones contra algunos de los líderes del gremio, e insiste en que "la cruzada camionera está muy unida, muy bien estructurada, entre nosotros no hay ninguna diferencia".

Por redes sociales han circulado fotografías en las que se muestran camiones estacionados y con letreros en contra del plebiscito por la paz; sin embargo, García asegura que la oposición al proceso de negociaciones no tiene nada que ver con el paro camionero, por lo que **el gremio no ha asumido una postura colectiva frente a la refrendación de los acuerdos**.

Para el gremio de transportadores está claro que en Colombia hay un problema grave con los camioneros y que el Gobierno nacional les está tratando de "medir el aceite", hecho con el que no están de acuerdo, por lo que seguirán "firmes en el paro, no vamos a claudicar porque vemos que **es la última oportunidad para decidir el futuro de las familias camioneras**", como concluye García.

<iframe src="http://co.ivoox.com/es/player_ej_12261831_2_1.html?data=kpefmJacd5Khhpywj5aYaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjazFtsSZpJiSo6nFaZO3jKvSxsrWpcTdhqigh6eXsozCwtGYpcbRrdDixtfc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

 
