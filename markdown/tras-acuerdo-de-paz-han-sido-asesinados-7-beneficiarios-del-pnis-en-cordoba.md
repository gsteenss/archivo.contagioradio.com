Title: Tras Acuerdo de Paz han sido asesinados 7 beneficiarios del PNIS en Córdoba
Date: 2020-02-11 15:32
Author: CtgAdm
Category: DDHH, Nacional
Tags: PNIS, sustitución
Slug: tras-acuerdo-de-paz-han-sido-asesinados-7-beneficiarios-del-pnis-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Beneficiarios-del-PNIS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @ascsucor\_org {#foto-ascsucor_org .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/david-ortega-integrante-ascsucor-sobre-asesinatos_md_47665672_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Jose David Ortega | Integrante de ACSUCOR

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La Asociación de Campesinos del Sur de Córdoba (Ascsucor) denunció que el pasado domingo 9 de febrero fue asesinado **Jaime Toscano Fernández**, beneficiario y gestor comunitario del Plan Nacional Integral de Sustitución (PNIS) en la vereda Puerto Colombia de San José de Uré, Córdoba. **Con Toscano, son 2 líderes y 5 los beneficiarios del PNIS asesinados en el departamento desde la firma del Acuerdo de Paz en 2016.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La Defensoría del Pueblo alertó por riesgos a beneficiario del PNIS**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según las primeras informaciones, Toscano estaba contratado por el PNIS para hacer trabajo comunitario, residía en una vereda de Puerto Libertador y salió rumbo a la Vereda Puerto Colombia, en San José de Uré, para visitar unos familiares y fue allí donde hombres armados lo asesinaron. (Le puede interesar:["Se libra una guerra por tomar el control del Nudo de Paramillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**José David Ortega, integrante de Ascsucor**, afirmó que en el territorio los grupos armados ponen reglas sobre quién puede entrar a la zona y qué se hace allí, entre otras cosas. Por esa razón, pidió que se esclarezca si Toscano fue asesinado por ir a un territorio en el que no lo conocían, o por su rol en el PNIS.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Ortega cuestionó el que no se haya hecho seguimiento a la Alerta Temprana 054 de la Defensoría del Pueblo en la que se señala que las personas que apoyan el PNIS están en riesgo en la zona del Bajo Cauca Antioqueño y el sur de Córdoba. (Le puede interesar:["Luis Darío Rodríguez, segundo líder asesinado en Córdoba en menos de una semana"](https://archivo.contagioradio.com/luis-dario-rodriguez-segundo-lider-asesinado-en-menos-de-una-semana-en-cordoba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los riesgos señalados en la Alerta Temprana se estarían cumpliendo con el asesinato de Toscano, Rafael Manotas (integrante de la Junta de Acción Comunal del corregimiento El Poblado, en Pueblo Nuevo), Luis Darío Rodríguez en Tierralta y Jorge Luis Betancurt en Montelíbano. (Le puede interesar: ["Dos nuevos casos de asesinato a líderes sociales enlutan al país"](https://archivo.contagioradio.com/dos-nuevos-casos-de-asesinato-a-lideres-sociales-enlutan-al-pais/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Han sido asesinados 7 beneficiarios del PNIS en el Sur de Córdoba**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ortega aclaró que desde que se firmaron los acuerdos de sustitución, 2 líderes que impulsaban el PNIS y 5 beneficiarios han sido asesinados. El primero de ellos, Alexander Padilla Cruz, a manos del Ejército en zona rural de Tierralta, el viernes 15 de diciembre de 2017. (Le puede interesar: ["Comunidad de El limón, en Córdoba, exige que Brigada 16 del Ejército sea retirada"](https://archivo.contagioradio.com/asesinado_campesino_coca_limon_tierra_alta_cordoba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto al avance del Plan, el vocero de Ascsucor declaró que en Montelíbano, Puerto Libertador y San José de Uré (los 3 municipios del sur de Córdoba) hay alrededor de 2 mil personas inscritas. A ellos les llegó el bono de asistencia alimentaria inmediata por 2 millones de pesos, pero hasta el momento, **"no hay proyectos productivos instalados".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Más de 300 familias se quedaron por fuera del P**lan

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ortega explicó que hubo familias en el departamento que no entraron al PNIS porque no sabían del programa y por intimidación de los grupos armados, entre otras razones. **Se trataría de cerca de 300 familias que no lograron inscribirse** (sin incluir en esta cifra a Tierralta), y a la que el Gobierno le cerró las puertas, porque decidió no vincular más personas al Plan.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ascsucor_org/status/1227248143055687680","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ascsucor\_org/status/1227248143055687680

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este martes 11 de febrero, la Asociación denunció que algunas de estas familias en la vereda El Guineo de Puerto Libertador se están resistiendo a la erradicación forzada porque es su única forma de subsistencia. Adicionalmente, rechazan la fumigación con Glifosato porque este elemento daña la tierra y los demás cultivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ortega concluyó que en el territorio se debería seguir impulsando la sustitución voluntaria, tomando en cuenta el apoyo que tiene de las comunidades, así como que **la resiembra según Naciones Unidas es del 4% mientras que con la erradicación forzada alcanza al 300%.** Igualmente, pidió garantías de seguridad para las y los líderes del territorio.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
