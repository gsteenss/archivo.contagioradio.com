Title: Sin Olvido - Julio Henríquez Santamaría
Date: 2015-01-06 15:32
Author: CtgAdm
Category: Sin Olvido
Slug: julio-henriquez-santamaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Julio-Henríquez-Santamaría.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cortesía de la familia 

[Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsyJdjoB.mp3)

El 29 de Marzo de 1952 en Cereté Córdoba nació Julio Henriquez Santamaría, hijo de un comerciante y una modista.

Estudió biología, en la Universidad Libre de Bogotá, en donde participó de una vida estudiantil activa siendo presidente del Consejo. También, hizo parte del comité editorial del periódico de la Unión Revolucionaria Socialista hasta que se trasladó a la ciudad de Santa Marta junto a su hija Nadia y su esposa Zulma Chacín, que por ese entonces se encontraba esperando a su hijo Julio.**  
**

El domingo 4 de febrero del año 2001, Julio se encontraba reunido con varios campesinos y parceleros de la región, en la asamblea de la constitución de la Asociación Ambientalista Comunitaria de Calabazo “Madre Tierra”, A las 10 de la mañana, ocho hombres armados identificados como paramilitares,llegaron en una Toyota blanca a la vereda, quienes operaban bajo el mando de Hernán Giraldo Serna y Francisco “Pacho” Muzo, quienes se lo llevaron por la fuerza al lugar conocido como Machete Pelado, frente a más de 20 campesinos que miraban con miedo e impotencia.

Ese día se empezaba a materializar las ideas de Julio quien tenía la intención de crear una gran reserva forestal con los predios de todos ellos. Su crimen fue atreverse a sustituir los cultivos de coca reforestando con cacao y organizar un proyecto eco turístico campesino que contaba con el respaldo de la Dirección de Parques Nacionales y del Comité de Cafeteros. Se tenía planeado llamar a topógrafos, biólogos, ingenieros y arquitectos para consolidar el proyecto que se iba a extender sobre 500 hectáreas, empezando por Calabazos e ir uniendo a los campesinos de toda la troncal del Caribe para hacer un tapón ecológico que rodeara toda la Sierra, era un proyecto productivo y sostenible.

Amigos y familiares desde ese día y durante seis años emprendieron la búsqueda de Julio, quienes a través de organizaciones de derechos humanos sumaron esfuerzos para hallarlo y exigir el castigo de este crimen de desaparición forzada.

Como resultado de todos los esfuerzos se logra llevar hasta etapa de juicio el caso de Julio y mediante indagatoria obtienen las coordenadas del lugar de la fosa en donde reposaban sus restos mortales.

La investigación permitió establecer que la desaparición y posterior asesinato de Julio fue motivada por el interés del ex jefe paramilitar de la Sierra Nevada de Santa Marta, Hernán Giraldo, de apoderarse de sus terrenos para sembrar cocaina. En contra de dicho paramilitar y de Leonidas Acosta, se emitió resolución de acusación, condenándolos a 38 años y cinco meses de prisión quienes antes de ser extraditados a los Estados Unidos, el 13 de mayo de 2008, fueron escuchados en juicio el 20 de marzo del 2007.
