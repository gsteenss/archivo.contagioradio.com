Title: "Inhabilidad a Magistrados del Tribunal de Paz es inconstitucional" Claudia Vacca
Date: 2017-11-16 12:47
Category: Entrevistas, Paz
Tags: acuerdos de paz, el derecho a la protesta social y la jurisdicción especial indígena, Tribunal de Paz
Slug: incostitucional-comite-escogencia-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 Nov 2017] 

Claudia Vacca, integrante del Comité de Escogencia del SIVJRNR, expresó que la decisión que se tomó ayer en el Congreso referente a que las personas que hayan tenido trayectoria en la defensa de los derechos humanos no podrán hacer parte de la JEP, **es confusa, contradictoria, inconstitucional y dilatará más el tiempo en que se inicien los procesos para esclarecer la verdad del conflicto armado**.

**Las contradicciones del Congreso**

Una de las contradicciones más importante que ahora deberán tramitarse en el Congreso será sobre el régimen de inhabilidades, debido a que la Corte Constitucional había manifestado que los criterios para los magistrados que hicieran parte del Tribunal de Paz, serían los mismas que para los magistrados de las Altas Cortes.

“Estamos a la expectativa como comité, de **cuál va a ser la salida que tomará el gobierno Nacional**, dado que nosotros en los tiempos establecidos por los mandatos legales en los que nos dieron las funciones cumplimos en estricto sentido” señaló Vacca. (Le puede interesar: ["Las preocupaciones que deja el fallo de la Corte sobre la JEP")](https://archivo.contagioradio.com/49091/)

De igual forma, la integrante del Comité de Escogencia afirmó que la contradicción más grande es que "entre más se dilate el inicio y conformación de la Jurisdicción Especial para la Paz, **más se dilatará el sometimiento de los diferentes implicados de delitos atroces**”.

### **Los criterios del Comité de Escogencia** 

“En principio el mandato del comité estaba claramente delineado en el acuerdo final de paz, en donde están contenidos los criterios generales para el efecto de seleccionar a los magistrados para el Tribunal de Paz y **el comité de escogencia consideró esos contenidos como un marco general**”.

Dentro de esos criterios se encontraban que la experiencia fuera la misma que se exige a los magistrados de Altas Cortes del país, **que tuvieran experiencia en el derecho Internacional Humanitario, conocimiento profundo en derechos humanos con enfoque en el tratamiento de los derechos de las víctima**s. Además, Vacca manifestó que en el proceso se tuvo en cuenta la idoneidad y la trayectoria de los candidatos y candidatas.

“Es muy contradictorio, e incluso me atrevo a afirmar que inconstitucional, que después de haber establecido unos criterios, se nos dé una señal como sociedad de que tendríamos que, después de haber hecho una designación, cambiar los criterios incluso más allá de lo establecido” afirmó Vacca. (Le puede interesar: ["Víctimas interponen tutela contra el Congreso para implementar el Acuerdo de Paz"](https://archivo.contagioradio.com/victimas-y-organizaciones-interpusieron-tutela-para-que-congreso-implemente-acuerdo-de-paz/))

Para Claudia Vacca, las exigencias que está haciendo el Congreso son similares a que se hiciera una convocatoria para docentes en Biología, se escogieran los docentes y luego se expresara que el requisito más importante es que no sepan de biología. Es decir que la inhabilidad que se está incorporando es la condición primordial para la escogencia de los magistrados.

### **Inhabilidad de magistrados es inconstitucional ** 

Por su parte la Secretaría Ejecutiva de la Jurisdicción Especial para la Paz, informó en un comunicado de prensa, que la inhabilidad aprobada por el Congreso es inconstitucional y agregó que hay que esperar a los que suceda el día de hoy en la Cámara de Representantes y si es aprobada esa inhabilidad, llevar este proceso hasta la Corte Constitucional.

Además reiteró que el proceso de implementación de los Acuerdos de paz continúa en marcha y que esperan que la Jurisdicción Especial para la paz esté en pleno funcionamiento, a más tardar a mediados del mes de enero de 2018.

<iframe id="audio_22116709" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22116709_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
