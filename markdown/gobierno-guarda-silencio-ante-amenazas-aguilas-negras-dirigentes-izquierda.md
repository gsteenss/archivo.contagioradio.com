Title: Gobierno guarda silencio ante amenazas de Águilas Negras a dirigentes de izquierda
Date: 2019-01-31 12:02
Author: AdminContagio
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas a dirigentes
Slug: gobierno-guarda-silencio-ante-amenazas-aguilas-negras-dirigentes-izquierda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-4-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 31 Ene 2019

Los dirigentes políticos amenazados por Águilas Negras este 30 de enero, no han recibido ni una sola llamada de la Fiscalía o del Ministerio de Defensa, que son dos de las entidades que deberían apersonarse de las denuncias**. Tampoco han recibido ningún tipo de entrevista por parte de la UNP para establecer si es necesario que se refuerce su seguridad, según la denuncia del Senador Antonio Sanguino.**

Antonio Sanguino, quien recibió las amenazas vía mensaje de texto, afirma que es “inexplicable” la actitud de las autoridades, no solamente porque no se ha hecho nada en este caso concreto, sino que desde hace varios años se han conocido panfletos firmados por las Águilas Negras, y según el senador no se conoce de una sola captura o un operativo contra dicha estructura que parecería tener un alcance nacional.

Lo cierto es que, tanto para los congresistas como para los líderes sociales, "es necesario que el Gobierno cambie de actitud y cumpla con su deber de proteger a los colombianos y deje de estar mirando a Venezuela cuando olvida sus labores en el país", aseguró el congresista.

> Denuncia: a mi washap me ha llegado esta amenaza contra varios dirigentes d oposición! Agregan además el mensaje “muerte a todo hp..” señores [@UNP](https://twitter.com/unp?ref_src=twsrc%5Etfw) y ⁦[@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw)⁩ , demandamos garantías y protección !! [pic.twitter.com/XGzxGtNrog](https://t.co/XGzxGtNrog)
>
> — Antonio Sanguino Senador (@AntonioSanguino) [30 de enero de 2019](https://twitter.com/AntonioSanguino/status/1090629208865419266?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Amenazas de las Águilas Negras son reiteradas y muchas veces terminan en asesinatos** 

El Centro Integrado de Información de Inteligencia contra el **Crimen Organizado (CI3-CO), un grupo de la Policía Nacional**, aseguró que no existen las Águilas Negras. Esto después del “análisis de 282 panfletos que fueron distribuidos en todo el territorio nacional desde 2006 hasta 2018.

De ellos, **dice el CI3-CO, 202 fueron panfletos impresos, 35 en WhatsApp, 32 en otras redes sociales y 13 por medio de correos electrónicos.** Además estas actuaciones se han presentado en ocho regiones del país. Sin embargo, como lo resalta Sanguino no hay una sola captura relacionada con esta denominación, y el hecho de negar su existencia pareciera volverse una excusa para no investigar estas actuaciones criminales.

###### Reciba toda la información de Contagio Radio en [[su correo]
