Title: Otra Mirada: La verdad del pueblo negro
Date: 2020-07-18 15:18
Author: PracticasCR
Category: Nacional, Otra Mirada, Otra Mirada, Programas
Tags: Comisión de la Verdad, Pueblos negros
Slug: otra-mirada-la-verdad-del-pueblo-negro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/La-verdad-del-pueblo-negro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @ComisionVerdadC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 28 de agosto, la Comisión de la Verdad hará un reconocimiento a las comunidades negras, afrocolombianas, indígenas y palenqueras en relación a los impactos que ha tenido el conflicto armado en estas comunidades, las cuales a pesar de la violencia y el olvido del Estado y de la misma sociedad, han resistido y ahora, junto a la Comisión, contribuyen a construir la paz. (Le puede interesar: [971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dado que la situación de los pueblos negros nuevamente es preocupante debido a nuevos actos de violencia por parte de grupos armados, siendo el racismo un factor explicativo de la violencia política y armada, es pertinente entender y conocer en qué consiste esa verdad del pueblo negro y cómo hasta el día de hoy se sigue luchando por conocerla.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el pacífico y el caribe estuvieron Orlando Castillo líder y defensor de derechos étnicos territoriales del Espacio Humanitario Puente Nayero - Buenaventura, Audes Jiménez González coordinadora territorial Atlántico-, Norte de Bolívar y San Andrés, Ángela Salazar comisionada de la verdad y Alicia Mosquera de la Asociación de Mujeres Desplazadas de Riosucio, Clamores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde sus experiencias, como comunidad negra y miembros de organizaciones, comparten cómo se resiste desde los pueblos afros, palenqueros y negros y cuál es la verdad que se ha venido construyendo y que debe llegar a los oidos de todos los colombianos para que entendamos lo que sucede en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, explican cuál es la situación actual en medio de la pandemia y cuáles son las acciones que como comunidad no solo negra sino como colombianos debemos realizar para ponerle fin a la violencia. Además, comparten cuál es el compromiso desde la Comisión para ayudar a que la verdad que se ha estado construyendo desde décadas, sea conocida.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esperan que se lleve a cabo un diálogo con todos los sectores del país para ver los factores de persistencia y comparten su apuesta de escuchar las voces de personas que fueron violentadas pero igualmente de actores responsables para avanzar a una propuesta de reconciliación y llegar a la verdad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Tenemos que seguir pensando que desde donde estemos podamos seguir aportando”.
>
> <cite>Audes Jiménez González </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 15 de julio:  <https://www.facebook.com/contagioradio/videos/222598968850182>

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/2654846704781926","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/2654846704781926

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
