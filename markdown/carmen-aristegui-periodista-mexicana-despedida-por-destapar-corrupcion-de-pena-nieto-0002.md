Title: Carmen Aristegui, periodista mexicana despedida luego de destapar corrupción de Peña Nieto
Date: 2015-03-16 18:39
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Carmen Aristegui, Libertad de expresión, mexico, MVS
Slug: carmen-aristegui-periodista-mexicana-despedida-por-destapar-corrupcion-de-pena-nieto-0002
Status: published

###### Foto:Periodicolarepublica.com.mx 

##### **Entrevista con [Carlos Fazio]:**  
<iframe src="http://www.ivoox.com/player_ek_4222135_2_1.html?data=lZeflJaXeY6ZmKiak52Jd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmzsrbjabWrdToxsziy4qWh4zkxtfW0cnNt9XVjNLS2s7Hpc%2FVjMnS1dXJqMrYwpDd0dePqMbnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

La compañía de comunicación mexicana **MSV** despidió este domingo a la periodista **Carmen Aristegui**, quién ha denunciado** **varios casos de corrupción del **presidente Peña Nieto y sus familiares cercanos.**

**Daniel Lizarraga e Irving Huerta**, dos de los **colaboradores de la periodista**, fueron **despedidos** por la empresa el viernes pasado, por comprometerla utilizando el nombre para promover **México-leaks**, portal para investigar casos de corrupción que serían publicados en 5 portales diferentes.

Aristegui, ha exigido el reintegro de Lizarraga y Huerta a su equipo de investigación, sin embargo MVS, consideró la exigencia como un ultimatum y un condicionamiento. Este Domingo se conoció que la empresa decidió retirar de su cargo a Aristegui argumentando que no hay interés por su parte en entrar al grupo de México-Leaks.

Ambos periodistas han trabajado con Carmen Aristegui en la investigación de la financiación de la mansión, **"La Casa Blanca"** del presidente Peña Nieto, la **mansión del secretario de hacienda Luis Videgaray, o la red de trata de personas y prostitución en la que estuvo implicado el presidente del PRI**. Todos ellos escándalos de corrupción y financiamiento ilegal.

La periodista recientemente había denunciado el escándalo inmobiliario del presidente de la nación y de su esposa. En declaraciones públicas Carmen ha dicho "Esta batalla, no lo dude, nadie es por nuestra libertad", y que va a acudir a la justicia para reclamar por su despido.

Según **Carlos Fazio**, periodista y analista político e la UNAM de México, el despido de la periodista responde a las **investigaciones realizadas por la misma sobre miembros del gobierno referidos a casos de corrupción.**

Para Carlos Fazio este es un intento de **controlar los medios de comunicación**, en un año donde queda clara la vinculación del gobierno con grupos paramilitares y al margen de la ley y la sistemática violación de derechos humanos del gobierno.

Un ejemplo de ello, es el **espacio radiofónico**, controlado en un **90% por 12 familias.** Para el analista político de la UNAM, este despido es un claro **ataque contra la libertad de expresión** y contra el periodismo independiente.

Por último Carlos Fazio indica que la **indignación y la protesta están en auge** en el país debido a los escándalos de violaciones de DDHH, corrupción y aplicación agresiva de políticas neoliberales. Pero ante todo la **lucha por esclarecer la desaparición forzada de los 43 estudiantes de Ayotzinapa** es la que más fuerza ha tenido y ha mantenido la protesta hasta el día de hoy.
