Title: Amenazas y atentados contra ambientalistas son "sistemáticas" y siguen impunes
Date: 2017-07-31 13:41
Category: Ambiente, Nacional
Tags: Amenazas, Cajamarca, consultas populares, defensores del ambiente, Global Witness, medio ambiente
Slug: amenazas-y-atentados-contra-ambientalistas-son-sistematicos-y-siguen-impunes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Megamineria-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [31 Jul 2017] 

Tras el atentado que sufrieron 2 miembros del Colectivo Socio Ambiental Juvenil de Cajamarca (COSAJUCA), ellos han manifestado que **las amenazas y la estigmatización en razón de su actividad de ambientalistas son sistemáticas**. Además este tipo de situaciones se han incrementado al mismo tiempo que las acciones en defensa del agua o los territorios en el país.

Para Jaime Tocora, integrante del Comité Ambiental “**es complicado ver cómo quieren eliminar de manera física a los que creemos en la posibilidad de defender el territorio**, el agua y el aire”. Igualmente, manifestó que “lastimosamente este tipo de actos contra los defensores del ambiente ya se han normalizado”.

Según la denuncia, el viernes en horas de la noche, las dos personas del Colectivo COSAJUCA, se estaban devolviendo a la ciudadela de Cajamarca desde la vereda Rincón el Placer. Mientras caminaban **“fueron alumbrados con linternas y posteriormente les dispararon”**. Sin embrago, las dos personas no resultaron heridas. (Le puede interesar: ["Atentan contra dos líderes de la consulta popular en Cajamarca"](https://archivo.contagioradio.com/44430/))

Este atentado no ha sido el único que han sufrido los miembros del Colectivo Juvenil Socio AmbientaL. Tocora expresó que son reiteradas y sistemáticas las agresiones y las violaciones a los derechos humanos de los defensores hasta el punto que **ya han sido asesinados dos de sus integrantes**, Juan Camilo Pinto en 2013 y Daniel Humberto Sánchez en 2014.

Según Tocora, la fuerza pública también ha actuado “de manera criminal porque en cada espacio que se quiere organizar para defender el ambiente, suceden actos violentos contra los defensores.”  **Ellos ya han interpuesto las denuncias requeridas ante las autoridades competentes** y esperan que no sucedan más atentados contra la vida de las personas que impulsaron la consulta popular en este municipio.

**Colombia es el segundo país más peligroso para la actividad de defensores del ambiente**

A mediados de Julio, en el informe “Defender la tierra”, la organización Global Witness alertó que Colombia **es el segundo país más peligroso para los defensores del ambiente después de Brasil**. Según la Organización, En 2016 fueron asesinados 37 defensores del ambiente y el gobierno da garantías para que los asesinatos no se queden impunes. Sin embrago, casos como el descrito anteriormente en Cajamarca, dan cuenta e ilustran la realidad de esta investigación. (Le puede interesar: ["Colombia es el segundo país más peligroso para defender el ambiente"](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/))

Ante esto, la Organización ha sido muy enfática en manifestar que a los defensores y defensoras se les debe dar **garantías para ejercer su labor y el Estado debe proteger las iniciativas de las comunidades que se encuentran defendiendo su territorio**. Es por esto que Tocora hizo un llamado al Gobierno Nacional para que de las garantías necesarias para garantizar la paz en los territorios.

Finalmente, el defensor del ambiente manifestó que se están enfrentando a **“empresas criminales que se han tomado atribuciones** y el Gobierno ha hecho caso omiso a la problemática”. Sin embargo, dijo que seguirán realizando la labor de defensa del territorio con la convicción de que “es necesario defender el ambiente, el agua y la vida”.

<iframe id="audio_20086407" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20086407_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
