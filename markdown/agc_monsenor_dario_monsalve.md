Title: La respuesta de las AGC al llamado de paz de Monseñor Darío Monsalve
Date: 2017-12-10 19:26
Category: Nacional, Paz
Tags: AGC, Monseñor Darío Monsalve
Slug: agc_monsenor_dario_monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Autodefensas-gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [9 Dic 2017] 

Tras el llamado del Arzobispo de Cali, Darío de Jesús Monsalve, en el que hace un llamado apremiante a la conciencias de quienes están alzados en armas para que se pronunciaran con hechos de paz, los paramilitares de las Autodefensas Gaitanistas de Colombia han respondido a Monseñor, **asegurando que saludan el llamado de la Iglesia y que quieren aportarle a la paz.**

![d896f4d4-05b8-42eb-afcc-a0cb233a9350](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/d896f4d4-05b8-42eb-afcc-a0cb233a9350-486x800.jpg){.aligncenter .wp-image-49834 .size-medium width="486" height="800"}

En ese sentido en un comunicado publicado el pasado 6 de diciembre, afirman que **están analizando las medidas que adoptarán como Organización** para sumarse "a tan noble empeño, con hechos que así lo atestigüen".

De igual manera en el comunicado señalan **su indignación por los incumplimientos a los acuerdos de La Habana**, y al aumento de la violencia en el país durante este año. "Por eso saludamos con esperanza y expectativa el llamado de Monseñor, porque apunta a romper la espiral de violencia en la que nuevamente estamos sumidos, después de un corto respiro. Es un llamado valiente, porque va dirigido a todos los alzados en armas, con el mejor ánimo inclusivo, que es lo que se requiere para empezar a salir de tanto problema".

**Aquí el comunicado completo**

*Frente al clima de desasosiego, de incertidumbre e incluso de desencanto que se percibe en el país con respecto a los esfuerzos de paz, hemos conocido el “apremiante” llamado hecho por el arzobispo de Cali, Monseñor Monsalve, para no desfallecer en la búsqueda de la concordia y la reconciliación nacional.*

*Muy oportuno el clamor de Monseñor. El país, que un momento pareció enrutarse por la senda de la paz, hoy vive nuevos desafíos que es necesario superar. Pero éste no es un problema exclusivo del Gobierno, de las Fuerzas de Seguridad del Estado, de la Fiscalía o de otras instancias oficiales. *

*No. Los problemas de la violencia, sus orígenes y sus soluciones, no empezarán a ser vistos en su real dimensión, que implica tomar los correctivos necesarios para evitar su incesante repetición, si una parte considerable de la sociedad colombiana no siente la violencia como uno de sus grandes problemas, si no dejamos de ver el conflicto como si se tratara de otro país, si solo lo sentimos por lo que nos informan de manera parcializada los noticieros de televisión, que obviamente representan sus propios intereses y no los de la sociedad.*

*Hemos practicado un autismo a conciencia, esperando que otros pongan los esfuerzos que se requieren para lograr la paz. Por eso vemos con total indiferencia como una clase política irresponsable incumple alegremente con los acuerdos de paz que se firmaron con las Farc, y antes les celebramos el chistecito. Con razón alguien ha calificado el hecho como perfidia el incumplimiento que se da en temas fundamentales como el de la JEP y quienes deben rendir cuentas ante esta justicia transicional. *

*Recordamos como en el pasado, cuando se creó un impuesto para la guerra, los gremios saludaron con entusiasmo la medida. Ahora, cuando se ha hablado de un impuesto para la paz, las críticas han sido acerbas, con veneno y rencor. *  
*Por eso saludamos con esperanza y expectativa el llamado de Monseñor, porque apunta a romper la espiral de violencia en la que nuevamente estamos sumidos, después de un corto respiro. Es un llamado valiente, porque va dirigido a todos los alzados en armas, con el mejor ánimo inclusivo, que es lo que se requiere para empezar a salir de tanto problema. *  
*De parte nuestra, seguimos con el mejor ánimo para aportarle a esta paz incluyente, en el mismo sentido del mensaje que nuestro Comandante Dairo Antonio Úsuga hizo en septiembre pasado dirigido a la sociedad colombiana, y la carta abierta que hizo pública enviada a su santidad Francisco, con motivo de su visita a nuestra patria. *  
*La iglesia católica sigue siendo un referente ético y un faro moral para la sociedad colombiana. De ahí la importancia que le damos a las palabras de Monseñor. Estamos analizando las medidas que adoptaremos como Organización para sumarnos a tan noble empeño, con hechos que así lo atestigüen. *  
*Montañas de Colombia, diciembre 6 de 2017*
