Title: Para esclarecer la verdad del paramilitarismo deben darse reformas en las FFMM
Date: 2015-12-01 12:03
Category: Nacional, Paz
Tags: Derechos Humanos, Diálogos de paz en la Habana, esclarecimiento del paramilitarismo, pablo catatumbo, paramilitares en Colombia, paramilitarismo en Colombia, Radio derechos Humanos
Slug: para-esclarecer-la-verdad-del-paramilitarismo-deben-darse-reformas-en-las-ffmm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Pablo-Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:las2orillas.com 

###### [30 Nov 2015]

La propuesta de esclarecimiento del paramilitarismo por parte de las FARC como **garantía de seguridad para un eventual post acuerdo y  una probable dejación de las armas**, ha sido calificada como un paso hacia el esclarecimiento de la verdad y una posibilidad de garantía de no repetición. Esto según diversas organizaciones sociales del país.

El anuncio por parte de las FARC expresa** una preocupación de diferentes organizaciones de derechos humanos por "denuncias que han venido directamente de las víctimas"**, afirma Danilo Rueda, integrante de la comisión de justicia y paz, como los últimos reportes de **amenazas a Reinaldo Rojas, líder indígena de la comunidad Sikuane** de Caño Ovejas, quien  fue hostigado por paramilitares que operan en Mapiripán.

Rueda valora que el paramilitarismo es un factor de riesgo si se llegan a firmar los acuerdos y hay dejación de armas por parte de la guerrilla para la reincersión a la vida civíl de los miembros ya que **este actor (los paramilitares) "iría a actuar contra algunas de las personas de este grupo guerrillero"**, si permanecen en el contexto social colombiano.

Esta notificación por parte de las FARC es importante ya que el paramilitarismo ha configurado en el país una "estrategia represiva" en el modelo de sociedad y Estado en Colombia, por esto **este esclarecimiento podrá identificar sujetos responsables con miras a un cambio de doctrina militar y de doctrina policial**.

Ese cambio de doctrina de las fuerzas oficiales estaría dirigido a d**efinir "cuál es el rol que cumple la fuerza pública y los organismos de seguridad en un Estado democrático que quiere construír la paz"**, indica el integrante de justicia y paz además de indicar el papel de Estados Unidos y Francia en la formulación de políticas de seguridad de inteligencia civíl, que han dado pie al paramilitarismo, sumado a esto cómo se dió "piso legal para estas operaciones", valora Rueda, esto para el desmonte de esta doctrina y también el desmonte de esa lógica de los ciudadanos.

Por último Danilo Rueda afirma que en **algunos de los sectores militares se percibe un ambiente favorable a la verdad sobre el paramilitarismo,** no solo en cuanto a miembros activos, sino también militares que están condenados en este momento "dispuestos a que haya modificaciones de la fuerza pública" para que "se esclarezca toda esta situación".
