Title: Políticos y empresarios no han sido investigados pese a declaraciones de "El Alemán"
Date: 2015-07-31 16:25
Category: Nacional, Paz
Tags: alvaro, Antioquia, el aleman, fredy rendon herrera, operación orion, Paramilitar, rito alejo, uribe
Slug: politicos-y-empresarios-no-han-sido-investigados-pese-a-declaraciones-de-el-aleman
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/adn.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: ADN 

<iframe src="http://www.ivoox.com/player_ek_5693828_2_1.html?data=lpumlZ2WfI6ZmKiak5iJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDghqigh6aouMrX0NiY25DJsdHmxtjO1M7Tt4zi0JDVw9OPt8rY0JDW0NvJt9XdyMbR0diPtMbnxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Danilo Rueda, Comisión de Justicia y Paz] 

###### [31 jul 2015] 

El 15 de julio del año 2003 se firmó el acuerdo de Santa Fé de Ralito, como antesala al proceso de entrega de jefes paramilitares ante la justicia, y en el cuál se acordó que estos cumplirían penas máximas de 8 años en prisión en las cárceles colombianas, si contribuían en el proceso de verdad y reparación.

Con base en este acuerdo, el día de ayer, jueves 30 de julio, Fredy Rendón Herrera, alias "El Alemán", uno de los responsables del desarrollo del paramilitarismo en los departamentos de Antioquia y Chocó, y conocido por haber promovido la Operación Génesis junto al General Rito Alejo del Rio en 1997, volvió a la libertad, tras dejar la cárcel de Itagüí en Antioquia.

<div>

Hace unos meses, las víctimas de "El Alemán" manifestaron sus objeciones ante la eventual libertad del jefe paramilitar. La Comisión de Justicia y Paz, que lleva la defensa jurídica de las comunidades del Cacarica, presentó en el año 2004 una solicitud ante la Corte Interamericana de Derechos Humanos para que se tomaran medidas frente a la violación a los Derechos Humanos que se generaron a raíz de la Operación Génesis; y en el 2013, la Corte Interamericana ordenó al Estado Colombiano investigar los hechos que rodearon la Operación, incluyendo la responsabilidad de altos mandos militares y sectores empresariales que se beneficiaron con su desarrollo.

A la fecha, y a pesar de la información develada por el jefe paramilitar, la FIscalía no ha adelantado investigaciones contra empresarios nacionales e internacionales, y políticos que participaron en la toma de decisiones en la Operación, o que se vieron beneficiados por la misma.

"Nos quedamos con un sinsabor. La verdad puede ser parcial, pero incluso lo poco que dijo EL Alemán no ha sido investigado por el aparato judicial, que teme vincular a altos generales de la república, a ex-presidentes, y a sectores empresariales de mucho poder", asegura Danilo Rueda, de La Comisión de Justicia y Paz. "No ha habido reparación. Lo que ha habido es la consolidación o pretensión de consolidación del proyecto económico que ampararon los paramilitares, y que esta relacionado con sectores antioqueños, y con multinacionales", agregó.

Las víctimas esperan que se investigue, por ejemplo, al vinculación de Maderas del Darien, empresa del grupo Sarmiento Angulo que según confesara alias "El Alemán" apoyó logísticamente la operación, o al entonces gobernador de Antioquia, Alvaro Uribe, por autorizar el desarrollo de la misma.

"Todo depende de la Fiscalía, que parece tener voluntad solo de judicializar a las FARC y al ELN. Ese es el fracaso del sistema penal en Colombia, que parece estar constituido para favorecer a un sector privilegiado de colombianos y no al conjunto de la sociedad", anadió Danilo Rueda.

En la constitución de una Comisión de Esclarecimiento de la Verdad, como se ha venido planteando, es necesario que las declaraciones de personas como alias "El Alemán" den elementos para esclarecer de manera  exhaustiva el fenómeno del paramilitarismo en Colombia.

</div>
