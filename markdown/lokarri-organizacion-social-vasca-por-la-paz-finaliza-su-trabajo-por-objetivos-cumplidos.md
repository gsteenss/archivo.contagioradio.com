Title: Lokarri, organización social vasca por la paz finaliza su trabajo por objetivos cumplidos
Date: 2015-02-24 23:15
Author: CtgAdm
Category: Entrevistas, Paz
Tags: abertzale paz, Elkarri, ETA desarme, Lokarri, País Vasco paz, Proceso de paz País Vasco
Slug: lokarri-organizacion-social-vasca-por-la-paz-finaliza-su-trabajo-por-objetivos-cumplidos
Status: published

###### Foto:Elcorreo.com 

##### **Entrevista a[ Manuel Vilabrille]:**  
<iframe src="http://www.ivoox.com/player_ek_4127878_2_1.html?data=lZafmZ2bfI6ZmKiakpyJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDfwtffy5DYqdPhytPOjdjZb9XmwsfOzNSPtNDmjMrZjdXWs8TZ1NSYxsqPtMLujMrbjcrQb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

A partir de marzo de 2015 **Lokarri dará por finalizado su trabajo en el País Vasco, debido al cumplimiento de sus objetivos principales** y a la situación histórica y proclive para un proceso de paz.

Elkarri predecesora de Lokarri nació hace 22 años con el **objetivo de facilitar un proceso de paz entre la organización armada ETA y el estado español**, con inclusión y participación de la ciudadanía vasca.

En **2006 y coincidiendo con la tregua de ETA se conforma Lokarri**, para poder conformar un clima de convivencia y aprovechar la tregua. Este proceso no funcionó y comenzó el trabajo para establecer unos mínimos para que se creara el clima de un nuevo proceso de paz.

Es cuando la organización decide fijarse dos objetivos, el primero **la inclusión de todos los partidos políticos, incluida la izquierda abertzale, en el panorama político vasco, y la finalización de la violencia de la organización armada ETA**. Es decir el fin de la lucha armada y la legalización de Sortu.

Estos **dos objetivos han sido cumplidos y según Lokarri el proceso de paz y la finalización de la lucha armada así como la participación de la izquierda abertzale es irreversible**. Es por ello que en la asamblea de 2013 se ha decidido finalizar el trabajo en marzo de 2015.

**A pesar de que no se ha dado el proceso de paz para la finalización del conflicto armado**, la participación y el consenso son realidades hoy en día y la situación es la más cercana para un sociedad en paz.

Entrevistamos a Manuel Vilabrille, miembro de Lokarri, quién nos cuenta las valoraciones de la asamblea y del trabajo durante estos años a favor de la paz.
