Title: Tejer para crecer
Date: 2016-04-25 08:48
Category: Eleuterio, Opinion
Tags: Brenda Portilla, mujeres
Slug: tejer-para-crecer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/tejedoras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Adrian Mealand 

#### Por **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 25 Abr 2016 

[En Colombia existen muchas comunidades indígenas, cada una con su propia cultura, su idioma y su particular visión del mundo. Hay 63 lenguas amerindias y al menos 2 afrocolombianas. “]*[Llamamos indígenas a aquellos que no participan de los ritmos y estilos de la vida en la ciudad. La idea de la naturaleza como madre, el vínculo que se vive con ella y su filosofía de cuidarla y protegerla es lo que tienen en común todos los pueblos indígenas.”]*[Nuestra compañera]**Brenda Portilla** [es oriunda de Nariño, en el sur de Colombia allí viven cinco comunidades indígenas bien diferenciadas. Esta región, con abundante selva virgen y una biodiversidad brutal, es todo un paraíso para los biólogos. Se trata también de una de las zonas más pobres del país, donde el conflicto armado se vive con mucha intensidad; allí convergen distintos actores: grupos de guerrilla y paramilitares y el fenómeno del narcotráfico.]

[Como consecuencia del] [conflicto armado colombiano que repercute sobremanera en la población de Nariño, son muchos los indígenas que se ven obligados a salir de sus tierras, son desplazados por la violencia y llegan a los pueblos primero y a las ciudades después. También el rápido avance del llamado progreso resulta una amenaza para su modo de vida. Brenda establece la diferencia entre el modo de vida indígena y la llamada modernidad sobre la concepción de] [los recursos naturales. “]*[Se trata de dos puntos de vista opuestos sobre la manera de entender la naturaleza, bien como algo a consumir, bien como algo que debe ser protegido.”]*

[Ante el modelo de vida del mundo moderno, que hasta hace poco era conocido como sociedad del bienestar, la compañera nos expone la idea del]**bienvivir.**[ “]*[Se trata de vivir con lo necesario y no depender de la moda, el consumo, el salario, el trabajo, el patrón... Lo llamamos bienvivir porque consiste en vivir tranquilo y feliz sin necesitar muchas cosas, tratando de aportar a tu sociedad lo que tengas y lo que te guste.”]*[ Se contrapone así al modelo del llamado desarrollo de consumo, lleno de necesidades siempre insatisfechas. “]*[El modelo del desarrollo y el consumo implica la explotación, la idea del hombre por encima de todo, de otros hombres, de las mujeres, de los animales, de la naturaleza.”]*

[Es en el mundo urbano donde se toman las decisiones acerca del progreso que luego se intenta aplicar a las periferias sin tener nunca en cuenta a la gente que vive allí. En Colombia, Bogotá centraliza toda la política. “]*[Nunca se le consulta a la gente de las zonas rurales, deciden lo que debe pasar en un lugar sin haber estado nunca allí. El pretendido progreso del turismo, el petróleo o el negocio del agua no nos interesa, llevamos mucho tiempo viviendo a nuestra manera y no por ello nos sentimos inferiores ni atrasados. Si quieren ayudar que nos procuren salud y educación”.]*

[El]**proyecto Tejedoras**[, un trabajo conjunto con las provincias de Putumayo, Cauca y Nariño, surge para mantener viva la tradición indígena así como para empoderar a las mujeres. Ellas son  quienes más sufren en comunidades amenazadas por el progreso y zonas de conflicto armado como es el caso de estas regiones. “]*[Estas situaciones repercuten en el aumento de violencia sobre las mujeres dentro de sus propias comunidades.”]*

[El proyecto trató en un primer momento de juntarlas para tejer para después ahondar en la recuperación de toda la tradición de su cultura que aparece en los tejidos, el simbolismo y la intención de los dibujos que tejen]*[ “Sirvió para ver cómo y por qué tejían, conocer los distintos materiales que cada cual emplea; se buscaba el sentido de esas confecciones y se fomentaba el intercambio de esos conocimientos. Muchos estaban basados en las ideas del cuidado de la madre naturaleza.”]*

[Estos intercambios de conocimientos a partir de una labor cotidiana, dieron pie a hablar de muchas otras cosas; de los otros trabajos que hacen, de su vida personal, de los problemas en sus comunidades, viendo como lo que les pasa a unas, les pasa también a las otras. “]*[Se llevaran consigo nuevas experiencias y conocimientos intercambiados, no sólo sobre los tejidos, sino también  diálogos que les hicieron reconocer la violencia de género que padecen  y esbozar ideas para dar una nueva visión de las cosas a sus hijas.]* *[Del encuentro se marcharon empoderadas. El proyecto Tejedoras logró crear espacios donde las mujeres se juntaran y hablaran de sus cosas y esto fue importante por la repercusión que tuvo después en los hombres, cuando las tejedoras regresaron a sus comunidades.”]*

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
