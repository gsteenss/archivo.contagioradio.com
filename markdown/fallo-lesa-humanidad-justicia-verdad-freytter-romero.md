Title: Con fallo de Lesa humanidad se avanza en justicia y verdad: hijo de Freytter Romero
Date: 2020-01-15 13:33
Author: CtgAdm
Category: DDHH, Judicial
Tags: Jorge Freytter Romero, policia, Universidad del Atlántico, víctimas de crímenes de Estado
Slug: fallo-lesa-humanidad-justicia-verdad-freytter-romero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/frey.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

La Dirección Especializada contra las Violaciones a los Derechos Humanos de la Fiscalía General de la Nación, declaró como delito de lesa humanidad el homicidio de **Jorge Adolfo Freytter Romero, profesor de la Universidad del Atlántico, ocurrido el 28 de agosto del 2001** y en el que estuvieron involucrados agentes del Gaula, la Policía y las Autodefensas Unidas de Colombia. Con esta declaración, el crimen pasa a ser imprescriptible, lo que equivale a que la investigación del caso seguirá hasta que sea resuelto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Su hijo, Jorge Freytter Florian, quien ha trabajado por el esclarecimiento este caso, señala que este es un primer paso para la verdad, la justicia, la reparación integral y la garantía de no repetición" lo que significa no solo un reconocimiento al caso Freytter, sino a su vez a las víctimas de crímenes de Estado y en particular a aquellas que surgen de la academia y del pensamiento crítico en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Freytter Romero quien defendía los derechos de los pensionados de la Universidad del Atlántico, fue víctima de seguimientos que condujeron a su posterior secuestro siendo llevado a una bodega sin ventilación, lugar en el que permaneció esposado a una tubería hasta que fue "torturado y ejecutado mediante asfixia con una bolsa y dos disparos". [(Le puede interesar: Exiliados colombianos exigen que "se detenga el genocidio político")](https://archivo.contagioradio.com/exiliados-buscan-participar-implementacion-del-acuerdo-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por medio de las investigaciones se ha podido establecer que en el asesinato participaron agentes del Estado junto a estructuras paramilitares, lo que ha permitido emitir condenas contra **exagentes de la Policía, el Gaula y contra Carlos Arturo Cuartas, alias Montería, quien hizo parte del bloque Norte de las Autodefensas Unidas de Colombia (AUC).** quien reveló que el asesinato fue cometido por Óscar Orlando Ortiz, alias 'Moncho', jefe paramilitar del frente José Pablo Díaz.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El fallo significa no solo un reconocimiento al caso Freytter, sino a su vez a las víctimas de crímenes de Estado y en particular a aquellas que surgen de la academia y del pensamiento crítico en Colombia"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a estas condenas, aún es necesario conocer quien o quienes fueron los autores intelectuales del homicidio, pues de avanzar en la investigación, no solo podría esclarecerse la verdad en el caso sino que arrojaría luces sobre el fenómeno paramilitar al interior de las universidades públicas del Caribe. [(Lea también: Universidad pública: Otro escenario donde se vive el conflicto armado)](https://archivo.contagioradio.com/universidad-publica-conflicto-armado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay que ponerles nombre y rostros a los ejecutores intelectuales del genocidio que vivió la Universidad del Atlántico y otras universidades públicas del [Caribe](https://www.justiciaypazcolombia.com/alfredo-rafael-francisco-correa-de-andreis/) y del país", afirma el hijo del académico.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/Ozfkuxvu46w","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/Ozfkuxvu46w

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph {"align":"justify"} -->

Según diversas investigaciones entre 2000 y 2019, se registraron 140 casos de asesinatos o desapariciones forzadas de estudiantes, sindicalistas y profesores, incluido el caso de Freytter Romero, por lo que, para su hijo, esta decisión da relevancia y visibiliza la situación de violencia que vive esta población y agrega que al ser considerado el primer caso de crimen de lesa humanidad de la Universidad del Atlántico, sirve como precedente jurídico para entender que existe un genocidio al interior de las universidades públicas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
