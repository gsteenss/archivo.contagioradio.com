Title: Campesinos de Tumaco persisten en los bloqueos de vía al mar
Date: 2017-04-12 13:13
Category: DDHH, Nacional
Tags: erradicación cultivos ilícitos, Tumaco
Slug: gobierno-prefiere-erradicacion-forzada-sobre-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jeremy Bigwood] 

###### [12 Abr 2017] 

Los campesinos de las comunidades que corresponden a las zonas del río Mira y la frontera con Ecuador decidieron retomar los bloqueos de la vía al mar entre Pasto y Tumaco, **luego de los incumplimientos del gobierno a los acuerdos del pasado 3 de abril, al continuar con la erradicación forzada.**

Durante las últimas conversaciones, el gobierno se había comprometido a **no erradicar durante una semana y ampliar los censos para que más campesinos se inscribieran en los planes**, sin embargo, ninguna de las dos condiciones se ha efectuado motivo por el cual los campesinos decidieron volver a bloquear. Le puede interesar: ["Campesinos de Tumaco y gobierno logran acuerdo sobre sustitución de cultivos"](https://archivo.contagioradio.com/campesinos-de-tumaco-y-gobierno-logran-acuerdo-sobre-sustitucion-de-cultivos/)

Para Iván Rosero, vicepresidente de la Junta de Acción del Carmen de Llorente, estos bloqueos se deben en gran medida a la **falta de divulgación del gobierno de los acuerdos que ya había firmado el pasado 3 de abril** con los campesinos cocaleros y al énfasis que continúa haciendo en querer erradicar forzadamente sin concertar con las comunidades.

“El difícil acceso a estas comunidades no permitió que funcionarios del gobierno hayan llegado o avisado oportunamente a los campesinos, **que se tenían que acoger a la sustitución y así evitar estos desmanes”.**

Las erradicaciones de cultivos ilícitos que se vienen haciendo en todo el territorio del país, hacen parte de una política Nacional que tiene como finalidad erradicar 100 mil hectáreas este año, de acuerdo con Rosero, lo que funcionarios del gobierno les han expresado es que se estima que sean “**50% de erradicación voluntaria y 50% de erradicación forzada y Tumaco al ser uno de los territorios con más cultivos, aproximadamente 17 mil**, esta como prioridad del gobierno”.

Esta política ha sido rechazada por las diferentes organizaciones campesinas del país, porque **va en contra de los Acuerdos de La Habana y de los planes de sustitución que se habían pactado como voluntarios y concertados**. Le puede interesar: ["Dijimos no a la erradicación, pero sí a la sustitución: Campesinos en Argelia, Cauca"](https://archivo.contagioradio.com/dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca/)

**La vía de Pasto-Tumaco se encuentra abierta durante una hora al día**, mientras que el gobierno de Nariño se está encargando de hacer una labor de acompañamiento a las comunidades para empezar con los planes de sustitución. Le puede interesar:["Un policía muerto y más de 5 campesinos heridos durante bloqueos en Tumaco"](https://archivo.contagioradio.com/un-policia-muerto-y-mas-de-10-campesinos-heridos-durante-bloqueos-en-tumaco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
