Title: Poeta cubano nuevo ganador del Premio Internacional Loewe
Date: 2015-11-11 14:11
Category: Cultura, Viaje Literario
Tags: Despegue, Maria Gómez Lara, Poemario Despegue, Premio internacional de poesía Iloewe, Premio Loewe, Premio Loewe 2015, Victor Rodriguez Nuñez
Slug: poeta-cubano-nuevo-ganador-del-premio-internacional-loewe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Rodriguez_Nunez_Victorc-Katherine_M_Hedeen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:poertyfundation.org 

###### [11 Nov 2015]

La Fundación Loewe" entregó** el Premio Internacional de Poesía al** escritor, periodista, crítico, traductor y profesor universitario cubano Víctor Rodríguez Núñez, **por su poemario "Despegue".**

Se trata de "un libro de veta cubana, osado, auténtico, con serenidad en el conflicto, que une el irracionalismo y la inmediatez social", calificativos del jurado integrado por Francisco Brines, José Caballero Bonald, Antonio Colinas, Óscar Hahn (ganador de la versión anterior), Cristina Peri Rossi, Soledad Puértolas, Jaime Siles y Luis Antonio de Villena.

El premio Loewe , creado en 1987, **busca impulsar la calidad en la creación poética en español.** Con Víctor Rodríguez son dos los escritores latinoamericanos que obtienen el premio, siendo el chileno Óscar Hahn el primero en obtenerlo por “Los Espejos Comunicantes” en 2014.

La organización entregó además un **galardón a Mejor Creación Jóven a Carla Badillo Coronado** del Ecuador, por su poemario “El color de la granada”, reconocimiento que el año anterior obtuvo la** colombiana María Gómez Lara** por el texto **“Contratono".**

**Sobre Víctor Rodríguez Nuñez**

Víctor Rodríguez ha publicado los poemarios "Cayama" (1979), "Con raro olor a mundo" (1981), "Noticiario del solo" (1987), "Cuarto de desahogo" (1993), "Los poemas de nadie y otros poemas" (1994), "El último a la feria" (1995), "Oración inconclusa" (2000), "Actas de medianoche I" (2006), "Actas de medianoche II" (2007), "Tareas" (2011), "Reversos" (2011), "Deshielos" (2013) y "Desde un granero rojo" (2013).

En la actualidad edita la serie latinoamericana de la editorial británica Salt y es subdirector de la revista literaria mexicana  "La Otra".
