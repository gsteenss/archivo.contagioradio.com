Title: "Romper conversaciones con ELN implica un nuevo ciclo de violencia"
Date: 2019-01-19 18:09
Author: AdminContagio
Category: Paz, Política
Tags: Álvaro Leyva, colombia, Cuba, ELN, Iván Cepeda, Iván Duque, Mesa de diálogos
Slug: romper-conversaciones-con-eln-implica-un-nuevo-ciclo-de-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Djh1sRdXgAAcPIg-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Equipopazgob] 

###### [19 Ene 2019] 

A través de un comunicado de prensa, el senador del Polo Democrático, Iván Cepeda y el político Álvaro Leyva, ambos facilitadores del proceso de paz, le recordaron al presidente Iván Duque que romper las conversaciones de paz con la guerrilla del ELN **"implica un nuevo ciclo de violencia"**, razón por la cual le pidieron al primer mandatario que reconsidere esa decisión, y al ELN que se pronuncie con relación al atentado perpetrado el pasado 17 de enero en la Escuela de Policía General Santander.

Los políticos reiteraron que la única forma para superar un escenario de guerra es la solución política, sin embargo afirmaron que, "en caso de que la decisión de dar por concluidas las conversaciones de paz con el Eln se mantenga", **le solicitan a Duque que los avances y acuerdos a los que llegaron las partes, durante estos años de negociación, sean preservados mediante un documento** que sea depositado ante el Consejo de Seguridad de la ONU y ante los países garantes, con el fin de que al reanudarse las conversaciones de paz se retome lo ya pactado.

Finalmente frente a la petición que Duque hizo al gobierno de Cuba, de capturar a los 10 integrantes del ELN que se encontraban en la mesa de diálogos en ese país, Cepeda y Leyva le recordaron que los términos en los que debe dejar el territorio de Cuba la delegación del Eln, **están taxativamente estipulados en los protocolos reservados que fueron convenidos para dicho fin entre las partes**. (Le puede interesar: ["En rede sociales señalan inconsistencias tras atentado a la Escuala Gral Santander"](https://archivo.contagioradio.com/las-inconsistencias-de-la-fiscalia-detras-del-atentado-a-la-escuela-general-santander/))

De igual forma, organizaciones defensoras de derechos humanos rechazaron el atentado en contra de la Escuela General Santander, en el que perdieron la vida 20 personas y manifestaron que "este hecho no puede convertirse en un pretexto para desistir de la vía negociada a la confrontación armada, incrementar la militarización y justificar el cierre de los espacios y libertades democráticas. Ante este panorama urge generar una amplia movilización de la ciudadanía para persistir en el fortalecimiento de la democracia y la vigencia irrestricta de los derechos y libertades públicas".

###### Reciba toda la información de Contagio Radio en [[su correo]
