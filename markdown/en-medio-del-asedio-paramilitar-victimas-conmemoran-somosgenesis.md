Title: En medio del asedio paramilitar víctimas conmemoran #SomosGénesis
Date: 2020-03-03 18:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #SomosGenesis, cacarica, clamores, ejercito, Festival de las Memorias, Operacion genesis
Slug: en-medio-del-asedio-paramilitar-victimas-conmemoran-somosgenesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.32-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.47-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-8.30.34-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-8.30.34-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.33.34-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto \#SomosGénesis/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Desde el 28 de febrero hasta el primero de marzo, se conmemoraron los 23 años de las víctimas de la Operación Génesis, un recorrido por la memoria que, aunque tuvo que sortear distintos obstáculos, logró romper el cerco del control paramilitar para rendir un sentido homenaje a la vida.*

<!-- /wp:paragraph -->

<!-- wp:image {"id":81573,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.33.34-PM-1-1024x576.jpeg){.wp-image-81573}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

La Operación Génesis fue una acción militar llevada a cabo por Brigada 17 del Ejército, con sede en Carepa, comandada por el general Rito Alejo del Río, en compañía de estructuras paramilitares, que produjo el desplazamiento de más de **15 mil personas** de la Cuenca del Cacarica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Más de dos décadas después las víctimas se reunieron durante dos días, con delegaciones internacionales de organizaciones defensoras de derechos humanos, la Comisión de la Verdad y líderes y lideresas de todo el país, para hablar de garantías de no repetición y la situación actual de las comunidades de esta zona que continúan **viviendo bajo el control paramilitar.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

La Ciudadela de Paz, una propuesta de vida
------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alicia Mosquera es una de las mujeres que integra la Asociación de víctimas de la violencia en Riosucio, Chocó, CLAMORES. El 24 de febrero de 1997 se encontraba ordeñando sus vacas cuando inició el bombardeo realizado por los batallones Fuerzas Especiales 1 y Contraguerrillas 35, adscritos a la Brigada 17.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ella, junto a algunos miembros de su familia, salieron despavoridos por el río Atrato, cruzaron el golfo de Urabá y llegaron hasta el Coliseo de Turbo, en el departamento de Antioquia, buscando refugio de las balas. Luego, conforme pasaron los días, el éxodo de **desplazamiento se hizo visible con el hacinamiento de más de 5000 personas** en el mismo coliseo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Vivíamos como animales, arrejuntados, muchos niños murieron" relató Alicia que, además de tener que padecer las precarias condiciones en el Coliseo por cuatro años, a ella le arrebataron a su hermano Adalberto Mosquera y posteriormente a su esposo Herminio Palomeque, asesinados en el marco de la operación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esos hechos de violencia provocaron que Alicia junto a otras mujeres decidieran quedarse en Turbo. Junto a ellas el pasado 28 de Febrero presentaran oficialmente, ante la Comisión de la Verdad, delegaciones internacionales y líderes sociales, su propuesta de reparación llamada Ciudadela de Paz, un conjunto de viviendas sostenibles, centros de memoria y **una sede de la Universidad de Paz** que garanticen el derecho a la vida en dignidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La propuesta de reparación se da después de cinco años de la decisión del Corte Interamericana de Derechos Humanos que reconoce la responsabilidad del Estado colombiano en la Operación Génesis. Razón por la que solicita medidas de reparación, que hasta la fecha actual no han tenido respuesta en los gobiernos de turno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desconocimiento que no ha frenado el trabajo de las víctimas ,"queremos que esta Ciudadela cuente con algunos módulos de la Universidad de Paz, que tengamos agua potable, parques recreativos para los niños, los adultos. **Necesitamos lugares para reunirnos, este es nuestro sueño**" afirmó Jeison Mena, jóven integrante de CLAMORES.

<!-- /wp:paragraph -->

<!-- wp:heading -->

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.47-PM.jpeg){.wp-image-81504}
---------------------------------------------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Alicia esta propuesta también hace parte de las garantías de no repetición, porque para las familias que no retornaron, la Ciudadela de Paz simboliza la posibilidad de no tener que volver a estar inmersos en las dinámicas del conflicto, que los niños y niñas no sean víctimas de reclutamiento forzado, que las mujeres no sean víctimas de violencias de género y que, de una vez por todas, el Estado cumpla con su deber de hacer efectivos los derechos que les han sido negados históricamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente al monumento, que relata el desplazamiento de las víctimas, y entre cantos y alabados, la Ciudadela de Paz nació con la esperanza de ser realidad. (Le puede interesar: "[Las memorias de Dabeiba: En busca de la verdad](https://archivo.contagioradio.com/las-memorias-de-dabeiba-en-busca-de-la-verdad/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

\#SomosGénesis se conmemora en medio de control paramilitar
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El 29 de febrero la conmemoración avanzó hacia el Golfo de Urabá para llegar a la zona humanitaria de Nueva Vida, ubicada en Cacarica, Chocó. Sin embargo, las cuatro embarcaciones que se dirigían hacia este lugar, fueron retenidas por más de tres horas en el punto de control de la Guardia Costera, aduciendo que no existían condiciones climáticas para cruzar el Golfo, y luego, a que había una medida de restricción por una visita que realizaba el presidente Iván Duque a la zona.

<!-- /wp:paragraph -->

<!-- wp:image {"id":81522,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![\#SomosGénesis](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-8.30.34-PM-1-1024x768.jpeg){.wp-image-81522}  

<figcaption>
Foto: Contagio Radio

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente el recorrido logró hacerse tras un acompañamiento que hizo la Guardia Costera hasta Bocas del Atrato. No obstante este no fue el único obstáculo que las 80 delegaciones tuvieron que sortear para llegar a Nueva Vida. Faltando algunos kilómetros para ingresar al río Cacarica, las barcas volvieron a ser detenidas, esta vez por un **punto de control del Ejército**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ambas situaciones, que reflejan la fuerte presencia de la Fuerza Pública en el territorio, se contraponen a las denuncias que han realizado las comunidades sobre el accionar de **grupos paramilitares en la Cuenca del Cacarica,** que no solo amenazan a las y los líderes sociales, sino que de acuerdo con las denuncias de la Comisión de Justicia y Paz, han mantenido en constante asedio a la población civil con acciones como el reclutamiento forzado de menores, obligar a campesino a la siembra de cultivos de coca y mantener el control sobre el lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hacia las 5 de la tarde, y luego de una extenuante caminata, la comitiva ingresó a la **zona humanitaria de Nueva Vida, un espacio conformado por más de 60 familias**, en donde está prohibido el ingreso de cualquier actor armado con la finalidad de que la población civil quede fuera de las acciones violentas. (Le puede interesar: ["Cacarica, más de dos décadas bajo el asedio paramilitar")](https://archivo.contagioradio.com/cacarica-mas-de-dos-decadas-bajo-el-asedio-paramilitar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Familias que tras 23 años, tampoco han podido retornar a sus tierras, porque, luego de la incursión del Ejército y el despliegue de asesinatos, desapariciones forzadas y torturas realizado por los paramilitares, **grupos empresariales, madereros y ganaderos ocuparon el territorio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y así, en medio del dominio paramilitar y la presencia del Ejército, abrazos se intercambiaron entre las bienvenidas y los reencuentros que hacen parte de los Festivales de las Memorias.

<!-- /wp:paragraph -->

<!-- wp:heading -->

\#SomosGénesis: la memoria es parte del tejido de reparación
------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Jhon Mena, integrante de la Asociación Comunidad de Autodeterminación, Vida y Dignidad, fue una de las personas que decidió conformar la zona humanitaria de Nueva vida y llegó allí con la ilusión de construir un lugar de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para él, este quinto Festival de las memorias tiene un papel fundamental, crear memoria con todos los integrantes de la comunidad, **desde los más chicos hasta los más grandes,** y hacerlo en el camino hacia la reparación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por ese motivo, la noche culminó con un conversatorio sobre garantías de no repetición, que contó con las perspectivas de la comisionada de la verdad, Patricia Tobón, ex integrantes de actores armados y líderes de distintas zonas de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Garantías que para las y los líderes que se reencontraron en esta zona pasan por la exigencia a los distintos grupos armados a que **cesen con las acciones violentas en contra de la población civil**, por la retoma de los diálogos entre el gobierno nacional y la guerrilla del ELN y el desmonte de las estructuras paramilitares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por ese motivo, y cansados de la inclemencia del conflicto las comunidades tomaron la decisión de enviar siete cartas a distintos responsables de la guerra, entre quienes se encuentran sectores empresariales y grupos armados, llamándolos a la construcción de la verdad y el fin de los enfrentamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hacia las seis de la mañana, salieron las delegaciones del Cacarica, con la satisfacción de tejer las apuestas de construcción de paz que vienen de las comunidades y que abogan por una Colombia distinta, en la que morir por defender la vida sea un mal recuerdo y no un temor latente. (Le puede interesar: [("Temor a retaliaciones de paramilitares de las AGC obligados a salir de Zona Humanitaria Nueva Vida")](https://www.justiciaypazcolombia.com/temor-a-retaliaciones-de-paramilitares-de-las-agc-obligados-a-salir-de-zona-humanitaria-nueva-vida/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":81505,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM-1024x576.jpeg){.wp-image-81505}  

<figcaption>
\#SomosGénesis

</figcaption>
</figure>
<!-- /wp:image -->
