Title: Hurtado y Moreno deben ayudar a esclarecer responsables en la cadena de mando hacia arriba
Date: 2015-03-02 20:24
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: bernardo moreno, Maria del Pilar Hurtado, Operaciones ilegales del DAS, ramiro bejarano
Slug: hurtado-y-moreno-deben-ayudar-a-esclarecer-responsables-en-la-cadena-de-mando-hacia-arriba
Status: published

###### Foto: Confidencial 

<iframe src="http://www.ivoox.com/player_ek_4154372_2_1.html?data=lZailpibdo6ZmKiakpqJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPV1JDgx9PYqc%2FXysaYxdTSqMbiwtnc1M7FaZO3jNjSjcrXtMbmwpDe18qPjNbm1cbR0ZDdb67jjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ramiro Bejarano, Ex-Director de el DAS] 

El viernes 27 de febrero, la Sala Penal de la Corte Suprema de Justicia declaró culpable a Maria del Pilar Hurtado y Bernardo Moreno, en el proceso por ilegalidades cometidas por el DAS.

En la ponencia presentada por Fernando Castro, logró demostrarse que si bien el Estado puede realizar labores de inteligencia, el modo de operar del DAS da cuenta de la realización de una serie de actividades ilegales, entre los que se destacan en el fallo condenatorio "concierto para delinquir", "peculado por apropiación", "violación ilícita de comunicaciones", "falsedad en documento público" y "abuso de autoridad".

Sin embargo, aún quedan cabos por atar el en caso del DAS. El próximo 5 de marzo se conocería la pena que cumplirían Hurtado y Moreno, sin embargo, se espera que ambos decidan colaborar con la justicia e identificar a más personas implicadas en la empresa criminal, a cambio de beneficios relacionados con rebaja de penas y acceso a casa por cárcel.

Aún es incierta la actitud que ambos ex-funcionarios tomarían frente a esta posibilidad. Para el ex-director del DAS, Ramiro Bejarano, una posible colaboración con la justicia debe poner en evidencia la cadena de mando hacia arriba de los ex-funcionarios, ya que de la cadena hacia abajo, ya hay condenados al rededor de 18 subalternos. María del Pilar Hurtado y Bernardo Moreno "están ad portas de enfrentarse a unas penas bastante rigurosas, de mínimo 10 años, y ellos tienen que sopesar si van a pagar eso solos, y van a dejar que otras personas queden impunes", indicó Bejarano.
