Title: La bicicleta, una alternativa a los hidrocarburos
Date: 2017-03-01 16:49
Category: Ambiente, Nacional
Tags: Bicicleta, Foro Mundial de la Bicicleta, Fundación Tortuga
Slug: la-bicicleta-una-alternativa-a-los-hidrocarburos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/bicicleta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twenergy] 

###### [1 Mar 2017] 

En los últimos años la bicicleta ha tenido bastante acogida en distintos países del mundo, este medio de transporte inventado en 1817, se ha convertido en la respuesta para muchos de lo que sería una apuesta **para la mitigación de emisiones de gases efecto invernadero y la reducción en el consumo de hidrocarburos.**

Estos serán algunos de los aspectos que se trabajarán en el Foro Mundial de la Bicicleta, que se llevará a cabo en Ciudad de México. En su sexta edición ‘Ciudades hechas a mano’, el Foro plantea hacer un trabajo minucioso al rededor de propuestas de ciudadanos y ciudadanas del mundo **para pensarse y construir ciudades mejor planeadas, “donde se priorice la accesibilidad, la movilidad sustentable, así como la seguridad e igualdad** de todos los usuarios de la calle”.

El Foro que tendrá una duración de cinco días, contará con diferentes ponencias, conferencias magistrales, paneles, talleres, intervenciones urbanas y presentaciones artísticas internacionales que tendrán lugar en diversos espacios del Centro Histórico de la Ciudad de México, así como en la Ciudad Universitaria y en la delegación Xochimilco.

### Dichos espacios de encuentro estarán a cargo de expertos en el tema, de 13 países diferentes. Una de ellas es Janette Sadik-Khan, norteamericana especialista en movilidad y urbanismo, ex Secretaria del Transporte de Nueva York y autora del libro ‘Manual de la Revolución Urbana’. 

Por otra parte, Juan Pablo Bejarano integrante de la Fundación Tortuga, una de las organizaciones colombianas que participará en el Foro, asegura que toda la ciudadanía tiene el derecho y la responsabilidad de exigir **“una movilidad más segura, equitativa y eficiente en las ciudades”.**

Bejarano también asegura que es fundamental realizar un trabajo educativo constante alrededor del uso de la bicicleta, no sólo en términos técnicos sobre su funcionamiento, sino en su utilidad para la vida cotidiana y el aporte que hacen los biciusuarios al ambiente, a entornos y estilos de vida más saludables y **“a la descongestión de nuestras ciudades tan mal pensadas y congestionadas”.**

Por último el integrante de la Fundación Tortuga, extiende la invitación a participar en este evento y a quienes no puedan asistir, a que particicipen enviando sus vídeos sobre la importancia del uso de la bicicleta al fanpage de Facebook del evento ‘Foro Mundial de la Bicicleta – World Bicycle Forum’.

<iframe id="audio_17304542" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17304542_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
