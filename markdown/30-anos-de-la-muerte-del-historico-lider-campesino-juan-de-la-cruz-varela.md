Title: 30 años de la muerte del histórico líder campesino Juan de la Cruz Varela
Date: 2015-01-26 23:16
Author: CtgAdm
Category: Movilización, Paz
Tags: Cabrera, Juan de la Cruz Varela, reforma agraria, Sumapaz resiste, ZRC
Slug: 30-anos-de-la-muerte-del-historico-lider-campesino-juan-de-la-cruz-varela
Status: published

**Crónica sobre el homenaje:**  
<iframe src="http://www.ivoox.com/player_ek_4001682_2_1.html?data=lZWdk5ucdo6ZmKiak5iJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtbVz5DRx5DQpYy309rnjbvFtsbgwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Contagioradio.com estuvo el viernes (23/01/50) pasado en el homenaje al histórico **líder campesino Juan de la Cruz Varela en la localidad de Cabrera**. En el mismo, acudieron distintas personalidades de la izquierda colombiana como Iván Cepeda o Jaime Caycedo, así como representantes de sindicatos y del Partido Comunista Colombiano, la Unión Patriótica o Marcha Patriótica.

En un ambiente combativo pero de gran **esperanza por las conversaciones de paz** que se adelantan en La Habana, los distintos líderes homenajearon la figura del dirigente campesino, haciendo hincapié en las repercusiones que ha tenido su lucha en los movimientos políticos actuales, en la lucha por la **reforma agraria** y en el propio proceso de paz.

Es importante remarcar que la localidad de **Cabrera**, siempre ligada a la trayectoria política del histórico líder, **es ejemplo de reforma agraria**, de sostenibilidad y de buen gobierno. El proceso de lucha que desembocó en la toma de tierras y en un armisticio, desarme y paz; es sin duda ,un claro ejemplo de lo que podría suceder en un escenario de postconflicto.

También los organizadores le dieron otro sentido, es decir **protestaron por la detención de 16 líderes campesinos de la región del Sumapaz**, acusados falsamente de rebelión por el estado colombiano.

No faltaron alusiones hacia los procesos de lucha que se están gestando en la región en contra de proyectos de extractivismo referidos a hidroeléctricas cuya ejecución pondría en peligro un recurso esencial para la vida, el agua de los páramos.

Para poder entender mejor la envergadura de la lucha de Juan de la Cruz Varela, entrevistamos a su hijo Juan de Dios Varela, también entrevistamos a Jennifer Cruz, edilesa de Sumapaz, para que nos explique lo sucedido con los detenidos y a Clímaco Pinilla, líder de la organización social de derechos humanos de Sumapaz DHOC, quién nos explica la amenaza extractivista que se cierne sobre el territorio.

\  
   
 
