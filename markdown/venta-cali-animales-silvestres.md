Title: Página de Facebook en Cali comercializa fauna silvestre
Date: 2016-01-17 21:35
Category: Voces de la Tierra
Tags: animales en vías de extinción, comercialización de fauna silvestre
Slug: venta-cali-animales-silvestres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/886917_1059512224080814_7517412292529272209_o-e1453082379649.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Facebook 

###### [17 Enero 2016] 

A través de redes sociales se ha denunciado una página de Facebook, conocida como **"Aves ornamentadas, Cali Colombia", donde se comercializa fauna silvestre.** La tienda se encuentra ubicada en la dirección  Calle 33A transversal 30-25 barrio, la Floresta en Cali.

Al ingresar, se promociona desde el 30 de diciembre la venta de **"Loros Amazonas" por pedido a \$300.000, así mismo, se encuentran  aves silvestres como flamingos, arrendajos, guacamayas amazonicas, tupirían toche, diamantes de Gould, gallito de roca, tortugas, turpiales, boa constrictor, serpientes coral, a** entre otros, en terribles condiciones, e incluso aseguran que hacen pedidos hacia otras ciudades del país.

"En aves ornamentales cali podras encontrar lo que estabas buscando. n**o solo manejamos aves. tambien tenemos variedad en mascotas, **tenemos casi toda clase de aves ornamentales. también trabajamos con sistema de separado o encargo donde puedes pagar la mitad para encargar el ave y la otra mitad cuando se te entregue el ave o mascota que estés buscando, todo interesado puede preguntar por la disponibilidad de las aves sin ningún compromiso", dice la página que cuenta con 1392 likes.

[![12570951\_10153680822220020\_854225949\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12570951_10153680822220020_854225949_n.jpg){.aligncenter .size-full .wp-image-19250 width="540" height="960"}](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12570951_10153680822220020_854225949_n/)[![12584136\_10153680822455020\_1879558502\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12584136_10153680822455020_1879558502_n.jpg){.aligncenter .size-full .wp-image-19259 width="540" height="960"}](https://archivo.contagioradio.com/venta-cali-animales-silvestres/12584136_10153680822455020_1879558502_n/)

[  
](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12584062_10153680822550020_703518922_n/) [![12584209\_10153680822340020\_672602485\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12584209_10153680822340020_672602485_n.jpg){.aligncenter .size-full .wp-image-19253 width="540" height="960"}](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12584209_10153680822340020_672602485_n/)

[![12576180\_10153680822485020\_1110443762\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12576180_10153680822485020_1110443762_n.jpg){.aligncenter .size-full .wp-image-19252 width="540" height="960"}](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12576180_10153680822485020_1110443762_n/)

[![12570907\_10153680822420020\_753491049\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12570907_10153680822420020_753491049_n.jpg){.aligncenter .size-full .wp-image-19249 width="540" height="960"}](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12570907_10153680822420020_753491049_n/)

[![12584062\_10153680822550020\_703518922\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/12584062_10153680822550020_703518922_n.jpg){.aligncenter .size-full .wp-image-19254 width="540" height="960"}](https://archivo.contagioradio.com/pagina-de-facebook-de-cali-comercializa-animales-silvestres/12584062_10153680822550020_703518922_n/)

Para denunciar esta situación ante las autoridades ambientales, es importante tomar pantallazos de los animales que son comercializados, y realizar la denuncia ante instituciones como **Policía Ambiental, Corporación Autónoma Regional al número (032)6206600, o al correo dipro.arpae@policia.gov.co.**
