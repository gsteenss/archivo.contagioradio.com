Title: Paramilitares asesinan a dos integrantes de la comunidad Wounnan
Date: 2017-08-24 13:06
Category: DDHH, Nacional
Tags: Indígenas Wounnan, paramilitares
Slug: paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/COLOMBIA-INDÍGENAS-e1496858353209.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Indígena] 

###### [24 Ago 2017] 

La organización indígena ASOWOUDACH, la Asociación de cabildos del Pueblo Wounaan y la Coordinación Nacional de Pueblos Indígenas denunciaron, a través de un comunicado, el asesinato de dos integrantes de su comunidad, las constantes amenazas a líderes y los riesgos que afrontan debido a los constantes enfrentamientos entre el ELN y el Ejército Nacional y **el control de estructuras paramilitares autodenominadas como Autodefensas Gaitanistas de Colombia, en el departamento del Chocó**.

De acuerdo con el comunicado, la semana pasada en la Cuenca de Quiparadó, **fueron asesinados un líder afro Manuel Ramírez y su compañera María Mepaquito, autoridad del resguardo**, dejando huérfanos a 4 menores de edad. Sumado a esta situación los indígenas denuncian “el confinamiento y las amenaza a líderes comunitarios dejando a la población en alta vulnerabilidad, destrucción de nuestro tejido social comunitario y graves afectaciones a nuestra vida cotidiana”.

De igual forma manifestaron que pese a las denuncias que han hecho de estas acciones en contra de la población, las autoridades institucionales y nacionales, no han tomado ninguna medida, razón por la cual están pidiendo **acompañamiento internacional y garantías para la vida digna en el territorio como voluntad de paz** por parte de todos los sectores armados que hacen presencia en este departamento. (Le puede interesar:["Paramilitares imponen control a transporte público en Chocó"](https://archivo.contagioradio.com/44498/))

“Exigimos se pare la guerra en nuestras comunidades y el fin del conflicto armado entre el ELN y el gobierno nacional que sigue justificando la muerte de nuestros comuneros con el accionar del **paramilitarismo en el departamento del Chocó que sigue operando aún en medio de la esperanza de paz** que se ha construido con los acuerdos de la Habana” afirmaron las comunidades en el comunicado de prensa.

### **Accionar de estructuras paramilitares en el territorio** 

De igual forma, la Comisión Intereclesial de Justicia y Paz denunció que el pasado 21 de agosto, en el territorio Humanitario y Biodiverso de la comunidad indígena Wounnan Unión Agua Clara, ubicado en Buenaventura, al margen del río San Juan, **el cuerpo de una mujer afro fue visto, con cinco impactos de arma de fuego**. (Le puede iteresar: ["La ONU advierte que la situación de los pueblos indígenas es peor que hace 10 años"](https://archivo.contagioradio.com/el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu/))

Este hecho también fue reportado a las autoridades, sin que se existiera reacción por parte del Ejercito Nacional o alguna otra entidad. Los habitantes afirmaron que debido al temor que sienten por el control paramilitar de grupos como los Urabeños o las autodenominadas Autodefensas Gaitanistas de Colombia, no retiraron el cuerpo del río.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
