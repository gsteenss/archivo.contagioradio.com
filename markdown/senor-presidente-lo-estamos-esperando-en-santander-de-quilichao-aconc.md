Title: "Señor Presidente, lo estamos esperando en Santander de Quilichao": ACONC
Date: 2019-05-09 19:03
Author: CtgAdm
Category: Comunidad, DDHH
Tags: Asociación de Consejos Comunitarios del Norte del Cauca, Santander de Quilichao
Slug: senor-presidente-lo-estamos-esperando-en-santander-de-quilichao-aconc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-95.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Francia Márquez] 

A pesar de que el Gobierno nacional estaba citado a una reunión con la Minga Suroccidental el pasado 8 de mayo, la Ministra del Interior Nancy Patricia Gutierrez nunca llegó a la vereda Lomitas, en Santander de Quilichao, Cauca, para dialogar con las comunidades étnicas sobre el cumplimiento de más de 300 acuerdos firmados desde 1986.

En su lugar, el Gobierno envió a la viceministra del Interior Camila Rivera, quien habló con representantes de la Minga sobre garantías de protección para los líderes sociales y el territorio, temas que tomaron mayor importancia en los últimos días después de que sujetos desconocidos atentaran en contra de líderes de procesos negros de la región el pasado 4 de mayo.

[Sin embargo, Víctor Hugo Moreno, consejero mayor de la Asociación de Consejos Comunitarios del Norte del Cauca  (ACONC), afirmó que no se pudo avanzar en ninguno de los puntos de la mesa ni firmar un acta de reunión por la falta de voluntad del Gobierno. (Le puede interesar: "[Atentan contra Francia Márquez y otros líderes de comunidades negras del Norte del Cauca](https://archivo.contagioradio.com/atentan-contra-lideresa-francia-marquez/)"]

Moreno señaló que el Presidente Iván Duque comunicó a través de la viceministra del Interior que no estaría dispuesto a firmar un decreto para garantizar la seguridad de los líderes sociales y el territorio. Esto a pesar de que las comunidades les han exigido medidas de seguridad que ataquen a raíz las causas de la violencia y puedan prevenir la ocurrencia de agresiones en contra de las comunidades.

El primer mandatario también propuso una reunión con la Minga Suroccidental en la Casa de Nariño en Bogotá el próximo 21 de mayo, no obstante Moreno manifestó que las comunidades negras lo seguirán esperando en Santander de Quilichao para discutir y refrendar el decreto. (Le puede interesar: "'[Muchos nos ven como una piedra en el zapato por defender el territorio': ACONC](https://archivo.contagioradio.com/muchos-nos-ven-como-una-piedra-en-el-zapato-aconc/)")

Frente a esta situación, las comunidades se han declarado en asamblea permanente para acordar una agenda que se trabaje en una futura reunión con el Presidente y protocolos de seguridad que se implementen durante su visita. Además, preparan una reunión en el marco de la Minga Nacional y buscan realizar actividades de sanación para los integrantes de esta movilización.

<iframe id="audio_35642719" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35642719_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
