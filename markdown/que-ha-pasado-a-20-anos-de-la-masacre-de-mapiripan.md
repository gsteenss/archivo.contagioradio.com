Title: ¿Qué ha pasado a 20 años de la Masacre de Mapiripán?
Date: 2017-07-16 10:41
Category: DDHH, Otra Mirada
Tags: mapiripan, Masacre de Mapiripan
Slug: que-ha-pasado-a-20-anos-de-la-masacre-de-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/mapiripan-pueblo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Jul. 2017]

Entre el 15 de julio y el 20 de julio de 1997 un grupo de paramilitares de las Autodefensas Campesinas de Córdoba y Urabá (ACCU), en connivencia con altos oficiales del Ejército incursionaron en Mapiripán, departamento del Meta. **Sembraron el terror durante 5 días, torturaron y asesinaron a centenares de personas y desplazaron a muchas más**, quienes atemorizadas y con la idea de salvarse dejaron para siempre sus tierras con la esperanza de algún día regresar.

Luego de 20 años, las víctimas aseguran que **aún no han podido encontrar la verdad, la justicia, la reparación y las garantías de no repetición**, sin embargo, dicen que continuarán caminando para lograr regresar a sus territorios, que hoy, están plagados de palma aceitera de la multinacional italiana Poligrow y en los que hacen presencia los paramilitares, llamados de diversas formas.

“Ahora estamos peor de lo que estaba antes Mapiripán, antes éramos como si todos fuéramos una familia, se podía andar por donde fuera y ahora pues es difícil” recuerda Marina SanMiguel, víctima de esta masacre.

También insistirán en no olvidar, en trabajar la memoria, para exigir que lo que sucedió en Mapiripán no se repita, que se garantice la vida y que se responda a la pregunta ¿Por qué dejaron que sucediera esta masacre?

### **La masacre fue parte de una estrategia de militares con paramilitares** 

“Llegaron a Mapiripán saliendo de un aeropuerto militarizado y controlado por la Brigada XVII a otro aeropuerto en San José de Guaviare controlado por la Brigada VII, luego **los paramilitares fueron acompañados por la Brigada Móvil Nº 2 del Ejército** que dirigía el entonces Coronel Lino Sánchez que estaba en curso de ascenso a General” recuerda Luis Guillermo Pérez del Colectivo de Abogados José Alvear Restrepo quienes acompañan jurídicamente a las víctimas.

### **20 años de justicia agridulce** 

A pesar de las decisiones tomadas en materia jurídica por organizaciones internacionales como la Corte Interamericana de Derechos Humanos **-CorteIDH-, que condenó al Estado por violación a los derechos de los habitantes de Mapirirán** y decisiones como las de la Corte Suprema de Justicia en la que halló culpable el General Jaime Uscategui por el delito de comisión por omisión, secuestro simple y homicidio, entre otros el abogado de las víctimas manifiesta que sigue habiendo un sabor agridulce.

“Luego de 20 años es un sabor muy agridulce, tenemos una sentencia de la CorteIDH que ha sido muy importante, pero **hay una estrategia de manipulación, de querer hacerle daño a las víctimas**, a nuestra imagen de credibilidad como defensores de derechos humanos, que no va a parar, menos ahora con la JEP” asevera Pérez.

Otras sanciones penales son las del Coronel Hernán Orozco Castro quien era el comandante encargado del Batallón Joaquín París y quien fue el que denuncio al General Uscategui por haber impedido que protegieran a la población de Mapiripán.

Leer más sobre este tema: [20 años de justicia agridulce para las víctimas de la Masacre de Mapiripán](https://archivo.contagioradio.com/20-anos-de-justicia-agridulce-para-las-victimas-de-la-masacre-de-mapiripan/)

### **Víctimas no han podido regresar porque hay intereses en sus tierras** 

Según la Unidad Nacional de Restitución de Tierras, en el sur del Meta, donde se encuentra Mapiripán, hasta diciembre de 2016 habían sido radicadas 2.272 reclamaciones en la Unidad Nacional de Restitución de Tierras por parte de las víctimas de la masacre. Sin embargo, a la fecha solo se han realizado 23 microfocalizaciones de las cuales 12 cuentan con sentencias de restitución.

Así las cosas, las **víctimas manifiestan que la restitución es difícil porque las tierras reclamadas no han tenido un desarrollo** satisfactorio y en otros casos hay presencia de multinacionales que a través de la siembra de palma de aceite han asumido el control de algunos de los territorios pedidos por ellos y ellas. Esto además de la presencia paramilitar que impide, en algunos casos que las familias se sientan seguras para solicitar la restitución.

**María Cecilia Lozano integrante de la Asociación de Mujeres Desplazadas del Meta – ASOMUDEM-** asegura que hace 20 años estaban tranquilos en Mapiripán donde vivían y tenían sus propiedades y “disfrutábamos y ayudábamos a otras personas con trabajo porque generábamos empleo y vivíamos una vida digna, porque la mayoría éramos propietarios de tierras”.

Leer más sobre este tema: [Las tierras de Mapiripán, uno de los asuntos centrales para las víctimas](https://archivo.contagioradio.com/las-tierras-de-mapiripan-uno-de-los-asuntos-centrales-para-las-victimas/)

### **El impacto de la Masacre en las mujeres** 

Según cifras oficiales, fueron **cerca de 511 familias las que tuvieron que salir de Mapiripán para salvar su vida,** de las cuáles no se sabe a ciencia cierta cuántas fueron mujeres, niños o niñas, sin embargo, tal y como lo han denunciado diversas organizaciones de mujeres, el conflicto armado las ha afectado de múltiples formas.

Cuenta **Viviana Barrera, víctima de la Masacre** que seguir en el territorio fue muy difícil pues “muchas quedaron viudas, solas, sus vidas no han sido fáciles, quedaron con niños y se tuvieron que enfrentar a la vida que no estaban acostumbradas, porque el esposo que tenían les daba todo. En mi caso con mi padre nunca me faltaba nada”.

Leer más sobre este tema: [Mujeres de Mapiripán resisten al olvido tras 20 años de la masacre](https://archivo.contagioradio.com/masacre-de-mapiripan-obligo-a-las-mujeres-a-reconstruirse/)

### **Los hijos también tienen en su memoria la Masacre de Mapiripán** 

Asegura **Marina SanMiguel otra de las víctimas de la Masacre** que no solo los adultos son quienes llevan grabada en su memoria la Masacre, sino que los “hijos también viven con esa historia, porque recuerdan que salimos de Mapiripán, lo perdimos todo, dicen perdí a mi padre”.

Leer más sobre este tema: [Víctimas de Mapiripán aseguran que la memoria es un pilar fundamental para resistir](https://archivo.contagioradio.com/victimas-de-mapiripan-aseguran-que-la-memoria-es-un-pilar-fundamental-para-resistir/)

### **¿Qué proponen las víctimas para que Mapiripán sea un territorio de paz?** 

Que el Estado reconozca que la masacre sucedió en Mapiripán, que hay unas víctimas que están esperando una verdad y **el regreso a su territorio con garantías, con inclusión social,** inversión en proyectos para las comunidades, mejoras al sistema de salud y a las vías son algunas de las propuestas que las víctimas de esta masacre tienen para que ese municipio sea un territorio de paz.

“Deben ser propuestas con inclusión social, que sean proyectos de vida, no como los que están dando de cualquier cosa, sino que sea un proyecto muy grande con salud, carreteras, devolución de tierras, para que haya una paz de verdad en Mapiripán” añade SanMiguel.

### **Que el Estado cumpla, exigen las víctimas** 

Luego de 20 años de luchar por intentar recuperar sus predios, dice Lozano que **los planes de vida se rompieron y hoy en día siguen exigiendo que devuelvan sus tierras**, pero además con garantías de seguridad para poder regresar.

“Hay mucha gente que está aguantando hambre, hay personas de avanzada edad porque han pasado 20 años, nuestros hijos ya crecieron, pero no hay garantías del Estado para poder regresar por lo menos con el derecho a una verdadera reparación, porque ni siquiera la reparación colectiva se ha podido hacer” dijo Lozano.

[![masacre de mapiripan 20 años](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2011/09/masacre-de-mapiripan-20-años2.png){.alignnone .wp-image-43761 .size-full width="1600" height="300"}](http://bit.ly/2t3g4Ve)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
