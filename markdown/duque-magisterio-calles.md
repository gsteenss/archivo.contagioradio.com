Title: Política del gobierno Duque obligó al magisterio a salir de nuevo a las calles
Date: 2019-02-14 13:50
Author: AdminContagio
Category: Educación, Movilización
Tags: fecode, maestros, profesores
Slug: duque-magisterio-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-14-at-12.53.13-PM-e1550169994374-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Feb 2019] 

Este jueves 14 de febrero los profesores regresaron a las calles por tres razones: la grave situación en materia de servicios de salud para el magisterio, el Plan Nacional de Desarrollo que no aporta los recursos necesarios para cubrir este derecho básico y  presentar el pliego nacional de peticiones, en el que se piden mayores aportes de la nación al presupuesto de educación.

### **Es la vida de los profesores la que está en riesgo** 

Como explica Carlos Rivas, directivo de la Federación Colombiana de Trabajadores de la Educación (FECODE), la primera razón de la jornada de paro y protesta son los "pésimos servicios de salud". No es la primera vez que se hace presente este reclamo, pues desde 2017 los maestros han denunciado la deficiente prestación del servicio para el magisterio; situación que se suma al reclamo para que se proteja la vida de líderes sociales y profesores rurales.

En segundo lugar, los profesores se manifestarán contra el Plan Nacional de Desarrollo que resulta, según Rivas, en una inequidad para los ciudadanos y un beneficio para los grandes empresarios. En lugar del plan presentado, lo que debería hacer un plan para la paz es destinar recursos a los municipios para que ellos suplan las necesidades en materia de educación. (Le puede interesar:["Por deuda histórica profesores vuelven a la calle en febrero"](https://archivo.contagioradio.com/profesores-vuelven-calles/))

[En tercer lugar la presentación del pliego nacional de peticiones, que en sus puntos más sensibles tiene que ver con la financiación de la educación, fundamentalmente, con una modificación de carácter constitucional a los artículos 356 y 357, para la financiación de la salud, el agua potable, el saneamiento básico y la educación de los municipios; porque hoy se requiere que el Gobierno central aporte gradualmente más a los entes territoriales para recuperar el equilibrio en los recursos para la educación. ]

### **Más de 25.000 profesores se movilizaron en todas las capitales del país** 

Las marchas se realizaron en todas las capitales del país. La que se movilizó por las calles de Bogotá culminó en el Ministerio de Educación Nacional, donde se readicó el pliego de peticiones que deberá ser abordado por la mesa de concertación. Sin embargo Rivas resalta que aunque los canales de diálogo siguen abiertos no han dado resultados puesto que solamente se ha cumplido un 50% de las exigencias del Magisterio.

<iframe id="audio_32559589" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32559589_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
