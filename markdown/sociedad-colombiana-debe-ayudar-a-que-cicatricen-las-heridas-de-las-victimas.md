Title: "Sociedad colombiana debe ayudar a que cicatricen las heridas de las víctimas"
Date: 2015-09-24 11:40
Category: Nacional, Paz
Tags: Acuerdo de justicia farc y gobierno, acuerdo de justicia firmado en la habana, ASFADDES, Desaparición forzada Colombia, Diálogos de paz en la Habana, justicia transicional, Mecanismo de justicia transicional proceso de paz en Colombia, Paz con justicia social, Radio de derechos humanos, Víctimas de desaparición forzada
Slug: sociedad-colombiana-debe-ayudar-a-que-cicatricen-las-heridas-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Gloria-Gómez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:www.youtube.com ] 

<iframe src="http://www.ivoox.com/player_ek_8589665_2_1.html?data=mZqlm5uaeY6ZmKiakp2Jd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmtNTQy8rIpcWfxNTZ0dLGrcLiwpDRx8fJb8Lt1snO1JDFb9LpxpDQy8jFuNPdxMrbjdHFt4zcjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloria Gómez, ASFADDES] 

###### [24 Sept 2015] 

[**“Es un paso histórico y muy importante, es un peldaño más para lograr iniciar el proceso de consolidación de la paz"** afirma Gloria Gómez, fundadora de la Asociación de Familiares de Detenidos-Desaparecidos ASFADDES, tras el anuncio de la firma del acuerdo de justicia entre el Gobierno colombiano y las FARC.]

[La fundadora de ASFADDES, afirma que **el acuerdo en alguna medida sintetiza las propuestas que diversas organizaciones de víctimas enviaron a la mesa de diálogos en La Habana**.  Para ella, ahora lo importante es que la sociedad civil colombiana le apueste a la construcción de la paz y participe activamente para garantizar que la nueva jurisdicción sancione a los autores intelectuales y financiadores de las graves violaciones al DIH y a los DDHH para asegurar la no repetición de crímenes.]

[Uno de los **grandes aportes del acuerdo es la no amnistía para delitos de lesa humanidad como la desaparición forzada**, asegura Gómez, en esa medida las exigencias de la sociedad colombiana en la materia deben cooperar para que **las víctimas cicatricen sus heridas**, pues "Sin memoria desde las víctimas no se puede hablar de verdad".   ]

[“No puede haber paz con impunidad”, dice Gómez, añadiendo que la **justicia social debe ser un componente de esta nueva legislación para el posconflicto**. ]
