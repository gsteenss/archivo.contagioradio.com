Title: Comunidad del corregimiento de La Silla en Tibú en riesgo de desplazamiento por enfrentamientos armados
Date: 2020-12-28 18:19
Author: CtgAdm
Category: Actualidad, Comunidad, Nacional
Tags: Derechos Humanos, Desplazamiento forzado, Paramilitarismo, violencia
Slug: comunidad-del-corregimiento-de-la-silla-en-tibu-en-riesgo-de-desplazamiento-por-enfrentamientos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ascamcat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la mañana de este lunes, **la Asociación Campesina del Catatumbo** **(Ascamcat)** informó a través de un comunicado, que en la vereda La Silla, municipio de Tibú, Norte de Santander se presentó, en las últimas horas, una incursión paramilitar que ha desatado **fuertes enfrentamientos entre las Autodefensas Gaitanistas de Colombia (AGC) y la guerrilla Ejército de Liberación Nacional (ELN).** Esta preocupante situación, pone en riesgo inminente de desplazamiento forzado a por lo menos 80 familias de la comunidad de la silla en Tibú.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/AscamcatOficia/status/1343601777762037769","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1343601777762037769

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:heading -->

Amenaza latente para la comunidad de Tibú
-----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según los habitantes de esta región, el grupo de las AGC desean recuperar su autoridad en el territorio. Ascamcat, además, recalca que el grupo criminal conocido como **Los Rastrojos también ha hecho presencia con intereses de permanecer en la región**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La disputa entre varios actores armados ilegales data de décadas, ya que este ha sido un corredor esencial para el narcotráfico, puesto que es zona fronteriza con Venezuela.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta nueva incursión paramilitar, da paso a una revictimización de los habitantes de La Silla en Tibú, que vuelven a sentir la cruda violencia cerca a sus hogares, recordando los [destrozos paramilitares en la región en los años 1999 y 2000.](https://www.justiciaypazcolombia.com/la-incursion-de-las-autodefensas-unidas-de-colombia-auc-en-el-municipio-de-tibu-en-el-ano-1999-la-consolidacion-del-paramilitarismo-en-el-catatumbo/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Exigencias al Estado
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dentro de las exigencias al estado colombiano están la garantía del derecho a la vida, libertad e integridad a las comunidades campesinas. y recuerdan que:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"la Defensoría del Pueblo había emitido la alerta temprana Nº 050, el 26 de noviembre, advirtió sobre los diferentes hechos que afectan a los derechos humanos por el actuar de los diferentes actores armados."***
>
> <cite>Asociación campesina del Catatumbo (Ascamcat)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Uno de los hechos denunciados por la asociación fue la masacre ocurrida el 10 de enero en la vereda La Silla, donde fueron asesinadas 4 personas, incluido el líder campesino Tulio César Sandoval Chía. A través de las alertas Nº 037, Nº 011 y Nº 035, la asociación y la comunidad, buscan exigirle al Estado garantías de protección para los pobladores y el desmonte del paramilitarismo. Adicionalmente, hacen un llamado urgente a las autoridades competentes para que se garantice los derechos humanos de la comunidad de la Silla en Tibú.     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: [&lt;&lt;En riesgo Zona de Reserva Campesina Perla Amazónica, en Putumayo&gt;&gt;](https://archivo.contagioradio.com/en-riesgo-zona-de-reserva-campesina-perla-amazonica-en-putumayo/))

<!-- /wp:paragraph -->
