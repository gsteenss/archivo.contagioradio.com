Title: Conmuévete y Muévete, una campaña contra los feminicidios y asesinatos de líderes
Date: 2017-07-26 17:02
Category: Mujer, Nacional
Tags: asesinatos de lideres, batucada, feminicidio, feminismo, Feministas
Slug: la-campana-que-movilizara-a-la-ciudadania-contra-los-feminicidios-y-asesinatos-de-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/revoltosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Facebook La Trremenda Revoltosa] 

###### [26 Jul. 2017] 

Se realizó en Bogotá el lanzamiento de **la campaña Conmuévete y Muévete organizada por la Batucada Feminista La Tremenda Revoltosa** que pretende sensibilizar a la población colombiana acerca de dos problemáticas que vive Colombia, los feminicidios y el asesinato de líderes y lideresas sociales.

El evento, que contó con la presencia de más de 100 personas y de los transeúntes que pasaban por la Torre Colpatria de Bogotá, buscaba a través de otros lenguajes llegar a muchas personas, conmoverlas y convocarlas a la movilización.

Según **Andrea Oramas, integrante de la Tremenda Revoltosa** previo a la realización de este evento, se hicieron varias reuniones en las que se discutió ¿cómo impactar a la ciudadanía en relación con las problemáticas del feminicidio y los asesinatos de líderes y lideresas que vive Colombia? “Y decidimos unirnos entre todos y todas para realizar acciones, llegar a más personas y mostrar que no estamos solas”.

Para el lanzamiento, el arte a través de diversas expresiones como la música, logró convocar y sensibilizar a muchas personas, algunas a través de las redes sociales y otras que se hicieron presentes en el lugar.

“Desde la Batucada **consideramos que a través de otros lenguajes como la música, los tambores y el arte se puede convocar mucho a la gente** y le tocan digamos como muchas de sus fibras muy profundas. Por eso creemos y la evaluación que hacemos es que tuvo una muy buena acogida”.

### **¿Qué viene luego del lanzamiento de la campaña?** 

Las actividades proyectadas dentro de la campaña, seguirán realizándose todos los días 25 de cada mes e irán hasta el 25 de noviembre, sin embargo, independiente de las actividades que han llamado centrales, se continuarán realizando piezas comunicativas, encuentros y acciones para lograr mantener a las personas conectadas con la iniciativa.

-   **25 de agosto:** Se realizará un encuentro con movimientos sociales para trabajar el debate y saber cuál es la posición de las organizaciones y los movimientos sociales de izquierda frente al tema de los feminicidios y articularse para repudiar los asesinatos.
-   **25 de septiembre:** Se llevará a cabo una actividad de tipo local en la que se dialogará a nivel local el tema de feminicidios y asesinatos a líderes sociales, en donde estarán presentes organizaciones, movimientos sociales y culturales.
-   **25 de octubre:** Se hará un encuentro llamado “El sentido político de los tambores” en el que se pretende convocar batucadas y colectivos que a través de los tambores ponen en el país su propuesta política. Esta será una actividad de tipo itinerante que durará varios días.
-   **25 de noviembre:** Movimientos y organizaciones sociales llevarán a cabo una gran movilización a propósito del día de la No Violencia contra la Mujer.

### **¿Dónde encontrar toda la información de las actividades de la campaña?** 

La Tremenda Revoltosa invita a todas las personas que estén interesadas en estas actividades a seguirlas a través de Facebook para poder estar al tanto.

“Ahí estamos informando y publicando toda la información acerca de la campaña, **aparecemos como La Tremenda Revoltosa Batucada Feminista** y ahí es donde también estarán todas las pieazas comunicativas”.

<iframe id="audio_20029864" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20029864_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
