Title: Elecciones presidenciales a segunda vuelta en Ecuador
Date: 2017-02-22 17:43
Category: El mundo, Movilización
Tags: Elecciones Ecuador, Lennin Moreno
Slug: elecciones-presidenciales-a-segunda-vuelta-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/elecciones-en-ecuador-1280x640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Los Alcarrizos News] 

###### [22 Feb 2017] 

Ecuador se prepara para una segunda vuelta electoral que se desarrollará el próximo 2 de abril, los votantes tendrán que elegir entre Lennin Moreno, **quien obtuvo un 40% del escrutinio en la primera vuelta, contra Guillermo Lasso, candidato al que la oposición ha expresado su apoyo.**

Este panorama desfavorece al candidato de Alianza País, debido a que el resto de partidos establecerían alianzas con la campaña de Lasso, bajo una misma consigna antigobierno. Para Federico Larssen, analista político, **esta alianza no se realizaría sobre asuntos programáticos sino sobre la idea de derrotar**, electoralmente a la coalición progresista de Alianza País, que ha mantenido el control durante 10 años. Le puede interesar:["Alianza País encabeza las elecciones en Ecuador"](https://archivo.contagioradio.com/alianza-pais-encabeza-las-elecciones-en-ecuador/)

No obstante, el gobierno de Correa y su partido **ha demostrado tener una fuerza representativa en escenarios locales que podrían jugar un papel importante** durante la segunda vuelta, sumado a la intención de reconquistar votos de sectores del movimiento social como las mujeres, los discapacitados y los indígenas.

En las diferentes ciudades de este país se ha registrado una calma tensa que responde a las campañas electorales que se avecinan y a los movimientos, que de acuerdo Larssen, se podrían dar en las calles suscitados por **alcaldes como el de Guayaquil, quién hizo un llamado a la protesta en defensa de los votos de la oposición. **Le puede interesar: ["Crónicas de las calles del Ecuador en campaña"](https://archivo.contagioradio.com/cronica-desde-las-calles-del-ecuador-en-campana/)

Para Larssen **“la oposición ha puesto contra las cuerdas al oficialismo”** y ha ejercido presión sobre el Consejo Nacional Electoral, ya que si en primera vuelta ganaba Moreno, la fuerza de calle que tiene la oposición iba a intentar desestabilizar al país.

De acuerdo con Larssen en un contexto en donde gane la oposición bajo el mandado de Guillermo Lasso, **Ecuador perdería la capacidad de tener a futuro un sistema financiero alternativo internacional y un rol de liderazgo en Latinoamérica.**

<iframe id="audio_17173000" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17173000_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
