Title: Caravana denuncia nueva crisis humanitaria en el sur de Bolívar
Date: 2020-07-27 22:18
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Bolívar, Caravana Humanitaria, Minería ilegal, Paramilitarismo
Slug: caravana-denuncia-nueva-crisis-humanitaria-en-el-sur-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Edc61FOXoAMkbtG.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Edc61FOXoAMkbtG-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Caravana Humanitaria al Sur de Bolívar /[CNA Colombia](http://@CNA_Colombia)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales del Sur de Bolívar decidieron emprender una caravana humanitaria creada para verificar la situación de DD.HH en los poblaciones de Norosí y Tiquisio donde las acciones violentas han ocasionado la zozobra y desplazamiento forzado de las comunidades. Dicha caravana busca denunciar la realidad de DDHH en la región, y que el Estado haga presencia integral ya que únicamente hace presencia militar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Teófilo Acuña, vocero de la Comisión de Interlocución del sur de Bolívar**, en este momento la región está en riesgo inminente y ante la ausencia de diálogo y escucha por parte del Gobierno, las organizaciones decidieron del 21 al 24 de julio, realizar una convocatoria para visibilizar las denuncias hechas en los territorios y evaluar además la situación de los sectores agro mineros, amenazados por la minería ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La caravana es impulsada por la Comisión de Interlocución del sur de Bolívar, centro y sur del Cesar, (CSBCSC) y fue nombrada en honor al líder agro-minero **Edwin Emiro Acosta Ochoa, asesinado el 26 de mayo 2020 por grupos paramilitares** y del que aún no existe avances en su investigación por parte de las autoridades. A su asesinato se suma el de Alberto Fernández de 22 años en la zona Alta de Norosí y que ha alertado a la población con relacion a los asesinatos selectivos que se están presentando. [(Le recomendamos leer: Asesinan a Edwin Acosta, líder social y minero de Tiquisio, Bolívar)](https://archivo.contagioradio.com/asesinan-a-edwin-acosta-lider-social-y-minero-de-tiquisio-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Mineria ilegal y paramilitarismo en Bolívar

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El vocero alerta sobre el incremento del accionar paramilitar, para finales del 2019, cerca de 40 paramilitares en Mina Seca, corregimiento de Tiquisio, obligaron a los habitantes a reunirse con ellos para acordar vacunas de la extracción del oro, en febrero - relata - ya no eran 40 sino 60 paramilitares quienes ordenaban las reuniones, hechos que señala, se dan a media hora de la presencia del Ejército, cuya presencia en Bolívar incluye en particular la Segunda División del Ejército Nacional, la Quinta Brigada, la Fuerza de Tarea Conjunta Marte y el Batallón de Selva 48.

<!-- /wp:paragraph -->

<!-- wp:image {"id":87262,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Edc61FOXoAMkbtG-1-1024x636.jpg){.wp-image-87262}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

"Uno se hace la pregunta, ¿una región tan militarizada y que estos grupos armados estén actuando ahí?" se cuestiona el vocero con relación al incremento de grupos paramilitares, ligado directamente al aumento de cultivos de uso ilícito en la región, donde según la Oficina de las Naciones Unidas contra la Droga y el Delito (UNODC), se pasó de 6.179 hectáreas de coca en diciembre de 2017, a 8.614 ha en diciembre de 2018, **con un crecimiento del 39%.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El vocero alerta además sobre el despojo de la tierra, la persecución que existe contra los líderes mineros como Edwin Acosta y la existencia de una "mafia minera" que es atribuida a grandes empresarios y que involucra a operadores que no trabajan las minas pero obtienen las regalías y licencias ambientales, Teofilo incluso advierte sobre la presencia de personas natales de la China que habrían llegado a explotar el territorio de forma ilegal

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Siempre hemos creido que grandes empresarios están detrás de la mafia del oro"
>
> <cite>Teofilo Acuña</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la caravana recorrió únicamente Norosí y Tiquisio, el vocero denuncia que esta situación no solo se presenta aquí sino en otros regiones como Arenal, Morales, Santa Rosa, Cantagallo Gamarra y Aguachica, por lo que el informe recopilará toda la información relacionada a la región del Bolívar. [(Le puede interesar: Fue asesinado líder social Jorge Ortiz quien denunciaba corrupción y violencia en Bolívar)](https://archivo.contagioradio.com/fue-asesinado-lider-social-jorge-ortiz-quien-denunciaba-corrupcion-y-violencia-en-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
