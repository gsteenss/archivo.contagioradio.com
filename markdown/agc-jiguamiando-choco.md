Title: AGC incursionan en territorio colectivo de Jiguamiandó, Chocó
Date: 2017-04-16 11:59
Category: Nacional
Tags: AGC, amenazas paramilitares, Chocó, Jiguamiandó, Puerto Lleras
Slug: agc-jiguamiando-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Autodefensas-Gaitanistas-e1459446173432.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Archivo 

##### 16 Abr 2017 

Según denuncia publicada por la Comisión Intereclesial de Justicia y Paz, durante los últimos días se ha registrado un nuevo **ingreso de paramilitares de las autodenominadas "Autodefensas  Gaitanistas de Colombia AGC" , al caserío de Puerto Lleras**, ubicado en territorio Colectivo de Jiguamiandó en el Bajo Atrato chocoano.

De acuerdo con lo publicado el sábado 15 de abril por la organización de Derechos humanos, **desde las 7:00 de la noche un número indeterminado de hombres portando armas largas y vistiendo prendas camufladas con distintivos de las AGC, ingresaron a terrenos de las comunidades**.

En su incursión, **los uniformados comunicaron a los pobladores su intención de dirigirse hacia el caserío Pueblo Nuevo**, declarado como Zona Humanitaria, situación que según el informe, alerto a varias familias y a las y los líderes de la comunidad, quienes **salieron desplazados de su territorio hacia otras comunidades y otros a zonas selváticas**.

La denuncia se suma a las múltiples amenazas que se ciernen sobre varios lideres del territorio colectivo de Jiguamiandó, entre los que se encuentran **Manuel Denis Blandón, Melkin Romaña, Romualdo Salcedo, Félix Álvarado, Erasmo y Benjamín Sierra** y el líder indígena **Argemiro Bailarín**. Le puede interesar: [Denuncian ingreso de 250 hombres de las AGC a Jigumiandó](https://archivo.contagioradio.com/jiguamiando-choco-agc/)

Hace menos de un mes, la misma Organización había denunciado la persistencia de este tipo de ingresos a las comunidades del Chocó, en zonas no tan alejadas a los lugares donde hacen presencia las Fuerzas Armadas, **sin que desde el Estado se emprendan acciones eficaces que garanticen la protección de la vida e integridad de la población civil**.
