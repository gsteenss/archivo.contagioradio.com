Title: Estamos 'ad portas' de un gran paro estudiantil en Colombia
Date: 2018-10-03 19:37
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, ENEES, estudiantes, paro, Universidades
Slug: gran-paro-estudiantil
Status: published

###### [Foto: Contagio Radio] 

###### [3 Oct 2018] 

A la fecha, **9 Universidades Públicas del país están en asambleas permanentes**, y otras, como la Universidad Francisco de Paula Santander (UFPS) de Cúcuta están definiendo su posición sobre el cese de actividades académicas, para acompañar **la marcha del próximo 10 de octubre,** en la que pedirán al presidente Duque mayor presupuesto para la educación. (Le puede interesar: ["Las 10 exigencias del movimiento de estudiantes al Gobierno Nacional"](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

Según** César Arias, estudiante de la UFPS,** los miembros de la comunidad académica se reunirán este viernes, 05 de octubre, para evaluar los términos para una asamblea permanente. En el caso de esa institución, los estudiantes pedirán que **se aumente la planta docente,** porque sólo hay 182 docentes contratados de forma permanente para los más de 20 mil alumnos; **se regule el costo de la matricula; y se mejore la infraestructura para investigación (laboratorios) y el bienestar estudiantil (comedores)**.

Arias sostuvo que aunque la cobertura de la Universidad está aumentando, no se incrementa la financiación para sostener económicamente la institución; sin embargo, no es un hecho que solo está ocurriendo en Cúcuta, sino que es un síntoma generalizado de la educación superior, razón que llevó a que los estudiantes radicarán un [pliego de peticiones](https://es.scribd.com/document/389624106/PLIEGO-NACIONAL-DE-EXIGENCIAS-UNEES#fullscreen&from_embed)exigiendo **que se salde la deuda con las Universidades Públicas, calculada en 16 billones de pesos**.

Por esta razón, el estudiante sostuvo que las asambleas y reuniones que se están viendo apuntan a reforzar la movilización nacional que se realizará el próximo 10 de octubre, que **será el primer paso para exigir mejores condiciones para acceder al sistema de educación superior** porque lo que se ha visto hasta ahora del actual gobierno es que "volvemos al país guerrerista y dejamos de lado el país educado que queremos". (Le puede interesar: ["Presupuesto de Colombia para 2019 es el de un país en guerra"](https://archivo.contagioradio.com/presupuesto-pais-guerra/))

### **Las Universidades Públicas que están en asamblea permanente: ** 

Universidad Nacional de Colombia (sedes Bogotá, Medellín y Manizales)

Universidad de Antioquia

Universidad de Córdoba

Universidad del Quindío (sede Armenia)

Universidad Pedagógica Nacional

Universidad Industrial de Santander

Universidad Pedagógica y Tecnológica de Colombia

Universidad del Valle

Universidad Distrital Francisco José de Caldas (sedes La Macarena y Tecnológica)

<iframe id="audio_29076329" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29076329_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
