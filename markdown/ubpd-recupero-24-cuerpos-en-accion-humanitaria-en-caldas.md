Title: UBPD recuperó 24 cuerpos en acción humanitaria en Caldas
Date: 2020-11-11 21:06
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Caldas, Samaná, UBPD, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: ubpd-recupero-24-cuerpos-en-accion-humanitaria-en-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ubpd-en-Samana-Caldas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Unidad de Búsqueda de Personas dadas por Desaparecidas -UBPD

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Unidad de Búsqueda de Personas dadas por Desaparecidas -UBPD- anunció que **se recuperaron 24 cuerpos que podrían corresponder a personas desaparecidas durante el conflicto armado en el municipio de Samaná, Caldas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La directora de la UBPD, Luz Marina Monzón, indicó que estas acciones humanitarias se realizaron entre el 26 de octubre y el 8 de noviembre [en el marco del Plan Regional de Búsqueda del Magdalena Medio caldense](https://twitter.com/UBPDcolombia/status/1321608849875443712?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1321608849875443712%7Ctwgr%5Eshare_3&ref_url=https%3A%2F%2Fwww.ubpdbusquedadesaparecidos.co%2Factualidad%2Fresultados-de-la-primera-accion-humanitaria-de-la-ubpd-en-el-magdalena-medio-caldense%2F).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la directora expresó que «ha sido una acción humanitaria exitosa en el sentido en que hemos podido recuperar del cementerio San Agustín de este municipio 24 cuerpos que pueden corresponder a personas desaparecidas y cuyas identidades debemos verificar y establecer las circunstancias en las que perdieron la vida». (Le puede interesar: [UBPD interviene cementerio en Samaná y otros 3 municipios de Caldas](https://archivo.contagioradio.com/ubpd-interviene-cementerio-en-samana-y-otros-3-municipios-de-caldas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Unidad pudo establecer que **el 90 % de las víctimas son hombres y sus edades oscilan entre los 17 y los 45 años. La mayoría ingresaron al cementerio de San Agustín entre los años 2002 y 2007**. Misma época en que hubo un recrudecimiento del conflicto armado en la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, los cuerpos, que en la época ingresaron al Instituto Nacional de Medicina Legal, **tenían impactos de armas de fuego, señales de aparente tortura y desmembramiento, entre otros signos de violencia.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

UBPD también realiza toma de muestras a familiares
--------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dentro de esta acción humanitaria también se realizó la toma de muestras a 180 familiares de personas desaparecidas tanto en Samaná como en los corregimientos de San Diego y Florencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La UBPD también puntualizó que además de Samaná, se espera intervenir en los municipios de La Dorada, Victoria y Norcasia, los cuales están referidos en esta estrategia de búsqueda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**«La gran esperanza es que a este Plan Regional se vincule la mayor cantidad de personas con la información que nos permita la identificación y no solo la recuperación de los cuerpos**», añadió Monzón.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La Unidad garantiza participación de los familiares
---------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el marco de su labor, la UBPD ratificó su compromiso con garantizar la participación tanto de los familiares que buscan a sus seres queridos, como de las organizaciones de DD.HH. que los acompañan. (Le puede interesar: [JEP hace entrega de restos de personas desaparecidas en Dabeiba](https://archivo.contagioradio.com/jep-hace-entrega-restos-de-personas-desaparecidas-en-dabeiba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«Esto nos permite empezar una metodología donde las víctimas puedan estar cerca de lo que ocurre en la búsqueda de sus seres queridos y tener acceso a explicaciones que les hagan claro los pasos a seguir después de esta acción humanitaria» agregó Luz Marina.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
