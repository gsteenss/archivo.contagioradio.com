Title: Crecen las amenazas contra integrantes del MOVICE
Date: 2020-11-10 15:21
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Amenazas contra defensores de DD.HH, Bogotá, MOVICE, soacha
Slug: crecen-las-amenazas-contra-integrantes-del-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/MOVICE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: MOVICE

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se han conocido nuevas amenazas contra integrantes del **Movimiento Nacional de Víctimas de Crímenes de Estado (Movice)**, esta vez con la aparición de panfletos firmados por el grupo paramilitar Águilas Negras Bloque Capital en contra de tres líderes y voceros de la organización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las amenazas, que esta vez se conocen contra Luz Marina Hache, Luis Alfonso Castillo y la lideresa del Movice Capítulo Bogotá, Carmen Mayusa hacen parte de una serie de intimidaciones que ha realizado el grupo paramilitar desde 2018 y que se han hecho más frecuentes desde el Paro Nacional que tuvo lugar desde el 21 de noviembre de 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el [MOVICE](https://movimientodevictimas.org/), estas amenazas, se dan como producto de los señalamientos del Gobierno y declaraciones que llegan desde el Ministerio de Defensa en contra de la protesta social, estigmatizándola, tal como se vio en escenarios recientes como los panfletos en contra de liderazgos y organizaciones de las localidades de San Cristóbal, Bosa, Ciudad Bolívar y el Municipio de Suacha. [(Le recomendamos leer: Panfleto de Águilas Negras amenaza a integrantes de la Colombia Humana en Suacha)](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-a-integrantes-de-la-colombia-humana-en-suacha/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A estos sucesos se suman una serie de amenazas, atentados y desplazamientos forzados a los que se han enfrentado diversos líderes y lideresas del MOVICE en territorios como Cauca, Tolima, Sucre y Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente de estos hechos, ocurrió el pasado 19 de octubre en contra de **Hernando Benítez, líder campesino de la comunidad de San Benito Abad, en Montes de María** y quien fue víctima de un atentado del cual salió herido. Benítez, de 72 años, lleva 30 años liderando la lucha por la tierra a favor de los y las campesinas. [(Lea también: En riesgo vida de Hernando Benítez líder campesino de Sucre)](https://archivo.contagioradio.com/en-riesgo-vida-de-hernando-benitez-lider-campesino-del-movice/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la organización denuncian que pese a lo reiterativo de las amenazas, desde la Fiscalía y diferentes entes de investigación no se han producido resultados o avances que permitan conocer quiénes están detrás de dichas amenazas. Agregan que la Fiscalía desde 2016 en materia de homicidios únicamente ha resuelto el 16% de sus investigaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Organización Somos Defensores, en la actualidad existe un aumento en los asesinatos a liderazgos sociales que corresponde a 95 personas durante el primer semestre del año 2020, lo que corresponde al 61% en comparación con el mismo periodo del año 2019. De igual forma, **según el informe trimestral de la plataforma presentado en junio, durante 2020 se han registrado 115 amenazas** contra defensores de DD.HH.  [(Lea también: 2019 fue el año más agresivo contra defensores de DDHH en toda la década: Somos Defensores)](https://archivo.contagioradio.com/el-ano-pasado-fue-el-mas-agresivo-para-defensores-de-dd-hh-de-la-decada-somos-defensores/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
