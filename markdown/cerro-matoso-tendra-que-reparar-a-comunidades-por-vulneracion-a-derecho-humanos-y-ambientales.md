Title: Cerro Matoso tendrá que reparar a comunidades por vulneración a derecho humanos y ambientales
Date: 2018-03-22 17:36
Category: Ambiente, DDHH
Tags: Cerro Matoso, comunidades, cordoba
Slug: cerro-matoso-tendra-que-reparar-a-comunidades-por-vulneracion-a-derecho-humanos-y-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/la-razón.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [22 Mar 2018] 

La Corte Constitucional de Colombia, a través de un falló, ordenó a la Mina de Cerro Matoso reparar a las **comunidades que han sido afectadas por más de 30 años en las actividades de exploración y explotación** minera, provocando afectaciones “irremediables a la salud y al medio ambiente”.

Las comunidades que han sido afectadas son Torno Rojo, Bocas de Uré, Puerto Colombia, Unión Matoso en Puerto Flecha, Guacarí en la Odisea, Centro América y Puente Uré y el Consejo Comunitario de las Comunidades Negras de San José de Uré y se ha calculado que podrían haber más de **3.000 personas afectadas**, en el departamento de Córdoba.

De acuerdo con Luis Álvaro Pardo, abogado especialista en derecho minero, las violaciones a derechos humanos y ambientales provocadas por la minera Cerro Matoso, ocurrieron por la falta de legislación y de seguimiento de las autoridades correspondientes, debido a que en 1.981 cuando llega al territorio solo adquiere permisos y licencias de la Autoridad regional y no realiza la consecuente solicitud para adquirir la licencia ambiental.

Posteriormente, con la Ley 99 de 1.993 que reglamenta las licencias ambientales, se ordena que las empresas que no las hayan adquirido lo hagan, Cerro Matoso no hizo este proceso **“el permiso de licencia que tiene Cerro Matoso vulnera y desconoce normas constitucionales y legales**, sin embargo, ellos insisten en tener la licencia” afirmó Pardo. (Le puede interesar:["14 empresas mineras le deben al Estado \$13.608 millones"](https://archivo.contagioradio.com/38858/))

### **La decisión de la Corte** 

Para el abogado, el fallo de la Corte Constitucional es el cumplimiento a las exigencias que tienen las multinacionales cuando realizan explotaciones en Colombia, de minerales e Hidrocarburos. El siguiente paso es que la Minera **Cerro Matoso consulte a las comunidades si aprueban o no que se realice** actividad minera en su territorio para poder adquirir la licencia ambiental.

Frente a la indemnización que debe pagar la Minera, la Corte manifiesta que es en abstracto debido a que la vulneración de derechos, tanto humanos como ambientales, s**e ha hecho por un periodo de 30 años y no puede ser aún calculado en su totalidad.**

Así mismo, para garantizar el cumplimiento del fallo, la Corte Constitucional nombró como veedores al Tribunal Administrativo de Cundinamarca, a la Contraloría y a la Procuraduría que deberán vigilar de cerca lo que pase con este proceso.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
