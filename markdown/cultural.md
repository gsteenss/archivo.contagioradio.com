Title: Cultural
Date: 2014-11-21 15:04
Author: AdminContagio
Slug: cultural
Status: published

### VIAJE LITERARIO EVENTO

[![10 poemas de Martí convertidos en canción](https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400.jpg "10 poemas de Martí convertidos en canción"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)  

###### [10 poemas de Martí convertidos en canción](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)

[<time datetime="2019-01-28T10:20:53+00:00" title="2019-01-28T10:20:53+00:00">enero 28, 2019</time>](https://archivo.contagioradio.com/2019/01/28/)Homenaje a su obra con una selección de 10 temas musicales inspirados en 1o escritos de José Martí[LEER MÁS](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)
