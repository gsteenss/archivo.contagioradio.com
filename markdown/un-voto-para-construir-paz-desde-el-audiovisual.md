Title: Un voto para construir paz desde el audiovisual
Date: 2016-03-30 20:14
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: Festival cine ddhh, Iniciativas de paz, RECON
Slug: un-voto-para-construir-paz-desde-el-audiovisual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Festicine-ddhh.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 30 Mar 2016 

A pocos meses de iniciar la tercera edición del Festival Internacional de Cine por los Derechos Humanos de Bogotá, iniciativa que apuesta por construir paz, reconciliación y memoria desde el audiovisual, ha sido preseleccionada como una de las propuestas finalistas del concurso RECON Paz en Movimiento.

Organizado por la Federación de Personeros de Colombia (Fenalper) y las Personerías Municipales de Colombia, RECON consiguió la participación de más de 400 iniciativas de diferentes regiones del territorio nacional que muestran sus ideas para la construcción de paz en el país.

Son cinco las categorías en las que se encuentran agrupadas estas propuestas colombianas: prácticas culturales, artísticas y deportivas; medio ambiente; tecnología y comunicación; y emprendimiento y generación de empleo.

La mecánica del concurso tiene en cuenta la participación de la comunidad escogiendo el ganador de cada categoría por la cantidad de votos.

Los organizadores del Festival, invitan a todas las personas que se identifiquen con la labor de la promoción de la paz y los derechos humanos a través del cine, en este caso desde su convocatoria 'Nuevos Realizadores', a votar en el enlace: <http://www.reconcolombia.com/iniciativas2016/festival-internacional-de-cine-por-los-derechos-humanos>

<iframe src="https://www.youtube.com/embed/su3qGqH_3z8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Se puede votar una vez al día con la cuenta de Facebook o inscribiendo sus cuentas de correo electrónico. Las votaciones cierran el 1 de abril.
