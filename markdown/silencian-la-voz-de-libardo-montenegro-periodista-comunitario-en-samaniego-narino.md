Title: Silencian la voz de Libardo Montenegro, periodista comunitario en Samaniego, Nariño
Date: 2019-06-12 11:40
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinatos de Periodistas, nariño, Samaniego
Slug: silencian-la-voz-de-libardo-montenegro-periodista-comunitario-en-samaniego-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Libardo-Montenegro.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

En horas de la noche del pasado 11 de junio, fue asesinado con arma de fuego en Samaniego, Nariño, el periodista comunitario de 40 años **Libardo Montenegro quien trabajaba para la emisora Samaniego Estéreo.** Según Fundepaz, con el comunicador ya son 15 los defensores de DD.HH. asesinados en este departamento a lo largo del 2019.

Libardo quien trabajaba como locutor desde hace 20 años, producía el programa El Despertar de Samaniego Estéreo, una de las emisoras que hacen parte de la iniciativa Radios Para la Paz y que busca fortalecer la radio comunitaria en los territorios a través de propuestas vinculadas a la paz y la cultura.

**Rosa Guevara, gerente de la estación radial** afirma que Samaniego es un municipio donde resulta difícil hacer periodismo, "nosotros nos cuidamos mucho, cualquier mensaje u opinión que se dé, somos muy cautelosos". Agregó que no existían razones por las que Libardo Montenegro fuera asesinado, pues incluso en épocas en que los diferentes grupos armados que operan en la región hicieron más presión, "la emisora nunca sufrió amenazas".

El mismo día también se conoció del homicidio de un joven en un taller de motocicletas y el pasado 20 de mayo, fue asesinada **Paula Andrea Rosero, abogada y personera municipal,** lo que refleja un incremento de la violencia en este territorio, donde operan el Ejército de Liberación Nacional, (ELN) y el Clan del Golfo.

Rosa explica que la familia de Libardo, propietaria de la emisora siempre se ha dedicado a la radio, **"ellos siempre han llevado en las venas la locución, por eso la sentía como propia, como persona era excelente, jocoso, se entregaba con todo y era muy servicial"**, recuerda. [(Le puede interesar: Asesinan a Paula Rocero, Personera y defensora de DD.HH. en Samaniego, Nariño)](https://archivo.contagioradio.com/asesinan-a-paula-rocero-personera-y-defensora-de-dd-hh-en-samaniego-narino/)

La ONU, la Unión Europea y organizaciones como la Fundación para la Libertad de Prensa (FLIP), rechazaron el crimen y pidieron celeridad a la Fiscalía para que esclarezca los hechos que acabaron con  la vida de Libardo, sin embargo como declaró la Gerente de la emisora, aún no hay un pronunciamiento oficial por parte de las autoridades.

> [\#ALERTA](https://twitter.com/hashtag/ALERTA?src=hash&ref_src=twsrc%5Etfw) | En la noche del martes 11 de junio fue asesinado Libardo Montenegro, periodista de Samaniego Estéreo, en el departamento de Nariño. La FLIP rechaza este crimen y exige a la [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) que investigue los hechos. [pic.twitter.com/XzkkPAceCO](https://t.co/XzkkPAceCO)
>
> — FLIP (@FLIP\_org) [12 de junio de 2019](https://twitter.com/FLIP_org/status/1138655091592970240?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<iframe id="audio_37025193" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37025193_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
