Title: Néstor Humberto Matínez nuevo Fiscal General ¿para la paz?
Date: 2016-07-11 16:37
Category: Uncategorized
Tags: Fiscalía, Nestor Humberto Martínez, proceso de paz
Slug: nestor-humberto-matinez-nuevo-fiscal-general-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [11 Jul 2016] 

[Con 17 votos a favor la sala plena de la Corte Suprema de Justicia eligió como nuevo Fiscal General de la Nación a Néstor Humberto Martínez, lo que significa que el ente acusador, queda en manos de** una persona que representa el gran poder económico nacional y transnacional**, como lo aseguran el defensor de derechos humanos Alberto Yepes y el abogado Gustavo Gallón.]

[Esa situación, de acuerdo a Yepes y Gallón, se debe a los estrechos vínculos de Martínez con Germán Vargas Lleras y empresarios como Luis Carlos Sarmiento Ángulo. "El poder del dinero termina sometiendo a los ciudadanos y a la democracia" afirma Alberto Yepes quien además expresa que esa decisión preocupa de cara al posconflicto y teniendo en cuenta que **"el poder corruptor del dinero ha permeado todos los poderes del Estado"** y que "es muy difícil confiar en la Fiscalía cuando su encargado es el defensor de los poderosos".]

Para los defensores de DDHH, el recién nombrado Fiscal General, debió haberse declarado impedido ética y políticamente para ejercer el cargo, pues ser el "abogado de los grandes capitales nacionales e internacionales" no le da la imparcialidad necesaria para las investigaciones relacionadas con las graves violaciones a los derechos humanos, cometidas por empresas colombianas y extranjeras.

Por su parte, Claudia Erazo, abogada de la Corporación Jurídica Yira Castro, señala que el **compromiso con la paz era uno de los requisitos más importantes para tener en cuenta a la hora de elegir el Fiscal **teniendo en cuenta que será quién implemente la jurisdicción para la paz.

**Tras esa situación podría quedar en el limbo la responsabilidad de la Fiscalía en el desmonte de las estructuras criminales**. A su vez, Erazo considera que el proceso de elección para la terna se realizó más con **fines políticos** y no pensado en los retos que se vienen en materia de justicia y protección de derechos humanos con la posible firma del proceso de paz que se adelanta en la Habana.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
