Title: Eco Oro renuncia a proyecto minero en Páramo de Santurbán
Date: 2019-06-27 16:22
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Agencia Nacional de Minería, Páramo de Santurbán
Slug: eco-oro-renuncia-a-proyecto-minero-en-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Páramo1-e1456273290780.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Agencia Noticias Unal] 

La empresa canadiense Eco Oro dio a conocer a través de una carta dirigida al Gobierno con fecha del pasado 29 de marzo que renunció a su proyecto minero Angostura, por medio del cual pretendía explotar oro y plata dentro de los límites del Páramo de Santurbán, en Santander.

De acuerdo con el documento, ciertas medidas que ha tomado el Gobierno, y en particular la Agencia Nacional de Minería (ANM), había hecho inviable el proyecto y obligó a Eco Oro a la renuncia de la concesión para mitigar sus pérdidas, haciendo referencia a la delimitación del Páramo de Santurbán del 2014 que redujo el proyecto minero en un 50%.

El Ministerio de Ambiente actualmente prepara una nueva delimitación de este ecosistema, tal como fue ordenado por la Corte Constitucional, sin embargo el área protegida no puede ser menor de la de la primera delimitación. Es decir, **el proyecto Angostura quedaría nuevamente en el perímetro del Páramo de Santurbán**.

Según Mayerly López, vocera del [Comité para la Defensa del Agua y el Páramo de Santurbán, la decisión de la compañía de cerrar el proyecto significa "un triunfo" para el movimiento ciudadano que llevaba 25 años en oposición a la minería y proteger este invaluable ecosistema, surtidor de agua a más de 2,5 millones de personas. (Le puede interesar:]"'[No se puede fraccionar el Páramo de Santurbán': Comité para la Defensa](https://archivo.contagioradio.com/no-se-puede-fraccionar-paramo-santurban-comite-la-defensa/)")

Sin embargo, López resaltó que la demanda que Eco Oro interpuso en el 2016 en contra del Estado por las pérdidas económicas sostenidas sigue en curso. La empresa **pide 764 millones de dólares como compensación al no poder realizar su proyecto minero,** una suma de dinero que será difícil de pagar para el Gobierno si la empresa llegase a ganar, sostuvo la vocera.

"Estas multinacionales nunca pierden, Eco Oro renuncia a su título pero demanda a la nación. Aquí **el único responsable es el Estado colombiano por dar títulos mineros en un ecosistema de alta importancia para el país** y el suministro de agua a las comunidades", afirmó.

Finalmente, Eco Oro indicó que se comunicará con las autoridades pertinentes para discutir un plan para el cierre de sus operaciones, teniendo en cuenta los aspectos técnicos y ambientales, tal como lo exige la normas.

<iframe id="audio_37718283" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37718283_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
