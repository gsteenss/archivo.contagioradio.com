Title: 77 defensores de DD.HH. han sido asesinados en el primer semestre de 2018
Date: 2018-09-25 13:12
Author: AdminContagio
Category: DDHH, Nacional
Tags: defensores de derechos humanos, lideres sociales, Somos defensores
Slug: 77-defensores-asesinados
Status: published

###### [Foto: Contagio Radio] 

###### [25 Sep 2018] 

El Programa Somos Defensores manifestó en su más reciente informe "Más allá de las cifras (segunda parte)" que entre enero y junio de 2018 el **Sistema de Información de Agresiones contra defensores de Derechos Humanos en Colombia (SIADDHH)** registró **397 agresiones individuales contra los líderes**, de las cuales, 77 corresponden a asesinatos.

Con el Informe, el Programa busca que se vaya "más allá de las frías estadísticas de asesinato de líderes" y se adentre en el análisis humano, así como político de las violencias que sufren los defensores de derechos humanos en el país. El documento contiene la información detallada sobre el perfil de los líderes asesinados, y detalla que la mayoría de ellos eran **"personas de escasos recursos, en zonas de alta vulnerabilidad, y que tras su asesinato, una familia fue desintegrada y un proceso social fue silenciado".**

Según datos del SIADHH, los procesos que más se han visto afectados son las Juntas de Acción Comunal, resguardos y comunidades indígenas, así como líderes que trabajan en la sustitución de cultivos de uso ilícito. Por otra parte, el informe destaca que **el asesinato de líderes aumentó en un 34% en relación con el mismo periodo durante el año anterior,** mientras el número de agresiones se incrementó un 16%. (Le puede interesar: ["Denuncian que helicóptero del Ejército atentó por error contra grupo de indígenas"](https://archivo.contagioradio.com/helicoptero-atento-error-indigenas/))

\[caption id="attachment\_57002" align="aligncenter" width="474"\]![Captura de pantalla 2018-09-25 a la(s) 11.50.06 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-25-a-las-11.50.06-a.m.-482x208.png){.wp-image-57002 width="474" height="204"} Informe: "Más allá de las Cifras, Segunda Parte"\[/caption\]

Somos Defensores destaca que **12 de los 77 defensores y defensoras asesinados, habían denunciado amenazas en su contra,** y que en 70 de estos casos, los homicidios se perpetraron en la modalidad de sicariato. Sin embargo, en el documento no se muestran cifras de avance en las investigaciones porque este dato aún no han sido reveladas por la Fiscalía General de la Nación. (Le puede interesar: ["En más del 90% de casos de asesinatos de líderes sociales aún no se alcanza justicia"](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/))

La Organización también manifiesta su preocupación sobre l**as Aguilas Negras, que son responsables "de aproximadamente el 60% de las amenazas contra líderes sociales desde 2006 a la fecha"** pero no se ha identificado la existencia si quiera de ese grupo armado. Igualmente, informó que en el 47% de los casos, los autores de los hechos son presuntamente paramilitares, mientras un 38% de los actores son desconocidos.

\[caption id="attachment\_57003" align="aligncenter" width="538"\]![Captura de pantalla 2018-09-25 a la(s) 11.53.17 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-25-a-las-11.53.17-a.m.-372x164.png){.wp-image-57003 width="538" height="237"} Informe: "Más allá de las Cifras, Segunda Parte"\[/caption\]

Un dato que resulta clave es que en **17 de los 77 casos, los hechos de violencia también afectaron a terceras personas,** es decir, que se vieron involucrados familiares o personas cercanas al líder. Del total de homicidios, un 74% fueron hombres, mientras el restante 26% eran mujeres. (Le puede interesar: ["Asesinan a Julían Areiza, sobrino del líder del movimiento Ríos Vivos"](https://archivo.contagioradio.com/asesinan-julian-areiza-rios-vivos/))

De igual forma, la investigación evidencia que los departamentos con mayor número de atentados contra líderes sociales son **Cauca (13 casos), Antioquia (12 casos) y Norte de Santander (10 casos)**; seguidos de Valle (6 casos) y Córdoba (5 casos). (Le puede interesar: ["El próximo 5 de octubre 'pasarán al tablero' autoridades que protegen líderes: Iván Cepeda"](https://archivo.contagioradio.com/5-octubre-lideres-cepeda/))

\[caption id="attachment\_57008" align="aligncenter" width="457"\]![Captura de pantalla 2018-09-25 a la(s) 11.54.36 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-25-a-las-11.54.36-a.m.-478x622.png){.wp-image-57008 width="457" height="595"} Informe: "Más allá de las Cifras, Segunda Parte"\[/caption\]

 

### **"Más que una cifra"** 

Con el Informe, Somos Defensores espera que los programas, decretos y compromisos firmados, como el 'Pacto por la Vida', se cumplan; y se afronte el peligro con el que muchos defensores tienen que vivir a diario por el ejercicio de su labor. De igual manera, el Programa espera que **se recuerde a las 77 vidas, familias y procesos sociales que se vieron afectadas durante los primeros 6 meses del año.** (Le puede interesar: ["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
