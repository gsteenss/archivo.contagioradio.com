Title: La relación entre Chiquita Brands, Álvaro Uribe y las Convivir
Date: 2018-09-04 08:33
Author: AdminContagio
Category: DDHH, Nacional
Tags: alvaro uribe velez, Bajo Atrato, Chiquita Brands, Convivir, Empresas Bananeras
Slug: la-relacion-entre-chiquita-brands-alvaro-uribe-y-las-convivir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/palma-y-paramilitarismo-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [03 Sept 2018] 

El llamado a juicio que hizo la Fiscalía a 14 empleados de la multinacional Chiquita Brands por su supuesta financiación a grupos paramilitares, permitió que, a su vez, se compulsaran copias para investigar a ex gobernadores y alcaldes los departamentos de Antioquia y Chocó, **entre ellos el actual senador Álvaro Uribe Vélez**. Hecho que según la defensa de las víctimas podría develar la relación entre esa multinacional, las Convivir y el expresidente.

### **Chiquita Brands y su responsabilidad en la financiación a estructuras paramilitares** 

Sebastián Escobar, abogado defensor en este caso, señaló que las víctimas han recibido este mensaje de la Fiscalía como un avance en la justicia frente a la financiación de estructuras paramilitares que “desvirtúa la impunidad que ha existido durante tantos años sobre estos hechos”.

Financiación que se habría producido a partir de las **filiales de Chiquita Brands en Colombia Banadex y Banacol**, a grupos paramilitares organizados bajo la figura de las convivir. Por tal motivo, los acusados deberán responder por el delito de concierto para delinquir agravado. (Le puede interesar: ["La multinacional Chiquita Brands podría afrontar juicio en Estados Unidos"](https://archivo.contagioradio.com/la-multinacional-chiquita-brands-podria-afrontar-juicio-en-estados-unidos/))

Los llamados a juicio son: Reinaldo Escobar de La Hoz, Luis Cuartas Carrasco, Víctor Buitrago Sandoval, Álvaro Acevedo González, Víctor Manuel Henríquez Velásquez, Javier Ochoa Velásquez, Juan Diego Trujillo Botero, Jorge Cadavid Marín, Dorn Robert Wenninger y John Paul Olivo, Charles Dennis Keiser, los tres de nacionalidad norteamericana, José Luis Valverde Ramírez de Costa Rica y Fuad Alberto Giacoman Hasbún de Honduras.

Además, el abogado expresó que si bien este proceso ha sido “traumático” debido a la salida de la multinacional del territorio colombiano, la Fiscalía ha logrado establecer que, **a través de algunas figuras del derecho comercial, Chiquita Brands ha seguido tendiendo actividades económicas en el país.**

Escobar aseguró que la financiación de estructuras paramilitares desde Chiquita Brands se dio a partir de dos mecanismos: **el primero consistió en una financiación directa y el segundo a través de la conformación de cooperativas de seguridad o CONVIVIR,** “entidades que sirvieron como fachadas de los grupos paramilitares para canalizar la recepción de ayudas de agentes económicos”. En este caso, particularmente se describe el accionar de la **Convivir Papagallo, en el Uraba Antioqueño.**

Para Escobar, la determinación también es un mensaje para quienes “siendo terceros” en el marco del conflicto armado y por ser denominados de esa forma en el Sistema de Verdad Justicia Reparación y no Repetición, decidan acudir a este escenario para aportar y asumir su responsabilidad en la violación a derechos humanos.

### **Chiquita Brands, las CONVIVIR y Álvaro Uribe Veléz** 

Al llamado de los 14 empleados de la multinacional, se sumó la compulsa de copias desde la Fiscalía a quienes ejercieron como gobernadores o alcaldes del departamento de Antioquia, entre quienes se encuentra el actual senador por el Centro Democrático **Álvaro Uribe Vélez, una de las personas que más promovió la creación de las Cooperativas de Seguridad CONVIVIR.**

Asimismo, según Escobar, se ha comprobado que la multinacional **Chiquita Brands y el gremio bananero, apoyó las diferentes postulaciones de Uribe** a cargos de elección popular, tanto a la gobernación de Antioquia como a la presidencia de la República, situación que demostraría “el compendio entre el gremio y Uribe, no solo en intereses electorales, sino también, de promoción de ese tipo de organizaciones”. (Le puede interesar: ["Ejecutivos de Chiquita Brands podrían ser investigados por la Corte Penal Internacional"](https://archivo.contagioradio.com/chiquitabananerasparamilitares/))

Además, Escobar también confirmó que existirían numerosos testimonios que ubican a **funcionarios de la gobernación de Uribe “en la promoción de esas cooperativas de seguridad**, y que no lo hicieron solo porque estuvieran amparados en la ley, porque efectivamente contaban con un respaldo legal, sino que **conocían de las practicas criminales**”, ejemplo de ello serían las declaraciones de algunos desmovilizados del paramilitarismo que habrían señalado a **Pedro Juan Moreno,** secretario de gobierno de Álvaro Uribe, como uno de los mayores promotores de las CONVIVIR.

<iframe id="audio_28316846" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28316846_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

<div class="osd-sms-wrapper">

</div>
