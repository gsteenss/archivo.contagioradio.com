Title: Al Shabaab atenta contra el Ministerio de Educación de Somalia
Date: 2015-04-14 12:57
Author: CtgAdm
Category: DDHH, El mundo
Tags: 10 muertos en Somalia atentado Al Shabaab, Al Shabaab, Al Shabaab Somalia, Somalia atentado en ministerio de Educación
Slug: al-shabaab-atenta-contra-el-ministerio-de-educacion-de-somalia
Status: published

###### Foto:Voxpopulis.com 

**Diez muertos** y más de una decena de heridos produjo el atentado perpetrado por la **milicia islámica rebelde** **Al Shabaab** contra el misterio de la educación de **Somalia**.

Tres integrantes de la milicia han **explotaron un coche bomba en la puerta del edificio, ingresaron atacando a la seguridad** y a funcionarios que se encontraban en el momento lo que produjo un enfrentamiento con la policía que terminó con la muerte de los tres atacantes.

Entre las diez víctimas mortales del ataque hay funcionarios civiles, principalmente del sector de la educación, militares, y policías que han fallecido durante el enfrentamiento posterior al ataque. También ha fallecido un soldado de la Unión Africana cuando ha explosionado el coche bomba.

**La radio rebelde de la milicia** islámica “Andalus” **ha reivindicado la autoría del atentado** en horas de la mañana en nombre de Al Shabaab, calificando la acción de “Operación sagrada”.

**Al Shabaab**, que significa «**Movimiento de Jóvenes Muyahidines**» nace en **Somalia** después de ser derrotada en las elecciones la Unión de Cortes Islámicas, formándose así el grupo armado que actualmente es considerado por Mogadiscio y por la comunidad internacional como terrorista.

Actualmente cuenta con más de **3.000 combatientes**, y tiene vínculos con otros grupos extremistas como **Boko Haram**, su **financiación proviene del tráfico ilegal de marfil** proveniente de los cuernos de los rinocerontes y su objetivo es imponer un estado islámico de carácter autoritario y aplicar la ortodoxa interpretación de la ley islámica conocida como ley Sharia.

El 2 de Abril perpetro un **atentado contra una universidad en Kenia dejando un saldo de 152 estudiantes muertos**, en represalia por los ataques de Kenia a sus bases en el sur de Somalia.

Entre sus acciones están el atentado contra la embajada estadounidense o el atentado en Uganda contra La Copa Mundial de Fútbol en 2010, así como de realizar juicios en consonancia con la **ley Sharia**, lapidando y torturando a personas inocentes.
