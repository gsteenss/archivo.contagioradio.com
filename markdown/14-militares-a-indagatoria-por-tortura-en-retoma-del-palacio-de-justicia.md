Title: 14 militares a indagatoria por actos de tortura en retoma del Palacio de Justicia
Date: 2015-10-22 11:37
Category: Judicial, Nacional
Tags: 1985, B2 del Ejército, Cantón Norte de la Policía, Carlos Alberto Fracica Naranjo, Casa Museo del Florero, Coici, Edilberto Sánchez Rubiano, Eduardo Matson, Iván Ramírez Quintero, militares acusados de tortura, Palacio de Justicia, Rafael Hernández López, Rafael Urrego, Retoma del Palacio de Justicia, Universidad Externado de Colombia, Yolanda Santodomingo
Slug: 14-militares-a-indagatoria-por-tortura-en-retoma-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/IMAGEN-11058581-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Tiempo 

###### [22, Oct 2015]

**Catorce militares han sido llamados por la Fiscalía General de la Nación** **para rendir indagatoria** por casos de tortura durante la retoma del Palacio de Justicia en noviembre de 1985.

**Iván Ramírez Quintero,** excomandante del Coici y Edilberto Sánchez Rubiano, ex comandante de Inteligencia del B2 del Ejército, son investigados por las torturas de las que fueron víctimas los jóvenes **Eduardo Matson y Yolanda Santodomingo** de 21 y 20 años de edad respectivamente, quienes eran estudiantes de la Universidad Externado de Colombia.

Así mismo, la Fiscalía llamó a indagatoria **al **excomandante de la escuela de Artillería general, Rafaél Hernández López**** y el **excomandante de operaciones, Carlos Alberto Fracica Naranjo**, por su presunta responsabilidad en **la muerte del magistrado auxiliar del Consejo de Estado, Carlos Horacio Urán**, a quien se vio salir con vida del Palacio de Justicia, y luego fue encontrado muerto.

Además de estos cuatro excomandantes, también fueron llamados a rendir declaraciones los militares William Vásquez Rodríguez, Ferney Causaya Peña, Fernando Nieto Velandia, Antonio Jiménez Gómez, Fernando Blanco Gómez, Gustavo Arévalo Moreno, Bernardo Garzón Garzón, Luis Carvajal Núñez, Eliseo Peña Sánchez, y Antonio Buitrago Tellez. Estos últimos, están citados por la Fiscalía debido a que conocerían sobre los actos de tortura a los que fueron sometidas varias personas a quienes el ejército “marcó” **como sospechosos de colaborar con el M-19 en la toma del Palacio.**

**Estudiantes torturados**

Los excomandantes Ramírez Quintero y Sánchez Rubiano, tendrían que responder frente a los hechos de tortura contra los jóvenes Eduardo Matson y Yolanda Santodomingo, quienes fueron sacados del Palacio de Justicia, luego fueron trasladados por un grupo de militares a la Casa Museo del Florero y finalmente fueron llevados  a una sede de la Policía.

Matson y Santodomingo, fueron torturados por militares desde que los llevaron a la Casa Museo del Florero hasta el Cantón Norte de la Policía. Según cuenta el joven de 21 años, **los militares los acusaban de guerrilleros, y fueron interrogados violentamente durante 8 horas, hasta que finalmente los soltaron.**

Ambos estudiantes de la Universidad Externado de Colombia, llegaron al Palacio de Justicia a las 11 de la mañana el 6 de noviembre de 1985, para presentar una prueba de práctica penal con el conjuez de la Sala Penal, Rafael Urrego.
