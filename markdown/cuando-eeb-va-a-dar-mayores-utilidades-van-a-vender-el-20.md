Title: Cuando EEB va a dar mayores utilidades van a vender el 20%
Date: 2016-11-08 11:32
Category: Economía, Entrevistas
Tags: Bogotá, Concejo de Bogotá, Distrito, EEB, Empresa de Energía de Bogotá, Enrique Peñalosa, SIndicatos, Venta de la EEB
Slug: cuando-eeb-va-a-dar-mayores-utilidades-van-a-vender-el-20
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/EEB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana ] 

###### [8 Nov. de 2016] 

Desde el año 2008, **Bogotá ha logrado recibir cerca de \$3,6 billones en utilidades de la Empresa de Energía de Bogotá –EEB-**, ésta es tan solo una de las cifras que diversos sectores que se oponen a la venta del 20% de las acciones de la EEB, han dado a conocer para dimensionar el daño que se haría, entregando a privados dicha entidad.

Pese a ello, el Concejo de Bogotá dará inició a la votación a este proyecto del alcalde Enrique Peñalosa que ha sido considerado como polémico luego de la venta de la ETB. **El objetivo principal de este proyecto de la venta del 20% de la EEB, es poder conseguir recursos para ejecutar importantes obras en la ciudad.**

Según Pablo Santos, presidente del Sindicato de Trabajadores de la Energía de Colombia-  Sintraeleconal "**si este proyecto sale favorecido en el Concejo de Bogotá, cuando se venda la EEB están primando intereses particulares, privados y no de la ciudad. Es un adefesio".**

Los sectores que se oponen a este proyecto han asegurado que en el momento que la EEB va a dar mayores utilidades es cuando optan por vender el 20% de las acciones. Y según Santos, la idea es recoger dinero para poder “pavimentar la reserva Van Der Hammen”, a su vez agregó que los recursos que se consigan van a ser invertidos en la compra de flota de Transmilenio para pasar por dicha reserva.

Según el líder de este sindicato, pretender privatizar las empresas dejan a las poblaciones en miseria, desfinanciación y más impuestos y afirmó que **“la Empresa de Energía de Bogotá no le pertenece a la alcaldía de la capital, sino a todos los bogotanos”** a quienes convocarán  a las diversas actividades que se pondrán en marcha para detener la venta de dichas entidades.

Pese a los argumentos en contra y las campañas en las que han instado a Peñalosa a no vender la EEB, las últimas deliberaciones en torno a esta iniciativa continuarán en el Concejo de Bogotá, que en su mayoría ha asegurado, respaldará el proyecto de la actual Alcaldía.

**La EEB en cifras**

Según recientes cifras, la Empresa de Energía de Bogotá está generando al año 1 billón de utilidades para la capital. Además, es líder en cadena productora de energía, lo que ha permitido que tenga presencia en Brasil, Perú y Guatemala.

Así mismo, la EEB es una de las mayores compañías transportadoras de gas de Colombia y le da 400 mil millones al metro subterráneo. Le puede interesar: [Vender el 20% de la EEB es una pérdida millonaria para el distrito](https://archivo.contagioradio.com/vender-el-20-de-la-eeb-es-una-perdida-millonaria-para-el-distrito/).

<iframe id="audio_13667454" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13667454_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
