Title: 210 cuerpos de agua cerca al Lago de Tota desaparecieron por actividad petrolera
Date: 2016-10-03 23:26
Category: Ambiente, Nacional
Tags: Ambiente, Lago Tota, pretróleo
Slug: 210-cuerpos-de-agua-cercanos-al-lago-de-tota-desaparecieron-por-actividad-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Lago-Tota-e1475510599290.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sites.google 

###### [3 Oct 2016] 

Hace 5 años, 6 municipios del departamento de Boyacá, vienen siendo afectados por el inicio de a actividad petrolera cerca al Lago de Tota. Hoy, **pese a las movilizaciones de las comunidades, denuncian que 210 cuerpos de agua han desaparecido,** y que la empresa Maurel & Prom pretende continuar con la explotación y extracción de crudo.

Laura López, integrante del Colectivo por la Defensa de la Provincia de Sugamuxi, relata como hace cinco años, ingenieros de la compañía le dicen a las comunidades que “sólo van a instalar unos aparatos de medición de sísmica para tomar algunas pruebas y luego retirarlos”, tiempo después de instalados, los habitantes se ven aturdidos por fuertes ruidos, y son testigos del **progresivo desecamiento de nacimientos de agua, pozos comunales, agrietamientos y deslizamientos.** Al profundizarse las aguas superficiales empiezan los inconvenientes de las comunidades con esta compañía.

Se trata de la multinacional francesa Maurel & Prom, que en el 2011 junto a la Compañía Geofísica Latinoamericana, CGL, dan a conocer a las comunidades de Betania, Tota, Pesca, Firavitoba, Iza y Sogamoso un engañoso proyecto se sísmica y extracción petrolera, que afectaría gravemente a estos territorios aledaños a la cuenca del Lago de Tota, según cuenta López.

"Nosotros vamos a ingresar una carga de Sismigel, que explota pero no explota", es la frase con la que ingenieros de la empresa ingresaron al territorio a realizar la actividad sísmica, como lo explica Laura, quien agrega que realmente **se trataba de un explosivo de nivel 6 de riesgo,** según pudieron conocer por medio de la información obtenida de la ficha técnica dada por Indumil en Sogamoso.

López dice que la empresa pagó a habitantes de la zona por dejar pasar los cables de activación del explosivo, a través de sus predios, que luego se vieron gravemente afectados, pues muchas viviendas elaboradas en adobe y tapia pisada se derrumbaron. Las familias afectadas denuncian esta situación y en respuesta, la empresa les ofrece a cada una 80 mil pesos para la reparación de sus viviendas y la apertura de vacantes laborales.

Esa actividad de sísmica, generó un impacto ambiental directo frente al recurso hídrico ya que existe el riesgo que se extienda la profundización de aguas y por tanto las aguas superficiales se sequen, entre ellas el río Chicamocha que atraviesa buena parte del país y cuya cuenca está ubicada en uno de los municipios más afectados. El colectivo actualmente se encuentra trabajando en determinar cuántos nacimientos de agua se han perdido, pero por el momento tienen registro de 210 aljibes, de acuerdo con denuncias expuestas por familias campesinas.

Inicialmente la licencia otorgada por el ANLA a la multinacional, para la exploración y explotación petrolera fue **de 35.000 hectáreas que se redujeron a 8.033 hectáreas,** al hacerse efectivas algunas denuncias en un comunicado emitido por la Contraloría afirmando que en esa zona no es posible hacer actividad petrolera por las irreversibles afectaciones que se generan en el Lago de Tota y comunidades aledañas. El perímetro se alejó de la cuenca del lago, pero se sigue afectando la cuenca del río y el impacto se extiende más allá de las comunidades rurales, llegando a los habitantes de Sogamoso quienes consumen las aguas de esta zona de recarga hídrica, afirma Laura López.

Frente a este panorama, **las comunidades decidieron organizarse para impedir la exploración petrolera en sus territorios,** desde diferentes organizaciones ambientalitsas exigieron una Audiencia Pública donde se evidenciaron las afectaciones y denuncias puntuales de las comunidades sobre el tema hídrico, de remociones en masa y e desplazamiento. Esa información fue enviada al Ministerio de Ambiente, la Procuraduría, la Contraloría, Corpo-Boyacá y la Agencia Nacional de Hidrocarburos, sin embargo, hasta el momento los pobladores no han obtenido respuesta o solución alguna.

“**Se nos olvida que los ecosistemas están conectados,** que la transformación del paisaje y del entorno, del territorio (…) por estas actividades extractivas genera otras formas de conflicto, me preocupa que vamos a cesar un conflicto armado y vamos a generar un conflicto ambiental  que genera conflictos socioeconómicos, de vivienda y salud de las familias (…) pérdida de identidad y desarraigo con el territorio. Entonces el lago muy bonito y al rededor de él desiertos, me preocupa”, manifiesta Laura López.

<iframe id="audio_13176935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13176935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
