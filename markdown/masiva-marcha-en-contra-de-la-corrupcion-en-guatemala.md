Title: Masiva marcha en contra de la corrupción en Guatemala
Date: 2015-05-19 18:30
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Corrupción en Guatemala, dimisión vicepresidenta Roxana Baldetti, Escandalo aduanero Guatemala, Manifestación en contra corrupción Guatemala, Red de corrupción aduanera Guatemala
Slug: masiva-marcha-en-contra-de-la-corrupcion-en-guatemala
Status: published

###### Fotos:Rt.com 

###### **Entrevista con [Brenda Hernández] activista de los movimientos sociales de Guatemala:  
<iframe src="http://www.ivoox.com/player_ek_4517393_2_1.html?data=lZqemZidd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9Dm09rdxc6Jh5SZo5jbjcrSb6jpwtnSz8bQpYztjNLO1c7apYzk09Thx9jYpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>** 

Masiva **marcha en contra de la corrupción y del actual gobierno en Guatemala**. Miles de manifestantes salieron a las calles la semana pasada cuando se anunció el escándalo de **corrupción aduanera que implica a la vicepresidenta del país**.

Después de que el viernes pasado se viera salpicada por el escándalo de corrupción aduanera la vicepresidenta** Roxana Baldetti** y dimitiera el mismo día; la indignación tomó las calles de las principales ciudades de Guatemala.

**Juan Carlos Monzón, ex-secretario privado de la vicepresidenta lideraba** la red de **corrupción**, su implicación y las escuchas realizadas a altos mandos del gobierno fueron el detonante para implicar a Roxana.

Dos días antes de que dimitiera la vicepresidenta la Corte Suprema ya había decidido quitarle la inmunidad parlamentaria debido a las sospechas emergidas de las investigaciones judiciales.

Actualmente el **ex-secretario de la vicepresidenta se encuentra huido** y se desconoce su paradero, aunque las últimas investigaciones apuntan hacía que podía haber huido a Corea del Sur, siendo este una pieza clave para desenmascarar la red de corrupción.

El éscandalo fue destapado el mes pasado por instancias judiciales del mismo país y organismos internacionales como la ONU.

En este caso fue la **Comisión Internacional contra la Impunidad en Guatemala,** organismo dependiente de la **ONU**, quién investigó y encontró más de 40 casos de sobornos ilegales que implican a gran parte del funcionariado del actual gobierno.

Según **Brenda Hernández, activista de los movimientos sociales de Guatemala**, el anuncio del desmantelamiento de la red de corrupción aduanera ha sacado a la gente espontáneamente a las calles, manteniendo la protesta día y noche.

Además la activista añade que las distintas incoherencias de los mandatarios actualmente en el gobierno, junto con la **gran cantidad de funcionarios implicados pone al gobierno en una situación difícil al haber amparado dicha red**.

Para Brenda "**...quienes negaron el genocidio y la memoria en la guerra civil, las graves  violaciones de DDHH por parte del estado son quienes mantienes esas estructuran que les permiten mantener su status quo...**".

Las protestas de los movimientos sociales y de la ciudadanía van encaminadas hacia la **renuncia** de los gobernantes implicados, su **judicialización** e internamiento en prisión y la **devolución de todo el dinero** robado al estado.

Hernández afirma que una de las reivindicaciones es la reforma de la ley de partidos electoral para impedir que en la financiación de los partidos exista corrupción, ya que según ella es en la financiación electoral donde comienza toda la estructura de evasión tributaria y robo de dinero estatal.

Las **protestas constituyen gran esperanza** para un país que aún no ha cerrado las heridas de la guerra civil y donde apenas habían protestas excepto en el interior en contra **macroproyectos extractivistas**.

   
   
 
