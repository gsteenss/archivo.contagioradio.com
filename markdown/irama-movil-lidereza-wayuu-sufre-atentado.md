Title: Irama Móvil, lidereza Wayúu sufre atentado
Date: 2020-10-30 11:03
Author: CtgAdm
Category: Actualidad, Líderes sociales
Slug: irama-movil-lidereza-wayuu-sufre-atentado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/06_jud_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: periodicolaguajira*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este jueves 30 de octubre en horas de la noche la **lideresa indígena Irama Móvil fue víctima de un atentado con arma de fuego** mientras se movilizaba en su esquema de seguridad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1321990543308632066","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1321990543308632066

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sobre las 9:00 de la noche de este jueves, diferentes líderes indígenas entre ellos Luis Kankui , Consejero Mayor de la Organización Nacional Indígena de Colombia (ONIC), denunciaron que **mientras Irama Móvil se desplazaban por el barrio 15 de mayo en la ciudad de Riohacha su vehículo recibió múltiples impactos de arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El hecho se registró cerca del Colegio Ecológico de El Carmen, lugar en donde según testigos se dio un intercambio de disparos entre los agresores de la lideresa y los integrantes de su esquema** de seguridad en defensa de su vida; acción qué causó dos heridos aún sin identificar, pero que según información de la UNP, se encuentran fuera de peligro así como **Irama Móvil quien resultó ilesa.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ***"Nos siguen matando y no pasa nada"***, no es la primera vez que Irama Móvil esta en riesgo

<!-- /wp:heading -->

<!-- wp:paragraph -->

La lideresa Wayúu fue candidato al Concejo Directivo por las Comunidades Indígenas, ante la Corporación Autónoma Regional (Corpoguajira) , y **su trabajo siempre ha estado enfocado hacia la defensa de los Derechos Humanos de las comunidades Wayúu en Riohacha y Manaure**. (

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Móvil, **había sido víctima el año pasado de un atentado también con arma de fuego** mientras se movilizaba en su vehículo en la vía que comunica de Riohacha a Cuestecita. ([Resistencia de comunidades indígenas durante conflicto armado](https://archivo.contagioradio.com/resistencia-de-comunidades-indigenas-durante-conflicto-armado/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MPCindigena/status/1322001155048804354","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MPCindigena/status/1322001155048804354

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a este hecho diferentes organizaciones de Derechos Humanos que se pronunciaron entre ellas la **[Onic](https://www.onic.org.co/)**, quién a través de su cuenta de Twitter **exigió que se protegiera la vida de la lideresa y la de su familia y se garantizará el ejercicio de su labor en la Guajira** así como el de las comunidades que ella acompaña.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/luiskankui/status/1322001857540169728","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/luiskankui/status/1322001857540169728

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al mismo tiempo la Mesa Permanente de Concertación de los Pueblos y Organizaciones Indígenas, señalaron que es necesario que se paren los crímenes contra nuestros líderes y lideresas indígenas en Colombia, ***"nos siguen matando y no pasa nada"***, esto ante los dos atentados contra los dirigentes indígenas, **Feliciano Valencia en Tacueyo, municipio de Toribio Cauca, y en horas de la noche hacia la lideresa Irama Móvil .** ([Atentan contra el senador Feliciano Valencia](https://archivo.contagioradio.com/atentan-contra-la-vida-del-senador-feliciano-valencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y recordaron, que en lo corrido del año 2020 el Observatorio de Derechos Humanos de los pueblos indígenas de Colombia, **han reportado el asesinato de 91 líderes indígenas,** ***"está claro que la integridad de las autoridades y dirigentes indígenas en el país se encuentra en un inminente riesgo** de muerte".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
He hicieron un llamado de urgencia para que el Gobierno garantice la vida de los pueblos indígenas y su dirigencia, así como a la comunidad internacional y organizaciones de Derechos Humanos para que respalden y garanticen la vida del movimiento indígena de Colombia. ([Duque expresó "el desprecio que siente por la ciudadanía", Ángela Robledo](https://archivo.contagioradio.com/duque-expreso-el-desprecio-que-siente-por-la-ciudadania-angela-robledo/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"**Estamo**s cansados de enterar día a día nuestros hermanos y hermanas** a lo largo y ancho del territorio nacional. Somos actores de paz y exigimos para del genocidio en contra de nuestra gente"*
>
> <cite>Mesa Permanente de Concertación Indígena -MPC</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
