Title: Comunidades exigen a la Corte Constitucional no modificar consultas populares
Date: 2018-04-11 13:41
Category: Ambiente, Nacional
Tags: consultas populares, Corte Constitucional, Ministerio de hacienda, Salento
Slug: se-alista-gran-movilizacion-en-defensa-de-las-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/6d2de502-6c16-45ca-ba07-34175e79ac63-e1523466584409.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lina Álvarez] 

###### [6 Abr 2018] 

Pese a las acciones y anuncios del gobierno que han frenado a realización de consultas populares en Colombia, las comunidades siguen exigiendo su derecho a decidir sobre sus territorios. En los municipios del Peñón y Carmen de Chucurí, Santander, la población se ha realizado diferentes acciones de movilización contra la minería y el petróleo, y en Salento, Quindío **el Tribunal Administrativo del Quindío dio el aval a la pregunta para la consulta popular.**

Además, teniendo en cuenta que **este 12 de abril la Corte Constitucional realizará una audiencia pública para definir el futuro de las Consultas Populares,** las organizaciones ambientalistas y las comunidades han convocado a una gran movilización en todo el país para "Exigir a la Corte no modificar la sentencia T-445 16 por la cual se faculta a los entes territoriales para decidir el uso del suelo y prohibir las represas, fracking y la minería a gran escala".

### **Ecopetrol le incumple al Carmen de Chucurí** 

De acuerdo con la denuncia de la comunidad del Carmen de Chucurí, la empresa de economía mixta, Ecopetrol, actualmente está invadiendo la carretera que se encontraba en pavimentación por parte del Consorcio Unidos por Santander.

Según  Nini Johana Cárdenas, integrante del Comité por el NO en la consulta popular del ese municipio, se trata de una obra que desde hace años vienen necesitando los habitantes, y aunque lo único que se le ha pedido a la empresa es que cambie de lado la tubería con la cual sacan el crudo, Ecopetrol ha hecho caso omiso a las exigencias de la comunidad, y a los compromisos pactados.

Es decir, que actualmente **Ecopetrol está obstaculizando 100 metros de vía, por lo cual el consorcio señala que solo podrá pavimentar 5 metros de los 100 que debería**. "La comunidad está cansada, y aunque dice que se van a  retirar, no lo han hecho, por eso llevamos tres días de plantón para exigir a Ecopetrol que cambie esa tubería, y no nos vamos a ir hasta que lo hagan", expresa Cárdenas.

Mientras tanto, en Carmen de Chucurí, la población sigue exigiendo su derecho a la consulta popular, pese a que, como ha sucedido en otros municipios el Ministerio de Hacienda se ha negado a proveer lo recursos necesarios para dar trámite a ese mecanismo de participación popular.

### **El Peñón sigue movilizándose** 

En este municipio de Santander, **cerca de mil personas se movilizaron la semana pasada exigiendo que se impida la entrada de empresas mineras y petroleras al territorio**. A los habitantes les preocupa que prácticamente la totalidad de su territorio está titulado para actividades mineras, especialmente para la extracción de cobre.

De acuerdo con Lina Álvarez, integrante del Comité de la consulta por el NO, la consulta para impedir que iniciaran las actividades mineras era el 5 de noviembre pero no se desembolsaron los recursos. No obstante, se han llevado a cabo otras acciones para blindar el municipio de actividades extractivas, de hecho el concejo del municipio ya aprobó un acuerdo para prohibir dichas actividades. "Decimos no a la minería, consulta popular ya, y queremos dejar claro que son campesinos los que están protegiendo las fuentes hídricas y por eso no deseamos que se realice ningún tipo de actividad extractiva", expresa Álvarez.

### **Audiencia sobre consultas populares** 

La audiencia que se realizará este jueves fue citada por la Corte Constitucional. Allí se invitarán tanto a analistas y especialistas en temas ambientales a favor de las consultas populares, como a representantes de empresas que realizan la explotación de hidrocarburos y minerales en Colombia. Esto con la finalidad de profundizar en los alcances y los límites de las consultas populares.

Para el abogado ambientalista Rodrigo Negrete, esta audiencia genera preocupación debido a que la Corte ya había fijado una posición clara y contundente frente a las consultas populares donde ratifica que se puede consultar a las comunidades sobre el uso del suelo. Sin embargo, **ahora se le pide a la Corte que vuelva a estudiar este mecanismo en el caso puntual de la consulta de Cumaral, lo que podría significar cambios en la decisión de este organismo.**

<iframe id="audio_25199424" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25199424_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
