Title: La cara oculta de Poligrow, empresa palmera en Mapiripán
Date: 2015-07-30 15:20
Category: Ambiente, Nacional
Tags: abilio, aceite, aceitera, comision, eia, Justicia y Paz, mapiripan, Palma, peña, Poligrow
Slug: poligrow-palma-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-30-a-las-15.18.04.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión Intereclesial de Justicia y Paz 

<iframe src="http://www.ivoox.com/player_ek_5594548_2_1.html?data=lpqmlpqYfI6ZmKiak5WJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9HZ08bQy9TSqdSf0cbfw9LNsMrowtfS1ZDJsozhxsnW0ZDIqYzkzcbb1sbHrdDixtiYxsqPlI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Abilio Peña, Comisión Intereclesial de Justicia y Paz] 

###### [30 jul 2015] 

La empresa de palma aceitera, Poligrow, genera daños ambientales en Mapiripán, departamento del Meta, y afectaciones a las comunidades indígenas de la región en connivencia con grupos paramilitares como las "Autodefensas Gaitanistas", los "Carachos", y el "Bloque Meta"; Así lo develaría el informe que presentan esta semana en Estados Unidos la Comisión Ide Justicia y Paz y la Agencia de Investigación Ambiental (EIA por sus siglas en ingles).

Según explica Abilio Peña, de la Comisión, las actividades de la empresa podrían generar sequías como las que conoció Colombia en el año 2014 en Paz de Ariporo, y que ocasionó la muerte de miles de chigüiros; esto como consecuencia de la plantación de 15 mil hectáreas palma aceitera que acaba con las palmas de moriche y las fuentes hídricas subterraneas de la región.

Las organizaciones aseguran que, además, la empresa ha prohibido de manera expresa a las comunidades indígenas Jiw Y Sikuani la práctica de actividades ancestrales, como la visita a la laguna sagrada de las Toninas, en la que se encuentran delfines rosados, y que fue adquirida por la empresa Poligrow para generar una zona franca e instalar plantas extractoras para el procesamiento de palma africana.

Finalmente, denuncian que la Brigada VII del Ejercito, Batallón Joaquín París, permite la presencia paramilitar en la región, que genera control social sobre el campesinado desplazado, las comunidades indígenas, y los mismos trabajadores de la empresa palmera. "En el informe que hemos elaborado existen testimonios, pruebas documentales sobre los distintos tipo de afectaciones", aseguró Peña.

El informe que se presentaría en Colombia en las próximas semanas, busca que se revisen los acuerdos firmados en torno a la garantía a los derechos laborales, ambientales y sociales, que se están violando -aseguran- por parte del Estado y de la empresa palmera.
