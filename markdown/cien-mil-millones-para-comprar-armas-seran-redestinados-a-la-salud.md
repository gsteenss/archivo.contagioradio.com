Title: Cien mil millones para comprar armas serán redestinados a la salud
Date: 2020-04-20 22:23
Author: CtgAdm
Category: Nacional
Slug: cien-mil-millones-para-comprar-armas-seran-redestinados-a-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/salud-menos-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: salud/ archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luego de que la semana pasada en la plenaria del Senado, Iván Cepeda propusiera que todo lo que se destinaría este año para la compra de armas se redestinara a la salud, este Lunes el Ministerio de Defensa anunció que, de ese rubro, cien mil millones de pesos estarán destinados al fortalecimiento del sistema de salud para atender la crisis sanitaria por cuenta del COVID 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El senador Cepeda celebró la decisión del ejecutivo, sin embargo  
la consideró insuficiente, pues según el propio congresista, la destinación  
presupuestal de Min Defensa para la compra de material del guerra para este año  
rondaría el billón de pesos. “Sin embargo, **el senador advirtió que los  
recursos son insuficientes** si se tiene en cuenta que la solicitud en  
principio fue por un billón de pesos y el anuncio del Gobierno Nacional ronda  
los 100.000 millones.” Reza el comunicado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Celebro que una proposición que ha tenido origen en 27 congresistas de la oposición haya sido acogida. Valoramos que dineros que regularmente se invierten en compra de armamentos vayan para la salud de los colombianos y esperamos que en el futuro así siga ocurriendo. Esta es una demostración de que es posible mediante el diálogo lograr acuerdos en un momento de dificultades y de emergencia, cuando lo primero es la defensa de la vida”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### UCI, pruebas de salud para COVID y bioseguridad tendrán la prioridad

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la oficina de prensa de Cepeda los recursos serán usados en la ampliación de Unidades de Cuidados Intensivos (UCI), compra de pruebas para identificar el COVID-19 y de implementos de bioseguridad. Adicionalmente, el Ministerio **se comprometió a incrementar sus esfuerzos en materia de ayudas humanitarias en los territorios más alejados del país,** dadas las capacidades logísticas con las que cuenta uno de los ejércitos más grandes de América Latina. [https://www.ivancepedacastro.com/recursos-de-compra-de-armas-seran-destinados-a-la-salud-](https://www.ivancepedacastro.com/recursos-de-compra-de-armas-seran-destinados-a-la-salud-mindefensa-acogio-proposicion-del-senador-ivan-cepeda/?fbclid=IwAR38ChW0KZH5Rdoh_lCzwTK30rrgyh4cFDaBn9fMPOdWH7p3QSpwSFR4tdo)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La comisión en la que se entregó la propuesta contó con la participación de 27 congresistas y una posterior, que finalmente logró la aceptación estuvo conformada por los senadores Lidio García, Iván Cepeda Castro, Roy Barreras, Antonio Sanguino y Guillermo García.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### FFMM deberían aportar aún más a la paz y la atención de la crisis

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la decisión de Min Defensa es positiva, vale la pena resaltar que diversos sectores sociales del país han exigido el cese de las operaciones de erradicación forzada de cultivos de uso ilícito en regiones en los que las comunidades están adelantando los planes de sustitución previstos en el acuerdo de paz con las FARC, principalmente en las zonas del Catatumbo, Nariño, Cauca y Putumayo en las que se han registrado fuertes protestas contra la acción de las Fuerzas Militares. Lea también <https://archivo.contagioradio.com/campesinos-cumplen-cuarentena-mientras-ejercito-erradica-forzadamente-en-cauca/>

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además, otra de las exigencias tiene que ver con la posibilidad de pactar un cese bilateral de fuego con la guerrilla del ELN para aliviar la situación humanitaria que afrontan poblaciones habitantes de las regiones en las que hacen presencia los frentes de guerra del ELN. Lea Carta [de Comunidades pidiendo Cese al Fuego](https://archivo.contagioradio.com/comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios/)

<!-- /wp:paragraph -->
