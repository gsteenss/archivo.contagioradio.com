Title: Feminicidios en Paraguay continúan en aumento
Date: 2017-01-25 16:20
Category: El mundo, Mujer
Tags: feminicidio, mujeres, paz, Violencia de género, Violencia de género en Colombia
Slug: feminicidio-en-paraguay-continua-en-aumento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Violencias-de-género.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [25 Ene. 2017]

**Durante** **el 2016, nueve de cada diez víctimas de feminicidio fueron asesinadas por su pareja o expareja en Paraguay**, sumando un total de 39 feminicidios, así lo manifestó ONU Mujeres en su reciente informe titulado *Violencia contra las mujeres en Paraguay: avances y desafíos.* La organización aseguró también que cada 9 días una mujer es víctima de violencia.

El estudio da cuenta de cifras alarmantes, por ejemplo dice que entre el 85 y el 95% de las víctimas que se reportan en Paraguay son mujeres, casos en los cuales casi el 80% de la violencia es cometida por sus parejas o exparejas sentimentales. Le puede interesar: [Mujeres rurales trabajan más que los hombres y ganan menos](https://archivo.contagioradio.com/mujeres-rurales-trabajan-mas-que-los-hombres-y-ganan-menos/)

Por su parte, en materia de denuncia, la violencia de género fue el segundo hecho que más se denunció en la Fiscalía de dicho país. Sin embargo, aún hace falta que más mujeres denuncien puesto que 9 de cada 10 mujeres no lo hicieron, ya que consideran que pueden resolver los problemas sin ningún tipo de ayuda.

En materia de violencia sexual el informe deja de manifiesto que **una mujer sufre una violación cada 11 horas en Paraguay, es decir, cerca de 70 mujeres víctimas.**

ONU mujeres ya había denunciado que ante esta problemática, las naciones se encontraban ante **“una violación de derechos humanos de proporciones pandémicas”.**

En otros países como **Colombia**, el informe de Medicina Legal (Forensis 2015), reveló que **970 mujeres fueron asesinadas y durante 2015 se registraron 47 mil 248 casos de violencia de pareja** en el que las mujeres fueron las más afectadas (86,66%). Le puede interesar: [Actualmente no hay ni una condena por feminicidio en Colombia](https://archivo.contagioradio.com/actualmente-no-hay-ni-una-condena-por-feminicidio-en-colombia/)

Mientras que **en Argentina**, el Primer Índice Nacional de Violencia Machista arrojó que **el 99% de las argentinas fueron agredidas por sus parejas y el 97% sufrieron acoso en espacios públicos y privados. **

Varias organizaciones defensoras de los derechos de las mujeres han hecho un llamado a los gobiernos del mundo para que se trabaje en pro del cuidado y el goce efectivo de los derechos de las mujeres. Le puede interesar: [Mujeres del mundo alzaron su voz en contra de Donald Trump](https://archivo.contagioradio.com/mujeres-del-mundo-alzaron-su-voz-en-contra-de-donald-trump/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
