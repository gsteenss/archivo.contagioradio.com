Title: #SiPasaDilo, las mujeres no deben callar más el abuso y el acoso
Date: 2020-11-26 15:18
Author: AdminContagio
Category: Actualidad, Mujer
Tags: acoso sexual, mujeres, Violencia contra las mujeres en Colombia
Slug: sipasadilo-las-mujeres-no-deben-callar-mas-el-abuso-y-el-acoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-25-at-11.39.27-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco del Dia internacional de la no violencia contra la mujer, este lunes 23 de noviembre se realizó el lanzamiento de la campaña [\#SiPasaDilo](#SiPasaDilo), liderada por[la Red Nacional de Mujeres](https://twitter.com/RNMColombia), con el apoyo de ONU Mujeres y USAID. **La campaña tiene como objetivo alertar sobre el aumento de los casos de violencia intrafamiliar y de acoso sexual contra las mujeres durante este año.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Junto con el lanzamiento de la campaña se llevó a cabo el Foro virtual: **‘La justicia en el acoso sexual: del escarnio público, la denuncia legal y otras vías’**, en el que participaron invitadas de otros países, analizando estrategias de denuncia. (Le puede interesar: [\#SiPasaDilo la campaña que busca detener la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/sipasadilo-la-campana-que-busca-detener-la-violencia-contra-las-mujeres-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a una de las formas de denuncia que se está utilizando actualmente, señalan que el escrache (poner en evidencia determinadas violaciones a DD.HH.) se ha volcado hacia las mismas víctimas que al tratar de denunciar, fueron señaladas por no estar respetando el principio de inocencia de quien es denunciado. Además comparten la enorme influencia que han tenido las redes sociales en este tipo de denuncia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

María José Hernández, integrante de la Comisión de Género, añade que un error en otra de las formas de "castigo" contra quien comente el delito, es que no se están penalizando conductas indebidas sino a la persona, mientras lo que se debería hacer es **«educar al colectivo de varones para que reflexionen y modifiquen conductas que a lo mejor también son patriarcales, por lo que individualizar la conducta que es el efecto de una estructura patriarcal**».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, expresa que no es lo que como feministas se debería intentar, sino **«**pensar en una teoría de castigo que sea afín al feminismo y posteriormente, pensar en las formas apropiadas para denunciar casos de violencia y abuso, para que se garantice apropiadamente las condiciones para denunciar, sancionar y reparar».

<!-- /wp:paragraph -->

<!-- wp:heading -->

Estrategias para denunciar
--------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de estas estrategias para denunciar, añade Hernández, es generar una red de contención y acompañamiento, tratando de redireccionar un escrache en una posibilidad de judicializar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra de las estrategias que comparte Mónica Godoy, antropóloga, es el asesoramiento para documentación de casos utilizando los relatos autobiográficos, **«**entendiendo que la percepción de las víctimas de acoso sexual es fundamental para entender este tipo de violencia».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, agrega que no hay una estrategia que no traiga consigo una posibilidad de daño contra las víctimas ya que el contexto no reconoce la dimensión del problema y no tiene protocolos centrados en las víctimas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Papel de las instituciones educativas frente al acoso sexual
------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado, comentan qué deben hacer las instituciones de educación superior frente a los temas de acoso y abuso sexual, y cómo pueden contribuir a que haya una respuesta adecuada y pronta con justicia. (Le puede interesar: [Ante el Tribunal ético serán presentados casos de mujeres panamazónicas y andinas](https://archivo.contagioradio.com/ante-el-tribunal-etico-seran-presentados-casosde-mujeres-panamazonicas-y-andinas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Godoy, indica que es importante generar sistemas de atención **en las universidades donde las estudiantes, trabajadoras y docentes puedan hablar de sus experiencias «con total garantía de que están hablando en un espacio seguro»**, es decir **«**lo que falta en las universidades es entender que no se trata de un tema de investigación penal sino de crear estrategias de atención con un alto grado de humanidad y empatía».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, manifiesta que en las instituciones de educación superior **«tienen que tener equipos de atención primaria muy bien calificados, para que se pueda trabajar desde una perspectiva de tranquilidad y restablecimiento de los derechos».**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Respuestas por parte del Estado
-------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además del rol de las universidades, Hernández comparte que la respuesta por parte del estado ante estas situaciones debe ser la educación sexual integral tanto para hombres como para mujeres y una transformación estructural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el periodismo Matilde Londoño, cofundadora de Volcánicas, afirma que esperan poder darle lugar a las historias de las mujeres para empezar a generar una reflexión colectiva que resulte en cambios culturales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, Mónica Godoy, señala que estos comportamientos de abuso van a cambiar en la medida en que haya una responsabilidad colectiva y que **«**quienes observen estos comportamientos los rechacen**»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el foro completo:

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=qdcI0urV2Ew\u0026amp;feature=youtu.be","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=qdcI0urV2Ew&feature=youtu.be

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
