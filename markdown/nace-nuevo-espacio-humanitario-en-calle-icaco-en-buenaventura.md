Title: Nace nuevo “Espacio Humanitario” en Calle Icaco en Buenaventura
Date: 2015-04-13 16:09
Author: CtgAdm
Category: DDHH, Nacional
Tags: buenaventura, Calle Icaco, Conpaz, Espacio Humanitario, Paramilitarismo
Slug: nace-nuevo-espacio-humanitario-en-calle-icaco-en-buenaventura
Status: published

###### Foto: colectivosurcacarica.wordpress 

<iframe src="http://www.ivoox.com/player_ek_4347705_2_1.html?data=lZihmZyUeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRksLXxpDb18ras4yZppeSmpWJfaS51NXOxc7Tb6npzsbby9nFtsrjhqqfh52UaZq4jMrbjcfFttPd0JC2xcbHs4zZz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Habitante de nuevo espacio humanitario] 

Las 75 familias habitantes de la **Calle Icaco**, en el barrio La Playita en Buenaventura decidieron constituirse como **espacio humanitario**, un año después de que la calle “Puente Nayero” se consolidara también como espacio de vida frente a la **presencia y control paramilitar**. Las 75 familias que habitan este nuevo espacio esperan que la armada y la policía nacional brinden las garantías de seguridad perimetral.

Según una de las habitantes del nuevo Espacio Humanitario, los avances en materia tranquilidad y seguridad para **los y las habitantes de Puente Nayero**, es lo que ha motivado a la gente de las otras calles a desarrollar el ejercicio. “es bonito ver que una vecina puede sentarse al frente de su casa a hablar con las amigas o que los niños y jóvenes puedan salir a jugar futbol con tranquilidad” y explica que los habitantes de todas las calles de La Playita, tenían que salir una sola vez al día a las tiendas, ahora se puede salir cada vez que se necesite.

Los habitantes del nuevo espacio esperan que las autoridades de **policía y de la armada** que hasta el solamente hacen rondas, estén de manera permanente en las entradas de las calles para garantizar la seguridad, puesto que los habitantes están decididos a sacar a los paramilitares que aún habitan en la calle Icaco.
