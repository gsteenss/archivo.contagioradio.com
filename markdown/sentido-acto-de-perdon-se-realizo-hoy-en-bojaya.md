Title: Gobierno también debe asumir su responsabilidad por masacre de Bojayá
Date: 2015-12-06 20:49
Category: Entrevistas, Paz
Tags: Acto de Perdón en Bojayá, Bojaya, Conversacones de paz de la Habana, FARC, Juan Manuel Santos, Masacre de Bojayá, Reconcilaición
Slug: sentido-acto-de-perdon-se-realizo-hoy-en-bojaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/bojaya_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

<iframe src="http://www.ivoox.com/player_ek_9636845_2_1.html?data=mpugmJ2YeY6ZmKiakpiJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bi1c7R0ZDFp9XjjMnSjdXJtsWZpJiSpJjSb9TZjNfSw9HNvoa3lIqvlZDMs9qfxtOYpNTOpdqZpJiSo5aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodrigo Castillo, CONPAZ] 

###### [7 Dic 2015]

Habitantes que regresaron al casco urbano del Bojayá nuevo y que durante todo el día permanecieron en el acto en la zona del Bojayá destruido por la guerra, describieron el acto como sentido y lleno de símbolos, “es un acto de paz” afirmaron luego de concluir hacia las 6 de la tarde la celebración en la que participaron por lo menos 300 personas, la mayoría afrodescendientes y con el acompañamiento de las comunidades indígenas de la región.

El acto que estuvo acompañado por la Guardia Cuimarrona y la Guardia Indígena arrancó con un ritual de armonización indígena en el que las sandalias o zapatos de todos los asistentes fueron llenados con tierra y una semilla de maíz “estamos sembrando la paz” afirmaron y cada quién camina con esa semilla desde la realización del acto. De este evento hicieron parte las Comunidades Construyendo Paz, CONPAZ, la Comisión de Justicia y Paz y la ONU entre otras.

"La guerrilla ha tenido un alto coraje y un actitud de humildad para poder asumir la responsabilidad, este es un hecho histórico que demuestra que las víctimas están en el centro de las discusión", señala Danilo Rueda defensor de derechos humanos de la Comisión de Justicia y Paz, quien junto a Piedad  Córdoba le plantearon a la delegación de paz de las FARC-EP la importancia de profundizar sobre los derechos de las víctimas con actos como este. Rueda agrega que el acto de perdón de esa guerrilla se trata de "una decisión ética muy importante que legitima el derecho a la verdad y a las garantías de no repetición".

<iframe src="http://www.ivoox.com/player_ek_9646565_2_1.html?data=mpuhmJqaeY6ZmKiak5eJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bi1c7R0ZDFp9XjjMnSjdXJtsWZpJiSpJjSb9TZjNfSw9HNvoa3lIqvlZDJsoy20M%2FO24qnd4a1kpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Danilo Rueda, Comisión de Justicia y Paz] 

Luego la obra de teatro en que se representó el pasado, el presente y el futuro de Bojayá y de la paz en Colombia cerró el evento en el que también se presentaron algunos símbolos como la imagen del cristo que ahora se encuentra en un cofre y que simboliza el dolor del momento de los hechos pero también la esperanza que se conserva a pesar de las dificultades.

Este acto de reconciliación surge una vez iniciadas formalmente las conversaciones de paz en Oslo- Noruega, a petición de las propias comunidades y de personas cercanas al proceso como Piedad Córdoba y Danilo Rueda de la Comisión de Justicia y Paz. Tanto para las delegaciones de paz como para las víctimas este acto representa verdadera voluntad de paz y de justicia y marca la pauta para la reconciliación con todas las víctimas.

Los medios de comunicación no fueron invitados, debido a que las víctimas solicitaron un ambiente de plena tranquilidad, respeto y solemnidad frente al acto.

**Las peticiones de las víctimas**

Las víctimas exigen dentro de las peticiones entregadas al gobierno, un acto de perdón por parte del Estado, teniendo en cuenta que actuaron por acción y omisión **"El gobierno debe asumir su responsabilidad de todo lo que ha pasado, se debe restablecer la confianza pueblo gobierno",** sin embargo enfatizaron en que **no les interesa "las solicitudes de perdón de dientes para fuera ni los shows mediáticos", expresó la lideresa Delis Palacios.  **

<iframe src="http://www.ivoox.com/player_ek_9646528_2_1.html?data=mpuhmJqWfI6ZmKiakpeJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezpdqJh5SZopbbxtSPqc2fyNTPy8rWstCfz9TgjdXJqMrmhqigh6aVb9HZ08mSpZiJhpTihpizjdvNp9XdzsbgjcnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Delis Palacio] 

"La justicia incluye verdad y perdón, la comunidad espera que haya una aceptación de responsabilidad de otros actores las circunstancias que permitieron que este hecho atroz se diera, ahí hay otras responsabilidades también y otros también tienen que explicar también su rol", aseguró Fabrizio Hochschild Coordinador Residente y Humanitario de las Naciones Unidas en Colombia.

<iframe src="http://www.ivoox.com/player_ek_9646483_2_1.html?data=mpuhmJmcd46ZmKiakpWJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfxNTa19PNqMLYjMrg0srWpYzl1sqYysbdpYzpz8aYw8jJtNXVxM6SpZiJhpTijMnSjdfJt9Hjz9iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fabrizio Hochschild, ONU] 

Dentro de las peticiones también solicitaron que se establezca en Bellavista un **Centro de Memoria de Víctimas para contar la historia** ocurrida. Así mismo, mediante el comunicado, la delegación de víctimas aseguró que **debe construirse un panteón propio para ubicar los restos** de las 79 víctimas mortales de este hecho.

Por otro lado, piden habilitar permanentemente un centro de atención médica para lesionados sobrevivientes y solicitan **garantías de rehabilitación.**

Así mismo, tras el acto de perdón se exigió al gobierno la conformación de **una comisión de alto nivel** encabezada por el Ministerio del interior, para que **garantice la tranquilidad y seguridad de las comunidades de la zona. **

https://www.youtube.com/watch?v=1jQF7mz3LuI
