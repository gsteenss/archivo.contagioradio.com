Title: En Colombia los niños crecen hablando de muerte: Oxfam
Date: 2020-01-30 18:55
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: conflicto, Informe
Slug: en-colombia-los-ninos-crecen-hablando-de-muerte-oxfam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ninos-en-la-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/carlos-mejia-director-oxfam-ninos-crecen-hablando-de_md_47283735_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Carlos Mejia | Director OXFAM

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves 30 de enero, organizaciones sociales presentaron el informe de seguimiento a la situación de riesgo para niños, niñas y adolescentes (NNA) en cinco departamentos del país donde persiste el conflicto. **El director de Oxfam Colombia Carlos Mejía** (una de las organizaciones que participaron en el informe), señaló que una de las conclusiones a las que llegaron es que los niños en estas regiones "crecen hablando de muerte".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe estuvo a cargo del Observatorio para la Protección de los Derechos y Bienestar de los niños, niñas y adolescentes, compuesto por Oxfam Colombia, la Corporación Jurídica Humanidad Vigente y Benposta Nación de Muchachos. El objetivo es hacer verificación en los departamentos de Norte de Santander, Meta, Guaviare, Valle del Cauca y Nariño sobre las violencias que padecen los y las jóvenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para este análisis tomaron en cuenta tres líneas de investigación: La utilización y reclutamiento de NNA; las distintas violencias que pueden estar asociadas al conflicto; y el seguimiento a su situación en el contexto del pos acuerdo. (Le puede interesar: ["Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños"](https://archivo.contagioradio.com/comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Reclutamiento, "los niños crecen hablando de muerte, de amenazas, de confrontación"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el informe se señala que el reclutamiento sigue siendo una práctica "generalizada y sistemática" a cargo de los actores ilegales que siguen en confrontación, presentándose con mayor persistencia en Buenaventura (Valle del Cauca); Tibú, El Tarra y La Gabarra (Norte de Santander); y en San Andrés de Tumaco (Nariño).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe del Secretario General de las Naciones Unidas presentado en 2019 sobre Niñez y los Conflictos armados, el **ELN (182 casos), las disidencias de las FARC (82) y las autodenominadas Autodefensas Gaitanistas de Colombia (47)** fueron los principales perpetradores de reclutamientos de menores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Mejía, el problema es de difícil resolución porque está asociado a resolver las causas estructurales del conflicto, entre ellas, brindar opciones económicas que permitan una vida digna. "Estos actores les ofrecen cosas que no son reales, pero cuyas alternativas son mejores que otras que están en los territorios", afirmó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como consecuencia, el Director de [Oxfam](https://www.oxfam.org/es/cinco-datos-escandalosos-sobre-la-desigualdad-extrema-global-y-como-combatirla) Colombia aseguró que en lugar de crecer hablando de sus sueños y lo que imaginan, en estas zonas **"los niños crecen hablando de muerte, de amenazas, de confrontación"**. (Le puede interesar: ["Con sus historias niños visibilizan las violencias que amenazan su libre desarrollo"](https://archivo.contagioradio.com/con-sus-historias-ninos-visibilizan-las-violencias-que-amenazan-su-libre-desarrollo/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Violencia sexual y estructural**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las organizaciones señalan que la violencia sexual está directamente relacionada con el conflicto armado y recuerdan el informe Forensis de Medicina Legal para el año 2018, en el que se recuentan **184 casos de violencia sexual.** Respecto a estos, 101 serían responsabilidad de la Fuerza Pública, 55 de grupos de delincuencia organizada y 28 de grupos al margen de la Ley.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de los hechos, las organizaciones alertan por **la posibilidad de que haya un subregistro de los casos por el temor a denunciar los hechos,** dada la presencia constante de los grupos armados en las regiones, "y la falta de confianza en las instituciones públicas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otros hechos relacionados con el conflicto que afectan particularmente a NNA son las afectaciones a su derecho a la educación y el homicidio. En cuanto a la educación, en el informe se sostiene que "**las amenazas a docentes y la aparición de panfletos que obligaban a suspender clases, dejaron a cerca de 1.110 niños, niñas y adolescentes sin estudio".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, según información obtenida de la Defensoría del Pueblo y del mismo Observatorio, en 4 de los 5 departamentos estudiados se registraron homicidios a NNA. En norte de Santander se presentaron 3 casos, en Valle del Cauca 2 y en Nariño otros 2.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La restitución de derechos a niños, niñas y adolescentes que participaron en la guerra**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Observatorio hizo seguimiento del programa "Un Camino Diferencial de Vida" para menores de 18 años desvinculados de las FARC en el que referencian que de los **124 adolescentes liberados de este grupo:** "116 habían cumplido 18 años al final de febrero de 2019, 104 habían recibido una asignación única de normalización, 99 estaban recibiendo la renta básica mensual en el marco del programa de reincorporación y 83 se habían beneficiado de reparaciones como víctimas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, el Observatorio también registró el asesinato de **un jóven beneficiaron del programa y la denuncia de amenazas contra siete menores** más. (Le puede interesar: ["Exintegrantes de actores armados piden perdón a menores víctimas del conflicto"](https://archivo.contagioradio.com/exintegrantes-de-actores-armados-piden-perdon-a-menores-victimas-del-conflicto/)).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las recomendaciones del Informe**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La primera recomendación del Informe es que se garantice y forme en el reconocimiento de los NNA como sujetos de derechos. Así mismo Carlos Mejía resaltó como importante la implementación del Acuerdo de Paz, como medio para resolver los problemas estructurales que enfrentan los menores en el contexto rural y urbano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También le recomendamos leer:[En el Bajo Cauca persiste el reclutamiento forzado de niños](https://archivo.contagioradio.com/en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
