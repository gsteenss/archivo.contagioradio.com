Title: Desalojarán familia que lleva 45 años viviendo en el Chorro de Quevedo
Date: 2016-06-30 12:11
Category: DDHH, Nacional
Tags: administración Enrique Peñalosa, alcaldía de La Candelaria, chorro de quevedo, desalojo viviendas en Bogotá
Slug: desalojaran-familia-que-lleva-45-anos-viviendo-en-el-chorro-de-quevedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Chorro-de-Quevedo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fetzeweerstra] 

######  

###### [30 Junio 2016]

Hace dos semanas, los siete integrantes de la familia Salazar Clavijo, entre ellos una mujer de 83 años, fueron notificados por la Alcaldía de La Candelaria que serían **desalojados de la vivienda en la que han vivido por más de 40 años** y en la que tienen el restaurante que les garantiza su subsistencia.

"Nosotros estábamos durmiendo y llegó el alcalde de la localidad con los funcionarios y la Policía, todo fue una sorpresa desagradable, llegaron y dijeron vamos a hacer el desalojo y al ver a mi abuela de más de 80 años \[se retractaron\] y **nos dijeron que teníamos que irnos en 3 meses**" asegura Octavio Salazar, uno de los integrantes de la familia.

Según afirma Salazar, el padre de la familia, en medio del desconcierto, accedió a firmar un documento en el que aceptan que el desalojo se haga en septiembre de este año, oficio que fue entregado por el alcalde local **bajo la amenaza de que sí no lo firmaban se procedería de inmediato.**

La vivienda ubicada en plena Plaza del Chorro de Quevedo, fue construida por encargo del distrito en la década del 70, por un amigo de la familia quien asignó al abuelo de Octavio hacer las puertas y ventanas y poner los pisos. **Tiempo después el señor invitó a la familia Salazar Clavijo a cuidar la casa y vivir en ella**.

"Nunca hemos discutido la razón que da la alcaldía para el desalojo, porque la casa no es nuestra, lo que nosotros estamos discutiendo es que **la alcaldía prácticamente pasó por encima de nosotros, no nos da ninguna garantía**, ni nos ha dicho cómo nos va a reubicar, ni para dónde se van a llevar a mi abuela porque ninguno de nosotros tiene la capacidad económica de comprar una casa para ella", asevera Octavio.

La falta de garantías para la reubicación de esta familia, no sólo afecta la supervivencia de la mujer de 83 años, también la de los seis miembros más, quienes pueden pagar servicios e impuestos y cubrir sus estudios y necesidades con los ingresos del restaurante y la venta de artesanías que tienen actualmente en la vivienda. Por lo que exigen a la administración capitalina les den **por lo menos un año para abandonar la casa y brinden alternativas económicas para su subsistencia**.

<iframe src="http://co.ivoox.com/es/player_ej_12079768_2_1.html?data=kpedmZ6bepmhhpywj5aXaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncbDX1cbjy9SPl8Lgwt_O1JCRb6Laxsjhw8nTcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
