Title: Otra Mirada: Legalización de drogas, es hora de hablarlo
Date: 2020-08-03 14:45
Author: PracticasCR
Category: Nacional, Otra Mirada
Tags: Drogas, Legalización de la marihuana
Slug: otra-mirada-legalizacion-de-drogas-es-hora-de-hablarlo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/2943408701_ae1ba4c356_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: aNdrzej cH -Flickr

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recientemente se anunció que el proyecto para legalizar la marihuana con fines recreativos va en camino, además el Consejo de Estado dejó sin efecto el decreto 1844 que ordenaba a la policía decomisar la dosis mínima a usuarios en espacios públicos, y solo podrá ser decomisada cuando se verifique con qué fin la carga el portador.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a estos antecedentes, se ha abierto el debate si finalmente se logrará legalizar las drogas en Colombia y qué podría traer para todo el país esta nueva decisión. (Le puede interesar: [Matrícula Cero: la clave para garantizar la educación pública superior](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desarrollando este tema estuvieron Juan Camilo Carvajal, licenciado en biología e investigador en educación y consumo de sustancias psicoactivas de Échele Cabeza, Ricardo Niño Izquierdo, ecólogo con una maestría en desarrollo rural y coordinador de la Comisión Nacional de Territorios Indígenas, Gloria Miranda magíster en construcción de paz y asesora legislativa, y Cam López, investigador de Temblores ONG.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados describen cuáles podrían ser las ventajas de la legalización tanto para los ya consumidores y quienes podrían llegar a serlo, como para el país y su lucha constante contra el narcotráfico.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También respondieron qué se tuvo en cuenta al realizar el proyecto y cuál es la posibilidad de que este proyecto sea apoyado en el Congreso. Además, comparten cuál es el trabajo que se ha venido realizando desde las organizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente comentan qué tan exitoso ha sido en otros países la legalización y que podríamos aprender de esas experiencias para aplicarlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ricardo Niño también explica cómo han sido afectados los pueblos indígenas y cómo se ha tergiversado el tema de la coca y su uso en las comunidades indígenas del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Finalmente, comparten su perspectiva en cuanto al sistema de salud y qué tan preparado podría estar para afrontar este tema. (Si desea escuchar el programa del 29 de julio: [Otra Mirada: Ecopetrol no se vende](https://www.facebook.com/contagioradio/videos/675277499723741))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Para escuchar el análisis completo [haga click aquí](https://www.facebook.com/contagioradio/videos/3713937161952931)**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

<!-- /wp:paragraph -->
