Title: "No puede ser que defender el agua nos cueste la vida" CORDATEC
Date: 2017-07-24 13:47
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, CORDATEC, Derechos Humanos, fracking, Mineria
Slug: no-puede-ser-que-defender-el-agua-nos-cueste-la-vida-cordatec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/San-Martin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [24 Jul 2017]

Integrantes de la Corporación Defensora del Agua, Territorio y Ecosistemas Cordatec de San Martin en el Cesar, han venido denunciando, desde el año pasado, **las constantes amenazas de las que son víctimas**. Ellos han manifestado que, por razón de su labor de protección del territorio ante los efectos de la locomotora minero energética, su vida ha estado en riesgo y no tienen ningún tipo de protección.

El pasado 20 de julio el líder comunal **Crisótomo Mansilla fue víctima de un ataque con arma de fuego**. Según Carlos Andrés Santiago, miembro de Cordatec, este hecho sucedió en razón de la oposición del señor Mansilla a los proceso de minería que se llevan a cabo en San Martín. (Le puede interesar: "[Crisótomo Mansilla, se recupera de ataque con arma de fuego en Aguachica, Cesar"](https://archivo.contagioradio.com/lider-ambiental-se-recupera-de-ataque-con-arma-de-fuego-en-aguachica-cesar/))

Según Santiago, **las amenazas que han recibido "no son aisladas y son sistemáticas".** Agregó que “a finales de mayo 2 presidentes de juntas comunales ya habían sido amenazados y en septiembre del año anterior puse una demanda por una amenaza contra mi vida”. De igual manera, hizo énfasis en que estos hechos son producto de “una campaña de estigmatización que ha venido haciendo la multinacional Conoco Phillips quienes han dicho que tenemos intereses políticos ajenos a la defensa del ambiente”.

**Cordatec está priorizada como organización de riesgo por la Defensoría del Pueblo**

Carlos Santiago fue enfático en manifestar que la Corporación está priorizada como organización de riesgo por la Defensoría del Pueblo y **"hay alertas tempranas por parte del Ministerio del Interior"**. Además afirmó que "los informes de la Defensoría señalan que Cordatec, por razón de su trabajo, tiene unos niveles altos de riesgo sobre sus dignatarios y aún así se siguen presentando estos hechos".** **

Santiago manifestó que es posible que haya **intereses políticos y económicos por parte del sector extractivo** para que los proyectos mineros y petroleros se ejecuten en el territorio de San Martin. “Ayer Ecopetrol dijo que el fracking en Colombia es el proyecto más ambicioso del país y en San Martín se está decidiendo sobre el futuro de este proyecto”. De igual forma dijo que “creemos que hay intereses para que no haya oposición a estas acciones y se están violando los derechos humanos”. (Le puede interesar "[Conoco Phillips no cumple requisitos de licencia ambiental para fracking en San Martín"](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/))

Finalmente ellos han hecho un llamado para que las autoridades avancen en las investigaciones sobre las amenazas que han recibido y que les **den garantías para poder ejercer su derecho a la protesta**. Santiago fue enfático en manifestar que “no es posible que defender el agua termine costándonos la vida a quienes nos oponemos a estos proyectos extractivos”.

<iframe id="audio_19968272" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19968272_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
