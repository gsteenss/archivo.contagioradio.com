Title: En el Bajo Cauca se registra masacre de 6 personas
Date: 2020-09-08 11:35
Author: AdminContagio
Category: Actualidad, Comunidad
Slug: se-registra-masacre-de-6-personas-en-en-el-bajo-cauca-antioqueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-5.37.21-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este lunes 7 de septiembre, en horas de la noche en el corregimiento de El Pato, municipio de Zaragoza, en el **Bajo Cauca, Antioqueño; la comunidad denunció el asesinato de seis personas.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlirioUribeMuoz/status/1303105000772640770","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlirioUribeMuoz/status/1303105000772640770

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la información revelada por medios locales, el hecho se registró en la vereda La Valentina, lugar al que lle**garon hombres armados y usando prendas con estampado militar, ingresaron a un billar** ubicado en el casco urbano de este municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí amarraron y asesinaron con arma de fuego a **cinco personas; pese a que aún no se conoce su identidad, se confirmó que 4 de las víctimas se desempeñaban como mineros, el otro hombre que perdió la vida, era el propietario** del establecimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo las denuncias apuntan a que este hecho es responsabilidad de integrantes del [Clan del Golfo](http://justiciaypazcolombia.com/ana-maria-cortes/), sin embargo el equipo de investigación de Indepaz señala que en el Bajo Cauca antioqueño, hay presencia de AGC, ELN, Caparros y los frentes 36 y 18 de las GAOR.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregan que esta presencia de grupos armados convierte a **[Antioquia](https://archivo.contagioradio.com/el-complot-de-los-que-perdieron-con-daniel-quintero/), en el departamento en donde se han cometido más masajes, con un total de 13** en lo que corrido del 2020, siendo así esté el lugar del país con más hechos de violencia hacia la comunidad y los liderazgos sociales.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89494,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-5.37.21-PM-1024x639.jpeg){.wp-image-89494}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

A ese hecho de violencia ocurrido en el Bajo Cauca, se suma también el asesinato de dos policías luego de un ataque armado en la vía Caucasia - Zaragoza, el pasado 2 de septiembre y el desplazamiento de 600 personas desde el corregimiento de Guarumo debido a las contantes amenazas de las AGC.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
