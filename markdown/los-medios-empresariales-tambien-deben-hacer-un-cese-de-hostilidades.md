Title: Los medios empresariales también deben hacer un cese de hostilidades
Date: 2016-01-25 18:24
Category: Otra Mirada
Tags: medios alternativos, medios de comunicación
Slug: los-medios-empresariales-tambien-deben-hacer-un-cese-de-hostilidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/medios-de-comunicacion-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Semanario Voz 

###### [25 Ene 2016] 

#### **Editorial Contagio Radio**

El pasado jueves 21 de Enero, la víctima fue la agencia de comunicación Prensa Rural, que recientemente publicó el reportaje “un fin de año con las FARC”. Caracol Radio del grupo empresarial Prisa, no dudó ni un segundo, después de citar el nombre de la agencia, en calificarla como “independiente” y también afín a la guerrilla de las FARC. No hubo, ni siquiera, disculpa por parte Caracol Radio.

El 6 de Diciembre del 2015 nuestra emisora fue víctima de los medios masivos de comunicación, en concreto Blu Radio del grupo empresarial Santo Domingo, citó un aparte de una entrevista nuestra a un integrante de la delegación de paz de las FARC con quien conversamos sobre los “Terrepaz” y sin citar la fuente se dispuso a señalarnos como “una emisora afín a las FARC”. La disculpa de su jefe de noticias, Ricardo Ospina, fue que había sido “un error del editor de turno”.

A finales del 2014, el 4 de Diciembre, el grupo paramilitar “Aguilas Negras” amenazó de muerte a 13 periodistas y a 14 medios de comunicación. Salmón Urbano, Reporte Colombia, El turbión, El Rebelde Medios, Colectivo Brecha, La tribu, Colombia Informa, El Macarenazo, Dejando Huella, Indivisibles,  Kino Rama, Galería de la Memoria  y Trochando Sin Fronteras. Según el texto de la amenaza los medios mencionados son “guiados por la FARC ELENO Política” expresando una forma de pensar y que estigmatiza a quienes ejercen el derecho a comunicar y a quienes se informan a través de esas otras formas.

Estos hechos sucedieron en el pasado y esperamos que se queden ahí, este tipo de estigmatizaciones, fueron seguidas de amenazas y ejecuciones de periodistas, defensores de DDHH, políticos de oposición, entre otros. Lo más preocupante es que en medio de unas conversaciones de paz que van muy avanzadas con las FARC y con el ELN y en la lógica de la verdad y la reconciliación se sigan dando este tipo de prácticas inmunes a la inconformidad ciudadana e impunes ante los organismos de justicia.

Así las cosas ¿Cómo corregir este tipo de prácticas?, ¿Serán los medios empresariales un obstáculo para la paz?, ¿Están dispuestos los dueños de esos medios a apostarle a la paz y al cese de hostilidades con el cumplimiento de criterios éticos básicos en la difusión de la información?, ¿Están dispuestos a reconocer su responsabilidad en la guerra por lo que dicen, han dicho y por lo que callan?, ¿Es posible la comunicación para la paz cuando dos o tres grupos ostentan el oligopolio mediático?, ¿Es posible la democratización de la pauta oficial para dar posibilidades de supervivencia a otros medios?, ¿O la pauta oficial solo sirve para silenciar detractores e impulsar temas de conveniencia política?.

Lo cierto es que el cese de hostilidades incluye a esos medios oligopólicos y está en nuestras manos presionar para que se asuman las responsabilidades y se apliquen los cambios que un post acuerdo requiere, eso si se quiere hablar de una democracia verdade
