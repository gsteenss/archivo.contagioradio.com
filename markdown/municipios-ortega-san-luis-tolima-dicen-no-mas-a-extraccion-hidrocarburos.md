Title: Los municipios de Ortega y San Luis en Tolima dicen no más a la extracción de hidrocarburos
Date: 2019-07-18 20:27
Author: CtgAdm
Category: Ambiente, Nacional
Tags: hidrocarburos, Hocol SA, Pijaos, Tolima
Slug: municipios-ortega-san-luis-tolima-dicen-no-mas-a-extraccion-hidrocarburos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/La-Lizama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Costa Noticias] 

La comunidad Pijao, que habita los municipios de Ortega y San Luis, Tolima, está exigiéndole a la empresa extractora de hidrocarburos Hocol S.A  que se vaya de sus territorios debido a las múltiples afectaciones que esa actividad ha dejado en fauna y flora.

Según David Alirio Uribe, Abogado de la Corporación Podion la extracción de hidrocarburos no solo ha tenido impactos en el ambiente como el  deterioro de acuíferos, también ha generado afectaciones a la comunidad, pues la contaminación auditiva que produce la empresa ha excedido los niveles de ruido determinados en  la resolución 0627 de 2006.

Alirio Uribe,  asegura que “la extracción de hidrocarburos por más de cinco décadas,  ha sido regulada por un plan ambiental, que consiste en limitar la actividad petrolera y no por la ley actual, es por ello que no es posible conocer con exactitud el impacto en términos medioambientales en la región”. Además este plan es previo a la ley 99 de 1993,que regula las políticas ambientales en el país. (Le puede interesar ["Debe suspenderse todo tipo de actividad relacionada al desarrollo de fracking en el país, no solo en Santander"](https://archivo.contagioradio.com/debe-suspenderse-todo-tipo-de-actividad-relacionada-al-desarrollo-de-fracking-en-el-pais-no-solo-en-santander/))

Según Uribe hay alrededor de 21 pozos petroleros, pero estos no cuentan con su propio plan ambiental, incluso algunos ya están  inactivos. Así mismo, el abogado asegura que tampoco se ha llevado el seguimiento correspondiente a acciones como sellamiento y manejo que evite que se derrame el petróleo para no perjudicar predios aledaños o acuíferos.

### Respuesta de la comunidad 

La comunidad de Pijao es quien se ha manifestado ante la situación que enfrenta la región, pues como expone  Uribe, se creó una minga de resistencia comunitaria en el 2015. Entre las peticiones de los pijaos se encuentran las exigencias a que la empresa extractora renuncie a continuar con la actividad petrolera para que los pueblos que habitan estos territorios puedan volver a sus raíces agrícolas y agropecuarias.

Según el abogado Uribe, entre las medidas que pondrán en marcha se encuentran la petición a  la Agencia Nacional de Licencias Ambientales (ANLA) para que sancione o suspenda a la compañía por las actividades petroleras y que la empresa compense el daño ambiental ha provocado.(Le puede interesar "[Organizaciones ambientalistas preparan movilización contra la minería contaminante y el fracking"](https://archivo.contagioradio.com/organizaciones-ambientalistas-preparan-movilizacion-contra-la-mineria-contaminante-y-el-fracking/))

Los habitantes de estos territorios mencionan que pueden desarrollar su propia economía, planes autónomos y terminar con los impactos colaterales que ha generado la extracción de hidrocarburos sin necesidad de compañías que extraigan sus riquezas naturales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38606619" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38606619_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
