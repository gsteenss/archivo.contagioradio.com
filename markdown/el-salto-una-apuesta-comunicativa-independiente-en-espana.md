Title: "El Salto" una apuesta comunicativa independiente en España
Date: 2016-12-05 17:04
Category: El mundo, Entrevistas
Tags: españa, medios de comunicación
Slug: el-salto-una-apuesta-comunicativa-independiente-en-espana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/marcha-españa-contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hipertextual.com] 

###### [5 Dic 2016] 

El Salto es la nueva propuesta comunicativa que está surgiendo en España y que pretende agrupar a diferentes medios más pequeños que se encontraban realizando proyectos similares. Esto con el fin de crear el **primer medio comunicativo que no pertenezca o se relaciones con grandes empresas o partidos políticos** y que tenga mayor incidencia frente a los grandes medios de información

De acuerdo con Martín Cuña, periodista de Diagonal este medio tendrá tres principios que surgen de un acuerdo entre los demás pequeños medios que lo conforman: el primero de ellos es **lograr independencia económica de grandes empresas**, partidos políticos e instituciones y **financiarse a través de aportes que harían personas asociadas.**

El segundo es que **intentarán ser un medio completamente horizontal**, es decir sin directivas o mandos superiores  y el último es colocar la rigurosidad y calidad periodística ante todo. Cuña afirma que **la finalidad es que estos medios alternativos y pequeños dejen de competir entre ellos y comiencen a cooperar.**

“Nuestra idea es **tener un contacto directo con esa sociedad movilizada, con los movimientos sociales y los colectivos que trabajan para mejorar la sociedad y compartir una línea política** y una agenda para no trabajar de forma aislada” expreso Cuña. El medio de información funcionará vía web, con contenidos audiovisuales, radio y televisión, además podrá tener emisiones locales.

Esta propuesta además es la respuesta a una crisis que estaría sufriendo España frente a sus instituciones, Cuña menciona que se han venido gestando cambios al interior de la sociedad que no son visibles en los medios de comunicación y  que no reflejan el descontento, motivo por el cual se hace necesario que esas inquietudes y reclamos tengan un lugar en donde poder hacerse públicas y conocidas.

<iframe id="audio_14639172" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_14639172_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
