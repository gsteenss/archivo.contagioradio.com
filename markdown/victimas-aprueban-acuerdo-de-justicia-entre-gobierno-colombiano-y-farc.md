Title: Víctimas aprueban acuerdo de justicia entre gobierno colombiano y FARC
Date: 2015-09-30 13:53
Category: Nacional, Paz
Tags: delegación de vícitmas en la Habana, Derechos Humanos, Jineth Bedoya, jurisdicción especial para la paz, Radio derechos Humanos, Restauración a las víctimas, Víctimas en la habana
Slug: victimas-aprueban-acuerdo-de-justicia-entre-gobierno-colombiano-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Pacifista-victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pacifista 

###### [30 sep 2015] 

Esta semana después de que se firmó la Jurisdicción Especial para la Paz (JEP), 43 de las 60 víctimas que viajaron a la Habana se reunieron y destacaron en un comunicado sobre el acuerdo que “*este es un paso decisivo para el reconocimiento y respeto pleno de los derechos humanos y el Derecho Internacional Humanitario en Colombia*”.

En el comunicado indicaron que “*El acuerdo sobre justicia, así como el de creación de la Comisión de Esclarecimiento de la Verdad, la Convivencia y la No Repetición, **abren la oportunidad histórica para que se de reconocimiento pleno de responsabilidades a los actores armados***".

El grupo de víctimas también afirmó que el **enfoque restaurativo del JEP es acorde con lo que ellos quieren**, que más que cárcel “es la reconstrucción de la verdad”. Hicieron hincapié para acelerar la firma del acuerdo final “*con la mayor celeridad se avance en lograr un acuerdo integral, siendo necesario acuerdos prontos en el tema de reparación y garantías de no repetición*” asegura el comunicado.

Este comunicado lo firmaron 43 de las 60 víctimas que viajaron a la Habana, debido a que algunos se encontraban fuera del país y otros se negaron a hacerlo ya que han recibido constantes amenaza en su contra, sobre esto Jineth Bedoya, miembro de una de las delegaciones de víctimas en la Habana, indicó que “*la responsabilidad del Estado en estos casos, más allá de brindar seguridad a través de la Unidad de Protección, es hacer un acompañamiento integral a lo largo del proceso*”.

**Estas son las cifras de víctimas por el conflicto :**

*Abandono o Despojo Forzado de Tierras:** *** **8.432**

*Acto terrorista/Atentados/Combates/ Hostigamientos:* **85.660**

*Amenaza: ***258.331**

*Delitos contra la libertad y la integridad sexual: ***11.037**

*Desaparición forzada:* **158.793**

*Desplazamiento:* **6.414.700**

*Homicidio: * **953.730**

*Minas antipersonal/Munición sin explotar/Artefacto explosivo:* **13.305**

*Perdida de Bienes Muebles o Inmuebles*: **96.627**

*Secuestro:* **40.577**

*Sin información: ***40**

Tortura: **9.617**

Vinculación de Niños Niñas y Adolescentes: **7.738**

En total, según datos de la Unidad Nacional de víctimas **en Colombia se registran 7.383.997 personas víctimas del conflicto.**
