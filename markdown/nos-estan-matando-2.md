Title: “Nos están matando”:  las causas y los rostros de los líderes sociales colombianos
Date: 2018-08-13 15:06
Category: eventos
Tags: conflicto, Festival de cine por los DDHH, lideres sociales
Slug: nos-estan-matando-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/nos-estan-matando.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 13 Ago, 2018 

Ante la grave situación de violencia que padecen los líderes sociales y comunitarios del país, en el marco de 5to Festival de Cine por los Derechos Humanos se realizará este lunes 13 de agosto el conversatorio “**Nos están matando”: las causas y los rostros de los líderes sociales colombianos**.

Tan solo en el año 2017 se registraron **más de 500 líderes que fueron víctimas de agresiones en todo el país**. Este espacio está encaminado no solo a visibilizar la situación de los líderes sino también a escuchar sus causas y motivaciones. Además de **reflexionar acerca de su papel e incidencia** y el motivo por el cual están en constante riesgo.

El conversatorio contará con la participacion de **Luz Marina Bernal**, Integrante fundadora del colectivo Madres de Soacha, activista por la paz en Colombia y defensora de los derechos humanos; **Carlos Guevara**, Coordinador del programa Somos Defensores, propuesta integral para prevenir agresiones y proteger la vida de defensores de derechos humanos y ** Ariel Ávila** de la Fundación paz y reconciliación.

También participarán **Misael Vaquero**, Campesino de Sumapaz, **Beatriz García** Líder del predio Chimborazo, ubicado en el Municipio Orihueca de la zona Magdalena, un corregimiento caliente que vivió la violencia y el despojo paramilitar; **Bladimir Sánchez**, Documentalista, **Eliseo Enciso Quevedo**, Líder del Meta,

En el encuentro que será moderado por el periodista **Gloria Castrillón** Directora editorial de Colombia 2020. Se proyectarán las producciones "Nos están matando" de Emily Wright · Tom Laffay y el Capítulo 4 de la serie documental "Positiva" de Somos defensores dedicado a la región del Sumapaz.

El evento tendrá lugar a partir de las 7 de la noche en la biblioteca del  Gimnasio Moderno de Bogotá,  Cra. 9 N° 74-99 (Le puede interesar: [Nos están matando: el grito de los líderes sociales en Colombia](https://archivo.contagioradio.com/nos-estan-matando/))
