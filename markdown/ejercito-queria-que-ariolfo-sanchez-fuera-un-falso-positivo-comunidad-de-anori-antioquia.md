Title: Ejército quería que Ariolfo Sánchez fuera un "falso positivo": Comunidad de Anori, Antioquia
Date: 2020-05-24 12:19
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Antioquia, falso positivo, PNIS
Slug: ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/FUERO-MILITAR-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El paso 21 de mayo conocimos el asesinato del **campesino Ariolfo Sánchez,** en el municipio de Anori, departamento de Antioquia, luego de ser retenido junto a otros miembros de su comunidad por miembros del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy y a pesar de las claras acusaciones de la comunidad y las organizaciones defensoras que hacen acompañamiento a estas, se dieron a conocer varios vídeos en donde el Ejército custodiaba el cuerpo de Sanchéz, evitando que la comunidad realizara su sepultura.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/i/status/1264301301099311106","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/i/status/1264301301099311106

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Organizaciones como la Comisión Intereclesial de Justicia y Paz, y defensores de Derechos Humanos como Víctor de Currea Lugo, rechazaron este hecho y señalaron que se esta acción del Ejército podría interpretarse como **un falso positivo, en donde la institución haría pasar al campesino por un supuesto guerrillero dado de [baja en combate](https://archivo.contagioradio.com/mario-montoya-jep/).**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DeCurreaLugo/status/1264200616798236674","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DeCurreaLugo/status/1264200616798236674

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### ¿Qué ocurrió en Anori, Antioquia?

<!-- /wp:heading -->

<!-- wp:paragraph -->

En día anteriores campesinos de este territorio habían **denunciado una incursión de cerca de 500 militar del Ejército, quienes tendrían la tarea de ejecutar acciones de erradicación forzosa de cultivos de uso ilícito**, pasando por alto nuevamente el Programa Nacional Integral de Sustitución de Cultivos Ilícitos (Pnis).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estas acciones organizaciones como **Asociación Campesina del Norte de Antioquia, Asociación de Campesinos en Vía de Extinción**, Coordinadora de Cultivadores de Coca Amapola y Marihuana (**Coccam**) , **Coordinación Colombiana Europa- Estados Unidos** y el **Proceso Social de Garantías para Defensores y Defensoras de Derechos Humanos**, presentaron una denuncia pública.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GarantiasPSG/status/1263518619020988416","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GarantiasPSG/status/1263518619020988416

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En la denuncia señalan que el pasado 8 de mayo, las comunidades habían solicitado acciones contundentes ante el incumplimiento del PNIS, una de ellas un diálogo entre civiles y militares, propuesta para el 15 de mayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al contrario de lo esperado el 18 de mayo las comunidades denunciaron la presencia de militares encapuchados movilizándose en las veredas Tacamocho y Tenche Salino.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que posteriormente fue seguida con un operativo de erradicación forzada en la vereda los Tenche , *"desde los helicópteros fueron ametralladas las comunidades que a esa hora se oponían a la erradicación"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al poco tiempo, *"un pequeño grupo de campesinos fueron detenidos arbitrariamente y despojados de su celular por miembros del Ejército Nacional, quienes según la comunidad presente ejecutaron de manera cobarde al campesino Adolfo Sánchez*", señala el comunicado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Aumenta la erradicación forzada y con esta los falsos positivos?

<!-- /wp:heading -->

<!-- wp:paragraph -->

A la denuncia se sumaron diferentes hecho como el del pasado 18 de mayo en el municipio de Cúcuta, en donde la Fuerza Pública disparo contra los campesinos, *"**asesinaron a Digno Emérito Buen Día**, desaparecieron a Didier Agudelo de 38 años en zona rural del municipio de Campamento y posteriormente lo asesinaron"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ultimo agregaron, *" **hechos como éstos demuestren la firme intención del Estado colombiano por asignar un tratamiento militar y de guerra contra las comunidades que exige sus derechos**, además infringe todas las obligaciones constitucionales de garantías de no repetición y reedita los crímenes de estado"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
