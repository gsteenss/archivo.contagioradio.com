Title: México otorga asilo político a Evo Morales
Date: 2019-11-11 18:30
Author: CtgAdm
Category: El mundo, Política
Tags: Andrés Manuel López Obrador, Bolivia, Crisis política, Evo Morales, Latinoamérica, mexico
Slug: mexico-otorga-asilo-politico-a-evo-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Evo-Morales-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Evoespueblo] 

La crisis política en Bolivia se intensificó este domingo desde los sucesos que muchos señalan como un golpe de Estado, cuando el ahora expresidente, **Evo Morales y su vicepresidente, Álvaro García Linera**, se vieron forzados a abdicar a sus cargos tras un pronunciamiento de la Fuerza Pública. Después de una ola de renuncias dentro de su Gobierno y que se emitiera una orden de aprehensión en su contra, el mandatario mexicano **Andrés Manuel López Obrador** ofreció asilo al líder boliviano.

**"La Cancillería mexicana decidió conceder el asilo por razones humanitarias y en virtud de la situación de urgencia que se enfrenta en Bolivia"** manifestó el canciller mexicano Marcelo Ebrard , a diferencia de países como Argentina, Brasil y Chile,  que se habrían negado a que el exmandatario sobrevalara su espacio aéreo.

> Denuncio ante el mundo y pueblo boliviano que un oficial de la policía anunció públicamente que tiene instrucción de ejecutar una orden de aprehensión ilegal en contra de mi persona; asimismo, grupos violentos asaltaron mi domicilio. Los golpistas destruyen el Estado de Derecho.
>
> — Evo Morales Ayma (@evoespueblo) [November 11, 2019](https://twitter.com/evoespueblo/status/1193702186024361985?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Desde que se conoció el pronunciamiento del Ejército de Bolivia solicitando la renuncia de Morales, en referencia a las irregularidades que se habrían registrado en las elecciones presidenciales del pasado 20 de octubre, el gobierno de López Obrador rechazó este pronunciamiento manifestando que “México demanda respeto al orden constitucional en Bolivia”.  [(Le puede interesar: Futuro incierto en Bolivia tras la renuncia de Evo Morales)](https://archivo.contagioradio.com/futuro-incierto-en-bolivia-tras-la-renuncia-de-evo-morales/)

### Bolivia continúa en caos tras la salida de Evo Morales 

Desde la renuncia de Morales a su cargo, las carreteras de acceso al Trópico de Cochabamba, lugar al que manifestó, llegaría el exmandatario, fueron cerradas por cocaleros quienes también habrían instalado una vigilia en el aeropuerto de  Chimoré, donde aterrizó el expresidente junto a Álvaro García Linera, y cerca de ocho colaboradores cercanos  a su Gobierno.

Después del anunció de Morales, los disturbios se extendieron a lo largo de la noche y del día lunes en diferentes ciudades como **La Paz, El Alto y Cochabamba** donde se ha alertado de violaciones de DD.HH contra la población, incendios, saqueos y ataques a viviendas incluidas las de el exmandatario y exfuncionarios de su gabinete. Según medios locales como el periódico boliviano La Razón, hay al menos 20 heridos mientras continúan los enfrentamientos entre la Policía y ciudadanos que exigen que Evo Morales regrese al poder.

Así mismo, se conoció que el Comandante General de la Policía de Bolivia, Vladimir Yuri Calderón,  renunció  a su puesto este lunes ante la presión de un sector amotinado de la Policía, cabe resaltar que el día anterior Calderón pidió la renuncia de Evo Morales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
