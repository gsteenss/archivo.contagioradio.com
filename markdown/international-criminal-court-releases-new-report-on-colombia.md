Title: International Criminal Court releases new report on Colombia
Date: 2019-12-10 17:02
Author: CtgAdm
Category: English
Tags: extrajudicial killings, false positive, International Criminal Court, paramilitary groups, Special Peace Jurisdiction
Slug: international-criminal-court-releases-new-report-on-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cour-Penale-Internacionale-e1575589878337.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Foto: @IntlCrimCourt  
] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Colombia has made significant advances in the investigation of war crimes and crimes against humanity, especially those related to the creation of and support for paramilitary groups, said the International Criminal Court (ICC) in its latest annual report.]

[The ICC has had Colombia under preliminary examination since 2004 to determine whether the Andean country has complied with international law to investigate and try human rights abuses. The 2019 report highlights the work of the Special Peace Jurisdiction (JEP) this year as well as advances made in cases related to extrajudicial killings and crimes committed by paramilitary groups.]

[According to the Court, some 9,713 former FARC members, 2,291 members of the Armed Forces and 63 third parties (or non-state agents) have committed to participate in the transitional justice system. The ICC highlighted that the JEP has decided to prioritize seven macro cases,  acknowledging, in particular, those that investigate crimes committed in the war-torn provinces of Nariño, Urabá and Cauca.]

[The report also applauded the entry of David Char Navas and Álvaro Ashton’s cases into the JEP, which are considered crucial to understanding the relationship between politicians and paramilitary groups during the armed conflict in the northern part of Colombia.]

[The ICC also examined the progress the ordinary justice system has made in trials against paramilitary groups and their supporters. The Court noted that the Attorney General’s Office is investigating 1,253 cases against civilians or businesspeople and 794 against state agents for crimes related to the development, support or financing of these groups as of October.]

The ordinary justice system took important steps in judicial processes against executives and employees of the multinational Chiquita Brands, charged in 2018 of the crime of conspiracy. They are suspected of financing the Arlex Hurtado Front. The Court referenced the cases related to the Calima Bloque, which have resulted in imprisonment without bail for two people and an inquiry for another three.

### More than 10,000 people have been investigated for extrajudicial killings

The ordinary justice system has started investigations in five potential cases on extrajudicial killings against the First, Second, Fourth, Fifth and Seventh Divisions of the Army. Despite reports that claim there could be more than 10,000 victims of these crimes, the Attorney General's Office only reported 2,268 active cases that involve 3,876 victims.

According to the ICC, 10,742 people have been investigated for these crimes. Meanwhile, only 1,749 have been convicted. Additionally, a total of 20 generals, 182 cornels and 143 commanders are currently under investigation.

Despite these advances, the Court said it would continue its evaluations of these judicial processes in the ordinary justice system, the Special Peace Jurisdiction and the jurisdiction of the Justice and Peace Law. The Court added that their preliminary evaluation of Colombia will end in 2020.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
