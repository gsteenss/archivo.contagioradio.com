Title: Fracking amenaza el abastecimiento de agua de Bogotá
Date: 2017-03-22 12:57
Category: Ambiente, Voces de la Tierra
Tags: Bogotá, Chingaza, fracking, Páramo de Sumapaz
Slug: fracking-amenaza-abastecimiento-agua-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Paramo_de_sumapaz_Bogota_D._C-e1490204448220.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AJMELOV 

###### [22 Mar 2017]

[Mientras la Agencia Nacional de Licencias Ambientales, ANLA, continúa negando que que existen 43 bloques petroleros destinados para el fracking en Colombia, la Agencia Nacional de Hidrocarburos, ANH, reconoce que hay dos nuevos contratos para el fracturamiento hidráulico en trámite, como lo denuncia Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC. Según el mapa de la ANH, se trata de 7 bloques ubicados **a los alrededores de Bogotá, puntualmente, cerca al Parque Natural Chingaza y al Páramo de Sumapaz**, que pondrían en riesgo el abastecimiento de agua de los capitalinos.]

[En el mapa también se muestra que existe un bloque que se sobrepone a la zona urbana del **municipio de Fusagasugá y otro más que se ubica en las jurisdicciones de Chía y Cajicá**. Al parecer también habría algún bloque más cerca de Guasca donde las comunidades de la zona vienen denunciado la realización de trabajos exploratorios de hidrocarburos para No Convencionales sin los debidos permisos.]

[La situación es alarmante, debido a que si se practica esa polémica técnica se pone en grave peligro el Páramo de Sumapáz, el más grande del mundo, una de las fuentes hídricas más importantes del país y la despensa agrícola de la capital. A su vez**, la amenaza sobre el Páramo de Chingaza pone en riesgo el 80% del agua potable de Bogotá.**]

![ANH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/YNC_ParamoBOG0317_2.png){.wp-image-38065 .aligncenter width="565" height="799"}

[“Basta con solo darle un vistazo al mapa para evidenciar como Bogotá, los municipios cercanos y sus principales fábricas de agua, están rodeados de bloques petroleros, la gran mayoría de ellos destinados para No Convencionales o Fracking”, afirma CORDATEC.]

[Ante esta nueva alarma,  desde la Alianza Colombia Libre de Fracking, que une a cerca de **40 organizaciones defensores del ambiente,** radican, este miércoles una carta dirigida al presidente Juan Manuel Santos en la que solicitan la declaración de la moratoria al fracking en Colombia, con perspectiva hacia la prohibición.]

[2017.03.14 Carta Presidente Santos\_FINALconLogos](https://www.scribd.com/document/342727327/2017-03-14-Carta-Presidente-Santos-FINALconLogos#from_embed "View 2017.03.14 Carta Presidente Santos_FINALconLogos on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_42488" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342727327/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-w82cow66lFZ6xP0Hd7fY&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
