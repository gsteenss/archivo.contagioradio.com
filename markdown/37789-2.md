Title: Venezuela rechazó informe de la OEA
Date: 2017-03-15 15:59
Category: DDHH, El mundo
Tags: Luis Almagro, Nicolas Maduro, OEA, Venezuela
Slug: 37789-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/flag-protest-crop.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Mar. 2017] 

Luego de conocerse el reciente informe presentado por **Luis Almagro, Secretario General de la Organización de Estados Americanos (OEA)**, en el que plantea suspender a Venezuela del organismo internacional de no realizar elecciones generales lo más pronto y en el que pide se tenga un nuevo Consejo Nacional Electoral y un nuevo tribunal supremo de Justicia, **la Cancillería de ese país calificó como repudiable ese discurso que dijo ser "intervencionista".**

**Según Jessica Dos Santos periodista de la Radio del Sur**, Almagro hace estas solicitudes "que han sido consideradas como extralimitadas y fuera de sus competencias como Secretario General de la OEA".

Ante estos hechos Venezuela ha asegurado que **se trata nuevamente de “agredir a Venezuela, está vez por intermedio de la figura de Luis Almagro**, buscando romper el orden democrático. Incluso hemos considerado a Almagro como enemigo del pueblo venezolano” relata Dos Santos. Le puede interesar: [La OEA, la intervención y Venezuela](https://archivo.contagioradio.com/la-oea-la-intervencion-y-venezuela/).

Hasta el momento, se han sumado a este repudio varios movimientos sociales que componen la Fundación Latinoamericana por los Derechos Humanos y el Desarrollo Social – **FUNDALATIN, quienes se manifestaron en contra de estas acciones “injerencistas por parte de Almagro”.**

“Incluso a través de una misiva señalan que Luis Almagro se está volviendo una ficha obsesiva de Washington y que, desde allí, desde Estados Unidos, están saliendo también todas estas instrucciones de **invocar contra Venezuela la carta democrática interamericana.** Cerca de 225 organizaciones suscribieron este documento” contó Dos Santos.

Para Dos Santos todas estas situaciones son “otros intentos de mal posicionar a Venezuela y de preparar el ambiente para que ocurran incluso cosas peores en contra de nuestro país, que de por sí ya está atravesando momentos complejos en el panorama económico”. Le puede interesar: ["Toma de Caracas" sería parte de un plan de desestabilización en Venezuela](https://archivo.contagioradio.com/vicepresindete-de-venezuelaa-alerta-plan-desestabilizacion-en-marcha-toma-de-caracas/)

Venezuela ya manifestó en su comunicado “que **tomarán las medidas correspondientes en el momento que deba ser y que denuncia ante el mundo** entero la intención agresiva de este triste malhechor contra nuestra indeclinable decisión de seguir viviendo en paz, con independencia y soberanía, como el derecho de todos los pueblos del orbe”.

De igual modo, para la cancillería Luis Almagro encabeza el concierto hemisférico de la derecha fascista que hostiga, agrede y ataca con saña a Venezuela **“la oposición en este momento está apoyando a Almagro y está apoyando todas estas pretensiones que se tienen** en contra de Venezuela, por eso también hay preocupación por el alcance posterior que podría haber” añade Dos Santos.

Las elecciones serán llevadas a cabo en diciembre de 2018 para presidente y para los gobernadores, que debían hacerse a final de 2016  se aplazaron para 2017 sin que se conozca una fecha definitiva hasta el momento.

### Comunicado de la Cancillería de Venezuela 

 

[Comunicado Cancillería de Venezuela](https://www.scribd.com/document/342006914/Comunicado-Cancilleria-de-Venezuela#from_embed "View Comunicado Cancillería de Venezuela on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_1281" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342006914/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-d42y6m5WzAjBCkmz6JxY&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###  Comunicado FUNDALATIN

[Comunicado FUNDALATIN](https://www.scribd.com/document/342007165/Comunicado-FUNDALATIN#from_embed "View Comunicado FUNDALATIN on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17569624" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17569624_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_46505" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342007165/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ojfuTmt2os2lsIHqMLep&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
