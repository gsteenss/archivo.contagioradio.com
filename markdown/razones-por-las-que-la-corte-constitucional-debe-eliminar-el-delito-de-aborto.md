Title: Razones por las que la Corte Constitucional debe eliminar el delito de aborto
Date: 2020-09-16 19:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Aborto legal, Causa Justa, Corte Constitucional, Despenalización del aborto
Slug: razones-por-las-que-la-corte-constitucional-debe-eliminar-el-delito-de-aborto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Causa-Justa-pide-eliminar-el-delito-de-aborto.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-16-at-7.23.57-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El movimiento Causa Justa que agrupa a 91 organizaciones y 134 activistas, presentó este miércoles, ante la Corte Constitucional, una demanda de inconstitucionalidad que **busca suprimir el artículo [122](http://www.secretariasenado.gov.co/senado/basedoc/ley_0599_2000_pr004.html#122) del Código Penal, el cual establece el aborto como delito en Colombia, por considerar que este es violatorio de derechos fundamentales de las mujeres como la libertad individual, la salud, la igualdad, la autonomía reproductiva, entre otros.** (Le puede interesar: [IVE: el derecho a decidir en paz](https://archivo.contagioradio.com/ive-el-derecho-a-decidir-en-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los demandantes argumentan que en el país, se realizan 400.412 abortos por año de los cuales solo entre un 1% y un 9% se realiza de forma legal, lo cual pone en riesgo la salud e integridad de las mujeres que se someten a estos procedimientos. **Actualmente el aborto es la cuarta causa de mortalidad materna en Colombia**, donde se estima que mueren 70 mujeres anualmente y cerca de 132.000 sufren complicaciones por esta razón.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«*Las cifras son inaceptables si se tiene en cuenta que los riesgos del aborto realizado en condiciones seguras son mínimos, por lo tanto, se trata de muertes y complicaciones evitables*»**
>
> <cite>Afirman las accionantes</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Adicionalmente, señalan que aproximadamente el 32% de los abortos clandestinos traen consigo complicaciones que, para el año 2012, le costaron al Sistema de Salud cerca de 40.000 millones de pesos**, por lo que, son cifras que podrían ahorrarse con la legalización y una debida reglamentación del aborto que permita acceder a procedimientos seguros. (Le puede interesar: [La lucha feminista sigue por la despenalización del aborto en Colombia](https://archivo.contagioradio.com/la-lucha-feminista-sigue-por-la-despenalizacion-del-aborto-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sietepolas/status/1306259357248745478","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sietepolas/status/1306259357248745478

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Los demandantes recurren a la Corte Constitucional, justificándose en un actuar omisivo por parte del Congreso de la República, órgano que se ha abstenido de reglamentar o dar impulso legislativo a iniciativas relacionas con el aborto.** Según datos de Causa Justa, en el período comprendido entre 2006 y 2017, el Congreso discutió 37 proyectos de ley referidos al aborto o la autonomía reproductiva: 51% de ellos en el primer período, 35% en el segundo y 14% en el tercero.; lo que según el Movimiento, «*refleja un declive importante del interés de los congresistas por legislar sobre la materia*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La diferencia entre despenalizar y legalizar el aborto

<!-- /wp:heading -->

<!-- wp:image {"id":90032,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-16-at-7.23.57-PM-1024x768.jpeg){.wp-image-90032}  

<figcaption>
Foto: Causa Justa

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Causa Justa destaca que el pronunciamiento de la Corte Constitucional en 2006, definiendo tres causales que despenalizaron el aborto, representó un avance importante para las mujeres y niñas; sin embargo, luego de 14 años, según el Movimiento, **«*se siguen evidenciando grandes obstáculos para el ejercicio pleno de la autonomía reproductiva,*** *los cuales están causados, en una importante medida, por el hecho de que el aborto, fuera de esas tres excepciones, sigue considerándose un delito y operando como una barrera estructural para el acceso al aborto seguro*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El aborto practicado fuera de esas tres causales, constituye un delito penado con entre 1 y 4 años de prisión,** tanto para la mujer gestante como para la persona que se lo induzca. Según Causa Justa, el hecho de que el aborto apenas haya sido despenalizado en esas tres particulares circunstancias, no ha permitido que el Estado lo reglamente y adopte políticas públicas en materia de salud para garantizar su acceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso, se destaca que acceder a un aborto legal y seguro, es en muchos casos un privilegio de clase y un indicador de la desigualdad social y el centralismo que caracteriza a Colombia. **Por ejemplo, el 99,2% de los abortos reportados por las Secretarías de Salud entre 2015 y 2017 ocurrieron en Bogotá.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":30075,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Despenalización-aborto.jpg){.wp-image-30075}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Las mujeres que viven en zonas rurales apartadas o en ciudades intermedias tienen mayores dificultades para acceder a servicios adecuados de salud y mucho más a un aborto, algo que tiene una implicación económica, social, cultural y evidentemente legal con su no legalización. **Otro dato que se reveló y que da cuenta de las desigualdades entre áreas urbanas y rurales es que el 97% de las mujeres denunciadas por aborto son habitantes de zonas rurales y solo el 3% pertenece a una zona urbana.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
