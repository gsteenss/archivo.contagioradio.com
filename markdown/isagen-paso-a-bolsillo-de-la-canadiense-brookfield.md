Title: Isagén pasó al bolsillo de la canadiense Brookfield
Date: 2016-01-13 09:16
Category: Economía, Nacional
Tags: ISAGEN
Slug: isagen-paso-a-bolsillo-de-la-canadiense-brookfield
Status: published

La empresa Isagén fue vendida a la multinacional canadiense Brookfield por  6.4 billones que equivale al  57.6 % de la participación, la subasta se realizó en la bolsa de valores de Colombia sin más oferentes. ([Con venta de ISAGEN Estado dejaría de recibir 225 mil millones de pesos anuales](https://archivo.contagioradio.com/con-venta-de-isagen-estado-dejaria-de-recibir-225-mil-millones-de-pesos-anuales/))

[La venta de la empresa se da luego que la Sección Cuarta del Consejo de Estado no atendiera las medidas interpuestas para frenar "la subasta", igualmente no se atendió las 8 tutelas presentadas por el sindicato de la empresa. La oferta se dio en medio de la indignación de hombres y mujeres que a las afueras de la bolsa manifestaban su oposición a la venta de la empresa que por más 20 tuvo como mayor accionista al Estado colombiano.]

<iframe style="overflow-y: hidden;" src="https://magic.piktochart.com/embed/10507844-isagen" width="800" height="1724" frameborder="0" scrolling="no"></iframe>

**Brookfield**, empresa con sede en Canadá y con un activo de más de 225 millones de dólares, maneja temas de infraestructura, sector de bienes raíces y por más de 100 años ha manejado empresas del sector hidroeléctrico en países como Estados Unidos, Brasil, Canada y ahora tendrá el control de la segunda empresa de generación de energía en Colombia, la multinacional ha estado bajo investigación en Brasil por haber pagado sobornos en la solicitud de permisos en el año 2013.

Leer más [Todo está calculado para el “espectáculo bochornoso” de venta de ISAGEN](https://archivo.contagioradio.com/todo-esta-calculado-para-el-espectaculo-bochornoso-de-venta-de-isagen/)

En desarrollo...
