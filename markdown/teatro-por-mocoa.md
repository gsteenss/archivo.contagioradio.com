Title: El Teatro se solidariza con Mocoa
Date: 2017-04-06 15:37
Category: eventos
Tags: Bogotá, Cultura, Mocoa, Seki sano, teatro
Slug: teatro-por-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Samadhi 

###### 06 Abr 2017

Con la [catástrofe ocurrida en Mocoa](https://archivo.contagioradio.com/no-fue-desastre-natural-lo-ocurrido-mocoa/) por cuenta de la avalancha del pasado fin de semana, la **Compañía de Teatro Samadhi**, se une solidariamente a las acciones de ayuda para los habitantes de esa región del sur de Colombia, donando los ingresos que se recojan por una función de su obra **"Besos entre Fusiles"**.

Esta pieza teatral, **dirigida por Eric Bernal García**, cuenta la historia de Ana María, una joven amante de los poemas y su novio Fabián, el hombre que le enseña el mundo a través de los libros. Pero, el amor de estos dos amantes cambia cuando Fabián se marcha a la guerra.

Ana Maria al no querer dejar a su enamorado decide enlistarse en las filas del cuartel como cocinera, situación que la colocara al acecho de los soldados que la ven solo como un objeto sexual y como la asesina de su novio. ¿Hasta que extremos puede llegar el amor para mantenerse vivo? ¿qué tanto son capaces de soportar los amantes?

<iframe src="https://www.youtube.com/embed/VDIbFq0_ZGw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La pieza teatral se estará proyectando **del 6 al 8 de abril, en la sala Seki Sano,** la entrada tiene costo, pero para la función del 6 de abril la compañía Samdahi, junto con la Corporación Colombiana de Teatro han dispuesto que el ingreso sea con aporte voluntario. El dinero que se recaude de la función, será para ayudar a las víctimas y damnificados de la población de Mocoa.

**Funciones** 7:30 de la noche

**Sala Seki Sano,** Calle 12 2-65
