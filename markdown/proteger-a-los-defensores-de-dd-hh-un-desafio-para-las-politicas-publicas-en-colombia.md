Title: Proteger a los Defensores de DD.HH, un desafío para las políticas públicas en Colombia
Date: 2020-12-08 22:00
Author: AdminContagio
Category: yoreporto
Slug: proteger-a-los-defensores-de-dd-hh-un-desafio-para-las-politicas-publicas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Portada-adelante-1-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Portada-adelante-1-2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*Por: Pensamiento y Acción Social*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Colombia es de los países más peligrosos para ejercer el derecho a defender los Derechos Humanos. De acuerdo con [Indepaz](https://archivo.contagioradio.com/la-politica-de-guerra-del-gobierno-reactivo-todas-formas-de-violencia/), entre el 2016 y el 2020 más de 1000 líderes defensores de Derechos Humanos asesinados. En medio de este panorama, parte de la respuesta del gobierno y las instituciones estatales a la crisis ha sido la expedición de leyes, decretos, sentencias y documentos. **Una investigación realizada por la ONG Pensamiento y Acción Social evalúa la implementación de esta estrategia en 3 casos particulares para extraer aprendizajes y proponer alternativas que realmente sirvan para proteger a quienes están en riesgo**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
La investigación evalúa la política pública entendida *“como una estrategia en la cual el gobierno coordina el comportamiento diversos actores a través de un conjunto de acciones que buscan lograr objetivos considerados importantes para hacer frente a situaciones socialmente relevantes”* -Jaime Torres-Melo y Jairo A. Santander-. En el caso de la política pública para la defensa de la vida de las y los defensores de derechos humanos lo que se observa, más que una estrategia unificada, **es una producción de muchas normas dispersas y desarticuladas que no logran eficazmente su objetivo**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Para entender mejor y ahondar en la respuesta estatal se eligieron 3 casos de estudio. **En la primera parte se aborda el caso de la comunidad de Las Pavas, en el Magdalena Medio, que nos enseña que es posible, en su contexto, lograr las condiciones para continuar y sostener la defensa de su territorio** y por ello continúan acumulando importantes triunfos jurídicos a pesar de que les espere aún un tiempo indefinido para recibir sus títulos de propiedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En la segunda parte, se presenta el caso de San José de Uré en el departamento de Córdoba, el cual evidencia la dura realidad de unas medidas protección atrapadas en acciones meramente humanitarias,** hasta tanto el Estado adopte decisiones de mayor calado que pongan fin a una violencia que se eterniza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En la tercera parte, referida al Consejo Comunitario Renacer Negro en Timbiquí, Cauca, se muestra el potencial que para la protección tiene la restitución y formalización de derechos territoriales**, convocando a llenar los vacíos y a profundizar su aplicación. ([Parlamento británico cuestiona el compromiso de Duque y la Fiscalía con la paz](https://archivo.contagioradio.com/parlamento-britanico-cuestiona-el-compromiso-de-duque-y-la-fiscalia-con-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
**La investigación analiza, el contexto existente al momento en que se definió la medida de protección, además, presenta la medida de protección, valora su impacto**, identifica los factores que contribuyen a explicar ese impacto, así como las razones que condujeron a su adopción y, finalmente, expone algunos de los posibles aprendizajes derivados de la experiencia. De esta forma no solo se exponen las falencias de la política pública, sino se presentan, de manera propositiva, alternativas que pueden implementarse a partir de las enseñanzas que dejan esos 3 casos de estudio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
En momentos en los que los ataques contra líderes y lideresas defensoras de derechos humanos parecen no acabar, es necesario detenerse a analizar lo que funciona, lo que no, y lo que debe cambiar para lograr una política pública que verdaderamente defienda la vida. En este sentido e**sta investigación es un insumo importante para repensar la forma en la que se ha venido trabajando la protección de líderes y defensores de derechos hasta el momento.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Para conocer más sobre la investigación, los 3 casos presentados y los análisis que, desde distintos sectores académicos, gubernamentales y de organizaciones de la sociedad civil se están dando frente a la investigación, **pueden conectarse al lanzamiento virtual que se hará el miércoles 9 de diciembre a través del [Facebook LIVE](http://www.facebook.com/pensamientoyaccionsocial/live)de Pensamiento y Acción Social**.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93899,"width":283,"height":366,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Portada-adelante-1-2-791x1024.png){.wp-image-93899 width="283" height="366"}

</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
