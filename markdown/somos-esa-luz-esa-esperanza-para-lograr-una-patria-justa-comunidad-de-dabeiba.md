Title: Somos esa luz, esa esperanza para lograr una patria justa: comunidad de Dabeiba
Date: 2018-11-19 15:59
Author: AdminContagio
Category: DDHH, Sin Olvido
Tags: La Balsita, Reconstrucción de Memoria
Slug: somos-esa-luz-esa-esperanza-para-lograr-una-patria-justa-comunidad-de-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/DsUg0GSWsAAg3J7-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @Justiciaypazcol 

###### 19 Nov 2018 

En medio de un homenaje a la vida y a la construcción de memoria histórica, el pasado 18 de noviembre la Comunidad de Vida y de Trabajo La Balsita conmemoró 21 años de desarraigo de su territorio como consecuencia de la Operación “Septiembre Negro” perpetrada por paramilitares y militares en los departamentos de Chocó y Antioquia en 1996.

Fue durante esta incursión que los pobladores del **Carmen del Daríen, Piedeguita y Mancilla, en el municipio de Río Sucio -Chocó, Larga Tumaradó**, en los municipios de Turbo y Río Sucio y en Dabeiba, Antioquia fueron desplazados de sus territorios después de sufrir graves violaciones a sus derechos.

21 años después reafirmado sus derechos y su dignidad, acompañados por diferentes comunidades de base y numerosas organizaciones defensoras de los derechos humanos, los dabeibanos realizaron un acto de memoria en el monumento Árbol de la vida, un espacio sagrado erigido en honor a las más de 143 personas asesinadas y desaparecidas durante la arremetida paramilitar.

De igual forma, en medio de conversatorios y muestras artísticas se exaltaron los diferentes símbolos que identifican a La Balsita y a sus habitantes quienes también fueron acompañados por grupos base de **Putumayo, Meta, Cauca, Valle y Chocó**, regiones que también han sido golpeadas por la violencia pero que comparten el mismo horizonte de ver un país unido alrededor de la memoria colectiva.

**Henry David Cardona**, parte de la Junta Directiva e integrante de la Comunidad de Vida y de Trabajo La Balsita afirma que su comunidad hace parte de la transformación de un nuevo país, “somos esa luz, esa esperanza y esa huella que dejamos para lograr esa patria justa y que haya más equidad en este país”.

Pese al dolor, ha sido precisamente el ejercicio de no olvido y de honrar a las víctimas y seres queridos lo que les ha impulsado a seguir en pie de lucha, **“nos duele recordar pero más nos duele olvidar y si olvidamos es posible que vivamos la misma historia que vivimos hace 21 años y no queremos vivir la guerra más”** asegura Henry exaltando las propuestas de paz y de convivencia humana que han surgido de su comunidad.

Tras más de dos décadas de resistencia y reconstrucción de memoria, los habitantes de La Balsita aún sueñan con volver a su territorio con la garantía de permanencia en un espacio ancestral heredado por sus matriarcas y patriarcas, “no podemos tirar la toalla, no nos quedamos ahí caídos en medio de la guerra, logramos ser comunidades resilientes” afirma Cardona quien destacó el acompañamiento de las diferentes comunidades del resto del país en este ejercicio colectivo.

<iframe id="audio_30176000" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30176000_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
