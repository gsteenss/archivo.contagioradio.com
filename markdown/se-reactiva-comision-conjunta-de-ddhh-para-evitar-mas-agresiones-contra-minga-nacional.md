Title: Se reactiva Comisión conjunta de DDHH para evitar más agresiones contra Minga Nacional
Date: 2016-06-04 20:11
Category: Paro Nacional
Tags: Comisión de Derechos Humanos, Gobierno de Colombia, Paro Nacional
Slug: se-reactiva-comision-conjunta-de-ddhh-para-evitar-mas-agresiones-contra-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/cumbre-agraria-gobierno-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cumbreagraria] 

###### [4 Jun 2016]

Tras la reunión de la Cumbre Agraria con el gobierno nacional se tuvo un balance positivo por parte de los voceros de las comunidades pues se logró restablecer la comisión conjunta de Derechos Humanos de la que hace parte el Ministerio del Interior y que estaría acompañada por la ONU con el fin de **evitar que se sigan presentando agresiones, asesinatos, detenciones y judicializaciones de los participantes de la protesta.**

A pesar de varios intentos del gobierno nacional y de los gobiernos regionales por establecer diálogos regionales con las comunidades en algunos departamentos del país, los voceros y voceras de la [Cumbre Agraria](https://archivo.contagioradio.com/programas/paro-nacional/) lograron que se realizara una **reunión con el ministro del interior y el ministro de agricultura en la ciudad de Cali** que finalizó hace algunos minutos.

A esta reunión también asistieron como garantes **Iván Cepeda, Alberto Castilla y Ángela María Robledo y Luis Évelis Andrade, así como la defensoría y la oficina de las Naciones Unidas para los [Derechos Humanos](https://archivo.contagioradio.com/aumenta-a-152-el-numero-de-heridos-y-a-145-los-detenidos-en-la-minga-nacional/)**, quienes también harán parte de la Comisión de DDHH que se instalará mañana y funcionará de manera paralela a la segunda ronda de conversaciones.

A continuación el comunicado emitido por Cumbre Agraria

“Finaliza primera reunión de Cumbre Agraria Campesina, Étnica y Popular y el Gobierno Nacional con los siguientes acuerdos de confianza:

1.  Garantías en Derechos Humanos y legitimidad de la protesta en términos de no mas agresiones de la fuerza pública a las personas movilizadas en todo el territorio nacional.

<!-- -->

2.  Reactivación de la Comisión de Derechos Humanos conjunta con Ministerio del Interior mañana en la mañana en acompañamiento de la Defensoria del Pueblo y Naciones Unidas

<!-- -->

3.  Intercambio de propuestas para la instalación y metodología de negociación para la Mesa Única el día de mañana.

La \#MingaNacional Continúa!!”
