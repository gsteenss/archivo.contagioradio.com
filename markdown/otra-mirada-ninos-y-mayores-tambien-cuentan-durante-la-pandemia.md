Title: Otra Mirada: Niños y mayores también cuentan durante la pandemia
Date: 2020-06-24 20:37
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Ana Isabel Mendivelso, confinamiento, pandemia, Save the Children en Colombia
Slug: otra-mirada-ninos-y-mayores-tambien-cuentan-durante-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ninos_pies_descalzos-e1472242353820.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Niños

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa del 18 de junio, el tema se centró en dos tipos de población que, en medio de la cuarentena, no han recibido la atención necesaria; por un lado, la primera infancia, niños y adolescentes, que fueron enviados a casa a continuar su proceso educativo virtualmente pero cuyo rol está limitado, y por otro lado, adultos mayores quienes tienen más restricciones y deben continuar en casa por ser más propensos a contagiarse de COVID-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, se analiza cómo han vivido y cuál ha sido el papel de estas dos poblaciones tanto en la ciudad como en territorios y zonas rurales. Para desarrollar el tema se habló con expertos para entender la situación actual en las diferentes zonas del país y cómo ha influenciado la salud mental de las dos partes. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los niños también cuentan en las comunidades

<!-- /wp:heading -->

<!-- wp:paragraph -->

Nuestro primer invitado fue el Profesor de la zona humanitaria de las Camelias, Jhon Hader Córdoba, quien realza la importancia de cuidar y proteger a los adultos mayores en la preservación de la cultura y de la ancestralidad. Además, señala que actualmente, se ve a los abuelos como un estorbo y en vez de utilizar su potencial de sabiduría para construir un país en paz, se lleva a abandonarlos y marginarlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, comparte que los niños y las niñas “son el presente y el futuro” por lo que es de gran importancia permitir que los adultos mayores dejen su fruto en los ellos para no perder el sentido de pertenencia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra de nuestras invitadas fue María Paula Martínez, directora ejecutiva de la ONG [Save the Children](https://twitter.com/savechcolombia)en Colombia, quien habló en nombre de las organizaciones y la situación de los niños del país. Según Martínez, con el confinamiento han aumentado las situaciones de riegos de niños y niñas, sin contar las problemáticas por la falta de educación virtual para muchos de ellos, la falta de acceso a alimento y a servicios de salud, y por sobre todo, el aumento del maltrato infantil. Además, comenta cuál es el balance con el actuar del gobierno y de autoridades locales.  (Le puede interesar: [Lecciones desde el territorio: la importancia de niños y ancianos en medio de la pandemia)](https://archivo.contagioradio.com/lecciones-desde-el-territorio-la-importancia-de-ninos-y-ancianos-en-medio-de-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su lado, Ana Isabel Mendivelso, psicóloga especialista en desarrollo de procesos afectivas y máster en psicología subraya la importancia de que tanto niños como adultos puedan tener espacios de recreación y socialización para asegurar que su salud mental y física no se vea deteriorada al estar tanto tiempo dentro de los hogares. 

<!-- /wp:paragraph -->

<!-- wp:quote -->

>  “Es necesario socializar, jugar y no aislarnos”. 

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por último, Eduardo Sarmiento, ingeniero y economista, manifestó que la cuarentena y el aislamiento decretado por el gobierno generó una crisis económica. Esto demuestra que el país carece de condiciones para garantizar una vida segura a la mayoría de la población y sobre todo niños y adultos mayores. De igual manera, explicó cómo está rigiendo la renta básica y la propuesta que surgió desde el Centro Democrático.

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
