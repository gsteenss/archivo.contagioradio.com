Title: Los 3 retos que enfrentan las zonas de reserva campesina
Date: 2018-12-03 15:55
Author: AdminContagio
Category: Nacional, Paz
Tags: ANZORC, reforma rural integral, tierras, zonas de reserva campesina
Slug: zonas-de-reserva-campesina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/anzorc-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación Chasquis] 

###### [30 Nov 2018] 

Recientemente la **Asociación Nacional de Zonas de Reserva Campesina (ANZORC)** eligió su nueva junta directiva, entre los retos que enfrentarán durante los próximos dos años se encuentran el fortalecimiento de la productividad campesina, el apoyo a la economía rural, y la gestación de un proceso social que impulse una reforma y encuentre soluciones al problema de los cultivos de uso ilícito.

La ANZORC se fundó en 2010 en Barrancabermeja, desde su nacimiento ha trabajado por el reconocimiento de los derechos del campesinado, entre ellos el acceso a tierra a través de las Zonas de Reserva Campesina. De la asociación hacen parte 69 procesos, y como lo recuerda Andrés Gil, nuevo integrante de la junta directiva de la Asociación, trabajan por los agricultores que **producen el 63% de los alimentos de los colombianos**.

### **La implementación del primer punto del Acuerdo**

Gil sostuvo que desde la Asociación ven con preocupación la falta de desarrollo de políticas para implementar el Acuerdo de Paz, al contrario, señaló que la situación actual que se vive en materia de tierras y los programas que se están promoviendo “nos regresa a 1997, cuando estábamos en lo más álgido de la guerra”. En ese sentido, uno de los grandes retos es lograr **la implementación efectiva del primer punto del Acuerdo de Paz que incluye Reforma Rural Integral (RRI)**.

### **Apoyo de la economía campesina**

El integrante de AZORC aseguró que además de la RRI, el catastro multipropósito, que determine en manos de quien están las tierras, y la creación de un sistema de salud que funcione para los agricultores, **se deben impulsar alternativas económicas de desarrollo para los campesinos**. Desde esta perspectiva, es necesario desarrollar sistemas de protección y fomento de la economía campesina, para que los pequeños y medianos productores alcancen la rentabilidad con sus cosechas.

### **Atención efectiva al programa de sustitución de cultivos de uso ilícito**

Gil explicó que mientras **el 91% del campesinado que se acogió al programa de sustitución ha cumplido con el acuerdo**, de los 87 mil acuerdos colectivos que existen, el Gobierno sólo financia 30 mil, mientras a los demás les incumple con los pagos. Este hecho se suma a que no hay un acompañamiento real que le permita al campesino reemplazar el cultivo de coca por otro producto que le permita sostenerse económicamente.

Por estas razones, el último gran reto será **solicitar al Gobierno el cumplimiento de los acuerdos de sustitución pactados**; además de reclamar que se ataque a otras partes de la cadena de producción de la coca, como los canales de distribución y los grandes expendedores, porque hasta ahora, uno de los grandes afectados ha sido el cultivador. (Le puede interesar: ["Organizaciones indígenas y afro radican tutela contra reforma a Ley de Tierras"](https://archivo.contagioradio.com/tutela-reforma-ley-de-tierras/))

<iframe id="audio_30511758" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30511758_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
