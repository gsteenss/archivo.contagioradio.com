Title: El salto ideológico
Date: 2019-12-16 16:13
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: 21n, ideologias, moral, Paro Nacional, uribismo
Slug: el-salto-ideologico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/74802282_1249874088532329_945839746624520192_o.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/record20191216063544-1-online-audio-converter.com_.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto de: Andres Zea/ Contagio Radio] 

#### ¿Qué hace falta para que las cosas cambien? 

Parece que tenemos todo; tenemos al pueblo en las calles, tenemos a la mayoría si no en descontento como mínimo inconforme con el mal gobierno uribista.

**¿Por qué al movimiento 21N le está faltando no la fuerza, sino la contundencia?**  Un terreno sigue incólume en todo lo que se puede denominar movimiento 21N y no ha sido cuestionado en profundidad; es el terreno cultural-moral de ese pueblo alzado. Parece que nos están ganando en ese terreno porque nuestra protesta está operando con sus códigos simbólicos, su sistema de comportamiento y de cultura, que ocultan en últimas todo el esquema de su moral.

**¿Cómo es esa moral?**  Es una moral perversa, individualista, protectora del terrateniente y el capitalista, justificadora de la barbarie *(como Goubertus)* y de la injustica, negacionista del problema estructural colombiano, negacionista de la utilidad de las luchas sociales; es una moral que portan todos sus defensores y algunos que se hacen pasar por “detractores”. Esta moral tiene la particularidad de que se recubre de civilísimo y pasa su infamia de agache, permitiendo que hasta un inconforme con el gobierno, defienda sin darse cuenta los intereses de los poderosos… así de compleja es la cosa.

De otra parte, **la ideología del movimiento 21N no ha querido definirse** y esto, más que un atractivo, puede constituirse en una derrota popular, pues un movimiento con ideología clara respondería con mayor contundencia a las exigencias de su momento histórico, no cuestionaría su accionar a partir de lo que el gobierno le dice, sino que operaría con mas coherencia y menos riesgo de división o disolución.

Creo que nadie del 21N está esperando las órdenes de ningún líder, el 21N podría perderse en la horizontalidad, una fachada que si bien ofrece muchas y variadas acciones, por lo general muestra intereses y sentidos siempre desconectados. No definir ideológicamente un movimiento es un error que la historia ya ha demostrado: **por favor revisar cómo subió Bolsonaro al poder y conectar con la idea que tienen algunos uribistas de cambiar a Duque por Martha Lucía.**

Definirse ideológicamente no se trata de seguir a un caudillo, o de cerrarse ortodoxamente a una idea y convertirse en un sectarista. Se trata de la comprensión individual, colectiva e histórica de la razón de ser y el rumbo del movimiento. La amplitud del 21N es su mayor virtud, pero eso no significa que no tenga capacidad para definir políticamente su forma de operar, su forma de solicitar o en tal caso, arrebatar el acceso al poder político. En ese proceso, quedarán atrás muchas personas que solo quieren salir a la calle a hacer fiestas, que quieren sentir un ratico qué significa “ser un revoltoso”, que creen que Uribe fue un mal “necesario” a la vez que hoy gritan “fuera Duque” y que sinceramente no desean que algo cambie en sus vidas, ni en la sociedad, ni en el país; en este caso, si hay que prescindir de ellas, simplemente hay que hacerlo.

Es importante recalcar que el 21N no lo dirige ningún político, pero de allí, a que vague a la deriva ideológica es un juego peligroso, puesto que va perdiendo el carácter y desacelera el proceso de reivindicaciones, permitiendo la visualización de un hoyo profundo que tiene un aviso al final diciendo “esto no sirvió”.

Le puede interesar: [¿Qué sigue? soluciones al 21N](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/)

#### Ideología no es solo “lo que tu piensas” 

Recordemos que con **Marx** significa distorsión de la realidad y que esto no implica ignorancia o falta de conocimiento del mundo; que con **Mannheim**, se puede comprender la ideología como pensamiento socialmente determinado, e incluso con **Gramsci**, como un todo orgánico y relacional que incluye prácticas productoras de subjetividades. Esto último es súper importante, puesto que, si la ideología construida por la moral de los opresores produce subjetividades, mientras las personas no sean conscientes de si su proyecto vital es autónomo y no dirigido, significa que esa diversidad sobre la que muchos alardean al interior de un movimiento social, mientras no tenga un norte colectivo, de país, de sociedad (político), realmente no implica ninguna diversidad estructural, tan solo será una diversidad simbólica cooptada perfectamente por el mercado, además de que mucho menos, la diversidad **per se**, implicará amenaza alguna contra el poder establecido.

No son pocos los uribistas y tibios que nos llaman “generadores de odio” a aquellos que promovemos la polarización política y la toma de posición definida; algo realmente absurdo pues el odio o el resentimiento no sirven de a mucho en la lucha política, son emociones demasiado efímeras y aquí lo que necesitamos es que la lucha dure, en esto no tienen razón.

##### Pero en algo tienen razón algunos viejos derechistas: 

Un movimiento social, no puede solo cargarse de soñadores, entusiastas, fantasiosos o utopistas, necesita mucho pragmatismo. No en vano, algunos críticos de las protestas aciertan cuando afirman que el romántico es demasiado neurasténico y sueña con los éxitos que podría obtener, pero no intenta nada para alcanzar su fin, y algo peor, es que cuando alguien lo intenta, tiende a criticarlo con rudeza por no ser “la forma perfecta” (¿o no vimos a todos los izquierda caviar criticar a Evo Morales y aceptando sin más el golpe de Estado?), a lo bien, que cobardes.

Sería patético caer en ese estilo del intelectual francés que te habla y te habla de la libertad pero que no sabrá qué diablos hacer cuando llegue el momento de la liberación y que, por lo general, todo le parecerá “rudo” “violento” “ilegal” sencillamente porque no desea profundamente que algo cambie, porque protestar sin saber cómo acceder a una victoria es tan útil como no protestar. La moral capitalista, esa moral del opresor y sus embrujos exponen el bienestar humano como su finalidad, dicen que obran por el bien “del país”, de “la nación” etc, excluyendo de su filantrópica mentira, que se refieren es al bienestar de un grupo social en particular, ese que goza de lo que tiene debido a las mil y una formas de explotación. La moral del capitalista contemporáneo se ufana de ser utilitarista, “si no trabajo no como” “yo no marcho yo produzco”, pero en realidad no son utilitaristas, **John Stuart Mill**, padre del utilitarismo,propuso el principio de la mayor felicidad, en el que las acciones son correctas en la medida que tienden a promover la felicidad ¡¡¡humanaaaaa!! ¡¡no solo de los capitalistas!!! Falacia absoluta es la frase de que “mientras a los ricos les vaya bien a todos nos va bien”, falacia absoluta es la frase “así no se protesta” porque en ellas se halla contenida otra que nunca dicen y es “no protestes así, confórmate porque desestabilizas mis privilegios”.

##### El problema es que la moral del opresor configura una ideología desde todos los puntos de vista Bien sea como tergiversación, construcción colectiva o productora de subjetividades. 

Esa moral capitalista te dice en líneas genéricas: más placer menos dolor, tu bienestar justifica los medios. Por ello, entender la felicidad como placer y ausencia de dolor, lo ha instrumentalizado muy bien la sociedad de mercado. Pero si el placer se equipara solo a consumir estamos jodidos. Por ejemplo:

Si las luchas sociales del 21N se constituyen en escenarios prácticos del “consumo de la desobediencia” pues el gobierno nos dará un aplauso por protestar así, juiciosos, domesticados, “como debe ser”, en orden (es decir el que les favorece) y nada, absolutamente nada pasará.

El salto ideológico implica la resignificación del placer en la sociedad contemporánea, pero esta no es una tarea moral exclusivamente, no implica solo aquello que valoramos bajo marcos de bondad o maldad, de si es “bueno o malo rayar la pared”, sino que también es una tarea intelectual. **Simón Rodríguez** escribió hace mucho **“el que estudia aprende”**. Por ende, intelectualizar la protesta no es sinónimo de montar sillones y discusiones esnobistas que no sirven para un culo, sino que implica la tarea de descubrir colectivamente la ideología inserta en el propósito de la protesta; la ideología cala en la cultura **¡y es allí donde estamos perdiendo la batalla!**

Mientras no derrumbemos sus visiones del orden social presentes en nuestra escala valorativa entonces nuestra conducta será inmodificable, nuestros pensamientos seguirán siendo presos, porque pensaremos solucionar el país bajo los esquemas morales de opresor, porque mientras no haya una subversión de la moral en el movimiento de protesta que ha nacido en Colombia, la ideología del opresor seguirá viviendo a merced de nuestra cultura, nos sentiremos mal por sentir que debemos irrumpir y bloquear todo su sistema y todas nuestras ideas de cambio nacerán muertas; seguiremos creyendo que protestar a nuestra manera es “hacer un mal”, seguiremos discutiendo si hacer o no el grafiti, si tenemos o no derecho a defendernos de la violencia estatal, o de si gritar hijueputazos contra el gobierno sea tal vez algo “sumamente desmedido”….

##### El pudor, el miedo, la vergüenza, el “así no es la manera”, el sentimentalismo, son las cadenas más gruesas que nos han impuesto sin disparar un solo tiro. 

A pocos les gusta la palabra ideología, pues solo la ven como tergiversar. Incluso algunos  intelectuales la tratan peyorativamente. No obstante, mientras ellos discuten en sus logias, un pueblo está en las calles, a la gente la siguen matando en el campo, ahora hasta vemos gracias a los celulares, el abuso descarado de la policía y como si fuera poco el gobierno sigue aprobando todo a espaldas de la situación. Por eso, la ideología como elemento cultural vinculante a la realidad, podría permitir generar prácticas sociales que logren establecer otra forma moral de ver lo político y quitarnos de encima de una buena vez, esa maña que tienen los poderosos en Colombia de hacer de su forma de ver el mundo, “la forma natural del mundo”.

 

**¡todo se puede transformar!**

¡Todo! Incluso este régimen político y económico que tienen Colombia. Destruyamos la moral del opresor dentro de nosotros, demos un salto ideológico, y ganemos la batalla a la cultura que conduce nuestras acciones, no olvidemos que subvertir el orden establecido es edificar las bases de una nueva sociedad; no tengamos miedo a las diferencias irreconciliables, abramos los ojos, que ellos jamás abandonarán el poder que tienen en medio de risas, flores y abrazos.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/record20191216063544-1-online-audio-converter.com\_.mp3"\]\[/audio\]

ver mas: [Otras notas de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)
