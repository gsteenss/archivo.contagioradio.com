Title: Alcaldía de Bogotá tiene 3 meses para convocar Consulta antitaurina
Date: 2017-05-10 14:59
Category: Animales, Nacional
Slug: alcaldia-de-bogota-tiene-3-meses-para-realizar-la-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/IMG_2241-e1494436890470.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Plataforma ALTO] 

###### [10 Mayo 2017]

En 2015 la consulta anti taurina fue aprobada por el Consejo de Bogotá y legalmente en el Tribunal Administrativo de Cundinamarca. A pesar de que no se realizó, hoy la Corte Constitucional da la orden para que se hable de la "fiesta brava" y establece un plazo de 3 meses para que se convoque a las urnas.

Hace dos años el Ministerio de Hacienda se opuso a la realización de la consulta por los altos costos que demanda la logística del mecanismo. La Corte Constitucional en días pasados revivió la ponencia y **le ordenó al alcalde Peñalosa, además de desincentivar esta práctica, adelantar los trámites pertinentes para la realización de la consulta.** Le puede interesar: ["Corte Constitucional ordena al distrito tramitar la consulta antitaurina"](https://archivo.contagioradio.com/corte_constitucional_distrito_consulta_antitaurina/)

Mateo Córdoba, miembro de la plataforma ALTO, celebró la orden de la Corte y aseguró que la consulta lleva dos años lista para realizarse y que **los costos que se deben pensar son únicamente los costos políticos al llamado a las urnas.** Córdoba afirmó que "el proyecto no debe pasar nuevamente por el Consejo ni por el Tribunal por lo que el alcalde tiene 3 meses para convocar a los ciudadanos a las urnas".

Con la respuesta a la pregunta "¿Está usted de acuerdo, Sí o No, con que se realicen corridas de toros y novilladas en Bogotá Distrito Capital?", los anti taurinos como Mateo Córdoba esperan que la consulta **siente unos precedentes éticos, legales y democráticos al momento de pensar en un referendo.** Le puede interesar: ["El verdadero costo de la Consulta antitaurina"](https://archivo.contagioradio.com/realizacion-de-consulta-antitaurina-realmente-costaria-11-mil-millones-de-pesos/)

La diferencia que se le atribuye al respaldo de la realización de la consulta anti taurina, frente al referendo contra la adopción de niños y niñas por parte de parejas homosexuales, solteros y viudos, está asociada a la naturaleza ética de los proyectos. **Córdoba afirma que la consulta anti taurina no pasa por encima de los derechos de nadie** y por el contrario busca proteger a los animales.

<iframe id="audio_18613721" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18613721_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
