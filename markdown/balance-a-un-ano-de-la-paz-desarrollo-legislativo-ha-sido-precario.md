Title: Balance a un año de la paz: desarrollo legislativo ha sido precario
Date: 2017-11-24 17:48
Category: Nacional, Paz
Tags: acuerdos de paz, Cámara de Representantes, Congreso de la República, legislatura del Congreso, Paz y reconciliación
Slug: balance-a-un-ano-de-la-paz-desarrollo-legislativo-ha-sido-precario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón ] 

###### [24 Nov 2017] 

A un año de haberse firmado el Acuerdo de Paz en el Teatro Colón de Bogotá, el Congreso de la República y su legislatura para la elaboración de las normas de paz, **ha puesto en riesgo y ha modificado** lo que se acordó en la Habana. El mecanismo de Fast Track está por terminar y aún hace falta aprobar  y debatir una gran cantidad de proyectos.

Por un lado y de acuerdo con el informe de la Fundación Paz y Reconciliación, desde el inicio de la segunda lesgislatura en el Congreso, que comenzó el 20 de julio, **“el avance ha sido precario** tanto por el número de iniciativas aprobadas como por los cambios introducidos para limitar el cambio de las reformas”. Indican que debía haberse tramitado 24 iniciativas en este periodo y sólo se han aprobado 12.

Allí estaban incluidas la reforma política, las circunscripciones especiales de paz, la JEP y estas **“han sido mutiladas a medida que fue avanzando el debate** y se han apartado en buena forma de los acuerdos de la Habana”. Por otro lado, 12 iniciativas siguen aprobarse, de las cuales 8 no fueron presentadas por el Gobierno y 4 relacionadas con el tema agrario no alcanzaron los debates necesarios. (Le puede interesar: ["Balance a un año de la paz: cuatro aspectos de la crisis de la reincorporación de FARC"](https://archivo.contagioradio.com/balance-a-un-ano-de-la-paz-en-crisis-reincorporacion-de-las-farc-a-la-vida-civil/))

### **“Cuando un proyecto busca cambiar las reglas de juego, los políticos lo entorpecen”** 

Paz y Reconciliación indicó que partidos como el Centro Democrático “en cabeza de Uribe, desató una dura controversia en contra de los Acuerdos de Paz, **haciendo gala de un lenguaje inapropiado y estigmatizante**”. Si bien esto ocurría, “el Gobierno capoteo a la oposición con unas mayorías limitadas pero suficientes para sacar adelante los proyectos y de eso da cuenta la primera legislatura del Fast Track”.

Sin embargo, en la segunda legislatura, la división de la Unidad Nacional, el retiro de Cambio Radical y el nombramiento de Rodrigo Lara como presidente de la Cámara de Representantes, **“complicaron las cosas”** y demostró la imprevisión de Santos para manejar el tema de la paz en el recinto legislativo. (Le puede interesar: ["Balance a un año de la paz: FARC, la guerrilla que mayor cantidad de armas ha dejado"](https://archivo.contagioradio.com/farc-ha-sido-la-guerrilla-que-mayor-cantidad-de-armas-ha-dejado/))

Adicional a esto, el clima hostil en el proceso de creación de normas aumentó por los cuestionamientos y las objeciones que hizo la Fiscalía y la Corte Constitucional frente a algunos temas. También, las actuaciones de los partidos políticos se empezaron a enmarcar teniendo en cuenta **los intereses políticos y “su reacomodo en los cargos del Gobierno”.**

La gestión de Rodrigo Lara en la Cámara **ha sido fuertemente criticada** por diferentes sectores de la sociedad en la medida que, bajo su presidencia, siguen aconteciendo actos históricos como el sabotaje de los debates y la votación negativa o en bloque abstencionista. Para la Fundación, si “la idea era abrir el sistema político, contar con nuevos actores en la democracia y lo más importante echar las bases de un sistema electoral abierto y transparente, se ha frustrado”.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
