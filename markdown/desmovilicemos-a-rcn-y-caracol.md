Title: Desmovilicemos a RCN y CARACOL
Date: 2015-09-27 09:25
Category: invitado, Opinion, superandianda
Tags: Acuerdos de paz en Colombia, Manipulacion mediatica, paramilitarismo en Colombia, RCN y Caracol oposicion a la paz, uribismo
Slug: desmovilicemos-a-rcn-y-caracol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/RCN-CARACOL-e1443363760425.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 

#### **Por [Superandianda ](https://archivo.contagioradio.com/superandianda/) - [@Superandianda](https://twitter.com/Superandianda)  ** 

##### [27 Sep 2015] 

[Quienes creemos que es necesaria una reestructuración social en Colombia, celebramos la firma del acuerdo de justicia firmado en la Habana, no porque seamos ilusos y mucho menos porque olvidemos todos los actos terroristas que durante 50 años sometieron poblaciones enteras. Lo celebramos porque entendemos la necesidad de un inicio, de un acuerdo que reconozca que dentro del conflicto armado no hay buenos ni malos sino víctimas; víctimas que deben repararse, reconocerse y que están dispuestas al perdón, creyendo al igual que nosotros que es urgente y necesario el acuerdo por la  paz y la reconciliación.]

[Pero el que parece no estar dispuesto a entender de qué se trata la salida política al conflicto armado, es el sector guerrerista y culturalmente mafioso de este país, quienes quieren confundir la palabra negociación con sometimiento y justicia con cárcel. Ocho años de seguridad democrática no fueron suficientes para acabar a las FARC militarmente, sin embargo, como  si hubiera sido todo un éxito, el gobierno de los falsos positivos y victimas sin nombre, insiste de manera obtusa, que con balas y  fusiles se resuelven las diferencias políticas, a pesar de no haber sido precisamente las FARC las que se arrodillaron ante Álvaro Uribe Vélez y el sometimiento que se lograra fuera el de la constitución a la corrupción y el paramilitarismo; entonces ¿por qué quieren hacer ver que los resultados dados con bombardeos funcionan si la realidad es otra? si la tesis uribista fuera verdad  hoy no estaría sentado el gobierno con la guerrilla buscando un mutuo acuerdo político, el que nunca logro Andrés Pastrana y que hoy tanto critica.]

[Al proceso de paz le han salido más enemigos que a la propia guerra, y como si fueran grupos terroristas –sin fusil y sin armas de fuego- RCN y CARACOL claramente se han organizado como oposición a los adelantos de las negociaciones de la Habana. Armados con micrófonos, cámaras y manipulación, estas dos empresas privadas,  que lamentablemente son los líderes en información e ignorancia en Colombia, promueven toda una campaña para desprestigiar todo lo que se realice en la Habana, todo su fundamento periodístico consiste en sembrar incertidumbre e intriga, confundiendo la opinión de aquellos que no ven más allá de estos dos grupos armados de mentiras.]

[No podemos seguir hablando de paz y reconciliación en la Habana si los canales de mayor audiencia televisiva solo invitan a mantener el conflicto y a llenarse de odio contra el proceso, estamos frente a dos grupos que sin necesidad de disparar una bala son tan peligrosos como cualquier grupo terrorista listo para atacar. Es necesario entonces invitar a RCN y CARACOL  a que se desmovilicen, entreguen las armas que le proporcionaron los grandes empresarios y dejen de hacerle más daño a Colombia, suficiente hemos tenido con décadas de violencia para que todos los días nos sigan bombardeando a través de los canales de televisión.]

[La paz del uribismo, no puede ser la paz que Colombia espera: la de la humillación y sometimiento con muerte. En una negociación política no existen ganadores ni perdedores, es un acuerdo de las partes ante el no éxito de ninguna de las dos en sus operaciones. La solución no es apagar el televisor porque muchos sí lo tienen encendido y lamentablemente son la mayoría que van a refrendar el acuerdo final. Estoy de acuerdo con la crítica y la opinión pero en el caso de lanzar todo el proceso de paz a un referendo ¿Qué tan preparada está Colombia para validarlo sanamente?]

[Imagino que después de la firma de la paz, mientras las FARC entregan las armas buscando participación política Claudia Gurisatti y Vicky Dávila cambiaran los micrófonos para tomarlas y salir inmediatamente a combatir.  Seis meses son suficientes para que los periodistas/periodistas se organicen y busquen la justicia uribista a la que tanta propaganda le hacen.]

[–Error, el poder de los medios genera más impacto que cualquier bala-]
