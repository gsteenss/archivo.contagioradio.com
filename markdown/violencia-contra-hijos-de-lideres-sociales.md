Title: Violentos se ensañan con hijos de líderes sociales
Date: 2017-09-29 12:38
Category: DDHH, Nacional
Tags: Asesinatos, Asesinatos a líderes comunitarios, CISBSC, desparición, lideres sociales
Slug: violencia-contra-hijos-de-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Sept 2017] 

La Comisión de Interlocución del Sur del Bolívar, Centro y Sur del Cesar (CISBCSC) **denunció el asesinato del joven Osnaider Jaimes Viviesca,** hijo de un líder social. El cuerpo del joven fue encontrado el 28 de septiembre en la vereda Vara Honda del municipio de Arenal al sur del Bolívar. Además en el Cesar está desparecido Anderson Vargas, hijo también de líderes sociales.

Osnaider era hijo del presidente de la Junta de Acción Comunal de la vereda la Bonita y líder campesino de Arenal, José David Jaimes Lemus, quien se encuentra detenido en la cárcel de Magangué por orden de la fiscal María Bernarda Puente, quien también está detenida y es acusada de **recibir sobornos para favorecer a paramilitares.**

De acuerdo con Teófilo Acuña, integrante de la CISBCSC, Osnaider había sido preguntado en las comunidades por el Ejército Nacional quienes “supuestamente tenían una orden de captura y dijeron que **lo necesitaban vivo o muerto**”. Dijo que fue un moto taxi de la vereda Vara Honda quien encontró el cuerpo del joven. (Le puede interesar: ["Desde enero hasta agosto de 2017  han sido asesinados 101 líderes sociales"](https://archivo.contagioradio.com/entre-enero-y-agosto-han-sido-asesinados-ciento-un-lideres-sociales/))

Acuña manifestó que en esta zona del país **ha incrementado la presencia de grupos armados** que “están atracando, robando y amenazando a la gente”. Las comunidades han afirmado que también hay presencia de grupos guerrilleros haciendo confusa la situación sobre quién pudo haber asesinado al joven Osnaider Jaimes.

Adicional a esto, afirmó que, en el sur del Bolívar, cuando ocurren estos asesinatos, **“no llega ninguna autoridad para hacer el levantamiento del cadáver** y le toca a la comunidad y a los familiares hacer este proceso”. En el caso de Jaimes, sus familiares lo levantaron para llevarlo al municipio de Arenal donde el comandante de la Policía no firmó los documentos necesarios y tampoco lo querían atender en ningún hospital”.

### **Hijo de líder social de Aguachica se encuentra desaparecido** 

Al asesinato de Osneider se suma la desaparición del joven **Anderson Vargas en el municipio de Aguachica,** Cesar, hijo también de un líder agro minero del sur de Bolívar. Acuña informó que los compañeros del joven están haciendo las diligencias para encontrarlo. (Le puede interesar: ["Asesinato de líderes sociales, memoria y resistencia"](https://archivo.contagioradio.com/lideres-sociales-memoria-resistencia/))

Según las denuncias, Vargas salió para el gimnasio y **la última vez que lo vieron fue cuando salía de allí** el pasado 27 de septiembre. El CISBSC informó que el padre de Vargas ha sido perseguido y amenazado por los paramilitares, sin embargo “no sabemos por qué está desaparecido si es un joven muy juicioso que se dedicaba a estudiar”.

<iframe id="audio_21178136" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21178136_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
