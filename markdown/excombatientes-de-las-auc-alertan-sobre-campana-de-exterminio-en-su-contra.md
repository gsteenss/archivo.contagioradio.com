Title: Excombatientes de las AUC alertan sobre "campaña de exterminio" en su contra
Date: 2019-08-27 23:39
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: asesinato de excombatientes, AUC, Justicia y Paz
Slug: excombatientes-de-las-auc-alertan-sobre-campana-de-exterminio-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Verdad Abierta] 

El Colectivo Nacional de Excombatientes de las AUC vienen alertando que existe una "campaña de exterminio" en contra de aquellos quienes  se han sometido a un proceso de reincorporación, una denuncia que cobra más fuerza tras conocerse el asesinato del excombatiente **Dilio Jose Romero Contreras**, ocurrido el pasado 26 de agosto en Bolívar y de **Eduardo Rafael Sierra Gámez**, de 48 años de edad, también excombatiente que se encontraba en Valledupar, La Guajira, el pasado 21 de julio.

Aunque la Agencia para la Reincorporación y la Normalización (ARN) reporta que de los 30.688 exparamilitares vinculados al proceso de reincorporación desde 2003, más de 2.000 han sido asesinados, **Luis Arley Arango, integrante del Colectivo, señala que esta cifra asciende a más de 3.500.**

Arango afirma que existen varios factores que consideran son los que han incidido en este incremento de asesinatos contra excombatientes de las AUC, señalando a disidencias de las FARC y agentes del Estado como responsables, mientras otros homicidios se han registrado como retaliaciones, consecuencia de las versiones libres como parte del proceso de justicia transicional que conforma la ley de Justicia y Paz.

A propósito, el excombatiente advierte que Dilio José Romero, fue asesinado "un día antes de presentare a una diligencia para declarar ante terceros".  [(Le puede interesar: Mapiripán y un reto histórico: superar la tragedia y construir la paz)](https://archivo.contagioradio.com/mapiripan-y-un-reto-historico-superar-la-tragedia-y-construir-la-paz/)

Luis Arley señala que en el transcurso de 12 años desde que se acogieron a un proceso de reintegración, no se ha hecho un pronunciamiento político por parte del Gobierno o de la ARN, "han sido ciegos, sordos y mudos a este genocidio en contra de los desmovilizados de las AUC".

**Síguenos en Facebook:**  
<iframe id="audio_40608069" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40608069_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
