Title: Respetado Papa Francisco - Carta desde el Closet
Date: 2017-09-02 09:36
Category: Cartas desde el Closet, Opinion
Tags: carta al papa, LGBTI, Visita papa Colombia
Slug: respetado-papa-francisco-carta-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/PAPA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mikdev 

###### **  
Septiembre, 2017** 

**Respetado Papa Francisco **

[ Reciba un cordial saludo]

Soy Julieta, la verdad no me imaginé nunca estar escribiéndole, pero siento que su actitud humana y compasiva hacia las personas homosexuales como yo, me causa sorpresa y alegría;  diferente a lo que he visto de la Iglesia, que ha maltratado de muchas maneras y desde hace mucho tiempo a las mujeres y hombres de mi misma condición.

[ Hay muchas personas que están felices por su venida a Colombia, y considero que su visita puede ayudar mucho a un país polarizado, que está profundamente herido. Creo en el impacto y la importancia que un líder espiritual tiene para enseñar a pacificar el corazón de las personas. Pero también sé que a muchas personas no les gusta su visita y era de esperarse, lo que me sorprende es el rechazo radical de miembros de la iglesia católica.  ]

[Esa hostilidad y discriminación, lo conozco porque me ha causado mucho daño ya que está presente en mi familia. Aunque he logrado aprender a vivir con eso, me tocó irme de mi casa y buscar espacios amorosos para mí.]

[Me sorprende que estemos compartiendo los sentimientos que genera el rechazo pero, creo que por eso mismo, lo siento más cercano. Usted ha tomado posiciones valientes, humildes y difíciles ante sectores “radicalizados” y por eso se entienden sus reacciones.]

[No soy creyente, pero deseo que sus palabras y los actos que acompañen su visita traiga a este país serenidad y calma, que se sienta bienvenido y tenga fortaleza para el desafío de venir a un país que ha tiendo el corazón marcado por la guerra, pero que ahora busca la paz en medio de quienes quieren mantener guerra de la que han sacado grandes ganancias.]

[ Con profundo reconocimiento]

**Julieta - [Parche por la vida, 7](https://archivo.contagioradio.com/cartas-desde-closet/)**

------------------------------------------------------------------------

###### Este texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida
