Title: FECODE a paro de 24 horas por incumplimientos del Gobierno y aumento de hechos violentos
Date: 2019-08-15 15:44
Author: CtgAdm
Category: Educación, Movilización
Tags: educacion, fecode, paro, profesores
Slug: fecode-paro-24-horas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/FECODE-anuncia-paro-nacional.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @rijecas] 

[Los próximos **28 y 29 de agosto se realizará un paro general de profesores en todo el país**, tras la convocatoria realizada este jueves por parte de la **Federación Colombiana de Educadores (FECODE).**  Según ellos, son dos las razones de esta convocatoria, por una parte los incumplimientos del Gobierno en torno al derecho a la salud, y el incremento de las amenazas, asesinatos y hostigamientos contra las y los maestros que van en aumento.]

[FECODE, que agrupa a más de cuatrocientos mil docentes del país, aseguró que “las entidades prestadoras del servicio médico-asistencial no cumplen puntualmente con los términos de los contratos”, por lo que hay negación para la entrega de medicamentos, dilaciones en los tratamientos, no asignación de citas e incluso, no programación de cirugías que requieren algunos de los docentes. (Le puede interesar:["ADEMACOR denunció el asesinato del profesor Carlos Mazo en Córdoba"](https://archivo.contagioradio.com/asesinato-profesor-carlos-mazo/))]

[Adicionalmente denunciaron que “El Gobierno Nacional, el Ministerio de Educación como fideicomitente, el Ministerio de Hacienda y la Fiduprevisora tampoco ejercen a cabalidad su función de control y vigilancia”, lo que está generando serios problemas en la administración del Fondo Nacional de Prestaciones Sociales, una entidad pública que estaría en camino a ser privatizada. (Le puede interesar:["¿Por qué paran 340 mil profesores de toda Colombia?"](https://archivo.contagioradio.com/paran-340-mil-profesores/))]

### **FECODE advierte que violencia contra docentes del país va en aumento** 

[Otra de las razones centrales de la protesta magisterial son los asesinatos de Carlos Arturo Mazo, en Córdoba; Leonardo León Trujillo, en Armenia; y el rector Orlando Gómez, en Cauca, registrados durante el último mes. Por ello exigen una pronta atención a las denuncias de amenazas dado que según ellos, **los incumplimientos del Gobierno están derivando en retrasos de los traslados a docentes amenazados.**]

[Adicionalmente, FECODE anunció la realización de la “Caravana por la Paz, por la Vida y por la Escuela como Territorio de Paz; la cual saldrá desde Bogotá y finalizará el 29 de agosto en Popayán. La caravana se repetirá hacia otras regiones, donde también se afronta una ola de violencia social.” (Le puede interesar: ["Por deuda histórica profesores vuelven a las calles en febrero"](https://archivo.contagioradio.com/profesores-vuelven-calles/))]

> El Presidente de Fecode, [@Nelsonalarcon74](https://twitter.com/Nelsonalarcon74?ref_src=twsrc%5Etfw), explica las razones y objetivos del Paro Nacional de 48 horas del Magisterio los días 28 y 29 de agosto. ¡Todos a las calles por la defensa de los derechos a la vida, la salud y la educación pública! <https://t.co/c25lUkwIfi>
>
> — fecode (@fecode) [August 15, 2019](https://twitter.com/fecode/status/1161810456446689280?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
