Title: "Ningún genocida suelto" la consigna contra ley del 2x1 en Argentina
Date: 2017-05-05 16:31
Category: El mundo, Otra Mirada
Tags: Argentina, dictadura, Madres Plaza de mayo
Slug: argentina-dictadura-ley
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/C_AZBVYXgAAJmLa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Prensa Madres 

###### 05 May 2016 

Organizaciones sociales de Argentina convocaron a un acto para el próximo **10 de mayo** en rechazo a la decisión de la Corte Suprema de Justicia al declarar aplicable la **ley 24.390, conocida como la ley del 2x1** que reduce la pena a los condenados por delitos de lesa humanidad de la dictadura cívico militar que vivió se país entre 1976 y 1983.

Bajo la consigna "**Señores Jueces: Nunca Más. Ningún genocida suelto. 30 mil detenidos desaparecidos presentes**", las agrupaciones invitan a la sociedad en su conjunto a unirse Frente al Palacio de Justicia para que nunca más haya impunidad en Argentina "**por que lo que no se juzga y condena, se repite**" aseguran.

La decisión del máximo tribunal, adoptada por los jueces Elena Highton, Horacio Rosenkrantz y Carlos Rosatti y votada en contra por los magistrados Ricardo Lorenzetti y Juan Carlos Maqueda, permitiría que se aplique el 2x1, una norma vigente solo entre 1994 y 2001, año en que fue derogada. Le puede interesar: [Estudiantes chilenos exigen no más deuda por estudiar](https://archivo.contagioradio.com/estudiantes-chile-deuda/).

La sentencia fue dictada en el caso de Luis Muiña, quien en 2013 fue condenado a 13 años de cárcel por ser 'coautor del delito de privación ilegal de la libertad e imposición de tormentos' en cinco causas, fallo que **abre la puerta para que los imputados por crimenes relacionados con la dictadura puedan acogerse a un beneficio similar**.

En un comunicado a la prensa, las madres de Plaza de mayo, organización que hace parte de las convocantes, aseguran que los argumentos, mismos bajo los cuales Lorenzetti y Maqueda votaron en contra la de la decisión, se sostienen en que **la reducción de pena no es aplicable a los crímenes de la dictadura**.

Los delitos cometidos durante ese periodo de tiempo **no precluyen hasta no sea revelado el destino de los desaparecidos**, nietos y nietas, por lo que no se pueden aplicar en estos casos leyes previas y no vigentes, mismo criterio exigido por la Corte Interamericana de Derechos Humanos, ante el cual la Corte Suprema ha hecho caso omiso.

En sus declaraciones, las madres aseguran que **el fallo también toca las condenas a los apropiadores de menores** "porque cambia el criterio respecto a la ley aplicable que venía sosteniendo la jurisprudencia hasta la actualidad, incluso refrendada por la propia Corte Suprema" quienes recibirían penas de tres años de prisión de ejecución condicional de manera que "**un delito que se viene cometiendo durante 40 años recibiría una condena no acorde a la gravedad del hecho**".

Las organizaciones que convocan al acto que tendrá lugar a las 18 horas en Plaza de mayo del próximo miércoles, son las Abuelas de Plaza de Mayo / Madres de Plaza de Mayo Línea Fundadora / Familiares de Desaparecidos y Detenidos por Razones Políticas / H.I.J.O.S. Capital / Centro de Estudios Legales y Sociales (CELS) / Asamblea Permanente por los Derechos Humanos (APDH) / Asamblea Permanente por los Derechos Humanos (APDH) La Matanza / Asociación Buena Memoria / Comisión Memoria, Verdad y Justicia Zona Norte / Familiares y Compañeros de los 12 de la Santa Cruz / Fundación Memoria Histórica y Social Argentina / Liga Argentina por los Derechos del Hombre / Movimiento Ecuménico por los Derechos Humanos.
