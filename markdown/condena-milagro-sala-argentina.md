Title: Tribunal condena por 3 años a Milagro Sala
Date: 2016-12-28 19:32
Category: Judicial
Tags: Argentina, Condena, Juicio, Milagro Sala
Slug: condena-milagro-sala-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/2334379w620.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: La nación 

##### 28 Dic 2016 

Dando cumplimiento a la disposición del [pasado 22 de diciembre](https://archivo.contagioradio.com/milagro-sala-argentina-juicio/), **el Tribunal Oral Federal de Jujuy en Argentina, emitió sentencia en el caso de Milagro Sala, condenándola a 3 años de prisión en suspenso**, por daño agravado en contra de Gerardo Morales actual gobernador de esa provincia.

En el mismo juicio, la activista **fue absuelta por el delito de amenazas**, sobre el cual la querella pretendía se impusiera una condena de 8 años. Como supuestos coautores en un escrache contra Morales en 2009 por el que se le procesa a Sala, **se condenó a los cooperativistas de la Red de Organizaciones sociales Graciela López y a Ramón Salvatierra a tres y dos años de prisión en suspenso respectivamente**.

Antes de conocer la sentencia, Milagro Sala se dirigió a los asistentes "Siento mucho dolor interno por la injusticia que estamos viviendo, porque no hemos robado nada, **hemos dignificado a miles de compañeros. (...) dignificar a los que menos tienen nos significó estar sentados en este sillón**".

López también tuvieron la oportunidad de hablar, insistiendo que en el juicio "**queda claro el revanchismo político hacia las clases sociales y organizaciones** de un sector político que es el primer responsable de la crisis de 2001 (...) Hoy s**e criminaliza la protesta** en una Argentina donde costó mucha sangre recuperar la democracia", mientras que Salavarrieta insistió en su inocencia.

Por tratarse de una condena a prisión en suspenso o condena condicional, **el juez puede no hacer efectiva la detención del procesado**, cuando se trate de un primer fallo por un plazo no mayor a tres años. La defensa de la activista ha anunciado que a**pelara la medida**, mientras se espera que continúen las manifestaciones de apoyo a nivel nacional e internacional. La lectura integral de la sentencia está prevista para el próximo 3 febrero.
