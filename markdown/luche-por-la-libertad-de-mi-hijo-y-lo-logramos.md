Title: "Luché por la libertad de mi hijo ¡y lo logramos!"
Date: 2019-06-05 10:21
Author: CtgAdm
Category: Expreso Libertad
Tags: Falsos Positivos Judiciales, Mateo Gutierrez, presos politicos
Slug: luche-por-la-libertad-de-mi-hijo-y-lo-logramos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Mateo-gutierrez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo familiar 

**Aracely León** es la mamá de **Mateo Gutiérrez León**, un joven estudiante de sociología capturado el 27 de febrero de 2017 bajo un falso positivo judicial, que logró recuperar su libertad a principios de este año. Una circunstancia que no solo alteró la vida del muchacho, sino que también **llevo a su familia más cercana a vivir situaciones de inseguridad y pruebas a su fortaleza**.

Aracely, relató para los micrófonos del Expreso Libertad, **las dificultades de asumir un proceso judicial,** para ella viciado y manipulado desde un primer momento, que se desarrolló en los medios de información y no en los estrados. Sin embargo aseguró que **siempre confió en la inocencia de Mateo y nunca desfalleció en la búsqueda de su libertad**.

###### <iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2237725106335202%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
