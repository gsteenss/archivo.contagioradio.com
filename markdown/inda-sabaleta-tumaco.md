Title: 5 personas muertas y 40 secuestradas en enfrentamientos en Inda Sabaleta, Tumaco
Date: 2020-09-26 21:24
Author: AdminContagio
Category: Actualidad, DDHH
Slug: inda-sabaleta-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Inda-Sabaleta-foto-ONIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La información difundida por el Colectivo de Abogados José Alvear Restrepo da cuenta de que desde la madrugada de este 26 de Septiembre se están presentando enfrentamientos entre el llamado frente Oliver Sinisterra y el grupo armado conocido como “Los Contadores”, durante los cuales se han presentado 5 muertes y un grupo de 40 personas fue secuestrado en el resguardo **Inda Sabaleta** en el departamento de Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia, los cuerpos de las cinco personas no han sido levantados y dos personas que resultaron heridas tampoco han podido recibir atención médica. Adicionalmente aseguran desde la comunidad que las 40 personas fueron llevadas para “realizar una investigación”, sin embargo se desconoce su paradero y **la comunidad asegura que son civiles habitantes del resguardo y que no quieren ser involucrados en la guerra.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el **gobernador del Resguardo Inda Sabaleta, Neider Sevillano,** asegura que a pesar de haber realizado las denuncias respectivas no han recibido respuestas por parte del gobierno nacional o departamental para evitar daños en la población civil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente Sevillano alerta sobre la situación de zozobra en la que se encuentra la comunidad debido a las múltiples incursiones de los actores armados en su territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es la primera vez que la guerra se ensaña contra el resguardo Inda Sabaleta

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya en el mes de Abril de este año se había presentado un ataque por parte de un grupo de erradicadores de la Fuerza Pública en el que resultaron una persona indígena de pueblo Awa asesinada y otros tres resultaron heridos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1253141161486815236","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1253141161486815236

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Resguardo Inda Sabaleta es una de las víctimas de las múltiples facetas de la guerra en Nariño

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Centro de Investigación y Educación Popular CINEP la violencia en Tumaco es multifacética y se ha presentado en una de las formas más complejas en comparación con el territorio nacional. Este análisis tiene como uno de sus centros el conflicto y las nuevas formas de violencia presentes en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Se han creado nuevos grupos armados como “Los Contadores”, “Oliver Sinisterra” y “Guerrillas Unidas del Pacífico (GUP)” que buscan recuperar las economías ilegales y las acciones de control que tenían las Farc en el territorio. Por otro lado, hay bandas criminales que operan en los barrios y que han incidido en el aumento y desborde de la violencia, “en la dinámica de su disputa territorial hay una clara intención por acudir a un tipo de violencia que los haga menos visibles y menos expuestos penalmente” se menciona en el informe  [“Transformaciones del conflicto armado y conflictividades sociales en San Andrés de Tumaco”](https://www.cinep.org.co/Home2/component/k2/tag/Tumaco.html)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
