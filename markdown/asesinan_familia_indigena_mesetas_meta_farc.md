Title: Denuncian masacre de una familia indígena en Mesetas, Meta
Date: 2017-10-26 13:18
Category: DDHH, Nacional
Tags: Asesinatos de indígenas, FARC, Mesetas
Slug: asesinan_familia_indigena_mesetas_meta_farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/IMG_2091.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Carolina Covelli - Contagio Radio] 

###### [26 Oct 2017] 

De acuerdo con una denuncia del Consejo Político Nacional de la Fuerza Alternativa Revolucionaria de los comunes (FARC), una familia indígena habría sido asesinada, en la Vereda Buena vista del municipio de Mesetas, Meta, donde se encuentra ubicado el Espacio Territorial de Capacitación y Reincorporación Mariana Páez.

Según indican, personas desconocidas habrían ingresado al predio donde vivía la familia, y allí habrían asesinado a **Diana Marcela Calvo Rojas, a Robinson su primo hermano y además fue desaparecido el esposo de Diana, Diego Ferney Pilcue.**

Se trataría de una familia que ha vivido en esa zona durante toda su vida. Asimismo, se ha indicado que una vez llegaron al lugar de los hechos, fueron **encontradas dos niñas, una de tan solo 4 años y la otra de 2 años**, quienes pertenecerían a la familia. (Le puede interesar: [Asesinan a gobernador indígena del Alto Baudó)](https://archivo.contagioradio.com/asesinado-gobernador-indigena-del-resguardo/)

Los integrantes de dicha zona de reincorporación señalan que **aún no se tiene claridad sobre los autores materiales del crimen,** y además piden presencia de la comunidad internacional, puntualmente de los los países garantes y acompañantes del proceso de paz, y de la Organización de Naciones Unidas, para que verifiquen la situación y se exijan garantías tanto para las comunidades, como para las y los excombatientes.

"Estos hechos sumados a los demás acontecidos en todo el territorio nacional generan gran preocupación para la militancia Fariana en proceso de reincorporación a la vida civil, política y social y de igual manera para el resto de la población civil que no ve aun el cumplimiento en materia de seguridad como lo estipulan los acuerdos de paz", concluye la denuncia.

### **Alarma por asesinatos de indígenas ** 

Con este crimen en Mesetas, **sumarían 28 los asesinatos contra integrantes de comunidades indígenas en lo corrido del 2017**. Esta semana también se cometió el homicidio contra el gobernador indígena del Resguardo Catrú, Dubasa y Ancosó en el Alto Baudó, Chocó, Aulio Isarama Forastero. Sumado a que continúa secuestrado el docente indígena Jhon Eriberto Isarama Forastero.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div>

</div>
