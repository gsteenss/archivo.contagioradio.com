Title: Restitución de tierras en 'La Europa" Sucre amenazada por paramilitares
Date: 2016-04-08 16:49
Category: DDHH, Nacional
Tags: Finca La Europa, Ovejas Sucre, paramilitares, Restitución de tierras amenazas
Slug: restitucion-de-tierras-en-la-europa-sucre-amenazada-por-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesino-e1460152084815.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 8 Abr 2016

Habitantes denuncian que el pasado jueves 7 de abril hombres vestidos de negro a bordo de motocicletas de alto cilindraje, recorrieron en diferentes direcciones el predio conocido como ‘La Europa’, ubicado en el municipio de Ovejas, Sucre.

De acuerdo con la denuncia elevada por el Comité permanente de Derechos Humanos, las cerca de 70 familias que habitan la extensión del territorio, quienes hacen parte del proceso de restitución de sus propiedades, se sienten hostigados y amenazados por las acciones del grupo de hombres quienes presuntamente pertenecerían a grupos paramilitares.

Adicionalmente, el CPDH pone en evidencia las amenazas recibidas el primero de abril por el líder de restitución y presidente de la asociación de campesinos y campesinas de ‘La Europa’, Argemiro Lara, a quien le fueron enviados a su celular mensajes intimidantes firmados por las autodenominadas “Águilas negras”.

Este tipo de advertencias, se suman a la continua persecución a las familias y líderes de restitución que han retornado, situación que se viene presentando desde 2008, y acciones como los crímenes contra Alex Miguel Arrieta, la quema de 10 viviendas en el año 2011, el atentado perpetrado contra el líder Andrés Narvaez en 2003, y recientemente con los panfletos amenazantes y el [paro armado](https://archivo.contagioradio.com/comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas/) promovido por las ‘Autodefensas Gaitanistas’.

Con la denuncia, se hace un llamado a las organizaciones encargadas de proteger los derechos de los miembros de las asociaciones de campesinos, con la inminente presencia de grupos paramilitares en la región caribe y particularmente en el Departamento de Sucre, en municipios como Palmitos, Carmen de Bolívar y Morroa, territorios que históricamente han sufrido la violencia de estas agrupaciones.
