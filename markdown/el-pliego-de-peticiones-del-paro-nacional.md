Title: El pliego de 15 puntos que lleva el movimiento social al Paro Nacional
Date: 2016-03-15 11:21
Category: Movilización, Paro Nacional
Tags: central unitaria de trabajadores, Cumbre Agraria, Paro Nacional Marzo 17
Slug: el-pliego-de-peticiones-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/DSC1707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### <iframe src="http://co.ivoox.com/es/player_ek_10811910_2_1.html?data=kpWlk5addZGhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYa3k4qvqLXTtozl1oqwlYqlfYzhwtfQysbWb8bgjJakjcnJb67V09%2Fcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Fabio Arias, CUT] 

###### [15 Mar 2016] 

Este 17 de marzo el Movimiento Social se pone cita para salir a las calles con el pliego de exigencias que da paso el PARO Nacional. Entre los diferentes sectores que confluirán en la movilización están la Central Unitaria de Trabajadores, La Cumbre Agraria, diferentes organizaciones estudiantiles, organizaciones gremiales, medioambientales, campesinas, indígenas y otras 46 organizaciones sociales del país.

Fabio Arias, presidente de la Central Unitaria de Trabajadores comenta que *“*El pliego consulta las necesidades más sentidas de la población y este es un buen motivo para que el gobierno nacional entienda que tiene que atender las inquietudes e inconformidades que tiene la gente, vamos a respaldar la paz pero también vamos a reclamarle al gobierno solución a nuestras peticiones*”.*

El pliego de exigencias que abandera el PARO Nacional y la movilización del 17 de marzo se construyó a partir de las inconformidades que presenta el movimiento social con los diferentes incumplimientos del gobierno frente a temas como la venta de ISAGEN, la firma de TLC con diferentes países, el aumento del desempleo, la crisis hospitalaria, y en general la falta de garantías laborales, económicas y sociales en las que se encuentra el país.  El pliego se  conforma por 15 puntos:

1.  Desarrollar un programa estructural con medidas de emergencia para el pueblo guajiro y otros más donde la hambruna y la muerte de la niñez, es una vergüenza para Colombia.

<!-- -->

2.  Cumplimiento de los acuerdos firmados entre el gobierno y las diferentes organizaciones representativas de los trabajadores, pensionados, campesinos, afros, estudiantes, indígenas, transportadores y empresarios agrarios.

<!-- -->

3.  Frente a la carestía de la vida, demandamos alza general de salarios y del subsidio de transporte, con equidad salarial para las mujeres y reducción de las tarifas de los servicios públicos y el transporte. Disminución del costo de la canasta familiar y el establecimiento de mínimos vitales.

<!-- -->

4.  Defensa del patrimonio público, especialmente de Ecopetrol (construcción del PMRB, no a la venta de las filiales ni al cierre de campos petroleros) y las empresas públicas territoriales, entre otras: Aseo, ETB, EAAB, EEB, Emcali y la participación del Estado territorial en ISA, ISAGEN.

<!-- -->

5.  Evaluación y revisión de los Tratados de Libre Comercio. Defensa de la producción nacional, tanto agropecuaria como industrial, mediante mecanismos arancelarios, tributarios, financieros, (condonación parcial de deudas y rebaja de intereses para los afectados por el verano), entre otros. Rechazo a la importación de alimentos con arancel cero y el desmonte de las licoreras.

<!-- -->

6.  No a la privatización de la salud y la educación a cargo del Estado y demandamos  Incremento presupuestal para estos sectores. Dignificación de la salud de los y las docentes y pago de deudas laborales. Reforma universitaria democrática y concertada y la condonación de deudas de estudiantes con el ICETEX y fortalecimiento presupuestal del SENA.

<!-- -->

7.  Promoción del empleo en condiciones de trabajo digno y decente, política de formalización laboral y ampliación de las plantas de personal en el sector público y privado, con quienes cumplan funciones permanentes y misionales, así como protección del derecho al trabajo y acceso al espacio público para los informales.

<!-- -->

8.  Promover un ordenamiento territorial con base a la protección del medio ambiente, el agua como un derecho humano fundamental y los ecosistemas estratégicos esenciales para la vida. Exigimos la moratoria minera, los acatamientos a los fallos de la Corte Constitucional, la consulta previa vinculante y decisoria para los proyectos minero- energéticos y prohibirlos en los cascos urbanos.

<!-- -->

9.  Reducción de los precios de los combustibles y congelamiento de peajes, así como también, impedir la cesión de corredores viales.

<!-- -->

10. La reforma tributaria no puede ser de carácter regresivo, por consiguiente, se elimine las exenciones tributarias a las multinacionales y no se aumente el IVA, ni se amplié su base gravable, ni se impongan cargas tributarias a la clase trabajadora y pensionados y se elimine el 4x1000.

<!-- -->

11. Defensa de la tierra y los territorios para la producción agropecuaria del campesinado y el reconocimiento de sus derechos. Fortalecimiento de la economía propia. Sustitución concertada y gradual para los pequeños productores de cultivos de uso ilícito.

<!-- -->

12. Garantía plena de los derechos humanos, libertades sindicales y no criminalización ni judicialización de la protesta social y reparación colectiva. Desmonte del ESMAD.

<!-- -->

13. Fortalecimiento del régimen de Prima Media (Colpensiones) y no realizar modificaciones regresivas al régimen pensional. Pleno reconocimiento de los derechos prestacionales de militares y policías en retiro.

<!-- -->

14. Disminución de las tasas de interés acabando la especulación financiera, protegiendo al sector de la economía solidaria (cooperativas, cajas de compensación).

<!-- -->

15. Definir una política pública concertada de lucha contra la corrupción y la impunidad, que tenga en cuenta el fortalecimiento de los órganos de control y la justicia.

La movilización tendrá diferentes puntos de concentración nacionales.

Bogotá: Parque Nacional, Universidad Nacional,  Centro Administrativo Nacional, Calle 72 con carrera Séptima, Hospital La Hortúa, localidades como Ciudad Bolívar, Usme, Suba y el municipio de Soacha contarán con puntos de concentración.

Medellín: Concentración será en Carabobo entre los sectores Jardín Botánico y Parque explora, y harán un recorrido hasta La Alpujarra.

Cali: Concentración en el parque de Las Banderas y recorrido hasta la gobernación.

Barranquilla: Concentración en la Plaza de la Paz y recorrido hasta la gobernación.

Cartagena: Concentración en Universidad de Cartagena sede Piedra de Bolívar y recorrido por la avenida principal hasta llegar al centro de la ciudad.

Ibagué: Concentración en el parque Galarza y recorrido hasta la gobernación.

Bucaramanga: Concentración en la Plazoleta Luis Carlos Galán y recorrido hasta la gobernación.

Villavicencio: Concentración en el Parque de los Fundadores y recorrido hasta la gobernación.

Ocaña: Concentración en la plazuela del complejo histórico y recorrido hasta el parque San Agustín realizando un pare en la plaza del 29 de Mayo.

Tunja: Concentración plaza de Bolívar y recorrido por la Avenida Norte hasta la entrada de Combita.

Manizales: Concentración en el parque Antonio Nariño y recorrido por la Avenida Santander hasta la Plaza de Bolívar.

Armenia: Concentración en el parque Los Fundadores de Armenia y recorrido por vías principales hasta llegar a la gobernación.

Neiva: Concentración en el parque Lesburg y recorrido por vías principales hasta el parque Santander frente a la gobernación.

Yopal: Concentración en el parque Resurgimiento y recorrido por vías principales.

\
