Title: Discurso de Iván Marquez tras nuevo acuerdo de paz
Date: 2016-11-14 13:12
Category: Nacional, Paz
Tags: acuerdo, conflicto armado, FARC, La Habana, paz
Slug: discurso-ivan-marquez-tras-nuevo-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Ivan-Marquez-e1459366203225.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [12 Nov 2016]

Discurso Delegación de Paz Farc-ep

Nuestro saludo cordial al Ministro de Relaciones Exteriores de Cuba, Bruno Rodríguez, y por su intermedio, a los comandantes Fidel y Raúl que generosamente ofrecieron su territorio y la logística necesaria para que los colombianos resolviéramos nuestras diferencias; Saludo a los representantes de los países garantes, Noruega y Cuba que estuvieron siempre al lado de los plenipotenciarios en los momentos más difíciles y los de satisfacción impulsando contra viento y marea los avances de la Mesa; Saludo a los “ángeles de la guarda” del proceso de paz, como denominara Roy Chaderton a los acompañantes de la República Bolivariana de Venezuela y de la República de Chile. A nombre de Colombia, gracias por ayudarnos durante tanto tiempo a encontrar la senda de la reconciliación y la paz. Gracias por su inmensa solidaridad.

Doctor Humberto de La Calle y representantes del Gobierno de Colombia

Compañeros de la Delegación de Paz de las FARC

Compatriotas:

Hemos trabajado de manera incansable día y noche para entretejer en un solo cuerpo de consenso los elementos del nuevo acuerdo final sobre el que empezaremos la edificación de la paz estable y duradera para Colombia.

En el día de hoy, como resultado de la inquebrantable voluntad y decisión de las partes, de su perseverante búsqueda de una solución política a la dilatada confrontación armada, presentamos a la nación colombiana el nuevo Acuerdo de Paz DEFINITIVO, que preferimos llamar el ACUERDO DE LA ESPERANZA, poderoso instrumento para la democratización del país y para la materialización de los derechos de la gente.

Han sido semanas de arduo trabajo, de audiencias sucesivas, de ejercicio humilde de escuchar con interés y respeto, de interlocución con el más amplio espectro del movimiento social y político, y de las iglesias. Han sido semanas de muy difíciles pero fructíferas conversaciones con la delegación gubernamental que han permitido reafirmar la vocación de paz y reconciliación de un país, al tiempo que han posibilitado comprender y aclarar dudas e inquietudes razonables de diversos sectores de la sociedad, y también desvirtuar tergiversaciones y falacias respecto del acuerdo suscrito el pasado 26 de septiembre en la Ciudad Heroica, Cartagena de Indias.

El resultado del plebiscito del 2 de octubre produjo un impacto, que además de atascar los mecanismos de implementación previstos, pusieron en serio riesgo cinco años de esfuerzos en la búsqueda de la reconciliación.

Pero para fortuna de millones de compatriotas, la paz sigue su marcha irrefrenable, IRREFRENABLE. Las espontáneas y multitudinarias movilizaciones sociales de apoyo a la paz activadas por la juventud, las numerosas manifestaciones de organizaciones sociales y populares, de partidos y movimientos políticos, y el reiterado acompañamiento de la comunidad internacional, nos incitan pensar que se ha iniciado el gran cambio histórico hacia una sociedad auténticamente democrática, pacífica y justa.

Los resultados de octubre también nos llevaron a pensar en sentido más reflexivo acerca de los límites de la democracia liberal y de la regla mayoritaria, particularmente cuando ésta se establece bajo las condiciones de mayúscula abstención y cuando están de por medio decisiones de semejante trascendencia, como las que conciernen a la posibilidad histórica del buen vivir y en paz para las nuevas generaciones.

Recordamos, en ese contexto, nuestras visiones de democracia discutidas en la Mesa, que apenas alcanzaron a ser recogidas de manera muy parcial en el acuerdo sobre “apertura democrática”. Y reforzamos nuestras certezas acerca de la necesidad de luchar por nuevos entendimientos, diseños y procedimientos democráticos que trasciendan el simple sufragio, a fin de garantizar una verdadera participación social y ciudadana en los asuntos que inciden sobre la vida nacional.

Aún en condiciones de esa precariedad democrática, en la medida en que accedimos al procedimiento de refrendación plebiscitaria en la búsqueda de la paz deseada, entendimos que pese a la débil mayoría, teníamos el compromiso político de aceptar el resultado adverso y de atender las múltiples voces que lo habían propiciado, distinguiendo entre aquellas que manifestaban preocupaciones sinceras por un nuevo y mejor acuerdo, y otras que tenían la pretensión de erigirse en obstáculo para el logro del propósito superior, aunque dada la coyuntura se cobijaran también con el manto y el discurso de la paz.

Comprendimos mejor, entonces, el sentido del pluralismo y de la diversidad, de los intereses particulares y de grupos y sectores específicos del conglomerado social, que -en medio de la conflictividad que es inherente al orden vigente- merecen respeto y reconocimiento. El mismo que demandamos hacia nosotros.

Así es, que, en atención a ello, nos dimos a la tarea, junto con el Gobierno Nacional, de reabrir la discusión en la Mesa de La Habana, para considerar -con el rigor exigido por las circunstancias- todas y cada una de las propuestas de precisión y ajuste del acuerdo suscrito el 26 de septiembre, bajo el entendido de que se trataba de un perfeccionamiento de lo ya convenido, teniendo en cuenta lo manifestado por todos los sectores políticos y sociales que participaron en un plebiscito cuyos resultados no debían comprenderse como una negativa al acuerdo de paz, sino como un llamado a su mejoramiento.

Aunque la X Conferencia Nacional Guerrillera refrendó el texto de un acuerdo que para nosotros ya estaba cerrado, comprendimos la importancia de reformularlo con un mayor consenso que incorporara muchas voces que estuvieron ausentes durante el proceso de conversaciones, y que incluso habiendo dicho No tienen su sentimiento del lado de la reconciliación.

Somos conscientes de que la posibilidad real de desatar la potencia transformadora de los acuerdos descansa sobre su legitimidad política y social. Por tal razón, nos produce una inmensa satisfacción anunciar que al tiempo que el nuevo Acuerdo preserva la estructura y espíritu del primer acuerdo convenido, incorpora un sinnúmero de ajustes y precisiones, que recoge observaciones y nuevas propuestas de redacción, que formula aclaraciones y despeja dudas donde se consideró necesario. Por ejemplo, en materia de Jurisdicción Especial para la Paz se incorporaron no menos del 65% de las propuestas provenientes de los diversos sectores que votaron NO en el plebiscito; casi el 90% de las iniciativas referidas al polémico tema de género; y algo más de 100 variaciones que tocan los temas concernientes a Reforma Rural Integral, Participación Política, Nueva política antidrogas, Víctimas, Fin del conflicto e implementación y verificación.

Al respecto, de nuestra parte hemos cedido, incluso extendiendo fronteras que nos habíamos trazado, desplazándolas hasta los límites de lo razonable y aceptable para una organización político-militar cuyas armas no fueron vencidas; que acudió por tanto a la mesa de diálogos a una negociación y no a un proceso de sometimiento; y que ha tomado la decisión de participar en la vida política legal, si se cumple un conjunto de condiciones que lo hagan posible.

Le decimos a la sociedad colombiana que hemos realizado nuestro mejor esfuerzo por responder a los anhelos de paz, y podemos afirmar con la frente en alto que hemos cumplido y que al nuevo acuerdo, el único camino que le espera, es su implementación, teniendo en cuenta que con él quedan sentadas las bases para comenzar una tarea aún más difícil y compleja: la construcción de una paz estable y duradera, a la que esperamos se puedan sumar, con nuevos aportes, fruto de su negociación, los compañeros del Ejército de Liberación Nacional, y que toda la institucionalidad del Estado, el poder Ejecutivo, el Congreso de la República, la altas Cortes, la Fiscalía General de la Nación, las Fuerzas Militares y de Policía, asuman su respaldo.

Con el nuevo Acuerdo Final se generan condiciones para iniciar el difícil proceso de la reconciliación nacional, propósito que compromete a las diferentes clases sociales, al empresariado nacional, a los sectores medios de la población, a la clase trabajadora urbana y rural, a los intelectuales y artistas, a los trabajadores de la cultura, a las comunidades campesinas, indígenas y afrodescendientes, a la comunidad LGBTI, a las mujeres y hombres del común, y desde luego a nuestros guerrilleros, que con expectativa han estado en paciente y disciplinada espera; y también a todos los partidos y movimientos políticos y sociales.

La firma de este nuevo acuerdo debe dar inicio a la construcción del país de la concordia que llevamos en el corazón y con el que hemos soñado durante toda la vida. Pero el solo acuerdo no es suficiente, porque un papel florecido de promesas y buenas intenciones, sin veeduría ciudadana, fácilmente puede ser arrastrado por el viento de la desidia hacia el desierto de la nada y la frustración de la esperanza. Lo reiteramos: el principal garante del cumplimiento y la implementación de los acuerdos, además del componente internacional, es el propio pueblo y sus organizaciones, porque nadie mejor que él puede sentir la urgencia de su concreción, porque toca con su dignidad y su derecho a vivir en paz.

Está triunfando la paz, no lo dudamos. Nos sentimos orgullosos de que Colombia siga siendo referente mundial de paz.

Reiteramos el llamado que hiciéramos con el Gobierno a concertar con todas las fuerzas vivas del país, “un gran ACUERDO POLÍTICO NACIONAL encaminado a definir las reformas y ajustes institucionales necesarios para atender los retos que la paz demande, poniendo en marcha un nuevo marco de convivencia política y social”.

Es tiempo de paz; de despliegue de una contienda política que admita, sin el ejercicio estructural de la violencia y el recurso de las armas, la existencia de diversas visiones de sociedad y en la que se pueda luchar por ellas por la vía democrática. Que la contienda se traslade al campo de las ideas enarbolando en lo más alto de las conciencias, la bandera de la verdad y de la honradez.

Que nadie trunque los sueños y las esperanzas de millones de almas. Hagamos de la paz una condición estable y duradera sobre la base del respeto a los derechos del pueblo y la justicia social. Nuestro futuro está fuertemente ligado al derecho a la tierra, a la salud, a la educación, a la vida digna, para que nadie en Colombia recaiga en la desesperanza. Ha llegado el momento de la construcción de sueños y de darle vida a la esperanza mediante la lucha política, llenando nuestros corazones del más inmenso amor por la patria.

Con la voz del tribuno del pueblo Jorge Eliécer Gaitán, digamos una vez más: “Bienaventurados los que entienden que las palabras de concordia y de paz no deben servir para ocultar sentimientos de rencor y exterminio...”.

Que renazcan, entonces, de la luz de este Acuerdo de Paz, las mariposas amarillas de Mauricio Babilonia para que invadan con su amor y reconciliación todos los rincones de este gran Macondo que es Colombia.

Amamos la paz, amamos a Colombia. Que Dios y el comandante Manuel Marulanda Vélez, bendigan este acuerdo.

Muchas gracias.

DELEGACIÓN DE PAZ DE LAS FARC-EP  
\[20:17, 12/11/2016\] Danilo: ALOCUCIÓN PRESIDENTE JUAN MANUEL SANTOS

Compatriotas,

Hace 40 días, el 2 de octubre, apenas se publicó, reconocí el resultado del plebiscitoen el que el No obtuvo la mayoría de los votos.

Pero ese resultado no podía sepultar la esperanza de paz.

Ese resultado, en vez de paralizar el país y ahogarnos en la incertidumbre, teníamos que convertirlo en una gran oportunidad para unirnos alrededor del deseo de pazexpresado por todos, independientemente de si votamos Sí o No ese día.

Esa fue mi reacción y por esa razón inicié desde esa noche un gran diálogo nacional por la unión y la reconciliación.

El objetivo era claro: Escuchar. Escuchar las voces de todos los colombianos, recoger sus esperanzas y sus preocupaciones sobre el acuerdo.

Recoger también sus propuestas de ajustes y cambios para lograr un nuevo, un mejor acuerdo de paz con las FARC.

Era necesario llegar a un acuerdo – fortalecido con esos ajustes y cambios—que reflejara el sentir de la inmensa mayoría de nuestros compatriotas y poder construir así una paz más amplia, más profunda.

Era indispensable además, lograr este acuerdo renovado muy rápido. El cese al fuego es frágil. La incertidumbre genera temores y aumenta los riesgos de echar este inmenso esfuerzo al traste.

Había que trabajar sin descanso, con dedicación y método para tener ese acuerdo que recogiera las aspiraciones de los colombianos sin poner en riesgo todo lo logrado durante seis largos años de negociaciones.

Así lo hicimos. Trabajamos duro, con honestidad, con generosidad, confranqueza y con apertura mental y de espíritu tanto en Colombia como en La Habana. Todos los días. Largas horas.

Recibimos más de 500 propuestas de todos los sectores: sociales, religiosos, víctimas, partidos políticos. Se agruparon en 57 temas para la discusión con las FARC.

Todos, absolutamente todos, fueron discutidos a profundidad con las FARC y defendidos por la delegación del gobierno con total lealtad y fidelidad a lo expresadopor los diferentes sectores.

Mantuvimos informados permanentemente a los principales voceros del No sobre los avances y dificultades de este ejercicio.

\*\*

Las últimas 48 horas fueron especialmente intensas. Se trabajó en jornada permanente. Con grandes resultados.

Logramos precisiones, ajustes y cambios en 56 de los 57 temas abordados.

El Centro Democrático, algunos dirigentes conservadores que votaron No, los partidos de la coalición para la paz, la Iglesia, las Altas Cortes y magistrados, las organizaciones religiosas y sociales, los empresarios, los cientos de miles de jóvenes que se movilizaron, los sindicatos, las comunidades indígenas y afrodescendientes, las víctimas, los militares retirados, los movimientos de mujeres y los que reiteraron su apoyo al acuerdo, todos, aportaron sus ideas y propuestas para ajustar el acuerdo.

A todos, GRACIAS.

Sus iniciativas contribuyeron a lograr este nuevo acuerdo que ahora es de todos. ¡DE TODOS!

¡Que bueno! Porque la paz es de todos.

A los negociadores del Gobierno Nacional y a los de las FARC, también un especial reconocimiento.

Su disciplina en el trabajo y su disposición a escuchar y reconocer las ideas distintaspermitieron desbloquear las negociacionesy encontrar soluciones.

Este acuerdo fortalecido con los aportes ciudadanos lo vamos a divulgarampliamente a partir de mañana para que sea conocido por todos.

Hoy me he reunido con el Ex Presidente Uribe en Rionegro por tres horas, y he hablado varias veces con el Ex Presidente Pastrana y con la Ex Ministra Martha Lucía Ramírez. Todos los voceros del NO recibirán los textos a más tardar mañana.

Desde ya quiero destacar los cambios más importantes que se hicieron:

Uno de los temas que más reclamaron los colombianos era que las FARC entregaran sus bienes y la plata que tengan disponiblepara reparar las víctimas. Eso se logró.

En el nuevo acuerdo, las FARC tendrán que declarar y entregar todos sus bienes, so pena de perder los beneficios, y se usarán para reparar a las víctimas.

Un reclamo generalizado de los del No y los del Sí era que se definiera en qué consistía la restricción efectiva de la libertad, pues fue criticado como impreciso.

Eso se logró.

El Tribunal debe fijar en cada caso:

1\) Los espacios concretos en donde deben estar los sancionados durante la ejecución de la pena (que nunca serán más grandes que una Zona Veredal Transitoria de Normalización)  
2) Los horarios en los que deben cumplir las sanciones restaurativas  
3) Establecer el sitio de residencia durante la ejecución de la sanción  
4) Imponerles el deber de solicitar autorización para salir de las zonas donde cumplan la sanción y  
5) Señalar la periodicidad con la que el órgano de verificación debe reportar sobre el cumplimiento de la sanción.

Se estableció además, y eso fue otra petición de partidarios del no, que el tiempo que pasen en las zonas veredales Transitorias de Normalización, se les tendrá en cuenta como parte de la sanción, siempre y cuando en ese período desarrollen actividades de reparación.

Escuchamos los comentarios válidos de varios sectores, incluyendo a nuestras altas Cortes para mejorar y articular la justicia transicional con nuestro sistema judicial ordinario.

Una petición expresa era que se pusiera un límite de tiempo a la Jurisdicción Especial de Paz. Eso se logró. Funcionará hasta por 10 años y sólo podrán recibir solicitudes de investigación durante los 2 primeros años.

También se estableció que las ONG no podrán actuar como fiscales y acusar. Sólo presentar información que será valorada y contrastada por los jueces y magistrados del Tribunal.

Otro tema que exigieron muchos de los del No era que no hubiera jueces extranjeros. Eso también se eliminó. Todos serán colombianos y tendrán las mismas calidades de los magistrados de nuestras cortes.

Otra controversia era el manejo de las tutelas frente a las decisiones de la Justicia Especial para la Paz.

Ahora como parte de la articulación con las otras jurisdicciones, las tutelas contra decisiones de la JEP podrán a ser revisadas.

Una de las principales preocupaciones del Centro Democrático era el respeto a la propiedad privada, a la iniciativa privada y que a nadie se expropie por fuera de la ley ya vigente. Todo eso se logró y se ratificó, como lo pidió expresamente el ex presidente Uribe. Que no queden dudas: ¡Se respetará el derecho a la propiedad!

Han surgido algunas inquietudes sobre la legislación agraria que no hacen parte del Acuerdo de Cartagena, pero que sabemos que deben ser atendidas. Por tal razón, decidimos crear una comisión de expertos para revisar esos temas.

El catastro –fundamental para formalizar la tierra—no modificará por sí mismo los avalúos de las tierras.

Dejamos absolutamente claro que en virtud de este acuerdo no se autorizaran nuevas zonas de reserva campesina, más allá del trámite normal de acuerdo a la legislación vigente que hay sobre estas zonas.

Varios grupos de empresarios expresaron su preocupación por el posible impacto de las inversiones del pos conflicto en la estabilidad macro económica. Para dar tranquilidad, se incluyó expresamente que la implementación se hará con respeto al principio de sostenibilidad fiscal, y se amplió de 10 a 15 años el plazo de implementación para reducir la presión fiscal, si es que la hubiera, y no afectar de manera alguna los programas prioritarios del gobierno.

Otra preocupación de los empresarios era que se desatara una posible cacería de brujas en la aplicación para ellos de la justicia transicional. Ese temor quedó totalmente disipado y los empresarios satisfechos. Los que no sean responsables de crímenes graves tienen la posibilidad de obtener la terminación de los procesos que hoy los puedan afectar en la justicia ordinaria.

Muchos sectores, en particular de militares retirados, veían con preocupación el tratamiento de agentes del Estado en la Justicia Especial de Paz. Este temor se resolvió. Logramos una fórmula que garantiza a nuestros soldados y policías, en servicio activo y retirados, los máximos beneficios, pero con total seguridad jurídica. Esta solución dejó tranquilos a todos. Es lo mínimo que podíamos hacer por ellos y había sido un compromiso personal mío.

Una queja reiterada de los voceros del No era que las 16 curules transitorias en la cámara de representantes, establecidas para las comunidades y víctimas afectadas por el conflicto, serían para las FARC.

En el nuevo acuerdo se incluyó expresamente que el partido que surja de la reincorporación de las FARC NO podrá, repito, NO podrá inscribir candidatos para esos espacios.

Esta modificación dejó satisfechos a muchos de los voceros del NO que habían expresado preocupación por este tema.

Por solicitud de los diferentes sectores políticos del Sí y del No, se redujo desde el primer año en 30% la financiación al partido de las FARC para que quede en igualdad de condiciones con los demás partidos.

En este mismo capítulo, quedó claro que la protesta social debe ser siempre pacífica, y que el Estado tiene la obligación de proteger los derechos de todos los ciudadanos.

Uno de los temas más delicados en todos los acuerdos de paz, es el de la protección de los reincorporados a la vida civil. En Colombia hemos sufrido especialmente ese drama. Por eso en el acuerdo se creó una comisión de protección y garantías de seguridad, en la que tenían participación las FARC.

Su presencia generó preocupación en muchos promotores del no. En el nuevo acuerdo, y siguiendo una recomendación de la doctora Marta Lucía Ramírez, se eliminó la participación de las FARC en esa comisión.

Se eliminó también las facultades de esa comisión para revisar hojas de vida, o ejercer facultades de inspección y vigilancia sobre las empresas de seguridad privada.

Sobre la lucha contra el problema de las drogas, el nuevo acuerdo obliga a todos los que se presenten a la Justicia especial de Paz a entregar toda la información relacionada con el narcotráfico de manera exhaustiva y detallada para atribuir responsabilidades. En este tema fue particularmente insistente el ex presidente Pastrana.

Se reiteró y subrayó también que el gobierno mantiene todas las herramientas para la erradicación, incluyendo la fumigación, además de los programas de sustitución para los campesinos.

No habrá formalización de ningún predio en Colombia sin que antes se constate que está libre de la presencia de cultivos ilícitos

Para atacar de manera más efectiva el problema del consumo de drogas, se robusteció el papel de la familia y de los grupos religiosos en la política de prevención y atención a los consumidores.

La idea de incorporar todo el acuerdo al bloque de constitucionalidad generó mucho rechazo en los expresidentes Pastrana y Uribe, en Marta Lucía Ramírez, y en muchos voceros del No y algunos del Sí.

Confieso que tenían razón, porque además generó muchas malas interpretaciones sobre el Acuerdo.

Eso se corrigió. Sólo quedarán los temas de derechos humanos y de Derecho Internacional Humanitario, que ya de por si hacen parte de la constitución.

La implementación del acuerdo fue otro tema sobre el que recibimos comentarios y propuestas. A algunos les preocupaba que hubiera una especie de co-gobierno con las FARC para la implementación.

Ha quedado ahora mucho más claro que el gobierno será el único – el ÚNICO— responsable de la implementación. Habrá, eso sí, una comisión que hará seguimiento, impulso y verificación del cumplimiento de los acuerdos.

Una preocupación que compartieron muchos colombianos, y en particular la iglesia y organizaciones religiosas, fue que el acuerdo de paz pudiera contener elementos de la llamada ideología de género y se afectaran los valores de la familia.

Pues bien, ese tema fue revisado con sumo cuidado por la Iglesia Católica, por los pastores cristianos y otros voceros del No.

Se hicieron las modificaciones para garantizar que la llamada ideología de género no está presente – nunca lo estuvo— ni siquiera de manera sugerida.

Lo que sí se dejó claro es que este capítulo busca garantizar que las mujeres, que han sufrido especialmente este terrible conflicto, sean tratadas con prioridad y que sus derechos como víctimas estén totalmente protegidos.

Se incorporaron en el nuevo acuerdo los principios de igualdad y no discriminación,de libertad de cultos y se reconoció a la familia y a los líderes religiosos como víctimas del conflicto. Esa fue otra solicitud expresa.

Estos son algunos de los principales cambios realizados. Pero como ya lo señalé, en 56 de los 57 temas hubo cambios y mejoras.

Un punto que reclamaban muchos de los del No era que los jefes guerrilleros no pudieran ser elegidos.

Yo entiendo que este es el sentir de muchos ciudadanos. En la mesa de La Habana los negociadores del gobierno insistieron mucho en ese punto para responder a esa preocupación.

Tengo que decirlo con franqueza. Aquí no se logró avanzar.

Es muy importante que los colombianos entendamos que la razón de ser de TODOS los procesos de paz en el mundo es precisamente que los guerrilleros dejen las armas y puedan hacer política dentro de la legalidad.

Este proceso con las FARC no es una excepción, ni puede serlo.

Las FARC tienen un origen político y su intención hacia el futuro es poder hacer política sin armas.

Muchos de ustedes recuerdan que en 1990, en el acuerdo con el M-19, sus líderes salieron directamente de la mesa de negociación a participar en las elecciones.

En El Salvador y en muchos otros países, los guerrilleros fueron congresistas desde la elección siguiente a la firma de los acuerdos.

En la constitución del 91, los artículos 12 y 13 transitorios le daban la posibilidad al presidente de nombrar a dedo a ex guerrilleros en el congreso. Eso no ocurre en este acuerdo.

En otros países, como en Irlanda del Norte, entraron a cogobernar desde la firma del Acuerdo. La semana pasada estuve en ese país reunido con la primera ministra que es protestante y con el viceprimer ministro que es católico. Eso fue parte del acuerdo. Esto no ocurre en este acuerdo.

Para ser claro. No tendrán curules a dedo. Al contrario, tendrán que participar en las elecciones. Tampoco tendrán cargos en el gobierno, como ha ocurrido en muchos otros casos. Pero si podrán ser elegidos

Colombianos,

El acuerdo que se firmó el 26 de septiembre en Cartagena era, según los estudiosos del tema, como el Instituto Kroc de la Universidad de Notre Dame, uno de los mejores –sino el mejor y más completo-- que se ha firmado en la historia recientepara resolver un conflicto armado.

Pero, con toda humildad, quiero reconocer que este nuevo acuerdo es un mejor acuerdo.

Mirando para atrás, el resultado del plebiscito nos abrió la oportunidad de unirnos y quiero agradecer nuevamente la buena disposición y la buena voluntad con la que participaron todos los voceros, en particular los del No.

Este acuerdo de paz con las FARC retoma y refleja las propuestas, y las ideas de todos los que participaron en este gran diálogo nacional. Responde y aclara las preocupaciones que muchos tenían frente a la letra, el sentido del acuerdo o su implementación.

Este acuerdo, renovado, ajustado, precisado y aclarado debe unirnos, no dividirnos.

Esa es mi invitación. A que nos unamos, así el acuerdo no satisfaga todas las aspiraciones de todos los sectores.

Invito a todos los colombianos, a los promotores del Sí y del No, a que le demos una oportunidad a la paz con este nuevo acuerdo.

Es lo que el pueblo colombiano nos está pidiendo y lo que nos pide la comunidad internacional. Por eso he dado instrucciones al Dr. Humberto de la Calle y al equipo negociador, a que regresen de inmediato a Bogotá para que les expliquen en detalle y con los textos en la mano, a los voceros del NO, el alcance de lo acordado y reciban sus reacciones.

Hemos trabajado juiciosamente y espero que ese trabajo satisfaga a los del NO y a la nación.

Además de lograr el fin del conflicto y de la violencia con las FARC, este acuerdo busca que la paz que construyamos de hoy en adelante sea no sólo la paz del silencio de las armas, sino la paz de la reconciliación y el respeto por la diferencia.

Que sea la paz que nos permita unirnos como nación y tomar a manos llenas las oportunidades que la tranquilidad, la seguridad y la unión nos abren.

¡Es la hora de la unión y la reconciliación!  
¡Es la hora de dejar atrás las divisiones!

¡Es la hora de sumar voluntades y unir esfuerzos para construir juntos la paz!

Muchas gracias
