Title: Innovadora herramienta para buscar personas desaparecidas en Colombia
Date: 2015-11-12 12:24
Category: DDHH, Entrevistas
Tags: Asociación Familiares Colombia, Búsqueda de desaparecidos en Colombia, Desaparición en Colombia, Desaparición forzada, EQUITAS, MESP, Modelamiento Espacial Estadístico Predictivo, paramilitarismo en Colombia
Slug: innovadora-herramienta-para-buscar-personas-desaparecidas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/radar-para-buscar-personas-desaparecidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio ] 

<iframe src="http://www.ivoox.com/player_ek_9366066_2_1.html?data=mpijmJWaeo6ZmKiak5aJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Fi0NvOxtTWpYzcxtffw9LNqc%2FowpDdw9fFb8Pp1MjO1JDUqdPn0NPO1ZDIqdTV0cbfx8jNqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Arango, EQUITAS] 

###### [12 Nov 2015 ] 

[El Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial EQUITAS en articulación con la Asociación Familiares Colombia, lanzan el Modelamiento Espacial Estadístico Predictivo MESP, una herramienta de mapeo que permitirá **localizar con un nivel de precisión cercano al 89% las fosas clandestinas** en las que se presume podrían hallarse las cerca de 122 mil personas que según cifras oficiales actualmente están desaparecidas en el país.]

[De acuerdo con Diana Arango, directora de EQUITAS, esta herramienta es **producto 4 años de arduo trabajo** en los municipios casanareños de Recetor y Chámeza, en los que fue reportada la **desaparición de 150 personas** a manos del grupo paramilitar ‘Autodefensas Campesinas de Casanare’. Esta labor de investigación consistió en **recolectar información con las comunidades afectadas** para hallar datos claves del accionar paramilitar en la zona.]

[Los datos hallados fueron cruzados con información suministrada por paramilitares y organismos estatales como La Fiscalía para crear mapas que permitieron **georeferenciar acciones paramilitares** y que determinaron la **eficacia de esta herramienta en otras regiones colombianas**, precisa Arango. El MESP funciona como un radar, creado a partir de **hallazgos internacionales perfeccionados para el contexto colombiano**, incorporando la cartografía existente.]

[El lanzamiento de la herramienta es un llamado a instituciones como La Fiscalía y Medicina Legal para que persistan en la **innovación para la búsqueda de personas desaparecidas**, y a la sociedad civil para que **acompañen de forma permanentement**e este proceso, agrega Arango. El evento se llevará a cabo hoy  a las 4:30 pm en el Hotel Suite Jones ubicado en la Calle 61 No. 5-39. ]

Conozca más sobre el MESP:

[[![INFOGRAFIA MESP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/INFOGRAFIA-MESP.jpg)

Cartilla: [Un radar para encontrarlos](https://es.scribd.com/doc/289476071/Un-radar-para-encontrarlos "View Un radar para encontrarlos on Scribd")

<iframe id="doc_82092" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/289476071/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

[Vea también: [[99.9% de impunidad en casos de desaparición forzada](https://archivo.contagioradio.com/casos-de-desaparicion-forzada-en-colombia-cuentan-con-99-9-de-impunidad/) ]]
