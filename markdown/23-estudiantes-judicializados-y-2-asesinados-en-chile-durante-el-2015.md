Title: 23 estudiantes judicializados y 2 asesinados en Chile durante el 2015
Date: 2015-10-07 15:54
Category: El mundo, Judicial
Tags: Asesinatos en Chile, Chile, Comisión Contra la Tortura en Chile, Movimiento estudiantil en Chile, Represión estudiantes en Chile, Violaciones a derechos humanos en Chile
Slug: 23-estudiantes-judicializados-y-2-asesinados-en-chile-durante-el-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/A_UNO_249761.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 24horas.cl] 

<iframe src="http://www.ivoox.com/player_ek_8843867_2_1.html?data=mZ2hlZ2ae46ZmKiak5WJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo1snWw9PYqdSfxM3WzsrSs9Sfxcrb19PHrcLijMbgx9jNssLo0NiSlKiPqMboxtPQy9TSqdSfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Enrique Núñez, Comisión Contra la Tortura Chile] 

###### [07 Oct 205] 

[En el marco de las jornadas de protesta adelantadas por el movimiento estudiantil chileno, **23 estudiantes han sido detenidos arbitrariamente, 2 han sido asesinados, cientos heridos y otros tantos desaparecidos**, como resultado de la represión ejercida por efectivos de Carabineros de Chile.]

[Enrique Núñez, integrante de la Comisión Contra la Tortura en Chile, afirma que es inaceptable la  represión contra el movimiento estudiantil pues sus reivindicaciones son legítimas, exigen **mayor financiación a la educación pública y la creación de becas para que los sectores más empobrecidos accedan a la educación superior**.]

[Núñez asegura que el movimiento estudiantil fue el primero en llamar la atención sobre el control que ejercen los **sectores empresariales** en Chile vinculados con **hechos de corrupción** y que han permeado la vida política a través de la **financiación de partidos**.]

[El que los efectivos de Carabineros sean juzgados por la Fiscalía Militar y que sea esta misma entidad la que investigue los casos de violaciones de derechos humanos cometidos contra estudiantes, de acuerdo con Núñez **perpetúa la impunidad**, por lo que distintas organizaciones sociales adelantan denuncias a nivel nacional con el fin de extenderlas a organismos internacionales.]

A continuación presentamos una secuencia cronológica de detenciones y hechos criminales que respaldan la denuncia de esta declaración realizadas durante el presente año:

Jueves 16 de abril, Santiago: fue detenido Fabián Vidal, estudiante de Escuela de Gobierno y Gestión Pública (INAP), acusado, falseando la realidad, de portar cinco bombas molotov.

Jueves 14 de mayo, Santiago: Luciano Debrott, estudiante de tercer año de ingeniería, fue impactado en su rostro por una bomba lacrimógena lanzada por un capitán de Fuerzas Especiales de Carabineros al interior de la Universidad de Santiago de Chile (USACH). Ese mismo día fueron asesinados Diego Guzmán, estudiante de la carrera de Prevención de Riesgos y Exequiel Borvarán, estudiante de Psicología, ambos pertenecientes a la Universidad Santo Tomás en el marco de una movilización nacional por parte de un civil.

Jueves 21 de mayo, Valparaíso: Paulina Estay, estudiante del AIEP de Valparaíso resultó con traumatismo encéfalo craneano luego de ser agredida por carabineros de fuerzas especiales en el marco de las manifestaciones desarrolladas en las inmediaciones del Congreso Nacional. En la misma ocasión el estudiante Rodrigo Avilés, alumno de Letras de la Universidad Católica de Santiago resultó gravemente herido tras ser alcanzado por un chorro proveniente de un carro lanza agua de carabineros.

Jueves 18 de junio, Santiago: dos estudiantes de la Universidad Tecnológica Metropolitana, Germán Urrutia y Cristóbal Miranda, fueron detenidos violentamente por efectivos de Fuerzas Especiales de Carabineros. Ambos estudiantes sufrieron graves torturas al interior de los vehículos policiales según lo constatado por la Comisión de Derechos Humanos del Colegio Médico.

Jueves 2 de julio, Santiago: fueron detenidos Víctor Zúñiga, estudiante de Trabajo Social de la Universidad Arcis, Felipe Román, estudiante de Diseño de la USACH; Natalia Alvarado, estudiante de Pedagogía en Castellano, Manuel Espinoza, estudiante de Pedagogía en Historia y María Paz Vera, estudiante de Psicología (los tres pertenecientes a la Universidad Academia de Humanismo Cristiano). Los cinco fueron formalizados por un supuesto ataque con bombas incendiarias a la Brigada de Homicidios de la Policía de Investigaciones el 24 de noviembre de 2014.

Jueves 9 de julio, Santiago: el estudiante y vice presidente del Liceo Cervantes, Pablo Antonio del Pozo Vásquez, fue reducido y detenido por diez efectivos de civil que lo golpearon y lo subieron a un vehículo desconocido. Sus compañeros fueron a hacer la denuncia a la Tercera Comisaría de Santiago encontrándose sorpresivamente que su compañero se encontraba detenido en ese recinto.

Jueves 6 de agosto, Temuco: fue detenido el Presidente de la Feufro, Ricardo Lüer, junto a otros once estudiantes en el marco de manifestaciones en contra del evento empresarial Enela (Encuentro Empresarial de La Araucanía).

Domingo 13 de septiembre, Santiago: fueron detenidos los estudiantes Claudio Valenzuela y Fabián Durán Ossandón, este último estudiante de Trabajo Social de la Utem. Ambos fueron formalizados por porte de artefacto incendiario y actualmente se encuentran en prisión preventiva.

Los hechos mencionados, ocurridos por lo general en marchas estudiantiles, evidencian que el Gobierno de Chile, que representa los intereses de los núcleos empresariales, considera a las y los estudiantes como enemigos internos que deben ser perseguidos pues representan un peligro para la existencia del neoliberalismo. Los asesinatos del estudiante Manuel Gutiérrez (2011), de numerosos comuneros mapuche, como Matías Catrileo, Alex Lemún, Jaime Mendoza Collío y Johnny Cariqueo y recientemente del trabajador minero Nelson Quichillao López, provocados por disparos de Carabineros de Chile, nos alerta hasta dónde puede llegar la barbarie que pretende aplastar los brotes de movilización social, dentro de los cuales se encuentra el estudiantil: asesinatos, mutilaciones, amedrentamientos, secuestros, persecución a dirigentes específicos, apaleos masivos y tortura.

[  
**Declaración oficial: **]

##### 

El discurso de la autoridad quiere hacer creer que no hay diferencia entre la movilización social y la delincuencia. El mismo subsecretario del Ministerio del Interior, Mahmud Aleuy, se atrevió a afirmar que un treinta por ciento de las y los estudiantes que marchaban eran delincuentes. Resulta evidente que el Gobierno y su propaganda pretende manipular y confundir a la opinión pública. Al montaje comunicacional se suma el arsenal de instrumentos que permiten legalizar los atropellos y abusos policiales en contra de las personas, particularmente de quienes se movilizan por transformaciones sociales. La actual discusión en el Parlamento en torno al establecimiento del control preventivo de identidad no es más que otro instrumento de persecución que viene a unirse a la Ley Antiterrorista, la Ley de Seguridad del Estado y a los organismos de inteligencia nacional en contra de las y los estudiantes movilizados.

El ejercicio del terrorismo de Estado debe alertar a los pueblos que habitan Chile y a sus movimientos sociales, pues la clase política y empresarial no se detendrá ante la defensa de sus intereses y, por tanto, a la mantención de un ordenamiento impuesto a sangre y fuego por la dictadura. Hoy hacemos un llamado al Gobierno a respetar de manera irrestricta los derechos humanos, particularmente de las y los estudiantes que se movilizan por la educación. Emplazamos a la presidenta de la República, Michelle Bachelet, a hacer gestiones efectivas que conduzcan a la libertad sin condiciones de los estudiantes en prisión.

Santiago, octubre de 2015

Melissa Sepúlveda, Presidenta de la Federación de Estudiantes de la Universidad de Chile 2013-2014  
Comisión Ética Contra la Tortura, Chile.  
Federación de Estudiantes de la Universidad de Chile (FECH)  
Marta Matamala Mejía, Presidenta FEUSACH 2015. Vocera Confech.  
Takuri Tapia, Presidente FEUSACH 2014  
Eloísa González Domínguez, Vocera ACES 2012  
Sergio Grez Toso. Historiador y académico U. de Chile  
Acción Libertaria  
Unión Nacional Estudiantil  
Frente de Acción Socialista  
Juventud Rebelde Somos USACH​
