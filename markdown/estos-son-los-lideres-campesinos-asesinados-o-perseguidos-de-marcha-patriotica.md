Title: Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica
Date: 2016-11-21 15:30
Category: DDHH, Otra Mirada
Tags: defensor de derechos humanos, marcha patriotica
Slug: estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/informe_derechos_onu.-lasdororillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las dos Orillas] 

###### [21 Nov 2016] 

Durante el último fin de semana se han registrado por lo menos 2 atentados y 3 asesinatos contra líderes sociales y campesinos. Organizaciones como Marcha patriótica afirman que “estamos ante un nuevo genocidio” que hay que ponerle un freno a esta situación porque, desde su fundación, hasta este momento se registran por lo menos 124 asesinados. A estas agresiones hay que sumar que durante lo corrido del año se han presentado por lo menos 70 asesinatos de defensores y defensoras de DDHH según el CPDH.Le puede interesar: ["Escalada de la violencia es responsabilidad de la "estrema derecha": Iván Cepeda"](https://archivo.contagioradio.com/escalda-de-asesinatos-y-atentados-extrema-derecha/)

### Dirigentes y líderes asesinados 

Rodrigo Cabrera era un **defensor de derechos humanos y líder campesino perteneciente al movimiento social Marcha Patriótica, desarrollaba su trabajo en la vereda San Antonio**, en el corregimiento de Altamira- municipio de Policarpa en Nariño, era padre de dos niños. Cabrera se encontraba dirigiéndose hacia su casa, en un moto taxí, cuando fue abordado por dos hombres encapuchados que le propiciaron 12 impactos de bala que le ocasionaron la muerte de forma instantánea, en el hecho el conductor del moto taxi resulto herido

Didier Losada era un líder campesino **perteneciente al movimiento social Marcha Patriótica, en el Platanillo**, una jurisdicción de La Macarena, en el departamento del Meta. Losada fue asesinado en su casa, por un encapuchado que le propino con un arma de fuego heridas letales.

Arley Monroy era un **líder campesino del movimiento social Marcha Patriótica, vinculado al sector ganadero** de la región de San Vicente del Caguan, **dirigente de la Asociación Agrícola Ascal-g** y había sido candidato a la asamblea departamental del Meta, además participó durante el paro agrario que se dio en el año 2013.  Monroy fue encontrado con impactos de bala en la zona rural de Losada-Guayabal y falleció mientras era trasladado al puesto de salud más cercano.

Atentados a dirigentes o líderes sociales 
-----------------------------------------

Danilo Bolaños Díaz **es el secretario de la subdirectiva de la Asociación de Trabajadores campesinos de Nariño** – ASTRACAN de Leiva, además es integrante de la Red de Derechos Humanos Francisco Isaías Cifuentes, hace parte de la Juventud Rebelde y del movimiento social Marcha Patriótica.  Bolaños fue interceptado junto con su compañera sentimental, cuando se dirigían hacia su casa, por dos hombres armados que le descargaron seis impactos de bala. Afortunadamente ninguno de los dos resultó herido.

Hugo Cuellar es presidente de la junta de la vereda La Victoria de la Macarena – Meta y **miembro de la Fundación de Derechos Humanos del Centro Oriente de Colombia**. Cuellar fue agredido por sicarios cuando salía del funeral de líder social Arley Monroy, en el hecho se presentó un forcejeo en donde  Cuellar recibió un impacto de bala en el abdomen

Le puede interesar:["Estamos frente a un nuevo genocidio: Carlos Lozano"](https://archivo.contagioradio.com/estamos-frente-a-un-nuevo-genocidio-carlos-lozano/)

###### Reciba toda la información de Contagio Radio en [[su correo]
