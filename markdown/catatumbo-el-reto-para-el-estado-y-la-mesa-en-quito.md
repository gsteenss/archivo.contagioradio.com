Title: Catatumbo: el reto para el Estado y la mesa en Quito
Date: 2018-04-17 13:28
Category: Paz, Política
Tags: Catatumbo, ELN, EPL, Gobierno Nacional
Slug: catatumbo-el-reto-para-el-estado-y-la-mesa-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/catatumbo-e1523989714870.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Defensoría del Pueblo] 

###### [17 Abr 2018] 

La situación en el Catatumbo, Norte de Santander, debido a la continuación de los enfrentamientos en entre el Ejército de Liberación Nacional (ELN) y el Ejército Popular de Liberación (EPL), quienes declararon paro armado, **es crítica** y las comunidades están viviendo en medio de la zozobra.

De acuerdo con el analista político Luis Eduardo Celis, el conflicto en el Catatumbo no es nuevo y el paro armado decretado allí **“evidencia que hay una situación crítica”** que representa un desafío para el Estado. Afirmó que el Estado nunca ha tenido el control “sobre el 100% del territorio y hay zonas donde las guerrillas y las bandas armadas se han disputado el control del territorio”.

### **Estado tiene la responsabilidad de proteger a las comunidades** 

Celis, argumentó que el Estado tiene un reto “enorme” en el Catatumbo “que no es fácil de cumplir, pero tiene **el deber de proteger las comunidades**”. Por esto, hay una preocupación generalizada por el paro armado que “genera desplazamientos, zozobra, asesinatos y presión contra las comunidades”.

Recordó que los actores armados deben **respetar a la población civil** y la sociedad colombiana debe solidarizarse con las iniciativas del Gobierno Nacional y las de la sociedad civil. Por esto, el analista invitó a los gestores de paz del ELN y a la comunidad de la región “para ver qué se puede hacer ante esta situación crítica”. (Le puede interesar:["Asesinan líder campesino en el Catatumbo"](https://archivo.contagioradio.com/asesinado-campesino-catatumbo/))

### **La mesa de negociación en Quito debe tratar el tema del Catatumbo** 

Asimismo, Luis Eduardo Celis recordó que en la mesa de negociación de Quito donde se desarrollan los diálogos entre la guerrilla del ELN y el Gobierno Nacional, debe tratarse la situación que se vive en el Catatumbo “para que se **logre un ambiente de convivencia"**. Dijo que es importante pactar un cese bilateral, que a su vez plantea un reto teniendo en cuenta que el enfrentamiento en esta región no es sólo con el Estado.

Por esto, recomendó que se tenga en cuenta una iniciativa “frente a la banda de los Pelusos” o el EPL. Afirmó que “el Estado no ha podido controlar esta banda que lleva en el territorio **más de 25 años**” por lo que es necesario un ambiente de negociación que contribuya “a un clima positivo en el Catatumbo”.

### **Sociedad civil debe ayudar a proteger a las comunidades** 

Finalmente, Celis afirmó que el Catatumbo es una región **muy organizada** en donde la sociedad civil ha realizado iniciativas para proteger a sus comunidades. Recordó que por parte de los colombianos es necesario “rodear a las organizaciones y a la sociedad catatumbera” para garantizar la protección de las comunidades.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
