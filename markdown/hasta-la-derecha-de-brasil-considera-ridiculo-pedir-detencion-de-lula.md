Title: Hasta la derecha de Brasil considera ridículo pedir detención de Lula
Date: 2016-03-11 18:54
Category: El mundo, Entrevistas
Tags: Brasil, Lula da Silva, Movimiento Sin Tierra, MST, Partido de los Trabajadores
Slug: hasta-la-derecha-de-brasil-considera-ridiculo-pedir-detencion-de-lula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Lula_da_silva_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: infobae] 

<iframe src="http://co.ivoox.com/es/player_ek_10783464_2_1.html?data=kpWkmpiYepWhhpywj5aVaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncanV1NnOjdHFb8XZ08rQysaPqMafo9fO1c7Qb8Tjz9jWxsrWpYzmysmSpZiJhaXX1tHcjdXJqMrmjMnSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Marcelo Borman Zero] 

###### [11 Mar 2016] 

Este jueves, el fiscal que adelanta las investigaciones por malversación de fondos públicos de la empresa Petrobras, entre otros delitos, solicitó al juzgado que se expida una orden de captura contra el ex presidente Lula, con el argumento de que sus manifestaciones públicas se considerarían un peligro porque incitan a la protesta, lo que, **hasta el partido de la derecha calificó de ridículo.**

Según la fiscalía, las declaraciones de Lula Da Silva y las movilizaciones que se suscitaron en favor de la democracia y el **Estado Social de Derecho**, podrían significar la posibilidad de una insurrección o incluso podrían provocar la fuga del ex presidente. Situación que es calificada como absurda puesto que en todo Estado está legítimamente protegido el derecho a la protesta, a la libre movilización y a la manifestación pública sin que eso represente una amenaza.

Otro de los argumentos usados por el ente acusador es que el ex presidente podría salir del país en medio de la convulsa situación. Sin embargo, tanto la defensa de Lula Da Silva, como el propio ex presidente han manifestado **públicamente que están dispuestos a afrontar cualquier investigación. Por su parte la presidenta Dilma Roseff aseguró que no cederá y no renunciará por la presiones de la derecha.**

Marcelo Borman Zero, asesor del partido de los trabajadores en Brasil, asegura que [la solicitud de la fiscalía es otra evidencia de la actuación política de las instituciones de la justicia y de la fiscalía](https://archivo.contagioradio.com/?s=brasil), que pretenden debilitar el gobierno de Dilma Roussef, al partido de los trabajadores y al ex presidente con el fin de dar un golpe desde la opinión general, las grandes empresas mediáticas y los sectores políticos de oposición.

Para el próximo 18 de Marzo se tiene programada una [gran **movilización en varias ciudades del país**](https://archivo.contagioradio.com/movimientos-sociales-de-brasil-exigen-senales-claras-del-gobierno-de-rousseff/) para respaldar a las instituciones, al gobierno de Rouseff, a la democracia y al Estado Social de Derecho, y se espera que la policía, así como ha protegido las movilizaciones en contra del gobierno proteja y garantice el derecho a la protesta de quienes, según Borman Zero, se oponen a los intereses de la derecha en ese país.
