Title: Actitudes machistas no pueden ser validadas como práctica cultural
Date: 2017-01-10 14:30
Category: DDHH, Mujer
Tags: mujeres, paz, víctimas, Violencia de género
Slug: actitudes-machistas-no-pueden-ser-validadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mujeres-colombianas-desnudas-protestando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rui Dong] 

###### [10 Enero 2017] 

**La sistematicidad, la naturalización y el aumento de la violencia contra mujeres y grupos vulnerables en la región Caribe sigue preocupando a organizaciones sociales y defensores de derechos de las mujeres**, así lo aseguró Wilson Castañeda director de Caribe Afirmativo a propósito del alto indicador de violencia en contra de las mujeres, sobre todo en el departamento del Atlántico que cerró 2016 con 51 casos, significando un aumento del 45% en relación con el 2015. Le puede interesar: [Asesinatos de mujeres en Atlántico aumentaron un 45% en 2016.](https://archivo.contagioradio.com/asesinatos-mujeres-atlantico-aumentaron-45-2016/)

Según Castañeda en términos de exclusión social, inequidad y falta de políticas sociales de las **5 ciudades con peor calificación del país Cartagena, Barranquilla y Montería hacen parte de ese ranking** “lo que visibiliza esto es que hace falta políticas sociales de largo plazo que garanticen los derechos de las mujeres y que logren reducir la inequidad y la exclusión social” agregó.

Si bien Medicina Legal ha advertido la disminución de la tasa de homicidios en los últimos años, esta entidad ha manifestado su preocupación en el aumento de violencia doméstica y riñas “esto lo que sigue es manifestando **un escenario de violencia permanente en Colombia, donde la región Caribe presenta las cifras más preocupantes, en lo que se ha llamado violencia cotidiana**” recalcó Castañeda.

Para Caribe Afirmativo, el panorama enunciado anteriormente es muestra de “falta de políticas sociales, ausencia de protección a los grupos históricamente vulnerables, que los hace más susceptibles a expresiones de violencia basada en género y que genera una honda preocupación en esta región”.

Wilson Castañeda asevera que la sociedad colombiana aún no ha entendido la relación que puede existir entre cultura y derechos humanos lo que conduce a que se naturalice como una práctica cultural la discriminación y la violación a los derechos humanos.

“Por ejemplo, lo que compete con prácticas misóginas o de violencia contra las mujeres en el Caribe colombiano, encontramos que hay discursos que la legitiman o que tratan de pormenorizar su efecto nocivo. Entre esos están las autoridades, medios de comunicación y ciudadanos”.

Asegura Castañeda que para finales e inicio de cada año Colombia tiene sus festividades y en esos espacios las violencias machistas se agudizan e incrementan **“no es gratuito encontrar que Barranquilla aumenta niveles de violencia contra las mujeres en épocas de carnavales o que en Cartagena se visibiliza más las agresiones contra comunidad LGBTI en noviembre”.**

Dice Caribe Afirmativo que en el año 2017 esperan que la institucionalidad pueda aunar los esfuerzos para que “deje de ser tan asistencialista” y por el contrario la disminución de las cifras sea notoria.  Le puede interesar: ["Con más mujeres en la política Colombia sería un paraíso"](http://%22Con%20más%20mujeres%20en%20la%20política%20Colombia%20sería%20un%20paraíso%22)

Y concluye su Director diciendo que estarán trabajando con asociaciones culturales para que “**promuevan contenidos que garanticen derechos. Que dejen claro que para nada riñe la cultura con los derechos humanos, que no se puede validar como una práctica cultural las actitudes machistas y la violación de los derechos humanos”.**

<iframe id="audio_16064808" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16064808_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
