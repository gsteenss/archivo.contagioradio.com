Title: Paro Nacional de transportadores se moviliza por abusos, falta de seguridad e incumplimientos
Date: 2019-09-23 17:46
Author: CtgAdm
Category: Movilización, Nacional
Tags: Paro Nacional, Paro nacional de taxistas, sistema de transporte
Slug: paro-nacional-transportadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Paro-Nacional-Transporte.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Impulsado por medio de las redes sociales, desde esta mañana inició la hora cero del paro indefinido del Sector Transportes, convocado por colectivos de conductores de buses, taxi y particulares. Algunos de los motivos de esta movilización, según los conductores, tienen que ver con abusos por parte del Fuerza Pública, la falta de seguridad para ejercer su labor y los incumplimientos por parte de la Secretaría de Transportes en torno al funcionamiento del Sistema Integral de Transporte Público.

Entre las organizaciones que se encuentran haciendo el llamado a paro se encuentran la Asociación de Transportadores (ASOTRANS), el Consejo Superior de Transporte (CST) y la Asociación Nacional para el Desarrollo Integral del Transporte Terrestre Intermunicipal (ADITT). (Le puede interesar:[Bogotanos se movilizan en rechazo al alza del sistema de transporte público](https://archivo.contagioradio.com/bogotanos-se-movilizan-en-rechazo-del-alza-al-sistema-de-transporte-publico/))

De acuerdo con Jorge García integrante de Confederación Colombiana de Transportadores, uno de los motivos para estas movilizaciones es la urgencia de la modificación de la Ley 1383 del Código Nacional de Tránsito, debido a que para los transportadores, esta normativa es poco equitativa con las sanciones hacia quienes infringen la norma.

Estos son otros de los motivos que exponen los conductores para su movilización:

-   Denuncia de abusos arbitrarios por parte de las autoridades en el marco de retenes ilegales y procedimientos atípicos como la imposición de multas y la suspensión de licencias de conducción;
-   La falta de seguridad para los transportadores colombianos debido al alto índice de robos de vehículos y asesinatos que afectan los conductores;
-   Insuficiente compromisos del Ministerio de Transporte y de la Secretaria de Movilidad en la toma de medidas equitativas para los conductores.

Asimismo el integrante de CTT agregó que: "nosotros desde un inicio quisimos contactar con el Consejo de Estado para ver la viabilidad de demandar la ley 1383 del Código Nacional de Tránsito, pero se dio primero el paro. Para nosotros esta no es la forma, de hecho nos parece peligroso hablar de bloqueos que atente la libre movilidad de las personas".

Las organizaciones de conductores trabajaran esta semana para demandar una modificación de la ley 1383 del 2010 con el objetivo de encontrar la manera  pacifica y legal de garantízar equidad y seguridad a los transportadores. (Le puede interesar:[Transporte Público de Bogotá puede ser suspendido por crisis de 4 empresas](https://archivo.contagioradio.com/transporte-publico-de-bogota-puede-ser-suspendido-por-crisis-de-4-empresas/))

### **Bloqueos y vias afectadas** 

Desde tempranas horas en la mañana, los transportadores han realizado bloqueos en zonas de alta congestión de trafico, algunos de estos lugares son: la Autopista Norte, en donde se han generado hechos de violencia para evitar la circulación de vehículos; estaciones como la de la calle 142, calle 146, Mazuren, calle 161 y Toberín, en sentido sur – norte que también presentaron bloqueos y los portales como el del Norte, Usme, 20 de Julio, Calle 80, Salitre y el Aeropuerto el Dorado, de igual forma localidades como Engativá, Teusaquillo, Usaquén San Cristóbal, Fontibón y Ciudad Bolívar, han presentado dificultades con la movilidad en el transcurso del día.

> Este señor que se movilizaba por la autopista norte, no quería participar en el [\#ParoDeTransporte](https://twitter.com/hashtag/ParoDeTransporte?src=hash&ref_src=twsrc%5Etfw); hasta que llegó un grupo de jóvenes transportadores, quienes amable y decentemente lo hicieron cambiar de parecer. [pic.twitter.com/W58DZMeenC](https://t.co/W58DZMeenC)
>
> — Oscar Iván Castro (@OscarCastroN) [September 23, 2019](https://twitter.com/OscarCastroN/status/1176210773133205507?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
De igual manera García agregó: "lo que pasa es que hay un abuso de la misma ley, la cual existe  desde el 2010 y hasta ahora la vienen a aplicar. Esta es  una sanción arbitraria, así que nosotros lo que vamos a apelar es a que sean clasificadas multas graves y leves y partir de ahi medir la suspensión de la licencia"

Frente a los hechos de bloqueos y las dificultades de movilidad,  algunas instituciones tomaron la decisión de suspender la jornada de clase para garantizar la seguridad de las y los estudiantes ,puesto que aún no se sabe cuanto se demorará la movilización. Se invita a la población a prestar atención y tener cuidado. (Le puede interesar:[Aumento en transporte público en Bogotá el más alto en 8 años](https://archivo.contagioradio.com/aumento-en-transporte-publico-en-bogota-el-mas-alto-en-8-anos/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
