Title: Tres profesores asesinados en desarollo del Paro, Johana Alarcón la última
Date: 2017-06-09 12:22
Category: DDHH, Nacional
Tags: Asesinatos, Docentes, maestros, Paro de maestros, profesores
Slug: 42004-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [09 Jun. 2017] 

El Sindicato Único de Trabajadores de la Educación del Valle (SUTEV) aseguró a través de un comunicado que **este 7 de junio en la ciudad de Cali se conoció del asesinato de Jhoana Alarcón, docente de la Institución Educativa Gabriel Mistral.** Este es el tercer asesinato de profesores en menos de 3 días a propósito del Paro Nacional del Magisterio que completa casi 30 días.

Jhoana se dirigía a su casa en Cali, cuando fue abordada por desconocidos quienes **le propinaron 4 impactos de bala, acabando con su vida. **Le puede interesar: [Ya se inició la movilización de docentes para la Toma de Bogotá](https://archivo.contagioradio.com/ya-se-inicio-la-movilizacion-de-docentes-para-la-toma-de-bogota/)

A través de la denuncia pública dada a conocer por SUTEV, exigieron **celeridad en las investigaciones que puedan dar con los culpables de este asesinato.** Así mismo, instaron a las autoridades competentes a hallar los responsables de los asesinatos de Washington Cedeño, asesinado el 6 de junio en el departamento de Córdoba y de Juan Artunduaga en Pitalito, Huila. Le puede interesar: [Asesinan a profesor Washington Cedeño en Córdoba](https://archivo.contagioradio.com/asesinan-a-profesor-en-cordoba/)

Según un informe de la Unidad de Víctimas **entre el año 1986 y 2014, en Colombia han sido asesinados por lo menos 1.005 maestros y maestras.** Sólo en el 2014, ya se contaban 12 asesinatos, 5.500 maestros y maestras amenazadas, 1.600 desplazados y desplazadas, y 70 exiliados. Le puede interesar: [Explosión en Bogotá pudo estar dirigida contra FECODE](http://Explosi%C3%B3n%20en%20Bogot%C3%A1%20pudo%20estar%20dirigida%20contra%20FECODE/)

[Denuncia Asesinato Contra Docente Jhoana Alarcon-06.08.2017](https://www.scribd.com/document/350840907/Denuncia-Asesinato-Contra-Docente-Jhoana-Alarcon-06-08-2017#from_embed "View Denuncia Asesinato Contra Docente Jhoana Alarcon-06.08.2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_24450" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350840907/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-kHSjeaygU1GvcWQvpxPj&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
