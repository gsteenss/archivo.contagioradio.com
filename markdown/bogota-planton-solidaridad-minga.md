Title: En Bogotá realizarán plantón en solidaridad con la Minga
Date: 2019-04-12 16:03
Author: CtgAdm
Category: eventos, Movilización
Tags: Batucada "La tremenda revoltosa", Minga en el Cauca, plantón solidaridad
Slug: bogota-planton-solidaridad-minga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56675791_2668006096548619_121898039513186304_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nubia Acosta 

Como una manifestación que busca apoyar a la Minga indígena, social y popular y en rechazo a las recientes declaraciones racistas hacia el movimiento indígena, integrantes de **La Tremenda Revoltosa Batucada Feminista** alzarán su voz y tambores contra el racismo, por la vida y la resistencia de los pueblos indígenas, afros y campesinos.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56675791_2668006096548619_121898039513186304_n-300x252.jpg){.alignnone .size-medium .wp-image-64793 .aligncenter width="300" height="252"}

Una movilización a la que invitan a sumarse a todas las personas que se sientan identificados con su lucha y deseen expresar su solidaridad. (Le puede interesar: [Duque tendrá que hablar con la Minga Nacional del 25 de abril: CRIC](https://archivo.contagioradio.com/duque-tendra-que-hablar-con-la-minga-nacional-del-25-de-abril-cric/))

**Fecha:** 12 de abril de 2019  
**Hora:** 6:00 p.m.  
**Lugar:** Carrera 7 calle 67, Bogotá
