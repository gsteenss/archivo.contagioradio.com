Title: Riosucio, Chocó las tragedias más allá de las llamas
Date: 2020-12-17 12:26
Category: Actualidad, Comunidad, Nacional
Tags: choco
Slug: riosucio-choco-las-tragedias-mas-alla-del-fuego-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/chocó-maestros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los habitantes de **Riosucio en Chocó, continúan construyendo su vida sobre los** escombros que dejó el **incendio del pasado 28 de noviembre que devastó los hogares de 97 familias, y afectó a 485 personas, una** comunidad que previo al incendio afrontaba los rezagos de conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Cerca de 29 mil habitantes enfrentan la falta de acueducto, de alcantarillado, de hospitales, de carreteras y las violaciones históricas a sus Derechos Humanos pues la presencia del paramilitarismo y de las AGC los mantiene en zozobra.** ([«Ríosucio no contaba con un departamento de bomberos, ni con acueducto»](https://archivo.contagioradio.com/riosucio-no-contaba-con-un-departamento-de-bomberos-ni-mucho-con-acueducto/)).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Que ha pasado con las ayudas que se prometieron a Riosucio ?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Edinson Palacios, Secretario de la Oficina de Paz, Posconflicto y reconciliación de Chocó**, recuerda la impotencia que vivió en horas de la madruga de ese sábado mientras realizaba llamadas de auxilio a las autoridades departamentales que pudieran mitigar las llamas, ***"con dolor pedía ayuda, al tiempo que veía como la comunidad intentaba apagar el incendio con baldes de agua, pero al final lo perdieron todo hasta los documentos"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posterior al incendio se activaron alertas en todo el país y cientos de ayudas de la comunidad civil empezaron a llegar al municipio, Palacios afirmó, *"agradecemos y reconocemos los esfuerzos de colombianos que han extendido su mano hasta acá, sin embargo **no tenemos un albergue que pueda acoger a todas las personas mayormente damnificadas que están esperando en casas de familia y amigos a que haya una respuesta,** y es allí donde debe actuar el Gobierno con acciones de fondo"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recordó que posterior al incendio Daniel Palacios, viceministro del Interior llegó al territorio afirmando que, a más tardar en el mes de enero Riosucio iba a contar con un carro de bomberos, así como una persona que pudiese capacitar a los voluntarios que conformen el cuerpo de bomberos, ***"hoy estamos en todo este proceso pero aún así las personas están devastadas y con solo promesas en sus manos "***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las otras promesas a corto plazo según Palacios esta la recuperación de las viviendas, *"**se está trabajando en la búsqueda de un nuevo terreno para que no sean el mismo lugar donde ocurrió el incendio, para esto el Alcalde está concertando con las víctimas**, para que se desplacen y convertir el espacio en un lugar de memoria de quienes murieron"*, esta reconstrucción se estima que este lista en menos de de 6 meses.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras esto se ejecuta las víctimas serán reubicadas, y se les entregará un dinero que deben disponer para arriendos, *"**a las familias les harán una entrega \$250.000, pero no es un dinero pensando, puesto que en Chocó la mayoría de las familias somos numerosas** superando las cinco personas, entonces no es fácil conseguir un arriendo de este valor, aún así ese fue el rubro estipulado por el Gobierno"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las acciones de fondo que se necesitan, Palacios indicó que estas deben empiezan reconociendo que municipios como **Ríosucio, Unguía , Acandí, Carmen del Darién y Bojayá, en Chocó**; en su mayoría están conformados por casas de madera, entonces *"****en cualquier momento ellos pueden enfrentar algo parecido*, *por tanto las acciones deben extenderse a todo el departamento*** *antes de que se reportes situaciones peores"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ***"Riosucio tiene una población de 29 mil habitantes de estos, 20 mil somos víctimas del conflicto armado"***

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el secretario las **97 familias que resultaron afectadas por el incendio además de ser víctimas de este, han enfrentado desde los años noventa el paso de la violencia por su territorio** *"han sido engañadas por el Gobierno en materia de ayudas y reparación, sin dejar de lado que aún hace falta que se reconozca gran parte de la verdad de lo que pasó acá".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Llaman a las víctimas para decirle que las indemnizaciones van a llegar el siguiente mes y así pasan mes tras mes sin que lleguen las ayudas, convirtiendo entonces el dolor y las necesidades de las víctimas en una burla"*
>
> <cite>**Edinson Palacios, Secretario de la Oficina de Paz, Posconflicto y reconciliación de Chocó**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y finalizó diciendo que el olvido histórico de las comunidades negras del Chocó, víctimas del conflicto armado del país se vio reflejada en la falta de presencia en medio de esta emergencia por parte de la [Unidad para la Atención y Reparación Integral a las Víctimas](https://www.unidadvictimas.gov.co/), *"**este es un municipio colmado de víctimas del conflicto armado, las comunidades esperaban a la Unidad, la cuál jamás llegó"***, una presencia que iba mucho más allá de un interés económica y *"hace parte de la solidaridad con las víctimas, y esto fue lo que le faltó al doctor Ramón Rodríguez, director de la Unidad".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El pueblo no olvida, son las instituciones estatales las que lo hacen "

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Yolanda Perea, reconocida lideresa y activista política del Chocó**, indica que en medio de la catástrofe que atraviesan las comunidades de Ríosucio como líderes y lideresas intentan ayudar en lo que pueden, pero pese a ello hay condiciones y auxilios que se salen de sus manos, ***"por eso nuestra voz se alza hacia el Gobierno, diciendo que es necesario una responsabilidad Estatal para realmente ver la garantía de igualdad de Derechos"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acciones gubernamentales que que deben ser inmediatas, *"este ya es el tercer incendio que se registra, **ahora que el daño cobro vidas es cuando llegan a prometer y a decir que van a hacer mil cosas, pero eso lo deben convertir en realidad de manera urgente y pronta**".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Las condiciones del Chocó hacen que mientras nos estamos inundando al mismo tiempo nos estemos quemando, todo mientras intentamos avanzar sobre las huellas del conflicto armado".*
>
> <cite>**Yolanda Perea, lideresa y activista política del Chocó**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y finalizó señalando, ***"no es el pueblo el que olvida, son las instituciones las que lo hacen y a las que además les compete garantizar el establecimiento integral derechos",*** en un olvido institucional que se da mientras las comunidades día a día luchan por decir, ¡aquí estamos presentes!.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://youtu.be/r5Q42iX3Bus","type":"video","providerNameSlug":"youtube","responsive":true,"className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed is-type-video is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/r5Q42iX3Bus

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
