Title: Jóvenes víctimas del conflicto le piden a Petro, De La Calle y Fajardo que formen la gran coalición
Date: 2018-03-26 15:26
Category: Nacional, Paz
Tags: Entrevista Humberto de la Calle, Gustavo Petro, jovenes, Sergio Fajardo
Slug: jovenes-le-piden-a-petro-de-la-calle-y-fajardo-que-formen-la-gran-coalicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/WhatsApp-Image-2018-03-26-at-3.15.11-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Mar 2018] 

Jóvenes víctimas del conflicto armado, de diferentes territorios del país, se juntaron para a través de la música, la danza y el arte hacer pedirle a Gustavo Petro, Sergio Fajardo y Humberto De La Calle que formen la gran coalición de la unidad que propenda por la implementación de los Acuerdos de Paz, **la búsqueda de la verdad, justicia y reparación y la posibilidad de construir una Colombia diversa**.

Los jóvenes que venían de regiones recónditas del país como la Cuenca del río Naya, el Bajo Atrato, Cacarica, Jiguamiadó, Inza del Cauca, de la comunidad indígena Embera, del pueblo Nasa del Putumayo y de Buenaventura, se juntaron a través de sus instrumentos autóctonos, como la marimba, los tambores, los palos de lluvia, para al unísono pedirles a estas tres candidaturas de diversas tendencias políticas que se unan en un ejercicio político y democrático.

**“Unificamos criterios, todos estamos seguros, una política nueva nos dará un mejor futuro”** es uno de los estribillos compuesto por afros, mestizos e indígenas, que a su vez, le pidieron a los candidatos que vinculen a la sociedad a este proceso, sin que prime los intereses de un solo sector. (Le puede interesar: ["Vida de jóvenes de Ciudad Bolívar amenazada por bandas criminales](https://archivo.contagioradio.com/vida-de-jovenes-de-ciudad-bolivar-amenazada-por-control-de-bandas-criminales/)")

Otras de las peticiones que hacen los jóvenes es que tanto Fajardo, como De La Calle y Petro, tengan en cuenta en sus agendas políticas los derechos a la Educación y la economía, como prioridades, junto con la implementación de los Acuerdos de paz y la búsqueda de una justicia social.
