Title: Congreso de EE.UU. pide que Trump presione a Duque para proteger a líderes sociales
Date: 2020-07-06 20:11
Author: CtgAdm
Category: Nacional
Tags: Asesinato de defensores de DD.HH., Congreso de los Estados Unidos, Gobierno Duque, Implementación del Acuerdo
Slug: congreso-de-ee-uu-pide-que-trump-presione-a-duque-para-proteger-a-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Congreso de los EE.UU / Foto: Pxfuel

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un grupo de 96 congresistas de Estados Unidos instó al secretario de Estado de los Estados Unidos, Mike Pompeo a que presione al gobierno Duque para que en medio de un escalamiento de la violencia en el país, proteja la vida de líderes sociales, además de esclarecer las acciones de vigilancia y seguimientos ilegales efectuados por el Ejército, los asesinatos de defensores de DD.HH. y excombatientes y detenga el recrudecimiento de la violencia que se vive en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de la carta firmada por los integrantess del Congreso y dirigida a la administración del presidente Donald Trump, se pidió la mejora de la protección de los defensores de DD.HH y de los líderes sociales, **incluyendo la investigación eficaz de ataques y amenazas además de identificar a quienes ordenaron estos delitos y la presentación pública de los resultados de dichas investigaciones a través de organismos como la Fiscalía,** señalando la importancia de poner fin a la impunidad en los asesinatos, las desapariciones, los ataques y las amenazas contra defensores DD.HH, liderazgos, sindicalistas y activistas. [(Le puede interesar: Unión Europea reafirma su apoyo a los liderazgos en Colombia)](https://archivo.contagioradio.com/union-europea-reafirma-su-apoyo-a-los-liderazgos-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, piden financiar medidas de protección colectiva con enfoques diferenciales de género y étnico en consulta con las comunidades, incluyendo a poblaciones indígenas y afrodescendientes a través de la Unidad de Protección Nacional (UNP) .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma exigieron que se respeten los mecanismos de autoprotección de poblaciones como las zonas humanitarias, además de desarrollar una hoja de ruta que incorpore los retos derivados de la pandemia incluyendo el uso de equipos de bioseguridad. [(Lea también: Alimentación, salud, agua y acuerdo humanitario, exigen 110 comunidades a Duque)](https://archivo.contagioradio.com/alimentacion-salud-agua-y-acuerdo-humanitario-exigen-110-comunidades-a-duque/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los congresistas **señalaron la importancia de desmantelar los grupos sucesores del paramilitarismo** responsables del tráfico de drogas y de "gran parte de la violencia ejercida contra los defensores de los derechos humanos y los líderes sociales", algo que debe lograrse en colectivo con la **Comisión Nacional de Garantías de Seguridad,** que fue creada por el Acuerdo de Paz y que hasta el momento no ha sido puesta en funcionamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asímismo desde el Congreso pidieron se identifique y penalice a los integrantes de Ejército Nacional, incluidos los más altos rangos, que dieron la orden y ejecutaron el perfilamiento e interceptaron de forma masiva e ilegal a 130 personas, incluyendo periodistas nacionales y del país norteamericano, defensores de DD.HH, políticos, y a otros militares quienes han denunciado abusos, agregando que EE.UU. debe buscar que la cooperación de seguridad e inteligencia estadounidense **"no apoye ni incite actos de inteligencia ilegal, ni ahora ni en el futuro".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No solo el Congreso de los EE.UU. pide respuestas al Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

“Esta no es la primera vez que el Congreso le ha exigido a los gobiernos de Estados Unidos y Colombia que proteja a los defensores de DD.HH. y líderes sociales en Colombia, sin embargo, los asesinatos continúan aumentando y la pandemia los ha hecho aún más vulnerables. Suficiente es suficiente, lo que sea que el gobierno colombiano piense que está haciendo, simplemente no lo está logrando". expresó uno de los firmantes, el congresista[James McGovern.](https://twitter.com/RepMcGovern)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tampoco es la primera vez que la comunidad internacional aboga por la defensa de los líderes sociales en Colombia, hacer casi un mes varios diputados del Parlamento Europeo expresaron su preocupación ante el incremento de la violencia en el país y pidieron respuestas a Iván Duque con relació a la implementación del Acuerdo de Paz y la protección que se debe garantizar a las comunidades, liderazgos y excombatientes. [(Lea también: "Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia)](https://archivo.contagioradio.com/incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Los congresistas reprocharon el hecho que desde la firma del Acuerdo de Paz en 2016, cerca de 400 defensores de DD.HH. hayan sido asesinados, señalando que al menos 100 han muerto en los primeros seis meses del 2020,** "la lentitud del gobierno colombiano en implementar el acuerdo, su incapacidad para llevar al Estado a las zonas de conflicto y su incapacidad continua para prevenir y enjuiciar los ataques contra defensores han permitido que esta tragedia no se controle", declararon. [](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)[(Le recomendamos leer: Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH)](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
