Title: Denuncian cerca de 130 irregularidades en elecciones en Bogotá
Date: 2015-10-31 14:24
Category: Nacional, Política
Tags: Bogotá, Fraude electoral, Irregulariddes electorales en Colombia, Misión de Observación Electoral, Misión de Observación Electoral Bogotá, Unión Patriótica
Slug: denuncian-cerca-de-130-irregularidades-en-elecciones-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/mapa_moe_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MOE 

###### [31 Oct 2015]

La **Misión de Observación Electoral**, denunció que durante la jornada electoral del pasado Domingo, solamente en la capital colombiana se presentaron irregularidades en torno a publicidad y medios de información, constreñimiento al elector, irregularidades por parte de las autoridades electorales, en la inscripción de cédulas, en el conteo de votos y la financiación, entre otras.

Solamente durante esta semana el Partido **Unión Patriótica asegura haber recuperado cerca de 5000 votos que no estaban contabilizados** y que ni siquiera fueron registrados en los formularios E14 que ahora están en los centros de escrutinio en la sede de Corferias. A este respecto la presidenta de la UP ha afirmado que no quedará ni un solo voto que haya sido depositado por los ciudadanos que no se contabilice.

Según algunas fuentes en Corferias, de continuar la tendencia en la recuperación de votos por parte de la UP, el partido Cambio Radical perdería un escaño que iría para Aida Avella, cabeza de lista de este partido, que a pesar de haber superado a mucho concejales no ha alcanzado la curul en la asamblea distrital.

Otra de las situaciones que para la MOE representa mayor gravedad son las denuncian en cuanto a compra de votos, según el reporte se presentaron por lo menos 25 denuncias en diversas localidades. Santa Fe, **Los Mártires, Ciudad Bolívar y Puente Aranda, son las localidades en que mayor número de irregularidades se denunciaron.**

Esta situación en la capital colombiana se suma a las múltiples denuncias por fraude electoral tanto en los propios municipios de Cundinamarca, [departamentos como el Cesar, municipios de Pueblo Bello y Pailitas, Meta en municipios como Mapiripan y Lejanías, Chocó en Carmen del Daríen entre otros.](https://archivo.contagioradio.com/?s=fraude+electoral)

Sin emabargo la MOE también reconoció que “se presentó en la mayoría de la ciudad un ambiente de paz, que permitió **la participación de la ciudanía en un 51,55% a alcaldía,** cuatro puntos por encima del año 2011. Al concejo de 49,29% , 3.46% superior al nivel de participación de lo presentado en las elecciones de 2011 y para JAL en un 52,73% , superando al 50.2% del año 2011.”

Consulte tablas de datos e imágenes a continuación

[Informe MOE](https://es.scribd.com/doc/288034317/Informe-MOE "View Informe MOE on Scribd")

<iframe id="doc_67508" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/288034317/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="505" height="505" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
