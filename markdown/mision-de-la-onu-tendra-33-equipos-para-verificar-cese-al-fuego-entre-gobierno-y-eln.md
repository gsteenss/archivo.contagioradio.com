Title: Misión de la ONU tendrá 33 equipos para verificar cese al fuego entre Gobierno y ELN
Date: 2017-10-06 13:34
Category: Nacional, Paz
Tags: Cese al fuego bilatetal, Cese al fuego ELN, ELN, FARC, Gobierno Nacional, Naciones Unidas
Slug: mision-de-la-onu-tendra-33-equipos-para-verificar-cese-al-fuego-entre-gobierno-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/mision-onu-e1500056661395.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Misión ONU Colombia] 

###### [06 Oct 2017 ] 

En rueda de prensa, el director de la Misión de Verificación de las Naciones Unidas en Colombia, Jean Arnault, **informó la decisión de ese organismo de autorizar la verificación** del cumplimiento del cese al fuego bilateral temporal entre el Gobierno Nacional y el ELN. La verificación se realizará hasta el 9 de enero de 2018.

En primer lugar, Arnault se refirió a la masacre que ocurrió el 5 de octubre de 2017 en Tumaco donde fueron asesinadas 9 campesinos. Indicó que la Misión **lamenta los hechos presentados** y recalcó que “los acontecimientos trágicos refuerzan la necesidad de propiciar que los campesinos de las regiones afectadas por el cultivo de coca puedan escapar de la disyuntiva de la extrema pobreza y la ilegalidad”.

** Mandato de verificación de cese al fuego bilateral**

Arnault manifestó que la totalidad de los miembros del Consejo de Seguridad de las Naciones Unidas autorizaron mediante Resolución 2381, **iniciar el mandato de verificación del cese al fuego** que incluye “la coordinación de un mecanismo de veeduría que integra al Gobierno Nacional, la Fuerza Pública, los representantes del ELN, la iglesia y a las Naciones Unidas”.

La Misión tendrá **tres objetivos específicos**: tratará de prevenir encuentros armados entre el ejército y el ELN, buscará facilitar una respuesta oportuna ante cualquier incidente que se pueda producir e informará los resultados de la verificación a las partes y a la sociedad. (Le puede interesar: ["Iglesia y ONU serán veedoras del cese al fuego bilateral entre el Gobierno y ELN"](https://archivo.contagioradio.com/la-iglesia-seria-veedora-del-cese-bilateral-entre-el-gobierno-y-el-eln/))

Para cumplir con el mandato, fue designado el actual coordinador del mecanismo de veeduría y verificación, el general del Salvador José Villacorta, como jefe de las Naciones Unidas para ese ejercicio. Además, Arnault recordó que a partir de la fecha se está realizando el proceso de verificación en 13 áreas y han designado “33 equipos que trabajarán **en los 33 lugares que las partes han designado** para la verificación y la prevención de incidentes”.

Con respecto al acompañamiento que realizará la iglesia católica en el proceso, Arnault enfatizó en que **su participación es una pieza clave** en la medida que es una institución que tiene “conocimiento, familiaridad y respeto en las zonas donde se realiza la verificación”.

Manifestó que el proceso de negociación con el ELN **ha generado expectativas en las comunidades** que han sufrido las consecuencias de la violencia por el conflicto armado. Dijo que “es importante que estas expectativas no sean defraudadas, la Misión hará todo lo posible para que esto se logre”.

**Reincorporación de los ex integrantes de las FARC**

El director de la Misión se refirió a la **preocupación del organismo frente al proceso de reincorporación** que se lleva a cabo con los ex integrantes de las FARC. Dijo que este proceso es un eje fundamental en la construcción de la paz, por lo que las Naciones Unidas realizaron un informe en donde hacen énfasis en la necesidad de que “la reincorporación productiva se convierta en el eje central de la reincorporación”. (Le puede interesar: ["FARC denuncia incumplimientos del Gobierno en reincorporación de menores de edad"](https://archivo.contagioradio.com/farc-denuncia-incumplimientos-del-gobierno-reincorporacion-menores-edad/))

Manifestó que se está realizando la **instalación de los consejos de reincorporación** tanto a nivel regional, como comunitario, donde “ha habido una disposición satisfactoria de las autoridades, las universidades y las empresas para consolidar el proceso de reincorporación de los ex integrantes de las FARC”.

###### Reciba toda la información de Contagio Radio en [[su correo]
