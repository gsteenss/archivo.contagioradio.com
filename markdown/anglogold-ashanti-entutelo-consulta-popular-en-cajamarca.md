Title: Anglogold Ashanti entuteló Consulta Popular en Cajamarca
Date: 2016-11-30 12:28
Category: Ambiente, Nacional
Tags: Anglogold Ashanti, Cajamarca, consulta popular minera
Slug: anglogold-ashanti-entutelo-consulta-popular-en-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cajamarca] 

###### [30 Nov 20216] 

El pasado 18 de noviembre la Multinacional Anglogold Ashanti impuso una tutela ante el Consejo de Estado, en contra del Tribunal Administrativo del Tolima, por emitir el concepto sobre la consulta popular que se realizará en Cajamarca. Tutela que la comunidad ha rechazado  y expresan que es un **“irrespeto a la democracia y al derecho constitucional del pueblo cajamarcuno** a decidir la realización de estos  proyectos mineros contaminantes sobre sus territorios”.

De igual modo Robinson Mejía, miembro del Colectivo Socio-Ambiental Juvenil de Cajamarca COSAJUCA, ha señalado que este no ha sido el único intento por frenar la consulta popular sobre el proyecto minero La Colosa, la **Procuraduría Nacional ha hecho un control de advertencia al alcalde diciéndoles que si se llega a hacer la consulta popular en Cajamarca sería un detrimento patrimonial**. Señalamiento que para Mejía no tiene sentido debido a que la inversión para la consulta es mínima y provendría del Ministerio de Hacienda.

Además, COSAJUCA, también ha expresado que no se han dado todas las garantías para que se lleve a cabo una campaña democrática en Cajamarca, esto debido a que desde que se autorizó la consulta este **equipo promotor del No, no ha tenido ningún permiso para hacer perifoneo y explicarle a sociedad en que consiste la votación**, mientras que el promotor del si ha tenido todos los medios de información.

**La consulta popular está convocada para el próximo 22 de enero**, mientras tanto el Consejo de Estado tendrá 10 días para estudiar la tutela interpuesta por Anglogold Ashanti y decidir si se continúa o no con este proceso .  A su vez el comité promotor por el No en la consulta en un comunicado de prensa expresó que continuará con las labores de pedagogía hasta el día previo a la convocatoria. Le puede interesar: ["Comité Ambiental de Cajamarca gana el primer pulso contra el Alcalde"](https://archivo.contagioradio.com/32861/).

<iframe id="audio_14324528" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14324528_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
