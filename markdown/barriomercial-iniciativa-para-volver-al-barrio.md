Title: Barriomercial, iniciativa para volver al barrio
Date: 2019-07-10 17:33
Author: CtgAdm
Category: Comunidad, eventos
Tags: Barriomercial, Conversatorio, Suba
Slug: barriomercial-iniciativa-para-volver-al-barrio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/BARRIOMERCIAL-2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/BARRIOMERCIAL.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Imagen: Colectivo La Hoguera] 

La iniciativa del colectivo la Hoguera y la Biblioteca Comunitaria El Fuerte del Viejo Topo ubicada en el barrio Aures de la localidad de Suba, pretenden disminuir el consumo en los centros comerciales y volver la mirada a las tiendas del barrio que se han visto afectadas y casi eliminadas dado el efecto económico de las grandes construcciones en la zona.

Este proyecto consiste en dar a conocer e invitar a disfrutar la variedad de ofertas en nuestros barrios a través de sus negocios y locales comerciales. La idea es que se logre la disminución del consumo en grandes superficies ya que “en los barrios encontramos tiendas, servicios, y variadas experiencias” comenta Iván Quiñones, miembro del colectivo La Hoguera.

### **Cómo consolidar un Barriomercial** 

El plan piloto que desean realizar en el barrio Aures, se divide en tres fases, la primera busca dar a conocer los atractivos comerciales del barrio para generar inquietud en sus habitantes y se potencie el consumo de productos que se generan o distribuyen en el sector. La segunda fase del proyecto busca que la comunidad se apropie de su barrio y realice un evento donde se ponga en común la implementación de la iniciativa y se evalúen los impactos dentro de la comunidad. La tercera fase se centraría en consolidar la iniciativa como un proyecto permanente.

Una de las primeras acciones es el conversatorio “Barriomercial, el barrio será la tumba del capitalismo” que se realiza este 10 de julio a las seis de la tarde en la calle 45 \# 22- 55 en el colectivo La Hoguera.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
