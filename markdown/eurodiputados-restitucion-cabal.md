Title: Europarlamentarios piden retirar proyecto de María Fda Cabal contra restitución de tierras
Date: 2018-12-12 16:46
Author: AdminContagio
Category: DDHH, Movilización
Tags: Europarlamentarios, Ley de Restitución de Tierras, María Fernanda Cabal
Slug: eurodiputados-restitucion-cabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Restitución-de-tierras-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las dos orillas 

###### 12 Dic 2018 

En una carta dirigida a la Comisión primera del Congreso colombiano, **un grupo de diputados europeos manifestó su preocupación frente al proyecto de ley 131 de 2018**, presentado por María Fernanda Cabal del Centro democrático, porque **aseguran obstaculizaría el proceso de restitución de tierras contemplado en la ley 1448 de 2011**.

En la comunicación firmada por **12 de los parlamentarios aseguran que de seguir adelante podría afectar el desarrollo del proceso de paz** "toda vez que negaría el derecho legítimo de las víctimas para en cambio **dar prioridad a proyectos agroindustriales e inversiones extranjeras en sectores extractivos, sobre tierras que han sido despojadas en el marco del conflicto armado**"

Así mismo, hacen alusión a la carta que recibieron el 23 de octubre pasado, por parte de **más de 50 organizaciones y víctimas del conflicto armado en la que rechazan el contenido del proyecto**, exponiendo la necesidad de que **se garantice la restitución, se agilicen los procesos estancados y se amplíe la vigencia de la ley que esta apunto de vencerse**.

Los firmantes instan a los congresistas a que **retiren el proyecto** como forma de garantizar la continuidad al proceso de paz, donde la **garantía de derechos para las víctimas de ver restituidas sus tierras es central,** por ser un tema fundamental para su supervivencia, así como la prorroga de la ley que permitiría responder al momento histórico que vive el país.

[Carta parlamentarios Europeos](https://www.scribd.com/document/395570236/Carta-parlamentarios-Europeos#from_embed "View Carta parlamentarios Europeos on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_23774" class="scribd_iframe_embed" title="Carta parlamentarios Europeos" src="https://www.scribd.com/embeds/395570236/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-dVNnLfgiE0K64akrSDnB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
