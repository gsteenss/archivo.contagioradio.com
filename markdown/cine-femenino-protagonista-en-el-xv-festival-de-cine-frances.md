Title: Cine femenino protagonista en el XV Festival de Cine Francés
Date: 2016-09-21 17:40
Category: Mujer, Otra Mirada
Tags: Bogotá, Cine frances, Mujeres en el cine
Slug: cine-femenino-protagonista-en-el-xv-festival-de-cine-frances
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/cropped-photo-fatima-daughters-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 21 Sep 2016 

Desde hoy y hasta el 31 de Octubre, 19 diferentes ciudades del país podrán disfrutar nuevamente del **Festival de Cine Francés**, que en su decimoquinta edición incluye la sección: ‘**Lumière sur le cinéma au feminin**’ o “Luz sobre el cine femenino” donde se expondrán filmes hechos por mujeres.

Para celebrar sus 15 años en Colombia, el Festival de cine francés que anualmente reúne a unas 80.000 personas, llega con varias sorpresas, entre ellas, el especial dedicado al cine femenino y a las mujeres cineastas, en el que se exhibirá el trabajo de 20 directoras galas como: **Mélanie Laurent** con [Mañana (Demain)](https://archivo.contagioradio.com/demain-acciones-para-salvar-el-planeta/); **Maïwenn** con Mi amor (Mon roi); **Amachoukeli y Claire Burger**, con Party girl; **Noémie Saglio** con Por primera vez (Toute première fois); entre otras.

En entrevista con la agencia EFE la agregada cultural de la embajada de Francia en Colombia, **Céline Chesnay**, aseguró que: “Este año hay una nueva temática: el cine femenino. Hay muchas más directoras francesas y es importante porque muestran una visión diferente a la de los hombres (...) Es importante mostrar su trabajo”.

Además de la sección femenina, el público podrá disfrutar de producciones que van desde “largometrajes inéditos”, “comedias cultas” hasta “fiesta del cine en animación” y “clásicos”. Asimismo, se rendirá un homenaje al director francés **Rachid Bouchareb** con la proyección de cuatro de sus películas y la presencia de la actriz belga Astrid Whetrnall.

Y finalmente se reunirán varios cineastas y expertos en el campo audiovisual, vinculando varios espacios académicos en Bogotá, donde estarán presentes **Sandro Romero** y **Franco Lolli**, quienes participarán activamente en el marco de la Carte Blanche, donde contarán todas sus experiencias y responderán preguntas relacionadas con la temática.

Para conocer la programación y salas de exhibición en su ciudad consulte www.cinefrancesencolombia.com

https://www.youtube.com/watch?v=ETcFAwYsFhw
