Title: 100 personas desplazadas en Chocó tras asesinato de guardia indígena
Date: 2020-01-10 17:06
Author: CtgAdm
Category: DDHH, Nacional
Tags: Chocó, guardia indígena, Nuquí
Slug: 100-personas-desplazadas-en-choco-tras-asesinato-a-guardia-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/desplzaamiento-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ArchivoContagioRadio] 

El 9 de enero **La Mesa de Diálogo y Concertación de los Pueblos Indígenas del Departamento del Chocó**, denunció un nuevo ataque hacia voceros comunales por parte de grupos armados, el hecho se registró en Nuquí, la víctima según la Defensoría del Pueblo fue el guardia indígena **Anuar Rojas Isaramá.**

El hecho se dio el pasado 5 de enero aproximadamente a las 10:00 p.m. en la en la comunidad Agua Blanca Tribugá, jurisdicción del Municipio de Nuquí, en un sector con alto flujo turístico de extranjeros y nacionales; allí llegaron hombres armados que según La Mesa eran integrantes del ELN.  (Le puede interesar: [Aumenta el control paramilitar en Chocó](https://archivo.contagioradio.com/aumenta-control-paramilitar-en-choco/)).

**Anuar Rojas Isaramá**, era agricultor y guardia indígena y pertenecía al Pueblo Embera Dóbida. A través de su cuenta de twitter  la Defensoría del Pueblo solicitó esclarecer  la muerte de guardia, así como la activación de las rutas institucionales de atención ante la ola de violencia que se ha evidenciado en Colombia en el 2020.   (Le puede interesar: [CIDH estudia aumento de la violencia en el Chocó](https://archivo.contagioradio.com/cidh-estudia-aumento-de-la-violencia-en-el-choco/)).

Por este hecho 23 familias (100 personas), entre ellos mujeres, niños y ancianos Emberas, decidieron dejar su territorio *"Debido al temor y zozobra se encuentran en desplazamiento forzado y masivo en la cabecera municipal de Nuquí y en el corregimiento de Tribugá"*, señaló en un comunicado la Organización Nacional Indígena de Colombia (ONIC).

En el mismo exigieron como movimiento indígena del Chocó que el  gobierno del presidente Iván Duque les brinde garantías para el fortalecimiento de los mecanismos que garanticen la protección de la vida y el territorio a través de la guardia indígena.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
