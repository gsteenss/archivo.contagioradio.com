Title: Víctimas del Palacio de Justicia esperan que Peñalosa respete la memoria
Date: 2016-01-21 13:16
Category: DDHH, Otra Mirada
Tags: Coronel Plazas Vega, Enrique Peñalosa, Palacio de Justicia
Slug: familiares-palacio-justicia-placa-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sin Olvido. 

<iframe src="http://www.ivoox.com/player_ek_10154752_2_1.html?data=kpWel5mbeZOhhpywj5aVaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncbeZpJiSo6nHuMrhwtiYxsrQb7HVzcbQy9SPqMafq9rg1s7HrcKfxtjdx9fFsozl1sqYssqJh5SZo5bOztTXpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rene Guarín] 

###### [[(]21 Ene 2016[)]] 

Luego que el Estado colombiano diera libertad al Coronel (r) Alfonso Plazas Vega implicado en los hechos que rodearon la toma y retoma del [Palacio de Justicia](https://archivo.contagioradio.com/?s=Palacio+de+Justicia), y sin que el gobierno haya cumplido las medidas de verdad, justicia y reparación a las víctimas, ahora los familiares de los desaparecidos deben luchar para que **el Alcalde de Bogotá, Enrique Peñalosa, no responda a la solicitud de Plazas Vega** de retirar la placa conmemorativa de las víctimas del 6 y 7 de noviembre de 1985.

**“Nosotros entendemos esto como un acto de revictimización y de ofensa a la memoria de nuestros familiares”**, expresa René Guarín, familiar de Cristina del Pilar, desaparecida durante la retoma del Palacio de Justicia,  frente a la solicitud del Coronel de desinstalar  la placa conmemorativa que fue instalada en el año 2012.

**Los familiares esperan que el alcalde no atienda esa petición y actúe respetando el derecho a la memoria de las víctimas y los fallos internacionales **emitidos en 2014. “Sería indignante que ahora el alcalde procediera a retirar la placa… Durante 30 años hemos estado victimizados y revictimizados por el Estado colombiano, pues la sentencia de la Corte Interamericana de Derechos Humanos no ha sido cumplida, ni en atención psicosocial, ni los pagos, ni la verdad”.

Se trata de una placa que representa un homenaje a los familiares desaparecidos por el Estado colombiano y que ya fueron reconocidos internacionalmente por CIDH y por el presidente de la República, Juan Manuel Santos el pasado 6 de noviembre de 2015. 27 años tuvieron que pasar para que por fin se diera el permiso a las víctimas para instalar la placa conmemorativa en uno de las columnas de la alcaldía de Bogotá con vista al Palacio de Justicia, y al lado de otra placa que **tergiversa la historia de los hechos, puesta por Jaime Castro, ex alcalde de Bogotá.**

**“Esperamos que el Estado no vaya a revictimizar a las víctimas más de lo que las ha revictimizado,** quitando la placa que nos costó mucho esfuerzo instalar, es una placa que reconoce los avances respecto a la comisión de la verdad, y que tiene el nombre de nuestra asociación”, señala René.

De parte de los familiares, se envió una carta a la Ángela Anzola de Toro, la nueva Alta Consejera de Víctimas  para que le haga seguimiento a esa solicitud y se impida el retiro, a lo que respondió que estará atenta al tema.

Esta situación enfrenta la verdad histórica versus la verdad judicial, ya que los familiares de los desaparecidos  tienen el legítimo derecho a mantener el ejercicio de la memoria, frente a una **verdad judicial muy cuestionada que le dio la libertad a Plazas Vega, y que pretende desconocer que hubo desaparecidos.**
