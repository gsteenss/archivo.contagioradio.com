Title: 62 masacres en los 12 municipios donde se desarrolla el proyecto Hidroituango
Date: 2018-01-22 18:04
Category: Ambiente, Nacional
Tags: Antioquia, EPM, Hidroituango, Proyecto Hidroituango
Slug: 62-masacres-los-12-municipios-donde-se-desarrolla-proyecto-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/62-masacres-en-los-12-municipios-donde-se-desarrolla-el-proyecto-Hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Ene 2018] 

Las cifras evidencian que no solo la fauna y la flora son víctimas del proyecto hidroeléctrico Hidroituango, otra de las víctimas es la memoria. En el cañón del Río Cauca, que sería inundado por Empresas Públicas de Medellín (EPM), **se cree que hay entre 300 y 600 víctimas enterradas de 62 masacres cometidas por paramilitares**, que han ocurrido en los 12 municipios afectados por Hidroituango.

De ser así, como ha dicho el Movimiento Ríos Vivos, sería una **"Una verdad que podría quedar ahogada y enterrada por la represa Hidroituango".**  Y es que como lo han denunciado en repetidas ocasiones los ambientalistas, el conflicto armado se volvió una excusa para sacar a la fuerza a las comunidades de sus territorios, para que luego estos sean objeto del modelo extractivista.

Un ejemplo de ello son las denuncias frente al proyecto hidroeléctrico de EPM. Muchas de las víctimas del conflicto fueron arrojadas desde el Puente Pescadero, ubicado a unos 40 minutos de donde se encuentra la finca La Carolina que hace parte de la jurisdicción de Yarumal. Allí fue creado y entrenado el grupo paramilitar los 12 Apóstoles que habría sido dirigido por Santiago Uribe, dueño de la finca y hermano del senador Álvaro Uribe. **Se cree que en su hacienda habrían sido asesinados al menos 533 personas,** y probablemente muchas de ellas podrían haber sido arrojadas al río.

### **En cifras** 

Dada la magnitud de la obra, esta ocupa territorio de los municipios de Ituango donde se presentaron 11 masacres que dejaron un total de 71 víctimas, Valdivia (11 masacres y 68 víctimas), Yarumal (6 masacres y 38 víctimas), San Andrés de Cuerquia (5 masacres y 32 vítimas),  Santa Fe de Antioquia (5 masacres y 23 víctimas), Buriticá (5 masacres y 22 víctimas), Peque (4 masacres y 31 víctimas), Briceño (4 masacres y 19 víctimas), Sanbanalarga (4 masacres y 26 víctimas), Toledo (4 masacres y 23 víctimas),  Liborina (3 masacres y 19 víctimas), para un total de 372 asesinatos en esa zona.

Además, de acuerdo con el Centro Nacional de Memoria Histórica, en la zona de influencia de Hidroituango se cree que hay 643 víctimas de desaparición forzada. El Registro Único de víctimas contabiliza 622, mientras que el Registro Nacional de Desaparecidos señala que son 343.

### **La memoria bajo el agua** 

El punte Pescadero se volvió un lugar para la memoria, y en el que los familiares van a orar por sus seres queridos, pues aún hay fosas comunes que, con la inundación que ocasionará Hidroituango, se perderán y los familiares no tendrían dónde llorar a sus muertos, ni la oportunidad de encontrar los restos.

Ruby Estela Posada es una de las víctimas. Manifiesta que el puente y el río Cauca **“son un cementerio de las víctimas de los grupos armados a quienes les abrían el estómago para que se llenaran de agua cuando llegaran al río y se hundieran”.** (Le puede interesar: [Memoria y resistencia en el Cañón del Río Cauca)](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/)

Ella, junto con otras víctimas, habían construido una casa de la memoria en la Playa la Arenera en el municipio de Toledo. Allí, tenían un museo donde guardaban las fotos de sus seres queridos que fueron asesinados y lanzados al río. Cada fotografía indicaba el lugar y la fecha donde fueron asesinados y guardaban también objetos como zapatos y rocas curiosas que sacaban del río. Sin embargo, cuando los barequeros fueron desalojados por parte de EPM, esa empresa quemó y destruyó la casa de la memoria.

**En 2018 iniciarían las actividades de la represa**

Este proyecto es la mayor obra de infraestructura realizada en Colombia y representa la construcción de una planta y una represa que utiliza los recursos hídricos como fuente de generación de energía.

EPM obtuvo la licencia ambiental por parte del Ministerio de Ambiente en el 2009. Un año después, la Sociedad Hidroituango suscribió un contrato con EPM para realizar las obras necesarias para construir una represa de más de 220 metros de profundidad sobre el río Cauca. E**n 2018, la hidroeléctrica comenzará a funcionar generando 2.400 mega vatios** de energía que será comercializada.

[![f07c7aa8-73df-4081-9226-addacf5b45d2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/f07c7aa8-73df-4081-9226-addacf5b45d2-800x565.jpg){.alignnone .size-medium .wp-image-50757 width="800" height="565"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/f07c7aa8-73df-4081-9226-addacf5b45d2.jpg)

###### Reciba toda la información de Contagio Radio en [[su correo]
