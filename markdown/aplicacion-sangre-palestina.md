Title: Estudiantes Gazatíes desarrollan aplicación móvil para conectar donantes de Sangre
Date: 2017-10-25 12:59
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Palestina
Slug: aplicacion-sangre-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/App-palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.middleeasteye.net 

###### 25 Oct 2017 

Un equipo compuesto por 4 jóvenes de la ciudad de Gaza desarrollaron una **aplicación móvil la cual pueden buscar como “The Palestinian Blood Bank”** o Banco Palestino de Sangre. El propósito de esta aplicación es la de **interconectar a posibles donantes de sangre y ayudar a personas que tengan la necesidad inmediata de este recurso** vital, que es muy escaso y necesitado en una ciudad con tantos problemas para el acceso a la salud como lo es Gaza.

Según el medio Middle East Eye, el mes pasado un joven Palestino sufrió un accidente de tránsito y perdió una gran cantidad sangre. Desafortunadamente la sangre de esta víctima era del tipo AB positiva, la cual es muy rara, pero **por medio de la aplicación los médicos del hospital donde era atendido pudieron entrar en contacto con un posible donante** quien no tuvo problema en acercarse al centro médico y brindar su sangre.

En palabras del Dr. Hussam Qwaider, del hospital al-Shiffa **“Esta aplicación es más efectiva para salvar vidas que las publicaciones en Facebook o las llamadas a personas aleatorias”**.

En esta emisión de Onda Palestina, también conversamos sobre el BDS en el Perú con la compañera Claudia Liberaty, sobre la decisión de los exclusivos almacenes Mitsukoshi en Tokio, de retirar de sus estantes los productos provenientes de las colonias ilegales que Israel mantiene en el territorio palestino, y sobre la liberación sexual y la población queer en Palestina, entre otros interesantes temas.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 

<iframe id="audio_21683076" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21683076_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
