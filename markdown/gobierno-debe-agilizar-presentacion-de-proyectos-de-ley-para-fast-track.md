Title: Gobierno debe agilizar presentación de proyectos de ley para Fast Track
Date: 2017-02-07 12:57
Category: Nacional, Paz
Tags: Imdelda Daza, voces de paz
Slug: gobierno-debe-agilizar-presentacion-de-proyectos-de-ley-para-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/debate-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular] 

###### [7 Feb 2017] 

**Hay demoras en la responsabilidad del gobierno a la hora de presentar los proyectos de ley  que se deben aprobar vía Fast Track y que garantizan la implementación** de los acuerdos de paz. Así lo resaltó Imelda Daza, vocera del movimiento Político Voces de Paz.

Pese a que vayan acordes al calendario legislativo los proyectos de ley y reformas constitucionales, **no han sido presentados con el debido tiempo para ser estudiados por todos los congresistas**, de tal manera que a la hora del debate hay suficiente información y se agilice el trámite. Le puede interesar:["Voces de Paz rechaza propuesta de para detener debates sobre JEP"](https://archivo.contagioradio.com/voces-de-paz-rechaza-propuesta-para-detener-debates-sobre-jep/)

“**Hay lentitud del gobierno, parece que no tiene los equipos necesarios para asumir esta labor y esa puede ser la causa**, preocupa un poco esta situación”. Hechos que se ven reflejados en las tardanzas por parte del Ministerio del Interior al presentar la ponencia de reforma a la Ley Quinta de 1992, por la que se regula la participación de Voces de Paz, en la plenaria del senado.

De hecho, aunque esta reforma ya fue aprobada en Cámara, Imelda Daza explicó que aún **no se han brindado las condiciones mínimas para operar en este escenario** debido a la falta de implementación de la reforma a la ley:

“Nosotros no tenemos sillas en donde sentarnos, nos hacemos en donde hay espacios libres, no tenemos apoyo logístico ni oficina, no contamos con asesorías de ningún tipo, y yo que vivo en Valle de Upar no cuento ni siquiera con los tiquetes, **no se nos han definido ingresos, hay descuido, es innegable”.**

La semana que viene se llevará a cabo el debate frente a la Jurisdicción Especial para la Paz, que ha generado inconformismo al interior de las organizaciones defensoras de derechos humanos p**or transformaciones que se estarían haciendo a este acto legislativo y que de acuerdo con Imelda Daza ya están siendo estudiados para garantizar que el acuerdo mantenga su esencia.** Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
