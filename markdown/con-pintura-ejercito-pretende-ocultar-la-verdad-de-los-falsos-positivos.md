Title: Con pintura, Ejército pretende ocultar responsabilidad de altos mandos en 'Falsos Positivos'
Date: 2019-10-19 08:57
Author: CtgAdm
Category: DDHH, Nacional
Tags: Altos Mandos del Ejército, Ejecuciones Extrajudiciales, falsos positivos, Mario Montoya, Nicacio Martínez
Slug: con-pintura-ejercito-pretende-ocultar-la-verdad-de-los-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Ejército.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La noche de este 18 de Octubre en Bogotá, mientras un grupo de artistas gráficos realizaban un mural alusivo a las Ejecuciones Extrajudiciales de las que serían responsables altos mandos del Ejército, **fueron intimidados por agentes de la Fuerza Pública** que además de intimidar a los autores del mural, borraron los rostros de quienes son investigados por el asesinatos de 5763 civiles falsamente presentados como muertos en combate

El mural que está ubicado en la calle 80 con carrera 30 en el sentido oriente - occidente exponía los rostros de **Juan Carlos Barrera, Adolfo León Hernández, Mario Montoya Uribe, Nicacio de Jesús Martínez y Marcos Evangelista,** altos mandos del Ejército investigados por su participación en estos hechos entre 2000 y 2010.

Aquella tarde, militares pasaron en vehículos particulares tomando fotos y grabando videos del mural que hace parte de la **Campaña por la Verdad**, para horas más tarde borrar los rostros ingresaron con tarros de pintura para cubrir los rostros. No hubo comparendo ni identificación. [(Lea también: Las Madres de Soacha no se rinden en la búsqueda de verdad)](https://archivo.contagioradio.com/las-madres-de-soacha-no-se-rinden-en-la-busqueda-de-verdad/)

**La Campaña por la verdad** es una iniciativa que nace de 11 organizaciones de DD.HH. para hacer visibles los crímenes de Estado en el marco de la justicia transicional. Pese al obrar del Ejército, a través del hashtag \#EjércitoCensuramural, en redes sociales se ha dado a conocer la imagen y esta forma de censura. [(Le puede interesar: Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos)](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/)

> [\#EjércitoCensuraMural](https://twitter.com/hashtag/Ej%C3%A9rcitoCensuraMural?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/dw6OGcoBv9](https://t.co/dw6OGcoBv9)
>
> — Movice (@Movicecol) [October 19, 2019](https://twitter.com/Movicecol/status/1185367624634556416?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

