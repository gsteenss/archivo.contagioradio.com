Title: Trump tendrá que acatar norma que pone limites a la industria petrolera
Date: 2017-07-04 18:24
Category: El mundo, Voces de la Tierra
Tags: Donald Trump, Estados Unidos, Petroleras
Slug: trump_limites_industria_petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/trump-e1485386330287.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gage Skidmore 

###### [4 Jul 2017] 

 

[Una decisión del Tribunal de Apelaciones del Distrito de Columbia, determinó que el Gobierno de EE.UU. no podrá impedir que una norma de la Agencia de Protección Ambiental, EPA limite la contaminación producida por la actividad petrolera.]

[**El gas metano por perforaciones petroleras y de gas natural, produce altos niveles de contaminación,** y aunque la administración de Donald Trump había querido impedir que este efecto de la explotación de crudo no fuera regulado el Tribunal indicó que la EPA no cumplió los requisitos la suspensión de dicha regla que había impuesto el gobierno de Barack Obama.]

[Desde la EPA se argumentaba que cuando el Gobierno de Obama aprobó esa norma, **no se permitió a los interesados, por ejemplo las empresas, discutir sobre dicha  regulación.** Aunque desde el gobierno estadounidense se quiso usar ese argumento para suspender la decisión de la anterior administración, el tribunal rechazó ese argumento.]

["El registro administrativo deja así claro que los grupos de la industria tuvieron una amplia oportunidad de comentar los cuatro temas sobre los cuales la EPA otorgó la reconsideración y, de hecho, en varios casos la agencia incorporó esos comentarios directamente en la regla final", señaló la corte, y añadió que "Por lo tanto, no era 'impracticable' para los grupos de la industria haber planteado tales objeciones durante el período de notificación y comentarios (de la Ley de Aire Limpio)".]

Esta no es la primera vez que una Corte de Estados Unidos echa para atrás una medida de Trump. Esta se enmarca en una serie de acciones del **magnate para fortalecer la industria, pero que van en contra de la lucha frente al cambio climático. **

[De hecho, organizaciones ambientalistas de ese país denuncian que se conoce de funcionarios del gobierno que están retrasando las regulaciones, sobre temas como **la contaminación por ozono, los planes de seguridad para las plantas químicas y la contaminación del metano de los vertederos.**]

[Cabe recordar que Trump ya anunció que Estados Unidos se retira de los acuerdos de la COP 21 de París, y además, hace unos meses informó que tampoco se acataría las solicitudes que obligaban a la industria de petróleo y gas a dar información sobre los equipos y emisiones en sus actividades.]

 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
