Title: Zidres pretendería extranjerizar y entregar tierras a empresarios
Date: 2015-06-17 16:52
Category: Economía, Nacional
Tags: ANZORC, apropiación ilegal de tierras, Asociación Nacional de Zonas de Reserva Campesina., baldios, César Jeréz, Gobierno Santos, Juan Fernando Cristo, proceso de paz, Zidres
Slug: zidres-pretenderia-extrangerizar-y-entregar-tierras-a-empresarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/campesinos22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.lapatria.com]

<iframe src="http://www.ivoox.com/player_ek_4654799_2_1.html?data=lZuilpydfY6ZmKiakp6Jd6KolZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRnsrY08rgjdXWqdXZz8nS1Iqnd4a1pcaYx93YtsLiyMrfy9%2FFtoztjMrb1tfJq8LmjNnWx9fWpdSfwpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Jeréz, Asociación Nacional de Zonas de Reserva Campesina, ANZORC] 

Este martes, organizaciones sociales y algunos congresistas, anunciaron su rechazo frente al proyecto de Ley 223, más conocido como Zonas de interés de desarrollo rural económico y social, Zidres, debido a que aseguran que **se trata de un proyecto que beneficiará a empresarios y no a los campesinos**, como lo viene afirmando el gobierno nacional.

De acuerdo a César Jeréz, vocero de la Asociación Nacional de Zonas de Reserva Campesina, ANZORC,  este proyecto de las **Zidres es el sexto  intento del gobierno del presidente Santos, para que los baldíos puedan ser adquiridos por latifundistas, testaferros y empresarios,** con el supuesto de adelantar grandes iniciativas de agroindustria, de manera que se estaría buscando "legalizar la adquisición ilegal de baldíos".

En ese contexto, según Jeréz, se trataría entonces de una propuesta del gobierno que es clientelista, al ser uno de los compromisos electorales que hizo Santos para beneficiar a los empresarios, y en cambio se estaría **perjudicando uno de los  acuerdos de paz** de La Habana donde se considera entregar baldíos a los campesinos.

Es por ello, que se planea demandar este proyecto de Ley propuesto por Juan Fernando Cristo, teniendo en cuenta que según un informe de la Contraloría del 2014, se registró que al menos hay **14 casos en la altillanura colombiana donde empresarios, familias políticas y terratenientes han acumulado varios baldíos.  **

El vocero de ANZORC, asegura que a través de las organizaciones sociales y algunos congresistas se está dando a conocer esta situación, donde **se pretende extranjerizar la tierra y continuar con casos de corrupción,** donde según Jeréz, el propio expresidente de la Corte Constitucional tiene que ver con casos de acumulación ilegal de tierras.
