Title: “No vamos a regresar al Cauca hasta que no salga toda la minería” Marcha de los turbantes
Date: 2014-12-26 16:16
Author: CtgAdm
Category: Otra Mirada
Slug: no-vamos-a-regresar-al-cauca-hasta-que-no-salga-toda-la-mineria-marcha-de-los-turbantes
Status: published

Crédito foto las2orillas  
[audio](http://ctgaudio.megadatesystem.com/upload/0/fsZdoTIQ.mp3)  
 

Cerca de 40 mujeres de varios municipios del Norte del Cauca, y otras que se han sumado, caminan desde esa región hasta la ciudad de Bogotá, para decirle al gobierno nacional que tiene que actuar para proteger los territorios de las comunidades negras, que se están “desbaratando” con la locomotora minera. El deterioro es muy evidente y  la situación hace que sean urgentes medidas de protección efectivas.

Sofía Garzón, integrante del Proceso de Comunidades Negras, relata que las mujeres caminan en medio de mucho temor pero con la esperanza y la decisión de defender sus tierras, sin importar el cansancio y la hinchazón de las piernas. Sin embargo se revitalizan todos los días por la solidaridad de muchas personas y organizaciones que caminan a su lado y que respaldan su lucha.
