Title: Disolución final de ETA tras 60 años de historia
Date: 2018-05-06 09:40
Category: Opinion
Tags: ETA, procesos de paz
Slug: disolucion-final-de-eta-tras-60-anos-de-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/eta-portada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Anagramde de ETA/ [Theklan](https://commons.wikimedia.org/wiki/User:Theklan "User:Theklan") 

#### **Por José Angel Sánchez Rocamora** 

[Han pasado ya 60 años desde que inició la actividad de la organización armada **Euskadi Ta Askatasuna (ETA)** (del euskera, 'País Vasco y Libertad') por la independencia de Euskal Herria ligada al marxismo-leninismo y a la liberación nacional.]

[Siendo el grupo armado más antiguo del estado español y de Europa su recorrido ha pasado por distintas etapas y en determinados momentos ha gozado de bastante popularidad. Hasta el día de hoy en el que la gran mayoría de sus antiguos simpatizantes demandaban su disolución total.]

\[caption id="" align="alignnone" width="800"\]![101977\_eta2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/101977_eta2-800x450.jpg){.wp-image-53170 .size-medium width="800" height="450"} Durante 60 años ETA utilizó el símbolo del hacha y la serpiente para identificarse, éste fue creado por el anarquista Félix Likiniano.\[/caption\]

[La estrategia de la lucha armada comenzó en la dictadura franquista, tiempos de represión contra toda disidencia.  Atentados como el de Carrero Blanco (sucesor de Franco), fueron los que posibilitaron el apoyo necesario para constituirse como grupo armado.]

[Entre los años 70´s y 80´s se vivió una etapa de fuertes luchas de carácter revolucionario tanto en Euskadi como en el resto del estado español. Después de años de dictadura, en la sociedad civil se habían gestado procesos de lucha, en los que distintos sectores abogaban por la lucha armada. Así se crearon distintos grupos armados como lo fueron, ETA, el MIL, GRAPO o Terra Lliure.]

[Fueron los conocidos años de plomo donde la ultraderecha asesinó a cientos de militantes de izquierdas, cuando la droga acababa con generaciones enteras y la sombra del franquismo continuaba en pie.]

[**A partir de 1975 comienza la guerra sucia del estado contra ETA.** Grupos armados de carácter paramilitar en connivencia con el gobierno comenzaron a asesinar a militantes de la banda, simpatizantes y población civil indiscriminadamente. Ministros del gobierno se vieron implicados por el escándalo y fueron judicializados, aunque a pesar de todo lo sucedido quienes fueron protagonistas de la guerra sucia gozaron de un gran nivel de impunidad hasta el día de hoy.]

[Entre los años 80´s y 90´s se realizaron distintas intentonas de negociaciones de paz que nunca funcionaron, fue entonces cuando comenzó la aplicación por parte del estado de la conocida “doctrina de seguridad ciudadana o del entorno” enarbolada por jueces como Baltasar Garzón. Es decir, fue judicializado todo el “entorno de ETA”, no solo simpatizantes sino también partidos políticos, periódicos, organizaciones sociales…etc.]

[ Cientos de personas fueron encarceladas en aplicación de la ley antiterrorista, vinculados con la banda armada a través de distintos montajes judiciales. Además, fue endurecida la política penitenciaria contra el colectivo de presos vascos, que además de la dispersión, les fue aplicada la “doctrina Parot” (en 2003 el Tribunal de Estrasburgo la ilegalizó), prolongando sus penas hasta casi la cadena perpetua.]

[Pero también fueron los años en los que los atentados y acciones armadas de ETA comenzaron a tener menos popularidad hasta el punto en el que la sociedad vasca y la española se organizó creando movimientos cívicos en contra, exigiendo el final del grupo armado y de la violencia.]

\[caption id="attachment\_53161" align="alignnone" width="800"\]![1525272030\_001751\_1525279640\_album\_normal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/1525272030_001751_1525279640_album_normal-800x528.jpg){.wp-image-53161 .size-medium width="800" height="528"} Atentado ETA contra refinería de Enpetrol (Empresa Nacional del Petróleo) - 1987 Foto: Antonio Espejo\[/caption\]

[Fue en el 2011 cuando, desde los distintos sectores de la sociedad civil vasca comenzó el actual proceso de paz. Ya en 2010 se habían iniciado contactos con expertos en resolución de conflictos armados y mediadores internacional; el 12 de noviembre se formó el Grupo Internacional de Contacto (GIC), formado por un nutrido grupo de expertos en conflictos armados con el objetivo de mediar entre las partes implicadas.]

[El 10 de enero de 2011 cumpliendo con las reivindicaciones de las organizaciones sociales y la sociedad vasca, ETA declara el alto al fuego permanente y verificable. El 17 de octubre de ese mismo año se realiza la Conferencia de Aiete en la que el GIC apoyado por personalidades como Kofi Annan presentaron la hoja de ruta para la resolución del conflicto vasco. Después de la conferencia y en el mismo mes ETA anuncia el final de la lucha armada.]

[En 2012 se realizan intentos de negociación con los estados francés y español recibiendo siempre una negativa como respuesta. ETA exigía el fin de la dispersión de los presos y una política menos represiva contra ellos a cambio de su disolución total. En este año se crea también la organización social “Bake Bidea” en el norte del País Vasco con el fin de sensibilizar y generar espacios para el debate en el marco del proceso de paz. Esta organización está constituida por sindicatos y partidos políticos además de personalidades a título individual.]

[En 2013 se produce una manifestación en Bilbao  a la que asisten 115.000 personas para exigir el respeto de los derechos humanos de los presos vascos. En este mismo año el colectivo de presos vascos y de refugiados se adhieren al proceso de paz y a las declaraciones de la conferencia de Aiete.]

[En 2014 ETA inicia el desarme con un acto simbólico de entrega de una parte de las armas, verificado por la CIV (Comisión internacional de verificación), ese mismo año la banda anuncia que sus estructuras habían sido desmanteladas.]

[En 2016 comienza el proceso de desarme mediado por la sociedad civil y el GIC, creándose los conocidos como “Artesanos de paz”, formado por un grupo de personalidades ligadas al pacifismo .Quienes se encargaron de escalonar el desarme, siendo detenidos por las autoridades estatales. Pero la verificación del desarme continuó.]

[En junio del mismo año se forma el Foro Social Permanente por la Paz para realizar un trabajo ligado a las consecuencias del conflicto vasco y continuar trabajando en el proceso de paz en base a cinco puntos: el desarme, los consensos básicos, la integración de los presos y refugiados, garantizar los DDHH y preservar la memoria y la verdad en pro de una convivencia futura.]

> "ETA surgió de este pueblo y ahora se disuelve en él"

![final eta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/final-eta-512x800.jpg){.size-medium .wp-image-53158 .aligncenter width="512" height="800"}En 2018 el gobierno francés confirma que acercará a los presos al País Vasco. El 3 de mayo ETA confirma su disolución final y el 4 del mismo mes se realiza el Encuentro Internacional para avanzar en la resolución del conflicto vasco en Kambo, localidad del País vasco francés.

https://www.youtube.com/watch?v=rtiTo\_qRNbc

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
[Distintas personalidades del GIC, Bake Bidea y del Foro social permanente para la paz se dieron cita en Kambo, en el País Vasco francés. Mediadores internacionales como Gerry Adams (líder histórico del Sinn Fein), Brian Curris (abogado Surafricano, activista contra el Apartheid) o Jonathan Powell (exjefe del gabinete de Tony Blair) junto con líderes de organizaciones sociales, partidos políticos y sindicatos de la sociedad vasca confirmaron el final de ETA y calificaron de histórico el momento.]

</p>
\

[Todos coincidieron en que la declaración de Kambo no tiene marcha atrás y valoraron positivamente que este proceso de paz haya sido el único que se ha impulsado por la sociedad civil. Pero también criticaron duramente que los estados español y francés no hayan participado al igual que la dispersión y la vulneración de derechos humanos de los presos.]

Son muchas las incógnitas que quedan por esclarecer y mucho el trabajo por realizar aún, por lo que el proceso de paz va a continuar.

Conceptos como memoria, verdad, reparación, garantías de no repetición, justicia social y reconciliación son parte de los ejes de trabajo a futuro. Además de la dignificación de las víctimas de la banda y de las del terrorismo de estado (estas últimas ninguneadas y clasificadas como de segunda), queda el principal problema para la reconciliación. Es decir, los presos de ETA y las durísimas políticas penitenciarias desplegadas por el estado español que vulneran sus derechos sistemáticamente.

[Otras también están en la agenda de paz como los más de 300 asesinatos que quedan aún por esclarecer, los refugiados vascos, y la problemática de cómo realizar la memoria histórica ligada a la verdad sin haberse realizado un proceso de paz entre las partes en conflicto.]

https://www.youtube.com/watch?v=hL3XEy485Ac

[Ante la negativa de gran parte de las organizaciones sociales de víctimas a negociar o participar del proceso de paz y del bloqueo constante del estado español y francés, dicho proceso de paz solo va a poder continuar trabajando mediante el apoyo de la sociedad civil y de los mediadores internacionales.]

858 victimas de ETA mortales, y 245 presos vascos es el balance que deja 60 años de lucha armada y guerra sucia por parte del estado español. Una historia de dolor y sangre que ahora ha encontrado un camino para su resolución sin olvidar la cultura de lucha contra la injusticia social que ha dejado este periodo de la historia.

Con la esperanza que las generaciones venideras vivan  en un futuro de paz con justicia social. Así termina esta etapa del conflicto vasco.

#### **Por José Angel Sánchez Rocamora** 
