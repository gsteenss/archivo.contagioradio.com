Title: En 2017 han sido asesinados 41 líderes sociales
Date: 2017-04-27 19:20
Category: DDHH, Entrevistas
Tags: lideres sociales, marcha patriotica, Paramilitarismo
Slug: en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Asesinan-a-Luz-Herminia-Olarte-lideresa-social-de-Yarumal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Abr. 2017] 

**41 asesinatos a líderes sociales y defensores de Derechos Humanos se han reportado en lo corrido del presente año**, según el Informe Especial “Sobre Homicidios de líderes sociales y defensores de Derechos Humanos e Integrantes de las FARC –EP y familiares” de la Comisión Nacional de Derechos Humanos Movimiento Político y Social Marcha Patriótica. Según Cristian Delgado, vocero del Movimiento Marcha Patriótica, el gobierno nacional no se ha manifestado frente a las investigaciones sobre los posibles autores.

El Informe Especial del Movimiento Marcha Patriótica, que contiene 18 páginas y habla sobre la situación de derechos humanos, en cuanto a seguridad y protección, expone que tan sólo en 12 días, entre el 14 de abril y el 25 de abril del 2017, **se ha reportado el homicidio de 2 integrantes activos de las FARC –EP y el homicidio de 6 familiares de integrantes activos de las FARC-EP** que se encuentran en zonas veredales.

El día de hoy 27 de abril, según Delgado, se recibió el reporte de un nuevo asesinato, del representante legal de un Consejo Comunitario de Mercaderes. Con esto, la cifra sube a 42 asesinatos en el transcurso del año.

Las cifras respecto al asesinato de integrantes de las FARC y sus familiares, según Delgado, se concentran en el mes de abril, justo cuando hay un descenso de homicidios de líderes sociales y defensores de derechos humanos, existiendo un patrón común puesto que ocurren en zonas donde se ha denunciado presencia paramilitar. **Los asesinatos se han presentado en Cauca, Antioquia, Córdoba, Chocó y Nariño.**

\[caption id="attachment\_39806" align="aligncenter" width="540"\]![Homicidios líderes sociales y defensores de Derechos Humanos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/tabla.jpg){.wp-image-39806 .size-full width="540" height="302"} Tomado de: Informe Especial - Movimiento Marcha Patriótica\[/caption\]

No obstante, la Fiscalía General de la Nación no ha manifestado los resultados de investigaciones acerca de los autores de los asesinatos. **“Persiste la negación del gobierno nacional sobre el recrudecimiento del accionar paramilitar en vastas regiones de la geografía nacional”**, según el Informe Especial del Movimiento Marcha Patriótica. Delgado afirma que el gobierno además de negar, ha buscado otras posibles causas de los homicidios que se están presentando a lo largo y ancho del país.

A pesar de que el Gobierno no ha considerado estos hechos como algo sistemático, Delgado afirma que **hay un problema de connivencia entre grupos paramilitares y agentes de la fuerza pública,** pues en algunos departamentos se han registrado denuncias por presencia de 250 a 300 paramilitares cercanos a bases o patrullas militares, concretamente en Córdoba, Antioquia y Chocó. Le puede interesar: [Confirman complicidad y connivencia de AGC y militares en córdoba](https://archivo.contagioradio.com/complicidad-y-connivencia-agc-militares-cordoba/)

Lo anterior se evidencia con la denuncia realizada por la comisión de Derechos Humanos de Marcha Patriótica en el departamento de Córdoba el pasado 22 abril, donde**se evidenció la connivencia de paramilitares de las llamadas Autodefensas Gaitanistas de Colombia, AGC, con las Fuerzas Militares,** concretamente del Batallón de Infantería No. 33 Junín de la Brigada.

Asimismo, Delgado pone como ejemplo lo ocurrido en Córdoba donde se hizo la **denuncia de la concentración de los paramilitares y las instituciones del Estado no hicieron nada, dejando como resultado el asesinato de 3 campesinos.** Delgado afirma que el Estado obvia en señalar que grupos paramilitares están llegando por el narcotráfico, a zonas de importancia geoestratégica para la movilidad en términos de guerra y a zonas con alta presencia de minerales.

Hecho que preocupa a las comunidades y líderes de las regiones. **“Desafortunadamente siguen ocurriendo hechos de violencia sociopolítica en el país y no hay acciones efectivas y eficaces por parte del estado colombiano”** afirma Delgado. Están asesinando a los defensores de territorio, a los que luchan por los Derechos Humanos y el Medio Ambiente.

<iframe id="audio_18397661" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18397661_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
