Title: En Colombia la población en cárceles aumentó 141%  desde el año 2000
Date: 2017-03-16 18:17
Category: Judicial, Nacional
Tags: crisis carcelaria, Cultivos de uso ilícito, narcotrafico
Slug: en-colombia-la-poblacion-en-carceles-aumento-141-desde-el-ano-2000
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ICRC 

###### [16 Mar 2017] 

“La cárcel se ha utilizado de forma excesiva y no ha resuelto nada de fondo”, es la principal conclusión del informe **‘Castigos irracionales: leyes de drogas y encarcelamiento de América Latina’.** El estudio analiza la crisis carcelaria que viven los países de América Latina y Estados Unidos, donde la sobrepoblación y las condiciones inhumanas representan un panorama imposible para la resocialización de presos.

De acuerdo con el informe, el continente americano es el que más recurre a encarcelar a la población, de hecho en los últimos años, la población en las cárceles creció un 41%. A su vez, es el continente con mayor número de personas en prisión por delitos relacionados con narcotráfico. Asimismo, el informe muestra que en **América se reportó la muerte violenta de 2.549 personas al interior de los centros carcelarios. **

En todos los países latinoamericanos estudiados, menos Bolivia, la población encarcelada por drogas aumentó más rápido que la población carcelaria en general, a un ritmo de entre 8 y 33 veces en los últimos 15 años.  En Brasil, la población en prisión por delitos relacionados con drogas aumentó un 267%. En México, en 2012, el 62% de las personas internas en centros penitenciarios federales lo estaban por delitos de drogas. De ellas, el 58.7% fueron sentenciadas por un delito relacionando al cannabis y, de esos casos, el 38.5% por posesión.

Coletta Youngers, Asesora Principal en WOLA y co-autora de la investigación, afirma que “Es irónico que en los Estados Unidos el 20% de los ciudadanos vive en estados que ya han legalizado o están por legalizar el uso recreacional del cannabis, y mientras tanto el gobierno de México sigue gastando recursos escasos en encarcelar a consumidores de marihuana”.

### El caso colombiano 

**En Colombia, entre 2000 y 2015, la población carcelaria creció 141%.** A su vez, las personas en prisión por delitos de drogas aumentaron  en un 289%.  Lo alarmante, es el 93% de las personas capturadas por ese tipo de delitos, tenían menos de 250 gramos de marihuana, cocaína o basuco. Lo que quiere decir que la justicia, solo está recayendo en el eslabón más dábil de la cadena del narcotráfico.

Para Sergio Chaparro Hernández, otro de los investigadores que hizo parte de la realización de informe, e integrante de Dejusticia, en el caso colombiano, la cárcel se ha utilizado de forma excesiva y sin arreglar los problemas de fondo.

Desde el enfoque de género en Colombia 5 de cada 10 mujeres en prisión lo están por hechos relacionados con el narcotráfico. **Entre 2010 y 2014, el 93% de las mujeres apresadas por ese motivo tenían hijos, mientras el 52.8% eran madres solteras.** De manera que los centros penitenciarios suelen implicar una mayor presión  y una grave afectación para las familias de esas mujeres que, en la mayoría de casos, comente estos delitos debido a la posición de vulnerabilidad económica y familiar en la que se encuentran. [(Le puede interesar: Encuentro internacional de mujeres cocaleras en el que las mujeres de diversos lugares del país)](https://archivo.contagioradio.com/mujeres-cocaleras-de-todo-el-pais-se-reunen-para-trabajar-propuestas-de-sustitucion/)

**“Las cárceles se han convertido en la primer respuesta al tema de las drogas, en vez de abordar la situación desde un punto de vista social, educativo y de salud”,** asegura Catalina Pérez Correa, coordinadora del CEDD y co-autora del estudio, quien agrega que “Los encarcelados no son los grandes traficantes que están atrás de la corrupción y violencia que acecha a nuestros países, sino en su gran mayoría gente pobre con un rol insignificante en el tráfico y consumidores, y muchas veces por sustancias como el cannabis”.

El informe estudió 10 países del continente en el que además de evidenciar la crisis carcelaria se dan una serie de recomendaciones a los Estados para combatir de una forma más efectiva los delitos. Entre ellas se aconseja evitar la detención preventiva en caso de delitos de drogas menores, aplicar amnistías especiales para los sentenciados por estos delitos y para quienes no tengan antecedentes penales, promover los clubes de cannabis, descriminalizar el consumo de drogas, y no encarcelar a las mujeres embarazadas y madres de menores condenadas por delitos de drogas no violentos.

<iframe id="doc_12664" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342124317/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-aSg0Af4s6ORMaDZ2RuLG&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
