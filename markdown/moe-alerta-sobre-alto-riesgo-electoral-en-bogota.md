Title: MOE alerta sobre alto riesgo electoral en Bogotá
Date: 2015-09-29 12:25
Category: Nacional, Política
Tags: Comisión de seguimiento electoral, Compra de votos, elecciones, Elecciones a alcaldía de Bogotá, Elecciones al concejo, Fraude electoral, MOE
Slug: moe-alerta-sobre-alto-riesgo-electoral-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Fraude.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:las2orillas.com 

###### [29 sep 2015] 

La misión de Observación Electoral (MOE) indicó que faltando un mes para las elecciones de alcaldes, ediles y concejales en el país, en **Bogotá 128 puestos de votación presentan irregularidades**, sobre los cuales las autoridades deben prestar especial atención, para evitar manipulación electoral.

El informe muestra que los casos más alarmantes se presentan en las localidades de Sumapaz, que de los 4 puestos de votación 4 están en riesgo; Ciudad Bolívar que tiene 48 puestos y 31 están en riesgo; Usme con 33 puestos, tiene 11 en riesgo, además de otras 12 localidades.

En el informe que **ya está en manos de la Comisión de seguimiento electoral** para que las autoridades se encarguen, se encontraron tres variables que condicionan esta atipicidad electoral:

-**Los tarjetones no marcados**

- **El dominio electoral por parte de un candidato, en donde en un mismo puesto todos los votos van destinados hacia una misma persona, en medio de una diversidad de votantes**

Aura Rodríguez, coordinadora de la MOE en Bogotá, indica que para que estos hechos no se presenten, la registraduría “*garantice jurados no solo bien capacitados, sino que provengan de diversas bases de datos, para mitigar posibles manipulaciones de las campañas durante preconteo y escrutinio*".

Por otra parte el Consejo Nacional Electoral afirmó esta mañana que **se han revocado las candidaturas de 974 candidatos** luego de que se recibieron en la institución cerca de 1800 denuncias por inhabilidades en todo el país. Según el CNE las principales irregularidades se presentan porque hay fallos judiciales o de la procuraduría.

El presidente del CNE, el magistrado Emiliano Rivera, afirmó que se deben revisar las actuaciones de los partidos políticos que no depuraron las listas de los candidatos a inscribir. **Además confirmó que se han anulado 1'182.196 cédulas inscritas por evidencia de transhumancia electoral**.

Frente a las denuncias la MOE indicó que los ciudadanos pueden realizar su denuncia de forma anónima o pública, reportando cualquier situación de irregularidades y denuncias en las elecciones, en todo el país. con los siguientes medios:

**Aplicación para celular**: Pilas con el Voto

**Vía Web**: [www.Pilasconelvoto.com](http://www.Pilasconelvoto.com)

**Línea de Atención** : 018000112101
