Title: Tras 20 años, nueve familias de Turbo, Antioquia recibirán los títulos de sus predios restituidos
Date: 2020-12-17 15:09
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, desplazamientos en Turbo, Restitución de tierras, Turbo
Slug: tras-20-anos-nueve-familias-de-turbo-antioquia-recibiran-los-titulos-de-sus-predios-restituidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesino-e1460152084815.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 18 de diciembre nueve familias víctimas de despojo de tierra en la vereda California, en Turbo subregión de Urabá en Antioquia, **recibieron el titulo que los acredita como propietarios de sus predios, luego de 20 años de salir de manera forzada de estos.**

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://cdn.shortpixel.ai/spai/w_570+q_lossy+ret_img+to_webp/https://www.forjandofuturos.org/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-18-at-10.23.06-AM.jpeg)

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Son 9 familias las que desde el mes de marzo de este año esperaban que se llevara a cabo la entrega material y oficial de los títulos que les reconocían legalmente como propietarios de sus predios, de los cuales habían sido despojados hace más de 20 años, y que por razones de la pandemia no se había podido llevar a cabo hasta este viernes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El espacio de entrega se llevó a cabo en la ciudad de Medellín y será precedido por el **director de la Unidad de Restitución de Tierras, Andrés Castro,** quién entregará los documentos que acredita a las familias como titulares de los predios. ([Otra Mirada: Riesgos de reclamar tierras en Colombia](https://archivo.contagioradio.com/otra-mirada-riesgos-de-reclamar-tierras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Secretaría de Gobierno de la Gobernación de Antioquia, la Alcaldía de Turbo, el Departamento de Policía de Urabá, la Procuraduría de Tierras,y la Defensoría del Pueblo, fueron testigos de esta entrega acompañada por la **Fundación Forjando Futuros, representante legal de las familias, y otras organizaciones sociales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hijo del reclamante de tierras Uber Quintero, asesinado en 1993, presente en la entrega de los títulos de restitución señaló que e**sta es una historia que empezó en 1987, a partir de una política de recuperación de tierras de organizaciones y sindicatos campesinos por medio de la organización política de la Unión Patriótica**, conformado por varias víctimas despojo entre ellas Quintero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Una acción de reclamación que estuvo acompañada de persecución por parte de la fuerza pública**, la policía Nacional y grupos al margen de la ley de ese momento; recordando este pasado de esfuerzos de su padre y otras víctimas indicó que esta acción ***"es un reconocimiento al esfuerzo y a la dignidad de de la memoria de quienes ya no están"***, pero que al mismo tiempo es necesario *"que nos dejen de revictimizar, que no nos presionen ni nos cierren las puertas, y que se mantenga el respeto por la memoria de las personas que perdieron la vida en este proceso, reiterando que ellos y ellas no son víctimas falsas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que el proceso de reclamación hoy de las víctimas, exigen verdad, justicia y reparación, destacando que en el caso del asesinato de su padre Uber Quintero se mantiene la impunidad frente a su homicidio hace más de 20 años.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los obstáculos en la restitución de tierras de las familias en Turbo
--------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las familias que reciben sus títulos fueron despojadas hace 20 años y luego para volver, fueron presionadas a pagar por esos predios al excomandante de las AUC [Raúl Emilio Hasbún](https://verdadabierta.com/wp-content/uploads/2018/12/Corte-Suprema-de-Justicia-segunda-instancia-43707.pdf) y el empresario Felipe Echeverry. Dicho territorios e**ran puntos estratégicos al estar ubicados en inmediaciones de Puerto Antioquia, Urabá, unas de las tierras más costosas y disputadas del país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A tales circunstancias se suman antecedentes históricos que han marcado el camino de obstáculos de los y las reclamantes de tierra en Turbo como la revisión de las actas del Comité de Justicia Transicional, en donde se develaba que las entidades responsables de estar presentes en las audiencias no asistían.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***«En el caso de Turbo, en las agendas del Comité ni siquiera estaba hablar el tema de reclamantes de tierras»,*** indica el texto *«El proceso de restitución de tierras en el Urabá Antioqueño»*, libro en el que se analiza el desarrollo que se ha dado a la Ley 1448 de 2011, o ley de víctimas. (*[Los seis obstáculos a la restitución de tierras en Urabá](https://archivo.contagioradio.com/obstaculos-restitucion-de-tierras/)*)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se suma también **un texto en el que la Defensoría del Pueblo en Urabá narraba cómo coordinar junto a la Alcaldía de Turbo el desalojo de campesinos, narrando la destinación de recursos por más de 60 millones para la operación**, en la que además se usaría un helicóptero que trasladaría al Escuadrón Móvil Antidisturbios (ESMAD) a los predios de los campesinos, mientras la Defensoría garantizaba que esto se pudiera realizar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el 2020, defensores de DD.HH. alertaron en varias ocasiones sobre las acciones violentas en contra de reclamantes de tierras, acciones que **además de causar temor en las familias buscan desarticular los procesos de los reclamantes, por medio de la confusión y persecución**-

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto llevó a que se exigieran garantías de vida, para los reclamantes de tierras, especialmente aquellos que han regresado a los [corregimientos de Blanquiceth y Macondo](https://archivo.contagioradio.com/en-turbo-campesinos-estan-siendo-obligados-a-entregar-el-50-de-las-tierras-que-habitan-y-trabajan/)con la esperanza de trabajar y vivir en aquellos territorios como sus legítimos propietarios, garantías de retorno y habitabilidad que se espera estén presentes en el retorno de las nueve familias en la vereda California, luego de la entrega física de los títulos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Un proceso legar que continua
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Forjando Futuros, *"esta entrega consolida el compromiso de autoridades civiles y militares con la orden proferida por el Tribunal Superior de Antioquia de restituir materialmente y de manera integral las tierras de estas personas"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron también mediante el balance que hacen de la entrega, que continua este proceso ante el Tribunal Superior de Antioquia , desde donde **ordenan a la Fiscalía investigar penalmente a los propietarios de las empresas bananeras: Uniban, Banacol, Bananeras de Urabá S.A.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Investigaciones que se extienden también a las y los empresarios R**osalba Zapata y Felipe Arcesio Echeverry Zapata, quienes desde el mes de julio realizaban campañas de desprestigio en medios locales a los reclamantes de tierra**s y las organizaciones acompañantes. ([Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El cargos según el tribunal por el que serán investigados son, *"presunto delito de concierto para delinquir por la financiación voluntaria a grupos paramilitares, que fue declarado como delito de lesa humanidad*".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
