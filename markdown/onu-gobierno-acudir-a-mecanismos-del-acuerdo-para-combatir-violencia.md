Title: Acuerdo de paz tiene herramientas para frenar la violencia en Colombia: ONU
Date: 2020-10-02 18:55
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de líderes sociales, Excombatientes asesinados, implementación acuerdos de paz, ONU
Slug: onu-gobierno-acudir-a-mecanismos-del-acuerdo-para-combatir-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Antonio-Guterrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: António Guterres, secretario General de la ONU/ @antonioguterres

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe trimestral del secretario general de las Naciones Unidas, Antonio Guterres será presentado ante el Consejo de Seguridad de la ONU el 14 de octubre y que abarca el lapso entre el 27 de junio y el 25 de septiembre de 2020, reiterando la necesidad de la implementación del Acuerdo, ante la escalada de violencia que se ha vivido en las últimos meses.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los datos recopilados por la ONU demostrarían que dos de tres tercios de los excombatientes de FARC, que corresponden a cerca del 70% de la población desmovilizada, según el Consejo Nacional de Reincorporación, que viven fuera de los Espacios Territoriales de Capacitación y Reincorporación **no cuentan con garantías de vida ni económicas.** Añaden que en los últimos tres meses han sido asesinados 19 firmantes de la paz, sumando un total de 50 en lo que va del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La ONU además destacó que 400 excombatientes continúan a la espera de que la Unidad Nacional de Protección les asigne un esquema de protección. Agregan que desde la firma del Acuerdo, **224 exguerrilleros han sido asesinados y 20 desaparecidos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe además advierte sobre el asesinato de 48 líderes sociales y 51 sucesos de este tipo que están en proceso de verificación y que han ocurrido en los últimos tres meses. Si bien estas son los registros que ha entregado la ONU, desde otras organizaciones como [Indepaz](http://www.indepaz.org.co/lideres/), **el balance es más desalentador con 221 líderes y 47 excombatientes asesinados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las Naciones Unidas además han alertado sobre el desplazamiento forzado de 19.000 personas en lo corrido del 2020. Además alerta sobre 42 casos de masacres verificadas por la Oficina del Alta Comisionada de Naciones Unidas para los Derechos Humanos de masacres ocurridas en el año y otras 13 que deben ser verificadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según expertos, las masacres ascienden a más de 63, explicando que los departamentos de Antioquia donde se han presentado 14 masacres, Cauca y Nariño con 9, Norte de Santander con 6 y Putumayo con 4, son los departamentos más afectados. [(Lea también: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano)](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Partes del acuerdo deben reconocer potencialidad de la implementación: ONU

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La ONU resalta que varios de los hechos de violencia ocurridos en los últimos tres meses ocurrieron en regiones donde la **Defensoría del Pueblo** activó su sistema **de alertas tempranas**, "estos crímenes están siendo perpetrados principalmente en zonas con escasa presencia estatal, altos niveles de pobreza, economías ilícitas y disputas entre grupos armados ilegales y organizaciones criminales", acuña.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los adelantos de este informe se conocen una semana después de que el Consejo de Seguridad de la ONU, en una decisión unánime, extendiera hasta el 25 de septiembre del año 2021 el mandato de la Misión de Verificación del Acuerdo de Paz en Colombia. [(ONU extiende Misión de Verificación del Acuerdo de Paz en Colombia)](https://archivo.contagioradio.com/onu-extiende-mision-de-verificacion-del-acuerdo-de-paz-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La ONU ha alertado una vez más sobre la necesidad de avanzar en la implementación del Acuerdo de Paz a través de un diálogo entre las partes, incluidas instancias como la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final y el Consejo Nacional de Reincorporación". y a **que se reconozca el potencial que tiene la implementación como un elemento clave en el desarrollo del país, más en épocas de pandemia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el mes de julio, cuando se entregó el informe pasado, la Misión ya había hecho un llamado de atención ante la alta cifra de asesinatos de líderes sociales y firmantes del Acuerdo e instó al Gobierno a adoptar medidas inmediatas y concretas para hacer frente estas problemáticas pues tal como han comunicado las comunidades se requiere una presencia estatal allá de la militar, basada en inversión social.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
