Title: Así avanza el paro nacional en la Región Andina
Date: 2016-05-30 00:06
Category: Paro Nacional
Tags: Minga Nacional, Paro Agrario, Paro Nacional
Slug: asi-avanza-el-paro-nacional-en-la-region-andina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/region-andina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

### Norte de Santander

**11 de Junio:** Sobre las 8 de la noche, el ESMAD y Ejército siguen haciendo presencia en La Lizama, a 50 metros del área de concentración de los manifestantes de la Minga, quienes a esta hora se encuentran atemorizados de que pueda existir una nueva arremetida por parte del ESMAD. Según se denuncia, el Mayor Toro del Ejército tiene la intensión de desalojarlos. A esta hora, cae fuerte aguacero y los manifestantes no tienen donde resguardarse.

Desde las 5 pm policía y FFMM en acción coordinada atacaron a manifestantes destruyendo comida, regando el agua, y además destruyeron los cambuches. Además, se denuncia que utilizan bloqueador de onda para no permitir el envío de información ni de fotos de la situación.

**10 de Junio:** Organizaciones de Marcha Patriótica del Norte de Santander inician plantón frente a la Gobernación  en respaldo a la Minga Campesina, étnica y Popular y en rechazo a las declaraciones Gobernador William Villamizar

**9 de Junio:** La toma en  la Defensoría del Pueblo, en Cúcuta  finalizó sobre las 9 de la noche del miércoles logrando un acta cuyos puntos se van a seguir discutiendo en diferentes instancias. Entre los pactos, se acordó una reunión con la Secretaria de gobierno para analizar la situación carcelaria, de la que están pendientes las Naciones Unidas y la OEA.

Esta toma tuvo una duración total de 12 horas. Allí se presentó el  pliego de peticiones sobre dos temas: la situación carcelaria y la situación de los pequeños comerciantes de gasolina. Luego se realizó una marcha de más de 300 familiares de los detenidos de la cárcel de Cúcuta.

**8 de Junio: **20 familias de comerciantes detenidos adelantan una toma pacifica de la Defensoría del Pueblo en Cúcuta, exigiendo el respeto de las personas detenidas y en apoyo a la Minga Nacional.

**7 de Junio:** Desde las 8 de la mañana en la Universidad de Pamplona, se realiza el Foro Educación Paz y Reconciliación con la participación del profesor de la universidad Nacional Víctor de Currea Lugo y Sebastián Quiroga, vocero del Congreso de los Pueblos

**5 de junio:** En La Lizama, Barrancabermeja denuncian que durante el transcurso del día miembros de la fuerza pública han mantenido presencia en el sitio de concentración de las comunidades. Hacia las  4:50 pm varios policiales estaban tomaron fotografías de los campesinos que entregaban volantes informativos a los ciudadanos y ciudadanas que transitaban por la carretera. Uno de los uniformados que estaba realizando el registro, mostraba permanentemente a sus compañeros las imágenes, señalando a los manifestantes captados.

**4 de Junio:** Salen los 115 detenidos en Berlín.

**3 de Junio:** Hacia las 5:30 de la tarde reportan detenidos en Berlín, 4 mujeres, 15 menores, 115 Hombres. Además se reportaron agresiones contra los manifestantes por parte del ESMAD entre ellos hay mujeres, adultos mayores, menores de edad.

**2 de Junio:** Comunidades denuncian que sobre las 9 de la mañana, comenzaron sobrevuelos de helicopteros militares en medio de las concentraciones de los manifestantes de la Cumbre Agraria en Catatumbo.

**31 de Mayo:** Desde La Lizama, Norte de Santander, se denuncia que desde la Administración Municipal se busca impedir la llegada de ayuda humanitaria de emergencia, que debían ser proveídas a las comunidades asentadas. Allí se requiere con extrema urgencia la provisión de agua potable, pues de no ser así, se podrían generar  epidemias o enfermedades gastrointestinales, afectando la integridad de quienes ejercen su derecho a la movilización. [(Más información)](http://bit.ly/1r1eelw)

**28 de Mayo:** 600 personas de diferentes organizaciones sociales se concentran en el punto conocido como La Lizama, donde acampan desde las 8 de la noche, y donde también las fuerzas militares instalaron un retén.

### Antioquia 

**10 de Junio:** En Medellín, manifestantes bloquean la entrada de la Gobernación de Antioquia en solidaridad con  la Minga Nacional.

**9 de Junio: **Cerca de 150 personas se concentran frente a la Alcaldía de Rionegro, uno de los epicentros del poder económico en Antioquia donde también hace presencia el Ejército Nacional para atemorizar a los manifestantes.

**8 de Junio:** Sectores adscritos a la Cumbre Agraria rechaza** **la detención de Juan Diego Muñoz y Jorge Vélez, luego de extender un cartel en apoyo a la Minga Nacional  durante el partido de Nacional contra Rionegro. Además se hace un llamado, para que se reivindique del fútbol como una plataforma de manifestación social y popular.

Además, 35.000 transportadores se unieron al paro nacional. Anunciaron que se concentrarán en cuatro municipios del departamento.

La Minga Nacional se tomó las Avenidas del centro de la ciudad de Medellín entre las 4:30 de la tarde hasta las 7:30 de la noche, con participación de diferentes organizaciones sociales.

**7 de Junio:** En Puerto Valdivia se realiza el Foro popular sobre el agua en la plaza pública con delegados internacionales del MAB Brasil y MAPDER México, donde asisten estudiantes, comerciantes y las iglesias**.**

**5 de Junio:**  Represión de la fuerza pública contra indígenas indígenas  Emberá y Zenú en el municipio de Apartadó deja  24 indígenas heridos, 4 mujeres y 19 hombres; 10 de estas personas fueron remitidos al hospital Antonio Roldan Betancourt de Apartadó por la gravedad de las lesiones que se le ocasionaron.  Se registra un centenar de afectados por efectos de gases lacrimógenos.

**4 de Junio**: Denuncian que en Medellín fueron detenidos dos personas cuando se disponían a poner un cartel de apoyo al paro en el partido nacional Rionegro.

También Medellín se realizó una jornada cultural y taller de MSP en Caldas Antioquia con la comunidad.

**3 de Junio: **En vista de la desatención del gobierno nacional, departamental y local a la exigencia de las comunidades campesinas de que se suspendan los desalojos forzosos por cuenta del proyecto Hidroituango, desde las 7 de la mañana de este viernes más de 500 pobladores se tomaron pacíficamente la alcaldía del municipio de Valdivia. Hacia las 4:30 ingresa violentamente el ESMAD, y desaloja por la fuerza a los manifestantes, resultando herido un menor de edad.

Hacia las 5:30 de la tarde un grupo de unos 2500 indígenas bloquearon la vía Apartadó - Chigorodó.

**2 de Junio:** Cerca de 2500 indígenas se tomaron pacíficamente la Alcaldía de Apartadó, debido a reiterativos incumplimientos.

**1 de Junio:** Más de mil 200 indígenas de Antioquia se suman a la Minga Nacional. Desde la mañana la Organización Indígena de Antioquia, OIA, se moviliza sobre la vía principal del Urabá Antioqueño. En la tarde las autoridades de la OIA en Asamblea y rituales de armonización ultimaban detalles de logística para la movilización. Reportan presencia de la fuerza pública, quienes estaban realizando sobre vuelos y tomando fotografías, sin embargo hasta el momento todo transcurría en normalidad.

Por otra parte, se denunció que el grupo paramilitar conocido como el Clan Úsuga, amenaza a los campesinos movilizados en Puerto Valdivia. el Movimiento Ríos Vivos denuncia que han recibido llamadas en las que se les exige acabar con la movilización

**31 de Mayo:** Desde tempranas horas de la mañana cerca de 100 personas integrantes del Movimiento Ríos Vivos de Antioquia salieron para encontrarse con otros manifestantes el municipio de Puerto Valdivia, sin embargo, la constante represión y retenes por parte de la Policía Nacional y el Ejército ha impedido que lleguen a su punto de encuentro, como lo denuncia Isabel Cristina Zuleta, vocera del Movimiento. Personas articuladas al Movimiento mantienen un debate Público con el alcalde de Valdivia en la plaza. Más de 400 personas llegaron de los municipios del noroccidente, y el bajo Cauca antioqueño.

**30 de Mayo:  **A la casa de William Gutiérrez, Orlando Gutiérrez (hermanos), Wilson Vargas y Rito Mena, llegaron presuntos paramilitares diciendo que si la movilización no se levantaba mañana, asesinarían a estas personas; todas ellas líderes sociales e integrantes del Movimiento Ríos Vivos Antioquia. Los hermanos Gutiérrez fueron víctimas de desalojo la semana del 23 de mayo, por solicitud de Hidroituango. (Información Ríos vivos). A su vez, en el municipio de Santa Rosa de Osos, Antioquia, la administración municipal el movimiento social denuncia que la Secretaria de gobierno departamental, Victoria Eugenia Ramírez Vélez, está criminalizando a varios campesinos de la región señalándolos como "gente pagada influenciada por el ELN".

### Bogotá 

**11 de Junio:** Desde las 2 de la tarde en el Parque Central de Bosa, jóvenes de Bogotá continúan  apoyando la Minga. Esta vez exigen que se acabe el servicio militar, se movilizan en defensa del territorio, por ciudades dignas y el respeto a indígenas, campesinos y afrodescendientes.

**9 de Junio:** Mientras se realizaba una bicicletada sobre las 7 de la noche en Bogotá, exigiendo la revocatoria de Enrique Peñalosa, y en apoyo a la Minga, el ESMAD arremetió contra quienes se movilizaban pacíficamente con gases lacrimógenos.

Desde el medio día,  en Bogotá se lleva a cabo la estampida por la revocatoria del Alcalde Enrique Peñalosa en medio de una marcha pacífica entre el Parque Nacional y la calle 90.

El Movimiento Ríos** **Vivos invita a conversatorio sobre desestabilización  de América latina, caso  golpe brasil y criminalización en la región en el marco de la Minga Nacional de la Cumbre Agraria.

**8 de Junio:** Desde las 6 de la tarde, se realizó una marcha de antorchas en la Plaza de Bolívar en apoyo a la Minga Nacional.

**7 de Junio:** Cerca de 400 estudiantes  de la Universidad Pedagógica Nacional, realizan una marcha en apoyo al paro agrario, desde las instalaciones de la universidad hasta el Ministerio de Agricultura.

Inicia paro camionero, con 7000 mulas parqueadas en Bogotá zona de Fontibón Nariño y Cauca.

**6 de Junio:** Movilización entre Madrid y Mosquera, en donde participaron delegaciones de ambos municipios más los de Zipaquirá, Facatativá y Funza; se realizó esta actividad en apoyo al Paro agrario, campesino y popular que se desarrolla en todo el país. Habitantes de la sabana de Bogotá manifestaron sus exigencias en torno a movilidad, salud, educación, y contra la militarización de la sociedad. Así mismo marcharon por la memoria del estudiante agredido por el ESMAD, Miguel Ángel Barbosa, y quien falleció el viernes en horas de la madrugada.

**5 de Junio:** Desde tempranas horas de la mañana cientos de personas se encuentra en Asamblea popular en Bogotá-sabana Usme por el cierre del botadero Doña Juana.

**3 de Junio:** Desde tempranas horas de la mañana se registran protestas en la Universidad Pedagógica, a lo que la fuerza pública respondió con represión. La Carrera 15 se encuentra bloqueada.

En respuesta  a la represión del gobierno y la no respuesta a las exigencias, la Minga Nacional convoca a las 6:00 de la tarde en todas las ciudades de Colombia un cacerolazo. En Bogotá la cita es en la Plaza Eduardo Umaña Mendoza, Carrera 7 con calle 20.

**1 de Junio:** Sobre las 10 de la mañana, manifestantes de la Cumbre Agraria decidieron ingresar a las instalaciones de Ecopetrol en Bogotá, con el objetivo de exigir que no se adelanten acciones para implementar el fracking en Colombia y también para que se proteja la naturaleza del modelo extractivista en el que se basa la economía colombiana. Se reporta presencia de agentes de la policía que ya amenazan con desalojar a los manifestantes de manera violenta. [(Más información)](https://archivo.contagioradio.com/minga-nacional-se-toma-instalaciones-de-ecopetrol-en-bogota/)

**31 de Mayo:** Frente al Hospital de Meissen, manifestantes protestaron durante 90 minutos. La Policía los desalojó, entre los cuales 3 jóvenes fueron detenidos y trasladados presuntamente a la URI de Molinos.

**30 de Mayo:** Desde la capital se realiza un monitoreo sobre cómo avanza la movilización a nivel nacional. De acuerdo con Jimmy Moreno, integrante del Congreso de los Pueblos, y de la Cumbre Agraria, desde ayer se vienen alistando las comunidades para la movilización. Esta se ha caracterizado por la fuerte represión por parte de la fuerza pública, particularmente se señala la Costa Atlántica, donde ha habido hostigamientos por parte de la misma.

Frente a esa situación, se han activado todos los mecanismos de protección de derechos humanos, y se trata de dialogar con el gobierno nacional para que se garantice la protesta social. Sin embargo, Moreno denunció que mediáticamente el gobierno sigue asegurado que sí se le ha cumplido a la Cumbre, que hay más de mil horas y 75 reuniones, "pero solo es eso, un diálogo, y lo que estamos exigiendo es que pasemos ese límite, del diálogo a reformas políticas estructurales, donde se tengan en cuenta nuestros reclamos", dice el integrante del Congreso de los Pueblos.

3:45 pm:** **una tanqueta del ESMAD llega a las instalaciones del Concejo de Bogotá para intimidar y expulsar a los manifestantes que se encontraban a las afueras del edificio exigiendo que no se venda la ETB.

<iframe src="http://co.ivoox.com/es/player_ej_11715105_2_1.html?data=kpakk5qVdJahhpywj5WaaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncavdztLmjbLTtsbi0JCajajTssjmxtjcjcnJb83j1JDd18rGsNDnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### [Boyacá]

**6 de Junio: **Los transportadores de carga que hacen parte de la Asociación Colombiana de Camioneros, de Boyacá este lunes festivo sobre la media noche, iniciaron el paro camionero inmovilizando los vehículos.

**3 de Junio: **En las horas de la tarde en Tunja se espera reunión entre el Gobernador de Boyacá, Carlos Amaya y los líderes campesinos para acordar algunos compromisos. Por el momento los manifestantes se encuentran bloqueando la vía Sogamoso-Yopal.

**1 de Junio:** El pueblo U´wa y el pueblo Bari, reunidos iniciaron la recuperación de la Finca Santa Rita. En las horas de la tarde reportaron las autoridades que continúan concentrados en las mediaciones del Pozo Gibraltar, sin reporte de bloqueos en las carreteras.

**31 de Mayo: **Bloqueos intermitentes en el sector de "Huerta Vieja" en la vía Aguazul - Sogamoso. El comercio se encuentra cerrado desde el día 30 de mayo, dificultando el abastecimiento de alimentos y víveres.

### Tolima 

**9 de Junio: **[En un panfleto las Águilas Negras amenazaron a las organizaciones y colectivos que participaron del ‘Carnaval por el agua’ en Ibagué, el pasado viernes. Las amenazas también han circulado vía mensajes de texto y correos electrónicos.]

[Sobre las 4 de la tarde,  aproximadamente 200 estudiantes de la Universidad del Tolima que realizaban un pupitrazo en apoyo a la Minga Nacional y para denunciar la crisis estructural de la institución fueron agredidos por el ESMAD con chorros de agua.]

**3 de Junio:** En Ibagué desde las 2:30 de la tarde, diferentes organizaciones ambientalistas convocaron un Carnaval por el Agua, la octava marcha en esa ciudad en contra del proyecto minero La Colosa de Anglo Gold Ashanti.

**31 de Mayo:** Desde las 5 de la mañana, 2 mil manifestantes se concentran en el municipio de Saldaña. Campesinos, indígenas y trabajadores de Natagaima, Purificación y Guamo se unieron a esta movilización para exigir al Gobierno el cumplimiento de los acuerdos alcanzados durante el paro anterior. La solicitud principal tiene que ver con  la compra de predios para los resguardos indígenas, también se rechaza la minería y se exige la protección por parte del gobierno nacional de los recursos naturales. A su vez se piden garantías para la educación y programas sociales con enfoque diferencial.

En las actividades del día se resalta la realización de dos marchas pacíficas, aunque la Gobernación del Tolima ha dicho que no permitirá el bloqueo de vías.

### Santander 

**8 de Junio:** Desde las 10 de la mañana en la Universidad Industrial de Santander, Casona la Perla, se realiza el Panel 'Transformaciones sociales y Construcción de Paz'. Desde las 5 de la tarde en el Parque Pío organizaciones sociales participan de un cacerolazo.

**7 de Junio:** Desde las 9 de la mañana se realizó una sesión informal en el Concejo de Bucaramanga, donde participan uno de los líderes campesinos de los bloqueos viales. A las 2 de la tarde, se realizó un perifoneo informativo en la sede de la Central Unitaria de Trabajadores de la capital de Santander.

**3 Junio:** Desde las 5:00 de la mañana en de Ranchería se desarrollan protestas sobre la vía Berlín a Pamplona por lo que el transporte se ve afectado.  En horas de la tarde, se denuncian 50 personas detenidas en el punto de Silos,  cerca de Berlín.

En el kilómetro 79 de la vía Bucaramanga - Cúcuta el ESMAD rodeó los campesinos. La fuerza pública utiliza camiones del Ejército y de la Policía para detener la movilización campesina, lo que además dejó un manifestante con las piernas fracturadas.

**31 de Mayo: **En el municipio de Berlín se concentran cerca de 300 personas, así mismo, en el municipio de Pamplona, se movilizan otro grupo de campesinos en medio de algunos actos de la fuerza pública que pretenden intimidar a los manifestantes.

###  

### Huila 

**7 de Junio:** Sobre las 9 de la noche de este martes, la Cumbre Agraria informó que las comunidades del corregimiento de Brusuelas, del municipio de Pitalito, Huila donde se concentran varios manifestantes de la Minga Nacional, fueron atacadas por un escuadrón del ESMAD donde también hacen presencia la Policía Nacional y el Ejército intimidando la movilización.[(Más información)](http://bit.ly/25MsDoQ)

**1 de Junio: **En la mañana se reportaron 35 heridos, los cuales se incrementaron a 87 de acuerdo al reporte oficial de las autoridades del Consejo Regional Indígena del Huila. De los cuales 6 heridos se encuentran internados en la red hospitalaria de los municipios del Hobo y Neiva, debido a trauma cerrado de tórax, pérdida de extremidades superiores y lesiones graves en la movilidad de los afectados, tras el bloque en el Puente El Pescador que de Neiva conduce a Pitalito y por lo cual recibiendo una fuerte represión por parte del ESMAD, Ejército y la Policía.

Mas tarde, sobre las 10 de la noche, una delegación de campesinos se reunión con el Ministro de Agricultura y el Gobernador del departamento en el punto de paro del pescador municipio de Hobo. Se hizo presentación y ratificación del punto de paro de Bruselas -Pitalito así como la delegación que se encuentra en el Hobo hasta tanto el gobierno nacional no reactive la mesa Nacional de concertación. Pese a ello, los indígenas Nasa, Yanacona, Misak del Huila, con refuerzo del pueblo Coreguaje, resistieron al embate desmedido, y solo fue disuelta luego de los acuerdos logrados con la delegación estatal.

**30 de Mayo:** Se presentan 2 puntos de concentración. En el municipio de El Hobo, a la altura del paso Puente el Colegio, indígenas y el Congreso de Los Pueblos se mantienen presentes. Los puntos más álgidos están en el sur del departamento, entre Pitalito y San Agustín, por el cruce a la Inspección de Bruselas, donde 150 efectivos del ESMAD han hecho presencia, y hubo algunos enfrentamientos con personas ajenas a la movilización.

### Caldas 

**8 de Junio:** Indígenas inicia la mesa de diálogo entre la  Gobernación y el pueblo  Embera. El sector de Remolinos no se encuentra bloqueado.

**3 Junio:** Sobre la vía Panamericana Pereira-Medellín. Punto de la Y Belén de Umbría. En el sitio de concentración Remolinos en,  Risaralda, se reportó dos mujeres embrazadas que participaban de la Minga Nacional, en medio de la represión del ESMAD sufrieron un aborto. Así mismo, se denuncia que que hace falta y baños adecuados que la Gobernación de Risaralda nunca suministró como se había comprometido.

**1 de Junio:** Sobre las 4 de la tarde, el ESMAD atacó a 300 indígenas integrantes de la comunidad Guascal de la parcialidad La Trina, municipio de Supia. Tras el ataque se reportan 10 heridos por esquirlas. Así mismo se ocasionaron daños al centro educativo y otras casas,  como lo denunciaron las Autoridades del Consejo Regional Indígena de Caldas – CRIDEC.  En la noche se detectaron sobrevuelos de drones y bloqueos de señal telefónica en el lugar de la concentración.

El pueblo Embera de Risaralda y Quindío, mantienen sus acciones de resistencia en la vía Remolinos vía a Belén de Umbría, con participación de más de 3 mil indígenas.

**31 de Mayo:** Más de 600 integrantes del [Pueblo Embera se movilizan por la vía]{.text_exposed_show} Panamericana en medio de la presencia del Escuadrón Antidisturbios.

Sobre las 3:30 p.m la ONIC denuncia que hubo un ataque indiscriminado con gases lacrimógenos en contra de integrantes del Consejo Regional Indígena de Caldas –CRIDEC- ubicados en el sector Palo Pintada. En esa acción dos personas fueron detenidas: Luisa Fernanda Hidalgo y Javier Antonio Gañan pertenecientes al Resguardo Indígena de San Lorenzo. Según la denuncia, Luisa presenta golpes en su rostro.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
