Title: Colombia "ad portas" de la prohibición de la caza deportiva
Date: 2018-09-19 17:32
Author: AdminContagio
Category: Ambiente, Animales, Nacional
Tags: Caza deportiva, procuraduria, protección animal, Toros
Slug: prohibicion-caza-deportiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/2ad4a0bcaea07d0edbc1f8c388334c23-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pinterest] 

###### [19 Sep 2018] 

Actualmente está cursando una demanda en la Corte Constitucional que buscaría prohibir la caza deportiva, y podría salir favorable en un plazo de uno a dos meses; la acción constitucional pretende cambiar la legislación colombiana actual que permite el asesinato de animales para recrear al ser humano. (Le puede interesar: ["Las exigencias del movimiento ambientalista y animalista a Iván Duque"](https://archivo.contagioradio.com/las-exigencias-del-movimiento-ambientalista-y-animalista-a-la-presidencia-de-ivan-duque/))

La activista por los derechos de los animales y abogada **Laura Santacoloma**, fue quien interpuso la demanda que es una "**acción constitucional dirigida a retirar del ordenamiento jurídico nacional 5 normas:** tres del Código Nacional de Recursos Naturales, que se refieren a los cotos de caza (espacios establecidos para caza deportiva), y dos de la Ley 84 de 1989, mediante la cual se crea un Estatuto Nacional de Protección de los Animales, pero donde se exceptúa a los cotos de caza y la caza deportiva".

Santacoloma recordó que en Colombia hay 5 tipos de caza: la de control de especies, de fomento, comercial, con objeto científico y **la deportiva, cuya única finalidad es recrear al ser humano.** No obstante, esta actividad va en detrimento del desarrollo constitucional que se alimentó recientemente con la sentencia [C-666 DE 2010](http://www.corteconstitucional.gov.co/relatoria/2010/C-666-10.htm), que señaló la restricción que tiene el ser humano para disponer de la vida e integridad de los animales.

### **Debemos avanzar en el reconocimiento de dignidad de los animales** 

La abogada sostuvo que las acciones de constitucionalidad como esta se mueven a buen ritmo, pero recientemente todos los procesos ordinarios se suspendieron por cerca de 8 meses para dar tramite a las normas de la JEP, además, dijo que estaba preocupada por el antecedente de la **Corte que anuló la sentencia C-041 de 2017,** con la que el Tribunal había dado plazo de 2 años al Congreso para legislar sobre las corridas de toros. (Le puede interesar:["Animalistas lamentan retroceso de la C.Constitucional en protección animal"](https://archivo.contagioradio.com/lamentan-retroceso-proteccion-animal/))

A pesar de ello, Santacoloma recordó que la **Ley 1774** sentó un buen precedente al reconocer a "los animales como seres sintientes no son cosas, recibirán especial protección contra el sufrimiento y el dolor, en especial, el causado directa o indirectamente por los humanos"; por lo tanto **los elementos jurídicos y sociales estarían dados para que en 1 o 2 meses el fallo sea favorable en materia de protección animal.**

Por su parte, recientemente la Procuraduría general de la nación afirmó en un comunicado que la caza deportiva es inconstitucional, por lo tanto "es imperioso que se retiren del ordenamiento jurídico las normas demandadas, que permiten la muerte, tortura y mutilación de los animales exclusivamente para recrear al ser humano".

<iframe id="audio_28733344" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28733344_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
