Title: Menos del 50% de quienes pueden votar en Bogotá ejercen este derecho
Date: 2015-10-25 10:09
Category: Nacional, Política
Tags: Bogotá, Clara López, elecciones, Polo Democrático Alternativo
Slug: menos-del-50-de-quienes-pueden-votar-en-bogota-ejercen-este-derecho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Elecciones-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [25 Oct 2015]

De acuerdo con la Registraduría Nacional del Estado Civil la renuncia a ejercer el derecho al voto ha sido una práctica frecuente para la elección de alcalde en Bogotá. Teniendo en cuenta las últimas 2 jornadas electorales de 2007 y 2011 el **nivel de abstención tras disminuir registró un leve aumento de 0.43%**; sin embargo, menos del 50% de la población capitalina habilitada para hacerlo vota.

En las pasadas **elecciones de 2011** de un total de 4.921.266 capitalinos habilitados para votar sufragaron 2.324.885, alcanzando el **47.40% de participación** del potencial electoral, ligeramente menor al logrado en 2007 cuando votaron 2.093.855 habitantes, correspondientes al 47,83% del potencial electoral habilitado para esta jornada.

A **nivel nacional la participación electoral** también es de **menos del 50%**, para la jornada electoral de 2014 votaron 14.3 millones de colombianos de los 32.7 habilitados. Entre las principales casusas se registra el desacuerdo con las hojas de vida o planes de gobierno de los candidatos o bien su desconocimiento, en todo caso es una situación que alarma hasta a la Misión de Veeduría Electoral de la Organización de Estados Americanos MVE/OEA que en 2014 recomendó a las autoridades colombianas **estudiar los altos niveles de abstención en los comicios** y buscar soluciones para superar el fenómeno.
