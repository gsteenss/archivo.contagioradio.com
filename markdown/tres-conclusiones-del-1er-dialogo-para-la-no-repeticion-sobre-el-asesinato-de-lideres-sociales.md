Title: Tres conclusiones del Diálogo para la No Repetición sobre asesinato de líderes sociales
Date: 2019-06-11 16:21
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: comision de la verdad, lideres sociales
Slug: tres-conclusiones-del-1er-dialogo-para-la-no-repeticion-sobre-el-asesinato-de-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Primer-Diálogo-para-la-No-Repetición-2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Leyner.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líderes-Sociales-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líderes-Sociales.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líderes-Sociales-3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líderes-Sociales-2-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Lideresa-Social.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Lideresa.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

Ante más de 50 testigos conformados por figuras públicas, líderes políticos y representantes de organizaciones sociales, la **Comisión de la Verdad** inauguró este 11 de junio, los Diálogos para la No Repetición: ‘Larga vida a los hombres y mujeres líderes sociales, y defensores de derechos humanos’ para responder a líderes, lideresas y al país en general, las razones por las cuales han sido asesinados 4757 defensores de DDHH a lo largo de los últimos 30 años.

**El padre Francisco de Roux, acompañado de la comisionada Patricia Tobón,** invitó a mantener un diálogo libre de preguntas de tipo jurídico o inclinación política y que fuera orientado a cuestionamientos históricos, éticos o culturales que permitieran explicar porqué se ha dado este fenómeno, invitando a los doce panelistas a reflexionar e intentar resolver algunos de los más duros cuestionamientos que ha dejado el paso de la violencia en Colombia

### Ser líder social, la actividad más peligrosa en Colombia 

Las lideresas Marylen Serna, del Cauca, María Doris Rivera, del Meta y los líderes: Andrés Chicá de Córdoba y Leiner Palacios de Chocó fueron los encargados de poner sobre la mesa, una vez más, las razones por las que están siendo asesinados los defensores de derechos en sus territorios.

Las causas, según los testimonios de los y las defensoras, confluyen en la disputa por las tierras y el poder acumulativo, "reclamar los derechos de la naturaleza nos ha puesto en riesgo", afirmó Marylen agregando que **"en lugar de ser vistos como una oportunidad de cambio, el Estado ve a los líderes como una amenaza".**

"Mientras que para los indígenas el territorios es madre, para los no indígenas el territorio es riquezas, es un problema estructural". afirmó el alcalde de Toribio y  **líder indígena, Alcibíades Escué. [('Lea también: 'Quebrantos': un clamor desde el arte por la vida de los líderes sociales)](https://archivo.contagioradio.com/quebrantos-un-clamor-desde-el-arte-por-la-vida-de-los-lideres-sociales/)**

> **Según datos de la Defensoría en 2018 fueron asesinados 178 líderes sociales**

Leyner Palacios agregó que en regiones como el Pacífico donde la población carece de los recursos más básicos y donde los líderes exigen con más fuerza que se respeten sus derechos, es donde esta se "ha vuelto la actividad más peligrosa". **Según datos de la Defensoría en 2018 fueron asesinados 178 líderes sociales, un dato que contrastado con 184 muertes en combate de personas al margen de la ley proporcionados según el Ministerio de Defensa permiten ver que en Colombia ser líder social es tan peligroso como pertenecer a un grupo al margen de la ley.**

\[caption id="attachment\_68817" align="aligncenter" width="1024"\]![Leyner Palacios](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Leyner-1024x576.jpg){.wp-image-68817 .size-large width="1024" height="576"} Foto: Comisión de la Verdad\[/caption\]

Asímismo se identificó la estigmatización por parte de los funcionarios públicos, en particular en poblaciones donde "la justicia no opera" y donde la corrupción, como señala Andrés Chica, se convierte en uno de los más peligrosos factores, "si el líder si no cae en el juego de ser corrupto es asesinado", sentenció.

### **Voluntad política y acciones integrales del Estado son clave** 

Al diálogo para la No Repetición también asistió el procurador Fernando Carrillo quien señaló la necesidad de realizar una protección comunal y no individual y con un enfoque diferencial en las áreas rurales. Agregó que la figura del líder social siempre ha estado ligada con la protesta social, un término que en el país ha tenido "una connotación negativa, estigmatizante que le incomoda al sistema y que sigue vigente", postura que confirmó Leyner al mencionar que "cuando se habla de líder social muchas veces las personas son señaladas erróneamente como subversivas”.

Por su parte desde el Gobierno, el **viceministro de Interior, Ricardo Árias,** señaló que el Gobierno tiene voluntad política para proteger a los líderes en Colombia y realizó un balance de gestión en la cartera, sin embargo, para los líderes sociales, la no asistencia de más representantes del Gobierno es una postura contraria a la voluntad que manifiestan tener, "todos sabemos que hay una resistencia  a lo que tiene que ver con el Acuerdo de Paz, es un diálogo que esperamos tenga una repercusión", señaló la lideresa Marylen Serna.

**Fabio Espitia, fiscal general (e)** insistió en que el indice de esclarecimiento en los casos de homicidio han ascendido de un 14%  a un 28% y resaltó las dificultades que tiene el ente investigador para llegar a algunas regiones, sin embargo concluyeron los líderes que el problema va más allá de eso, y que en algunas poblaciones, el Estado nunca ha llegado. [(Le puede interesar: Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales)](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/)

\

### **No ocupar los territorios dejados por las FARC, un error del Gobierno** 

Otro de los cuestionamientos que se dio durante este díalogo para la No Repetición, fueron las razones por las que el Estado se ausentó de los territorios que ocupaban las FARC y que al entregar las armas fueron ocupados paulatinamente por otros actores armados quienes ahora invaden los territorios que los líderes intentan proteger.

Según el **gobernador de Nariño, Camilo Romero** "se le apostó a la firma pero no a al implementación" y recordó cómo alertó al Gobierno en numerosas ocasiones sobre el potencial peligro que corría el departamento posterior a la firma, “como es posible que un grupo al margen de la ley le puede ofrecer a un joven en Tumaco más de un millón mensual y el Estado no puede ofrecerle educación superior”, inquirió.

La postura fue respaldada por **Alberto Brunori, representante del Alto Comisionada de la ONU** para los Derechos Humanos, quien indicó que los líderes sociales son asesinados "para perpetuar el statu quo. Hay sectores que viven de imponer la inequidad”, al final del encuentro, Brunori destacó que con estos espacios, "se vio la necesidad de acelerar la implementación del Acuerdo y de tener más concertación entre entidades que puedan proveer una política integral de protección.

### Este es tan solo el Primer Diálogo para la No Repetición

De igual forma, el presidente de la Comisión, el padre De Roux. concluye que con este el Primer Diálogo para la No Repetición se profundizará esta conversación para generar nuevas preguntas que serán llevadas al territorio, para continuar debatiendo acerca del asesinato de líderes sociales y de otras problemáticas planteadas para esclarecer la verdad.

**La comisionada Patricia Tobón** explicó que este espacio surgió a raíz de las solicitudes de las comunidades y que  sin importar la orilla política, las problemáticas que afectan a estas poblaciones deben ser resueltas por lo que, lo que fue recogido durante este diálogo "será incluido en el informe final de la Comisión de la Verdad".

**"No queremos perdonarnos ninguna pregunta de fondo, sentimos que el país escapa de preguntas duras y se refugia en soluciones simples"** afirmó el padre quien expresó que siente satisfacción de "que hemos empezado, este es un camino largo pero iremos ajustando cosas, solo la verdad nos llevará a la justicia y la construcción de un país distinto".

<iframe id="audio_37074004" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37074004_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
