Title: 505 hectáreas de cerros orientales en Bogotá estarían en peligro
Date: 2017-08-02 13:35
Category: Ambiente, Voces de la Tierra
Tags: Bogotá, cerros orientales, Enrique Peñalosa, medio ambiente
Slug: bogotanos-van-a-defender-los-cerros-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/cerros-orientales-e1486663422599.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rodolfo Franco] 

###### [02 Ago 2017] 

El jueves 3 de agosto, organizaciones sociales y la ciudadanía de Bogotá, defenderán los cerros orientales de la capital en una audiencia pública en el marco de **las intenciones de Enrique Peñalosa por urbanizar 505 hectáreas** del área que comprenden esta cadena montañosa.

Para María Mercedes Maldonado, veedora de la defensa de los cerros orientales, **“la franja que se pretende urbanizar es importante para los bogotanos** porque es la que está donde se termina la ciudad y comienzan los cerros”. En esta franja no hay construcciones y hay bosques, quebradas, senderos que utilizan los capitalinos y es un pulmón vital para la capital. (Le puede interesar:["Enrique Peñalosa desconoce reservas en los cerros orientales"](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/))

Sin embargo, Maldonado denunció que hay unas **intenciones de grupos constructores por apropiarse de este territorio**. Ella aseguró que “a la administración actual no le interesa el medio ambiente y es muy proclive a favorecer a los constructores”. Igualmente manifestó que la administración busca modificar una resolución del Tribunal Administrativo de Cundinamarca emitida 2015 que defiende los cerros, para poder urbanizar la zona.

**Esto es lo que ha pasado con los cerros orientales**

Maldonado explicó que hay una parte de los cerros orientales que **ya no tiene un régimen de reserva forestal protectora** en la medida que “fue extraído de ese régimen por el Ministerio de Vivienda en 2005”. En esta franja están los barrios populares de origen informal que “el Consejo de Estado ordenó que fueran legalizados y se deben ajustar a unas normas de defensa del medio ambiente”. (Le puede interesar: ["Continúan construcciones en zonas de conservación de cerros orientales"](https://archivo.contagioradio.com/cerro-de-los-alpes-golpe-al-ecosistema-de-cerros-orientales/))

Si bien ya hay una zona construida,  de acuerdo con Maldonado, en 2015 el Tribunal Administrativo de Cundinamarca estableció que las 505 hectáreas contiguas, **“hacen parte de una gran área de aprovechamiento ecológico** con usos recreativos para los ciudadanos, defendiendo así el derecho colectivo al medio ambiente”. Por lo tanto, hay una orden para que no se hagan construcciones en esta franja.

De igual forma, Maldonado manifestó que el magistrado del Tribunal de Cundinamarca había expedido un auto el 8 de agosto de 2016 donde aclaraba lo expuesto por el Consejo de Estado quien había manifestado que **“las licencias de construcción obtenidas antes del 2005 podían ser estudiadas** y evaluadas en la medida que no se conocía de la existencia de la reserva”. (Le puede interesar: ["Construcción de cinco viviendas de lujo tienen en riesgo cerros orientales de Bogotá"](https://archivo.contagioradio.com/construccion-de-cinco-viviendas-de-lujo-tienen-en-riesgo-cerros-orientales-de-bogota/))

Sin embargo, ella indicó que el magistrado que llevaba el caso cambió y **“la administración está aprovechando esta situación para urbanizar los cerros”**. Con todos los enredos jurídicos que ha habido Maldonado afirmó que Peñalosa está intentando construir en una zona que ya fue declarada de aprovechamiento ecológico y los defensores de los cerros le harán un seguimiento al fallo para pedir explicaciones del caso.

<iframe id="audio_20135703" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20135703_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
