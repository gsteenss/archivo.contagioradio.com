Title: Amazon rainforest fires threaten 350 indigenous groups in Brazil and Bolivia
Date: 2019-08-26 10:58
Author: CtgAdm
Category: English
Tags: Amazon rainforest, Bolivia, Brazil, Evo Morales, forest fires, jair bolsonaro
Slug: amazon-rainforest-fires-threaten-350-indigenous-groups-in-brazil-and-bolivia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Indígenas-Amazonía.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Photo: COIAB] 

 

As Amazon rainforest fires continue to rage in Brazil and Bolivia, with more than 500,000 hectares of forest gone so far, indigenous peoples have declared an environmental and humanitarian emergency in Brazil and Bolivia due to the impact of these fires on the ecosystem and nearby communities. Close to 1.5 million indigenous people that live in the Amazon Basin could be affected by the crisis.

During August 20 and 21, 3,752 fires broke out in South America. Fifty-nine percent of these took place in Brazil (25,871), 27.8% in Bolivia (12,144), 4.7% in Paraguay (2,054) and 1.2% in Peru (522). According to preliminary reports, more than 100.000 indigenous people have been affected.

Róbinson López, climate change and biodiversity coordinator at the Coordinator of Indigenous Organizations of the Amazon River Basin (COICA by its Spanish initials) said that aside from the serious environmental effects, these fires also threaten indigenous peoples' traditional and spiritual practices.

> Indigenous peoples have an intrinsic relationship with the lands and without them, we cannot live. We have are our sacred plants there. They have put our oral traditions at risk. We see our lands in an integral way.

López pointed out that one of the most emblematic cases is that of the Chiquitano Forest in Bolivia, where 20 days of continuous fires have destroyed 471,000 hectares of endemic forest. This is an area where 554 species of animals and more than 55 species of plants exist. Meanwhile, the Chiquitano people that inhabit the town of Santa Cruz today need humanitarian aid, clean water and food.

### **The responsibility of the government**

While Bolivian President Evo Morales has been criticized for allowing fires in the Chiquitano dry forest to consume more than 600,000 hectares of forest, López pointed out that the Brazilian President Jair Bolsonaro has assumed a “regressive” stance when it comes to the conservation of the Amazon rainforest. According to the environmentalist, Bolsonaro is taking steps to dismantle environmental protections, “clearing the way for private companies and creating norms that permit the free access of lands and the cultivation of soy and sugar cane.”

For López, these actions are giving way to mining, deforestation, extensive livestock production and the expansion of the agricultural frontier in the Amazon. “It’s not the environmentalists’ fault. It’s the responsibility of governments that don’t understand the importance of the Amazon biome,” said López, adding that foreign governments should use sanctions to pressure governments like that of Bolsonaro.

### **The impacts on the Amazon are serious and the harm irreversible**

If the fire continues, warns López, not only are the 350 indigenous groups that live in the basin at risk, but also 6.7 square kilometers of forest; 44,000 species of plants; 2,200 species of animals; 2,500 species of freshwater fish; 10 % of the planet’s carbon reserves; and 17 to 20% of the planet’s freshwater.

According to information from Brazil’s National Institute for Space Research, the country lost 10,788 square kilometers of forest this year in its Amazonian region — an area equivalent to the size of Jamaica.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
