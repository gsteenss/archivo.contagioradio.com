Title: ¿Cuáles serían las afectaciones del Sendero Las Mariposas?
Date: 2019-02-05 08:00
Author: AdminContagio
Category: Ambiente, DDHH
Tags: administración Enrique Peñalosa, San Cristóbal, Sendero Las Mariposas
Slug: cuales-serian-las-afectaciones-del-sendero-las-mariposas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cerros-orientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bogota.gov.co 

Las comunidades de San Cristobal, una localidad en el suroriente de Bogotá, manifestaron ante la Autoridad Nacional de Licencias Ambientales (ANLA) su desacuerdo con el desarrollo del **Sendero Las Mariposas**, un proyecto del Alcalde Enrique Peñalosa que pretende ser "el principal atractivo turístico de Colombia."

Desde el 2016, cuando la Alcaldía primero anunció el proyecto, el Sendero Las Mariposas ha generado controversia. Por un lado, la administración afirma que el camino ecológico, un sendero entre una zona de conectividad con el páramo y subpáramo, no solo permitiría a turistas y bogotanos a conocer al patrimonio y ecológico de la capital sino que serviría como un circuito cortafuegos para defender la zona reserva forestal de incendios. (Le puede interesar: "[Enrique Peñalosa desconoce reservas en los Cerros Orientales](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)")

Sin embargo, las comunidades de las zona de influencia del proyecto, que incluye a Usaquén, Chapinero, Sante Fe, Candelaría, San Cristobal y Usme, aseguran que al contrario de conservar los Cerros, un zona de gran importancia por sus nacimientos hídricas y especies endémicas, este sendero ocasionaría graves afectaciones ambientales, como **la tala de bosques, incendios forestales y la urbanización de este bosque protegido**. Además, algunas **comunidades que serían desalojados de sus terrenos de manera forzada**.

De acuerdo con información de la Alcaldía, el Sendero Las Mariposas serían conformado por un camino ecológico de **100.7 kilómetros** que travesaría por la zona reserva forestal y la franja de adecuación de los Cerros Orientales, así como un circuito cortafuegos, miradores, aulas ambientales, un puente peatonal de 300 metros entre Monserrate y Guadalupe y una conexión con los parques planeados en San Rafeal y Tominé.

### **Senderos en los Cerros: ¿Bueno o malo para el ambiente?** 

En la temporada del fenómeno de "El Niño", las precipitaciones de lluvia disminuyen y los incendios incrementan en los Cerros Orientales. Dado el deterioro de los bosques que estos fuegos ocasionan, la Alcaldía propone que el Sendero Las Mariposas también sea empleado como un circuito de caminos cortafuegos, los cuales ayudarían a detener al progreso de un incendio y facilitaría el ingreso de los bomberos.

Sin embargo, ambientalistas sostiene que el proyecto estaría disponiendo los bosques a más riesgos de los cuales ya padecen a raíz de la construcción de 17.5 kilómetros de senderos y el incremento en presencia humana en esta zona protegida. Según Iván Felipe Murcía, integrante de la comunidad de Laureles, esto "aumenta la probabilidad de incendios forestales, el incremento de basura y la deforestación de las plantas nativas que mucha gente hoy en día corta y recoge por diversión."

Según las familias locales de La Cecilia, donde estaría pasando Las Mariposas, en esta localidad ya se ha reportado afectaciones ambientales en los últimos 30 años por el incremento de caminantes, mientras que una de los senderos más importantes de Bogotá, la Quebrada La Vieja la mantuvieron cerrada por más de un año[ por el presunto daño ecológico ocasionado por el número de visitantes que ingresaban, el cual llegó a sobrepasar la capacidad de carga establecida.]

### **¿Qué pasaría con las comunidades de San Cristobal?** 

Unas de las principales preocupaciones de las comunidades de San Cristobal es que la construcción del proyecto llevaría su reubicación. En la audiencia pública convocada por la ANLA, funcionarios del [Consorcio Sendero Las Mariposas, una asociación de tres compañías que realizó el estudio ambiental para la solicitud a la licencia, sostuvieron] que el sendero no creará el desalojo de familias porque estaría pasando en mayor parte por predios de propiedad de la Empresa de Acueducto de Bogotá y la Corporación Autónoma Regional, así como unos lotes vacíos de ciudadanos.

Sin embargo, las comunidades dicen que la información que les han entregado las autoridades sobre las obras y las reubicaciones de los habitantes en las zonas de influencia ha sido poco claro y incompleta, hechos que "genera preguntas, inquietudes y preocupaciones en la gente", manifiesta Murcia.

<iframe id="audio_32337628" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_32337628_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
