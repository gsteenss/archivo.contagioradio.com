Title: Denuncian desaparición de lideresa de la Ruta Pacífica de las Mujeres
Date: 2017-06-15 12:58
Category: Mujer, Nacional
Tags: desaparecida, mujer, Putumayo, Ruta Pacífica de las Mujeres
Slug: denuncian-desaparicion-de-lideresa-perteneciente-a-la-ruta-pacifica-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Adelaida-Davila.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Personal] 

###### [15 Jun. 2017] 

A través de un breve comunicado, la Ruta Pacífica de las Mujeres regional Putumayo manifestó que desde el pasado 12 de junio **la lideresa Adelaida Dávila, integrante de esa organización, se encuentra desaparecida,** luego de ser vista por última vez en el “Terminalito de Acevedo” en la ciudad de Pitalito.

“Con gran tristeza anunciamos que nuestra compañera, hermana y lideresa Adelaida Dávila Estupiñan de la Ruta Pacífica regional Putumayo se encuentra desaparecida” manifiesta la comunicación. Le puede interesar: ['Águilas Negras' amenazan a organizaciones y lideres sociales del Cauca](https://archivo.contagioradio.com/panfletos-amenazantes-aguilas-negras/)

Según asegura la Ruta Pacífica, **en diciembre de 2016 tras recibir amenazas, Adelaida tuvo que desplazarse del Putumayo hacia Popayán – Cauca**, reincorporándose a la Ruta Pacífica regional Cauca.

**Adelaida es una mujer afrodescendiente, víctima de múltiples violencias, madre de una hija y un hijo** y también hace parte de la organización Cimarrón. Desde hace cerca de 10 años es una reconocida lideresa que ha adelantado un trabajo en salud mental comunitario en diferentes regiones del territorio putumayense y además ha trabajado con víctimas del conflicto armado y en salud mental con victimarios. Le puede interesar: [Las mujeres y la defensa de los derechos humanos en Colombia](https://archivo.contagioradio.com/las-mujeres-y-la-defensa-de-los-derechos-humanos-en-colombia/)

**Los últimos registros que se tienen de Adelaida es que viajó el viernes 9 de junio de 2017 desde la ciudad de Popayán - Cauca hacia la Ciudad de Pitalito - Huila** para participar en una Mesa de trabajo que se está adelantando para generar propuestas conjuntas y hacer incidencia desde el suroccidente colombiano a la Mesa de negociaciones con el ELN en Quito - Ecuador.

Estando en Pitalito, **Adelaida estuvo sustentando su proyecto de grado como psicóloga el lunes 12 de junio de 2017.** Según Martha Vinasco Docente de la Universidad Nacional en Pitalito, quien la hospedó en su casa “el martes 13 de junio la llevó en su carro hasta el “Terminalito de Acevedo” en la misma ciudad por petición Adelaida, donde ella tomaría trasporte hacia otro lugar, asumiendo que el recorrido era de regreso hacia Popayán”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
