Title: Protestas contra golpe de Estado en Brasil paralizan vías en 7 estados
Date: 2016-05-10 15:37
Category: El mundo, Movilización
Tags: Brasil, Dilma Rouseff, Juicio Político, MST
Slug: protestas-contra-golpe-de-estado-en-brasil-paralizan-vias-en-7-estados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/protestas-brasil-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brasil de Fato] 

###### [10 May 2016]

Sao Paulo, Rio de Janeiro, Bahia, Paraiba, Piauí, Brasilia y Pernambuco son las ciudades en que se han presentado bloqueos de las principales vías. En la jornada por la defensa de la Democracia impulsada por el **Movimiento de los Trabajadores sin Techo y el Movimiento de los Trabajadores Sin Tierra,** se han congregado miles de personas que se oponen al proceso de juicio político contra la presidenta Dilma Rouseff.

Los movimientos sociales y en general los manifestantes aseguraron que aunque la derecha puede ganar el juicio político que irrespeta el voto y la democracia en Brasil, pero **“no van a tener las condiciones para gobernar”** como lo aseguró Guilherme Boulos, presidente del MTST, “**un recado de que ellos puedan hasta ganar en el 'tapetão' \[juego sucio\] pero no van a tener condiciones de gobernar**” entrevistado por Brasil de Fato.

Las manifestaciones se dan en medio de una **fuerte confusión política provocada por el [cambio de postura por parte de Waldir Maranhao](https://archivo.contagioradio.com/por-falta-de-pruebas-anulan-votacion-de-impeachment-en-camara-brasilena/)**, presidente interino de la Cámara, en reemplazo del destituido Eduardo Cuhna. Maranhao en principio afirmó que el proceso de votación de juicio no tenía validez, pero luego se retractó. Se pudo establecer que Maranhao fue amenazado con la expulsión del Partido Progresista al que pertenece si negaba la validez de la votación.

Por su parte, el presidente del Senado decidió continuar con el proceso que deberá tener una primera votación esta semana. Ese mismo escenario recibió este fin de semana al presidente de la OEA quien afirmó que **[no había ni siquiera indicios del crimen de responsabilidad](https://archivo.contagioradio.com/multitudinarias-movilizaciones-en-brasil-rechazan-intentos-de-golpe-de-estado/)**, y por tanto no habrá certezas, según lo reportó el portal Prensa Latina.

Ante ese escenario también compareció el presidente de la Corte Interamericana de DDHH que afirmó que **[en un juicio no se puede anunciar el voto previamente](https://archivo.contagioradio.com/hasta-la-derecha-de-brasil-considera-ridiculo-pedir-detencion-de-lula/)**, como sucedió en la Cámara de representantes la semana pasada. Según la Corte IDH, en un juicio político los congresistas actúan como jueces y no se garantiza el debido proceso ni el derecho a la defensa cuando ya se ha anunciado el sentido del voto.

Por otro lado, la presidenta [Dilma Rouseff](https://archivo.contagioradio.com/futuro-de-rousseff-se-definira-en-plenaria-del-senado/), hizo un recuento de los decretos con los que se le acusa de acciones similares en gobiernos anteriores y recordó que el gobierno anterior al Partido de los Trabajadores **se aprobaron 30 decretos, en el gobierno de Lula se aprobaron 4 y ella ha aprobado 6** y agregó que en procesos anteriores no se ha dado ninguna acusación, concluye que lo que se está llevando a cabo es un juicio con intenciones políticas porque nadie en Brasil votará por quienes pretenden asumir el gobierno.

Vea el discurso completo de la presidenta...

\[embed\]https://www.youtube.com/watch?v=QDgwBn9t2UA\[/embed\]
