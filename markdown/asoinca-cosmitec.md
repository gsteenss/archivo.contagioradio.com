Title: Docentes de Asoinca se oponen a que Cosmitec se encargue de su salud
Date: 2017-10-27 17:31
Category: Educación, Nacional
Tags: ASOINCA, Cosmitec, fecode
Slug: asoinca-cosmitec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/asoinca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOINCA] 

###### [Oct 27 de 2017] 

Docentes de Asoinca en el Cauca manifestaron estar inconformes con la selección de Cosmitet Ltda, como la prestadora del servicio de salud, debido a las demandas que ya han interpuesto maestros a esta entidad por el “pésimo servicio que presta". De acuerdo con ASOINCA, hasta el momento **hay 12 profesores muertos, en lo corrido del año, por causa de la precaria atención de esta entidad.**

La entidad anteriormente manejaba la licitación para prestar el servicio de salud a los docentes y sus beneficiaros y algunas de las demandas se han interpuesto debido a la falta **de citas médicas o los periodos muy largos de espera para citas con especialistas**, e incluso  hay que esperar hasta dos años por cirugías, afirma Tito Torres de Asoinca.

“Todas las evidencias se encuentran en Procuraduría y Fiscalía, por eso en el Cauca estamos descontentos. Los entes de control no están funcionando, **estas empresas criminales que llevan a los docentes a fallecer, que no cumplen con la prestación de sus servicios**, siguen operando” señala.

De igual forma, los docentes manifestaron que tienen miedo de que sigan existiendo malos manejos desde Cosmitet “los recursos alcanzan para los procesos de salud, **el problema es el manejo que se les da y la corrupción que hay de por medio en estos contratos**” señaló Torres.

Los docentes habían solicitado que se tuviera en cuenta la Red Pública de Salud del Cauca como entidad que podría asumir la prestación de salud de los docentes y sus familias, además, Torres **afirmó que son más de 37 mil personas las que serían atendidas** por esta empresa, sin garantías para un buen servicio médico. (Le puede interesar: ["Red Pública de salud del Cauca esta lista para recibir a maestros: ASOINCA"](https://archivo.contagioradio.com/asionca-vuelve-a-paro-porque-gobierno-sigue-incumpliendo/))

Los maestros están planteando la posibilidad de unirse a las movilizaciones de los campesinos del Cauca para exigir un nuevo proceso de licitación en el que, debido a las demandas, Cosmitet no pueda participar. De igual forma señalaron que están exigiendo a FECODE que cumpla su papel como organización defensora de los derechos de los maestros y que se oponga a esta designación en el Cauca, **incluso en Risalralda donde también hay quejas contra la misma entidad**.

<iframe id="audio_21735495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21735495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
