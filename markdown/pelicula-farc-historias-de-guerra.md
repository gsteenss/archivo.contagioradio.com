Title: Así se ve la primer película realizada por excombatientes de las FARC
Date: 2018-07-11 12:45
Category: Cultura
Tags: Cine, FARC
Slug: pelicula-farc-historias-de-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/37004265_622433101474993_7057272755053395968_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FARC occidente 

###### 11 Jul 2018 

Con una amanecer al fondo y tropas en formación, inicia el trailer de Historias de Guerra, la primera película escrita, producida y protagonizada por excombatientes de las FARC, que han encontrado en la realización audiovisual un ejercicio novedoso de reincorporación a la vida civil.

Ocho profesionales del sector, vienen apoyando y asesorando a la ex guerrilla en un ejercicio que resultó seleccionado para participar del Bogotá Audiovisual Market (BAM), donde esperan conseguir la financiación necesaria para culminar su proceso de post-producción y exhibición en salas colombianas y del mundo.

La producción, que fue rodada en el \#ETCR CarlosPatiño de La Elvira, en el Cauca, en agosto del año pasado, busca según sus realizadores "mostrar la guerra narrada por quienes la sufrieron en carne propia. Un relato de la guerra nunca antes visto", del que vale la pena destacar la participación significativa de mujeres ex-combatientes.

Como parte de la sección screening del BAM, la película tendrá su presentación junto a otras 12 producciones elegidas entre más de 50 postuladas, en dos funciones: el miércoles 11 de julio a las 12 del medio día y el jueves 12 a las 2 p.m. en la sala 2 de Cine Colombia Avenida Chile.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/NC-Noticias-Historias-de-Guerra.mp4\[/KGVID\]
