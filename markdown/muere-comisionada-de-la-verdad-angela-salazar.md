Title: Muere Ángela Salazar integrante de la Comisión de la Verdad
Date: 2020-08-07 08:15
Author: AdminContagio
Category: Actualidad, Nacional
Tags: comision de la verdad
Slug: muere-comisionada-de-la-verdad-angela-salazar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Ángela-Salazar.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información inicial la **Comisionada de la Verdad Ángela Salazar murió en la ciudad de Apartadó la mañana de este 7 de Agosto** después de padecer varios días de Coronavirus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Salazar, lidereza de las comunidades Afro era la encargada de recoger la verdad de las diversas problemáticas en la región de Antioquia y Chocó; según allegados había sido contagiada por el virus del Covid 19, y había sigo hospitalizada hacia una semana pasada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que información reciente revelaba que se encontraba estable, aún se desconoce que pasó en las últimas horas, y que clase de atención había recibido; sin embargo, la lidereza Francia Márquez aseguró que el Gobierno Nacional no está atendiendo la pandemia y *"nos está dejando morir"*

<!-- /wp:paragraph -->

<!-- wp:html -->

> !Cuanto dolor maldita sea!  
> Este Gobierno nos está dejando morir.  
> Nuestra alta comisionada en la [@ComisionVerdadC](https://twitter.com/ComisionVerdadC?ref_src=twsrc%5Etfw) Angela Salazar encarga de la verdad desde los pueblos etnicos, acaba de fallecer por COVID19. [pic.twitter.com/jELHRD51JK](https://t.co/jELHRD51JK)
>
> — Francia Márquez Mina (@FranciaMarquezM) [August 7, 2020](https://twitter.com/FranciaMarquezM/status/1291718507223232518?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

**Salazar había sido una de las primeras comisionadas en conocer la situación de las comunidades del [Cacaria](https://archivo.contagioradio.com/la-salida-mas-facil-es-la-cadena-perpetua-y-no-es-la-correcta/),** en la primera visita que se realizó desde dos de los organismos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comisionada participó con gran entusiasmo en actividades de las comunidades negras del Bajo Atrato Chocoano como el [Festival de las Memorias del Cacarica y las cenas de la Memoria](https://www.justiciaypazcolombia.com/festival-de-las-memorias-somos-genesis/) en la que se dieron encuentros entre víctimas, responsables y terceros para avanzar en el reconocimiento de la verdad y la construcción de la memoria colectiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además estaba a la cabeza de la organización de reconocimiento público planificado por la [Comisión para el Esclarecimiento de la Verdad](https://comisiondelaverdad.co/) para el próximo mes de Agosto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Compartimos con ustedes el último diálogo que tuvimos con la comisionada Ángela Salazar en medio de la pandemia. Ella siempre estuvo dispuesta para compartir espacios con las comunidades negras y con el equipo de Contagio Radio. <https://archivo.contagioradio.com/otra-mirada-la-verdad-del-pueblo-negro/>

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2654846704781926%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
