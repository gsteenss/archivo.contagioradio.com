Title: La Forma del Agua: fantástica y actual
Date: 2018-02-02 11:31
Author: AdminContagio
Category: 24 Cuadros
Tags: Agua, Cine, fantástico
Slug: laformadelagua-pelicula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/La-Forma-Del-Agua-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fox Searchlight Pictures 

###### Por: Jake Sebastián Estrada Charris 

###### 02 Feb 2017 

A pesar de ser un producto que se acerca a la fantasía y a la imaginación, La Forma Del Agua (2017) nos aproxima a varios aspectos que tocan nuestra puerta para que no olvidemos que aún tenemos mucho por mejorar. De esta manera, el tener en los papeles principales a una mujer muda, una mujer negra, un homosexual y una criatura desconocida que conviven diariamente con distintos personas desinteresadas en tratarlos como iguales, permite que problemáticas atemporales como el racismo, la homofobia y la intolerancia se aborden; por eso, la película es coyunturalmente actual.

![La Forma Del Agua 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/La-Forma-Del-Agua-4.jpg){.alignnone .size-full .wp-image-51128 .aligncenter width="600" height="400"}

Otro de los aspectos para destacar, radica en que este filme es también la apuesta por realizar un cine alejado de varios estereotipos hollywoodenses. Porque, seamos honestos: lo más fácil hubiese sido que Guillermo del Toro pensara en crear a una chica dependiente, joven y sexualizada para el rol protagonista (un cliché que no solo abunda en las películas de monstruos); después de todo, se trata de una mujer muda, algo que para muchos es garantía de vulnerabilidad y sumisión. De esa manera, se caería en el lugar común –pero exitoso- dentro del que los personajes femeninos dicen menos y muestran más. Sin embargo, este cineasta no apostó por lo predecible. Se inclinó por el talento de una actriz adecuada y la visión que él mismo tenía. Como resultado, tenemos el placer de ver a la encantadora Sally Hawkins empoderando un rol al que le saca el máximo provecho.

Esto nos lleva a una de las partes más destacables del filme: la construcción de personajes es realmente maravillosa. Tal aspecto tan bien logrado se nota en la calidad de los personajes que encarnan los actores Richard Jenkins y Octavia Spencer para enriquecer la historia. El mismo antagonista (interpretado por Michael Shannon) en ningún momento decae; es más, se va convirtiendo en un sujeto que no teme mostrar su verdadera naturaleza. También se debe mencionar que aspectos como el diseño de producción y la concepción de arte en la película son un deleite visual que otorga una atmósfera realmente bella.

![La Forma Del Agua 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/La-Forma-Del-Agua-2.jpg){.alignnone .size-full .wp-image-51129 .aligncenter width="600" height="400"}

La Forma Del Agua toma elementos de películas como El Monstruo De La Laguna Azul (1954) para adaptarlos dentro de la historia que se quiere contar: la de una mujer que construye poderosos lazos emocionales con una extraña criatura encontrada en las profundidades del Amazonas que fue puesta en cautiverio por los humanos que se le toparon. Una gran variedad de películas han abordado esta temática. Podríamos nombrar a King Kong o inclusive, El Hombre Manos de Tijera para citar algunos ejemplos.

Por tal razón, hay que decir que este largometraje, ambientado en 1962 tiene tiempo para mostrar filmes de la época y hacerle homenaje no solo a ellos, sino que tal vez involuntariamente adiciona elementos literarios (Frankenstein) y hasta musicales dados a conocer anteriormente (una de las melodías principales compuestas por el maestro Alexandre Desplat es muy similar al tema principal de la película Up). Claro está, no hay que dejar a un lado las referencias al universo de películas del mismo Guillermo del Toro (El Laberinto del Fauno).

<iframe src="https://www.youtube.com/embed/FMNTFFhR__g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Ir al cine a observar esta historia es una buena experiencia y un placer que vale la pena darse porque el arte que se trabaja y hace con amor, se disfruta muchísimo mejor. (Le puede interesar: [Mother una obra maestra en el cine del nuevo siglo](https://archivo.contagioradio.com/mother-pelicula-aronofsky/))

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
