Title: Juicio a empresa Rediba podría salvar la Ciénaga de San Silvestre
Date: 2017-09-21 15:11
Category: Ambiente, Nacional
Tags: Barrancabermeja, CAR, Ciénaga de San silvestre
Slug: empresa_rediba_juicio_cienaga_san_silvestre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/relleno_barrancabermeja-e1486502799163.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Corporación Yariguíes 

###### [21 Sep 2017] 

[Después de meses más 2 años buscando respuestas por parte de la justicia y las instituciones ambientales, los habitantes y colectivos ambientalistas de Barrancabermeja han logrado que la **Fiscalía impute cargos contra la gerente relleno Yerbabuena, que fue construido en zona especial de protección ambiental** en la Ciénaga de San Silvestre, y que ha generado graves impactos ambientales y sociales en la zona.]

[Se trata de Liliana Forero, representante de la empresa Rediba, propietaria del basurero. La gerente deberá responder por los delitos de fraude procesal, contaminación, ocultamiento y destrucción de material probatorio, e invasión a propiedad privada. Por esos cargos, el **25 y 26 de septiembre la justicia deberá dictar la sentencia contra la gerente**, pero también contra el contratista de la misma empresa Reinaldo Bohorquez.]

[Óscar Sampayo, impulsor del Centro de Estudios Sociales, Extractivos y Ambientales del Magdalena Medio-GEAM e integrante del Comité por la Defensa del Agua de Barrancabermeja, cuenta que dicha imputación es el resultado de un trabajo de la ciudadanía y organizaciones ambientales como la Corporación Yariguíes, quienes con sus propios recursos empezaron una recolección de pruebas contratando drones, geólogos y otra serie de expertos y material que evidenciaran la contaminación en la Ciénaga.]

### **¿Por qué se dañó la Ciénaga de San Silvestre?** 

[Lo que en un principio iba a ser la construcción de un camino veredal de uso de suelo agropecuario en la vereda El Tapazón, y la supuesta puesta en marcha de un sistema de reciclaje en la vereda de Patio Bonito, hoy son basureros.]

[De acuerdo la Corporación Yariguíes, la Corporación Autónoma Regional de Santander, CAS, otorgó dos licencias para dos rellenos sanitarios en 2013 y 2014, a pesar de las protestas de la población, teniendo en cuenta que la zona donde se ubican los rellenos es área de protección ambiental, **exactamente 70 mil hectáreas de la Ciénaga de San Silvestre, una zona que está inscrita en Parques Naturales y en el Registro Único de Áreas Protegidas.**]

### **Los daños ambientales** 

[Tras la decisión de la Fiscalía, la comunidad espera que no solo se condene a la representante de Rediba, sino que se empiece inmediatamente un plan de restauración ambiental de la Ciénaga de San Silvestre, ya que, pese a la imputación de cargos, en la actualidad el relleno sigue funcionando y** llegan cerca de 160 toneladas diarias de derechos.** Una situación profundiza cada vez más la grave crisis en este complejo de humedales.]

[Según explica Sampayo, esa basura que llega diariamente al relleno produce alrededor de 72 mil litros de lixiviados cuyo paradero se desconoce, pues no existe una planta de trate esos líquidos resultantes de los desechos que llegan al relleno, pues la CAR nunca lo ha exigido. ]

[Ese panorama ha generado diversas consecuencias en materia de sanidad a los más de **300 mil habitantes de Barrancabermeja quienes tienen sus fuentes hídricas contaminadas**, pues el basurero se construyó sobre una fuente de agua. Directamente son 42 familias las más afectadas, y un colegio. El experto señala que, pese a que legalmente está establecido que un basurero no puede ser construido a menos de mil metros, La Escuela de Patio Bonito se encuentra a 20 metros, y la vereda  está a menos de 30 metros.]

**Mortandad de peces, cinco manatíes muertos, tortugas, y enfermedades en niñas y niños** son solo algunas de las consecuencias que ha dejado la corrupción y la falta de atención estatal sobre la Ciénaga de San Silvestre. Además es de resaltar que la zona donde está construido el relleno hace parte del corredor por el cual sigue su camino hacia Suramérica el jaguar que se encuentra en peligro de extinción. (Le puede interesar: [Agoniza Ciénaga de San Silvestre](https://archivo.contagioradio.com/35957/))

[En medio de este panorama, la comunidad ve como un avance que se juzgue a la gerente de la empresa, pero también esperan que se investigue y judicialice a los funcionarios de la alcaldía y a las autoridades ambientales por permitir que operara ese relleno que hoy tiene agonizando a la ciénaga.  ]

<iframe id="audio_21021738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21021738_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
