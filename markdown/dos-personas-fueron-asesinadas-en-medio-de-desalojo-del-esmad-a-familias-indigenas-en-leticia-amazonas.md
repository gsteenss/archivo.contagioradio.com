Title: Dos personas fueron asesinadas en medio de desalojo del ESMAD a familias indígenas en Leticia, Amazonas
Date: 2020-10-28 15:52
Author: PracticasCR
Category: Actualidad, Comunidad
Tags: amazonas, ESMAD, indígenas, Leticia, violencia policial
Slug: dos-personas-fueron-asesinadas-en-medio-de-desalojo-del-esmad-a-familias-indigenas-en-leticia-amazonas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Desalojo-Leticia.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Familias-indigenas-son-desalojadas-en-Leticia-Amazonas.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

La Organización Nacional de los Pueblos Indígenas de la Amazonía Colombiana -OPIAC- [confirmó](http://opiac.org.co/wp-content/uploads/2020/10/COMUNIDADO-LETICIA-.pdf) la **muerte de una mujer adulta mayor y un bebé en medio del operativo que desarrolló el Escuadrón Móvil Antidisturbios -ESMAD- de la Policía para desalojar a comunidades indígenas de un predio en Leticia, Amazonas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde tempranas horas de este miércoles, la Organización Nacional Indígena de Colombia -[ONIC](https://twitter.com/ONIC_Colombia)- había denunciado que **625 familias pertenecientes a 12 pueblos indígenas en Leticia, Amazonas; fueron desalojadas por la fuerza **por parte del ESMAD**, de un predio en el que se encontraban asentadas.** (Le puede interesar: [Antes del 9 de septiembre se presentaron por lo menos 1.708 denuncias de abuso policial](https://archivo.contagioradio.com/antes-del-9-de-septiembre-se-presentaron-por-lo-menos-1-708-denuncias-de-abuso-policial/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1321453743406276608","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1321453743406276608

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

El senador Feliciano Valencia, señaló que las familias tuvieron que asentarse en el predio en Leticia como consecuencia de las precarias condiciones en las que se encuentran, producto de la emergencia generada por la Covid-19; sin embargo, **William Capiul, autoridad tradicional del Pueblo Yucuna, agregó que ese territorio pertenece a las comunidades ancestrales quienes lo han ocupado de manera milenaria y que fueron instituciones estatales las que arrebataron dichas tierras a los indígenas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El desalojo del predio se realizó desde las 4 de la mañana, con el uso de la fuerza del ESMAD ante una comunidad indígena indefensa; pese a las alertas previas que la propia comunidad elevó frente a entes oficiales de control y defensa de los Derechos Humanos como la Personería y la Defensoría del Pueblo. (Le puede interesar: [Comunidades indígenas denuncian que menor fue víctima de abuso de la Policía en Jambaló, Cauca](https://archivo.contagioradio.com/indigenas-denuncian-menor-abuso-policia-jambalo-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El escuadrón de Policía habría llegado proveniente de la ciudad de Bogotá, y desalojó a los pobladores indígenas que se encontraban en el asentamiento, generando tensiones que luego se convirtieron en enfrentamientos.

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Desalojo-Leticia.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

La ONIC había reportado que en medio de esta acción de la Fuerza Pública se registraron varios heridos y se tenía indicio de que habría 3 indígenas muertos, información que estaba a la espera de confirmación; no obstante hasta el momento se ha podido establecer que **son dos las víctimas fatales y al menos 20 heridas de gravedad.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1321452086807023621","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1321452086807023621

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Organización Nacional Indígena, rechazaron el accionar por parte del ESMAD y el silencio de los entes de control ante las solicitudes señalando que **“*realizamos la alerta por este medio, solicitamos información al respecto sobre el tema y no recibimos respuesta alguna, hoy es muy lamentable tener que estar pendiente y contar heridos en la comunidad, más aún cuando era un tema que se pudo haber evitado*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/instagram {"url":"https://www.instagram.com/tv/CG4vPE_gIaz/?igshid=1lqklkcbf2k7o","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-instagram wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/tv/CG4vPE\_gIaz/?igshid=1lqklkcbf2k7o

</div>

<figcaption>
Prensa UPC

</figcaption>
</figure>
<!-- /wp:core-embed/instagram -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
