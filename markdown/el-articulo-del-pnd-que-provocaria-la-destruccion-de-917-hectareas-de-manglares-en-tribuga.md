Title: El artículo del PND que provocaría la destrucción de 917 hectáreas de manglares en Tribugá
Date: 2019-05-02 15:41
Author: CtgAdm
Category: Ambiente, Política
Tags: Chocó, Plan Nacional de Desarrollo, Puerto de Tribugá
Slug: el-articulo-del-pnd-que-provocaria-la-destruccion-de-917-hectareas-de-manglares-en-tribuga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-84.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

[La Cámara de Representantes aprobó ayer un artículo del Plan Nacional de Desarrollo (PND), presentado por el Presidente Iván Duque, que daría paso a la construcción del Puerto de Tribugá, en Nuquí (Chocó), lo cual ha generado preocupación entre las comunidades locales, que dicen que este megaproyecto provocaría graves afectaciones para el ambiente. ]

La construcción de este proyecto, que se anuncia desde finales de los años 90, recibió un nuevo impulso recientemente, gracias a la inclusión del **artículo 78** en el PND, el cual da prioridad al **desarrollo de infraestructura portuaria y de transporte que conecte los accesos marítimos al resto del país**.

Como lo anunció anoche la Representante Catalina Ortiz, la sanción de este artículo por la Cámara en segundo debate significa un paso adelante para la edificación de esta obra así como la conexión vial Las Ánimas - Nuquí.

Según [Luis Alberto Angulo, coordinador de comunicaciones del Consejo Comunitario Los Riscales,]esta decisión ya tiene a las comunidades preocupadas pues los planes de construir este puerto se pueden hacer realidad, a pesar de que el artículo en cuestión tendrá que ser revisado y aprobado por el Senado. (Le puede interesar: "[El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)")

Angulo resalta que este 30 de abril también se realizó una socialización del proyecto por parte de **la Sociedad Portuaria Arquímedes**, encargada de su construcción. En esta reunión, Arquímedes, conformada en parte por las gobernaciones de Chocó, Caldas y Risaralda, plantearon que este puerto sería más grande que el de Buenventura y uno de los más grandes de Ámerica Latina además de traer desarrollo para esta región del país.

Sin embargo, organizaciones comunitarias, ambientales y culturales se han pronunciado en contra de la construcción dado que pasaría por encima de la voluntad de los habitantes de Tribugá, quienes establecieron allá un Distrito Regional de Manejo Integral que conserva la diversidad biológica de la zona y permite zonas de economías sostenibles como el ecoturismo y la pesca artesanal.

Además, sostienen que la obra podría desaparecer **917 hectáreas de manglares**. También, estarían amenazados especies de tortugas, pescados, ballenas y otra fauna. "Es un ecocidio para toda nuestra flora y fauna en el territorio", señala Angulo.

Frente estos hechos, el coordinador del Consejo Los Riscales dijo que "haremos resistencia porque la visión del puerto no va a favor de nuestra idiosincrasia, no hace parte de resolver los problemas nativos". El Senado tiene hasta **el 7 de mayo** para aprobar el PND.

<iframe id="audio_35293527" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35293527_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
