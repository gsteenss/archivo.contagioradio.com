Title: Seis jóvenes fueron torturados y asesinados en El Tambo, Cauca
Date: 2020-08-22 19:58
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Asesinato de jóvenes, Cauca, El Tambo
Slug: seis-jovenes-fueron-torturados-y-asesinados-en-el-tambo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-22-at-3.59.31-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

[Foto: Contagio Radio]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La noche de este 21 de agosto fue confirmada una nueva masacre en el sector de Uribe, límites con Ortega Llanos a cuarenta minutos del casco urbano de El Tambo, Cauca donde seis personas fueron retenidas por un grupo armado ilegal, y luego halladas sin vida, según información del alcalde del municipio, Carlos Vela. [(Lea también: Sigue en aumento el número de líderes sociales asesinados en Cauca)](https://archivo.contagioradio.com/sigue-en-aumento-el-numero-de-lideres-sociales-asesinados-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Medios locales señalan que la víctimas, identificadas como **Cristian David Millán; Jaime Muñoz Campo; los hermanos Hiner y Esneider Collazos Huila; Yulber Edilson Flor Muñoz y Nicolás Orlando Hurtado,** hallados en zona rural de El Tambo con las manos amarradas, heridas en los dedos y cortes en su garganta habrían sido citados por el grupo armado a una reunión. A través de un Consejo de Seguridad en la zona se estableció que los hermanos Collazos eran a su vez, víctimas de desplazamiento forzado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aún no ha se ha podido establecer quiénes fueron los responsables del hecho, sin embargo información preliminar de personas de la zona, apunta a la Segunda Marquetalia, disidencia de las Farc que arribó a la zona hace algunas semanas. [(Lea también: "En Cauca las comunidades somos el peón de los grupos armados")](https://archivo.contagioradio.com/en-cauca-las-comunidades-somos-el-peon-de-los-grupos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"has-text-align-justify"} -->

Con anterioridad, los habitantes de municipios como E**l Tambo, Argelia y López de Micay,** han atribuido los asesinatos a los grupos armados presentes en el territorio, reconocidos por las comunidades como los frentes Carlos Patiño y Jaime Martínez, parte de la disidencia de las FARC, y quienes mantienen un enfrentamiento constante con el frente José Maria Becerra del ELN por el manejo de las rutas del narcotráfico en Argelia, Patía y El Tambo. [](https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/)[(Le recomendamos leer: Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto)](https://archivo.contagioradio.com/comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a su gran militarización y la presencia de 10.500 hombres de la Fuerza Pública, Cauca es uno de los departamentos donde se ha intensificado la violencia en medio del confinamiento, **Según [organizaciones sociales](http://www.indepaz.org.co/), para el mes de junio 63 defensores de DD.HH. habían sido silenciados mientras durante el mes de abril ocurrieron dos masacres en menos de un mes en los municipios de Micay y Piendamo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las situaciones de confinamiento, reclutamiento de menores y agresiones, han llevado **a las comunidades ha expresar una vez más que no se identifican con ningún bando bélico ni acción militar**. [(Le puede interesar: Población de Argelia Cauca, en medio del horror de la guerra)](https://archivo.contagioradio.com/poblacion-de-argelia-cauca-en-medio-del-horror-de-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta es una de las tres masacres ocurridas en el país en menos de 24 horas, en horas de la tarde del viernes se conoció del asesinato de cinco personas habitantes del corregimiento El Caracol en zona rural de Arauca, mientras este sábado también se confirmó el asesinato de otras seis personas en Tumaco, Nariño. [(Lea también: Tres masacres en una semana, seis jóvenes fueron asesinados en Tumaco, Nariño)](https://archivo.contagioradio.com/tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
