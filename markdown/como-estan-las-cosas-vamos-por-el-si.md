Title: Como están las cosas, vamos por el sí
Date: 2016-05-20 08:23
Category: Cesar, Opinion
Tags: FARC, La Habana, proceso de paz
Slug: como-estan-las-cosas-vamos-por-el-si
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/proceso-de-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: farc-epaz 

#### Por [César Torres Del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

###### 20 May 2016 

[Y bueno, podemos decir que nos acercamos al momento final del proceso de negociaciones. Las tendencias políticas indican que los colombianos participaremos del mecanismo de refrendación, en el entendido, claro, de que el “Acuerdo Especial” que se discute en el Congreso sea aprobado y, además, declarado exequible por la Corte Constitucional. El  ]*[coup d’Etat]*[  es pura invención maniobrera.]

[Elevar lo pactado entre las FARC y el Estado a la categoría de “Acuerdo Especial”  ha sido buena medida. Establecida en el artículo 3 común de los Convenios de Ginebra (1949), hace parte del Derecho Internacional Humanitario y es adecuada en cuanto a los propósitos de la justicia transicional. Sin esta última las Víctimas y las Memorias no habrán aparecido en el escenario socio-político y económico.]

[He defendido el]*[conjunto del proceso]*[ y sostengo que cualquiera que sea el mecanismo de refrendación los colombianos lo deberemos votar afirmativamente.]**Eso sí, con independencia política frente a las FARC y frente al Estado**[. Incluso, los menores de edad a partir de los 14 años deberían ser convocados, no simbólicamente, sino con]***“fuerza de ley”***[. Est@s niñ@s fueron obligados a sufrir el horror de la guerra social; han sido sus víctimas  física, sexual, material y mentalmente; perdieron su infancia. Tienen entonces todo el derecho a votar sí a los acuerdos. Ello nos impone la reflexión, entonces, sobre la reducción de años para la “mayoría de edad”, la cual bien podría estar entre los 14 y los 16.]

[Dicho lo anterior refirámonos brevemente a lo que serán las FARC en el futuro inmediato. Afirmo tajantemente que ellas no representan el universo de los subalternos, del proletariado, del pueblo, del vencido. Como organización, movimiento y/o partido las FARC continuarán con su proyecto  estalinista pero en versión socialdemócrata; reformismo puro, así tal cual. “Comunismo Ilustrado” es su cuño: de etapa en etapa hasta el fin de la Historia; el típico modelo utopista. Defensoras de la razón moderna siguen fielmente su línea temporal: la de “progreso”; como decía la Escuela de Francfort: “Salvemos la Razón por medio de la Razón”.]

[Sostendrán un modelo económico neoliberal pero con tinte social; en virtud de ello apoyarán el emprendimiento modernizador de las multinacionales y el papel virtuoso del capital nacional. Propondrán reformas profundas en cuanto a seguridad social, jubilación, medicamentos genéricos y, tal vez, educación para lo cual necesitarán de su clientela electoral en las zonas que controlan. En cuanto a la gestión ambiental podrían seguir los pasos del modelo adelantado por Correa en Ecuador, Morales en Bolivia y Lula-Rousseff en Brasil: destrucción del ambiente para el desarrollo … En unos dos lustros, después de haber pasado por algún ministerio (al estilo de Clara López, Angelino Garzón y Lucho Garzón), aspirarán seriamente a la Presidencia de la República teniendo como modelo político-social a sus antecesores petistas del Brasil y a Chávez-Maduro.]

[En torno al Leviatán catapultarán la tríada de la senadora “verde” Claudia López:]*[Ciudadanía, mercado y Estado Fuerte]*[.  Todo lo anterior implicará hacer política “moderna”, es decir, alianzas por arriba con los de arriba en nombre de los de abajo (el PSUV venezolano y el PT institucional de Brasil); no habrá lugar para la independencia política frente a los vencedores ni espacio para la movilización social anti-sistema. Acojámonos, dirán, al “principio de responsabilidad” (Hans Jonas).]

[Las FARC, como organización política, no tienen hoy nada sustancial que ofrecer aunque hay que reconocer que agitarán los medios académicos y políticos, nacionales e internacionales. Su novedad consistirá en presentar lo viejo como lo moderno realmente existente y como lo políticamente correcto. Con base en su práctica político-militar de décadas, sus propuestas globales se ubicarán en el conservadurismo; su multiculturalismo seguirá siendo liberal.]

[No hay superación cualitativa - política, ideológica - en la visión fariana sobre la sociedad de hoy.  Un paso adelante y dos pasos atrás.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
