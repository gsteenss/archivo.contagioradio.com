Title: La protesta sumisa   
Date: 2018-10-25 09:55
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: marcha de estudiantes, marcha pacifica, Movilización, protesta, RCN
Slug: la-protesta-sumisa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### @Freakmoka / Twitter. 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 25 Oct 2018 

Lo que ocurre en el continente es tan grave, tan peligroso, que aún no se ha tomado consciencia a gran escala, de un método funcional para combatir la emergencia de fenómenos tales como Trump en los Estados Unidos, Hernández en Honduras, Peñalosa en Bogotá, Duque en Colombia, Bolsonaro en Brasil, Keiko en Perú, Macri en Argentina y la lista sigue… Esa gama de gerentes derechistas retrógrados que se metieron al poder público para absorber mediante el embrujo contratista de su clientela todo el dinero público, en medio de la verborrea más hipócrita contra lo público. Tamaño absurdo que incluso fomentan muchas universidades y medios de comunicación ya no sorprende.  

[Como ya nada sorprende y parece no cambiar, muchos sueltan las ganas de lucha y se suicidan en la corriente que consideraron imparable. ¿Qué está pasando? ¿Por qué tanto inconformismo en Colombia, pero tanta solidez del poder que domina?]

[Es complejo, pero últimamente la mayoría de las protestas nunca han sido una lucha. Han sido protestas emotivas, para la foto, videos bien hechos, pero para la tesis, para llegar a un público joven, para facebook. Hoy la derecha retrógrada tomó terreno porque lograron el control moral de lo político, lograron ofender a los manifestantes más por un grafiti que por las razones de una movilización; lograron preocupar más a los manifestantes por cómo se ve la protesta, que por las razones de la protesta; lograron prevenir incluso a los manifestantes, de no causar tan siquiera un trancón porque “]*[qué vergüenza con el orden que deseamos tumbar]*[” … Nos gobernaron las ganas de cambio, y no fue a punta de palo, fue a punta de moral reciclada por la cultura y vomitada en protestas sumisas, muy útiles a los poderosos.]

[Claro, lo simbólico es importante, es un campo en disputa con los poderosos, pero el problema aquí es que la domesticación de la protesta no es tanto una responsabilidad de quienes están en el poder, como si de otros ¿quiénes? ¿quiénes tienen la culpa de la protesta sumisa?  ]

[Es culpa de quienes a todo nivel (político, simbólico y cultural) están abanderando la supuesta lucha social en la actualidad, o más exactamente el inconformismo. Hoy lo más “revolucionario” que existe en Colombia es Daniel Samper, los chicos de La Pulla, y ahora hasta la sobrina de Juan Manuel Santos con su +Vale. Hoy lo más “revolucionario” en el parlamento es un Robledo que luego de cacarear contra el sistema, almuerza delicioso con Obdulio Gaviria. Hoy lo más “revolucionario” que existe es un profesor con 4,5 millones de votos que prefirió callar y disolver, antes que luchar, introyectando sin darse cuenta la parálisis de la polarización, una vaina tan necesaria para los cambios sociales. Hoy incluso el ché colombiano con 8 millones de votos, más ché que el propio ché, avala que el M-19 dejó las armas, pero la cargó contra las FARC-EP cuando se volvió partido, para no perder los voticos de los moralistas que aplaudían a la extinta guerrilla del M-19, pero aborrecían a las Farc-ep.]

[¿Y todos ellos qué tienen en común? Que su grupo social, no está en una disputa vital y total contra quienes están en el poder, sino que la disputa es solo administrativa, no son opuestos, solo tienen puntos en desacuerdo, puntos de vista que no distan mucho, que son reconciliables en últimas. Lo que hace que todos y cada uno, en su forma de hablar, de caminar, de pensar, de planear, de leer lo político, sean la cosa más distante que pueda existir de lo popular, de la gente que sufre, de la gente que lucha, de la gente que conoce la realidad social del país.]

[¿En realidad los intereses de esta gente están asociados con los de campesinos y líderes sociales que están luchando por su existencia? ¿tendrán algo que ver sus intereses con aquellos a quienes les toca hacer velorios sin el cubrimiento de esas entrevistas de una hora que hace Arizmendi a cualquier opositor venezolano? ¿en realidad están asociados con los intereses de estudiantes que no tienen un peso ni para ser deudores y poder pagar una carrera universitaria? ¿están estos intereses asociados con los sindicalistas agrarios, con los indígenas, con los campesinos, con los comunistas y socialistas que han sufrido persecución, desprestigio, negación y estigmatización a tal nivel que muchos despistados pensarán que ni existen? … lo dudo mucho.   ]

[Pareciera que últimamente a muchos no les importa tanto la lucha, sino que les importan dos cosas: 1. Cómo se ve su puesta en escena. 2. Qué dicen de la protesta. En el fondo buscan quedar bien ante los medios de comunicación, que en principio significa quedar bien ante los mismos contra los que supuestamente se luchan…. ¿están luchando o simplemente siguiendo las reglas sobre cómo se debe protestar?]

[La protesta sumisa es esa que olvida su propósito. Olvida que estamos protestando contra su orden, contra su moral, contra su forma de ver la realidad del país, contra su manera de organizar lo político, contra su forma de organizar lo económico, contra su forma de educarnos para la defensa del sistema, contra su forma de percibir lo bueno y lo malo, contra su manera de cómo se debería desarrollar una protesta… ¿qué ha pasado? ¿Tendrá que ver con el hecho de que algunas universidades públicas están cada vez más llenas de pequeñuelos que podrían pagar medicina en los Andes? ¿tendrá que ver con que las entrevistas de admisión se convirtieron en mecanismos de segregación lo suficientemente dóciles como para no generar ruido?  ]

[De aquí a mañana cuando suba un loco sin estudio como Macías al poder, no tendremos los elementos para combatirle, nos los están robando, le están echando agua a una hoguera que costó la vida de mucha gente que sí luchó para prenderla. ¿Tiene dudas de que Macías pueda ser el próximo presidente de Colombia? ¡¡aterrice!! Trump y Duque son presidentes hoy.]

[Nos están educando para la defensa del sistema, y no me refiero solo a la organización de currículos flexibles donde gradúan gente ya casi que con la sola asistencia, sino a la manera en cómo propagamos la moral de la clase dominante, las formas de su cultura, las apreciaciones sobre la vida, la visión sobre el mundo, incluso sobre cómo se deben cambiar las cosas, se está deconstruyendo una real oposición, envolvieron en papel regalo una protesta sumisa y muchos la están aceptando.]

[La razón para la lucha social no es solo un desacuerdo; pasa más allá del fetiche de la libertad de expresión, pasa por encima de lo que diga RCN o Caracol sobre si es correcto o no, no se trata de salir a opinar o poner en escena una protesta cool. Sino que se trata de exponer una indignación que hace parte de la carne y del alma, de la sangre olvidada, del respeto y homenaje a la gente que ha muerto luchando o que la mataron por luchar, se trata de la organización de base para transformar problemas de los que se conoce su causa, sus culpables reales y no lo que dice el marketing político. Se trata de comprender, cueste lo que cueste, que hay cosas entre los que dominan y nosotros, absolutamente irreconciliables,]**¡comprender eso no es un crimen!** **¡ya tienen el poder, nos les demos el control!.**

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
