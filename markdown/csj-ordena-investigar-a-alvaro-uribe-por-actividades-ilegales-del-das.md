Title: Corte ordena investigar a Álvaro Uribe por actividades ilegales del DAS
Date: 2015-04-30 19:14
Author: CtgAdm
Category: Judicial, Nacional
Tags: Actividades ilegales del DAS, alvaro uribe velez, bernardo moreno, Comisión de acusaciones de la Cámara, Corte Suprema de Justicia, das, Iván Cepeda, Jose Obdulio Gaviria, Maria del Pilar Hurtado
Slug: csj-ordena-investigar-a-alvaro-uribe-por-actividades-ilegales-del-das
Status: published

###### Foto: elpais.com.co 

En el documento de lectura del fallo contra Bernardo Moreno y Maria del Pilar Hurtado, la sala penal de la Corte Suprema de Justicia compulsó copias para que la Comisión de Acusaciones de la Cámara investigue al expresidente Uribe por su responsabilidad en las actividades ilegales cometidas por el DAS durante su gobierno.

"*Frente a la petición elevada por las víctimas para iniciar investigación contra exfuncionarios de la Presidencia de la República en 2007 y 2008, la Corte, teniendo en cuenta que varios testigos hicieron referencia directa a la intervención de José Obdulio Gaviria, César Mauricio Velásquez, Edmundo del Castillo, Jorge Mario Eastman, e indirectamente del expresidente Álvaro Uribe, se transmitirá dicha solicitud a la Comisión de Acusaciones de la Cámara en lo que respecta a Uribe Vélez, a la Sala de Instrucción de este tribunal por el ahora senador Gaviria y a la Fiscalía relativo a los otros exfuncionarios para que decidan lo pertinente en caso de que aún no se hubiere iniciado la respectiva acción penal*", reza el fallo.

En esta misma decisión la Corte Suprema de Justicia condenó a 14 años de prisión a la exdirectora del DAS, María del Pilar Hurtado por los delitos de falsedad ideológica en documento público, peculado por apropiación, actos arbitrarios e injustos, concierto para delinquir, abuso de función pública.

Mientras que Bernardo Moreno Villegas fue sentenciado a 8 años de prisión por los delitos de violación ilícita de comunicaciones, abuso de función pública, abuso de autoridad y por actos arbitrarios e injustos.

La diferencia en las dos condenas radicaría, según la CSJ en que mientras que Bernardo Moreno se ha hecho presente en todas las audiencias, demostrando responsabilidad para asumir el proceso. Por su parte Maria del Pilar Hurtado estuvo prófuga de la justicia durante varios años.

Además el ex secretario de Álvaro Uribe, pagará su condena en prisión domiciliaria puesto que la Corte Suprema acogió la solicitud de la defensa y de la procuraduría. Hurtado, por su parte, seguirá recluida en el bunker de la fiscalía en donde se encuentra desde que se entregó a la justicia a principios de este año.

A pesar de las condenas en contra de altos funcionarios del gobierno Uribe, los procesos en su contra siguen sin dar frutos en la Comisión de Acusaciones de la Cámara. Para Iván Cepeda, una de las víctimas de estas actividades, el máximo responsable es el ex presidente Álvaro Uribe y debe responder ante la justicia por estos crímenes.
