Title: "Gobierno tiene que disponer toda su voluntad para implementar acuerdos" Carlos Medina
Date: 2016-12-06 12:27
Category: Abilio, Paz
Tags: FARC-EP, Gobierno Nacional, implementacion acuerdos
Slug: gobierno-tiene-que-disponer-toda-su-voluntad-para-implementar-acuerdos-carlos-medina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/f852363e-918e-4c86-9de2-b70b2be986a5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [6 Dic 2016] 

Luego  de la alocución del presidente Santos sobre darle paso al proyecto de la Ley de Amnistía hasta que la Corte Constitucional apruebe el Fast track, la implementación de los acuerdos regresa a la incertidumbre, a esto se le suma que **aún no están listas todas las adecuaciones a las zonas veredales** para que los guerrilleros puedan iniciar el traslado hasta estos lugres.

El analista político Carlos Media, Medina considera que **hay una serie de situaciones que están dificultando que esta implementación se dé conforme a como se había planeado** en un principio, primero  expone que  una vez refrendado el acuerdo de paz, debía darse inicio con el día D y al mismo tiempo las tropas de las FARC-EP empezarían a movilizarse a las zonas veredales.

Pero para que esto sucediera Medina explica que era necesario que **“las FARC pudiera contar con todas las posibilidades de garantizar una vida digna en esos territorios”** agrega que por el momento no hay acueductos, carreteras, abastecimientos de alimentos y no se sabe con claridad cómo serán los procesos que garanticen estos mínimos.

De otro lado el analista también expone que pudo haberse generado un **trabajo conjunto entre guerrilleros y militares para llevar a cabo esta carrera en el menor tiempo posible**.  Estos hechos incluso imposibilitarían que se pongan en marcha los diferentes programas para que los guerrilleros tengan un proceso de reincorporación a la sociedad civil.

Por otro lado Medina expresa que hay un problema frente a las **“incertidumbre jurídicas”** debido a la Ley de Amnistía, instrumento que garantiza la seguridad de los miembros de las FARC-EP y que se habría convertido en el primer incumplimiento por parte del gobierno, el analista afirma que **“es desafortunado que la Corte Constitucional haya hecho este receso para saber si es posible usar el Fast track”.**

Finalmente  Medina aseguro que “el gobierno tiene que disponer toda su voluntad y celeridad y debe ser eficiente y eficaz en la implementación de las normas según el orden de las prioridades que está demandando el proceso”. Le puede interesar: ["Ley de Amnistía urgente para avansar en implementación de Acuerdos" ](https://archivo.contagioradio.com/ley-de-amnistia-urgente-para-avanzar-en-implementacion-de-acuerdos/)

<iframe id="audio_14666295" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_14666295_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
