Title: Bolivia da un paso en la protección de especies silvestres
Date: 2017-01-12 17:41
Category: Ambiente, Otra Mirada
Tags: Bolivia, Fauna, protección
Slug: bolivia-especies-silvestres-proteccion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/alpaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:[[Thomas Kirchner]

##### 12 Ene 2106 

Con la firma del decreto 3048, el Ministerio del Medio ambiente y agua de Bolivia busca **frenar el comercio ilegal de especies silvestres en el país**, fortaleciendo la normativa nacional y ratificando el cumplimiento al reglamento de la Convención sobre el Comercio Internacional de Especies Amenazadas de Fauna y Flora Silvestres (Cites).

La ministra de la cartera Alexandra Moreira, afirmó que gracias a esta decisión se "**va a proteger a nuestra biodiversidad**, para que nuestras especies estén controladas y resguardadas" añadiendo que "en muchas ocasiones **se desconoce que el tráfico de especies se incluye dentro de los cinco delitos internacionales**, conjuntamente con el de drogas, armas y personas".

Dentro de la disposición legislativa se constituyó el Certificado Cites, documento que da las pautas normativas vigentes para la exportación, reexportación e importación, **permitiendo a las comunidades indígenas originarias y campesinas exportar productos como la fibra de vicuña y el cuero de lagarto de forma legal** y sustentable de la vida silvestre en equilibrio y armonía con la Madre Tierra.

La Convención Cites, es un acuerdo internacional en el que los gobiernos firmantes, 182 hasta la presente, **se comprometen a velar porque el comercio entre naciones de especies animales y de plantas silvestres no constituya una amenaza para su supervivencia**. Fue firmada en Washington en 1973 y entró en vigencia a partir de 1975.
