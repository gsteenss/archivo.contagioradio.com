Title: 5 propuestas de Marcha Patriótica para frenar oleada de violencia
Date: 2016-11-24 17:24
Category: Nacional, Paz
Tags: Asesinatos de líderes Marcha Patriótica, asesinatos de líderes sociales, Implementación de Acuerdos, marcha patriotica
Slug: 5-propuestas-marcha-patriotica-frenar-oleada-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marchapatriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [24 Nov 2016] 

Frente a los constantes asesinatos y ataques dirigidos a importantes líderes del movimiento político Marcha Patriótica, que han venido afectando no sólo a esta organización sino a las distintas comunidades, dicho movimiento se reunió con representantes del Gobierno para construir una agenda de cinco puntos que **garantice la seguridad de las comunidades y el desarrollo de actividades políticas en los territorios.**

David Flórez vocero nacional de la Marcha Patriótica aseguró que es “urgente que hayan soluciones concretas porque **esta situación perjudica la implementación de los acuerdos”,** es por ello que se hicieron una serie de propuestas **“para frenar este nuevo genocidio político”. **Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/) Marcha Patriótica.

### **Estas son las propuestas:** 

1.  Implementación de los acuerdos en forma inmediata, especialmente las garantías de seguridad y desmantelamiento de grupos criminales.
2.  Crear un programa especial de protección para la Marcha Patriótica.
3.  El Gobierno debe acoger medidas de legitimación política que reconozcan y evidencien la importancia que desarrolla la organización política.
4.  Dar incentivos positivos y negativos en términos de las violaciones de Derechos Humanos, para las entidades territoriales y para los batallones más vinculados en esas violaciones.
5.  Concentrar todas las investigaciones de estos crímenes en la unidad especializada de derechos humanos y en la unidad especial que crea el acuerdo de paz.

Para que las comunidades y los líderes regionales “tengan un poco de tranquilidad es necesario que esos acuerdos se materialicen, leemos positiva la disposición de diálogo del Gobierno **pero lo acordado debe hacerse realidad lo más pronto posible”** señaló Flórez.

El vocero nacional también aseguró que el Gobierno se comprometió con la Fiscalía para **esclarecer los hechos sucedidos en días pasados, a dialogar sobre la creación del programa especial y dar inicio a la refrendación del acuerdo** una vez esté en el Congreso.

Además reveló que el próximo miércoles “se va a evaluar el avance de cada uno de los puntos, nos reuniremos con la Unidad Nacional de Protección y llevaremos avances a las delegaciones del Gobierno, pero **nos preocupa que a pesar de las reuniones el día 23 de Noviembre asesinaron a otro compañero en Calima Buenaventura”.**

Por otra parte Flórez manifestó que “el acuerdo ha logrado también un leve cambio de algunos medios frente a las organizaciones como Marcha Patriótica, se han hecho evidentes cambios en el lenguaje, ya no apuntan tanto hacia la estigmatización, sin embargo **es fundamental el apoyo y acompañamiento de los medios alternativos”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
