Title: "Sumando ausencias", recordando a las víctimas
Date: 2016-10-10 16:05
Category: eventos, Paz
Tags: Doris Salcedo, Iniciativas de paz, memoria, performance paz, Sumando ausencias
Slug: sumando-ausencias-recordando-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/PVelazquez2010_00.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Medc.gob.es 

###### 10 Oct 2016 

La reconocida artista nacional **Doris Salcedo**, se suma a las iniciativas por la paz de Colombia, con **"Sumando ausencias"**, **una acción colectiva de memoria** que busca integrar a la sociedad civil con estudiantes, víctimas del conflicto, artistas entre muchos otros, para **actuar y recordar a las víctimas del conflicto armado**.

Con esta manifestación de arte vivo, Salcedo propone una **acción de duelo colectivo**; una invitación a la reflexión y al conocimiento público de lo acontecido durante las últimas décadas de violencia que se suma a otras instalaciones y piezas realizadas por la artista en temas de **resistencia y memoria**.

En la que será su más reciente obra, la artista junto a la Universidad Nacional de Colombia, apuesta por **visibilizar de manera pública los nombres de 2000 víctimas**, ilustrándolos colectivamente con cenizas **sobre una tela blanca de 7 kilómetros** de largo, en la que cada nombre ocupa 2.50 metros.

La primera etapa de la obra, comprendida entre el 6 y el 10 de octubre, tuvo lugar en el Museo de arte , el auditorio León de Geriff, el Claustro de San Agustin y el polideportivo de la Universidad Nacional, y este martes 11 **las telas serán cortadas y llevadas a la Plaza de Bolívar** donde, desde las 8 de la mañana y durante todo el día, **serán unidas con aguja e hilo** por voluntarios, transeúntes y los jovenes que actualmente adelantan el campamento por la paz. (Lea también: [Paz a la calle se sigue movilizando en toda Colombia](https://archivo.contagioradio.com/paz-a-la-calle-se-sigue-movilizando-en-toda-colombia/))
