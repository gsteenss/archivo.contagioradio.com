Title: Verónika Mendoza en entrevista con Contagio Radio
Date: 2016-07-12 08:20
Category: El mundo, Entrevistas
Tags: Elecciones Peru, Frente Amplio Perú, Verónika Mendoza
Slug: veronika-mendoza-en-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/veronika-mendoza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [11 Jul 2016] 

Más allá de los resultados de la carrera presidencial finalizada el 5 de junio y que Pedro Pablo Kuczynski sea mandatario electo de los peruanos, la figura de la joven **congresista y candidata por el Frente Amplio Verónika Mendoza**, se erigió como la representación renovada de los movimientos sociales, en un país históricamente gobernado por los partidos de derecha.

Psicóloga, antropóloga, maestra, y mujer líder, por sus venas corre sangre peruana y francesa. Nació hace 35 años en la provincia del Cuzco y desde muy temprana edad se vínculo al trabajo con organizaciones juveniles y de género, experiencias y conocimientos valiosos sobre el país y sus realidades que le motivarían a postularse al Congreso de la República trabajando por los Pueblos Andinos, Amazónicos y Afroperuanos.

Su decisión de aspirar a la presidencia en 2016, avalada por el Frente Amplio y con gran aceptación popular, enfrentó con una campaña austera y transparente, al poder político y económico representado por Keiko Fujimori, hija del expresidente ahora detenido por corrupción y crímenes de lesa humanidad Alberto Fujimori, el empresario Pedro Pablo Kuckzynski y el expresidente Alan García, obteniendo el tercer lugar en primera ronda con 18,8% de los votos.

Finalizando el mes de junio, Verónika Mendoza visitó la ciudad de Bogotá, para sumarse a las voces que apoyan la paz de Colombia y abogó por la necesidad de continuar en conversaciones con todos los actores del conflicto armado. Aprovechando su presencia en el país, Contagio Radio conversó con ella sobre su vida política, su posición ideológica, el papel del frente amplio en la democracia peruana y el presente de los movimientos sociales en América Latina.

<iframe src="https://www.youtube.com/embed/5RzzwZI3S-s" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
