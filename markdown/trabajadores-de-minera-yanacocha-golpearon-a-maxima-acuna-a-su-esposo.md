Title: Denuncian que trabajadores de minera Yanacocha golpearon a Máxima Acuña
Date: 2016-09-19 15:44
Category: El mundo, Entrevistas
Tags: defensores del ambiente, Máxima Acuña, Mineria, Yanacocha
Slug: trabajadores-de-minera-yanacocha-golpearon-a-maxima-acuna-a-su-esposo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Maxima-Acuña-e1474317506982.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  You tube 

###### [19 Sep 2016]

Hijos de la defensora del ambiente peruana y ganadora del premio Goldman, Máxima Acuña, denunciaron que funcionarios de la empresa minera Yanacocha, atacaron a Máxima y a su esposo Jaime cuando se acercaron a reclamar por la invasión pidiendo que no intervengan sus tierras. Sin embargo, según la denuncia, los funcionarios de la minera respondieron con golpes hacia Máxima y su esposo Jaime Chaupe, **dejándolos gravemente heridos.**

Daniel Chaupe, hijo de Máxima y Jaime, relata que sobre las **9:30 de la mañana del domingo, cerca de 80 agentes de seguridad de la empresa ingresaron al predio de Tragadero Grande,** sin ningún tipo de autorización y empezaron a intervenir el terreno con diversas herramientas. Cuando Máxima y su esposo se dieron cuenta de la presencia de los funcionarios les exigieron que se fueran pero la respuesta fueron golpes contra los líderes campesinos.

**“Agredieron a mi madre, con sus escopetas, la golpearon en la cabeza, la espalda, los brazos, tenemos miedo por su estado de salud”,** expresa Daniel, quien añade que Máxima fue la que resultó más afectada por los golpes que la dejaron hospitalizada debido a la gravedad. Así mismo, afirma que esas tierras fueron adquiridas desde 1994 por su familia, incluso tienen las escrituras y títulos que lo demuestran.

Por su parte, Yanacocha ha dicho que la empresa actuó dentro de su propiedad, “e**n firme defensa de sus derechos, y protegiendo la integridad física y los derechos humanos** de nuestros trabajadores y los miembros de la familia Chaupe, que no cuentan con autorización judicial para expandir sus actividades en los terrenos de Yanacocha, por lo que la actividad realizada hoy responde al cumplimiento estricto de la ley”.

Además, desde la compañía minera señalan que “Máxima Acuña y su esposo, Jaime Chaupe Lozano, **quisieron impedir esta acción legal arrojando piedras con hondas,** e incluso utilizaron una vara con clavos para golpear a quienes en ese momento estaban encargados de realizar la defensa. Asimismo, la pareja sacó una cámara y un trípode y filmó el evento”.

Cabe recordar que la mina se encuentra en Cajamarca, y es la segunda mina más grande a cielo abierto del mundo desde donde buscan extraer oro y cobre. Por cuenta de esa actividad se ha confirmado que **las aguas se han contaminado, toneladas de peces han muerto, y no se han respetado los derechos de las comunidad.**

<iframe id="audio_12970534" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12970534_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
