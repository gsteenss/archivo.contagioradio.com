Title: Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz
Date: 2019-05-14 17:26
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinatos, excombatientes, FARC
Slug: asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/excombat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:pazcífico noticias 

La tarde de este martes se conoció que **Wilson Saavedra**, ex comandante del bloque occidental Alfonso Cano de las FARC fue asesinado. Saavedra fue activo promotor durante el proceso de paz y en la reincorporación de excombatientes y estuvo encargado por algún tiempo del ETCR de Marquetalia, en Planadas, Tolima.

El hecho sucedió en el lugar conocido cono Rancho Panorama, salida norte del municipio de Tuluá, en departamento del Valle del Cauca, donde Jorge Enrique Corredor, conocido como **Wilson Saavedra de 49 fue herido de varios disparos, el excombatiente llegó con vida a un centro asistencial pero a los pocos minutos falleció.**

Saavedra, trabajaba en la zona rural de Tuluá, en el sector de La Moralia y Venus, adelantaba un proyecto productivo agrario entre la comunidad campesina y excombatientes con la ONU, el Pnud y la Secretaria de Paz de la Gobernación del Valle.

Con el crimen de **Wilson Saavedra, aumenta a  130** el número de excombatientes que han sido asesinados en diferentes zonas del país luego de la firma del acuerdo de paz. [(Lea también: Ejército habría torturado, asesinado e intentado desaparecer a Dimar Torres)](https://archivo.contagioradio.com/ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres/)

> [\#URGENTE](https://twitter.com/hashtag/URGENTE?src=hash&ref_src=twsrc%5Etfw)? || Siguen asesinando a excombatientes y el [@mindefensa](https://twitter.com/mindefensa?ref_src=twsrc%5Etfw) no ha hecho nada por garantizar la vida de quienes le han apostado a este proceso.  
> ¿Hasta cuándo este exterminio y este conejo a la paz?  
> Hoy fue Wilson Saavedra ¡Van al menos 112 compañeros asesinados!¡Indignante! [pic.twitter.com/lxQulouDQr](https://t.co/lxQulouDQr)
>
> — Sandra Ramírez (@SandraFARC) [14 de mayo de 2019](https://twitter.com/SandraFARC/status/1128429071455666180?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

