Title: Frente Amplio por la Paz presentará segundo informe de cese unilateral del fuego
Date: 2015-02-18 20:42
Author: CtgAdm
Category: Nacional, Paz
Slug: frente-amplio-por-la-paz-presentara-segundo-informe-de-cese-unilateral-del-fuego
Status: published

###### Foto: Álvaro Cardona 

<iframe src="http://www.ivoox.com/player_ek_4102550_2_1.html?data=lZadlJqZdI6ZmKiakp6Jd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPZz9nSjabRtM3d0JDd0dePsMKfscbnjdXWqdTZz9nO1Iqnd4a1kpDgx8zZssXjjM7byNTWscafxcqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Marisela Londoño, Frente Amplio por la Paz] 

El próximo 24 de febrero, en Caloto, Cauca, el **Frente Amplio por la Paz** presentará el segundo informe de verificación del cese al fuego unilateral decretado por la guerrilla de las FARC-EP desde el pasado 20 de diciembre.

Este proceso de empoderamiento social, que refleja la voluntad de la sociedad civil de participar y defender el proceso de paz que se adelanta con las guerrillas del **ELN y las FARC en Colombia**, ha estado trabajando a lo largo y ancho del país para reunir información sobre las acciones e inacciones militares por parte del Ejercito y las FARC.

Para Marisela Londoño, coordinadora de Comunicaciones de la **ONIC** y parte del equipo del Frente Amplio que redactó el informe final sobre el cese al fuego, ha sido un reto trabajar si la colaboración suficiente por parte del Estado. Aún así, el **Frente Amplio se ha apoyado en las organizaciones sociales** y populares que existen en los territorios, y ha reunido todas las fuentes de información existentes, desde informes de prensa, hasta reportes de organizaciones humanitarias.

La veeduría social, que no contó con el apoyo inicial del presidente **Juan Manuel Santos**, ha sido reconocida nacional e internacionalmente como una herramienta trascendental para el proceso de paz, al punto que el jefe de gobierno ha aceptando su importancia, y ha fijado una reunión con los voceros y voceras del Frente para discutir las garantías que debe brindar el Estado y los pasos a seguir por parte de la sociedad civil en el proceso de **desescalamiento del conflicto**.

Es por este motivo que el Frente Amplio por la Paz prepara la presentación del segundo informe, que además de contar con los elementos de contexto y las fuentes consultadas para el primer informe, busca incluir un también un capítulo en el que se explique lo que ha significado este cese de hostiles por parte de la guerrilla en los casi **70 días que se cumplen el 25 de febrero.**

Se convoca a colombianos y colombianas a enviar sus fotografías, videos, audios o escritos en los que, para compartir cómo se vive en el cese al fuego en los territorios, escribiendo al correo electrónico **millonesdeveedoresporlapaz@gmail.com** o como enlace a los hashtag **\#MillonesDeVeedoresXLaPaz y \#YoQuieroSerVeedorXLaPaz** .
