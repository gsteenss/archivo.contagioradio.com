Title: COP 21: La sociedad civil solo ha podido jugar el papel de observador
Date: 2015-12-01 11:29
Category: Ambiente, El mundo
Tags: cambio climatico, Carolina Roatta, COP 21, Efecto invernadero, francia, Medioambiente, Naciones Unidas, París
Slug: cop-21-la-sociedad-civil-solo-ha-podido-jugar-el-papel-de-observador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/cop21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: nación.com 

###### [30 Nov 2015]

La versión número 21 de la Conferencia Internacional sobre Cambio Climático supone el reto de adoptar políticas que mitiguen el impacto ambiental de las naciones hacia el medio ambiente. Aunque las decisiones –acertadas o erradas- que acuerden los gobiernos repercuten sobre todas las personas, muchos afirman que la sociedad civil no está representada, ni tiene voz y voto dentro del encuentro.

Desde París, la periodista Carolina Roatta expone que el papel de la sociedad civil “ha sido importante en este tipo de conferencias, pero siempre ha sido en calidad de espectadores” “dentro del lugar donde se desarrolla la reunión, investigadores y activistas han ejercido una presión para que los temas de la sociedad civil se aborden en la convención”. Entre ellos están responsabilidad histórica, agricultura, soberanía alimentaria y seguridad alimentaria.

Sin embargo, La Marcha por el Cambio Climático, que era la manifestación pública de la sociedad civil que se realizaría en Francia, en aras de mostrar la presencia de la sociedad civil en la apertura de la conferencia, no pudo llevarse a cabo, ya que las autoridades francesas la obstruyeron y no permitieron que se desarrollara, debido a que las marchas en plazas públicas no están permitidas en el corto plazo, por el Estado de Emergencia declarado por el gobierno de ese país después de los ataques terroristas en París, ocurridos el pasado 13 de noviembre.

Alrededor de la COP 21 que ha dado inicio este 30 de noviembre, se han dado dos anuncios en temas de financiación: el primero de ellos consiste en que el gobierno de Estados Unidos y el Banco Mundial, han anunciado iniciativas de financiación de recursos para países en vías de desarrollo en donde se fomenten proyectos de desarrollo que mitiguen el impacto ambiental.

La segunda, consiste en que se espera que se pacte un acuerdo trasnacional durante la cumbre, puesto que los representantes de los países le han otorgado importancia en sus agendas al evento: “de los 195 países miembros de la Conferencia de las Naciones Unidas por el cambio climático, 172 ya presentaron su propuesta para mitigar el cambio” dice Roatta.

Respecto a la situación de orden público en Francia, la periodista afirma que constantemente se realizan requisas en centros comerciales, y se cancelan los recorridos de los trenes “por temor a un ataque terrorista o la presencia de un paquete sospechoso”. Cabe destacar que según la entrevistada, los protocolos de seguridad se intensifican y se hacen más rígidos con las personas de apariencia árabe, lo que hace referencia a una constante tensión racial.
