Title: Producción agrícola se salva con la victoria del no en consulta popular de Arbeláez
Date: 2017-07-10 13:11
Category: Ambiente, Nacional
Tags: Arbeláez, consulta popular, hidrocarburos, producción agrícola
Slug: 43359-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/arbelaez-cultivos-e1499705552256.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias UN] 

###### [10 Jul 2017] 

Luego de que en la consulta popular de Arbeláez en Cundinamarca, los habitantes dijeran no a la realización de actividades de exploración de materiales de hidrocarburos, **se habría salvado un gran porcentaje del abastecimiento de frutas y aves** para el consumo de los capitalinos, además de la producción de agua para 5 municipios agrícolas de la región que se abastecen del páramo de Sumapaz

Así lo aseguró Ramiro Acuña, miembro del Comité por el No en la Consulta Popular, quien defendió que la consulta no es solamente para que no se realice minería o explotación petrolera sino que se actuó en defensa de las tradiciones agrícolas de una región vital en la producción de alimentos y que es **responsable del abastecimiento de un gran porcentaje de algunos productos básicos para los habitantes de Bogotá. **(Le puede interesar: ["Comunidades votaron no a la minería en Pijao y Arbeláez"](https://archivo.contagioradio.com/arbelaez-piajo-consulta-minera/))

Según Acuña, Arbeláez está ubicado en la provincia del Sumapaz donde **está el páramo más grande del mundo**. Adicional a esto, la diversidad de los suelos le permite a este territorio disfrutar de zonas de páramo, manantiales, zonas áridas y arbóreas que representan una diversidad ambiental de conservación.

En las zonas aledañas al páramo, los y las arbelaences **cultivan productos como mora, lulo, habicuela, arveja y tomate de árbol** y este municipio es uno de los mayores productores de aves para el consumo humano. Al día, según Acuña, "vienen de Bogotá 15 camiones dispuestos a llevar pollos que posteriormente distribuyen en la capital".

**Agua de Cundinamarca no alcanzaría para abastecer las necesidades de la industria petrolera**

Para Ramiro Acuña, miembro del Comité por el No en la Consulta Popular, **“aquí se defendió el agua como recurso vital para la región de Cundinamarca”**. Afirmó además que, en Arbeláez, empresas como Alange Energy “han hecho exploraciones sísmicas desajustando la tierra y afectando los recursos hídricos necesarios para el cultivo de productos agrícolas”. (Le puede interesar: ["El municipio de Arbeláez sale a votar contra las actividades petroleras"](https://archivo.contagioradio.com/arbelaez_consulta_popular_contra_petroleo/))

De igual manera, Acuña hizo énfasis en que los habitantes de Arbeláez se abastecen de agua por medio de un tubo de 6 pulgadas que surte 22 litros por segundo. “Por medio del fracking, las empresas, instalan tuberías de 45 centímetros de ancho **capaz de transportar 54 litros por segundo** sólo para las actividades de extracción de hidrocarburos, esa cantidad de agua ni siquiera la tiene el municipio” afirmó Acuña.

**Colombia no es un país que disfrute de la producción de petróleo**

Frente a las afirmaciones de la Agencia Nacional de Hidrocarburos, que manifestó que los impuestos en el país deben subir debido al freno que ha puesto los resultados de las consultas populares, Ramiro Acuña manifestó que **“existen países que no producen petróleo y tienen condiciones de vida de calidad”**.

Por esto afirmó que “la producción de petróleo en el país **no se disfruta, se lo llevan las grandes empresas petroleras** y nos lo devuelven como gasolina con los precios más altos que hay en el mercado”. (Le puede interesar: ["Es hora de hablar enserio de agua y petróleo"](https://archivo.contagioradio.com/es-hora-de-hablar-en-serio-de-agua-y-petroleo/))

Finalmente, Acuña recordó que “este tipo de actividades petroleras no trae ningún beneficio a los pueblos porque deja enfermedades, destrucción de recursos y al pueblo no le queda nada”. Por esto manifestó que **“el pueblo colombiano ha empezado a tomar conciencia con respecto a lo que genera este tipo de actividades** y esto se ha hecho visible con la victoria en las consultas populares de diferentes municipios”.

<iframe id="audio_19719658" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19719658_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
