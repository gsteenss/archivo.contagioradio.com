Title: Le tienen miedo a la democracia
Date: 2017-10-04 10:05
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: JEP, liberales, Robledo, Vargas Lleras
Slug: le-tienen-miedo-a-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/miedo-a-la-justicia1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Double X Economy]

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 4 Oct 2017

Temeroso de una democracia real. Pareciera que así es el liberalismo y todos sus representantes, así son los que se hacen llamar la “nueva cara” los que se hacen llamar “la coalición centro derecha”, los que incluso son tan descarados de autodenominarse “libertarios” cuando saben que son más neoliberales que Friedman.

[Ya todo está claro… las corporaciones mediáticas de información han hecho de las suyas cuando encarnizadamente se abalanzan exclusivamente contra los gobiernos de izquierda y los acusan de dictatoriales movilizando a masas de espectadores a odiar o a amar según la conveniencia de los grupos de poder que ni cortos ni perezosos necesitan idiotas útiles que griten]*[viva la democracia]*[pero por supuesto no lo suficientemente fuerte como para que se escuche.]

[Pasó desde los años 60 con Fidel Castro en Cuba, pasó en el 73 con Allende en Chile cuando la CIA y la ONU organizaron la escasez de alimentos, el paro de camioneros y la satanización del socialismo para preparar los “ánimos” para recibir los bombazos de “Pinocho”; pasó con el ex presidente Fernando Lugo en Paraguay en 2012, un sacerdote católico de izquierda que fue acusado infamemente por el caso de Curuguaty y destituido por la derecha liberal; pasó con Evo Morales en Bolivia durante al año 2008 cuando fue acusado por la masacre de Pando y querían bajarlo como a Lugo, precisamente cuando los indios del Sumak Kawsay le quitaban el poder a los blancos liberales de Thomas Hobbes.]

[Pasó en Honduras en el 2009 y el golpe de Estado que le dieron al socialista Zelaya, un golpe del que no se dijo nada en la OEA, ¿OEA? ¡hipócritas! tampoco se pensó en sanciones contra los golpistas ni nada. Pasó en Ecuador en el 2010 cuando un grupito de policías, tal como había pasado en Bolivia (extraña coincidencia) se sublevaron, secuestraron a Rafael Correa y éste tuvo que ser rescatado a sangre y fuego por el ejército. Pasó en Venezuela en el 2002… “casi matamos a Chávez pero lo bajamos” celebraba la CIA, la gente no se aguantó un golpe de Estado, el ejército defendió a la población y Chávez regresó para después morir de cáncer… naturalmente... hasta ahora.]

[Todo está claro… hasta el escritor de derecha Mario Vargas Llosa, alguna vez decía en Buenos Aires, que “el liberalismo no tenía nada que ver con las dictaduras” mientras desde Chile lo aplaudían fuertemente todos los discípulos consagrados de Friedman para salvar así fuera a punta de aplausos la absurda falencia de la teoría neoliberal, que consistió y consiste hasta ahora en vender humo, anunciando la unión entre libertad económica y libertad política… ¡humo! pues la democracia no sirve al capitalismo, lo que le sirve es la sensación de democracia. Por eso Pinochet, un sanguinario, sin necesidad de democracia, demostró que el neoliberalismo funciona…  ]

[Cómo no notarlo… en Colombia la gente no estuvo en un letargo siempre, lo que sucede es que han matado a tantos y a tantas que las lágrimas se abrazaron con la rabia para fabricar la desazón que acompaña la disminución física de tanta gente que le apostó al cambio político en Colombia y está bajo tierra, en los ríos, en el mar, en los hornos crematorios de Norte de Santander, en las fincas, en la selva que se guarda los secretos y pudre los cuerpos en cuestión de días.]

[Las consultas mineras, las movilizaciones sociales, las revocatorias, avanzan en Colombia bajo el ataque de las corporaciones masivas de información, aludiendo que son ilegales, que no permiten avanzar al país, recordándonos todos los días que la democracia consiste en representantes y “ciudadanos responsables”, ocultando el engaño rotundo que ha sido endosar la libertad política, la estafa que ha resultado cuando se otorga el poder de decisión, a candidatos que han desangrado la riqueza producida por el trabajo de la gente.  ]

**El poder de decidir. ¡Eso es democracia! ¿decidir quién sube al poder a decidir? No. El poder decidir cosas no al funcionario. El poder que los actores sociales tienen sobre decisiones que afectan su territorio, su equilibrio, y que no están en función del capital, ni bañadas por la basura mediática.  **

[La gente ya entendió, incluso desde otras latitudes, se ofrece el panorama de una democracia desenmascarada que deja ver su rosto fascista. Una democracia desenmascarada, porque precisamente nunca lo fue. Fue siempre un fascismo, un autoritarismo impuesto por grupos de poder que suavizaron su ejercicio con ayuda de los medios masivos (de su propiedad) con la instrumentalización de la tecnología para generar un efecto político en el que nadie busca, nadie lee y solo mira y mira las imágenes que ocultas tras bambalinas lingüísticas de democracia y libertad, se alteran y reprimen con fuerza a quienes optaron por decidir y responden con estupidez a los que todo lo ven a través de las pantallas o las pantallitas.]

[Los liberales, bien sean medio derechosos como Claudia López, bien sean medio izquierdosos como Robledo, bien sean dementes como la prole derechista del centro democrático, a todos y sin excluir incluso a los que creen que el mundo comenzó con ellos y juran que son “la cara nueva” cuando en realidad están trasnochados, ¡¡a todos los liberales!! Lo que menos les conviene es una democracia donde los actores sociales, es decir, los que quieren gobernar y no dejarse “gerenciar”, tomen las decisiones.]

[A los liberales les da miedo la democracia sencillamente porque la gente tendría de nuevo el poder de decidir, y todos ellos quedarían a merced de decisiones que afectarían sus intereses particulares. El estado dejaría de estar secuestrado por una clase política mezquina, la leyes serían redactadas sin los ronquidos, desde los gremios, desde los barrios, las votaciones serían para decidir algo, y no para elegir el que va a quedarse con la ventaja de tener el campo de las decisiones a su criterio.]

[El sistema democrático liberal en Colombia caducó, falló, fracasó, y el futuro de una patria nueva está en las manos de capacidad que tengan las personas para detallar el tamaño del engaño. ¿es fácil descubrir el engaño? Para nada, pero el mundo entero, está despertando, y Colombia no es la excepción. Cualquier esfuerzo por no dejarnos someter por este sistema altamente especializado, deberá ser considerado como resistencia.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
