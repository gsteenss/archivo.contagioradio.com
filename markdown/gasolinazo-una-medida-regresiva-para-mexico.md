Title: Gasolinazo una medida regresiva para México
Date: 2017-01-09 12:08
Category: Economía, El mundo
Tags: gasolinazo, mexico
Slug: gasolinazo-una-medida-regresiva-para-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/pf-3108120314_PROTESTA_GASOLINA_HC02-1-d.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### [9 Enero 2017] 

Después de la medida del aumento sobre la gasolina o gasolinazo impuesta por el presidente de México Peña Nieto, que ha sido calificada como regresiva y perjudicial para la clase media y baja, **cientos de ciudadanos de este país, han salido a las calles para exigir que se derogue el aumento hasta del 20%** sobre el combustible, que además generaría el alza en el transporte público, servicios y los productos de la canasta familiar que se transporten por vía terrestre.

De igual forma diputados como Emiliano Álvarez y Guillermo Santiago, expresaron que el argumento que ha usado Peña Nieto, sobre que esta medida es necesaria debido al aumento del precio en que Estados Unidos exporta la gasolina, es falso y que el verdadero debate se encuentra en si **“el presidente quiere apoyar la economía popular o proteger el negocio gasolinero”.**

Al respecto el Partido de la Revolución Democrática mexicano exigió en el Senado que se haga público el documento que expone los propietarios de las estaciones de gasolina, en donde al parecer abrían ex funcionarios de Pemex o legisladores que en un principio apoyaron la reforma energética que llevo al cierre de muchas refinerías en este país.

De igual forma, la ciudadanía ha señalado que el aumento de la gasolina es desproporcional, esto debido a que en algunos estados solo subió un 9% y en otros ha alcanzado hasta el 20%, aumentos que se reflejaran en otros servicios como el transporte público, los alimentos de la canasta familiar. Además a esta situación se le agrega la inflación y el desempleo que afronta el país y que ha generado una **economía poco estable con un crecimiento del 7% para el 2017 en el salario mínimo.**

No obstante, diferentes organizaciones y sectores del **movimiento social en México ya se han organizado para expresar su rechazo ante esta medida**, en las últimas horas el movimiento estudiantil informó que hará parte de las tomas pacíficas que se realizan a las calles y estaciones de gasolina de este país, mientras que en **25 estados ya se han realizado marchas** y plantones exigiendo se derogue el aumento del precio de la gasolina.
