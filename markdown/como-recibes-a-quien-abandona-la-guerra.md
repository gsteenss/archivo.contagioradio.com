Title: Jóvenes se alistan para recibir a quienes abandonan la guerra
Date: 2016-09-15 18:15
Category: Nacional, Paz
Tags: Conversaciones de paz con las FARC, Diálogos de paz Colombia, menores de edad en las farc
Slug: como-recibes-a-quien-abandona-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Si-a-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Si a la paz] 

###### [15 Sept 2016] 

Este jueves en la Biblioteca Nacional se realiza el panel ¿Cómo recibes a quien abandona la guerra?, organizado por la Red Político Artística de Mujeres Jóvenes y la Corporación Humanas, con el objetivo de analizar el proceso de[ [reincorporación de la población infantil y juvenil](https://archivo.contagioradio.com/asi-sera-el-procedimiento-de-salida-de-menores-de-las-farc/)], así como los retos y desafíos que la sociedad civil, organizaciones e instituciones estatales deberán asumir **para contribuir a que hayan garantías educativas, laborales y de seguridad la reinserción**.

De acuerdo con Natalia Bejarano, integrante de la Red, es fundamental garantizar que los menores de edad que dejen las armas no sean perseguidos y que haya **acompañamiento por parte de organizaciones con experiencia en la atención de niños, niñas y jóvenes**, para garantizar que ámbitos como la educación, el trabajo y la formación artística brinden las herramientas necesarias para fortalecer sus proyectos de vida y de formación política.

Bejarano asegura que la sociedad civil debe comprender que es más provechoso invertir en la paz que en una guerra en la que a diario se han invertido \$22 mil millones, en este sentido una de las [[apuestas fundamentales debe ser la inversión en la educación](https://archivo.contagioradio.com/se-lanza-la-iniciativa-la-paz-si-es-con-educacion/)], no para implementar cátedras **sino para que la paz se convierta en un eje central que permita sanar las heridas de la guerra**, como un compromiso de largo aliento.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
