Title: En Colombia el 62% del agua concesionada está en manos de un 1% de empresarios
Date: 2017-03-14 16:45
Category: Ambiente, Nacional
Tags: extractivismo, Hidroeléctrica El Quimbo, Miller Dussán investigador de ASOQUIMBO
Slug: en-colombia-el-62-del-agua-concesionada-esta-en-manos-de-un-1-de-empresarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/hidroeléctricas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: WEG] 

###### [14 Mar 2017] 

De acuerdo con el Movimiento Ríos Vivos, en Colombia hay más de **120 megacentrales hidroeléctricas y más de 800 microcentrales. Según la información, el 1.1% de los dueños de las concesiones de agua en el país tienen el 62% del volumen del agua** concesionada. Comunidades y organizaciones que defienden el agua, la vida y el territorio, han denunciado que es uno de los países más desiguales en términos de acceso a la tierra y, sobre todo, en relación con los derechos para el uso del agua.

### Día Mundial Contra las Represas 

Por ello, cada 14 de marzo, se celebra en todo el mundo el Día por la defensa del Agua Contra las Represas. En el marco de esta celebración, el Movimiento Ríos Vivos llevará a cabo en Risaralda, Quindío, Cauca, Cundinamarca, Huila, Antioquía, Bajo Sinú y Santander, distintas actividades como marchas carnaval, conversatorios y talleres para sensibilizar a la población sobre la importancia de la **protección, conservación y restauración de las fuentes hídricas que hacen de Colombia uno de los países con más agua** dulce en el continente.

Frente a ello, el profesor de la Universidad Surcolombiana e integrante de ASOQUIMBO, Miller Dussan, comentó que la privatización del agua y los territorios, se viene imponiendo a través de iniciativas del Gobierno como los Planes Departamentales de Agua a nombre de “Aguas para la Prosperidad”, **proyectos extractivos de minería e hidrocarburos, represas como El Quimbo, Hidrosogamoso o Hidroituango, la agroindustria y el Pago por Servicios Ambientales.**

Dussan, manifestó que una de las mayores exigencias por parte de diversas comunidades campesinas, indígenas, negras y mestizas, es que **las instituciones del Estado “no pueden seguir estando al servicio del capital corporativo”**, explica que entidades como la ANLA “nunca ha cumplido con sus funciones constitucionales de seguimiento y control, sino que se ha vuelto subsidiaria de las multinacionales”.

Señala que, a pesar de las fuertes reivindicaciones de las poblaciones, “la institucionalidad es un gran obstáculo”, sin embargo, resalta que ya son varios los municipios que acogidos en la **sentencia T 445 de 2016, la cual determina que cada municipalidad podrá decidir el modelo de desarrollo que considere más favorable** para su entorno, se ha logrado suspender actividades de multinacionales y frenar proyectos de licitaciones. ([Le puede interesar: El ‘no’ a hidroeléctricas y minería en cabrera ganó con el 97.28% de los votos](https://archivo.contagioradio.com/cabrera_dijo_no_hidroelectricas_mineria/))

### Paz con Justicia Socio Ambiental 

Dussan aseguró que, en el marco de la implementación de los acuerdos, es fundamental que “se trabaje en una paz con justicia socio ambiental”, señaló que tantos años de guerra y de extractivismo a costa de las comunidades y los ecosistemas “**ameritan un derecho a vivir en armonía, que ahora hayan garantías para la conservación de la vida”.**

Se refirió puntualmente al caso de El Quimbo e indicó que desde la puesta en marcha de la hidroeléctrica, prácticas como la pesca artesanal han desaparecido, **“son más de 3.500 las familias que hacían pesca artesanal y se vieron afectadas, en total, decimos que son 33.000** las víctimas que ha dejado a su paso EMGESA”. ([Le puede interesar: Organizaciones ecologistas impulsan la creación de una comisión de la verdad ambienta](https://archivo.contagioradio.com/organizaciones-ecologistas-impulsan-la-creacion-una-comision-la-verdad-ambiental/)l)

Por último, el profesor y defensor ambiental señaló que debe haber una fuerte veeduría ciudadana a la implementación de los acuerdos, sobre todo, **“en los temas ambientales que poco resueno han tenido, pero que le incumben a toda Colombia”**, por otra parte el Movimiento Ríos Vivos invita a la comunidad académica para que se una a la defensa del agua, la vida y los territorios y apoye en distintas labores como estudios de impacto ambiental que favorezcan a las comunidades e inventarios de biodiversidad, “para saber qué y cómo vamos a defenderlo”.

[Sentencia T 445 del 2016](https://www.scribd.com/document/341875019/Sentencia-T-445-del-2016#from_embed "View Sentencia T 445 del 2016 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17543931" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17543931_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_60051" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/341875019/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-w6CSTElr5kkf8D6wFWIz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6545718432510885"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
