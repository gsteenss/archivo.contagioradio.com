Title: Los retos de la implementación con enfoque de género
Date: 2017-03-04 22:18
Category: Mujer, Nacional
Tags: enfoque de género, implementación acuerdos de paz, Subcomisión de género
Slug: los-retos-d-ela-implementacion-con-enfoque-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/campesinas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Semanario] 

###### [4 Mar 2017] 

La formación política para la participación de las mujeres, rurales y urbanas, **la promoción de su papel como veedoras de la implementación y la incidencia en políticas públicas respecto a los derechos de mujeres** y personas con orientación sexual e identidad de género diversa, son algunos de los retos planteados por la Subcomisión de Género de las FARC.

Victoria Sandino integrante de la Subcomisión resaltó que desde la creación de dicha comisión en 2014, las mujeres de las FARC han venido trabajando en la recolección de las demandas y la articulación de propuestas de distintas organizaciones de mujeres y feministas de toda Colombia, **“para garantizar que sus voces fueran escuchadas, y sus apuestas tenidas en cuenta para los Acuerdos”.**

Comenta que después de los resultados del plebiscito, fue necesario replantear la estrategia para incluir el enfoque de género, pues **“las discusiones sobre la supuesta ideología de género, entorpecieron el proceso, sin embargo no se dañó todo lo que habíamos logrado”**. ([Le puede interesar: La participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/))

### Manuela, otra de las integrantes de la subcomisión, señaló que el objetivo del enfoque de género, más allá de generar rupturas en el modelo social, pretende dar solución a un conflicto que tiene “raíces estructurales  en causas políticas muy profundas”, explica que la guerra en Colombia no comenzó **“por si la familia es de hombre o mujer, por lo tanto eso no le compete a un acuerdo de paz”.** 

Sandino manifiesta que las mujeres insurgentes han tenido como formación a lo largo de su participación en las FARC, una formación político-militar que de una u otra ha generado rupturas con “todos los roles establecidos que nos había impuesto la sociedad en la medida que somos mujeres libres, que tenemos decisión sobre nuestro cuerpo, y que tenemos de manera igualitaria derechos y deberes con nuestros compañeros”. Resalta que eso ha facilitado la comprensión de los contextos y las situaciones que viven las mujeres rurales a diario, **“queremos ahora poder transmitir todo esto a  la sociedad a la que nos vamos a incorporar”**, puntualizó.

Victoria hizo énfasis en que urge trabajar en el **“cómo vamos a participar en la implementación de forma activa y cómo vamos a ser beneficiarias de los mismos”**, aseguró que unos de los mayores retos es la formación al interior de la organización y de allí, propender por formar en el tema a las mujeres rurales y urbanas de Colombia, “para que tengan un papel protagónico y sean las garantes de la implementación”. ([Le puede interesar: Participación Política, el reto de organizaciones de Mujeres y Disidentes del Género](https://archivo.contagioradio.com/participacion-politica-reto-organizaciones-mujeres-disidentes-del-genero/))

<iframe id="audio_17381704" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17381704_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
