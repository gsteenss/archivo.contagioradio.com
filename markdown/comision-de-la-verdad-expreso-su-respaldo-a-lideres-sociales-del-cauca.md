Title: Comisión de la Verdad expresó su respaldo a líderes sociales del Cauca
Date: 2019-05-14 10:41
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ACONC, Cauca, comision de la verdad, lideres sociales
Slug: comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Comision-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

La Comisión de la Verdad en cabeza del padre Francisco de Roux y 7 de sus 11 comisionados visitó la vereda Lomita en Santander de Quilichao para escuchar y expresar su respaldo a los líderes de la **Asociación de Consejos Comunitarios del Norte del Cauca (ACONC)**, víctimas de un atentado contra su vida el pasado 4 de mayo.

### La Comisión pidió a las comunidades mantener el valor de la verdad 

Alexis Mina Ramos, integrante de ACONC, destacó la visita de la Comisión, exaltando el interés que ha mostrado esta institución por "escuchar las voces de quienes sufrimos las debacles del conflicto armado" agregando que es necesario avanzar en su reconocimiento como sujetos étnicos de especial protección, la condición de víctimas del conflicto armado y poder avanzar en la reparación.

Durante su visita, el padre De Roux  y los comisionados  invitaron a las comunidades negras a mantener el valor de la verdad, "hoy vamos a dar un paso adelante, hacemos de su causa y de sus vidas, nuestra causa y lucharemos para que no se repitan las amenazas, las incertidumbres y las muertes", declaró el presidente de la Comisión.

### **El presidente debe venir a darle la cara a la gente** 

En días pasados, integrantes de la Comisión de Paz del Senado también acudieron a Santander de Quilichao donde escucharon a los líderes sociales quienes les solicitaron interceder para que el presidente Duque visite el Cauca y cumpla con lo pactado en el marco de la Minga del suroccidente del país.

**"El quinto hombre más custodiado de este hemisferio dice que no viene a hablar con la comunidad indígena, negra y campesina porque le da miedo, si eso dice una persona que tiene todas las garantías de seguridad, qué puede pensar la comunidad que está siendo afectada**", declara el líder social. [(Le puede interesar: "Señor Presidente, lo estamos esperando en Santander de Quilichao": ACONC)](https://archivo.contagioradio.com/senor-presidente-lo-estamos-esperando-en-santander-de-quilichao-aconc/)

Alexis Mina afirma que continuarán trabajando junto a la Comisión y la JEP para avanzar hacia la verdad, "necesitamos saber quiénes son los autores intelectuales, necesitamos que se repare a la comunidad y que este tipo de cosas no sigan pasando".

Inicia el acercamiento con líderes de comunidades negras y afros del Norte del Cauca. Participan [@FranciscoDeRoux](https://twitter.com/FranciscoDeRoux?ref_src=twsrc%5Etfw), presidente de la Comisión, y las comisionadas Ángela Salazar, Marta Ruiz, Patricia Tobón, Saúl Franco, Carlos Ospina y Alejandro Valencia. [\#LaVerdadEsConLosLíderes](https://twitter.com/hashtag/LaVerdadEsConLosL%C3%ADderes?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/gX0fegTIYT](https://t.co/gX0fegTIYT)

> — Comisión de la Verdad (@ComisionVerdadC) [11 de mayo de 2019](https://twitter.com/ComisionVerdadC/status/1127253331339096064?ref_src=twsrc%5Etfw)

<h6>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_35766880" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35766880_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</h6>
###### Reciba toda la información de Contagio Radio en [[su correo]
