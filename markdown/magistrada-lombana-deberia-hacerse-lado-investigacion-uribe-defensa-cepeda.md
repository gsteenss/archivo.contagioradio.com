Title: Magistrada Lombana debería hacerse a un lado en investigación sobre Uribe: defensa de Cepeda
Date: 2019-03-26 13:36
Category: Entrevistas, Judicial
Tags: alvaro uribe velez, Corte Suprema de Justicia, Iván Cepeda
Slug: magistrada-lombana-deberia-hacerse-lado-investigacion-uribe-defensa-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/IvánCepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 26 Mar 2019 

[Tal como lo reveló el periodista Daniel Coronell en su columna, la **magistrada Cristina Eugenia Lombana Velásquez**, ocultó haber trabajado con el abogado **Jaime Granados,** defensor del senador Álvaro Uribe, el hecho de omitir este vínculo es una evidencia no solo de que información fue eludida sino del impedimento moral que acarrearía asumir la investigación.  
]

**Reinaldo Villalba, abogado representante del senador Iván Cepeda** quien interpuso las demandas contra el senador del Centro Democrático**, **afirma que no tenían conocimiento del vínculo existente entre Lombana y el abogado Jaime Granados y que al c[onocer la noticia a través de la columna de Coronell les "genera todo  tipo de preocupación" pues queda en duda  el nexo establecido entre el juez y las partes que están actuando en el caso y podría significar una ausencia de objetividad en los procesos contra el senador Uribe.]

A la información revelada se suma el hecho que la magistrada Lombana **sea a su vez integrante de las Fuerzas Militares,** "lo que deja grandes dudas sobre su independencia con relación a Iván Cepeda, quien ha estado siempre del lado de las víctimas, las que en muchos casos han sido vulneradas por actos cometidos por la Fuerza Pública" añade.

El abogado señala que aunque no han realizado ninguna actuación al respecto en el proceso después que el caso fue asumido por la magistrada, con esta nueva información surgen muchas inquietudes que evaluarán para poder actuar al respecto y tomar decisiones. [(Lea también: Uribe está haciendo una tenebrosa campaña contra la Corte Suprema: Iván Cepeda)](http://“URIBE%20ESTA%20HACIENDO%20UNA%20TENEBROSA%20CAMPAÑA%20CONTRA%20LA%20CORTE%20SUPREMA”%20IVÁN%20CEPEDA)

Respecto al sesgo que podría verse evidenciado en este caso, es válido hacer un paralelo con los integrantes de la Jurisdicción Especial para la Paz, criticados por partidos como el Centro Democrático quienes denuncian que al estar vinculados con organizaciones de DD.HH. no serían objetivos con la Fuerza Pública a lo que Villalba argumenta que los jueces en general deben tener un amplio conocimiento en materia de derechos humanos y que como tal no pueden estar interesados en condenar a alguien que según la pruebas es inocente.

[Para el abogado, la salida más obvia y transparente de la magistrada Cristina Eugenia Lombana es que dé un paso al costado y se separe  del caso, **"sería la salida más tranquila para todos e incluso para la propia magistrada".** Villalba agrega que acerca de los demás magistrados que hacen parte de la sala y sus hojas de vida solo tiene conocimiento de lo que se ha revelado a través de medios de comunicación. ]

<iframe id="audio_33751172" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33751172_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
