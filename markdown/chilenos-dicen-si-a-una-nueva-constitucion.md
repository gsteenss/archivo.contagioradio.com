Title: Chilenos dicen SÍ a una nueva constitución
Date: 2020-10-26 19:54
Author: AdminContagio
Category: Actualidad, El mundo
Tags: Chile, Constitución chilena, Plebiscito en Chile
Slug: chilenos-dicen-si-a-una-nueva-constitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Chile-aprueba-una-nueva-constitucion-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @NicolasMaduro

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este domingo 25 de octubre, se llevó a cabo el plebiscito nacional en el que los chilenos decidirían si se cambiaba la Constitución del país. **Con el 78% de los votos a favor (**5.885.721 votos**) y 21,73% en contra (**1.633.932 votos****)**, Chile reemplazará la actual Constitución, redactada durante el régimen militar de Augusto Pinochet.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El cambio de Constitución fue una de las principales demandas tras el estallido social de octubre del año pasado, por lo que para la sociedad chilena, esta decisión es una oportunidad para realizar e implementar las reformas sociales de fondo que clama el país en relación a distribución del poder, desigualdad y bienes públicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El presidente Sebastián Piñera señalo que este «es un triunfo de todos los chilenos y chilenas que amamos la democracia, la unidad y la paz» e hizo un llamado a la «unidad para enfrentar los grandes desafíos del futuro. Y les pido a mis compatriotas que estemos a la altura de este desafío histórico». (Le puede interesar: [Bolivia lejos de elecciones libres por las múltiples amenazas del gobierno de facto](https://archivo.contagioradio.com/bolivia-lejos-de-elecciones-libres-por-las-multiples-amenazas-del-gobierno-de-facto/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sebastianpinera/status/1320521191530975232","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sebastianpinera/status/1320521191530975232

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La opción de 'Apruebo' abarcó todas las regiones del país y en la Región Metropolitana, la mayor parte de las comunas votaron por la aprobación, a excepción de Las Condes, Vitacura y Lo Barnechea.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En horas de la tarde, la Plaza de la Dignidad fue llenándose de cientos de chilenos y chilenas que celebraron con banderas y cacerolazos por la victoria como un reflejo del "renacimiento" en la historia del país sudamericano.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Qué prosigue para Chile?
-------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En estas votaciones, además de elegir si se escribía una nueva constitución, se definiría qué órgano redactaría la nueva Constitución y el resultado, con 78,98% de los votos determinó que sería una Convención Constitucional. **Esta modalidad tendrá 155 miembros, en la cual todos sus integrantes deben ser elegidos popularmente y quienes estarán repartidos equitativamente entre hombres y mujeres, con un número de escaños reservados para comunidades indígenas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La elección de los miembros que integrarán la Convención Constitucional serán elegidos en abril del año 2021. Posteriormente, tendrán nueve meses para entregar la propuesta constitucional. Finalmente, se realizaría un nuevo Plebiscito para aprobar o rechazar la propuesta**.** (Le puede interesar: [Carabinero a prisión por arrojar a joven al río en Chile](https://archivo.contagioradio.com/carabinero-a-prision-por-arrojar-a-joven-al-rio-en-chile/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, **se realizaría un nuevo Plebiscito para aprobar o rechazar la propuesta, aproximadamente 60 días después de la entrega de esta, y será con voto obligatorio.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muchos analistas y ciudadanos han señalado este suceso como histórico pues es la primera vez en la historia del país en que la Constitución será redactada por una asamblea completamente elegida por voto popular.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
