Title: La Franquicia de las Águilas Negras
Date: 2020-11-30 10:00
Author: AdminContagio
Category: Actualidad, Nacional
Tags: #AguilasNegras, #Líderes, #panfleto, #Paramilitares
Slug: la-franquicia-de-las-aguilas-negras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/refugio.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/refugiohumanitario.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alejandro-Nieto.mp3" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alejandro-Nieto-1.mp3" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/aguilas-negras-770x400-770x400-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Panfleto-ZOX-1280x720-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alberto-Yepes-Integrante-de-Coordinacion-Colombia-Europa-y-Estados-Unidos_1.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras el reciente panfleto que llegó a la sede de un medio de comunicación, en el que se amenaza a distintos políticos, líderes sociales, periodistas y medios de comunicación, entre los que se **encuentra Contagio Radio**, una vez más reaparece el fantasma de las Águilas Negras en el país, y con este, la duda referente a quién está detrás de esta firma y cuáles son sus intenciones.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El fantasma de las Águilas Negras
---------------------------------

<!-- /wp:heading -->

<!-- wp:image {"id":93351,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/aguilas-negras-770x400-770x400-1.jpg){.wp-image-93351}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En lo corrido de este año, los panfletos firmados por las Águilas Negras han aparecido en las localidades de Ciudad Bolívar, Usme y Bosa, en Bogotá; y en el municipio de Suacha, aledaño a la capital. (Le puede interesar: ["Águilas Negras amenazan a Contagio Radio y a periodistas de medios de comunicación"](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-contagio-radio-varios-periodistas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las cartas los objetivos señalados comparten algunas similitudes. Algunos pertenecen a corrientes de izquierda, denuncian violaciones a derechos humanos o realizan trabajo comunitario en los distintos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Casualidad? De acuerdo con el abogado Alberto Yepes, integrante de la Coordinación Colombia Europa Estados Unidos, estos panfletos cumplen una función de "disciplinamiento y control social, mediante el **terror a los sectores que expresan inconformidades** con las políticas públicas, con las decisiones del Estado y con las pretensiones de sectores autoritarios y conservadores".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hecho que para Alejandro Nieto, líder del Refugio Humanitario de Ciudad Bolívar, se ejemplifica con los hostigamientos del pasado 15 de noviembre. En el panfleto, distintos líderes, la mesa de víctimas e integrantes de la Alcaldía de Claudia López fueron amenazados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este refugio humanitario permanente, está conformado por 25 organizaciones que agrupan comunidades indígenas, campesinas y víctimas del conflicto armado. En el momento hay más de 3600 familias, que se encuentran ocupando el predio de Villa Gloria, en Ciudad Bolívar.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93164,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/refugio.png){.wp-image-93164}  

<figcaption>
Refugio Humanitario Ciudad Bolívar

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La iniciativa se genera en torno a la exigencia de la defensa a la vida, la vivienda digna y el territorio. Tres problemáticas que en la localidad tejen un espacio siniestro para sus habitantes; encadenados al microtráfico, el control de estructuras neoparamilitares y los desplazamientos internos ocasionados por intereses de constructoras que están causando una gentrificación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Nieto, la amenaza radica en la propuesta que se encuentra haciendo el refugio, una alternativa viable que se contrapone a la política pública distrital. Para él, en este caso las **Águilas Negras podrían ser desde las constructoras hasta las bandas criminales**. Según el líder, cualquiera de estas se vería beneficiada con el fin de un proceso que no solo cuenta con respaldo comunitario, sino que posee una evidente organización popular.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:image {"id":93165,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/refugiohumanitario-1024x1024.png){.wp-image-93165}

</figure>
<!-- /wp:image -->

<!-- wp:heading -->

¿Qué hay detrás de cada panfleto?
---------------------------------

<!-- /wp:heading -->

<!-- wp:image {"id":93352,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Panfleto-ZOX-1280x720-1-1024x576.png){.wp-image-93352}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Al ser una herramienta de control social, como lo señala Yepes, estos hostigamientos también harían parte de una estrategia contra insurgente, que tendría un brazo ilegal, con operaciones encubiertas y de forma clandestina, que según el abogado, es lo que garantiza éxito en su accionar violento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Puede constatarse cómo este grupo de Águilas Negras tiene una presencia nacional, desde la Guajira al Putumayo y desde el Amazonas hasta el Chocó. Con **capacidad de identificar quiénes son los líderes sociales**, cuáles son sus correos electrónicos, sus números telefónicos" afirma Yepes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A la fecha no se conoce algún campamento de las Águilas Negras, su cadena de mando o el funcionamiento de la estructura. Además, la Policía Nacional en cabeza del general Atehortúa, asegura la **no existencia de este grupo**, en ausencia de información sobre los delitos perpetrados.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En torno al cumplimiento de las mismas, las autoridades también señalan que hay muy pocas probabilidades de que se lleven acabo. Sin embargo, para Yepes, la acción sí se cumple, solo que de una manera más encubierta:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Águilas Negras cumple la función de identificar quiénes serán los blancos a atacar. Una vez identificados hay un segundo paso con la eliminación cuando dice ejecutado por desconocidos, actores indeterminados, sicarios... Por ejemplo, todas las amenazas que se dieron antes de las movilizaciones al mes de octubre y la estigmatización, terminaron concretándose en que ese **20 de octubre fueron asesinados dos miembros de la Colombia Humana; Gustavo Herrera y Eduardo Alarcón**".

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Gobierno y las Águilas Negras
--------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La respuesta de las autoridades usualmente son estudios de riesgo, que arrojan un esquema de seguridad conformado por botones de pánico, vehículos de seguridad y algunas veces, escoltas. Pero, para Nieto, esto sigue sin responder a la pregunta de fondo: **¿Quiénes están detrás de las amenazas?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe "La Ceguera" de la organización Somos Defensores, durante el 2019 se presentaron 347 amenazas con panfletos, de las cuales 117 fueron firmadas por las Águilas Negras. Esta cifra los convierte en los segundos responsables de hostigamientos en Colombia. El primero son las Autodefensas Gaitanistas con 157.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, y como lo señalaba el general Atehortúa, no se han registrado capturas frente a estos hechos. Razón por la cuál el nivel de impunidad frente a las amenazas es del 100% en el país. (Le puede interesar: ["La Ceguera"](https://drive.google.com/file/d/1jYXd8GjrDjOERyTOJG5gDA4A55UEqYVN/view))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este motivo, distintos procesos barriales, entre ellos el refugio humanitario, pusieron en marcha sus propias guardias; heredadas de las iniciativas de seguridad de las comunidades indígenas y cimarronas. Sin embargo continúan exigiéndole, tanto al gobierno Nacional como distrital, responder "**¿cuál es el precio que hay que pagar para conquistar la vida digna y hacer efectivos derechos humanos?**".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado, en Suacha, un panfleto de las Águilas Negras amenazó a distintos líderes sociales y políticos, entre ellos el edil José Alcides Alba. Las víctimas de la misiva denuncian que en su caso, el Alcalde Saldarriaga no ha querido activar las alertas de seguridad y estigmatiza el ejercicio de defensores de derechos humanos y sectores de oposición como la Colombia Humana y la Unión Patriótica.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Cuándo vuelan las Águilas Negras?
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La aparición de estos panfletos, también es una particularidad para tener en cuenta. Yepes manifiesta que "estas estructuras son muy funcionales en momentos en que se desborda el autoritarismo y que busca impedir la continuidad de la protesta social". Accionar que demostraría, de acuerdo con el abogado, que esta es una estructura que **se activa o desactiva dependiendo del momento político.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Moisés Cubillos, líder juvenil integrante de la Plataforma Social Usme y de la Escuela Popular de Arte Público la Quinta Porra, fue víctima de amenazas de las Águilas Negras recientemente junto a otros procesos de la localidad. Él manifiesta que hay un panorama que relaciona la insitucionalidad, los **medios de comunicación y el accionar represivo de las Águilas Negras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el líder, esos hechos se evidenciaron con las protestas en contra de la brutalidad policial del mes de septiembre, tras el asesinato de Javier Ordoñez. Durante esos días, el joven señala que medios masivos afirmaron tener información sobre procesos barriales que estaban infiltrados, reclutaban personas y las hacían movilizarse. Una situación que evidencia la estigmatización al derecho a la protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente habrían aparecido los panfletos de las Águilas Negras, en donde anunciaban una limpieza social y declaraban objetivo militar a los procesos populares de Usme. (Le puede interesar: ["Los intereses tras las Águilas Negras en Colombia"](https://archivo.contagioradio.com/los-intereses-tras-las-aguilas-negras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Al hacer acciones alternativas, comunitarias y populares, empezamos a ser transgresores de la lógica oficial. En ese entendido **nos convertimos en flanco de ataque** de las instituciones que representan al Estado, legales y no legales. Precisamente porque nosotros desde el trabajo comunitario le apostamos a la defensa de derechos humanos, a arrebatarle a los menores a la guerra en el microtráfico" afirma Cubillos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cuidar la vida, el llamado de las y los líderes sociales
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los tres líderes sociales concuerdan en una cosa**, cuidar la vida y continuar en sus procesos** es la única manera de acabar con las amenazas y frenar el accionar de los intereses empresariales e ilegales en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alcides manifiesta que en Suacha, se está generando una transformación comunitaria. Muestra de ello son las últimas elecciones en donde aumentó la participación de la Colombia Humana y la Unión Patriótica en el Consejo municipal. Asimismo asegura que continuarán en la defensa de los derechos humanos atendiendo a las distintas problemáticas sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte Moisés Cubillos, manifiesta que es un convencido de que "a través de la organización comunitaria y la movilización social se siembran afectos, y en momento en que el miedo quiere imponerse a nivel nacional que mejor que los afectos para salir abantes".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente Alejandro Nieto ratifica que el ejercicio del refugio humanitario continuará adelante hasta que obtenga la vida digna para las más de 3000 familias que se encuentran vulnerables en Ciudad Bolívar. Además afirma que el fortalecimiento del tejido social en todo el país es evidente, razón por la cuál aspira a que entre todos **"cuiden la vida".**

<!-- /wp:paragraph -->

<!-- wp:audio {"id":93342} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alejandro-Nieto-1.mp3">
</audio>
  

<figcaption>
Entrevista con Alejandro Nieto

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:audio {"id":93417} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alberto-Yepes-Integrante-de-Coordinacion-Colombia-Europa-y-Estados-Unidos_1.mp3">
</audio>
  

<figcaption>
Entrevista con Alberto Yepes

</figcaption>
</figure>
<!-- /wp:audio -->
