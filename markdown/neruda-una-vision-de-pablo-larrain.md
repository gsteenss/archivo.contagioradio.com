Title: 'Neruda' una visión de Pablo Larraín
Date: 2016-08-02 14:31
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cine chileno, Pablo Larrain, Pablo Neruda
Slug: neruda-una-vision-de-pablo-larrain
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Neruda_PabloLarrain-700x293-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Neruda Film 

###### [1 Agos 2016] 

Luego de su ovacionado paso por Cannes, el próximo 11 de agosto llegará a las salas chilenas '**Neruda'**, la película dirigida por **Pablo Larraín** que tiene como protagonistas a **Luis Gnecco** en el papel del nobel y al mexicano **Gael García Bernal** encarnando a Oscar Peluchonnea, el detective ficticio que tiene la misión de capturarlo.

Aunque la trama de la cinta no corresponde en el sentido estricto a la de un filme biográfico, 'Neruda' toca aspectos importantes en la vida del poeta de Isla Negra, como lo fue su vinculación al Partido Comunista, razón por la cual fue perseguido políticamente y tuvo que exiliarse; situación de la que parte la construcción de los personajes y la historia que se cuenta en la película.

Ambientada en 1948, la cinta recrea los días en que Neruda vivió en la clandestinidad, luego de acusar al presidente Gabriel González Videla (Alfredo Castro) de traicionar al Partido Comunista. En compañía de su esposa Delia del Carril (Mercedes Morán) escapará de la persecución de Peluchonneau (Gabriel García Bernal), mientras busca reinventarse, ser aclamado y convertirse en un símbolo.

<iframe src="https://www.youtube.com/embed/PdB_ZrjlO08" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por su película, Larraín ha recibido comentarios positivos, por parte de la crítica especializada  calificándola algunos como "el mejor trabajo de su carrera" un elogio significativo si se tiene en cuenta que "No" otra de sus producciones, ganó en 2012 en la Quincena de Realizadores de Cannes y fue candidata al Oscar por Latinoamérica y "El Club" estuvo dentro de las 10 películas pre-seleccionadas en la más reciente edición de los premios de la Academia.
