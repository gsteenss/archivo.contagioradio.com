Title: The Balde Boys en Sonidos Urbanos
Date: 2016-08-18 13:39
Category: Sonidos Urbanos
Tags: Cultura, Música
Slug: the-balde-boys-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/the-balde-boys-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En la novena sesión de **Sonidos Urbanos** estuvimos con 'The Balde Boys', banda bogotana que hace arte y magia musical con instrumentos construidos a partir de material reciclado (botellas, baldes, latas, y otros.) Conversamos sobre su trayectoria y sus proyecciones artísticas, conectándonos con risas y vibraciones sonoras, pues si algo tienen de sobra es eso, humor y sabor.

Haciendo un recorrido por el origen y los tránsitos de la banda, nos acercamos a su cotidianidad, las anécdotas que han vivido entre calles, escenarios, bares, y salones de clase; los amores; las tusas; y la expresión de indignaciones y bellas rebeldías a través de la música, asumiendo como apuesta el arte para transformar, para hacer memoria y expresar a través de sus letras el inconformismo de un país, una banda que representa voces sin censura.

Con canciones como Locomotora, El Tubo, y una de sus nuevas composiciones Doctor sin doctorado, transcurrió este encuentro con The Balde Boys en Sonidos Urbanos.

<iframe id="audio_12587930" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12587930_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
