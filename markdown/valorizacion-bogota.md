Title: ¿A quién afecta la nueva propuesta de cobro de valorización en Bogotá?
Date: 2018-11-14 15:50
Author: AdminContagio
Category: Nacional, Política
Tags: Bogotá, Concejo de Bogotá, Presupuesto, Universidad Distrital, Valorización
Slug: valorizacion-bogota
Status: published

###### [Foto: @mjsarmientoa] 

###### [14 Nov 2018] 

El cobro por valorización que se está discutiendo en el Concejo de Bogotá, con el que se busca recaudar **900 mil millones** de pesos para financiar 16 obras de infraestructura, es anti-técnico, afecta el ya asfixiado bolsillo de los bogotanos y **no tendría relevancia para la ciudad**. Sin embargo, el Proyecto no cuenta con el apoyo mayoritario del Concejo, y en caso de ser aprobado, enfrentaría la oposición de la ciudadanía.

Así lo calificó el **Concejal por el Polo Manuel Sarmiento**,  quien sostuvo que el Proyecto que se está debatiendo es completamente anti técnico, porque para hacer dicho cobro, el Estado debe evaluar como la obra valorizará los predios aledaños, pero **la Administración Distrital no hizo este ejercicio**; y tampoco tuvo en cuenta la afectación a la economía de las zonas en las cuales se aplicaría el ajuste.

Los 900 mil millones que se pretenden recaudar, serían cobrados en 3 zonas de la ciudad, **afectando a los propietarios de 382 mil predios quienes tendrían que pagar 2,5 veces más que el impuesto predial,** y se destinarían en la construcción de 16 obras entre las cuales se encuentran: 6 proyectos de ciclorutas y aceras, 5 proyectos de infraestructura vial, 3 conexiones peatonales, 1 corredor ambiental y 1 centro cultural. (Le puede interesar: ["Irregularidades y mentiras sobre el Proyecto Metro de Bogotá"](https://archivo.contagioradio.com/mentiras-metro-bogota/))

Sarmiento señaló que **estas obras son absolutamente secundarias y no van a mejorar la movilidad de la ciudad,** y puso por ejemplo el caso de los barrios Montevideo y Pensilvania (Puente Aranda), en los que se pavimentarán avenidas en mal estado, pero que no generan descongestión; agregando que se construirán ciclorutas cerca a humedales, hecho que podría generar un daño ambiental.

Adicionalmente en juicio de Sarmiento, **el Proyecto no cumple con el criterio de progresividad que deben tener los impuestos**; por lo tanto, una alternativa sería el cobro por plusvalía, que es el tributo pagado por los grandes urbanizadores de acuerdo al proyecto urbanístico que realizan. Según el Cabildante, por concepto de este impuesto la ciudad recauda 15 mil millones de pesos al año, pero dicha cifra podría superar "fácilmente" los 100 mil millones.

Aunque el proyecto llega con las fuerzas medidas al Concejo de la ciudad (Polo y Centro Democrático anunciaron que votarán contra el proyecto), Sarmiento anunció que en caso de ser aprobado sería con un margen mínimo, y **se tendría que enfrentar a la movilización ciudadana que ya se ha pronunciado contra el cobro**. (Le puede interesar: ["Frenar el cambio climático en Bogotá nos será posible con la nueva flota de transmilenio"](https://archivo.contagioradio.com/nueva-flota-de-transmi/))

> "El proyecto de obras públicas con el que la alcaldía pretende justificar la valorización, no es sobre obras nuevas sino sobre mantenimiento de obras existentes, para lo que pagamos una sobre tasa del 25% en la gasolina." Gilberto Olarte empresario de [@AcopiBtaCund](https://twitter.com/AcopiBtaCund?ref_src=twsrc%5Etfw) [@PPRadio919](https://twitter.com/PPRadio919?ref_src=twsrc%5Etfw) [pic.twitter.com/5MrEqsqW2R](https://t.co/5MrEqsqW2R)
>
> — ACOPI Bogotá Cund. (@AcopiBtaCund) [13 de noviembre de 2018](https://twitter.com/AcopiBtaCund/status/1062426459728416768?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **De los cobros por Valorización a los recortes para educación** 

Sarmiento denunció que en el Proyecto de Presupuesto presentado por Enrique Peñalosa al Concejo para el próximo año, se recortan cerca de 32 mil millones a la Universidad Distrital (UD), **pasando de tener 334.387 millones en 2018 a 302.640 en 2019**. Esto ocurre en un contexto de [marchas estudiantiles](https://archivo.contagioradio.com/al-menos-30-estudiantes-resultaron-heridos-durante-movilizacion-en-bogota/)en la que se busca la solución a la crisis que sufre la educación superior, y que en el caso de la UD, se evidencia la falta de 116 mil metros cuadrados de infraestructura para su adecuado funcionamiento.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
