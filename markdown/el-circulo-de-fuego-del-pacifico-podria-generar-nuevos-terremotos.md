Title: Círculo de fuego del pacífico podría generar nuevos terremotos
Date: 2016-04-21 13:23
Category: Nacional, Política
Tags: Chile, Cinturón de fuego del pacífico, ecuador
Slug: el-circulo-de-fuego-del-pacifico-podria-generar-nuevos-terremotos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/anillo-pacifico-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: blog.alertandote] 

###### [21 Abril 2016] 

Al igual que Ecuador, Chile y Japón, Colombia se sitúa en el  cinturón de fuego del pacífico, zona con alta actividad sísmica lo que aumenta la probabilidad de un temblor e incluso un tsunami. Los departamentos que mayor riego presentan son **Choco, Risaralda, Caldas, Quindío, Cauca, Valle del Cauca, Nariño, Putumayo, Huila, Santander y Norte de Santander**, según  informa el centro de alertas de tsunami en el Pacifico (PTWC).

Este cinturón de fuego, está situado en las costas del océano Pacifico en la que los territorios con los que limita se caracterizan por el hundimiento de placas litosferas (subducción) **en más de 20 países. Esta situación se acentúa en el 7% del territorio colombiano**,  lo que genera a su vez una constante actividad sísmica y volcánica.

Además Latinoamérica se sitúa  sobre la placa suramericana,  la cual está siendo levantada por la placa de Nazca. La primera, en la que se sitúa el continente, es menos densa, la segunda es de más alta densidad y atraviesa por la parte posterior la placa suramericana, duplicando la actividad sísmica en el continente, que llega a 1.500 sismos por año, con intensidad promedio de 5 en la escala de Ritcher. Sin embargo las dos situaciones convergen y la magnitud de los temblores puede ascender por la subducción de estas dos placas en el cinturón de Fuego en el Pacifico.

\[caption id="attachment\_23006" align="alignleft" width="334"\][![subduccion](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/subduccion.jpg){.wp-image-23006 width="334" height="198"}](https://archivo.contagioradio.com/el-circulo-de-fuego-del-pacifico-podria-generar-nuevos-terremotos/subduccion/) Fenómeno de Subducción\[/caption\]

La advertencia viene desde el **terremoto en Chile el 27 de febrero del 2010** que tuvo una intensidad de 8,8 en la escala de Ritcher, que fue seguido en Latinoamérica por Ecuador, el pasado 16 de abril, con un sismo de 7,8 en la escala de Ricther, estos eventos confirman la coherencia con las  dinámicas de la corteza terrestre, y ocurren  de forma aleatoria, siendo los límites del pacifico los más propensos a estos catástrofes naturales.

El territorio Colombiano está expuesto a un catástrofe natural similar al del  pasado 16 de abril en Ecuador, lo que genera un llamado de alerta, puesto que **estudios de la Universidad Nacional, aseguran que parte de la infraestructura colombiana no cumple con una reglamentación antisísmica**, ya que son edificaciones y construcciones antiguas.

Por este motivo, **se debe generar una preparación adecuada para los pobladores por parte de la entidad adscrita como Sistema Nacional de Gestión del Riesgo (UNGRD)**, que tiene como fin proveer a la sociedad que habita el territorio Colombiano con simulacros, revisión de infraestructura, preparación de diferentes estrategias de planeación, en si una adecuación tanto infraestructural como de acción para la prevención de catástrofes naturales, cumpliendo las funciones ya decretadas en la ley 1444 del 2011:

##### *"1.Dirigir y coordinar el Sistema Nacional para la Prevención y Atención de Desastres,- SNPAD, hacer seguimiento a su funcionamiento y efectuar para su mejora en los niveles nacional y territorial. 2. Coordinar, impulsar y fortalecer capacidades para el conocimiento del riesgo, reducción del mismo y manejo de desastres y su articulación con los procesos de desarrollo en los ambitos nacional, territorial del Sistema Nacional para la Prevención y Atención de Desastres-SNPAD. 3. Proponer y articular las políticas, estategias, planes, programas, proyectos y procedimientos nacionales de gestión del riesgo de desastres, en el marco del Sistema Nacional para la Prevención y Atención de Desastres- SNPAD y actualizar el marco normativo y los instrumentos de gestion del SNPAD. 4. Promover la articulación con otros sistemas administrativos, tales como el Sistema Nacional de Planeación, el Sistema Nacional Ambiental, el Sistema Nacional de Ciencia, Tecnología e Innovación y el Sistema Nacional de Bomberos, entre otros en los temas de su competencia. 5. Formular y coordinar la ejecución de un plan nacional para la gestión del riesgo de desastres, realizar el seguimiento y evaluación del mismo. 6. Orientar y apoyar a las entidades nacionales y territoriales en su fortalecimiento institucional para la gestión del Riesgo de desastres y asesorarlos para la inclución de la política de gestión del riesgo de desastres en los planes territoriales. 7. Promover y realizar los análisis, estudios e investigaciones en materia de su competencia. 8. Prestar el apoyo técnico, informativo y educativo que requieran los miembros del Sistema Nacional para la Prevención y Atención de Desastres -SNPAD-(articulo 18 de la ley 1444 de 2011)."* 

Por lo tanto, es de vital importancia que se responda de forma adecuada y se realicen las labores de prevención que ordena la ley para evitar una catástrofe de altas proporciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
