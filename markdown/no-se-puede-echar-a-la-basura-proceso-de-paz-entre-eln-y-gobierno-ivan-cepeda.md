Title: "No se puede echar a la basura proceso de paz entre ELN y Gobierno" Iván Cépeda
Date: 2018-01-29 15:49
Category: Nacional, Paz
Tags: ELN, Gobierno, Gustavo Bell, Mesa en Quito, Santos
Slug: no-se-puede-echar-a-la-basura-proceso-de-paz-entre-eln-y-gobierno-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telégrafo] 

###### [29 Ene 2018] 

Luego de que el presidente Juan Manuel Santos anunciara la suspensión del inicio del quinto ciclo de conversaciones de paz con la guerrilla del ELN en Quito, son múltiples las voces que siguen pidiendo que se reanuden las conversaciones. Para ello algunos sectores proponen el anuncio de medidas de **desescalamiento del conflicto, incluso un anuncio de cese unilateral por parte de esa guerrilla.**

En ese sentido diferentes organizaciones sociales y políticos del país, lamentaron la decisión del presidente Santos de suspender el quinto ciclo de conversaciones en Quito, sin embargo, afirmaron que **continúan respaldando al proceso de paz y están a la espera de que prontamente se dé luz verde en este escenario para seguir con los diálogos.**

<iframe src="https://co.ivoox.com/es/player_ek_23428715_2_1.html?data=k5ihlJ2bdZahhpywj5WbaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjN6YzsaPrc7k0Nfhw9PHrcKfxcqYz8bSuMbixteYzsaPscbnwpDRx5C1ucqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El senador Iván Cépeda afirmó que se ha hecho un gran esfuerzo de varios años en primera medida para concertar una agenda, lograr que se creara una mesa de conversaciones, comenzar a impulsar el desarrollo de esa agenda a través de la participación social y ciudadana y en segunda instancia para **lograr pactar un cese bilateral que fue provechoso durante los 4 meses en los que se estableció**.

“**Eso no se puede echar a la basura, porque tendríamos que volver a recorrer ese camino y eso lo hemos visto en Colombia infinidad de veces**: se rompen los diálogos y a los años hay que volver a sentarse a conversar” recalcó Cépeda y recordó que en 2015 hubo una situación muy similar en los diálogos de La Habana que fue superada.

Por su parte el representante a la Cámara por Bogotá, Alirio Uribe, instó al ELN a que declare un cese unilateral de hostilidades y de esta manera “ganar la confianza para que se retorne a las conversaciones”. (Le puede interesar: ["Parlamentarios de Reino Unido instan a Gobierno y ELN a retomar la mesa")](https://archivo.contagioradio.com/parlamentarios-de-reino-unido-instan-a-equipos-de-mesa-de-quito/)

Así mismo, la Red de iniciativas de Ciudadanas por la paz y contra la guerra, Redepaz expresó en un comunicado de prensa que rechaza los atentados ocurridos en los departamentos de Atlántico y Sur de Bolívar, en donde el ELN asumió la autoría, **“ponen en vilo la confianza de la sociedad en la efectividad de los diálogos de paz”**.

En esa medida exigieron que se respete y proteja la vida y por ello instaron al gobierno y al ELN a que continúen con los diálogos de paz y que no se levanten de la mesa hasta que se haya llegado a un acuerdo definitivo que ponga fin al conflicto armado.

### **Los actos de la guerra no permiten que se avance en la negociación** 

<iframe src="https://co.ivoox.com/es/player_ek_23428680_2_1.html?data=k5ihlJ2afJGhhpywj5WXaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZC6qc3Vz8nWw4qWh4zn0Mffx5DQpYzh1srf1sqPqMaflpDd0dHNp4a3lIqupsbXb8bijKfO1NeRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Frente a la muerte de 5 policías en Barranquilla, el gestor de paz, Carlos Velandia, señaló que este tipo de actos no ayudan a construir un ambiente favorable y propicio para la paz, y que por el contrario se convierten en un “patadón” a la mesa en Quito. Sin embargo, **aseguró que de manera inmediata tendría que empezarse a realizar un desescalamiento de las acciones armadas**.

“Hay que sentarse de manera inmediata a discutir y rediseñar el cese bilateral del fuego y hostilidades, pretender otro tipo de salidas no es viable para el proceso” manifestó Velandia y agregó que ante esta situación debe apelarse a la voluntad que han demostrado ambas partes, al momento político que vive el país y a **la sensibilidad que tiene la sociedad colombiana para llevar este proceso a buen puerto**.

El senador Iván Cépeda por el Polo Democrático, manifestó que el ELN debe seguir insistiendo en la declaración del cese bilateral, sobre todo por el tiempo electoral en el que se está, en dónde cualquier hecho podría generar mayor instabilidad para los diálogos.

Por su parte el ELN a través de redes sociales señaló que desde el término del Cese al Fuego han muerto 19 guerrilleros y que esas muertes también le duelen a las familias de ellos y ellas. (Le puede interesar: ["4 propuestas para destrabar la mesa ELN-Gobierno y reanudar cese bilateral"](https://archivo.contagioradio.com/4-propuestas-para-destrabar-la-mesa-eln-gobierno-y-reanudar-cese-bilateral/))

###### Reciba toda la información de Contagio Radio en [[su correo]
