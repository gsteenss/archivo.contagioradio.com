Title: Ánderson Pérez, excombatiente y comunicador fue asesinado en Cauca
Date: 2019-06-17 18:26
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Ánderson Pérez, asesinato de excombatientes, Caloto, Cauca
Slug: anderson-perez-excombatiente-y-comunicador-fue-asesinado-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ánderson.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  Personal] 

En las últimas horas se conoció del asesinato del excombatiente de FARC, **Ánderson Pérez Osorio** en el municipio de Caloto, Cauca, se desempeñaba como acompañante del proceso de reincorporación en este departamento, además destacó por su trabajo como comunicador y dirigente político.

Según lo denunciado por la **Organización Defensora de Derechos del Suroccidente Colombiano Francisco Isaías Cifuentes,** Ánderson Pérez era a su vez integrante de la Asociación de Trabajadores Pro-Constitución Zonas de Reserva Campesina de Caloto (ASTRAZONACAL). [(Lea también: Asesinan a excombatientes que esperaban proyectos productivos en Nariño y Cauca)](https://archivo.contagioradio.com/asesinan-a-excombatientes-que-aguardaban-proyectos-productivos-en-narino-y-cauca/)

Asímismo era parte de la **Comisión de Comunicaciones de la Cooperativa Multiactiva ECOMUN ** y en particular acompañaba estos procesos con proyectos productivos en los municipios de Caloto y Miranda, también era integrante de la Comisión de Juventud de FARC en este departamento y fue actor en el cortometraje Historias de Guerra.

> [\#Urgente](https://twitter.com/hashtag/Urgente?src=hash&ref_src=twsrc%5Etfw)? Otro líder asesinado en en el Cauca, se trata de Anderson Pérez Osorio, en el Municipio de Caloto. Era acompañante del proceso de reincorporación integrante de Fensuagro y [@marchapatriota](https://twitter.com/marchapatriota?ref_src=twsrc%5Etfw) ¡No más asesinatos! Garantías para la vida.[@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [pic.twitter.com/oufn0ndIU5](https://t.co/oufn0ndIU5)
>
> — Sandra Ramírez (@SandraFARC) [17 de junio de 2019](https://twitter.com/SandraFARC/status/1140753498851729410?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Hoy lloramos tu muerte querido compañero, hoy vienen a nuestra mente los buenos momentos, las sonrisas, las amarguras, las enseñanzas sobre cámaras, periodismo y fotografía, vienen a la mente las pláticas llenas de rebeldía y alegría pensando cómo transformar la realidad de este país.
>
> Hoy te despedimos con un nudo en la garganta, con nuestros ojos cargados de lágrimas y con muchos sueños inconclusos.
>
> Hoy te han asesinado David, los enemigos del pueblo te han cegado la vida, pero no se dan cuenta que detrás tuyo seguimos miles de mujeres y hombres alzando tus banderas, siguiendo tu ejemplo y decididos a seguir luchando por eso que tanto anhelaste, una Nueva Colombia en PAZ con Justicia Social.
>
> Acá seguiremos firmes, así como tu compromiso con cambiar este país, aquí seguiremos, con el corazón adolorido por tu partida pero con la cabeza en alto por haber aprendido de ti,de tu alegría, de tu camaradería y de tu compromiso con aprender para servir al pueblo.
>
> Que la tierra te sea leve compañero.  
> Tu sonrisa permanecerá con nosotros.

> **Juventud Rebelde Cauca**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
