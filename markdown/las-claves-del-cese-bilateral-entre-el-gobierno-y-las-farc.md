Title: Las claves del cese bilateral entre el gobierno y las FARC
Date: 2016-06-21 16:32
Category: Nacional, Paz
Tags: Cese Bilateral de Fuego, proceso de conversaciones de paz con las FARC, proceso de paz
Slug: las-claves-del-cese-bilateral-entre-el-gobierno-y-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Jun 2016] 

La mañana de este martes se conoció que el presidente Santos viajará en las próximas horas a la Habana para firmar el acuerdo sobre cese bilateral de fuego y otros aspectos entre la guerrilla de las FARC y el gobierno nacional. Además el presidente afirma que la posible firma de un acuerdo final sería cercano al 20 de Julio. Por su parte Timoleón Jiménez afirmó que no era muy benéfico poner fechas a los acuerdos lo cual generó polémica.

Alfredo Molano afirma que se va avanzando en un proceso que lleva mucho tiempo y que concretamente el **[cese bilateral al fuego](https://archivo.contagioradio.com/?s=cese+bilateral)**, necesita unas condiciones para que sea verificable y que además tengan el acompañamiento internacional. Para Molano **hay unas condiciones que debe tener ese cese bilateral** para que funcione de manera eficaz y concuerde con lo que se espera.

### **Zonas de Campamento** 

Estas serían sitios [específicos en las que se concentren los integrantes de la guerrilla](https://archivo.contagioradio.com/terrepaz-podria-ser-la-solucion-integral-para-el-conflicto-en-colombia/) de las FARC en zonas del país que estén cerca de los sitios de operación de los diferentes frentes para facilitar la movilidad de las personas. Estas zonas deberán estar alejadas de las **zonas de conflicto con el ELN y del riesgo del paramilitarismo** para garantizar la seguridad de los miembros de las FARC.

### **Condiciones de Seguridad** 

Además de que las zonas temporales de campamentos ([Así explican las zonas de campamento los integrantes de las FARC](https://archivo.contagioradio.com/delegacion-de-paz-farc-desmiente-que-haya-crisis-en-proceso-de-paz/)) deberán estar alejadas de sitios de conflicto con el ELN y de la presencia y control paramilitar deberán comuplirse una serie de garantías de seguridad en terreno. Una de ellas los cordones de seguridad que tendrían 3 niveles. Uno controlado por integrantes de las FARC, otro externo con presencia de la comunidad internacional y una tercera con integrantes de las FFMM o de policía.

### **Cese bilateral debe ser verificable objetivamente** 

Firmar el acuerdo no significa que se implementa de una vez sino que comienzan a aplicarse los mecanismos establecidos en el acuerdo. Es decir que después de la firma vendrá la implementación y se definiría un tiempo estimado para la concentración de las tropas de las FARC. En ese tiempo tendrán que afinarse los mecanismos de verificación y deberán llegar los delegados de la ONU y la UNASUR para garantizar la seguridad y la veracidad del cese bilateral.

<iframe id="audio_11980848" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11980848_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
