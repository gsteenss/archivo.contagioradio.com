Title: Asesinan al rector indígena Aquileo Mecheche en Río Sucio, Chocó
Date: 2019-04-13 14:27
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: Chocó, Indígenas Emberá, Líderes asesinados, Río Sucio
Slug: asesinan-aquileo-mecheche-rio-sucio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56819137_532169773857049_2647074870935945216_n-e1555165397749.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Profesor-e1555175220475.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagio Radio 

La Organización Nacional Indígena de Colombia ONIC, informó la mañana de este sábado sobre **el asesinato del líder y Rector indígena embera Aquileo Mecheche**, en el municipio de Río Sucio, Chocó.

Según las primeras informaciones que se han conocido, **Mecheche habría sido asesinado la noche del viernes por paramilitares** de las autodenominadas Autodefensas Gaitanistas de Colombia. Sobre la situación del docente, desde la ONIC y ASOREWA **habían solicitado desde noviembre a la UNP implementar medidas de protección**, así como para otros líderes en peligro.

Mecheche se desempeñaba como **rector de la Institución Educativa de Jagual, Río de Chintadó**. Desde las organizaciones indígenas alertan que los grupos paramilitares siguen confinando a las comunidades indígenas de Jagual, Marcial y Pichindé, sin que se presenten acciones efectivas por parte de las autoridades (Le puede interesar: [Asesinan a Ánderson Gómez, ingeniero aliado de la Acción Comunal en el Meta](https://archivo.contagioradio.com/asesinan-a-anderson-gomez-ingeniero-aliado-de-la-accion-comunal-en-el-meta/))

###### Reciba toda la información de Contagio Radio en [[su correo]

 
