Title: Víctimas de Rito Alejo del Río esperan toda la verdad
Date: 2017-09-28 12:34
Category: Otra Mirada, Paz
Tags: cavida, General Rito Alejo del Río, JEP, Marino López
Slug: victimas-rito-alejo-del-rio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/cavida-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Sept 2017] 

Luego de que se conocería la noticia de que el general Rito Alejo del Río fue dejado en libertad condicional para acogerse a la Jurisdicción Especial para la Paz, las comunidades que integran el proceso colectivo de Autodeterminación, Vida y Dignidad CAVIDA, manifestaron que **hay esperanza de saber la verdad** y que se inicie el proceso de reparación individual y colectiva.

El general se encontraba cumpliendo una condena de 25 años por el **asesinato del habitante del territorio colectivo de Cacarica en el Chocó, Marino López**. Las autoridades establecieron que él conocía las actividades que estaban realizando los paramilitares, con colaboración de las Fuerzas Armadas, en el cacerío de Bijao, Cacarica de donde era el líder social y no hizo nada por evitar las acciones violentas. (Le puede interesar:[" Generales Mauricio Santoyo y Rito Alejo del Río son vinculados a crimen de Jaime Garzón"](https://archivo.contagioradio.com/generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon/))

Adicional a esto, **el general comandó la operación "Génesis"** que, según la orden de operaciones, buscaba retomar las zonas donde había presencia de la guerrilla. En esta operación fueron desplazadas cerca de 5.000 personas y se cometió el asesinato de Marino López, uno de los crímenes más estremecedores de la historia. Los paramilitares jugaron fútbol con su cabeza.

Según la Comisión Intereclesial de Justicia y Paz, alias “el Alemán” quién en el marco de la Ley de Justicia y Paz, "confesó que paramilitares del bloque Élmer Cárdenas iniciaron, el 23 de febrero de 1997, una incursión en la zona con el supuesto fin de avanzar hacia el sur del Urabá chocoano y disputarle territorio a las FARC-EP". El asesinato de Marino, **" marcó el inicio de las acciones criminales** de esta incursión paramilitar conocida también como “Operación Cacarica”.

### [**¿Quién ordenó y quiénes se beneficiaron de las actividade paramilitares y militares en Cacarica?**] 

Un habitante de Cacarica afirmó que **“hoy hay una esperanza de conocer la verdad** con el hecho de que el general se haya acogido a la JEP”. Dijo que desde hace muchos años ellos y ellas han estado trabajando y luchando para esclarecer los hechos y saber realmente que pasó y porqué.

Desde hace un largo tiempo “las víctimas hemos entendido que él era el piloto del barco pero **aún no sabemos quién es el que mandaba y de quién era que el general recibía las órdenes”**. Desde CAVIDA, las comunidades manifestaron que la Jurisdicción Especial para la Paz es un mecanismo que puede servir como medida restaurativa en la medida que los victimarios cuenten toda la verdad. (Le puede interesar: "Cerca de 650 paramilitares ejercen control territorial en Cacarica")

Finalmente, las víctimas indicaron que, como parte de la pena alternativa de reparación el general **“debe darle la cara a las comunidades sin odio** y teniendo de presente que nosotros lo recibiremos como un gesto de acogimiento”. Dijeron que Rito Alejo de Rio debe reconocer los hechos que se cometieron en esta zona del país y además indicar cuáles fueron las razones para cometer el crimen de Marino López.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
