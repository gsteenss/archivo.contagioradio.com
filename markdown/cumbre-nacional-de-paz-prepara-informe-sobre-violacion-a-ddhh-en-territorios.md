Title: Cumbre Nacional de Paz prepara informe sobre violación a DDHH en territorios
Date: 2016-12-09 15:46
Category: Movilización, Nacional
Tags: Agresiones contra defensores de DDHH, Cumbre Agraria Campesina Étnica y Popular
Slug: cumbre-nacional-de-paz-prepara-informe-sobre-violacion-a-ddhh-en-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/cumbrepaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [9 Dic 2016] 

Durante el segundo día de la Cumbre por la paz, los asistentes a este escenario ya han avanzado en diferentes temas como hostigamientos y amenazas a líderes en territorios, en donde se **construyó un informe sobre violaciones a los derechos humanos que será presentando ante el gobierno Nacional** y en el que también se exige respuestas frente a quienes están detrás de estos  hechos y al cumplimiento tanto del acuerdo de paz con las FARC-EP, como los compromisos adquiridos con la Cumbre.

Y es que muchas de las personas víctimas de asesinatos y atentados hicieron parte del movimiento social y de diferentes plataformas, entre ellas Marcha Patriótica y Congreso de los Pueblos. De acuerdo con Jimmy Moreno, vocero de la Cumbre Agraria Étnica y Popular, les “**preocupa el actuar del gobierno** dado que casi la totalidad de los hechos que han ocurrido a lo largo del año, que son más de 80 asesinatos, **están en la completa impunidad**”. Le puede interesar:["Fiscalía niega asesinatos de sistemáticos contra líderes y defensores de DDHH"](https://archivo.contagioradio.com/fiscalia-niega-asesinatos-sistematicos-en-territorios/)

De igual forma las organizaciones pertenecientes a Cumbre Agraria Étnica y Popular hicieron un llamado al gobierno para que le dé mayor importancia tanto a los panfletos y amenazas que se vienen dando en el país, a la **presencia de grupos paramilitares como el actuar de las multinacionales** frente a la explotación mineroenergética y le piden que se activen las misiones de verificación de los territorios para recoger de primera fuente la información de lo que suceda.

Por otro lado, frente a la ley de amnistía y la implementación de los acuerdos de paz, Jimmy Moreno considera que  la Cumbre saldrá a respaldar estos procesos pero que debe darse con todos los sectores del conflicto armado  **“hacemos un ejercicio de llamado al gobierno para que comprenda que para poder avanzar en el proceso de paz tiene que sentarse a negociar con el ELN** y hacer ejercicios exploratorios con el EPL”. Le puede interesar: ["Llega la Cumbre Nacional de Paz"](https://archivo.contagioradio.com/llega-la-cumbre-nacional-de-paz/)

Se espera que el día de mañana se tenga la plenaria final de la Cumbre por la paz en  donde se obtenga una hoja de ruta más clara para las comunidades frente a los objetivos de las apuestas en sus territorios y los mecanismos para viabilizarlas. El cerrará con un acto simbólico a la una de la tarde en la sede de la ADE sur.

<iframe id="audio_14809416" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14809416_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
