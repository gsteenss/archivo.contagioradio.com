Title: Enrique Peñalosa: El alcalde del conflicto
Date: 2017-05-09 11:52
Category: Javier Ruiz, Opinion
Tags: Bogotá, Peñalosa, revocatoria
Slug: enriquepenalosarevocatoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cb2cbcb05372f3c0e805aeed82be2db5_1475097756_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El espectador 

###### 08 May 2017 

#### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

Es evidente la crisis y el desgobierno en Bogotá gracias a la mala administración de Enrique Peñalosa que está concentrado en unos intereses bien particulares como imponer el Transmilenio que ha sido un sistema perverso de transporte y que sigue colapsando. Ni que decir de la destrucción de la Reserva Thomas Van Der Hammen para llenarla de cemento o la corrupción de los alcaldes locales elegidos por el mismo alcalde.

Lo anterior más un sin fin de errores y desaciertos han provocado que Enrique Peñalosa sea considerado como uno de los peores alcaldes del país y eso que tiene al establecimiento, a los grandes medios de comunicación a su favor y a un grupo disfrazado de “fundación” que se dedica exclusivamente atacar la iniciativa ciudadana de hacer la revocatoria.

Al alcalde le quedan escasos defensores pero quiero que miremos otra de las facetas por las cuales el señor Peñalosa se ha ganado su desprestigio ante los ciudadanos y esa es su faceta conflictiva y de cero dialogo con los bogotanos porque el señor alcalde cuando es confrontado en debates evade y porque no le gusta ser confrontado recurre al autoritarismo, a la mentira, a la pedantería y al abuso de poder.

Enrique Peñalosa no escucha a los ciudadanos y no escucha a sus votantes más cuando la ciudad anda muy mal en muchos aspectos. El alcalde cree que sus decisiones son lo mejor para Bogotá pero no ve que esas mismas decisiones pueden perjudicar a la gran mayoría de los bogotanos que le han hecho resistencia y oposición de manera pacífica y democrática. Cuando es confrontado por quienes se han visto afectados por sus decisiones no los escucha e impone su “razón” sobre las inquietudes o argumentos de los ciudadanos como aquella vez cuando una vendedora ambulante le pidió soluciones para ellos y la dejo hablando sola. Después en un ataque de populismo la invitó a su despacho y la abrazó para que los medios le lavaran la imagen mientras el ESMAD sigue golpeando a los vendedores ambulantes en el centro de Bogotá y robándoles su sustento.

Ante la protesta ciudadana Enrique Peñalosa no le gusta el diálogo, le gusta la represión y mostrar a esos ciudadanos indignados como unos revoltosos y vagos que quieren generar desorden en la ciudad. A quienes protestan contra Transmilenio por su pésimo servicio no llegan más buses para superar el caos sino el ESMAD para golpear a los ciudadanos. Es increíble que para reprimir está alcaldía sea eficaz como aquella vez que una señora murió esperando una ambulancia y el primero que llegara al lugar fuera el ESMAD pero para reprimir a quienes protestaron por la no llegada a tiempo de la ambulancia para poder salvarle la vida.

O aquella vez que Bogotá volvía a ser escenario de ese espectáculo horrible llamado “toreo” donde Peñalosa posaba de antitaurino mientras ordenaba al ESMAD golpear a los manifestantes con la excusa de proteger a los pocos que iban a la plaza de toros y que a la vez provocaban a los manifestantes. Tan desastroso fue ese día que un miembro del ESMAD desfiguró el rostro de un joven disparando un gas lacrimógeno a quemarropa. Detestable el manejo que le dio Peñalosa a ese momento critico que tomó la decisión autoritaria de que cada fin de semana se cerrara gran parte del centro mientras duraba la temporada taurina.

Prácticamente fue militarizado el centro de la ciudad y no sirvió de mucho porque siendo en ese entonces la zona más segura de Bogotá fue activado un petardo que provocó muertos, varios heridos y daños materiales. En eso falló la gran seguridad desplegada por el alcalde. Le puede interesar: [Con movilizaciones y acciones legales defenderán revocatoria contra Peñalosa](https://archivo.contagioradio.com/mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa/).

Pero sin duda el mayor acto de autoritarismo y donde se demostró que Enrique Peñalosa no le gusta el diálogo fue cuando la policía gaseo adrede a un grupo de discapacitados que protestaban en la Plaza de Bolívar porque el alcalde les redujo el subsidio de transporte. No representaban un peligro y estaban protestando pacíficamente y con esta escena de abuso contra los ciudadanos Enrique Peñalosa es el alcalde del conflicto que en vez de escuchar o solucionar es autoritario y se impone bajo el eufemismo que él es “preparado”, un “gerente” o un “visionario”.

Estamos empezando el posconflicto y es muy grave que la capital de Colombia no sea la ciudad del posconflicto sino la ciudad del conflicto y de la polarización gracias al autoritarismo de Enrique Peñalosa.

ADENDA: Una invitación cordial quiero hacer a todos ustedes para que nos escuchen en el podcast que estoy realizando con el amigo Andres Balaguera llamado “Pos La Verda” que es un Conversatorio sobre la realidad nacional colombiana, con apuntes y conceptos sobre diversos argumentos, en busca de generar debate y puntos de vista alternativos basados en la verdad sin temas vedados. Nos pueden escuchar por SoundCloud https://soundcloud.com/user-12690773

#### [**Leer más columnas de Javier Ruiz**](https://archivo.contagioradio.com/javier-ruiz/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
