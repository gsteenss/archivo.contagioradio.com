Title: Avance paramilitar en Puerto Guzmán, Putumayo deja tres personas asesinadas
Date: 2019-09-14 17:55
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Las Perlas, Paramilitarismo, Puerto Guzmán, Putumayo
Slug: avance-paramilitar-en-puerto-guzman-putumayo-deja-tres-personas-asesinadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

La Red de DDHH del Putumayo, Piamonte Cauca y Cofanía Jardínes de Sucumbiíos Ipiales denuncia el asesinato de tres habitantes de la vereda Las Perlas, inspección Galilea en Puerto Guzmán el pasado 13 de septiembre. Los responsables fueron descritos como un grupo de cerca de 100 personas que poseían armas largas tipo fusil y vestían prendas de uso privativo de las fuerzas militares. **La comunidad pide al Estado detener el avance paramilitar en el departamento **

Según se ha logrado establecer por lugareños de la región, el grupo reunió a la población de la vereda Las Perlas a tempranas horas de la mañana del 13, presentándose como la nueva guerrilla de Sinaloa, para luego, cerca de las  6:30 de la mañana, tomar por la fuerza a un agricultor que es identificado como "el señor Jimmy", quien fue sacado por la fuerza de su casa en la misma vereda y asesinado de varios impactos de fusil. [(Le puede interesar: Reportan asesinato del campesino Alneiro Guarín en Puerto Asís, Putumayo)](https://archivo.contagioradio.com/reportan-asesinato-del-campesino-alneiro-guarin-en-puerto-asis-putumayo/)

Minutos después del  homicidio del agricultor, hombres uniformados con prendas de uso privativo, llegaron en una camioneta, llevando en el platón a un hombre identificado como 'Mono' y a su esposa Maritza a quienes procedieron a asesinar en aquel mismo lugar. [(Lea también: Asesinan a líder social de la zona de reserva campesina de la Perla Amazónica, en Putumayo)](https://archivo.contagioradio.com/asesinan-a-lider-social-de-la-zona-de-reserva-de-la-perla-amazonica-en-putumayo/)

### Casos de extorsión también se presentan en Putumayo

Según indican las organizaciones defensoras de derechos humanos del lugar, en el municipio de Puerto Caicedo, también se presentaron durante el mes de agosto llamadas telefónicas con el fin de extorsionar a "reconocidas personas de la sociedad civil", exigiéndoles pagos de altas sumas de dinero o apoyarles con la compra de prendas militares, algo que desde la red, se consideró como una forma de conformar nuevos grupos armados en el Putumayo.

La comunidad ha solicitado a las autoridades y al Gobierno garantizar la protección de los habitantes de Puerto Guzmán, en especial de quienes viven en El Cedro y Galilea, inspecciones que serían el centro de operación de este fenómeno en el municipio.  ([Le puede interesar: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo)](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
