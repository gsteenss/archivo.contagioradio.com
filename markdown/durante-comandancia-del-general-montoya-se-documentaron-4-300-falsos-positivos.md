Title: Durante comandancia del General Montoya se documentaron 4.300 falsos positivos
Date: 2016-03-29 14:43
Category: Judicial, Nacional
Tags: Antioquia, Ejecuciones Extrajudiciales, falsos positivos, Mario Montoya
Slug: durante-comandancia-del-general-montoya-se-documentaron-4-300-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/general_mario_montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Zero 

###### [29 Mar 2016] 

La Fiscalía General de la Nación imputará cargos al General (r) Mario Montoya, por su presunta responsabilidad en siete casos de ejecuciones extrajudiciales cuando fue comandante del Ejército Nacional entre los años 2006 al 2008. Sin embargo, de acuerdo con organizaciones defensoras de derechos humanos, no se trata únicamente de esos casos, sino de los más de 4.300 'falsos positivos' documentados cuando Montoya fue comandante de la Cuarta Brigada de Medellín.

"Lo que hemos sostenido las organizaciones de derechos humanos, es que  **no son solo 7 casos sino 4.300 que se han documentado cuando del 2001 al 2003 Montoya se desempeñó como máximo mando de la Cuarta Brigada, **mientras se llevaron a cabo la operaciones como Orión, Meteoro, Mariscal, Marcial, que arrojan un sin número de víctimas, que no son hechos aislados sino que son una política de Estado, encargada de convertir a las fuerzas militares en una empresa criminal”, señala Sergio Arboleda, abogado de las víctimas e integrante de la Corporación Jurídica Libertad.

De acuerdo con arboleda, **Antioquia es el departamento con mayor casos de ejecuciones extrajudiciales que representa un 22,44%** de la cifra nacional. Los casos que se le imputarán a Mario Montoya se ubican en los años 2001 a 2008, periodo en el cual se registró el mayor número de víctimas por falsos positivos, con una cifra total de 20,8%, en 31 departamentos de 32 que tiene el país.

El año pasado se le impuso medida de aseguramiento al Coronel José Angulo Duarte, al teniente Coronel Edgar Emilio Dávila Doria, al Jefe de Operaciones Diego Hernán Padillo Ospina, miembro del Batallón Pedro Nel Ospina  por sus nexos con casos de falsos positivos durante el año 2006, por lo que las organizaciones de derechos humanos ven con preocupación que solo hasta hoy se imputen los cargos a Mario Montoya, debido a que estas denuncias  se vienen realizando desde el 2002.

A su vez, el abogado Arboleda dice que se continuará con la defensa de las víctimas y que es necesario profundizar en la **reformación de la doctrina militar para evitar que la población civil se vea como el enemigo.**

"La teoría del enemigo interno legitima las acciones en contra de la población civil, y es por eso que las organizaciones de derechos humanos instamos a que se reforme la doctrina militar, que haya una reducción de la fuero público con el fin de que haya un trato más humanos. Además es necesario darle una mayor participación a la víctimas, porque son ellas las que tienen la mayor información sobre esos casos lo que permitiría es darle una mirada diferente pero también un contexto diferente", dice el abogado.

Arboleda, asegura que para que se logre dar con todos los culpables frente a las ejecuciones extrajudiciales es necesario que se plantee un análisis de contexto pues estos casos no se dieron de forma aislada, sino como una práctica sistemática estatal.  A su vez, para el abogado, Mario Montoya no es el único culpable, sino que **"los comandantes del Ejercito Nacional tenían pleno conocimiento y promovían esas prácticas dentro de sus filas".**

<iframe src="http://co.ivoox.com/es/player_ej_10971785_2_1.html?data=kpWmmZabfJahhpywj5aZaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncaXp08bb1sqPp9DhwtPRw9PHrcKfxcrZjazJssbmwtGYr9TSuNDtwpDgx5DIs8Tpzsrb1sbWs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
