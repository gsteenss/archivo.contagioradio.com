Title: Colombianos y chilenos invitan a conversar sobre el Plebiscito de Paz
Date: 2016-09-27 13:08
Category: eventos, Paz
Tags: Antofagasta, Chile, colombia, paz, Plebiscito
Slug: colombianos-y-chilenos-invitan-a-conversar-sobre-el-plebiscito-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Thrutmedia 

##### [27 Sep 2016]

**Antofagasta es una de las ciudades que recibe la mayor cantidad de inmigrantes colombianos** en Chile y en latinoamerica; 95% de los cuales provienen de regiones de marcadas por el conflicto armado y en su mayoria llegan en condición de desplazados. Con ese marco se abrirá un espacio pedagógico y de sensibilización entorno al **Plebiscito por la Paz** a realizarse el próximo 2 de octubre en Colombia.

La propuesta concebida por un grupo de colombianos y chilenos, conscientes de la importancia de conocer los procesos políticos e históricos regionales, busca conversar a partir de preguntas como **¿Cuál es la importancia de la voz y el voto de personas migrantes en el proceso del plebiscito en Colombia?, ¿Qué relevancia tiene el plebiscito de Colombia en Chile y en Latinoamérica?, ¿Por qué es importante conversar sobre el papel de Chile como país garante del proceso?**

Además del conversatorio "[Hablar es gratis, callar también](https://www.facebook.com/events/644802822347577/)", durante la jornada se proyectarán **4 videos informativos** entorno al proceso político y la necesidad de participar en el plebiscito, y se disfrutarán unas **onces chilenas a ritmo colombiano** con Sonidos tropicales emergentes. Todas las actividades se realizarán el próximo viernes 30 de septiembre a las 19:00 horas en Patio Remolino, Eduardo Orchard 1643.

![14368853\_10155247563310898\_5869406900988456778\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14368853_10155247563310898_5869406900988456778_n.jpg){.size-full .wp-image-29874 .aligncenter width="960" height="667"}
