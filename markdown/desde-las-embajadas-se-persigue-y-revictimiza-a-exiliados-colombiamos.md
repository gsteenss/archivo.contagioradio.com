Title: Es incierto el número de víctimas colombianas exiliadas
Date: 2015-06-22 13:10
Category: DDHH, Entrevistas
Tags: 20 de junio, alvaro uribe velez, das, Desde el Exilio y la Migración, el retorno y la Paz de Colombia, Exiliados colombiano, II Foro Internacional de Víctimas en el país Vasco, la reparación, Operación la Europa, País Vasco, proceso de paz, terrorismo de Estado
Slug: desde-las-embajadas-se-persigue-y-revictimiza-a-exiliados-colombiamos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-22-a-las-17.06.43.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: forointernacionalvictimas 

<iframe src="http://www.ivoox.com/player_ek_4673367_2_1.html?data=lZuklZiae46ZmKiak56Jd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnxcqYzsbXb8bhw8bXw8nFt4znxpDdx9fXrcjpxpDmjdfJusrX1c7ay9%2FFb8Kfxt3Wzs7FqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Calixto, exiliado ] 

###### [22 de junio 2015]

El pasado 20 de junio se llevó a cabo el **II Foro Internacional de Víctimas en el País Vasco,** donde se convocó aproximadamente a 50 personas, entre ellos colombianos y colombianas exiliados y autoridades locales, donde se llegó a la conclusión de la necesidad de caracterizar a la población colombiana que se encuentra en ese país, y así mismo, implementar propuestas de reparación de las víctimas del conflicto colombiano que se han visto obligadas a salir del país.

**“Desde el Exilio y la Migración, la reparación, el retorno y la Paz de Colombia”**, era el eslogan del foro. Allí, se habló sobre el tipo de reparación que requieren las víctimas del exilio,  teniendo en cuenta que esta es una forma de llenar el vacío del exilio, que a su vez debe ir acompañada de justicia real, como dice Mario Calixto, colombiano exiliado.

En este foro se consolidó como objetivo, contabilizar a las personas exiliadas, a partir de las bases de datos de los consulados. Sin embargo, Calixto, denuncia que **las embajadas no han facilitado la cifra exacta de colombianos y colombianas que se encuentran en situación de exilio,** y asegura que si bien, se logra conseguir ese registro único de victimas, se espera que no sirva para que se persiga a los exiliados y se les termine revictimizando.

Por ejemplo, Mario resalta cómo ya se han visto **perseguidos y revictimizados, durante el gobierno del presidente Álvaro Uribe Vélez,** a través de la operación La Europa del DAS, con la que se logró realizar un ataque cibernético a la página web, donde los exiliados colombianos tenían un programa radial desde hace 13 años, además les robaron su información.

En ese contexto, se determinó iniciar la campaña del **"20 de junio"**, con el objetivo de solicitar la información sobre la cantidad y la caracterización de las personas exiliadas, teniendo en cuenta que actualmente, no existe un censo sobre este tipo de colombianos, no hay claridad sobre la aplicación de la Ley 1448 en relación con los refugiados y tampoco una campaña de divulgación ni información en los consulados para las víctimas del exilio, como lo afirma Mario Calixto.

Finalmente, se concluyó que se espera que en el marco del Proceso de paz, las víctimas realmente sean el centro de los acuerdos, ya que no podrá haber reparación si el gobierno pretende hacer “borrón y cuenta nueva”.
