Title: Avanzar en lo nuevo, no hay más
Date: 2017-03-21 09:37
Category: Camilo, Opinion
Tags: Asesinatos ambientalistas, Frente Nacional, JEP, paz
Slug: avanzar-en-lo-nuevo-no-hay-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por[Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 21 Mar 2017 

Ni muchos de los que legislan con el variopinto frente nacional llamado hoy partidos como la U, Centro Democrático, Cambio Radical, liberal y conservador, tienen disposición al cambio; ni muchos de los que dicen representar los intereses de los militares, de los que están procesados y o privados de la libertad con sentencia, quieren asumir el momento histórico, y por supuesto eclesiásticos de las más diversas tendencias, los terroristas de las FARC y el ELN, son un pretexto que se les puede ir agotando. Allá, ellos es su problema, el nuestro es otro.

Ahí están pintados, de tiempo atrás, los mismos apellidos, los mismos poderosos políticos; las mismas empresas legales e ilegales con cambios de fachadas, siempre con la misma actitud. Ayer fueron los Pablo Escobar, los Rodríguez Orejuela, hoy los Odebrecht. En el siglo XX se evidenció que la política degeneró en negocio, en clientelas de arriba y de abajo, la ideología del interés particular con los bienes públicos, desde el puesto prometido a necesitados ciudadanos hasta la licitación de millones de millones de ricos negociantes

Ahí también, se reflejan los militares que a nombre de un vano honor militar arrastran tras de sí un estela de odios infundados, o dinámicas jerárquicas que además de crímenes inconfesables a nombre de la democracia, hieden a negocios y corrupción, porque son una institución que mata el alma. Estas instituciones están en metástasis, imposibilitadas de permearse del momento histórico.

También, los cómodos clérigos que bendicen con sus manos, con agua bendita, con retóricas de terror, de infierno y apocalipsis, llenando sus arcas mundanas, que absuelven a los corruptos y a los violadores de derechos humanos, que  hablan de la paz del cielo incentivando el fuego de las mil y unas guerras en la tierra; guerras de odios, guerra santas para proteger riquezas mal habidas, para guardar a políticos y criminales, a misógenos, a violadores de niños, del infierno, para sostener su poder desleal a la libertad que proclamó, el que dicen que los inspiró.

Ni los Santos, ni los Uribe, ni los Samper, ni los Gavirias, ni los Vargas Lleras, ni los Moreno, ni los Galán, ni los Ardila Lulle, ni los Santo Domingo, ni los Sarmiento Angulo, con Obispo de familia, están en total disposición, todos ciegos del amor a sus propios intereses, particulares, específicos, combinados conforme a lo que les sea funcional para salvar sus instituciones.

Ya se preveía. Creo que padecemos de la inocencia profunda. Aquella que cree que esos sectores políticos, esos sectores de empresarios, esos militares, esos eclesiásticos de familias con apellidos de sangre azul, incluso, esos sectores de iglesias cristianas, tienen la generosidad de ponerse en el lugar del otro, más allá de sus marcos ideológicos y doctrinales, y de comprender que el poder puede ejercerse en función del bien común.

Padecemos de inocencia. La inaceptable de creer que ellos, Ellos son Poderosos, como lo expresó tan profundamente William Ospina, en ese poema, están dispuestos a un mínimo de generosidad en la verdad. Inocencia al hacer tolerable la corrupción como un mal menor de nuestra identidad. Parodiando a Marx, si Santos celebra con Herencia de Timbiquí el matrimonio de su hija, con Obispo castrense abordo, con el respeto debido a esa creativa banda; los de abajo lo hacen con Kevin Roldan o Maluma

Así es. Desde el frente nacional se institucionalizó la corrupción en la política, y la economía privada  en los pagos para hacerse a una licitación. En las propias fuerzas militares y policiales, muchas de los que hoy viven del pago de cuotas narcas y neoparamilitares. O los de ayer, que crearon sus propios grupos extorsivos y de secuestro, como los del famoso General responsable del asesinato de Jaime Garzón, Eduardo Umaña, Mario y Elsa, mientras otros guerreaban. O los que controlan subrepticiamente las miles de calles del Bronx en Colombia. O los que se benefician de la famosa Piscina, en el centro de Bogotá. Allí,  en donde niñas de universidades privadas están para el mejor postor de la noche, en donde la droga de todo tipo circula en una zona protegida para un alto ex general de la policía.

Como cantamos, la pobre inocencia de la gente, la propia, las de las organizaciones que creen que la vida cambia por un Acuerdo, por un decreto, por una ley.

Se creyó que la Jurisdicción Especial de Paz, pasaría sin modificaciones en el congreso, pero es que la JEP, la hace real es la gente organizada, el ejercicio real y concreto de afirmar unos derechos.

Acaso, el congreso conformado por sectores que prefieren el silencio cómodo al riesgo de la verdad, que tienen compromisos de protección de empresarios o de sus propias campañas que tienen algo de sangre, iban a aceptar la JEP, tal como se diseñó. Simplemente, no. La arquitectura novedosa de la JEP, con sus imperfecciones, se sostiene sobre la verdad, y a la verdad social le temen. Sectores políticos, militares, eclesiásticos se deben en una falsa lealtad a quienes los han financiado o porque ellos mismos, se han hecho a injustas riquezas con la violencia y la corrupción, esa es su verdad que no resiste un escrutinio público y ético.

Y que esperar de los militares en libertad y aquellos que se representan en ACORE, que desde noviembre sustrajeron del Acuerdo Final, el artículo 28 de la competencia de la Corte Penal Internacional, CPI, para proteger supuestamente a militares condenados. Su acto atrajo mucho más  a la comunidad internacional, que sorprendida vio con la anuencia, del debilitado presidente, tan estúpida actuación. La mira de la CPI continúa en los militares. Pero los que creen, siguieron creyendo que ahí paraba la cosa, y era el punto final, pues ya vimos en estos días, que no era así. Vale estar sorprendidos dada la inocencia, pero es un tiempo, otro tiempo de prepararnos en lo nuevo.

Las fachadas se desmoronan de todas y todos lo que se identifican con ese establecimiento, unos y otros se sacan los cueros, todos se quieren salvar. Poco servirá esta circunstancia si se debilita la JEP, en insondables e inmamables críticas de vanidad jurídica, desde lo que creemos en otro país, el mundo es más qué el derecho. Si desconectamos el escandaloso espectáculo de la privatización de lo público en la política de los Acuerdos del Teatro Colón y de lo que se pueda lograr en la mesa de Quito: Estamos más lejos que cerca, de ese momento en que el alma de la gente empieza a percibir que ellos son más de lo mismo, si seguimos encerrados en nuestro inveterado ejercicio de hablarnos a nosotros mismos, de ser el ombligo del mundo, mientras los empobrecidos, las clases medias,  los ricos, los militares y ex militares, los sectores creyentes, los ambientalistas y demás de a pie,  hastiados de tanta vergonzosa insanidad,  esperan con otro discurso y apuesta ética, encontrar un sentido de esperanza en un proyecto de nuevo país.

#### [**Leer más columnas de opinión de  Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
