Title: Líneas de transmisión de Hidroituango podrían afectar la salud: Comunidades
Date: 2017-04-07 09:12
Category: Ambiente, Nacional
Tags: ANLA, Hidroituango
Slug: comunidades-afectadas-lineas-transmision-hidroituango-denuncian-irregularidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Ríos_Vivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ríos vivos 

###### [7 Abr 2017] 

Los municipios afectados por las actuaciones de EPM en el marco del desarrollo del proyecto hidroeléctrico Hidroituango, ahora denuncian las posibles afectaciones que puede generar la puesta en marcha, del proyecto de líneas de transmisión por parte de la empresa INTERCOLOMBIA.

De acuerdo con el Movimiento Ríos Vivos Antioquia se han constatado una serie irregularidades frente a este proyecto. La población denuncia que no han sido informados sobre las afectaciones que implicarían las líneas de transmisión, pues aseguran que los funcionarios de la empresa,  **han dicho a las comunidades que no se requiere licencia ambiental pues es la misma de Hidroituango**.

Las principales preocupaciones de los pobladores se centran en los **efectos en la salud de la radiación electromagnética producida por las redes de transmisión y  los transformadores de alta tensión.** Pese a ello, ni la ANLA, ni el Ministerio de Salud y Protección Social han dado respuesta sobre esas preocupaciones de los pobladores.

Las organizaciones han señalado que según estudios de la Agencia Internacional de Investigación sobre el Cáncer o el Instituto Nacional de las ciencias de Salud Ambiental de los Estados Unidos y la OMS, alertan sobre la posible relación entre leucemia linfoblástica aguda, linfomas y tumores cerebrales en niños que viven cerca de instalaciones de alto voltaje.

A su vez preocupa las consecuencias que pueda haber sobre el ecosistema, ya que los terrenos donde se piensa poner las líneas de transmisión, son **corredores naturales utilizados por las aves migratorias** y por otras **poblaciones de aves propias de la región como las guacamayas.**

Ante los riesgos ambientales y de salud, las comunidades han expresado su oposición a que las líneas de transmisión pasen por sus territorios, sin embargo en las socializaciones de la empresa se denuncia que los funcionarios de **ISA e INTERCOLOMBIA** se han negado a dejar consignada esta oposición en las actas de las reuniones con los pobladores.

Frente a la situación Ríos Vivos  solicita respuestas al Ministerio de Ambiente,  la ANLA, CORANTIOQUIA, CORPOURABA y demás dependencias del Ministerio del Medio Ambiente, y piden que se fortalezca la labor que debe llevar a cabo la ANLA y las Corporaciones Ambientales  Regionales, teniendo en cuenta que megaproyectos como los de **Hidroituango,  el Quimbo o Hidrosogamoso, que se encuentran sobre fallas geológicas podrían repetir que se produzcan tragedias como la de Mocoa.**

<iframe id="audio_18023943" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18023943_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
