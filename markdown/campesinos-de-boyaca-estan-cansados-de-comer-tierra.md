Title: Campesinos de Boyacá están cansados de "comer tierra"
Date: 2016-01-25 14:51
Category: Movilización, Nacional
Tags: Argos, Aserías Paz del Río, bloqueo en Sogamoso, explotación minera en Boyacá, Holcim
Slug: campesinos-de-boyaca-estan-cansados-de-comer-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Bloqueo-en-Sogamoso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pais.] 

<iframe src="http://www.ivoox.com/player_ek_10194138_2_1.html?data=kpWem5mVd5mhhpywj5WdaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncaTVztXS1c7Ss9SfxcqYpNTdpcSZpJiSo5aPqdTohqigh6aVsozXwtPgw8nTt4zYxpCSlJfHs87Z05Dhy8rWtsKZk5eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Aida Avella, U.P.] 

###### [25 Ene 2016. ] 

[Inconformes por los **impactos negativos de la explotación minera** en sus territorios, comunidades campesinas de Boyacá decidieron bloquear algunas vías del departamento, aseguran que la extracción de carbón en municipios como Sogamoso **ha destruido casas y secado todas las fuentes hídricas**, y en Nobsa el procesamiento de residuos tóxicos por parte de Holcim, está generando **enfermedades asociadas al cáncer que han provocado muertes de mujeres y niños**.]

[De acuerdo con Aida Avella, presidenta de la ‘Unión Patriótica’, los pobladores denuncian que la polución generada por la industria del hierro y el acero en Sogamoso está **provocando cáncer de pulmón, silicosis y algunos tipos de cáncer en la piel** que para los médicos son desconocidos. En municipios como Nobsa o Tameza “la gente come tierra, porque **como hay tanto polvillo de la explotación minera, en los platos cae la tierra**, uno entra a un restaurante y come papas con tierra, arroz con tierra, carne con tierra, esto es una cosa insoportable”, asevera Avella y agrega que **en los niños el asma es cada vez más recurrente **y muchos de ellos están desarrollando **cáncer de pulmón y de garganta**.]

[La explotación minera en el departamento de Boyacá corre por cuenta de empresas como Holcim, Argos, Aserias Paz del Río, Sidenal, Hornasa e Indumil que extraen materiales como carbón, hierro, acero, cemento, petróleo y esmeraldas, y frente a las cuales las **comunidades manifiestan su voz de denuncia y exigencia de respeto**, pues como afirma Abella “se cansaron de quedarse sin agua y sin empleo”, **se cansaron de la legalidad que promueve el Gobierno nacional, que los deja sin agua, sin tierra y sin casas**, porque “sacan toda la riqueza de Boyacá y no le dejan absolutamente nada”. ]

[Los pobladores exigen que las **autoridades locales, nacionales y departamentales se hagan presentes en la región y cierren la planta de Holcim en Nobsa**, en la que procesan residuos tóxicos que son traídos de otros países en los que han prohibido procesarlos y llaman la atención sobre las **prácticas de corrupción en Corpoboyacá**, cuyos funcionarios **otorgan títulos mineros bajo la modalidad de sobornos** y sin tener en cuenta los nefastos impactos ambientales y sociales de la actividad minera en la región. ]

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio ](http://bit.ly/1ICYhVU)] 
