Title: Minuto a minuto: Caso de jóvenes capturados por explosiones en Bogotá
Date: 2015-07-14 12:26
Category: Otra Mirada
Tags: congreso de los pueblos, Falsos Positivos Judiciales, Manuel Garzón
Slug: minuto-a-minuto-caso-de-jovenes-capturados-por-explosiones-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/pagina-falsos-positivos-judiciales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Siga el transcurso y actividades que se adelantan en el proceso jurídico contra los y las jóvenes del movimiento social, acusados y acusadas de ser responsables de explosiones en la ciudad de Bogotá.

Vea aquí las notas relacionadas con este suceso.

-   [Ver perfiles de los jóvenes capturados ](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/)
-   [Capturas a líderes del movimiento social serían "falsos positivos judiciales](https://archivo.contagioradio.com/capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales/)"
-   ["Demostraremos que los defendidos no representan un peligro para nadie"](https://archivo.contagioradio.com/demostraremos-que-los-defendidos-no-representan-un-peligro-para-nadie/)
-   ["General Palomino hace graves señalamientos contra jóvenes procesados y abogados defensores"](https://archivo.contagioradio.com/grl-palomino-hace-graves-senalamientos-contra-jovenes-procesados-y-abogados-defensores/)

------------------------------------------------------------------------

[**Julio 14 2015**]

</p>
[**7: 00 a.m**] General Rodolfo Palomino realiza declaraciones a través de medios de información contra jóvenes y abogados defensores.

**[10:30 a.m]** Por insistencia de abogados se aplazó audiencia durante una hora.

[**10: 35 a.m**] Abogados denunciarán señalamientos del General de la Policía Nacional, Rodolfo Palomino,  por falsas acusaciones contra abogados defensores de los jóvenes capturados.

**[10: 37 a.m]**"Video que muestran en los medios del capturado David Rodríguez no tiene nada que ver con los hechos que se investigan"  Manuel Garzón, abogado.  Entrevista con Contagio radio.

**[11: 30 a.m]** En la embajada de Colombia en Argentina se entrega denuncia por falsos positivos judiciales de jóvenes retenidos en Bogotá.

**[11: 35 a.m]** Se convoca a las 5 p.m en la Plaza de Bolivar en Bogotá a un plantón en apoyo a los jóvenes capturados.

**[5:00 p.m]** Desde Barranquilla, jóvenes estudiantes manifiestan su apoyo a los capturados.

------------------------------------------------------------------------

**Julio 15 2015**  
**[8:10 a.m]** Dio inicio la audiencia de amputación de cargos de la defensora de derechos humanos Paola Salgado.  
[**8:52 a.m**] La abogada Gloria Silva anuncia tras audiencia que los cargos por los que están siendo procesados y procesadas los y las jóvenes son todos diferentes. "Se confunden los hechos". Así mismo, se hacen públicos los comunicados de docentes de la Universidad Nacional en defensa y solidaridad con los detenidos y detenidas.

</p>

------------------------------------------------------------------------

   
**Julio 16 2015**  
Fueron imputados cargos a los y las detenidos y detenidas en dos grupos, tres por terrorismo, rebelión y agresión a servidores públicos.

</p>

------------------------------------------------------------------------

   
**Julio 17  2015**  
**[Alrededor de las 11:00 a.m]** concluye audiencia de imputación de cargos.  
**[12:30 p.m]** Se reúnen alrededor de 200 personas en solidaridad con todos los jóvenes detenidos y detenidas  frente los juzgados de Paloquemao.  
**[2:00 p.m]** Inicia audiencia de medida de aseguramiento.  
...Noticia en desarrollo...

</p>

