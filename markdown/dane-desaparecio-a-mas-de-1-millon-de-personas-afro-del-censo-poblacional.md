Title: DANE desapareció a más de 1 millon de personas afro del Censo Poblacional
Date: 2019-11-15 18:00
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: afro, Censo, DANE, Negras
Slug: dane-desaparecio-a-mas-de-1-millon-de-personas-afro-del-censo-poblacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Organizaciones del pueblo negro, afrocolombiano, raizal y palenquero (NARP) en Colombia denunciaron que en las útlimas cifras del censo realizado por el Departamento Administrativo Nacional de Estadísticas (DANE) se redujo en más de 1.3 millones el número de las personas identificadas en este grupo étnico. De esta forma, se pasó de 4.311.757 afros en el censo del 2.005, a 2.982.224 identificados en la medición estadística de 2.018, lo que permitiría reducir desde la asignación de recursos para atender a esta población, hasta las políticas públicas que los afectan.

### **El etnocidio estadístico**

Ariel Palacios, integrante del Consejo de Paz Afrocolombiano (CONPA), calificó como un 'etnocidio estadístico' la situación, señalando que en el censo del 2.018, el DANE realizó un operativo que no tuvo en cuenta elementos necesarios para ofrecer una estadística certera, sino una apróximación. Según explicó, en 2.005 se hizo una trabajo para que se tuviera en cuenta la etnografía nacional y se incluyera una casilla para identificar la población NARP.

Al tiempo, se trabajó con estas poblaciones para que se reconocieran étnicamente, lo que dió como resultado a 4,3 millones de personas NARP. No obstante, el último censo del DANE arrojó que 15 años después, esta población es de solo 2,9 millones de personas, un 30,8% menos. (Le puede interesar:["Día de la afrocolombianidad: una fecha para reafirmar la lucha contra el racismo"](https://archivo.contagioradio.com/dia-de-la-afrocolombianidad-una-fecha-para-reafirmar-la-lucha-contra-el-racismo/))

El efecto de este mal cálculo afectaría la agenda étnica de ciudades y políticas públicas, puesto que se reducirían las asignaciones presupuestales, al suponer que también se redujo la población NARP. Para Palacios, en un país que tiene graves problemas de distribución de las riquezas, que tienen desafíos en materia de racismo, el que se desconozca la cantidad de personas NARB es un problema que "pone grandes retos a lo que tendrá que hacer la institucionalidad colombiana para comprender que las ya diagnosticadas barreras en acceso a derechos que existen, y que se deben resolver".

### **¿Por qué falló el DANE?**

En sus estadísticas oficiales, el DANE solo censó a 44.164.417 colombianos, pero en total, seríamos 48.258.494. La diferencia, del 8,5%, correspondería a un ajuste que debió hacerse por diversos factores, lo que ha impulsado que en Antioquia, Valle del Cauca, y Cartagena se cambien las cifras de personas que las habitan. Para Palacios, estas situaciones se explican por 3 factores: No tomar en cuenta los desplazamientos forzados, el conflicto y no hacer un censo adecuado.

La Consejería para los derechos humanos y organizaciones como [a la Consultoría para los Derechos Humanos y el Desplazamiento (CODHES) advirtieron que en el país hay más de 8 millones de personas desplazadas, por lo que pidieron al DANE tener esto en cuenta en su operativo para que la población desplazada fuera efectivamente censada.  El organismo reconoció que se encontró con zonas que estaban más densamente pobladas de lo que se creía, lo que para el CONPA significa que no se tuvo en cuenta la variable del desplazamiento forzado.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Para el caso de la población NARP esto "es importante porque la población afro e indígena son los que más sufren los desplazamientos, pero al ser los afro más numerosos" se ven más afectados por esta omisión en las cifras, de acuerdo a Palacios. Adicionalmente, el líder afro denunció que los censistas debían preguntar a las personas si se identificaban como NARP, pero lo que pasó en realidad fue que "muchos censistas solo acudieron a su subjetividad pra saber si una persona es afrofescendiente o no con base en su color de piel".

Por último, el problema general señalado por el DANE que evitó tener cifras confliables para el censo fue el conflicto, puesto que hubo zonas en las que no pudieron entrar los funcionarios porque están inmersas en dinámicas de violencia o disputa territorial. (Le puede interesar: ["Comunidad afro denunciará ante la CIDH violaciones a los DD.HH."](https://archivo.contagioradio.com/afro-cidh-violaciones-dd-hh/))

### **El Censo del DANE no puede ser base para hacer políticas públicas  
**

Entre las soluciones que ha brindado el CONPA y las demás organizaciones para evitar que un mal censo afecte a la población NARP sugieren que el DANE reconozca sus fallas y "exhorte a las instituciones a no aplicar estos datos, es decir, que no se tomen desiciones con base en esas estadísticas". En segundo lugar, Palacios pidió que las ciudades y municipios hicieran apuestas para tener una dimensión de lo que significa la presencia afro en lo local, de tal forma que se tomen desiciones con base en información real.

> El crimen del DANE: El genocidio estadístico de la gente Negra, Afrocolombiano, Raizal y Palenquera en Colombia,[@DANE\_Colombia](https://twitter.com/DANE_Colombia?ref_src=twsrc%5Etfw) desaparece de forma criminal a 1.3 millones de personas de nuestro pueblo.
>
> Un atentado más por parte del Estado al cumplimento de nuestros derechos. [pic.twitter.com/ESrH7ZQu3y](https://t.co/ESrH7ZQu3y)
>
> — Anthon Manyoma (@AnthonManyoma) [November 15, 2019](https://twitter.com/AnthonManyoma/status/1195304583570055168?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44419980" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44419980_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
