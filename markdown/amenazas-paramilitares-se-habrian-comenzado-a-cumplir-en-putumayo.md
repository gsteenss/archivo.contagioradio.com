Title: Amenazas paramilitares se habrían comenzado a cumplir en Putumayo
Date: 2016-03-02 11:53
Category: DDHH, Nacional
Tags: Asamblea Departamental del Putumayo, FFMM, Paramilitarismo, policia, Putumayo
Slug: amenazas-paramilitares-se-habrian-comenzado-a-cumplir-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/luis-obando-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: justiciaypazcolombia] 

<iframe src="http://co.ivoox.com/es/player_ek_10645324_2_1.html?data=kpWjlpqXdpWhhpywj5aVaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaLhxtPO3MbXb9HV08bay9HNuMLmxtiY1cqPrMLW04qwlYqliMLijMjcz8rSvsLY0JDOjcjZsdHgyteYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Yuri Quintero, diputada Asamblea Putumayo] 

###### [02 Mar 2016] 

Según la denuncia el pasado 29 de febrero, hacia las 8:40 p.m, **Luis Obando Pantoja, un joven de 22 años de edad fue asesinado en zona urbana del municipio de Puerto Asís,** luego de haber sido amenazado por estructuras paramilitares mediante un panfleto que circulaba** **en la zona. Un niño de 12 años, sobrino del hombre terminó herido en el brazo cuando le propinaron varios impactos de bala.

Yuri Quintero, diputada de la Asamblea departamental, asegura que la policía y las FFMM presentes en una reunión de la asamblea afirmaron que los panfletos no tenían un origen de tipo paramilitar y que obedecían a una persona que los había distribuido. **Lo preocupante del hecho, es que las autoridades militares y de policía estarían desestimando las denuncias acerca de la presencia paramilitar en ese departamento, según dice Quintero.**

La diputada señala que es muy difícil pensar en que el **[departamento del Putumayo](https://archivo.contagioradio.com/?s=putumayo)** sea una zona para implementar los **planes piloto de paz** cuando el paramilitarismo estaría asumiendo el control. Se espera que este 2 de Marzo se realice una reunión en la gobernación para evaluar la situación se seguridad en la que estarán presentes las autoridades civiles, militares y de policía.

Otra de las situaciones que se tendrían que abordar en dicha reunión es la circulación de un nuevo panfleto firmado por   un grupo paramilitar autodenominado **“Comando de Limpieza Social Escuadrón de Justicia y Muerte”**, en el que aseguran que hacen presencia en los municipios y caseríos del medio y bajo Putumayo, “Mocoa, Villagarzón, Puerto Humbria. Puerto Caicedo, Santa Ana, Puerto Asís, Orito, El Tigre, La Hormiga, la Dorada San Miguel”.
