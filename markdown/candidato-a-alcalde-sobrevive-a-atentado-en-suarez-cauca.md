Title: Candidato a la alcaldía sobrevive a un atentado en Suárez, Cauca
Date: 2019-05-17 15:44
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Atentados contra líderes sociales, Cauca, Suárez
Slug: candidato-a-alcalde-sobrevive-a-atentado-en-suarez-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/César-Cerón.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Atentado.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fuerza Común] 

Organizaciones defensoras de DD.HH. denunciaron el atentado realizado en contra del líder social César Cerón el pasado 16 de mayo en ** Suárez, Cauca**, César quien se encuentra fuera de peligro es actualmente candidato a la Alcaldía de este municipio [a partir de un proceso de unidad social entre distintos sectores indígenas, afro y campesinos.]{.text_exposed_show}

[Según la denuncia de Fuerza Común, hombres que se desplazaban a bordo de una moto interceptaron a César  y lo derribaron  mientras transitaba en cercanías del corregimiento La Toma, en el lugar conocido como “El Hato”. Acto seguido, César fue intimidado con armas de fuego mientras los hombres le preguntaban constantemente **“Quién te mando a** [**aspirar?”**.]{.text_exposed_show}]{.text_exposed_show}

 

\[caption id="attachment\_67266" align="aligncenter" width="1024"\]![César Cerón](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/César-Cerón-1024x857.jpg){.wp-image-67266 .size-large width="1024" height="857"} Foto: Fuerza Común\[/caption\]

[ ]{.text_exposed_show}

[En medio del atentado César logró huir hacia un barranco mientras los sujetos armados comenzaron a disparar en su dirección, mientras el líder social logró comunicarse con la guardia Cimarrona de La Toma quienes acudieron en su ayuda.]{.text_exposed_show}[(Lea también: líder social Jorge Castrillón Gutiérrez es asesinado al sur de Córdoba)](https://archivo.contagioradio.com/lider-jorge-castrillon-gutierrez-es-asesinado-al-sur-de-cordoba/)

[Al regresar al lugar donde fue derribado, encontraron su vehículo incinerado.]{.text_exposed_show}Después de ser atendido en un centro médico César se encuentra fuera de peligro y a la espera de esclarecer los hechos del atentado para dar con los autores materiales e intelectuales.  **[César Cerón ha sido representante del Consejo Comunitario de La Meseta; es profesor y ha ejercido un liderazgo comunitario a favor de la diversidad intercultural del municipio. ]{.text_exposed_show}**

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
