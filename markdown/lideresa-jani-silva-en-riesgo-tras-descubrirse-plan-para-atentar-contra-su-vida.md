Title: Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida
Date: 2020-03-27 21:10
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Jani Silva, lideres sociales, Zona de Reserva Campesina
Slug: lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Jany-SIlva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo {#foto-archivo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la denuncia de la Comisión de Justicia y Paz, la lideresa Jani Silva estaría en grave riesgo de muerte, pues se habría develado un plan para atentar contra su vida. La lideresa ha sido amenazada de muerte junto a otros tres lideres del departamento del Putumayo que, en lo que va corrido del año ha afrontado el asesinato de al menos, ocho personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Comisión, organización de DDHH que acompaña a varias comunidades y organizaciones en el territorio nacional, el plan para atentar contra Jani Silva habría sido revelado por parte de una persona que pidió la reserva de su identidad. El mismo consistiría en atentar contra Jani Silva en medio de uno de los obligados recorridos que hace entre la Zona de Reserva Campesina (ZRCPA) y la ciudad de Puerto Asís, en Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización también resalta que "desde hace dos meses atrás existe movilidad de estructuras armadas en el territorio de la ZRCPA, y sobre el río Putumayo y sus afluentes en medio de operaciones de control de unidades de las Fuerzas Militares. Entre la significativa presencia de la Brigada XXVII de Selva y la Fuerza Naval del Sur, "continúa la movilidad de estructuras armadas irregulares." Lea también: [Denuncian plan para atentar contra lideresa](https://www.justiciaypazcolombia.com/riesgo-de-ataque-en-contra-de-lideresa-jani-silva/) Jani Silva.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No solo es Jani Silva, líderes del Putumayo están en riesgo permanente

<!-- /wp:heading -->

<!-- wp:paragraph -->

Vale la pena recordar que hace tan solo una semana se reportaba el asesinato de Marco Rivanadeira, en medio de una de las reuniones que constantemente realizaba para agilizar la mesa de organizaciones sociales del departamento, en torno a los planes para adelantar la sustitución de cultivos de uso ilícito y frenar los daños provocados por la actividad de empresas petroleras en la zona. Lea también: [Las amenazas que enfrentan los líderes del Putumayo](https://archivo.contagioradio.com/marco-rivadeneira-el-lider-que-hasta-su-ultimo-dia-defendio-la-sustitucion-voluntaria-de-cultivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma la presencia permanente y el control que realizan los grupos armados en medio de la fuerte presencia militar. Recientemente se ha denunciado que varios de los asesinatos cometidos en el departamento se llevan a cabo en los llamados "recorridos de la muerte" en los que sicarios a bordo de motocicletas asesinan a las personas cerca, o en sus propias casas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La sustitución de cultivos de uso ilícito, un factor de alto riesgo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dada la presencia de grupos armados en la región y el alto nivel de cultivos de uso ilícito en la región dado que la población no tiene otros medios de sustento, implementar el plan de sustitución se convierte en un factor de riesgo. Y es que solamente en el Sur de Córdoba, otra de las regiones críticas, han sido asesinados por lo menos [7 beneficiarios de este plan](https://archivo.contagioradio.com/tras-acuerdo-de-paz-han-sido-asesinados-7-beneficiarios-del-pnis-en-cordoba/) sin que hasta el momento se hayan adoptado las medidas de control, prevención y protección de las personas amenazadas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
