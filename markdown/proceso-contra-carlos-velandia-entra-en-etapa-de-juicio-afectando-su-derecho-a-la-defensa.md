Title: Proceso contra Carlos Velandia entra en etapa de juicio afectando su derecho a la defensa
Date: 2016-06-21 12:13
Category: Judicial, Nacional
Tags: Carlos Velandia, ELN, Proceso de conversaciones de paz
Slug: proceso-contra-carlos-velandia-entra-en-etapa-de-juicio-afectando-su-derecho-a-la-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carlos-velandia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: news.vice] 

###### [21 Jun 2016]

Carlos Velandia, ex integrante del ELN y uno de los impulsores del proceso de paz con esa guerrilla, fue detenido cuando arribaba a Colombia a través del aeropuerto internacional “El Dorado”. Según Franklin Castañeda, del Comité de Solidaridad con Presos Políticos, Velandia **no fue notificado de un proceso en su contra, ya terminó la etapa probatoria y solamente queda a la espera del proceso de juicio.**

Velandia, quien estuvo detenido durante 10 años y luego exiliado de Colombia durante otros 7 años es acusado de secuestro, hurto calificado y homicidio agravado, estos hechos serían por **un proceso que se deriva de hechos del año 2000, año en que Velandia estaba detenido**. En el caso la Fiscalía ya tiene resolución de acusación y el periodo probatorio ya pasó por lo que se estaría afectando su derecho a la defensa.

Alfredo Molano, periodista e investigador afirma que este proceso contra Carlos Velandia es **sospechoso porque podría tratarse de un hecho ya juzgado o de una** **“boleta perdida”** y agregó que esta situación preocupa puesto que ese tipo de cosas serían la muestra de que no hay garantías para los integrantes de las [guerrillas en el marco del proceso de paz.](https://archivo.contagioradio.com/eln-explica-en-que-va-el-proceso-de-conversaciones-de-paz/)

Por su parte Carlos Medina afirma que se está afectando el trabajo por la paz que desarrollan las **[personas que como Velandia han estado trabajando como facilitadores de las conversaciones de paz con la guerrilla del ELN.](https://archivo.contagioradio.com/51-congresistas-de-estados-unidos-respaldan-proceso-de-paz-con-el-eln/)**

Por parte de algunas organizaciones de Derechos Humanos se está citando a un **plantón frente a los juzgados de Paloquemao en Bogotá** para exigir respeto por el debido proceso, garantías para el derecho a la defensa y su liberación. Hasta el cierre de esta nota la defensa no había podido acceder al expediente completo.

> Con este mensaje Carlos Velandia agradece la solidaridad que han tenido tras su detención <https://t.co/6EZJdvtewq> [pic.twitter.com/TM69UcYbm0](https://t.co/TM69UcYbm0)
>
> — Contagio Radio (@Contagioradio1) [21 de junio de 2016](https://twitter.com/Contagioradio1/status/745358249310949376)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<iframe id="audio_11978313" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11978313_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
  

<iframe id="audio_11978360" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11978360_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
