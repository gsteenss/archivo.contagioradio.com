Title: Atentan contra la vida de la lideresa social Milena Quiroz
Date: 2019-12-27 14:57
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: amenazas contra líderes sociales, bolivar, cesar, Milena Quiroz
Slug: atentan-contra-vida-lideresa-social-milena-quiroz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Milena-Quiroz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular ] 

Este 27 de diciembre, en horas de la mañana, la líder social Milena Quiroz fue víctima de un atentado cuando desconocidos dispararon en varias ocasiones en contra del esquema de protección en el que se transportaba. Quiroz, junto con su escolta lograron salir ilesos del hecho.

La defensora de derechos humanos se trasladaba desde El Arenal, sur de Bolívar, hacia Aguachica, César, cuando se presentaron los acontecimientos, en dónde, de acuerdo con las primeras versiones, las personas que atacaron el esquema de seguridad se encontraban en la vía, esperando el vehículo para disparar. [(Lea también: En libertad líderes sociales del Sur de Bolívar tras 8 meses de prisión) ](https://archivo.contagioradio.com/48792-2/)

Actualmente **Quiroz es la vocera de la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar y de la Cumbre Agraria.** A su vez, es integrante del Congreso de los Pueblos y representante legal del Consejo Comunitario Afrodescendiente Casimira Olave Arincón Amela.

### La persecución en contra de Milena Quiroz 

La líder social junto a otros de sus compañeros de la organización del Sur de Bolívar, fueron víctimas de un montaje judicial el pasado 22 de marzo de 2017, tras ser acusados de incitar a la movilización social. [(Le puede interesa"Detención de Milena Quiroz es terrorismo de Estado" Defensa)](https://archivo.contagioradio.com/detencion-de-milena-quiroz-es-terrorismo-de-estado-defensa-de-quiroz/)

Posteriormente, el 7 de noviembre de 2017 el Juez Primero Penal, del circuito de Cartagena, ordenó la libertad inmediata de Milena Quiroz y 5 líderes debido a que no existió una investigación seria por parte de la Fiscalía y que los testimonios fueron contradictorios. Este caso ha sido asociado a los mal llamados “falsos positivos judiciales”.

La plataforma política, Congreso de los Pueblos, afirmó que justamente por esos hechos de persecución a los liderazgos en el Sur de Bolívar, el atentado de este 27 de diciembre debe ser atendido con urgencia y que cualquier cosa que llegue a pasar a Quiroz "es responsabilidad del gobierno Nacional de Colombia en cabeza de Iván Duque".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
