Title: En Fotos: Así fue la jornada de #Agroindignados en Bogotá
Date: 2015-09-03 11:46
Category: Movilización, Nacional
Tags: ANZORC, CNA, Cumbre Agraria, Juan Manuel Santos, marcha patriotica, Ministerio de Agricultura, Ministerio del Interior, PCN
Slug: en-fotos-asi-avanza-la-jornada-de-agroindignados-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cumbre_contagioradio-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto destacada: radiomundoreal 

\

###### [3 Sept 2015]

La Cumbre Agraria Étnica y Popular que desarrolla por estos días una jornada de \#Agroindignados en Bogotá partió desde el Coliseo el Campín a las 8 am hasta la plaza de Bolívar para realizar una Asamblea Pública en la que se informarán los avances a los que se ha llegado hasta el momento en las distintas reuniones con el gobierno nacional.

De manera paralela en el Auditorio Luis Guillermo Vélez del Congreso de la República se realiza la audiencia pública sobre la situación de las comunidades indígenas y la construcción de la paz.
