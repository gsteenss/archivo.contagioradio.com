Title: Morelia Caquetá busca consulta popular contra hidrocarburos
Date: 2017-07-19 13:51
Category: Ambiente, Nacional
Tags: Caquetá, consulta popular, hidrocarburos, Morelia, petroleo
Slug: morelia-caqueta-alista-proceso-para-consulta-popular-contra-hidrocarburos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/morelia-caqueta-e1500489977916.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [19 Jul 2017] 

En Morelia, municipio del Caquetá, habitantes buscan reunir los requerimientos que les permitan iniciar el proceso de **consulta popular contra la actividad de explotación de hidrocarburos.** La comunidad ha venido trabajando desde 2013 por crear una iniciativa popular que le dé la posibilidad a la ciudadanía de decidir sobres los procesos mineros que se llevan a cabo en su territorio.

De acuerdo con Omar Vallejo, concejal del municipio, existe un respaldo muy importante de toda la comunidad al proceso **"en este momento estamos entregando formularios para recoger las 376 firmas que necesitamos**” aspirando a recoger más de 1500 firmas que les permitan recurrir al mecanismo de participación ciudadana que ha sido implementado en otros municipios del país. (Le puede interesar: ["Tres municipios del Tolima se sumarían a consultas populares"](https://archivo.contagioradio.com/tres-municipios-del-tolima-se-sumarian-a-consultas-populares/))

El municipio tendrá un plazo de 6 meses para entregarle las firmas a la Registraduría Nacional para que allí las verifiquen. “Si como comunidad conseguimos alcanzar la meta que nos propusimos quedamos a disposición de la Registraduría para hacer la elección”. Una vez se cumpla este requisito, los morelianos entrarán a definir **“si quieren o no la explotación de hidrocarburos en el territorio”.**

**Emerald Energy hace las exploraciones de hidrocarburos en Morelia**

Según Vallejo, en Morelia **“la empresa Emerald Energy ha hecho estudios ambientales y ha trazado líneas para ver si hay petróleo en el territorio”**. Sin embargo, fue enfático en manifestar que “en ese proceso la petrolera dejó a las comunidades divididas porque vino con engaños” asegurando que la compañía “desinformó a los habitantes para que firmaran los permisos y después hubo diferencias entre las familias que firmaron”. (Le puede interesar: ["Sibaté prepara consulta popular contra la minería"](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/))

Frente a la posibilidad de que en Morelia se dejen de percibir algunos recursos que provienen de la exploración de hidrocarburos, Vallejo manifestó que “es posible que el municipio deje de percibir algunos recursos"  pero afirmó que  **van a defender el ambiente y el territorio. ** “Cuando acaben con los hidrocarburos empezarán a explotar otro renglón de la economía y poco a poco acaban con el ambiente”.

<iframe id="audio_19895343" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19895343_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
