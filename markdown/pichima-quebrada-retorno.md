Title: Sin garantías del Estado, comunidad indígena de Pichimá Quebrada regresa a su territorio
Date: 2019-11-30 21:25
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Desplazamiento, pichima quebrada
Slug: pichima-quebrada-retorno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pichimá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

Transcurridos más de cinco meses sin que el Estado haya atendido la situación de **hacinamiento, seguridad, desnutrición e insalubridad que ha vivido** la comunidad indígena de Río Pichimá Quebrada, desplazada de forma forzosa debido a la presencia de actores armados en su territorio, las 95 familias del pueblo Wounaan han tomado la decisión de abandonar **Santa Genoveva de Docordó, Chocó y** retornar a su tierra sin que existan medidas de protección por parte de la institucionalidad que garanticen su regreso.  
[  
Sobre su retorno, el profesor Guillermo Peña, había expresado con anterioridad que la comunidad decidió de forma autónoma regresar a su territorio, y que la fecha establecida sería este 30 de noviembre, sin embargo indicó que hasta ese entonces, **debido a aplazamientos de las instituciones, no se había logrado coordinar con los comités de justicia transicional** - propiciados por la administración municipal  - medidas a discutir para el retorno de esta comunidad indígena del Litoral del Bajo San Juan.]

"Esta situación ha sido bastante compleja para la comunidad, queremos retornar lo más pronto posible y para ese regreso necesitamos que por parte de la institucionalidad pueda estar presente y esa es una de las grandes fallas", manifestó. [(Lea también: Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños)](https://archivo.contagioradio.com/comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos/)

Esta ausencia de garantías y el silencio que han guardado las instituciones cuyo deber es velar por la población, se suma a la falta de atención en temas de salud, lo que generó la muerte de un menor de un año de edad durante el tiempo que han permanecido fuera de su territorio. Así mismo tampoco se dio asistencia de alimentación para la comunidad, en la que varios menores se encuentran en estado de desnutrición. ([Lea también: 50 días de desplazamiento del resguardo Pichimá Quebrada)](https://archivo.contagioradio.com/50-dias-de-desplazamiento-del-resguardo-pichima-quebrada/)

Ante estas falencias y previo a su viaje,  la comunidad de Pichima Quebrada, advirtió que **hace responsable al Estado de "cualquier hecho que ponga en riesgo la vida e integridad personal de los miembros del resguardo"**; agregando que es necesaria la presencia de la Procuraduría, la Alcaldía de el Litoral San Juan, la Defensoría del Pueblo, la Unidad Nacional de Protección y los ministerios de Interior, Salud y Educación para establecer medidas que permitan la permanencia de la comunidad en el territorio. [(Le puede interesar: Pichimá Quebrada, dos veces desterrada por la guerra)](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
