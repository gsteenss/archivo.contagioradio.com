Title: Proyecto de ley desconoce voluntad del pueblo en consultas populares
Date: 2017-07-25 14:07
Category: Ambiente, Nacional
Tags: consultas populares, Fast Track, Gobierno Nacional, medio ambiente
Slug: consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/consultas-populares-en-colombia.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/consultas-populares-en-colombia1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Consultas-populares-colombia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/consultas-populares-en-colombia2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/consultas-populares.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ambiente y Sociedad] 

###### [25 Jul 2017]

Las comunidades de diferentes territorios en el país han manifestado su **rechazo al anuncio del Gobierno Nacional que busca limitar el accionar de las consultas populares** frente a  proyectos mineros y petroleros. Algunas de ellas han manifestado que las intenciones del Gobierno son inconstitucionales y violan los derechos de los colombianos a decidir sobre su territorio.

### **El proyecto de Ley del Gobierno** 

Lo que busca el Gobierno Nacional es presentar un proyecto de ley, vía Fast Track, que **reforme la ley Orgánica de Ordenamiento Territorial (LOOT).** Con esto, podría adjudicarse la propiedad del subsuelo haciendo que las comunidades no puedan decidir sobre los proyectos mineros y petroleros que se desarrollan allí. El argumento que utiliza para justificar la realización de los proyectos es que debe primar el interés general sobre el particular. (Le puede interesar: ["Presentan firmas al presidente Santos exigiendo respetos a consultas populares"](https://archivo.contagioradio.com/presentaran-firmas-al-presidente-santos-exigiendo-respeto-a-consultas-populares/))

<iframe src="http://co.ivoox.com/es/player_ek_19993096_2_1.html?data=kp6mm5iUfZehhpywj5aVaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdfWydSPksbb08rhx5DXs8PmxpDSzpDFstbixM7cjcnJsIy70MfWx9fSs4zYxpDZy9LNuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Sin embargo, para el abogado Rodrigo Negrete, esta medida sería inconstitucional pues “este proyecto no tiene justificación en la implementación de los acuerdos de paz y por el contrario **cercena libertades, derechos y conquistas sociales**”. Además, manifestó que “lo que busca esta ley es ponerle un límite al proceso de las consultas populares y a la autonomía territorial”.

Negrete manifestó que el Gobierno “**está desesperado y ha hecho acuerdos con el sector privado para tramitar estos proyectos de ley en el Congreso**”. Además el abogado, argumentó que, con este tipo de decisiones, lo que va a conseguir el Gobierno es una reacción popular de la ciudadanía “que quiere hacer valer sus derechos”. Ante esto recordó que lo que tiene que hacer el gobierno es generar espacios para llegar a acuerdos con las comunidades “que son co propietarias del subsuelo y no puede excluir a los municipios de las decisiones que se tomen con respecto a  sus territorios”. (Le puede interesar: ["Más de 30 municipios marcharán en defensa de las consultas populares"](https://archivo.contagioradio.com/mas-de-30-municipios-del-pais-marcharan-en-defensa-de-las-consultas-populares/))

### **¿Qué dicen las comunidades?** 

<iframe src="http://co.ivoox.com/es/player_ek_19993166_2_1.html?data=kp6mm5iVepehhpywj5WbaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncavVytLSjbnTp9DmwoqfpZDdb83V1JDOyMrHuMLXytTbx9iPqMbgjNLcxsrQs4zhytPS1NSRqc_Z05KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Jaime Tocora, miembro del Comité por la Defensa de la Vida, “**estas acciones lo que evidencian es el desorden que ha sido el modelo minero en el país** que ha pasado por encima de las comunidades y de los territorios”. Tocora recordó que el Gobierno fue quien “quitó la necesidad de establecer las licencias ambientales en etapa de exploración y esto llevó a las comunidades a que se expresaran y que dijeran que este tipo de proyectos pasa por encima de los entes territoriales”.

De igual manera, Tocora manifestó que el argumento del gobierno es “absurdo” en la medida que **“el interés general es a un ambiente sano y no el de las multinacionales”**. En este mismo sentido estableció que no es verdad que los territorios vayan a recibir beneficios económicos adicionales por la realización de estos proyectos pues “están tratando de comprar a los entes territoriales”. (Le puede interesar:["Actividad petrolera ya había contaminado 49 aljibes y 58 pozos de agua en Acacias, Meta"](https://archivo.contagioradio.com/actividad_petrolera_habia_contaminado_pozos_aljibes_ecopetrol/))

Finalmente, para el ambientalista, **las comunidades van a seguir desarrollando mecanismos para que se fortalezcan procesos agroindustriales** y para hacerle ver al Gobierno nacional que “no puede imponernos un modelo que se contradice con la realidad del país a manera de dictadura”.

Del mismo modo, el abogado Negrete recordó que **las consultas populares que ya se realizaron no pierden su carácter vinculante** y  “las 40 consultas que están pendientes deben hacerse antes de que el proyecto de ley culmine por la vía Fast Track”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
