Title: Asesinado dirigente de ASODECAS Jose Yimer Cartagena
Date: 2017-01-11 18:45
Category: DDHH, Nacional
Tags: ASODECAS, marcha patriotica, paramilitares
Slug: asesinado-dirigente-de-asodecas-jose-yimer-cartagena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/jose-cartagena-marcha-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: twitter Cesar Jeréz] 

###### [11 Ene 2016]

En horas de la tarde de este miércoles se conoció el asesinato **Jose Yimer Cartagena Usuga, vicepresidente de la Asociación de Campesinos para el Desarrollo del Alto Sinú**, ASODECAS, y reconocido líder comunitario de la región.

Integrante del **Movimiento Marcha Patriótica** en ese departamento, junto a su organización había denunciado la presencia creciente de paramilitares luego del preagrupamiento de las FARC.

Según la denuncia, el campesino se dirigía hacia el municipio de Carepa en Antioquia cuando fue abordado por desconocidos que se movilizaban en una camioneta blanca, en el punto conocido como “La petrolera” a pocos minutos del casco urbano. Los ocupantes del vehículo **se lo llevaron advirtiendo a los testigos del hecho que no debían denunciar lo ocurrido**. ([Lea también: Paramilitares refuerzan su presencia en el Sur de Córdoba](https://archivo.contagioradio.com/paramilitares-refuerzan-presencia-sur-cordoba/))

En la mañana de este 11 de enero, la policía de Carepa informó a ASODECAS que en la morgue municipal se encontraba un cuerpo sin vida que podría corresponder a la identidad de José Yimer. **Posteriormente, integrantes de la Comisión de DDHH de Marcha Patriótica y familiares de la víctima identificaron el cuerpo sin vida de José Yimer,** y procedieron a realizar las denuncias respectivas. Le puede interesar: ([Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/))

Hasta el momento se desconoce la autoría de este nuevo crimen contra el dirigente social; sin embargo, los antecedentes denunciados por ASODECAS dan cuenta de la relación de este hecho con **amenazas recientes y el liderazgo que la organización ha asumido en la aplicación de proyectos productivos, así como en el avance de los acuerdos de paz**, concretamente por la cercanía de la ZVTN del corregimiento de Crucito. ([Lea también: denuncian fuerte militarización en el Sur de Córdoba](https://archivo.contagioradio.com/refugio-humanitario-del-alto-sinu-denuncia-la-militarizacion-de-su-territorio/))

Una de las situaciones que más preocupa a los líderes sociales de la región del Sur de Córdoba es que **en el mes de junio ya se habían presentado amenazas en su contra y se había alertado de la situación a las autoridades**, quienes hicieron caso omiso a las denuncias. A pesar de ello, el 23 de noviembre de 2016 el **Sistema de Alertas Tempranas emitió un S.O.S** al cual “las autoridades tampoco reaccionaron” relata el comunicado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

[DENUNCIA PUBLICA Asesinado Vice Presidente de La Asociacion Campesina Para El Desarrollo Del Alto Sinu ASOD...](https://www.scribd.com/document/336319785/DENUNCIA-PUBLICA-Asesinado-Vice-Presidente-de-La-Asociacion-Campesina-Para-El-Desarrollo-Del-Alto-Sinu-ASODECAS#from_embed "View DENUNCIA PUBLICA Asesinado Vice Presidente de La Asociacion Campesina Para El Desarrollo Del Alto Sinu ASODECAS on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_25384" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336319785/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-UVFQouFc4WUV67HNMv7T&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
