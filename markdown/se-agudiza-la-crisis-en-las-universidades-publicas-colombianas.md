Title: Se agudiza la crisis en las universidades públicas colombianas
Date: 2016-05-19 17:21
Category: Educación, Nacional
Tags: crisis universidades públicas, educación superior colombia, universidad pública colombia, Universidades
Slug: se-agudiza-la-crisis-en-las-universidades-publicas-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-universidades-publicas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [19 Mayo 2016 ] 

Universidades públicas como la Pedagógica y Tecnológica de Colombia, la de Sucre o la Distrital, están distantes en términos geográficos; sin embargo, la **desfinanciación estatal**, las elecciones antidemocráticas de sus rectores, las atribuciones de poder de los Consejos Superiores Universitarios CSU y la **precariedad en la contratación docente**, son problemáticas que las conectan y las llevan hoy por hoy a completar, por lo menos, tres semanas de paro indefinido.

De acuerdo con Juan Carlos Amador, profesor de la Universidad Distrital, la institución tiene un déficit anual de más de \$60 mil millones, una situación que no es ajena **en las universidades del Tolima y del Chocó en las que el déficit supera los \$23 mil millones**. Según el docente la desfinanciación estatal es un asunto de años atrás que hoy se agudiza y hace parte de las políticas que buscan desfinanciar a las instituciones públicas para subsidiar a las privadas que operan como nichos de mercado.

A esta desfinanciación, se suma la precarización de la contratación docente, cada día más evidente en las universidades públicas colombianas en las que **el 80% de los profesores son ocasionales o catedráticos y sólo un 20% es permanente**, en el mejor de los casos la relación es 70-30. Situación que afecta la calidad educativa, pues no se puede dedicar el mismo tiempo a la investigación.

Por sí fuera poco, las comunidades universitarias deben enfrentarse a las arbitrariedades de los Consejos Superiores en asuntos como la elección de los rectores. En la Universidad de Sucre, por ejemplo, Vicente Periñán, completa su tercer periodo de gobierno, y en las Universidades Pedagógica de Tunja y Distrital de Bogotá, los actuales rectores no fueron elegidos democráticamente. Situación común **en la mayoría de universidades en las que los rectores fueron delegados por los CSU para pagar favores políticos**.

Según asevera el profesor Amador tanto la composición como la dinámica de los Consejos Superiores Universitarios, se hereda de la aplicación de la Ley 30 de 1992, que los faculta de poderes decisorios, usados muchas veces por conveniencia institucional, lesionando la autonomía universitaria y **desconociendo las reformas estructurales propuestas por las constituyentes universitarias**, como sucede en la [[Universidad Distrital](https://archivo.contagioradio.com/?s=UNIVERSIDAD+distrital)].

Justamente para discutir la crisis estructural que enfrentan las universidades públicas, se dieron cita este miércoles en el Congreso, representantes estudiantiles, docentes, rectores, trabajadores, integrantes de los CSU, y delegados departamentales y municipales, quienes expusieron argumentos y consideraciones, de las que se espera salgan los **insumos para la reforma estructural de la Ley 30 que la Comisión Sexta del Senado estudia proponer**.

Vea también: [[La burocracia le cuesta más de \$10 mil millones anuales a la Universidad del Tolima](https://archivo.contagioradio.com/?s=u+tolima+)]

<iframe src="http://co.ivoox.com/es/player_ej_11592718_2_1.html?data=kpaim5ebdZmhhpywj5aWaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbWsNDnjKbaw8nTtoyhjKncxcrSuMaftqmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio [[en su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio ](http://bit.ly/1ICYhVU)]] 
