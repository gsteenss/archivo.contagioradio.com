Title: Prisión preventiva: ¿Mujeres víctimas de la justicia?
Date: 2019-06-19 11:07
Author: CtgAdm
Category: Expreso Libertad
Tags: carceles, mujer, presos politicos, prisión preventiva
Slug: prision-preventiva-mujeres-victimas-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/A_UNO_246756.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Uno 

Un reciente informe de la Oficina de Washington para Lationamérica (WOLA) y la organización Dejusticia, reveló el **aumento en la aplicación de la prisión preventiva sobre mujeres en Latinoamérica,** que para el caso puntual de Colombia, **desde el 2000 hasta  2018 ha aumentado en un 88%.**

En este Expreso Libertad,  los investigadores **Teresa García y Luis Cruz,** aseguraron que este incremento no solo es producto de un uso violatorio de los derechos humanos de la prisión preventiva, sino que **evidencia el fracaso de las políticas de drogas en los países del cono Sur y da cuenta de ** **las vulneraciones políticas, económicas, culturales y sociales a las que están expuestas las mujeres en este continente**.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2242226492529611%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
