Title: Covid-19 amenaza con extinguir la sabiduría indígena ancestral del Amazonas
Date: 2020-05-12 21:07
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: amazonas, Covid 19, ONIC, pandemia, pueblos indígenas
Slug: covid-19-amenaza-con-extinguir-la-sabiduria-indigena-ancestral-del-amazonas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-12-at-4.23.39-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Tejiendo Amazonas/ Ángea López

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Leticia, Amazonas, el primer caso de Covid-19 se conoció el pasado 17 de abril, un mes después, los registros han llegado a las 718 personas, un incremento del 213 %. que en tan solo un día registró 191 nuevos casos, **convirtiendo a Leticia en la cuarta ciudad con más fallecimientos en Colombia. Una región donde el 75% de su población es indígena** que viven en las cabeceras urbanas y en asentamientos rurales.  
  
Mientras seis pueblos indígenas del Amazonas han sido afectados por el virus, la [Organización Indígena Nacional de Colombia (ONIC)](https://twitter.com/ONIC_Colombia)ha advertido que son 384 comunidades de la región que están en riesgo de contagio, por su cercanía con las fronteras de Perú y Brasil señalando que las condiciones de desigualdad y la ausencia estatal son claves para entender la expansión del contagio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Leticia es una zona de triple frontera, está unida con Tabatinga en Brasil y Santa Rosa en Perú", países en los que sus respectivos Gobiernos establecieron políticas de frontera "erráticas e insuficientes para enfrentar la actual pandemia", expresa Luisa Sarmiento, presidenta de la Fundación Tejiendo Amazonas. [(Le puede interesar: Casi 6.000 familias indígenas en riesgo por COVID en sus territorios)](https://archivo.contagioradio.com/casi-6-000-familias-indigenas-en-riesgo-de-crisis-humanitaria/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Población de Amazonas no cuenta con garantías económicas ni de salud

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Luisa Sarmiento explica que hasta el momento solo hay casos confirmados en Leticia y Puerto Nariño, sin embargo, **90 de cada 10.000 habitantes del departamento han sido diagnosticados con Covid-19**, según el Instituto Nacional de Salud, una tasa muy alta pues "están muriendo los sabedores y guardianes del conocimiento cultural de los pueblos indígenas". [(Lea también: Gobierno debe dialogar con Cuba para enfrentar la pandemia)](https://archivo.contagioradio.com/gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es una población vulnerable producto de la variedad de escenarios históricos y contextos actuales", incluida la precariedad del servicio de salud y la ausencia de medicina de occidente en territorios étnicos y distantes", y aunque el virus no ha llegado a estas zonas lejanas, de llegar sería fulminante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La presidenta de Tejiendo Amazonas resalta que tampoco hay garantías económicas para las poblaciones indígenas que carecen de agua y que en ocasiones deben elegir entre el dinero para la comida o para el jabón o el alcohol con el que pueden limpiar sus manos y cuerpo. [(Le puede interesar: Ausencia estatal tiene al borde de la hambruna a líderes comunales de Tumaco)](https://archivo.contagioradio.com/ausencia-estatal-tiene-al-borde-de-la-hambruna-a-lideres-comunales-de-tumaco/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A ello se suma la ausencia de recursos de salud en Leticia, pues existen solo dos hospitales que suman 68 camas para hospitalización y solamente ocho de cuidados intermedios. **Tan solo el pasado 20 de abril se conoció la renuncia de cerca de 30 médicos y especialistas del hospital San Rafael** debido a la ausencia de garantías laborales y de equipos e instrumentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el Congreso ya son varias las bancadas que han exigido al Gobierno Nacional la adopción de medidas diferenciales y una atención especial para contrarrestar la pandemia que incluya un control fronterizo. Al respecto Luisa Sarmiento señala que estas medidas deben tener un enfoque étnico e intercultural que incluya su lengua, cosmogonía y sean acordes a la realidad de esta región del país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
