Title: ANLA otorga licencia para la expansión de mina de oro en Buriticá
Date: 2016-12-04 20:55
Category: Ambiente, Nacional
Tags: Autoridad Nacional de Licencias Ambientales, Minería de oro, multinacionales extractivistas
Slug: anla-otorga-licencia-para-la-expansion-de-mina-de-oro-en-buritica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/mina-de-oro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Campana] 

###### [4 Dic 2016] 

El pasado 30 de Noviembre la Autoridad Nacional de Licencias Ambientales otorgó el permiso ambiental a la canadiense Continental Gold para expandir su actividad en la mina de oro en Buriticá Antioquia. La mina que produciría cerca de **280 mil onzas del metal cada año, requerirá 400 millones de dólares para su construcción y tendrá una vida útil de de 14 años aproximadamente.**

El proyecto ubicado a dos horas y media de Medellín comprende un área agregada de **61,749 hectáreas** e iniciará para su expansión obras como carreteras y una planta de beneficio de oro que **extraerá unas 3.200 toneladas por día.**

Por último a través de un comunicado, Continental Gold aplaudió la resolución No. 01443 del 30 de noviembre que **“autoriza la expansión de sus actuales actividades mineras a pequeña escala a unas actividades totalmente integradas de minería y beneficio"**

Según la multinacional, “los retrasos en la aprobación de la licencia que estaba en lista de espera desde comienzos del 2016, han contribuido a la caída de las acciones en los últimos seis meses, **dado a que los inversores se alarmaron ante los riesgos de regulación colombianos”. **

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)]
