Title: Pacto nacional podría ser la oportunidad de unificar procesos de paz
Date: 2016-10-03 15:34
Category: Nacional, Paz
Slug: pacto-nacional-podria-ser-la-oportunidad-de-unificar-procesos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mesa-de-dialogos-eln-gobierno-copia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Albatv] 

###### [3 de Oct de 2016] 

Durante las últimas semanas, la guerrilla del ELN ha enviado **mensajes contundentes frente a su disposición de retomar los diálogos y las conversaciones de paz con el gobierno**. Con el panorama del plebiscito y la polarización del país, este escenario se transforma en una posibilidad de gestar un gran diálogo nacional que involucre a todos los sectores políticos y sociales del país, para construir el camino hacia la paz.

De acuerdo con el analista y docente de la Universidad Nacional, Victor de Currea, **“Es el momento del ELN**, que ha planteado primero, una convención nacional y luego un gran dialogo nacional en donde estén todos los sectores desde los campesinos, hasta los militares”.

Este espacio podría **ser una alternativa que si bien es cierto parece riesgosa**, el analista considera que el llamado pacto nacional, por lo menos tendría la ventaja de sentar en el debate al Uribismo, al que no se le deberían endosar todos los votos que se manifestaron por el No en el plebiscito por la paz, pero de todas maneras si tiene una responsabilidad en el resultado.

De igual forma De Currea afirma que ésta puede ser la [oportunidad de recoger los avances de los acuerdos entre las FARC-EP y el gobierno en un solo proceso y sumar al ELN](https://archivo.contagioradio.com/ad-portas-de-la-fase-publica-de-conversaciones-eln-gobierno/), para tener una sola mesa que responda a la pregunta de cómo establecer la paz al interior de país. **“El problema es si vamos a hacer un pacto para salvar la legitimidad de Santos o un pacto para salvar la paz**, lo que hay que rodear es la propuesta de paz, en donde debe estar el ELN y este es el momento más adecuado que legitime los debates de los cuatro años”.

Según el analista, mañana se hará una ronda preliminar gobierno - ELN, y espera que esta guerrilla tenga la [audacia política de pronunciarse esta misma semana](https://archivo.contagioradio.com/eln-decreta-cese-de-operaciones-ofensivas-para-votacion-del-plebiscito/). Sin embargo, la urgencia no debe ser salvar el destino de Santos. Para Victor de Currea no hay una premura debido a que las fuerzas armadas han sido respetuosas del cese bilateral al igual que, las FARC-EP, lo que da un compás de espera, **“el proceso de paz continúa siendo legal más no legitimo políticamente, hay que dar salidas urgentes más no precipitadas”** afirmo.

La agenda preliminar con la guerrilla del ELN se estableció el pasado 30 de marzo, sin embargo el proceso se ha visto frenado por puntos como el secuestro de algunas personas que aún tiene en su poder la guerrilla. [Diferentes organizaciones sociales han presionado y se han ofrecido para destrabar este diálogo](https://archivo.contagioradio.com/comision-etnica-se-ofrece-como-mediadora-para-reanudar-negociacion-gobierno-eln/) y generar una paz estable con todos los sectores políticos del país.

<iframe id="audio_13166025" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13166025_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
