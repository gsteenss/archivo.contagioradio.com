Title: Ultimátum del Gobierno al ELN "hace poco bien a la paz"
Date: 2016-02-03 11:19
Category: Entrevistas, Paz
Tags: ELN, proceso de paz
Slug: ultimatum-del-gobierno-al-eln-hace-poco-bien-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/ELN-e1454516275251.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimerica 

<iframe src="http://www.ivoox.com/player_ek_10302470_2_1.html?data=kpWgkpeYe5Ghhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbbg1c7ah6iXaaKl1drajcnJsIy70MfWx9fSs4zVzZCyrrOPaZOmycbQx5DUs8TjjMfWx9OPpYzgwpDdw9%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Victor de Currea Lugo] 

###### [3 Feb 2015] 

Este martes Frank Pearl, jefe negociador del Gobierno en los diálogos exploratorios con el ELN, aseguró que a esta guerrilla “se le está acabando el tiempo para hacer parte de la solución política al conflicto armado en Colombia”, refiriéndose al comunicado en el que el Comando Central del ELN aseveró que desde noviembre está a la espera de que la delegación gubernamental concrete la fecha y el lugar del próximo encuentro para **iniciar la fase pública de las conversaciones de paz**.

El analista Víctor de Currea Lugo, asegura que estas acusaciones entre las partes y el anuncio de una suerte de ultimátum**“evidencia una falta de confianza” que “hace poco bien a la paz”**, pues se ponen en riesgo los avances logrados en estos tres años de fase exploratoria entre el Gobierno y el ELN, y está en manos de la sociedad civil llamar a la apertura del proceso de paz para **evitar que la agenda de trabajo ya pactada “se bote a la basura”**.

De acuerdo con el analista es necesario que todos los **sectores de la sociedad civil presionen a las partes para que se formalice la mesa de conversaciones** con el ELN pues no es posible plantear un proceso de paz integral sin esta guerrilla, “la convocatoria a la paz debe ser con todas las instituciones que la afectan” y en esa medida se debe llamar tanto al Gobierno como al ELN a que **reconozcan los adelantos de la fase exploratoria y la culminen, dejando de lado el afán de endilgar culpas**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
