Title: El delito de estudiar sociología en la Nacho
Date: 2017-03-02 11:56
Category: Opinion
Tags: Bogotá, Capturas de jóvenes, ELN, sociologia
Slug: el-delito-de-estudiar-sociologia-en-la-nacho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Foto-artículo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por: [Mateo Córdoba Cárdenas]** 

###### 2 Mar 2017 

Cuentan que el término “chivo expiatorio” tiene su origen en rituales religiosos practicados por pueblos judíos. Eran seleccionados dos machos cabríos y, al azar, uno de ellos sería elegido para ser cargado con las culpas de todo el pueblo. Paso seguido, era enviado al desierto a morir luego de ser apedreado por la comunidad de fieles. Aquel chivo era llamado por los judíos ‘Azazel'.

Quizás cuando se es demasiado pecador hay que buscar –a como dé lugar– algún Azazel que permita, a costa de su libertad y su vida, darse cierto respiro suponiendo que las culpas ya han sido perdonadas. Y en Colombia, país de penitentes, hay un pecador que tiene tantas culpas encima, que no puede hacer otra cosa que buscar, cada que puede, un nuevo Azazel: el Estado. Y sólo hay algo peor que un Estado pecador, y es ese que, además de enviciarse con el pecado, ha escogido a una sola granja como cuna de todos y cada uno de los chivos para sus rituales de exculpación. El peligro es que esa granja se quede, tarde o temprano, sin quienes pasten sobre su terreno.

Dicha tragedia parece tener lugar en Colombia. Un Estado que tiene tanta sangre derramada en su nombre ya escogió el lugar del cual sacar, cada que lo necesite, un nuevo chivo para dar trámite a sus rituales de limpieza. Si a usted le da curiosidad saber en dónde queda ese lugar, la manera más fácil de llegar es ingresando a la Universidad Nacional de Colombia (sede Bogotá) por la entrada de la Calle 26. Después de cruzar el odioso esquema de seguridad, camine en línea recta por el sendero dispuesto y, tras cruzar el anillo vial y pasar en medio de dos edificios de los viejos, va a encontrar a su mano izquierda una construcción en ladrillo. Y, para mejorar la escena, va a encontrar a Camilo Torres pintado en la fachada. El edificio es el 205, se llama “Orlando Fals Borda”, y es ese, el departamento de sociología, el lugar que ha elegido el Estado colombiano como ‘criadero’ de los chivos que usará para limpiar sus culpas. (Ver: [Exigen libertad de Mateo Gutiérrez a través de Change.org](https://archivo.contagioradio.com/libertad-mateo-gutierrez-change-org/))

Hace unos años, el gobierno Santos decidió entrar en un mordaz juego con el Centro Democrático. Como la guerra no se ha acabado, a veces la muerte sigue ensuciando el panorama nacional, y como el cinismo no tiene límites, el Centro Democrático usa políticamente esas muertes para atacar al gobierno. Pero aún peor, como la arrogancia ha sido marca registrada en la lucha de élites a la que asistimos, el gobierno responde al cinismo uribista con falsos positivos judiciales, todo para aparentar la contundencia contra el crimen que le permita erguirse en la disputa contra los nostálgicos de la guerra.

Pero dicha fijación del Estado colombiano con el departamento de sociología de la Universidad Nacional no es producto del azar, hay una doble intención tras las detenciones y la criminalización de quienes estudiamos allí; además de responder mediáticamente a las acciones de guerra en la ciudad, el Estado busca acallar al pensamiento crítico que lleva floreciendo en ese edificio de la Nacho hace más de 50 años. Por eso mismo la amenaza paramilitar sigue golpeando la vida de varios estudiantes de sociología, aún cuando el gobierno diga que dicha cosa de los paramilitares ya no existe en Colombia.

A Mateo Gutiérrez, estudiante detenido cuatro días después del atentado del ELN contra el ESMAD el pasado 19 de febrero en La Macarena, le está costando la libertad el hecho de comprender y soñar un país distinto desde las aulas de sociología. Así como al profesor Miguel Ángel Beltrán también le fue arrebatada la libertad por enseñar, también en sociología, la historia del conflicto armado sin los lugares comunes del relato oficial (contrainsurgente) respecto a la guerra. A Mateo la Fiscalía le puso el mismo alias que me pondría a mí, pues, carentes de pruebas, usan nuestro nombre de pila como parte del montaje. Así mismo, desesperada por responder a la necedad mediática, la Fiscalía usó a mi compañero de aulas, y su detención me hace pensar, una vez más, que con la decisión de estudiar sociología asumí la condena de la persecución y la criminalización por parte del Estado.

Así las cosas, como sociólogos y sociólogas en formación, seguiremos incansables en la tarea de construir una Colombia en paz. Que el desescalamiento de la confrontación se consolide porque, con este Estado hecho para la guerra, parece que cualquier ofensiva insurgente se convierte en argumento para encarcelarnos a nosotros o a nuestros profesores. Que cese el fuego, que cese la persecución y que nos devuelvan a Mateo.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
