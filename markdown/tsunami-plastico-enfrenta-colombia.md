Title: ¿Que hacer frente al Tsunami de plástico que amenaza a Colombia?
Date: 2018-12-13 15:47
Author: AdminContagio
Category: Voces de la Tierra
Tags: Ambiente, greenpeace, plásticos
Slug: tsunami-plastico-enfrenta-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/DuJdeELXQAAAzvR-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Greenpeace Colombia 

###### 29 Nov 2018 

Ante el "Tsunami de plástico" que viene amenazando Colombia, organizaciones como Greenpeace vienen adelantando **acciones para visibilizar el impacto que este tipo de compuestos tiene sobre los ecosistemas, la fauna y sobre los mismos humanos.** Iniciativas como la campaña **"Mejor sin plástico"** y la exigencia al Ministro de Ambiente para que tome medidas contundentes que ayuden a mitigarlo.

En esta edición de Voces de la Tierra, **Silvia Gómez,** Directora de la organización ambiental para Colombia, y **Martha Vallejo** bióloga e investigadora en el manejo de basura marina,en el laboratorio de  ecosistemas estratégicos de la Universidad Javeriana, nos acompañaron para hablar de la grave situación a la que se enfrenta el país y las alternativas que existen para afrontarla.

<iframe id="audio_30769179" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30769179_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
