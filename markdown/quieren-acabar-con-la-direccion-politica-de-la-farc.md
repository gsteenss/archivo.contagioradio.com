Title: "Quieren acabar con la dirección política del partido": FARC
Date: 2018-04-10 13:15
Category: Nacional, Paz
Tags: FARC, Jesús Santrich, Juan Manuel Santos, Nestor Humberto Martínez
Slug: quieren-acabar-con-la-direccion-politica-de-la-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farc-2-e1518197319974.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Abr 2018] 

El partido político Fuerza Alternativa Revolucionaria del Común afirmó, a través de su rueda de prensa que la detención contra Jesús Santrich es "un plan orquestado por el Gobierno de los Estados Unidos con el concurso de la Fiscalía colombiana". El objetivo, según la FARC, sería acabar con la **dirección política de este partido, evitar que ejerzan su derecho a la participación política e impedir la construcción de paz en el país.**

Para el partido, este acto es un pretexto que busca afianzar la política de la "guerra contra las drogas" que ha fracasado en Colombia, y la mejor forma de hacerlo, fue a partir de apuntar a la prolongación de las presuntas actividades delictivas de la organización. (Le puede interesar. "[Captura de Santrich es un montaje: Iván Marquez"](https://archivo.contagioradio.com/captura-de-jesus-santrich-es-un-montaje-ivan-marquez/))

El partido político también aseguró que el Gobierno colombiano está enviando un  “pésimo mensaje de incumplimiento a los excombatientes que se encuentran en los Espacios Territoriales de Capacitación y Reincorporación”. En ese sentido enviaron un mensaje de calma a sus militantes y señalaron que es **"Es indiscutible que se pretende forzar la desbandada del proceso para justificar la continuidad de la violencia"**.

Frente a esta situación la FARC, pidió un total acompañamiento de  los países garantes: Cuba y Noruega, a la Segunda Misión de Naciones Unidas, a la Unión Europea y en general a toda la comunidad internacional para que **no se deje de lado el acompañamiento a los Acuerdos de Paz y en particular a esta situación.**

[**El proceso judicial de Santrich **]

La Presidenta de la Jurisdicción Especial para la Paz (JEP) Patricia Linares, afirmó   que  la JEP  revisará el caso para determinar si este proceso debe ser investigado por la JEP o por la justicia ordinaria. A su vez, en cabeza de la JEP exigió a **“las instituciones del Estado evaluar los acontecimientos** actuales con el mayor discernimiento, teniendo en cuenta que las decisiones que se tomen tendrán consecuencias profundas para el proceso de paz de Colombia.”

En ese mismo sentido, La Misión de Verificación de las Naciones Unidas en Colombia hizo un llamado para que se evalúe y haga cumplir lo señalado en el artículo Artículo 19 transitorio del Acto Legislativo 01 de 2017 donde se estable que “Cuando se alegue, respecto de un integrante de las FARC-EP o de una persona acusada de ser integrante de dicha organización, que la conducta atribuida en la solicitud de extradición hubiere ocurrido con posterioridad a la firma del Acuerdo Final, la Sección de Revisión del Tribunal para la Paz evaluará la conducta atribuida para determinar la fecha precisa de su realización y decidir el procedimiento apropiado”.

###### Reciba toda la información de Contagio Radio en [[su correo]
