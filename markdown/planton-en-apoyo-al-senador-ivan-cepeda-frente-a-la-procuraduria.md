Title: Plantón en apoyo al senador Iván Cepeda frente a la Procuraduría
Date: 2015-10-21 11:05
Category: Movilización, Nacional
Tags: alvaro uribe velez, Comuna 13 de Medellín, Iván Cepeda, Masacre El Aro, operación orion, paramilitarismo en Colombia, pliego de cargos con Iván Cepeda, Polo Democrático Alternativo, Procurador Alejandro Ordoñez, Procuraduría General de la Nación
Slug: planton-en-apoyo-al-senador-ivan-cepeda-frente-a-la-procuraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Ivan-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [laotracara.co]

###### [21 Oct 2015]

**Desde las 12 del medio día, ciudadanos y ciudadanas convocan un plantón de solidaridad** con el senador del Polo Democrático Alternativo y defensor de derechos humanos Iván Cepeda, a quien la Procuraduría General de la Nación le compulsó pliego de cargos por supuestas prebendas a dos testigos contra el senador Álvaro Uribe.

El plantón se realizará **frente a las instalaciones de la Procuraduría**, y también se está usando **\#YoApoyoAIvanCepeda** en redes sociales para demostrar solidaridad con el senador.

Cabe recordar, que Cepeda aseguró que **se trata de una acusación “irrisoria”,** teniendo en cuenta que no existe necesidad de entrar en ilegalidades debido a que **distintos ex-miembros paramilitares han acusado al expresidente Álvaro Uribe por distintos nexos con masacres y grupos paramilitares**, caso que se demuestra en las dos últimas semanas con las acusaciones por la masacre del Aro y otra por la operación Orión, en la comuna 13 de Medellín.
