Title: Un partido no se desmorona por las críticas: FARC
Date: 2020-01-23 17:32
Author: CtgAdm
Category: Paz, Política
Tags: FARC, partido, Tanja
Slug: un-partido-no-se-desmorona-por-las-criticas-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @SandraFARC {#foto-sandrafarc .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras el anuncio de Tanja Anne Marie Nijmeijer de renunciar al partido FARC, diversas voces han señalado que es un golpe más a un partido que se está desmoronando. Sin embargo**, la senadora Sandra Ramírez bajó el nivel de los señalamientos, y sostuvo que el trabajo del partido ha sido bueno.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La salida de Tanja, ¿un desacuerdo sobre la implementación?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanja Nijmeijer renunció al partido y ha afirmado que **su deseo es regresar a Holanda**. Por su parte Sandra Ramírez, senadora de FARC, sostuvo que en la colectividad no quisieran que dichas renuncias se presentaran pero es un hecho que puede ocurrir en cualquier partido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ramírez recordó que quienes se vinculan lo hacen voluntariamente y asimismo, se pueden desvincular en el momento en que no se sientan acogidos en las decisiones que se toman, como ocurrió con Tanja. (Le puede interesar:["Las mujeres integrantes de FARC avanzan en su reincorporación"](https://archivo.contagioradio.com/mujeres-farc-avanzan-reincorporacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la Senadora, la diferencia en la lectura que se hace sobre la implementación del Acuerdo de Paz es clave para entender las renuncias. "Algunos pensarán que por la forma como se ha implementado es mejor retirarse, nosotros pensamos diferente", sostuvo. (Le puede interesar: ["Continúan los asesinatos a excombatientes de las Farc"](https://archivo.contagioradio.com/nuevo-asesinanto-excombatiente-farc/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Un partido nunca se desmorona por las críticas"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ramírez declaró que **"un partido nunca se desmorona por las críticas, un partido se fortalece y nosotros estamos abiertos a las críticas",** de hecho, la Senadora expresó que incluso en el pasado invitaron a quienes ahora están en armas para discutir sus diferencias. De igual forma, han abierto espacios fuera del partido para recibir de primera mano las críticas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de ello, la Congresista manifestó que el trabajo del partido ha sido positivo: Han tenido que trabajar en medio de un contexto difícil, han buscado desescalar el lenguaje que es [agresivo](https://www.justiciaypazcolombia.com/defendamos-la-paz-envia-carta-a-mr-adama-dieng-sobre-los-creciente-asesinatos-de-lideres-sociales-y-defensores-de-ddhh-y-excombatientes-de-las-farc-ep/) hacia el partido, "pero cuando hemos estado en la calle la gente nos ve bien, creemos que eso le abre las puertas al Partido".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para concluir, Ramírez sostuvo que ahora enfrentan los problemas de un partido legal, "en la guerrilla se cumplían órdenes, cosas que pasan en una estructura militar, pero ahora la estructura desapareció, **ahora es nuestra conciencia y el interés" lo que sostiene al colectivo.** (Le puede interesar: ["La participación política de FARC no puede ser asesinada"](https://archivo.contagioradio.com/la-participacion-politica-de-farc-no-puede-ser-asesinada/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46925656" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46925656_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
