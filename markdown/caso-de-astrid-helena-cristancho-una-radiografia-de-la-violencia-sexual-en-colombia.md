Title: Caso Astrid Helena Cristancho una radiografía de la violencia sexual en Colombia
Date: 2016-01-27 17:45
Category: Mujer, Nacional
Tags: Astrid Helena, Defensor Otálora, Defensoría del Pueblo
Slug: caso-de-astrid-helena-cristancho-una-radiografia-de-la-violencia-sexual-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensor-del-pueblo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal. ] 

<iframe src="http://www.ivoox.com/player_ek_10222250_2_1.html?data=kpWflJeWeZGhhpywj5aUaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncaTV1NSYxsqPhdTo087Rja3JsMbiwpCw1M7XuMLixM3cjdrSpYzmwsnW0czWpceZpJiSo6nFb8XZjNHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Claudia Mejía, Sisma Mujer] 

<iframe src="http://www.ivoox.com/player_ek_10222264_2_1.html?data=kpWflJeWepWhhpywj5aWaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5yncaTV1NSYo9jYtsrYjK3SzsrSpYy3087g1sbSp8njjNrbw5DWpcXd0Mzfw8uJh5SZoqnOjcnJb83VjNvWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alberto Castilla, Polo Democrático] 

###### [27 Ene 2016. ]

Tras conocerse el caso de Astrid Helena Cristancho sobre el **acoso sexual y laboral del que fue víctima** mientras ejercía como la secretaria privada de Jorge Armando Otálora, actual Defensor del Pueblo, organizaciones defensoras de los derechos de las mujeres y algunos Congresistas han presionado al funcionario para que renuncie a su cargo, pues se trata de un **servidor público encargado de velar por la defensa y la garantía de los derechos humanos**, que** **presuntamente ha incurrido en un acto de vulneración de los mismos.

Claudia Mejía, directora de ‘Sisma Mujer’, asegura que “la conducta de Otálora constituye un delito tipificado por la normativa nacional desde el 2008 a través de la Ley 1257” y que **el caso de Astrid Helena Cristancho no es un caso aislado ni excepcional** sino que constata “la profunda brecha que existe en nuestra sociedad, una sociedad que aspira entrar a un proceso de paz, que aspira a un silencio de los fusiles para poder pensar colectivamente”, pero en la que **algunos hombres ejercen su poder para violentar a las mujeres**, tanto en el plano institucional como cotidiano.

Distintos tipos de violencia que son confirmados por las cifras que la Policía Nacional presentó a finales del año pasado en las que se ratifica que **la violencia sexual contra las mujeres incrementó en un 206% y la violencia intrafamiliar en 120%**. “Por donde se miren las cifras se confirma que persisten los imaginarios sexistas y asimétricos entre las mujeres y los hombres, pero también muestran la progresiva concienciación de las mujeres y su progresivo empoderamiento, **han empezado a decir no más, no acepto más acoso, no acepto más violencia intrafamiliar, no acepto más violencia sexual**”, asevera Mejía.

Por su parte Alberto Castilla, senador del Polo Democrático, indica que la denuncia contra el Defensor del Pueblo comprueba que **la violencia ejercida contra las mujeres en Colombia es permanente y sistemática**, “una práctica cotidiana en el poder” frente a la que se debe trabajar por “una normatividad mucho más estricta” que **obligue al retiro de los funcionarios públicos que vulneración de derechos**, pues "en Colombia los funcionarios se apoltronan en sus cargos en la inmoralidad, incurriendo inclusive en hechos reprochables y no pasa nada”.

Mejía concluye insistiendo en que “**aquello que históricamente se ha llamado amor para permitir la comisión de excesos y hasta hechos delictivos contra una mujer** no puede seguir llamándose así mientras vulnere su derecho a la libertad, integridad y autonomía”, por lo que es necesario que se **erradique todo tipo de violencia contra las mujeres**, pues la solución al conflicto armado debe incluir la reconfiguración de un Estado “en el que cualquier funcionario público tenga una relación directa con la moral, la ética y las buenas prácticas de gobierno”, como lo asevera Alberto Castilla.
