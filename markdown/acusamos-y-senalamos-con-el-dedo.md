Title: Acusamos y señalamos con el dedo
Date: 2015-11-13 06:00
Author: ContagioRadio
Category: Dragonfly, invitado, Opinion
Tags: Alexander Mondragon, indignacion en el pais, optimismo en colombia
Slug: acusamos-y-senalamos-con-el-dedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/sañalar-con-el-dedo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

#### [**[Dragonfly](http://bit.ly/1QkHTjT)**- **[@[MeVle5] 

###### 13 Nov 2015 

[En muchas ocasiones nos ha aturdido la indignación por alguna razón al leer un artículo o ver una noticia en un video, nos puede conmover, vemos con malos ojos lo que algunos puedan hacer, y creemos casi con total certeza que no somos nosotros quienes tengamos alguna culpa en ello, quizás sea así y en muchos casos nos sentimos ajenos luego condenamos desde el mismo gobierno hasta esas otras personas que no somos nosotros, todos son culpables menos nosotros, porque nosotros nos sentimos completamente inocentes.]

[Y si, es usual que nos quejemos de muchas cosas, y por qué no hacerlo si quejarnos es lo más fácil que se puede hacer ante todo aquello que sea deplorable desde lo que nos puede dictar nuestras creencias, creencias que están acorde con nuestra moral individual, que si por el cambio climático satanizamos a las petroleras, que la contaminación es culpa de los carros y las industrias, que el maltrato a los animales son todos esos que golpean caballos, quienes a los perros atropellan o dejan morir de hambre, a los traficantes de animales y a los que los matan para robarles partes de sus cuerpos para comercializarlos, a los que incendian bosques o los que arrojan basura a la calle, y así en muchas situaciones en las que pretendemos hacernos ver a nosotros mismos como los más buenos porque comentamos el rechazo en notas de prensa, porque discutimos con un extraño “x” en Facebook, en twitter o cualquier otra red social, pero será tan cierto que podamos nosotros acusar señalando con el dedo a los demás, y entonces podríamos pararnos frente a un espejo y no señalar a ese quien vemos en el reflejo?]

[Quizá sea que hayamos cierto alivio al administrarnos como un placebo tratar de evidenciar a los culpables de tal o cual acontecimiento, pero la mayoría de las veces, después de usar los dedos por cinco minutos en el teclado no se hace más, colocamos el punto final del párrafo y seguimos con las actividades cotidianas con normalidad creyendo que ya con haber escrito un par de líneas hemos cumplido, sentimos una especie de efecto de aspirina que calma un síntoma pero que en realidad no cura absolutamente nada, porque con un click cerramos la página, pasamos la hoja y nos paramos al lado del camino, quizá con ello olvidamos al día siguiente aquello que nos conmovió tanto porque ya sentimos haber hecho todo lo que más estaba a nuestro alcance por hacer desde el facilismo sin movernos de la silla, pero haber hecho eso puede verse quizás no más que un egoísmo por lo que sentimos, porque nos puede herir a nosotros en esas creencias de lo que es justo y lo que es injusto, pero en realidad los dolidos son otros y en verdad al final así no hicimos nada.]

[En realidad quisiéramos creer que acusando y señalando podamos ser parte de la solución, pero si no hacemos nada más, si no nos movemos de la silla y seguimos estando desde la comodidad del teclado una y otra vez, nos puede convertir incluso en una parte más del mismo problema que rechazamos sin haber podido cambiar nada. Han sido tantas las convocatorias a movilizarnos por tantas causas que nos conmueven, nos limitamos a retuitear y marcar favoritos por miles, pero a la hora de la verdad llegado el día y la hora vemos que no hay compromiso, algunos llegan y a otros se les queda esperando, quizás a muchos les falta un algo, un detonador que los lleve a pasar de las creencias a las convicciones y realmente acudan, participen, se mojen y se unten para lograr los cambios que quisiéramos conseguir.]
