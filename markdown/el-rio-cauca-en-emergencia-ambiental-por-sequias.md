Title: El Río Cauca en estado de emergencia tras operación de Hidroituango
Date: 2019-01-30 16:06
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Antioquia, Hidroituango, Movimiento Ríos Vivos, Río Cauca
Slug: el-rio-cauca-en-emergencia-ambiental-por-sequias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [30 ene 2019] 

[A través de redes sociales habitantes de Caucasia, Antioquia, expresaron alarma sobre los bajos niveles del Río Cauca, que en algunas imágenes se observa bastante disminuido por el cierre de compuertas efectuado por Empresas Públicas de Medellín (EPM) en la represa Hidroituango.]

[Según Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos - Antioquia, esta emergencia ambiental en la parte baja del río es solo una de las anormalidades que se ha presentado tras el cierre de la compuerta 2 en la casa de máquinas de la represa Hidroituango el 16 de enero. (][[Le puede interesar: "Sequía y mortandad de peces alcanzan niveles críticos por operación de Hidroituango"]](https://archivo.contagioradio.com/sequia-mortandad-peces-alcanzan-niveles-criticos-operacion-hidroituango/)[)]

Zuleta afirmó que las autoridades ambientales han insistido que los niveles de agua que se registren son consideres el caudal ecológico. Sin embargo, las comunidades de Caucasia, quienes han observado las dinámicas naturales del Río Cauca por años, reportan que estas sequías no tienen precedente en el municipio.

"Sentimos que nos siguen engañando, que siguen haciendo a la opinión pública que esto es realmente un asunto natural," dijo Zuleta. Sin embargo, todo los registros del proceso de sequía del río demuestran lo opuesto. Normalmente, el caudal del río baja paulatinamente, pero en esta ocasión, ocurrió "de un día para el otro." Por tal razón, las organizaciones ambientales han señalado a la reciente operación de Hidroituango como factor principal de esta sequía.

Actualmente, los efectos ambientales de la operación ya han generado consecuencias para la población de Caucasia. Además del desabestecimiento de agua, que ha sido aliviado temporalmente por el transporte de agua al municipio por medio de carrotanques, se ha reportado afectaciones a la pesca, el cual ha amenazado el sustento de varios habitantes.

### **Nuevas preocupaciones sobre temblores  
** 

Sobre los recientes temblores que sacudieron el país, Zuleta resaltó que un sismo de 3,5 grados se registro el 21 de enero en el municipio de Ituango, muy cerca de la represa. Dado que las comunidades han documentado el desprendimiento de rocas en las montañas del municipio, han cuestionado el efecto que pueda tener estos temblores en el proyecto hidroeléctrico o en la montaña de Hidroituango.

Sin embargo, Zuleta dice que EPM aún no ha anunciado información al público sobre el tema y en reportes no oficiales, afirman que no hay registros de movimiento de suelo en el municipio. Según Zuleta, este silencio solo ha generado más desconfianza de las comunidades con la entidad ambiental.

### **Nuevas alarmas de EPM** 

Frente a las denuncias de las comunidades, EPM remplazó las alarmas de emergencia que presentaban anormalidades. Sin embargo, Zuleta manifestó que les preocupa que los líderes sociales ya no tienen control sobre el uso de las alarmas, los cuales ahora solo se pueden prender por la entidad EPM. Para Zuleta, esta nueva situación es preocupante dado que ella considera que las organizaciones y los líderes son los que están "pendientes realmente de lo que está ocurriendo porque a EPM nunca les ha interesado."

> Todas estas playas eran [\#RioCauca](https://twitter.com/hashtag/RioCauca?src=hash&ref_src=twsrc%5Etfw) hoy están secas por [\#HidroituangoRiesgoEterno](https://twitter.com/hashtag/HidroituangoRiesgoEterno?src=hash&ref_src=twsrc%5Etfw) y ninguna institución se pronuncia Nos han robado una fuente de vida [\#HidroituangoEstaMatandoElRío](https://twitter.com/hashtag/HidroituangoEstaMatandoElR%C3%ADo?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/BnohHsohCm](https://t.co/BnohHsohCm)
>
> — Movimiento RíosVivos (@RiosVivosColom) [28 de enero de 2019](https://twitter.com/RiosVivosColom/status/1089996144116199426?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
