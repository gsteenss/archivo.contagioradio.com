Title: Trabajos en "Doña Juana" son insuficientes y no resuelven el problema estructural
Date: 2017-12-07 15:06
Category: DDHH, Nacional
Tags: Bogotá, Mochuelo Alto, plagas en mochuelo alto, relleno doña juana
Slug: acuerdo-entre-distrito-operadores-y-habitantes-de-mochuelo-alto-son-superficiales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Relleno-Doña-Juana-1200x630-e1503006517453.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  El Correo Confidencial] 

###### [07 Dic 2017] 

Tras la denuncia de los habitantes del Mochuelo Alto en la localidad de Ciudad Bolívar en Bogotá, en donde reiteraron las precarias condiciones en las que están viviendo debido al aumento de plagas por el mal manejo del relleno sanitario de Doña Juana, se realizó una reunión con representantes de la Unidad de Servicios Públicos del Distrito y las empresas operadoras del relleno en donde las autoridades **se comprometieron a eliminar las plagas** y los malos olores.

De acuerdo con Wilmer Albornoz, vocero de las comunidades de Mochuelo Alto, el acuerdo al que se llegó **es temporal y superficial**. Indicó que está basado únicamente en el control de vectores como moscas y malos olores, “en términos generales, el operador privado se compromete de aquí al 15 de diciembre a implementar todos los métodos que consideren para disminuir la cantidad de moscas que hay”, afirmó.

El operador se comprometió también a eliminar los malos olores y junto con la Secretaria de Salud, “a generar un **plan de choque para esparcir insecticidas** alrededor de las zonas residenciales”. Enfatizó en que el 14 de diciembre tendrán otra reunión para revisar cómo va el compromiso de la empresa privada de “generar estructuras de basuras de acuerdo a como se lo obliga el contrato que es con cal y arcilla”. (Le puede interesar: ["Insoportable es la situación de los habitantes en Mochuelo Alto por Relleno Doña Juana"](https://archivo.contagioradio.com/insoportable-es-la-situacion-de-los-habitantes-en-mochuelo-alto-por-relleno-dona-juana/))

### **Medias impuestas no resuelven el problema de fondo** 

Las comunidades han manifestado que este tipo de medidas sólo solucionan el problema **de manera temporal**. Por esto, en la reunión le manifestaron a las autoridades las necesidades que ellos tienen como la urgencia de la presencia del alcalde de Bogotá Enrique Peñalosa “para discutir al respecto de Doña Juana pero no de cómo controlar unas mosquitas y unos ratones, se necesita una discusión estructural”.

Ellos quieren preguntarle al alcalde **“¿cuál es el destino de Doña Juana** con respecto al avance tecnológico que se ha visto en otros países en la implementación de otras tecnologías de transformación de la basura?” Esto teniendo en cuenta que se puede generar electricidad y gas a partir del buen manejo de las basuras.

Además, quieren discutir acerca de los **recursos pasivos de la administración distrital** con respecto a la compensación por daños a las comunidades que “es un dineral larguísimo”. Albornoz recordó que “las inversiones, a la par de la implementación de una tecnología alternativa, se genera la distribución económica y la ejecución de procesos ambientales y de recuperación”. (Le puede interesar: ["Aumentaron las moscas y los malos olores en zonas aledañas al basurero de Doña Juana"](https://archivo.contagioradio.com/aumentaron-moscas-y-malos-olores-en-zonas-aledanas-al-basurero-de-dona-juana/))

### **Alcalde de Bogotá sigue empeñado en expandir el botadero de basura** 

Albornoz afirmó que el alcalde de Bogotá “sigue empeñado en que la solución **es expropiar los terrenos de los campesinos** y ampliar el botadero". Enrique Peñalosa en repetidas ocasiones ha dicho que no hay más predios en donde se pueda depositar la basura que genera la capital por lo que se debe ampliar Doña Juana. Sin embargo,  lo que han dicho las comunidades es que “no debe haber más predios en ningún lugar”.

Para las comunidades **es absurdo que se siga amontonando la basura** “en un país que es rico en agua y en biodiversidad”. En ese sentido, los habitantes colindantes con el relleno han rechazado la idea de Peñalosa de “expropiar a los campesinos de la tierra donde se siembra comida y se genera agua para abrir huecos y echar basura”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
