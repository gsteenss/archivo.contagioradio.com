Title: Boletín informativo Mayo 22
Date: 2015-05-22 17:58
Category: datos
Tags: Actualidad informativa, cese bilateral, contagio radio, FARC, Noticias del día, Organizaciones sociales, Popayán
Slug: boletin-informativo-mayo-22
Status: published

*Noticias del Día: *

<iframe src="http://www.ivoox.com/player_ek_4536219_2_1.html?data=lZqgmJeVfY6ZmKiakp2Jd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Organizaciones de DDHH y representantes de comunidades negras del Cauca**, con presencia en Guapi, **alertan sobre la difícil situación de los habitantes** del municipio **luego del operativo militar** que deja 28 integrantes de las FARC muertos. **Orlando Pantoja de COCOCAUCA** advierte sobre la posibilidad de un **desplazamiento masivo en el territorio**.

-Según el informe presentado el 20 de mayo por el **CERAC**, al cumplirse 5 meses del cese unilateral al fuego decretado por las FARC, se **redujeron en un 85% las acciones ofensivas**; **las muertes de la fuerza pública cayeron en un 64%; y las muertes de la población civil en el marco de acciones relacionadas con el conflicto armado, un 73%.** Habla **David Correal**, del CERAC.

-A pesar de la evidente reducción de las confrontaciones, tanto el CERAC como las organizaciones sociales y de Derechos Humanos que realizaron acompañamiento al cese al fuego entre diciembre del 2014 y mayo del 2015, **denunciaron que se registraron al rededor de 50 eventos de confrontación como consecuencia de acciones ofensivas del Ejercito contra la guerrilla**. Habla **David Florez**, del **Frente Amplio por la Paz**.

-Luego del anuncio realizado por el **Secretariado del Estado mayor de la guerrilla de las FARC** de romper el **cese al fuego unilateral** tras los bombardeos del jueves 21 de mayo en la tarde, el analista **Luis Eduardo Celis de la Corporación Arcoiris**, no cree que las conversaciones de paz en la Habana se pongan en riesgo por tal decisión.

-**Irlanda** se ha convertido en **el primer país del mundo que organiza un referéndum sobre la legalización del matrimonio entre personas del mismo sexo**.

-Organizaciones de comerciantes y trabajadores de las plazas de mercado en Popayán **denuncian intereses del gobierno municipal para trasladarlos hacia la periferia** de la ciudad y dejar el campo abierto **para las multinacionales de supermercados.** **Martín Chicangana**, presidente de la Asociación de Trabajadores y Comerciantes de la plaza de mercado del barrio Bolívar.

-La asociación de **usuarios de servicios públicos de Popayán** **denuncia la falta de atención de las autoridades de la ciudad** para atender los **problemas de contaminación de las fuentes hídricas** que proveen de agua a los pobladores, habla **Fanny Mera**, integrante de la Asociación.

-**Organizaciones y Ambientalistas** colombianos hicieron llegar una **carta al Primer Ministro de China, Li Keqiang**, con el objetivo de que se **garanticen los derechos del ambiente** de acuerdo a la intensificación de las relaciones comerciales, tecnológicas y culturales entre China y Colombia.

-Luego de los últimos sucesos de guerra entre el Gobierno y las FARC en los que la guerrilla suspendio el Cese Unilateral, **organizaciones sociales y de DDHH de Colombia se han organizado para emitir un comunicado, instando a las partes a decretar un Cese Bilateral al Fuego**, con el objetivo de parar el derramamiento de sangre y poder continuar con los Diálogos de Paz de La Habana, sin hostilidades de por medio. Habla **Aura Rodríguez** de **Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo.**

-**Estado Islámico avanza y logra controlar la mitad de Siria**. Según ha informado el Observatorio Sirio para los DDHH el **Estado Islámico ha tomado el último puesto fronterizo con Irak**, consolidando así la toma del norte de esta nación y el sur de Siria.

-Nueva l**ínea de crédito ICETEX "Tu Eliges"** presentada el día de ayer por el presidente Juan Manuel Santos **fomenta de manera exorbitante el endeudamiento de estudiantes en Colombia**. Habla **Carlos Acero**, r**epresentante estudiantil de la Facultad de Derecho de la Universidad de los Andes y miembro de la MANE**.
