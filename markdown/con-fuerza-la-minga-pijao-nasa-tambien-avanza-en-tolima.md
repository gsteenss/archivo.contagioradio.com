Title: ¡Con fuerza! La Minga Pijao Nasa también avanza en Tolima
Date: 2019-03-30 13:08
Category: Comunidad, Movilización
Tags: Minga del Tolima, Minga Nacional por la Vida, Pueblos Pijao
Slug: con-fuerza-la-minga-pijao-nasa-tambien-avanza-en-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-30-at-11.04.14-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [30 Mar 2019]

Desde el pasado 27 de marzo la Minga del pueblo **Pijao se desplazó hasta la quebrada Guaguarquito en medio del cerro de Pacandé, un lugar de culto para las comunidades** donde habitan los dioses y donde esperan que este fin de semana se congreguen cerca de 2000 mingueros  que se han unido a las demandas  por laa ampliación y reconocimiento de resguardos y la defensa de territorios en conflicto por explotación minero energética

Una de las integrantes del equipo de la **Consejería Mayor de la ONIC y parte de la Minga Pijao - Nasa**,  aseguró que "esta es una Minga pacífica, tenemos que desmontar los mensajes de estigmatizacion contra la movilización" señalando que el pueblo Pijao "no tiene intención de  afectar los derechos de otros ciudadanos" como piensan algunos sectores, sino que buscan defender sus derechos económicos, sociales, culturales y ambientales.

En respaldo al llamado de la ONIC, cerca de 1000 integrantes del pueblo Pijao se han unido a esta causa, **exigiendo el cumplimiento integral del Acuerdo de Paz, la protección de los ríos Saldaña y Magdalena,** la defensa de los líderes sociales e indígenas del país y además ser tenidos en cuenta en  la asignación de los recursos pactados en el Plan Nacional de Desarrollo. [(Lea también: Pueblos indigenas de ocho departamentos se suman a la Minga)](https://archivo.contagioradio.com/jose-silva-nacion-wayuu/)

De igual forman se han manifestado en contra de las afectaciones hechas por la explotación de hidrocarburos, exigiendo que se dé inicio a un proceso de restauración reparación colectiva a comunidades y resguardos indígenas afectados por estas explotaciones

Mientras la Minga del suroccidente del país continúa los diálogos con el Gobierno en La Delfina, Valle del Cauca, como parte de su movilización, los mingueros del Tolima acordaron tomar posesión de **la vía Natagaima - Aipe,** lugar al que llegó la Fuerza Pública con la que  después de dialogar en un consejo de seguridad se decidió alternar las cuatro horas del bloqueo liberando un carril.

Según Leidy Arévalo, intengrante del Observatorio minero energético de re-existencia y quien también ha hecho acompañamiento a las comunidades, **desde el día 29 arribó al lugar una delegación de distintos secretarios de Gobierno** con los que se establecieron cuatro mesas de diálogo en torno a los temas de salud educación, planeación y territorio.[(Le puede interesar No pedimos tierras para una sola persona, sino para miles Minga Indígena)](https://archivo.contagioradio.com/no-pedimos-tierras-para-una-sola-persona-sino-para-miles-minga-indigena/)

Arévalo indica que no han existido avances pues aunque **"hay disposición de diálogo no hay concertación pues desde la gobernación se está dilatando la negociación afirmando que ya termina su periodo de mandato**", razón por la que se espera que para el lunes continúen los diálogo, este 30 de marzo a la Minga Pijao se ha unido la Universidad del Tolima para hacer un acompañamiento, a la que esperan se sumen otros sectores para fortalecer la movilización.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
