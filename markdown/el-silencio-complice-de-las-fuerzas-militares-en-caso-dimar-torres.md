Title: El silencio cómplice de las Fuerzas Militares en caso Dimar Torres
Date: 2020-01-28 17:38
Author: CtgAdm
Category: DDHH, Judicial
Tags: Catatumbo, Dimar Torres, Ejecuciones Extrajudiciales
Slug: el-silencio-complice-de-las-fuerzas-militares-en-caso-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Dimar-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/diego-martinez-sobre-complicidad-militares-caso_md_47057105_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Diego Martinez | Abogado Familia Dimar Torres

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Dimar Torres Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras [otorgar medidas](https://twitter.com/FiscaliaCol/status/1220359252310708229) de aseguramiento en centro de reclusión militar al coronel (r) **Jorge Armando Pérez Amézquita** y a otros tres militares más, investigados por el asesinato de **Dimar Torres**, el abogado que representa a la familia del excombatiente, **Diego Martínez** manifestó que no solo se trata de llevar ante la justicia a los responsables, sino de ser conscientes que se está ante un crimen de Estado que evidencia una conducta que pone en duda el actuar del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Lo que no hemos dimensionado alrededor del caso es que Dimar Torres, quien firmó un acuerdo de paz, es que estaba protegido por el Derecho Internacional Humanitario (DIH), porque fue un combatiente que dejó las armas y posteriormente fue asesinado y cuyo cuerpo se ocultó a la comunidad"**, explica el abogado basándose en las múltiples pruebas incluidos audios y chats que dan cuenta de no solo una ejecución extrajudicial sino de una conducta ligada directamente a la desaparición forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las pruebas aportadas por la Fiscalía han revelado cómo el crimen hizo parte de un plan de venganza por la muerte del soldado Pablo Borja García, en de abril de 2019, en zona rural de Ocaña. incluyó la creación de un grupo de WhatsApp para informar sobre Dimar Torres, a quien hombres bajo las órdenes del coronel (r) Pérez, señalaron como el supuesto responsable de la muerte del uniformado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Se trata de una conducta o permisividad militar ?

<!-- /wp:heading -->

<!-- wp:quote -->

> El tema centra no es la cárcel, es lograr esclarecer un crimen de Estado y que se apliquen las sanciones previstas en el DIH.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Para Martínez, no es suficiente el esclarecimiento del hecho, aún es necesario conocer la responsabilidad de la línea de mando y saber lo que ocurrió después de cometido el asesinato, **"aquí lo primero es conocer si existe una orientación del alto mando militar de cometer estas conductas, sobre todo en regiones donde el Estado no llega".** En ese sentido, el abogado se cuestiona si acaso desde las FF.MM. existe permisividad para cometer un crimen, "más allá del móvil como una orientación por sospecha o por la teoría del enemigo interno". [(Le puede interesar: Es necesaria una comisión independiente que diga qué pasa al interior del Ejército)](https://archivo.contagioradio.com/es-necesaria-una-comision-independiente-que-diga-que-pasa-al-interior-del-ejercito/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hasta ahora, Pérez Amézquita es el uniformado de más alto rango que ha sido vinculado al caso del excombatiente, sin embargo ni él ni los otros tres acusados aceptaron su participación en el delito de homicidio, declarándose inocentes y justificando que la responsabilidad fue únicamente del cabo que haló el gatillo, desconociendo así su participación, pero a su vez admitiendo que existió una causal para asesinar a un excombatiente. [(Lea también: Coronel Jorge Armando Pérez será investigado por asesinato de excombatiente Dimar Torres)](https://archivo.contagioradio.com/coronel-jorge-armando-perez-sera-investigado-por-asesinato-de-excombatiente-dimar-torres/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La tierra de Dimar Torres resiste

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A casi un año del asesinato, el abogado hizo referencia a la familia del excombatiente, y la forma en cómo se ha adelantado el proceso afirmando que lo que en realidad esperan es una "verdad completa y una solicitud de perdón por parte del Estado colombiano. A propósito, destacó su resiliencia y la de la comunidad de Convención, una región afectada donde "los efectos de la guerra y la pobreza son inclementes y pese a ello, han sobrevivido a años de confrontación" por medio de la organización social, la misma que impidió que el asesinato de Dimar fuera silenciado. [(Lea también: Condenan a 20 años de cárcel al cabo Daniel Gómez por homicidio de Dimar Torres)](https://archivo.contagioradio.com/condenan-a-20-anos-de-carcel-al-cabo-daniel-gomez-por-homicidio-de-dimar-torres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el caso de Dimar parece ser uno de los pocos que ha avanzado con aparente celeridad ante la justicia, el abogado reconoce que se trata de un nivel de esclarecimiento mínimo en la medida en que van 180 excombatientes de FARC asesinados y de los que al menos en la opinión pública no se ha tenido mayor visibilidad, sino de forma contraria, se "ha establecido una hipótesis mediática muy peligrosa que plantea que los excombatientes se están asesinando entre ellos", una postura, que para el abogado, sin desconocer que pudiesen existir estas situaciones, no se centra realmente en el incumplimiento del Estado . [(Lea también: Un partido no se desmorona por las críticas: FARC)](https://archivo.contagioradio.com/un-partido-no-se-desmorona-por-las-criticas-farc/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
