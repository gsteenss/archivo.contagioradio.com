Title: La seguridad desde una visión de mujer
Date: 2018-02-01 15:04
Category: Mujer
Tags: Inseguridad, mujeres, posconflicto
Slug: mujeres_seguridad_foro_internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DU91Jn-WAAEgSjw-e1517508932858.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación Böll  
] 

###### [1 Feb 2018] 

En medio de debate sobre la necesidad de fortalecer las condiciones de seguridad de las y los colombianos, mujeres diversas se reúnen este 1 y 2 de febrero para discutir sobre esta problemática y plantear otro tipo de propuestas, más allá de aumentar el número de policías o prohibir el parrillero hombre.

Se trata del Foro Internacional ‘Las mujeres hablamos sobre seguridad; propuestas para un país en transición’, organizado por el Colectivo de Pensamiento y Acción: Mujeres, Paz y Seguridad (Colectivo Pensamiento y Acción MPS), que reúne 29 iniciativa de mujeres de todo el país que buscan dar cuenta de la importancia de incorporar la perspectiva de las mujeres en la creación de estrategias y políticas de seguridad.

“Las mujeres creemos que para lograr seguridad tenemos que generar redes de democracia, confianza, reconocimiento de la otredad y pérdida de temor, que nos permitan una sana convivencia”, manifiesta Rosa Emilia Salamanca, de la Corporación de Investigación y Acción Social y Económica (CIASE) y Secretaria Técnica del Colectivo.

### **En cifras: la violencia contra las mujeres** 

Durantes los últimos días ha sido noticia, el atraco a una mujer embarazada en el barrio Rosales que terminó con una fuerte herida con arma de fuego, sin embargo, este caso es apenas un ejemplo de la violencia de la que son víctimas las mujeres en todo el país, que a veces se da, por el simple hecho de ser mujeres.

De acuerdo con  Medicina Legal, una mujer es asesinada cada tres días y a diario se producen 55 casos de violencia sexual; la mayor parte de las agresiones son causadas por su pareja, por familiares y ocurren en su propio hogar o en su entorno cercano. El mismo organismo revela que, las armas de fuego se han convertido en el principal instrumento para acabar con la vida de las mujeres por encima de las armas blancas y los golpes contundentes. Por su parte, el Informe de Cifras de Violencia Nacional de Colombia Diversa revela que en 2016 hubo un total de 687 ataques contra la población LGBTI, y en el 64% de los casos no hay claridad sobre los presuntos responsables.

A partir de ese contexto, las organizaciones de mujeres señalan que es importante que “La seguridad no se siga pensando desde escenarios en los que las instituciones crean los esquemas basados solamente en cifras que, indudablemente son muy importantes, pero que, además, vayan acompañadas en su lectura y elaboración estratégica de la integración de la perspectiva de las mujeres, si se quiere contribuir a la disminución de cualquier tipo de agresión”.

### **La seguridad en el posconflicto** 

El foro se presenta, en medio de la transición actual que vive el país con el objetivo de pensar una propuesta innovadora al diálogo desde distintos sectores que buscan aportar a la construcción de paz y seguridad desde una mirada integral con las mujeres, reconocidas como protagonistas y referentes en el tema.

Ante el escenario actual del posconflicto, desde el Colectivo MPS, se apuesta a conocer y reconocer las apuestas y percepciones frente a lo qué es la seguridad.  Por eso, escuchando a mujeres rurales, indígenas y afro también se crean estrategias y recomendaciones frente a las rutas de restitución de tierras y evaluar acciones que podrían mejorar notablemente los niveles de seguridad para las mujeres y las comunidades.

En el foro, las mujeres buscan responder una pregunta básica: "¿Cómo transformamos nuestra cultura de seguridad, que hasta ahora se manifiesta en encerrarnos cada vez más, hacia una cultura de seguridad que se hace en la relación y generación de confianza con las demás personas?”, dice  Rosa Emilia Salamanca.

Dicho cuestionamiento será analizado por entidades nacionales, organismos internacionales y expertos de talla internacional como Jacqueline O’Neill, experta canadiense en seguridad y presidenta de Inclusive Security (Seguridad Inclusiva), que lidera en alrededor de 30 países el incremento del aporte de las mujeres en las políticas de seguridad y paz; y el español Daniel Torres, Director Adjunto de DCAF (the ‘Geneva Centre for the Democratic Control of Armed Forces‘) y Jefe de la División de Género y Seguridad.

<iframe id="audio_23502657" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23502657_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
