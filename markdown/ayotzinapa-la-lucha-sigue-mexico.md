Title: Ayotzinapa: la lucha sigue
Date: 2016-01-07 10:34
Category: Eleuterio, Opinion
Tags: Ayotzinapa, estudiantes, mexico
Slug: ayotzinapa-la-lucha-sigue-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/ayotzinapa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

#### **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### [7 Ene 2015] 

[Ayotzinapa ha pasado en poco más de un año, de ser un topónimo semidesconocido a señalar la lucha contra el crimen, la barbarie y la impunidad en el estado de México. Lo acontecido con los 43 estudiantes normalistas de Iguala hizo surgir un clamor nacional e internacional reclamando justicia al gobierno mexicano señalado como corrupto y responsable directo de lo sucedido. Sin embargo, la historia del estado de las cosas en el país viene de lejos.]

[Bajo el gobierno del PRI, México funcionó durante décadas como un narco-estado, en el que también se daba una narco-política. Los carteles de la droga pagaban a los políticos y ambas partes se enriquecían manteniendo un negocio oscuro que no tenía una influencia violenta tan directa en el día a día de la gente. Pero el nuevo panorama político tras la caída del PRI en el año 2000, abrió un nuevo escenario en el que surgieron luchas de poder que esta vez sí, tuvieron violentas consecuencia para los ciudadanos de a pie. La llamada guerra contra el narco comenzó con el gobierno de Vicente Fox que relevaba al PRI después de 70 años en el poder. Surgieron nuevos cárteles que intentan hacerse hueco en el negocio del narcotráfico, el resultado es una violencia insoportable que creció exponencialmente. A Fox le sucede Calderón también del Partido Acción Nacional, en unas elecciones más que fraudulentas celebradas en 2006]*[.]*[Desde entonces, en México han desaparecido cerca de 30 mil personas; datos que se aproximan a las desapariciones sufridas bajo las dictaduras militares de Argentina (1976-1980) y Chile (1973-1990). La supuesta guerra contra el narco] [fue una forma de encubrir a grupos de poder que habían aupado al nuevo gobierno y que éste apoyaba. Así, todo desembocó en una guerra interna entre grupos de narcos que acabó repercutiendo en la población civil. Esta situación continúa a día de hoy aun con la vuelta del PRI a la presidencia.]

[La noche del 26 de septiembre del 2014, los estudiantes de la escuela Raúl Isidro Burgos de Ayotzinapa fueron asaltados conjuntamente por la policía municipal de Iguala y pistoleros vinculados al narco. Su destino era llegar a Ciudad de México el 2 de octubre para asistir a las conmemoraciones por los sucesos de Tlatelolco. El camino les sirve para recorrer diferentes pueblos y recoger colaboraciones con las que seguir manteniendo “las normales rurales”, escuelas para hijos de campesinos.] [Uno de los supervivientes del asalto lo narraba así: “…]*[llega una camioneta roja y otros coches particulares escoltados por la policía municipal. De ellos bajan varios hombres armados vestidos de paisano que comienzan a disparar contra nosotros. Nos vemos en la necesidad de huir. Uno siente la impotencia, vas corriendo y tu compañero corre detrás. Cuando volteas la cabeza de nuevo ya no está porque lo alcanzaron las balas y cayó asesinado. Corres por temor a que te ocurra lo mismo y parece milagroso que algunos saliéramos ilesos. Nos escondemos. El ataque dura 45 minutos en los que somos cazados como animales. Hubo 7 muertos, más de una veintena de heridos y 43 desaparecidos.”]*

A día de hoy los estudiantes siguen desaparecidos y sus padres, familiares y compañeros siguen exigiendo justicia a pesar de las evasivas del gobierno, al que ya molesta que el tema se alargue y no se olvide como ocurre con tantas otras atrocidades que suceden impunemente en México.

[El compañero y periodista mexicano O. Zapata nos explicaba la necesidad de la constancia  en el reclamo de justicia ante la estrategia disuasoria y de olvido que emplea el sistema: “]*[el gran reto es que Ayotzinapa no se convierta en un tema de los grandes medios de comunicación que están intentando solidarizarse con el movimiento para llevárselo a su terreno. El peligro es que la solidaridad se convierta en una moda pasajera. Debe continuar y crecer la idea de que se debe cambiar al gobierno de raíz y seguir luchando por trasformar la realidad de nuestro país.”]*
