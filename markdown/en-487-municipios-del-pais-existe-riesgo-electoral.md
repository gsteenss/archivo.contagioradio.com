Title: En 487 municipios del país existe riesgo electoral
Date: 2015-09-09 15:15
Category: Uncategorized
Tags: amenazas de riesgo electoral, Fraude electoral, Mapa de riesgo electoral, Misión de Observación Electoral, Riesgo electoral, violencia electoral
Slug: en-487-municipios-del-pais-existe-riesgo-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/MOE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: confidencialcolombia.com 

<iframe src="http://www.ivoox.com/player_ek_8247796_2_1.html?data=mZehmZydeo6ZmKiak5eJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BflZ2kjdLZssrXytXW0diPqMbgjNXOh6iXaaK41JDS2s7XuMaf087S1czTb8bgxsjh0dfFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Camilo Vargas, MOE] 

###### 9 sep 2015 

Camilo Vargas, coordinador del Observatorio Político-Electoral de la MOE, indicó que **en medio de un desescalamiento del conflicto las cifras de violencia electoral son alarmantes** y que si fueran hoy las elecciones, en 59 municipios no podrían llevarse a cabo.

Esta es una de las conclusiones más alarmantes a las que llegó la Misión de Observación Electoral, durante el lanzamiento de la radiografía de las principales anomalías y riesgos en el país para las elecciones regionales del próximo 25 de octubre.

El estudio indica que en **487 municipios del país, es decir el 43% del total**, existen riesgos electorales. Entre ellos, **438 cuentan con índices de riesgo por violencia y en 204 municipios coincide el fraude electoral y el conflicto armado**, donde los departamentos de **Guaviare, Caquetá, Arauca, Cauca, Nariño y Chocó** son los más afectados.

De los **438 municipios que muestran riesgo  por violencia, 129 presentan un riesgo alto y 229 un riesgo medio**, siendo cifras alarmantes para el desarrollo de las elecciones y la seguridad tanto de los votantes como de los candidatos. Vargas, resalta que “**la violencia no decrece al nivel que uno se imaginaria**” ya que se evidencia un aumento considerable en la violencia política.

Amenazas, asesinatos, violencia contra la libertad de prensa, desapariciones, secuestros de candidatos, trasteo de votos, son algunas de los actos que tienen en alto riesgo las elecciones regionales de este año.

El libro fue presentado a la comunidad en general, allí también  se contó con la participación de distintas entidades estatales como la Defensoría del Pueblo, la policía, la Registraduría Nacional y el Consejo Nacional Electoral, lo cual permite “contrastar datos con las distintas estancias estatales, para poder tener distintas versiones de poder prevenir una alteración de las elecciones”, indica el coordinador del Observatorio Político-Electoral de la MOE.

[[Presentación Mapas de Riesgo Electoral 2015](https://es.scribd.com/doc/279783994/Presentacion-Mapas-de-Riesgo-Electoral-2015 "View Presentación Mapas de Riesgo Electoral 2015 on Scribd")]

<iframe id="doc_9140" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/279783994/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
