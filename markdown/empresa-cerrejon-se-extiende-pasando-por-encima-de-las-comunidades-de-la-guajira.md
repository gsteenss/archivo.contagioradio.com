Title: Empresa Cerrejón se extiende pasando por encima de las comunidades de La Guajira
Date: 2016-02-25 14:54
Category: Ambiente, Nacional
Tags: El Cerrejón, La Guajira, Roche
Slug: empresa-cerrejon-se-extiende-pasando-por-encima-de-las-comunidades-de-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/CcAlbePXEAA-_3E.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cajar 

<iframe src="http://co.ivoox.com/es/player_ek_10574443_2_1.html?data=kpWimZmYeJShhpywj5aYaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncabh0dfS1caPh8bm08rXh6iXaaOnz5Dgx5DJvNXdxtPRx5DUpdTVz8ncjdXTtozZz8jWz8aPqMafzcbgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Samuel Arregocés] 

###### [25 Feb 2016] 

Una familia integrada por **50 personas de la comunidad de Roche, en el departamento de La Guajira** fue desalojada por la administración municipal de acuerdo a  los intereses que tiene la empresa El Cerrejón sobre el predio. En el operativo, el ESMAD y la policía sacaron a las personas de sus hogares a golpes y luego procedieron a destruir sus viviendas.

**“Llegaron brutalmente con ESMAD a golpear a la familia, luego destruyeron las casas”,** relata Samuel Arregocés, víctima del desalojo y defensor de Derechos Humanos. La juez habría dado la orden de proceder pese a que El Cerrejón le había incumplido a la comunidad con el compromiso de reubicarlos con vivienda digna, servicios públicos (especialmente agua) y tierras productivas. Sin embargo, la empresa no le ha cumplido ni a la comunidad de Roche, ni al resto de las familias que ya han sido desalojadas.

De acuerdo con la información de Arregocés, la compañía ha logrado apropiarse de 9 mil hectáreas de tierras de comunidades de La Guajira, quienes denuncian que “las viviendas, la iglesia y el colegio entregados por la empresa se están cayendo”, además no existen los hay proyectos productivos que habían prometido y tampoco agua potable, **“El agua no sirve ni siquiera para bañarse”, dice la víctima del desalojo.**

Seis personas terminaron heridas luego de recibir los gases lacrimógenos y los golpes por parte del ESMAD, uno de ellos terminó con un brazo fracturado. Así mismo, el periodista guatemalteco, Rafael Ríos quien cubría el desalojo fue detenido varias horas y su material fue borrado, adicionalmente, tres personas más fueron arrestadas, aunque una de ellas ya se encuentra en libertad.

“Se habla de la desnutrición infantil en La Guajira, pero no entiendo por qué no se habla de la minería… **El Cerrejón tiene control sobre 30 mil hectáreas”**, dice indignado el defensor de DDHH, quien añade que el gobierno está a favor de las multinacionales, pues durante el desalojo no hubo presencia de la Secretaria de Gobierno ni del personero.

Por el momento, la comunidad queda a la espera de concretar una mesa de trabajo con el alcalde para buscar una solución sobre la situación que viven las familias. **Las personas desalojadas se encuentran atemorizadas y preocupadas debido a que han sido objeto de amenazadas** en ocasiones anteriores y no han recibido ningún tipo de respuesta o apoyo a las denuncias interpuestas por esa situación.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
