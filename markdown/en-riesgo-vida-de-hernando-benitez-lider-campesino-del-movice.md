Title: En riesgo vida de Hernando Benítez líder campesino de Sucre
Date: 2020-10-20 11:35
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: colombia, Lider social, Sucre
Slug: en-riesgo-vida-de-hernando-benitez-lider-campesino-del-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/BuscarlasHastaEncontrarlas-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Movice*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes el Movimiento Nacional de Víctimas de Crímenes de Estado (Movice), en su capítulo sucre denunció el a**tentado en contra de la vida de Hernando Benítez**, líder de este movimiento en San Benito Abad, Sucre.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Movicecol/status/1318536454356848641","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Movicecol/status/1318536454356848641

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se registró este lunes 19 de octubre a las 7:00 de la noche en el municipio de San Benito Abad, departamento de Sucre, **cuando Benítez se encontraba en su en su vivienda, a donde ingresaron dos personas y le propinaron 5 disparos con arma de fuego**, uno de ellos impacto en su brazo derecho dejándolo herido y tendido en el piso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Los responsables al creer que lo habían asesinado huyeron vía Santiago** y su rastro fue perdido a la altura del lugar conocido como El roble".*
>
> <cite>Movice</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Hernando Benítez, **es un dirigente campesino defensor de Derechos Humanos y líder social del proceso de restitución de tierras de playón Caño Palomo de San Benito Abad**, un territorio en Sucre compuesto por más de 302 hectáreas de ciénaga y siete municipios de la supresión de La Mojana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este territorio junto a la comunidad lleva más de **30 años de lucha en el marco de una disputa territorial entre el acaparamiento de tierras por parte de los ganaderos** y los campesinos y campesinas que defienden su territorio y el ambiente. ([Otra Mirada: Riesgos de reclamar tierras en Colombia](https://archivo.contagioradio.com/otra-mirada-riesgos-de-reclamar-tierras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tres décadas de trabajo encabezado por Hernando Benítez que tuvieron reconocimiento por parte de la Agencia Nacional Territorial (ANT), quién el **11 de marzo del 2020 realizó la entrega a 17 familias campesinas del, *"el Playón comunal Caño Palomo, con fines de acceso, uso, conservación y recuperación".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una acción que en vez de mejorar las condiciones territoriales agudizó el conflicto, esto debido a la ocupación del playón por parte de los ganaderos del sector, ***"perjudicando las labores del campesinado y generando amenazas contra los líderes campesinos del comité campesino de caño palomo"***, indicó el Movice.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Violencia que consigo afectó las **condiciones de seguridad Fernando Benítez quien algunos días después tuvo que solicitar protección**, y que según el Movice, le fueron asignados dos escoltas para resguardar su vida pero que pese a ello, *"[estas medidas no fueron suficientes](https://movimientodevictimas.org/denuncia-publica-lider-campesino-e-integrante-del-movice-en-sucre-fue-victima-de-cinco-disparos/)".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para las organizaciones de víctimas el atentado contra Benítez, se dió no solamente por su trabajo como defensor de la comunidad campesina de San Benito Abad, al rechazar la actividad ganadera en el territorio; sino también en medio de la presencia de grupos armados, *"que tienen estrechas relaciones con el poder político de la región".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

E**l Movice hace un llamado para que se respete y proteja la vida de los líderes sociales defensores de derechos humanos y firmantes de paz en los [montes de María](https://archivo.contagioradio.com/la-violencia-se-reagrupa-y-reestructura-en-los-montes-de-maria/)**al mismo tiempo que exigen a la fiscalía investigar el atentado contra Hernando Benítez y establecerá los responsables tanto materiales como intelectuales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
