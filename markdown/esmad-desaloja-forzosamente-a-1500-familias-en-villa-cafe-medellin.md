Title: ESMAD desaloja forzosamente a 1500 personas en Villa Café, Medellín
Date: 2015-05-28 12:14
Category: DDHH, Nacional
Tags: brutalidad policial, desalojos forzosos, ESMAD, Juzgado Departamental de Policía, ladrillera Santa Rita, Medellin, Villa Café
Slug: esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ESMAD-6.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alberto Castilla 

<iframe src="http://www.ivoox.com/player_ek_4564075_2_1.html?data=lZqjlpWbeY6ZmKiak5yJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmYxsrXpc3jy8aYyNTWvtDnwtLS0NnJb8KfkpqdkpDUqdPn0NPO1ZDJsozKytHZw5CnpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Walter Caicedo, líder comunitario de Villa Café] 

###### [28 de May 2015]

En Medellín continúan los desalojos forzosos de las familias más pobres de esta ciudad. Esta vez se trata del barrio Villa Café, donde esta mañana desde las 6:30, **1500 personas de estratos 0 y 1 han sido desalojados a manos de 1000 agentes de la policía**, que han usado la violencia para lograr desplazarlos.

De acuerdo a Walter Caicedo, líder comunitario de Villa Café, los desalojos se han desarrollado **de manera ilegal y sin cumplir los requerimientos** necesarios para que se pueda sacar a las personas de sus casas, entre las que se encuentran **madres comunitarias y cabezas de familia, ancianos, niños y niñas y discapacitados**. Además, según Caicedo, la Alcaldía de Medellín no ha dado ninguna solución de vivienda a las familias desalojadas.

Esta situación se debe a que la empresa ladrillera Santa Rita habría logrado demostrar que el predio es de esa compañía, sin embargo, Walter Caicedo, asegura que esos predios los han habitados desde hace aproximadamente 26 años. Así mismo, denuncia que “no hay que ser abogado” para ver todas las **incoherencias que tiene el procedimiento desarrollado** en el **Juzgado Departamental de Policía** desde donde se ratificó la orden de desalojo de las familias asentadas en ese terreno.

**"Hoy solo tenemos la cruz de haber nacido pobres",** expresa el líder comunitario, quien además cuenta que “con lágrimas en los ojos”, pudo evidenciar como el **ESMAD agredió físicamente a un niño de 10 años** que intentaba proteger a su padre, mientras unos agentes de la fuerza pública jalonaban y le pegaban  al hombre.

Cabe recordar que en días pasados, cuando se conoció la orden inicial de desalojo, algunos de los asentados en Villa Café exigieron de manera pacífica su **derecho a una vivienda digna** en la Parroquia Nuestra Señora de Belén.

[![ESMAD 5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ESMAD-5-e1432833139336.png){.aligncenter .wp-image-9321 .size-full width="493" height="371"}](https://archivo.contagioradio.com/esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin/esmad-5/)

[![ESMAD 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ESMAD-1.png){.aligncenter .wp-image-9325 .size-full width="764" height="425"}](https://archivo.contagioradio.com/esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin/esmad-1/)

[![ESMAD 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ESMAD-3.png){.aligncenter .size-full .wp-image-9323 width="808" height="452"}](https://archivo.contagioradio.com/esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin/esmad-3/)

[![ESMAD 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ESMAD-4.png){.aligncenter .size-full .wp-image-9322 width="809" height="453"}](https://archivo.contagioradio.com/esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin/esmad-4/)
