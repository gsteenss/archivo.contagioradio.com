Title: Hola Papá - Cartas desde el Closet
Date: 2017-11-23 10:51
Category: Cartas desde el Closet, Opinion
Tags: Homosexualidad, LGBTI, sexualidad
Slug: hola-papa-cartas-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/desde-el-closet-papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Freephoto 

###### [Noviembre, 2017] 

#### [**Parche por la vida**] 

Me preguntaste hace unos días qué me está pasando porque de un tiempo para acá he cambiado mucho y la comunicación entre los dos, prácticamente, ha desaparecido.

[Estoy de acuerdo que he cambiado mucho y que la comunicación ha desaparecido.]

[Respondo por este medio. Espero que algún día puedas leer esta carta y comprender que te respondo en ella por la buena comunicación que tuvimos hasta hace un tiempo; ya que no me atrevo a abordar este tema de frente.]

[Una primera razón, es que con el paso de mi adolescencia a la juventud he tenido muchos cambios y me ha tocado enfrentar una realidad muy fuerte para mí; una crisis de identidad sexual. Identificar si en este momento soy homosexual o no.  ]

[Como te conté alguna vez, he sido muy tímido con las mujeres y me cuesta trabajo establecer una relación con ellas como lo hacían la mayoría de mis compañeros de colegio y de estudio táctico, lo cual me generaba un sentimiento que no puedo describir bien.]

[Ahora tengo la incertidumbre de si era mi timidez con las mujeres o si nací homosexual lo que me llevaba a actuar así. Esto me hace sentir mal en mis relaciones contigo y con el resto de mi familia y no sé cómo abordar esta realidad.]

[La segunda razón, son los comentarios tan fuertes, descalificantes y vulgares que se hacen en casa frente a los homosexuales y a la llamada “ideología de género”.]

[Se afirma que los personas LGTBI son personas malas y pervertidas y yo empezaba a sentirme así, pero luego pensé que no elegí esta ambigüedad sexual y, por lo que he sabido, muchos homosexuales no eligieron ser como son y entonces me pregunto: ¿cómo puede ser mala y pervertida una persona que no eligió la orientación sexual que tiene? ¿Por qué a las personas heterosexuales se les ve como normales los comportamientos promiscuos, violentos y abusivos en el tema de la sexualidad solo por ser “normales”?]

[Frente a la llamada “ideología de género”, nunca me han dado una respuesta más o menos convincente cuando he preguntado que entienden por “ideología de género”, simplemente repiten que eso es malo, que quieren acabar con la familia, que atenta contra la moral cristiana pero sin dar argumentos, es como si repitieran discursos de memoria con el que descalifican los reclamos del derecho a la equidad de las mujeres y al respeto de la población LGTBI.]

[Estas son las razones por la cuales he dejado de hablar y he preferido encerrarme en mis cosas, pero en el fondo quisiera recuperar la posibilidad de hablar de estos temas y de otros como lo hacíamos antes.]

[Un abrazo,]

[Tu hijo que te quiere y se siente mal por los problemas actuales]

#### **Parche por la vida 11 - Ver más [Cartas desde el Closet](https://archivo.contagioradio.com/cartas-desde-closet/)** 
