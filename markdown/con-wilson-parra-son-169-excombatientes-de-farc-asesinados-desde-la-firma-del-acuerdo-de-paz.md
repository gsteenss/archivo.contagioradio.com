Title: Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz
Date: 2019-10-29 13:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinatos de excombatientes, Caquetá
Slug: con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Excombatientes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

Cerca de las 19:30 del pasado 28 de octubre, en el lugar conocido como Puerto Mafia o la antigua estación de bombeo en Curillo, Caquetá, fue asesinado con arma de fuego **Wilson Parra Lozada**, excombatiente de 42 años quien perteneció al Espacio Territorial de Capacitación y Reincorporación La Carmelita en Putumayo y que desde el 2018 habitaba en este municipio del sur del país.

Según información preliminar, Wilson quien era apodado 'El Indio', se encontraba en una vivienda hasta la que llegaron dos sujetos que se movilizaban en bote por el río Caquetá y dispararon en repetidas ocasiones contra el excombatiente causando su muerte, **tras lo sucedido los hombres huyeron en el bote río abajo.** [(Le puede interesar: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR) ](https://archivo.contagioradio.com/alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr/)

El pasado 25 de octubre, dirigentes del partido político FARC se refirieron a los asesinatos sistemáticos de excombatientes y exigieron al Gobierno que adelante las investigaciones pertinentes, pero sobretodo **que cumpla con la implementación del Acuerdo de Paz y centre sus esfuerzos en hallar a los autores intelectuales de dichos delitos. ** [(Lea también: ¿Existen garantías de seguridad para excombatientes que habitan los ETCR?)](https://archivo.contagioradio.com/existen-garantias-de-seguridad-para-excombatientes-que-habitan-los-etcr/)

Con el asesinato de Wilson, **son tres los excombatientes que han muerto en menos de una semana**, Alexander Parra fue asesinado al interior del ETCR Mariana Páez en Meta y Dago Galindez, de 36 años y oriundo de Icononzo, Tolima también fue víctima de homicidio en Patía Cauca, elevando la cifra de excombatientes que han muerto desde la firma del Acuerdo a 169. [(Lea también: Asesinan en Patía, Cauca al excombatiente Dago Galíndez)](https://archivo.contagioradio.com/asesinan-en-patia-cauca-al-excombatiente-dago-galindez/)

Noticia en desarrollo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
