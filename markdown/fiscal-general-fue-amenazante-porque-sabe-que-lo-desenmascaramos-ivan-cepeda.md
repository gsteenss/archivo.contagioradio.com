Title: Fiscal general fue amenazante porque sabe que lo desenmascaramos: Iván Cepeda
Date: 2018-11-28 12:43
Author: AdminContagio
Category: Nacional, Política
Tags: Congreso, corrupción, Fiscal Néstor Humberto Martínez, Odebretch
Slug: fiscal-general-fue-amenazante-porque-sabe-que-lo-desenmascaramos-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [28 Nov 2018] 

Más de 4 horas duró el debate de control político al Fiscal General Néstor Humberto Martínez, citado en el Congreso, encuentro que, para el senador Iván Cepeda, fue un escenario para **"hacer evidente ante la opinión pública que la actitud amenazante** del Fiscal, obedece al hecho de que se ha logrado desenmascarar su responsabilidad"

**El desenlace del debate**

Fueron cuatro los congresistas que citaron al debate de control político del Fiscal: Gustavo Petro, Jorge Robledo, Angélica Lozano e Iván Cepeda, que se encargaron de presentar argumentos para demostrar las relaciones entre Martínez y la corrupción de Odebrecht, exigiendo la renuncia del Fiscal, quien ratificó su intención de mantenerse en el cargo hasta que la Fiscalía, ente que lo investiga, genere un dictamen.

Para Cepeda, las posturas de Martínez **"rayaron en el cinismo"**, debido a  que agredió a la oposición y reconoció que se estarían haciendo seguimientos a la misma desde días previos al debate, Además, frente a  las nuevas pruebas que se presentaron para demostrar la inocencia del Fiscal, aseguró que son "nada solidas". (Le puede interesar: ["Coger al fiscal diciendo una verdad es una hazaña: Jorge Enrique Robledo"](https://archivo.contagioradio.com/coger-al-fiscal-diciendo-una-verdad-es-una-hazana-jorge-robledo/))

"El **Fiscal no logró demostrar que no conociera a fondo lo que estaba aconteciendo antes de llegar a la Fiscalía**, no logró demostrar que el doctor Pizano no fuera un investigador riguroso que le entregó a él pruebas y documentos sobre hechos puntuales que demostraron toda clase de irregularidades" aseveró Cepeda.

### **No se respetó el debate**

Los congresistas que citaron al debate manifestaron que ninguno de sus derechos a replica fue respetado, mientras que la intervención de Cepeda no pudo ser escuchada porque su micrófono fue apagado y posteriormente se clausuró el debate, hechos que para el congresista ratifican el **"miedo al debate"**.

"Eso lo único que muestra **es el desespero, el miedo al debate y el intentar rehuir a toda costa de los argumentos que tiene la oposición en este Congreso**, para demostrar la responsabilidad que le cabe al Establecimiento político y económico sobre esta situación de corrupción generalizada" aseguró el senador. (Le puede interesar: ["¡Que renuncie el Fiscal"](https://archivo.contagioradio.com/que-renuncie-el-fiscal/))

De acuerdo con Cepeda el siguiente paso es la sesión que continuará el día de hoy, en la que se espera se respeten tanto los derechos a replica como al uso de la palabra para los congresistas. Asimismo aseguró que seguirán utilizando las herramientas que les da el Estado para llegar hasta el fondo de este entramado político de corrupción vinculado a la multinacional brasilera Odebrecth.

<iframe id="audio_30399953" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30399953_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
