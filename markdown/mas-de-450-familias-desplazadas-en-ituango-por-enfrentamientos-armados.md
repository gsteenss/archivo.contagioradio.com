Title: Más de 450 familias desplazadas en Ituango por enfrentamientos armados
Date: 2020-02-24 11:52
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Antioquia, Ituango
Slug: mas-de-450-familias-desplazadas-en-ituango-por-enfrentamientos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Desplazamientos-en-Ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @ejariasv {#foto-ejariasv .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/oscar-zapata-sobre-desplazamiento-masivo-hituango_md_48222907_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Oscar Zapata | integrante de la coordinación Colombia, Europa, Estados Unidos (COEUROPA)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este domingo 23 de febrero organizaciones sociales denunciaron el desplazamiento de 409 familias, constituidas por 815 personas del sector conocido como el Cañón del Inglés en Ituango, Antioquia. La situación se estaría presentando por una confrontación armada entre las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y las disidencias llamadas Nuevo Frente 18, por una pugna territorial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CceeuAntioquia/status/1231603350552162306?s=20","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CceeuAntioquia/status/1231603350552162306?s=20

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Óscar Zapata, integrante de la coordinación Colombia, Europa, Estados Unidos (COEUROPA) nodo Antioquia, explicó que mientras estaban en una reunión, a la que no asistió el alcande a pesar de haber sido convocado, en territorio se informó que habitantes de aproximadamente 12 veredas madrugaron para llegar al casco urbano de Ituango por una confrontación entre grupos armados ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos de los desplazados señalaron que sería el Nuevo Frente 18 el grupo que les ordenó que abandonara el territorio, para poder confrontar a las AGC. Entre los desplazados hay 40 líderes y lideresas sociales, además de 13 reincorporados de las FARC con sus núcleos familiares, aunque Zapata aclaró que ellos no hacían parte del Espacio Territorial de Capacitación y Reincorporación[(ETCR) de Santa Lucía](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-las-farc-en-el-etcr-de-santa-lucia-ituango/).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Por qué se presentó este desplazamiento masivo en zona rural de Ituango?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explicó el defensor de derechos humanos, el Cañón del que se desplazaron las familias es una zona estratégica que permite la movilidad hacia el occidente de Antioquia, al norte del departamento, al Nudo del Paramillo y al Bajo Cauca. Por lo tanto, las AGC quieren tener el control del lugar pero allí hacen presencia las Disidencias, por eso se producen los enfrentamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zapata agregó que a los intereses ilegales se suman el de la tenencia de la tierra, para proyectos minero energéticos, entre otros, que también hacen presencia en la zona. (Le puede interesar:["En el Bajo Cauca Antioqueño la violencia se está tomando el poder"](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

**"El responsable de los desplazamientos es el Estado"**
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El integrante de[COEUROPA](https://coeuropa.org.co/solicitud-de-activacion-de-alerta-temprana-para-las-veredas-del-corregimiento-santa-rita-municipio-de-ituango-por-posibles-casos-de-violacion-y-vulneracion-de-los-derechos-humanos-derechos-para-esta/) sentenció que el responsable de los desplazamientos es el Estado, porque no se entiende la estrategia para combatir a las estructuras armadas o incluso, si dicha estrategia existe. A ello se suma que hace poco la Defensoría del Pueblo había alertado sobre posibles desplazamientos en esa zona, pero fue un llamado ignorado por el Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zapata expresó que en Ituango no hay "ni siquiera un defensor comunitario", es decir que la Defensoría del Pueblo no tiene presencia en la zona, sometiendo este territorio a posibles violaciones de derechos humanos. En el mismo sentido, expresó que las personas desplazadas no están recibiendo la atención necesaria en términos de ayudas humanitarias para enfrentar su difícil situación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar:[Se libra una guerra por tomar el control del Nudo de Paramillo](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
