Title: Comisión Ética pide a la Ministra de Justicia escuchar a las prisioneras políticas del Buen Pastor
Date: 2018-11-19 17:03
Author: AdminContagio
Category: Mujer, Nacional
Tags: Cárcel Buen Pastor, Gloria Borrero, Ministra de Justicia, mujeres, Prisioneras políticas
Slug: comision-etica-pide-a-la-ministra-de-justicia-escuchar-a-las-prisioneras-politicas-del-buen-pastor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/carceles-y-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario Femenino ] 

###### [1 Nov 2018] 

La Comisión Ética, que visitó la semana pasada Colombia, **le pidió a través de un comunicado de prensa a la ministra de Justicia, Gloria Borrero, que entable un diálogo con las prisioneras políticas del pabellón 6 de la Cárcel del Buen Pastor** para poner fin a la huelga de hambre que adelanta y que ha llevado a 5 reclusas a graves estados de salud.

Según el texto, la Comisión se reunió con algunas de las voceras que representan al grupo de 16 prisioneras políticas, que les manifestaron que el cierre de su patio podría traducirse en un  castigo por su "identidad política"; motivo por el cual solicitaron que, "conforme a las decisiones de la Corte Constitucional sobre la situación carcelaria, los tratados internacionales suscritos por Colombia, **se adopten y apliquen los principios de derecho humanitario como mujeres"**. (Le puede interesar: ["Cinco prisioneras políticas en grave estado de salud por huelga de hambre en el Buen Pastor"](https://archivo.contagioradio.com/cinco-prisioneras-politicas-en-grave-estado-de-salud-por-huelga-de-hambre-en-el-buen-pastor/))

La huelga que ya completa una semana, ha buscado abrir una mesa de conversaciones con la Ministra de Justicia, para que dimita la orden sobre trasladar a las mujeres del pabellón sexto a un pasillo con celdas, como parte de una re estructuración del patio para madres gestantes, sin embargo, las prisioneras políticas han expresado que se puede adecuar sin restarles garantía a sus derechos humanos.

En ese sentido  la Comisión Ética manifestó que actue con prontitud en un diálogo con las mujeres del patio seis, y le solicitaron que pida la intervención del Comité Internacional de la Cruz Roja, **a la mayor brevedad posible, para sus gestiones humanitarias.** (Les puede interesar: ["Prisioneras políticas del Buen Pastor denuncian posible cierre de su patio"](https://archivo.contagioradio.com/prisioneras-politicas-del-buen-pastor-denuncian-el-posible-cierre-de-su-patio/))

###### Reciba toda la información de Contagio Radio en [[su correo]
