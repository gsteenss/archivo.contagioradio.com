Title: "Brigadas de EE.UU. en Colombia tiene potestad para asesorar grupos irregulares"
Date: 2020-06-11 16:37
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Brigadas de EE.UU., grupos ilegales, Política exterior
Slug: brigadas-de-ee-uu-que-llegaron-a-colombia-tienen-potestad-para-asesorar-grupos-irregulares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Ejercito-de-EE.UU_.-y-soberanía-de-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Brigadas EE.UU. /Creative Commons

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la llegada de las Brigadas de Asistencia de Fuerza de Seguridad estadounidenses a Colombia a principios de junio, senadores de la República expresaron que tras leer el manual de operaciones de esas Fuerzas Especiales, se puede concluir que dentro de las capacidad que tendría está unidad estará **permitido asociarse y asesorar a grupos ilegales en el país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de estos congresistas fue el senador [Iván Cepeda](https://archivo.contagioradio.com/miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda/) quién durante un debate de plenaria del Senado señaló que el manual, ***"menciona en forma explícita la función de asesorar milicias no gubernamentales y socios irregulares"***, lo que según él congresista, se podría interpretar como **dar permiso a estas fuerzas internacionales para que se asocien con grupos ilegales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma agregó, *"ese es el escenario que con la perversa complicidad de nuestro Gobierno estamos viendo surgir ante nuestros ojos"* y destacó que **estas tropas ya han sido desplegadas en Afganistán Europa y África y que esta sería la primera vez que lleguen a territorio latinoamericano.** Ante esta intervención señaló que de no tomarse medidas, se puede llegar a situaciones como las que se viven en naciones como ***"Libia, un estado destruido, convertido en un yacimiento de las multinacionales del petróleo".***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A estos señalamientos se unió también el senador **Wilson Aria**s quien a través de su cuenta de Twitter, compartió la carta de la **Security Force Assitance Brigade** , agregando que está lo que hará es permitir *"fortalecer grupos militares y trabajar con ellos"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1271111998869131264","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1271111998869131264

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Brigadas podrían intervenir desde Colombia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En el mismo debate se presentaron acusaciones por parte de la oposición a la entrada de esta brigada de seguridad, con argumentos que señalaban no solo los riesgos de su actuación, **sino también los posibles intereses de intervenir desde Colombia otros países, esto en referencia a un posible ingreso de la brigada a Venezuela.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Cepeda agregó, *"**la extrema derecha colombiana corrompe sectores del Ejército, busca poner la justicia al servicio de sus fines** o si es necesario, mutilarla para mantener su impunidad y sobretodo persistir en su obsesiva y frustrada acción destructiva del proceso de paz".* [(Lea también: Política exterior de Colombia obedece a intereses de EEUU: Congresistas)](https://archivo.contagioradio.com/politica-exterior-de-colombia-obedece-a-intereses-de-eeuu-congresistas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraparte el **Ministro de Defensa Carlos Holmes Trujillo**, sostuvo que *"**la presencia de los militares de EE.UU. no está relacionada con el tránsito de tropas hacia otro país, ni mucho menos a la invasión**; sino al contrario estos hacen parte de la cooperación internacional en términos militares".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante esta respuesta, y lo leído en la carta, congresistas señalan que se **ha sido violado el derecho fundamental a la participación política y *"el Gobierno desconoció el Congreso y concretamente al [Senado](https://twitter.com/SenadoGovCo) de la República***". [(Lea también: Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso)](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como consecuencia, se anunció que se presentará en **los próximos días una acción de tutela**, entorno a la presencia de las tropas extranjeras, y sus "intenciones de políticas de guerra" desconociendo los pactos internacionales y el mismo **Acuerdo de Paz.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
