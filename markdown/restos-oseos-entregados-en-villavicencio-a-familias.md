Title: Restos óseos de víctimas del conflicto armado serán entregados en Restrepo, Meta
Date: 2015-12-16 22:32
Category: DDHH, Nacional
Tags: conflicto armado, Desaparición forzada, proceso de paz, víctimas
Slug: restos-oseos-entregados-en-villavicencio-a-familias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/entrega-de-cuerpos-villavicencio1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [16 Dic 2015] 

[Este jueves La Fiscalía General de la Nación **entregará los restos óseos de 29 personas que fueron inhumadas en condición de no identificadas** en 5 cementerios municipales de los Llanos Orientales, las víctimas fueron reportadas por el Ejército Nacional como muertas en combate. Esta acción humanitaria se adelanta en el marco de los acuerdos pactados en octubre entre el Gobierno y las FARC-EP, para **aliviar el sufrimiento de las familias de quienes han sido desaparecidos en el contexto y en razón del conflicto armado**.]

[Familias provenientes de distintos municipios del país tendrán consigo los cuerpos de sus esposos, padres, tíos y hermanos de quienes no tienen noticia desde hace décadas, **buscaran la verdad frente a muchos de sus interrogantes y exigirán al Estado el reconocimiento de su responsabilidad en la muerte de sus familiares**. "Llevábamos 20 años sin saber de mi hermanito, hace 1 año nos llamó La Fiscalía, hoy nos dice que lo encontraron vestido con un camuflado, pero no nos mostraron sus prendas, esperamos nos digan la verdad", asegura familiar de una de las víctimas.]

[El acto público de '**Reconocimiento y Dignificación a Víctimas del Conflicto Armado**', se realizará en Restrepo, Meta desde las 9:00 a.m, contará con la presencia del Alto Comisionado para la Paz, el Vicefiscal General, la Unidad para la Atención de las Víctimas y organizaciones de derechos humanos acompañantes de las familias, quienes han organizado **un acto de memoria en reivindicación de sus familiares en el que presentarán sus nombres, fotografías, objetos significativos** y mensajes de exigencia de verdad y justicia en cada uno de los casos.]
