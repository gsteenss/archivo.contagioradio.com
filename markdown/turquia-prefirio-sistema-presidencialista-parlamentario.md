Title: Turquía prefirió el sistema presidencialista sobre el parlamentario
Date: 2017-04-18 06:51
Category: El mundo, Política
Tags: erdogan, Turquía, Unión europea
Slug: turquia-prefirio-sistema-presidencialista-parlamentario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/erdogan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ekathimerini.com 

###### [17 Abr 2017]

Tras 9 meses de un fallido golpe de Estado contra el presidente de Turquía, Recep Tayyip Erdogan, el orden político de ese país ha dado un giro este domingo con la realización de un referendo. Luego de la votación, el sistema de **Turquía dejará de basarse en un gobierno parlamentario para ser presidencialista,** lo que quiere decir que el poder recaerá casi completamente sobre Erdogan.

**Un 51,3% de los votos a favor del nuevos sistema presidencialista, frente al 48,7% del "No",y con el 99% de los sufragios** contados, Erdogan asegura que estos cambios en la Constitución turca promoverán un país más fuerte y contribuirán en la lucha contra militantes kurdos y el autodenominado Estado Islámico. Sin embargo, **los resultados definitivos se anunciarán en 12 días.**

El sistema presidencialista aprobado por un poco más de la mitad de los turcos, significa que el parlamento ya no será el que tenga mayor poder de decisión, sino que será el presidente, por ende, podrá disolver el Parlamento, declarar el estado de emergencia, nombrar y despedir a ministros, y funcionarios del máximo órgano judicial, también podrá emitir decretos sin necesidad de que estos sean o no aprobados por los congresistas, y además da la posibilidad al actual presidente turco de **permanecer en el poder hasta 2029** **si llega a ganar las elecciones 2019 - 2024.**

### Reacciones 

La oposición al gobierno turco ha denunciado irregularidades en la realización del referendo y ha advertido **sobre la instauración de un sistema totalitarista,** por lo que aseguró que impugnarán el resultado, no obstante, al continuar el estado de emergencia es inviable que existan movilizaciones en las calles. También cabe resaltar que en el marco de la jornada del domingo, **se vivió un tiroteo en una mesa electoral en Diyarbakir, que es una provincia en su mayoría, donde fallecieron tres personas.**

Para occidente, y específicamente la Unión Europea, este resultado se ha señalado como la muerte de la democracia en Turquía. Asimismo expresan que con un margen tan estrecho, que ha dejado al país polarizado, es esencial que se tenga especial cuidado con las siguientes decisiones que se tomen con el nuevo sistema. Angela Merkel, la canciller de Alemania  insta a evitar la división. Por su parte Donald Trump felicitó este lunes al presidente turco.

### Anuncios 

Durante su discurso de victoria en Estambul, Recep Tayyip Erdogan dio luces sobre un **posible plebiscito para que Turquía vuelva a la pena de muerte,** suspendida en el desde 2002.

Otro de los anuncios de Erdogan  es la posible solicitud de **un nuevo referendo para que el pueblo turco decida si quiere o no que su país haga parte de la  Unión Europea**. Cabe recordar que Turquía mantiene relaciones con la UE desde 1963. Luego en 1987 solicitó su adhesión formal, y las conversaciones empezaron en 2005, pero desde 2007 esas negociaciones se encuentran paralizadas tras el conflicto de Chipre.

“A la vista del resultado de la consulta y de las profundas implicaciones que suponen las reformas constitucionales, hacemos un llamamiento (...) a las autoridades turcas a buscar un consenso nacional lo más grande posible”, dijo el presidente de la Comisión Europea, Jean-Claude Juncker, la jefa de la diplomacia europea, Federica Mogherini, y el comisario de Ampliación, Johannes Hahn.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
