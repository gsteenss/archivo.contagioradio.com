Title: Marcha por la Dignidad arribará el 20 de julio a Bogotá buscando una «verdadera independencia»
Date: 2020-07-18 11:11
Author: PracticasCR
Category: Actualidad, Nacional
Tags: 20 de julio, Marcha por la dignidad, Movilización social, Ruta Libertadora
Slug: marcha-por-la-dignidad-arribara-el-20-de-julio-a-bogota-buscando-una-verdadera-independencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Marcha-por-la-Dignidad-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Marcha por la Dignidad / Trochando Fronteras

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Dos rutas de la Marcha por la Dignidad, iniciada el pasado 25 de junio, se sumaron a la movilización social emprendida por comunidades y organizaciones sociales**, con el fin de exigirle medidas al Gobierno para combatir las problemáticas que afectan sus territorios, especialmente los asesinatos y demás vulneraciones a los Derechos Humanos de las que son víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ambas caminatas se emprendieron el pasado lunes. **La denominada «Ruta Libertadora» partió desde Gibraltar, municipio de Toledo, Norte de Santander, en tanto que la «Ruta Comunera» lo hizo desde Barrancabermeja, Santander.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dos grupos de caminantes por la vida y la dignidad se encontrarán en la ciudad de Tunja,  para continuar rumbo a Bogotá, donde prevén llegar el próximo lunes 20 de julio «Día Nacional de la Independencia». (Lea también: [Movilización social sobrevive a la pandemia](https://archivo.contagioradio.com/movilizacion-social-sobrevive-a-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vale recordar que estos recorridos se suman a la **Marcha por la Dignidad inicial que partió el pasado 25 de junio desde Popayán, Cauca en la que los caminantes transitaron más de 590 kilómetros** arribando a Bogotá el viernes 10 de julio. (Le puede interesar: [Todo lo que necesita saber sobre el avance de la Marcha por la Dignidad](https://archivo.contagioradio.com/a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Ha sido a partir de la movilización social que hemos podido defender los derechos fundamentales, este derecho constitucional  ha sido necesario y efectivo en hacer cumplir los demás derechos que como pueblo y como individuos tenemos**»
>
> <cite>Sonia López, integrante de la Ruta Libertadora de la Marcha por la Dignidad</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/C_Pueblos/status/1284227575607418882","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/C\_Pueblos/status/1284227575607418882

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La Marcha por la Dignidad por una «verdadera independencia»

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Sonia López**, vocera del Movimiento Político de Masas Social y Popular del Centro Oriente de Colombia e integrante de la «Ruta Libertadora» de la Marcha por la Dignidad, afirmó a su llegada el día de hoy a Duitama, que **su objetivo como marchantes es «*levantar banderas de lucha en busca de una verdadera independencia*»**, ya que, es un deber con la memoria de quienes han sido asesinados en ejercicio de la lucha de las comunidades para que «*el pueblo sea soberano*» y así «*pueda ejercer en sus territorios autonomía, autodeterminación y poder popular*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Hemos querido retomar esa gesta y tomar el legado de nuestros campesinos, indígenas, afro y en general todos los hombres y mujeres que hacen parte de las clases populares y que se han visto en la necesidad de organizarse, movilizarse y luchar por sus derechos**»
>
> <cite>Sonia López, integrante de la Ruta Libertadora de la Marcha por la Dignidad</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otra parte, Sonia aseguró que si bien han cambiado los actores de dominación, y quizá ya no se está propiamente ante el «*yugo e invasión españoles*»; hoy en día Colombia padece las mismas circunstancias en las que **«*los grandes imperios norteamericano y europeo siguen en nuestros territorios apropiándose de nuestras riquezas, recursos naturales y sometiendo el pueblo a la miseria, a la explotación y a la exclusión social*».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El Estado es un instrumento de poder, que en el caso \[colombiano\] está al servicio de la oligarquía y de los intereses económicos transnacionales y en ese sentido la legislación y todo su accionar va encaminado a defender esos intereses foráneos por encima del bienestar de nuestro pueblo**»
>
> <cite>Sonia López, integrante de la Ruta Libertadora de la Marcha por la Dignidad</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por ello, insiste en que **hay que resaltar el papel de las comunidades campesinas, étnicas y populares en el desarrollo de proyectos de vida alternativos** al «*modelo de muerte* *que nos han querido imponer los gobernantes, \[un\] modo de producción capitalista que comercializa con la vida y la dignidad del pueblo*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hostigamientos por parte de la Fuerza Pública a las movilizaciones

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sonia, señaló que en Sácama, Casanare; **la Fuerza Pública quiso impedir el ingreso al municipio de la Marcha pese a que se tramitaron los permisos previos con la autoridad local.** También que desde Güicán hasta Duitama en Boyacá, sitio al que arribaron el día de hoy, habían tenido el seguimiento constante de la Policía monitoreando cada paso de la marcha.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido, **Erika Prieto, integrante de la «Ruta Comunera»** de la Marcha por la Dignidad, afirmó a su arribo a Motavita, que la movilización ha sufrido hostigamiento por parte de las autoridades, especialmente **la Policía de Tránsito y Transporte del Magdalena Medio que inmovilizó el bus en el que almacenaban sus equipajes y documentos, según Erika, sin siquiera permitirles retirar estos objetos del vehículo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, la marchante, señaló que al conductor le fue impuesto un comparendo siendo que la misma caravana y vehículo habían atravesado tres retenes previos sin que se les hubiera impuesto contravención alguna. (Lea también: [Abuso de la Fuerza Pública ha dejado 30 homicidios en medio de la movilización social en cuarentena](https://archivo.contagioradio.com/abuso-de-la-fuerza-publica-ha-dejado-30-homicidios-en-medio-de-la-movilizacion-social-en-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sebaquiropa/status/1282771556083605509","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sebaquiropa/status/1282771556083605509

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Además, Erika afirmó que una de las causas por las que la Marcha por la Dignidad fue convocada, era precisamente porque en los territorios sufren el hostigamientos de la Fuerza Pública y denunció particularmente los «*abusos y arbitrariedades*» que, según ella, llevan a cabo las Fuerza de Tarea Marte y Fuerza de Tarea Aquiles del Ejército Nacional en el sur de Bolívar y en el Bajo Cauca antioqueño respectivamente.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Parte de lo que venimos denunciando es la reconsolidación del paramilitarismo en nuestras zonas rurales gracias al apoyo que tienen del Ejercito Nacional y de la Policía**»
>
> <cite>Erika Priero, integrante de la Ruta Comunera de la Marcha por la Dignidad</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/900877967075430","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/900877967075430

</div>

<figcaption>
Otra Mirada: 210 años después... ¿Qué es la independencia?

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
