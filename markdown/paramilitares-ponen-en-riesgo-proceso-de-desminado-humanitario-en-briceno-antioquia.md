Title: Paramilitares ponen en riesgo proceso de desminado humanitario
Date: 2015-08-13 14:12
Category: Nacional, Paz
Tags: Autodefensas Gaitanistas, Conversaciones de paz de la habana, Cuarta Brigada, desminado humanitario, Empresas Públicas de, EPM, FARC, Juan Manuel Santos, Movimiento Ríos Vivos, Paramilitarismo
Slug: paramilitares-ponen-en-riesgo-proceso-de-desminado-humanitario-en-briceno-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desminado_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: septimadivisionmil.co 

###### [13 Ago 2015]

El Movimiento Ríos Vivos publicó un comunicado en el que afirma que el pasado 2 de Agosto, **paramilitares montaron un retén con más de 20 personas fuertemente armadas** y que se identificaron como “Gaitanistas” en la carretera que conduce a la vereda **“orejón” del Municipio de Briceño**,** zona incluida en plan piloto de desminado**,  en el marco de las conversaciones de paz entre el gobierno y las FARC.

Según la denuncia en el puesto de control paramilitar fueron detenidas unas **200 personas que viajaban en 4 buses “escalera” durante más de 20 minutos.** Otra de las situaciones preocupante es que las veredas y municipios aledaños al sitio y en donde se realiza el proceso de desminado hay una fuerte militarización por parte de la cuarta Brigada del Ejército.

El **Movimiento Ríos Vivos** agrega que se están presentando constantes violaciones a los DDHH de los pobladores de la región por parte de los militares que están permanentemente hostigando y fotografiando a la gente porque según ellos ***“es para tener un álbum para cuando se desmovilice la guerrilla señale los colaboradores”*** relata el comunicado.

Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos afirma también que para los pobladores de Briceño es muy extraño que **se esté realizando el desminado en los predios privados pertenecientes a Empresas Públicas de Medellín, EPM**, y no a las fincas de los pobladores que están contaminadas también con material sin explotar.

Acá el comunicado completo

[Reten Paramilitar Pone en Riesgo Desminado en Orejon](https://es.scribd.com/doc/274425445/Reten-Paramilitar-Pone-en-Riesgo-Desminado-en-Orejon "View Reten Paramilitar Pone en Riesgo Desminado en Orejon on Scribd")

<iframe id="doc_12457" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/274425445/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
