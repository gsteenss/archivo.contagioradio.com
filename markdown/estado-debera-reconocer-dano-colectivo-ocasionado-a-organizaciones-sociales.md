Title: Estado deberá reconocer daño colectivo ocasionado a organizaciones sociales
Date: 2017-12-01 12:53
Category: DDHH, Nacional
Tags: Amenazas a defensores de Derechos Humanos, defensores de derechos humanos, Redepaz, Reparación colectiva
Slug: estado-debera-reconocer-dano-colectivo-ocasionado-a-organizaciones-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/pazpazpaz-e1512150798177.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [01 Dic 2017] 

Por primera vez, **el Estado deberá reconocer la condición de víctimas de los movimientos sociales, sindicales e indígenas** contra quienes se realizaron asesinatos, masacres y estigmatización por parte de los grupos armados y las mismas Fuerzas del Estado. La Red Nacional de Iniciativas Ciudadanas por la Paz y Contra la Guerra (REDEPAZ), en conjunto con 15 organizaciones sociales más, harán parte del acto de dignificación y reparación colectiva que se desarrolla en el país.

Redepaz como organización y movimiento social, ha venido trabajando por la consecución de la paz y la defensa de los derechos humanos teniendo en cuenta a los ciudadanos y las comunidades de diferentes territorios. Por esto, en el marco del conflicto armado, algunos integrantes de este movimiento **fueron asesinados, amenazados, desplazados** y obligados a exiliarse.

Esto generó, según Redepaz, “estigmatización, pérdida o ruptura de procesos organizativos con las comunidades en los territorios, **pérdida de confianza en el Estado**, limitación de acciones de democracia participativa y afectaciones psicosociales”. Estos daños colectivos, los hace sujetos de reparación colectiva por parte del Estado quien también deberá garantizar la no repetición. (Le puede interesar: "[Violencia contra líderes sociales y defensores es el mayor obstáculo para la paz"](https://archivo.contagioradio.com/a-mayor-posibilidad-de-participacion-mayor-es-la-violencia-contra-lideres-informe/))

### **Estado tiene el deber de reconocer los daños colectivos ocasionados** 

La reparación colectiva que deberá hacer el Estado, “implica un diálogo político entre la institucionalidad y la sociedad civil que conforma los sujetos de reparación en la perspectiva de recuperar niveles de confianza desde el diálogo alrededor de los hechos ocurridos”. Esto debe servir para que **se fortalezcan las garantías de no repetición** y se satisfaga el derecho a saber la verdad que tienen las víctimas y la sociedad en general.

De acuerdo con Luis Emil Sanabria, director de Redepaz, “algún día, en medio del conflicto armado, **alguien creyó que éramos peligrosos** y decidió atacarnos y romper nuestra organización”. Ante esto, con la ley 1448 de 2011, el Estado reconoció que parte de ese daño lo había hecho el Estado. Enfatizó en que no se trata solamente una reparación para el movimiento sino también para las organizaciones sindicales, comunales e indígenas que también han sido estigmatizadas y violentadas.

Además, indicó que en la reparación debe existir un plan para que las organizaciones que son sujetas de reparación colectiva, vuelvan a estar “mínimamente en las condiciones en que estábamos antes de que estos grupos armados y el **Estado decidiera hacernos daño**”. Por esto, han propuesto realizar un acto de dignificación sujetos sociales y políticos. (Le puede interesar ["Entre 2009 y 2016 han sido asesinados 458 defensores de derechos humanos en Colombia"](https://archivo.contagioradio.com/entre-2009-y-2016-han-sido-asesinados-458-defensores-de-dd-hh-en-colombia/))

### **Estado debe proteger a los defensores de derechos humanos** 

Ante el aumento significativo en los asesinatos contra líderes y defensores de derechos humanos, Sanabria estableció que “una de las tareas fundamentales que tiene el Estado es **proteger a los defensores y garantizar la no repetición de los hechos violentos** contra las organizaciones sociales constructoras de paz”. Por esto, en el marco del Acto de Dignificación le harán saber al Estado la necesidad urgente de crear mecanismos para garantizar la vida de quienes trabajan por la defensa de la vida.

El Acto de Dignificación se llevará a cabo el **2 de diciembre de 2017 en el Auditorio Félix Restrepo de la Universidad Javeriana** en Bogotá a las 4:30 pm. Allí, estarán presentes las demás organizaciones sujetos de reparación colectiva en donde el Estado reconocerá la labor histórica que, junto con Redepaz, han realizado por la defensa de los derechos humanos.

<iframe id="audio_22403854" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22403854_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
