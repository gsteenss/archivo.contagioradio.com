Title: La actitud contradictoria de muchos creyentes ante la reconciliación
Date: 2019-08-02 13:00
Author: A quien corresponde
Category: Opinion
Tags: Religión
Slug: la-actitud-contradictoria-de-muchos-creyentes-ante-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***No hagas a nadie lo que no quieres que te hagan.***

[(Tobías 4, 15)]

[Bogotá, 29 de julio 2019]

*[El amor no se impone por la fuerza, la autoridad o el poder.]*

*[El mensaje de Jesucristo es armonía, verdad, justicia, y amor. ]*

[Estimado]

**Hermano en la fe**

[Cristianas, cristianos, personas interesadas]

[Cordial saludo, ]

[Recordando diálogos y discusiones contigo, con personas cercanas a tu manera de ser y pensar,  personas cercanas a la fe, me sorprendo con las contradicciones entre lo que pensamos, decimos y hacemos.]**Contradicciones**[ muy evidentes pero que no reconocemos; más aún, creemos que no existen y se lo hacemos creer a los demás. Explico lo que quiero decir. ]

[La mayoría de cristianos y cristianas, que se llaman a sí mismos practicantes, convertidos o fieles dicen que la Palabra de Dios, la Sagrada Biblia es el centro de sus vidas, la llevan a todas partes, la leen frecuentemente y afirman ponerla en práctica, la citan en encuentros y reuniones civiles y sociales; dicen que todo lo hacen en nombre del Señor Jesús, lo nombran constantemente en sus oraciones, reflexiones, celebraciones y todo tipo de conversaciones. Todo muy bien, valioso y respetable. ]

[Muchos de nosotros hemos escuchado, leído o conocido]**mensajes**[, por diferentes medios, creados, reproducidos y trasmitidos por personas religiosas y creyentes,que invocan a Dios constantemente; mensajes que estimulan odios,racismos,fanatismos y el rechazo o discriminación a personas por razones ideológicas, sexuales, políticas o sociales; nos hemos encontrado con personas muy religiosas y poco respetuosas con quienes no comparten la manera de vivir sus creencias y convicciones; a veces el diálogo con estas personas genera temor religioso y social, malestar y disgusto, cuando se esperaría que motivaran para ser mejores personas y ciudadanos, para saber más de Dios y para cuestionar nuestra manera de vivir, de consumir, de respetar la naturaleza, de relacionarnos con los demás. ]

**La contradicción** [se reconoce cuando leemos con atención la Palabra de Dios y la ubicamos en el contexto en el que fue escrita, cuando escuchamos atentamente los Evangelios que nos hablan de los pensamientos, palabras y actuaciones de Jesús de Nazaret y lo relacionamos con el pensar, decir, y hacer de quienes nos decimos sus seguidores porque la fe se muestra con la manera de vivir, con las obras, dice el apóstol Santiago: “]*[Lo mismo pasa con la fe que no va acompañada de las obras, está muerte del todo. Uno dirá: tú tienes fe, yo tengo obras: muéstrame tu fe sin obras, y yo te mostraré por la obras mi fe”]*[(Santiago 2, 17 – 18).]

[En la historia reciente de Colombia, hemos visto y escuchado a creyentes escandalizados porque una guerrillera y un policía bailaron en una fiesta de fin de año, porque hay víctimas que se han encontrado con victimarios y han empezado un camino de reconciliación, porque la mayoría de las víctimas del conflicto armado, muchas de ellas no religiosas,  están dispuestas a perdonar a sus victimarios con la condición que digan la verdad de lo que pasó con sus familiares, de los autores materiales e intelectuales, de los determinadores y beneficiarios de la violencia. A estos creyentes no parecen importarle mucho las vidas de militares, policías, guerrilleros y civiles que se han salvado, gracias al “proceso de paz” con las FARC y a los diálogos con el ELN, y lo más contradictorio es que lo hacen en nombre Dios.]

[En nombre de Dios se oponen a la posibilidad de un proceso de reconciliación que asuma la verdad del conflicto armado colombiano en todas sus dimensiones, afectaciones, causas, beneficiarios y determinadores; en nombre de Dios van cerrando el camino para la construcción de una paz estable y duradera, fruto de la justicia social, la honestidad, fruto de una verdad que recoja todas las verdades; fruto de transformaciones profundas en las relaciones de los seres humanos consigo mismos, con la sociedad, con la naturaleza y con el Dios de Jesús de Nazaret. ]

**Esta contradicción**[ se ha legitimado con mentiras proclamadas constantemente en diversos medios por personas de la vida pública; con afirmaciones sin fundamento o mentirosas por parte de generadores de opinión con altavoces de gran poder; se ha legitimado por reconocidos personajes con pasados corrupto, delictivos y cuestionamientos éticos, que sin arrepentimiento público, sin reconocer su responsabilidad y por la ausencia de sanción social siguen con un papel protagónico en la vida del país. No olvidemos que una mentira repetida cientos de veces se vuelve verdad. “Miente, miente que algo quedará”, dice un dicho popular. ]

**Para tomar conciencia de la magnitud de la contradicción**[, en primer lugar, preguntémonos, ¿es posible imaginarnos al Jesús de Nazaret, quien murió perdonando a quienes lo estaban torturando, crucificando y asesinando, con las mismas actitudes de quienes usando el nombre de Dios se opone al camino de la verdad y la justicia restaurativa que lleva a la reconciliación?.]

[En segundo lugar, ¿cómo entender que cristianos y cristinas, que decimos leer la Palabra de Dios y ponerla en práctica, no la tengamos en cuenta cuando nos dice:]*[“Y todo es obra de Dios, que nos reconcilió con él por medio de Cristo y]****nos encomendó el ministerio de la reconciliación****[”]*[? (2 Corintios 5,18) ]

[En tercer lugar, ¿cómo creerle a alguien que se dice cristiano en Colombia y reproduce odio, discriminación, violencia, mentiras, injusticia  y oposición a la paz, mientras la Palabra de Dios dice:]*[“No devuelvan mal por mal ni injuria por injuria, al contrario bendigan, ya que ustedes mismos han sido llamados a heredar una bendición”]*[ (1 Pedro 3,9); “]*[Bendigan a los que los persiguen, bendigan y no maldigan nunca… A nadie devuelvan mal por mal, procuren hacer el bien delante de todos los hombres”]*[ (Romanos 12,14, 17);]*[“Cuidado, que nadie devuelva mal por mal; busquen siempre el bien entre ustedes y con todo el mundo”]*[ (1 Tesalonicenses 5,15);]*[“Si tu enemigo tiene hambre, dale de comer; si tiene sed, dale de beber”]*[ (Proverbios 25,21); “]*[No guardarás odio a tu hermano… No serás vengativo ni guardarás rencor a tu propia gente. Amarás a tu prójimo como a ti mismo. Yo el Señor]*[(Levítico 19, 18-19)?.]

[Fraternalmente, su hermano en la fe,]

[Alberto Franco, CSsR, J&P]

[[francoalberto9@gmail.com]](mailto:francoalberto9@gmail.com)
