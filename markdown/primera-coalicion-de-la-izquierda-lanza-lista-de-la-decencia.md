Title: Primera coalición de la izquierda lanza Lista Unitaria de la Decencia
Date: 2017-12-04 17:22
Category: Nacional, Política
Tags: Candidatos al congreso, Congreso de la República, Gustavo Petro, lista de la decencia, Senado y Cámara
Slug: primera-coalicion-de-la-izquierda-lanza-lista-de-la-decencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQNoVFDXkAEFrKE-e1512425891209.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Prensa Rural ] 

###### [04 Dic 2017] 

Los partidos políticos y movimientos ciudadanos MAIS, ASI, Todos Somos Colombia, Progresistas, la Unión Patriótica y Colombia Humana, presentaron en rueda de prensa la Lista Unitaria de la Decencia; **una alianza que busca llegar al Congreso de la República en 2018.** Invitaron al partido Verde y al Polo Democrático a hacer parte de esta alianza para formar un contrapeso a los partidos tradicionales y “recuperar la confianza en el legislativo”.

Clara López, precandidata presidencial, indicó que la lista unitaria que van presentar al Senado y la Cámara, **“tiene la doble característica de ser abierta a los electores para que escojan su preferencia y abierta también a nuevas y más amplias personalidades”.** Dijo que Colombia está pidiendo alianzas “para darle mandato a una bancada firme, limpia y comprometida que defenderá los Acuerdos de Paz y buscará saldar la deuda social con el pueblo colombiano”.

<iframe src="https://co.ivoox.com/es/player_ek_22446874_2_1.html?data=k5ehlpuce5Whhpywj5WWaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nVw5C0qdPVzdnOh5enb8ri1crU1MbSuMafxcrZjdXFttXdxdSY0tTQaaSnhqax1s7Hs4zBoq7Aj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, Martha Peralta, integrante del partido político MAIS, manifestó que el partido indígena **apoyará la candidatura presidencial de Gustavo Petro** porque “es una candidatura cercana a la defensa de la madre tierra y del medio ambiente”. Afirmó que solo a través de una coalición “será posible transformar la realidad del país”. (Le puede interesar: ["Los cuestionamientos a las candidaturas presidenciales por firmas según la MOE"](https://archivo.contagioradio.com/recoleccion-de-firmas-para-elecciones-2018-debilita-la-democracia-moe/))

Peralta le dijo a Contagio Radio que la alianza va dirigida a las personas y movimientos alternativos del país que **buscan oxigenar el Congreso de la República**. Manifestó que “hay que convencernos de que necesitamos una transformación que involucre a las mujeres y los jóvenes y que incluya a los pueblos indígenas”.

### **Aún no hay alianza presidencial que represente a estos movimientos** 

Gustavo Petro, candidato presidencial por el movimiento Colombia Humana, enfatizó en que esta alianza “es un primer paso para quitarle a las **mafias de la corrupción en Colombia el Congreso de la República** y por tanto la creación de la ley”. Dijo que debe ser la ciudadanía la que haga parte del “parlamento decente” para que se construya un programa común.

En cuanto a las candidaturas presidenciales, dijo que “la idea no es dar pasos ciegos que generen dudas o decepciones porque sí puede haber un aglutinamiento amplio a la presidencia de la República con un programa común”. Anunció que la lista de candidatos al Congreso, **hace parte de una primera iniciativa** “que implica que quien quiera la paz y las transformaciones de Colombia, sólo tengan un candidato a la presidencia”.

### **Comunidad LGBTI busca representación en el Congreso de la República** 

<iframe src="https://co.ivoox.com/es/player_ek_22447156_2_1.html?data=k5ehlpyVeZehhpywj5WbaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncbXV1c7O0MaPlMqZpJiSpJbJttDnhpewjcjFssXdxcbhw5DFsIznxtPOxtSPkMrn1caYt9PNuMLmysaYxsqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Tatiana Piñeros, candidata al senado por esta alianza, manifestó que, de llegar al Congreso de la República, “vamos a poder hacer un control político honesto y eficiente para velar porque los recursos de la gente se ejecuten de la mejor forma”. Dijo que, si bien representa al sector LGBTI, **no es esta su única bandera**. Afirmó que el Congreso en la actualidad se ha apartado de las luchas por la diversidad y “no han legislado para garantizar los derechos de estas personas”. (Le puede interesar: ["El riesgo de los Acuerdos de Paz en las elecciones de 2018"](https://archivo.contagioradio.com/el-riesgo-de-los-acuerdos-de-paz-en-elecciones-presidenciales-2018/))

Además, afirmó que es necesario que desde el Congreso se ejecute una **ley de identidad de género** que le permita a las personas LGBTI “reconocernos como ciudadanos y que la sociedad nos abra los espacios”. Dijo que la sociedad colombiana ha confinado a las personas trans a la prostitución y no hay garantías para que estas personas se desenvuelvan en los diferentes sectores de la sociedad. “Necesitamos que la gente se concientice sobre que somos personas comunes y corrientes”.

Piñeros manifestó que, con la caída de la reforma política, **se cerró la puerta a los movimientos políticos** que no tienen participación política por lo que “nos tocó revisar quién nos podía avalar en esta alianza de la Decencia”. Por esto, los movimientos sociales alternativos, optaron por hacer parte de esta unión en donde con un programa común, puedan representar las diferentes realidades.

### **Estos son algunos de los candidatos al Congreso de la lista de la Decencia** 

Olmedo López, Rafael Ballén, Alba Luz González, Rosario Pinto, Máximo Noriega, Valmiro Rangel, Orlando Salcedo, Argemiro Burbano, Ricardo Mantilla, Camilo Hernández, Rubén Darío Sánchez, Pedro Esquivel, Luis Evelis Andrade, Feliciano Valencia, Milena Estrada, Arelis Uriana, Juvenal Arrieta, Abel David Jaramillo, Aida Avella, Román Vega, Gustavo Bolívar, Edgar Papamija, Bruno Díaz, Diego Vélez, Hollman Morris, Tatiana Piñeros, Gloria Flórez, Ana Teresa Bernal, Augusto Campo, Francisco Sumaqué, padre Gabriel Gutiérrez, David Rasero, Gonzalo Díaz, José Cuesta, Alberto López, Andrés Charry.

**Natalia Parra**, directora de la plataforma Alto, fue nombrada dentro de la lista de la Decencia. Sin embargo, ella misma ha negado la posibilidad de una nueva candidatura.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
