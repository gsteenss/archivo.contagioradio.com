Title: Y como por arte de mafia, la Lista Clinton parió un cartel de abogados
Date: 2015-03-12 12:52
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: bandas criminales, Carteles medellin, DEA, lista clinton, Medellin, narcotrafico, paraíso criminal
Slug: y-como-por-arte-de-mafia-la-lista-clinton-pario-un-cartel-de-abogados
Status: published

#### Por **[Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/), [@[FdoQuijano]

Al pronunciar la palabra cartel se nos cruza por la cabeza, de forma casi inmediata, el **cartel de Medellín y el cartel de Cali**, haciéndonos asociar la palabra con algo siniestro y criminal, pero eso sí, muy lucrativo.

Hace más de 30 años hablamos de carteles relacionados con el narcotráfico, posteriormente asociados a estructuras paramilitares, volviéndose más sanguinarios y crueles, organizados así para fortalecer negocios legales e ilegales.

Hoy conocemos la existencia de carteles criminales que usurparon las tierras de las víctimas del conflicto armado. También sabemos de carteles en el Valle de Aburrá, vinculados a los ‘Urabeños’ y a la‘Oficina’, que controlan la venta de alcohol (adulterado y no) y de productos como huevos, arepas, enlatados, jabones y gas. Criminales que igualmente controlan los carteles del contrabando, préstamos (paga diario) y apuestas ilegales.

Días atrás nos enteramos que tras la venta de pañales y papel higiénico, también se escondía otro cartel, y que se podría estar repitiendo con la venta de azúcar, hierro y cemento. Empresas prestantes, nacionales o extranjeras, confabuladas para manipular los precios de estos productos a fin de obtener billonarias utilidades, enriqueciéndose a costa de acrecentar la pobreza del país.

Podríamos mencionar los carteles de la toga, los falsos testigos, la contratación oficial, la Dian paralela, la seguridad, el cartel de oficiales y, hasta hace poco, el cartel de las tres letras, el DAS. Estos acaparan riquezas delinquiendo, al punto de ser partícipes de asesinatos, y haciendo concesiones a pares criminales,[ ]{.Apple-converted-space}acumulando tal poder que logran evadir cualquier sanción judicial.

Y como si no bastaran, hace poco conocí la existencia de dos nuevos carteles, surgidos como por arte de mafia y compuesto por abogados renombrados educados y de buenas conexiones, decididos a ayudar a quienes podrían tener problemas legales, especialmente con los Estados Unidos.

El primero de ellos encontró su nicho de trabajo criminal en cierto tipo de población: personas que tuvieron problemas jurídicos en el pasado o familias que con algún familiar en la criminalidad, y por ese mal azar, estigmatizados y convertidos en sospechosos de complicidad en actividades delictivas, casi siempre narcotráfico.

Este cartel de abogados asecha a esta población, le presenta documentos filtrados por funcionarios de organismos de seguridad, como la Dipol, y enseña que estas personas figuran como miembros de alguna organización vinculada al narcotráfico. Aclaran que son informes de inteligencia, no investigaciones concretas, y proceden a pedir cifras hasta de mil millones de pesos a cambio de sacar sus nombres de la documentación. Si la persona o familia entrega el dinero, sólo será estafada; si no desembolsa, los abogados entregan los falsos informes a sus contactos en la Diran y la Dijin, para que estos soliciten investigación a un fiscal de extinción de dominios y arrancar así el viacrucis procesal.

El segundo cartel de abogados encontró su gallina de huevos de oro en la **Lista Clinton**, que no es otra cosa que una lista oscura - generada por[ ]{.Apple-converted-space}Estados Unidos- que incluye el nombre de personas, naturales y jurídicas, presuntamente vinculadas al narcotráfico.

Estos leguleyos se acercan a los grandes empresarios y les informan sobre la próxima publicación de una nueva Lista Clinton que haría estremecer a todo el país. Los juristas alertan a sus víctimas sobre una posible vinculación, al tiempo que les brindan la oportunidad de no dejarlos incluir en ella. Ofrecen contactar a sus pares americanos y que estos a su vez contactarán con agentes de la DEA a fin de evitar la vinculación.

Acá hay que entender que cualquier empresario, por inocente que sea, se asusta con este tipo de alerta: es imposible conocer completamente a todas las personas con las que se cierra un negocio, especialmente cuando son grandes empresas.

Como parte de la pantomima, al estafado se le cita a una reunión en Panamá con el abogado y los presuntos miembros de la DEA, y allí acordar y entregar el monto de la colaboración que la víctima debe aportar, que va desde 500 mil hasta 3 millones de dólares.

La sorpresa viene después, cuando el estafado investiga en Estados Unidos y se encuentra con que no existe, ni existió, requerimiento judicial en su contra, y que la documentación con la que fue atemorizado no era otra cosa que un papel con un falso *indictment*. La historia termina con un abogado manejando un *porsche* y varios bienes inmuebles a su nombre.

En resumen, Colombia, y por ausencia de voluntad política, sigue siendo un paraíso criminal, en donde hasta lo extranjero, en este caso la Lista Clinton, puede parir su propio cartel.
