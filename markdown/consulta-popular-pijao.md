Title: Consulta popular en Pijao busca impedir actividad minera
Date: 2017-07-04 13:10
Category: Ambiente, Nacional
Tags: consulta popular, Mineria, Pijao, Quindío
Slug: consulta-popular-pijao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/salinas-1-e1494957735234.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Poder Ciudadano.com.co] 

###### [04 Jul 2017] 

El 9 de julio se llevará a cabo la consulta popular en el municipio de Pijao en el Quindío. Los promotores de la campaña, que busca impedir los procesos de extracción de metales en el municipio, **esperan superar el umbral de 2,026 votos** y así defender los recursos naturales del lugar emblema del eje cafetero.

Para Mónica Flórez, miembro del Comité Ecológico y la Fundación Pijao CittaSlow, **“hemos recorrido todo el municipio, vereda por vereda para trabajar con las comunidades y** que ellas comprendan la importancia de salir a votar en la consulta popular”.

Florez manifestó que buscarán que un tercio de las 6,055 personas habilitadas para votar en Pijao lo hagan respondiendo no a la pregunta “¿Está usted de acuerdo, si o no, en que en el municipio de Pijao, se lleven a cabo procesos de minería de metal?” (Le puede interesar: ["Lista consulta popular en](https://archivo.contagioradio.com/lista-la-consulta-popular-de-mineria-en-pijao-quindio/)[[Pijao, Quindío"](https://archivo.contagioradio.com/lista-la-consulta-popular-de-mineria-en-pijao-quindio/))]

Flórez además manifestó que la Registraduría Nacional “**hizo un recorte de 17 a 6 mesas de votación argumentando que la consulta es un derroche de dinero**”. Sin embargo, ella destacó el entusiasmo general que hay en el municipio, desde las autoridades hasta los estudiantes, por el desarrollo de la consulta popular. “En conjunto con el grupo Marcha Carnaval de Quindío le estamos pidiendo a la gente que salga a votar temprano para que a pesar del recorte en las mesas logremos alcanzar el umbral”.

Adicionalmente, Pijao es un distrito minero que tiene hasta el momento 6 títulos mineros y 8 solicitudes mineras de oro, plata y Niquen de empresas relacionadas con Anglo Gold Ashanti. Ante esto Flórez aseguró que el Vice Ministro de Minas, Carlos Andrés Cante, **“ha estado en Pijao planteando una campaña por el si en la consulta popular”**.

Según ella, “no es gratuito que el Vice Ministro de minas venga, pues hay en cola solicitudes que necesitan del apoyo del Gobierno”. (Le puede interesar: ["Vía libre para la consulta popular minera en](https://archivo.contagioradio.com/via-libre-para-la-consulta-popular-minera-en-pijao/)Pijao["](https://archivo.contagioradio.com/via-libre-para-la-consulta-popular-minera-en-pijao/))

### **Pijao es el primer lugar de Latinoamérica que hace parte de la red CittaSlow** 

La consulta popular en Pijao es especialmente importante debido a que este municipio hace parte de los **153 lugares en el mundo que pertenecen a la Red Internacional CittaSlow** que significa “cuidad lenta” en italiano. Los miembros de esta Red se caracterizan por ser lugares que viven en comunión con la naturaleza, las tradiciones culturales y el patrimonio arquitectónico.

La preservación del entorno natural y la reactivación de las economías locales que se articulan alrededor de la familia, **se verían fuertemente afectadas con la presencia de grandes empresas mineras.** Estas afectarían el ritmo de vida desacelerado que hace parte del movimiento CittaSlow y pondrían en riesgo los ecosistemas de páramo, montaña y los afluentes hídricos.

<iframe id="audio_19621376" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19621376_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
