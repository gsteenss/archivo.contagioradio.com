Title: 3 imperdibles de 'La Candelaria' llegan al Teatro Colón
Date: 2016-06-28 16:32
Category: Cultura, eventos
Tags: Camilo obra teatral, Don Quijote Teatro, Si el rio hablara, teatro la candelaria
Slug: 3-imperdibles-de-la-candelaria-llegan-al-teatro-colon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Escena_Camilo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro La Candelaria 

##### [28 Jun 2016]

Como parte de la conmemoración de los 50 años del emblemático Teatro la Candelaria de Bogotá, tres de sus obras más importantes "El Quijote", "Si el río hablara" y "Camilo" llegan en el mes de julio al Teatro Colón en una nueva temporada.

![Afiche el Quijote](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Afiche-el-Quijote.jpg){.aligncenter .size-full .wp-image-25864 width="1500" height="1200"}

Un homenaje en 12 momentos al soldado, poeta, novelista y dramaturgo Miguel de Cervantes Saavedra y su incalculable legado a la literatura universal. Narrando las aventuras de ese personaje de apariencia física débil y mentalidad fantasiosa, que tiene la valentía para perseguir sueños y nobles ideales.

[![Afiche Si el río hablara](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Afiche-Si-el-río-hablara.jpg){.aligncenter .size-full .wp-image-25862 width="4372" height="2810"}](https://archivo.contagioradio.com/3-imperdibles-de-la-candelaria-llegan-al-teatro-colon/afiche-si-el-rio-hablara/)

[Una obra sobre la memoria](https://archivo.contagioradio.com/el-teatro-la-candelaria-apuesta-por-la-memoria-en-si-el-rio-hablara/) de las víctimas de la guerra, donde tres personajes quieren salir, a su manera, de esa zona gris que es la pérdida del sentido de la vida, ocasionado por una guerra interminable de la cual el Estado ha sido partícipe.

[![Afiche Camilo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Afiche-Camilo.jpg){.aligncenter .size-full .wp-image-25863 width="1582" height="1067"}](https://archivo.contagioradio.com/3-imperdibles-de-la-candelaria-llegan-al-teatro-colon/afiche-camilo/)

Una [combinación de teatro, elementos de performance, danza, video y música en vivo](https://archivo.contagioradio.com/camilo-torres-en-el-teatro-la-candelaria/). La historia narra las vivencias, pasiones, conflictos y convicciones que rodearon la vida del sacerdote y sociólogo Camilo Torres, quien al encontrar la estigmatización de sus ideas se sintió solo y abatido y casi sin avisarle a nadie se alistó en la guerrilla.

   
 
