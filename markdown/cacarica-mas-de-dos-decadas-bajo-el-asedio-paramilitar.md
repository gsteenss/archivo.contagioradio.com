Title: Cacarica, más de dos décadas bajo el asedio paramilitar
Date: 2020-01-09 11:40
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, cacarica, Chocó, Control Paramilitar
Slug: cacarica-mas-de-dos-decadas-bajo-el-asedio-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Zona-humanitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

La Comisión de Justicia y Paz reiteró las denuncias sobre el control paramilitar en medio de la presencia y el desarrollo de operaciones militares en el Chocó. Según la más reciente comunicación de la Comisión el 6 y 7 de Enero, unidades militares hicieron presencia en la **Zona Humanitaria Nueva Vida, territorio colectivo del Cacarica** y no realizaron ningún tipo de acción contra las unidades paramilitares que se mantienen de forma permanente en el territorio.

En la denuncia la organización de DD.HH. afirma que por lo menos 17 líderes están en alto riesgo de desplazamiento por las reiteradas amenazas de los paramilitares contra quienes han denunciado sus operaciones en los territorios. La situación se agrava cuando las unidades militares no ejercen ningún tipo de acción en contra de los paramilitares presentes en el territorio.  [(Lea también: Hay un plan para asesinar líderes en Chocó: Comisión de Justicia y Paz)](https://archivo.contagioradio.com/plan-para-asesinar-lideres-en-choco/)

### **El control paramilitar ya se ha denunciado en reiteradas ocasiones** 

Tanto la Comisión de Justicia y Paz como las propias comunidades han denunciado en reiteradas ocasiones la presencia paramilitar y más recientemente los mecanismos de control que las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) usan para permanecer en el territorio y amedrentar a las comunidades.

[Una de las primeras denuncias sobre estas nuevas formas de presencia se dieron en 2016 cuando arribaron a la región unos 120 paramilitares que fueron denunciados inmediatamente por la comunidad asentada en la Zona Humanitaria Nueva Vida.  [(Lea también: Cerca de 650 paramilitares ejercen control territorial en Cacarica)](https://archivo.contagioradio.com/650-paramilitares-ejercen-control-en-cacarica-35568/)]

Pobladores de Cacarica han señalado que los paramilitares obligan a toda la comunidad a asistir a reuniones en las que han prohibido en ocasiones que sus pobladores se movilicen luego de las 6:00 pm, haciendo énfasis en la intención que tienen de tomarse el control territorial de las comunidades, hechos que se suman **a los allanamientos casa por casa y amenazas de las que han sido víctimas** los y las habitantes del lugar humanitario

Según la organización de DD.HH. en Cacarica también se han dado reiteradas intimidaciones contra las mujeres, se han saqueado los bienes de la población y se realiza constantemente **un control a la movilidad de los líderes y lideresas del territorio** que hacen parte de la Asociación de Familias de los Consejos Comunitarios de Autodereminación, Vida y Dignidad (CAVIDA).

Dichos hechos se presentan en medio de la presencia militar a menos de dos horas de los lugares humanitarios y a pesar de que las denuncias con ubicación concreta, dan cuenta de la presencia paramilitar, y el control territorial ejercido por  los armados, no se presenta ningún control por parte del Fuerza Pública.

### **Los mecanismos de control social por parte de paramilitares siguen vigentes** 

Uno de los mecanismos es de los llamados “Puntos” que son personas que se instalan en las poblaciones dotados con armas cortas y radios de comunicación a través de los cuales ejercen presión sobre la comunidad para que se adelanten las acciones que lleva a cabo su tropa y se logre consolidar como autoridades en el territorio en medio de la presencia militar pero la paradójica ausencia del Estado civil. [(Le puede interesar: Persisten incursiones paramilitares en el Bajo Atrato)](https://archivo.contagioradio.com/persisten-incursiones-paramilitares-en-el-bajo-atrato/)

En la denuncia de la organización de Derechos Humanos también se menciona que los paramilitares de las AGC, con censo en mano, ofrecieron regalos a los niños y niñas y cabezas de ganado para “aportar” en la celebración de las festividades. Este mecanismo, es otra estrategia de control en la que los paramilitares intentan comprar el silencio y cooptar los liderazgos.[(Le recomendamos leer: Con lista en mano paramilitares amenazan pobladores de Cacarica)](https://archivo.contagioradio.com/paramilitares-amenazan-pobladores-cacarica-36208/)

Como precedente, en mayo del 2018, se presentó ante la Comisión Interamericana de Derechos Humanos, evidencia que recopila testimonios de los jóvenes de la Zona Humanitaria Nueva Vida denuncian cómo esta facción del paramilitarismo **busca constantemente la forma de seducir y ofrecer dinero a jóvenes para que se unan a sus filas generando zozobra al interior de la comunidad.** [(Lea también: La estrategia del paramilitarismo para atraer a jóvenes del Chocó a sus filas)](https://archivo.contagioradio.com/la-estrategia-del-paramilitarismo-para-atraer-a-jovenes-del-choco-a-sus-filas/)

### **Comunidad Embera Juin Pubuur confinada por el Estado y los paramilitares** 

La comunidad de Juin Pubuur, indígenas embera que habitan desde hace más de medio siglo el territorio que se ha definido como parque los Katios se encuentra confinada por dos factores. Uno de ellos el avance de los cultivos de coca promovidos por los integrantes de las AGC presentes en esa zona. Por otro lado la delimitación del parque los Katios que pone a la comunidad en una disyuntiva sobre la titulación de su territorio.

Pese a que en junio de 2019, el juzgado noveno penal del circuito de conocimiento de Bogotá emitió un incidente de desacato interpuesto por la Comisión de Justicia y Paz contra diversas entidades del Estado como la Unidad de Restitución de Tierras, Fiscalía, Unidad de Víctimas y los ministerios de Salud, Relaciones Exteriores, Interior, Salud y Defensa, **quienes deben velar por la reparación integral de las comunidades y desarrollen planes para su proteccion, estos no han sido desarrollados**.  [(Le puede interesar: Juez exige cumplimiento inmediato de medidas de reparación a comunidades en Cacarica, Chocó) ](https://archivo.contagioradio.com/incidente-de-desacato-obliga-al-estado-a-cumplirle-a-las-comunidades-de-cavida/)

Al respecto, líderes sociales de la zona han señalado que incluso tras ser reconocida la responsabilidad del Estado  en  sucesos como la Operación Génesis ocurrida en 1997 y la posterior orden de la Corte Interamericana de Derechos Humanos de restaurar a las personas afectadas, **no se ha avanzado más allá de un 3%** indicando que para 2021, año en el que se cumplen los 10 años de la ley 1448 de 2011, – la cual dicta medidas de atención, asistencia y reparación integral a las víctimas del conflicto armado – aún quedará mucho por hacer y reparar, lo que dificulta que se cumpla con el mandato de la Corte IDH.

La Corte Interamericana de Derechos Humanos declaró en aquel entonces al Estado colombiano, internacionalmente responsables por haber incumplido su obligación de garantizar los  derechos e integridad de los integrantes de las comunidades,  situación que está sucediendo nuevamente.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
