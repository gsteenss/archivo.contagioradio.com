Title: Informe sobre torturas de agentes Estatales en medio del conflicto armado llegan a la JEP
Date: 2020-07-07 21:23
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Agentes, #Conflicto, #DD.HH, #Estado, #FARC-EP
Slug: informe-sobre-torturas-de-agentes-estatalesen-medio-del-conflicto-armado-llegan-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito.militares.militar.soldado.soldados.afp_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 7 de julio, la Corporación Solidaridad Jurídica hizo la entrega del informe **"Violencia Contrainsurgente"** a la Jurisdicción Especial para la Paz. Documento que da cuenta de violaciones a DD.HH **cometidas por agentes del Estado contra miembros de la entonces guerrilla FARC-EP** cuando fueron capturados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el informe estas acciones hicieron parte de una estrategia estatal antisubversiva , "que a través de la combinación del sistema penal y la Fuerza Pública, se centró en la lógica de la **eliminación del enemigo interno a través de prácticas como** tratos o penas crueles e inhumanas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las torturas en medio de la guerra
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este documento revela patrones y formas de operar que evidencian la macrocriminalidad llevada a cabo por agentes del Estado; así como el funcionamiento del aparato criminal dentro de las instituciones estatales, que avalaron la comisión de estas prácticas. (Le puede interesar: "[Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el documento señala las responsabilidades del Estado en relación con el derecho humanitario y la normativa internacional o el derecho internacional humanitario. De igual forma, **identifica e individualiza a los presuntos responsables a partir de la cadena de mando.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

La impunidad en los crímenes de Estado
--------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con la entrega de este informe, la Corporación Solidaridad Jurídica también extendió la petición a la JEP para que estos casos **no queden en la Impunidad**. Razón por la cual afirma que finalmente buscan que se investigue y enjuicien los crímenes internacionales cometidos por agentes del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa misma vía, señalan que es importante que se **aplique una justicia simétrica**, en torno a todos los actores del conflicto armado en Colombia. Ello con la intensión de que no exista una justicia de "los vencedores sobre los vencidos". (Le puede interesar: ["Corporación Solidaridad Jurídica"](http://solidaridadjuridica.com/noticias/))

<!-- /wp:paragraph -->
