Title: Comienza recolección de firmas para reducir salario de congresistas
Date: 2017-01-02 13:33
Category: Nacional
Tags: colombia, Congresistas, salario minimo
Slug: comienza-recoleccion-firmas-reducir-salario-congresistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Razón 

###### [2 Ene 2017] 

[Una resolución de la Registraduría Nacional dio vía libre para que se inicie la recolección de firmas y pueda tramitarse en el Congreso un referendo constitucional que busca disminuir el salario de los senadores y representantes colombianos, uno de los más altos en América.]

[Se trata de una propuesta que pone en la palestra pública la desigualdad en Colombia. Mientras **un congresista obtiene mensualmente** **27.929.064 de pesos,** sin incluir beneficios como pago por seguridad y comunicaciones; **el salario mínimo subió para el 2017** **un 7%, es decir 48.262 pesos**. Cifras que dejan a la nación como [el tercer país de América Latina con el salario más bajo](https://archivo.contagioradio.com/salario-minimo-cada-vez-mas-minimo-en-colombia/) en comparación con la tasa de ingreso Per Capita, y la mantiene en el quinto lugar de los salarios más altos para los legisladores.]

[De tal manera, con la resolución la registraduría reconoció al comité promotor que busca convocar a los colombianos a este mecanismo de participación ciudadana. Una iniciativa que deberá reunir  **un millón 900 mil firmas en seis meses, y que de ser aprobadas,** darán paso para que el Congreso debata esa posibilidad.]

### ¿Qué sigue? 

[De acuerdo con el texto de la resolución, la propuesta cuenta con Juan José Santacruz Rodríguez, como vocero y responsable de las actividades,”administrativas, financieras, de campaña de la iniciativa, así como la vocería durante el trámite”.]

[Si se logra la recolección de las firmas, el siguiente paso sería la revisión por parte de la Corte Constitucional para que entregue su concepto sobre si ese referendo se ajusta a la ley. De ser así,]**se establecería una fecha para que los colombianos** [**voten Sí o No a la reducción de los salarios de los senadores y representantes**. El referendo será aprobado si la opción del Sí obtiene la mitad más uno de los votantes, siempre y cuando en la votación participe al menos la cuarta parte de los ciudadanos que componen el censo electoral.  [Formulario de recolección de firmas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Formulario-RECOLECCION-FIRMAS-RCA-2016-02-001.pdf)]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
