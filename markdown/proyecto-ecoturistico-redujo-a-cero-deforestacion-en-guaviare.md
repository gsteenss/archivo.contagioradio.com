Title: El proyecto ecoturístico que redujo a cero la deforestación en Guaviare
Date: 2019-04-22 17:54
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: deforestación, Guaviare, San José del Guaviare
Slug: proyecto-ecoturistico-redujo-a-cero-deforestacion-en-guaviare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-65.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ecoamem] 

En la vereda Bocas del Raudal, ubicada en el municipio de San José del Guaviare (Guaviare), familias campesinas se reunieron desde 2014 para emprender un proyecto ecoturístico que tendría como objetivo proveer una manera de subsistencia a las comunidades y a su vez, conservar los bosques de la Amazonía. En sus cuatro años de existencia, los resultados de esta iniciativa han sido sorprendentes: **en este municipio, donde se concentra 4,4% de toda la deforestación a nivel nacional, se redujo a cero la desaparición de estos ecosistemas**.

Esta propuesta, proveniente del **operador comunitario EcoAmem**, ha promovido una relación de beneficio mutuo entre las comunidades y el ambiente que aprovecha de los ecosistemas únicos de la región, donde confluyen la Orinoquia, la Amazonía y los Andes, para generar un desarrollo sustentable para una zona históricamente olvidada por el Estado. Oscar Hernandez, representante legal de EcoAmem, relata que en los años posteriores al Acuerdo de Paz se multiplicó el número de visitantes a la Amazonía que incluso en 2018, esta zona atrajo casi 17.000 visitantes.

La mayoría llegan con el sueño de visitar Cerro Pinturas o Cerro Azul, uno de los lugares con más pinturas rupestres del mundo. Para llegar, turistas viajan en transporte fluvial por el Río Guayabero desde la capital del departamento hasta la vereda Bocas del Raudal, unos 40 minutos de distancia donde lo recibirán los guías de EcoAmem. Acá, pueden encontrar hospedaje o un plato tradicional de la zona como lo es el pescado dorado y luego partir, acompañados por EcoAmem, por un camino a través de la selva húmeda donde encontrarán murales de pigmento rojizo y de más de 1.000 años de antiguedad, conocido localmente como Cerro Pinturas.

### **Un modelo diferente de turismo**

Hernandez resalta que toda la logística del proyecto turístico, la programación, la alimentación y el transporte, es acordado por las comunidades a través de Juntas de Acción Comunales y diferentes comités, lo cual contrasta con el modelo propuesto por la industria que normalmente llegan desde afuera de los territorios para imponer proyectos económicos sin la participación de los habitantes locales. Por tal razón, el representante legal sostiene que estos proyectos no logran los mismos resultados en términos de conservación en comparación con las iniciativas de las comunidades.

Los efectos de los proyectos de ecoturismo comunitarios son que "los recursos se terminan quedando en la región, la gente mejora su calidad de vida y comienza a abandonar prácticas que degradan el ambiente como la siembra de coca, la extracción de madera y la ganadería extensiva". Según Hernandez, la tala de árboles ha cesado en la vereda y en prados donde previamente se realizaba ganadería, ahora los habitantes emprenden procesos de recuperación de la selva nativa. "**Cuando vinculas a la gente que vive en los atractivos turísticos, ellos comienzan a conservar**", dijo el ambientalista.

Incluso, las comunidades han implementado unas normas ambientales que controlan las actividades que afectarían al ambiente como la pesca, la caza, la tala de árboles y la extracción de arena, que incluye un sistema de multas que sancionan con trabajo comunitario a ciudadanos que se encuentren responsables de efectos ambientales nocivos para los ecosistemas.

En consecuencia, esta vereda de Bocas del Raudal redujo a cero la deforestación, lo que significa para el ambiente la preservación de la diversidad de flora y fauna, mucho de ellos no han sido descubiertos o catalogados aún por los científicos. "La ciencia y las instituciones no conocen lo que hay allá, las comunidades sí", sostiene Hernandez, quien agrega que para "construir este relato" se requerirá el conocimiento de las comunidades, apoyados por la comunidad científica y el Estado.

<iframe id="audio_34807029" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34807029_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
