Title: La lucha por la que habría sido asesinado Temístocles Machado
Date: 2018-01-29 18:24
Category: DDHH, Entrevistas
Tags: asesinato de líderes sociales, Paro Buenventura, Temístocles Machado
Slug: temistocles_machado_lider_buenaventura_paro_asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/temisto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Elizabeth Otálvaro] 

###### [29 Ene 2018] 

[Tras el asesinato este sábado de Temístocles Machado, líder del paro cívico de Buenaventura, los demás líderes y todo el movimiento social se declararon en Asamblea permanente a la espera de respuestas por parte de la Unidad Nacional de Protección, UNP, y la Fiscalía.]

A este líder le silenciaron su vida este sábado hacia las 5 de la tarde, cuando sicarios en una moto le dispararon en tres ocasiones en el parqueadero de su propiedad, ubicado en el barrio Isla de la Paz. Aunque de inmediato la comunidad intentó auxiliarlo, al ser trasladado a la clínica Santa Sofía, falleció por la gravedad de las heridas.

Temístocles ya había sido víctima de amenazas y por eso, durante 6 años, contó con esquema de la Unidad Nacional de Protección. De hecho, en el marco del paro en Buenaventura se hizo una evaluación con la UNP, y **el líder social les manifestó que quería la protección colectiva, pero nunca desistió de la seguridad.**

La comunidad, como los demás líderes de Buenaventura, tienen como primera hipótesis que el asesinato se debe a las acciones en defensa de las tierras que lideraba Temístocles, y por lo cual, años atrás había sido obligado a tener un esquema de protección por parte la de la UNP. Sin embargo, el secretario de gobierno del municipio, Luis Fernando Ramos, dijo que descartaba que el asesinato se debiera a la labor social que adelantaba Machado.

Sin embargo, la defensa del territorio fue uno de los temas que atravesó las exigencias del paro cívico en mayo. De hecho, Jorge Cabra, coronel la policía de Buenaventura ha asegurado que **"El tenía amenazas por tierras, y que esto sería uno de los detonantes de lo que se presentó, había informado sobre unas amenazas".**

### **La lucha por tierra** 

Javier Torres, integrante del Comité del paro cívico de Buenaventura, recuerda a Temístocles como un líder "muy reconocido y compañero de lucha durante muchos años. Lo único que había hecho es defender la tierra", expresa. La noticia del asesinato del líder se conoció justo cuando se llevaba a cabo una asamblea general, la cual debió ser suspendida ante la triste noticia.

La vida de Temístocles se basó en la defensa de las tierras tanto de su familia como las de la comunidad. "Logró unir a la gente para que defendieran su territorio, hicieron un frente común en cabeza de él y logró arrebatarle tierras a grupos armados y empresarios que llegaron a amenazarlo. Además hicieron mingas y cordones humanitarios para que no les quitaran la tierra", cuenta Torres.

**El barrio Oriente y El Bosque, fueron algunos de los lugares donde fue más visible su lucha**. Gracias a su liderazgo, hoy en esos lotes hay construidas viviendas para las familias. Asimismo, en la Isla de la Paz, La Cima, El Jardín, entre otros lugares donde había una puja por esas tierras de las que se buscaban adueñarse empresarios como **Jairo Arturo Salamando, quien buscaba quitar las tierras de la Isla de la Paz.**

Incluso, según denuncia Torres, intentaron robar una carpeta donde se llevaba el registro de los procesos que lideraba Temístocles. Una persona, haciéndose pasar como funcionario de la Fiscalía intentó hacerse a la carpeta y los papeles que contienen la historia del proceso, incluso los nombres de los empresarios que se oponían al trabajo del lide, entre ellos Jairo Arturo Salamando, una persona que amenazó a los habitantes de Isla de la Paz con desplazamiento y hasta la muerte si no salían del barrio.

Además, el integrante del Comité del Paro cívico, manifiesta que el líder social había acabado de llegar de Bogotá con buenas noticias sobre los litigios que llevaba en defensa del territorio.

### **Cambia la forma de interlocutar con el gobierno** 

Ante esta situación, el Comité del Paro Cívico llama a las autoridades, para que se investigue quienes fueron los que asesinaron y quienes dieron la orden de disparar contra Temístocles. "Esto no es un caso aislado, no es un tema de líos de faldas, aquí no haya otro elemento distinto sobre la lucha por las tierras".

En los próximos días, desde el Comité se anunciará una serie de directrices y manifestaciones, para que quede claro que "**va a haber un cambio en la forma de interlocutar con el gobierno (...) Los líderes podrán ser asesinados, pero las ideas y las luchas seguirán, porque cuando cae un líder, nacen cien",** concluye Javier Torres. (Le puede interesar:[Buenaventura volverá a marchar por incumplimientos del gobierno)](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/)

<iframe id="audio_23428950" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23428950_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
