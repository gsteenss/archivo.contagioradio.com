Title: Minga por la dignidad del pueblo Embera desplazado en Bogotá
Date: 2018-07-27 15:44
Category: Nacional
Tags: Desplazamiento, embera, Minga Indígena
Slug: minga-embera-desplazado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/9-de-Abril-093_Bianca_web-L.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: PBI Colombia 

###### 27 Jul 2018 

Bajo la consigna "**Somos más que una comunidad habitando en las calles, somos oro vivo**", una iniciativa ciudadana busca solidarizarse con las cerca de **1000 personas del Pueblo indígena Embera que se encuentran en condición de desplazamiento** en la ciudad de Bogotá por cuenta del conflicto armado.

Reconociendo su valor cultural y su condición de víctimas, el Colectivo **ha propuesto una serie de estrategias**, entre las que se encuentran acciones participativas, a corto y mediano plazo, que **ayuden a mejorar las condiciones de vida de los Embera**; por lo menos mientras el Estado brinda garantías para que retornen a sus territorios ancestrales.

Como parte de las actividades propuestas, se encuentra la realización de una **Minga el próximo 29 de julio en la Plaza de Bolívar de la capital**, donde esperan que posteriormente a su realización, se consolide un amplio colectivo ciudadano, con apoyo de instituciones públicas y privadas, desde las cuales se multipliquen este tipo de acciones solidarias.

Durante el evento se realizara una **jornada cultural y deportiva**, gestionada gracias a los aportes de ciudadanos e instituciones que se vinculan voluntariamente, con apoyo financiero para temas de alimentación, chaquiras como insumos para la producción de artesanías, chalecos para la guardia indígena que proteja su propia comunidad y trajes para las presentaciones de danza.

Además de disfrutar de los actos y presentaciones, los asistentes **podrán aportar adquiriendo un bono que es intercambiable con alguno de los artesanos de la comunidad**, por una muestra de su trabajo el día del evento.

![afiche evento](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/37643209_10214575873502192_5109766963358334976_o-560x800.jpg){.alignnone .size-medium .wp-image-55165 .aligncenter width="560" height="800"}

**Sobre el Pueblo Embera**

El pueblo Embera, ancestralmente está asentado en los departamentos de R**isaralda, Chocó y Antioquia**, divididos en tres grandes familias: **Embera Dovida, Embera Chami y Embera Katio**, poseedores de una cultura caracterizada por su comovisión, lengua propia y prácticas como la caza, la pesca y trabajos de artesanía.

Desde hace más de una década, el recrudecimiento de la violencia fruto del conflicto armado y la indiferencia estatal, ha conllevado al **masivo desplazamiento de estas comunidades a ciudades como Bogotá**, a la que llegan hombres, mujeres, niños y niñas para habitar en condiciones de pobreza absoluta, engrosando así los cinturones de miseria.

La mayoría **reside en sectores como La favorita, San Bernardo y la Cruces**, lugares donde se exponen a flagelos como la drogadicción, prostitución, explotación infantil, y habitancia de calle, o en los llamados **"pagadiarios"** donde deben dormir en el piso y pagar cada día para poder dormir bajo techo. (Le puede interesar: [Ejército nunca llegó a comunidad indígena confinada por paramilitares](https://archivo.contagioradio.com/ejercito-nunca-llego-comunidad/))

**Programacíón.**

[Programación Minga Embera](https://www.scribd.com/document/384850686/Programacion-Minga-Embera#from_embed "View Programación Minga Embera on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_82300" class="scribd_iframe_embed" title="Programación Minga Embera" src="https://www.scribd.com/embeds/384850686/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-PEx9eTlPjzMdE0M4AwWB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
