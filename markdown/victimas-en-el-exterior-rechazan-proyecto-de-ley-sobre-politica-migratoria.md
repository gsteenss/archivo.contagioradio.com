Title: Víctimas en el exterior rechazan proyecto de ley sobre política migratoria
Date: 2017-11-07 13:23
Category: DDHH, El mundo, Nacional
Tags: conflicto armado, política migratoria, reparación a víctimas, Víctimas en e exterior
Slug: victimas-en-el-exterior-rechazan-proyecto-de-ley-sobre-politica-migratoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/colombianos-exterior.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [07 Nov 2017] 

Las víctimas en el exterior, el exilio y la migración, enviaron un comunicado a la opinión pública en el cual manifiestan que la reforma a la política integral migratoria en Colombia, cuyo [proyecto](https://vlex.com.co/vid/proyecto-ley-148-2017-693902413) fue presentado en la Cámara de Representantes el 19 de septiembre de 2017, **“desconoce las propuestas formuladas por las víctimas** y se limita a proponer lineamientos para una política migratoria”.

De acuerdo con representantes de víctimas en el exterior, que han estado realizando actividades de veeduría nacional sobre la adecuación y el fortalecimiento participativo de la política de atención y reparación integral a víctimas, el proyecto de ley presentado por Ana Paola Agudelo del movimiento político Mira, **“afecta el proceso de ajuste normativo de la ley de víctimas en lo referido a víctimas en el exterior”.**

Indicaron que en la propuesta **“desconocen las discusiones, propuestas y las inquietudes formuladas hasta ahora por las víctimas** y organizaciones acompañantes en los países de acogida”. También afirmaron que “debilita los esfuerzos de organización y participación colectiva en torno a la implementación del punto 5.1.3 del Acuerdo Final para la Terminación del Conflicto”. (Le puede interesar:["Víctimas exigen a congreso mantener esencia de los acuerdos de paz"](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/))

Teniendo en cuenta el parágrafo 3 del proyecto de ley, que indica ciertas restricciones al derecho al retorno de las personas en el exterior, manifestaron que es problemático “condicionar el derecho al retorno **a temas de seguridad nacional**”, yendo así en contravía con el espíritu de la Declaración Universal de los Derechos Humanos de 1943 y al Pacto Internacional de Derechos Civiles y Políticos de 1966.

Adicionalmente, manifestaron que es **inconveniente que esta reforma sea presentada como una política pública** “para el retorno voluntario, digno y con garantías de las víctimas en el exterior” teniendo en cuenta que no hay “alternativas reales para las víctimas en el exterior” pues no hay recursos ni dispositivos institucionales para su acompañamiento mientras están fuera del país. (Le puede interesar:["Colombianos en el exterior rompen el cerco y hacen pedagogía en el exterior"](https://archivo.contagioradio.com/colombianos-en-el-exterior-rompen-el-cerco-y-hacen-pedagogia-por-la-paz/))

Por esto, indicaron que es necesario que los programas y las políticas de retorno **se construyan con la participación directa de la población** y se dicten garantías efectivas para quienes has sido víctimas y se encuentran fuera del país. Manifestaron que desde hace varios años han “generado espacios de estudio, reflexión, discusión y construcción colectiva de propuestas serias, pertinentes y coherentes con nuestras vivencias, procesos y proyectos de vida, dentro y fuera del país”.

Finalmente, enfatizaron en que **no respaldan ni reconocen las propuestas que excluyan sus solicitudes** “y menos aún aquellas que desconozcan los aspectos establecidos por el Acuerdo Final”. Por lo tanto, ratificaron su disposición al trabajo colectivo “siempre y cuando se parta desde el reconocimiento de las víctimas en el exterior”.

[Comunicado víctimas en el exterior](https://www.scribd.com/document/363768535/Comunicado-vi-ctimas-en-el-exterior#from_embed "View Comunicado víctimas en el exterior on Scribd") by [Anonymous 9gchRS](https://www.scribd.com/user/379499957/Anonymous-9gchRS#from_embed "View Anonymous 9gchRS's profile on Scribd") on Scribd

<iframe id="doc_16525" class="scribd_iframe_embed" title="Comunicado víctimas en el exterior" src="https://www.scribd.com/embeds/363768535/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Ymrl8OF0FxmipSBbSYqp&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

 
