Title: Movimientos sociales de Brasil respaldan y exigen compromisos serios del gobierno de Rousseff
Date: 2015-08-20 17:45
Category: Otra Mirada, Política
Tags: Brasil, democracia, Democracia en Venezuela, Derecha continental, ecuador, Gobierno de Dilma Rousseff, Movilizacion brasil
Slug: movimientos-sociales-de-brasil-exigen-senales-claras-del-gobierno-de-rousseff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: dw 

##### <iframe src="http://www.ivoox.com/player_ek_7279897_2_1.html?data=mJekm52de46ZmKialJWJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs9Sf1NTQy8bQqdSfxcqYxNfFt8rgjNfS1dXFsMXVz5DmjcrcrcjZz5DQ0dLUto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Paola Estrada, Secretaria continental del ALBA e integrante del movimiento Consulta Popular] 

###### [20 Ago 2015 ]

Si bien las manifestaciones adelantadas por la derecha sectores de la clase media y media alta de Brasil, persiste el descontento en diversos movimientos sociales por las actuales medidas de ajuste económico impulsadas por el Gobierno de Dilma Rousseff.  Según Paola Estrada, **Secretaria continental del ALBA e integrante del movimiento Consulta Popular** el gobierno debe dar muestras claras favorables a las exigencias de la clase trabajdora.

Estrada afirma que el actual gobierno brasilero enfrenta una aguda crisis en términos económicos, sociales y políticos. **Hay bajos niveles de crecimiento que han llevado a la apropiación de políticas de ajuste fiscal**, de acuerdo a modelos europeos neoliberales y que vulneran los derechos de sectores sociales, principalmente los trabajadores. Problemática que ha desplegado toda una **ofensiva mediática contra el Partido de los Trabajadores en Brasil, liderada por sectores de la ultraderecha brasilera.**

Estrada, asegura que la **actual izquierda brasilera tiene distintas posiciones en relación con el actual gobierno**, una parte impulsa alianzas con diversos sectores para impedir su salida y otra fracción, rechaza las políticas económicas neoliberales promovidas, alegando que éstas responden a las presiones de los sectores empresariales y desatienden las necesidades de las bases populares.

Salvamento a bancos, disminución de garantías laborales, reducción del papel del Estado y altos cobros en los servicios públicos, son algunas de las medidas que se pretenden imponer para salvar a Brasil de la crisis económica, pero que, de seguir siendo abanderadas por el gobierno, incrementarían la indignación de los sindicatos, movimientos sociales y diversos sectores que eligieron a **Dilma Rousseff y que exigen el cumplimiento de su plan de gobierno.  **

Según la analista y militante, **lo más apropiado es que el gobierno se comprometa con las bases sociales**, tanto organizadas como no organizadas y que cada día se ven menos orientadas, pues las movilizaciones van a continuar exigiendo cambios en la política actual de Rousseff en Brasil y proyectan construir un programa con puntos mínimos que permita articular luchas sociales para la unidad de distintos sectores, así como elegir una Constituyente que posibilite reformas políticas que reivindiquen a los sectores sociales brasileros.
