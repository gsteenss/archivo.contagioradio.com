Title: Cuba y Noruega reiteran su apoyo al Proceso de Paz
Date: 2019-09-02 18:40
Author: CtgAdm
Category: Paz, Política
Slug: cuba-y-noruega-reiteran-su-apoyo-al-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/El-Gobierno-de-Colombia-y-las-FARC-han-llegado-a-un-acuerdo-sobre-las-condiciones-para-la-liberación-anunciaron-representantes-de-Cuba-y-Noruega-países-garantes-del-proceso-de-paz.-EFE..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Los países de Cuba y  Noruega, países garantes en la Mesa de  Conversaciones entre el Gobierno de  Colombia y las Fuerzas Armadas Revolucionarias de Colombia- Ejercito del Pueblo (FARC- EP), ratificaron por medio de un comunicado su compromiso e interés en el desarrollo de la paz, así como  su preocupación en la retoma de armas y el desarrollo de nuevos enfrentamientos por parte de algunos exjefes guerrilleros.

Como defensores del Acuerdo de Paz, ambas naciones señalan la importancia de blindar  la Jurisdicción Especial para la Paz, la cual se creó con  la finalidad de asegurar la justicia, reparación y no repetición en todo el territorio, así mismo como el desarrollo de temas como la "reforma agraria, sustitución de cultivos  de uso ilícito, seguridad y la plena reintegración y reincorporación de los ex combatientes  de las FARC-EP  a la vida civil, en interés de consolidar el Proceso de Paz". (Le puede interesar: [Cuba y Noruega piden a Iván Duque sancionar la Ley Estatutaria de la JEP](https://archivo.contagioradio.com/cuba-noruega-piden-sancionar-la-ley-estatutaria-la-jep/))

El señalamiento de los diferentes intereses en pro de la continuación del Acuerdo, tuvo como finalidad recordar  el trabajo que sigue vigente, y la esperanza de no clausurar o detener los trabajos realizados en los últimos meses por la paz, "reiteramos nuestro apoyo y acompañamiento para alcanzar una paz estable y duradera", apunta el comunicado. Le puede interesar: ([El mensaje no llegó como queríamos que llegara: Partido Verde](https://archivo.contagioradio.com/quieren-acabar-con-la-direccion-politica-de-la-farc/))

 
