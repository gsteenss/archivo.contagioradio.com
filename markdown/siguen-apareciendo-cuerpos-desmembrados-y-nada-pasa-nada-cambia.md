Title: Siguen apareciendo cuerpos desmembrados… Y nada pasa, nada cambia
Date: 2015-05-22 07:53
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: Casas de pique, Copacabana, Fernando Quijano, Girardota, Medellin, Septima division del ejercito, Valle de Aburra
Slug: siguen-apareciendo-cuerpos-desmembrados-y-nada-pasa-nada-cambia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/jóvenes-desmembrados-en-Villa-Liliam-29sept2012-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [~~@~~FdoQuijano](https://twitter.com/FdoQuijano)

La violencia, la criminalidad, la convivencia, la paz y la seguridad urbana y rural siempre son objeto de debate público en Medellín y, en general, en Antioquia. Más en época de elecciones para gobiernos locales y regionales, cuando se oyen todo tipo de propuestas de los aspirantes a estos cargos de elección popular; promesas populistas que no se cumplirán por ser tan solo banderas electorales que olvidarán los ganadores de los comicios electorales pues es claro que la agenda es otra y está amarrada a los intereses particulares del poder real, quien dirige el poder formal. O sino que lo diga el empresariado paisa que ha gobernado en la sombra desde hace décadas.

Por años he denunciado la compleja situación de violencia y criminalidad que viven Medellín y el Valle del Aburrá. He hablado de forma insistente sobre los pactos entre criminales y las consecuencias lógicas que traen dichos acuerdos, por ejemplo, la supuesta reducción de homicidios, las fronteras invisibles inactivas, la sensación de tranquilidad por el receso en la guerra urbana, entre otros; pero también el desarrollo de la “guerra silenciosa” que se libra en las calles y deja diariamente un rastro macabro de atrocidades: cuerpos con evidencias de asfixia mecánica, envueltos en sábanas o metidos dentro de carros, cuerpos que bajan por el río amarrados y torturados y otros que aparecen descuartizados en cualquier lugar. Eso, en últimas, son los famosos acuerdos entre criminales apuntalados institucionalmente.

Pactos como el de la “paratranquilidad urbana” o “don-bernabilidad” y el más reciente, “el pacto del fusil”, negado por el gobierno local —pues le ayuda a mejorar su imagen a nivel nacional e internacional—, pero reconocido tácitamente por el presidente Juan Manuel Santos y confirmada su existencia la semana pasada por el ministro de Defensa, Juan Carlos Pinzón, en la comuna 5, Castilla. Hechos que demuestran hasta la saciedad que hay un cogobierno criminal ejercido por los protagonistas de este pacto: Urabeños y Oficina del Valle del Aburrá, que ejercen el control a través de las bandas.

Monotemático, aburridor y exagerado, son algunos apelativos que recibo por mis denuncias. La institucionalidad, por ejemplo, me considera enemigo de la ciudad y del poder formal y real que la controla; dicen que soy apocalíptico, ave de mal agüero; o, como dice tajantemente el comandante de la Séptima División del Ejército, soy un manipulador de la información; en últimas, aporto a crear mitos urbanos.

Con todo y esos denominadores, queda claro que algo anómalo viene sucediendo en esta urbe de progreso y criminalidad, ciudad de contrastes que oculta la dimensión de su conflictividad urbana con el único objetivo de favorecer los intereses de quienes se lucran de ella, pues necesitan mostrar una Medellín hermosa y pujante aunque solo sea un espejismo.

**¿Existen casas de tortura en el Valle del Aburrá?**

Desde finales de los noventa y del 2000 en adelante, empezaron los rumores en Medellín de que había casas en la comuna 1 donde la música era estridente para ocultar el horror que en el interior se vivía: torturas, desmembramientos y asesinatos. Lo mismo ocurría en otras subregiones del departamento. El Estado —tardíamente—, reconoció la situación al escuchar los testimonios de víctimas y victimarios, entre estos últimos el “profesor de los descuartizadores”, Francisco Enrique Villalba Hernández, alias Cristian Barreto; quien junto a otros como él, enseñaban esta actividad condenable.

Los recientes casos de desmembramiento evocan las épocas del terror paramilitar ejercido por los bloques urbanos compuestos en su mayoría por las bandas armadas y sus aliados estatales, quienes recibieron la “protección oficial” necesaria para llevar a cabo sus planes de control territorial.

Desde el año 2000 se ha insistido en la existencia de dichas prácticas. Hoy los actos de terror los siguen ejerciendo los herederos de ese paramilitarismo en casas de tortura.

Estos delitos siguen causando preocupación. La reciente aparición de varios cuerpos desmembrados en Medellín y uno en el Oriente antioqueño demuestra que el tema es vigente y podría ser presagio de reacomodos en el bajo mundo.

Las informaciones apuntan a que el jefe militar de la Oficina, alias Carlos Chata —también conocido como Tom—, de la mano de los Urabeños, habría desatado parte de esta carnicería para asumir el control de Barrio Antioquia y otras zonas del Valle del Aburrá. Hay rumores de que esta *vendetta* se extenderá a la zona Nororiental de la ciudad y a municipios como Itagüí, Copacabana, Girardota y Barbosa.

¿Será que la institucionalidad seguirá negando la existencia de las casas de tortura, a pesar de la aparición de cuerpos desmembrados? ¿Será que el alcalde Aníbal Gaviria Correa seguirá las malas enseñanzas del exministro de Defensa Juan Carlos Pinzón, quien ante las contundentes evidencias de la existencia de casas de pique en Buenaventura, afirmaba con vehemencia que no existían? Y eso que días después seguían apareciendo cuerpos desmembrados en el puerto. Hoy el ministro se va guardando silencio sepulcral ante la realidad. ¿Cuántos más dejarán sus cargos en medio de un silencio culposo?
