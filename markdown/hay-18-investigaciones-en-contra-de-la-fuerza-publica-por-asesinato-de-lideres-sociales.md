Title: En 18 asesinatos de líderes sociales habría implicación de la Fuerza Pública
Date: 2020-09-24 19:26
Author: AdminContagio
Category: Actualidad, DDHH
Slug: hay-18-investigaciones-en-contra-de-la-fuerza-publica-por-asesinato-de-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Asesinatos-en-Cauca-y-Narino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de septiembre la **Procuraduría General de la Nación** entregó un informe en el que revela que existen 22 investigaciones activas por el asesinato de líderes sociales, de estas **18 son en contra de servidores de la Fuerza Pública**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe fue entregado a la Cámara de Representantes, revelando que 18 de las investigaciones que se realizan contra servidores de la Fuerza Pública, 14 se han iniciado por posibles conductas de homicidio, *"**18 cursan contra servidores de la Fuerza Pública, 2 contra civiles relacionados a la administración municipal y 2 por establecer la calidad o categoría de servidor público**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de estas **se suma una por abuso de autoridad, otra por amenazas y torturas**, lesiones personales y finalmente una por estigmatización social. De estas investigaciones, 15 están en etapa de indagación preliminar, seis de investigación disciplinaria, y una de desacato. ([Gobierno gastará \$13.800 millones en ESMAD y Policía](https://archivo.contagioradio.com/gobierno-gastara-13-800-millones-en-esmad-y-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto la **Defensoría agrega que hasta el 19 de abril han registrado 57 asesinatos de líderes sociales** y personas defensoras de Derechos Humanos, por otro lado**[Indepaz](http://www.indepaz.org.co/pazparaliderar/)**agrega que han sido asesinados 66 líderes, en el registro que llevan hasta julio del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo queda la duda del avance que podrían tener estas investigaciones **en la administración de la nueva procuradora Margarita Cabello**, quien fue funcionaria del gobierno Duque y que se ha empeñado en negar los múltiples hechos en contra de líderes sociales en el país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Informe de la MOE concuerda con el de la procuraduría**

<!-- /wp:heading -->

<!-- wp:paragraph -->

A estos datos se suma el informe que presentó la Misión de Observación Electoral (MOE), en el que evidencia solamente en el primer semestre del año y durante la pandemia **se incrementó un 85% los asesinatos de líderes sociales**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una lista que es encabezada por el departamento del Cauca, seguida por Arauca, Norte de Santander, Antioquia y Córdoba, en dónde se registra casi el 50% de los hechos violentos. ([Asesinatos contra líderes sociales se incrementaron un 85%: MOE](https://archivo.contagioradio.com/asesinatos-contra-lideres-sociales-se-incrementaron-un-85-moe/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado el pasado 20 de septiembre, la plataforma Somos Defensores en alianza con la Cumbre Nacional de Mujeres y Paz, Sisma Mujer entre otras, presentaron el informe, "Defensoras, voces de vida y resistencia" . ([Homicidios contra defensoras de DD.HH alcanzan el 91% de impunidad)](https://archivo.contagioradio.com/casos-homicidios-contra-defensoras-de-dd-hh-se-encuentran-en-un-91-de-impunidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el que **da cuenta del aumento de las agresiones contra mujeres que defienden la vida y el territorio** en medio de riesgos ligados a la brecha de género, revelando que las acciones violentas en contra las defensoras incrementaron un 58,3% en el último año. ([“Matan uno, nacen mil”: una comunicadora nasa recuerda a sus compañeros asesinados \#YoReporto](https://archivo.contagioradio.com/matan-uno-nacen-mil-una-comunicadora-nasa-recuerda-a-sus-companeros-asesinados-yoreporto/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, este informe **será entregado a la Comisión de Derechos Humanos del Congreso y respaldarán el debate de control político qué se hará en los próximos días a la fuerza pública,** en el que se determinarán las responsabilidades penales y disciplinarias de los uniformados vinculados.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
