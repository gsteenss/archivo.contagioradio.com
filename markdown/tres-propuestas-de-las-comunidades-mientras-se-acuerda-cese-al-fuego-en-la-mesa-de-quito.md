Title: CONPAZ pide a gobierno y ELN respetar propuestas humanitarias de cara a un Cese Bilateral
Date: 2017-07-04 12:42
Category: Nacional, Paz
Tags: Cese al fuego, Conpaz, Conversaciones en Quito, ELN
Slug: tres-propuestas-de-las-comunidades-mientras-se-acuerda-cese-al-fuego-en-la-mesa-de-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/conpaz_4_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  CONPAZ] 

###### [04 Jul. 2017] 

A través de una carta dirigida al presidente Juan Manuel Santos y al jefe negociador del ELN Nicolás Rodríguez, las Comunidades Construyendo Paz en los Territorios – **CONPAZ -, saludaron la posibilidad de discutir el cese al fuego y de hostilidades en la mesa de diálogos en Quito, Ecuador** y aprovecharon para dar a conocer 3 propuestas para vivir en los territorios mientras dicha decisión se da. Le puede interesar: [ELN y Gobierno abren las puertas a un posible cese al fuego](https://archivo.contagioradio.com/eln-y-gobierno-abren-las-puertas-a-un-posible-cese-al-fuego/)

En la primera las comunidades **solicitan al Gobierno respetar las Zonas Humanitarias y Ecoaldeas de Paz** que aseguran, son propuestas que han sido pensadas como espacios de protección de civiles tal como lo propone el derecho humanitario. Le puede interesar: [ELN está listo para firmar cese al fuego bilateral](https://archivo.contagioradio.com/42291/)

La segunda propuesta que entrega CONPAZ, es poder **adoptar decisiones que estén encaminadas a proteger a la sociedad a propósito de las operaciones paramilitares** que se llevan a cabo en varios territorios del país y que han sido denunciadas por organizaciones sociales, internacionales y las comunidades afectadas.

La última propuesta esta **dirigida al ELN, en la que instan a respetar las Zonas Humanitarias y Ecoaldeas de Paz** como su propuesta “humanitaria en desarrollo de la guerra de guerrillas y como espacios de protección de civiles conforme al derecho humanitario”. Le puede interesar: [CONPAZ pide al Gobierno y al ELN decretar cese bilateral al fuego](https://archivo.contagioradio.com/conpaz-pide-al-gobierno-y-al-eln-decretar-cese-bilateral-al-fuego/)

Concluyen la comunicación manifestando que **están en total disposición de compartir sus experiencias e iniciativas**, en las cuales vienen trabajando hace años en sus territorios para salvaguardar sus vidas y luchar por su derecho a la tierra.

[Carta Pública 1 Julio](https://www.scribd.com/document/352906589/Carta-Publica-1-Julio#from_embed "View Carta Pública 1 Julio on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_66714" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/352906589/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-0UgrVDVDy0InX8cI5uLT&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
