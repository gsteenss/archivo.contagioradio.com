Title: Así reciben las comunidades el acuerdo de paz entre Gobierno y FARC
Date: 2016-08-25 14:05
Category: Nacional, Paz
Tags: comunidades, conflicto armado, FARC, Gobierno
Slug: asi-reciben-las-comunidades-el-acuerdo-de-paz-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-46.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [25 Ago 2016] 

En medio de abrazos, camisetas blancas y lágrimas, las principales ciudades del país y algunas del mundo celebraron  la firma del acuerdo final en La Habana que da paso a la verdadera construcción de paz en Colombia; un festejo que se adquiere un sentido especial para las comunidades que han vivido de cerca el conflicto armado.

**“Día histórico para Colombia”** es la frase que repiten historiadores políticos, sociólogos, medios internacionales tras la firma del Acuerdo Final, integral y definitivo al que llegaron las delegaciones del Gobierno Nacional y de las FARC, un acuerdo que desde ya empezó a generar sus frutos, pues este jueves el presidente Juan Manuel Santos, entregó el documento final de los acuerdos al Congreso de la República y anunció **cese al fuego bilateral y definitivo a partir del lunes 29 de agosto.**

Un hecho que toca directamente a las comunidades que han sido las más afectadas por el conflicto, y las que hicieron parte esencial en el desarrollo de las negociaciones. Ellos nos contaron cómo viven estos momentos junto a sus comunidades y cómo se preparan para construir la paz en Colombia.

### **Luz Marina Cuchumbe -Inza Cauca – Rep. de las víctimas en La Habana** 

<iframe src="http://co.ivoox.com/es/player_ek_12664945_2_1.html?data=kpejmJmdeJahhpywj5WZaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5ynca3p25C6w9fNssKfpNrQytrRpsaZk6iYq9PeaaSnhqaejajFucTVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

“Me siento muy contenta y feliz, de que se haya firmado ese acuerdo que tanto anhelamos las víctimas principalmente. Para mí ha sido lo más importante representar al cauca como víctima, cuando estuve en La Habana entregué una veladora a Humberto de la Calle y al General Naranjo, y espero que ayer se hayan acordado de mí”… “No esperemos que nos quieten a nuestro hijos para poder creer que si podemos trabajar por la paz. La guerra no nos trae nada bueno”.

### **Jani Silva - Zona de Reserva Campesina Perla Amazónica** 

<iframe src="http://co.ivoox.com/es/player_ek_12664886_2_1.html?data=kpejmJmcfJehhpywj5WZaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKZk6iYvLenb7HZ09HOjabRpduZpJiSpJjSrcTVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

“Este acuerdo significa mucho, son muchos años de estar en el conflicto, estamos muy alegres, porque con este momento se abre un nuevo capítulo en la historia para construir un país más justo para todos…  Con mucha alegría hemos acogido esta noticia, fue muy emocionante, es como un resurgir”… “Seguiremos luchando para que el gobierno le cumpla al Putumayo”.

### **Saúl Chamarra- Comunicador CONPAZ, Puerto Pisario, Chocó** 

<iframe src="http://co.ivoox.com/es/player_ek_12665129_2_1.html?data=kpejmJqVdpqhhpywj5WWaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbTV1tGYpc3FscLm08aSlKiPh9Dh1tPWxcbIs9OfpLS7sqa-cYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

“Esta es una noticia histórica, los pueblos indígenas estamos muy contentos. La población indígena quiere vivir en paz en sus territorios, ojalá el gobierno y las FARC cumplan los acuerdos, esperamos que nunca nos atropellen los territorios ancestrales”.

### **Fuerza Mujeres Wayú** 

“Estamos ante un hecho histórico, lo acordado entre Estado y FARC es una oportunidad para tejer nuestra visión de Paz. Estamos listas para aportar a la construcción de paz en nuestro territorio, valiéndonos del sentir, pensar y soñar como wayúu”.

### **Olga Quintero - Asociación Campesina del Catatumbo, ASCAMCAT** 

“Con alegría se le abre las puertas a la construcción de La Paz con Justicia social, y con garantías de no repetición. La fuerza del Amor y la esperanza tienen que superar los odios ajenos e infundidos de aquellos que no quieren la paz, porque la paz si es Posible, ahora decirle SI al plebiscito es una tarea para seguir avanzando”.

### **Rodrigo Castillo -  CONPAZ** 

"Es un hecho histórico, entendiendo que desde el inicio siempre la mirada de la gente ha estado pendiente del acuerdo final. Es la posibilidad de transformar nuestro país Colombia. Tenemos que seguir haciendo pedagogía para el SI por el plebiscito".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
