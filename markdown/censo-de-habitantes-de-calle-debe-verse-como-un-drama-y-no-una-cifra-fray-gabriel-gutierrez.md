Title: Censo de habitantes de calle "debe verse como un drama y no una cifra": Fray Gabriel Gutiérrez
Date: 2018-03-13 12:38
Category: DDHH, Nacional
Tags: asesinatos de habitantes de calle, Bogotá, censo habitantes de calle, Fray Gabriel, Habitantes de calle
Slug: censo-de-habitantes-de-calle-debe-verse-como-un-drama-y-no-una-cifra-fray-gabriel-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/habitantes-de-calle-e1516372912796.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blog Luz para los Habitantes de calle] 

###### [13 Mar 2018] 

Luego de que autoridades como la Secretaría de Integración Social, el Departamento Administrativo Nacional de Estadística en conjunto con el Distrito realizaran un censo que retrata la situación de la habitabilidad en calle en Bogotá, las organizaciones que defienden los derechos de esta población indicaron que el censo no debe verse como una cifra sino como **un llamado de atención sobre la situación humanitaria**. Según el Distrito, en la capital hay 9.538 personas que habitan la calle, pero de acuerdo con las organizaciones podría haber más del doble.

Según el censo, en Bogotá hay 8.477 habitantes de calle hombres y 1.061 mujeres, el **promedio de edad es de 35 años**, el 68% llevan más de 6 años viviendo en la calle, el 90% de ellos son alfabetas y el 19% de ellos sobreviven pidiendo dinero. Además, la mayor concentración de este tipo de población se encuentra en la localidad de los Mártires seguido de Santa Fe y Kennedy.

### **Los problemas con el censo del DANE** 

Para el sacerdote Fray Gabriel Gutiérrez, defensor de los derechos de las personas en condición de habitabilidad en calle, las cifras del Distrito no corresponden con las que ellos manejan en la medida en que **“pueden ser el doble** y difiere muy poco del censo de 2011 de 9.614 habitantes de calle”. Teniendo en cuenta ese número, para el sacerdote la situación no puede leerse como una cifra sino como un “drama”.

Esto, teniendo en cuenta que los datos que maneja la Alcaldía indican que las políticas públicas para atender a esta población **no están funcionando** y no corresponden “al drama humanitario que están viviendo estas personas”. Gutiérrez recalcó que en el país hay más de 120 mil habitantes de calle por lo que debe verse la situación como un llamado de atención “alarmante donde miles de seres humanos están viviendo de forma infrahumana”. (Le puede interesar: ["Piden declarar emergencia humanitaria por asesinatos de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/policia-realizo-operativo-contra-habitantes-de-calle-en-bogota/))

Dijo además que “ninguna organización social fue llamada a participar de la elaboración de las preguntas del censo y **muchos habitantes de calle denunciaron que no fueron censados**”.  Asimismo reiteró que los defensores de los derechos de esta población quieren conocer ciertas problemáticas como por ejemplo que “el censo anterior dice que son agredidos por la Fuerza Pública y no hay un pronunciamiento sobre esta denuncia que el mismo censo hace”. (Le puede interesar: ["Aumentan asesinatos y desapariciones de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/aumentan-asesinatos-y-desapariciones-de-habitantes-de-calle-en-bogota/))

### **En Bogotá debe decretarse emergencia humanitaria** 

Teniendo en cuenta los fenómenos que han sucedido contra esta población como lo son asesinatos y desapariciones, los defensores de los derechos humanos han dicho que es necesario que el Estado **haga caso a las alertas tempranas** para evitar que este tipo de situaciones sigan ocurriendo. Ante esto, Fray Gabriel manifestó que “en Bogotá se debe decretar la emergencia humanitaria”.

El sacerdote indicó que en los últimos cinco años han sido asesinados cerca de **500 habitantes de calle** y ha habido “un silencio absoluto por parte de los entes gubernamentales”. Por esto, si se decreta la emergencia humanitaria “tendríamos que sentarnos con los habitantes de calle, con las organizaciones sociales y las instituciones para buscar salidas humanitarias”.

Finalmente, Gutiérrez manifestó que es necesario que la ciudadanía **“comience a comprender la problemática con una dimensión humana** y no de represión”, ya que la sociedad en general debe trabajar por la construcción de políticas públicas inclusivas que tengan en cuenta las necesidades de los habitantes de calle.  Y concluyó que se debe poner por encima la búsqueda de las causas estructurales que generan la habitabilidad en calle y no poner solamente números.

<iframe id="audio_24387267" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24387267_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
