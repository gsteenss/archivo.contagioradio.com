Title: Jesús Santrich en cuidados intensivos luego de 18 días de huelga de hambre
Date: 2017-07-14 16:24
Category: DDHH, Nacional
Tags: Acuerdo de paz de la habana, FARC-EP, Huelga de hambre, Jesús Santrich
Slug: jesus-santrich-cuidados-intensivos-luego-18-dias-huelga-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SANTRICH.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagio radio] 

###### [14 Jul 2017]

Hacia el medio día de este 14 de Julio, integrantes de las FARC-EP, quienes se encuentran en huelga de hambre desde el 26 de junio en protesta por el incumplimiento del Estado en cuanto a la salida de las cárceles de integrantes de esa organización, informaron que **Jesús Santrich fue internado en la clínica Shaio y en la Unidad de Cuidados Intensivos de esa institución.**

Según la información el integrante del Estado Mayor de las FARC habría convulsionado tras un **colapso por la extendida huelga de hambre que adelanta** y su estado de salud es crítico. Hasta el momento se sigue a la espera de un comunicado oficial por parte del cuerpo médico de la clínica. (Le puede interesar: ["Más de 1.400 presos políticos de las FARC continúan en huelga de hambre"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre/))

Los integrantes de las FARC EP han afirmado que el Estado colombiano y el gobierno serían responsables por la situación de Santrich y los demás integrantes de esa guerrilla que **continúan en huelga de hambre, pues la salida de las cárceles de ellos y ellas es parte de un acuerdo firmado y debe cumplirse.**

Desde el pasado 26 de de Junio **más de 1500 integrantes de esa guerrilla adelantan una huelga de hambre dado que la salida de las cárceles, las amnistías e indultos, no alcanzan el 50% de la población** de esa guerrilla recluida y se sigue exigiendo el cumplimiento de la ley 1820 aprobada por el congreso desde Diciembre de 2016. (Le puede interesar: ["Amenazan con aislamiento a presos políticos de las FARC"](https://archivo.contagioradio.com/amenazan-con-aislamiento-a-presos-de-las-farc-en-huelga-de-hambre/))

Ya la organización de las Naciones Unidas se había manifestado frente a esa situación y había afirmado que era urgente resolver esa situación dado que genera zozobra en los integrantes de las FARC-EP por el incumplimiento de este y otros acuerdos a los que se llegó.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
