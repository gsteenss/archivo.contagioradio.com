Title: CNE debe respetar Constitución: Comité de revocatoria a Peñalosa
Date: 2017-05-25 12:28
Category: DDHH, Nacional
Tags: Consejo Nacional Electoral, Revocatoria a Peñalosa
Slug: la-revocatoria-a-penalosa-continua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_de_las_antorchas_para_revocar_a_penalosa_foto_blu_radio_2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [25 May 2017] 

El comité Unidos revocaremos a Peñalosa expresó, que pese a la decisión del Consejo Nacional Electoral de no respaldar la ponencia que daba via libre a la revocatoria, el proceso de revisión de firmas **continúa en la Registraduría y de ser aprobadas deberán convocar a votaciones.**

El magistrado Emiliano Rivera será quien asuma la tarea de exponer una nueva ponencia sobre si debe ser o no el CNE el encargado de tomar decisiones sobre las revocatorias y su reglamentación. Para Carlos Carrillo, vocero del comité, este cambio en los magistrados ponentes **evidencia las intenciones políticas por parte de la organización y su interés por respaldar al alcalde**. Le puede interesar: ["Comités de revocatoria escépticos por decisión del CNE"](https://archivo.contagioradio.com/comites-de-revocatoria-escepticos-por-decision-del-cne/)

“Confiamos en que este magistrado al igual que lo hizo Hernández, respete la Constitución, que es lo único que nosotros estamos pidiendo” afirmó Carrillo y agregó que **lo que está buscando el CNE es una ponencia que sea arbitraria e inconstitucional** que detenga el proceso de revocatoria. Le puede interesar: ["Consejo Nacional Electoral tiende trampa a la ciudadanía: Unidos Revocaremos  Peñalosa"](https://archivo.contagioradio.com/consejo-nacional-electoral-tiende-trapa-a-ciudadania-unidos-revocaremos-a-penalosa/)

Mientras una decisión final del Consejo Nacional Electoral, la revocatoria continúa en la **Registraduría que tiene poco más de 20 días para dar su concepto** sobre la verificación de las firmas recolectas y posteriormente deberá llamar a elecciones.

Del otro lado, el abogado de Enrique Peñalosa manifestó que se pondrán en marcha diferentes mecanismos legales, uno de ellos sería una demanda contra la revocatoria por estar violando el derecho a la elección, Carrillo manifestó que en este momento esa afirmación no tiene ningún sentido porque aún **“no ha pasado nada” es decir no ha sido revocado**.

El segundo argumento enunciado por Peñalosa, es que apenas a trascurrido un año de su mandato, por lo cual no se ha podido cumplir a cabalidad su programa de gobierno y la insatisfacción no podría ser un motivo para la revocatoria, sin embargo, Carrillo señaló que este es uno de los motivos para convocar un proceso revocatorio y expresó que **“la ciudadanía no puede permitir que se cambien las leyes sobre la marcha”**.

<iframe id="audio_18900108" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18900108_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
