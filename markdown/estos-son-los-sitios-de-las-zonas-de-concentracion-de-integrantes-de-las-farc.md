Title: Estos son los sitios de las Zonas de Concentración de integrantes de las FARC
Date: 2016-06-24 10:20
Category: Nacional, Paz
Tags: Cese al fuego bilateral, FARC
Slug: estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cese-unilaterla-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: josecardenas.co] 

###### [24 Jun 2016]

Estas son las veredas y municipios en los que se ubicarán las zonas de concentración temporal para integrantes de las FARC a partir de la firma del acuerdo final. Estos sitios fueron anunciado hace pocos minutos por parte del Ministerio de Defensa.

Cesar: La Paz  
Norte de Santander: Tibú  
Antioquia: Remedios, Ituango, Dabeiba  
Tolima: Planadas, Villarica  
Cauca: Buenos Aires, Caldono  
Nariño: Policarpa, Tumaco  
Putumayo: Puerto Asis  
Caquetá: Montañita, Cartagena del Chairá  
Arauca: Arauquita, Tame  
Meta: Macarena, Mapiripán, Mesetas, Vistahermosa  
Vichada: Cumaribo  
Guaviare: San José del Guaviare (este), San José del Guaviare (oeste)

 Además, los ocho campamentos especiales que también fueron definidos por las partes en La Habana serán:

Guajira: Fonseca  
Antioquia: Vigía del Fuerte  
Chocó: Riosucio  
Córdoba: Tierra Alta  
Cauca: Corinto  
Caquetá: San Vicente  
Meta: Losada, Macarena  
Guainía: Puerto Colombia

Los sitios en los que funcionarán las sedes de las oficinas de las Naciones Unidas para la atención y verificación de los acuerdos para dejación de armas y concentración de tropas de las FARC serán:

Cucuta, Norte de Santander

Valledupar, Cesar

Medellín, Antioquia

Quibdó, Chcocó

Bogotá, Cundinamarca

Villavicencio, Meta

Florencia, Caquetá

Popayán, Cauca

Cali, Valle del Cauca
