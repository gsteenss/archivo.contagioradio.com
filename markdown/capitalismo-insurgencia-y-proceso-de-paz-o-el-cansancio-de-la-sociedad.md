Title: Capitalismo, insurgencia y proceso de paz: o el cansancio de la sociedad
Date: 2015-03-06 19:47
Author: CtgAdm
Category: Nicole, Opinion
Tags: conversaciones de paz, proceso de paz, Socialización de acuerdos
Slug: capitalismo-insurgencia-y-proceso-de-paz-o-el-cansancio-de-la-sociedad
Status: published

###### Foto: Caros Barragan 

**Por [[Nicole Jullian]](https://archivo.contagioradio.com/nicole-jullian/)**

Al inicio de este semestre, cuando revisaba el listado de cursos que ofrecía la Universidad Nacional en relación al conflicto social y armado, hubo uno que capturó inmediatamente mi atención: ***Capitalismo, insurgencia y proceso de paz**.* El título me pareció bastante progresista y muy acotado a la contingencia actual del país. Debo confesar que también me llamó la atención el hecho de que fuera dictado por el profesor Jairo Estrada. Pensé que sería un curso muy visitado. Pero no es así. En promedio asistimos sólo 8 personas. Me he estado preguntado cuál puede ser la razón de la poca asistencia.

¿Desinterés?  ¿Cansancio? De lo primero puedo decir muy poco y cualquier juicio sería algo tan impreciso como lo son siempre las encuestas: se hacen consultas dentro de un universo limitado de personas para después buscar respuesta a un fenómeno que abarca un número muchísimo mayor que los pocos consultados. Pero sobre el cansancio sí creo poder decir algo. Pues lo vivo en carne propia y lo veo en otros y otras día a día.

Y es que después de 50 años de violencia no podría ser de otra manera. Ahora incluso es sorprendente como esta sociedad, a pesar de todo lo vivido, logra disimular el cansancio y también esconder que muchas de las heridas aún están abiertas. Este es un pueblo de gente fuerte, del cual los hermanos mapuches, otros hermanos fortachudos, están, con absoluta seguridad, muy orgullosos.

¿Pero cuánto más resistirá esta sociedad? El hecho de que las FARC-EP se hayan sentado a negociar con el gobierno, es muestra clara de que las energías de esta heroica sociedad van en descenso. No lo digo, porque sea de las que comparte la tesis de que la guerrilla está debilitada, que no pudo vencer al Estado y que por eso anda por allá en Cuba negociando la amnistía. Nada de eso. Lo digo más bien pensando en que seguramente el cansancio es tan generalizado que tanto insurgencia como gobierno han llegado al acuerdo de negociar la paz mientras haya algo de energía.

Toda negociación es agotadora, mucho más cuando sobre la mesa se ponen en evidencia toneladas de años de violencia, toneladas de responsabilidades no asumidas, toneladas de verdades no aclaradas, toneladas de acuerdos que han beneficiado social y económicamente a un grupo pequeño de la sociedad. Sin duda, la negociación deberá conducir a los distintos a aceptar que les tocó vivir juntos. Que el negro puede vivir con el cachaco, el indígena con el costeño y así entender que esta tierra no por nada es tan grande y tan fructífera.

Del momento en que insurgencia y gobierno lleguen a un acuerdo, lo que se va a necesitar es una buena cantidad de energía. No para reconstruir las instituciones debilitadas, ni  tampoco para curar a la tan herida democracia. Quiero creer que Colombia nunca ha existido como Estado, y que después de muchos esfuerzos fallidos, está ad portas de poder nacer por primera vez.
