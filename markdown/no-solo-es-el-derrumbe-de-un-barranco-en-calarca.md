Title: No solo es el derrumbe de un barranco en Calarcá
Date: 2020-01-30 14:44
Author: CtgAdm
Category: Ambiente, Opinion
Slug: no-solo-es-el-derrumbe-de-un-barranco-en-calarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/97927384-ddba-4113-a37d-9b73fc723ef9.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/0a76d57f-5780-4abb-92bc-42583571b085.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/31d75ae1-1941-4542-a5a9-e5f554982323.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/1c0fea70-ac68-4abb-8e1c-118147124c1a.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/1a579108-b7bd-44ad-b55e-2176ba11424e.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*En la vereda El Crucero lo que se está derrumbando es el basurero Villa Karina de la Empresa Multipropósito de Calarcá S.A.S. ESP.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No se trata del simple derrumbe de un barranco por las lluvias de los últimos días, lo que va quedando a la vista es la **desidia y la incapacidad de autoridades y entidades estatales, locales y departamentales, que durante 17 años han permitido allí todo tipo de irregularidades** afectando a más de mil campesinos que deberían gozar de especial atención.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El origen

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los problemas empezaron cuando se estableció en esa vereda el basurero Villa Karina, sobre fallas geológicas, muy cerca de corrientes de agua superficiales, en zona de recarga de acuíferos subterráneos, y **engañando a los vecinos con mentiras y falsas promesas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hablamos de *“basurero”* porque nunca fue un *“relleno sanitario”* ni, mucho menos, un *“Parque Ambiental”*, como lo llamaba y sigue llamando, la empresa con el beneplácito y la complicidad de la Corporación Autónoma Regional del Quindío (CRQ) y demás autoridades de la localidad y el departamento.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80068,"width":512,"height":382,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![Comunidad de Calarcá en la CRQ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/0a76d57f-5780-4abb-92bc-42583571b085-1024x764.jpg){.wp-image-80068 width="512" height="382"}  

<figcaption>
Foto: Comunidad de Calarcá

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Primero fue el paso de maquinaria y vehículos pesados que con sus vibraciones afectaron la carretera y taludes adyacentes**. La situación empeoró cuando empezaron a construir el vaso Las Azucenas para ampliar el basurero, lo hicieron sobre un nacimiento de agua, movieron más de 100.000 metros cúbicos de tierra y empezaron a impermeabilizar fondo y paredes laterales.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80069,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![posos en la vereda Crucero en Calarcá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/31d75ae1-1941-4542-a5a9-e5f554982323-1024x764.jpg){.wp-image-80069}  

<figcaption>
Foto: Comunidad de Calarcá

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Estas obras alteraron completamente las características hidrogeológicas del sector. Tan evidente era la inestabilidad creada allí que en 2014 la empresa construyó apresuradamente un muro de contención. Preciso ahí donde ahora se derrumba la pared sur del vaso Las Azucenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde 2015 los habitantes del sector denunciaron la situación ante las autoridades, con visitas y cartas, caracterizando bien la situación con pancartas en las que se lee: **"Villa Karina amenaza desastre ambiental"** . Por fortuna y luego de **15 años de lucha y padecimientos los vecinos lograron el cierre definitivo del basurero Villa Karina.** (Res. CRQ No. 3085 / Nov. 29 de 2017).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A estos campesinos debemos agradecer que evitaron un desastre**. Si hubiese continuado allí el basurero hoy estaríamos frente un grave derrame de lixiviados sobre la quebrada la Duquesa y el Río Quíndio causando inmensos daños, aguas abajo, sobre los acueductos de La Tebaida y Cartago.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80070,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Derrumbe de tierra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/1c0fea70-ac68-4abb-8e1c-118147124c1a-1024x576.jpg){.wp-image-80070}  

<figcaption>
Foto: Comunidad de Calarcá

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Pero a pesar de todo lo anterior la empresa **"Mugrepropósito"**, c**omo se le conoce popularmente en Calarcá, ha seguido con la intención de volver a abrir el basurero Villa Karina** en el vaso de Las Azucenas y para eso ha contado con el apoyo de algunos concejales del Municipio. **No habían pasado tres meses desde el cierre definitivo del basurero y ya buscaban la manera de reabrirlo**; ese es el valor que la empresa, y otras “autoridades”, le dan a las resoluciones de la CRQ.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso es inadmisible que ahora la empresa multipropósito pretenda hacernos creer que lo sucedido el lunes pasado en El Crucero nada tiene que ver con el basurero Villa Karina ni con el vaso Las Azucenas, y que trate de eludir su responsabilidad culpando a los vecinos y al Comité de Cafeteros. Y menos se puede aceptar que quiera cargar todos los costos de atención a la emergencia al Municipio y el Departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estaremos atentos al manejo de la situación por parte de las autoridades locales y departamentales y **exigimos la presencia de los entes de control del Estado que hasta ahora han sido indiferentes e inoperantes**. A los campesinos del sector se les debe resolver pronto la situación y, además, deben ser **indemnizados** por los atropellos de Mugrepropósito y los perjuicios que les ha causado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como por la ausencia de las autoridades en estos 17 años en los que han padecido el problema del basurero. Apenas lo justo para agradecer a quienes han evitado un desastre mayor en el Quindío. (Le puede interesar: <https://archivo.contagioradio.com/oposicion-putumayo-cuesta-vida/>)

<!-- /wp:paragraph -->

<!-- wp:image {"id":80071,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![ Vereda el Crucero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/1a579108-b7bd-44ad-b55e-2176ba11424e-1024x576.jpg){.wp-image-80071}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->

*Texto por: Nestor Campo - Integrantes de la comunidad Calarqueña*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
