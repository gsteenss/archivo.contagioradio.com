Title: ¿Qué sigue para los 13 jóvenes liberados?
Date: 2015-09-14 13:22
Category: DDHH, Movilización, Nacional
Tags: Colombia Informa, Comité de Solidaridad con los Presos Políticos, cpdh, INPEC, Irregularidades libertad de los 13, Justicia y Paz, Libertad para los 13, Paola Salgado, Sergio Segura, Universidad Nacional de Colombia
Slug: que-sigue-para-los-13-liberados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/13ede7788c591f582248c40209bdc538_XL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: barriodelcarmen.net] 

<iframe src="http://www.ivoox.com/player_ek_8376606_2_1.html?data=mZikmJuUeo6ZmKiak5aJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezs9qJh5SZop6Y1c7Lucaf0cbfw5DQs9SfkpiYzIqnd4a2lNvS0MrXb83dw8rfw8nTt4anp5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloria Silva, Comité Solidaridad Presos Políticos] 

###### [14 sep 2015]

El día viernes el juzgado 44 penal de conocimiento ordenó libertad inmediata a los 13 detenidos, declarando ilegal el procedimiento de captura y nula la medida de aseguramiento, sin embargo, en los sitios de reclusión **hubo irregularidades en su trámite de salida**.

Después de que se diera esta orden , la boleta de libertad inmediata se presentó en la cárcel Modelo y el Buen Pastor, ante lo que funcionarios del INPEC respondieron que “*no estaba el funcionario encargado y que el Director del INPEC no había dado ordenes en este sentido, lo cual el mismo Director, desmintió*” indica Gloria Silva, abogada de uno de los detenidos y miembro del comité de solidaridad de presos políticos.

Estas demoras en el procedimiento ocasionaron **que los jóvenes recuperaran su libertad hasta el sábado en la medianoche**. Hoy se encuentran en sus hogares con una medida de aseguramiento preventiva y cargos por porte, fabricación transportes de sustancias explosivas, ante lo cual Silva afirma que el caso “***Se restringe a la presunta participación en eventos que se dieron en la Universidad Nacional de Colombia***”.

**"Todo un show mediático":**

Hoy los jóvenes dejados en libertad** se encuentran con sus familias,  pero para la abogada Silva existe un riesgo constante contra la vida de los liberados, luego del show mediático y la estigmatización que se jugo desde los medios de comunicación  en este caso.**

“Seguimientos contra sus familias, amigos, personas que se solidarizaron con esta situación y ataques directos contra abogados y familias”, dice Silva que son algunos de los motivos por los cuales ellos sienten presente ese riesgo, además que **fueron condenados y estigmatizados mediáticamente**, declarándolos insurgentes.

Ver también: [Libertad para los 13 jóvenes capturados](https://archivo.contagioradio.com/en-libertad-los-13-jovenes-capturados/)  
[Estos son los perfiles de las personas capturadas en Bogotá ](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/)
