Title: Simpatizantes del Centro Democrático agreden a periodista de Telesur
Date: 2015-01-30 21:03
Author: CtgAdm
Category: Judicial, Política
Tags: Centro Democrático, Oscar Ivan Zualuga, Telesur
Slug: simpatizantes-del-centro-democratico-agreden-a-periodista-de-telesur
Status: published

Durante el cubrimiento a la manifestación de simpatizantes del Centro Democrático frente a las instalaciones del Bunker de la Fiscalía, la **corresponsal de Telesur y su camarógrafo fueron agredidos tanto física como verbalmente** por un grupo de personas que los acorralaron. Afortunadamente los demás periodistas que cubrían el acto respaldaron a sus colegas y evitaron que sufrieran más daños.

En un video publicado en la red social YouTube se observa al grupo de manifestantes golpeando con palos de banderas a la periodista y se logran escuchar los insultos. Al mismo tiempo se evidencia que fueron los periodistas de los demás medios los que lograron impedir más agresiones.

\[embed\]https://www.youtube.com/watch?v=2tij2oD4Fac\[/embed\]

Recientemente varios medios de información han sido víctimas de amenazas por parte de paramilitares y **en 2014 el senador Álvaro Uribe, líder del Centro Democrático, señaló públicamente a Telesur y Canal Capital como medios “serviles al terrorismo”**, lo que desató una ola de amenazas y señalamientos contra dichos medios.

\[embed\]https://www.youtube.com/watch?v=QmIol8XI574\[/embed\]
