Title: La respuesta del Consejo Político de FARC ante señalamientos del embajador de EE.UU
Date: 2017-09-29 13:08
Category: Nacional, Política
Tags: embajador de EEUU, FARC, Kevin Whitaker
Slug: la-respuesta-las-farc-ante-senalamientos-del-embajador-ee-uu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/embajador-eeuu.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mauricio Alvarado Colprensa] 

###### [29 Sep 2017] 

Ante las declaraciones del embajador de Estados Unidos en Colombia, Kevin Whitaker, quien aseguró que **las FARC no han cumplido respecto a los compromisos en el tema de drogas, y que por ende, permanecerán en la lista de organizaciones terroristas**. El Consejo Político Nacional FARC respondió que "si Estados Unidos quiere ayudar a la consolidación de la paz, no puede quedarse en el pasado con un discurso sin fundamento".

En ese sentido, señalan que la lucha antidrogas no solo compete a las FARC y al gobierno, sino que a toda la comunidad internacional, y es así como **EE.UU "en lugar de chantajear o amenazar a Colombia con la descertificación, debiera mostrar resultados en la persecución al blanqueo de dinero** -que es el alma del narcotráfico-, frenar el flujo hacia la periferia pobre de los precursores químicos, y ayudar a Colombia con recursos para que formalice, distribuya y titule tierras a los campesinos, asegurando la financiación de los planes alternativos de sustitución consensuados con las comunidades rurales", señala la carta.

Justamente, respecto al tema de los pobladores, exigen que Estados Unidos y las autoridades, dejen de confundir a los campesinos pobres de Tumaco con FARC, y niegan que estén incentivando los cultivos de uso ilícito. De hecho, expresan que "Hasta con el Presidente hemos estado en las zonas cocaleras invitando a las comunidades a la sustitución".

### Incumplimientos del gobierno 

En la carta dirigida al embajador aprovechan para exigir que el gobierno nacional cumpla con el acuerdo, ya que **no se puede poner en marcha los programas de sustitución sin aprobar ley de tratamientos penales alternativos para cultivadore**s, que  debe ir acompañada de una Reforma Rural Integral que implique proyectos productivos para las comunidades.

"Afirmaciones como las del embajador Whitaker merecen nuestro rechazo, las consideramos tendenciosas y con evidentes propósitos de reposicionar la fracasada política de interdicción y aspersión aérea". Finalmente consideran que se trata de una **pretensión de la Embajada de los Estados Unidos y de la Fiscalía de ponerlos por fuera del acuerdo,** demandar la acción penal y habilitar condiciones para la extradición y afectarlos como alternativa política, concluye la carta.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
