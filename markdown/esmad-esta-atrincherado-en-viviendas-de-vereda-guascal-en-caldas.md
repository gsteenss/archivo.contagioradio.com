Title: ESMAD está "atrincherado" en viviendas de Guascal en Caldas
Date: 2017-11-02 14:58
Category: Movilización, Nacional
Tags: Caldas, ESMAD, indígenas de Colombia, Minga Indígena, Movilización Indígena
Slug: esmad-esta-atrincherado-en-viviendas-de-vereda-guascal-en-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/indigenas-caldas-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria] 

###### [02 Nov 2017] 

La MINGA indígena que continúa realizándose en el país, sucede también en el departamento de Caldas. Allí, los indígenas Embera y el Consejo Regional Indígena de Caldas CRIDEC **han denunciado violaciones a los derechos humanos** por parte del ESMAD. Indicaron que la comunidad del Guascal, **“está invadida de integrantes del ESMAD**, que están atrincherados en las diferentes viviendas”.

De acuerdo con Erika Giraldo, integrante del equipo de comunicaciones del CRIDEC, las comunidades de los 17 cabildos se han organizado **para discutir las exigencias** que se han planteado alrededor de la MINGA indígena. Pese a que intentaron salir con pancartas para evidenciar las exigencias, han denunciado que la presencia del ESMAD no “ha permitido que se desarrolle la movilización de manera satisfactoria”.

### **En Caldas, no se han concretado acciones para cumplimiento de acuerdos** 

Giraldo indicó que desde el 2016, se han realizado reuniones protocolarias con las diferentes autoridades, pero **“no se ha avanzado en acciones concretas para cumplir con lo establecido”.** Dijo que los avances en los programas de salud, educación y vivienda han sido muy pocos debido a que las instituciones argumentan falta de presupuesto. (Le puede interesar: ["Dos heridos graves en la Delfina por ataques del ESMAD"](https://archivo.contagioradio.com/dos-heridos-de-gravedad-en-la-delfina-por-ataques-del-esmad/))

Además, afirmó que **“hay población de indígenas Embera en situación de pobreza extrema** y el abandono institucional es recurrente”. Indicó que no ha habido un avance significativo en las investigaciones contra las violaciones sistemáticas de los derechos humanos que han sufrido históricamente los pueblos indígenas en Caldas.

También han denunciado en repetidas ocasiones **la presencia de grupos armados en los territorios indígenas** y “el Gobierno departamental ha negado que existan estos grupos”. Fue enfática en establecer que varios líderes y autoridades indígenas han sido amenazados de muerte por paramilitares de las AUC y las Águilas Negras.

### **Fuerza Pública ha vulnerado los derechos humanos de estas comunidades** 

Giraldo informó que la Fuerza Pública **no ha permitido el ejercicio de prensa** que han realizado las comunidades y tampoco ha brindado las garantías para el desarrollo de la movilización social. “El comandante del ESMAD dijo en tono grosero que iba a impedir por todos los medios que bajáramos a la carretera”, aseguró Giraldo. (Le puede interesar: ["Indígenas denuncian agresiones con armas de fuego por parte del ESMAD"](https://archivo.contagioradio.com/indigenas-denuncian-agresiones-con-arma-de-fuego-por-parte-del-esmad/))

Manifestó que cuando una comisión intentó bajar a verificar lo que estaba sucediendo, **“el ESMAD lanzó gases lacrimógenos** y se metieron en la comunidad indígena”. Cuando la comunicadora se dispuso a grabar los hechos, un integrante del Escuadrón la hirió en la pierna con balas de goma y le solicitó que le indicara la razón de la agresión a los que el uniformado le contestó que no se había identificado.

Sin embargo, Giraldo asegura que en ese momento **tenía puesto su chaleco del Consejo Regional**. Luego del incidente, las agresiones contra los manifestantes continuaron y  fueron denunciadas a través de un comunicado.

Finalmente, Giraldo dijo que los indígenas no han podido descender a las carreteras y les tienen restringido el paso por las diferentes vías. También indicó que los medios de comunicación regionales **tienden a dar información errónea** por lo que las autoridades indígenas han pedido que no se estigmatice a los indígenas.

[Comunicado violación de derechos Minga en Caldas (1)](https://www.scribd.com/document/363317804/Comunicado-violacio-n-de-derechos-Minga-en-Caldas-1#from_embed "View Comunicado violación de derechos Minga en Caldas (1) on Scribd") by [Anonymous 9gchRS](https://www.scribd.com/user/379499957/Anonymous-9gchRS#from_embed "View Anonymous 9gchRS's profile on Scribd") on Scribd

<iframe id="doc_59727" class="scribd_iframe_embed" title="Comunicado violación de derechos Minga en Caldas (1)" src="https://www.scribd.com/embeds/363317804/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-S0PDrxOynwSSQUnieQcX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_21847502" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21847502_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
