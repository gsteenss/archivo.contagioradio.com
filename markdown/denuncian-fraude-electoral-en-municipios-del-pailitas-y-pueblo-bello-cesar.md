Title: Denuncian fraude electoral en municipios del Pailitas y Pueblo Bello, Cesar
Date: 2015-10-27 17:00
Category: Judicial, Nacional
Tags: candidato del partido Liberal Luis Said Castro, Carlos Javier Toro Velásquez del Movimiento Alternativo Indígena y Social “MAIS”., irregularidades en los comicios, Municipio de Pailitas, Municipio de Pueblo Bello, por primera vez postulo un candidato a la alcaldía Saúl Tobías Mindiola del partido Verde
Slug: denuncian-fraude-electoral-en-municipios-del-pailitas-y-pueblo-bello-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/municipios-cesar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MAIS 

<iframe src="http://www.ivoox.com/player_ek_9184232_2_1.html?data=mpallpeXdo6ZmKiakpyJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb8fmwtrRx5DJsMbX1dTfw9GPqc%2Bfztrby8jNtMrj1JDRx9GPlMLdzc7hw9iPvY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodolfo Vega, MAIS] 

###### [27 oct 2015] 

En el departamento del Cesar en los municipios de Pailitas y Pueblo Bello, las comunidades denuncian que se presentaron irregularidades en los comicios del pasado domingo.

Por un lado, en el municipio de Pailitas, l**a oposición desconoce los resultados electorales en los que fue ganador el candidato de MAIS Carlos Javier Toro,** por lo que se han generado acciones de incitación a la violencia de parte de las autoridades. Así mismo, se denuncia que el alcalde actual "descaradamente esta metido en los escrutinios para favorecer a su candidato", dice la información entregada por la comunidad.

En Pueblo Bello, se denuncia que a la comunidad indígena Arhuaca **"le están robando literalmente la alcaldía en las urnas",**  teniendo en cuenta que por primera vez el pueblo arhuaco postuló un candidato a la alcaldía, Saúl Tobías Mindiola del partido Verde quien obtuvo 3.287 votos, y pese a que el 80% de población  del municipio es indígena, el candidato por el partido de la U, Juan Francisco Villazón, fue el ganador. **"Ganamos en el pueblo pero perdimos en la Registraduría”, dice Rodolfo Adán Vega presidente del Movimiento Alternativo Indígena y Social MAIS.**

**“Queremos garantía de los entes de control como la MOE y La Registraduría** para que exista transparencia en el reconteo electoral que da como ganador a Carlos Javier del Movimiento Alternativo Indígena y Social.

De acuerdo con Vega, en los resultados electorales se evidenció trashumancia electoral, fraude en las mesas de  control de los testigos, por lo que se pide que durante el reconteo **exista una veeduría internacional.**
