Title: Nuevo año de movilizaciones tras incumplimientos del gobierno
Date: 2015-05-15 10:53
Author: CtgAdm
Category: Movilización, Nacional
Tags: ANZORC, Asociación Nacional de Zonas de Reserva Campesina., César Jeréz, Cumbre Agraria, Cumbre Agraria Étnica y Popular, incumplimientos del Gobierno, Juan Manuel Santos, movilizaciones campesinas, Paro Agrario
Slug: nuevamente-habra-un-ano-de-movilizaciones-tras-incumplimientos-del-gobierno
Status: published

##### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_4501024_2_1.html?data=lZqdk5WWeI6ZmKiak5aJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktbZ18bax9PYqYzcwsffh6iXaaKljNrbjcaJh5SZo5bcjcnJb87j187Zy9%2FFp8rjz8rgjdnWpdSfytPQ19LUcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Jeréz, Asociación Nacional de Zonas de Reserva Campesina.] 

**“Nuevamente el balance es negativo, el gobierno se raja en el cumplimiento”,** es la valoración que se hizo el día de ayer durante la Asamblea Nacional de la Cumbre Agraria, y como la reseña César Jerez, vocero de la ANZORC, Asociación Nacional de Zonas de Reserva Campesina.

**Aproximadamente 300 personas, de todas las regiones y organizaciones sociales**, hicieron parte de la segunda Asamblea Nacional de la Cumbre Agraria, Étnica y Popular, donde se realizó una evaluación de los 2 años de diálogos con el gobierno nacional y un año de la instalación oficial de la mesa única de la Cumbre Agraria.

**“El gobierno busca hacerle conejo a los compromisos con las organizaciones campesinas negras e indígenas,** así mismo se hizo una valoración de las mesas del país y se hizo un balance que resultó negativo”, asegura Jerez.

Aunque el vocero de la ANZORC, señala que el gobierno aun tiene tiempo para corregir, si sigue en contra de las soluciones de la mano de las organizaciones “**vamos a tener un año de movilizaciones nuevamente”.** De hecho, en Catatumbo, ya hay una ruta establecida y una serie de movilizaciones, así mismo al sur y nororiente del país los campesinos se están alistando para manifestarse en contra de los incumplimientos, apropósito también de la **crisis que viene para el sector cafetero, cocalero** **y la minería artesanal** que muchas veces es tildada de criminal.

Por su parte, el presidente Santos no ha manifestado intensión de reunirse con los voceros de la Cumbre hasta que no realice una valoración propia al interior del gobierno, según dice Jerez, quien agrega que “el gobierno siempre deja pasar el tiempo y cuando se ve encerrado por la circunstancias toma acciones de última hora pero nunca cumple”, por ello, hace falta voluntad política  para que se concreten los acuerdos.

**La reunión sirvió como proceso de unidad de las organizaciones** que  hacen parte de la Cumbre Agraria, pese a las diferencias que hay entre las mismas, por lo que aseguran que las organizaciones salieron fortalecidas de la asamblea nacional, hay un nivel de comprensión mutuo, “vamos a salir adelante y gananciosos frente a la estrategia del gobierno que busca nuevamente no cumplir”, expresa César Jerez, ya que de acuerdo a él, "**las personas están animadas y no están dispuestas a aceptar mas conejos**… nuestra fuerza no solo está en la propuesta programática sino en la capacidad de movilización”.
