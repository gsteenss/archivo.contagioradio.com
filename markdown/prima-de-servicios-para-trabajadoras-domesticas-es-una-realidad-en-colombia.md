Title: Prima de servicios para trabajadoras domésticas es una realidad en Colombia
Date: 2016-11-28 21:20
Category: Mujer, Nacional
Tags: Empleadas Domésticas, Prima de Servicios Trabajadores Domésticos, Trabajo Digno
Slug: prima-de-servicios-para-trabajadoras-domesticas-es-una-realidad-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/primadomesticas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ángela Robledo] 

###### [28 Nov 2016] 

El 28 de Noviembre del presente año fue presentada por el Ministerio de Trabajo la campaña pedagógica que busca sensibilizar a la ciudadanía sobre la nueva **Ley 1788 del 7 de Julio de 2016,** la cual otorga prima de servicios para trabajadoras domésticas a partir del 1 de Diciembre.

Clara López,  Ministra de Trabajo afirmó que el proyecto de ley impulsado por el **Partido Verde desde el año 2015**, en cabeza de las congresistas **Claudia López y Ángela María Robledo,** **“beneficiaría aproximadamente a un millón de personas en todo el país,** que desarrollan labores como el cuidado del hogar, cuidado de adultos mayores, conductores familiares, mayordomos, jardineros y trabajadores de fincas”.

Dicha Ley **entra en vigencia a partir del 1 de Diciembre, es decir que los y las empleadoras deberán hacer el primer pago de la prima de Navidad a sus trabajadores domésticos hasta el 20 de Diciembre.** El pago que correspondería al semestre de Julio a Diciembre generaría a quienes devenguen un salario mínimo de \$689.455, una prima de servicios de \$383.578.

### **¿Qué pasa con quienes nos cumplan la Ley?** 

Las y los trabajadores podrán notificar al Ministerio de Trabajo **en** **caso de incumplimientos de pago por parte del empleador a la línea gratuita 120**, desde cualquier celular o teléfono fijo, para que la entidad pueda hacer seguimiento del cumplimiento de la Ley.

De ser necesario se aplicarán las sanciones correspondientes a los empleadores que evadan la ley, según lo dispuesto en el **artículo 65 del Código Sustantivo del Trabajo que indica un pago de sanción moratoria** correspondiente a un día de salario por cada día de retraso en el pago de la prima.

### **¿Cómo fue el proceso?** 

María Roa presidenta de la **Unión Sindical de Trabajadoras Afrocolombianas del Servicio Doméstico**, relata que dicha iniciativa nace a partir del descontento de varias empleadas domésticas que manifestaban **malos tratos, discriminaciones, arduas jornadas laborales y casos de esclavitud doméstica.**

La asociación que nace en la ciudad de Medellín, da inicio a un proyecto que recoja sus demandas y las de otros y otras trabajadoras de estas áreas, y encuentran apoyo de la abogada **Viviana Osorio de la Escuela Nacional Sindical quien les brinda asesoría y lleva dicho proyecto a manos de las congresistas del Partido Verde en 2015.**

La representante a la Cámara Ángela María Robledo señaló que sólo algunos sectores se opusieron a dicho proyecto de Ley argumentando que **“al otorgarle beneficios a las y los trabajadores domésticos esos empleos van a desaparecer”**. Frente a ello el equipo del Partido Verde junto a la Escuela Nacional Sindical lograron argumentar que dicha labor es de primera necesidad para muchas familias y empresas, por lo que no es posible que desaparezca.

Por otra parte María Roa, manifestó que reciben con alegría la vigencia de la Ley puesto que es **“una deuda histórica con los trabajadores domésticos, que en su mayoría son mujeres y nunca se nos había hecho un reconocimiento”.**

### **¿Qué pasa con quienes trabajan sólo por días?** 

Teniendo en cuenta que una buena parte de trabajadores domésticos en Colombia trabajan por periodos inferiores a un mes, el Ministerio de Trabajo estableció una regla para la liquidación de la prima de servicios:

**Prima de Servicios = valor del salario mensual x Número de días trabajados / en 360 días del año. **

También esta cartera dispone que a quienes trabajan por días, es decir que devenguen menos de dos salarios mínimos, debe incluírseles un auxilio de transportes.

###### Reciba toda la información de Contagio Radio en [[su correo]
