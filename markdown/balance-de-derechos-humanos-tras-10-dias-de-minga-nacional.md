Title: Balance de derechos humanos tras 10 días de Minga Nacional
Date: 2016-06-09 16:46
Category: Paro Nacional
Tags: agresiones esmad, Cumbre Agraria, Minga Nacional
Slug: balance-de-derechos-humanos-tras-10-dias-de-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Balance-DDHH-Minga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [9 Junio 2016 ] 

[Integrantes de la Minga Agraria denuncian **hostigamientos, amenazas y agresiones físicas y verbales contra los manifestantes, por parte de la fuerza pública** que se ha presentado en los territorios en los que diferentes sectores populares se movilizan en el marco del paro agrario, pese a que el Gobierno ha asegurado priorizar la garantía de los derechos humanos y estar abierto al diálogo. ]

[A pesar de las declaraciones públicas del Gobierno, los reportes de la situación de derechos humanos indican que hasta el momento cinco indígenas han muerto en los primeros diez días de Minga Nacional, así mismo **persiste la estigmatización de la fuerza pública y las agresiones** que profieren contra los manifestantes, vulnerando sus derechos, según se reitera en el más reciente comunicado del Alto Comisionado de las Naciones Unidas para los Derechos Humanos.]

[Entre los hechos denunciados se destaca la irrupción del ESMAD el pasado martes en el [[corregimiento de Bruselas](https://archivo.contagioradio.com/esmad-arremetio-contra-manifestantes-en-pitalito-huila/)], Huila, mientras los campesinos desarrollaban una actividad pedagógica; el **disparo a quemarropa con el que un miembro de la fuerza pública atacó a un estudiante** de la Universidad Del Valle, que se manifestaba en apoyo a la Minga; así como la herida en un ojo que sufrió un estudiante de la Universidad Distrital con un gas lacrimógeno que fue lanzado este jueves en el marco del acto que se realizó en memoria de [[Miguel Angel Barbosa](https://archivo.contagioradio.com/fallecio-estudiante-udistrital-agredido-por-el-esmad/)].]

[Otra de las denuncias tiene que ver con la inteligencia ilegal de la que fueron víctimas los estudiantes del SENA en Mosquera, Cundinamarca, quienes mientras marchaban fueron seguidos por un taxi de placas ODR-520, con vidrios polarizados, desde el que fueron filmados y fotografiados; así mimso, en Santander en Palmas del Socorro, **agentes de la Policía, fotografiaron a voceros y líderes campesinos**, quienes temen por el destino de este material, dada la alta presencia de paramilitares en la región. ]

[Tras la reunión que se dio en Santander de Quilichao con organizaciones internacionales, la Cumbre denuncia que **aumentó el número de agentes de la fuerza pública, quienes se concentran en los territorios muy cerca a los manifestantes**, pese a que les soliciten que se distancien para evitar incidentes y heridos. Así mismo, el vocero campesino José Vidal, fue amenazado por un anónimo vía telefónica, quien le pidió una gran suma de dinero, lo más sospechoso señalan integrantes de la Minga, es que al teléfono celular que se comunicaron fue obtenido exclusivamente para informar sobre el desarrollo de la movilización.]

[En el corregimiento de Besotes, Cesar se concentran efectivos del ESMAD, en diferentes puntos que impiden el paso de los manifestantes, quienes detectaron el **sobrevuelo** **de drones y aviones de las fuerzas armadas**;** **mientras que en el corregimiento La Mata se observa el patrullaje de uniformados en motos sin placas. En Antioquia, en el municipio de Santa Rosa, uniformados de la policía, identificados con las placas 55468, 56607 y 96485, agredieron y provocaron verbalmente a los manifestantes.  ]

[Teniendo en cuenta estos hechos, la Minga Nacional Agraria, Campesina, Étnica y Popular en su más reciente comunicado asegura que las **acciones puestas en marcha por el gobierno, no son una muestra de conciliación y mucho menos de apertura al diálogo**, pues reflejan una falta de respeto hacia la protesta y los movimientos sociales que vulnera los derechos humanos.]

[Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ]
