Title: "Esta es mi tierra" Eduardo Galeano y su relación con Montevideo
Date: 2015-09-03 11:43
Author: AdminContagio
Category: 24 Cuadros
Tags: "Esta es mi tierra" documental, 75 años Eduardo Galeano, Eduardo Galeano y Montevideo, Televisión Española
Slug: eduardo-galeano-y-su-relacion-con-montevideo-en-esta-es-mi-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/eduardo-galeano-2027089h430.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lanación.com.ar 

###### [3 Sep 2015] 

Un 3 de septiembre de 1940, Montevideo, Uruguay fue testigo del nacimiento del periodísta,escritor y crítico pensador Eduardo Galeano; y es de su ciudad natal, de la que habla en el documental "Esta es mi tierra".

Producido por Televisión Española TVE, dentro de un ciclo en el que figuras del arte, las letras y la cultura, recorren un lugar del mundo, a partir de su relación física, espiritual y sensorial de los espacios.

La perspectiva ofrecida por el escritor latinoamericano en el audiovisual, es claramente crítica aunque optimista de la capital Uruguaya, esa que inspiró sentimientos profundos expresados en sus relatos y crónicas entre otros.

Hoy en la conmemoración de los 75 años de su nacimiento, compartimos el trabajo audiovisual, disponible de manera libre en la red, con el propósito de difundir parte de su pensamiento y las reflexiones hechas con respecto a su relación con la tierra, la misma de la que partió el pasado 13 de Abril.

<iframe src="https://www.youtube.com/embed/E-dCaE_K724" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
