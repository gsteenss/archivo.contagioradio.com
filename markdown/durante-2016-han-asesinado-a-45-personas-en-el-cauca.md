Title: Durante 2016 han asesinado a 45 personas en el Cauca
Date: 2016-03-08 15:11
Category: DDHH, Nacional
Tags: Cauca, Derechos Humanos, Paramilitarismo
Slug: durante-2016-han-asesinado-a-45-personas-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [8 Mar 2016] 

La Red por la Vida y los Derechos Humanos del Cauca denuncia que en el primer trimestre del 2016 se han presentado varias situaciones de violación de los derechos humanos, entre las que se **encuentran 45 asesinatos, varios atentados y múltiples amenazas a activistas en esta región del país,** que las autoridades han pasado por alto.

Popayán, El Tambo, Buenos Aires, Mercaderes,  La Vega, San Sebastián, Almaguer, Santander de Quilichao, Argelia, Puerto Tejada y Patía, son los lugares en los que se han presentado las agresiones contra los líderes sociales, **entre los que se encuentra el asesinato de cinco mujeres.**

De acuerdo con el comunicado de la Red, **grupos neoparamilitares que se autodenominan  “Los Urabeños” y “Autodefensa Unidas de Colombia-AUC”,** han circulado panfletos anunciando “limpieza social” y además, realizan retenes en las vías del departamento, amenazando la vida de los campesinos y organizaciones que lideran procesos por la defensa de los territorios y en contra de la extracción de recursos naturales.

Entre los asesinatos se cuenta  el de la expresidenta de la Asociación Campesina Ambiental de Playa Rica – Tambo, Maricela Tombé; también el de Willar Alexander Oimé Alarcon, comunicador indígena y Gobernador Principal del Resguardo de Río Blanco-Sotará asesinado por sicarios motorizados en el centro histórico de Popayán el 2 de marzo del 2016; el homicidio de Iván Yardani Burbano, en Sucre el pasado 25 de febrero; y el asesinato de Hanner Sebastián Corpus Ramos, comunero indígena, en el municipio de Santander de Quilichao, el pasado 26 de febrero.

La Red por la Vida y los Derechos Humanos del Cauca, hace un llamado a las autoridades regionales y nacionales para que se tomen las acciones inmediatas necesarias para hacerle frente a estos sucesos que vulneran y amenazan la vida de los habitantes del departamento.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
