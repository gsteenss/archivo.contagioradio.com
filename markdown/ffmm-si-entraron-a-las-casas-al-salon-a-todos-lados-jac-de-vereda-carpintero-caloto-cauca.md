Title: “FFMM si entraron a las casas, al salón, a todos lados” JAC  de vereda Carpintero-Caloto, Cauca
Date: 2015-02-17 16:54
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Cese unilateral de fuego, Conversacioines de paz en Colombia, Derecho Internacional Humanitario, Derechos Humanos, FARC, FFMM
Slug: ffmm-si-entraron-a-las-casas-al-salon-a-todos-lados-jac-de-vereda-carpintero-caloto-cauca
Status: published

###### Foto: lapluma.net 

<iframe src="http://www.ivoox.com/player_ek_4094083_2_1.html?data=lZWmlpWcd46ZmKiak5eJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPZ1M7Rx9PYqYzYxpC3o6iPusbmxsnOjajFttHdz9nS1NSJdqSfpMbZ0dnTcaTV1sjOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Presidente de JAC Vereda Carpintero] 

Según el presidente de la Junta de Acción Comunal de la vereda Carpintero en el municipio de Caloto en Cauca, desde las 6:30 am, hasta el mediodía del pasado 16 de febrero **se presentaron combates entre las FFMM e integrantes de un grupo armado que no pudieron identificar**. “no podíamos salir de las casas” afirma el líder comunal.

Según la denuncia del líder, las FFMM ingresaban a las casas y desde allí disparaban, en una verificación posterior se pudo corroborar **que una de las casas de la vereda tenía 23 impactos de fusil**, lo que representa para ellos una clara infracción al **Derecho Internacional Humanitario,** puesto que se está poniendo en riesgo a la población civil con el uso de las casas como “trincheras” de combate.

La **Fuerza de Tarea Apolo**, emitió un comunicado en que afirman que no se infringió el DIH y los militares no se apostaron en las casas ni en la escuela de la vereda, comunicado que contradice la versión de la comunidad y de la propia **Defensoría del Pueblo**, puesto que las dos partes coinciden en señalar **el actuar de las FFMM como violatorias de los Derechos Humano e infractoras del DIH**.

Por un lado el comunicado de las FFMM afirma que “…*el Ejército Nacional en sus operaciones militares respeta los Derechos Humanos y el Derecho Internacional Humanitario*” la defensoría respalda las afirmaciones de la comunidad y señala que *“…la Defensoría Regional Cauca solicitó investigar las versiones según las cuales un contingente militar habría usado la escuela como sitio de defensa y respuesta al ataque de los presuntos guerrilleros*.”

Para el presidente de la JAC de la **vereda Carpintero**, tanto los militares como los medios de información “tapan” lo que las comunidades denuncian y “lo que digan los campesinos eso no vale, para el gobierno somos como una piedra en el zapato”. Adicionalmente las comunidades indígenas y campesinas realizarán este martes 17 de febrero una **inspección para verificar si hubo o no personas capturadas** y devueltas a la comunidad como afirman las FFMM.

 
