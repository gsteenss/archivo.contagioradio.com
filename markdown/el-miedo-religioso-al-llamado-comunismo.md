Title: El miedo religioso al llamado “comunismo”, un arma política peligrosa.
Date: 2020-03-05 16:07
Author: CtgAdm
Category: A quien corresponda
Tags: cristianismo., izquierda, izquierda y derecha, Religión
Slug: el-miedo-religioso-al-llamado-comunismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Con la medida que midan a los otros, los medirán a ustedes** (Mateo 7,2)   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bogotá, 26 de febrero del 2020  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales, mientras exigimos a los demás que no pequen o los criticamos por pecadores.  *  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hermano en la fe**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cristianos, cristianas, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordial saludo, 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dudé escribir esta carta para evitar las discusiones interminables y sin argumentos de tu “entorno religioso y político” que cataloga de “comunistas” o “izquierdistas” a personas, movimientos e instituciones de pensamiento crítico del mundo político, religioso, social, económico y cultural que cuestionan su manera de comprender y vivir la fe y la política y, que cuando les faltan conocimientos y argumentos para responder, acuden a los insultos y las descalificaciones personales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Decidí escribir por las repercusiones éticas, políticas y religiosas para la vida de creyentes y ciudadanos que el uso del miedo al llamado “comunismo” genera, en contra de los intereses de las mismas personas que lo difunden.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Espero que esta reflexión ayude a repensar mejor esta realidad.  

<!-- /wp:heading -->

<!-- wp:paragraph -->

La religión ha sido cuestionada, incluso “atacada”, por  movimientos sociales, políticos y culturales o por corrientes de pensamientos filosóficos, teológicos, sociológicos, científicos, humanistas y ambientales, que reivindican los derechos humanos, sociales, económicos, culturales, ambientales y de género, porque han visto en ella una opositora, a veces virulenta, a sus búsquedas, luchas y reivindicaciones, orientadas al bien común, al respeto a los derechos fundamentales de todas las personas (no de un grupo reducid), a la superación de racismos, esclavitudes, violencias y que exigen una mejor distribución de los bienes de la creación de Dios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es cierto que “lo religioso” ha sufrido persecuciones inaceptables y repudiables por parte de grupos que se dicen radicales y que desconocen su importancia para la sociedad. También es cierto que “sectores religiosos” han cometido abusos y atropellos con quienes piensan distinto, son distintos o cuestionan el poder establecido, con frecuencia, usando la religión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es un hecho que sectores religiosos, especialmente cristianos, se opusieron a la lucha contra la esclavitud y del racismo, por la democracia, los derechos humanos, la igualdad de las mujeres frente a la ley, por la defensa del medio ambiente, incluso por los derechos religiosos y que los llamaron “comunistas”, sin análisis serios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“*Cuando doy limosna a los pobres, dicen que soy un santo y cuando pregunto por qué hay pobres dicen que soy comunista”* (recordaba Mons. Helder Cámara)   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tanto sectores religiosos tradicionalistas como sociopolítico “liberadores”, hacen generalizaciones y descalificaciones “del otro lado”; dicen que “todos son lo mismo”, “están cortados con la misma tijera”, “son los culpables de todo lo malo”, “quieren acabar con las tradiciones familiares y religiosas”. En este ambiente de descalificación del “otro lado” y de agresiones mutuas no es posible ver el fondo de la realidad en sus dimensiones, reconocer las equivocaciones, abusos y violencias del “propio lado”; tampoco lo positivo, valiosos o las buenas intenciones que puede tener “el otro lado”, aunque tenga equivocaciones, use medios y formas cuestionables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo contradictorio es que lo que criticamos como malo en los otros, lo tenemos nosotros y tanto lo hemos “naturalizado” que no nos damos cuenta que nos parecemos a ellos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Los cristianos y cristianas, poco pensamos y aplicamos en estas discusiones las palabras de Jesús:

<!-- /wp:heading -->

<!-- wp:paragraph -->

*“La medida que usen para medir la usaran con ustedes. ¿Por qué te fijan en la pelusa que está en el ojo de tu hermano y no miras la viga que hay en el tuyo?”* (Mateo 7, 2-3).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por mirar una sola cara de la moneda, muchísimas personas religiosas viven a la defensiva frente a las corrientes de pensamiento, movimientos y organizaciones sociales críticas de esclavitudes, discriminaciones y opresiones sociales, raciales, sexuales, de género, religiosas, de la naturaleza o económicas (con limitaciones y contradicciones) que llaman genéricamente “comunistas” o “extrema izquierda”; por falta de un discernimiento adecuado arrancan la cizaña y con ella también el trigo, la buena semilla, olvidando que Jesús decía que había que dejar la cizaña hasta el momento de la cosecha, cuando los cosechadores echaran al fuego la cizaña y el trigo a los graneros (Cf. Mateo 13,24-30).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es decir, que con sus actuaciones desconocen la Palabra de Dios que dice:

<!-- /wp:heading -->

<!-- wp:paragraph -->

“*No apaguen el fuego del espíritu, no desprecien la profecía,* ***examinarlo todo y quédense con lo bueno”*** (1Tes 5,19-21); apagan el fuego de espíritu que hace nacer realidades para mejorar las condiciones de vida de las personas, los pueblos y la naturaleza; desprecian los profetas que denuncian el engaño y el abuso que hace personas del poder con personas buenas y bien intencionadas, llevándolas a tomar decisiones contrarias al “querer” de Dios.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El miedo al liberalismo, al comunismo, a la democracia, a los derechos humanos, a la justicia social, a la izquierda de una parte representativa del cristianismo, lo han llevado a rechazar sin discernimiento, sin análisis y, muchas veces, sin saber lo que se rechaza, a personas, movimientos y corrientes de pensamiento, sin darse cuenta que en sus reclamos hay “semillas del verbo”, es decir, que en ellas hay cosas que Dios quiere. No se trata de aceptarlo todo sin cuestionamiento, tampoco de rechazarlo todo sin análisis.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Este miedo religioso al “comunismo” difundido en las redes sociales está siendo usado por sectores del poder corrupto para su beneficio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Poder constituido y mantenido con injusticias, mentiras, engaños, manipulación, incluso a “sangre y fuego”. Poderes que se han enriquecido con la violencia y la guerra, sin enviar su familia al campo de batalla. Poderes que se han presentado como defensores de la religión, de las buenas costumbres, de la tradición, de la familia y de la “propiedad”, conseguida con injusticia y violencias. Poderes que ha usado y usan la religión para que nada cambie.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la historia de la humanidad, la alianza de estos poderes con sectores religiosos, han perseguido, estigmatizado y asesinado a profetas, santos, místicos, personas espirituales. Luego, sus sucesores, cuando la historia y la sociedad han reconocido su fidelidad a Dios, les hicieron monumentos, honores, los santificaron y les rezaron mientras perseguían, asesinaban, calumniaban y descalificaban a los nuevos profetas, místicos, santos y personas espirituales, con nuevas “razones” para los mismos intereses. Muchos creyentes, buena gente y bien intencionados repiten ideas sin saber lo que dicen cuando hablan, por ejemplo, de “izquierda internacional”, “castrochavismo”, “ideología de género”, “foro de San Pablo”, “conspiración venezolana”, “comunismo” y otras expresiones que generan miedos, odios y descalificaciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Definitivamente, es urgente y necesaria tener una mirada crítica de lado y lado.   

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para dejar de hacerle el juego a sectores del poder, lo invito a abrir los ojos, usar los “talentos” que Dios nos ha dado, **meditar, aplicar a la realidad actual,** las palabras de Jesús de Nazaret a los escribas o doctores de la ley y a los fariseos, las personas religiosas de su tiempo: 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*^“^¡Ay de ustedes, escribas y fariseos, hipócritas! Porque edifican los sepulcros de los profetas y adornan los monumentos de los justos, y dicen: “Si nosotros hubiéramos vivido en los días de nuestros padres, no hubiéramos sido sus cómplices en derramar la sangre de los profetas”.* *^ ^Así que dan testimonio en contra de ustedes mismos, que son hijos de los que asesinaron a los profetas. ¡Llenen, pues, la medida de la culpa de sus padres! *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*“¡Serpientes! ¡Camada de víboras! ¿Cómo escaparán del juicio del infierno?* *^ ^Por tanto, miren, Yo les envío profetas, sabios y escribas. A algunos de ellos, ustedes los matarán y crucificarán, y a otros los azotarán en sus sinagogas y los perseguirán de ciudad en ciudad, para que recaiga sobre ustedes la culpa de toda la sangre justa derramada sobre la tierra” (*Mateo 23,29-35).   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe, 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, JyP [francoalberto9@gmail.com]{}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [El Sínodo de la Amazonía. Una buena noticia para la biodiversidad, para el planeta.](https://archivo.contagioradio.com/el-sinodo-de-la-amazonia-una-buena-noticia-para-la-biodiversidad-para-el-planeta/)

<!-- /wp:paragraph -->
