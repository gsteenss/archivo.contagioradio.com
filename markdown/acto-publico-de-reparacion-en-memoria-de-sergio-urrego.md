Title: Acto público de reparación en memoria de Sergio Urrego
Date: 2015-11-11 15:54
Category: LGBTI, Nacional
Tags: Acto público de reparación en memoria de Sergio Urrego, Alba Reyes madre de Sergio Urrego, Corte Constitucional Colombiana, Sentencia T478 Corte Constitucional, Sergio Urrego, Suicidio Sergio Urrego
Slug: acto-publico-de-reparacion-en-memoria-de-sergio-urrego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Sergio-Urrego1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Diversa ] 

<iframe src="http://www.ivoox.com/player_ek_9355864_2_1.html?data=mpiil52aeI6ZmKiakp2Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcTo0JDdh6iXaaO1w9HWxdSPqMaf08rdw9fFp8qZpJiSpJjSb8bijNLSz9TWrcKfxcqYtcrWq8rjjLrf1MrLcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alba Reyes, madre de Sergio Urrego] 

###### [11 Nov 2015 ] 

[En las instalaciones del Colegio Gimnasio Castillo Campestre se  realizó el **acto público de reparación y memoria de Sergio Urrego**, ordenado por la sentencia T478 de la Corte Constitucional y que incluye la instalación de una **placa conmemorativa**, un **grado póstumo** y una **declaración pública por parte del colegio** en la que reconozca la orientación sexual del joven y el respeto que como institución educativa debía haber mostrado.]

[El acto público como espacio de reflexión y reparación por los hechos de acoso sexual sufridos por Sergio, de acuerdo con su madre Alba Reyes, no le devolverán la vida al menor pero **permitirá que continúe su legado en la lucha por una sociedad incluyente** en la que se respete al ser humano y no se le discrimine por ningún motivo, tal como lo deseaba Sergio.]

[De acuerdo con Alba Reyes desde que se denunció el caso de su hijo **los mismos niños han sido activistas en la denuncia de los manuales de convivencia** que son hechos como “códigos penales que se devuelven en su contra y no como normas de seguridad para sus vidas”, así mismo se han evidenciado casos relacionados que estaban invisibilizados ante la opinión pública.]

[El caso en términos penales continuará con la **audiencia de imputación de los cargos de discriminación, falsa denuncia y ocultamiento de pruebas contra las directivas del Colegio**, que se realizará el próximo 3 de diciembre, en la que de acuerdo con Alba Reyes se espera que no haya dilaciones pues como familia la labor que les queda es exigir el cumplimiento cabal de la sentencia.]
