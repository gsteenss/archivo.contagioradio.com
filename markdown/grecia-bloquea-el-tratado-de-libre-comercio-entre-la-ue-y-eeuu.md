Title: Grecia bloquea el Tratado de libre comercio entre la UE y EEUU
Date: 2015-02-04 20:37
Author: CtgAdm
Category: Economía, El mundo
Tags: Syriza bloquea TTIP, Tratado de libre comercio entre UE y EEUU, TTIP EEUU y EU
Slug: grecia-bloquea-el-tratado-de-libre-comercio-entre-la-ue-y-eeuu
Status: published

###### Foto:Stop-Ttip.org 

El ministro para la reforma administrativa de Grecia ha anunciado que no apoyará el Tratado de libre comercio entre la UE y EEUU. De esta forma **quedaría bloqueado a través del veto griego**, siendo imposible su aprobación, ya que es necesaria la unanimidad del pleno del Consejo Europeo.

Yorgos Katrúgalos añadió que, si es necesario, también lo bloqueará a través del parlamento nacional. En declaraciones ha dicho que el **peligro de dicho tratado radica en que las multinacionales pasen a un primer plano controlando no solo lo económico, sino también las políticas sociales de los Estados miembros.**

El TTIP inicia en las negociaciones entre el presidente Geoge Bush de EEUU y del europarlamento Giulio Andreotti en 1990. Con el objetivo de favorecer los lazos comerciales entre la Comunidad económica Europea y EEUU.

Hoy el TTIP es cuestionado por todas las fuerzas de izquierda de Europa, sindicatos y movimientos sociales, quienes lo califican como una amenaza para las libertades, los derechos laborales y  las políticas sociales que quedan supeditadas a los interés de las grandes corporaciones.

Algunos analistas señalan que con el TTIP el aparato de **justicia quedaría supeditada al sector privado** y sus intereses**,** la perdida de derechos laborales en referencia a que **EEUU solo ha firmado 2 de 8 acuerdos de la OIT y la privatización de sectores públicos claves** en beneficio de las multinacionales.

Otra de las críticas es el **total hermetismo con el que se esta negociando, convirtiéndolo casi en secreto** para los grupos parlamentarios. Según las proyecciones la negociación que debería finalizar este mismo año.

Dicho tratado sería un paso más en la aplicación de las políticas neoliberales de los principales partidos europeos, que conlleva recortes en derechos laborales y sociales, y como consecuencia el empobrecimiento de la población.
