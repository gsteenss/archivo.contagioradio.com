Title: Congreso no ha autorizado tropas estadounidenses y pide rectificar a Mindefensa
Date: 2020-09-03 18:58
Author: AdminContagio
Category: Actualidad, Nacional
Slug: congreso-no-ha-autorizado-tropas-estadounidenses-y-pide-rectificar-a-mindefensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Carta-Lidio-Garcia-militares-estadounidenses.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Lidio-Garcia-pide-rectificar-a-Carlos-Holmes-Trujillo-supuesto-permiso-a-tropas-estadounidenses.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El expresidente del Senado Lidio García, mediante una carta dirigida al Ministro de Defensa, Carlos Holmes Trujillo**, pidió rectificar la información difundida por este, en la que aseguraba que el Congreso había autorizado la presencia de tropas estadounidenses en el país.** (Lea también: [Episodio de las Brigadas militares estadounidenses es ejemplo de la política exterior colombiana](https://archivo.contagioradio.com/episodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El senador García, señaló en la misiva que con esa declaración se estaba faltando a la verdad, ya que el Senado de la República no había aprobado la presencia de dichas tropas** y clarificó que la carta que el Ministro de Defensa utilizó como justificación para el regreso a las labores de los militares extranjeros **«*no tiene ningún alcance jurídico que comprometa al Congreso de la República*»** y que esta tenía un alcance simplemente «*informativo»*.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89180,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Carta-Lidio-Garcia-militares-estadounidenses-784x1024.jpg){.wp-image-89180}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Algunas horas después de que el expresidente del Senado, hubiese solicitado la rectificación oficial; el ministro Carlos Holmes Trujilo respondió que no se rectificaría. (Le puede interesar: ["Brigadas de EE.UU. en Colombia tiene potestad para asesorar grupos irregulares"](https://archivo.contagioradio.com/brigadas-de-ee-uu-que-llegaron-a-colombia-tienen-potestad-para-asesorar-grupos-irregulares/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CarlosHolmesTru/status/1300481468024619013","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CarlosHolmesTru/status/1300481468024619013

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a este episodio, también se conoció una carta que le envió el senador Jorge Enrique Robledo, al presidente Duque en la que le pide **retirar a Carlos Holmes Trujillo del Ministerio de Defensa por** **«*haber mentido, agredido al Senado e inducido al Gobierno a violar la ley*».**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las tropas estadounidenses retomaron labores pese a decisiones judiciales que ordenaron suspenderlas

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado viernes el ministro Carlos Holmes Trujillo, por medio de una rueda de prensa, oficializó la reactivación de operaciones de las tropas militares estadounidenses en el territorio colombiano. **El ministro declaró que las tropas reanudaron labores desde el pasado 20 de julio, pese a que el Tribunal Administrativo de Cundinamarca había ordenado suspender las actividades de estas Brigadas a través de un fallo de tutela.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1299311239886254081","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1299311239886254081

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El ministro justificó la decisión indicando que la Presidencia de la República había recibido una comunicación remitida por el entonces presidente del Senado, Lidio García, la cual fue firmada por 69 congresistas, en la que, según él, daban visto bueno a la presencia de las tropas estadounidenses en Colombia. **Dicha comunicación es por la que actualmente, el senador García está pidiendo la rectificación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente al hecho, varios senadores aclararon que e**l Congreso nunca aprobó la presencia de esas tropas en el territorio nacional, porque el tema no se ha había sometido a deliberación y votación en la plenaria del Senado.** (Lea también: [Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Particularmente, el senador [Iván Cepeda](https://twitter.com/IvanCepedaCast), señaló que **el Ministro de Defensa estaba violando la ley y la Constitución y que era un «*prevaricador*».** Además, Cepeda afirmó que se estaba violando una orden judicial, que la retoma de actividades de las tropas estadounidenses coincide con la agudización de la violencia y las masacres en el país; y que citaría a un debate de control político a Carlos Holmes Trujillo por este actuar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el ministro de Defensa insiste en señalar que las labores que están realizando las tropas estadounidenses en el territorio nacional no requieren autorización del Senado, esto se contradice con lo fallado por el Tribunal Administrativo de Cundinamarca que suspendió las actividades hasta tanto no se produjera la autorización formal por parte del Congreso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe anotar, que **la omisión del trámite formal para la reanudación de estas operaciones por parte del Ministro Carlos Holmes, podría ser interpretada por el Tribunal como un desacato a la orden emitida por su despacho, lo cual es sancionado incluso con el arresto del funcionario.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
