Title: Comisión de Acusaciones de la Cámara actúa con celeridad pero contra la JEP
Date: 2019-04-10 22:42
Author: CtgAdm
Category: Paz, Política
Tags: Comisión de acusaciones de la Cámara, JEP, Patricia Linares
Slug: comision-de-acusaciones-de-la-camara-actua-con-celeridad-pero-contra-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/presidenta_de_la_jep_patricia_linares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:EFE 

Luego de mucho tiempo de pública “inactividad” **la Comisión de Acusaciones de la Cámara de Representantes anunció la apertura de una investigación en contra de la magistrada Patricia Linares**, presidenta de la Jurisdicción Especial para la Paz (JEP), organismo del SIVJRNR que ha sido blanco de múltiples acciones, **consideradas por amplios sectores sociales como, ataques del uribismo en contra del Acuerdo de paz**.

Lo que resulta paradójico de la situación es que a pesar de que en el tribunal reposan múltiples denuncias contra altos funcionarios del Estado desde las épocas de Belisario Betancourt o Julio Cesar Turbay, pasando por Alvaro Uribe, Andrés Pastrana o Juan Manuel Santos, **la única investigación que muestra avances es la que inicia un congresista del uribismo contra la misma Jurisdicción Especial de Paz**.

### **El saldo en rojo de la Comisión de Acusaciones de la Cámara** 

Además, la actuación de **Ricardo Ferro, representante a la Cámara por el Tolima e integrante del Centro Democrático, se da tan solo una semana después de su posesión como presidente de la Comisión de Acusaciones de la Cámara** en la que han reposado, por lo menos, 3.528 denuncias contra magistrados de altos tribunales, de los cuales el 99% han sido archivados y los demás están apenas en etapa de indagación preliminar.

Uno de los cuestionamientos a esta **falta de funcionamiento del organismo parlamentario tiene que ver con las múltiples denuncias contra el senador Álvaro Uribe.** Según una investigación periodística de 2018, en la Comisión de Acusaciones hay por lo menos **51 procesos activos contra el expresidente de los cuales ninguno está avanzando.**

Cabe resaltar que dentro de las tareas pendientes que tiene la Comisión están las denuncias en contra de altos funcionarios por el escándalo de Odebrecht o el llamado “Cartel de la Toga”, situaciones que llaman la atención dado que **por primera vez en la historia esta comisión recibe una denuncia, abre una indagación preliminar y afirma que acogerá versiones preliminares de personas acusadas en esa instancia**.

### **Las reacciones al anuncio del presidente de la Comisión de Acusaciones contra la JEP** 

A este respecto se pronunció el senador Iván Cepeda, quien a través de su cuenta de Twitter afirmó que “La única actuación que ha producido en mucho tiempo la comisión de acusaciones de la Cámara -presidida por un uribista- es contra la magistrada Patricia Linares, presidente de la JEP, y otro magistrado de la misma jurisdicción. Una más de las artimañas de la campaña contra la Paz”.

### ¿Funcionará como Comisión de “Absoluciones” en un proceso contra la presidenta de la JEP? 

Lo que se sabe de la acusación en contra de Patricia Linares y Alejandro Ramelli es poco. Sin embargo **hay dos posibles situaciones.** Ya hubo una denuncia sobre que, **supuestamente, Linares habría incurrido en delitos de tráfico de influencias e interés indebido en la celebración de contratos para el nombramiento de su hermana en la Unidad de Víctimas**.

Otra situación tendría que ver con un, supuesto cruce de correos de **Ramelli con Linares en las que, supuestamente, se habría entregado información privada de la Corte Constitucional** respecto al fallo que ese tribunal debía expedir en torno a la estructura administrativa del Tribunal Especial para la Paz. Ninguna de estas acusaciones ha sido probada y se espera que la Comisión actúe en derecho y sin presión política del partido que actualmente la preside.

###### Reciba toda la información de Contagio Radio en [[su correo]
