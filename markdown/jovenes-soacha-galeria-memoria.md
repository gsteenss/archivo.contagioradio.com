Title: Una galería para recordar a jóvenes de Soacha a 10 años de su desaparición
Date: 2018-09-21 11:00
Author: AdminContagio
Category: Sin Olvido
Tags: Luz Marina Bernal, madres de soacha
Slug: jovenes-soacha-galeria-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/20170727_105935-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Soacha Ilustrada 

###### 21 Sep 2018 

Luego de 10 años de la desaparición de 16 jóvenes de Soacha, Cundinamarca, víctimas de ejecuciones extrajudiciales, **Luz Marina Bernal**, madre de Fair Leonardo Porras, convoca a la ciudadanía a participar en una **"Galería de la memoria" que tendrá lugar este 21 de septiembre en la céntrica Plaza de Bolívar de Bogotá**.

El evento, busca mantener viva la exigencia de justicia, verdad y reparación, "en la memoria de los colombianos, los **crímenes cometidos por el Estado**, que dejaron como víctimas **a las que como yo somos madres y nos arrebataron cruelmente a nuestros hijos**", aseguró Bernal en una comunicación.

**El caso de Fair Leonardo**

Fair Leonardo Porras Bernal, era un joven de 26 años que **el 8 de enero de 2008** fue reportado como desaparecido. El 12 de enero, medicina legal reportó a su familia **haberlo encontrado en una fosa común de Ocaña, Norte de Santander**, con 13 impactos de bala dos de los cuales le destruyeron el rostro.

Fair, era un joven que presentaba una serie de discapacidades cognitivas, lo que lo hacía ser más ajeno e inocente del fenómeno de violencia en Colombia, **no sabía leer, ni escribir, no conocía el valor del dinero; y fue obligado a conocer el mal de la peor forma**.

Después del asesinato de Fair su madre Luz Marina Bernal, se unió a otras cuatro madres víctimas, y desde su dolor **comienzan a luchar por reivindicar el buen nombre y la memoria de sus hijos**. El 18 de ese mismo año, Luz Marina conoce el Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), desde allí se empodera y empieza a realizar activismo.

Hoy es integrante y fundadora del Colectivo Madres de Soacha, defiende los derechos humanos y promueve la paz. Su frase **“Parí a mi hijo, pero él me parió para la lucha por los derechos humanos**” se ha convertido en un símbolo de su lucha.

La organización clama por la verdad, la justicia, la reparación integral y garantías de no repetición, lo que le ha valido  varios reconocimientos internacionales por los derechos humanos, viajó a la Habana, Cuba para hacer presencia en los acuerdos de paz y en las elecciones recientes se presentó como candidata al Senado de la República en representación de las víctimas.
