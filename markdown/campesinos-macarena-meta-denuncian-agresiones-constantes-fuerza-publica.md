Title: Campesinos de La Macarena, Meta denuncian agresiones constantes por parte de la fuerza pública
Date: 2019-01-25 18:24
Author: AdminContagio
Category: Comunidad, DDHH
Tags: abusos Fuerza Pública, Meta
Slug: campesinos-macarena-meta-denuncian-agresiones-constantes-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito.militares.militar.soldado.soldados.afp_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 25 Ene 2019

[**La asociación ambiental campesina del Lozada Guayabero ASCALG** denunció a través de un comunicado el incremento de  la presencia de miembros de la fuerza pública en el municipio de La Macarena en el Meta, habitantes de la zona temen que se presenten afectaciones humanas y materiales tal como sucedió cuando militares procedieron a allanar las viviendas y fincas de los campesinos en octubre de 2018.]

En aquel entonces, el Ejército realizó un fuerte operativo **en las veredas Guaduas y Platanillo** contra los campesinos del sector, como resultado cinco campesinos fueron detenidos, fueron decomisadas **más de 700 cabezas de ganado** y destruidos varios establos, lo que además de generar temor en la población también afectó la economía de la región.

Los habitantes de la zona denuncian que desde mediados de enero ha sido evidente un incremento de la fuerza militar en las veredas de **Bocas del Perdido, Paraiso, La Atlántida, Los Alpes, El Tapir, Bajo Raudal, y Alto Raudal**, según el comunicado,  los militares les han dicho a los pobladores "que vienen a realizar allanamientos en todas las viviendas campesinas y que solo están esperando la orden para iniciar".

Ante nuevos casos de personas que han sido detenidas e interrogadas, "señaladas de pertenecer a la guerrilla solo porque pasan por la carretera y regresan otra vez", las comunidades han decidido hacer un llamado al diálogo civilizado y  que se evite el uso de la violencia por parte de la Fuerza Pública.

[Denuncia Asociación ambient...](https://www.scribd.com/document/398240899/Denuncia-Asociacion-ambiental-campesina-del-Lozada-Guayabero#from_embed "View Denuncia Asociación ambiental campesina del Lozada Guayabero on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Denuncia Asociación ambiental campesina del Lozada Guayabero" src="https://www.scribd.com/embeds/398240899/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=false&amp;access_key=key-OPu7AK6D6ieWhjyvUzIR" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
