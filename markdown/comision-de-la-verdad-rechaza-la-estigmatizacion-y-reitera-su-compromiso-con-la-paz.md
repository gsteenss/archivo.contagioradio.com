Title: Comisión de la Verdad rechaza la estigmatización y reitera su compromiso con la paz
Date: 2020-07-14 10:00
Author: CtgAdm
Category: Actualidad, Nacional
Tags: comisionados, comsión de la verdad, DDHH, Francisco de Roux, paz
Slug: comision-de-la-verdad-rechaza-la-estigmatizacion-y-reitera-su-compromiso-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Primer-Diálogo-para-la-No-Repetición-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray"} -->

[*Foto: ComisiónVerdadC*](Foto:%20ComisiónVerdadC)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Comisión de la Verdad** este 14 de julio lamentó, a través de un comunicado, no recibir retractación alguna tras los señalamientos que hizo el ex ministro Juan Carlos Pinzón hacia los y las comisionados. Además señala que dar por terminada la discusión sobre este asunto informa que **estudiaran las formas de proteger los derechos violados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 10 de julio Juan Carlos Pinzón Bueno, ex Ministro de Defensa de Colombia en el período 2011 a 2015, a través de sus redes sociales señaló que, "l***a mayoría de los comisionados \[Comisión de la Verdad\] registran afinidad ideológica o nexos con grupos armados"*** , lo que desató diferentes comentarios de quienes daban razón a Pinzón, y los que apoyaban el trabajo de la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre estas la de la **Jurisdicción Especial para la Paz y la Unidad de Búsqueda de Personas Dadas por Desaparecidas**, organizamos que [rechazaron](https://www.ubpdbusquedadesaparecidos.co/comunicados/comunicado-a-la-opinion-publica/)las declaraciones en contra de los miembros de la Comisión de la Verdad. Señalando que *"**ha sido recurrente por parte de algunas personalidades públicas que se oponen al proceso de paz la estigmatización** y cuestionamiento de la idoneidad, objetividad y compromiso de quienes conforman el Sistema Integral de Verdad Justicia Reparación y No Repetición (SIVJRNR*)*".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Las y los integrantes de la comisión de la verdad así como todas las personas que conformamos las otras instituciones del Sistema Integral, nos identificamos en que no estamos de acuerdo con la guerra y que nuestras acciones apuestan a una sociedad influyente y democrática"*
>
> <cite>*UBPD* | Comunicado </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte la Comisión de la Verdad señaló, que "[esta afirmación es contraria a la verdad](https://comisiondelaverdad.co/actualidad/comunicados-y-declaraciones/sobre-las-afirmaciones-de-juan-carlos-pinzon-bueno), deslegitima a la institución y pone en riesgo la vida de las y los comisionados, y de todos los miembros de la entidad"; por tanto el 10 de julio exigió una retractación pública de parte del ex ministro.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Todos los comisionados han puesto sus vidas al servicio de la verdad y de la paz (..), es totalmente falso que alguna de ellas o de ellos y menos la mayoría tengan cualquier nexo de pertenencias o subordinación o lealtad ante grupos armados".*
>
> <cite>Comisión de la Verdad | Comunicado 10 de julio</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y en ese mismo comunicado extendieron al ex Ministro Pinzón la invitación para conocer la institución y a sus miembros, *"y participar con sus aportes en la búsqueda de la verdad".* Invitación que concluyó en una reunión el día 11 de julio entre el ex Ministro y el director de la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez Pinzón respondió en una [declaración](https://pinzonbueno.com/declaracion-publica/) que "***un segmento grande de colombianos que no se siente representado por las instituciones que creó el Acuerdo de La Habana"***, y por tanto su pronunciamiento es un *"llamado de atención sobre la necesaria corrección del rumbo. Una mayor representatividad, sería garantía"*, de estas instituciones que hacen parte de SIVJRNR.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y aclaró que el ***"haber utilizado la palabra “nexos” tenia la intención de referir a nexos ideológicos o políticos"***, y ante el uso de la palabra "afinidades" señaló que se da tras una *"observación de algunas opiniones públicas y trayectorias de **miembros de la Comisión que considero están alineadas con una de las partes que aspiran a que su visión de los hechos se convierta en la única verdad."***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y pese a que aclaró varios puntos y reiteró su compromiso con la verdad y la paz en Colombia, no presentó algún tipo de retractación ante las acusaciones presentadas días atrás, y agregó que la Comisión de garantizar una ***"conversación equilibrada, representativa y confiable que garantice una construcción de una memoria para la no repetición".***

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/i/status/1283042590703706115","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/i/status/1283042590703706115

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Analizando la situación la Comisión de la Verdad, informó que daba por terminada la discusión sobre este asunto; y que estudiará las formas de proteger los derechos violados que dieron lugar a las exigencias de retractación. (Le puede interesar: [«La Comisión de la Verdad y el esclarecimiento de crímenes ambientales»](https://archivo.contagioradio.com/crimenes-ambientales-marco-del-conflicto-armado/))

<!-- /wp:paragraph -->
