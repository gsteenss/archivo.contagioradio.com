Title: Cinco efectos perturbadores del cambio climático
Date: 2015-03-13 18:40
Author: CtgAdm
Category: Ambiente, Otra Mirada
Tags: Ambiente, Ambiente y Sociedad, Calentamiento global, cambio climatico, medio ambiente
Slug: cinco-efectos-perturbadores-del-cambio-climatico
Status: published

##### [Foto: www.proclamadelcauca.com]

<iframe src="http://www.ivoox.com/player_ek_4210895_2_1.html?data=lZeekp2deY6ZmKiak5WJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiReYzX0NPgx8jZqc%2FXysbgjcnJt8Tjz9TQy8nFt4zn0Mffx5DJsIzXwtLPy9SPp83dzoqwlYqlddXdxNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Milena Bernal, Ambiente y Sociedad] 

El último informe del Panel Internacional de Cambio Climático (IPCC), **“Cambio Climático: Impactos, Adaptación y Vulnerabilidad”** enunció las cinco observaciones más “perturbadoras” sobre los efectos del calentamiento global.

El presidente del IPCC, Rajendra Pachauri, afirma que  “**nadie en este planeta será intocable a los efectos del cambio climático”**, advirtiendo que la situación va a empeorar.

El informe realizado por 500 expertos internacionales, enumera cada una de las cinco observaciones respecto a las consecuencias de este fenómeno. Milena Bernal, quien dirige este nodo en la organización Ambiente y Sociedad habló para Contagio radio sobre cada una de los efectos respecto al caso colombiano.

**1. La producción de alimentos no estará a la par del crecimiento poblacional:** Este es uno de los temas que más afecta a Colombia, ya que se generarán problemas en la producción agrícola, la pesca y aumentará la acidificación de los mares.

Además, debido al cambio climático, en los últimos años el Fenómeno del Niño y la Niña se presentan cada año, cuando debía ser cada 5. Estos cambios, generan una disminución en la producción de alimentos, lo que produce un aumento en el precio de los mismos, “lo que hará muy probable el aumento de la pobreza y la agitación social en muchos países”, informó Ambiente y Sociedad quien cita al “Diario Ecología”.

De acuerdo al estudio, cuando el calentamiento local exceda los 4ºC “los riesgos en la seguridad de los alimentos se volverán muy significantes”, de hecho, el aumento de tan solo un grado de temperatura, producirá graves impactos en cultivos de maíz y trigo.

**2. Las islas pequeñas enfrentarán amenazas grandes:** Con el aumento del nivel del mar las islas pequeñas empezarán  desaparecer, en el caso colombiano, "la isla de San Andrés, podría inundarse en unos 50 años" expresa Milena Bernal.

**3. El crecimiento del nivel del mar desplazará a millones de personas:** Según el informe “las zonas costeras más bajas de todo el mundo representan solo el 2% de la tierra pero hospedan al 10% de la población mundial, unas 600 millones de personas (13% es población urbana)”. Debido al aumento del mar, las personas que habitan en lugares costeros deberán mudarse.

**4. La adaptación no podrá prevenir todos los impactos negativos:** De acuerdo a los científicos la mitigación y la adaptación, son las formas de enfrentar este fenómeno, sin embargo, “hay algunos lugares y algunas amenazas para las que existen solo unas pocas opciones para adaptarse. Por ejemplo, sin importar lo que los humanos hagan ahora, el Ártico seguirá calentándose y derritiéndose, así mismo, los mares serán más acidos”, señala la revista Smithsonian.

**5.** **A los 4ºC de calentamiento, el cambio climático será el impacto humano dominante en el planeta:** “Más del 80% de la superficie de la Tierra ha recibido el impacto de la actividad humana”, indica el Diario Ecología. Esto hace que el cambio climático sea el mayor factor de impacto en los ecosistemas.

Milena Bernal, de Ambiente y sociedad, concluye que “sino se hace una reducción inmediata y drástica los efectos del cambio climático continuarán perjudicando el nivel de vida de la población mundial”.
