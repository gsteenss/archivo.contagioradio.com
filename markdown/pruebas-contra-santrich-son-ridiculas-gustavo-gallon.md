Title: Pruebas contra Santrich son "ridículas" Gustavo Gallón
Date: 2018-04-30 15:17
Category: DDHH, Política
Tags: Humberto de la Calle, Iván Márquez, Jesús Santrich, proceso de paz
Slug: pruebas-contra-santrich-son-ridiculas-gustavo-gallon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Santrich.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [30 Abr 2018] 

<iframe src="https://co.ivoox.com/es/player_ek_25710416_2_1.html?data=k5qkk5WYdZehhpywj5aYaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPi8LgzYqwlYqmd8-f1NTP1MqPsMKf1M7h18bHrYa3lIqvldOPqMafq8rgh6iXaaO11JDAw9PYtsrXyZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Jesús Santrich, integrante de la dirección política del partido FARC, cumple 22 días capturado, hecho que, para el director de la Comisión Colombiana de Juristas, Gustavo Gallón, se ha realizado con pruebas **“ridículas” que no tendrían ninguna validez jurídica**. De igual forma aseguró que el proceso debe ser llevado por la JEP sin intervenciones de autoridades extranjeras y respetando la presunción de inocencia.

### **Las pruebas contra Santrich** 

De acuerdo con Gustavo Gallón, las pruebas que hasta el momento se han exhibido contra Jesús Santrich son “ridículas”. En primera medida afirma que los audios de las grabaciones de conversaciones sostenidas por el integrante de la FARC con Marlon Marin, quien es un colaborador de la DEA y otras dos personas, **no merecen tener mayor credibilidad**.

“Marlon Marín, es colaborador de la DEA, ha sido señalado de tener relación con actos indebidos, con contratos y estar pidiendo coimas y demás. De un momento a otro la DEA se lo lleva como testigo protegido, y bueno él y sus supuestos contactos en el cartel de Sinaloa, han declarado que el señor Santrich estaría negociando la entrega de 10 toneladas de coca. Y es la palabra de ellos que son unos infiltrados, y los que proponen la operación, contra Santrich”.

Además de esa conversación, está la grabación de una conversación sostenida solo entre Santrich y Marín en la que, el integrante de la FARC afirma “yo a esa reunión no voy solo, yo sin usted no voy”. **Frase que para Gallón demuestra que Santrich tenía conocimiento de lo que se estaba proponiendo Marín**.

Finalmente está el cuadro que Santrich le regaló a Rafael Caro Quintero, un peligroso narcotraficante mexicano, que se ha presentado como prueba. Hecho que para Gustavo Gallón evidencia que Santrich creía que Caro era una persona que simpatizaba con la paz. Frente al rumor de que no se han mostrado todas las pruebas, el abogado manifestó que **“si se muestran pruebas que por lo menos alguna de ellas sea convincente”**.

### **El proceso contra Santrich** 

Asimismo, otra de las denuncias que han envuelto la captura y el proceso de Santrich, es validez que tenían las autoridades nacionales para hacer efectiva la orden de captura internacional, pasándose por alto los Acuerdos de Paz y la Jurisdicción Especial para la Paz. En ese sentido el abagado Gallón afirmó que “**sí Santrich está implicado en algún acto indebido posterior a la firma del acuerdo, por supuesto que se deben tomar las acciones correspondientes**”.

Allí entrará la JEP a revisar si el delito cometido hace o no parte de lo tramitado dentro de sus competencias y si las pruebas lo implican o no, sin embargo, Gallón afirmó que "las autoridades judiciales nacionales y el ejecutivo tienen que ser prudentes para no tomar una decisión apresurada".

De igual forma señaló que en este proceso debe prevalecer una presunción de inocencia y que es lamentable cómo la gente se ha dejado convencer de las pruebas que han mostrado los diferentes medios de información. (Le puede interesar: ["Defensa de Santrich alerta sobre el deterioro de su salud tras su captura"](https://archivo.contagioradio.com/defensa-de-santrich-alerta-sobre-deterioro-de-su-salud-tras-su-captura/))

### **La supuesta investigación contra Iván Marquez** 

<iframe src="https://co.ivoox.com/es/player_ek_25710502_2_1.html?data=k5qkk5WZdJOhhpywj5WZaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjbLFttWZpJiSo6nSqduZk6iY1dTGtsafxtGYw9fYaaSnhqaxxdrQs4zYxtGYucbQsIzH1dfSx9mPjtDpjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Referente al artículo publicado por el Wall Street Journal en donde se afirmaba la existencia de una investigación contra Iván Marquez por narcotráfico, el abogado asesor del proceso de paz, Diego Martínez, aseguró que tales pruebas no existen, que la Fiscalía Nacional negó que esa investigación exista y que ese rumor hace parte de un intento de una alianza entre sectores de Estados Unidos y campañas políticas en Colombia para “tirarse la paz”. (Le puede interesar: ["Manejo del caso de Santrich por parte de la Fiscalía ha sido imprudente: Enrique Santiago"](https://archivo.contagioradio.com/manejo-del-caso-santrich-por-parte-de-la-fiscalia-ha-sido-imprudente-enrique-santiago/))

De otro lado aseguró que este tipo de acciones solo envían un mensaje de inseguridad jurídica y de engaño hacia las bases del partido político FARC, “al final lo que refleja todo esto, **es que existen sectores que añoran regresar a la guerra y están queriendo forzar a la FARC para que regrese a ello**”.

### **“Se están tirando los Acuerdos de Paz”** 

En una rueda de prensa el candidato a la presidencia Humberto De La Calle, manifestó que tanto el Centro Democrático, en representación de Álvaro Uribe Vélez e Iván Duque, con las “falacias” que han tejido en contra de los Acuerdos, como la falta de voluntad por parte del gobierno para implementarlos y la poca pedagogía que ha hecho la FARC, **han ido deteriorando los Acuerdos.**

Razón por la cual hizo un llamado a la sociedad en general a que se movilicen en defensa de los Acuerdos de paz y que generé una presión que obligue al gobierno del presidente Santos a tomar medidas urgentes frente a la protección de este pacto.

###### Reciba toda la información de Contagio Radio en [[su correo]
