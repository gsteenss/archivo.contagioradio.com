Title: El Cerrejón buscaría desalojar a 27 familias Wayúu para desviar el arroyo Bruno
Date: 2017-04-08 21:17
Category: Ambiente, Nacional
Tags: ANLA, El Cerrejón, La Guajira, Mineria
Slug: cerrejon-buscaria-desalojar-27-familias-wayuu-desviar-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat Agua Viva 

###### [8 Abr 2017] 

La empresa minera Carbones El Cerrejón continúa buscando la manera de desviar el arroyo Bruno en el departamento de La Guajira. Pero en medio de ese afán, cerca de **27 familias indígenas Wayúu que viven a menos de 10 metros del arroyo** serían víctimas de desalojo forzoso, como lo denuncia Leobaldo Sierra, líder de la comunidad.

De acuerdo con Sierra, actualmente la empresa usa a dos mujeres que dicen ser dueñas del predio que habitan las comunidades indígenas desde hace 40 años. Sin embargo, una mujer de nombre Soraida ha llegado al predio en los últimos meses asegurando que esos son terrenos de ella, y que los indígenas son “invasores”, como lo narra el líder comunitario, quien agrega que **la mujer amenazó son “sacarlos a las buenas o a las malas”.**

**Y es que desde 1992 las comunidades vienen siendo amenazadas y violentadas,**  según cuenta Leobaldo Sierra. Para ese año, grupos paramilitares asesinaron varios integrantes de la comunidad y por lo que las familias se vieron obligadas a desplazarse. Luego en el 2010 la guerrilla de las FARC acabó con la vida de varios indígenas y nuevamente debieron desplazarse.

La versión de Sierra asegura que **El Cerrejón llegó a ese territorio con un abogado en el año 2012 alegando que esos terrenos no eran de los indígenas sino de la empresa**, sin embargo la compañía no pudo comprobar sus supuestas denuncias y, usando la fuerza pública amenazaron a la población con desalojarlos a la fuerza, pero nunca lo hicieron.

Las comunidades que vienen gestionando una consulta popular para impedir que en esa zona se lleve a cabo la desviación del arroyo Bruno, recibieron hace un mes a entidades como **la, Agencia Nacional de Licencias Ambientales, ANLA y Corpoguajira,** quienes llegaron a la zona  a verificar si se puede o no realizar la consulta. “Ellos lo que quieren es sacarnos a la fuerza, porque **el desvío del arroyo Bruno pasa en medio de nosotros y no les conviene tenernos acá** porque que les interrumpimos la explotación de carbón".

“Nosotros no estamos de acuerdo con ningún desvío. No nos interesa el billete que ofrecen, nos interesa el agua, que es la vida”, asevera Sierra, y añade, que si la empresa desvía el arroyo, **deberán caminar más de 30 minutos para conseguir algo de agua.**

<iframe id="audio_18058567" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18058567_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
