Title: Empresas bananeras de Uraba son investigas por concierto para delinquir y financiación de grupos armados
Date: 2020-05-13 09:50
Author: CtgAdm
Category: Nacional
Tags: Empresas Bananeras
Slug: empresas-bananeras-de-uraba-son-investigas-por-concierto-para-delinquir-y-financiacion-de-grupos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-13-at-11.37.55-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: ContagioRadio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Tribunal Superior de Antioquia, Sala Especializada en Restitución de Tierras, entregó este 12 de mayo una sentencia en la que **ordena realizar investigaciones a dos empresarios y tres empresas bananeras del Urabá**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las empresas que serán investigadas son Bananeras de Urabá S.A, BANACOL y UNIBAN, entidades con mayor exportación de este producto en Colombia; y a quienes el Tribunal abrirá investigación por **concierto para delinquir y financiación voluntaria a grupos paramilitares,** una acción declarada como delito de lesa humanidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado los empresarios involucrados son, **Rosalba Zapata y Felipe Arcesio Echeverri Zapata,** a quienes se les acusa de *"**presuntamente, crear la trama del despojo,en la que resultó favoreciendo a Bananeras de Urabá S.A.**, empresa que entró a ocupar y explotar indebidamente las tierras"*, indica la Fundación Forjando Futuros en su página web.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo **Gerardo Vega**, director de la Fundación Forjando Futuros y representante de las familias víctimas, señala, "*Las empresas Bananeras se aprovecharon de las circunstancias de un entramado jurídico y lo que hizo fue difamar una contrarreforma agraria en contra de la Constitución y en contra de la ley"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Para nadie es un secreto que hay investigaciones y múltiples denuncias contra las empresas bananeras en el Urabá en completa impunidad,** mientras que en otros casos se condena en Colombia no pasa nada los procesos van y vienen de despacho"*
>
> <cite>**Gerardo Vega** | Director de la Fundación Forjando Futuros</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La orden también señala la [Restitución](http://www.forjandofuturos.org/noticia_interior.php?id_ent=787)de **11 predios a familias campesinas en la vereda California corregimiento de Nueva Colonia, en Turbo**; lugar con mayor plantación bananera junto a las zonas de influencia de la construcción del Puerto de Urabá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Son 50 hectáreas aproximadamente lo que en esta sentencia restituye a 11 familias que fueron despojadas de sus tierras hacia entre 1999 y 2000. **Despojos a cargo de frentes paramilitares y empresas bananeras**"*, afirmó Vega.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó que a la fecha no se ha podido hacer entrega de los predios, *"**por las circunstancias sanitarias no ha sido posible hacer la entrega material**, actualmente estamos a la espera de que se levanten las restricciones para solicitar al juez que haga la entrega de estos predios"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez informó que otro obstáculo para la entrega a sido el abandono por parte de la empresa bananera que lo usaba, *"s**e pueden llegar a presentar una situación fitosanitaria debido a que no hay fumigación ni quién cuide los cultivos, lo que puede traer plagas** que ponen en riesgo los cultivos y a los campesinos"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Son muchos los procedimientos para hacer la entrega material, por eso estamos a la espera de que las familias reciban por ahora una entrega administrativa, mientras los jueces hacen la entrega material*"

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente Vega agregó que la orden de la jueza también exige una compensación económica a las 13 familias despojadas de su tierra. (Le puede interesar: <https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/> )

<!-- /wp:paragraph -->
