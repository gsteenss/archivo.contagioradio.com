Title: La táctica de la derecha para generar caos y miedo
Date: 2019-05-23 19:24
Author: CtgAdm
Category: Nacional, Política
Tags: Centro Democrático, Conmoción interior, constituyente, Duque
Slug: tactica-derecha-generar-caos-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DUQUE-Y-CUPULA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Presidencia] 

En el trascurso de la última semana diferentes hechos alteraron la actualidad política del país: la decisión de la Jurisdicción Especial para la Paz (JEP) en el caso de Jesús Santrich, la renuncia del Fiscal Néstor Humberto Martínez y la propuesta de **decretar una situación de conmoción interior** para dar manejo a ambos hechos; pero, ¿estamos en medio de una coyuntura que amerite la declaratoria de conmoción interior o el llamado a una asamblea nacional constituyente?

### **"Esto no es sino una táctica de la derecha de este país para generar caos y miedo"** 

> !Pillados!: quieren dar esta sensación de caos para ambientar una constituyente que reforme la justicia, cierre las cortes y reduzca la rama judicial a una única corte que sea del bolsillo del eterno.
>
> — Daniel Samper Ospina (@DanielSamperO) [16 de mayo de 2019](https://twitter.com/DanielSamperO/status/1129089609193934848?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Para Mauricio Torres, abogado constitucionalista, lo que ocurrió la semana pasada fue la "puesta en escena de un ambiente de inestabilidad institucional generado por la renuncia del Fiscal". En su momento, Néstor Humberto Martínez sostuvo que su decisión se fundamentó en razones morales, ante un peligro contra la constitución y el orden jurídico en el país causado por la decisión de la JEP, que permitía la libertad de Jesús Santrich y garantizaba su no extradición.

A ello podría sumarse el rechazo votado por el legislativo de las objeciones presidenciales a la Ley Estatutaria de la JEP, que pretendían modificar la capacidad de juzgar de este mecanismo. Ambas situaciones fueron calificadas por Martínez como un desafío al "Estado de derecho". (Le puede interesar:["JEP niega extradición de Jesús Santrich y ordena que sea liberado"](https://archivo.contagioradio.com/jep-niega-extradicion-de-jesus-santrich-y-ordena-que-sea-liberado/))

Sin embargo, el Jurista afirmó que **"no estamos en ningún escenario de inestabilidad institucional o choque de trenes"** entre la Fiscalía, la JEP y las Altas Cortes; esto, porque la Constitución política establece los procedimientos y mecanismos que deben operar en casos como el de Santrich: si se acusa a un rincorporado de las FARC de cometer un delito, se deben enviar las pruebas ante la Jurisdicción para que analicen la fecha de la comisión de la conducta.

En cambio, el Abogado sostuvo que las razones de la renuncia de Martinez, y del presidente Iván Duque para apoyar su renuncia **generaron "el ambiente propicio para hablar de una constituyente o una reforma a la justicia"**; aunque para Torres esté claro que "no estamos en ningún escenario de choque de trenes" o de conmoción interior. (Le puede interesar:["Comunidades rurales opinan sobre la renuncia de Martinez Neira"](https://archivo.contagioradio.com/comunidades-rurales-opinan-sobre-la-renuncia-de-martinez-neira/))

### **Un choque de trenes... ¿provocado?** 

Tras la decisión de la JEP en el caso Santrich (y la renuncia del Fiscal) apareció una prueba en vídeo no entregada a la Jurisdicción; en este caso, Torres explicó que estos archivos deberían ir de nuevo al mecanismo de Justicia Transicional para ser analizados. Sin embargo, el hecho de que la Fiscalía haya cuestionado el procedimiento, y señalado que culminaba en una violación a la Constitución, hace pensar que es **el ente acusador el que quiere provocar un 'choque de trenes**'; y que el mismo tiene "fines medíaticos y fines políticos, pero es innecesario".

### **¿Qué posibilidades hay de que se busque la constituyente o declarar la conmoción interior?** 

La asamblea constituyente se declara cuando existe una condición difícil de superar por la Constitución vigente, situación que para el Abogado no ha ocurrido; adicionalmente, requeriría de la unión de muchas fuerzas políticas para aprobar tal decisión. Aunque este es un escenario que Torres ve difícil y que "no se puede dar en un corto plazo, su discusión genera efectos de zozobra en las personas".

> Hay quienes dicen q en la reunión q sostuvo el Pr Duque con los partidos de gbno, excepto la U, se dijo q sino se lograba un acuerdo político se procedería a decretar la conmoción interior.Parece q el acuerdo imposible es la excusa para la conmoción interior y hacer trizas La Paz
>
> — Guillermo Rivera (@riveraguillermo) [20 de mayo de 2019](https://twitter.com/riveraguillermo/status/1130613426613575681?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por su parte, la conmoción interior es un recurso del Presidente de la República para responder a grandes riesgos de tipo político, económico o social que puedan afectar al país. Mediante este recurso se otorgan facultades extraordinarias al primer mandatario para emitir decretos con fuerza de ley, que son revisados únicamente por la Corte Constitucional, y pueden incluso suprimir derechos como la libertad de prensa.

Sobre esta opción, que se advirtió como una posibilidad del Presidente para extraditar a Santrich, Torres dijo que parecía el producto de un choques entre el ala más radical del Centro Democrático y la posición del presidente Duque; sin embargo, aclaró que era un escenario posible, en el que **se podría dar un golpe a las dinámicas de la JEP** entre otras cosas, con la extradición del negociador de paz.

Torres manifestó que a pesar de que el estado de conmoción interior es una posibilidad, sería rechazado por la opinión pública, y añadió que los decretos amparados por una declaración de este tipo probablemente no contarían con el apoyo de la Corte Constitucional. No obstante, el constitucionalista reiteró que no hay un escenario que amerite tal declaración. (Le puede interesar:["Crece rechazo al llamado acuerdo nacional convocado por el presidente Duque"](https://archivo.contagioradio.com/rechazan-acuerdo-nacional/))

> Cuando el subpresidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) dice que no considera la Constituyente una solución, ¿podemos creerle como cuando dijo que no apoyaría el fracking, que no subiría los impuestos, que no sería títere de [@AlvaroUribeVel](https://twitter.com/AlvaroUribeVel?ref_src=twsrc%5Etfw), que protegería a los líderes sociales, etc. etc.?
>
> — Enrique Santos Molano (@esantosmolano) [21 de mayo de 2019](https://twitter.com/esantosmolano/status/1130689712547598336?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_36299640" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36299640_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
