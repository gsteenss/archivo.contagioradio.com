Title: Paramilitares controlan hasta el transporte de las FARC a Zona Veredal
Date: 2017-02-02 15:55
Category: Entrevistas, Paz
Tags: FARC, paramilitares, Zona Veredal
Slug: paramilitares-torpedean-construccion-de-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [ 2 Feb. 2017] 

Control perimetral y del transporte, cobro de dinero en el puerto, chantajes a los trabajadores de la Zona Veredal Brisas y el silencio de los militares y la Policía, son tan solo algunas de las dificultades que han enfrentado los integrantes del frente 57 de las FARC al arribar a Brisas del Atrato en Chocó.

Pablo Atrato, comandante del frente 57 de las FARC hizo una grave denuncia en cuanto al transporte que los está movilizando **“la información que tenemos es que los transportes que nos trajeron de Bojayá hasta acá (Brisas)** la mayoría de los conductores y las personas que contrataron **tienen tratamiento directo con los paramilitares”. **Le puede interesar: [Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

Agrega el comandante, que muchos de estos paramilitares son reconocidos por personas en la zona lo que ha generado bastantes dificultades para los guerrilleros “hemos conocido que **los constructores que deben venir a las Zonas Veredales han venido negociando con los paramilitares** para que les facilite hacer el trabajo, porque los están chantajeando”. Le puede interesar: [FARC denuncia incumplimientos del gobierno en reincorporación de menores de edad](https://archivo.contagioradio.com/farc-denuncia-incumplimientos-del-gobierno-reincorporacion-menores-edad/)

En la actualidad los integrantes del Mecanismo Tripartito de Monitoreo y Verificación ya están al tanto de esta denuncia, a quienes también se les ha manifestado que **en Brisas existe una cooperativa de mototaxistas que tienen relación con paramilitares “los cuales llegaron ayer en masa a vernos, a reconocernos** y la gente está preocupada porque se tiene esa situación” afirmó Atrato.

Aunque este frente ya ha exigido al gobierno nacional respuestas a la grave situación que se afronta por la presencia de paramilitares en la zona, Atrato asevera que esperan las soluciones sean prontas pues **“confianza en los militares o en la Policía no tenemos porque ellos conviven juntos**, trabajan juntos y no pasa nada, creemos que debe cumplirse el acuerdo que se ha establecido”.

Según Atrato, tuvieron un sin número de tropiezos en el tema de transportes y a su arribo a Brisas del Atrato **"tuvimos que dormir en la playa porque no estaban las condiciones dadas para llegar hasta la Zona Veredal** dispuesta para los hombres y mujeres de este frente".

Para el comandante de las FARC, **la guerrilla está siendo expuesta a un alto grado de improvisación** en este proceso “aunque nos entregaron la comida, no tenemos agua, tampoco áreas comunes” y agrega "nos ha tocado estar como hemos estado hace 52 años, haciendo nuestros cambuches para dormir. Y en eso estamos todavía. (…) De esta manera estamos aportando a la construcción de la Zona donde vamos permanecer los guerrilleros durante todo este proceso". Contenido relacionado: [Protocolo de reincorporación para menores de las FARC fue improvisado](https://archivo.contagioradio.com/el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado/)

Concluye diciendo que las FARC pese a todos los tropiezos a los que se han visto enfrentados se mantendrán para cumplir lo acordado en La Habana “pero **no dejamos de tener esos temores y las desconfianzas frente a todo el fenómeno del paramilitarismo** en la zona. Pero sobre todo preocupa que no vemos la voluntad política del establecimiento por solucionar esta situación”.

\

<iframe id="audio_16798294" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16798294_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
