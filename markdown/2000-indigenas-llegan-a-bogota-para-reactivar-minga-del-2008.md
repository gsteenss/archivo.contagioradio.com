Title: 2000 indígenas llegan a Bogotá para reactivar minga del 2008
Date: 2015-11-24 15:32
Category: Movilización, Nacional
Tags: alirio uribe, CRIC, el derecho a la protesta social y la jurisdicción especial indígena, Iván Cepeda, Minga, Minga Social y comunitaria por la defensa de la vida, ONIC, Organización Nacional Indígena de Colombia
Slug: 2000-indigenas-llegan-a-bogota-para-reactivar-minga-del-2008
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Onic-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

<iframe src="http://www.ivoox.com/player_ek_9517428_2_1.html?data=mpqemZmWfI6ZmKiakpaJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpGkkZDW0MmJh5SZoqnUx9PFt4zgzcrUw9OPpYy20Mzc1oqnd4a1kpDdw9fFb9PZwsjhy9vFtozhytPUw5DIcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Macca, Consejero Mayor CRIC] 

<iframe src="http://www.ivoox.com/player_ek_9517462_2_1.html?data=mpqemZmado6ZmKiakpiJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpGkkZDW0MmJh5SZoqnUx9PFt4zgzcrUw9OPpYy20Mzc1oqnd4a1kpDdw9fFb9PZwsjhy9vFtozhytPUw5DIcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juvenal Arrieta, Consejero ONIC] 

###### [24 Nov 2014]

**Alrededor de 2000 indígenas de 115 cabildos indígenas del Cauca y diferentes comunidades a nivel nacional hacen parte de la Minga por la Defensa de la Vida, el Derecho a la Protesta Social y la Jurisdicción Especial Indígena (JEI)** con tres objetivos dirigidos al reconocimiento de la autonomía de los pueblos indígenas; unirse a la discusión frente al país sobre el tema de garantías de la jurisdicción especial para la paz y además convocar a la movilización de la cumbre agraria étnica y popular en el 2016.

Carlos Macca, Consejero Mayor regional del Cauca, indicó que **esta movilización busca que “el Estado colombiano reconozca” lo que el movimiento indígena ha ganado en cuanto a la autonomía**, esto no solo “representado en la diversidad étnica y cultural” y ejemplificó el caso de Feliciano Valencia, **víctima de la “criminalización, penalización y judicialización”.**

Otro de los motivos que convoca a estas comunidades a Bogotá **es tratar las garantías para movilizarse** “cuando están siendo afectados cada uno de los derechos de los colombianos” afirmó Macca. Juvenal Arrieta, Consejero de la ONIC, apuntó que la agenda que trabajará esta minga será similar a la del 2008 y se realizará hasta el próximo 27 de Noviembre.

Arrieta hizo **un llamado “a la coherencia al gobierno nacional frente al trato que le está dando a los luchadores sociales y al trato que le está dando hoy a quienes están alzados en armas”**, refiriéndose a Feliciano Valencia y alertó sobre la **preocupación del papel de jurisdicción** “no entendemos como un estado que avanza sobre la jurisdicción especial para la paz, se dedique a encarcelar a los luchadores sociales”, por tanto se buscarán estrategias para trazar una ruta que coordine  esta jurisdicción y la jurisdicción indígena.

Las organizaciones aclaran  que desde el 12 de octubre de este año, las organizaciones indígenas convocaron al gobierno a un diálogo en la María Piendamó en el Cauca, pero no se llegó a ningún acuerdo con la delegación enviada por el gobierno.

**Este miercoles se realizará una gran movilización en Bogotá** con la participación de las organizaciones indígenas y diferentes sectores sociales a favor del derecho a la protesta y además en **pro de el "derecho que tienen las mujeres a no ser afectadas por la violencia que existe en Colombia"**, advirtió Carlos Macca. A estas actividades se le suman distintas ponencias y trabajo en comisiones en el Coliseo Cubierto el Campín, con las que se invita a la Cumbre Agraria, Étnica y Popular en el 2016.

[Agenda de la Minga 2015](https://es.scribd.com/doc/290999321/Agenda-Minga-Final "View Agenda Minga Final on Scribd")

<iframe id="doc_86709" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290999321/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
