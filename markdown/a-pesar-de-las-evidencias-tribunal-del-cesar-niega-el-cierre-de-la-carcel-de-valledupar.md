Title: A pesar de las evidencias Tribunal del Cesar niega el cierre de la cárcel de Valledupar
Date: 2016-02-11 17:49
Category: Uncategorized
Tags: carcel, Carcel de Valledupar, crisis carcelaria, FCSPP
Slug: a-pesar-de-las-evidencias-tribunal-del-cesar-niega-el-cierre-de-la-carcel-de-valledupar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Cárcel-La-Tramacua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FCSPP] 

###### [11 Feb 2016]

Según la abogada Gloria Silva, del Comité de Solidaridad con Presos Políticos, el tribunal contencioso administrativo del Cesar **dio crédito a la intención del INPEC** de solucionar los problemas de hacinamiento, tratos degradantes, mala calidad de la alimentación, desabastecimiento de agua, entre otros, **sin que hasta el momento se [haya realizado ningún trabajo iniciado en el último año.](https://archivo.contagioradio.com/?s=tramacua)**

En la decisión del tribunal se califica como buena la intención del INPEC de realizar los trabajos de adecuación, cuando solamente se ha intervenido una de las torres del centro de reclusión y **a un mes de haber sido entregada ya hay goteras en los techos, las baterías sanitarias están despegadas y parte de los pisos se instalaron en mal estado.**

A pesar de que caben pocos recursos para intentar hacer efectiva la orden de tutela de la Corte Constitucional que dice que se debe garantizar las condiciones de vida de los internos recluidos en la Tramacua, conocida también como “cárcel de castigo” la abogada Gloria Silva afirma que se va a impugnar la decisión del tribunal para buscar que se garantice a cabalidad lo ordenado por la tutela.
