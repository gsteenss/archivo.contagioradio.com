Title: Ejecutivos de Chiquita podrán ser investigados por la Corte Penal Internacional
Date: 2017-05-19 12:44
Category: DDHH, Judicial, Nacional
Tags: Bananeras, Chiquita, paramilitares, uraba
Slug: chiquitabananerasparamilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/palma-y-paramilitarismo-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 18 May 2017 

Organizaciones de derechos humanos solicitaron a Fiscalía de la Corte Penal Internacional (CPI) **abrir investigación sobre las responsabilidades de ejecutivos de la compañía Chiquita Brands** **en crímenes de lesa humanidad** cometidos particularmente en las regiones bananeras de Colombia.

La coalición de organizaciones, sustenta su denuncia y solicitud **en una serie de documentos internos de esa compañía** a partir de los cuales rastrearon la forma y montos entregados por los ejecutivos a paramilitares **entre 1997 y 2004**, operaciones que a pesar de haber sido reconocidas por la compañía ninguno de los responsables ha rendido cuentas ante la ley.

En la comunicación enviada, las organizaciones aseguran aportar un apéndice sellado en que **identifica los nombres de 14 empleados, ejecutivos y miembros de la junta directiva**, sobre quienes en su criterio debe centrar su atención la fiscalía de la CPI. Le puede interesar: [La macabra alianza entre paramilitares y bananeras](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/).

Su aporte a organizaciones armadas ilegales por concepto de “seguridad” permitió a la compañía consolidarse como la más rentable en el mercado bananero en el mundo, **pagos que desde el Departamento de Justicia de los Estados Unidos son considerados ilegales**.

Desde ese país, la corporación ya había sido declarada culpable en un tribunal federal en el año 2007, sin embargo **a la fecha no se ha realizado ninguna “investigación penal en contra de los ejecutivos que supervisaron e implementaron tal esquema de pagos”** y que no se ve un horizonte para un procesamiento penal en Estados Unidos a pesar de continuar el litigio civil en sus tribunales.

Las organizaciones buscan, en última instancia, que **el Fiscal de la CPI pida autorización formal de la sala de Cuestiones Preliminares para abrir una investigación** contra los ejecutivos de la compañía, si las autoridades colombianas no avanzan en el caso, debiendo ser este trabajo prioritario por la coyuntura histórica que vive el país por la implementación de los acuerdos de paz y la veeduría que de los procesos de verdad, justicia y reparación debe realizarse.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
