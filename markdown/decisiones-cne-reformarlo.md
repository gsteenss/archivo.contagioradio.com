Title: Las polémicas decisiones del CNE, ¿es necesario reformarlo?
Date: 2018-08-29 13:20
Category: Entrevistas, Política
Tags: CNE, Enrique Peñalosa, magistrados, Odebrecht
Slug: decisiones-cne-reformarlo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/colp_128924_a3b7e.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [29 Ago 2018] 

Nuevamente el **Consejo Nacional Electoral (CNE)** fue criticado desde diferentes sectores por la incoherencia en sus decisiones. La más reciente, tiene que ver con la negación de la personería jurídica del movimiento político de la **Colombia Humana,** decisión que contrasta la orden de archivar las investigaciones sobre los dineros de **Odebrecht** en las campañas presidenciales de 2010 y 2014, la suspensión del trámite de le revocatoria de **Peñalosa** y la sanción al vocero del comité que lideraba esta campaña, entre otras polémicas decisiones.

### **El CNE suspende las investigaciones a las campañas presidenciales por Odebrecht** 

En su decisión más reciente, el Consejo decidió declarar el vencimiento de la investigación contra la campaña Santos por el ingreso de dineros provenientes de la multinacional Odebrecht. Según el Tribunal electoral, **el proceso caducó porque ya pasaron 3 años desde que se cometieron los hechos.** La misma entidad se había pronunciado de forma similar sobre la campaña de Santos para los comicios presidenciales de 2010. (Le puede interesar: ["Odebrecht, la punta del iceberg de la corrupción en Colombia"](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/))

Mientras que la investigación contra la campaña de Oscar Iván Zuluaga, ya había sido archivada porque el organismo no encontró pruebas de la relación entre el ex candidato uribista y la multinacional brasileña. En esa ocasión el tribunal no halló la prueba reina que estableciera la entrada de dineros de la multinacional a la campaña del Centro Democrático, **a pesar de que hubo diferentes reuniones entre ambas partes, e incluso, hay pruebas de un viaje realizado por Zuluága y Duda Mendoça, representante de Odebrecht.**

### **CNE mató la revocatoria del Alcalde de Bogotá, Enrique Peñalosa** 

Luego de que el Comité de revocatoria al Alcalde Enrique Peñalosa logrará cumplir los requisitos necesarios para iniciar el proceso electoral que llevaría a los capitalinos nuevamente a las urnas, el CNE inició una investigación contra uno de los comités que impulso la medida y su vocero, Gustavo Merchán, por supuestamente violar los topes de financiación.

A pesar de que el Tribunal Administrativo de Cundinamarca solicitó que se aclarara prontamente esta investigación, **el organismo demoró el proceso hasta hace cerca de una semana,** fecha en la cual decidió suspender la revocatoria. Decisión con la que se impediría el desarrollo de la votación para retirar el mandato al Alcalde, en razón del tiempo que se necesita para ponerla en marcha. (Le puede interesar: ["En revocatoria no hay un desfase de dinero, pero sí un desfase democrático"](https://archivo.contagioradio.com/no-hay-un-desfase-de-dinero-pero-hay-un-desfase-de-democracia/))

### **La Colombia Humana se queda sin personería jurídica por decisión del CNE** 

Para el senador por el Polo Democrático, Iván Cepeda, la decisión sobre la personería jurídica de la Colombia Humana es otra muestra de que **el CNE, es una institución "parcializada, antidemocrática y anacrónica"** que favorece interese políticos del establecimiento y por lo tanto, es incapaz de brindarle garantías a la oposición política.

Aunque el Senador aclaró que el CNE es un organismo muy poderoso, hasta ahora, el sistema electoral del país ha demostrado que toma decisiones parcializadas porque **no está conformado de manera que sus integrantes puedan cumplir la labor de un tribunal independiente y autónomo.**

### **Es necesaria una reforma al sistema electoral colombiano** 

Cepeda afirmó que mientras se reforma el sistema electoral, los congresistas de la Bancada de Oposición buscarán impulsar las candidaturas del Abogado **Luis Guillermo Pérez Casas** y de **Fabiola Marquez,** quienes harán parte de la lista de candidatos de los cuales el Congreso elegirá hoy los **9** magistrados que compondrán el Consejo en el periodo 2018-2022. El senador agregó que esperan "tomar una decisión unificada que nos permita elegir 1 o 2 miembros del nuevo CNE".

El Congresista por el Polo aseguró que desde el legislativo, propondrán en conjunto con el **Senador Gustavo Bolivar**, una reforma para crear un nuevo poder electoral imparcial "que esté a la cabeza de un tribunal electoral, tenga expresiones regionales, y permita efectivamente investigar y sancionar todos los delitos electorales como la compra y venta de votos, los trasteos electorales, la trashumancia y el tráfico clientelista".

Cepeda concluyó recordando que el CNE es un organismo que tiene funciones importantes con influencia sobre la vida política muy significativa, en tanto es el encargado de regular todo el proceso de participación electoral, así como de **controlar y sancionar a quienes participan en la dinámica de la política de elecciones.** (Le puede interesar:["Consejo inició persecución política contra FARC: Carlos Lozada"](https://archivo.contagioradio.com/cne-inicio-persecucion-politica-contra-farc-carlos-lozada/))

<iframe id="audio_28200771" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28200771_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
