Title: Caravana de Juristas constata 54 asesinatos en Norte de Santander y Cauca en los últimos meses
Date: 2016-08-24 14:12
Category: DDHH, Nacional
Tags: Asesinatos defensores DDHH, Derechos Humanos, Víctimas en Colombia
Slug: continua-riesgo-para-defensores-de-derechos-humanos-y-victimas-en-colombia-caravana-de-juristas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/informe_derechos_onu.-lasdororillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:lasdosorillas] 

###### [24 Ago 2016 ] 

Después del recorrido de la Caravana de Juristas por Colombia, muchas son las preocupaciones que quedan, entre ellas el **riesgo que persiste para las y los defensores de derechos humanos**, el **hostigamiento por parte del Ejército a las poblaciones**, la **persecución y asesinato a militantes de plataformas de izquierda** y la **falta de atención y garantías del Estado a las víctimas y comunidades**.

Al finalizar el último día de la Caravana,[cada una de las comisiones dio su informe sobre la situación de derechos humanos en las regiones](https://archivo.contagioradio.com/55-juristas-internacionales-verificaran-situacion-de-defensores-de-ddhh-en-colombia/). En Valle del Cauca, las visitas hechas por los abogados participantes, evidenciaron las **pésimas condiciones de salubridad y el hacinamiento en la Cárcel de Jamundí**. De igual forma, corroboraron las dificultades y limitaciones que enfrentan los defensores de los presos políticos para  entrevistarse con sus defendidos.

En el departamento del Cauca, la caravana se reunión con víctimas y algunos miembros de la comunidad de Corinto cercanos a la zona aledaña a la vereda la Cominera, en donde se establecerá uno de los campamentos de las FARC, allí la comunidad [denunció el asesinato de varias personas,](https://archivo.contagioradio.com/se-agrava-crisis-de-derechos-humanos-en-el-cauca/) entre ellos militantes de Marcha Patriótica y la Unión Patriótica que ya habían sido amenazados o se encontraban en proceso de restitución de tierras, uno de ellos **recibió cuatro tiros en la cabeza** con un arma de largo alcance utilizada solo por los actores del conflicto armado.

De igual modo los habitantes informaron que batallones del Ejército Nacional se encuentran acantonados en los acueductos veredales de los municipios de Miranda, Corinto y Caloto, **generando contaminación debido a que arrojan en el agua sus desperdicios y heces fecales**, ocasionando enfermedades en las comunidades.

En Cúcuta, la comisión expresó preocupación frente al asesinato y persecución a defensores y defensoras de derechos humanos, ya que en lo corrido del año [se han registrado 50 asesinatos de líderes y lideresas](https://archivo.contagioradio.com/3-desapariciones-y-30-asesinatos-alarman-a-comunidades-del-catatumbo/), además interpelaron a la **Unidad de Víctimas y a la Procuraduría acerca de la crisis humanitaria de las víctimas**, el poco acceso a la justicia y los altos niveles de corrupción en la misma.

Frente a este panorama la Caravana de Juristas se comprometió con **acciones inmediatas como dirigir un derecho de petición a la canciller María Ángela Holguín** para que envíe una comisión y garantice los derechos a fundamentales de las comunidades afectadas en el Cauca, **realizar labores de seguimiento y respaldo a casos específicos de violación a derechos humanos** como el proceso de ASCAMCAT en el Catatumbo y de la defensora Judith Maldonado amenazada en Cúcuta.

De igual forma aseguraron que **respaldarán  las propuestas e iniciativas de paz en el país** y harán un llamado a sus respectivas embajadas para analizar más de cerca el desarrollo del pos conflicto, ya que de acuerdo con Alexander Montaña abogado de la Corporación Justicia y Dignidad,** "los escenarios en donde se habla de paz aumentan el riesgo para las y los defensores de derechos humanos y las víctimas".**

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
