Title: Ernesto: ríos de rebeldía
Date: 2020-05-02 22:20
Author: AdminContagio
Category: DDHH, Memoria, Sin Olvido
Tags: lideres sociales asesinados, memoria de colombia, Sin Olvido
Slug: ernesto-rios-de-rebeldia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-02-at-1.50.57-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-02-at-12.27.05-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

***Por: Johan Higuita***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Semblanza de Ernesto Ríos Arias a los 25 años de su asesinato: germina su legado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

A Ernesto lo mataron los paramilitares, pero lo han asesinado principalmente el silencio y el olvido; muchos años han pasado ya, la falta de memoria o la amnesia obligatoria cumplieron su labor de olvido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Atrás han quedado las historias que por miedo no se atrevieron si quiera a escapar de los labios para ser contadas. Atrás parece haber quedado ese Oriente antioqueño que se convirtió en un mar de rebeldía e inundó las calles con la voz de indignación que las comunidades levantaban, para denunciar los atropellos que en la región se cometían en nombre del desarrollo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ernesto Ríos Arias fue otro más de esta generación de profesionales e intelectuales que se ubicó del lado del sentir de los sectores populares, que supo poner sus ideas al servicio de los más necesitados. Al lado de líderes como Ramón Emilio Arcila y Julián Conrado aportó a la organización y construcción del **Movimiento Cívico del Oriente Antioqueño** durante la década de 1980-1990.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ernesto, oriundo del municipio de La Unión (Antioquia), tuvo el privilegio para su tiempo de asistir a una universidad, fue de los primeros profesionales en su municipio, abogado penalista de la Universidad de Antioquia formado bajo la influencia marxista de la convulsionada década de 1970 en el contexto estudiantil de las universidades públicas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Desde muy joven se le conoció en su pueblo como líder vinculado a las luchas juveniles por la educación pública, el deporte  y la cultura.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hacia el año 1982 se agudizó en la región la problemática que se venía presentando con la Electrificadora de Antioquia, empresa prestadora del servicio de la energía eléctrica, que compraba energía a **EPM** para revender en la región a costos exorbitantes –aun cuando la región era productora del 36% de la energía nacional gracias al complejo hidroeléctrico asentado en su territorio–. Habitantes de municipios como San Carlos y Marinilla venían manifestando su inconformidad y llevando a cabo movilizaciones locales frente a esta situación (esto se sumó a los reiterados paros locales que se presentaron en el Oriente hacia finales de los 70); también el municipio de La Unión empezaba a hacerle frente a ésta problemática y Ernesto, con su vocación de líder y la legitimidad de que gozaba en la comunidad, empezó a perfilarse como el gran líder del municipio pero también con grandes proyecciones como dirigente regional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Fue de los representantes de la Junta Cívica de La Unión en la Coordinadora Cívica Regional.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ernesto fue uno más de los dirigentes que lideró los famosos paros cívicos regionales, protestas que paralizaron por completo al Oriente antioqueño, y que incluyeron dos paros en el año 1982 y uno en 1984, a los que se refirió el gobernador Villegas Moreno como “un paro subversivo programado por doce anarquistas”. Además integró la comisión que representó a las comunidades en la negociación con la Gobernación de Antioquia, EPM y la Electrificadora de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a esta labor, en el marco del segundo paro cívico en el municipio de La Unión, horas antes de que se llevara a cabo la manifestación programada, Ernesto en compañía de otro líder fue apresado por la policía como una forma de contener la manifestación popular, lo que desembocó en una protesta local que adelantó el paro unas horas antes de lo planeado, para exigir la liberación inmediata de líder, quien de hecho había alentado y descrito la jornada como “la gente unida, los sectores políticos, comerciales, estudiantes, amas de casa, todos en el paro hasta que no soltaran el último detenido”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hizo parte de todo ese proceso de maduración del Movimiento Cívico que Ramón Emilio Arcila denominó “de la protesta a la propuesta”, haciendo mención a ese tránsito que las comunidades organizadas del Oriente antioqueño habían tenido al pasar de las movilizaciones y los paros a una propuesta política para la región, que disputara el poder a los viejos gamonales y caciques del bipartismo, como una forma de ampliar la democracia y propiciar la participación real de los sectores populares. Al respecto Ramón Emilio Arcila y el mismo Ernesto Ríos Arias, llegaron a comentar que la organización social del Oriente antioqueño era un auténtico germen de poder popular, gracias al empoderamiento  de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También Ernesto con su agudeza política vio como un avance significativo   la reforma política que permitió la elección popular de alcaldes a finales de la década de 1980, pues allí el Movimiento Cívico vislumbró la posibilidad de hacerse al poder en las localidades para trabajar en beneficio de las comunidades; por ello desde el primer momento encabezó la lista al concejo del municipio de La Unión por el Movimiento Cívico, y logró ser concejal durante dos períodos con una buena votación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### También llegó a la  presidente del Concejo Municipal.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para ese momento el apoyo popular con el que contaba el Movimiento Cívico en la región y su apuesta político-electoral, le había costado fuertes persecuciones y asesinatos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el año 1983 fue asesinado Julián Conrado en San Carlos, un médico líder en la localidad que fue el primero en caer, a partir de allí se inició un exterminio sistemático del Movimiento de Acción Sancarlitana que era la expresión del Movimiento Cívico local en San Carlos; ya en 1989 fue asesinado el máximo referente del Movimiento Cívico, Ramón Emilio Arcila un gran intelectual y dirigente, cuando era candidato -prácticamente electo- a la alcaldía de Marinilla. Aproximadamente 200 personas entre líderes y militantes de base  cayeron asesinadas en éste genocidio político, Ernesto fue uno de ellos.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el municipio de La Unión el liderazgo de Ernesto ya era reconocido, logró proyectos importantes como lo es la primera política pública deportiva del municipio, documento que él mismo construyó; también junto al Movimiento Cívico se había logrado un gran avance urbanístico gracias a la autoconstrucción de vivienda que promovieron, donde a través de la organización popular se construyeron dos barrios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ernesto Ríos participó  dentro de la recién fundada Alianza Democrática M-19, expresión política  que nació de la negociación de paz  entre el M-19 y el Gobierno Nacional; fue un arduo activista en favor de la Asamblea Nacional Constituyente como un paso fundamental para la construcción de la paz en Colombia,  también fue candidato a la Cámara de Representantes de Antioquia en el año de 1991 por éste movimiento político, logró jalonar alrededor de 6.000 votos encausando el caudal electoral del Movimiento Cívico en la región.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83897,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
[![Defendió luchas por la educación pública, la cultura y el deporte. Fue perseguido, amenazado y estigmatizado por su labor política y pensamiento crítico.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-02-at-1.50.57-PM.jpeg){.wp-image-83897}](https://bit.ly/2WluAap)  

<figcaption>
Ernesto Rios SIN OLVIDO

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El **3 de mayo de 1995** fue asesinado Ernesto a manos de sicarios en la ciudad de Medellín mientras departía en su oficina en el centro de la ciudad. Su asesinato se dio en el marco de la incursión de las Autodefensas Campesinas de Córdoba y Urabá (**ACCU**) en la zona del Altiplano del Oriente antioqueño, éste paramilitar operó en la región a través del Bloque Metro,  financiado por sectores económicos de poder, narcotraficantes y políticos tradicionales de la región, a la par que con claras alianzas con la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de Ernesto, según investigaciones llevadas a cabo por los Tribunales de Justicia y Paz, se le atribuye al Bloque Metro, en el marco de la persecución a los líderes y lideresas ligados al Movimiento Cívico del Oriente antioqueño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Respecto a la incursión paramilitar en ésta zona del Altiplano, el mismo Carlos Castaño llegó a decir que se debía a que en éste lugar estaba asentada “la verdadera oligarquía antioqueña y colombiana” con sus fincas de recreo y veranero, lo que hace creer que hubo fuertes intereses desde sectores tradicionales de poder en la región y el departamento, que tenían en sus planes “descabezar” completamente el movimiento social en la región asesinando a sus dirigentes,  por los avances significativos que había tenido en la participación política copando espacios de elección popular  y el peligro en que esta alternativa ponían el poder político de gamonales y caciques tradicionales de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [34 años persistentes en la búsqueda de las y los desaparecidos del Palacio de Justicia](https://archivo.contagioradio.com/34-anos-persistentes-en-la-busqueda-de-las-y-los-desaparecidos-del-palacio-de-justicia/)

<!-- /wp:paragraph -->
