Title: Allanamientos, detenciones ilegales y atropellos por cuenta de las FFMM en Saravena, Arauca
Date: 2015-09-08 13:35
Category: DDHH, Nacional
Tags: Arauca, Derecho Internacional Humanitario, Ejército Nacional de Colombia, Fundación Joel Sierra, Militarización de territorios, Niños y niñas víctimas del conflicto armado, Población civil en medio del conflicto armado, Saravena, Sonia López, violación a derechos humanos
Slug: allanamientos-detenciones-ilegales-y-atropellos-por-cuenta-de-las-ffmm-en-saravena-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/arauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [elturbion.com] 

<iframe src="http://www.ivoox.com/player_ek_8199839_2_1.html?data=mZamm52XfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhc3gwtPOz87JstXj1IqfpZDIqdXZz8jW0dPJt4zdzcrUw9HJt4ztjMbh1NTUqc3g0NiY0tTWb8TpjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sonia López, Fundación  Joel Sierra] 

###### [08 Sept 2015] 

[El pasado sábado 5 de septiembre en horas de la mañana, **efectivos del Ejército Nacional allanaron viviendas, detuvieron ilegalmente y atropellaron a pobladores del caserío Puerto Contreras** zona rural del municipio de Saravena, Arauca, en el marco de un operativo militar.]

[Los militares irrumpieron en las casas, alegando que allí se guardaban armas pertenecientes a grupos guerrilleros, **atacaron a los moradores tirándolos al suelo, encañonándolos y acusándolos de pertenecer a frentes de la guerrilla** con presencia en la zona.]

[El joven Neftali Duran, junto con otros pobladores, fue detenido arbitrariamente por más de 24 horas y dejado en libertad por la Fiscalía, al no encontrar pruebas que constataran su pertenencia a grupos insurgentes, como habrían asegurado efectivos militares. ]

[Sonia López, presidenta de la Fundación de Derechos Humanos Joel Sierra, asegura que miembros del Ejército Nacional mantienen acordonada la zona, impidiendo la libre movilidad. Además, se han tomado las viviendas de los pobladores,** pues anuncian que estarán allí tres meses.**]

[**Otra de las preocupaciones de los pobladores se basa en que los militares ingieren bebidas alcohólicas intimidando a las mujeres y niñas**, a quienes señalan de ser compañeras de los guerrilleros. Así mismo, l**os niños y niñas han sido usados por parte de los militares para o**btener información sobre los grupos guerrilleros de la zona, violando el Derecho Internacional Humanitario y poniendo en riesgo sus vidas.]

[Ante esta situación, se adelantan denuncias ante organismos como la Personería y la Defensoría del Pueblo, "esperando pronunciamientos contundentes y acciones efectivas que permitan solucionar la problemática", explicó Sonia López.     ]
