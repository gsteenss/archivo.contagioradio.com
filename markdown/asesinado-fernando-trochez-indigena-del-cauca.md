Title: Asesinado Fernando Trochez, indígena del Cauca.
Date: 2020-10-17 16:20
Author: CtgAdm
Category: Actualidad, DDHH
Slug: asesinado-fernando-trochez-indigena-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/55949702_1676960492406329_5701197755452489728_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

  
Las autoridades del municipio de corinto confirmaron la muerte del indígena Fernando Trochez, en el corregimiento el Jagual en Cauca. El cuerpo presentaba varios impactos de arma de fuego. La víctima era integrante de la reserva campesina de la vereda El Descanso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son 60 líderes indígenas y sociales asesinados en 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

La ola de asesinatos de indígenas en el Cauca es preocupante, este año van más de 59 líderes sociales y defensores de derechos humanos solo en ese departamento. Y en los últimos 4 años van 230 líderes y defensores de derechos humanos asesinados, según Indepaz. Encabezando así la lista de departamentos más violentos de Colombia. [Lea también: Asesinan a Angelina Collo de 65 años de edad.](https://archivo.contagioradio.com/angelina-collo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el anonimato, una líder de Argelia afirma que: “Aquí no se puede hablar de más, porque esa gente tiene ojos y oídos en todos lados. Lo malo es que uno sabe quién dispara y por qué, y está claro que ellos no hablan, asesinan. Quieren una comunidad dispersa y temerosa”.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **“Aquí no se puede hablar de más, porque esa gente tiene ojos y oídos en todos lados. Lo malo es que uno sabe quién dispara y por qué, y está claro que ellos no hablan, asesinan. Quieren una comunidad dispersa y temerosa”.**

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte el CRIC afirma que: “Estas muertes violentas, que engrosan el listado de 234 líderes indígenas asesinados durante el periodo del actual gobierno Iván Duque, la mayoría de ellos en nuestro departamento del Cauca, esto confirman que para los sectores dominantes en Colombia no se podrá cerrar el conflicto armado hasta que nuestras organizaciones, luchas y territorios queden completamente debilitadas y nuestros derechos hayan desaparecido."

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Estas muertes violentas, que engrosan el listado de 234 líderes indígenas asesinados durante el periodo del actual gobierno Iván Duque"
>
> <cite>CRIC</cite>

<!-- /wp:quote -->
