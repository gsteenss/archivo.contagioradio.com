Title: Se lanza Cartilla para la comprensión del Conflicto armado en Colombia
Date: 2016-07-25 15:00
Category: Entrevistas
Tags: Herramientas para la paz, Padre Javier Giraldo
Slug: se-lanza-cartilla-para-la-comprension-del-conflicto-armado-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/padre-Giraldo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [25 de Jul] 

El pasado jueves 21 de julio, se llevó a cabo el lanzamiento de la cartilla** "Aportes para comprender el conflicto armado en Colombia"**, una herramienta pedagógica que expone, a partir del ensayo del padre Javier Giraldo con el mismo título,  **la semilla de la guerra que ha durado más de 50 años en el país**.

La cartilla inicia con una introducción sobre **la rebeldía como derecho** y punto de partida para comprender **las raíces del conflicto armado en el país**. Por ende se explica **la problemática de la tierra en Colombia**, la acumulación de la misma, el despojo que se ha vivido y como se entrelaza con la **participación política**,  la configuración histórica del Estado y el **derecho a la información**.

De acuerdo con el padre Javier Giraldo, miembro de la Comisión Histórica sobre el Orígen del Conflicto y sus Víctimas, el reto más grande para alcanzar la paz "**es que se toque de una manera directa y eficaz la raíz de la violencia**, yo creo que en las conversaciones de la Habana no se han tocado esas raíces y creo que si no se le da una solución pues se esta dejando **viva la matriz de la violencia que en cualquier momento se puede reactivar**".

La cartilla es una iniciativa generada por la organizaciones Colombianos y Colombianas por la paz, Clamor social por la paz, la Coordinación Colombia, Europa, Estados Unidos, Conpaz, Dipaz, Movice, la Comisión de Justicia y Paz, la Plataforma Colombiana de derechos humanos, democracia y desarrollo y La Alianza.

"Una Colombia en paz, es una Colombia en donde **no haya tanta hambre**, **donde no haya tanta desigualdad**, **donde no haya tanta concentración de la propiedad de la tierra**, donde la gente tenga sus necesidades más elementales por lo menos mínimamente satisfechas" afirmo el padre Javier Giraldo.

\[embed\]https://www.youtube.com/watch?v=KVrb91n-Dig\[/embed\]
