Title: Ponencia eliminaría 170 cabeceras municipales de circunscripciones de paz
Date: 2017-09-21 16:41
Category: Nacional, Paz
Tags: Circunscripciones de paz, Fast Track
Slug: ponencia-eliminaria-170-cabeceras-municipales-de-circunscripciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/despojo-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Sept 2017] 

Como preocupante calificó el senador del Polo Democrático Iván Cepeda, el proyecto de ley radicado en  la Comisión Primera de Cámara que pretende eliminar de la votación de las circunscripciones de paz a 170 cabeceras de municipios. Para Cepeda este hecho representa** “un temor de clase política tradicional” por la participación de los sectores que han sido excluidos tradicionalmente de la política**.

Además, se podría suprimir el derecho excepcional a elegir simultáneamente, las circunscripciones especiales como las circunscripciones ordinarias, en el respectivo departamento. En un comunicado de prensa la Comisión de Seguimiento Impulso y Verificación del Acuerdos de Final, **señaló que esta ponencia viola lo pactado “carece de sentido, denota incomprensión de la ruralidad** y la territorialidad de las 16 circunscripciones de paz.

En ese mismo sentido Iván Cepeda aseguró que se está buscando una “grave deformación de los acuerdos”, a partir del debate e introduciendo este tipo de modificaciones que ya se había intentado realizar en los debates del senado, con la intensión de “darle gabelas a la clase política”. (Le puede interesar:["Circunscripciones de paz deben respetar liderazgo de jóvenes, mujeres y afros"](https://archivo.contagioradio.com/circusncripciones-de-paz-deben-respetar-liderazgos-de-jovenes-mujeres-y-afros/))

Las implicaciones de esta modificación, de acuerdo con Cepeda, es que los sectores que siempre han estado marginados por la democracia y que han sido históricamente víctimas del conflicto armado continúen sin la posibilidad de participar en la política de su país, “se podría empezar a dar un despertar de la conciencia y de la acción políticas en zonas del **país que tradicionalmente han estado bajo el estricto control de la compra venta de votos” afirmó el senador**.

### **El destino de las circunscripciones de paz** 

Cepeda expresó que el paso siguiente en la Comisión primera de la Cámara del Congreso, es que se legisle de una manera correcta “respetando el espíritu del Acuerdo de paz” y de no ser así afirmó que el siguiente paso es apelar a la Corte Constitucional que en su jurisprudencia ha dicho, que cualquier ley que se adopte bajo el mecanismo de Fast Track debe corresponder y tener conexidad con los Acuerdos de Paz.

<iframe id="audio_21022131" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21022131_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
