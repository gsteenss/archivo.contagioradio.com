Title: Radican en la CIDH alerta contra avance del fracking
Date: 2020-06-26 16:30
Author: AdminContagio
Category: Actualidad, Nacional
Slug: ante-cidh-se-radica-alerta-en-contra-del-avance-del-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/fracking.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Alianza Colombia Libre de Fracking informó este 25 de junio que **radicó una alerta ante la Comisión Interamericana de Derechos Humanos (CIDH), frente al avance acelerado del *fracking* impulsado por el Gobierno Nacional en medio de la pandemia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización el contexto de pandemia, aislamiento y el estado de emergencia decretado por el gobierno "***imposibilita la veeduría ciudadana y el control legal, ya que, la mayoría de despachos judiciales se encuentran cerrados por causa de la cuarentena*",** afectando así el derecho directo de la comunidad a la participación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Alianza Colombia Libre de Fracking— en este contexto se presentan dos irregularidades: **«Violación al acceso a la información» y «violación al derecho de acceso a la justicia»** por parte del Gobierno. (Lea también: [Fracking: una apuesta gubernamental de un modelo inconveniente](https://archivo.contagioradio.com/fracking-una-apuesta-gubernamental-de-un-modelo-inconveniente/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"very-dark-gray"} -->

### **El camino del fracking en el Gobierno de Iván Duque** {#el-camino-del-fracking-en-el-gobierno-de-iván-duque .has-very-dark-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cabe recordar que en medio de la campaña electoral de 2018, el hoy presidente **Iván Duque, señaló en varios escenarios de debate que ante un eventual gobierno suyo, no se desarrollaría la técnica del *fracking* para la [extracción de petróleo](https://archivo.contagioradio.com/el-fracking-no-es-necesario-no-le-mientan-al-pais/).** *«Colombia no necesita entrar a la discusión del fracking porque el potencial que tenemos para los próximos 10 años se puede desarrollar perfectamente con \[yacimientos\] convencionales»* «***En Colombia no se hará fracking»*** afirmó más de una vez.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/ivanduquemarquez/videos/1971394639749727/","type":"video","providerNameSlug":"facebook","align":"center","className":""} -->

<figure class="wp-block-embed-facebook aligncenter wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/ivanduquemarquez/videos/1971394639749727/

</div>

<figcaption>
Ver: Min. 6:25 a 7:02. 11/Abr/2018 Foro para la Democracia Universidad Autonoma de Bucaramanga

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Una vez en el Gobierno, Iván Duque convocó una comisión de expertos, elegidos por la misma presidencia, para evaluar la viabilidad en la implementación del *fracking* en Colombia, lo cual fue reprobado por parte de algunos sectores políticos y ambientalistas en su momento.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Esto dijo el presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) en campaña sobre el [\#Fracking](https://twitter.com/hashtag/Fracking?src=hash&ref_src=twsrc%5Etfw)  
>   
> En breve empiezo a contarles qué dijo el panel de expertos completamente escogidos por el gobierno, sobre [\#Fracking](https://twitter.com/hashtag/Fracking?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/8eeWjP3HeU](https://t.co/8eeWjP3HeU)
>
> — Angélica Lozano Correa?️‍? (@AngelicaLozanoC) [February 14, 2019](https://twitter.com/AngelicaLozanoC/status/1096162524834209792?ref_src=twsrc%5Etfw)

<!-- /wp:html -->

<!-- wp:paragraph -->

Una vez dado el aval por parte de la comisión de expertos para desarrollar Proyectos Piloto, el Gobierno expidió el [Decreto 328 de 2020](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20328%20DEL%2028%20DE%20FEBRERO%20DE%202020.pdf), poco antes de la declaratoria de la emergencia por la pandemia,  donde fijaba los lineamientos para desarrollar dichos proyectos. (Le puede interesar: [Plan piloto de fracking "es jugar con el ambiente y la salud de las comunidades"](https://archivo.contagioradio.com/plan-piloto-para-fracking/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"very-dark-gray"} -->

### Se solicita la suspensión de toda la gestión del Gobierno para implementar el fracking {#se-solicita-la-suspensión-de-toda-la-gestión-del-gobierno-para-implementar-el-fracking .has-very-dark-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Alianza señaló que el Gobierno ha emprendido una acción **«acelerada» para la implementación del *fracking* en el territorio nacional, con la expedición de normas por parte de instituciones como la Agencia Nacional de Hidrocarburos (ANH) y el Ministerio de Ambiente** —en abril y junio respectivamente— con miras a reglamentar el proceso de contratación para las empresas que llevarían a cabo los Proyectos Piloto, que serían la antesala de la aprobación formal de la técnica, en caso de que las firmas establecieran su viabilidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, solicitan al Gobierno a través de la interventoría de la Comisión Interamericana de Derechos Humanos, abstenerse de expedir cualquier tipo de reglamentación que no esté directamente relacionada con la Pandemia COVID 19, en especial aquellas relacionadas con el fracking y por ende, **suspender toda la actividad tendiente a la implementación del desarrollo de Yacimientos No Convencionales en el país**, hasta tanto se generan las condiciones necesarias para acceder a toda la información del proceso por parte de la ciudadanía y a la reanudación de actividades de la rama judicial para que pueda ejercer un debido control jurídico-legal. Le puede interesar: [Debe suspenderse todo tipo de actividad relacionada al desarrollo de fracking en el país, no solo en Santander](https://archivo.contagioradio.com/debe-suspenderse-todo-tipo-de-actividad-relacionada-al-desarrollo-de-fracking-en-el-pais-no-solo-en-santander/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Alianza junto con 4 organizaciones del Magdalena Medio esperan pronta respuesta por parte de la CIDH, esperando que las posibles acciones de parte de Corte no queden en papel por parte del Gobierno de Ivan Duque, poniendo como ejemplo el Acuerdo de Esacazú. (Le puede interesar: [Acuerdo de Escazú, un compromiso por el ambiente que va más allá de un firma](https://archivo.contagioradio.com/acuerdo-de-escazu-un-compromiso-por-el-ambiente-que-va-mas-alla-de-un-firma/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
