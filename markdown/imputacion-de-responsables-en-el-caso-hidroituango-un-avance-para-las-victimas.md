Title: Imputación de responsables  en el caso Hidroituango un avance para las víctimas
Date: 2020-12-03 03:06
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Fajardo, Hidroituango, Ituango, represa, Rios Vivos
Slug: imputacion-de-responsables-en-el-caso-hidroituango-un-avance-para-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Contraloría General presento este jueves 3 de diciembre los nombres de 19 personas imputadas en el detrimento de 4,1 billones de pesos** por la no entrada del proyecto hidroeléctrico de Hidroituango, uno de los nombres es el del exalcalde Sergio Fajardo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/NoaladobleMoral/status/1334507091462152193","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/NoaladobleMoral/status/1334507091462152193

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Aunque el informe de la Contraloría da cuenta de como la aceleración del proyecto sumando negligencias de los actores directores e indirectos, un punto que es celebrado como un avance, l**as organizaciones y víctimas exigen que así mismo acciones frente a los procesos de reparación de ellos y ellas**. ([Nos dejaron sin casa y sin con qué vivir: Barequeros desalojados de Ituango](https://archivo.contagioradio.com/nos-dejaron-sin-casa-y-sin-con-que-vivir-barequeros-desalojados-de-ituango/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hidroituango un proyecto presupuestado en \$1,1 billones que resultó en un d**etrimento de 4,1 billones**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El proyecto hidroeléctrico fue presupuestado inicialmente en un valor de \$1,1 billones, posteriormente en un segundo valor de \$2,9 billones producto de la ineficiencia en la gestión fiscal. ([Los informes ocultos de Hidroituango](https://archivo.contagioradio.com/los-informes-ocultos-de-hidroituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Datos que se suman al informe presentando en septiembre a la opinión pública, la Fiscalía y la misma Contraloría sobre el proyecto Hidroituango, elaborado por expertos en geología y mecánica de suelos, contratados por EPM y  MAPFRE y la reaseguradora del proyecto, y que  **dejaba al descubierto que Federico Gutiérrez y exmiembros de la Junta de Empresas Públicas de Medellín –[EPM](https://www.epm.com.co/site/)– ocultaron información*.***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por tanto esta imputación se convierte en un eje fundamental en el procesos de verdad y justicia que esperan cientos de víctimas. Por un lado esta el detrimento que evidenció la Contraloría que dio paso a uno de los procesos fiscales más rigurosos especialmente por la lista que conforma el proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los imputados fiscales están, **el exgobernador de Antioquia y excandidato presidencial Sergio Fajardo, ** el exgobernador **Luis Alfredo Ramos**, y en calidad de gerentes de EPM  **Federico Restrepo Posada y Juan Esteban Calle Restrepo**, **Álvaro Julián Villegas**, **Sergio Betancur Palacio** y **Ana Cristina Moreno Palacios**. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los demás imputados integraban el Instituto para el Desarrollo de Antioquia (IDEA), en la lista están, **Alejandro Antonio Granda Zapata** y **Jorge Mario Pérez Gallón**, funcionarios, y los gerentes **Álvaro de Jesús Vásquez Osorio** e **Iván Mauricio Pérez Salazar**, este último además era delegado por Sergio Fajardo en la junta directiva.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También deberán responder como gerentes de Hidroituango **Luis Guillermo Gómez Atehortúa**, **John Alberto Maya Salazar,** y el gerente de EPM-Hidroituango **Luis Javier Vélez Duque.** ([Los conflictos de intereses entre la exdirectiva de EPM y el GEA](https://archivo.contagioradio.com/los-conflictos-de-intereses-entre-la-exdirectiva-de-epm-y-el-gea/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como Fajardo, también será llamado el exalcalde de Medellín que **Fabio Alonso Salazar Jaramillo,** y junto a él **María Eugenia Ramos Villa**, Secretaría de Planeación Departamental, una lista desde la administración distrital que es cerrada por el exsecretario de Infraestructura R**afael Andrés Nanclares Ospina**. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**De igual forma deberán responder tres consorcios y contratistas**, entre ellos el informe resalta, **Túneles Ituango,** el **Consorcio Ingetec**, al ser los responsables de interventor de la obra, y finalmente el Consorcio **Generación Ituango**, como encargados de diseñador y asesor el mega proyecto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que implica la imputación fiscal** de la Contraloría

<!-- /wp:heading -->

<!-- wp:paragraph -->

El proceso que le espera a los 19 funcionarios, exfuncionarios, empresas y consorcios al enfrentarán un proceso fiscal, el cual empezó desde noviembre del 2019 cuando **la Contraloría General** abrió la investigación por el detrimento que sería superior a los 4 billones de pesos, es **si después del juicio son hallados culpables y son condenados fiscalmente por la Contraloría entrarían en un boletín de responsables fiscales. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Lo que les impediría ser contratados para trabajar con el Estado,o aspirar a ejercer cargos públicos**, un acción que se aplicaría hasta que paguen el valor de la condena fiscal o la acción correspondiente imputada por el juez.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/oh-Ax64Mxwg","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/oh-Ax64Mxwg

</div>

<figcaption>
Conozca mas detalles de este proceso

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading {"level":3} -->

### Más allá de la responsabilidad presupuestal, debe existir una responsabilidad con las víctimas de Hidroituango

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Desde que se registró el colapsó  el 28 de abril del 2018** **de uno del los túneles luego del aumento del caudal del río Cauca que sobrepasó su capacidad de descarga**, generando una contingencia que pese a la alerta roja en la estabilidad de la megaobra, sumado a la omisión de las gerencias, tres años después el impacto en la vida y los hogares se cientos de Antioqueños que lo perdieron todo luego de la catástrofe aún sigue en un limbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo desde el inició de la construcción de este proyecto, el **[Movimiento Ríos Vivos](https://riosvivoscolombia.org/), ha venido denunciando desalojos forzados, amenazas, persecuciones y asesinatos de sus integrantes**, y en donde **cerca de 500 familias han sido desalojadas de sus territorios sin las garantías de reubicación** y muchas perdieron sus enseres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma que sus actividades ancestrales también se han visto en riesgo como lo es la pesca y la barequería, debido al desvío del caudal del río. Los pescadores han tenido que trasladarse o simplemente buscar una alternativa para su trabajo. ([Sequía y mortandad de peces alcanzan niveles críticos por operación de Hidroituango](https://archivo.contagioradio.com/sequia-mortandad-peces-alcanzan-niveles-criticos-operacion-hidroituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de las graves **afectaciones en la búsqueda de los cuerpos de cerca de 2 mil personas que se cree que fueron asesinadas y enterradas e**n el marco del conflicto armado en Colombia** en el bosque que fue arrasado con la ruptura del túnel**. Cuerpos que finalmente no serán encontrados pues, como lo hizo saber el Estado colombiano en 168 audiencias de la Comisión Interamericana de Derechos Humanos, no hay una certeza ni una cifra consolidada sobre el número de personas que puede haber en la zona de influencia del proyecto.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
