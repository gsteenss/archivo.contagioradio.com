Title: Mujeres en Colombia se suman para exigir aborto legal y seguro
Date: 2019-09-28 17:38
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: aborto, colombia, DDHH, libre y seguro, mujeres
Slug: mujeres-en-colombia-exigir-aborto-legar-y-seguro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Este 28 de septiembre se desarrolló el Día de Acción Global por el aborto legal y seguro, donde organizaciones defensoras de derechos humanos de mujeres  de diversas regiones del país, se reúnen bajo el lema "**Aborto Libre y Seguro",** más de 7  ciudades se unieron con el fin de tomarse diferentes escenarios públicos haciendo un llamado a las instituciones de salud para garantizar el derecho a la Interrupción Voluntaria del Embarazo (IVE) y disminuir  los  más de 53 casos de mujeres que no pueden abortar por causa de su EPS. (Le puede interesar:[Colectivos feministas marcharán en defensa del derecho al aborto](https://archivo.contagioradio.com/movilizacion-en-defensa-del-derecho-al-aborto/))

Según la Organización Mundial de la Salud el aborto es fundamental para garantizar la salud y las vida de las mujeres, a la luz de este ideal Juliana Martínez  Londoño coordinadora de la **Mesa por la Vida y la Salud de las Mujeres** mencionó, "el aborto debe ser un derecho para todas, y no solo cuando se presentan afectaciones o enfermedades en  el momento de la gestación, este debe garantizar un estado de bienestar completo físico, mental y  emocional a las mujeres que desean acceder este"

 Martínez resaltó  que estas manifestaciones tienen como finalidad educar a las mujeres sobre su derecho al aborto, exigir el cumplimiento de los derechos sobre las acciones en el cuerpo de la mujer y romper los estigmas  que existen al rededor del aborto, "una de las razones por la que nos movilizamos es eliminar las barreras que siguen enfrentando las mujeres para exigir el aborto, que es un servicio de salud al que todas debemos tener derecho sin excepción", exclamó Martínez. (Le puede interesar: [Mujeres imparables, 20 años de lucha por la despenalización del aborto en Colombia](https://archivo.contagioradio.com/mujeres-imparables-20-anos-de-lucha-por-la-despenalizacion-del-aborto-en-colombia/))

Así mismo esta movilización tuvo como objetivo ampliar el espectro ante las dificultades que tienen que pasar las mujeres que viven en zonas rurales del país para acceder a este servicio, "una cosa es hablar de aborto en la ciudad donde las mujeres a travez de redes sociales tienen acceder a  mucha información y logra conectarse con instituciones legales y seguras que prestan este servicio, y otra cosa es lo que tienen que pasar aquellas que viven en el campo; primero no existen instituciones especializadas en esto, segundo las EPS se niegan a prestan este servicio o incluso a dar información, y tercero esto es visto como pecado; son juzgadas y estigmatizadas por el simple hecho de contemplar el aborto como opción" resaltó Martínez.

> Este es el pronunciamiento que las organizaciones feministas, juveniles, de mujeres y de derechos humanos colombianas hacemos a propósito del Día de Acción Global por el Aborto Legal y Seguro [\#28Sept](https://twitter.com/hashtag/28Sept?src=hash&ref_src=twsrc%5Etfw)  
> ¡Ser madre es una opción, no una obligación! [\#AbortoLibreYSeguro](https://twitter.com/hashtag/AbortoLibreYSeguro?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/NfFGx8Rxr0](https://t.co/NfFGx8Rxr0)
>
> — MESA POR LA VIDA (@mesaporlavida) [September 27, 2019](https://twitter.com/mesaporlavida/status/1177670453382524928?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Aborto libre y seguro: "¡Ser madre es una opción, no una obligación!"

Adicional la feminista y coordinadora de la Mesa por la Vida y la Salud de las Mujeres, señaló que uno de los  principales obstáculos que la sociedad colombiana tiene es el desconocimiento, debido tabú en el que aún se encuentra este derecho, "es necesario educar a travez de campañas públicas, redes sociales, medios de comunicación, colegios y universidades sobre los lugares, métodos y políticas que respaldan el acceso al aborto", apuntó Martínez. (Le puede interesar:[Las nuevas visiones de la sociedad colombiana frente al aborto](https://archivo.contagioradio.com/encuesta_aborto_colombia/))

Este encuentro por el "aborto libre y seguro", se desarrolló en ciudades como Bogotá, Medellín, Ibagué, Cartagena, Santa Martha y Cali, donde se llevaron a cabo actividades como la toma de las ciudades desde los monumentos emblemáticos, como La Pola, La Rebeca, Simón Bolívar, las gordas de Botero, entre otros; desde la  organización  Mesa por la Vida , en cabeza de múltiples organizaciones que luchan por los derechos de las mujeres,  este día tuvo como objetivo visibilizar  este tema para que las mujeres puedan decidir si quieren o no tener hijos y para que ninguna mujer muera o vaya presa por ejercer su derecho.

<iframe id="audio_42417738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42417738_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
