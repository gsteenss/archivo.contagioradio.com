Title: Bolsonaro quiere cambiar a Paulo Freire como "patrono de la educación" en Brasil
Date: 2019-05-02 16:32
Author: CtgAdm
Category: El mundo
Tags: educacion, jair bolsonaro, paulo freire
Slug: bolsonaro-quiere-cambiar-a-paulo-freire-como-patrono-de-la-educacion-en-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Paulo-Freire-evento-Cienfuegos-Cuba-696x460.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Diario de la educación 

En una entrevista concedida durante su visita al municipio Riberao Preto en Sao Paulo, el presidente **Jair Bolsonaro, defendió la idea de cambiar la denominación de "patrono de la educación" de Brasil a Paulo Freire,** conferida en el año 2012 mediante un proyecto de ley.

Sin citar verbalmente el nombre de Freire, Bolsonaro aseguró que el patrón actual deberá ser cambiado “**Quien incluso sabe que tenemos un patrón de educación, pero es un patrón horrible. No necesitamos decir quién es, pero será cambiado. Estamos esperando a alguien diferente "** afirmó.

El título fue conferido a Freire en 2012, luego de ser aprobado por un proyecto de ley, razón por la cuál **cualquier cambio que pretenda realizarse debe ser aprobado por el Congreso**. El comentario de Bolsonaro se dio en la víspera del 22 aniversario de la muerte del hito de la educación popular.

Desde su campaña el año pasado, el cuestionado mandatario brasilero se ha manifestado en **contra de la de la influencia de Paulo Freire en los métodos educativos en las escuelas públicas**, y en los últimos días **desestimó la importancia de la formación en filosofía y sociología en el país**. A fines de 2017, la Comisión de Derechos Humanos del Senadoya había rechazado una solicitud, presentada por acción popular, para revocar el título emérito que ostenta el educador.

###### Reciba toda la información de Contagio Radio en [[su correo]
