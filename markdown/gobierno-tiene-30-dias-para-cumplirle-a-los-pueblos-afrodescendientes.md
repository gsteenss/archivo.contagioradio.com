Title: Gobierno tiene 30 días para cumplirle a los pueblos afrodescendientes
Date: 2017-05-19 13:50
Category: DDHH, Nacional
Tags: acuerdos, afrodescendientes, buenaventura, Chocó, CONPA, mujeres, paro
Slug: gobierno-tiene-30-dias-para-cumplirle-a-los-pueblos-afrodescendientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/20170519_133756_resized-e1495219785677.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [19 mayo 2017] 

En rueda de prensa, el Consejo Nacional de Paz Afrocolombiano, CONPA, le dió un tiempo de **30 días al gobierno para que cumpla los acuerdos pactados con la comunidad.** De no ser así, los pueblos afrodescendientes acudirán a las instancias internacionales para denunciar la crisis humanitaria y la falta de inclusión de la comunidad en los acuerdos de paz.

En el comunicado que hizo público el CONPA, establecen que “en los territorios continuamos **encontrando cadáveres mutilados y torturados, continuamos viviendo combates y hostigamientos armados,** recibiendo amenazas, detenciones arbitrarias, sufriendo atentados contra liderezas y líderes y reclutamientos urbanos.” Le puede interesar: ["Pescadores de buenaventura que están en paro cívico son amenazados por paramilitares"](https://archivo.contagioradio.com/pescadores-de-buenaventura-son-amenazados-por-paramilitares/)

De igual forma, entre las propuestas que le hicieron al presidente Juan Manuel Santos se encuentra “Trabajar en una Norma Marco que recoja todos los asuntos y temas necesarios para la implementación de los 29 acuerdos contenidos en el Capítulo Étnico.” Además, le han pedido que les garantice **“la conformación de un fondo específico para la implementación de los acuerdos en los territorios y comunidades Negras”.**

<iframe src="http://co.ivoox.com/es/player_ek_18792367_2_1.html?data=kp2km5eXepihhpywj5WXaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpC2s87Vz9SYr9TWqc_jhpewjavTttCfqtPhx9ePidXiysjcjbjTsMrYwtfWxsbIb8XZzZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Abid Manuel Romano Moreno, integrante del Foro Inter Etnico Solidaridad del Chocó, “el gobierno fue hace dos días al chocó y no llegaron a ningún acuerdo”. Para Romano, los delegados del gobierno no tienen capacidad de decisión sobre los asuntos de la comunidad y por eso le han pedido al gobierno que **organice una comisión que pueda tomar decisiones** mientras continua el paro en Chocó y en Buenaventura. Le puede interesar: ["Inicia el paro cívico cultural de Buenaventura"](https://archivo.contagioradio.com/inicia-el-paro-civico-cultural-de-buenaventura/)

**EXIGENCIAS DE LAS MUJERES AFRO**

Las mujeres afrodescendientes han manifestado en varias ocasiones que **no han sido tenidas en cuenta para la toma de decisiones** tanto en los acuerdos de paz, como en la crisis que se vive en los departamentos de mayoría afro. Para ellas, “las mujeres llevan en su cuerpo las secuelas de la guerra y el olvido”.

<iframe src="http://co.ivoox.com/es/player_ek_18792379_2_1.html?data=kp2km5eXe5qhhpywj5WXaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncbPj2cbbw5CxqcuZpJiSo6nFaZO3jKbg0cjNpcTdhqigh6eXsozYxtGYpdTSt8be0JCw0dLZssrowtfW0ZDIqc2fjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Roxana Mejía, miembro de la Asociación del Consejo Comunitario del Norte del Cauca, “las mujeres hemos alzado las voces y nos hemos movilizado para **denunciar ante los entes internacionales que se nos están violando los derechos humanos”.** Le puede interesar: ["Buenaventura irá a paro y exigirá vivir con dignidad y paz en sus territorios"](https://archivo.contagioradio.com/40089/)

Cabe resaltar que en la rueda de prensa las comunidades afrodescendientes denunciaron la **violación de más de 1000 mujeres afro en el marco del conflicto armado.** Según Mejía, “No hay avance en las investigaciones para prevenir las situaciones de agresión a las mujeres”. Además, Mejía denunció que “han recibido amenazas de las Autodefensas Gaitanistas quienes han dicho que nuestras hijas e hijos serán violadas y asesinadas si son encontrados fuera de las casas después de las seis de la tarde”.

[ULTIMÁTUM A GOBIERNO NACIONAL POR CRISIS HUMANITARIA Y FALTA DE INCLUSIÓN DE PUEBLOS AFRODESCENDIENTES EN...](https://www.scribd.com/document/348865538/ULTIMA-TUM-A-GOBIERNO-NACIONAL-POR-CRISIS-HUMANITARIA-Y-FALTA-DE-INCLUSION-DE-PUEBLOS-AFRODESCENDIENTES-EN-ACUERDOS-DE-PAZ#from_embed "View ULTIMÁTUM A GOBIERNO NACIONAL POR CRISIS HUMANITARIA Y FALTA DE INCLUSIÓN DE PUEBLOS AFRODESCENDIENTES EN ACUERDOS DE PAZ on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_75322" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/348865538/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-kVgCybMfBUAQXttvgxbl&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
