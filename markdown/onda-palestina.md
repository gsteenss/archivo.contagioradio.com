Title: Onda Palestina
Date: 2017-05-31 08:32
Author: AdminContagio
Slug: onda-palestina
Status: published

### ONDA PALESTINA

[![Fuertes protestas en Palestina contra decisión de Trump sobre Jerusalén](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/trump-palestina-e1513015551439.jpg "Fuertes protestas en Palestina contra decisión de Trump sobre Jerusalén"){width="750" height="500" sizes="(max-width: 750px) 100vw, 750px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/trump-palestina-e1513015551439.jpg 750w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/trump-palestina-e1513015551439-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/trump-palestina-e1513015551439-370x247.jpg 370w"}](https://archivo.contagioradio.com/palestina-trump-jerusalen/)  

###### [Fuertes protestas en Palestina contra decisión de Trump sobre Jerusalén](https://archivo.contagioradio.com/palestina-trump-jerusalen/)

[<time datetime="2017-12-11T13:15:43+00:00" title="2017-12-11T13:15:43+00:00">diciembre 11, 2017</time>](https://archivo.contagioradio.com/2017/12/11/)El reconocimiento de Jerusalén como capital de Israel por parte de Donald Trump representaría un retroceso en la política de EEUU frente a la ocupación a Palestina[LEER MÁS](https://archivo.contagioradio.com/palestina-trump-jerusalen/)  
[![Palestinos son encarcelados por dar likes en facebook](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds1-1170x540.jpg "Palestinos son encarcelados por dar likes en facebook"){width="1170" height="540"}](https://archivo.contagioradio.com/palestinos-encarcelados-facebook/)  

###### [Palestinos son encarcelados por dar likes en facebook](https://archivo.contagioradio.com/palestinos-encarcelados-facebook/)

[<time datetime="2017-11-30T15:28:31+00:00" title="2017-11-30T15:28:31+00:00">noviembre 30, 2017</time>](https://archivo.contagioradio.com/2017/11/30/)Fuerzas de ocupación israelíes han arrestado a alrededor de 280 personas, por haber dado “likes” en contenidos “pro-palestinos” en la red social Facebook[LEER MÁS](https://archivo.contagioradio.com/palestinos-encarcelados-facebook/)  
[![Mexico y Palestina dijeron No a los muros “World without Walls”](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds-1170x540.png "Mexico y Palestina dijeron No a los muros “World without Walls”"){width="1170" height="540"}](https://archivo.contagioradio.com/mexico-y-palestina-muros/)  

###### [Mexico y Palestina dijeron No a los muros “World without Walls”](https://archivo.contagioradio.com/mexico-y-palestina-muros/)

[<time datetime="2017-11-22T13:46:35+00:00" title="2017-11-22T13:46:35+00:00">noviembre 22, 2017</time>](https://archivo.contagioradio.com/2017/11/22/)El pasado 9 de noviembre organizaciones de México y Palestina celebraron lo que denominan el Día Sin Muros, una acción que lideraron a nivel global.[LEER MÁS](https://archivo.contagioradio.com/mexico-y-palestina-muros/)  
[![La dureza de la cárcel para las mujeres palestinas.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-ta.jpg "La dureza de la cárcel para las mujeres palestinas."){width="617" height="413" sizes="(max-width: 617px) 100vw, 617px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-ta.jpg 617w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-ta-300x201.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-ta-370x248.jpg 370w"}](https://archivo.contagioradio.com/mujeres-palestinas-carcel/)  

###### [La dureza de la cárcel para las mujeres palestinas.](https://archivo.contagioradio.com/mujeres-palestinas-carcel/)

[<time datetime="2017-11-17T12:18:10+00:00" title="2017-11-17T12:18:10+00:00">noviembre 17, 2017</time>](https://archivo.contagioradio.com/2017/11/17/)Mujeres palestinas denuncian ser sometidas a situaciones de aislamiento total, privando toda posibilidad de ser visitadas por sus seres queridos en prisión[LEER MÁS](https://archivo.contagioradio.com/mujeres-palestinas-carcel/)  
[![La tierra, el cuerpo y las mujeres palestinas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-800x540.jpg "La tierra, el cuerpo y las mujeres palestinas"){width="800" height="540"}](https://archivo.contagioradio.com/mujeres-palestinas-cuerpos/)  

###### [La tierra, el cuerpo y las mujeres palestinas](https://archivo.contagioradio.com/mujeres-palestinas-cuerpos/)

[<time datetime="2017-11-10T15:11:07+00:00" title="2017-11-10T15:11:07+00:00">noviembre 10, 2017</time>](https://archivo.contagioradio.com/2017/11/10/)Las mujeres de Palestina mantienen formas de resistencia cuyo origen está fuertemente arraigado a la tierra y a su corporalidad[LEER MÁS](https://archivo.contagioradio.com/mujeres-palestinas-cuerpos/)  
[![16 millones de campesinos boicotean a Israel en solidaridad con Palestina](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds.jpg "16 millones de campesinos boicotean a Israel en solidaridad con Palestina"){width="708" height="399" sizes="(max-width: 708px) 100vw, 708px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds.jpg 708w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds-300x169.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds-370x209.jpg 370w"}](https://archivo.contagioradio.com/campesinos-india-bds/)  

###### [16 millones de campesinos boicotean a Israel en solidaridad con Palestina](https://archivo.contagioradio.com/campesinos-india-bds/)

[<time datetime="2017-11-02T17:35:47+00:00" title="2017-11-02T17:35:47+00:00">noviembre 2, 2017</time>](https://archivo.contagioradio.com/2017/11/02/)La organización más grande de campesinos y trabajadores agrícolas de la India, All India Kisan Sabha (AIKS), anunció que se adhería al movimiento BDS[LEER MÁS](https://archivo.contagioradio.com/campesinos-india-bds/)
