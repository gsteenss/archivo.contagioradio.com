Title: Las amenazas a la vida silvestre en Colombia
Date: 2017-03-03 12:47
Category: Ambiente, Voces de la Tierra
Tags: biodiversidad, colombia, Día Mundial de la Vida Silvestre
Slug: las-amenazas-la-vida-silvestre-colombia
Status: published

###### Foto: Diario Palmira 

###### [3 Mar 2017] 

[Bajo el lema “Escucha las voces jóvenes”, se celebra el Día Mundial de la Vida Silvestre. Una fecha que sirve para recordar la enorme responsabilidad que tiene Colombia con el mundo siendo el segundo país más biodiverso del mundo. A la fecha, **hay 56.343 especies registradas en el país, según el Sistema de Información sobre Biodiversidad en Colombia**.]

[Pese a esa responsabilidad, un total de 798 especies de plantas se encuentran amenazadas, 313 animales vertebrados y 74 invertebrados, de los cuales 264, 50 y 6 respectivamente, se encuentran en peligro crítico. La construcción de megaproyectos y la minería son unas de las principales causantes de la extinción de la fauna y flora silvestre, además de la comercialización y prácticas como la caza indiscriminada de animales.]

En el país los animales más amenazados son el armadillo, la guacamaya bandera, el manatí del Caribe, el oso perezoso, la rana dorada, el mono tití del Caquetá, el mono tití cabeza blanca, la rana cornuda del amazonas, el oso hormiguero, el oso de anteojos, entre otros, para un total son **359 especies. Colombia es uno de los ocho países responsables de la mitad del deterioro del planeta,** según un informe de la Unión Internacional para la Conservación de la Naturaleza, Uicn y la ONU.

[“Todos los rincones de Colombia son vulnerables y están expuestos al cambio, pero tenemos algunos mucho más vulnerables que son muchas veces las regiones más pobres, las que tiene menos capacidad de respuesta ante los fenómenos extremos.]**El Chocó, la Amazonía, algunas partes de los Andes están más expuestas que otras a estos eventos extremos** y tiene menos capacidad de respuesta para atender la emergencias y prepararse a la ocurrencia de estos fenómenos”,[ lugares en los que se encuentran la gran mayoría de especies, muchas de las cuales solo se encuentran en Colombia, como lo explica Susana Vélez, Especialista en Política Forestal y Cambio Climático.]

El modelo extractivista, la gandería, la tala de árboles, el tráfico de fauna silvestre son las principales causas de la pérdida de biodiversidad, sumado a la falta de eficacia de políticas del gobierno para que se garantice la vida de los ecosistemas.

### A nivel mundial 

[De acuerdo con la organización World Wildlife Fund for Nature, WWF por sus siglas en inglés,]**una tercera parte de las especies animales del planeta podrían extinguirse**[ sino se actúa desde ya para frenar el cambio climático]**impidiendo que la temperatura aumente entre 1,5 y 2 grados.**

[Uno de los impactos más importantes tiene que ver con el deshielo del Ártico, lo que genera, por ejemplo, que las crías de las focas estén en riesgo de morir debido a que el hielo se rompe y mueren ahogados, también los osos polares tendrán cada vez más complicaciones para conseguir su alimento.]

[El informe, “]**Los años más cálidos de la historia. ¿Por qué se calienta nuestro planeta?”, realizado en Madrid**[, en el que participó la WWF, evidencia que ha habido un descenso de las nevadas en los últimos años, además asegura que en los próximos años va a ser cada vez más difícil predecir la nieve debido al continuo cambio de comportamiento de los frentes atmosféricos.]

[E]**ntre 1970 y 2012, se produjo una disminución del 58% en las poblaciones de vertebrados, peces, aves, mamíferos, reptiles y anfibios, en todo el mundo.** [Además advierte que si la tendencia actual sigue su curso, "se calcula que en cuatro años, viviremos la desaparición de dos tercios de la vida silvestre en el mundo".]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
