Title: La deuda de la Alemania nazi al pueblo griego es mayor a la que el actual gobierno debe
Date: 2015-02-16 20:02
Author: CtgAdm
Category: Economía, El mundo
Tags: Alemania nazi, Deuda Alemania con Grecia, deuda Grecia con Alemania, ocupación nazi Grecia, Syriza
Slug: la-deuda-de-la-alemania-nazi-al-pueblo-griego-es-mayor-a-la-que-el-actual-gobierno-debe
Status: published

###### Fotos:Exordio.com 

Según el nuevo gobierno de Syriza en Grecia, Alemania le adeuda a ese país, el **doble de lo que actualmente debe Grecia a Alemania por los rescates económicos de los 3 últimos años, patrocinados por la Troika.**

Así, tras 70 años de finalizada la segunda guerra mundial, el debate sobre **la reparación de los costes de la ocupación nazi en Grecia** se vuelve a abrir destapando la caja de pandora de una reivindicación histórica del pueblo griego que siempre ha negado Alemania.

Después de finalizada la contienda, se decidió entre todos los estados afectados, la cuantía a pagar por el país germano a todos los que habían sufrido las consecuencias de la ocupación. Pero Grecia nunca estuvo de acuerdo, hasta que ratificó los acuerdos de 1990, para la unificación de las dos Alemanias, y se aceptó el pago acordado por la reparación.

Con el paso del tiempo y el auge del nacionalismo griego, dicha reivindicación se ha ido extendiendo por todas las fuerzas políticas hasta el punto de que los distintos gobiernos lo han demandado pero no ha sido hasta el gobierno de Syriza cuando la demanda ha tomado fuerza.

La problemática no solo proviene de la ocupación y el posterior saqueo, siendo esta quizás la más cruel y con mayor afectación de toda la Europa ocupada, **sino de que Hitler obligó a Grecia a contraer un crédito, con el que luego financió la ocupación**.

Por otro lado fueron los países del sur quienes más apoyaron la condonación del 63% de la deuda alemana para que esta pudiera crecer económicamente.

**Manolis Glezos de 92 años y actual parlamentario de Syriza fue quién quitó la bandera nazi de la Acrópolis cuando finalizó la ocupación**.Ahora él como tantos otros reclaman una justicia histórica y quién describe **el nuevo gobierno como el gobierno de los marginados históricamente, en referencia a las víctimas de la Alemania nazi**.
