Title: Organizaciones de DDHH emiten alerta por militarización del Alto Naya #YoReporto
Date: 2015-03-24 00:06
Author: CtgAdm
Category: DDHH, Nacional, yoreporto
Tags: Alerta temprana, Alto Naya, Militarización, Paramilitarismo
Slug: organizaciones-de-ddhh-emiten-alerta-por-militarizacion-del-alto-naya-yoreporto
Status: published

###### Foto: usahispanicpress.com 

En horas de la tarde del sábado 21 de marzo de 2015, varios helicópteros del ejército nacional  efectuaron un desembarco de tropas conjuntas de Ejército y Policía antinarcóticos en la vereda La Mina jurisdicción del Municipio de Buenaventura del Valle del Cauca, y  el día de hoy 22 de marzo en horas de la mañana arribaron cuatro helicópteros más a la vereda El Placer jurisdicción del municipio de Buenos Aires Cauca, completando un numero de trescientos miembros de la fuerza pública fuertemente armados en el sector

Los comuneros indígenas y la población afrodescendiente que habita el territorio del Alto Naya se encuentran muy preocupados de la inusual militarización de sus territorios ancestrales ya que se pueden provocar enfrentamientos armados con la insurgencia.

O como temen los líderes del sector, podría tratarse de un operativo para asegurar la zona para la entrada de los paramilitares, como ocurrió en el pasado cuando la fuerza pública preparo el terreno para que las AUC cometieran la execrable masacre del Naya en el 2001., aun en la impunidad.

Llama la atención que si se tratara de un operativo legal para erradicar cultivos de uso ilícito por vía terrestre o aérea, las autoridades debieron haber concertado la consulta respectiva con las  comunidades negras e indígenas que habitan la zona, a fin de adoptar medidas para proteger a la comunidad y garantizar la vigencia de sus derechos a la vida e integridad personal.

Hacemos un llamado a la fuerza pública para que respeten los derechos de los habitantes ancestrales del territorio del Alto Naya, así mismo para que no pongan en riesgo ni atenten contra su vida  e integridad personal.

Hacemos un llamado a la Defensoría del Pueblo y a la Oficina del Alto Comisionado de Naciones Unidas para que realicen una misión de verificación inmediata y brinden el acompañamiento necesario en la zona para evitar daños irreparables en las Comunidades

Al movimiento social colombiano, a las ONG de derechos humanos y a las personas solidarias la divulgación de la información y el seguimiento a lo que pueda suceder.

Buenos Aires- Cauca, 22 de marzo de 2015

Por Corporación Justicia y Dignidad para \#YoReporto
