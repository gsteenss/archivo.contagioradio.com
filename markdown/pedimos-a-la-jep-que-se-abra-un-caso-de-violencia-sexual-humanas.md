Title: "Pedimos a la JEP que se abra un caso de violencia sexual": Humanas
Date: 2020-07-02 14:10
Author: CtgAdm
Category: Actualidad, Mujer
Tags: Violencia Sexual en colombia
Slug: pedimos-a-la-jep-que-se-abra-un-caso-de-violencia-sexual-humanas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Foto1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: @HumanasColombia*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 2 de julio **21 mujeres sobrevivientes de violencia sexual de Norte de Santander**, en conjunto con la **[Corporación Humanas Colombia](https://archivo.contagioradio.com/mujeres-detenidas-en-carcel-picalena-exigen-protocolos-contra-covid-19/)**  le entregaron a la Jurisdicción Especial para la Paz (JEP), su segundo informe, de **Justicia para Todas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta representa la segunda entrega de información recopilada en torno a la violencia sexual de mujeres en los territorios, en el marco del conflicto armado Colombiano ante la JEP , y con la participación de las mujeres que decidieron [romper el silencio](http://Mujeres%20víctimas%20de%20violencia%20sexual%20en%20Montes%20de%20María%20rompen%20el%20silencio%20ante%20la%20JEP) en la búsqueda de justicia, reparación y no repetición.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Teniendo un dolor que solo como mujer he vivido, guardo la esperanza de que se haga justicia (...) quise no ser más una voz callada ni estar en lo oculto"***
>
> <cite>Testimonios de las mujeres víctimas de violencia sexual que entregan sus casos a la Jurisdicción Especial para la Paz.</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/ea40jHiRJM8","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/ea40jHiRJM8

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading {"level":3} -->

### Informe de Humanas recoge el horror de violencia sexual contra las mujeres de N. Santander

<!-- /wp:heading -->

<!-- wp:paragraph -->

El [informe](ttps://humanas.org.co/alfa/10_471_21-mujeres-sobrevivientes-de-violencia-sexual-de-Norte-de-Santander-piden-%EF%BF%BDjusticia-para-todas%EF%BF%BD-ante-la-JEP.html) recopila **23 casos de violencia sexual contra 21 mujeres,** entre los años de **1991 y 2016, registrados en 10 municipios del departamento de Norte de Santander**: 6 casos en Ocaña , Convención, Tibú, Sardinata, El Tarra, la Playa de Belén; y 4 en Cúcuta, Lourdes, Cucutilla y El Zulia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También señala que los rangos de edad que tenían las mujeres en el momento de los hechos fue de 10 a 43 años, lo que indica que "**una tercera parte de las víctimas (7 de 21) fueron agredidas siendo menores de edad (5-17 años)**", señaló Humanas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Relatan también que **17 casos se pudieron vincular con integrantes de la Fuerza Pública y 10 por las FARC**, denuncias que no se encuentran en el informe, por lo tanto la Corporación solicitó a la JEP activar las investigaciones correspondientes que permitan registrar estos casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destacando que de estos 23 casos, solamente dos habían sido denunciados ante la Fiscalía, Humanas resaltó que **varias de las mujeres sobrevivientes hablaron por primera vez de lo ocurrido en este informe**, por tanto *"para las víctimas todas sus esperanzas en justicia están puestas en la JEP".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento también cuenta el proceso de acompañamiento no solo legal sino físico y psicológico, de estas 23 mujeres que además de sufrir violencia sexual atravesaron por otras violencias como el *"**desplazamiento forzado, amenazas, intentos de reclutamiento, prostitución forzada y utilización de menores de edad en el conflicto armado**".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El romper el silencio las hace ser ejemplo para otras mujeres en Colombia y el mundo", ONU Mujeres

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la entrega virtual de este informe también hizo presencia el **embajador de Alemania Peter Ptassek** quién agradeció la valentía de las mujeres que hicieron parte de este informe y reiteró su compromiso con la paz de Colombia, ***"sabemos que el Acuerdo de Paz es un texto y se queda en letras muertas sin la iniciativa de las comunidades y aún más sin justicia".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte **Heidi Abuchaibe ,Procuradora General Segunda**, señaló que *"**en el caso 007 solamente se resaltan dos casos de violencia sexual en este departamento, por lo tanto esta entrega es indispensable** para romper con el silencio y la invisibilización que ha tenido esta violencia en el marco de conflicto armado"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Ana Güezmes, vocera de ONU Mujeres Colombia**, afirmó que estos 23 casos son apenas *"una porción de la realidad horrorosa de la violencia de género que tenemos que nombrar y poner en la mesa de diálogo"*, y enfatizó en que la ***"justicia no sólo es reconocer sus crímenes, sino llegar a los territorios con mecanismos para que esto no se repita".***

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1278688642979020802","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1278688642979020802

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### 40% de los informes que recibe la JEP incluyen casos de violencia sexual

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente **Adriana Benjumea, directora de la Corporación Humanas** resaltó que los casos allí recopilados y otros miles que se dan a lo largo y ancho del país "***hacen necesaria la reconstrucción de patrones de crímenes sexuales cometidos por miembros de la Fuerza Pública**; no solo en Norte de Santander sino en todo el país". *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este informe se suma a otros 38 que ha recibido la JEP entorno a este tipo de violencia, haciendo evidente aún mas la necesidad de **"abrir un caso nacional de violencia sexual, violencia reproductiva y otros crímenes vinculados a la sexualidad para que la justicia no siga llegando tarde para las mujeres**", afirmó Benjumea.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto **Catalina Díaz, presidenta de la Sala de Reconocimiento de la JEP**, señaló que el 40% de los informes que recibe la Jurisdicción incluyen casos de violencia sexual, por parte de grupos armados legales e ilegales, *"**yo sé que es poco lo que hemos hecho, y son muchas las exigencias de Justicia que hay; tenemos un desafío enorme para hacer justicia** sobre todo esto y por eso hemos emprendido ya los pasos para la segunda ronda de priorizaciones de casos".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
