Title: Para aprobar el PND ha funcionado la “aplanadora de la Unidad Nacional”
Date: 2015-05-05 17:36
Author: CtgAdm
Category: Economía, Entrevistas
Tags: Conversaciones de paz de la habana, Iván Cepeda, OCDE, Plan Nacional de Desarrollo, Unidad Nacional
Slug: para-aprobar-el-pnd-ha-funcionado-la-aplanadora-de-la-unidad-nacional
Status: published

###### Foto: archivo 

<iframe src="http://www.ivoox.com/player_ek_4452045_2_1.html?data=lZmilJWYeY6ZmKiak5eJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLmwpDO0tfTpsLmjMrZjbWyiIzcwpDT19PHrdDiwsncjdHFb4a5k4qlkoqdh8Lkzcbbw8nTtsKfxcqYzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Iván Cepeda, Senador del Polo Democrático] 

Senadores del Polo Democrático y representantes a la Cámara han tratado de enfrentar la “**aplanadora de la Unidad Nacional**” con proposiciones en más de 60 artículos, sin embargo, a la hora de la votación siempre funcionan las mayorías, señala el Senador **Iván Cepeda**. De todos los artículos se han escogido “unos 60 o 70 que son los más lesivos” explica.

Según Cepeda, la estrategia consiste en señalar cuales son los puntos más lesivos, hacer las proposiciones de los cambios y preparar una **demanda de inconstitucionalidad,** cumpliendo los trámites que indica la norma, pero “*la única salida realmente es la movilización de la gente y la construcción de un gobierno distinto, es la única manera de que podamos hacer un cambio de modelo*”

El pasado Lunes se aprobaron **142 de los 262 artículos del PND** que deberá estar aprobado en su totalidad para el próximo 8 de Mayo, hasta el momento faltan por aprobarse rubros como el presupuesto militar, las normas para permitir la entrada de la gran minería y para cumplir las condiciones que impone la **OCDE**.

En el tema de la Paz, lo que realmente preocupa, según el Senador Cepeda, es que a diferencia de lo que se anuncia retóricamente, el PND no tiene ninguna política pública seria, además hay muchas disposiciones en el **plan que riñen directamente con lo que hasta el momento se ha acordado en el proceso de conversaciones de paz de la Habana.**
