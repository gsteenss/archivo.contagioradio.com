Title: "Son fundamentales las garantías de no repetición": Aida Avella
Date: 2016-09-16 14:21
Category: DDHH
Tags: Aida Avella, Familiares de víctimas genocidio UP, Genocidio UP, Unión Patriótica, UP
Slug: son-fundamentales-las-garantias-de-no-repeticion-aida-avella
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Genocidio-UP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Espectador ] 

###### [16 Sept 2016 ]

Este jueves en el Palacio de Nariño se realizó el acto de reconocimiento de responsabilidad estatal en el exterminio de la Unión Patriótica, en el que el presidente Juan Manuel Santos aseguró que en este momento histórico por el que atraviesa Colombia, es necesario que el Estado reconozca que "**no tomó medidas suficientes para impedir y prevenir los asesinatos**, los atentados y las demás violaciones, a pesar de la evidencia palmaria de que esa persecución estaba en marcha".

"Me comprometo solemnemente hoy ante ustedes a tomar todas las medidas necesarias y a **dar todas las garantías para que nunca más en Colombia una organización política vuelva a enfrentar lo que sufrió la UP**", afirmó el mandatario, e insistió en que es responsabilidad del Estado brindar las condiciones de seguridad y libertad para quienes participen en política, incluidos aquellos que conformen el nuevo movimiento que surja de las FARC-EP.

Por su parte Aida Avella, presidenta nacional de la Unión Patriótica, asevera que pese a la importancia del acto, debe quedar claro que el Gobierno colombiano no pidió perdón, sino que **reconoció la participación de agentes estatales en el genocidio**, más de 6 mil crímenes que hubieran podido haberse impedido si en su momento el Estado hubiera atendido las denuncias.

Para la directiva, éste es el momento de profundizar el paso democrático que se está dando en el país, reparando políticamente a las víctimas de la Unión Patriótica, con garantías de no repetición, verdad y justicia, que incluyan el [[esclarecimiento de los hechos que rodearon el exterminio](https://archivo.contagioradio.com/estado-colombiano-permitio-propicio-y-tolero-genocidio-de-la-up/)], el que se acabe con la estigmatización y persecución por pensar diferente y el **reconocimiento de los 20 años de litigio en la Comisión Interamericana de Derechos Humanos**.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
