Title: Tras dos meses de huelga de hambre, preso palestino entra en coma en cárcel de Israel
Date: 2015-08-14 12:00
Category: El mundo, Política
Tags: Huelga de hambre, Israel, Mohamed Allan, Palestina, Presos Políticos Palestinos
Slug: tras-dos-meses-de-huelga-de-hambre-preso-palestino-entra-en-coma-en-carcel-de-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Mohammad_allan_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: itongadol.com 

###### [14 Ago 2015] 

El preso palestino Mohammed Allan, entró en estado de coma, luego de casi 2 meses de huelga de hambre. Allan, detenido por la policía de Israel desde noviembre de 2014, **sin cargos en su contra y sin que se haya iniciado un juicio**, entró en huelga desde el pasado 18 de junio exigiendo respeto por sus derechos y en solidaridad con los cerca de **5000 palestinos detenidos en cárceles de Israel en similares condiciones.**

El abogado Jamil Al Khatib, defensor de Mohamed Allan, denunció este viernes que su cliente se encuentra en estado de coma y **está conectado a un respirador artificial**. Al Khatib y voceros de la comunidad Palestina mostraron su **preocupación por la posibilidad de que se alimente al detenido por la fuerza**, cuestión que ha generado rechazo también entre los propios médicos israelíes.

Allan fue capturado por ser un supuesto activista de la Jihad Islámica y su caso ha tomado especial relevancia por la **solidaridad a nivel local e internacional que han suscitado sus demandas.**
