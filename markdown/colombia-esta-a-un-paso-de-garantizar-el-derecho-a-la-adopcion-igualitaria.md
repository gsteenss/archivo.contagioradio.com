Title: Colombia está a un paso de garantizar el derecho a la adopción igualitaria
Date: 2015-02-18 17:05
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Corte Constitucional, Homosexualidad, LGBTI, Universidad de La Sabana, Vivian Morales
Slug: colombia-esta-a-un-paso-de-garantizar-el-derecho-a-la-adopcion-igualitaria
Status: published

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4101357_2_1.html?data=lZadk5iZe46ZmKiakpyJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dm1cqYpdTSt9Xd1drQy9TSpc2fxtjhh6iXaaKljMaY0tTHpdSfydTfw9iPqMafxtLW1s7Wb8fVzdGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Germán Rincón, abogado] 

En las próximas horas, la Corte Constitucional deberá emitir su decisión sobre la posibilidad o no de que de las parejas homosexuales puedan adoptar. El conjuez José Roberto Herrera, es quien empieza la discusión en ceros debido a que la votación pasada terminó en empate.

En medio de diversas posiciones sobre este tema, Germán Rincón, abogado y defensor de derechos humanos de la comunidad LGBTI, afirma que **“esperamos que la Corte Constitucional continúe con la misma línea progresistas** con la que concedió a Ana y Verónica, la adopción conjunta de la hija biológica de una de ellas”.

La senadora liberal, Vivian Morales, alistó un referendo por el que se pretende que únicamente las parejas heterosexuales sean quienes puedan adoptar. La iniciativa ha generado todo tipo de comentarios, debido a que no sólo se le estaría quitando la posibilidad de adoptar a las parejas del mismo sexo, sino **además a hombres y mujeres solteros que tengas las capacidades para brindarle un hogar** a una niña o un niño.

El defensor de los derechos de la comunidad LGBTI, asegura que “detrás de esta iniciativa están los grupos conservadores fundamentalistas a los cuales hace parte la senadora Vivian Morales”, y agrega que, “**la justicia de diferentes países ha dicho que los derechos humanos no pueden ser objeto de consulta ciudadana** y ha tumbado estos esperpentos jurídicos, razón por la cual debería pasar lo mismo en Colombia”.

Por otro lado, respecto al documento del doctor Pablo Arango, profesor asociado del Departamento de Bioética de la Facultad de Medicina de esta universidad de la Sabana, donde se afirmaba que las personas homosexuales son enfermas y que los niños y niñas supuestamente, tienen graves efectos sociales y  psicológicos; el abogado Rincón, aseguro que este pronunciamiento fue “**una vergüenza pública, desde el punto de vista científico y académico (…) en buena hora la universidad pidió a la Corte no tener en cuenta el texto**”.

A propósito del documento emitido por el profesor Arango, la Asociación Colombiana de Psiquiatría, emitió un comunicado donde afirma que, por un lado, la homosexualidad no es una enfermedad mental, y por otro que “el desarrollo psicológico y social de los niños en custodia, adopción,  visita o cuidado subrogado por padres homosexuales o parejas del mismo sexo no muestran diferencias en comparación con los de padres heterosexuales”.

Así mismo, la asociación subrayó que “el bienestar físico, psicológico y emocional de los niños está determinado por múltiples variables interactuantes como el afecto, la calidad de la interacción, la capacidad de ofrecer seguridad y confianza, la dedicación y el compromiso de la familia, independiente de la orientación sexual de los padres”.

Cabe recordar, que el alto tribunal debe resolver la demanda contra el artículo 68 del Código de Infancia y Adolescencia que se refiere a la adopción, y se basa en la ponencia del magistrado Jorge Iván Palacio, que propone la posibilidad de que las parejas del mismo sexo adopten niños y niñas, **teniendo en cuenta los mismos requisitos que se les exigen a las parejas heterosexuales.**
