Title: La gente apoya el cine cubano porque se identifica con su realidad
Date: 2016-01-31 10:26
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine cubano, Fresa y Chocolate, hay festival, Jorge Perugorria
Slug: la-gente-apoya-el-cine-cubano-por-que-se-siente-identificacion-con-su-realidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/perugorria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cartas de Cuba 

<iframe src="http://www.ivoox.com/player_ek_10263304_2_1.html?data=kpWfmJiXdJWhhpywj5aYaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5ynca3VjMzS0NnJb8Lk0N7OjcrQb8Tdz8qYxdrGpc%2FjjNXc1JDVucaf1MqYy8nJstXdx87Qw5DHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jorge Perugorria] 

###### [31 Ene 2016] 

##### [**Carolina Garzón - Contagio Radio**] 

Resulta inevitable asociar el nombre de Jorge Perugorria con el título de la película “Fresa y Chocolate”, cinta representativa del cine cubano de los años 90, gracias al éxito alcanzado por la producción a nivel local e internacional, donde su interpretación como Diego, un joven artista homosexual, fue elogiada por el público y la crítica.

Pero del trabajo del actor y director, nacido en la Habana, hay mucho por destacar. En 25 años de trayectoria en la gran pantalla Perugorria ha protagonizado más de cincuenta películas y dirigido varios documentales en la Isla y en el exterior, que le permiten hablar con amplio conocimiento de causa sobre el cine que se produce en Cuba, sus transformaciones y los retos que enfrenta.

Aprovechando su participación como invitado de la edición número 11 del Hay Festival en Cartagena, Contagio Radio habló con el actor sobre la actualidad del cine cubano y los cambios que pueden venir a partir de la llegada de las nuevas tecnologías, el nacimiento de un movimiento independiente de nuevos realizadores y las oportunidades que vendrían con el restablecimiento de relaciones con Estados Unidos.

**El cine cubano y su público.**  
En concepto de Perugorria, el cine cubano goza del favor del público desde hace muchos años gracias a que “la gente se siente identificada por que siempre ha sido un reflejo de la realidad, de sus problemas, sus contradicciones, sueños y frustraciones”, además por ser “un espacio abierto al pensamiento crítico”, algo inexistente en medios masivos como la televisión o la prensa.

**Relaciones con Estados Unidos.**  
A propósito del restablecimiento de relaciones entre la Habana y Washington, Perugorria es bastante optimista al considerar que mejorará la calidad de vida de la gente “siempre que el restablecimiento se haga bajo el respeto a la diferencia”, lo que incluye al cine por el gran “interés de rodar grandes producciones de la industria (Hollywood) en Cuba” lo que puede resultar beneficioso si los recursos percibidos son “reinvertidos en el cine cubano”.

**Apoyo al cine de autor**.  
El actor considera que desde las instituciones encargadas de asignar los recursos para el cine, tanto en Cuba como en Latinoamérica, “se apuesta por temas comerciales más que por los complejos y profundos” lo que dificulta enormemente el trabajo de conseguir presupuesto para el cine de autor, por lo que considera que deben existir “leyes que apoyen a los cineastas” y no se subestime el criterio del público que puede ser formado con otro tipo de oferta cinematográfica.

**Actor, Director y Pintor.**  
Una de las facetas menos conocidas de Perugorria es la de pintor, actividad que llegó a su vida como un pasatiempo “la pintura es algo personal, lo disfruto y ahora le dedico más tiempo, tengo taller en mi casa y siempre estoy haciendo cosas” material con el que realizará una exposición en la Habana en una galería de su propiedad. Su cercanía con la actuación, que ha representado el grueso de su trabajo, le llevó a encontrar en la dirección otra manera de expresarse, particularmente en el desarrollo de documentales.

**El presente y lo que viene**  
En la actualidad Perugorria promociona su película “Fátima o el parque de la fraternidad”, que fue estrenada en el Festival de la Habana, y que está basada en un cuento del escritor cubano Miguel Barnet, simultáneamente participa en la adaptación de la tetralogía del escritor Leonardo Padura “Cuatro estaciones” donde interpreta al detective Mario Conde, que será presentada en cine y en la televisión.

Adicionalmente estará en frente del "Festival de Cine Pobre Gibara", creado por el fallecido director Humberto Solás, a través del cual se apoya al cine independiente, de bajo presupuesto y que tiene pocos espacios para verse.

Como buen cineasta siempre está en búsqueda de nuevos personajes e historias que le permitan encontrar nuevas maneras de expresarse, no descarta volver a trabajar en Colombia donde participo en producciones como Edipo Alcalde de Jorge Alí Triana y Edificio Royal de Ivan Wild, “siempre ha sido un placer, y siempre que he podido volver a trabajar acá lo hago, para los cubanos es un lugar donde nos quieren mucho, se siente como en casa”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
