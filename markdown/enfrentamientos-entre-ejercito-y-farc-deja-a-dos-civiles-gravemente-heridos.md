Title: Enfrentamiento entre Ejército y FARC deja a dos civiles gravemente heridos
Date: 2015-06-01 12:37
Category: DDHH, Nacional
Tags: Alto Comisionado de las Naciones Unidas para los Derechos Humanos, Cese al fuego bilateral, Conflicto armado en Nariño, Defensoría del Pueblo, Derecho Internacional Humanitario, FARC-EP, nariño, Red de Derechos Humanos Francisco Isaías Cifuentes, Tumaco
Slug: enfrentamientos-entre-ejercito-y-farc-deja-a-dos-civiles-gravemente-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/operaciones_militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.radiosantafe.com]

<iframe src="http://www.ivoox.com/player_ek_4579928_2_1.html?data=lZqkm56WfI6ZmKiak52Jd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fa08rb1sbRrcbi1dTgjcrSuNPZjKrXh6iXaaKt08jW1tSPvYy6orewjcnJrsKfwpDR0diPp8rqytGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Pantoja, Red de Derechos Humanos Isaías Cifuentes] 

Desde las 6 de la mañana el pasado 29 de mayo en el municipio de **Tumaco, Nariño,** el **Ejército Nacional inició enfrentamientos con integrantes de la Columna Móvil Daniel Aldana de las FARC–EP.** En medio del hostigamiento resultaron gravemente heridos dos civiles, entre ellos una menor de 16 años en estado de embarazo, y un campesino de 36 años de edad.

De acuerdo a Alejandro Pantoja, quien hace parte de la Red de Derechos Humanos Isaías Cifuentes, **Diana Marcela Quiñones, quien tiene seis meses de embarazo,** terminó herida la luego de ser impactada por una bala en su cadera y por la onda explosiva de una granada que cayó cerca a su lugar de residencia.

Así mismo, **Seljibes Jasier Sambrano, un campesino que salía hacia su trabajo,** terminó herido por siete impactos de fuego, cuatro de ellos en la pierna, una en el abdomen, otro en el hombro y uno en la cabeza, lo que en este momento lo tiene con un diagnóstico reservado delicado en cuidados intensivos.

Ambas personas fueron atendidas por la comunidad y posteriormente fueron trasladas de al hospital departamental donde la joven embarazada se encuentra saliendo de una cirugía con su bebé a salvo. Por otro lado, Jasier Sambrano se encuentra en la unidad de cuidados intensivos con **pronóstico reservado** debido a la gravedad de la herida en su cabeza.

Según afirma Pantoja, citando a la Defensoría del Pueblo que realizó un proceso de verificación, los hechos se presentaron debido a que soldados de la Brigada Móvil No. 32 de La Fuerza de Tarea Pegaso orgánica de la Tercera División del Ejército Nacional, que llevaron a cabo una **emboscada contra guerrilleros de la Columna Móvil Daniel Aldana** de las FARC.

Cabe recordar que el departamento de Nariño ha sido uno de los epicentros del conflicto armado, de hecho, según Pantoja, cuando se decretó el cese unilateral por parte de las FARC, hubo una ofensiva por parte de la fuerza pública en el territorio, por lo que pese al cese al fuego, la tensión se mantuvo por cuenta de las **acciones militares que generaron infracciones a los derechos humanos y al derecho internacional humanitario.**

Actualmente, tras la suspensión del cese al fuego unilateral, se **continúa la avanzada de las fuerzas militares y se agudiza la guerra**; así mismo se **estigmatiza la protesta social**, ya que la comunidad se declaró en paro y este fue tomado como una manifestación armada, donde los pobladores terminan siendo los afectados, por lo que estos se unen a la exigencia de otras comunidades pidiendo el cese al fuego bilateral.
