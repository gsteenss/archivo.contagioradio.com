Title: Partidos de izquierda Españoles y Europeos se alían en contra del TTIP
Date: 2015-05-19 18:31
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Acuerdo secreto entre EEUU y Europa, Partidos españoles firman pacto contra TTIP, Tratado de libre comercio entre España y Europa, TTIP España y EEUU
Slug: partidos-de-izquierda-espanoles-y-europeos-se-alian-en-contra-del-ttip
Status: published

###### Foto:Tenacarlos.wordpress.com 

**Partidos políticos de izquierda, nacionalistas, movimientos sociales y sindicales** se reunieron hoy en Madrid para anunciar que presentarán **mociones en contra del TTIP** en los municipios donde gobiernen, de cara a las elecciones del 24 de Mayo en España.

Después del  **Encuentro municipal contra el Tratado Transnacional de Libre Comercio e Inversión (TTIP)** entre Estados Unidos y Europa. Izquierda Unida, Bildu, Equo, Podemos, y Xunta Aragonesista decidieron **boicotear el TTIP.**

A través de los **ayuntamientos y municipios** donde obtengan gobernaciones se declararán **Zonas Libres del TTIP,** mediante mociones alegando que atenta contra la soberanía de los pueblos y la democracia. Actualmente son **30 los ayuntamientos** que ya se han declarado en Zona Libre del TTIP, y estos se suman a las iniciativas Europeas de **Francia, Austria y Alemania**.

Esta última ya ha recogido más de **800 mil firmas en contra del tratado**, a esto hay que añadir el **veto de Grecia** que complica aún más su aprobación.

La **UE solo ha permitido a los eurodiputados acceder a una parte de los documentos secretos**. Solo han podido verlos durante **dos horas y bajo la estricta vigilancia** de un funcionario en un aula habilitada con cámaras y servicio de seguridad.

El TTIP se inicia con las negociaciones entre el presidente George Bush de EEUU y del europarlamento Giulio Andreotti en 1990. Con el objetivo de favorecer los lazos comerciales entre la Comunidad económica Europea y EEUU.

Hoy el TTIP es **cuestionado por todas las fuerzas de izquierda de Europa, sindicatos y movimientos sociales,** quienes lo califican como una amenaza para las libertades, los derechos laborales y  las políticas sociales que quedan supeditadas a los interés de las grandes corporaciones.

Algunos analistas señalan que con el TTIP el aparato de **justicia quedaría supeditada al sector privado** y sus intereses**,**la perdida de derechos laborales en referencia a que **EEUU solo ha firmado 2 de 8 acuerdos de la OIT y la privatización de sectores públicos claves** en beneficio de las multinacionales.

Otra de las críticas es el **total hermetismo con el que se esta negociando, convirtiéndolo casi en secreto** para los grupos parlamentarios. Según las proyecciones la negociación que debería finalizar este mismo año.

Dicho tratado sería un paso más en la aplicación de las **políticas neoliberales** de los principales partidos europeos, que conlleva recortes en derechos laborales y sociales, y como consecuencia el empobrecimiento de la población.
