Title: 12 ex carabineros procesados por secuestro ocurrido en Chile en 1974
Date: 2016-07-28 12:22
Category: El mundo
Tags: Dictadura chilena, Secuestrados durante dictadura Chilena
Slug: 12-ex-carabineros-procesados-por-secuestro-ocurrido-en-chile-en-1974
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/origin_9584201675.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: Radio UChile 

##### [28 Jul 2016] 

Una docena de ex integrantes del **Servicio de Inteligencia de Carabineros SICAR**, serán procesados por su responsabilidad en el **secuestro de dos mujeres militantes del Partido Socialista chileno** (PS), ocurrido a mediados del año 1974 durante la dictadura militar de Augusto Pinochet.

Por el auto dictado por Mario Carroza, ministro en visita para causas por violaciones a los Derechos Humanos, será procesado **Manuel Muñoz Gamboa en calidad de autor del secuestro** de Ana María Campillo Bastidas que tuvo lugar desde el 19 de junio del año en cuestión y de Patricia Herrera Escobar, ocurrido 8 días después.

De acuerdo con la información obtenida durante la investigación, las mujeres habrían sido detenidas en distinto puntos de la región metropolitana, para luego ser conducidas a los los subterráneos de la Plaza de la Constitución donde fueron **retenidas y sometidas a diversas prácticas de tortura y abusos de tipo sexual**.

En la lista de los ex- agentes procesados como cómplices de los delitos aparecen los nombres de José Hoffmann Oyarzún, Francisco Illanes Miranda, José Luis Contreras Valenzuela, Winston Cruces Martínez, Ernesto Lobos Gálvez, Gilberto Mora Garay, Pedro Retamal Ortega, Sabino Roco Olguín, Alejandro Sáez Mardones, José Alvarado Alvarado y Sergio Retamal Hernández.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
