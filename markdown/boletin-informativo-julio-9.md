Title: Boletín informativo Julio 9
Date: 2015-07-09 16:46
Category: datos
Tags: demanda en contra de las reformas al fuero penal militar, Encuentro Mundial de Movimientos Populares en Bolivia, Foro Internacional Memoria Histórica y la Verdad de las Mujeres, hackeada la empresa “Hacking Team”, plantón carnaval consulta popular taurina
Slug: boletin-informativo-julio-9
Status: published

[Noticias del Día:]

<iframe src="http://www.ivoox.com/player_ek_4751335_2_1.html?data=lZyik5iXeY6ZmKiakpmJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaTijK7byNTWscLoytvcjcnJb6vpzc7cjZ6RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El 8 de julio se llevó a cabo el Foro Internacional Memoria Histórica y la Verdad de las Mujeres, que contó con la participación de más de 300 personas, quienes realizaron un debate con el fin de preparar a las mujeres para un posible escenario de posconflicto con una eventual comisión de la verdad y marco jurídico para la paz. Habla Marina Gallego, coordinadora de la Ruta Pacífica de las Mujeres.

-Tras ser hackeada la empresa italiana de cibervigilancia y ciberseguridad para los gobiernos “Hacking Team”, salieron a la luz más de 30 países de todo el mundo que contrataron servicios de espionaje para observar de cerca a sus ciudadanos. Colombia hace parte de ellos.

-Organizaciones defensoras de derechos humanos, congresistas y representantes de víctimas, preparan una demanda en contra de las reformas al fuero penal militar, que avanzan en el Congreso de República. Habla Alberto Yepes de la Coordinación Colombia – Europa- Estados Unidos.

-El Encuentro Mundial de Movimientos Populares en Bolivia le abre las puertas al papa Francisco con el fin de generar acercamientos entre el pueblo y la iglesia en temas ambientales y territoriales tras ser publicada la Enciclica ecológica. El día de hoy se llevará a cabo un encuentro multitudinario desde las 5:30 P.M.

-Este viernes 10 de julio a la 1:00 de la tarde se realizará un plantón carnaval a la entrada del Concejo de Bogotá, con el objetivo de solicitar a los concejales la realización de la consulta popular y demostrar si aun existe o no arraigo frente a las corridas de toros. Natalia Parra, directora de la Plataforma ALTO.
