Title: Lanzan unamos nuestras voces, la canción para la reconciliación
Date: 2017-03-26 20:28
Category: Cultura, Nacional
Tags: FARC, Reincidentes, Tanja, Unamos nuestras voces
Slug: lanzan-unamos-nuestras-voces-la-cancion-par-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Disco.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Mar. 2017] 

La banda **Reincidentes BTÁ y varios integrantes de la guerrilla de las FARC** dieron a conocer este domingo ***Unamos nuestras voces, *una canción** que, trabajada de manera conjunta, pretende mostrar cómo la música hace parte de las herramientas para la exigibilidad del proceso de implementación de los Acuerdos de Paz y contribuye a la construcción de un nuevo país.

En una breve comunicación aseguraron que **"tejimos esta melodía de alegría y esperanza como canto conjunto de exigencia del cumplimiento del Acuerdo de Paz y su adecuada implementación". **

Esta producción que tuvo varios días de trabajo y parte de varios proyectos artísticos que se avecinan, podrá ser escuchada a través de la plataforma de vídeos Youtube.

En *Unamos nuestras voces participaron* **Manuel Garzón vocalista de Reincidentes, Alexandra Nariño, Tania García, Anderley Sánchez y Boris Guevara integrantes de las FARC.* ***

**Escuche aquí la canción completa.**

\[embed\]https://www.youtube.com/watch?v=D8hTesp6AnY&feature=youtu.be\[/embed\]

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
