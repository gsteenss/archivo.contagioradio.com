Title: Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto
Date: 2020-04-27 08:57
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Cuaca, Pacífico colombiano
Slug: comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pacífico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Verdad Pacífico  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como respuesta a la escalada de violencia que ha visto el país en las últimas semanas en particular en departamentos como **Nariño y Cauca**, las organizaciones étnicoterritoriales y sociales del Pacífico junto a las jurisdicciones eclesiásticas, recalcan ante el Gobierno y los distintos grupos armados su autoridad para exigir una salida negociada al conflicto en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El accionar de grupos armados legales e ilegales y paraestatales ha dejado un saldo de 84 líderes sociales asesinados, al menos 26 en el departamento de Cauca. Las masacres en los municipios de Micay y Piendamo en menos de un mes, además de las situaciones de confinamiento, reclutamiento de menores y agresiones, han llevado a las comunidades ha expresar una vez más que no se identifican con ningún bando bélico ni acción militar. [(Le puede interesar: Con derecho de petición, Ejército exige a líderes del Chocó demostrar lo que denuncian)](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Contamos con la autoridad para hacerlo ya que son nuestras comunidades las que están sufriendo la tragedia, las consecuencias y nuestros familiares son los que están perdiendo la vida" afirma el líder social Leyner Palacios, sobreviviente de la masacre de Bojayá y blanco de recientes amenazas. [(Lea también: Al menos 80 personas desplazadas permanecen en retén ilegal en Argelia, Cauca)](https://archivo.contagioradio.com/desplazamientos-reten-ilegal-argelia-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidades del pacífico respaldan labor de la iglesia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A través del comunicado, las comunidades expresa su rechazo hacia el ELN, grupo que accedió a un cese al fuego durante un mes y al que señalan de descalificar la labor que monseñor Dario Monsalve ha realizado al recorrrer el pacífico, reiterando el apoyo de la Comisión Interétnica de la [Verdad del Pacífico](https://twitter.com/VerdadPacifico) hacia el religioso. [(Lea también: Asesinan a tres personas en Micay, Cauca)](https://archivo.contagioradio.com/micay-asesinato-tres-personas/?fbclid=IwAR0xe_pBhvJdpr02nfRGZzyWUnH_CsXJ6UMaSszlLUhILdYGKqC0lirXMu0)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta petición, los obispos de Cauca y Nariño hicieron un nuevo llamado a las estructuras armadas que promueven el narcotráfico para que cesen su accionar violento y den prioridad a la vida de las comunidades en medio de la crisis humanitaria que vive el país a causa del covid-19. [(Lea también: Obispos de Cauca y Nariño piden parar el flagelo del narcotráfico)](https://archivo.contagioradio.com/obispos-de-cauca-y-narino-piden-parar-el-flagelo-del-narcotrafico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 17 de abril, en Cauca han sido asesinados seis lideres sociales: Andrés Cacimance Burbano, Teodomiro Sotelo Anacona, Mario Chilhueso y Sabino Angulo, en El Tambo, Cauca, Jesús Albeiro Riascos, en Aguas Claras, Cañón del Micay y Hugo de Jesús Giraldo López en Santander de Quilichao.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
