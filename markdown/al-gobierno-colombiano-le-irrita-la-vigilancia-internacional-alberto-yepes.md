Title: Al Gobierno colombiano le irrita la vigilancia internacional: Alberto Yepes
Date: 2019-10-30 17:12
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Derechos Humanos, Gobierno, Naciones Unidas, paz
Slug: al-gobierno-colombiano-le-irrita-la-vigilancia-internacional-alberto-yepes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Gobierno-y-Naciones-Unidas-e1572473414153.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [@UNHumanRights]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}  
] 

A lo largo de esta semana diferentes organizaciones sociales advirtieron por la no renovación del mandato en el país de la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, sin embargo, **este miércoles en la mañana se anunció la renovación.** Para defensores de derechos humanos, la extensión del mandato hasta 2022 de la Oficina es una forma de protección a las comunidades, la implementación de la paz, y una forma de mantener informada a la comunidad internacional sobre lo que realmente está ocurriendo en el país.

### **¿Un proceso lento para evitar la vigilancia internacional?** 

El pasado 28 de octubre el expresidente Ernesto Samper denunció en su cuenta de twitter que funcionarios del gobierno colombiano estarían en Ginebra, Suiza, exigiendo que cese la función de la Oficina. Diferentes organizaciones sociales durante esta semana reaccionaron a dicha publicación, pidiendo al Gobierno aclarar su posición sobre este tema, y solicitando la ampliación del mandato. (Le puede interesar: ["La seguridad decae en antiguas zonas de conflicto alerta informe de la ONU"](https://archivo.contagioradio.com/la-seguridad-decae-en-antiguas-zonas-de-conflicto-alerta-la-onu/))

Dicha ampliación ocurrió este miércoles, y según **Alberto Yepes, director de la Coordinación Colombia, Europa, Estados Unidos (COEUROPA),** la demora del Gobierno en dar este paso "demuestra una voluntad del Gobierno de mantener presión sobre los organismos de vigilancia de derechos humanos". La presión respondería a una incomodidad del Gobierno porque ojos internacionales vean que en el último año "hay una agudización de la crisis humanitaria expresada en factores como las desapariciones forzadas, la intensificación de la confrontación armada, el regreso de los falsos positivos y la represión a la protesta social".

### **La lentitud y poco interés en el Acuerdo de Paz** 

En hechos pasados también se ha cuestionado la prontitud con que el Gobierno resuelve ciertos casos, y la lentitud con que asume otros, como lo fue la aprobación de la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP). Yepes declaró que las situaciones respecto a la JEP y la Oficina obedecen a "malos cálculos del Gobierno, a la percepción de que puede eludir compromisos con los derechos humanos". (Le puede interesar:["Presidente Duque sanciona Ley Estatutaria de la JEP"](https://archivo.contagioradio.com/presidente-duque-sanciona-ley-estatutaria-de-la-jep/))

Yepes señaló que el Gobierno maneja un doble discurso. Uno para la opinión pública, que parece estar comprometido con los derechos humanos, la protesta social, el acuerdo de paz, pero en realidad hace lo contrario. (Le puede interesar: ["Líderes del Cauca esperan que ONU se lleve una visión real de lo que sucede a nivel territorial "](https://archivo.contagioradio.com/lideres-del-cauca-esperan-que-onu-se-lleve-una-vision-real-de-lo-que-sucede-a-nivel-territorial/))

Para develar ese discurso oculto, el Abogado resaltó el papel que juega la Oficina del Alto Comisionado, al evidenciar las situaciones de vulneraciones a los derechos humanos en las regiones y los incumplimientos en el Acuerdo de Paz. Adicionalmente, declaró que esta supervisión internacional juega un papel importante en aras de aportar a la defensa de la democracia y la legitimidad de las instituciones. (Le puede interesar:["Duque y su estrategia internacional para desajustar el proceso de paz"](https://archivo.contagioradio.com/duque-y-su-estrategia-internacional-para-desajustar-el-proceso-de-paz/))

### **A este Gobierno no le gusta la supervisión y le irrita la vigilancia internacional: Yepes  
** 

El director de COEUROPA concluyó que **“a este Gobierno no le gusta la supervisión y le irrita la vigilancia internacional, pero es necesario que la comunidad permanezca vigilante porque lo que está pasando es muy grave”.** En consecuencia, llamó a que la sociedad civil juegue un papel determinante en la vigilancia e impulso para que la comunidad internacional mantenga un rol vigilante respecto a lo que ocurre en el país. (Le puede interesar: ["Duque pone ‘pico y placa’ a visitas internacionales en los ETCR"](https://archivo.contagioradio.com/gobierno-pico-placa-visitas-etcr/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
