Title: POT de Peñalosa satisface intereses económicos y no las necesidades de Bogotá
Date: 2019-08-01 17:45
Author: CtgAdm
Category: Nacional
Tags: Bogotá, Concejo de Bogotá, Metro, POT
Slug: pot-satisface-intereses-economicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Fotos-editadas1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @EnriquePenalosa  
] 

El Consejo Territorial de Planeación de Bogotá, emitió un concepto negativo del Plan de Ordenamiento Territorial (POT) presentado por la administración de Enrique Peñalosa. Según denunciaron quienes integran el Consejo, el **Plan estaría destinado a satisfacer grandes intereses económicos** y no atiende los problemas de movilidad ni seguridad de Bogotá, además no prioriza su cuidado ambiental. (Le puede interesar: ["Más de 150 activistas se reúnen en Bogotá para hacer resistencia pacífica a la militarización"](https://archivo.contagioradio.com/mas-de-150-activistas-se-reunen-en-bogota-para-hacer-resistencia-pacifica-a-la-militarizacion/))

**Aura Rodríguez, integrante de Viva la Ciudadanía y secretaria del Consejo Territorial de Planeación**, explicó que el POT es la forma en que una ciudad organiza el uso del suelo; el proyecto del Plan se presentó el pasado 14 de julio ante el Consejo, y ellos tenían 30 días hábiles para emitir un concepto. El martes de esta semana fue emitido dicho concepto, cuyo resultado fue negativo considerando que va en contravía de los derechos ambientales, sociales, económicos y culturales de los y las habitantes de Bogotá.

### **Las cinco críticas al Plan de Ordenamiento Territorial**

En primer lugar, Rodríguez señaló que el Plan se proyectó usando cifras del censo realizado por el DANE de 2005, **"esas cifras muestran un crecimiento poblacional que creemos podría ser mayor que el esperado"**. Para la Activista, ello significa que se está proyectando una ciudad más grande que la planeada de acuerdo al más reciente censo; ello justificaría "7 macroproyectos de vivienda, que al final lo que hace es beneficiar a empresas constructoras de la ciudad en territorios protegidos ambientalmente", por ejemplo, como el que se quiere construir en la reserva Thomas van der Hammen.

En segunda medida, dijo que el POT pasa por encima fallos del Consejo de Estado que protegen los cerros orientales como reserva natural de la ciudad, "haciendo que se pueda construir más allá de lo permitido". En el mismo sentido, la integrante de Viva la Ciudadanía, aseguró que también desconocía fallos del alto Tribunal en la protección del Río Bogotá, en tanto plantea la creación de parques lineales, lo que implica asumir su cuidado desde un enfoque de desarrollo urbanístico y no de conservación ambiental.

(Le puede interesar:["¿Cuáles serían las afectaciones del Sendero Las Mariposas?"](https://archivo.contagioradio.com/cuales-serian-las-afectaciones-del-sendero-las-mariposas/))

En cuanto a movilidad, Rodríguez criticó que el Plan sigue **"planteando el centro de la movilidad en transmilenio y no el metro"** a través de la construcción de dos líneas para el metro, cuya función sería alimentar troncales que ya están operando al límite de su capacidad. (Le puede interesar: ["Concejales piden medidas cautelares para evitar corrupción en Metro de Peñalosa"](https://archivo.contagioradio.com/evitar-corrupcion-metro-penalosa/))

Una crítica al POT que había surgido de distintas voces era la ausencia de un capítulo sobre la implementación del Acuerdo de Paz en Bogotá; sobre este tema, la Secretaria del Consejo aseguró que el Plan disminuye la ruralidad de la ciudad, permitiendo que el suelo rural se convierta en suelo urbano y "empiece a ser parte de la tierra para especular". Para Rodríguez, esto **"profundiza conflictos históricos como el de la tierra"**. (Le puede interesar: ["En 2025 Colombia tendrá un invetario de sus tierras gracias al catastro multipropósito"](https://archivo.contagioradio.com/inventario-tierras-catastro-multiproposito/))

Por último, los integrantes del Consejo criticaron que el POT se haya presentado sin un proceso de concertación y participación ciudadana de fondo; en conclusión de Rodríguez, "cuando uno ordena el territorio priorizando los intereses de solo unos pocos, agranda todas las conflictividades que existen en la ciudad". (Le puede interesar: ["Peñalosa: ¿107 millones de razones para apoyar el Proyecto Proscenio?"](https://archivo.contagioradio.com/penalosa-proyecto-proscenio/))

### **¿Que viene para el POT luego del concepto negativo?**

Rodríguez declaró que "si la Administración fuera responsable con lo que estamos haciendo, tendría que volver a armar todo un proceso de participación y concertación sobre un nuevo POT"; sin embargo, dijo que **lo más probable es que lo pasará sin modificaciones al Concejo de Bogotá,** el cual tiene 90 días para discutirlo y decidir qué aprobar. Esta situación es posible debido a que el concepto emitido por el Consejo Territorial no tiene un carácter vinculante.

Pese a ello, la activista aseveró que "todavía tenemos mucha pelea para dar", porque esperan que la ciudadanía se movilice y le haga saber a la Alcaldía su desacuerdo con el Plan; adicionalmente, manifestó que esperaban que las distintas campañas al Concejo, la Alcaldía y las Juntas Administradoras Locales (JAL) pongan este tema en debate, y generen cambios en el documento planteado. (Le puede interesar: ["La violencia neoparamilitar en la frontera sur de Bogotá"](https://archivo.contagioradio.com/neoparamilitar-frontera-bogota/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39460319" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39460319_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
