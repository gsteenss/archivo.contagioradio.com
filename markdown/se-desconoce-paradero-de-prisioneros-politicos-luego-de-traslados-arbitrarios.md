Title: Se desconoce paradero de prisioneros políticos luego de traslados arbitrarios
Date: 2020-03-24 10:11
Author: CtgAdm
Category: Actualidad, DDHH
Tags: FARC, INPEC, prisioneros políticos
Slug: se-desconoce-paradero-de-prisioneros-politicos-luego-de-traslados-arbitrarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde tempranas horas de la mañana, colectivos de derechos humanos están denunciado que e desconoce paradero de prisioneros políticos luego de traslados arbitrarios. Según el Colectivo Libres e Inocentes, estos movimientos se están realizando de forma incomprensible por parte del INPEC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los traslados que ya se realizaron se encuentran los de los prisioneros políticos **Moises Quintero**, José Ángel Parra y Oscar Rodríguez, que se encontraban recluidos en la cárcel La Picota. Sus compañeros de celda denunciaron que otros prisioneros políticos de FARC también habrían sido sustraidos del penal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma el colectivo Libres e Inocentes informa que las las prisioneras políticas Lizeth Rodríguez, Lina Jiménez y Alejandra Mendez, víctimas del montaje judicial conocido como Caso Andino, fueron sustraidas de forma **arbitraría de su celda para traslado con destino desconocido.** (Le puede interesar: "Colombia: [El Estado debe tomar medidas idóneas para proteger la población carcelaria ante la pandemia del COVID-19")](https://www.justiciaypazcolombia.com/colombia-el-estado-debe-tomar-medidas-idoneas-para-proteger-la-poblacion-carcelaria-ante-la-pandemia-del-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hechos se presentaron a las tres de la mañana y de acuerdo con las demás reclusas, la guardia del INPEC le advirtió a las demás mujeres que no se acercaran a las celdas, asimismo, aseguran que las indicaciones de la guardia a las mujeres, fue **"alistar tres mudas de ropa porque van a tierra caliente".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tanto el Colectivo Libres e Inocentes como el Movimiento Carcelario expresan que hace responsable al Estado por la vida y seguridad de las y los prisioneros políticos trasladados y exigen respuesta inmediata frente a los lugares de traslado. (Le puede interesar[: "Negligencia de Estado contra reclusos que protestaba por riesgo de COVID-19"](https://archivo.contagioradio.com/hay-negligencia-de-estado-contra-reclusos-que-protestaba-por-riesgo-de-covid/))

<!-- /wp:paragraph -->
