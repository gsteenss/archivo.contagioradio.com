Title: Histórico encuentro entre comunidades indígenas y delegaciones de paz en Cauca
Date: 2016-09-07 15:59
Category: Nacional, Paz
Tags: acuerdos de paz, CRIC, Diálogos de La Habana, enfoque étnico acuerdos de paz
Slug: autoridades-indigenas-proponen-criterios-para-implementar-enfoque-etnico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CRIC-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC ] 

###### [7 Sept 2016] 

Este miércoles 120 autoridades indígenas del Consejo Regional Indígena del Cauca CRIC, se reúnen con Sergio Jaramillo, Marcos Calarca, Matías Aldecoa y delegados de la ONU, en el Resguardo La María de Piendamó, Cauca, con el fin de acordar los **criterios bajo los cuales serán implementados los seis puntos del enfoque étnico territorial** [[acordado en La Habana el pasado 23 de agosto](https://archivo.contagioradio.com/que-lograron-incluir-los-pueblos-afro-e-indigenas-en-los-acuerdos-de-paz/)].

De acuerdo con Luz Edith Chilo, Gobernadora del Resguardo Pioyá de Caldono, Cauca, el propósito del encuentro es concertar principios para que la implementación de los acuerdos de paz no implique de ningún modo la vulneración de los derechos de las comunidades indígenas, que han decidido como gesto humanitario, **destinar parte de su territorio para la instalación de una de las zonas** veredales transitorias.

"Desde hace 45 años venimos construyendo paz", asegura la Gobernadora y agrega que una de las propuestas que presentarán a las delegaciones es que la **Guardia Indígena haga parte del mecanismo de seguridad** [[en las zonas verdales](https://archivo.contagioradio.com/iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias/)], con el fin de garantizar que la implementación de los acuerdos permita la defensa de la territorialidad, la autonomía, los derechos colectivos y la consolidación de un verdadero camino de armonía equilibrio y convivencia entre los pueblos.

<iframe src="https://co.ivoox.com/es/player_ej_12817738_2_1.html?data=kpelk5ybd5mhhpywj5WcaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5ynca3p25Cyxs7YrIy3yc7Z0YqWh4y70MfS1NPFqNDmwpDRx9GPlsbnyNrO1MnTb7Hd0N6SpZiJhZKfxcqYpcaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
