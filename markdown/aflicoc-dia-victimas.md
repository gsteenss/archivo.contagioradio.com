Title: Así conmemoran en colegio AFLICOC, el día de las víctimas
Date: 2018-04-19 16:16
Category: Comunidad, Nacional
Tags: AFLICOC, Colegio, Curvarado, educacion
Slug: aflicoc-dia-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/estudiantes-colegio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colegio AFLICOC] 

###### [Yo Reporto]

[Este 17 de abril, en medio de la lluvia, se desarrolla la semana de clases del colegio AFLICOC en la zona humanitaria “Camelias es tesoro” ubicada en el departamento del Chocó, en el corregimiento de Belén de Bajirá. Cerca de 90 estudiantes y sus profesores realizamos una actividad de solidaridad con nosotros mismos, víctimas de la violencia estatal, y con las millones de víctimas en Colombia.]

[Nos reunimos en un espacio conjunto estudiantes y profesores. Un espacio en el que todos y todas nos encontramos para reflexionar sobre aquellos temas que nos competen como colegio en resistencia ante el sistema social y educativo que impera.]

[Este espacio ha sido pensado con la intención de reconocer y conmemorar el día de la solidaridad con las víctimas del conflicto armado en Colombia, para ello, los profesores iniciamos con una contextualización de lo sucedido el día 9 de abril de 1948, día en que históricamente se reconoce el inicio de una oleada de violencia que hoy intentamos detener y que ha dejado millones de víctimas a lo largo y ancho del territorio colombiano.]

[ Para reconocer qué hace que una persona sea considerada víctima realizamos “Teatro Imagen”, actividad en la que a partir de una representación corporal congelada damos cuenta de una situación, siendo así, un grupo de estudiantes participa voluntariamente mientras los demás observan y reconocen qué situación están dramatizando, de esta forma, reconocemos que el desplazamiento, asesinato, tortura, secuestro, entre otros delitos, son acciones que, en el marco del conflicto armado, atentaron contra nuestra vida e integridad, tal como lo indica la ley 1448 del 2011 por la cual se reconoce la necesidad de brindar atención y reparación a las víctimas.]

[Luego de habernos concientizado y de entender que ayer y hoy las víctimas en Colombia siguen aumentando, sentimos la necesidad de expresar un mensaje de fuerza y resistencia para continuar construyendo una Colombia en paz. Razón por la cual cada uno, en una hoja blanca escribe, desde la solidaridad que lo convoca, unas palabras que nos den la fortaleza para seguir caminando en esta utopía de otro mundo posible.]

[Mensajes como “*a pesar de lo que suceda no nos daremos por vencidos, lucharemos por nuestro territorio, hogar y biodiversidad, aunque haya Sido mucho el sufrimiento y la sangre derramada, estaremos firmes como una roca para lograr la paz en nuestras vidas, fuerza para seguir luchando y si desmayamos nos levantamos unos a otros con la frente en alto y nos ayudamos para no volver a caer*” fueron los que vibraron en el corazón de los resistentes, y quedaron plasmados en el papel que se convirtió en una paloma mensajera que vuela desde nuestros corazones hacia el de las víctimas como una muestra de amor y defensa de la vida digna.]

![sin olvido palomas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/sin-olvido-palomas-600x450.jpg){.wp-image-52890 .aligncenter width="449" height="337"}

[Finalmente, todas las palomas se reunieron en un cartel, en el que SIN OLVIDO fue la base fundamental de este reconocimiento histórico, y que nos llevó a expresar: Ayer, hoy, mañana y siempre no olvidamos la sangre derramada, las selvas desiertas, los corazones desolados… ayer, hoy, mañana y siempre resistiremos desde la verdad, la libertad, la solidaridad, la justicia y la no repetición como comunidades que construyen paz desde los territorios por la defensa de la vida.]

[ Otro mundo si es posible, lo llevamos nosotros en nuestro corazones.]

[Vanesa Bustos - Laura Daniela López - Oscar Torres]

[Profesores voluntarios colegio AFLICOC]

19 Abr 2018
