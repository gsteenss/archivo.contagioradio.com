Title: Atentan contra vida de defensora de derechos humanos María Ruth Sanabria
Date: 2018-05-23 16:01
Category: DDHH, Política
Tags: Arauca, cpdh, defensores de derechos humanos, María Ruth Sanabria
Slug: atentan-contra-vida-de-defensora-de-derechos-humanos-maria-ruth-sanabria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Dd6QzyhU8AIjT5O.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Federación Luterana Mundial] 

###### [23 May 2018] 

La defensora de derechos humanos e integrante del Comité Permanente por la Defensa de los Derechos Humanos, María Ruth Sanabria, fue víctima de un atentado ayer en la noche, cuando sicarios que se movilizaban en una camioneta roja, le dispararon con un arma de fuego. **La defensora fue trasladada por su esquema de seguridad a una estación de policía y se encuentra a salvo**.

De acuerdo con Érika Gómez, integrante del CPDH, María Ruth se dirigía hacia Fortun en el departamento de Arauca, acompañada de otros integrantes de esta organización, cuando fue atacada por los hombres. (Le puede interesar: ["No hay paz para las personas defensoras de derechos humanso en Colombia: OMCT"](https://archivo.contagioradio.com/no-hay-paz-para-las-personas-defensoras-de-derechos-humanos-omct/))

 De igual forma Gómez recordó que esta no es la primera agresión que se hace contra la vida de María Ruth. El año pasado, **la defensora también fue víctima de una emboscada, cerca al municipio de Arauquita**. Este hecho fue puesto en conocimiento de las instituciones correspondientes y aún no se tiene información sobre quiénes estarían detrás del mismo.

María Ruth Sanabria se encontraba denunciado la presencia de grupos armados, que podrían ser identificados como paramilitares y que, además, se encuentran en territorios altamente militarizados o en donde hay una fuerte presencia de la Fuerza Pública. De igual forma, **el CPDH se encuentra denunciando un caso ejecución extrajudicial, cometido en el departamento**.

Sanabria fue reconocida hace dos años por la organización Diakonía como la defensora del año 2016 debido a su compromiso y trayectoria en la defensa de los derechos humanos y al trabajo que ha desarrollado específicamente en el departamento de Arauca.

### **Las agresiones a defensores de derechos humanos** 

Gómez señaló que de igual forma, en el departamento de Arauca se ha reportado varias agresiones a defensores de derechos humanos sin que existan respuestas o medidas por parte del Estado para garantizar la vida de los mismos, **“desafortunadamente las investigaciones, tanto en la Fiscalía como en las demás autoridades competentes no han tenido ningún avance significativo”**.

De igual forma expresó que, los integrantes del CPDH han sido víctimas en reiteradas ocasiones de hostigamientos en donde hay personas “extrañas” cerca a los sitios de vivienda o a las oficinas, haciendo un registro fílmico o fotográfico y en última instancia atacan a los defensores.

<iframe id="audio_26142080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26142080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
