Title: Más de 186 mujeres padecen Covid 19 en la Cárcel El Buen Pastor
Date: 2020-07-28 23:34
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: #Cárcel #covid19 #IvánDuque #contagio #Crisis carcelaria, #MovimientoCarcelario #Covid-19 #, carceles de colombia, Carceles de mujeres, Covid-19, Expreso Libertad
Slug: mas-de-186-mujeres-padecen-covid-19-en-la-carcel-el-buen-pastor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/02.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 23 de julio se conoció la muerte de una reclusa de la Cárcel del Buen Pastor producto del contagio de Covid 19. A la fecha denuncian que ya son más de 186 mujeres que dieron positivo al virus en 3 de 9 patios que tiene el centro penitenciario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, mujeres integrantes del Colectivo Mujeres Libres expresan la ausencia total de atención médica adecuada para las mujeres que padecen del virus. Así como la falta de protocolos de bioseguridad para proteger a las demás reclusas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Martha Franco y Jazmin Reyes, señalan que actualmente las mujeres al interior de esta cárcel tienen miedo del avance del virus y de la falta de acciones por parte del presidente Iván Duque para salvaguardar la vida de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_57840254" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_57840254_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

[Vea mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
