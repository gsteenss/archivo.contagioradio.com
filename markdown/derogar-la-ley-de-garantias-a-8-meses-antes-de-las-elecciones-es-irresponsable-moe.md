Title: “Derogar la ley de garantías a 8 meses antes de las elecciones es irresponsable” MOE
Date: 2015-03-30 21:29
Author: CtgAdm
Category: Entrevistas, Política
Tags: BID, FMI, Ley de garantías. Corte Constitucional, Misión de Observación Electoral, Plan Naci
Slug: derogar-la-ley-de-garantias-a-8-meses-antes-de-las-elecciones-es-irresponsable-moe
Status: published

###### Foto: elpais.com.co 

<iframe src="http://www.ivoox.com/player_ek_4285946_2_1.html?data=lZell56Yeo6ZmKiak5iJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56niMbm0MzO1JDQpYzgxt6YxsqPq8LmwtPhh6iXaaK4wtiYw5Ccb87Z1MrgjcbSuMbnjMnSjdHFt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandra Barrios, Directora de la MOE] 

La propuesta de reconfigurar la **“Ley de Garantías”** ha tomado por sorpresa a una serie de organizaciones entre las que se encuentra la **Misión de Observación Electoral**. **Alejandra Barrios, directora de la MOE** explica que aunque es importante hablar de este tema, es necesario que se discuta con mayor profundidad, puesto que no se pueden cambiar las reglas electorales a tan sólo 8 meses de la realización de las elecciones regionales.

Para Barrios, hacer cambios tan sustanciales a tan poco tiempo de las elecciones es irresponsable, se pueden hacer las discusiones pero no los cambios. La directora del MOE señala que al mismo tiempo en que se deroga la **Ley de Garantías habría que reglamentar el seguimiento a la inversión de 5.8 billones de pesos que se podrían ejecutar**. También se debería establecer un protocolo en que todos los organismos del Estado puedan hacer ese seguimiento.

Frente al argumento del gobierno en que afirman que la situación económica del país necesita de los 4 meses para realizar proyectos de infraestructura, Barrios señala primero que no se entiende este planteamiento puesto que el gobierno afirma que el avance económico de Colombia es firme, y segundo porque lo que se destrabaría sería presupuesto ya programado y fijado.

De cara al escándalo de la **Corte Constitucional y sus magistrados**, señala que hay un punto que fue modificado en el 2**005 para la reelección de Álvaro Uribe**, es decir que el **presidente puede elegir a 3 de 9 magistrados**, y esto en el ejercicio de la presidencia durante 8 años evidentemente lograría afectar porque iniciaría un proceso de selección en que priman los intereses políticos. Barrios rescata la decisión del presidente en el sentido de transparentar la elección de los magistrados.
