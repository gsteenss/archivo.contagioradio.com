Title: 1088 colombianos han sido repatriados de Venezuela según OCHA
Date: 2015-08-28 14:18
Category: El mundo, Política
Slug: 1088-colombianos-han-sido-repatriados-de-venezuela-segun-ocha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Colombianos-deportados-Venezuela-Foto-Efe_NACIMA20150825_0106_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

<iframe src="http://www.ivoox.com/player_ek_7663296_2_1.html?data=mJujlZedeo6ZmKialJeJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZGsmZDQ0dHTscPdwtPc1ZDMpc%2Bf1M7R0ZDWqdHV1dfWw8nTt4zYxpDjx9PJvtbZzcaY1crLaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Victor Rodríguez, OCHA] 

<iframe src="http://www.ivoox.com/player_ek_7663322_2_1.html?data=mJujlZiWdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRdZGsmZDQ0dHTscPdwtPc1ZDMpc%2Bf1M7R0ZDWqdHV1dfWw8nTt4zYxpDjx9PJvtbZzcaY1crLaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Nubia Mendoza, MOVICE] 

###### [28 Ago 2015] 

Según cifras de la Oficina de Naciones Unidas para la Coordinación de Asuntos Humanitarios, OCHA, **1.088 colombianos han sido repatriados**, cerca de **4.260 habrían retornado a Colombia de forma espontánea**, al menos **929 se encuentran en seis albergues** habilitados en Cúcuta y Villa del Rosario y 369 se han trasladado a sus ciudades de origen en Colombia.

Al respecto, Víctor Rodríguez, coordinador del medio alternativo Corresponsales del Pueblo, asegura que **los colombianos indocumentados no han sido deportados formalmente**, lo que han hecho las autoridades venezolanas es entregarlos en condiciones dignas al Consulado de Colombia en Táchira para que el gobierno colombiano se encargue de su regreso al país.

No obstante, se registra que **gran cantidad de estos colombianos están ingresando de nuevo a Venezuela por Zulia**, otro punto fronterizo que el presidente Nicolás Maduro está considerando cerrar y sobre el que el monitoreo es constante.

Por su parte, Nubia Mendoza, integrante del MOVICE Capítulo Norte de Santander, asegura que los **dos albergues instalados en Cúcuta son espacios pequeños que no cuentan con servicios básicos por lo cual son insuficientes para atender a las más de 100o personas que han llegado de Venezuela.**

### **El problema del contrabando** 

Rodríguez, asegura que el contrabando de alimentos y gasolina venezolana son muestra de la guerra económica que padece este país. Con el cierre de la frontera se ha puesto freno a la reventa en territorio colombiano de productos de primera necesidad procedentes de Venezuela.

En los últimas días **los venezolanos han podido comprar productos que hace 23 semanas no se encontraban en los supermercados** y tanquear sin hacer filas de más de 5 horas, por lo que están de acuerdo con las acciones adelantadas por el Gobierno.

Por su parte Mendoza, afirma que el  contrabando de víveres de la canasta familiar de procedencia venezolana, se da en parte porque debido a la falta de recursos económicos **para las familias colombianas es más rentable comprar los productos venezolanos**.

¿Por qué con un cierre de frontera Cúcuta queda desabastecida?

La actual situación en la frontera, según Mendoza, evidencia la **alta tasa de desempleo e informalidad, que superaría el 50%, así como de desplazamiento forzado que afecta a Norte de Santander**. Gran parte de la población residente en Cúcuta que vive del contrabando de productos venezolanos, hace parte de quienes huyeron del conflicto armado, tratando de salvaguardarse y buscar condiciones económicas para vivir, puesto que **el contrabando de gasolina resulta más rentable que el narcotráfico**.

La presencia de los grupos paramilitares, como Los Urabeños, y el fuerte control que ejercen sobre las actividades legales e ilegales de las zonas de frontera como Cúcuta, Ureña y Puerto Santander, ha posibilitado que entren a Colombia toneladas de alimentos y galones de gasolina para ser vendidos no sólo en Cúcuta sino en el interior del país, así como el pago de vacunas por parte del sector de transportes.

De acuerdo a Mendoza, **la gasolina sale de Venezuela con el aval de integrantes de la Guardia y de la oposición venezolana**, ingresa a territorio colombiano y a gracias al pago de sobornos a autoridades aduaneras, policías y militares, llega a Aguachica y es guardada en camiones cisterna para ser distribuida como si fuera producida en Colombia.

Rodríguez, asegura que **las cerca de 600 viviendas destruidas son lugares en los que se ha verificado que bandas de colombianos y venezolanos traficaban drogas y personas**, y que éstas hacen parte de las 3000 residencias que fueron construidas en áreas fronterizas protegidas, en la rivera del rio Táchira, con el respaldo del exgobernador Cesar Pérez e integrantes de la derecha colombiana, pese a que la jurisdicción internacional prohíbe la construcción de inmuebles en zonas fronterizas.

Dentro de las medidas **se estima que puedan volver a Venezuela estudiantes y empresarios colombianos** a los que se les compruebe legalidad en su accionar, para que se reactiven los mercados que han sido frenados en Táchira.

Esta situación es un llamado de atención para que los gobiernos tanto de Venezuela como de Colombia asuman **medidas jurídicas que frenen el contrabando que impera en las zonas fronterizas** con la tutela de la Guardia Venezolana y de autoridades colombianas, como la policía y las Fuerzas Militares.
