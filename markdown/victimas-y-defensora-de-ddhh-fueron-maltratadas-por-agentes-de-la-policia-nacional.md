Title: Víctimas y defensora de DDHH fueron maltratadas por la Policía
Date: 2017-10-05 12:26
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Comisión Intereclesial de Justicia y Paz, Policía Nacional
Slug: victimas-y-defensora-de-ddhh-fueron-maltratadas-por-agentes-de-la-policia-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/policia-alcaldia-1200x800-e1507224817473.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro] 

###### [05 oct 2017] 

La Comisión Intereclesial de Justicia y Paz denunció que el 4 de octubre de 2017, la defensora de DDHH Erika Carvajal junto con tres víctimas del consejo de Jiguamandó y de la Comunidad de Autodeterminación Vida y Dignidad (CAVIDA), **fueron maltratadas por parte de agentes de la Policía Nacional**. Indicaron que éstos los acusaron de llevar drogas y dinero ilícito.

La Comisión indicó que, a un kilómetro antes del área urbana del municipio de Mutatá, en la carretera que conduce al mar, “unidades policiales de tránsito **retuvieron el vehículo de protección** de la UNP donde se movilizaban la defensora y las víctimas”. (Le puede interesar:["Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/))

Los policías, “en tono insultante y agresivo, en un principio se negaron a identificarse y **aseguraron que al interior del vehículo había drogas y dinero**”. Tanto la defensora como las víctimas indicaron que, después de haber sido interrogados y maltratados verbalmente, “se acercó el Subteniente Espinosa Sandoval de la Policía de Tránsito y Transporte de Urabá”, quien les dijo: “quién sabe ustedes a quién llevan ahí, hasta que no abran el carro acá, no los dejamos ir”.

Igualmente, denunciaron que los agentes de Policía, **desconocieron los derechos de los ciudadanos al buen trato** y no le permitieron a la defensora, “expresarse en protección de los derechos de las víctimas y de ella”. Ese mismo día, fueron sometidos a una requisa en la estación policial. (Le puede interesar: ["185 defensores de derechos humanos han sido asesinados en el último año"](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/))

La Comisión de Justicia y Paz, denunció que los agentes de tránsito **realizaron tratos hostiles** contra la defensora “a la vez que desconocieron su valor como mujer y su rol para la protección de los derechos de las víctimas que estaban atemorizadas”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
