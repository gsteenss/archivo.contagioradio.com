Title: En redes sociales señalan inconsistencias de Fiscalía tras atentado a la Escuela Gral Santander
Date: 2019-01-19 16:24
Author: AdminContagio
Category: Nacional, Política
Tags: Atentado, Escuela General Santander, Fiscal General de la Nación, Guillermo Botero, Ministro de defensa, Nestor Humberto Martínez
Slug: las-inconsistencias-de-la-fiscalia-detras-del-atentado-a-la-escuela-general-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/190118_02_InformeAtentadoEscuela-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilón] 

###### [19 Ene 2019] 

Tras las primeras versiones dadas a conocer por parte del ministro de defensa, Juan Guillermo Botero y el fiscal general, Néstor Humberto Martínez, sobre el atentado del pasado 17 de enero en la Escuela General Santander y responsabilidad del ELN,  bajo la autoria material de alias "Mocho Kiko", identificado como José Aldemar Rojas Rodríguez y la captura de Ricardo Carvajal Salgar, algunas denuncias en redes sociales han tenido eco entorno a "inconsistencia" en las pruebas que se han enseñado, **cuestionando la hipótesis del gobierno.**

### **"Alias el Mocho" había sido dado de baja por el Ejército en el 2016** 

Uno de los primeros interrogantes que se ha planteado, tiene que ver con la persona que se identifica como el autor material del atentado, debido a que alias "El Mocho", quien el Ministro de Defensa señaló como el responsable, se reportó como muerto en el año 2016 por el Ejército Nacional.

Medios de información como El Heraldo o la Silla Vacía han manifestado que se trata de dos personas diferentes, es decir que alias **"El Mocho" o "Franklin" en efecto fue abatido en el año 2016, mientras que alias "El Mocho" o "kiko"** sería el autor del atentado.

Otro hecho cuestionable es que Rojas habría prestado servicio militar en el Batallón Patriotas, en Honda Tolima, situación que no fue mencionada por Botero cuando se refirió al prontuario de alias "El Mocho Kiko".

![ejercito](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito-800x344.jpg){.alignnone .size-medium .wp-image-60137 width="800" height="344"}

### **El vehículo utilizado para el atentado** 

El fiscal Néstor Martínez, afirmó que el vehículo con el que se había cometido el acto terrorista fue plenamente identificado, logrando establecer que  José Aldemar Rodríguez era su último dueño y que entre los últimos trámites que él habría hecho se encuentra la expedición del soat y la revisión técnico mecánica.

Frente a estos hechos, el analista Víctor de Currea manifestó que este hecho es extraño, debido a que las guerrillas en el mundo **han operado bajo la lógica de obtener carros robados y falsificar documentos.**

### ** La velocidad con la que el vehículo ingresó a la Escuela General Santader** 

Durante la rueda de prensa en donde participaron tanto el Ministro de Defensa como el Fiscal General, se indicó que el automóvil había ingresado a gran velocidad por la entrada de vehículos de carga, aprovechando la apertura de las puertas. Inmediatamente después se informó a los demás guardias de la Institución.

Sin embargo, imágenes reveladas por Red+ Noticias indicarían que el vehículo no ingresó a gran velocidad a la Escuela General Santander, recorrió más de 100 metros al interior del lugar y no se habría dado una orden de alerta al resto de uniformados, debido a que se observa el carro pasando justo al lado de un batallón, **sin que estos rompan filas, como lo harían frente a una alerta**. ([Le puede interesar: vídeo de Red+ Noticias](https://www.youtube.com/watch?v=pSrWVyLv5To))

![WhatsApp Image 2019-01-19 at 4.37.45 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-19-at-4.37.45-PM-800x503.jpeg){.alignnone .size-medium .wp-image-60150 width="800" height="503"}

### **Las medidas del presidente Iván Duque** 

El presidente Duque expresó a través de una rueda de prensa, que la responsabilidad del atentado es de la guerrilla del Ejército de Liberación Nacional, ELN, razón por cual manifestó que tomó la decisión de levantar la suspensión de las ordenes de captura  a los 10 miembros del **ELN que integraban la delegación de ese grupo en Cuba, en donde se mantenía la mesa de diálogos de paz**.

Asimismo, le pidió a la autoridades de Cuba que realicen la captura de las personas que se encuentran allí para entregarlas a las autoridades colombianas, razón por la cual la cancillería de ese país señaló que "Cuba actuará en estricto respeto a los Protocolos del Diálogo de Paz firmados entre el Gobierno y el ELN",  el cual determina el curso de acción en caso de ruptura de la negociación.

![Bruno Rodríguez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Bruno-Rodríguez-581x157.jpg){.alignnone .size-medium .wp-image-60140 width="581" height="157"}

Frente a esas solicitudes, diversas figuras políticas como Iván Cepeda, Álvaro Leyva y organizaciones defensoras de derechos humanos, le pidieron al presidente que reconsidere su decisión y le recordaron que solamente una solución política y dialogada ha demostrado ser la herramienta más efectiva para acabar los conflictos armados en el país.

Hasta el momento no se conoce ningún comunicado de prensa por parte del ELN en donde reconozca su responsabilidad en el atentado, mientras que sus cuentas de Twitter han sido eliminadas de la red social. (Le puede interesar:["Comunidades le piden a Duque retomar diálogos con el ELN y llevar paz a los territorios")](https://archivo.contagioradio.com/comunidades-le-piden-a-duque/)

###### Reciba toda la información de Contagio Radio en [[su correo]
