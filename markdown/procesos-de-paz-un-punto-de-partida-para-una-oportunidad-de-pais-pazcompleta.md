Title: Procesos de paz "un punto de partida para una oportunidad de país" #PazCompleta
Date: 2016-03-31 17:03
Category: Nacional, Paz
Tags: Dialogos en Colombia, ELN, FARC, Paz Completa
Slug: procesos-de-paz-un-punto-de-partida-para-una-oportunidad-de-pais-pazcompleta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Paz-Completa-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Paz Completa ] 

##### [31 Mar 2016]

[['Por una Paz Completa'](https://archivo.contagioradio.com/parte-hacia-arauca-la-caravana-por-una-paz-completa/)] es la campaña que han liderado organizaciones eclesiales, iglesias, líderes y políticos de distintas regiones, entre ellas Cauca, Arauca, Putumayo, Boyacá y Casanare, para contribuir al establecimiento de los procesos de negociación con las dos insurgencias vigentes en Colombia, buscando que no se cometiera "el error histórico de fraccionar el proceso de paz y dejar por fuera al ELN", dada la necesidad y relevancia de abrir una mesa de negociaciones con esta guerrilla, para **aportar al camino de la paz, la solución al conflicto armado y la reconciliación nacional**, según afirma Katherine Torres, integrante de la Campaña.

Frente al [[anuncio del inicio de la fase pública](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)] de conversaciones Gobierno - ELN, Torres asegura que la sociedad civil debe rodear y apoyar este proceso de la misma manera en que lo ha hecho con el de las [[FARC](https://archivo.contagioradio.com/delegacion-de-paz-farc-desmiente-que-haya-crisis-en-proceso-de-paz/)], así mismo, extiende desde la Campaña el agradecimiento a **la disposición y el apoyo brindado por la comunidad internacional** que ha jugado un papel muy importante y protagónico, tanto en los momentos de apogeo como en los tiempos de crisis por los que ha pasado la Mesa de Diálogos de La Habana.

"Sabemos que no se van a resolver todos los temas que tienen que ver con las causas estructurales del conflicto en esta Mesa de Negociación con el ELN, pero sabemos que **es un punto de partida para una oportunidad de país**", afirma Torres, e insiste en que es posible que se discutan en la Mesa las causas del conflicto armado, para que se generen las transformaciones necesarias que lleven a la paz, lo que implica que la sociedad colombiana "tenga paciencia" porque es un proceso que "requiere tiempo" para que lo que se pacte se cumpla.

<iframe src="http://co.ivoox.com/es/player_ej_10999791_2_1.html?data=kpWmm56bfZKhhpywj5WaaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncazV1crfy9PJb7Xj09fS1ZCJdpjE0NeY19PFb9HV25DQ0dLUsMbowoqfmZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
