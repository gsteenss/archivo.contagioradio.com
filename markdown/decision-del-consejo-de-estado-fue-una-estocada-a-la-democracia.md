Title: Decisión del Consejo de Estado es "una estocada a la democracia"
Date: 2015-09-24 18:27
Category: Animales, Nacional, Voces de la Tierra
Tags: Alcaldía de Bogotá, Bogotá, Consejo de Estado, Consulta Antitaurina, corridas de toros, Maltrato animal, Martha Lucía Zamora, Natalia Parra, Secretaria de Gobierno
Slug: decision-del-consejo-de-estado-fue-una-estocada-a-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG_9p68ht.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [24 Sept 2015] 

Tras la decisión del Consejo de Estado de suspender la Consulta Popular para dar la posibilidad de que la ciudadanía demuestre su hay o no arriago mayoritario frente a las corridas de toros en Bogotá, la Secretaria de Gobierno, Martha Lucía Zamora, calificó como **“una estocada a la democracia”** esta decisión.

Durante la rueda de prensa de esta tarde desde la Alcaldía de Bogotá, la secretaria de gobierno, aseguró que la consulta antitaurina cumplió con todos los requerimientos legales y constitucionales, sin embargo, debido a las 16 acciones de tutela presentadas por los taurinos, la consulta fue suspendida, lo que **para Zamora, significa que la democracia en el país es muy frágil.**

Según el fallo, "el Tribunal Administrativo violó el derecho fundamental al debido proceso de los accionantes, por cuanto **desconoció de manera flagrante el precedente constitucional,** que definió expresamente que en el Distrito Capital está habilitada la actividad de la tauromaquia. En consecuencia, a partir de esas decisiones, es claro que, salvo que el legislador lo prohíba, no es dable acudir a la consulta para que, por una decisión mayoritaria, se determine si se puede o no practicar el referido espectáculo en la edificación dispuesta para el efecto: la plaza de toros de Santamaría”.

Por su parte, Natalia Parra, directora de la Plataforma ALTO, indicó que los taurinos no representan ningún tipo de minoría constitucional, por el simple hecho de que 'les gusten las corridas de toros'.

"**Las verdaderas minorias como las raciales, etnicas y sexuales, son grupos de millones, si se analiza los grupos taurinos, son pocos pero muy poderosos, lo cual los quita de tajo de la tipología de minoría constitucional",** dijo Parra.

Martha Lucía Zamora, señaló que el paso a seguir será presentar una **acción de revisión del fallo.** Así mismo, las organizaciones defensoras de animales **pedirán una solicitud de nulidad.**

Así mismo, Natalia Parra, afirmó que “l**a consulta vive y la lucha sigue”**, teniendo en cuenta que el próximo 18 de octubre, se realizará en Bogotá, una gran marcha por la defensa de los derechos de los animales, pero también por la defensa de la democracia.
