Title: Centro de Memoria, Paz y Reconciliación realizará informe de las protestas de septiembre
Date: 2020-10-29 18:12
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Centro de Memoria Paz y reconciliación, Claudia López, Protestas contra abuso policial
Slug: centro-de-memoria-paz-y-reconciliacion-realizara-informe-de-las-protestas-de-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/ESMAD-durante-protestas-de-septiembre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio/Andrés Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de una sesión pública, la alcaldesa Claudia López solicitó al director del [Centro de Memoria, Paz y Reconciliación](http://centromemoria.gov.co/), José Antequera, **realizar un informe de lo ocurrido en las jornadas de protestas, entre el 9 y 11 de septiembre, después de que se conociera el asesinato del abogado Javier Ordóñez a manos de dos policías.** (Le puede interesar: [127 heridos y 13 asesinadas en medio de represión a protestas contra abusos policiales](https://archivo.contagioradio.com/mas-de-12-personas-heridas-y-dos-personas-asesinadas-en-medio-de-protestas-contra-accionar-de-fuerza-publica/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Claudia López señaló que es deber de la alcaldía documentar como parte de la memoria histórica lo que ocurrió durante esos tres días, las violaciones contra los derechos de la ciudadanía y demás actos violentos que resultaron en la muerte de 14 personas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Yo no me voy a ir de esta alcaldía sin que haya un informe de verdad, de qué ocurrió... ¿Qué ocurrió esos tres días?. Por eso te pido que crees un informe de memoria histórica de qué fue lo que pasó, de las diferentes fuentes, voces y percepciones.
>
> <cite>Alcaldesa Claudia López</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Además, agregó que no será un informe judicial sino una memoria histórica de lo que pasó. «Es un deber moral y ético con la ciudad, las familias y los 75 ciudadanos que fueron heridos a bala». Asimismo lo indicó Antequera a través de su twitter, explicando que sería un informe «por la verdad, la justicia, la reparación y la memoria».

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Antequerajose/status/1321570937251266560","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Antequerajose/status/1321570937251266560

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El director del Centro también agregó que ya se están realizando alianzas con el Instituto Distrital de la Participación y Acción Comunal -IDPAC- y con la Dirección de Derechos Humanos de Secretaria Gobierno para obtener información de los hechos. (Le puede interesar: [Así avanza la peregrinación por la Vida y por la Paz](https://archivo.contagioradio.com/corte-suprema-ordena-a-min-defensa-ofrecer-disculpas-por-exceso-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, indicó que se está desarrollando una propuesta para presentar una exposición temporal el próximo 10 de diciembre, la cual permitiría «hacer apertura al diálogo».

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
