Title: ESMAD agrede a campesinos que se oponen a multinacional petrolera en Caquetá
Date: 2019-08-30 18:47
Author: CtgAdm
Category: Comunidad, Nacional
Tags: agresión ESMAD caquetá, Caquetá, explotación de petróleo
Slug: esmad-agrede-a-campesinos-que-se-oponen-a-multinacional-petrolera-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Caquetá-ESMAD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Coordosac] 

Desde el pasado 18 de agosto, comunidades de **Fragita, Zabaleta y Yurayaco en Caquetá** se encuentran en una jornada de protesta pacífica contra la petrolera Gran Tierra Energy, que escoltada por el Escuadrón Móvil Antidisturbios (ESMAD) y el Ejército busca ingresar maquinaria pesada al territorio, no solo reprimiendo a los campesinos que exigen la salida de las multinacionales petroleras de la región sino, según líderes de la región afectando las fuentes hídricas del territorio.

Rigoberto Abello, integrante de la Coordinadora Departamental de Organizaciones Sociales, Ambientales y Campesinas del Caquetá (Coordosac) explica que desde hace seis años, Gran Tierra Energy  ha tenido a su disposición la explotación de hidracarburos en Piamonte, Cauca y ahora que buscan la ampliación del Pozo Miraflor 1 del Bloque Santana, han movilizado maquinaria pesada sin la aprobación de las comunidades.

La vía que conduce de San José del Fragua a Piamonte es un tramo que pese a ser una vía nacional no ha sido intervenida, por lo que han sido las propias comunidades las que han construido y mantenido una carretera que resiste un peso de 20 toneladas y por la que **ahora 40 tractomulas y 10 volquetas con un promedio de 36 toneladas buscan ingresar a la región. ** [(Lea también: "La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo )](https://archivo.contagioradio.com/la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo/)

### ESMAD y Ejército acompañan a la petrolera 

Según relata Abello, el Ejército y el ESMAD han acompañado y escoltado el paso de las maquinarias que actualmente están detenidas por el accionar de las comunidades, frente a ello, la Fuerza Pública ha ingresado a las veredas, atacando a sus habitantes con gases y repetidoras dejando como resultado una persona herida y a algunas aturdidas por los gases. [(Lea también: ESMAD agrede a comunidades que se habían acogido a sustitución de cultivos de uso ilícito)](https://archivo.contagioradio.com/esmad-agrede-a-comunidades-que-se-habian-acogido-a-sustitucion-de-cultivos-de-uso-ilicito/)

Además del problema causado a las vías, la militarización y la exclusión de las comunidades que han rechazado la explotación petrolera en el lugar, también denuncian que mientras transitan, los vehículos estarían derramando material  y sustancias tóxicas, pasando por cuerpos de agua, única fuente hídrica de las veredas aledañas que no cuentan con acueducto, **afectando a cuatro núcleos comunales que reúnen a cerca de 40 o 30 veredas afectadas y un estimado de 50 o 60 familias.**

### Negligencia por parte de las Alcaldías

Abello denuncia que por parte de la Alcaldía de San José del Fragua se han dado reuniones con esta empresa **"haciendo acuerdos a espaldas de las comunidades**", por lo que se ha optado por acudir al ESMAD y al Ejército afirmando que este territorio ya está concesionado.

Mientras que desde la Alcaldía de Piamonte, señala el integrante de Coordosac, opinan que el esfuerzo de las comunidades por proteger las vías y el ambiente, "es oponerse al desarrollo de la región", una opinión que se ve respaldada por la Gobernación de Caquetá ya habría pactado la consecución de más convenios petroleros en siete municipios del departamento.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_40652321" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40652321_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
