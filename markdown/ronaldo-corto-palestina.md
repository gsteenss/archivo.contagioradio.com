Title: Ronaldo, un cortometraje que plasma la realidad de la niñez en Palestina
Date: 2017-08-19 12:00
Category: Onda Palestina
Tags: BDS, futbol, Israel, Palestina
Slug: ronaldo-corto-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Ronaldo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ronaldo Corto 

###### 19 Ago 2017 

Queremos hablar de un curioso pero a la vez estremecedor cortometraje elaborado por un equipo de animadores parisinos. Ese corto se llama **Ronaldo**, e intenta plasmar la cotidianidad de **miles de niñas y niños palestinos ante la sombra de la constante amenaza israelí**.

A pesar que el corto fue estrenado los primeros días de julio de este año, se ve clarísima **la dolorosa referencia a la masacre de los cuatro niños en las playas de la Franja de Gaza**, ocurrida el 16 de junio de 2014. (Le puede interesar: [Pese a bloqueo de jugadores por parte de Israel, Gaza gana la copa Palestina de fútbol](https://archivo.contagioradio.com/deporte-palestina-israel/))

Bilel Allem, principal productor del filme, aclara que llamó al corto así d**ebido a que el jugador portugués del Real Madrid ha empezado a tener mucho protagonismo pasivo en las calles de Palestina**, siendo modelo de muchos jóvenes.

Lo que más llama la atención de este conmovedor cortometraje es ver como la imaginación de los miles de niñas y niños palestinos es **capaz de derribar toda limitante para ganar su partido**: En el corto se muestra como corren entre la gente, entre las trochas, los barrios desordenados, escombros, y finalmente llega al muro, donde se queda fijo en los graffitis porque cree que son reales…. ¡Y ve que lo llaman a cruzar el muro!

A continuación, les invitamos a ver el corto.  
<iframe src="https://www.youtube.com/embed/pfKFsWwsBIA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio.
