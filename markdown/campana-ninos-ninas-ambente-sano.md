Title: Lanzan campaña por el derecho de las niñas y niños a un ambiente sano
Date: 2020-10-29 17:45
Author: AdminContagio
Category: yoreporto
Tags: Ambiente, sano
Slug: campana-ninos-ninas-ambente-sano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/ambiente-sano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La organización de derechos de niñas, niños y adolescentes Terre des Hommes Alemania lanza hoy la campaña internacional \#MiPlanetaMisDerechos, en la que insta a los gobiernos a reconocer el derecho de niñas y niños a un ambiente sano y sostenible, mediante la adopción de una resolución de Naciones Unidas y un Protocolo Facultativo de la Convención de las Naciones Unidas sobre los Derechos del Niño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como parte de la campaña, Terre des Hommes Alemania, sus copartes y la red International de jóvenes están iniciando una petición mundial en línea que se entregará al Secretario General de las Naciones Unidas y al Presidente del Comité de los Derechos del Niño de las Naciones Unidas durante la Asamblea General en 2021 para instar a los Estados Miembros a  
tomar acción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se llevarán a cabo proyectos dirigidos por jóvenes en casi 40 países, incluyendo 10 países de Latinoamérica, para promover la campaña y realizar un llamado mundial a la firma de la petición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este ambicioso llamado a la acción llega en un momento en que las vidas de los niños y niñas corren un riesgo crítico. La crisis medioambiental, uno de los principales impulsores de COVID-19, está obligando a millones de niños y niñas a abandonar sus hogares, a no ir a la escuela, a vivir en la pobreza y a poner en riesgo su vida. A medida que nos acercamos a un punto de inflexión ecológico irreversible, muchas más vidas se verán amenazadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo, hay una ventana de oportunidad para un cambio a gran escala. Las niñas, los niños y jóvenes continúan impulsando fuertemente la adopción de acciones contra el cambio climático en uno de los mayores movimientos sociales mundiales de la historia. Los gobiernos que trabajan en sus planes de recuperación de COVID-19 tienen la oportunidad de  
poner a las niñas y los niños en el centro de la construcción de un futuro más sostenible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En septiembre pasado, varios gobiernos se comprometieron públicamente a liderar el reconocimiento del derecho humano a un medio ambiente sano.  
"Los niños y niñas vivencian cómo la crisis climática y el deterioro de los ecosistemas destruyen sus oportunidades de un mejor futuro. Por eso queremos que nuestra campaña garantice que el Derecho de las niñas y los niños a un Medio Ambiente Sano y Sostenible se consagre en un protocolo adicional a la Convención de las Naciones Unidas sobre los  
Derechos del Niño", dijo Birte Kötter portavoz de la junta directiva de Terre des Hommes.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Esto obligaría a todos los estados signatarios a centrar sus políticas medioambientales con más fuerza en el bienestar y la participación de los niños y las niñas. A la vez, marcaría una nueva era para las oportunidades de vida de las generaciones futuras".  
> “Los niños y las niñas no causaron la crisis del medio ambiente y el cambio climático, sin embargo, tienen que asumir sus consecuencias por más tiempo. Todas las decisiones políticas tienen que reflejar el interés superior de la niñez, por eso, estamos exigiendo el Derecho a un Ambiente Sano y Sostenible”
>
> <cite>Thomas Mortensen, Coordinador de Terre des Hommes Alemania para América Latina.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

  
Enlace a la campaña \#MiPlanetaMisDerechos y petición online: http://www.my-planet-myrights.org/es/

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contacto de prensa: Julio Antolinez +57 3184017770 / Sara Ramirez + 57 3007489535 miplanetamisderechos@tdh-latinoamerica.de  
  
www.tdh-latinoamerica.de  

<!-- /wp:paragraph -->
