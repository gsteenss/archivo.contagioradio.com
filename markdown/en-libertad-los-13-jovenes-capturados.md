Title: Ordenan libertad a los 13 jóvenes capturados en Bogotá
Date: 2015-09-11 16:50
Category: DDHH, Nacional
Tags: 13 jóvenes, Atentados en Bogotá, congreso de los pueblos, Jovenes capturados en Bogotá, libertad a jovenes, Naciones Unidas
Slug: en-libertad-los-13-jovenes-capturados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/los-13-jovenes-detenidos1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [11 Sep 2015] 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Manuel-Garzon.mp3"\]\[/audio\]

###### [Manuel Garzón, Abogado] 

Los 13 jóvenes capturados por la realización de una protesta estudiantil, serían dejados en libertad, luego de que se declarara ilegal la captura por sobrepasar vencimiento de términos, puesto que el procedimiento se realizó después de cumplidas las primeras 36 horas de la captura.

El juzgado 44 del circuito, revocó la medida de aseguramiento por sobrepasar los términos establecidos en la ley, sin embargo en este momento se realiza la audiencia de segunda instancia en contra de la medida de aseguramiento, se espera que en los próximos minutos se decida si los jóvenes quedan en libertad este mismo viernes.

Los jóvenes fueron capturados el pasado 8 de Julio y desde ese momento la movilización social y la solidaridad de diversas organizaciones, así como el seguimiento al caso por parte de la Oficina de las Naciones Unidas y otras instituciones han logrado que se actúe en derecho y se revoquen las medidas tomadas en primera instancia.

Leer: [Estos son los perfiles de jóvenes capturados en Bogotá ](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/)

Noticia en desarrollo...
