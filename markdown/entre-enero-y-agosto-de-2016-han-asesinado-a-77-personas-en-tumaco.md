Title: Entre enero y agosto de 2016 han asesinado a 77 personas en Tumaco
Date: 2016-08-30 17:28
Category: DDHH, Nacional
Tags: Asesinatos, Ejército Nacional, indígenas, Tumaco
Slug: entre-enero-y-agosto-de-2016-han-asesinado-a-77-personas-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Awa-e1472595876270.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El que piensa gana 

###### [30 Ago 2016] 

Por lo menos 4 indígenas Awá han sido asesinados en los últimos días en el departamento de Nariño, pese a las medidas cautelares y tutelas en favor del pueblo indígena que debería estar protegido por el Estado colombiano. Lo que evidencia un total de **77 personas asesinadas entre enero y agosto de 2016,** según cifras de la Pastoral Social de la Diócesis de Tumaco.

El primer asesinato fue el pasado 26 de agosto, cuando **el líder indígena awá Camilo Roberto Taicus Bisbicusm,** salió hacia el casco urbano de Tumaco y cuando regresaba a su comunidad un taxi y dos motos lo detuvieron y le dispararon. “Además de ser líder era un docente, un compañero que nos ayudó mucho en el proceso organizativo, por eso desde el viernes estamos en asamblea permanente”, cuenta Rider Pai, Consejero Mayor de la Unidad Indígena del Pueblo Awá, UNIPA.

El lunes, cuando la comunidad se encontraba homenajeando a Taicus, **los indígenas Alberto y Luciano Pascal García,** quienes se desplazaban desde el municipio de Llorente hasta el resguardo Palangala- Hojal La Turbia, fueron también víctimas de un ataque, cuando una moto con dos personas los persiguió y luego les disparó.

Más tarde, sobre las 11 de la mañana del mismo lunes, fue asesinado **Diego Alfredo Chirán Nastacuas** quien apareció con signos de tortura y siete impactos de bala en La María municipio La Barbacoa, a pocos metros de donde está ubicado el Ejército Nacional.

Rider Pai, asegura que en los últimos días, la comunidad ha sido objeto de amenazas por medio de panfletos, que de acuerdo con la **Diócesis de Tumaco, estarían firmados por las “AUC”**.

Los  cuerpos se encuentran en las respectivas sedes municipales del Instituto de Medicina Legal, mientras  las comunidad Awá espera que la **Fiscalía General de la Nación, la Alta Consejería Presidencial para los Derechos Humanos,** entre otras autoridades investiguen y esclarezcan los hechos, y asimismo se tomen las medidas necesarias para enfrentar esas situaciones que tienen en riesgo al Pueblo Awá.

<iframe src="http://co.ivoox.com/es/player_ej_12720132_2_1.html?data=kpeklJWVd5Ohhpywj5aZaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncbPdxcrfjbXFrYampJCw0dPXqcvZ09SYr8bds9OfxcqYzsaPmc_dxcbRja7SqIa3lIqupszJssKfxcrZjbWRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
