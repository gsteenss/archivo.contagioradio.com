Title: Plan nacional de desarrollo 2015-2018 supone retroceso de 100 años en DDHH
Date: 2015-02-18 22:33
Author: CtgAdm
Category: Economía, Nacional
Tags: Ley 133 sobre terrenos Baldíos, Ordenamiento territorial, Oxfam, paz, Plan Nacional de Desarrollo Colombia 2014-2018
Slug: plan-nacional-de-desarrollo-2015-2018-supone-retroceso-de-100-anos-en-ddhh
Status: published

###### Foto:Contagioradio.com 

El martes 17 de febrero se realizó una rueda de prensa convocada por distintas organizaciones sociales como Oxfam, CCJ, CINEP, Cumbre agraria, CODHES, y Dignidad Agropecuaria entre otras, para poder **evaluar riesgos y alcances del PND**.

En la rueda de prensa participaron los ponentes: la directora de **Oxfam** Aída Pesquera, Jhenifer Mojica de la CCJ, los senadores Alberto Castilla y Jorge Robledo, los representantes Inti Asprilla, Alirio Uribe, Oscar Gutierrez de dignidad Agropecuaria, y el  analista económico y político Aurelio Suárez y Sergio Coronado del CINEP.

Según los ponentes, la problemática está en que el PND** deroga mas de 10 leyes e introduce muchas otras, por ejemplo la ley 133 sobre terrenos baldíos** que legaliza el acaparamiento por parte de grandes productores y de multinacionales la producción agrícola nacional. Además la minería socava la forma tradicional realizada por las familias colombianas en varias regiones del país.

Los analistas concluyen que el PND es una amenaza contra la paz, no solo porque retrocede en los derechos del campesinado, sino también por las implicaciones que tiene en materia de restitución de tierras puesto que las que están en manos de campesinos  van a pasar a manos de multinacionales.

En conclusión, para los ponentes el PND es la** facilitación del gobierno a las multinacionales para que puedan sacar el mayor beneficio** en detrimento del que va a sacar el pueblo colombiano.
