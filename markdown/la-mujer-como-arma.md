Title: La mujer como arma
Date: 2015-06-16 11:03
Category: Eleuterio, Opinion
Tags: Aixa G, Bahira Abdulatif, Burundi, guerra, mujer, mujer en el conflicto, violaciones
Slug: la-mujer-como-arma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/sobreviviente-Bosnia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Planeta mujer 

#### [Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/) 

###### [16 Jun 2015] 

Cuando hablamos con mujeres cuyos países han vivido situaciones de guerra, nos encontramos, como no puede ser de otro modo, con testimonios de gran dureza. Una de las denuncias que se repiten y que les afecta directamente a ellas, es la cruel y miserable práctica de la violación como arma de guerra. Esta atrocidad, que hunde sus orígenes en tiempos tan inmemoriales como el propio machismo, se sigue empleando en las guerras de hoy.

[Lo hemos encontrado en la última invasión de Irak, como explicaba la profesora Bahira Abdulatif:]{.s2}*“En esa ideología bélico-machista, la mujer se considera como representante de una cultura, como portadora del honor familiar y del de un país. Violar, ultrajar y asesinar a una mujer es una forma de agresión y humillación al adversario.”*

[También Iris M. nos relataba la experiencia en las guerras de Burundi en los años 90:]{.s3}[ *“*]{.s2}*La violación* *se aplicaba sistemáticamente como un arma de guerra; si quieres mermar a un hombre vas a por su mujer, ella representa su honor, su intimidad. Esta funesta práctica, también funcionaba estratégicamente, ya que obliga al enemigo a retroceder para proteger a sus mujeres.”*

Dicen que en la guerra todo está permitido y esto provoca que situaciones anormales se conviertan en cotidianas, como la violación y el asesinato. Sin embargo hay cotidianidades lejanas a la situación de guerra abierta en las que la utilización de la mujer como un medio para lograr objetivos, es también común. A veces el objetivo es sencillamente crear terror. El terror paraliza a las personas, permite la impunidad y fomenta la sumisión.

Desde Argentina la compañera Aixa Gª, activista por los derechos de la mujer, publica prácticamente a diario fotografías de las niñas y mujeres desaparecidas en su país. Tal vez el caso más paradigmático en este sentido es el que ocurre en la tristemente conocida Ciudad de Juárez, allá en la frontera mexicana con Estados Unidos. Las causas de los feminicidios y desapariciones que allí suceden desde hace décadas, son bien oscuras. Sin embargo hay quien apunta a esa voluntad de causar terror en la población.

Desde el Estado de México, pegado al Distrito Federal de la capital del país, los feminicidios también suenan de forma atroz. La tasa es de las más altas en México. Allí tampoco hay guerra pero para muchas el día a día puede ser de una tensión similar. El crimen organizado del que no se excluyen dirigentes políticos, agranda su sombra de poder con la constante aparición de mujeres muertas y violadas encontradas en plena calle, a la vista de todos. ¿Quién se atreverá a denunciarles, a combatirles, a resistirles? ¿Qué hombre no tiene una hermana, una esposa, una hija? ¿Qué mujer se expondrá tanto sabiendo de lo que son capaces? Con todo, siempre hay resistencia. Las vecinas se turnan en grupos de cinco para ir a recoger a sus hijas a la escuela.

En Burundi Iris M. trabajó con mujeres de su país que habían sufrido la violación de los soldados. Fueron ellas mismas quienes lograron cambiarle los esquemas. “S*e negaban a considerarse como víctimas, se llamaban así mismas supervivientes. Todo un cambio de perspectiva que les permitía poder mirar por su futuro y el de sus propias hijas”.*
