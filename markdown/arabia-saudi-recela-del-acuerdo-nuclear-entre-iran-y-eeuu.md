Title: Arabia Saudí recela del acuerdo nuclear entre Irán y EEUU
Date: 2015-05-11 15:42
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Acuerdo nuclear Iran-EEUU, Anwar Gargash, Arabia Saudí, Arabia Saudí en contra acuerdo nuclear con Irán, Bahrein, Consejo de Cooperación del Golfo, Emiratos Árabes Unidos, Kuwait, Omán, primavera árabe, Qatar, Rey Salaman de Arabia Saudí planta a EEUU
Slug: arabia-saudi-recela-del-acuerdo-nuclear-entre-iran-y-eeuu
Status: published

###### **Foto:News.yahoo.com** 

En la Cumbre del miércoles del **Consejo de Cooperación del Golfo** no acudirá el rey Salman de Arabia saudí, esta ausencia se interpreta como un desplante por parte de Arabia Saudí a **Barack Obama** por el **acuerdo nuclear entre** **Irán y EEUU.**

Arabia Saudí no está totalmente de acuerdo con el acuerdo nuclear con Irán,  no tanto por la posibilidad muy poco real de que el país persa pueda tener un abomba atómica, sino por la fuerte presencia de este en Irak o Siria y por su influencia en el mundo árabe desde la conocida como ¨**Primavera Árabe**¨.

Además el apoyo de Irán a Hezbolá en Libano y a los Huthi en Yemen son considerados por los Sauditas como una intromisión en su área de influencia. El problema radica en la expectación que produce el acuerdo que en un futuro permitiría a la **República Islámica poder expandirse** en la región apoyando las comunidades **Chiítas en Bahrein** o al este de la propia Arabia Saudí.

En el trasfondo está el deterioro entre las relaciones de EEUU y Arabia Saudí, en el primer caso por el apoyo de los Sauditas a los crecientes grupos islamistas radicales como el **Estado Islámico o el ISIS** y en el segundo de los casos por la permisividad de EEUU con el régimen Sirio y la poca intervención que ha realizado este en la crisis de Egipto dejando caer al gobierno de Mubarack.

En la reunión donde deberían acudir altos representantes de **Omán, Kuwait, Emiratos Árabes Unidos, Bahrein, Arabia Saudí  y Qatar**, solo acudirán con representación de alto nivel Kuwait y Qatar.

Los demás países también recelan del acuerdo iraní y Omán, Bahrein y Emiratos Árabes Unidos tampoco enviarán a su representación máxima sino a delegados  en un acto de desconfianza hacía la política de EEUU en la región.

[Según **Anwar Gargash**, ministro de exteriores de Emiratos “…El principal dilema en las relaciones entre Irán y los árabes es el deseo Iraní de expandirse… El acuerdo nuclear entre Occidente e Irán ha creado una nueva dinámica…”.]{lang="ES"}

[Jhon Kerry se reunió con representantes de los países del Golfo para explicarlos sobre la construcción de un futuro acuerdo para defender a los países miembros de una posible injerencia de Irán. Pero la defensa Estadounidense actualmente es contraria a intervenciones y esto ha creado desconfianza entre los países árabes.]{lang="ES"}
