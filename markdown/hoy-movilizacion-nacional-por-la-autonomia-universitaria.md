Title: Estudiantes y profesores de todo el país defienden la autonomía universitaria
Date: 2015-11-11 14:00
Category: Educación, Nacional
Tags: ‪#‎El11YoMeMovilizo‬, calidad en la educación colombiana, gina parody, Ministerio de Eduación, Movilización 11 de noviembre de 2015, Movimiento estudiantil Colombia, Resolución Licenciaturas MEN 2014, Universidad Pedagógica Nacional de Colombia
Slug: hoy-movilizacion-nacional-por-la-autonomia-universitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Estudiantes-y-profesores-defienden-la-autonomía-universitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo a la carga 

##### <iframe src="http://www.ivoox.com/player_ek_9368158_2_1.html?data=mpijmpaZfI6ZmKiak5WJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo1snWw9PYqdSf2pDd1NTKqdTj08rgjcnJqsrZz8nS0JDQpYzV1tnc0NTRaaSnhqaxw5DZssrqxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[José Gaitán, estudiante Universidad Pedagógica Nacional] 

###### [12 Nov 2015]

[En el marco de la discusión del proyecto de resolución presentado por el Ministerio de Educación Nacional para reformar los programas de licenciaturas, **estudiantes y profesores de todo el país se movilizan** **para exigir** escenarios de interlocución en la construcción de política pública de formación docente, **respeto a la autonomía universitaria**, dignificación de esta profesión y **mayor financiación de la educación pública**. ]

[El principal motivo según profesores y estudiantes de diversas universidades colombianas, radica en que este proyecto de resolución presentado en 2014 y que pretende modificar las 457 licenciaturas ofertadas en las universidades tanto públicas como privadas del país, se ha promovido **sin** **la** **participación activa y efectiva de las comunidades universitarias** y en detrimento de la autonomía universitaria consagrada en el artículo 28 de la Ley 30 de Educación Superior de 1992.]

[De acuerdo con estudiantes de la Universidad Pedagógica Nacional, pese a que el presidente Juan Manuel Santos y la ministra de educación Gina Parody han anunciado que esta iniciativa pretende mejorar la calidad de la educación superior, este proyecto no presenta soluciones para saldar la **deuda del Estado con las universidades públicas.**]

[José Gaitán  líder por el derecho educación y el movimiento estudiantil hace un llamado al presidente a que **"reconozca el déficit de 16 billones de pesos que tiene con las 32 universidades públicas, sólo de esta manera se podría avanzar en términos de calidad". **]

[Otras de las críticas tienen que ver con las **directrices que propone el proyecto de resolución** para los programas de formación docente que las universidades deben ofertar, sus denominaciones, énfasis y componentes, indicando además la organización de las actividades académicas y el número de créditos para cada una, según estudiantes y profesores "**obedeciendo a lógicas de mercado** y de 'calidad' dictadas por organismos internacionales como el Banco Mundial". ]

["Nosotros queremos apelar ese proyecto de resolución y **si nos toca seguir empapando las calles de dignidad y si nos toca cerrar los edificios de las entidades académicas lo vamos hacer** todo en pro de que se respete la autónoma universitaria" concluye el estudiante José Gaitán.]

https://www.youtube.com/watch?v=xIuN7zZ978I
