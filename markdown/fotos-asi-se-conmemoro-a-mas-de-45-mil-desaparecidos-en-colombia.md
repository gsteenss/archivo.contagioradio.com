Title: Fotos: Así se conmemoró a más de 45 mil desaparecidos en Colombia
Date: 2015-08-31 14:17
Author: ContagioRadio
Category: Nacional, Sin Olvido
Tags: 30 de agosto, detenidos desaparecido, Día de las Víctimas de Desaparición Forzada
Slug: fotos-asi-se-conmemoro-a-mas-de-45-mil-desaparecidos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparecidos-7-800x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: MOVICE y Contagio Radio] 

Para conmemorar el Día Internacional de las Víctimas de la Desaparición Forzada en Colombia, organizaciones de víctimas y de derechos humanos, realizaron diversas jornadas de movilización y otras actividades culturales con el fin de mantener en la memoria a los más de 45 mil desaparecidos.

En Cali, Medellín, Bogotá, Bucaramanga y Villavicencio, los familiares de desaparecidos salieron a las calles a marchar, así mismo se realizaron conciertos y galerías de la memoria para conmemorar el 30 y 31 de agosto el Día Internacional de Víctimas de la Desaparición Forzada.

 
