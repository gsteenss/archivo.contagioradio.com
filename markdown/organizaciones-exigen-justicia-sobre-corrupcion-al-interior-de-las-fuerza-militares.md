Title: Organizaciones exigen justicia sobre corrupción al interior de las Fuerza Militares
Date: 2018-04-25 13:06
Category: DDHH, Política
Tags: carta de las organizaciones, corrupción fuerzas militares, desvíos de dinero, Fuerzas militares
Slug: organizaciones-exigen-justicia-sobre-corrupcion-al-interior-de-las-fuerza-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/fuerzasmilitares543-e1524675574963.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [25 Abr 2018] 

Luego de las revelaciones sobre la corrupción al interior de las Fuerzas Militares, plataformas de derechos humanos, organizaciones sociales a nivel nacional e internacional al igual que comunidades de fe y congresistas de la República, enviaron una carta donde solicitaron a las autoridades que se **investiguen los hechos**, se proteja a los testigos y que se garanticen las medidas de no repetición.

La carta afirma que “debido a las graves y contundentes pruebas reveladas, la investigación emprendida por la Procuraduría General de la Nación, debe llegar hasta sus máximos responsables”. Esto implica investigar al **General Juan Pablo Rodríguez** Barragán “a quien el Presidente de la República debería apartar de su cargo diplomático” en Corea del Sur y al **General Martín Nieto**, que ocupó la Jefatura de Inteligencia y Contrainteligencia Militar Conjunta del Comando General de las Fuerzas Militares.

### **Estos son los hechos de corrupción por los que envían la carta** 

Según la revista Semana, entre el 2013 y el 2017, el Comando General de las Fuerzas Militares **realizó desvíos de recursos** que terminaron en los bolsillos de algunos militares y fueron también usados para comprar equipos tecnológicos con objetivos que no se han clarificado aún.

Estas actividades las habría realizado, a través de varias compañías, la **Regional de Inteligencia Militar Estratégica Conjunta** adscrita al Comando General de las Fuerzas Militares. Las compañías tenían nombres como Huila, Neiva, Espinal y Villavicencio y operaban desde Bogotá y Huila; era la unidad especializada en labores de ciberdefensa y ciberataques. (Le puede interesar:["Fuerzas militares colombianas al servicio de las empresas extractivistas"](https://archivo.contagioradio.com/estamos-ante-una-privatizacion-de-las-ffmm-al-servicio-de-las-multinacionales/))

Fue desde estas compañías, que recibían recursos para sus operaciones por parte del Comando Conjunto de inteligencia, **donde realizaron los desvíos**. Después de recibir los recursos depositaban una cantidad específica en lo que llamaron “el ahorro”. Fue con ese dinero, que ascendió a los 20.000 mil millones de pesos, con los que se pagaban gastos personales de generales y oficiales.

Estas compañías respondían a los generales **Rodríguez y Nieto** quienes además organizaron los desvíos haciendo alusión a fuentes que debían recibir un pago y así legalizaron los trámites. Con el dinero realizaron pagos de abogados para los militares sindicados de cometer delitos de falsos positivos, así como la compra de aparatos tecnológicos que tenían la capacidad de acceder a información personal de políticos, periodistas y personas de la sociedad civil.

### **Organizaciones, movimientos y congresistas piden que se investiguen los hechos** 

Específicamente, quienes firmaron la carta pidieron que se investigue y sancione a los **máximos responsables**, que se preserven los archivos que están desapareciendo que tienen relación con los hechos, y que se “dispongan las medidas necesarias de no repetición” como lo es la reforma estructural al interior de las fuerzas militares.

Consideran que la Fiscalía General de la Nación “debe iniciar las respectivas investigaciones por los delitos en que habrían incurrido los implicados como violación ilícita de comunicaciones, **utilización ilícita de equipos transmisores y receptores**, falsedad en documento público y concierto para delinquir”. Además, afirman que es “preocupante que los recursos públicos del Estado se hayan destinado a gastos personales de oficiales de alto nivel”.

### **“Preocupa que los equipos tecnológicos adquiridos hayan sido usados para espiar”** 

Aseguraron que es de especial gravedad que “se hayan adquirido con ellos (los recursos) equipos de inteligencia para espiar de manera ilegal al director de la Organización Human Rights Watch, **José Miguel Vivanco**, a integrantes de las mismas fuerzas armadas y a otros ciudadanos y organizaciones”. (Le puede interesar:["Fuerzas militares abrirán sus archivos a la Comisión de la Verdad"](https://archivo.contagioradio.com/fuerzas-armadas-abriran-sus-archivos-a-la-comision-de-la-verdad/))

Finalmente, recordaron que “el general Rodríguez está involucrado en casos de **ejecuciones extrajudiciales**” cuando se desempeñó como comandante de IV Brigada del Ejército entre 2006 y 2008. Enfatizaron en que las investigaciones judiciales “han establecido que los llamados gastos reservados tanto de las Fuerzas Militares como de otros organismos de inteligencia, incluido el extinto DAS, han sido utilizados para la comisión graves violaciones a los derechos humanos”.

[190418-FINAL-Comunicado-Corrupción-en-FFMM](https://www.scribd.com/document/377387052/190418-FINAL-Comunicado-Corrupcio-n-en-FFMM#from_embed "View 190418-FINAL-Comunicado-Corrupción-en-FFMM on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_6444" class="scribd_iframe_embed" title="190418-FINAL-Comunicado-Corrupción-en-FFMM" src="https://www.scribd.com/embeds/377387052/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-E5SrjO1IPQAeIBeFe6on&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.707221350078493"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
