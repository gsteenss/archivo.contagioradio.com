Title: Montería instala campamento por la paz
Date: 2016-10-14 13:26
Category: Nacional, Paz
Tags: #AcuerdosYA, #PazALaCalle
Slug: monteria-instala-campamento-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/WhatsApp-Image-2016-10-14-at-9.46.21-AM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Campamento por la Paz Montería ] 

##### [14 Oct 2016] 

Desde el 12 de octubre  se instaló el campamento por la paz en Montería, en el Parque Simón Bolívar, una iniciativa que se ha venido incrustando en los ciudadanos en respaldo a los acuerdos de paz de la Habana. El campamento ya cuenta con la participación de **50 personas y  permanecerá en el parque hasta el 31 de diciembre, fecha límite del cese bilateral.**

Elena Mercado miembro de la Fundación Cordoversia, hace parte de las personas que se encuentran acampando en este lugar y comenta que están preparando actividades para continuar con la pedagogía por la paz, algunas de ellas son los cineforos que se proyectan en las noches. Se espera que en lo corrido del **fin de semana lleguen más delegaciones de otras zonas del departamento.**

Por otro lado en Bogotá, siguen llegando personas a sumarse al campamento que se encuentra en la Plaza de Bolívar, pese a las fuertes lluvias que se han presentado en la capital. Hasta el momento aproximadamente **200 personas han participado de esta iniciativa y actualmente hay 80 carpas en la Plaza.** Lea también:["Campamento por la paz cumple 7 días](https://archivo.contagioradio.com/campamento-por-la-paz-cumple-7-dias/)"

“Ni siquiera la lluvia va a levantarnos, las inclemencias nos acercan un poco más entre las personas del campamento, no nos puede parar el clima, ni las actividades que vayan a realizar acá en la Plaza, porque más allá de nosotros están los campesinos, por las víctimas” afirmo, Katherine Miranda, una de las voceras de esta iniciativa

Otro de los actores que ha impulsado fuertemente las acciones por la paz es el movimiento juvenil del país, que durante la movilización del pasado miércoles 12 de octubre, recibió en un acto simbólico el bastón de mando por parte de los voceros indígenas como compromiso de asumir ser la generación de la paz.

Para Omar Gómez, este es un proceso que los estudiantes han venido gestando desde hace cuatro años con el inicio del proceso de paz y con la intención de proponer una educación para la paz. Las y los estudiantes se reunirán **este sábado en el Encuentro de la comunidad estudiantil el sábado desde las 9 de la mañana** en la Universidad Nacional y tendrán pequeños comités organizativos todos los miércoles. Lea también ["Comunidad estudiantil se suma a los esfuerzos por acuerdos ya"](https://archivo.contagioradio.com/comunidad-estudiantil-se-suma-a-los-esfuerzos-por-acuerdos-ya/)

<iframe id="audio_13324645" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13324645_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
