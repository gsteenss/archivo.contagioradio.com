Title: Otra mirada: Voces contra el racismo estructural
Date: 2020-06-20 00:54
Author: PracticasCR
Category: Otra Mirada, Programas
Tags: afrodescendientes, Discriminación, Fuerza Pública, George Floyd, racismo
Slug: otra-mirada-voces-contra-el-racismo-estructural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/SOS-Racismo-aumento-discriminacion-ambitos_352174943_13654161_1628x1024.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 17 de junio, nuestro programa Otra Mirada abordó el tema del racismo estructural debido a los 2 conocidos casos de asesinato por discriminación racial vividos este año en Estados Unidos y los cuales han puesto foco en cómo se trata este tema no solo en el país norteamericano sino también al rededor del mundo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la muerte de George Floyd, el pasado 25 de mayo a manos de la Fuerza Pública, se han desencadenado protestas en países como Alemania, el Reino Unido, Francia y Colombia y en Estados Unidos en las que según el Centro de Investigaciones Pew , han participado 17 millones de personas aproximadamente. Además, se ha cuestionado si el racismo es una problemática que ha desaparecido. Sin embargo, y a pesar de lo que se creía, el racismo sigue tan vivo como lo estaba hace 80 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que la atención está puesta en Estados Unidos, país con un historial de discriminación bastante amplio, Colombia, aún siendo uno de los países más marcados por la diversidad racial y étnica, se ve afectado por esta problemática. Para desarrollar este tema invitamos a varios conocedores del tema.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ofunshi, habitante de Minneapolis, comenta que con el asesinato de Floyd, la rabia se ha extendido por todos los Estados Unidos y también internacionalmente, pero gracias a las movilizaciones y la lucha en coalición, hay una oportunidad de cambio en la que los protagonistas son los jóvenes. Le puede interesar: [Los 8 minutos y 46 segundos que arrodillaron a demócratas de EE.UU. en memoria de George Floyd](https://archivo.contagioradio.com/los-8-minutos-y-46-segundos-que-arrodillaron-a-democratas-de-ee-uu-en-memoria-de-george-floyd/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, reconoce que la lucha en Colombia es más complicada pues las personas afro e indígenas enfrentan grandes desafíos de seguridad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Según el El **Consejo** Nacional de **Paz Afrocolombiano** (CONPA) y La **Consultoría** para los **Derechos Humanos** y el **Desplazamiento**, (CODHES) 20.231 personas afro-colombianas y 10.036 indígenas se convirtieron en desplazados solamente en el 2020. Esto es racismo.

<!-- /wp:quote -->

<!-- wp:heading {"customTextColor":"#094d6a"} -->

Racismo en Colombia 
-------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por su parte, Clemencia Caribalí, representante de la Asociación de Mujeres Norte Cauca, considera que el racismo es una pandemia que resiste a pesar de la lucha, a lo que se le suma que los derechos de las comunidades no se respetan. "Se nos ve como un estorbo y por eso nos desplazan y nos asesinan".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, considera que se debe seguir exigiendo al gobierno el cumplimiento de los acuerdos que garanticen el goce de los derechos y aunque reconoce que no es fácil, sabe que no es imposible si se mantiene la unidad. Para ella, lo colectivo tiene mucho más impacto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, Luis Olave, gestor de la Ley Antidiscriminación explica que las estructuras de fuerza publica tienen en una ideología de guerra y política de odio hacia la población negra que es considerada como una amenaza. Recalca que esto no va a cambiar hasta que cambie el Estado y el poder que sigue gobernando en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, comenta la importancia de la ley 70 que, aunque sirve de referente para muchas luchas, no ha habido voluntad política para que se reglamente y se ha buscado limitar los derechos de la ley.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El Estado ve como una amenaza que la población negra tenga tanto territorio y que tengan que hacer consulta previa.

<!-- /wp:quote -->

<!-- wp:heading {"customTextColor":"#094d6a"} -->

Rol de la comunidad internacional 
---------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Olave, lo que se ha logrado a nivel internacional con la Alianza Latinoamericana para el Decenio de los Afrodescendientes y junto a organizaciones de América Latina y del Caribe es la presentación de una solicitud a Naciones Unidas para la creación del Foro Permanente para los Afrodescendientes a nivel mundial. En este se podrían generar propuestas para inversión a los estados desde el seno de Naciones Unidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, insiste que hay que hacer denuncias y para esto ha sido fundamental la Comisión Interamericana de Derechos Humanos (CIDH) pues ya que no hay atención directa, las comunidades deben denunciar ante las entidades internacionales pertinentes para que emitan medidas cautelares y de esta manera, el Estado tome las medidas necesarias.

<!-- /wp:paragraph -->

<!-- wp:heading {"customTextColor":"#094d6a"} -->

**Reflexión en materia de acciones** 
------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Olave concluye que las poblaciones afro y negra se han quedado solos y los medios de comunicación no visibilizan esa lucha. Más gente se debe juntar a la lucha no solo por los atentados que se hacen visibles, sino por cada persona y comunidad que ha sido lastimada. (Le puede interesar: [El racismo permanece en las instituciones del Estado: CRIC](https://archivo.contagioradio.com/el-racismo-permanece-en-las-instituciones-del-estado-cric/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Se necesita que la lucha sea masiva y fuerte; que la sociedad entienda que la comunidad negra ha aportado al desarrollo y construcción del país.

<!-- /wp:quote -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
