Title: Informe de la Fiscalía omite crímenes cometidos por agentes del Estado
Date: 2017-01-12 17:41
Category: Nacional, Paz
Tags: Implementación de Acuerdos, Indultos FARC, jurisdicción especial para la paz, Ley de Amnistia
Slug: informe-de-la-fiscalia-omite-crimenes-cometidos-por-agentes-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/victimas-del-estado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Justicia y Paz] 

###### [12 Enero 2017] 

La Fiscalía reveló que **440 guerrilleros de las FARC** quedarían en libertad con la Ley de Amnistía 1820 de 2016 y más de 15.000 de los 27.000 delitos relacionados con esta guerrilla serían archivados. Sin embargo, **el informe no tiene en cuenta las víctimas y las implicaciones en procesos adelantados contra agentes del Estado y grupos paramilitares. **

Ningún aparte del informe señala las 3.512 víctimas de ejecuciones extrajudiciales hechas bajo el Gobierno que impulsó la política de seguridad democrática, ni los 60.000 casos de desapariciones forzadas de los cuales, el más reciente informe del Centro Nacional de Memoria Histórica, revela que **46,1% corresponde a grupos paramilitares, 8% a agentes del Estado y el 15,9% a grupos armado no identificados.**

### ¿Y los crímenes contra Defensores de Derechos Humanos? 

Tampoco se hace mención en el informe de las acciones legales que deberá adelantar la justicia colombiana frente a los **94 casos de asesinatos a defensores de derechos humanos y reclamantes de tierras, registrados por la Cumbre Agraria, durante 2016** y los más de 126 asesinatos a integrantes del movimiento Marcha Patriótica desde el año 2010.

De acuerdo con el informe de protección del MOVICE, titulado *Protección de las Víctimas de Crímenes de Estado Como un Imperativo Para la Paz*, **desde agosto de 2013 y hasta julio de 2016 se presentaron 168 agresiones sólo contra integrantes del Movimiento. **Le puede interesar: [Fiscalía niega asesinatos sistemáticos contra líderes y defensores de DDHH.](https://archivo.contagioradio.com/fiscalia-niega-asesinatos-sistematicos-en-territorios/)

Dicho informe, también revela que **15 de los 22 capítulos del MOVICE se vieron afectados con las agresiones**, junto al Comité de Impulso Nacional, el Equipo Técnico Nacional y las regiones de Sucre, Atlántico, Caldas, Valle del Cauca y Nariño son las que tienen mayor riesgo.

Además, el informe trimestral de julio a septiembre de 2016, elaborado por el programa Somos Defensores, indica que dentro de los presuntos responsables de las 63 agresiones contabilizadas, los paramilitares aparecen con responsabilidad supuesta en 15 casos (24%), la Fuerza Pública en 4 casos (6%), la guerrilla del ELN en 1 casos (2%) y actores desconocidos en 43 casos (68%).

### ¿Qué ha pasado con los 3.000 amnistiables? 

Por otro lado, el Colectivo de Prisioneros Políticos de Guerra de las FARC-EP y la Columna Domingo Biohó del Patio 4 de la Picota en Bogotá, manifestaron a través de una carta abierta a Santos, que lo acordado en La Habana sobre el beneficio de amnistía para más de 3.000 personas "ha sido un proceso lento y tortuoso, **hasta la fecha sólo alrededor de 60 compañeras y compañeros han sido liberados y no cuentan con las garantías mínima**s para su reincorporación plena a la vida civil".

Distintos sectores sociales han manifestado su preocupación por una posible situación de impunidad, sobre ello Diego Martínez secretario ejecutivo del Comité Permanente por la Defensa de los Derechos Humanos indica que **“delitos como genocidio, crímenes de guerra o de lesa humanidad no podrán ser amnistiados** ni para integrantes de la guerrilla ni para agentes del Estado”.

Organizaciones de víctimas y defensoras de derechos humanos, como el MOVICE han dicho que es necesario “elevar el debate político y público sobre los ataques a comunidades, a defensoras y defensores para obtener respuestas eficaces de las autoridades, **cesen estos crímenes y se consoliden garantías de no repetición en el contexto de la implementación de los acuerdos de paz”.**

[Boletin Julio - Septiembre Somos Defensores - SIADDHH 2016](https://www.scribd.com/document/336412320/Boletin-Julio-Septiembre-Somos-Defensores-SIADDHH-2016#from_embed "View Boletin Julio - Septiembre Somos Defensores - SIADDHH 2016 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_82843" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336412320/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-KPrNGKhKuBvEUZknkUGD&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
