Title: Universidad de Antioquia continúa en paro indefinido
Date: 2015-10-14 12:35
Category: Educación, Nacional
Tags: Crisis financiamiento educación, Estudiantes Universidad de Antioquia, Mauricio Alviar, Movimiento estudiantil en Colombia, Paro en Universidad de Antioquia, Universidad de Antioquia, Universidades públicas en Colombia
Slug: universidad-de-antioquia-continua-en-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/UdeA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACEU Medellín] 

<iframe src="http://www.ivoox.com/player_ek_8982339_2_1.html?data=mZ6llJiXfY6ZmKiak5eJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmc%2Fd18rf1c7IpcWfxcqYo9PYrdDl1s7OjcjTstXdz4qwlYqmhcKfxtOY0sbWs4zdz8nSyM7SrcXjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rodrigo Vergara, U. Antioquia] 

###### [14 Oct 2015] 

[Tras la **negativa de las directivas de derogar el Acuerdo 48 del Concejo Académico** por medio del cual se modifica el reglamento estudiantil y ante la **inasistencia del rector Mauricio Alviar a las asambleas multiestamentarias** y los encuentros adelantados en el marco de la coyuntura, cerca de 8000 estudiantes de la Universidad de Antioquia continúan en el paro indefinido desde el 8 de octubre.]

[Rodrigo Vergara, estudiante de la Universidad, asegura que la modificación del reglamento estudiantil promovida desde hace 5 meses por las directivas en cabeza del rector, deteriora la calidad educativa de la institución, pues propone **cerrar el bachillerato nocturno y establece la presentación de una prueba específica** como parte del examen de admisión.]

[El rector argumenta la presentación de esta prueba, porque “asegurará la **permanencia de 24 estudiantes por cada 6000 que ingresen**”, sin embargo para Vergara **“la deserción no tiene nada que ver con el examen de admisión”**. Las **condiciones económicas o** las **características del pensum** son razones que nada tienen que ver con el examen de ingreso.  ]

[Vergara asegura que ha sido **constante la inasistencia del rector a las asambleas multiestamentarias** y a los encuentros con candidatos a la gobernación de Antioquia, programados para discutir sobre la situación por la que atraviesa la universidad, “A la asamblea multiestamentaria en la que se tomó la decisión de entrar en paro, **el rector no asistió  porque se encontraba de viaje en el exterior**”.]

[Las “medidas antidemocráticas” que el rector está asumiendo preocupan tanto a los que actualmente estudian en la Universidad como a quienes proyectan ingresar, agrega Vergara. Además de que son **pocos los cupos ofertados por los programas académicos**, en cada semestre se presentan cerca de 1000 personas que deben pagar altos costos por el examen de admisión, razón por la que “Nosotros proponemos que sean gratuitos”.]

[Es preocupante la actual **crisis de financiación del sistema de educación superior pública** en Colombia, con programas como “Ser pilo paga”, por cada estudiante que ingresa a la **Universidad de los Andes** el Estado asigna al semestre cerca de **\$2 millones**, mientras que a las **universidades públicas** les gira un rubro de **\$236 mil**. Por lo que “toda la estructura educativa del país debe ser reconfigurada”, indica Vergara.]

[**“Exigimos que la rectoría se haga presente en las asambleas y debates”** y “Esperamos que más sedes” se unan al paro, pues “Hay profesores y estudiantes que no han suspendido las clases”, lo que afecta la movilización estudiantil, concluye Vergara.  ]
