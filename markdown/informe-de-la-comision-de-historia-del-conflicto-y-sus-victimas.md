Title: Informe de la Comisión de Historia del conflicto y sus víctimas, es un paso hacia una comisión de la verdad
Date: 2015-03-04 20:40
Author: CtgAdm
Category: Hablemos alguito
Tags: Comisión de Historia del conflicto y sus víctimas Colombia, Cuba, Mesa de Diálogos de La Habana
Slug: informe-de-la-comision-de-historia-del-conflicto-y-sus-victimas
Status: published

###### Foto:sociologandoudea.blogspot.com 

##### **Entrevista:**  
<iframe src="http://www.ivoox.com/player_ek_4166056_2_1.html?data=lZajmJWZeo6ZmKial5eJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Fa0Nfax5Cns87d1M6SpZiJhpTijMnSja3Nt9Xj087OjcnJsIy30NPTzs7HuNCf2pDg19iPuoa3lIqupsjYcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

El informe sobre memoria histórica presentado por la **Comisión Histórica del conflicto y sus víctimas (CHCV)** se deriva del acuerdo entre el Gobierno Nacional y los delegados de las FARC.

**El 5 de agosto de 2014 en el marco del Acuerdo y de la agenda para adelantar las conversaciones de paz se decidió conformar una comisión de 12 expertos** y dos relatores para poder esclarecer las **causas**, las **consecuencias**, los **impactos** sobre la población y los factores que han permitido su persistencia a través de estos 60 años.

En la Mesa de Diálogos se calificó como "... insumo fundamental para la comprensión de la complejidad del conflicto y de las responsabilidades de quienes hayan participado o tenido incidencia en el mismo y para el esclarecimiento de la verdad..", como "...un insumo básico para una futura comisión de la verdad..."

De tal forma el informe configura una **lectura múltiple y pluralista** de la **historia** reciente del conflicto armado colombiano.

Entrevistamos a **Alfonso Castillo, análista político y vocero del MOVICE**, quién nos explica que el propio informe es plural pero esto hace que las opiniones al respecto sean dispares, y en algunos casos dispares. Los 12 informes y las dos relatorías presentadas en Febrero de 2015 son la base de las distintas percepciones del conflicto de cara a realizar una comisión de la verdad oficial.

También conversamos con Marco Velazquez de las **comunidades CONPAZ (Comunidades construyendo paz)**, quién como vocero de las víctimas en las comunidades, nos explica la utilidad que va a tener el informe y con el que las comunidades están conformes. Por otro lado Marco presenta el informe realizado por las comunidades enviado a la **Mesa de Diálogos de La Habana** para poder fortalecer la futura comisión de la verdad.
