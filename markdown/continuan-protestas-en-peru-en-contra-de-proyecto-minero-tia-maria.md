Title: Perú: A pesar de parón de proyecto minero Tía María continúan las protestas
Date: 2015-05-19 18:36
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: #Arequipa, Cocachacra, Proyecto minero Tía María, Southern Perú Copper Corporation, Sur de Perú en paro por Tía María, Violaciones de DDHH Perú protestas Tía María
Slug: continuan-protestas-en-peru-en-contra-de-proyecto-minero-tia-maria
Status: published

###### Foto:Semanariocontroversia.com 

###### **Entrevista con [Rocío Silva], Coordinadora Nacional de DDHH de Perú:** 

<iframe src="http://www.ivoox.com/player_ek_4523270_2_1.html?data=lZqflZebdI6ZmKiakp2Jd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1c7bh6iXaaO1wtOY0tfTuMbn1cbgjcrSb7HZ09qY0tTWb9Hm0N7SxdnTb7WZpJiSo6nFb67V04qwlYqliMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Continúan las **fuertes protestas en el sur de Perú** debido a que el gobierno no se ha pronunciado sobre el proyecto minero Tía María.

**Portavoces gubernamentales han dicho que no es posible romper la contratación con la multinacional de capital Mexicano**. El lunes la multinacional anunciaba un **parón de dos meses** con el objetivo de renegociar con el estado y de que se calme la situación.

Pero los antimineros no van a finalizar el **paro** y han **decidido continuar secundando desde la región de Arequipa,** a la que se han sumado todas las otras **regiones del sur del país.**

Además se está preparando **una jornada de lucha y paro en todo el país con el objetivo de paralizar el proyecto** que afectaría amplios sectores agricultores del país, siendo la región del Valle del Tambo la más afectada.

Para **Rocío Silva, Coordinadora Nacional de DDHH de Perú**, la tregua decretada por el gobierno y la multinacional pretende tranquilizar la situación pero los afectados por el proyecto **han decidido continuar con el paro** y a este se han sumado más regiones del sur del país.

El proyecto minero "Tía María" fue presentado en **2011 **por la **multinacional de capital Mexicano Southern Perú Copper Corporation.** Es en este momento cuando ya hubo un primer conato de protestas que dejó **3 manifestantes muertos, por ésto las** organizaciones sociales de DDHH y afectados de la región exigieron a Naciones Unidas realizar un informe sobre las posibles afectaciones que pudiera presentar el proyecto minero.

Fue entonces cuando **UNOC emitió el informe en el que se advertía de 137 observaciones en referencia daños ambientales y de carácter social y económico para la región.**

El proyecto se desarrollaría en una zona árida, por un lado, y en otra zona altamente fructífera conocida como el **Valle del Tambo, donde más del 80% de la economía depende de la agricultura.** En total 13.000 hectáreas quedarían afectadas por el proyecto y en **grave riesgo de secarse el río Tambo.**

La afectación en el valle dejaría **sin agua para poder cultivar**, dejando toda la región sin la zona más prospera donde los agricultores y campesinos dependen de las exportaciones de fruta y verdura.

La concesión minera también **afectaría al Puerto de Mollendo, donde se instalaría una desalinizadora** con el objetivo de extraer más agua, los residuos vertidos por la planta y la propia infraestructura afectarían ambientalmente al puerto más importante del país que conecta el atlántico con el Pacífico.

En **2014** la multinacional encarga un estudio en el que refleja que las 137 observaciones del informe de UNOC ya se han solventado, se presenta al gobierno, y éste sin verificarlo **adjudica la concesión minera para la región de Arequipa.**
