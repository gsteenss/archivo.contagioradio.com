Title: "Con Carlos Ledher en Alemania se bloquea un capítulo del narcotráfico y la política colombiana"
Date: 2020-07-15 23:01
Author: CtgAdm
Category: Nacional
Slug: con-carlos-ledher-en-alemania-se-bloquea-un-capitulo-del-narcotrafico-y-la-politica-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small","className":"has-text-color has-cyan-bluish-gray-color"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 15 de junio y luego de 33 años en una prisión de máxima seguridad en Estados Unidos fue enviado directo a Alemania por su doble nacionalidad, el **exnarcotraficante Carlos Lehder,** en donde permanece en libertad a pesar de tener asuntos pendientes con la Justicia colombiana. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos han cuestionado la inoperancia del **Gobierno Colombiano, señalando que *"no hizo nada para que la verdad de Lehder llegara al país"***, y con esto el capítulo criminal de la mano derecha de Pablo Escobar quedó clausurado, así como la verdad sobre el narcotráfico y la política colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En búsqueda de estas respuestas, se han emprendido acciones como la solicitud del senador Rodrigo Lara Restrepo, **hijo del asesinado exministro de Justicia Rodrigo Lara Bonilla, para ser aceptado oficialmente en la Jurisdicción Especial para la Paz (JEP)** como víctima del conflicto y así buscar una declaración de Lehder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lehder no solamente estuvo involucrado en el asesinato del exministro Lara, también e**stuvo a cargo de la narcoflotilla de la mafia y conoce los secretos de [Tranquilandia](https://archivo.contagioradio.com/sucandidaturapudosercontradictoriaconsufe/)**. Por ello, las acciones para que Colombia escuche su verdad resuenan en el Parlamento Alemán.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Heike Hänsel, vicepresidenta de la bancada del partido socialista [Die Linke](https://www.linksfraktion.de/presse/pressemitteilungen/detail/blockade-im-un-sicherheitsrat-und-politische-instrumentalisierung-humanitaerer-hilfe-fuer-syrien-beend/) en el Parlamento alemán**, no es ajena a Colombia, y se ha sumado a esta presión internacional para que el Gobierno siga implementando los acuerdos de paz, y esto incluye el esclarecimiento de la verdad y la justicia en casos sin resolver entorno al capitulo del narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Contagio Radio: ¿Qué tiene que aportar Carlos Lehder a Colombia?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Heike Hänsel**: Lehder no es solamente uno de los criminales más temidos en la historia de Colombia, la información que tiene que decir es muy importante para aclarar diferentes hechos de este capitulo de la violencia en Colombia. Y tiene que comparecer ante la JEP y contribuir a la verdad en el contexto de la Comisión de la Verdad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: ¿Es posible que esta verdad llegue?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**HH:** El problema consiste ahora en que según el derecho alemán Lehder no puede ser extraditado a Colombia, esto significaría que su participación en la que sería en un nivel legal, es muy difícil, pero sí podría contribuir en la Comisión de la Verdad. Y a pesar de al Gobierno Colombiano pareciera no interesarle esta verdad, yo insistiré en que el Gobierno Federal Alemán permita que este partícipe en la Comisión y aclare su papel en el conflicto armado y de otros actores dentro de este.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "... a pesar de al Gobierno Colombiano pareciera no interesarle esta verdad, yo insistiré en que el Gobierno Federal Alemán permita que este partícipe en la Comisión y aclare su papel en el conflicto armado y de otros actores dentro de este."
>
> <cite>Heike Haensel </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### CR: ¿Carlos Lehder hablará de personas en concreto?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**HH:** Obviamente sería especulación, pero parece que hay una empresa dentro de Colombia y algunos sectores que tienen interés para que Lehder no contribuya a aclarar sobre el pasado y los vínculos entre el narcotráfico, la política y el paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo existen afirmaciones de Leder y su participación en el asesinato de sobre Rodrigo Lara Bonilla, así que es muy probable que tenga información sobre estos vínculos de autores materiales e intelectuales, y como este caso muchos otros en donde revelaría nombres de responsables.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: ¿Puede haber intereses de alguna institución colombiana para que Carlos Lehder no venga a Colombia?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**HH:** No tengo indicios de que por parte del gobierno colombiano o alguna entidad del Estado colombiano se haya impedido la extradición de Carlos Lehder hacia Colombia, pero es muy evidente de que no hubo ningún intento o solicitud por parte del Gobierno colombiano o alguna entidad del Estado de evitar que este fuera extraditado a Alemania.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Es posible que haya una acción de parte del parlamento alemán junto con el parlamento europeo para que en Colombia conozcamos esa verdad?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este es un asunto muy reciente que apareció en los medios, desde el parlamento alemán estaremos muy de cerca para que esté comparezca ante la comisión de la verdad y pueda contribuir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El gobierno alemán en sí mismo dice que es un garante de la implementación de los Acuerdos de Paz así que por tanto debe reflejar su responsabilidad y no dejar pasar esas verdades.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué se está haciendo para conseguir esa verdad; por ejemplo la comisión de la verdad ya se comunicó con ustedes?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hasta el momento no ha habido ningún contacto por parte de la Comisión de la Verdad, pero los logros también se puede ejecutar por el otro camino, que es el de nosotros como parlamentarios buscar este contacto y determinar los pasos a seguir y para que la Comisión reciba este verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"has-text-color has-cyan-bluish-gray-color"} -->

*Vea la entrevista completa con la parlamentaria Heike Hänsel:*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D609873543240563&amp;show_text=false&amp;width=734&amp;height=411&amp;appId" width="734" height="411" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
