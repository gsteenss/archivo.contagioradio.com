Title: Paz a la Calle, iniciativa de los jóvenes por los acuerdos de paz
Date: 2016-10-04 15:03
Category: Entrevistas, Paz
Tags: Apoyo al proceso de paz, Jóvenes por la Paz, Plebiscito
Slug: no-somos-una-patria-boba-y-vamos-a-demostrarlo-pazalacalle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14519881_1773529309582798_2613154662689174702_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz A La Calle] 

###### [4 Oct 2016] 

Después del resultado del domingo en el plebiscito, un grupo de  jóvenes convocó  en redes sociales a una Asamblea Ciudadana. Ayer en horas de la noche **más de 100 personas se reunieron en el Park Way** en la capital,** para congregarse en la iniciativa que llamaron \#PazAlaCalle** y que recoge el sentir de los jóvenes de Bogotá. Una de las propuestas es exigir, a través de la movilización, que **los promotores del NO sean concretos en sus propuestas para no permitir que se dilate el acuerdo de paz.**

Durante las 3 horas del encuentro, los participantes hicieron un [análisis de la coyuntura](https://archivo.contagioradio.com/pacto-nacional-podria-ser-la-oportunidad-de-unificar-procesos-de-paz/) y conformaron cinco comisiones de trabajo: paz territorial, organización, comunicaciones, diálogo con iglesias e iniciativas jurídicas.

Además definieron una agenda en la que plantean las siguientes actividades:

1.  Extienden una invitación a toda la ciudadanía para participar en la Marcha Universitaria que se llevará a cabo el próximo Miércoles desde las 5:00 p.m en el Planetario, una marcha de antorchas hacia la Plaza de Bolívar para demostrar que “no somos una patria boba” y que continua el compromiso para construir Paz.
2.  Se hará una reunión virtual con distintas ciudades hoy a las 4 p.m, la información estará disponible en el Facebook de de Paz A La Calle.
3.  Este Jueves a las 7:00 p.m en el Park Way continúa la Asamblea Ciudadana, se hablará de la movilización del miércoles, se presentaran las conclusiones de las comisiones de trabajo y más estrategias de acción colectiva, para llevar este trabajo a otras localidades, ciudades y municipios.

El motivo principal de esta iniciativa ciudadana es, como manifestó Javier Cuadros uno de los integrantes: **"transformar la tristeza en acciones colectivas que logren concretar una paz estable y duradera** (...) [si todos ayudamos a construir una representación colectiva](https://archivo.contagioradio.com/todavia-quedan-vias-para-salvar-los-acuerdos-de-paz/)y plantear estrategias novedosas en la calle, lograremos que el poder constituyente haga válido el artículo 22 de la Constitución Política, la **paz como un derecho fundamental de obligatorio cumplimiento”.**

“La paz es posible y Colombia no es violenta por naturaleza, tiene una ciudadanía formada y lista para asumir el presente y futuro del país”, concluyó Cuadros.

Los puntos de encuentro en Bogotá y otras ciudades serán:

En Medellín, **Parque de los Deseos** a las 5:00pm

En Villavicencio el punto de encuentro será el **Parque de las Banderas** a las 4:00pm

En Pereira el movimiento juvenil se encontrarán en el **Parque El Lago** a las 5:00pm y el recorrido llegará a la Plaza de Bolivar.

En Cali la cita será en el **Parque el Ingenio** a las 5:00pm y la marcha culminará en el Parque de las Banderas.

En Bogotá otros puntos de encuentro, a parte del Planetario serán:

La estación de Transmilenio Museo del Oro

El Centro de Memoria, Paz y Reconciliación

<iframe id="audio_13180067" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13180067_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
