Title: Paramilitares amenazan de tomar el control en zona de La Gabarra, Norte de Santander
Date: 2017-02-10 12:00
Category: DDHH, Otra Mirada
Tags: Norte de Santander, paramilitares
Slug: paramilitares-amenazan-de-tomar-el-control-en-zona-de-la-gabarra-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 Feb 2017]

Habitantes de La Gabarra, en el Norte de Santander que se encontraban acompañando la caravana de movilización de la guerrilla de las FARC-EP hacia su punto de concentración, denunciaron la presencia de un grupo de **15 hombres armados que se identificaron como paramilitares.** Por este motivo y **frente a la falta de accionar de las autoridades, los campesinos de esta región, decidieron frenar la caravana hasta que se garantice la vida** y la integridad de las comunidades de esta región del país.

Los hechos ocurrieron en la vereda las timbas, a 30 minutos del corregimiento especial de la Gabarra, cuando campesinos que transitaban por esta zona fueron abordados por un grupo de **15 hombres armados, que se identificaron como paramilitares de las Águila Negras**. De acuerdo con los campesinos los hombres amenazaron diciendo que “**el que no trabaje con ellos se tenía que ir de la región y que ahora con la salida de las FARC-EP ellos se quedarán mandando en la zona**”.

Indígenas de la Comunidad Bari también denunciaron, en un comunicado de prensa, la **presencia de grupos paramilitares sobre las 3:00pm del 9 de febrero**, en la vía que conduce de La Gabarra hasta la comunidad de Brunbuncanina y Sahpadana. Motivo por el cual algunos de ellos no han podido ingresar a las comunidades, temiendo por su seguridad. Le pude interesar: ["Paramilitares torturaron a indígena en el Bajo Calima"](https://archivo.contagioradio.com/paramilitares-torturaron-a-indigena/)

Muy cerca de este lugar se encuentra una **base Militar, que de acuerdo con Olga Quintero, vocera de las Asociación Campesina del Catatumbo, no reaccionó,** pese a que esta a media hora del lugar en donde estuvieron los hombres armados. Le puede interesar: ["Sigue el asedio de paramilitares contra Comunidad de Paz de San José de Apartadó"](https://archivo.contagioradio.com/paramilitares-atacan-a-la-comunidad-de-paz-de-san-jose-de-apartado/)

Por esta situación **20 familias de las veredas Vella Vista y Caño Tomás se han desplazado**, mientras que campesinos han asegurado que no dejarán salir la caravana de las FARC-EP, de La Gabarra, hasta que no haya una presencia del Gobierno y autoridades

Organizaciones  defensoras de derechos humanos como el Colectivo Luis Carlos Pérez, informaron que harán una comisión de verificación frente a los hechos, de igual forma Quintero expresó que “**están exigiendo la presencia inmediata del gobierno en esta región para visibilizar las violaciones a los derechos humanos del campesinado** y evitar que se repita una incursión paramilitar como la que sucedió hace 17 años y que provocó la masacre de La Gabarra”. Le puede interesar:["La macabra alianza entre empresas Bananeras y Paramilitares"](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/)

<iframe id="audio_16943582" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16943582_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
