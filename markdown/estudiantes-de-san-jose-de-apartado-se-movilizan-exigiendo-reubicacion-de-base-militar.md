Title: Estudiantes de San José de Apartadó se movilizan exigiendo reubicación de base militar
Date: 2015-08-05 13:18
Category: Movilización, Nacional
Tags: Antioquia, Aparatadó, base militar, brigada 17 del Ejército, Derecho a la educación, El Mariano, FARC, Germán Rojas, San josé de Apartadó, Urabá Antioqueño
Slug: estudiantes-de-san-jose-de-apartado-se-movilizan-exigiendo-reubicacion-de-base-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/San-Jose-de-Apartado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [pacifista.co]

<iframe src="http://www.ivoox.com/player_ek_6080227_2_1.html?data=l5WlkpeWe46ZmKiak5uJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo1snWw9PYqdSfxcqYtcbSb6vj1IqwlYqlfYzYxpCu0sbWuMLYhqigh6eXb9TZjNLc2M7QrdvVz5DS2s7LcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ubaldo Muñoz, Consejo Estudiantil del Colegio rural El Mariano] 

###### [5 Agosto 2015]

Este miércoles se inicia una movilización en San José de Apartadó, en la región del Urabá Antioqueño, por parte de los **estudiantes del colegio El Mariano quienes exigen se reubique un puesto militar ubicado a 50 metros del nuevo plantel educativo,** además reclaman condiciones optimas para que los niños y niñas de esa región cuenten con una educación digna y de calidad.

En total son **486 niños y niñas entre 5 y 18 años de edad,** los que se encuentran en peligro por tener ubicado su plantel educativo a pocos metros de un puesto militar. Fue por eso que desde el 22 de julio la comunidad estudiantil decidió entrar en cese de actividades, con el fin de exigir la reubicación de la **Brigada 17 del Ejército.**

Sin embargo, pese a que se ha tratado de una manifestación pacífica, de acuerdo con Ubaldo Muñoz, integrante del Consejo Estudiantil del Colegio rural El Mariano, **Germán Rojas, comandante de la Brigada,** había señalado que ese paro estudiantil era ordenado por el 5° Frente de las FARC.

Según Muñoz, el 22 de julio se militarizó el corregimiento con tanquetas de guerra lo que preocupó a la comunidad, “**somos estudiantes que reclamamos nuestros derechos pero nos tratan como terroristas…** lo que queremos es que se reubique la base militar, no pedimos que la fuerza pública sea retirada”, puntualiza el estudiante, quien agrega que “han contado con la suerte” de que se haya declarado cese unilateral ya que no ha habido enfrentamientos.

Otra de las exigencias de los estudiantes, padres de familia y el resto de la comunidad que participarán en esta movilización tiene que con **el derecho a la educación.**

Muñoz cuenta que las condiciones de los niños y niñas que tienen clases en escuelas de las veredas “**son inhumanas… no hay servicio de electricidad,** aunque el municipio es certificado en educación hay escuelas que están con techos de plástico, armadas con carpas improvisadas, los niños se tiene que sentar en el sueño o troncos de madera”, lo que es contradictorio con el lema **´Antioquia más educada\`, cuando las condiciones de educación no son optimas.**

La marcha pacífica iniciará este miércoles desde el barrio 20 de enero y recorrerá la ciudad hasta alcanzar las instalaciones de la alcaldía de Apartadó.
