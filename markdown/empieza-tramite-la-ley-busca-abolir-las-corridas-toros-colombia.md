Title: Empieza el trámite de la Ley que busca abolir las corridas de toros en Colombia
Date: 2017-05-25 18:05
Category: Animales, Nacional
Tags: corridas de toros
Slug: empieza-tramite-la-ley-busca-abolir-las-corridas-toros-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DAsQweeXUAIJYgN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Plataforma ALTO 

###### [May 25 de 2017] 

Este jueves empieza uno de los primeros trámites en la Comisión Séptima de Cámara de Representantes de la Ley que busca abolir la fiesta taurina en todo el país: la reunión de los ponentes. Aunque no es una tarea fácil, desde ya la ciudadanía ha mostrado su apoyo y buscado presionar a los congresistas de esta Comisión para que el próximo 30 de mayo aprueben en su primer debate esta iniciativa.

Este proyecto **elimi**[**na las expresiones “rejoneo, corridas de toros, novilladas, becerradas y tientas”** contenidas en el artículo 7º de la Ley 84 de 1989 y deroga la Ley 916 de 2004 “Reglamento Nacional Taurino”.]{.text_exposed_show}

Se trata de una propuesta presentada por el ministro del Interior, Juan Fernando Cristo y acompañada de organizaciones animalistas. "**En momentos de paz y convivencia es necesario que la sociedad tome decisiones frente a las corridas de toros.** Sin embargo, la invitación es a hacerlo por el camino del respeto", aseguró el jefe de la cartera política", argumenta el ministro.

Cabe recordar que son diversas las iniciativas que se tramitan en el Congreso buscando debilitar o acabar con los espectáculos taurinos, entre ellas, se busca impedir que los menores de edad ingresen a las corridas.

Las iniciativas contra los espectáculos crueles con animales cada vez toman más fuerza, sobre todo en este momento cuando la **Corte Constitucional ha revivido la consulta antitaurina en Bogotá, y además le dio un ultimátum al congreso de la Repúblic**a para que en dos años legisle sobre el tema, o estas serán abolidas.

###### Reciba toda la información de Contagio Radio en [[su correo]
