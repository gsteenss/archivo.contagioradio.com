Title: Las mentiras de Peñalosa sobre la Reserva Forestal van der Hammen
Date: 2016-01-27 17:30
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, CAR, Enrique Peñalosa, Reserva Forestal Van der Hammen
Slug: las-mentiras-de-penalosa-sobre-la-reserva-forestal-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://www.ivoox.com/player_ek_10222220_2_1.html?data=kpWflJeWdpGhhpywj5aZaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDax9PYrdPV1JDRx5C0qYa3lIqvk8bQs9TVjNjcxNfJb83VjLfS1crWusKfx9Tfx9jYpc2ft8bbj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Fernando Gómez] 

###### Foto: Cop 20 Bogotá 

###### [27 Ene 2016] 

En una entrevista el Alcalde de Bogotá, Enrique Peñalosa, aseguró que extenderá la ciudad construyendo sobre gran parte de la [Reserva Thomas Van der Hammen](https://archivo.contagioradio.com/?s=van+der+hammen), **que en el 2011 fue declarada por la CAR,** como Área de Reserva Forestal Productora Regional del Norte, lo que significa, que tiene un fin forestal que debe conservarse y protegerse.

Según afirmó Peñalosa, la reserva no es más que “potreros” y apenas “un bosquecito” de 5 hectáreas, pese a que este proyecto ambiental  constituía la creación del bosque urbano más grande del mundo, al tratarse de **1.395 hectáreas** que ubicadas en el extremo norte de la ciudad constituirían el segundo pulmón de la capital.

De acuerdo con el alcalde, el plan urbanístico contempla varios beneficios frente a los que los ambientalistas estarán “**muy de acuerdo” pues se trata de ”la única reserva forestal del mundo que no tiene árboles… 1400 hectáreas de potreros**”, como asegura Peñalosa.

Contagio Radio habló con el promotor de la Red Ambiental de la Reserva forestal Thomas Van der Hammen, Fernando Gómez quien contrario a Enrique Peñalosa, señaló que esta reserva es una zona estratégica importante por las siguientes razones.

**Cambio climático:** Peñalosa, afirmó que su proyecto urbanístico “minimiza la problemática del cambio climático”, sin embargo, de acuerdo con Fernando Gómez, este pensamiento es totalmente contrario a la realidad, ya que se dejaría de recuperar este lo que sería el segundo pulmón de la ciudad, teniendo en cuenta que de acuerdo con el Jardín Botánico José Celestino Mutis, Bogotá tiene un déficit de más de millón y medio de árboles, y si el alcalde asevera que la población se multiplicará progresivamente, este déficit aumentaría aún más pues según la Organización Mundial de la Salud, OMS, mínimo debe haber un árbol por cada tres habitantes.

“La deuda social y ecológica es monumental”, expresa Gómez, quien más bien cree que la reserva contribuye significativamente a la adaptación y mitigación del cambio climático.  “Si se incentiva la densidad demográfica, si se ataca la sabana donde se está haciendo la producción de alimentos, no habrá mitigación frente al cambio climático, habrá inseguridad alimentaria, contaminaremos las fuentes hídricas” provocando un “colapso urbano”.

**Función de conectividad:** La reserva Van der Hammen cumple funciones de conectividad con la estructura ecológica de la ciudad, y la estructura de la región. De acuerdo con la CAR, “La reserva es un área de importancia estratégica para la conectividad ecológica entre los Cerros Orientales y el Cerro del Majui, así como la conectividad hídrica entre la red hídrica natural que nace en los Cerros Orientales alimentando los humedales de Torca, Guaymaral y La Conejera, finalizando en el río Bogotá”.

**Biodiversidad:** La reserva representa una extraordinaria riqueza natural, histórica, y paisajística. Cuenta con un inventario en flora de más de 500 plantas, así mismo, reposan aves migratorias y acuáticas, conejos, curíes, murciélagos dos especies endémicas que son el Chamicero y el Picono rufo, que utilizan como hábitat los bosques primarios y secundarios, así como matorrales de la reserva.

**Riqueza en subsuelos:** Según el estudio que realizó la CAR, el subsuelo de la reserva cuenta con importantes recursos hídricos subterráneos, lo que a su vez implica que esta zona contienen un alto potencial productivo en términos agrícolas, pecuarios y forestales, pues “el 78,51% de los suelos son catalogados en estas categorías; en consecuencia, la calidad de estos suelos se constituye en base fundamental para el potencial establecimiento de proyectos productivos agroforestales, silvopastoriles y de seguridad alimentaria con enfoque ecológico”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
