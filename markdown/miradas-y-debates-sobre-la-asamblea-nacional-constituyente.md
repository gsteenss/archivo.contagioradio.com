Title: Miradas y debates sobre la Asamblea Nacional Constituyente
Date: 2015-06-17 15:18
Category: Nacional, Paz
Tags: ANZORC, asamblea nacional, congreso de los pueblos, constituyente, constituyentes, FARC, gobienro, habana, nestro humberto, Poder Ciudadano, progresistas, Santos, USO
Slug: miradas-y-debates-sobre-la-asamblea-nacional-constituyente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/AC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fundación Crea 

<iframe src="http://www.ivoox.com/player_ek_4654448_2_1.html?data=lZuilpmYfI6ZmKiakpqJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfotjOz8fQqcKfr8bQy9TSpc2fpNTb1dnNuNbtxtPhx4qWh4zYxsfO1srXb9qfzs7fw8nFt4zYjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sammy Sánchez, Constituyentes por la Paz] 

###### [17 jun 2015] 

Desde que inició el proceso de paz entre el gobierno de Juan Manuel Santos y la guerrilla de las FARC, varias son las hipótesis que se han manejado y los sectores que se han pronunciado frente a cuál es el mecanismo idóneo para refrendar los acuerdos que se alcancen en la Mesa de Diálogos. Mientras el gobierno mantiene como discurso oficial la apuesta por realizar un referendo, preferiblemente acompasado a un proceso de elección popular; las FARC se mantienen en la propuesta de realizar una Asamblea Nacional Constituyente, como pacto de paz.

Diversas organizaciones sociales han sumado argumentos a esta última propuesta. En el foro de "Participación política" convocado por la ONU y la Universidad Nacional de Colombia en el añó 2013, se puso sobre la mesa la necesidad de ampliar el marco de reformas que deberían realizarse en Colombia, y no restringirlo a los 4 puntos acordados entre gobierno e insurgencia; En las últimas semanas, además, diversas figuras del gobierno han manifestado que esta podría ser una posibilidad realizable, entre ellos Nestor Humberto Martinez, saliente Ministro de la Presidencia, quien manifestó que la ANC sería necesaria para implementar los acuerdos, pero no para refrendarlos.

La discusión, cada vez más cercano para la Mesa de Diálogos de Paz, también gana lugar en escenarios de encuentro ciudadano.

Organizaciones sociales que convergen en el proceso de Constituyentes por la Paz, están realizando una convocatoria para debatir y sopesar argumentos frente al tema, durante la realización del Foro "Dialoguemos sobre la Asamblea Nacional Constituyente", que se realizará los días 18 y 19 de junio en el Centro de Memoria, Paz y Reconciliación.

El foro contará con la participación de organizaciones sociales y populares como ANZORC, la USO, y procesos ciudadanos como Progresistas, la Unión Patriótica, el Congreso de los Pueblos, Poder Ciudadano, entre otros.

Las personas interesadas en participar pueden inscribirse a través del correo electrónico relacionamiento.constituyente.com, o pueden seguir la transmisión en vivo a través de www.contagioradio.com

\[caption id="attachment\_10189" align="aligncenter" width="480"\][![Foro ANC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/FB_IMG_1434543160933.jpg){.size-full .wp-image-10189 width="480" height="421"}](https://archivo.contagioradio.com/miradas-y-debates-sobre-la-asamblea-nacional-constituyente/fb_img_1434543160933/) Foro ANC\[/caption\]
