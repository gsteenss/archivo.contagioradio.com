Title: Bila Colombia, nuestras tradiciones culturales en clave de comedia
Date: 2018-09-24 14:46
Author: AdminContagio
Category: eventos
Tags: Bogotá, danza, teatro
Slug: bila-colombia-musica-danza-teatro-y-circo
Status: published

###### Foto: Bila Colombia 

###### 24 Sep 2018 

**Bila Colombia** es un espectáculo que reúne la danza, la música, el circo y el teatro en clave de comedia, inspirado en nuestra cultura y construido a partir del talento de artistas nacionales. Evento que **hasta el 28 de octubre** estará presentando su segunda temporada en la ciudad de Bogotá.

Felipe Saray, Director del espectáculo asegura que desde la concepción del proyecto artístico "**lo que nosotros queremos lograr algún día es tener un show icono donde la gente puede ir a ver un poco de nuestra cultura**" refiriéndose tanto a los públicos locales como foráneos.

Con una infraestructura moderna que incluye **luces, video mapping y músicos en vivo**, durante aproximadamente 90 minutos, **en Bila se narran historias ancestrales, culturales y diversas de forma divertida**. (Le puede interesar: [Ditirambo Teatro, 3 décadas de lucha y resistencia](https://archivo.contagioradio.com/ditirambo-teatro-lucha-y-resistencia/)).

<iframe src="https://www.youtube.com/embed/kTnNGJc97Ic" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Son más de 15 artistas en escena quienes permiten comparar a través de su espectáculo, el ayer con el hoy, recrear la memoria y fortalecer la identidad cultural y artística, algo que valora Saray al afirmar que "parte del secreto del éxito que hemos tenido es la combinación de este casting porque **tenemos bailarines autóctonos folclóricos que conocen perfectamente el vestuario, la puesta en escena y las coreografías tradicionales colombianas de diferentes partes del país**, pero también hay gente que está más enfocada al circo, y al combinarlos todos es lo que lleva a hacer de esto algo tan particular y que finalmente todos son artistas 100% colombianos que es lo que queremos mostrar".

Mientras la agrupación se prepara para la próxima temporada, programada para 2019, **Bila Colombia se estará presentando en el Teatro Libre de Chapinero** calle 62 \#9A-65 , los **domingos a partir de las 11 de la mañana** hasta el próximo 28 de octubre (excepto el día 14), en un horario para compartir con toda la familia.

<iframe id="audio_28823591" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28823591_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
