Title: Entre 2015 y 2017 un indígena fue desplazado forzadamente cada hora según ONU
Date: 2017-06-07 13:11
Category: DDHH, Nacional
Tags: DDHH, indígenas, ONU
Slug: onu-comision-de-derechos-humanos-de-la-poblacion-indigena-velara-por-sus-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/COLOMBIA-INDÍGENAS-e1496858353209.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Herald] 

###### [07 jun 2017] 

El informe “Vulneraciones DDHH-DIH en contra de pueblos indígenas. Enero 2015- Febrero 2017” de la Misión de las Naciones Unidas en Colombia revela **que cada hora una o un indígena fue víctima de desplazamiento forzado.** Cada día 4 indígenas fueron confinados y cada mes 39 líderes, lideresas y autoridades indígenas fueron amenazados.

En su programa radial para Colombia, las Naciones Unidas afirmó que **varias poblaciones indígenas han sido afectadas por el desplazamiento forzado** aumentando la vulneración de sus derechos. Recordaron que la creación de la Comisión de Derechos Humanos de la Población Indígena es para mitigar esta situación.

La Comisión integrada por delegados indígenas, del Gobierno Nacional y de órganos de control, velará por la protección, promoción y defensa de los derechos humanos de los indígenas. **Diseña medidas de prevención, estrategias de investigación para contribuir con los procesos penales y disciplinarios** y aporta a la construcción de programas especiales de atención a las víctimas mientras que diseña políticas públicas de derechos humanos para los pueblos indígenas con enfoque diferencial. Le puede interesar: ["Indígenas dan ultimátum al Gobierno sobre consulta previa"](https://archivo.contagioradio.com/41094/)

Pedro Santiago Posada, Delegado para Asuntos Indígenas y Minorías Étnicas de la Defensoría del Pueblo, le dijo a la ONU que “En Colombia se están presentando violaciones a los derechos de los pueblos indígenas. Esto se debe a que en los territorios donde se ubican sus resguardos, generalmente **hay presencia de grupos armados, minería ilegal, cultivos ilícitos, y demás fenómenos de violencia**.”

De igual forma, Helfer Andrade, Representante Legal de la Asociación de Cabildos Indígenas del Chocó, Orewa, denuncia ante la ONU la creciente disputa territorial y expansión de grupos armado en el departamento del Chocó. Le puede interesar: ["Indígenas del Cauca denuncian hostigamientos por combates entre el ELN y Policía"](https://archivo.contagioradio.com/indigenas-de-cauca-denuncian-hostigamientos-por-combates-entre-el-eln-y-policia/)

Cabe resaltar que, en 2016, la Organización Nacional Indígena de Colombia ONIC, había denunciado la vulneración a los derechos humanos en contra del pueblo indígena Wounan Nonam que habitan en la cuenca baja del río San Juan.

Según la ONIC, **los líderes indígenas han sido detenidos por tropas la infantería de marina,** los enfrentamientos entre ejército y grupos armados han dejado en confinamiento y desabastecimiento a los indígenas y la restricción de la movilidad impuesta sobre ellos implica la des armonización del territorio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
