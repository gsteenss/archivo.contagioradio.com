Title: ¿Colombia necesita su propio modelo de Justicia transicional?
Date: 2015-02-17 20:41
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Conversaciones de paz en Colombia, dialogos de paz, ELN, FARC, habana, justicia, Paramilitarismo, paz
Slug: justicia-transicional-para-colombia
Status: published

###### Foto: Tomada de Internet 

<iframe src="http://www.ivoox.com/player_ek_4094659_2_1.html?data=lZWmlpuZfY6ZmKialJaJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPj0drS1dnFb8XZjIqflLXJtsWZpJiSpJjSb9qf0NHjy8nTaZOmjNfSw8fWqYzYxsfO1sqPt9DW08qYh5eWrtahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Federíco Andreu, Investigador y Jurísta] 

Para el abogado Federico Andreu, experto en **Derecho Internacional Humanitario**, las experiencias de "perdón y olvido" en América Latina han sido negativas, y ello debería ser un ejemplo para que Colombia evite transitar por ese camino.

La política de **"perdón y olvido"** fue promovida por sectores militares de gobierno con el fin de las dictaduras militares en el cono sur del nuevo continente en las décadas de 1960 y 1970. Ante ellos, organismos internacionales crearon un marco jurídico intentando encarar los crímenes cometidos en procesos de conflicto que evitara esas experiencias, y crearon el concepto de **Justicia Transicional**. Este marco jurídico busca abordar de manera integral el tratamiento de los crímenes, con perspectiva de construcción de sociedades con paz sostenible y duradera.

La Justicia Transicional se rige por 5 principios ([ver infograf](https://archivo.contagioradio.com/datos/los-cinco-principios-de-la-justicia-transicional/)ia):

-   1\. **[Justicia].** No son admistiables o indultables crímenes de guerra, lesa humanidad, ni genocidios, independientemente de la procedencia de los actores que los cometieron.

<!-- -->

-   2\. **[Verdad].** Debe haber esclarecimiento de la verdad de manera integral sobre los crímenes cometidos: Quienes los cometieron? Por qué razón? Cn qué motivos? Cómo? Basados en qué ideología? Con qué estructuras?

<!-- -->

-   3\. **[Reparación].** Las víctimas tienen derecho a la indemnización, compensación, satisfacción.

<!-- -->

-   4\. **[Garantías de no repetición].** En el proceso de paz deben incorporar políticas y hacerse reformas institucionales que garanticen que los crímenes no se vuelvan a cometer.

<!-- -->

-   5\. **[Depuración del Estado].** El proceso debe garantizar que aquellos funcionarios que han instigado, permitido o cometido crímenes, deben ser retirados de las instituciones de administración para evitar la impunidad de sus crímenes, y someterse a la justicia.

En este orden de ideas, ni los crímenes de Estado, cometidos por el Ejercito, e incluso organizaciones paramilitares, como desapariciones forzadas, ejecuciones extrajudiciales, homicidios de civiles o combatientes puestos fuera de combate, violencia sexual, tortura, etc., no son crímenes condonables; Como tampoco los crímenes cometidos por las guerrillas, que no sean conexos con el delito político.

Sin embargo, cómo señaló Iván Marquez, miembro del Secretariado de las FARC-EP y miembro plenipotenciario de la Delegación de Paz de esa guerrilla en Cuba, el concepto "Justicia Transicional" hace referencia a la transición de una dictadura a una democracia, y en Colombia no se puede hablar en esos términos. La existencia de un conflicto político, social y armado, tendría que dar necesariamente matices a la discusión en Colombia.

En el mismo sentido, Para el abogado Andreu, una vez desarrollado el proceso jurídico, cada Estado está en la capacidad de disponer de condenas alternativas a las tradicionalmente establecidas, generalmente relacionadas con la privación de la libertad.

En América Latina las experiencias han sido diversas. En El Salvador, por ejemplo, los Acuerdos de Chapultepec permitieron una amnistía general para miembros del Estado, Ejercito e insurgencia. Sin embargo, huno una depuración al interior de las Fuerzas Militares. Sin embargo, a pesar de que los gobiernos de centro derecha de este país centroamericano se negaron a realizar políticas de memoria durante sus gobiernos en la década de los 90, cuando el partido político de la antigua insurgencia, el FMLN llegó a la presidencia, empezó a promover políticas de memoria y verdad.

En el caso de Argentina, en que la política de "perdón y olvido" fue promovida por los gobiernos militares en las décadas de 1970 y 1980, casi 20 años después fue la misma sociedad la que exigió políticas de verdad, justicia y reparación. En Argentina han abierto procesos jurídicos contra militares con cargo en las dictaduras, condenando en los últimos 10 años a más 500 miembros del ejercito.

En Colombia en los últimos días se ha puesto sobre la mesa la posibilidad de que militares y empresarios enfrenten responsabilidades en el surgimiento y desarrollo del conflicto social y armado. Más allá de la responsabilidad por financiar la guerra, que preocupa al senador Rangel, la justicia transicional exigiría la verdad sobre la contribución y/o participación en la comisión de crímenes de lesa humanidad por acción u omisión. En los tribunales de Nüremberg, en Alemania -por ejemplo- tras la Segunda Guerra Mundial, fueron condenados empresarios que utilizaron trabajo esclavo de los campos de concentración para aumentar sus ganancias.

Más aún sería responsabilidad de los empresarios en un país como Colombia, en que se ha demostrado jurídicamente que el paramilitarismo existe como estrategia política, cuya aplicación es vigente por parte del Estado en los manuales de las fuerzas militares y de policía. En este sentido Andreu indica que los empresarios deben ser judicializados penalmente en Colombia, si se comprueba su responsabilidad en la generación y desarrollo del conflicto armado.

Para el abogado "Es obvio que si no se des-estructuran estos grupos paramilitares seriamente, es muy difícil para la guerrilla desmovilizarse, porque no tiene ninguna garantía para su vida, como ocurrió con la Unión Patriótica en la década de los 80".

Para que un proceso de paz sea estable y duradero en Colombia, las reformas de carácter estructural del Estado deberían analizar la "doctrina de la seguridad, evaluar si es compatible con las normas del estado social de derecho y el Derecho Internacional Humanitario, y si no, derogar esa legislación, y tomar todas las medidas prácticas que impida que se produzca o se reproduzca el paramilitarismo", concluyó Andreu.
