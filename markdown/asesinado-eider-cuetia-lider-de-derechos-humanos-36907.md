Title: Asesinado Eder Cuetia Conda, líder defensor de derechos humanos de Cauca
Date: 2017-02-27 12:59
Category: DDHH, Nacional
Tags: Cauca, Red Francisco Isaías
Slug: asesinado-eider-cuetia-lider-de-derechos-humanos-36907
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/eder-cuetia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [27 Feb. 2017] 

Este domingo, en el casco urbano de Corinto en Cauca, se conoció la noticia del **asesinato de Eder Cuetia Conda, Coordinador de la Seguridad Campesina del Corregimiento Los Andes, comunero del Resguardo Indígena de López Adentro, integrante de la Junta de Acción Comunal de la Vereda Siberia, y del Movimiento Político y Social Marcha Patriótica en el Departamento del Cauca.**

Según la denuncia, hombres encapuchados le propinaron varios impactos de arma de fuego en la cabeza al defensor de derechos humanos quien se encontraba departiendo con unas personas cerca de las 7:30 p.m.

Deivin Hurtado, integrante de la Red de Derechos Humanos Francisco Isaías Cifuentes -Red FIC-, aseguró que **“desde el año pasado han estado insistiendo en que este accionar obedece a grupos paramilitares, por más que la institucionalidad no haya querido reconocer** la existencia de estos grupos paramilitares y la persecución directa a líderes sociales”.

Para Hurtado, **si el Gobierno nacional no reconoce la existencia de esta problemática es muy difícil atacar el problema**, por lo que dicen que además de las diversas políticas gubernamentales que se han implementado, se debe tomar las medidas necesarias para atacar este fenómeno que se está rearmando en el departamento del Cauca y en todo el país. Le puede interesar: [117 líderes fueron asesinados durante 2016](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

**Los homicidios contra defensores de DD.HH han alcanzando 80 casos durante el 2016**, es decir un 22% más que en el 2015, siendo **Cauca el departamento que ocupa el primer lugar en cifras con 22 asesinatos**, según el reciente informe publicado por Somos Defensores.

Hasta la fecha no se cuenta con resultados sobre los asesinatos a líderes sociales y defensores de derechos humanos, pese a que la Fiscalía dijo estar investigando de la mano de un fiscal que está dedicado al tema de líderes sociales.

“En los pocos avances que hay, la Fiscalía se queda solo en los autores materiales y no se buscan los intelectuales, que realmente son los que generan el tema de la zozobra y quienes llevan la persecución a los líderes sociales” añadió Hurtado. Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

### **¿Qué es la seguridad campesina?** 

Ante las reiteradas violaciones a los derechos humanos, las comunidades campesinas se vieron en la obligación de organizarse en materia de seguridad, por ello, esta seguridad campesina comenzó a ejercer vigilancia propia desde el pasado mes de Octubre como mecanismo de protección a la ciudadanía. Le puede interesar: [Ante amenazas campesinos vigilan cabeceras municipales en Norte del Cauca](https://archivo.contagioradio.com/campesinos-realizan-vigilia-en-cabezas-municipales-en-norte-del-cauca/)

Lo que han hecho las comunidades campesinas es generar un mecanismo de protección realizando guardias en las esquinas de calles que han sido acordadas para vigilar, hacen rondas a pie o en motos, brindando seguridad a los pobladores y ayudan a preservar la vida de los pobladores.

<iframe id="audio_17247524" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17247524_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
