Title: Boletín informativo junio 16
Date: 2015-06-16 17:22
Category: datos
Tags: Actualidad Contagio Radio, Corte Interamericana de Derechos Humanos, Cuenca del Cacarica, desastre ambiental en Colombia, descontento con el proyecto hidroeléctrico del Quimbo, ex Ministro de Ambiente Manuel Rodríguez, Ley de Herencias y Plusvalía Ecuador, Transexuales logran realizar el cambio de género en su cédula de ciudadaní
Slug: boletin-informativo-junio-16
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4649781_2_1.html?data=lZuhm5ycdY6ZmKiakpqJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZaacYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Habitantes de la **cuenca del Cacarica**, en el departamento del Chocó, denuncian que **el Ejercito saboteó la visita de verificación** que realizaría la cancillería colombiana al territorio, para iniciar un proceso de **reparación integral** que sentenció la **Corte Interamericana de Derechos Humanos**. Habla **Bernardo Vivas**, Representante de **CAVIDA**.

-Proyecto de **Ley de Herencias y Plusvalía** mantiene movilizada a la ciudadanía ecuatoriana. Mientras **la oposición convocó** a una caravana de automóviles en lo que se denominó el **domingo negro**; los sectores afines a la reforma salieron a las calles el lunes 15 de junio bajo la consigna **\#YoDefiendoMiRevolución**. Habla el historiador **Juan de Paz y Miña**.

-Colombia vive un hecho histórico para la comunidad **LGBTI** en materia de derechos humanos. Por **primera vez**, varias personas **transexuales** logran realizar el **cambio de género en su cédula** de ciudadanía, por medio de un trámite notarial sin necesidad de someterse a una supuesta disforia de género para que fuese aprobado

-En el **Quimbo**, departamento del **Huila**, se han desarrollado varios actos culturales en defensa del territorio y en contra de la hidroeléctrica que allí se construye. El plantón cultural que se adelanta desde horas de la mañana frente a la gobernación departamental, busca **incorporarse a las actividades san pedrinas** y al folclor regional sin dejar de expresar el **descontento** con el proyecto hidroeléctrico del Quimbo. **Miller Dussan** vocero de la **Asociación de Víctimas del Quimbo**.

-De acuerdo con el ex Ministro de Ambiente **Manuel Rodríguez**, el desastre ambiental en Colombia **no** obedece unicamente a las **acciones bélicas de las guerrillas** en contra de la infraestructura petrolera, sino que **existen también responsabilidades** por parte de otros sectores de la economía y del mismo gobierno nacional.
