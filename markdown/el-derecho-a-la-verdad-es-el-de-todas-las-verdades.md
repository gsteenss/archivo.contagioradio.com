Title: El derecho a la verdad es el de todas las verdades
Date: 2019-08-29 15:20
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: JEP, Plan 002811, verdad
Slug: el-derecho-a-la-verdad-es-el-de-todas-las-verdades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líder-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de la Verdad 

La pretensión de sectores militares de constituir un relato o versión institucional sobre su verdad del conflicto armado tiene un trasfondo de negacionismo que puede ser un acto fallido. El Plan 002811 es así interpretado como una nueva intentona de oficializar la historia. Su nombre, Plan ya indica la pretensión de un algo, y ese algo es la negación.

Tratar de expresar un relato institucional exclusivamente castrense, es decir, específico de la milicia y la fuerza, indica que las operaciones armadas y sus elementos doctrinales eran de su propia competencia, se regulaban autónomamente, de algún modo sin subordinación a lo civil. Así, implícitamente todas las políticas de seguridad desde la llamada ley de los caballos muertos, la del Estatuto de Seguridad, la de la inSeguridad Democrática, y en general, las prácticas de los Estados de Excepción quedan como de su propia responsabilidad, así fueran decisiones del ejecutivo, aprobadas algunas de ellas por el Congreso, y otras pasando por controles institucionales.

Sin pretenderlo,  las hace ver así mismas como un poder real, fáctico, con formalidades legales como un poder autónomo. Y, ese poder, por ejemplo, ¿cómo se identificó en la contra-toma del Palacio de Justicia, o en las operaciones rastrillo en zonas rurales?. Si el Plan es parte de una estrategia de negación, omitirá identificar el desarrollo de la estrategia militar encubierta (paramilitar) a través de la que se ocultaron la mayoría de crímenes de lesa humanidad, crímenes de guerra como parte de un plan criminal en que muchos aseguraron su riqueza, otros se hicieron a ella, y otros garantizaron su poder político, económico, militar..

Un segundo elemento de gravedad es que el Plan comprende marcos comprensivos y valorativos que pueden orientar la comparecencia ante la JEP y la CEV, limitando   la libertad de expresión del compareciente o un siendo un sutil llamado a la debida obediencia  y propiciando un ocultamiento en las responsabilidades de mando en el desarrollo del uso de la fuerza y planes represivos que se hicieron criminales.

El plan es la apuesta por una enunciación oficial o institucional castrense que sería comprensible si la versión estuviera lejos de ser un manual de instrucciones, y en particular, a que el mismo estuviera apuntando a producir relatos con contextos de enunciación de verdad, más que de tergiversación, legitimación y omisiones estructurales, para un propósito estratégico: la negación de responsabilidad

Este Plan se suma a los recortes ya existentes en el punto 5 del Acuerdo. El primero de ellos de la mano de Santos y Timochenko, cuando estos sustrajeron de la competencia de la Jurisdicción Especial de Paz, JEP, la eventual comparecencia de los expresidentes, propósito salvaguardado por el liberal Humberto De la Calle Lombana. A este corte se sumó meses después, en la madrugada del día de la firma del Teatro Colón, la sustracción de los militares de la responsabilidad de cadena de mando, conforme a lo contemplado en el Estatuto de Roma.

Así es cierto que los miedos a las verdades están patentes y otros son latentes, pero esos miedos son personales, y arrastran a las instituciones, entre ellas la castrense. Y tal, como Álvaro Uribe, astuta y perversamente lo ha hecho, se usan los mecanismos y las instituciones para encubrirlos, legitimarlos u omitirlos. El asunto es que las verdades históricas y penales, más allá del negacionismo y de la propia historia personal que pretende ocultarse, generan graves consecuencias a las propias instituciones que pretenden mostrarse incólumes, fuera de pecado y a sus individuos.

Si las repeticiones ocurren es debido al naufragio de las verdades por los ocultamientos a las generacionales, a la ausencia de reconocimiento de responsabilidades, y de inclusión conciente en la historia de un país de los horrores y sus dolores. Solo cuando lo aterrador puede verse es posible superarlo. Seguir esquivando las verdades, haciendo burlas a la sociedad, será de algún modo, el detonante de nuestra continuidad en el ejercicio de la violencia. Una violencia que solo puede desterrarse, si por una única vez, los sectores más poderosos del establecimiento se arriesgan al proceso de construcción de las verdades de las responsabilidades en la historia de esa conflictividad, que arrojó tantos millones de víctimas.

##### Ver más[columnas de opinión de Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 
