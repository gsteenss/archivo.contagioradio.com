Title: Argelia, hasta mil personas desplazadas por combates y el olvido del Estado
Date: 2020-03-11 15:42
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Argelia, combates
Slug: argelia-hasta-mil-personas-desplazadas-por-combates-y-el-olvido-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Desplazados-en-Argelia.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: ASCAMTA {#foto-ascamta .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el pasado martes 3 de marzo, grupos armados sostienen enfrentamientos en Argelia, Cauca, generando desplazamientos masivos de hasta mil campesinos. Según habitantes de la zona, los combates se enmarcan en una lucha por el control del territorio, en medio de uno de los departamentos más militarizados del país. (Le puede interesar: ["Más medidas integrales y menos accionar policial y militar: ONU"](https://archivo.contagioradio.com/onu-mas-medidas-integrales-menos-accionar-policial-militar/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Argelia, el sufrimiento de lo que significa hacer trizas la paz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explicó Guillermo Mosquera, integrante de la Asociación Campesina de Trabajadores de Argelia (ASCAMTA), el municipio fue fundado por personas que buscaban refugio de la violencia en la década de los 30's y 40's, luego llegó el narcotráfico y después del 2000, el paramilitarismo. Aunque el Acuerdo de Paz significó calma para la región, Mosquera señaló que desde hace tiempo se presentan combates entre estructuras armadas ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para él, "Argelia padece el sufrimiento de lo que significa hacer trizas la paz", agravado por una presencia poco efectiva de las instituciones Estatales y la confluencia de muchos intereses económicos por ser una zona geoestratégica. Su localización, cercano al centro del Macizo Colombiano, limítrofe con territorios que dan salida al mar y rico en minerales así como en agua explican lo 'estratégico' del territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Incluso hubo hasta mil personas desplazadas"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mosquera afirmó que desde el 3 de marzo, se iniciaron combates en los sectores de Plateado y La Emboscada presuntamente entre el ELN y una estructura que se autodenomina FARC-EP, frente Carlos Patiño. Situación por la que se han visto afectadas "muchas veredas", y ha generado el desplazamiento de familias hacia el casco urbano de Argelia, a otros municipios e incluso, a ciudades como Popayán y Cali.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo al líder campesino, los enfrentamientos forman parte de una puja por tener el control territorial, razón por la que los combates cambian de lugar según un grupo u otro gane terreno. Este tipo de confrontaciones hace que las familias salgan de sus casas para resguardarse, pero retornan cuando los grupos se desplazan, lo que ha hecho difícil saber el número exacto de desplazados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, Mosquera aseguró que en uno de los censos realizados por ASCAMTA se registraron 740 personas que salieron de su casa por temor a enfrentamientos, "incluso hubo hasta mil desplazados, solo que por los prontos retornos nunca quedan registradas". Por la situación, se declaró toque de queda y se suspendieron las actividades en las escuelas rurales.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ArielAnaliza/status/1235673202975805440","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ArielAnaliza/status/1235673202975805440

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **¿Y el papel del Estado?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En cuanto a la Fuerza Pública, el integrante de [ASCAMTA](https://twitter.com/ASCAMTA) afirmó que las personas no confían en los uniformados "porque históricamente ellos se han relacionado con grupos paramilitares". Respecto a los recientes combates, además, dijo que no se explicaba cómo en uno de los municipios más militarizados del país y que solo tiene dos entradas se presentan enfrentamientos de grupos de más de 100 hombres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, cuestionó que el Estado no ha llegado de forma suficiente con toda su institucionalidad para ofrecer garantías de vida digna para las personas. (Le puede interesar: ["Ola de homicidios no se detiene en Argelia, Cauca en 2019"](https://archivo.contagioradio.com/ola-homicidios-argelia-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Habitantes de Argelia, Cauca, se cansaron de ser "trinchera" de la policía](https://archivo.contagioradio.com/la-poblacion-de-argelia-cauca-se-canso-de-ser-trinchera-de-la-policia/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
