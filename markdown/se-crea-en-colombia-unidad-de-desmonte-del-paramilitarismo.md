Title: Los retos de la Unidad de desmonte del Paramilitarismo
Date: 2017-05-30 18:08
Category: Nacional, Paz
Tags: proceso de paz, Unidad de desmonte del Paramilitarismo
Slug: se-crea-en-colombia-unidad-de-desmonte-del-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 May 2017] 

Dos son los decretos que firmó el Presidente Juan Manuel Santos que avanzarían en la búsqueda de Verdad Justicia y Reparación para las víctimas del conflicto armados en Colombia. El primero de ellos consiste en el **Sistema de Garantías de seguridad para el ejercicio de la política y el segundo es la creación de la Unidad que pretende el desmonte del paramilitarismo**.

El primer decreto es el 895 de 2017 que conforma el Sistema Integral de Seguridad para el Ejercicio de la Política y que **pretende asegurar la promoción y protección de las personas, el respeto por la vida, la libertad de pensamiento y opinión**, para así fortalecer y profundizar la democracia. Le puede interesar: ["Califican como positivo decretos del Gobierno vía Fast Track"](https://archivo.contagioradio.com/califican-como-positivos-los-decretos-del-gobierno-via-fast-track/)

De acuerdo con Diego Martínez, defensor de derechos humanos y abogado que acompañó el proceso de paz entre el Gobierno y de las FARC-EP, este decreto permite que se **creen mecanismos para proteger a defensores de derechos humanos y a comunidades afectadas por el conflicto armado**.

 “yo creo que se abre la posibilidad de concentrar la acción de Estado en un mecanismo sistémico que permita desarrollar instrumentos preventivos, pero también de respuesta rápida en los territorios” afirmó Martínez. Le puede interesar:["Víctimas exigen al Congreso mantener esencia de los Acuerdos"](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/)

El segundo decreto es el 898 de 2017, por el cual se crea una Unidad Especial para el desmonte de organizaciones paramilitares, que a su vez investigará quiénes están detrás de los asesinatos a líderes del movimiento social y defensores de derechos humanos en el país. De igual forma se crearán tres fiscalías delegadas: **contra el crimen organizado, sobre la seguridad ciudadana y contra las finanzas ilegales.**

Diego Martínez señaló que, para la creación de esta Unidad, se dialogó directamente con el Fiscal General de la Nación que tenía algunos reparos, sin embargo, el abogado afirmó que “es un paso significativo en la medida en que se va a contar con un cuerpo robusto para **investigar las agresiones contra integrantes del movimiento social, defensores y víctimas**”.

### **La magnitud del paramilitarismo en Colombia** 

Recientes informes de varias organizaciones de Derechos Humanos han dado cuenta de la presencia de grupos paramilitares post desmovilización o "neo paramilitares" a los que se les atribuye el asesinato de varios líderes sociales en el país y que tendrían presencia en **12 departamentos del país.**

Uno de los grupos sobre los cuales se ha logrado documentar presencia en 7 departamentos es el llamado "clan del golfo" o "Autodefensas Gaitanistas de Colombia". Las **AGC han demostrado su interés de apoderarse de varias regiones en Chocó y Antioquia con la idea de controlar los negocios del narcotráfico** y copar los territorios dejados por las FARC. Le puede interesar: ["300 paramilitares impiden la movilidad de comunidades en Putumayo"](https://archivo.contagioradio.com/300-paramilitares-impiden-la-movilidad-de-comunidades-en-putumayo/)

Este fin de semana se conoció un video que da cuenta de su estrcutura armada, que funciona como ejército y que incluso cuenta con bases militares que albergan a unos 60 hombres. Aunque la Comunidad de Paz de San José de Apartadó ha denunciado que **hay bases con hasta 200 hombres asentados en los caseríos de esa región de Antioquia.**

Sin embargo, ese tipo de operaciones se endilgan a 3 organizaciones entre las que se encuentran **"Los puntilleros", "Los pelusos"** y otros con operaciones en diversos sectores del territorio colombiano. Le puede interesar:["Paramilitares estarían detrás de daños y robos en Buenaventura"](https://archivo.contagioradio.com/paramilitares-estarian-detras-de-danos-y-robos-en-buenaventura/)

### **Las tareas urgentes de la Unidad de Desmonte del Paramilitarismo** 

Organizaciones de Derechos Humanos han insistido en que el desmonte del paramilitarismo es una tarea integral del Estado colombiano que pasaría por la depuración de las Fuerzas de Seguridad, **el descubrimiento de los archivos clasificados del Estado y el desmantelamiento del narcotráfico como principal fuente de financiación. **Le puede interesar:["Hay que desclasificar los archivos de seguridad para conocer la verdad"](https://archivo.contagioradio.com/hay-que-descalsificar-los-archivos-de-suguridad-para-cononcer-la-verdad/)

Además se afirma que dicha unidad deberá tener autonomía investigativa y administrativa para garantizar que la fiscalía general pueda seguir desarrollando las labores que hasta el momento ha tenido, e incluso **no interfiera en las tareas que se puedan desprender de dicha unidad** que también tendría que investigar la participación de terceros financiadores de ese tipo de actividades. [Leer el decreto 898](http://es.presidencia.gov.co/normativa/normativa/DECRETO%20898%20DEL%2029%20DE%20MAYO%20DE%202017.pdf)

<iframe id="audio_18989551" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18989551_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
