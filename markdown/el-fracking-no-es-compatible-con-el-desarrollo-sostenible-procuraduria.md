Title: El fracking no es compatible con el desarrollo sostenible: Procuraduría
Date: 2020-11-06 08:47
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Consejo de Estado, fracking, No al Fracking, Procuraduría General de la Nación
Slug: el-fracking-no-es-compatible-con-el-desarrollo-sostenible-procuraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/No-al-fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**La Procuraduría General de la Nación solicitó ante el Consejo de Estado la nulidad de dos normas** emitidas por el Gobierno que establecieron los criterios y procedimientos para la exploración y explotación de hidrocarburos en yacimientos no convencionales, entre ellos el *fracking*. (Le puede interesar: [No al fracking: Deuda de campaña que Duque debe cumplir](https://archivo.contagioradio.com/no-al-fracking-deuda-de-campana-que-duque-debe-cumplir/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fcarrilloflorez/status/1323416330205302785","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fcarrilloflorez/status/1323416330205302785

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La solicitud se sustenta en que **el *fracking* y otras técnicas de extracción de yacimientos no convencionales, no son compatibles con los principios de precaución y desarrollo sostenible previstos en la Constitución Política.** En ese sentido, el ente de control solicitó al Consejo de Estado declarar nulos el Decreto 3004 de 2013 y la Resolución 90341 de 2014, que reglamentaron dichas técnicas. (Le puede interesar: [Senado le dice No al fracking pero todavía no se puede cantar victoria](https://archivo.contagioradio.com/senado-le-dice-no-al-fracking-pero-todavia-no-se-puede-cantar-victoria/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PGN_COL/status/1323409478604214273","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PGN\_COL/status/1323409478604214273

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al rendir concepto ante la Sección Tercera del Consejo de Estado, la Procuraduría pidió aplicar el principio de precaución teniendo en cuenta que **existe peligro de daño al medio ambiente, en especial, la contaminación de aguas subterráneas y superficiales, el aumento de la actividad sísmica en los lugares donde se desarrolla la práctica, y un potencial daño a la salud del ser humano.** (Lea también: [Aguas ultra-profundas: la nueva frontera del extractivismo petrolero en Colombia](https://archivo.contagioradio.com/aguas-ultra-profundas-la-nueva-frontera-del-extractivismo-petrolero-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, la Procuraduría alertó sobre la necesidad de declarar la nulidad de estas normas, ya que, **de llegar a materializarse alguno de los riesgos que trae consigo el *fracking*, el daño resultaría irreversible,** teniendo en cuenta que Colombia no cuenta con una línea base confiable de información ambiental e hidrológica, una capacidad de monitoreo y control para ello, ni acceso a la información ambiental, geológica, geofísica, geoquímica y sísmica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, **la Procuraduría señaló que en caso de contaminación hídrica, esta repercutiría en daños irreversibles a los ecosistemas, fauna y flora del país, sin descartar el potencial daño a la salud del ser humano,** no solo de quienes laboran y se exponen a las sustancias que hacen parte del proceso de fracturación, sino de quienes habitan en cercanía de las zonas de explotación. (Lea también: [Proyectos extractivos representan un mayor riesgo de violencias contra las mujeres: ONG´s](https://archivo.contagioradio.com/proyectos-extractivos-representan-un-mayor-riesgo-de-violencias-contra-las-mujeres-ongs/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, la solicitud se sustentó también jurídicamente, frente a lo cual la Procuraduría sostuvo que las normas que regulan estas técnicas de extracción, **son contrarias a lo dispuesto en los artículos [79](https://www.constitucioncolombia.com/titulo-2/capitulo-3/articulo-79) y [80](https://www.constitucioncolombia.com/titulo-2/capitulo-3/articulo-80) de la Constitución, y al artículo 1° de la Ley 99 de 1993,** que incorporan los principios de precaución y de desarrollo sostenible en el manejo de las decisiones que puedan afectar el medio ambiente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pedido de la Procuraduría se da en el marco de una demanda interpuesta por el ciudadano Esteban Antonio Lagos quien solicita la nulidad del Decreto 3004 de 2013 y la Resolución 90341 de 2014 expedidos por el Gobierno Nacional, fundado en muchas de las razones de índole científico y jurídico, recogidas por la propia Procuraduría en su concepto, quien se sumó al pedido de nulidad expuesto por el accionante.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
