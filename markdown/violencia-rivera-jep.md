Title: Ficha clave para entender la violencia en Rivera, Huila, llega a la JEP
Date: 2019-07-23 17:04
Author: CtgAdm
Category: Judicial, Paz
Tags: Gil Trujillo, Huila, JEP, masacre de rivera
Slug: violencia-rivera-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Nación  
] 

El exconcejal Gil Trujillo Quintero, quien ocupo el cargo desde la representación del municipio de Rivera, en el departamento del Huila, fue aceptado en la Jurisdicción Especial para la Paz (JEP) para aportar verdad en el caso de la masacre de los cabildantes de esa jurisdicción. La noticia fue bien recibida por las familias de las víctimas, quienes dijeron que la aceptación del sometimiento voluntario de Trujillo ante la JEP podría ayudarles a 'armar el rompecabezas' de lo que pasó durante el conflicto armado.

### **Gil Trujillo parece tener intenciones de aportar la verdad**

El ex Concejal fue condenado en diciembre de 2009 a 40 años de prisión por la justicia ordinaria, bajo el delito de homicidio agravado múltiple en concurso homogéneo simultáneo. Esta condena fue posible gracias a testimonios de perpetradores de la masacre, quienes testificaron en contra de Trujillo, sindicándolo de ayudarles a cometer el hecho. (Le puede interesar: ["Sin renunciar a la verdad, habitantes de Trujillo resisten desde la memoria"](https://archivo.contagioradio.com/sin-renunciar-a-la-verdad-habitantes-de-trujillo-resisten-desde-la-memoria/))

Posteriormente, en noviembre de 2017, Trujillo Quintero pidió ser acogido ante la JEP; según explicó **Marta Aguirre, directora de la Fundación Sonrisas de Colores y representante de víctimas,** la Jurisdicción tomó tiempo en decidir sobre el sometimiento ante las dudas sobre la verdad que podría entregar el político, pero tras sus declaraciones sobre la intención de aportar verdad plena, decidió aceptarlo.

### **¿Cuál es su verdad?**

Aguirre afirmó que en Rivera, además de la masacre, fueron asesinados dos concejales, un alcalde, y otros integrantes de diferentes partidos políticos. En el caso de Trujillo, su verdad podría estar orientada a esclarecer por qué asesinaron a los Concejales de Rivera y quién dio la orden para hacerlo; asimismo, **se espera que su testimonio motive a que otras personas que participaron en el conflicto aporten a la verdad** para finalmente "armar el rompecabezas sobre lo que pasó en el conflicto".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
