Title: Madres de Soacha rechazan 'verdades a medias' de militares en la JEP
Date: 2018-08-13 15:19
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, General Mario Montoya, JEP, madres de soacha
Slug: madres-soacha-rechazan-verdades-medias-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-13-a-las-11.36.37-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @\_danicallejas] 

###### [13 Ago 2018] 

Tras cumplirse la primera audiencia por el Caso 003 de la Jurisdicción Especial para la Paz (JEP) sobre ejecuciones extrajudiciales, las Madres víctimas de Falsos Positivos (MAFAPO), **se mostraron inconformes** por los testimonios que los militares entregaron al tribunal, que consideraron estar  hechos a modo de libreto y no contando lo que realmente sucedió.

### **Los militares están leyendo todo lo que tienen que decir, ¿qué verdad es esa?** 

En la audiencia citada por la Sala de Reconocimiento y Responsabilidad de la JEP fueron llamados un total de **15 miembros de las Fuerzas Militares** entre los cuales hay 2 oficiales, 1 suboficial y 11 soldados, quienes responderán por los delitos de **concierto para delinquir, homicidio y desaparición forzada. ** (Le puede interesar: ["JEP pide información de 9 brigadas implicadas en ejecuciones extrajudiciales"](https://archivo.contagioradio.com/9-brigadas-implicadas-en-ejecuciones-extrajudiciales/))

Para Jacqueline Castillo, hermana de Jaime Castillo Peña, quien fue asesinado en 2008 y presentado como  una baja en combate, los militares no quieren ofrecer verdad sobre lo ocurrido porque **están leyendo sus testimonios y todo "se los tienen escrito".** En ese mismo sentido, Castillo aseguró que el fin último de la JEP es conocer la verdad de lo ocurrido, y  en este tipo de casos **"hubo grandes cabezas detrás de las ejecuciones"**  que espera se den a conocer en el transcurso de las audiencias.

### **"Se está hablando de crímenes de guerra, pero son crímenes de Estado"** 

La primera audiencia, del caso 003 de la JEP, tuvo que ver con el asesinato y desaparición de los jóvenes **Julio César Mesa Vargas, Jader Andrés Palacios Bustamante, Víctor Fernando Gómez Borrero, Jhonatan Orlando Soto Bermúdez y Diego Alberto Tamayo Garcerá;** por esta razón, el Tribunal de paz quiso que asistieran solo los familiares de los 5 jóvenes.

Sin embargo,  la MAFAPO exigió que a la audiencia debían ingresar todas las madres que fueron víctimas de ejecuciones extrajudiciales en Soacha, Cundinamarca, y Bogotá. Tras lograr el ingreso al recinto de la JEP, **las mujeres se mostraron inconformes por el traslado de estos casos de la justicia ordinaria al sistema de justicia transicional.**

Según la hermana de Jaime Castillo, en la JEP **se está hablando de crímenes de guerra, cuando en realidad se trata de crímenes de Estado.** Razón por la cual, considera que ese tribunal no tiene competencia para juzgar estos delitos, y en lugar de ello, tiene la esperanza de que su caso llegue ante la Corte Interamericana de Derechos Humanos **(Corte IDH)**.

<div class="stream-item-footer">

Los casos por los que se está juzgando a estos militares tuvieron lugar entre los meses de enero y agosto de 2008, fechas en las que era comandante del Ejército el **General Mario Montoya,** quien también se acogió a la JEP y podría esclarecer los casos de ejecuciones extrajudiciales de jóvenes de Soacha y Bogotá. (Le puede interesar: ["Los casos por los que debería responder el Gral. Montoya ante la JEP"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

<iframe id="audio_27780073" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27780073_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>
