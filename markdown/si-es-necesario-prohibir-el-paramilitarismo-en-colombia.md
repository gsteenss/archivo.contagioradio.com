Title: Es necesario prohibir el paramilitarismo en Colombia: Iván Cepeda
Date: 2017-08-22 13:58
Category: DDHH, Nacional
Tags: acuerdos de paz, constitucion, paramilitares, prohibición de paramilitarismo
Slug: si-es-necesario-prohibir-el-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Captura-de-pantalla-2017-07-11-a-las-12.16.56-p.m.-e1499793834908.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomada de Google] 

###### [22 Ago 2017] 

Frente al proyecto que está en el Congreso de la República, que busca prohibir de forma constitucional la creación de grupos paramilitares, algunos sectores de la sociedad incluidas las víctimas de crímenes de Estado, han manifestado que **es necesario que se ataque el problema del no reconocimiento del fenómeno paramilitar** para garantizar medidas de no repetición en el futuro.

Según el senador Iván Cepeda, se debe estudiar con detenimiento los argumentos y el fondo de la discusión para incluir esta prohibición en la Constitución. Indicó que **“el paramilitarismo es un flagelo que ha sacudido la historia del país** desde hace décadas como una política del desdoblamiento del Estado en organizaciones que han sembrado el terror”. (Le puede interesar: ["Política de miedo" de paramilitarismo debe ser afrontada con movilización social"](https://archivo.contagioradio.com/paramilitarismo-es-una-politica-del-miedo-impuesto/))

El senador recordó que en Colombia **el fenómeno del paramilitarismo no está prohibido ni existe como delito** en la medida en que “en el código penal no hay una disposición formal que diga que formar grupos paramilitares está prohibido”. Igualmente señaló que es falaz también argumentar que ponerlo como prohibición en la Constitución implica un reconocimiento de las responsabilidades por parte del Estado.

Para Cepeda, **lo que existe es un temor de algunos sectores de la sociedad** ante la posibilidad de buscar responsabilidades por la historia del paramilitarismo. Dijo también que se debe señalar que “el Estado no solo ha tolerado la existencia del paramilitarismo sino que ha creado leyes, decretos e instituciones que han favorecido la creación y las acciones de los paramilitares”. (Le puede interesar: ["Hay un disfraz del desmonte del paramilitarismo": Mesa Nacional de Víctimas"](https://archivo.contagioradio.com/victimas-de-la-mesa-nacional-reclaman-proteccion-ante-amenazas-contra-su-vida/))

**Si no se aprueba la prohibición del paramilitarismo se estaría incumpliendo el acuerdo de paz con las FARC**

Iván Cepeda recordó que este asunto **está anunciado de manera expresa en el acuerdo de paz como medida de no repetición**. Dijo además que, si bien esta prohibición no soluciona el problema de fondo, “si reconoce la existencia del problema que desde el Estado siempre se ha negado y desconocido”.

Finalmente, el senador recordó que “no se debe pasar por encima de los hechos y hay que esclarecer y asumir las responsabilidades de este fenómeno”. Igualmente afirmó que “hay personas a las que no les gusta que se ponga en evidencia la existencia del paramilitarismo y **se necesita favorecer los derechos de las víctimas**”.

<iframe id="audio_20466388" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20466388_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
