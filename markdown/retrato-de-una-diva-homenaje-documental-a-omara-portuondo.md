Title: "Retrato de una diva" homenaje documental a Omara Portuondo
Date: 2015-12-18 12:01
Author: AdminContagio
Category: 24 Cuadros, En clave de son
Tags: Buenavista social club, Documentales son cubano, Omara Portuondo
Slug: retrato-de-una-diva-homenaje-documental-a-omara-portuondo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/omara-cantando-c2a9-livio-deldado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:wmft.com 

##### [18 Dic 2015] 

En el marco de la edición 31 del Festival Jazz Plaza, se estrenó este jueves en la Habana el documental: "Retrato de una diva", una producción de la realizadora Ileana Rodríguez, que busca hacer un acercamiento a la cantante de son y boleros Omara Portuondo, desde un retrato personal que muestra la importancia de su trabajo para la música cubana.

El audiovisual sobre la "Diva del Buenavista Social Club" fue filmado durante las sesiones de estudio que conllevó la grabación y monaje de "Magia negra", el más reciente trabajo de la artista lanzado bajo el sello de Producciones Colibrí, capturando fielmente el proceso de creación del disco.

De acuerdo con la directora, el objetivo del documental es transmitir la "atmósfera creada por Omara y sus músicos mientras trabajaban y así conocer más sobre su labor", complementado por los testimonios de varios artistas cercanos a la vida de la cantante, siendo algunos de ellos los instrumentistas del disco.

Aunque la artista de 85 años de edad, es reconocida en casi todo el mundo por su participación en el documental y disco "Buena Vista Social Club" del director Win Wenders, la carrera de Portuondo es mucho mas extensa, remontando su debut al año 1958 con un trabajo homónimo de su nueva producción, donde interpretó temas de autores cubanos como Adolfo Guzmán, Orlando de la Rosa y César Portillo de la Luz; y norteamericanos como Duke Ellington, Harold Arlen y Johnny Mercer.

<iframe src="https://www.youtube.com/embed/YRvuefiSHnc" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
