Title: Desaparición Forzada, una lucha por la verdad que persiste
Date: 2018-09-12 17:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: descaparición, víctimas
Slug: desaparicion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Desaparición-forzada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Sept 2018]

Colombia conmemora 45 años del día de la desaparición forzada. El Centro Nacional de Memoria Histórica asegura que han sido **82.998 personas víctimas de la desaparición forzada** hasta Febrero del 2018.

Erik Arellana miembro de H.I.J.O.S , Gloria Gómez de la fundación Asfaddes y Luz Helena de Madres Caminando por la Verdad, son familiares de víctimas de desaparición forzada y hoy integran organizaciones sociales que lideran la **lucha contra este crimen de lesa humanidad. **

Según las víctmas, tanto los medios de comunicación, como el gobierno y la ciudadanía han sido indiferentes a esta problemática, y las familias de las víctimas se encuentran inconformes “**los medios de la comunicación son cómplices de la impunidad**, de la persecución a los familiares que buscamos la verdad, han desinformado e invisibilizado la realidad de la problemática” Gloria Gómez de ASFADDES. (Puede interesarle: [Min. Hacienda tiene "engavetado" presupuesto para buscar a los desaparecidos](https://archivo.contagioradio.com/ministerio-de-hacienda-tiene-engavetado-presupuesto-para-buscar-los-desaparecidos/))

### **Víctimas de desaparición forzada piden solidaridad para encontrarlos ** 

Por ello, cada 30 de Agosto ellos y ellas insisten en hacer un llamado, no solamente a la institucionalidad estatal que es la responsable de la búsqueda y hallazgo de las personas dadas por desaparecidas, sino que también llaman a la sociedad en general a exigir que este crímen llegue a su fin para poder hablar de una paz estable y duradera.

<iframe id="audio_28459656" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28459656_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)] 
