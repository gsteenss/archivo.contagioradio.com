Title: Empresas bananeras responsables de crímenes de lesa humanidad
Date: 2017-02-02 17:21
Category: Judicial, Otra Mirada
Tags: AUC, Empresas Bananeras, Fiscalía General de la Nación, Paramilitarismo en el Urabá
Slug: empresas-bananeras-responsables-de-crimenes-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/bananeras-e1486074005794.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RT] 

###### [2 Feb 2017 ] 

La Fiscalía General de la Nación, declaró como crimen de lesa humanidad la financiación de empresas bananeras a grupos paramilitares en Urabá, desde el año 1996 hasta el 2004. El ente acusador aseguró que se trató de **“una conducta sistemática y generalizada en varias regiones del país”.**

La Fiscalía logró determinar que los empresarios bananeros “dieron una importante suma de dinero” para el sostenimiento del Frente Arlex Hurtado del Bloque Bananero de las AUC, esto gracias a los testimonios de Raúl Emilio Hasbún Mendoza alias 'Pedro Bonito' y de Hebert Veloza alias HH, quienes además hicieron **“señalamientos directos que revelan nombres de empresarios bananeros de la zona de Urabá”** puntualiza la resolución.

Esta decisión sin precedentes, puso en evidencia que el gremio bananero hizo dichas transacciones a través de las cooperativas convivir, legales en ese entonces y determinó que los empresarios son también “directos responsables” de los homicidios, desplazamientos y desapariciones forzadas, violencias basadas en género, reclutamiento ilícito y torturas que **“afectaron de forma indiscriminada a la población”.**

El fiscal Carlos Villamil manifestó que dicha financiación “no sólo garantizó el funcionamiento, permanencia y crecimiento del grupo armado al margen de la ley” sino que con dichos recursos **“se compraron las armas” con las que se ejecutaron los delitos antes mencionados**. (Le puede interesar: [No repetición: el caso de paramilitares y mercenarios](https://archivo.contagioradio.com/paramilitares-y-mercenarios/)).

Por último, la resolución firmada por el fiscal Villamil destaca que la decisión puede servir como un referente “para dar igual tratamiento a empresarios” que hayan contribuido “por convicción o conveniencia” **al sostenimiento de grupos armados presentes en esta y otras regiones del país, desde la década de los 70.**

###### Reciba toda la información de Contagio Radio en [[su correo]
