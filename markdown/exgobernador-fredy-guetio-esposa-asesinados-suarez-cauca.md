Title: Exgobernador indígena Fredy Güetio y su esposa fueron asesinados en Suarez, Cauca
Date: 2020-10-13 13:57
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Asesinato de indígenas, Cauca
Slug: exgobernador-fredy-guetio-esposa-asesinados-suarez-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Fredy-Guetio-Zambrano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Fredy Güetio/ ACIN

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el Tejido de Defensa de la Vida y los Derechos Humanos de los [Cabildos Indígenas del Norte del Cauc](https://twitter.com/ACIN_Cauca)a (ACIN), en la tarde del pasado 12 de octubre fue asesinado el exgobernador indígena **Fredy Güetio Zambrano de 51 años y su esposa Reina Mera** en zona rural del Resguardo de Cerro Tijeras en el municipio de Suarez, Cauca. Los sucesos se dieron mientras la Minga se desplazaba hacia Cali con la intención de exigir garantías al Gobierno de la vida y los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la ACIN, los hechos ocurrieron al medio día cuando hombres armados dispararon contra la pareja cuando llegaban de un trabajo familiar en la vereda Agua Clara Resguardó indígena de Cerro Tijeras. Sus cuerpos fueron llevados hasta el municipio de Santander de Quilichao, mientras se esclarecen los responsables y las motivaciones de este crimen. [(Le puede interesar: 528 años después continúa el exterminio para razas y sectores sociales)](https://archivo.contagioradio.com/528-anos-despues-continua-el-exterminio-para-razas-y-sectores-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"No pueden seguir sembrando la muerte mientras los pueblos están movilizados en defensa de sus derechos"** señalan desde la ACIN a propósito de que los hechos sucedieron en medio de la movilización de la minga que dentro de sus exigencias al Gobierno exigen el respeto y garantías a la vida de sus comunidades. [(Lea también: Avanza la Minga en defensa de la vida)](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fredy Güetio Zambrano de 51 años se suma al registro de más de 269 indígenas que han sido asesinados desde el año 2016 y los más de 167 durante la presidencia de Iván Duque con fecha de corte en junio de este año según Indepaz. Las autoridades indígenas señalan que con Fredy y su esposa, Reina Mera, **ya son 76 los comuneros indígenas asesinados en el norte del Cauca, agregan que 6 de estos tenían un perfil de liderazgo.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
