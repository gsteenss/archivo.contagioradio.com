Title: La justicia restaurativa, una forma de tejer esperanza
Date: 2019-04-09 17:17
Author: CtgAdm
Category: Expreso Libertad
Tags: Justicia restaurativa, paz, Reconciliación
Slug: justicia-restaurativa-tejer-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP- Luis Robayo 

Con la firma de los Acuerdos de paz se creó en Colombia la Jurisdicción Especial para la Paz, un mecanismo que le apuesta a la **Justicia Restaurativa**, es decir a la búsqueda de la verdad desde quienes has sido victimarios en el conflicto armado, con la finalidad de responder al por qué del conflicto armado y dar tranquilidad a las víctimas.

En este programa del Expreso Libertad, las voces de 3 jóvenes víctimas de diferentes actores armados intercambiaron sus opiniones frente a las ventajas de la justicia restaurativa. **José Antequera** hijo del homónimo líder político de la Unión Patriótica y víctima del Estado; **Luisa Zuñiga**, hija de Ubaldo Zuñiga, excombatiente y ahora integrante del partido político FARC; y **Carolina Charry**, hija del diputado del Valle Alberto Charry, expusieron cómo ha sido su tránsito hacia el perdón y cómo desde allí le apuestan a la construcción de un nuevo país.

###### <iframe id="audio_34245711" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34245711_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
