Title: Fotografía: refugio de vida en medio de la devastación en la Franja de Gaza
Date: 2016-11-09 12:06
Category: DDHH, El mundo
Tags: Fotografías en Palestina, Franja de Gaza, Iz Zanún, Niñas, niños, Palestina
Slug: llenando-de-vida-y-color-a-ninos-refugiados-en-la-franja-de-gaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre] 

###### [09 Nov. de 2016] 

En las calles de Shati, un campo de refugiados ubicado en la Franja de Gaza, viven cientos de niños y niñas que desde muy pequeños han tenido que vivir la dura guerra que aqueja hace años a esta población. Por esta razón, **Iz Zanún, un joven de 24 años de Palestina, ha decidido darle vida a este asentamiento y llevar alegría y esperanza a los ojos y rostros de todos los niños y niñas, a través de fotografías** que recrean las playas, los campos y los niños de otros tiempos en Palestina.

**Alrededor de 50 fotografías que ocupan el espacio de la ropa que habitualmente está colgada en las cuerdas ubicadas entre las casas**, son parte de esta iniciativa en la que Zanún quiere “intentar llevar felicidad y alegría a los niños que sufren mucho en su dura existencia".

**Las paredes grises y los muros que encierran este campo de refugiados, fueron embellecidos, llenados de color, de esperanza** esperando atraer a cientos de niños y niñas para "contribuir a inculcar la cultura del arte en la sociedad Palestina y desarrollar en ellos amor por lo hermoso de la vida en sus pequeños corazones".

En las fotografías frente a las cuales se apostan los niños y las niñas para poderse perder entre los colores y las formas, pueden apreciarse playas de la Franja, escenas que reflejan niños jugando, riendo o estudiando y jardines frondosos. Imágenes de los lugares en los que algunas vez vivieron.

**En Shati, sus calles no superan el metro de ancho, por lo que poder tener esta muestra fotográfica permite a sus habitantes convivir muy cerca de ellas para recordar la cara bonita de sus territorios,** como lo asegura Mohamed Kaskin uno de sus habitantes.

Gaza ha sido escenario de tres duras operaciones militares israelíes desde el año 2008, la más complicada en el año 2014. Son cerca de 1,9 millones de personas de los cuales casi el 70% son refugiados las que han sufrido y quienes con miedo y tristeza no han podido disfrutar de la vida en sus territorios desde hace años.

**Los niños y niñas son los más beneficiados con este tipo de iniciativas en la que pueden realizar el recorrido por las fotografías colgadas en las cuerdas y luego de esto pueden encontrase con Zanún y hacer algunas fotos**, mientras el fotógrafo les enseña cómo emplear el aparato. Le puede interesar: [“La cámara es mi arma”: periodista Palestina de 10 años](https://archivo.contagioradio.com/la-camara-es-mi-arma-es-mas-fuerte-que-una-pistola-periodista-palestina-de-10-anos/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
