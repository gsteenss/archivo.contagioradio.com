Title: Comunidades del Cacarica sitiadas por presencia paramilitar
Date: 2017-01-30 14:30
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Paramilitares en Cacarica, Paramilitares en Chocó, Territorio Colectivo del Cacarica
Slug: choco-paramilitares-gaitanistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ene 2017] 

Comunidades sitiadas, escasez de alimentos, y reuniones obligatorias, son algunas de las situaciones denunciadas por los habitantes de las veredas Bijao y Varsovia, desde hace 6 días cuando registraron la presencia de **unos 200 paramilitares de las Autodefensas Gaitanistas alrededores de la cuenca del río Cacarica,** y que aún no tienen respuesta por parte del Estado.

Hasta el momento, en tres de las comunidades el grupo paramilitar ha realizado reuniones, uno de los pobladores reveló que los integrantes de este grupo justificaron su presencia porque las comunidades “tienen muchas necesidades y ellos traían proyectos”, argumentaron que se quedaran en el territorio para “garantizar el desarrollo de los territorios”. “Para nosotros es evidente que hay empresas detrás, quieren canalizar el río y hacer minería” puntualizó el poblador.

Por otra parte, también aseguró que en los últimos días se registraron nuevos desplazamientos de varias familias hacia la **zona humanitaria de Nueva Esperanza**, indicó que los paramilitares han llegado a las veredas preguntando por personas concretas, situación que también es de conocimiento de los militares y “no han hecho control a estos armados ni a civiles que ingresan y no pertenecen a la comunidad”.

El habitante describió a los integrantes, indicó que visten con uniformes “como los de los militares” y tienen brazaletes que dicen **Autodefensas Gaitanistas**, “andan públicamente por al cuenca del Cacarica, incluso muy cerca de donde está el Ejército”.

### A 20 años de la operación Génesis 

El poblador, expresó que las familias se encuentran muy “atemorizadas y preocupadas”, la nueva situación ha traído a la memoria de los habitantes los desplazamientos ocasionados hace 20 años por grupos paramilitares, durante la operación Génesis, ocurrida del 24 al 27 de febrero de 1997 y por la que el Estado colombiano fue  declarado por la Corte Interamericana de Derechos Humanos como “internacionalmente responsable”, de haber incumplido con su obligación de garantizar los derechos a las comunidades.

Denunció que sólo hay protección en **la Zona Humanitaria**, pero que “el resto de comunidades están a la deriva”, aseveró que la situación se agrava aún más porque “en este momento se están agotando los alimentos porque nadie quiere salir de sus casas, en adelante tendremos que aguantar hambre (…) no hemos podido sembrar ni salir a trabajar, la gente está asustada, está hacinada”.

Por último, el habitante de la cuenca del río Cacarica señaló que los 200 paramilitares ingresaron a través del Parque Nacional los Katíos, en donde hay alta presencia militar, y sobre el río Atrato, “el ejército sabía que estaban ingresando y no les dijo nada”.

<iframe id="audio_16725757" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16725757_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
