Title: Créditos del ICETEX, deudas impagables para jóvenes del país
Date: 2016-09-13 14:01
Category: Educación, Nacional
Tags: créditos, ICETEX, jovenes
Slug: creditos-del-icetex-deudas-impagables-para-jovenes-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/piki.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

###### [13 de Sept de 2016] 

La entidad financiera ICETEX, que genera créditos estudiantiles para los jóvenes que quieran acceder a la educación superior, anunció que hará en los próximos días un “perdón a morosos”, sin embargo, de acuerdo con Daniel Torres, vocero del movimiento "ICETEX te arruina", **este ejercicio no es útil porque finalmente solo aumenta la deuda y los intereses de los préstamos**.

Para Torres iniciativas como esta no benefician a las personas que adquirieron los créditos porque **continúa con la capitalización de intereses, es decir con el cobro de interés sobre interés**, y desnaturaliza este tipo de préstamos, que **de ser un crédito social se transforma en uno de Consumo**.  Esos intereses capitalizados sumado al alto porcentaje de **desempleo juvenil del 15,5%** en el país, empuja a las personas a retrasarse en el pago de la deuda y terminar en la lista de morosos con cobros hasta 3 veces más altos que la deuda inicial.

La jornada denominada “Ponte al día” se ha realizado periódicamente cada año y busca a partir de planes, refinanciar la deuda de los usuarios para llegar a un **acuerdo de pago y normalizar la cartera de morosos.**

“[Hemos recopilado miles y miles de casos de los cobros desmedidos del ICETEX, y además del acoso con llamadas que aterrorizan](https://archivo.contagioradio.com/pedi-al-icetex-29-millones-ya-he-pagado-30-pero-aun-debo-29/). Lo más grave de todo es que las personas no pueden acceder a un crédito de vivienda, no puede continuar con su vida, no puede iniciar una familia porque están reportados en Data crédito” afirma Daniel Torres.

Frente a posibles soluciones a los créditos estudiantiles, el representante a la Cámara Senén Niño radicó un proyecto de ley para[controlar y regular los abusos del ICETEX](https://archivo.contagioradio.com/programa-tu-eliges-del-gobierno-fomenta-el-endeudamiento/) en donde expone unos parámetros y criterios que beneficien a la población juvenil, entre ellos se encuentran **eliminar la capitalización de intereses**, que se cobre solo el valor del IPC sin aumento de puntos y **que se establezca la información necesaria** para que el estudiante tome la mejor decisión a la hora de adquirir un crédito.

<iframe id="audio_12892549" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12892549_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
