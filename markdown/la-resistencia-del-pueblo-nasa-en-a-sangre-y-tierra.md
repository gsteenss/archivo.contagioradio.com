Title: La resistencia del pueblo Nasa en "A sangre y Tierra"
Date: 2016-04-15 12:16
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Documental pueblo Nasa, Feliciano Valencia, Resistencia indigenas Nasa
Slug: la-resistencia-del-pueblo-nasa-en-a-sangre-y-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/nasa-3-e1460740233372.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Entrelazando 

##### [15 Abr 2016] 

Este viernes se estrena en el Centro memoria paz y reconciliación de Bogotá el documental “A Sangre y Tierra” Resistencia indígena del Norte del Cauca, un trabajo de la productora independiente ‘[Entrelazando](http://entrelazando.com/portfolio-item/resistencia-indigena-norte-del-cauca/)’, bajo la dirección de Ariel Arango.

El documental de 40 minutos, hace parte del proyecto fotográfico “Nasa significa humano”, resultado del proceso de acompañamiento de un año al pueblo que resiste en el Cauca, para visibilizar la problemática de tierras y la producción de etanol, en busca de establecer el diálogos y reflexiones alrededor de estos temas, desvirtuados y tergiversados por los grandes medios de comunicación.

El lanzamiento del documental, se dio en el marco de la conmemoración de los 45 años del Consejo Regional Indígena del Cauca CRIC, en los resguardos La María Piendamó, en Huellas Caloto y en el Paraninfo de la Universidad del Cauca en Popayán con lleno total. A nivel internacional, “A Sangre y tierra” fue proyectado en el circuito audiovisual independiente de Argentina.

<iframe src="https://player.vimeo.com/video/136112042" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Más allá de lo bélico, en el documental se muestran algunas tradiciones culturales del pueblo Nasa que están en riesgo de perderse, un recuento histórico del proceso de surgimiento del CRIC, las consecuencias de la masacre del Nilo luego de la desmovilización del Quintin Lame y los acuerdos de paz, la problemática de la producción de etanol, el proceso Nasa de liberación de madre tierra y la lucha por la autonomía, la defensa jurisdicción especial indígena y la movilización social a partir de la detención de [Feliciano Valencia](https://archivo.contagioradio.com/primeras-declaraciones-de-feliciano-velencia-desde-la-carcel/) el año pasado.

El estreno del documental en Bogotá, tendrá como antesala la presentación del portafolio fotográfico del proyecto “Nasa significa humano” en compañía de la agrupación musical “Falutas pa´los del monte” y de 6 a 7 pm se proyectará el audiovisual con participación de algunos miembros del pueblo Nasa y otros invitados especiales.

Posterior a la premier, la producción realizará un recorrido por diferentes espacios culturales de la capital, y el 6 de mayo se estrenará en la ciudad de Cali, por gestión del Instituto de Estudios culturales de la Universidad Javeriana, para continuar con algunas proyecciones en la ciudad de Medellín.

<iframe src="http://co.ivoox.com/es/player_ej_11179540_2_1.html?data=kpaemZ6ZeJGhhpywj5aUaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncaLmysrZjabWpc%2Fb0JCajarSuNPZzcbnw9PIs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
