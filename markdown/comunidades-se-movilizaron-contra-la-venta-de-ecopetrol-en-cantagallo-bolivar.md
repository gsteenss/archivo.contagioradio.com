Title: Comunidades se movilizaron contra la venta de Ecopetrol en Cantagallo, Bolívar
Date: 2020-11-21 10:48
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Ecopetrol
Slug: comunidades-se-movilizaron-contra-la-venta-de-ecopetrol-en-cantagallo-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Comunidades-se-movilizan-en-contra-de-la-venta-de-Ecopetrol.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Comunidades-se-manifiestan-contra-venta-Ecopetrol.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves, mientras que en varias ciudades del país se llevaba a cabo una jornada más del Paro Nacional promovida por centrales obreras y organizaciones sindicales, comunidades rurales de más de 14 veredas y 4 municipios de los departamentos de Bolívar y Antioquia se movilizaron por la defensa del patrimonio público y para alzar su voz en contra de la venta de la empresa Ecopetrol.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Decenas de personas decidieron tomarse pacíficamente las instalaciones de Ecopetrol en el municipio de Cantagallo en el sur de Bolívar según señalan, luego “de los reiterados incumplimientos del Gobierno Nacional a los acuerdos firmados con las comunidades en paros pasados y a la falta de inversión social por parte de Ecopetrol y las autoridades administrativas del orden nacional, departamental y local.

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Comunidades-se-manifiestan-contra-venta-Ecopetrol.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Esta es una lucha que vienen librando los trabajadores de la empresa hacer largo tiempo y que  estado encabezada por organizaciones sindicales como la Unión Sindical Obrera -[USO](https://twitter.com/usofrenteobrero)- que tuvo a algunos de sus representantes en una extensa huelga de hambre frente a las instalaciones de  Ecopetrol en Bogotá en lo que denominaron el Machín de la Resistencia. (Lea también: [La USO completa 30 días de huelga por la defensa del patrimonio](https://archivo.contagioradio.com/la-uso-completa-30-dias-de-huelga-por-la-defensa-del-patrimonio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También por parte de los trabajadores en las distintas plantas en los territorios que como en Cantagallo han alzado su voz en contra de la venta de la empresa, lo que implicaría una “*masacre laboral*” por la pérdida de muchos de los puestos de trabajo que actualmente provee Ecopetrol. (Lea también: [Trabajadores de 54 plantas de ECOPETROL protestan contra la posible venta de la empresa estatal](https://archivo.contagioradio.com/trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/C_Pueblos/status/1329771938294673410","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/C\_Pueblos/status/1329771938294673410

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, los abanderados en esta lucha han insistido que esta no solo se da en defensa de la empresa Ecopetrol y sus derechos laborales, sino en general del patrimonio público pues una eventual venta traería consecuencias negativas para las finanzas del Estado lo cual podría repercutir en mayores impuestos y la imposición de  una reforma tributaria. (Le puede interesar: [Otra Mirada: Ecopetrol no se vende](https://archivo.contagioradio.com/otra-mirada-ecopetrol-no-se-vende/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
