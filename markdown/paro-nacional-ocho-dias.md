Title: Campesinos completan 8 días de Paro Nacional
Date: 2017-10-30 14:27
Category: Movilización, Nacional
Tags: campesinos, campesinos de colombia, movilización de campesinos, paro de campesinos, paro nacional indefindio
Slug: paro-nacional-ocho-dias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/campesinos-e1509391619572.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Radio Súper Popayán] 

###### [30 Oct 2017] 

En el marco de la gran jornada de indignación, **continúa la movilización campesina en el país.** Desde los diferentes puntos de la movilización, las y los campesinos continúan exigiéndole al Gobierno Nacional que cumpla los acuerdos pactados. Esperan que las instituciones del Estado Colombiano hagan presencia en el Catatumbo para avanzar con las negociaciones.

<iframe src="https://co.ivoox.com/es/player_ek_21776053_2_1.html?data=k5akmZuUeZShhpywj5WYaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncYy-0M3b0N6PhcPmytGSlKiPt9DW08qYzsaPp9Di1c7b187IpcWfxcqYz9Tarc3d28bQy9TSqdSfxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Johnny Abril, integrante de la Asociación de Campesinos del Catatumbo, **la movilización continúa de manera pacífica**. Allí, se espera que el Gobierno Nacional haga presencia en el punto de encuentro de la Y de Astilleros para que se avance con las negociaciones de los diferentes puntos que hacen parte del pliego de peticiones de los campesinos a nivel nacional.

Dijo que es necesario que se instalen de manera urgente, **las mesas de negociación a nivel nacional y a nivel regional en el Catatumbo**. Indicó que los campesinos han sido agredidos por parte del ESMAD a pesar del acuerdo con la Fuerza Pública que permitía la movilización pacífica de los campesinos en 12 departamentos. (Le puede interesar: ["Campesino alertan sobre inminente ataque del ESMAD en Catatumbo"](https://archivo.contagioradio.com/campesinos-catatumbo-esmad/))

### **En el Cauca avanza la movilización de manera satisfactoria** 

<iframe src="https://co.ivoox.com/es/player_ek_21776117_2_1.html?data=k5akmZuVdZihhpywj5WZaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncavjz8bhysbSb6TZz9nS0NSPt9DW08qYzsbXb87j187Zy9_Fp8rjz8rgjcrSb8bgjKnd1tSPqMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Jonathan Centeno, vocero de Marcha Patriótica en el Cauca, indicó que la movilización en ese departamento avanza de manera satisfactoria sobre la carretera Panamericana **“donde hay tráfico restringido para hacer actividades de pedagogía con las personas que se movilizan por allí”.** Afirmó que ha habido avances en las negociaciones con el Gobierno Nacional y “posiblemente se instalará la Mesa de Interlocución y Acuerdo a nivel Nacional”.

Adicionalmente, Centeno informó que en el Cauca **se mantienen los 3 puntos de movilización** en Patía, Cajibío y Caldono, donde se han reunido más de 5 mil campesinos. Dijo que es posible que las movilizaciones indígenas se unan con las campesinas en la medida que “todos hemos sido abandonados y olvidados en el país”. (Le puede interesar:["Inicia movilización en Colombia por el incumplimiento de 1300 acuerdos"](https://archivo.contagioradio.com/inicia-minga-indigena-en-colombia/))

Finalmente, los campesinos de las diferentes regiones **saludaron las movilizaciones sociales de los indígenas**, quienes empiezan una movilización indefinida a partir de hoy 30 de octubre. Los campesinos manifestaron que hasta tanto no se instale la mesa de interlocución y pare la erradicación forzada de los cultivos de coca, no se detendrá la movilización y el paro nacional.

###### Reciba toda la información de Contagio Radio en [[su correo]
