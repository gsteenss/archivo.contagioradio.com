Title: A paso lento avanza cumplimiento de los acuerdos del Gobierno con el Chocó
Date: 2017-07-10 15:18
Category: DDHH, Movilización, Nacional
Tags: Juan Manuel Santos, Paro cívico Chocó
Slug: a-paso-lento-avanza-cumplimiento-de-los-acuerdos-del-gobierno-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro-choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 Jul 2017] 

Emilio Pertuz, vocero del Comité del Paro Cívico del Chocó, manifestó que si bien es cierto que el gobierno ha avanzado en el cumplimiento en temas pactados como salud, educación, vías e infraestructura, **hace falta mayor compromiso en generación de políticas públicas específicas pensadas en las características del pueblo chocoano y sus necesidades**.

Algunas de esas políticas públicas, de acuerdo con Pertuz, deben estar enfocadas en poblaciones como las mujeres, los jóvenes y las víctimas del conflicto armado de esta región. De igual forma señaló que es importante que se construyan a partir de la geografía del territorio. (Le puede interesar: ["Estos son los acuerdos entre el Gobierno y Chocoanos"](https://archivo.contagioradio.com/se-levanta-paro-en-el-choco-tras-establecer-pre-acuerdos-entre-comunidad-y-gobierno/))

“**Todas las mayorías de proyectos nacionales, que se montan con políticas nacionales que no responden a la idiosincrasia nuestra** terminan fracasando en nuestro territorio, y eso es lo que no queremos” afirmó Pertúz y manifestó que no se ha avanzado porque no han llegado los funcionarios con el nivel adecuado que puedan generar compromisos sobre este tema.

Para la comunidad afrodescendiente un paso que podría ayudar a que la creación de las políticas públicas sea más rápida, es la reglamentación de la **Ley 70 que reconoce a las comunidades** negras y que según, el integrante del comité del paro cívico, se podría tramitar el 27 de agosto con la firma del presidente Juan Manuel Santos.

De igual forma el lider  aseguró que el tema del paramilitarismo en la región no ha tenido un control adecuado por parte de la Fuerza Pública, porque no hay un tratamiento de esta problemática de forma integral, sino que se focaliza en ciertas comunidades. (Le puede interesar: ["Comunidades responsabilizan a Santos por compromisos incumplidos en el Chocó"](https://archivo.contagioradio.com/el-presidente-santos-es-el-responsable-de-los-compromisos-asumidos-e-incumplidos-en-el-choco-emilio-pertuz/))

### **Inversión en Salud** 

Referente a los compromisos que adquirió el gobierno en el tema de Salud, Pertúz señaló que **ya se está definiendo el lugar en donde se construirá el Hospital de tercer nivel para el departamento** y posteriormente se realizará el diseño, estudios estructurales y estudio de suelos para iniciar la construcción.

Sin embargo, aún no se ha resuelto qué presupuesto tendrá el Hospital de segundo nivel Ismael Roldan que se encuentra ubicado en la zona del Bajo San Juan, Pertúz expresó que la designación de recursos podría gestarse esta semana en una reunión con delegados del gobierno. (Le puede interesar:["No estamos dispuestos a hacer una negociación mal hecha: líder del Chocó"](https://archivo.contagioradio.com/el-ministerio-que-chocoanos-califican-como-obstaculo-en-las-negociaciones/))

### **Belén de Bajirá** 

Otro de los compromisos que ya cumplió el gobierno, es la publicación del mapa en donde se señalan los límites geográficos entre el departamento del Chocó y el departamento de Antioquía **que ratificó que Belén de Bajirá hace parte del Chocó**.

En este sentido tanto el Ministerio de Educación como el Ministerio de Salud se comprometieron en adelantar los trámites para **transferir los recursos pertinentes al Chocó que garanticen los derechos de la comunidad de Belén de Bajirá**.

### **Construcción de vías** 

En materia de vías ya se aprobó y firmó el CONFIS para la realización de las vías del Chocó y se realizó la asignación de los primeros 440 mil millones de pesos para la vía Medellín-Quibdó y Pereira-Quibdó. (Le puede interesar: ["Gobierno hace oídos sordos al paro en Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/))

### **Acuerdos del paro cívico del Chocó** 

1.El gobierno se comprometió a adicionar 440 mil millones de pesos al presupuesto del departamento en infraestructura, que se les adhieren a los 280 mil millones con los que actualmente cuenta el Chocó.

2.El dinero aprobado por el gobierno se ejecutará en 5 años y los restantes serán incluidos en el marco macroeconómico o fiscal del 2018.

3.Se invertirán 84 mil millones de pesos, en el sistema de salud del departamento y se darán otros 2 mil millones que se entregarán a la gobernación.

4.El Gobierno definirá la situación de Belén de Bajirá, un territorio de 2.500 kilómetros y de 16.000 habitantes que lleva más de 15 años siendo disputado entre Antioquia y Chocó.

5.La activación de las mesas temáticas que se encontraban trabajando el pliego de exigencias del paro del 2016.

<iframe id="audio_19721830" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19721830_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
