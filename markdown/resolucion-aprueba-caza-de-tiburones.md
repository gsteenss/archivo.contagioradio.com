Title: Lo que debe saber sobre la resolución que aprueba la caza de tiburones en Colombia
Date: 2019-10-30 18:20
Author: CtgAdm
Category: Ambiente
Tags: #YoDefiendoLosTiburones, Aleteo, Duque, Min. agricultura, Min. Ambiente, tiburones
Slug: resolucion-aprueba-caza-de-tiburones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/blacktip-reef-shark-in-aquarium.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Lynn Greyling] 

La resolución 000350 intenta regula la pesca y comercialización de especies marinas, entre ellas el tiburón, tema que ha generado gran indignación entre los ambientalistas al ver la novedad de este año incluye de manera directa a los escualos, especies que podrán ser cazados mediante la pesca artesanal, en una totalidad de 125 toneladas y 5,2 toneladas de aletas; con un único control en los puertos donde se verificará que los tiburones lleguen con sus aletas adheridas al cuerpo, para muchos sectores esta regulación lo que puede generar es una caza descontrolada de la especie y el aumento de pesca ilegal.

El acuerdo se da luego de la reunión entre el Comité Ejecutivo para la Pesca, el Ministerio de Agricultura y el de Ambiente,  donde  participaron funcionarios expertos en este tema, que fundamentaron sus argumentos en documentos técnicos basados en los programas de monitoreo y observadores pesqueros  y  la cuota de aprovechamiento de los recursos marinos; documentos que según Maria Claudia Díaz directora del programa Marino y de Conservación para el desarrollo Internacional, no son de conocimiento público y no están contrarrestados con información de organizaciones ambientales.

De igual forma Díaz aclaró que esta es la primera vez que la resolución deja claro la utilización de aletas por especie, "en Colombia no hay una pesca dirigida a tiburones, lo que se captura es en pesca artesanal de manera incidental en Colombia, que está establecido por ley, sin embargo en la práctica ocurre otra cosa". (Le puede interesar:[Leyes colombianas se quedan en el papel a la hora de proteger la fauna marina](https://archivo.contagioradio.com/dia-mundia-de-los-oceanos-leyes-colombianas-se-quedan-en-el-papel-a-la-hora-de-proteger-la-fauna-marina/))

### ¿Realmente la solución está en la regulación?

Según Claudia Díaz la pesca ilegal, no declarada y no reglamentada es uno de los grandes problemas que tenemos en el país, "a San Andrés y a diferentes  archipiélagos  llegan barcos con banderas extranjeras que vienen y pescan tiburones y demás especies  de manera ilegal y obviamente no reportan la cantidad que capturan"; para la directora el problema está en hacer control en toda esta zona, y generar una reglamentación que vaya más allá de las autoridades pesqueras y marítimas.

> "hay mecanismos desarrollados pero las denuncias son muy pocas para poder realmente evaluar y controlar la pesca ilegal", afirmó Claudia Díaz

En el país no se sabe exactamente cuantos tiburones vivos hay debido a que la gran mayoría de estas especies son migratorias y eso dificulta su conteo, además son muy pocas las organizaciones que se han encargado de desarrollar técnicas que reconozcan su reproducción, territorialidad y hábitos, entre ellas la Fundación Malpelo y la Fundación Escualos, "es aquí cuando nos preguntamos cómo el gobierno ha podido establecer cuotas de consumo, cuando desconoce todo esto y sus únicos datos están en los animales capturados por los pescadores".

### ¿Cuál es el objetivo de la pesca de tiburones?

Desde la aprobación de la resolución se ha venido difundiendo el mensaje de que esta responde a intereses mercantiles con China, país que registra uno de los mayores indices de consumo de aletas de tiburón en el mundo, por eso dentro de los estándares de aprovechamiento por lo general se comercializan las aletas y se descarta el resto del tiburón.

En Colombia hay un consumo mínimo de esta especie y se vende en zonas costeras con el nombre  de Tollo, "las personas no tienen mucho conocimiento de esto, por eso el aprovechamiento de los escualos localmente es insignificante y lo que se aprecia es el valor de exportación de las aletas a países asiáticos que las pagan muy bien", resaltó Díaz. (Le puede interesar:[Tortugas ¿Cuál es su importancia y que hacer para protegerlas?](https://archivo.contagioradio.com/tortugas-marinas-peligro-extincion/))

### Un alto a la pesca de tiburones ahora

Esta resolución, según Diáz, pasa por alto las características de los animales, "hay que hacer un control de la cantidad de tiburones que se extrae , y lo más importante **evitar la captura de individuos pequeños**, esto lo que impide es que las poblaciones se puedan reproducir, fenómeno que ya se está dando en algunas zonas del país", afirmó la directora.

> *"se capturan tiburones pequeños, casi recién nacido para suplir con un consumo local", Claudia Díaz.*

Actualmente diferentes organizaciones se esfuerzan para hacer un monitoreo sobre las especies libres, uno de ellos es el programa de avistamiento de tiburones que  permite conocer con exactitud la variedad de especies y hacer un mejor control. Asimismo Claudia Díaz sugirió que se haga la clasificación de animales óseos  para poder definir cual es el nivel de aprovechamiento, "invito a la misma autoridad pesquera y a los ministerios a actualizar las tallas mínimas de captura legales, y con esto establecer la base para hacer un mejor control y protección de las especies marinas".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44015217" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44015217_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
