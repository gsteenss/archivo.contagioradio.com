Title: Gobierno continúa sin dar respuestas a víctimas de megaproyectos
Date: 2016-11-21 19:24
Category: DDHH, Nacional
Tags: Congreso, Hidroituango, megaproyectos
Slug: gobierno-continua-sin-dar-respuestas-victimas-megaproyectos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/debate-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: M. Ríos Vivos 

###### [21 Nov 2016] 

Según denuncian las comunidades, el proyecto hidroeléctrico **Hidrosogamoso en Santander ha dejado cerca de 20 mil víctimas de desplazamiento,** el megaproyecto **Hidroituango en Antioquia más de 20 mil personas desalojadas y El Quimbo en el Huila más de 1500.** Sin embargo, nuevamente el gobierno continúa sin darle una respuesta a las exigencias de estas comunidades en torno a la necesidad de buscar otros modelos energéticos que no generen daños ambientales ni el desalojo de poblaciones.

Así lo concluyeron los asistentes a la audiencia ‘¿Utilidad pública beneficio de todos?’, convocada por el Movimiento Ríos Vivos y los congresistas Ángela María Robledo, Alirio Uribe, Iván Cepeda, Alberto Castilla, Alexander López y Víctor Correa, en la que se buscaba evidenciar el conflicto alrededor del uso de la figura de utilidad pública frente a la implementación de megaproyectos y los desalojos forzosos que estos generan.

De acuerdo con Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, **se buscaba solicitar al gobierno la visita del relator especial para el desplazamiento forzado** con énfasis en el desplazamiento por el desarrollo, para que este realizara un recorrido por el país y conociera de la situación de las comunidades desalojadas por la actual política minero-energética. No obstante el **gobierno continúo negando la necesidad de cambiar de modelo energético y rechazó la propuesta del Movimiento.**

“Fue un debate fuerte sobre quién tiene la responsabilidad de la manera como se ha utilizado la figura de utilidad pública, el gobierno plateaba que ha sido responsabilidad del congreso por la normativa, mientras que el congreso le respondía al gobierno que ha sido por su política minero-energético que se ha impulsado esa normatividad y su aplicabilidad”, dice Zuleta.

El objetivo de la audiencia era discutir las implicaciones que tiene la declaratoria  de utilidad pública de actividades económicas como proyectos hidroeléctricos, mineros, agroindustriales y de infraestructura; también se debatió sobre las directrices establecidas por Naciones Unidas con relación a proyectos de desarrollo en materia de desalojos que no están siendo tenidas en cuenta por las empresas ni el Estado Colombiano.

En el debate participaron líderes de las comunidades, los congresistas e  instituciones del gobierno nacional, aunque **no se contó con la presencia del Ministro de Minas y Energía Arce Zapata, ni la Procuradora General de la Nación, Martha Isabel Castañeda,** quienes enviaron a delegados de sus despachos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
