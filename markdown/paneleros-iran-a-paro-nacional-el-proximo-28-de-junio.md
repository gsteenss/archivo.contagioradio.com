Title: Paneleros irán a Paro Nacional el próximo 28 de junio
Date: 2019-06-25 17:30
Author: CtgAdm
Category: Movilización, Nacional
Tags: paneleros, Paro Nacional
Slug: paneleros-iran-a-paro-nacional-el-proximo-28-de-junio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-131.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto:] 

Después de 18 meses de crisis, las organizaciones Unión Panelera Nacional y Dignididad Agropecuaria Colombiana convocaron a los paneleros a un paro nacional, para el próximo 28 de junio, para **mejorar las condiciones de vida digna de 350.000 familias campesinos en el país**.

El precio de la panela que comenzó a bajar en el 2017, sumado a la falta de garantías por el Gobierno, ha dejado a estos trabajadores en condiciones precarias. Actualmente, **el kilo de panela** [**está a \$1.200 y la producción cuesta \$3.200 que significa que en la producción ya hay pérdidas**.]Además, denuncian que la situación económica no ha permitido que los paneleros atiendan sus créditos, mejoren sus trapiches o cultivos y en algunos casos, moler sus cultivos de caña, perdiendo su ingreso.

Luis Fernando Paipilla, presidente ejecutivo de Dignidad Agropecuaria Colombiana, señaló que el Gobierno no ha desarrollado una política específica para abordar la crisis a pesar de las varias llamadas de atención que ha realizado el sector panelero. Desde septiembre de 2018, **la organización campesina ha pedido el respaldo del Gobierno en cinco reuniones distintas con el Ministerio de Agricultura y Desarrollo Rural**.

Sin embargo, Paipilla sostiene que en la última reunión con delegados de dicho ministerio, del Banco Agrario y el  Fondo para el Financiamiento del Sector Agropecuario (FINAGRO) el pasado 18 de junio, el Gobierno propuso soluciones que no abordaron las demandan concretas que solicita este sector.

"Nos presentaron una nuevas tasas de crédito pero si no podemos pagar los créditos que actualmente tenemos porque estamos en quiebra y adicionalmente hay embargos, **difícilmente nos van a generar nuevos créditos**. Nos van a decir que no somo sujeto de créditos entonces ¿qué solución le puede dar a la gente?", afirmó Paipilla.

### **Las demandas de los paneleros**

Al ver que el Gobierno no provee soluciones, los paneleros, dirigidos por las uniones de Boyacá y Santander, irán a un paro nacional hasta que sus peticiones sean cumplidas. Estas abordan la crisis de profunda que, según las asociaciones, nace de los tratados de libre comercio que permite la importación de productos de competición a la panela como azúcar y jarabe de maíz.

Para los paneleros, también es grave la decisión del Gobierno del expresidente Juan Manuel Santos de autorizar importaciones de etanol, que ha conducido a un incremento en la producción de azúcar y de la producción de panela falsa, ya que se está derritiendo el azúcar sobrante para mezclarlo con miel de purga en los derretideros. También ha llevado a que se haga más panela verdadera, en ingenios que violan la Ley 40 de 1990, inundando el mercado y bajando los precios de compra.

El sector panelero ha formulado 11 puntos concretos:

1.  Condonación de deudas con el Banco Agrario y FINAGRO para los productores paneleros
2.  Primas de seguros para el sector agrícola panelero con coberturas y amparos donde incluya todos los riesgos que son inherentes a las actividades desarrolladas del sector panelero
3.  Incluir en todos los programas sociales del Gobierno Nacional mínimo el 50% de panela natural colombiana como endulzante de bebidas, postres, refrigerios, etc.
4.  Que en los programas de sustitución de cultivos NO sea la caña de azúcar como una alternativa. No incentivar la siembre de caña panelera en otras zonas donde su tradición no ha sido este cultivo.
5.  Establecer un plan de consumo de la panela para los mercados institucionales donde estos sean cubiertos única y exclusivamente por las asociaciones de productores de panela de cada municipio y no por intermediarios.
6.  Cierre total de las importaciones de azúcar y de étanol con el fin de proteger la producción nacional de étanol, control de derretideros y contrabanda de panela.
7.  Baja de aranceles de insumos agrícolas o en su defecto subsidio para estos, utilizados en los cultivos de caña panelera
8.  Establecer campañas de promoción al consumo en medios televisivos pagadas por el Gobierno nacional que incentive el consumo de la panela como alimento natural, no como solo un endulzante sino como un alimento, el cual ya es considerado a nivel mundial.
9.  Establecer un techo y un piso del precio de la panela considerando el costo real de producción panelera de una manera formal, es decir, cumpliendo con todos los requisitos de ley (Costos de Producción de kilo de Panela \$3.250).
10. Que el Gobierno de unos lineamienos en donde no se ejerza e monopolio de los licores para el consumo humano y demás alcoholes con el fin que los trapiches paneleros puedan producir y comercializar licores para no tener que importar más estos productos.
11. No sancionar la Ley de la Panela hasta tanto no se aclaren ciertos interrogantes que van en prejuicio de los pequeños productores.

<iframe id="audio_37584795" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37584795_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
