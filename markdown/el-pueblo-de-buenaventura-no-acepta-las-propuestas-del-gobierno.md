Title: Bonaverenses califican como insuficientes propuestas del gobierno
Date: 2017-05-24 14:20
Category: DDHH, Nacional
Tags: buenaventura, Juan Manuel Santos, Paro cívico, Toque de queda
Slug: el-pueblo-de-buenaventura-no-acepta-las-propuestas-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Buenvanetura-CONPAZ-FINAL.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Conpaz] 

###### [24 May. 2017] 

Después de varias horas reunidos con el Gobierno, los integrantes del Comité del Paro Cívico aseguraron que no se ha logrado llegar a ningún acuerdo, porque el Gobierno insiste en no decretar la emergencia social y económica “en síntesis se siente que el **Gobierno quiere desviar o dilatar la propuesta que tiene el pueblo de Buenaventura”** **aseguró Javier Torres, Presidente de la Flota de Barcos de Cabotaje de Buenaventura.**

Según el dirigente, durante toda la reunión el Gobierno aseguró que tenía mucho “miedo” de que la Corte Constitucional echara para atrás la declaratoria de emergencia, no obstante, los insumos que ha expuesto el comité del Paro Cívico para que se dé dicha declaración son suficientes “hay suficientes razones para que lo haga. Eso hace que los 9 puntos del pliego de peticiones funcionen” recalcó Torres.

### **El pueblo ha perdido la confianza y por eso no acepta las propuestas del Gobierno** 

Así lo aseveró Torres, quien recuerda que **en el 2014 se hicieron una serie de propuestas al Gobierno donde se entablaron 19 mesas, de las cuales ninguna funcionó** “entonces el pueblo de Buenaventura estamos cansados de mesas, de actas, de promesas incumplidas (…) el gobierno tiene unas propuestas, lo que pasa es que ya cuando un pueblo pierde la confianza en el Gobierno es muy difícil volver a retomar mesas”.

Para los Bonaverenses, las propuestas que les han dado a conocer desde el Gobierno central los hace trasladarse al pasado en donde ya han establecido mesas sin resultados reales y positivos. Le puede interesar: ["Gobierno hace oídos sordos al paro del Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

### **El Comité del Paro Cívico está listo para trabajar en mesas, falta declaratoria de emergencia** 

Torres dice que ya hay 9 mesas organizadas con representantes del comité del paro, quienes podrían sentarse de manera inmediata a dialogar con los encargados de cada ministerio y dar prontos resultados “si es el tema del punto de vivienda, es reunirnos con el Ministerio de Vivienda, lo que es con Agricultura con ese ministerio, cada cosa tiene una mesa temática, entonces **declaratoria y ahí comenzamos a trabajar punto por punto”.**

Sin embargo, el Gobierno ha exigido que antes se comience a trabajar en las mesas temáticas, lo que ha sido en reiteradas ocasiones respondido negativamente por parte del comité del paro “la declaratoria es el semáforo que nos va a dar la luz verde para pasar (…) el primer punto es la declaratoria, **no hay mesas temáticas si no hay declaratoria” reitera el líder. **Le puede interesar: [“No levantaremos el paro si no se decreta la emergencia” Buenaventura](https://archivo.contagioradio.com/no-levantaremos-el-paro-si-no-se-decreta-la-emergencia-buenaventura/)

### **Queremos dialogar con el presidente Santos** 

“¿Por qué el presidente ha querido traer discursos políticos para captación de votos, por qué no ha tenido la dignidad de venir a Buenaventura y darle la cara a este pueblo?” es lo que se preguntan los Bonaverenses, manifiesta Torres. Le puede interesar: [“Si no cerramos el puerto, el Gobierno no nos presta atención” habitantes de Buenaventura](https://archivo.contagioradio.com/si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura/)

Razón por la cual, **continúan exigiendo que el presidente de Colombia se haga presente en ese municipio** con un gabinete o un equipo delegado que pueda resolver las inquietudes, tomar decisiones y garantizar la ejecución de los compromisos a largo plazo “un ministro que ya va de salida como Cristo no nos sirve, porque se nos va y nos toca empezar de ceros después” relató Torres.

### **El toque de queda continúa** 

**Victor Vidal integrante del Proceso de Comunidades Negras dijo que el toque de queda sigue estando en toda Buenaventura** "este martes, en el marco de la reunión que se sostuvo algunos sectores de la comunidad le pidieron al Alcalde que lo levantara, él dijo que lo iba a estudiar con sus asesores a ver qué hacía". Le puede interesar: [Paramilitares estarían detrás de daños y robos en Buenaventura](https://archivo.contagioradio.com/paramilitares-estarian-detras-de-danos-y-robos-en-buenaventura/)

<iframe id="audio_18883725" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18883725_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
