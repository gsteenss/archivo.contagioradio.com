Title: Arquidiócesis de Cali pide retractación de Fernando Londoño
Date: 2018-02-13 17:23
Category: Entrevistas
Tags: Arquidiócesis de Cali, Centro Democrático, Monseñor Darío Monsalve. Fernando Londoño
Slug: arquidiocesis_cali_rpide_retractacion_fernando_londono_dario_monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/join-us-for-a-ride-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Semana / Zona Cero] 

###### [13 Feb 2018] 

Ante las afirmaciones de Fernando Londoño en el programa 'La hora de la verdad', en las que se refería a que Monseñor Darío Monsalve había obligado a otros párrocos a  **votar por las listas de la FARC** bajo amenazas de ser suspendidos, la Arquidiósesis de Cali señaló que se trata de acusaciones falsas asegurando además que **"dan fe" de que se trató un simple acto de cortesía para presentar a los integrantes de dicho partido.**

"Quienes estuvimos en reunión pastoral, damos fe de que solamente el Arzobispo, que tenía una reunión privada con algunos miembros del partido político FARC, los saludó públicamente, **los presentó como ciudadanos en un signo de cortesía, y de inmediato y sin que hubiera ningún comentario adiciona**l, se dirigió con ellos a la reunión acordada.

En el comunicado aseguran que Monseñór Monsalve encomendó al Obispo Auxiliar de Cali, Mons. Luis Fernando Rodríguez Velásquez, para que en su nombre diera inicio a la sesión plenaria y acompañara a los sacerdotes mientras él estaba en la mencionada reunión", dice el comunicado de la Arquidiócesis de Cali.

De acuerdo con el Twitter del programa que dirige Londoño, el integrante del Centro Democrático, "El Arzobispo de Cali reunió a los sacerdotes diocesános de Cali en un centro de retiros en Tuluá. Llegaron en camionetas lujosísimas 3 personajes: Calarcá, Sandino y un bandido Cepeda. El Arzobispo los obligó a votar por las listas de las FARC o quedan suspendidos"-

### **"Es típico en de una campaña basada en la mentira"** 

Para Rubén Darío Gómez, integrante del Observatorio de realidades del Arozbispado, dicho comentario se da en medio de una campaña electoral  en la que se continúa con el juego de la posverdad. Desde el Observatorio han hecho un llamado a Londoño para que r "esa posición de hacer campaña a través de la mentira".

De hecho, en el comunicado de la Arquidiócesis, se afirma que **por el contrario, Monseñor Monsalve había ratificado que ningún sacerdote puede hacer uso de su investidura** para tomar partido públicamente, ni orientar a los fieles a votar por algún candidato específico.

Gómez, también manifiesta que justamente esa forma de hacer política está generando un desgaste en la política, contribuyendo en la desesperanza del pueblo "ante la posibilidad de reconstruir un país de reconciliación y paz".

Finalmente en el comunicado piden que Fernando Londoño se **retracte "inmediatamente en los mismos medios a través de los cuales se ha querido hacer daño al arzobispo de Cali y a la iglesia en general con información malintencionada".**

<iframe id="audio_23783188" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23783188_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
