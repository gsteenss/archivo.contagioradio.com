Title: Denuncian 335 agresiones a defensores de DDHH en 2017 "Agúzate"
Date: 2017-08-17 11:46
Category: DDHH, Nacional
Tags: aguzate, asesinatos líderes sociales, defensores de derechos humanos, Somos defensores
Slug: somos-defensores-agresiones-en-2017-aguzate
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/aguzate-e1502989561932.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Somos Defensores] 

###### [17 Ago 2017] 

El informe "Agúzate" de la organización Somos Defensores, **pone en alerta la situación de inseguridad** en la que se encuentran los defensores de derechos humanos en el país. Entre enero y junio de 2017 registraron 335 agresiones individuales contra líderes sociales en el país, un aumento del 6% con relación al mismo periodo del año pasado.

El informe habla de **51 asesinatos, 32 atentados, 18 detenciones arbitrarias y 9 casos de judicialización**. Adicionalmente, indican que el fin del conflicto con la guerrilla de las FARC no fue garantía para que disminuyera la violencia, “ahora con el silencio de los fusiles con las FARC, la violencia contra los activistas se focaliza aún más”.

Pero más allá de las cifras, el informe hace un **recuento individual de cada uno de los casos de los líderes que fueron asesinados**. Para cada caso realizan un resumen de quien era la persona, la forma como sucedieron los hechos, si había denunciado o no amenazas en el pasado y nombran el proceso social afectado. (Le puede interesar: ["Somos Defensores documentó 51 asesinatos a líderes de DDHH en 2017"](https://archivo.contagioradio.com/somos-defensores-documento-51-asesinatos-de-defensores-de-derechos-humanos-en-2017/))

**Fin del conflicto con las FARC  aumentó inseguridad para los defensores**

Carlos Guevara, director de Somos Defensores, manifestó que las zonas más complicadas para ejercer la labor de defensa de los derechos humanos **continúan siendo los territorios donde hubo una mayor presencia del conflicto armado**, “allí la guerra se enquistó y no se ha acabado porque hay nuevos actores que antes eran invisibles”.

Indicó que entre enero y junio se registraron agresiones en departamentos donde antes **no había registros de violencia contra líderes sociales**. “Nuestros registros de violencia en el Tolima eran muy bajos y allí sucedieron asesinatos al igual que en la Guajira y el Cesar”. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

**Paramilitarismo representa el 50% de la responsabilidad en la comisión de crímenes**

"Agúzate" detalla que el fenómeno del paramilitarismo se ha agudizado y son **“grupos criminales con jerarquías y estructuras que funcionan a nivel regional”.** Guevara manifestó que a través de informes como el de la Fundación Paz y Reconciliación e investigaciones realizadas por la revista Semana, “se ha visto como el paramilitarismo actual es una mezcla con el anterior y se volvió un fenómeno gigantesco”.

Ante esto, Guevara manifestó una vez más que **“el gobierno no puede seguir cerrando los ojos** cuando los resultados de la existencia del paramilitarismo son evidentes”. Alertó además que el paramilitarismo “va a mutar con el surgimiento de muchos grupos armados y van a tener demasiado poder regional para poner en jaque la paz”. (Le puede interesar: ["Informe de Fiscalía sobre asesinatos a defensores de DDHH es otro "falso positivo"](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo/))

Finalmente, Carlos Guevara recalcó que el informe es una invitación a que se siga exigiendo al **Estado mecanismos y garantías para ejercer la labor de defensa de los derechos humanos**, “no estamos hablando de 2 o 3 personas, son 51 muertos en los primeros 6 meses de la paz”.

Indicó también que **debe haber solidaridad entre los líderes sociales para generar un ambiente más seguro**, “debemos compartir información y estar atentos a lo que suceda entre nosotros mismos”. Dijo que Agúzate es “un llamado a protegerse con lo que los defensores saben hacer y es defender los derechos humano”.

["Agúzate" Somos Defensores](https://www.scribd.com/document/356548442/Agu-zate-Somos-Defensores#from_embed "View "Agúzate" Somos Defensores on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_20388369" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20388369_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_51752" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/356548442/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XOcMibRCR9fFztK6oNbs&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6664201183431953"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
