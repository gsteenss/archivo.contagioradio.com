Title: Bogotá, la nueva "empresita" de Peñalosa
Date: 2016-01-31 22:24
Category: yoreporto
Tags: Bogotá, Enrique Peñalosa, ETB, Marchas, Vendedores informales
Slug: marcha-de-vendedores-ambulantes-penalosa-calle-72
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/penalosa-vendedores-ambulantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por: Johana Jaramillo - Yo Reporto **] 

###### 31 Ene 2016 

Llevamos un mes de la nueva alcaldía de Enrique Peñalosa y sus políticas antipopulares y de gerente de empresa no se han hecho esperar. Atacar el arte urbano y la organización de jóvenes potenciada por la alcaldía pasada, la empresa pública de telecomunicaciones que da una suma importante de ingresos a la Universidad Distrital en riesgo de una posible venta,  el alza del pasaje de Transmilenio que clases populares y medias tienen que pagar con el mísero salario mínimo,  y la recuperación de la vía pública desalojando a los vendedores ambulantes a costa de la comida y la poca seguridad que tienen estos... Ah, y el uso desmedido de la fuerza pública para todo, les da un uso similar al que le dan al acétaminofen las EPS.

En la empresita de Peñalosa, la estética parece ser más importante que la comida del pobre, por eso desde la semana antepasada viene adelantando el desalojo de vendedores informales de la carrera 7ma y la calle 72. Estos desalojos que no cuentan con un plan de acción de reubicación laboral para los jóvenes y planes de acción efectivos para los 14 adultos mayores que sacó de la 72. Esto sin contar los adultos que están entre los 40 y los 60 años.

Por ejemplo, tenemos el caso de Don Bernabé y doña y doña Bárbara de 81 y 80 años respectivamente. Les quitaron sus cosas sin que les den soluciones a esa necesidad tan humana que los poderosos parecen desconocer, comer y pagar arriendo y en su caso la necesidad de atención médica propia para su edad.

Con ellos y otros trabajadores, nos hemos reunido para empezar a movernos jurídicamente y por medio de marchas, es posible que la fuerza pública como raro arremeta contra los manifestantes así como lo hizo contra la gente que pidió recoger el cuerpo de la mujer que falleció en el norte de la ciudad.

Justamente, mañana lunes 1ro de febrero a las 7 a.m desde la calle 72 con 11, estaremos movilizandonos con los vendedores exigiendo al gobierno distrital soluciones claras para los vendedores informales.

### **Comunicado de los vendedores informales: ** 

Comunicado a la opinión pública por parte de lo vendedores informales de las calles 70 a 73 de Chapinero.

Las y los vendedores informales de la calle 70 a 73 entre carrera caracas y carrera7ma, nos hemos reunido a lo largo de estos días para dar a conocer a la opinión pública la situación por la que estamos pasando.

El martes 19 de enero desde las 5 am llegaron alrededor de 300 policías con camiones a hacer levantamiento de nuestros puestos de trabajo. Manifestaron que por orden de la Alcaldía Mayor, Secretaría de Gobierno y específicamente desde el Alcalde local de Chapinero Mauricio Jaramillo se dio orden de no sólo levantarnos sino de quitarnos nuestros instrumentos de trabajo. Nos quitaron parasoles, carretas y la comida fue botada al suelo.

Los medios de comunicación masiva han dicho que el Alcalde nos ha planteado soluciones, pero dichas soluciones no responden a las necesidades reales que tenemos, es decir, a los vendedores menores de 40 años nos ofrecen o quioscos o trabajo formal de aseadores o obreros, muchos de nosotros no tenemos conocimientos ni condiciones físicas para hacerlo. Adjunto a este documento adjuntaremos las pruebas de que el total de quioscos disponibles son 20 más de 400 personas.

A los adultos mayores de 40 años no les dan empleo y las opciones de reubicación en casas para mayores no responden. Muchos nosotros fuimos direccionados al IPES donde nos atendió la señora Susana Schifer y quien nos dijo que ella no tenía conocimiento de la problemática y que si queríamos podíamos a aplicar a algún empleo para lo cuál necesitamos experiencia y grado de bachilleres y sencillamente no lo tenemos, y los empleos son por 6 meses.

Convocamos a todas y todos los vendedores informales de la ciudad y personas parte de la clase popular a que nos acompañen el lunes 1 de febrero a las 7 am en la calle 72 con 11 a que nos acompañen pues este arbitrario desalojo que nos pone en vulnerabilidad económica y social hace parte del plan de acción del Gobierno Distrital que es uno y seguramente se replegará a lo largo de la ciudad.
