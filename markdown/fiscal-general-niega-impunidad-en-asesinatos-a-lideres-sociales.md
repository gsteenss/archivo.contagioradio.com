Title: Fiscal General niega impunidad en asesinatos a líderes sociales
Date: 2018-08-31 15:25
Category: DDHH, Nacional
Tags: Fiscal General de la Nación, lideres sociales, Nestor Humberto Martínez
Slug: fiscal-general-niega-impunidad-en-asesinatos-a-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/twitter-e1535747061603.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fiscalía General de la Nación] 

###### [31 Agos 2o18]

Frente a la afirmación que hizo Néstor Humberto Martínez, Fiscal General de la Nación, al decir que en Colombia no hay impunidad en los asesinatos a líderes sociales, el coordinador de la organización Somos Defensores señaló que la Fiscalía conoce que la cifra de impunidad es del 91,9%, en los casos reportados desde 2008 a la fecha.

Martínez, quien hizo estas aseveraciones durante la reunión que sostuvo con el presidente de España, Pedro Sánchez, señaló que desde 2016 a la fecha, **se han presentado 132 asesinatos de líderes sociales**, de los cuales 90 ya han sido esclarecidos, es decir se han producido 18 condenas con sentencias en firme, en 30 procesos ya hay formulaciones de imputación y en 24 se han radicado los respectivos escritos de acusación.

Sin embargo, esas cifras no concuerdan con la que ha registrado Somos Defensores y otras organizaciones defensoras de derechos humanos que han señalado que solo en lo corrido de este primer semestre del año, **se han registrado 123 asesinatos, mientras que de 2016 a la fecha, serían 312 personas asesinadas**. (Le puede interesar[: "¿Por qué renuncias los líderes sociales y por qué evitarlo?"](https://archivo.contagioradio.com/renuncian-lideres-que-hacer-evitarlo/))

### **Las cifras de la Fiscalía** 

De acuerdo con Guevara, "por primera vez, la Fiscalía General de la Nación da resultados y avanza en investigaciones y se debe reconocer que gran parte de ese trabajo se ha hecho bajo el mandato de Martínez", pero al mismo tiempo, señaló que estos resultados también es producto del trabajo y voluntad de la Oficina de Naciones Unidas en Colombia que labora en conjunto con la Fiscalía.

Además, afirmó que la impunidad solo se acaba "en la medida de que haya un responsable en la cárcel pagando por ese delito, se identifiquen los móviles y motivaciones para el asesinato de esas personas y se tengan indagaciones sobre los autores intelectuales" y agregó que en los últimos 9 años solo se han registrado **49 sentencias de más de 600 casos reportados, es decir que solo hay un avance del 8.5% en materia de avances. **

Para tener un reporte total de las víctimas, la Procuraduría General de la Nación le recomendó a la Fiscalía generar su propio sistema de información, donde no solo se documenten los asesinatos a líderes sociales, sino que también se registren otras agresiones que quedan por fuera de las investigaciones. De esta forma se podría saldar el debate sobre las diferentes cifras que se tienen entre organizaciones defensoras de derechos humanos, la Fiscalía y la ONU.

###### Reciba toda la información de Contagio Radio en [[su correo]
