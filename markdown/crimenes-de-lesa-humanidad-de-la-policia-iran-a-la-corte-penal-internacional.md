Title: "Crímenes de lesa humanidad" de la Policía irán a la Corte Penal Internacional
Date: 2020-09-30 10:10
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Carlos Holmes Trujillo, Corte Penal Internacional, Crímenes de lesa humanidad, Iván Duque, Policía
Slug: crimenes-de-lesa-humanidad-de-la-policia-iran-a-la-corte-penal-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes, congresistas, concejales, y defensores de Derechos Humanos; elevaron **una comunicación ante la Fiscalía de la Corte Penal Internacional -CPI-, en la que denuncian la presunta comisión de delitos de lesa humanidad por parte de la Policía,** durante los escenarios de protesta y manifestaciones que han tenido lugar entre el 24 de agosto de 2018 y el 13 de septiembre de 2020. (Le puede interesar: [La violencia policial es la nueva pandemia: Casos a nivel mundial](https://archivo.contagioradio.com/la-violencia-policial-es-la-nueva-pandemia-casos-a-nivel-mundial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la comunicación, se documentan violaciones de los Derechos Humanos por medio de **delitos como tortura, privaciones graves de la libertad física, violencia sexual, deportaciones forzadas y asesinatos, presuntamente cometidos por los uniformados.** (Le puede interesar: [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los solicitantes documentan que **desde que se posesionó Iván Duque como Presidente, han muerto al menos 57 personas por el accionar policial, se han cometido 20 actos de tortura, 1.923 detenciones irregulares y 5 actos de violencia sexual.** Asimismo, agregaron que **14 de las personas muertas, perdieron la vida en la noche del pasado 9 de septiembre luego de la jornada de protestas,** en contra del abuso policial y del asesinato del ciudadano Javier Ordoñez, donde se observó a varios miembros de la Policía disparando de manera indiscriminada contra los manifestantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las cifras fueron recopiladas en medio de audiencias públicas y debates de control político, citados para tratar el tema de la violencia policial ocurrida en las marchas y suministradas por organizaciones defensoras de derechos humanos, como la [Campaña Defender la Libertad](https://twitter.com/DefenderLiberta), que hicieron un seguimiento minuto a minuto de las jornadas de protesta y recogieron las denuncias que les hizo llegar la ciudadanía; junto con información oficial de las autoridades; y reportes de prensa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según los accionantes, las numerosas cifras y su prolongación en el tiempo, **le da el carácter sistemático a los actos ilegales de la Policía, con lo que estarían violando el artículo 7 del Estatuto de Roma, que prevé los crímenes de lesa humanidad.** Instrumento internacional al que se acogió Colombia, desde el año 2009; de ahí que sus disposiciones sean vinculantes para el Estado colombiano y los agentes que lo representan, al tiempo que sustenta la competencia de la CPI para conocer de esas denuncias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, los peticionarios afirmaron que **«*la sistematicidad de estos hechos ha sido acompañada por una ausente investigación penal y disciplinaria en la mayoría de los casos*»;** así como resaltaron que varios de los casos han sido remitidos a la Justicia Penal Militar sobre la que señalan es «*un tribunal que cuenta con jueces militares y que según los estándares internacionales no garantiza los procedimientos que deben regir un juicio justo*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La responsabilidad del Presidente y MinDefensa en el accionar de la Policía

<!-- /wp:heading -->

<!-- wp:paragraph -->

Quienes suscribieron la comunicación, no omitieron la responsabilidad política que cabe sobre el accionar de la Policía señalando que **«*durante la actual administración, el presidente de la República, Iván Duque Márquez y su Ministro de Defensa, Carlos Holmes Trujillo, han sostenido una política de negación de la comisión de estos crímenes y graves hechos, intentado aludir que se trata de casos aislados, como conductas de algunos “malos agentes” o el conocido discurso de las “manzanas podridas"».***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Iván Cepeda, agregó que el Presidente y el Ministro de Defensa, **«*están incumpliendo órdenes judiciales y burlando a la justicia, lo cual, puede ser un detonante de la competencia de la Corte Penal Internacional en Colombia*»**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1310906249786032129","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1310906249786032129

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sobre la responsabilidad puntual de estas dos figuras, como máximas cabezas de la Fuerza Pública en Colombia, ya se habían emprendido acciones ante la misma Corte Penal Internacional el pasado 17 de septiembre. (Lea también: [Anuncian demanda contra Iván Duque y Carlos Holmes por crímenes de lesa humanidad](https://archivo.contagioradio.com/anuncian-demanda-a-ivan-duque-y-carlos-holmes-por-crimenes-de-lesa-humanidad/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
