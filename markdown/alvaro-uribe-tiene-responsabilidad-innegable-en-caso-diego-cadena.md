Title: “Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda
Date: 2020-07-28 21:32
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Corte Suprema de Justicia, Diego Cadena, Iván Cepeda, Manipulación de testigos
Slug: alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Álvaro-Uribe-y-Diego-Cadena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Álvaro Uribe y Diego Cadena / Las2Orillas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito de **la imputación de cargos realizada el día lunes en contra del abogado del expresidente Álvaro Uribe, Diego Cadena y su socio Juan José Salazar**; el senador [Iván Cepeda](https://twitter.com/IvanCepedaCast), víctima en este proceso, concedió una rueda de prensa para pronunciarse en torno al caso. (Lea también: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Cepeda señaló que del acervo probatorio con el que cuenta la Fiscalía y en el cual se fundó para realizar la imputación, se pudo evidenciar **un «*concurso criminal*» en el que participaron los abogados Diego Cadena y Juan José Salazar; y enfatizó que de dicho actuar delictivo salió beneficiado el hoy senador Álvaro Uribe Vélez «*cuya responsabilidad es innegable*».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1288176728339288071","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1288176728339288071

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**«*Ayer quedó demostrado que los abogados Cadena y Salazar no iban a las cárceles a entregar ‘ayudas humanitarias’****, ni eran víctimas de conductas extorsivas, sino que en forma coordinada le habrían ofrecido al exparamilitar Carlos Enrique Vélez la suma de 200 millones de pesos \[…\] a cambio de remitir a la Corte Suprema de Justicia escritos previamente elaborados por \[ellos\]*», sostuvo el senador Cepeda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, se pronunció sobre las **prebendas jurídicas, que reseñó la Fiscalía en su imputación,  ofrecidas por Cadena y Salazar al exparamilitar Juan Guillermo Monsalve** para que este se retractara de sus declaraciones en contra de los hermanos Uribe Vélez como fundadores del Bloque Metro de las Autodefensas Unidas de Colombia -AUC-.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «*existe el peligro real*» de que Cadena y su socio, con dichos antecedentes, busquen también obstruir el juicio en su contra e incluso evadir el actuar de la justicia lo que podría producir «*una fuga de los imputados*».
>
> <cite>Senador Ivan Cepeda</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Iván Cepeda, expresó que ante el actuar fraudulento de los abogados, que les fue imputado por la Fiscalía, «*existe el peligro real*» de que Cadena y su socio, con dichos antecedentes, busquen también obstruir el juicio en su contra e incluso evadir el actuar de la justicia lo que podría producir «*una fuga de los imputados*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, avaló el pedido de medida de aseguramiento elevado por la Fiscalía, haciendo claridad en que una vez superada la emergencia sanitaria producida por el Covid-19 —razón que argumentó el ente acusador  para solicitar la medida en la residencia de los imputados— **era necesario que ambos procesados fuesen dirigidos a un centro penitenciario.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El rol de Álvaro Uribe en la imputación contra Diego Cadena y Juan José Salazar

<!-- /wp:heading -->

<!-- wp:paragraph -->

Reinaldo Villalba, abogado de Cepeda, celebró la imputación realizada por la Fiscalía. No obstante, **argumentó el retraso y dilación que ha tenido el proceso, pues** **«*la denuncia contra el abogado Cadena se hizo hace dos años y medio*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La imputación de la Fiscalía se fundamentó en las visitas del abogado Cadena al exparamilitar Carlos Enrique Vélez en la cárcel de Palmira en 2017 y al también exmiembro de las AUC, Juan Guillermo Monsalve, en la cárcel la Picota en 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Daniel Hernández, fiscal del caso, Diego Cadena habría ofrecido 200 millones de pesos a Carlos Enrique Vélez, de los cuales entregó 48 millones, por intermedio de su socio Juan José Salazar, repartidos a algunos de sus familiares entre los que se encuentran su hermana, su pareja y un sobrino.  **Los dineros habrían sido entregados con el fin de cambiar el testimonio en favor de Álvaro Uribe y para que además dijera falsamente que el senador Cepeda le había ofrecido prebendas para ensuciar el nombre del expresidente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a Monsalve, la Fiscalía argumentó que uno de los ofrecimientos que se le hicieron, consistía en tramitar una revisión sobre su condena y facilitar su ingreso a la JEP; **a cambio de que este se retractara de lo dicho sobre la vinculación de los hermanos Uribe Vélez con la creación del Bloque Metro de las AUC.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otra parte, en la audiencia de imputación, el fiscal Hernández sugirió que Álvaro Uribe habría sido engañado por Cadena, pues este le había ocultado la información de los pagos. Sin embargo, Reinaldo Villalba, aseguró que **definir la responsabilidad del expresidente «*está fuera de las atribuciones de la Fiscalía*»**, ya que, quien ostenta esa competencia es la Sala de Instrucción Penal de la Corte Suprema de Justicia, tribunal que debe determinar, en un trámite judicial diferente, **la responsabilidad de Álvaro Uribe «*como beneficiario de los comportamientos delictivos que adelantaron los dos abogados*».**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué viene para el proceso?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Reinaldo Villalba, **se espera que sean vinculados al proceso más presos que fueron contactados por Diego Cadena** **para hacerles ofrecimientos y prebendas a cambio de modificar su testimonio**, no obstante, señaló que no podía dar a conocer sus nombres porque aún eran parte de «*la reserva sumarial del proceso*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado, también afirmó que  “*de ser diligente la justicia y cumplidora de los términos procesales*” antes de noviembre tendría que haberse presentado el escrito de acusación ante el Juez por parte de la Fiscalía; para que **«*durante el primer semestre de 2021, a más tardar, se esté realizando la etapa de juicio y se tenga una decisión definitiva*» en relación con la responsabilidad del abogado Diego Cadena y socio Juan José Salazar.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
