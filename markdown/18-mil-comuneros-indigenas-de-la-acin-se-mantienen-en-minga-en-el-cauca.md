Title: 18 mil comuneros indígenas de la ACIN se mantienen en minga en el Cauca
Date: 2016-06-09 13:29
Category: Paro Nacional
Tags: ACIN, Bloqueo vía Panamericana, CRIC, Cumbre Agraria, Minga Nacional, PCN
Slug: 18-mil-comuneros-indigenas-de-la-acin-se-mantienen-en-minga-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Bloqueo-Indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Camilo Ara ] 

###### [9 Junio 2016 ]

Por lo menos 18 mil indígenas de la Asociación de Cabildos Indígenas del Norte del Cauca ACIN, **continuarán en Minga con el bloqueo de la vía Panamericana**, pese al pre-acuerdo firmado entre el Gobierno y el Consejo Regional Indígena del Cauca CRIC, según afirma la líder Celia Umanza. Las comunidades se mantendrán en la vía que comunica La María con Caldono y El Pital con La Agustina, los campesinos y afrodescendientes estarán en el siguiente tramo de la vía.

Andrés Almendra, consejero del CRIC, asegura que el pre-acuerdo hecho con el Gobierno para el desbloqueo de la Panamericana se dio porque **se acordó la discusión del tema mineroenérgetico en una comisión que sesionará en La María**; sin embargo, sí no se llegan a avances puntuales las comunidades retomaran el bloqueo, teniendo en cuenta que llevan por lo menos 17 años esperando que el Estado les cumpla.

Pese a la determinación del CRIC, **los 13 procesos que integran la Cumbre Agraria "están más unidos que nunca"**, así lo asegura José Santos, miembro del 'Congreso de los Pueblos' y del 'Proceso de Comunidades Negras', quien agrega que se trata de una negociación a nivel nacional con procedimientos que ya habían sido acordados con el Gobierno, pero que son desconocidos para "romper la unidad del movimiento social".

La Cumbre Agraria según la Mesa Única Nacional y el procedimiento que estableció con el Gobierno, "sigue firme en esta gran Minga y en las carreteras", con el respaldo de las comunidades campesinas, indígenas y afrodescendientes que continúan movilizándose en las diferentes regiones del país **a la espera de que se instale la mesa de negociación para establecer un cordón humanitario** en las vías que permanecen bloqueadas.

<iframe src="http://co.ivoox.com/es/player_ej_11843156_2_1.html?data=kpallpiVeZehhpywj5WZaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncaTZzc7OjbrRpc_uwpCajai2jaShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_11843476_2_1.html?data=kpallpiYe5ehhpywj5aWaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncaLixdeSpZiJhZrnjKbZz8rSqNPVhpewjcjTstTZy8rf0ZDIqc2fpLe2pZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 

 
