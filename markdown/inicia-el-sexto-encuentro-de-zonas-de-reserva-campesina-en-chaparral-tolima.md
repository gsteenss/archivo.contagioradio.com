Title: Sexto encuentro de Zonas de Reserva Campesina en Chaparral, Tolima
Date: 2017-10-14 11:34
Category: Movilización, Nacional
Tags: ANZORC, zonas de reserva campesina
Slug: inicia-el-sexto-encuentro-de-zonas-de-reserva-campesina-en-chaparral-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/anzorc1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Anzorc] 

###### [14 Oct 2017]

Inicia el sexto encuentro nacional de las Zonas de Reserva Campesina, en Chaparral, Tolima, que irá desde este 14 hasta el próximo 17 de octubre, que buscará estrategias para seguir en la construcción de paz territorial, desde la implementación de los puntos 1 y 4 de los Acuerdos de Paz. **De igual forma en el marco de este evento se desarrollará el segundo encuentro de mujeres de las Zonas de Reserva Campesina.**

Al evento asistirán las delegaciones de procesos de Zonas de Reserva Campesina que hay en el país, así como instituciones, académicos, investigadores y organizaciones regionales, nacionales e internacionales, que harán presencia en los diferentes escenarios de debate con la finalidad de crear rutas de trabajo que integren más perspectivas sobre lo que ha sido la construcción de las **zonas de reserva campesina en Colombia y los retos que deberán afrontar de cara a los incumplimientos por parte del gobierno** en la implementación de los acuerdos y la defensa del mismo.

El punto 1 sobre la reforma rural, y el 4 sobre la sustitución de cultivos ilícitos han sido, de acuerdo con los y las campesinas incumplidos en reiteradas ocasiones debido a la falta de compromiso por parte del gobierno de sacar adelante los proyectos de ley que permiten la implementación de los mismos y por la continuación de políticas como la erradicación forzada de cultivos ilícitos, yendo en **contravía de lo acordado y pasando por alto los planes de concertación que se han realizado en diferentes regiones**. (Le puede interesar:["En Colombia la lucha contra la droga es contra los campesinos: César Jerez"](https://archivo.contagioradio.com/en-colombia-la-lucha-contra-la-droga-es-contra-los-campesinos-cesar-jerez/))

De igual forma, frente al segundo encuentro de mujeres de las Zonas de Reserva Campesina pretende avanzar en la construcción de una política pública de mujer a partir de las experiencias se han tejido desde las zonas de reserva campesina y continúa en la defensa de l**a implementación del enfoque de género planteada en los Acuerdos de Paz**.

Durante el encuentro también habrá espacios dedicados a la cultura y a la venta de productos que hacen parte del Mercado Campesino, una iniciativa de producción que permite que las y los campesinos vendan sus productos a partir de la economía campesina.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
