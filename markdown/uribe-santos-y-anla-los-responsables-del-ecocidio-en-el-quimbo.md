Title: Uribe, Santos y ANLA los responsables del ecocidio en El Quimbo
Date: 2016-05-02 17:58
Category: Ambiente, Entrevistas
Tags: ANLA, El Quimbo, Juan Manuel Santos, uribe
Slug: uribe-santos-y-anla-los-responsables-del-ecocidio-en-el-quimbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cf8ikh9UYAAyFJJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ANLA 

###### [2 May 2016] 

A pesar de los múltilples efectos ambientales provocados por la Hidroeléctrica el Quimbo en el departamento del Huila, y las reiteradas denuncias de las comunidades afectadas, el director de la ANLA, Fernando Iregui afirma que han seguido de manera “estricta” los controles.

Sin embargo, de acuerdo con Miller Dussán, investigador de Asoquimbo, lo que ha seguido estrictamente la ANLA son las modificaciones de las licencias ambientales que han estado a favor de EMGESA, empresa a cargo del proyecto El Quimbo. **“35 Veces se ha modificado la licencia ambiental, la última vez se modificó 14 veces para autorizar el llenado de la represa”,** asegura Dussán.

Hoy la ANLA enfrenta dos demandas por este proyecto hidroeléctrico. Una de ellas se encuentra en la Fiscalía 35 desde el 2012, cuando la Contraloría General de República aseguró que los daños socio-ambientales causados por la construcción de la represa tenía un costo de 352 mil millones de pesos. La otra investigación se adelanta por prevaricato por acción, pues pese a los estudios, se realizó el llenado de El Quimbo causando impactos irreparables.

**“Tenían que sustraerse de la zona del embalse mil hectáreas de material orgánico, en consecuencia se afectó el patrimonio nacional, dejando 50.000 m3** de biomasa. La ANLA estuvo presente en el momento del llenado de El Quimbo, los funcionarios que dieron concepto favorable, deberían tener una sanción disciplinaria y penal”, expresa el investigador de Asoquimbo, quien añade que al presidente Juan Manuel Santos también debería llamarse a juicio, pues pasó por encima del fallo del Tribunal Administrativo del Huila, que había suspendido la generación de energía de El Quimbo hasta que EMGESA retirara del vaso del embalse la materia vegetal.

Incluso, según la información de Dussán, la Contraloría dijo que la ANLA nunca fue interlocutora de la comunidad y tampoco defendió los intereses regionales ni de los de los afectados. Así mismo, el más reciente concepto del Tribunal Administrativo del Huila dice que la ANLA actuó como “notario de EMGESA avalando siempre las solicitud de la empresa, en contra de los intereses nacionales, regionales y los afectados del proyecto”.

De acuerdo con lo anterior, para Dussán, **la ANLA, “es el principal autor del ecocidio causado por el proyecto hidroeléctrico de El Quimbo.** Las evidencias son claras y contundentes”.

Iregui había asegurado en una entrevista al periódico El Espectador, que la mortandad de peces se debía a la sobreproducción de los mismos, pero lo cierto, es que la misma Autoridad Nacional de Acuicultura y Pesca advirtió a los pescadores que **la biomasa presente en el embalse contaminó las agua acabando con el 100% de la pesca artesanal, dejando en la miseria a 3.500 familias que vivían de esa actividad**.

A su vez, **la responsabilidad también recae en el gobierno de Uribe Vélez, cuando autorizó El Quimbo por encima de la "Constitución y la Ley**", como lo denuncia Dussán, pues cabe recordar que la década de los 90, el Ministerio de Agricultura consideró inviable el proyecto por el potencial agrícola de los terrenos que podrían ser inundados. Pero como el mismo Iregui lo sostuvo, ese Ministerio en 2008 emitió un nuevo concepto en el cual se le daba viabilidad, pese a los estudios que demostraban los daños ambientales que podían causarse.

Es así como se evidencia que las responsabilidades han sido compartidas por los últimos gobiernos, pero además la ANLA no ha cumplido con su papel, lo que se traduce en la citación que la Fiscalía envió a Fernando Iregui, ahora exdirector de la ANLA para interrogarlo por el presunto llenado irregular de la represa El Quimbo. Iregui es investigado por el delito de prevaricato por acción pues habría actuado sin la autorización debida para emitir un concepto técnico y así atender la petición de EMGESA.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
