Title: Esos muertos no duelen, no los mató las FARC
Date: 2015-04-23 07:26
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: Buenos Aires Cauca, Cauca, Erpac y Oficina del Valle de Aburrá, Fernando Quijano, la esperanza, paz, Rastrojos, Urabeños
Slug: esos-muertos-no-duelen-no-los-mato-las-farc
Status: published

###### Foto: Pablo Ferri 

#### Por  [[**Fernando Quijano**]](https://archivo.contagioradio.com/fernando-quijano/) – [~~@~~FdoQuijano](https://twitter.com/FdoQuijano)

El 14 de abril, en la vereda La Esperanza del municipio de Buenos Aires, Cauca, murieron diez soldados en un ataque perpetrado por la columna móvil de las Farc, ‘Miller Perdomo’; un episodio sangriento en medio de la tregua unilateral decretada por la organización insurgente. Mucho se ha dicho sobre lo ocurrido. Algunos, entre ellos el presidente Santos, consideran que la guerrilla traicionó su palabra e incluso el mandatario afirmó: “**No es con muertos y asesinatos aleves como se construye la confianza. Señores de las Farc la pelota queda de su lado**”, olvidando, una vez más, que la paz es un derecho de todos y su deber es buscarla a pesar de las vicisitudes; en últimas, fue su bandera para ser reelegido.

Las Farc, por su parte, salió a decir que próximamente responderán tres interrogantes relacionados con lo acaecido en La Esperanza: ¿Qué sucedió? ¿Por qué sucedió? ¿Cómo sucedieron los hechos del 14? Concluyen no sin antes dejar el mensaje de no ser los únicos responsables de lo ocurrido: “lo que deseamos manifestar no toca con la versión gubernamental de lo sucedido esa noche. Nuestra aspiración es que en los próximos días se pueda dar respuesta a los tres interrogantes arriba formulados. Eso sí, reconstruyendo los movimientos del ejército desde las vísperas. Su llegada a la zona: A Comedulce, y su desplazamiento a Naranjal, Bellavista y Betulia. Su entrada a La Esperanza. Todo dejando entrever en la región, que no se trataba de un preparativo para una fiesta. Y esto en medio de la tregua unilateral e indefinida declarada por el Secretariado de las FARC-EP el 17 de diciembre de 2014”.

Mientras los enemigos desde diferentes orillas del conflicto debaten sobre la mesa de diálogos y negociación el álgido punto sobre las víctimas, por fuera de ésta el país refleja un forcejeo: Por un lado están los que exigen el fin de los diálogos, rechiflando al presidente, y vociferando que es necesario volver a la **guerra** (que entre otras cosas nunca ha acabado) utilizando, incluso, la muerte de los soldados como florero de Llorente para justificar el fin del proceso de paz; por el otro lado se encuentran los que siguen exigiendo (y en esos me incluyo) el inmediato cese bilateral del fuego, logrando evitar más muertes que sólo sumarían a la larga cadena de dolor que ha producido el conflicto armado, social y político que padece Colombia desde hace más de 50 años.

Todo lo anterior lo reseño para preguntar a los que piden frenar el cierre de una de las puertas de esta guerra: ¿Será que solo duelen los muertos que causan las FARC?

Los guerreristas de oficina, cafetín y redes sociales aparentemente no conocen la realidad de un país en guerra con múltiples expresiones de violencia, incluyendo la gubernamental, muchas veces igualada en crueldad a las organizaciones armadas ilegales que dice perseguir. Hay cientos de muertes violentas en Colombia, las cuales no importa porque no las causó las FARC.

Extrañamente, en la misma región donde se presentó la muerte de los soldados seis indígenas comuneros fueron asesinados en el cabildo Cerro Tijeras de la vereda Agua Clara y en la vereda Agua Bonita del corregimiento de Robles, por hombres de civil que portaban armas largas y se trasladaban en camionetas, coincidiendo, además, con la conmemoración de la masacre del Naya y la muerte de un indígena por arma de fuego en enfrentamientos en Caloto Cauca, entre indígenas y la policía.

Será que los guerreristas orales olvidan que cientos de colombianas y colombianos diariamente mueren en las principales ciudades de Colombia, producto de la violencia que generan las bandas armadas, muchas de ellas al servicio de estructuras paramafiosas como Urabeños, Rastrojos, Erpac y Oficina del Valle de Aburrá.

En últimas, esos muertos no importan porque sus verdugos no ponen en riesgo el *statu quo* del cual se alimentan muchos de los que hoy claman por la guerra. La hipocresía se siente en cada clamor que realizan.
