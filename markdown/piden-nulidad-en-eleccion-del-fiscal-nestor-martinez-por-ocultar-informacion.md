Title: Las razones para pedir nulidad en elección del Fiscal Néstor Martínez
Date: 2019-01-14 16:19
Author: AdminContagio
Category: Judicial, Nacional
Tags: Comisión Colombiana de Juristas, Dejusticia, Fiscal General de la Nación, Nestor Humberto Martínez
Slug: piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion
Status: published

###### [Foto: Contagio Radio] 

###### [14 Ene 2019] 

Organizaciones defensoras de derechos humanos demandaron la elección del Fiscal Néstor Humberto Martínez, argumentando que la Corte Suprema de Justicia realizó su elección con información incompleta y e insuficiente, ya que Martínez no mencionó al organismo que durante el tiempo en que ejerció como abogado privado "**conoció múltiples irregularidades que rodeaban los contratos del tramo II de la Ruta del Sol".**

Los abogados de las organizaciones Dejusticia, Comisión Colombiana de Juristas y Jorge Iván Cuervo, pidieron en la demanda presentada ante el Consejo de Estado la nulidad del Acuerdo 871 de 2016, con la que la Corte eligió al Fiscal, exponiendo que tienen la evidencia suficiente para comprobar que Martínez omitió  los conflictos de interés potenciales y aparentes que tenía en relación con investigaciones.

### **Las pruebas contra Martínez ** 

A través de un comunicado de prensa, las organizaciones señalaron que tras la muerte de Jorge Pizano, controller del contrato para la construcción del** tramo II de la Ruta del Sol, **medios de información revelaron grabaciones en donde este informó a Martínez sobre las irregularidades que había encontrado en la ejecución de ese contrato y posteriormente le mencionó al actual Fiscal los posibles delitos relacionados.

Hecho que evidencia que Martínez si estaba enterado de actos de corrupción que alcanzan la cifra de \$33.000 millones de pesos y que involucra a varios de sus anteriores clientes, como abogado privado, asimismo, el Fiscal manifestó a la opinión pública que, en ese entonces, se le encomendó **la redacción de un contrato de transacción para conciliar los dineros** “mal gastados” entre Odebrecht y los demás miembros de la concesión. (Le puede interesar: ["Fiscal general fue amenazante porque sabe que lo desenmascaramos: Iván Cepeda"](https://archivo.contagioradio.com/fiscal-general-fue-amenazante-porque-sabe-que-lo-desenmascaramos-ivan-cepeda/))

Al ocultar esta información a la Corte Suprema de Justicia, "**los magistrados consideraron que él estaría en las mejores condiciones para desempeñar las funciones propias del cargo", **sin analizar los conflictos de intereses que tenía con Odebrecth  y la vinculación en este caso de empresas y personas cuyos intereses había defendido. Hecho que probaría que la motivación de la Sala Plena al elegirlo resultó ser falsa.

### **¡Fuera Fiscal Néstor Humberto Martínez!** 

Para las organizaciones, un pronunciamiento del Consejo de Estado ante este caso es importante, debido al ocultamiento de información sustancial por parte de Martínez a la hora de ser ternado y la legitimidad que debe tener la Fiscalía a la hora de investigar la corrupción en el país. De igual forma le exigieron al Fiscal que **cumpla con su deber moral de ser modelo en la lucha contra la corrupción** y que ofrezca garantías de imparcialidad judicial reales y aparentes.

A estas exigencias, se sumaron las de miles de ciudadanos el pasado 11 de enero, que asistieron a las diferentes sedes de la Fiscalía General de la Nación en el país, para **demandar la renuncia de Martínez por los hechos de corrupción** en los que se le ha involucrado en relación a Odebrecht y las personas que representaba. (Le puede interesar:["Con plantón y linternas, ciudadanía exigirá salida del Fiscal"](https://archivo.contagioradio.com/ciudadania-se-moviliza-para-exigir-la-renuncia-del-fiscal/))

###### Reciba toda la información de Contagio Radio en [[su correo]
