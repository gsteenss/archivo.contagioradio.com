Title: Reinsertados piden ser parte de la mesa de diálogos en la Habana
Date: 2016-08-11 14:23
Category: Nacional, Paz
Slug: reinsertados-piden-ser-parte-de-la-mesa-de-dialogos-de-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/excombatientes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [11 de Agost] 

A través de un comunicado de prensa, **reinsertados de diferentes grupos alzados en armas**, miembros de la Mesa Nacional de Ex- Combatientes y en compañía la Fundación Juntos de la mano por un camino mejor, **expresaron su propuesta hacia la mesa de diálogos de la Habana de poder participar en ella como sujetos víctimas del conflicto** armado en el país y como constructores de paz, a partir de sus experiencias de reinserción y participación en la sociedad.

En el comunicado los reincertados aseguran que si bien es cierto que apoyan las dinámicas en la que se ha desarrollado el proceso de paz en la Habana, e**s importante que se les tenga en cuenta como actor fundamental y participativo dentro de la ciudadanía** que ha trabajado en torno a una propuesta que generaría mayor inclusión de los desmovilizados hacia la sociedad, **la propuesta consiste en construir espacios que fortalezcan las democracia desde los territorios como lo serían los campamentos por la paz y la Escuela Nacional de Paz.**

En el documento la Mesa Nacional de Ex- Combatientes afirma que se dispondrán para la paz - **"Nos declaramos militantes del amor** y esperamos el llamado para ir al frente y en cada región o en cualquier lugar de Colombia, reconstruir las diferencias, la diversidad, los sueños y el futuro, que es la única forma para que la vida prevalezca".

Por otro lado, Jefferson Perea presidente de la Mesa Nacional de Ex- Combatientes, aseguro que **"así como tuvimos la capacidad para hacer la guerra, también les aseguramos que tenemos la capacidad de llegar a los campos, a los barrios marginales y hacer la paz**".

El próximo evento que realizará la Mesa Nacional de Ex- Combatientes y la Fundación Juntos de la mano por un camino mejor será un **torneo de microfutbol que reúna equipos de todos los sectores sociales afectados por el conflicto armado,** mientras esperan la respuesta de las delegaciones de paz en la Habana, a su vez reiteraron el llamado al ELN para iniciar las conversaciones en la mesa con el gobierno.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

 
