Title: Se verificará si hubo heridos civiles en operación contra "Megateo"
Date: 2015-10-02 12:18
Category: DDHH, Nacional
Tags: Acarí, ASCAMCAT, Aserrío, Catatumbo, EPL, Fuerzas Armadas, Juan Manuel Santos, Megateo, Norte de Santander, operativo militar
Slug: se-verificara-si-hubo-heridos-civiles-en-operacion-contra-megateo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/cms-image-000039192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [confidencialcolombia.com]

###### [2 Oct 2015]

Esta mañana, a través de su cuenta de twitter, **el presidente de la República, Juan Manuel Santos, confirmó que  el comandante máximo del EPL, Víctor Ramón Navarro Serrano, 'Megateo',** murió  durante un operativo de las Fuerzas Armadas en Catatumbo, Norte de Santander.

De acuerdo con César Jerez, vocero de la Asociación de Zonas de Reserva Campesinas, ANZORC, la comunidad asegura que se produjeron bombardeos desde las 11 de la noche del día anterior, debido a que se generaron combates entre miembros de la fuerza pública con la guerrilla del EPL en el corregimiento San José del Tarra, municipio de Hacarí, en Norte de Santander.

Aún no se tiene información sobre personas civiles que hayan terminado heridas por cuenta del operativo de las Fuerzas Armadas, sin embargo, según Jeréz, s**e solicitará una verificación de las autoridades competentes como la Personería y la Defensoría del Pueblo del departamento, para que se verifique esa información. **

Algunos campesinos, relatan que hace dos días la zona estaba siendo sobrevolada por dos aviones de guerra mientras se adelantaba una operación contra Navarro Serrano. Los agricultores aseguran todavía continúan los enfrentamientos entre la guerrilla y el Ejército.

El comandante máximo del EPL, durante años fue una de las personas  más buscadas por el gobierno debido al negocio del narcotráfico que lideraba y que realizaba en la frontera, según fuentes oficiales.

Cabe recordar que meses atrás, ASCAMCAT había denunciado que por cuenta de los operativos que se adelantaban contra “Megateo”, **la fuerza pública estaba poniendo en riesgo a los habitantes del corregimiento "el Aserrío" violando el Derecho Internacional Humanitario.**
