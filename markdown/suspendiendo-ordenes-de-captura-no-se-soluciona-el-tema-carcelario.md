Title: “Suspendiendo órdenes de captura no se soluciona el tema carcelario”
Date: 2017-06-17 11:00
Category: DDHH, Nacional
Tags: carceles, hacinamiento, órdenes de captura, sistema carcelario
Slug: suspendiendo-ordenes-de-captura-no-se-soluciona-el-tema-carcelario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/mujeres-en-las-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo] 

###### [16 Jun. 2017] 

"La problemática carcelaria se resuelve abriendo el debate, encontrando soluciones de base, de fondo y no desde los medios y suspendiendo órdenes de captura como lo está haciendo la Fiscalía", así lo aseguró **Fernando Quijano director de la Corporación para la Paz y el Desarrollo Social – Corpades** – a propósito de la decisión de esa institución de suspender órdenes de captura.

La decisión se da luego de que el Instituto Nacional Penitenciario y Carcelario – INPEC – hubiera manifestado que no recibirá más sindicados, dado el hacinamiento que viven las cárceles de Antioquia que superan el 200%.

Para Quijano **la responsabilidad de esta situación no solo le atañe al INPEC, sino que también deben hacerse responsables el Gobierno nacional, departamental y municipal** y llamó la atención al decir que “esa situación tan grave por la que atraviesa el tema carcelario no es solo en Medellín sino en muchas partes de Colombia”.

### **“El debate es mucho más profundo” Quijano** 

Pareciera ser una noticia grave la suspensión de las órdenes de captura, pero para Quijano no es así, asevera que el debate es mucho más profundo y dice que lo que hay que preguntarse es ¿a quiénes se les expiden órdenes de captura en el Valle de Aburrá o Medellín? Le puede interesar: ["Condiciones en las cárceles Colombianas son preocupantes" Alan Duncan](https://archivo.contagioradio.com/condiciones-en-las-carceles-colombianas-son-preocupantes-alan-duncan/)

**"Se necesita un debate mucho más profundo que responda a ¿qué hacer con el tema penintenciario en Colombia?** ¿qué hacer con el hacinamiento? ¿con la política criminal?" recalca.

Añade que en la actualidad quienes se encuentran en las cárceles son personas que están por Ley 30 o por delitos que podrían resolverse con servicio social o en las Comunas de Medellín “**la responsabilidad es de un Estado que cree que la cárcel se hizo para llenarse de pobres**. Porque los grandes culpables gozan de grandes pabellones, lujos o tranquilidad”.

### **La violencia está disparada en Antioquia** 

Para Quijano con o sin suspensión de órdenes de captura Antioquia está viviendo enfrentamientos armados muy fuertes y menciona lugares como la parte alta de Comuna 16, homicidios en Comuna 7 en los cuales incluso “se está involucrando sectores de Comuna 5 y 6 en Medellín”. Le puede interesar: [En Colombia la población en cárceles aumentó 141%  desde el año 2000](https://archivo.contagioradio.com/en-colombia-la-poblacion-en-carceles-aumento-141-desde-el-ano-2000/)

Así mismo, dice que Bello está en manos del crimen y un porcentaje importante del Valle de Aburrá está en manos de estructuras paramilitares, de mafiosos y sus bandas “entonces decimos qué más podría afectar esta decisión que toma la Fiscalía. **Suspender órdenes de captura no disparará criminalidad, eso está disparado hace rato”** relata Quijano.

### **La Fiscalía debe dejar de lavarse las manos** 

Dice Quijano que este tipo de decisión tomada por la Fiscalía da cuenta de cómo se están lavando las manos entre diversas entidades, para que endilgar responsabilidades a otras instituciones y que **contrario a esa actitud lo que se debería hace es “trabajar por tratar de resolver el tema de hacinamiento** que es una flagrante violación a los derechos humanos”. Le puede interesar: [En las cárceles de Colombia existe pico y placa para poder dormir](https://archivo.contagioradio.com/en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir/)

Y además hizo un llamado al Alcalde de Medellín y al Gobernador de Antioquia a debatir seriamente sobre la resocialización de las personas que son capturas o sindicadas “debemos buscar soluciones, no podemos seguir echándonos la culpa entre unos y otros, ni vivir de buenas intenciones. **Pasemos más allá de estar preocupados en la redes sociales”.**

### **Hay que seguir trabajando en conjunto** 

Por último, relata Quijano que **una de las soluciones es debatir sobre una política criminal seria, responsable y acorde con un país que está en tránsito a la paz** “el Estado colombiano debe sentarse y darle una real salida a este problema, pero no sé si la solución sea suspender órdenes de captura, porque lo que se está creando es una zozobra a la sociedad y no resolviendo el problema de fondo”.

Pese  esta polémica suscitada el ministro de Justicia, Enrique Gil Botero, aseguró que no es cierto que se hayan suspendido las órdenes de captura y que durante éste jueves se realizaron 37 capturas.  Le puede interesar: [La alarmante situación de las cárceles colombianas](https://archivo.contagioradio.com/39173/)

<iframe id="audio_19310465" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19310465_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
