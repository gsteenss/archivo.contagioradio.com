Title: Corto documental La Roja, cerveza de la paz
Date: 2019-09-01 18:49
Author: CtgAdm
Category: Nacional
Tags: Cerveza La Roja, etcr, Implementación del Acuerdo, Proyectos Productivos, Tolima
Slug: lanzamiento-corto-documental-la-roja-cerveza-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/La-Roja.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

De la vereda La Fila en Icononzo, Tolima, nace La Roja: un proyecto de cervecería artesanal impulsado por excombatientes y que según sus creadores representa la construcción de una historia de más de 100 años en los territorios y que hoy llega de la mano con el corto documental: La Roja, la cerveza de la paz.

Rubén, productor de La Roja señala que a medida que la producción avanza han podido llegar a restaurantes, panaderías y demás locales y con el tiempo, esperan poder llevar la bebida fermentada con más frecuencias a lugares aledaños como Melgar y Girardot. [(Lea también: La Roja, una cerveza artesanal fruto de la reincorporación)](https://archivo.contagioradio.com/roja-cerveza-artesanal-reincorporacion/)

Con el aporte del profesor universitario Wally Broderick y la voluntad y disposición de los excombatientes fueron creados los primeros 25 litros de una cerveza que **"sabe a esperanza, a solidaridad y a fraternidad"** tal como expresa Valentina Beltrán, quien también hace parte de esta iniciativa.

"Creemos que La Roja debe ser un proyecto que propenda por el desarrollo, no solo de los excombatientes sino de las comunidades que están cercanas a nosotros", bajo esa premisa nace La Roja, la cerveza de la paz.

Corto documental producido por Contagio Radio, lanzamiento: Domingo 1 de septiembre,  8 p.m en nuestra cuenta de [Facebook ](https://www.facebook.com/contagioradio/)

<iframe src="https://www.youtube.com/embed/xxvu2N_2wxU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
