Title: Corrupción también tuvo su parte de responsabilidad en avalancha de Mocoa
Date: 2017-04-03 16:30
Category: Ambiente, Nacional
Tags: avalancha, CAR, Mocoa. POT
Slug: corrupciontambienfueculpabledeavalnchademocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa-5-e1491163018613.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz ] 

###### [03 Abr. 2017] 

**Aunque la naturaleza tuvo su parte en la situación que aconteció en Mocoa, la corrupción también ha hecho su trabajo**. Las modificaciones a los Planes de Ordenamiento Territorial sin importar los riesgos, los intereses políticos que se mueven con las urbanizadoras y la falta de presupuesto con el que cuentan los municipios para atender estas emergencias, son tan solo otras situaciones que suman a la crisis humanitaria por la que atraviesa Mocoa.

**El municipio de Mocoa está situado en un lugar que es muy vulnerable**, pues se encuentra en un pie de monte en una región que es muy lluviosa y adicionalmente el sistema montañoso colombiano es muy joven, lo que quiere decir que los deslizamientos que se presentan son naturales, y por otra parte todo el tema de bosques ha sido muy afectado por la acción humana en Colombia. Le puede interesar: [Los más pobres las principales víctimas de la avalancha en Mocoa, Putumayo](https://archivo.contagioradio.com/a-2-dias-de-la-avalancha-la-situacion-de-mocoa-sigue-siendo-critica/)

Para Manuel Rodríguez, ex ministro de Ambiente “uno de los problemas que tenemos en Colombia es que es uno de los pocos países del mundo **tan habitados en la zona montañosa**, y eso ha generado unas afectaciones. **Otro problema es que con frecuencia se deforesta en forma innecesaria para hacer ganadería** y evidentemente los ríos que atraviesan Mocoa están muy afectados por la deforestación”.

Según Rodríguez, en el país existe un problema y es que los planes y las leyes existen, pero no se cumplen “la Corpoamazonía en el Plan de Ordenamiento Territorial – **POT – objetó que se construyera en muchas áreas del barrio San Miguel, prácticamente sepultado por la avalancha.** Si se objeta eso por la Corporación Autónoma Regional - CAR -, eso se convierte en un mandato de ley y no se puede aprobar una urbanización si la CAR lo ha objetado”.

En el caso de Mocoa, muchas de las personas que vivían en sitios que no eran urbanizables son muy pobres, que han sido desplazadas de sus territorios y que no han encontrado otro lugar para vivir “esto hace que **muchas de estas personas construyan casi que en el cauce del río o en la rivera propia"** añadió Rodríguez.

El problema ahora es que muchos de los POT están siendo modificados de manera continua y "muchas veces **esas modificaciones se hacen en favor de unos urbanizadores, de unos intereses políticos,** pasando por encima del tema del riesgo" contó Rodríguez.

De acuerdo a un estudio que hizo la Universidad Nacional, **en todos los ríos que atraviesan Mocoa debía haber por lo menos una distancia de 20 metros sin urbanizar**, lo cual implica que se debe hacer una reubicación de viviendas y hacer obras. Le puede interesar:[ No fue un "desastre natural" la tragedia en Mocoa](https://archivo.contagioradio.com/no-fue-desastre-natural-lo-ocurrido-mo)

Como si esto no fuera suficiente, en la actualidad Mocoa no cuenta con los recursos necesarios para poder hacer mejoras a sus construcciones, sin embargo habría que preguntarse ¿qué pasó con el tema del POT y el ordenamiento territorial en este municipio?.

La pregunta que surge, asegura el ex ministro es ¿por qué no se ha cumplido con la norma o con la ley? a lo que responde que uno puede **“encontrarse con muchos casos en los que hay fuertes intereses que impiden que eso se haga.** Como Mocoa están muchos municipios de Colombia (…) y allí viven gentes de muy bajos recursos. Estamos frente a un fenómeno de inequidad social, pobreza y de urbanizadores inescrupulosos y eso requiere intervención”.

Para el exministro el tema no se reduce solamente a Mocoa y cita a la capital "yo recuerdo en la Sabana de Bogotá muchas veces han sido denunciados los grandes actos de corrupción (...), es que si usted tiene una zona no urbanizable y se la declaran urbanizable el valor del predio se multiplica por 100 o 50 veces, (...) **los políticos son los que están en los concejos municipales y muchos de esos en alianza con el sector privado permiten la urbanización de zonas de alto riesgo,** como lo vimos en Bogotá en la pasada ola invernal".

Ahora lo que queda, asegura Rodríguez es investigar para poder saber qué fue lo qué pasó y aprender una lección “**¿por qué hay gente viviendo en zonas de alto riesgo en Mocoa? ¿quién permitió que eso fuera así?** ¿por qué no se corrigió esa situación desde el 2012 con el mapa de riesgo? Ahí hay cosas que aprender e incluso puede haber actos de corrupción.

### **Hay que elegir mejores gobernantes** 

Ojalá, agrega el defensor del ambiente que este asunto no se olvide con el pasar de los días **“ojalá que todos los colombianos no tengamos tan mala memoria y que busquemos exigirle al gobierno, a las entidades públicas y a quienes sean responsables del sector** privado todas las acciones para que se eviten estas tragedias tan dolorosas como la que ha ocurrido en Mocoa. Lo que pasó en Mocoa es un anunció de lo que puede pasar en otros lugares del país incluyendo las grandes ciudades” puntualizó.

<iframe id="audio_17932029" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17932029_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
