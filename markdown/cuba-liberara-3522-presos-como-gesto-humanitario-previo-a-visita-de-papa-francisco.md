Title: Cuba libera 3522 presos como gesto humanitario previo a visita de papa Francisco
Date: 2015-09-11 13:39
Category: El mundo, Otra Mirada, Política
Tags: Bloqueo económico, Bloqueo económico de Cuba, Cuba, Estados Unidos, Indulto a 3522 cubanos, Leyes de indulto, presos politicos, visita del Papa
Slug: cuba-liberara-3522-presos-como-gesto-humanitario-previo-a-visita-de-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Cuba-si.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cubasi.cu 

<iframe src="http://www.ivoox.com/player_ek_8321284_2_1.html?data=mZifk5eceI6ZmKiakpyJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bWwpDZy8fJtsLmhqigh6aVb5Spk5eY0tfJt9DnjMjcz9SPq8bn1dSYytrRpc%2Fd1cbfy9SPtNPZ186ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Felix Albizú, Prensa Latina] 

###### [11 sep 2015] 

En el marco de la visita del Papa el domingo 13 de septiembre, **el gobierno cubano anunció indultos para 3522 detenidos** teniendo en cuenta los hechos por los cuales fueron sancionados, su comportamiento y condiciones de salud. Las personas quedarían en libertad en las próximas 72 horas.

Felix Albizú, corresponsal de Prensa Latina, indica que “estas **acciones humanitarias representan algo positivo**” y que son una acción común para el gobierno de la isla, ya que en visitas anteriores de los máximos jerarcas de la iglesia católica se han presentado situaciones similares.

El Concejo de Estado cubano indicó que estos indultos serán para personas mayores de 60 años de edad y jóvenes menores de 20 años sin antecedentes penales, aunque cuentan con algunas **excepciones para delitos** como asesinato, Homicidio, Violación, Pederastia con Violencia, Corrupción de Menores, Hurto y Sacrificio Ilegal de Ganado Mayor, Tráfico de Drogas, Robo con Violencia e Intimidación en las Personas en sus modalidades agravadas, ni aquellos por delitos contra la Seguridad del Estado.

En 1998, unos 200 presos fueron liberados con motivo de la visita del papa Juan Pablo II a la isla y a finales de 2011, el gobierno castrista liberó a más de .2900 como previa a la visita del papa Benedicto XVI en 2012 y por el 400 aniversario de la Virgen de la Caridad del Cobre, patrona de Cuba.

### **Cuba se engalana para visita del Papa:** 

Albizú, de Prensa Latina, afirma que para esta visita del Papa Francisco, la plaza de la revolución se preparó con mucho más tiempo que para las anteriores “*se hicieron construcciones muy bellas y de calidad, para adornar esta visita*”, la cual “***es muy importante para Cuba y América latina, con un Papa que conoce de cerca las realidades de nuestro continente***", dijo el corresponsal de prensa latina.

### 

### **Mediación del papa Francisco en acercamientos entre Cuba y Estados Unidos** 

Según diversas informaciones divulgadas el Papa Francisco habría sido fundamental en los acercamientos entre Cuba y Estados Unidos que avanzaron hacia el **restablecimiento de las relaciones diplomáticas entre las dos naciones y la reapertura de las embajadas**.

Sin embargo los avances en cuanto al **bloqueo económico de EEUU hacia Cuba y las leyes que favorecen e impulsan la migración de Cuba hacia ese país persisten** a pesar de los esfuerzos del cuerpo diplomático cubano para que se levanten las sanciones impuestas desde hace más de 50 años.

### **Visitas  anteriores de Papas a la Isla:** 

En el año de 1998 el 21 de enero, el Papa Juan Pablo II dio una visita de cinco días a Cuba y **se pronunció por la “globalización de la solidaridad”** y en **contra del bloqueo económico cubano**.

En el año 2012 el Papa Benedicto XVI visitó a Cuba del 26 al 28 de marzo en donde indicó que "Con las armas de la paz, el perdón y la comprensión, l**uchen para construir una sociedad abierta y renovada**, una sociedad mejor, más digna del hombre, que refleje más la bondad de Dios".
