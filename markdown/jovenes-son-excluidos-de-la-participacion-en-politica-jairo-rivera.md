Title: Jóvenes son excluidos de la participación en política: Jairo Rivera
Date: 2017-08-23 15:31
Category: Nacional, Política
Tags: Generación de paz, jovenes, Reforma Política
Slug: jovenes-son-excluidos-de-la-participacion-en-politica-jairo-rivera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fot0: Contagio Radio] 

###### [23 Ago 2017] 

La organización Voces de Paz radicó en el Congreso una proposición para que aumente la participación de los jóvenes en política, a partir de la disminución de la edad para aspirar a cargos tanto en el Senado como en la Cámara, y de esta manera **“se llene de vida el Capitolio”**. Para Jairo Rivera integrante y vocero de esta organización esta propuesta permite que la generación que construirá la paz de Colombia se empodere a partir del ejercicio de su ciudadanía.

El proyecto busca que, en el marco de la reforma electoral, se pueda reducir la edad de los jóvenes para presentarse a las corporaciones públicas como Senado, **en donde actualmente deben tenerse más de 28 años y Cámara, con más de 23 años**. (Le puede interesar: ["Prohibición del paramilitarismo sería un paso necesario para reconcilición"](https://archivo.contagioradio.com/prohibicion-del-paramilitarismo-seria-un-paso-necesario-para-la-reconciliacion/))

Rivera manifestó que actualmente los jóvenes no están en la política del país y que son excluidos, hecho que se materializa en los altos niveles de abstención y escenarios como los institucionales, “hay una suerte de fobia por parte de la vieja clase política a que los jóvenes se empoderen y **hagan parte de espacios de deliberación y decisión política**” afirmó Rivera.

Además, manifestó que la "generación de la paz" ha demostrado un fuerte sentimiento contra la corrupción, por la defensa del ambiente y el pluralismo que se ha evidenciado en la movilización social y que, en ese sentido, **la mayor tarea que tendrán que afrontar será la realización de un pacto político que supere la violencia hacia las diferencias ideológicas**.

La propuesta deberá ser aprobada por la Comisión Primera de la Cámara y posteriormente pasar a plenaria para que sea puesta en marcha, sin embargo, Rivera aseguró que espera que este proyecto también tenga una acogida y respaldo al interior de las juventudes que permita presionar a congresistas para ampliar el debate. (Le puede interesar: ["Partidos políticos logran acuerdo para impulsar reforma política"](https://archivo.contagioradio.com/partidos-minoritarios-buscan-fortalecer-su-incidencia-en-la-contienda-politica/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
