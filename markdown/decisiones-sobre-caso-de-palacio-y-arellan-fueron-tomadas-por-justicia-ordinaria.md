Title: Decisiones sobre caso de Palacio y Arellán fueron tomadas por Justicia ordinaria
Date: 2017-05-15 14:34
Category: Entrevistas, Nacional, Paz
Tags: Diego Palacios, jurisdicción especial para la paz
Slug: decisiones-sobre-caso-de-palacio-y-arellan-fueron-tomadas-por-justicia-ordinaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [15 May 2017] 

La polémica despertada por la liberación de Herminsul Arellán Barajas, autor del atentado del Club el Nogal y la negativa a el ex ministro Diego Palacio para entrar a la Jurisdicción Especial de Paz no debería tener comparación, así lo señaló el abogado Enrique Santiago quién además expresó, que aún la **JEP no ha comenzado a funcionar y por la tanto el debate es producto de la tergiversación de información**.

### **La JEP no ha entrado en funcionamiento ** 

En primera medida Enrique Santiago indicó que las decisiones judiciales que se han tomado en ambos casos no han estado bajo el marco de la Jurisdicción Especial para la Paz, sino bajo la justicia ordinaría colombiana. Le puede interesar: ["10 razones de víctimas contra Decreto que suspende ordenes de captura a  la Fuerza Pública"](https://archivo.contagioradio.com/decreto-concede-trato-preferencial-a-victimarios-miembros-de-la-fuerza-publica/)

Específicamente sobre el caso de Diego Palacio afirmó que **“ni siquiera se conoce la resolución judicial”** por lo tanto no se conocen los motivos para denegar la libertad condicional y agregó que podrían existir dos motivos distintos: el primero de ellos es que los jueces ordinarios hayan entendido que los delitos por los que está condenado Palacio no son competencia de la JEP y el segundo podría ser que Palacio no cumpla con los requisitos para acceder a la libertad condicional, como llevar 5 años en prisión.

Palacio había enviado una carta a la secretaría de la JEP para que su caso fuese parte de este tribunal como requisito previo para pedir amnistía o cualquiera de las otras medidas que se encuentran en la ley, no obstante, **este órgano aún no tiene funciones jurisdiccionales, y por ahora solo cumple con la labor de levantar actas** y dar fe, de la manifestación de personas de someterse a la JEP.

### **Libertades transitorias una medida provisional** 

Frente a las libertades transitorias que se han ido concediendo, Enrique Santiago explicó que estas hacen parte de la Ley de Amnistía y **del sistema transitorio, que se pactó en los Acuerdos de La Habana, que soluciona provisionalmente las situaciones** de todas aquellas personas que han participado del conflicto armado, para que no tuviesen que esperar año y medio mientras se instala la JEP. Le puede interesar: "

La Jurisdicción Especial para la Paz, además cuenta con dos salas: una de amnistía e indulto y otra de definición de situaciones jurídicas, que otorgarán medidas equivalentes a las previstas en la Ley de Amnistías, **la primera sala lo hará para integrantes de las FARC-EP, mientras que la segunda lo hará a miembros de la Fuerza Pública y agentes estatales**.

### **"La comparación no es esa" Enrique Santiago** 

En tercera medida el abogado, expresó que la comparación que se ha hecho entre los casos de Arellán y Palacio, no tiene sentido “me sorprende que la comparación sea entre esta persona condenada por los hechos del Nogal y Diego Palacio, la comparación no es esa, **sería entre esta persona y el General Uscátegui o los militares puestos en libertad, acusados de falsos positivos que es un crimen de lesa humanidad**”.

De igual forma, manifestó que estos hechos lo que demuestran es que “**la justicia ordinaria viene poniendo en libertad a ambas partes, implicadas en el conflicto**, y además ante delitos de la misma gravedad”.

Enrique Santiago reiteró que estas polémicas lo único que hacen es confundir a la opinión pública y “degradar la Jurisdicción Especial para la Paz”. Le puede interesar:["Decreto dejaría en libertad a agentes estatales que han violado derechos humanos"](https://archivo.contagioradio.com/gobierno_decreto_agentes_estatales/)

### **Queremos toda la verdad: Berta Fries ** 

Berta Fries, víctima del atentado al Club el Nogal, expresó referente a la libertad condicional de Arellán, que “esa es una decisión judicial de un juez ordinario” y que entiende que es una libertad condicional que pone a los sujetos a disposición de las condiciones de la JEP y que al ser **“condicional” significa que Arellán debe comparecer para reconocer la verdad y responsabilidad en el caso del Nogal** y recibir una sanción restaurativa.

**“Si no dice la verdad, entonces se devolverá a la cárcel, lo que debemos hacer es pedagogía desde el punto de vista jurídico”** afirmó Fries y agregó que lo que las víctimas del Nogal esperan es una reparación que incluya la verdad del Estado frente a por qué se permitió el atentado y qué detalles hay de fondo.

y agregó que comprende las reacciones contra la decisión del Juez, sin embargo expresó que "**sacaron a un personaje de lo más tenebroso de la cárcel**, pero quiero que a mi me digan por qué a las víctimas no nos han respondido, llevamos 14 años y no hemos recibido ni una curita". Le puede interesar: ["El Estado sabía que se iba a perpetrar un atentado en el Club El Nogal"](https://archivo.contagioradio.com/estado-sabia-que-se-iba-a-perpetrar-el-atentado-del-club-el-nogal/)

###### Reciba toda la información de Contagio Radio en [[su correo]
