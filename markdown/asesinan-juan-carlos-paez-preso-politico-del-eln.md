Title: Asesinan a Juan Carlos Páez preso político del ELN
Date: 2018-02-24 21:27
Category: DDHH, Nacional
Slug: asesinan-juan-carlos-paez-preso-politico-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/18814561.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: panoramio] 

###### [24 Feb 2018]

La denuncia la hace el Colectivo Rafael Lombana Cabrera que agrupa a presos políticos del ELN. En el escrito informan que Juan Carlos Páez, quien había sido detenido hace dos años y medio, fue amparado con la medida de prisión domiciliaria, trasladado a San Pablo en Bolívar y la misma noche fue asesinado por sujetos que llegaron hasta su lugar de residencia.

Páez había sido condenado por el delito de rebelión y la medida de prisión domiciliaria se le otorgó después de dos años y medio de estar detenido en la cárcel de mediana seguridad de “Palogordo”  en Girón, Santander. El prisionero **fue trasladado a su casa familiar en San Pablo, Bolívar el pasado 20 de febrero de 2018 en horas de la mañana.**

Justo, el mismo 20 de febrero, en horas de la noche, dos hombres que se movilizaban en una motocicleta dispararon contra Páez en su propia casa, ubicada en el barrio La Victoria, según información inicial, no había ningún tipo de seguridad. Además denuncian que en el ataque armado también resultó herido un habitante de la misma localidad.

Los integrantes del Colectivo Rafael Lombana Cabrera, exigieron al Estado colombiano una investigación pronta y eficaz para evitar que este tipo de situaciones quede en la impunidad y advirtieron que el casco urbano de San Pablo ha sido foco de una fuerte militarización y también cuenta con presencia policial constante.

 

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
