Title: La violencia obstétrica, otra forma de violencia contra la mujer
Date: 2015-11-25 11:18
Category: Mujer, Nacional
Tags: dia de la no violencia contra la mujer, Ley orgánica de los derechos de la mujer, Mujeres Bachue, Parto Humanizado, Venezuela, Violencia Obstétrica
Slug: la-violencia-obstetrica-otra-forma-de-violencia-contra-la-mujer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/violencia_obstétrica_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: [cinthiapomaski]

###### [25 Nov 2015]

La violencia obstétrica ha sido definida, según la UNESCO como “el tipo de violencia ejercida por el profesional de salud sobre el cuerpo y los procesos reproductivos de las mujeres”. En Colombia no se habla del tema como un escenario objeto de debate pero en Venezuela ya se establecieron las formas de maltrato, siendo el primer país del mundo en tipificar y castigar las formas de violencia obstétrica.

En ese país la “ley orgánica sobre el derecho de las mujeres a una vida libre de violencia” en su artículo 51 concreta algunas de las formas de violencia obstétrica entre las que se encuentran:

-   “Obligar a la mujer a parir en posición supina y con las piernas levantadas, existiendo los medios necesarios para la realización del parto vertical”
-   La práctica del parto “por vía de cesárea existiendo condiciones para el parto natural, sin obtener el consentimiento voluntario, expreso e informado de la mujer”
-   “No atender oportuna y eficazmente las emergencias obstétricas” (situaciones que constituyen un peligro para la vida de la madre o del feto
-   “Obstaculizar el apego precoz del niño o niña con su madre sin causa médica justificada, negándole la posibilidad de cargarlo o cargarla y amamantarlo o amamantarla inmediatamente al nacer”
-   “Alterar el proceso natural del parto de bajo riesgo, mediante el uso de técnicas de aceleración, sin obtener el consentimiento voluntario, expreso e informado de la mujer”

Argentina es el otro país que ha iniciado la reglamentación para prevenir la violencia obstétrica, puesto que las cifras que se han recogido son alarmantes, según estudios publicados por el observatorio de violencia obstétrica de Argentina en los que evidencia que por lo menos el 70% de las mujeres con hijos han sido víctimas de este tipo de violencia.

Una entrevista realizada a Violeta Osorio, fundadora del Observatorio la organización mundial de la Salud publicó que solamente el 12% de los partos requieren una intervención quirúrgica, pero en Argentina por lo menos el 40% de los trabajos de parto por ese medio, lo cual estaría ligado a la falta de garantías del derecho a la salud.

Algunas organizaciones de mujeres en Colombia, como la casa de Mujeres Bachue, han señalado de manera informal, que la violencia obstétrica está ligada a las prácticas de salud y a los sistemas de salud, puesto que un solo profesional podría asistir unos 10 partos diarios sin el tiempo suficiente ni los elementos necesarios para que sea la naturaleza la que haga su trabajo, con la obligación de realizar la dolorosa episiotomía (rasgadura de la vagina) o sin obligar al bebe a transitar por el cuello uterino con los fórceps.

Testimonios de mujeres alrededor del mundo recogen los episodios cotidianos de la violencia obstétrica… “Por ser una cesárea, no dejaron que mi marido me acompañara. Ya de entrada, después de anestesiarme, me ataron con unas vendas a la camilla \`para que no intentara tocarme\` y cuando les pedí que no lo hicieran porque es algo que me pone muy nerviosa, la enfermera ni siquiera me contestó. Al rato entró el equipo de médicos charlando entre ellos y sin mediar palabra se pusieron a trabajar en mi panza: me sentí como que yo no estaba ahí y que mi hijo era algo que tenían que extirparme”.

El testimonio de Andrea Perini, recogido por el diario El Dia de Argentina, evidencia que se debe seguir trabajando en evidenciar la violencia obstétrica que muchas veces se hace cotidiana y se reproduce desde los espacios vitales tan cercanos como la propia familia.
