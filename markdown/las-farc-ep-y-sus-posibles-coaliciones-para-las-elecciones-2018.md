Title: Las FARC-EP y sus posibles coaliciones para las elecciones 2018
Date: 2017-04-30 10:44
Category: Nacional, Política
Tags: elecciones 2018, FARC-EP
Slug: las-farc-ep-y-sus-posibles-coaliciones-para-las-elecciones-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Farc-en-Yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Abr 2017] 

El próximo 7 de agosto, las FARC-EP harán su primera asamblea para aprobar y conformar los lineamientos de su partido político y elegir a los integrantes que serán postulados, tanto a Cámara como a Senado, en las próximas elecciones de 2018. De igual forma, este espacio, debatirá lo que podrían ser las posibles coaliciones con diferentes sectores, **para crear un movimiento de convergencias que le apueste a la construcción de la paz.**

**Los Movimientos políticos y sociales como Marcha Patriótica y los partidos de izquierda, serán los primeros sectores con los que las FARC-EP** entablarán diálogos sobre estas coaliciones, esto como parte de dos criterios trazados en la Décima Conferencia de las FARC-EP, el de la creación del nuevo partido político y el de la búsqueda de convergencias. Le puede interesar: ["Marcha Patriótica definirá el 15 de mayo candidatos a elecciones del 2018"](https://archivo.contagioradio.com/15-de-mayo-marcha-patriotica-definira-candidatos-a-elecciones-del-2018/)

“Hemos dicho que trabajaremos por un movimiento de convergencia donde estén los **sectores que mayor identidad política compartan con nosotros, pero también sectores que le apunte a las expansión de la democracia**” afirmó Santrich.

En esa nueva plataforma, que busca crear las FARC, **tendrían participación organizaciones populares, grupos de ciudadanos, organizaciones ambientalistas** y todas aquellas expresiones de la sociedad que no han sido recogidas por ninguna otra plataforma. Le puede interesar:["Sanciones del Consejo Electoral a la UP límitan participación política"](https://archivo.contagioradio.com/sancion-consejo-electoral/)

Sin embargo, Santrich, expresó que  descartan la posibilidad de coaliciones con otras tendencias políticas**, más de centro como Petro o Fajardo, desde que se comprometan “con un discurso que sea de beneficio para la implementación de los acuerdos** de paz y que de manera práctica las ejecute”.

Referente a la decisión de si las FARC **se lanzarán a través de lista cerrada o abierta**, Santrich señaló que “esto también se decidirá en el Congreso Interno, para algunos casos puede ser muy favorable la lista cerrada, pero para otros no, este es un detalle de mecánica política electoral que hay que analizarlo con mucho cuidado”.

<iframe id="audio_18449623" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18449623_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
