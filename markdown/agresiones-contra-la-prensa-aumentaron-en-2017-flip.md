Title: Agresiones contra la prensa aumentaron 43.5% en 2017: FLIP
Date: 2018-02-09 14:47
Category: DDHH, Nacional
Tags: agresiones a la prensa, FLIP, Fundación para la Libertad de Prensa, Libertad de expresión, Libertad de Prensa
Slug: agresiones-contra-la-prensa-aumentaron-en-2017-flip
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/violencia-prensa-e1501178542597.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [09 Feb 2018] 

En el marco de la celebración del día del periodista, la Fundación para la Libertad de Prensa (FLIP) lanzó su informe anual sobre la situación de la libertad de expresión y de prensa en el país tras los 8 años de gobierno de Juan Manuel Santos. En 2017, documentaron **310 casos de agresiones** donde resultaron heridos 368 periodistas, un aumento del 43.5% con relación al año pasado. Además, fue asesinada por la Fuerza Pública la comunicadora indígena Efigenia Vásquez.

El informe se titula “Estado depredador” y alerta que el año pasado, “los poderes ejecutivo, legislativo y judicial **faltaron a su deber de garantes** de la libertad de prensa y tomaron conscientemente acciones de censura”. Indica el informe que las restricciones a la libertad de prensa se registraron desde el presidente de la República, el Congreso y las cortes.

Específicamente se refiere a “la violencia sin protección ni sanciones, la pauta oficial que compra silencios, **la estigmatización de la prensa** como respuesta a las críticas, la violencia estatal y letal contra periodistas, las decisiones judiciales que abren la puerta a la censura y, sobretodo, aquellos extensos territorios carentes de información”.

### **Libertad de prensa en cifras** 

La alerta la enciende la Fundación en la medida en que, desde el 2006, el año que pasó fue el que evidenció **el mayor número de agresiones** contra la prensa. En materia de amenazas, registró 129 casos y las denuncias por los delitos de injuria y calumnia continúan en aumento. (Le puede interesar:["Hurtan equipos y material sensible de oficina de la Agencia Prensa Rural"](https://archivo.contagioradio.com/hurtan-equipos-y-material-sensible-de-oficina-de-la-agencia-prensa-rural/))

Además, lo que más le preocupa a la FLIP es que la Fuerza Pública continúa siendo uno de los principales actores que **ha violentado el ejercicio de la prensa** en escenarios como la protesta social. Concretamente, “durante el 2017, la FLIP registró 27 casos de agresiones contra reporteros por parte de uniformados del Ejército y la Policía, de los cuales 15 se presentaron en medio de protestas sociales”.

Otros datos adicionales, hacen referencia a por ejemplo que, en 2017, se presentaron 8 casos de obstrucción a la información, **14 casos de acoso judicial**, 31 casos de agresión física, 129 amenazas, un asesinato, 3 atentados, 3 desplazamientos, 6 casos de daños a infraestructura, 4 detenciones ilegales, 8 casos de espionaje, 29 casos de estigmatización, 16 de hostigamiento, 25 casos de obstrucción al trabajo periodístico, 6 casos de robo y un secuestro.

### **Posibles agresores contra la prensa** 

Teniendo en cuenta las diferentes agresiones, el informe detalla que “los actores ilegales como la guerrilla y los paramilitares, han dejado de ser los principales agresores contra la prensa”. Por el contrario, quienes más agreden el ejercicio periodístico son los **servidores estatales y particulares**. En 2017 hubo 33 agresiones por actores ilegales, 76 por funcionarios y 72 por particulares. (Le puede interesar:["Simulación de Saúl Cruz fue un ataque a la libertad de prensa"](https://archivo.contagioradio.com/simulacion-de-saul-cruz-viola-obligaciones-de-libertad-de-prensa-del-estado-fllip/))

Finalmente, a pesar del compromiso que dijo haber tenido el presidente Juan Manuel Santos para defender la libertad de prensa, donde reconoció las violaciones a los derechos humano de los periodistas y se comprometió a la reparación integral, los esfuerzos del Gobierno **no correspondieron con estos compromisos**. Alerta la FLIP que el mecanismo que se estableció para reparar a los periodistas, no tuvo legitimidad, fue improvisado y careció de una articulación institucional.

[Informe FLIP 2017 Estado Depredador](https://www.scribd.com/document/371162614/Informe-FLIP-2017-Estado-Depredador#from_embed "View Informe FLIP 2017 Estado Depredador on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_7986" class="scribd_iframe_embed" title="Informe FLIP 2017 Estado Depredador" src="https://www.scribd.com/embeds/371162614/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Tf2wqJ2xNZ4LhHT67avA&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.7790927021696252"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
