Title: Pedagogía para la paz debe ser con todos los sectores y desde las dos partes
Date: 2016-02-20 07:25
Category: Entrevistas, Paz
Tags: Conversaciones de paz de la habana, FARC, proceso de paz
Slug: farc-gobierno-y-pedagogia-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/farc-en-la-guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: aldia] 

<iframe src="http://www.ivoox.com/player_ek_10503172_2_1.html?data=kpWikpiVe5Ohhpywj5acaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncYamk7jSjc3Fb8LX0NfRw8nTb9LpxpDZw9iPiqLGpJDhw9LGrYa3lIqum9OPtNbZxcrbjc3Fp8bmjNXSxsaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### **Carlos Medina Gallego** 

###### [20 Feb 2016 ] 

Tras conocerse que integrantes de la delegación de paz de las FARC, participaron en un evento organizado en el corregimiento guajiro El Conejo, Carlos Medina Gallego, asegura que esta visita evidencia la madurez del proceso de conversaciones de paz y la necesidad de que **las dos partes, en igualdad de condiciones, puedan hacer pedagogía con todos los sectores de la sociedad civil colombiana**, planteando los alcances de los acuerdos pactados y sus proyecciones.

Sí la delegación del Gobierno participa en actividades pedagógicas sobre el proceso de paz con gremios, universidades y la fuerza pública, es importante que las FARC lo puedan hacer con sus bases guerrilleras, pero también con las comunidades de los territorios en los que están presentes, afirma Gallego, e insiste en que “lo desafortunado no es que las FARC estén haciendo una pedagogía de paz (…) **sino que en alguna medida se quiera que haya un solo discurso legitimo al interior de la sociedad colombiana** (…) y que este sea el discurso institucional”.

De acuerdo con el analista es evidente que **las comunidades tienen incertidumbre por lo que sigue tras la firma de los acuerdos**, “con la forma en que el Estado va a hacer presencia en los territorios impidiendo que el paramilitarismo someta o las formas de participación política de los integrantes de las FARC y la dejación de las armas (…) y que mejor que san las FARC quienes expliquen, tanto a la población civil como a sus integrantes, la realidad de los acuerdos”.

“**Hay mucha gente que no cree en el proceso y es muy importante que las voces autorizadas” que están saliendo por los medios de comunicación” unifiquen su discurso**, afirma Gallego “falta un lugar donde Gobierno y FARC le expliquen a las Fuerzas Militares, a las universidades, a los gremios y a los medios de comunicación los logros, alcances y posibilidades del proceso, porque eso llena de confianza el país y permite que el proceso se empiece a consolidar. De verdad no puede mandarse a las FARC a un encerramiento total para que únicamente hagan pedagogía con sus militantes, así no funciona la paz”, agrega.

Las FARC tuvieron armas durante su intervención porque “están en un cese unilateral al fuego y no hubiese sido prudente que esta organización acudiera como si se estuviese en un cese bilateral y como si no hubiera ningún riesgo de incidente frente a acciones de la oposición o la institucionalidad colombiana”, asegura el analista y recomienda enfatizar el carácter civilista por sobre el militar, pues **“lo que define la presencia es un  propósito pedagógico y político en términos del proceso que está andando”**.

“Hay una ofensiva de los sectores de la oposición” por descalificar el proceso de paz, afirma Gallego y concluye aseverando que **“no se puede permitir que la oposición siga erosionando y desgastando este proceso con señalamientos”** pues “la gente quiere saber de voz directa de la guerrilla cómo va el proceso, son las mismas FARC las que deben decir lo que piensan y hacer saber cuáles son sus propias angustias y socializarlas con la población, eso es lo que le da legitimidad al proceso” y un impase como el de esta semana “genera situaciones poco pertinentes al momento histórico que vive el proceso”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 

######  
