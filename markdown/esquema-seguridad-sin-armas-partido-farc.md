Title: Retiran parte del armamento a algunos escoltas del Partido FARC
Date: 2019-09-05 10:59
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: armas, esquemas, Partido FARC, seguridad, Unidad de Protección
Slug: esquema-seguridad-sin-armas-partido-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Por medio de un comunicado la Dirección General de la Unidad de Protección **(UNP)**, solicitó  el reintegro a las bodegas del armamento tipo subametralladora a algunos de los esquemas de seguridad designados a integrantes del **Partido FARC**.

La medida generó respuestas por parte de los miembros  del Partido FARC que son protegidos por estos esquemas, entre las preocupaciones está la inseguridad que les generaba tener escoltas  que no puedan proteger o reaccionar con arma de fuego  en caso de presenciar un ataque.

> [\#Atención](https://twitter.com/hashtag/Atenci%C3%B3n?src=hash&ref_src=twsrc%5Etfw)
>
> A la vez que aumentan las amenazas alertamos sobre esta medida inexplicable del Gobierno, en la cual se le quitan elementos de protección a los esquemas que se encargan de la seguridad de nuestro partido. <https://t.co/IXwyuG2mdD>
>
> — FARC (@PartidoFARC) [September 5, 2019](https://twitter.com/PartidoFARC/status/1169404887962193920?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La orden fue dada por **Pablo González, Director de la Unidad**, a través de un comunicado, donde señala irregularidades  en los protocolos de los funcionarios de seguridad, entre ellos no regresar el armamento a las bodegas cuando se dan por terminadas sus jornadas de trabajo, o cuando solicitan vacaciones, según el comunicado estas fueron las razones para la determinación

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
