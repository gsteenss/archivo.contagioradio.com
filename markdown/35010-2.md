Title: Habitantes de Cabrera decidirán sobre micro-hidroeléctricas en Sumapaz
Date: 2017-01-21 15:25
Category: Ambiente, Nacional
Tags: Cabrera, consulta popular, Hidroeléctricas, Sumapaz
Slug: 35010-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  revistaccesos 

###### [21 Ene 2017] 

Los habitantes del municipio de Cabrera en Cundinamarca, podrán decidir si quieren o no que la compañía EMGESA construya **14 microcentrales hidroeléctricas en el páramo de Sumapaz,** luego de que la Registraduría Nacional aprobara la realización de una consulta popular el próximo domingo 26 de febrero de 2017.

La decisión, se toma gracias a la capacidad organizativa de la comunidad y específicamente a la gestión de **la Zona de Reserva Campesina (ZRC)** teniendo en cuenta que la comunidad teme por los impactos negativos que puede acarrear la construcción de las hidroeléctricas al páramo de Sumapaz.

En foros y encuentros diferentes sectores sociales y sus asociaciones de productores, de bienes y servicios, mujeres, jóvenes, Juntas de Acción Comunal, sindicatos, colectivos académicos han expresado su rotundo rechazo al proyecto hidroeléctrico “El Paso” de la empresa multinacional Emgesa-Enel.

### **¿En qué consiste  ‘el Paso’?** 

Se trata de la construcción de una **cadena de microcentrales alrededor de la cuenca del rio Sumapaz, que intervendría  32 veredas de Cabrera**. La comunidad denuncia que, contrario a ser un proyecto amigable con el ambiente como lo sostiene la compañía, **los municipios serían atravesados por líneas de alta tensión, cuya radiación podrían afectar los cultivos**.

Desde el Comité de impulso de la Zona de Reserva Campesina de Cabrera, **se denuncia que la empresa ha realizado un estudio ambiental “amañado”, en el que “hay muchas ausencias y mentiras”,** pues aunque no se trata de un megaproyecto como 'El Quimbo', de todas formas se ocasionarían alteraciones en los hábitats terrestres y acuáticos en diferentes tramos del río Sumapaz.

Otra de las preocupaciones, se debe a posible intervención en una zona de amortiguamiento del parque natural del Sumapaz, para la cual se estarían usando cerca de 11.5 m^3^ de agua por segundo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
