Title: Autoridad afrocolombiana denuncia negación de consulta previa sobre plan de desarrollo
Date: 2015-02-06 21:31
Author: CtgAdm
Category: Comunidad, DDHH, Movilización
Tags: ANAFRO, Autoridad nacional afrocolombiana, Proyecto de ley nacional de desarrollo 2014
Slug: autoridad-afrocolombiana-denuncia-negacion-de-consulta-previa-sobre-plan-de-desarrollo
Status: published

###### **Foto:Ebanolatinoamerica.com** 

<iframe src="http://www.ivoox.com/player_ek_4049798_2_1.html?data=lZWhm5ydfI6ZmKiakpuJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FVx9fcjcnJstbixM7OjdPJq8LXyoqwlYqmd8%2BfxcqYxdTSt9bg1caY0tfJusrVjNjcxNfJb9HgwtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista con Yolanda García, representante de ANAFRO:] 

En rueda de prensa , ANAFRO, Autoridad Nacional Afrocolombiana denunció la **negación de una consulta previa por parte del gobierno de Santos**, con las comunidades y organizaciones sociales negras para poder consensuar las reivindicaciones de cara a incluirlas en el plan de desarrollo de 2014-2018.

Esta semana el gobierno presenta el plan de desarrollo (PND 2014-2018), pero actualmente mas del **85% de las propuestas del anterior plan aún no han sido realizadas**, tampoco ha sido consultado el pueblo negro ni invitado a la formulación de dicho proyecto.

Por eso desde el Pueblo Negro, Afrocolombiano, Palenquero y Raizal se exige al presidente Santos a paralizar el plan, a reformularlo con las propuestas de las organizaciones sociales afrocolombianas y a realizar una consulta previa en las comunidades para así ratificarlo.
