Title: Países de América Latina alistan protección legal para defensores ambientales
Date: 2018-03-02 18:08
Category: DDHH, Nacional
Tags: América Latina, defensores del ambiente, ECLAC, tratado ambiente
Slug: paises-de-america-latina-alistan-medida-de-proteccion-legal-para-defensores-del-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/defensores-del-ambiente-e1520023953930.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Polémos/Wayka.pe] 

###### [02 Mar 2018] 

De acuerdo con el periódico The Guardian, tras dos años de negociación, países de América Latina habrían llegado a un acuerdo para establecer la primera protección legal vinculante de la región para los defensores del ambiente en el marco de la novena reunión del Comité de Negociación del Acuerdo Regional del Principio 10. Esto teniendo en cuenta el aumento en los asesinatos “que alcanzaron números record” el año pasado donde fueron asesinados **dos defensores cada semana**.

El medio de comunicación indicó que esta protección legal se derivaría de un tratado de **democracia ambiental** producto del acuerdo para el acceso a información ambiental, de participación y justicia que hace parte de la Declaración de Ambiente y Desarrollo de Río de Janeiro que se realiza en Costa Rica entre el 28 de febrero hasta el 4 de marzo.

### **El instrumento legal sería la medida más útil para proteger a los defensores del ambiente** 

Diplomáticos de países como Chile le aseguraron a The Guardian que un instrumento legal es la medida **más útil para asegurar la protección de los derechos humanos** teniendo en cuenta que no es una solución completa “pero si es un paso importante para reducir los conflictos socio ambientales en la región”.

Además, el medio enfatizó en que se espera que varios países firmen el tratado, pero no entrará en vigencia hasta que **sea formalmente ratificado** por los ocho miembros de la Comisión Económica para América Latina y el Caribe (ECLAC) que hace parte de las Naciones Unidas. Esta Comisión deberá revisar y monitorear el mecanismo en los países para ver el progreso a las normas que protegen los derechos humanos, pero “la aplicación se debe hacer a nivel nacional”. (Le puede interesar: ["Colombia es el segundo país más peligroso para defensores del ambiente"](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/))

### **Estados deben crear medidas que prevengan la violencia contra los defensores** 

La negociación de este tratado empezó en 2016 y según una fuente de las Naciones Unidas del periódico británico, “es muy probable que la reunión termine con la adopción del **primer acuerdo vinculante**”. Esto obligaría a sus firmantes a tomar las medidas necesarias y más adecuadas para proteger y promover los derechos a la vida, libre movimiento, libre expresión y organización de los defensores del ambiente.

Los Estados también deben propender por tomar medidas efectivas que prevengan las **amenazas o ataques contra los defensores del ambiente** y debe investigar y castigar a los responsables cuando estas conductas ocurran. Establece el reporte que “algunos actores regionales han sido acusados de complicidad en los asesinatos de activistas como Berta Cáceres” quien fue asesinada en 2015 en Honduras.

Finalmente, países como México se habrían opuesto a la medida y **Colombia y Brasil, dos de los países que más peligrosos** para los defensores del ambiente han mantenido una postura “cerrada”. Cabe resaltar que alrededor del mundo y tan sólo en 2017 fueron asesinados 197 defensores de los derechos del ambiente y se estima que el 60% de estos hechos ocurrieron en América del Sur.

###### Reciba toda la información de Contagio Radio en [[su correo]
