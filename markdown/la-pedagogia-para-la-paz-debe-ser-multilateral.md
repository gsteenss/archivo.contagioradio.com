Title: Pedagogía para la paz, una tarea multilateral
Date: 2016-03-14 17:25
Category: Entrevistas, Paz
Tags: carlos medina gallego, dialogos de paz, Mesa de conversaciones de paz
Slug: la-pedagogia-para-la-paz-debe-ser-multilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Diálogos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal ] 

###### [14 Mar 2016 ]

<iframe src="http://co.ivoox.com/es/player_ek_10800347_2_1.html?data=kpWlkpWXeJihhpywj5aXaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca3VjNXSxsbLs8iZpJiSo6nFb9HV08aYzsaPtMLujMrgjdrSpYzowtfSw5DRuc3oytHO1srWpc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [**Carlos Medina Gallego**]

Recientes declaraciones de las delegaciones de paz confirman que el 23 de marzo no se dará la firma del acuerdo final y según afirma el analista Carlos Medina Gallego, lo más probable es que ese día haya una reunión y se establezcan algunos criterios que podrían ser comunicados, teniendo en cuenta que **aún no está cerrada la discusión sobre los puntos de terminación conflicto y mecanismos de refrendación y verificación**.

De acuerdo con Medina, aún no se puede hablar de pedagogía de paz cuando los medios de comunicación, opositores y Gobierno califican negativamente las acciones pedagógicas de las FARC-EP con sus frentes en distintas regiones, se trata más bien de una "pedagogía de la confrontación", que debe ser transformada en **acciones pedagógicas conjuntas a través de las que las dos delegaciones puedan despejar dudas frente al proceso** con sectores como la fuerza pública, los empresarios o las organizaciones sociales, "eso envía un mensaje más sólido que el descalificar", agrega el analista.

"El proceso está andando en un periodo de decisiones fundamentales y esto demanda **ser muy cuidadosos en relación con la información que se envía a la sociedad colombiana**", afirma Medina, insistiendo en que lo importante no es aligerar la firma del acuerdo sino dejar que las delegaciones cuenten con el tiempo necesario para que éste "salga bien"

El analista llama la atención sobre la **necesidad de un cese multilateral y definitivo al fuego, teniendo en cuenta la existencia de otros actores armados** como la guerrilla del ELN que ha aumentado sus ofensivas y las estructuras paramilitares que en los últimos días han vuelto a asesinar líderes sociales, "hay otros actores y la conflictividad no se acaba en términos militares con que las FARC-EP y el Gobierno lleguen a un cese bilateral, sino que es necesario incorporar a los otros actores por eso es urgente llamar al ELN y al Gobierno a sentarse en una mesa de conversaciones para que la paz sea global", asevera Medina.

Según afirma Medina este cese multilateral posibilitará a la fuerza pública dedicarse a combatir el paramilitarismo actual, "un paramilitarismo articulado a la administración política, económica y social de los territorios", que funciona como **una red compleja y con el objetivo de someter y explotar económicamente, acumulando capital por la vía criminal**, aliándose con el narcotráfico, la extorsión y las administraciones territoriales.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
