Title: Gobierno no ha aceptado el 51% de las solicitudes de restitución de tierras: ONU
Date: 2016-03-23 15:07
Category: DDHH, Nacional
Tags: Alto Comisionado de la ONU, Derechos Humanos, informe onu
Slug: gobierno-no-ha-aceptado-el-51-de-las-solicitudes-de-restitucion-de-tierras-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Restitución-de-tierras-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

###### [23 Mar 2016] 

El Alto Comisionado de las Naciones Unidas publicó su informe anual sobre el estado de los derechos humanos en Colombia, develando la **precaria situación en la que aún se encuentran los defensores de derechos humanos** y el aumento de las solicitudes de las víctimas por despojo de tierras.

El informe señala que **durante 2015 se reportaron 179 hechos relacionados con violencia política**, de los que 124 fueron amenazas, 29 atentados, 20 asesinatos, 4 secuestros y 2 desapariciones, todos ellos dirigidos a diferentes activistas y miembros de organizaciones sociales. Así mismo se registraron 295 ataques contra 885 defensores, de los cuales 63 terminaron en asesinatos y sólo 41 casos fueron verificados.

En relación con la situación de las víctimas de despojo de tierras, el Alto Comisionado indicó que la Unidad de Restitución de Tierras reportó **87.119 solicitudes de reubicación**, 49% de las cuales fueron respondidas en zonas microfocalizadas destinadas por el Ministerio de Defensa frente al **51% que esperan ser tramitadas**. Así mismo, el informe refiere que las principales dificultades para los reclamantes de tierras son la no adecuada atención por parte de las instituciones y la desarticulación que existe entre ellas a la hora de responder las solicitudes.

El informe también precisa que incrementaron las agresiones dirigidas a integrantes de la comunidad LGTBI y mujeres, durante 2015 se conocieron **39 panfletos amenazantes en contra de 211 mujeres líderes** o pertenecientes a organizaciones sociales y fueron asesinados 18 líderes LGTBI. Se estima que la impunidad en casos de violencia sexual en contra de estos grupos en el marco del conflicto armado es del 100%.

El Alto Comisionado de la ONU concluye el informe llamando la atención sobre la ley [[ZIDRES (Zonas de Interés de Desarrollo Rural)](https://archivo.contagioradio.com/campesinos-de-casanare-defienden-el-agua-y-dicen-no-a-ley-zidres/)]debido a la **posible concentración de tierras** que podría generar, e insta al gobierno a fortalecer el Sistema Integral de Verdad, Justicia y Reparación, en términos de aumentar el presupuesto y los recursos materiales y humanos, para cumplir a cabalidad las directrices del sistema.

De igual forma Todd Howland, representante en Colombia del Alto Comisionado de las Naciones Unidas, **animó a la guerrilla del ELN y al gobierno Nacional a avanzar hacia el inicio de las negociaciones formales** y reconoció la importancia del proceso de paz en La Habana para el goce pleno de los derechos humanos por parte de la ciudadanía.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
