Title: Emergencia en Meta por ataques a la población civil desde helicópteros de la fuerza pública
Date: 2020-02-22 12:55
Author: AdminContagio
Category: Comunidad, Nacional
Tags: ataques, campesinos, violencia
Slug: emergencia-en-meta-por-ataques-a-la-poblacion-desde-helicopteros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Archivo de Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 22 de febrero miembros de los Parque Cordillera Picachos jurisdicción del municipio de la Macarena, departamento del Meta, reportan sobrevuelo de helicópteros del Ejército y la Polícia cuya acción ha causado daño a varios habitantes de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los ataques se dieron mientras 300 campesinos y campesinas habitantes de Picachos y Tinigua en la vereda Caño San Juan y el Tapir, se reunían para hablar sobre los hechos de violencia sucedidos el día anterior en la vereda El Rubí, cuando uniformados del Ejército ingresaron para desplazar a la comunidad de su territorio.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Mientras se realizaba esta reunión helicópteros del Ejército atacaron con gases lacrimógenos, la comunidad también escuchó sonidos de disparos en el caserío de la vereda El Tapir".
>
> <cite>Información de la comunidad</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Afirmaron también,*"son 6 helicópteros que los están atacando a bala, están disparando desde el centro y ya hay varios heridos"*, ante la comunidad hace un llamado a la ciudadanía, organizaciones sociales, ONG´s, e instituciones comprometidas con el bienestar de las comunidades *"a difundir esta información y alertar y hacer seguimiento en caso de que la violación de Derechos Humanos continúa en la zona en la zona"* (Le puede interesar:<https://archivo.contagioradio.com/segundo-atentado-en-menos-de-un-mes-contra-directivos-de-la-uso-en-meta/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma **solicitan atención medica urgente**, puesto que el accionar de las aeronaves ha dejado a varios civiles heridos, hasta el momento se desconoce el número exacto de lesionados, *"necesitamos enfermeros o personal médico que venga a auxiliar y prestar su servicio de primeros auxilios"* . (Le puede interesar: <https://archivo.contagioradio.com/en-colombia-los-ninos-crecen-hablando-de-muerte-oxfam/>)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Reiniciarco/status/1231244367446335488","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Reiniciarco/status/1231244367446335488

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Finalmente señalaron que en el lugar se encuentran miembros de la junta de acción comunal en su mayoría de las veredas y zona directivas de las organizaciones ASCAL-G y 2 representantes de CEALDES. (Le puede interesar: <https://www.justiciaypazcolombia.com/el-negacionismo-no-es-una-politica-aceptable-hoy/>)

<!-- /wp:paragraph -->
