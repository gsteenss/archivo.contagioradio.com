Title: Mujeres de la India son señaladas como impuras cuando tienen la menstruación
Date: 2015-11-23 17:21
Category: El mundo, Mujer
Tags: autoridades del Templo Sabrimala, descriminación de mujeres en la india, India, Mujeres en la india, Prayar Gopalakrishnan presidente de la Oficina de Propiedades Divinas del templo de Sabrimala
Slug: mujeres-de-la-india-se-movilizan-por-ser-declaradas-impuras-cuando-tienen-la-menstruacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/la-india.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ipsnoticiasnet 

###### [23 nov 2015]

A través de redes sociales, mujeres de la India protestaron ante la consideración de las autoridades del Templo Sabrimala de sólo permitir su ingresar al Recinto sagrado hindú cuando exista una máquina que detecte en qué momento de su ciclo menstrual se encuentran, teniendo en cuenta que en ese país la mujer que se encuentra con la menstruación es declarada impura.

Prayar Gopalakrishnan, presidente de la Oficina de Propiedades Divinas del templo de Sabrimala,  aseguró en rueda de prensa  que **“Llegará un día en el que se inventará una máquina para escanear si es ‘el momento adecuado, para que una mujer entre al templo. Solo entonces hablaremos si permitirles la entrada”**.

Ante las palabras del dignatario  las mujeres se manifestaron calificando tales comentarios como **“misóginos y arcaicos”** reclamando la **“naturalidad”** de la menstruación, con fotos de sus compresas y algunas con mensajes.

Las mujeres de la India durante su periodo menstrual se convierten en víctimas, se les prohíbe manipular alimentos y son obligadas a dormir en camas separadas de su familia o pareja.

En la India, **335 millones de niñas y mujeres tienen la menstruación cada mes, pero sólo un 12 % tiene acceso a compresas o paños higiénicos y 200 millones carecen de información adecuada sobre la higiene durante ese momento.**
