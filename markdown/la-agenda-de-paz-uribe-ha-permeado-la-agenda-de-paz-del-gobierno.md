Title: “La agenda de paz Uribe, ha permeado la agenda de paz del gobierno”
Date: 2015-04-27 18:01
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Carlos Medina, cese al fuego unilateral FARC-EP, Conversaciones de paz de la habana, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: la-agenda-de-paz-uribe-ha-permeado-la-agenda-de-paz-del-gobierno
Status: published

###### Foto: eluniversal.com 

Este martes 28 de Abril se inicia el ciclo 36 de conversaciones de paz entre las FARC y el gobierno nacional, sin embargo, el desarrollo de esos diálogos se ha visto afectado, a pesar de los criterios de la agenda, por los hechos de la guerra que se continúan presentando y que solo se evitarían con un cese bilateral de fuego, según lo señala el Frente Amplio por la Paz y diversos analistas.

Carlos Medina Gallego, integrante del Centro de Pensamiento de la Universidad Nacional, explica que las recientes declaraciones del senador Álvaro Uribe en torno a que apoyaría el proceso de paz por los giros del gobierno, corresponden a una situación política complicada por parte de sectores de la derecha colombiana, además requieren una especie de legitimidad que han ido perdiendo dadas sus posturas políticas y los reiterados escándalos de corrupción y nexos con la criminalidad.

Sin embargo, mucho de este cambio de postura tiene que ver con decisiones que se han reversado y que tienen que ver con el des escalamiento del conflicto, se suspendieron los bombardeos, pero se reactivaron con los hechos sucedidos en el departamento del Cauca, es muestra de que los sectores que se oponen al proceso de conversaciones de paz están logrando permear la propuesta de conversaciones de paz y puede seguir permeándolo en la medida en que avanza la discusión sobre la justicia transicional en la que se pretende centrar el debate en si hay o no penas privativas de la libertad o extradición.

Medina Gallego afirma que en esa discusión la mesa de conversaciones cambiaría de la formula Desarme-Desmovilización-Reinserción, por el Dejación-No repetición- participación política, puesto que el hecho de ir a la cárcel inhabilita a los integrantes de las FARC para hacer participación política.

Por su parte el gobierno nacional va cediendo campo a los opositores puesto que para ellos es mucho más fácil continuar con el proceso sin la oposición del Centro Democrático o ACORE “esa mesa paralela va a acercar al gobierno a los opositores pero va a tensionar las relaciones con la delegación de las FARC”

Este ciclo que se va abordar puede tender retomar la agenda y a tratar de avanzar en el desarrollo de la misma, sobre los dos temas que están conversando, víctimas y terminación del conflicto, afirma Medina.
