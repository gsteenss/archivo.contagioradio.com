Title: No es necesario pagar testigos contra Uribe "hay decenas" Iván Cepeda
Date: 2015-10-20 13:14
Category: Entrevistas, Política
Tags: Debate control político Álvaro Uribe, Derechos Humanos, Iván Cepeda, Pliego de cargos contra iván Cepeda, Polo Democrático Alternativo, Radio derechos Humanos
Slug: no-es-necesario-pagar-testigos-contra-uribe-hay-decenas-de-testimonio-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/El-pais.com_.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Elpais 

<iframe src="http://www.ivoox.com/player_ek_9905557_2_1.html?data=mp6dl5qZe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRktCfxtiY0MrHqdTV087cjdXFq8LmjNnS1dnNq9DnjMjc0NnWpYzJ087Px5CJdpPcwt6YxsrHqc%2FVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ivan Cepeda, Polo Democrático] 

###### [20 oct 2015]

Frente al pliego de cargos que la Procuraduría compulsó contra Iván Cepeda, por supuestas prebendas a dos testigos contra el senador Álvaro Uribe , el  senador del Polo Democrático **desmiente e indica que este es una nueva acción de la Procuraduría por “eliminar la oposición”** y que se ha dado desde que el “se atrevió a hacer el debate político contra Uribe”

Respecto del proceso ante la procuraduria el Senador afirma que está seguro de que se va a intentar una destitución "vamos a ver si lo logra" y agrega que está seguro de que la senadora Piedad Córdoba va a recuperar sus derechos políticos y todas las demás personas que han sido condenadas por esa instancia.

Cepeda indica que los testimonios que dieron los paramilitares **Pablo Hernán Sierra y Juán Monsalve,** fueron “debidamente judicializados” y en ningún momento se ofrecieron “dineros, ni prebendas, ni dádivas para que estas personas den su testimonio contra Uribe”, por tanto es "una acusación espúrea".

El senador del Polo afirma que “Uribe ha enviado a un detective privado” a “inducir una especie de farsa” con los testigos paramilitares para que dieran las versiones que presentó la Procuraduría en la tarde de ayer. Cepeda indica que quien recolectó las pruebas presentadas contra él, fue un “ex agente del DAS”.

También  afirma que esta acusación es “irrisoria” ya que no existe necesidad de entrar en ilegalidades debido a que al **senador Álvaro Uribe distintos ex-miembros paramilitares le acusan por distintos nexos con masacres y grupos paramilitares**, caso que se demuestra en las dos últimas semanas con las acusaciones por la masacre del Aro y otra por la operación Orión, en la comuna 13 de Medellín.

Cepeda afirma que esta es una clara “**Construcción de una acusación falsa para que un funcionario que no tiene ningún tipo de imparcialidad actúe”**, haciendo referencia al Procurador Ordoñez. El senador indica que está seguro de que el procurador “actuará de manera arbitraria, unilateral, desconociendo el derecho a la defensa y al libre proceso” buscando quitarle la investidura como lo hizo con Piedad Córdoba, Gustavo Petro y Guillermo Asprilla.

Por último Cepeda indica que en una eventual pérdida de facultades políticas por este proceso no solo **se están violando sus derechos sino “los de mis electores y electoras”**, por esto la ciudadanía puede pronunciarse de manera jurídica.

Recordemos que el debate de control político fue realizado en el mes de septiembre de 2014 y presentó serios inconvenientes para su ejecución como que Cepeda fue acusado ante la comisión ética del Senado por este debate, s**e le prohibió mencionar a Uribe** y además el senador del Centro democrático se levantó en medio del debate.
