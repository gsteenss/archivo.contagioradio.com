Title: "No estoy tan convencido de que el ELN vaya a 'incorporarse' a las negociaciones con las FARC así como así"
Date: 2015-05-12 17:25
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Alejo Vargas, Carlos Lozano, dialogos, ELN, FARC, Gabino, Humberto de la Calle, paz, Santos, timohenko
Slug: no-estoy-tan-convencido-de-que-el-eln-vaya-a-incorporarse-a-las-negociaciones-con-las-farc-asi-como-asi-carlos-lozano
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4485409_2_1.html?data=lZmll5mUfY6ZmKiak5WJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmzdTgjbHTvsLi0NiY1dTGtsaf08ri0M6Jh5SZo5jbjbnNsdDXycrbzdSPcYy7wsfW0NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Lozano, Semanario Voz] 

###### [12 may 2015] 

La noche del lunes 11 de mayo, el presidente Juan Manuel Santos confirmó ante medios de información, que el gobierno permitió y facilitó una reunión en Cuba entre Timochenko y Gabino, comandantes de las FARC y el ELN respectivamente, como paso necesario en los proceso de paz que se adelantan entre las insurgencias y el gobierno.

Al paso le salió el jefe del equipo negociador del gobierno en La Habana, Humberto de la Calle, quien afirmó la mañana del martes 12 de mayo, que "la incorporación del ELN al proceso, corresponde al interés de la sociedad colombiana". Palabras mayores utilizadas por el representante del gobierno en La Habana, pues si bien, desde diferentes sectores, incluidas las guerrillas, se ha hecho énfasis en la necesidad de que la paz tenga en cuenta a todas las insurgencias, ninguna de estas ha hablado -a la fecha- de sumarse, adherirse o "incorporarse" a lo adelantado en la mesa de Diálogos de Paz de La Habana.

Carlos Lozano, director del Semanario Voz y vocero de Marcha Patriótica, asegura no estar "tan convencido de que el ELN vaya a incorporarse a las negociaciones con las FARC así como así". Indica que si bien la paz es integral, y es necesario que el proceso se haga con todas las insurgencias en relación uno con otro, es difícil que una guerrilla se adhiera al proceso adelantado por otra.

"El ELN no va a aparecer como un agregado a la mesa de La Habana. Es una equivocación que el gobierno lo piense así -asegura el periodista-. Esto es un conflicto que tiene el mismo origen, las mismas causas, pero sus protagonistas tienen particularidades. Las guerrillas tienen connotaciones distintas, por eso no son una sola, y el gobierno tiene que entenderlo".

Se podría considerar, por otra parte, que las guerrillas se reunieran en un solo proceso para la discusión de los puntos que se encuentran en el llamado "congelador", que como saben las y los colombianos, son los puntos álgidos de los puntos frente a los cuales no han podido llegar a acuerdos. "Si hay una incorporación, seguramente lo harán sobre una base de acuerdos y compromisos", se anima a señalar Lozano. Pero habría que "esperar un pronunciamiento por parte de las dos organizaciones insurgentes que aclare el alcance de la reunión y los acuerdos de la misma".

Sin embargo, lo cierto para Lozano es que los dos procesos van a tener un punto de encuentro, pues no pueden llevarse a cabo dos procesos de refrendación diferentes, ni dos Asambleas Nacionales Constituyentes, si ese fuese el caso.

En este sentido, la reunión se habría hecho en función de la paz, no solamente en lo que tiene que ver con el desarrollo de la mesa de La Habana, sino en lo que tiene que ver con las perspectivas reales para que pronto inicien los diálogos con el ELN

"No sé si es esta fue la primera reunión entre Timochenko y Gabino, pero es seguro que no va a ser la ultima. Estas reuniones tendrán que seguirse dando para que no se alejen las posibilidades de ese encuentro de los dos procesos. Eso es indispensable -Afirma Carlos Lozano-. El punto de encuentro entre el proceso de las FARC y el ELN será inexorable".

<iframe src="http://www.ivoox.com/player_ek_4485439_2_1.html?data=lZmll5mXfY6ZmKiakpqJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zy9SYuMbWq8LnjNjcxNfJb9PZ1tPW0dOPmMrh0MjVx9PPs467wsfW0NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejo Vargas, Profesor universitario] 

Por su parte, para el profesor y analista Alejo Vargas, "hay cada vez mas voces en el sentido de realizar la Asamblea Nacional Constituyente". Al comunicado de las FARC con el que finalizaron el ciclo 36 de conversaciones en el que se que se insiste en la necesidad de la ANC, se sumó esta semana el pronunciamiento del Fiscal General, y las columnas de opinión del profesor Francisco Barbosa de la Universidad Externado, y de Gustavo Álvarez Gardeazabal. "Es algo que se puede seguir debatiendo, es una posibilidad que no se puede descartar", concluye el profesor Vargas.
