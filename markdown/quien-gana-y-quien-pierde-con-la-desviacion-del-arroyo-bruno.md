Title: ¿Quién gana y quién pierde con la desviación del Arroyo Bruno?
Date: 2016-04-27 14:42
Category: Ambiente, Nacional
Tags: ANLA, arroyo Bruno, La Guajira
Slug: quien-gana-y-quien-pierde-con-la-desviacion-del-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario del Norte 

###### [27 Abr 2016] 

**¿Quién gana y quién pierde con la desviación del Arroyo Bruno?**, es la pregunta que se responderá durante el foro que se realizará en la Universidad Nacional este miércoles desde las 5 de la tarde en el auditorio Margarita González. (Transmisión en Vivo desde las 17:00 hora de Colombia).

https://www.youtube.com/watch?v=l4EXKQudJcU

Corpoguajira, representantes de las comunidades Wayúu y afrodescendientes, junto otras organizaciones sociales y ambientales, participarán en el foro, en el que también se espera que se cuente con la presencia de un represente de la empresa el Cerrejón, que en los últimos días ha estado en el ojo público, por sus planes con [el arroyo Bruno, una de las principales afluente principal del Río Ranchería](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)y fuente de agua de la que dependen comunidades Wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao.

“**Inmediatamente nosotros diríamos, pierden las comunidades por un modelo minero de más de 30 años que ha despojado los derechos fundamentales que no ha podido garantizar el Estado”**, dice Danilo Urrea, integrante de[Censat Agua Viva.](https://archivo.contagioradio.com/la-desviacion-del-arroyo-bruno-llamemos-las-cosas-por-su-nombre/)

De acuerdo con Urrea, hace unos años cuando [El Cerrejón p](https://archivo.contagioradio.com/mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua/)idió los permisos a Corpoguajira para intervenir la zona del Arroyo Bruno, esa autoridad ambiental no dio los permisos porque se según los propios planes de manejo ambiental de esa entidad, resultaba obvio las graves afectaciones ambientales que podrían existir teniendo en cuenta la importancia del arroyo Bruno, de manera que cuando  Corpoguajira negó los permisos, lo traslado a la Agencia Nacional de Licencias Ambientales en Bogotá, “**lo que deja ver a que hay una situación de lobby político más que un análisis ambiental, frente a asuntos como el principio de precaución”,** expresa el ambientalista, quien agrega que este será uno de los temas que se subrayarán durante el debate.

Las comunidades indígenas denuncian que [las consecuencias del desvío podrían ser nefastas](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/). Primero porque el arroyo podría secarse totalmente, profundizando aún más la falta de agua en La Guajira, a su vez  se generaría afectaciones frente a la espiritualidad de las comunidades indígenas que mantienen un vínculo muy importante con el arroyo que para ellos es sagrado y además comunidades como Alvania, algunas de Rioacha y Maicao, entre otras se verían gravemente afectadas por la falta de agua.

Por esas razones en rechazo a la [desviación del Arroyo Bruno,](https://archivo.contagioradio.com/empresa-cerrejon-desviara-una-de-las-principales-fuentes-de-agua-de-la-guajira/) **El Cerrejón se ha visto obligado a llevar a su junta directiva la posibilidad de que no se intervenga el cauce** del arroyo Bruno hasta que no se aclare completamente el tema.

El debate se platea como un espacio donde se puedan escuchar diversas voces y posiciones, “conscientes de la necesidad de establecer un diálogo democrático, y contribuir al debate nacional frente a la expansión del modelo extractivo minero de carbón a cielo abierto” asegura Censat Agua Viva.

[![foro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/foro.jpg){.aligncenter .size-full .wp-image-23252 width="600" height="359"}](https://archivo.contagioradio.com/quien-gana-y-quien-pierde-con-la-desviacion-del-arroyo-bruno/foro-2/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
