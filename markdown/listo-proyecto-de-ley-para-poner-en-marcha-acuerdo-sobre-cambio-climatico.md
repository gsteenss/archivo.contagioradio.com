Title: Ningún lugar de Colombia se salva de los impactos del cambio climático
Date: 2017-06-16 20:16
Category: Ambiente, Nacional
Tags: Acuerdo de París, cambio climatico
Slug: listo-proyecto-de-ley-para-poner-en-marcha-acuerdo-sobre-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa-2-e1491162849872.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Comisión J y P 

###### [16 Jun 2017] 

Por una unanimidad la Plenaria de Cámara de representantes aprobó el Proyecto de Ley referente al Acuerdo Global sobre cambio climático, con el que Colombia se compromete a **reducir en un 20% las emisiones de Gases de Efecto Invernadero, según se acordó en el marco de la COP 21  de París.**

Ya que el proyecto de Ley pasó todos los debates, queda que el presidente Juan Manuel Santos firme la Ley, que también busca** avanzar hacia la adaptación frente a los efectos del aumento del calentamiento global, y proyectar el modelo económico** entorno a actividades que signifiquen pocas emisiones.

"Esperamos que el Estado colombiano, **al ratificar este acuerdo  se comprometa de manera eficaz a cambiar políticas como la locomotora mineroenergética que causa tanto daño a los ecosistemas** del país. Asimismo que no se vuelva a las fumigaciones aéreas", expresó el representante Alirio Uribe, quien añade también que se espera que que tratado se cumpla y que se desarrollen las políticas públicas necesarias, recordando que el Ministerio de Ambiente tiene la centésima parte del Ministerio de Defensa. "Eso tiene que cambiar", afirma.

### Vulnerabilidad de Colombia 

**El 100% de los municipios del país tienen algún grado de riesgo frente al cambio climático.** Eso es lo que concluye un estudio del IDEAM y el PNUD, divulgado esta semana justo en momentos de discusión del proyecto de Ley en el Congreso.

El “Análisis de vulnerabilidad y riesgo por Cambio Climático para los municipios de Colombia”, señala que **el mayor riesgo será para el año 2040 y afectará principalmente a San Andrés y Providencia, Vaupés y Amazonas.** Asimismo indica que Bogotá, es la segunda capital con mayor riesgo por cambio climático.

El IDEAM explica que “El estudio facilitará la correcta toma de decisiones para orientar la adaptación a la problemática, ya que se basa en el análisis detallado del departamento o el municipio, usando las matrices de indicadores y no solamente los mapas de promedios, como insumo fundamental para la formulación de los POT Modernos y la actualización de planes de ordenamiento municipal”.

Se trata de un estudio que duró en su desarrollo más de 3 años y que es el primero en realizarse con información detallada por departamentos y municipios. El **IDEAM, asegura que este análisis se considera como el más completo a la fecha**, y que además se realizó bajo estándares del Panel Intergubernamental de Cambio Climático.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
