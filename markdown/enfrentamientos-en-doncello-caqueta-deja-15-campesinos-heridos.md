Title: Enfrentamientos en Doncello, Caquetá dejan 15 campesinos heridos
Date: 2016-09-13 13:46
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Ambiente, Caquetá, Comunidad, Petroleras, resistencia
Slug: enfrentamientos-en-doncello-caqueta-deja-15-campesinos-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Valparaiso-e1473791807911.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blog El Río 

###### [13 Sep 2016] 

En medio de las acciones de resistencia de la comunidad del municipio de Doncello, en el departamento de Caquetá, que buscan impedir el ingreso de las empresas petroleras a su territorio, los enfrentamientos entre los manifestantes y la fuerza pública ya dejan **15 campesinos heridos y otros 2 integrantes del ESMAD lesionados.**

Jhon Fredy Criollo, gerente de Empresas Públicas de Doncello, cuenta que cerca **6 mil personas se encontraban en la vía Río Negro realizando un plantón pacífico** a la entrada de dos de las veredas donde hace falta realizar la sísmica, con el fin de impedir el paso de la empresa Petroseismic. Luego llegó el ESMAD y tan pronto se encontró con el plantón empezó a lanzar bombas aturdidoras y gases lacrimógenos, lo que promovió el enfrentamiento con los campesinos que respondieron a la fuerza pública con palos y piedras. Según relata el líder social, algunos gases cayeron cerca de un **ancianato que tuvo que ser desalojado varias veces.**

En total son aproximadamente 30 mil habitantes de Doncello los que le [solicitan al gobierno que detenga la actividad petrolera en su departamento](https://archivo.contagioradio.com/500-campesinos-se-movilizan-para-rechazar-operaciones-de-emerald-energy-en-caqueta/), pues denuncian que el año **2013** también se hicieron perforaciones sobre la carretera Río Negro, **generado graves afectaciones ambientales y deteriorando puentes y vías del municipio.**

Criollo, señala que desde hace 4 meses Ecopetrol se encuentra realizando supuestas actividades de socialización sobre el  Bloque Carbón 2D, para realizar sísmica, sin embargo, según denuncia el líder social, “**Esas socializaciones han sido muy débiles, con actas firmadas por 8 asistentes**”, incluso asegura que algunas persona manifiestan que pese a que aparecen como si hubieran estado en la reunión, realmente no estuvieron y nunca firmaron ninguna acta.

“Ecopetrol hace unos procedimientos sin cumplir requisitos por lo que la administración municipal le ha escrito a la Agencia Nacional de Hidrocarburos, (ANH) pero esta que no ha respondido”, dice Jhon Fredy.

En ese municipio la **[ANH, ha adjudicado 6 bloques petroleros](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/).** En este momento **Ecopetrol** es el dueño del 50% del bloque Carbón 2D, del otro 50% es dueña la compañía China **Emerald Energy,** y la empresa Petroseismic es la encargada de realizar la sísmica. “Tienen dos años para evaluar si hay petróleo, y si es así, volverán a las malas a sacarlo”, expresa el habitante del Doncello.

Pese a las denuncias y las solicitudes radicadas no ha habido respuesta alguna por parte de las autoridades ambientales, y tampoco por parte de otras instancias como la Defensoría del Pueblo que reciban las denuncias que evidencien las actuaciones violentas por parte de la fuerza pública hacia los campesinos. “**Es una guerra del Estado contra unos campesinos desamparados, pues hay una debilidad institucional muy grande de la ANLA** (Agencia Nacional de Licencias Ambientales) y la ANH, pues no hay un seguimiento y control a las licencias”, indica Criollo.

<iframe id="audio_12892523" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12892523_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
