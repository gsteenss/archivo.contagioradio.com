Title: Siete obispos firman comunicado por la paz y en contra del glifosato
Date: 2019-06-25 20:02
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdos de paz, Glifosato, lideres sociales, Obispos, pacífico
Slug: siete-obispos-firman-comunicado-por-la-paz-y-en-contra-del-glifosato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-25-at-7.15.58-PM-2.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-25-at-7.15.58-PM-3.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

En un comunicado fechado este 25 de junio en Popayán, 7 obispos católicos instaron al gobierno a que de manera inmediata y eficaz proteja a los líderes sociales, rechazaron el anuncio de la reactivación de fumigaciones con glifosato. Pidieron la implementacion del acuerdo de paz con las Farc e invitaron al gobierno a reabrir los diálogos con el ELN .

En el comunicado de 5 puntos, los arzobispos de Cali, Popayán, Palmira, Apartadó, Tumaco, Itsmina e Ipiales Monseñor Darío de Jesús Monsalve y Monseñor Luis José Rueda. También aseguraron que " la próxima jornada de elección de autoridades regionales y locales", es una oportunidad para consolidar el camino hacia la paz territorial.

Los obispos del pacifico y sur occidente colombiano se sumaron a la preocupación creciente por el asesinato continuo y selectivo de líderes sociales y se sumaron a la exigencia de activar las medidas que sean necesarias para frenar la muertes .

En torno a la fumigación con glisofato se unieron al comunicado firmado por 8 obispos en el que afirman "rechazamos formalmente la decisión ya anunciada por parte del gobierno... de reanudar las fumigaciones de cultivos de uso ilícito con glisofato, dadas las consecuencias negativas para la vida humana y el medio ambiente" y reiteraron el llamado al gobierno para que se priorice la vida humana y natural .

En cuanto a la construcción de paz, aseguraron que se unen a la creación de un movimiento regional llamado servidores de la paz para impulsar la implementación de los acuerdos con las Farc y reabrir la mesa de conversaciones con el ELN y las distintas organizaciones armadas. Así mismo reiteraron su compromiso con la defensa de la vida de lo cual se pudo entender que se ofrecen como mediadores .

El comunicado surge en un contexto que puede leerse como favorable a la paz en medio de fuertes manifestaciones sociales y políticas en torno a potenciar cambios estructurales en la vida política económica y social del país.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-25-at-7.15.58-PM-2-225x300.jpeg){.aligncenter .size-medium .wp-image-69591 width="225" height="300"}

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-25-at-7.15.58-PM-3-225x300.jpeg){.aligncenter .size-medium .wp-image-69593 width="225" height="300"}
