Title: "Elecciones constatan la degradación y corrupción del sistema electoral colombiano”
Date: 2015-10-26 17:42
Category: Entrevistas, Judicial, Política
Tags: Bogotá, coaliciones, daniel libreros, elecciones regionales Colombia, Enrique Peñalosa, Germán Vargas Lleras, Guajira, marcha patriotica, neoliberalismo, Polo Democrático Alternativo, Sucre, Unión Patriótica
Slug: elecciones-constatan-el-grado-de-degradacion-y-corrupcion-del-sistema-electoral-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/daniel-libreros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.youtube.com]

<iframe src="http://www.ivoox.com/player_ek_9170916_2_1.html?data=mpakkp6Veo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZOmptHSxcjNs8%2FZ1JDQ0dPXuMLowtOYx9GPq9PVxdSYxsqPqMbb08bRw8jNaaSnhqeg0JDdb8Tj09fi0pKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Daniel Libreros] 

###### [26 oct 2015] 

Daniel Libreros, analista y profesor, destaca la trashumancia electoral y la corrupción ligada al narcotráfico en departamentos como Sucre o la Guajira donde continuarán liderando los poderes regionales clientelistas, lo que para él significa que **“Estas elecciones constatan el grado de degradación y corrupción del sistema electoral colombiano”.**

Por otro lado, el analista aseguró que hay una crisis identitaria de los partidos políticos con las llamadas coaliciones que finalmente generan una **“desfiguración de fronteras, ideológicas”**. En ese mismo sentido, Libreros, asegura que la derrota nacional de la izquierda implica la necesidad de realizar una re-discusión de los planteamientos estratégicos de ese sector político, teniendo en cuenta que las elecciones de ayer demuestran una **crisis sobre la forma como la izquierda gobierna y gestiona poderes electorales.**

**“Se debe abrir una discusión fuerte al interior de la Unión Patriótica, el Polo y Marcha Patriótica,** para que sin sectarismos se busque salidas de corto y mediano plazo frente al panorama que se avecina tanto nacional como internacionalmente”, dice el analista.

Así mismo, no discute que **el verdadero ganador de estas elecciones regionales es Germán  Vargas Lleras,** quien sale favorecido por sus influencias tanto en las elecciones regionales como en las locales, asociado a estructuras mafiosas de cuyos proyectos saldría el dinero para su campaña presidencial, dice Libreros.

Finalmente, frente a la victoria de la Alcaldía de Bogotá de Enrique Peñalosa el analista indica que los resultados electorales implican que “**se viene una arremetida brutal del neoliberalismo en el próximo periodo… Para Bogotá viene en aumento las privatizaciones**”, en todos los sectores de la capital, entre ellos, los servicios públicos.
