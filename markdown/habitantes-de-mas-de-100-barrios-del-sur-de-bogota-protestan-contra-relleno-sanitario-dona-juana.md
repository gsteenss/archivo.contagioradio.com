Title: Habitantes de más de 100 barrios del Sur de Bogotá protestan contra relleno Doña Juana
Date: 2017-08-14 11:28
Category: Ambiente, Nacional
Tags: Bogotá, Enrique Peñalosa, Mochuelo, problemas de salubridad, Relleno sanitario Doña Juana
Slug: habitantes-de-mas-de-100-barrios-del-sur-de-bogota-protestan-contra-relleno-sanitario-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/relleno-doña-juana-e1502727346379.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Habitantes del sur de Bogotá] 

###### [14 Ago 2017] 

Desde las 5 de la mañana de este 14 de Agosto, habitantes de cerca de 130 barrios del sur de la ciudad de Bogotá decidieron **bloquear los puntos de acceso al relleno sanitario Doña Juana**, según ellos por las graves afectaciones ambientales que producen las basuras depositadas en ese lugar. Muestra de ello es la gran cantidad de moscas que, según los manifestantes, han invadido las casas de varios de los barrios y a las que consideran como una plaga.

Según Yurani Muñoz, vocera de los habitantes de ese sector de la ciudad, la situación es desesperante y desde 1997, cuando explotó el relleno sanitario, **no han parado los olores, las plagas de roedores** y ahora los insectos invaden sus casas sin que hasta el momento haya una salida a la situación de insalubridad que afrontan más de 500.000 mil personas habitantes de ese sector. (Le puede interesar: ["Comunidades reclaman cierre definitivo del relleno Doña Juana"](https://archivo.contagioradio.com/comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana/))

En medio de la protesta, los manifestantes denuncian que **hacia las 9 de la mañana comenzaron operativos policiales** que intimidan a quienes participan del plantón, incluso denuncian la llegada de integrantes del Escuadrón Móvil Antidisturbios, ESMAD, por lo que se teme una agresión por parte de ese cuerpo policial.

### **Exigencias de la comunidad** 

Los habitantes más afectados **le han exigido al alcalde Enrique Peñalosa que haga presencia en la zona** para tomar decisiones frente al problema. Igualmente manifestaron que hay un “mal manejo de los desechos por parte del operador del relleno y esto causa un problema de salud pública”. Yurani Muñoz manifestó que la basura “no la tapan y no la cubren con arcilla y plástico". (Le puede interesar: ["¿Cómo podría aprovecharse la basura del relleno sanitario Doña Juana?"](https://archivo.contagioradio.com/como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana/))

Adicionalmente, Muñoz indicó que los desechos llegan al río Tunjuelo ocasionando un problema de salud pública por lo que **han enviado derechos de petición a la Alcaldía Mayor de Bogotá** comentando la problemática, pero no han recibido respuesta alguna. Muñoz hizo énfasis en que “Entre las instituciones se tiran la pelota y no solucionan nada”.

Finalmente, los habitantes indicaron que “para que solucionen una deuda histórica de 30 años” están preparando un **paro cívico con 8 localidades del sur de la capital** para exigir respuestas a al problema de salubridad de estas comunidades y además conmemorar el derrumbe del relleno sanitario que ocurrió en 1997.

<iframe id="audio_20324184" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20324184_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
