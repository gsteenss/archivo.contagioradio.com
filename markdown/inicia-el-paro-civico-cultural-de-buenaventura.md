Title: Inicia el paro cívico cultural de Buenaventura
Date: 2017-05-16 12:19
Category: Movilización, Nacional
Tags: buenaventura, Paro cívico
Slug: inicia-el-paro-civico-cultural-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/WhatsApp-Image-2017-05-16-at-11.29.51-AM1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 May 2017]

Desde este 16 de mayo los habitantes de Buenaventura entraran a un paro cívico para exigirle al gobierno mayor inversión en infraestructura vial, **acciones contra la corrupción, garantías del derecho a la salud con la mejoría de este servicio** y garantías de vida para las y los jóvenes de la ciudad que son víctimas del reclutamiento ilegal por parte de bandas criminales que operan en el territorio.

**Los puntos de concentración de la población serán**: El Boulevard del Centro, la Casa de la Cultura, el Puente del Piñal, Santa Cruz, el SENA, la carretera Alterna-Interna, el barrio Los Pinos, San Antonio, EL Reten, Gallinero y La Delfina.

### **Agenda de movilización** 

El próximo 17 de mayo se **realizará la marcha por el desempleo, la tercerización laboral y la no formalización del trabajo**. El punto de encuentro de la marcha será desde Almacenes la 14, a las 9:00 am y culminará en el punto llamado El Reten. Le puede interesar:["Buenaventura irá a paro y exigirá vivir con dignidad y paz en sus territorios"](https://archivo.contagioradio.com/40089/)

El 18 de mayo se llevará a cabo el **cacerolazo por la salud, para exigirle al gobierno** que se respete y garantice este derecho en la población, **se realice la reapertura del Hospital Departamental**. Los puntos de concentración serán los ya mencionados anteriormente, desde las 6:00pm.

Para el 19 de mayo, los habitantes tienen planeado **realizar una quema masiva de servicios públicos**, los lugares de encuentro serán los mismos, desde las 7:00pm y el 20 de mayo se hará la gran marcha por el territorio, tendrá cómo punto de encuentro el Puente del Piñal a las 9:00am e irá hasta el Boulevard del Centro.

**El 21 de mayo se conmemora el día de la Afrocolombianidad**, razón por la cual, durante toda la jornada, cada uno de los puntos de concentración tendrá diferentes actividades lúdicas y culturales. Le puede interesar: ["Gobierno hace oídos sordos al paro del Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

De acuerdo con Victor Vidal, integrante del comité promotor del paro cívico, pese a que el anunció se había hecho con mucho tiempo de anticipación, el gobierno aún no se ha expresado frente a las exigencias de la ciudadanía de Buenaventura “**tenemos problemas de corrupción en lo local y el Estado tampoco nos ayuda porque no respalda nuestras denuncias** y sus formas de contratación terminan en otras maneras de corrupción”.

La ciudadanía espera que en el transcurso de esta semana el gobierno nacional entable una mesa de conversación que atienda las exigencias y de pronta solución a estas problemáticas, de igual forma Vidal aseguró que los actos de movilización se realizarán de forma pacífica y esperar que la **Fuerza Pública no hostigue a los manifestantes ni provoque confrontaciones.**

###### Reciba toda la información de Contagio Radio en [[su correo]
