Title: "Levantaremos el paro cuando haya compromisos serios": pueblo chocoano
Date: 2016-08-18 17:31
Category: Movilización, Nacional
Tags: Abandono estatal Chocó, Foro Interetnico Solidaridad Chocó, Paro cívico Chocó, Paro en Quibdó
Slug: levantaremos-el-paro-cuando-hayan-compromisos-serios-pueblo-chocoano
Status: published

###### [Foto: Archivo Particular ] 

###### [18 Ago 2016] 

El pueblo indígena, afro y mestizo del Chocó se cansó del histórico abandono estatal y de la forma excluyente en la que los gobiernos y el país los ha tratado, asegura Richard Moreno, coordinador del 'Foro Interetnico Solidaridad Chocó', agrega que el actual paro en la región busca **la reivindicación de los derechos que han sido vulnerados con la desatención** de los gobiernos nacional, departamental y local, en los sectores de servicios públicos, salud, educación e infraestructura, principalmente.

De acuerdo con Moreno hace 12 años el Estado intervino la secretaria de salud, cuando el sector tenía un pasivo cercano a los \$4 mil millones, hoy después de más de una década de administraciones enviadas de Bogotá, Tunja y Cali, el déficit es de por lo menos \$60 mil millones, el departamento no cuenta con hospitales de quinto, cuarto o tercer nivel y de los únicos dos hospitales de segundo nivel, **sólo queda uno con capacidad para internar a 60 personas** **cuando la zona cuenta con una población aproximada de 500 mil habitantes**.

Por otro lado, la pavimentación de la carretera que conecta a Quibdó con Medellín fue adjudicada por el Gobierno nacional a la firma Solarte en 2008, pero **la obra no se ha terminado y la vía lleva más de dos meses taponada a causada de un deslizamiento de tierra** que, según afirman los pobladores, se originó por la mala gestión del proyecto, resalta que por lo menos 80 personas han perdido la vida en los accidentes que se presentan.

Por si fuera poco, **13 cabeceras municipales del departamento no cuentan con suministro permanente de la energía eléctrica** que es manejada por Electricaribe y por la que deben pagar facturas de hasta \$150 mil y en Quibdó, el acueducto que el Gobierno nacional encargó a Empresas Públicas de Medellín, sólo cubre al 30% de la población.

Es por estos motivos que todos los sectores sociales del departamento se unieron en la movilización pacífica que ya completa dos días, con la que buscan llamar la atención del Gobierno nacional, quien según afirma Moreno **tiene una responsabilidad del 60%**, frente al 40% restante que le compete a las autoridades locales y a la sociedad civil, por terminar eligiendo a quienes históricamente han detentado el poder con índices de corrupción.

El poblador asevera que la salida a la guerra debe ser negociada y que el pueblo chocoano prefiere a las FARC proponiendo "proyectos de ley en el Congreso, que tirando balas en el Chocó", y agrega que **su apuesta por la paz es firme porque han sufrido en carne propia las tragedias** de Bojayá y Riosucio, y todo tipo de vejámenes en el marco del conflicto armado, por lo que tienen "la autoridad para decir que la guerra es sólo para los que viven de ella".

Moreno concluye afirmando que el paro es indefinido, que el Gobierno nacional conoce el pliego de peticiones y que están dispuestos a negociar en medio del paro **que será suspendido cuando hayan compromisos serios, con un cronograma claro de implementación y disponibilidad presupuestal**, porque "el Gobierno no puede cometer el error de jugar con nuestra paciencia" en una toma pacífica que puede transformarse en bloqueo de vías.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
