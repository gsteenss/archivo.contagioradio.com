Title: Mesa de negociación de paro de Buenaventura no se traslada a Bogotá
Date: 2017-06-02 13:45
Category: DDHH, Movilización
Tags: Fuerza Pública, Paro de Buenaventura
Slug: bunaventura-denuncia-violaciones-a-dd-hh-por-parte-de-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Santafe] 

###### [02 Jun 2017] 

El comité de defensa de derechos humanos del paro cívico de Buenaventura  advirtió que son falsos los señalamientos de trasladar la mesa de negociaciones a Bogotá y que el paro continúa. Además **señaló que la cifra de personas heridas, que hasta el momento era de 300 personas, podría aumentar tras la arremetida del ESMAD durante esta mañana**.

En su más reciente informe que hay más **300 personas heridas y lesionadas, 10 que recibieron impactos de bala y una adolescente que habría perdido a su bebé** durante las arremetidas del ESMAD hacia la población civil. Le puede interesar: ["Represión en Buenaventura deja 9 heridos con arma de fuego"](https://archivo.contagioradio.com/represion-a-paro-civico-en-buenaventura-dejo-9-heridos-por-arma-de-fuego/)

De acuerdo con Adriel Ruíz, integrante del Comité de Defensa de Derechos Humanos,  en estos momentos el hospital en donde están siendo atendidas la mayoría de personas no da abasto “la coyuntura tan compleja visibiliza la realidad, no hay un hospital que atienda la gente, **hay una clínica privada a la que el comité tuvo que pagarle para que atendiera a una joven embarazada, que perdió su bebé por los gases lacrimógenos**”.

Durante el día de hoy se espera que se dé un nuevo encuentro entre el comité de defensa de derechos humanos y la Procuraduría general en donde se expongan **las graves violaciones cometidas contra la comunidad por parte de la Fuerza Pública**. Le puede interesar:["Persiste arremetida del ESMAD contra población en Buenaventura"](https://archivo.contagioradio.com/persiste-arremetida-del-esmad-contra-poblacion-de-buenaventura/)

Sin embargo, Ruíz fue enfático al afirmar que las peticiones que hace el pueblo de Buenaventura son los derechos fundamentes que históricamente se le han negado a esta ciudad “**la gente está pidiendo cosas necesarias para cualquier ser humanos, colombiano, que quiera vivir dignamente como agua, salud, servicios públicos,** un alcantarillado, educación, infraestructura”.

### **El Puerto en donde el gobierno no ha respetado el Paro** 

Los pobladores de Buenaventura le han expresado en diferentes ocasiones al presidente Santos la indignación por permitir que haya entrada y salida de tracto mulas del puerto, **cuando el resto de la ciudad se encuentra en toque de queda, y además, que estos ingresos se den bajo la vigilancia del ESMAD**.

Esta situación ha generado tensiones al interior de la ciudadanía y choques con la Fuerza Pública que han dado como resultado 3 tracto mulas incineradas. Le puede interesar: ["ESMAD impide labores de defensores de DD.HH y periodistas en Buenaventura"](https://archivo.contagioradio.com/41556/)

Sin embargo, hasta el momento no se conoce ningún pronunciamiento por parte del presidente Santos, para Ruíz este silencio es una muestra de “**una influencia grande de los capitales en donde se privilegia el puerto y no la ciudad**” y frente a esta situación expresó que están entablando una denuncia internacional, ante las embajadas.

<iframe id="audio_19047175" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19047175_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
