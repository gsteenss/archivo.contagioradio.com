Title: El respaldo a los Acuerdos en el Plebiscito es el primer paso hacia la paz
Date: 2016-09-08 14:57
Category: Carlos Julio, Opinion
Tags: implementacion acuerdos, proceso de paz, Sindicalismo
Slug: el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-de-mayo-16-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [**Carlos Julio Díaz Lotero - Director general ENS**](https://archivo.contagioradio.com/carlos-julio-diaz/) 

###### 8 Sep 2016

Tras cuatro años de negociaciones, las Farc y el Gobierno anunciaron el pasado 24 de agosto el acuerdo definitivo que pone fin a 52 años de una confrontación armada responsable de más de 220 mil muertes, más de 30 mil secuestros, más de 10 mil víctimas de minas anti-persona, y más de 7 millones de desplazados.

Violencia que afectó al sindicalismo en sus derechos a la vida, la libertad y la integridad, en diferentes modalidades. Según datos de la Escuela Nacional Sindical, desde 1977 a hoy van más de 3 mil homicidios y 14 mil hechos de violencia, que se expresaron en desplazamientos forzados, detenciones arbitrarias, atentados, desapariciones, secuestros y torturas, entre otros.

El fin del conflicto armado con las FARC no resuelve los problemas de desempleo, informalidad laboral, bajos ingresos, hambre, desnutrición infantil, delincuencia, desigualdad, colapso del sistema de seguridad social en salud, etc. Su importancia estriba en que se empieza a cerrar esa fábrica de víctimas y nos abre una oportunidad para reconstruir el país. Es un acuerdo que pone fin al enfrentamiento armado que desde hace más de 50 años ha sido centro en las agendas públicas, y hoy nos va a permitir concentrarnos en resolver los problemas que han servido de caldo de cultivo a una violencia que llegó a niveles de degradación nunca imaginados.

Los acuerdos de La Habana en los temas de desarrollo rural, participación política, reparación integral a las víctimas y solución al flagelo de las drogas ilícitas, hacen parte del proceso de solución a los problemas estructurales del país. Su implementación es una condición necesaria, pero no suficiente. Es necesario complementar la agenda de La Habana con las agendas democráticas de los movimientos sociales y políticos, incluido el sindicalismo. La paz depende, no solo de la implementación, sino de la puesta en juego de las diferentes agendas democráticas para que sean también políticas públicas que complementen los faltantes que no se lograron abordar en las negociaciones Farc–Gobierno.

### El sindicalismo ha planteado una propuesta de agenda de 5 puntos, a saber: 

1.  Una política pública de trabajo decente para los centros urbanos y el sector rural, que en este último caso involucra los acuerdos de formalización laboral y protección social del punto uno en Desarrollo Rural Integral de los acuerdos de La Habana. La política pública de Trabajo Decente tiene un componente en materia de generación de empleo e ingresos que abre un debate al modelo de desarrollo.
2.  Garantías a los derechos de libertad sindical con énfasis en los derechos de organización sindical, negociación colectiva, huelga y protesta social, que hacen parte de los acuerdos del punto dos sobre Participación Política.
3.  [Promoción del diálogo social como procedimiento propio de los sistemas democráticos para la resolución de los conflictos, que es un mensaje implícito en la integralidad de los acuerdos de La Habana. De ahora en adelante será la fuerza de los argumentos, y no los argumentos de la fuerza y la violencia, el método para resolver nuestras diferencias.]
4.  En desarrollo del Acuerdo de La Habana sobre derechos de las víctimas, se garantizan los derechos de las víctimas de la violencia antisindical a la verdad, la justicia, la reparación individual y colectiva, así como a la Garantía de No Repetición. Se busca superar la violencia contra el sindicalismo, la impunidad que es superior al 95%, la reparación para sanar tanto dolor y crear condiciones para reconstruir al sindicalismo.
5.  Fortalecimiento del Estado en las funciones de inspección laboral y de administración de justicia en el mundo del trabajo. En desarrollo del enfoque de paz territorial de los acuerdos de La Habana se necesita priorizar los territorios más afectados por la violencia, para que con participación de las comunidades organizadas y el sindicalismo se construya Estado e institucionalidad con y desde las regiones.

Para transitar de la paz negativa, en que hoy nos encontramos, a una paz positiva, que es la paz con justicia social, necesitamos en principio de la implementación de los acuerdos de La Habana. Pero de todos nosotros depende que se puedan ejecutar.

El próximo 2 de octubre debemos salir todos a respaldar con nuestro voto el plebiscito que someterá a refrendación estos acuerdos. No puede ganarnos la pereza o la indiferencia, está en juego el futuro de nuestro país, para que nuestros hijos y nietos tengan una oportunidad de sentir y vivir en un país que tiene una cultura de convivencia y tramite los conflictos por la vía democrática.

A la pregunta: ¿Apoya usted el acuerdo final para terminar el conflicto y construir una paz estable y duradera? El 2 de octubre todos debemos responder SÍ.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
