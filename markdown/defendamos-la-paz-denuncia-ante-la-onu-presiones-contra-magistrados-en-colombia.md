Title: Defendamos la Paz denuncia ante la ONU presiones contra magistrados en Colombia
Date: 2019-05-13 16:25
Author: CtgAdm
Category: Paz, Política
Tags: Corte Constitucional, Corte Suprema de Justicia, Defendamos la Paz
Slug: defendamos-la-paz-denuncia-ante-la-onu-presiones-contra-magistrados-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DefendamosPaz.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Corte-Constitucional.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Medium] 

A través de una comunicación, este 13 de Mayo, integrantes de la iniciativa **“Defendamos la paz”** solicitaron a la **Alta Comisionada de la ONU, Michelle Bachellete**, que se pronuncie y adelante todas las acciones necesarias para que cese la persecución y presión en contra de los jueces en Colombia, dado que ellos y ellas están ad portas de tomar decisiones importantes para la construcción de la paz.

La solicitud se basa en varios hechos concretos. Por un lado el reciente retiro de las visas a dos magistrados de la **Corte Constitucional y uno de la Corte Suprema de Justicia.** Por otro lado la denuncia en contra de magistrados de la JEP, entre ellos Patricia Linares, ante la Comisión de Acusaciones de la Cámara.

> La independencia judicial es un valor enorme que tiene la democracia colombiana. Hoy como nunca está seriamente amenazada desde distintos frentes,mientras el gobierno calla en forma inexplicable .
>
> — Juan Fernando Cristo (@CristoBustos) [10 de mayo de 2019](https://twitter.com/CristoBustos/status/1126928292882407424?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por otro lado están las recientes denuncias sobre interceptaciones ilegales y otros actos de espionaje a la Corte Constitucional que fueron denunciado recientemente por la presidenta de la entidad, Gloria Stella Ortiz y que fueron confirmadas por la Fiscalía.

"Defendamos la Paz" también se refiere a que frente a estas situaciones, el Gobierno se han pronunciado, **“un silencio preocupante en condiciones en las que se hace patente que los altos tribunales y sus jueces no cuentan con las garantías mínimas necesarias que requiere su labor”.** [(Lea también: La iniciativa ciudadana que invita a que \#DefendamosLaPaz)](https://archivo.contagioradio.com/iniciativa-ciudadana-defendamoslapaz/)

Destacan a través del comunicado que estos ataques se dan un contexto en que las instituciones afectadas se encuentran resolviendo diversas cuestiones vinculadas a la normativa del Acuerdo de Paz, la legislación de la justicia transicional, el tratamiento de los cultivos de uso ilícito y el mecanismo de extradición y que al verse comprometidas ven afectada directamente la independencia de los poderes públicos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
