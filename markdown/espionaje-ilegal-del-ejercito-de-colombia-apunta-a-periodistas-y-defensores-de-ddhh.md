Title: Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DDHH
Date: 2020-05-02 13:02
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Chuzadas, #DerechosHumanos, #Ejército, #Estado, #Inteligenciamilitar, #Periodistas
Slug: espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/fuerzas-militares.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de varias revelaciones sobre las operaciones de espionaje ilegales del Ejército en contra de políticos, periodistas nacionales y extranjeros, así como organizaciones de abogados y no gubernamentales en Colombia, según lo revelado por la revista Semana, se construyeron perfiles con direcciones, teléfonos, redes sociales, interacciones y todo tipo de información personal de mas de 100 “blancos”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La gravedad de esta situación se agudiza pues, aunque hay anuncios por parte del gobierno y el Ministerio de Defensa acerca del retiro de algunos mandos militares, no hay acciones de fondo para impedir que este tipo de hechos se repitan, **año tras año como ha venido sucediendo desde hace cerca de 20 años**, cuando se realizaron las operaciones ilegales del DAS.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente se han conocido prácticas por parte de las FFMM en las que en sus redes sociales tachan como “opositores” a una serie de medios de comunicación entre los que se encuentran **Contagio Radio y Cuestión Pública**. Esta acción fue calificada como de extrema gravedad por parte de abogados y periodistas, dado que casi siempre una manifestación pública de esta índole es precedente para amenazas de muerte y acciones contra la vida de las personas señaladas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los periodistas siguen siendo los principales objetos de operaciones ilegales del ejército

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la publicación de la revista semana se revela que periodistas extrajeron y colombianos fueron víctimas de los seguimientos, espionajes y perfilamientos por parte del ejército, además de Nick Casey según los reportes de la investigación periodística fueron espiados periodistas de corte internacional y nacional, citando medios como Rutas del Conflicto, aunque no se descarta que otros medios sean también víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“La orden de los mandos del Bacib, Caimi y Caci, fue que, por instrucciones de mi general, había que conseguir todo lo que se pudiera sobre el periodista gringo, especialmente porque consideraban que, por lo que publicó, estaba atacando a la institución y, en particular, a mi general Martínez. Había que averiguar con quién habló para eso y, además, conseguir elementos para tratar de desprestigiarlo a él y al medio. Eso se hizo con unos portales” señala la publicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque menciona Semana que son más de 130 las llamadas “carpetas secretas” se hace referencia especial a “La liga contra el Silencio” de la que hacen parte periodistas nacionales e internacionales, **así como los ya mencionados “Rutas del Conflicto”**. Vale la pena resaltar que una de las operaciones especiales fue dirigida en contra de periodistas que han logrado entrevistas de dirigentes del ELN. Contagio Radio también ha desarrollado dicha labor en varias ocasiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son necesarias acciones de fondo para que espionaje no se hagan costumbre

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de los principales problemas que se afrontan en Colombia, cuando de operaciones ilegales se trata, es la impunidad. Una tasa del 90% de impunidad en casi todas las esferas del poder político o militar, conllevan a pensar que este tipo de acciones no tendrán u castigo por lo cual su práctica puede hacerse regular.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En palabras de José Miguel Vivanco, otra de las víctimas, “**Hay enormes riesgos de que estas prácticas que violan el derecho a la privacidad** y atentan contra el sistema democrático terminen por normalizarse en el país”. (Le pude interesar: "[Los crímenes de la inteligencia militar llegaron a la JEP](https://archivo.contagioradio.com/los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las lecciones que dejan las investigaciones contra las personas que hicieron parte del DAS y que fueron determinantes en la práctica de seguimientos y amenazas contra organizaciones de DDHH, incluso, en las que se determinó la participación de sus agentes en asesinatos, es que **los altos mandos, o las personas que dieron la orden nunca fueron judicializados**, además muchos de los procesos precluyeron o resultaron en condenas que en nada repararon los daños causados al tejido social colombiano y tampoco restablecieron la confianza en las instituciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Anuncios de prensa son insuficientes, la inteligencia no puede seguir sirviendo para desarrollar operaciones ilegales

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Ministro de Defensa hizo un anuncio en el que, supuestamente, retiró del cargo a por 10 militares activos, sin embargo, se negó a entregar los nombres y las responsabilidades de estas personas en las operaciones ilegales del ejército. (Le puede interesar: "[Exigimos sanción disciplinaria y judicial a integrantes de la fuerza pública involucrados en espionaje contra el Colectivo de Abogados José Alvear Restrepo](https://www.justiciaypazcolombia.com/exigimos-sancion-disciplinaria-y-judicial-a-integrantes-de-la-fuerza-publica-involucrados-en-espionaje-contra-el-colectivo-de-abogados-jose-alvear-restrepo/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia se desconoce la efectividad de esta medida, así como se confirma que la salida de Nicacio Martínez de la comandancia del Ejército **si tuvo relación directa con este tipo de operaciones ilegales**, contrario a lo que afirmó el gobierno en su momento. (Le puede interesar: "[«Las peores prácticas de la inteligencia militar nunca se fueron»)](https://archivo.contagioradio.com/las-peores-practicas-de-inteligencia-militar-nunca-se-fueron/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el colectivo de Abogados José Alvear Restrepo, víctima reiterada de las operaciones ilegales del ejército, del DAS y de todo tipo de instituciones estatales hizo un llamado a que la fiscalía actúe con celeridad y se despeje el manto de impunidad como única garantía para que este tipo de crímenes no sigan lesionando el estado de derecho.

<!-- /wp:paragraph -->
