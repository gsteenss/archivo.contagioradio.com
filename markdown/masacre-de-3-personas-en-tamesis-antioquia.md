Title: 3 personas son masacradas en Támesis, Antioquia
Date: 2020-11-10 11:37
Author: AdminContagio
Category: Nacional
Tags: Antioquia, masacre
Slug: masacre-de-3-personas-en-tamesis-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Masacre-Nechi-Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 10 de noviembre **en el municipio de Támesis, noroeste del departamento de Antioquia** se registró una nueva masacre de 3 personas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1326141773094285313","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1326141773094285313

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**El hecho se registró sobre las 5:30 de la tarde de este lunes 9 de noviembre en el barrio La Ramada**, ubicado en el corregimiento de Palermo municipio de Támesis, Antioquia. ([Son asesinadas 5 personas en masacre de Nechí, Antioquia](https://archivo.contagioradio.com/son-asesinadas-5-personas-en-masacre-de-nechi-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este lugar ingresaron hombres armados en una motocicleta y dispararon contra dos personas de la comunidad, poniendo en peligro habitantes de la zona que se encontraban en inmediaciones del hecho. ([En menos de 24 horas dos líderes de Colombia Humana son asesinados](https://archivo.contagioradio.com/en-menos-de-24-horas-dos-lideres-de-colombia-humana-son-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según autoridades locales **las víctimas mortales fueron tres, uno de ellos fue identificado como Luis Parra Sepúlveda de 65 años,** quién según indican testigos murió producto de una bala perdida mientras se encontraba cerca a su vivienda, **de las otras dos víctimas aún se desconocen sus nombres pero se establece que están en un promedio de edades entre los 20 y 23 años.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que aún se desconoce el motivo y los perpetradores de este hecho, la comunidad de Antioquia alerta sobre el aumento de la violencia en el departamento, la cual según el Instituto de Estudios para el Desarrollo y la Paz -Indepaz- **se resume en 15 masacres registradas en este departamento, el primero con más masacres durante el 2020, las cuales ya recopila 72.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1322650351351353351","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1322650351351353351

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

(Le puede interesar: [INFORME DE MASACRES EN COLOMBIA DURANTE EL 2020, por Indepaz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/))

<!-- /wp:paragraph -->
