Title: Joven sufre grave trastorno mental tras 26 días de reclutamiento por FFMM
Date: 2015-05-05 15:07
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Cauca, Comisión Intereclesial de Justicia y Paz, ejercito, Hospital Militar, Hospital Universitario del Valle, Julián Narvaez, Puerto Carreño, servicio militar, soldado, Vichada, Villavicencio
Slug: joven-sufre-trastorno-mental-tras-26-dias-de-reclutamiento-forzado-por-ffmm
Status: published

##### Foto: las2orillas.co 

<iframe src="http://www.ivoox.com/player_ek_4451609_2_1.html?data=lZmik5uUfY6ZmKiak5aJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDqxtOY1drKtsaf1dfO1dnTts%2FjjNLS0NnFsIzo08bgjZeab8WZpJiSo6nFt4zYxpDfx8jQudXVzs6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yolis Gutiérrez, madre de Julián Narváez, joven reclutado] 

**“Trastornos psicóticos agudos y transitorios" y "escasa coherencia en sus relatos, sentimientos de minusvalía y alteraciones de conducta motora”** son los diagnósticos sobre el dictamen médico del estado de salud del soldado bachiller, Julián Andrés Narváez Gutiérrez, quien 27 días antes de presentar este cuadro psiquiátrico, fue reclutado el mes de marzo de este año en Bogotá, a los **18 años de edad.**

El joven oriundo del departamento del Cauca había partió hacia Bogotá buscando oportunidades de empleo para ayudar a su familia “*Mamá yo me quiero ir para la ciudad, mi ilusión es estudiar, tener un trabajo para poderlos ayudar a ustedes, porque quiero superarme*”, es el relato de Yolis Gutiérrez, madre de Julián, quien cuenta las razones de su hijo para llegar a Bogotá.

**Julián fue vinculado a la 28 Brigada de Selva, Batallón de A.S.P.C No. 28 “Bochica”** para prestar servicio militar desde el mes de marzo. El 11 de marzo informó por teléfono a su padre sobre su ingreso al Ejército Nacional, señaló que **le practicaron algunos exámenes médicos y que había salido apto,** por lo que sería llevado a Villavicencio y luego trasladado a Puerto Carreño, en Vichada.

De acuerdo a la [Comisión Intereclesial de Justicia y Paz, que acompaña el caso](http://justiciaypazcolombia.com/Violacion-de-derechos-humanos-al), el 5 de abril la Brigada expidió una boleta de buen trato firmada supuestamente por el joven, en ese papel se encontraba la huella digital de Julián y estaba escrito su nombre al revés, y **su letra y firma no coinciden con la de él, **según denunció su madre**.**

El día siguiente, Julián fue remitido al Batallón de Apoyo y Servicio para el Combate No. 28 “BOCHICA”- BASER por el teniente Andrés Castillo Narváez, para que se sometiera a una valoración psicológica por: **“subordinación y por presentar problemas de aprendizaje”,** lo que llevó a que el joven fuera devuelto por personal militar a su tía en Bogotá, quien asegura que **Julián no los reconocía como integrantes de la familia, ni respondía a su nombre de pila.**

La madre de Julián, explica que tuvo que dejar sus otros dos hijos 8 y 13 años en Cauca, para llegar a Bogotá a estar pendiente de la salud de su hijo y en búsqueda de respuesta sobre las causas que generaron el cuadro psiquiátrico de Julián, quien estaba en perfecto estado de salud antes de ser reclutado, “**el 7 abril nos lo entregaron completamente loco y en condiciones lamentables, no sabemos qué pasó**, y por qué 27 días después nos lo devuelven por problemas psicológicos, cuando ya había sido apto, no nos lo esperábamos”, expresa Yolis.

**Ningún ente del gobierno y tampoco del ejército ha dado alguna explicación** sobre la situación de Julián, y además han negado la atención psiquiátrica y neurológica de urgencia que requiere el soldado bachiller. En este momento es atendido gracias a que es beneficiario de salud por parte de Yolis, sin embargo, días antes cuando necesitaba una resonancia magnética y su familia requería una constancia para la realización de dicho examen,  **el ejercito la negó y no quiere entregar la historia clínica de Julián,** hecho que tampoco se explica la madre, quien afirma que el joven fue ingresado al Hospital Militar Central en calidad de particular, siendo soldado bachiller y no ha sido trasladado a una unidad especializada del hospital pues los psiquiatras que lo atendieron advirtieron de la importancia de la remisión a unidad de salud mental donde pueda ser hospitalizado.

**“Uno no sabe qué hacer, la desesperación es tremenda, por que cómo es posible que uno despide a sus hijos con la esperanza de que van regresar bien,** y tener una noticia así de un momento a otro que su hijo está mal, es muy triste, y todo por culpa del gobierno”, dice la madre comunitaria, quien adicionalmente hace un llamado al gobierno sobre la información que entrega respecto a las condiciones para que los jóvenes presenten servicio militar, “si la gente fuera consiente de cuáles son los procedimientos del ejército ninguna mamá aceptaría que sus hijos presenten servicio”.

Mientras tanto la Comisión de Justicia y Paz acompaña el caso y a la familia de Julián. Desde hace un mes los padres han enviado derechos de petición a la Defensoría del Pueblo, la Procuraduría, el ejército, la Contraloría y aun no hay respuesta, ya que el ejército argumenta que Julián no había jurado bandera y por ende, no tiene acceso a los servicios de salud que por derecho le corresponderían al joven soldado.

El caso de Julián Andrés Narváez, solo es un ejemplo de las consecuencias de la guerra y del servicio militar obligatorio, pues se han conocido otros casos como el de un **joven  de 18 años, quien fue encontrado y remitido al Hospital Universitario del Valle, tras sufrir un trastorno mental después de incorporarse al Ejército y permanecer 9 días en la unidad militar.**

El padre de este joven al igual que la madre de Julián, exigen atención a la salud de sus hijos, y las explicaciones sobre las condiciones y acciones a las que fueron sometidos los jóvenes para terminar en ese estado de salud mental.
