Title: Proceso de Comunidades Negras celebra su quinta asamblea Nacional
Date: 2017-08-04 17:58
Category: Movilización, Nacional
Tags: acuerdos de paz, Proceso de Comunidades Negras
Slug: proceso-de-comunidades-negras-celebra-su-quinta-asamblea-nacional
Status: published

###### [Foto: Proceso de Comunidades Negras] 

###### [04 Ago 2017] 

El Proceso de Comunidades Negras (PCN) celebrará su quinta asamblea nacional en Buenaventura, del 4 al 7 de agosto, en donde se espera que más de 600 delegados de todo el país reafirmen **su voluntad por reivindicar las luchas de los pueblos afro en defensa de la paz, la libertad y la esperanza.**

**Al evento también llegarán delegaciones internacionales de países como Estados Unidos, Bolivia, Uruguay,** entre otros, de igual forma, organizaciones internacionales como la ACNUR y la MAPP OEA, estarán acompañando el escenario. (Le puede interesar: ["¿Qué ha pasado a dos meses del paro cívico de Buenaventura?"](https://archivo.contagioradio.com/que-ha-pasado-a-dos-meses-del-paro-civico-en-buenaventura/))

De acuerdo con María Inés Quiñonez, vocera del PCN, este espacio servirá para realizar un intercambio de procesos y experiencias que se gesten en cada comunidad, entre ellos el que se ha llevado con la consulta previa **“hemos hecho la presentación y la propuesta de cómo debe reivindicarse la consulta previa y libre, y sobretodo reivindicar los acuerdos de La Habana a través de este mecanismo con la implementación**”.

Otro de los temas fundamentales que se debatirán en la quinta asamblea es el capítulo étnico de los acuerdos y su implementación, “**nosotros en nuestros territorios, hemos vivido la guerra y por tanto es fundamental que la implementación tenga en cuenta nuestras voces**, nuestros saberes y sobretodo nuestro ánimo y deseo porque la paz exista” manifestó Quiñonez.

La Ley 70 de 1993, que reconoce a las comunidades negras, también será un punto de la agenda de la asamblea, debido a que el PCN ha manifestado que 24 años después, muchos de sus artículos no han sido regulados ni puestos en reglamentación. (Le puede interesar: ["Se creará Instancia con pueblos étnicos para implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz/))

El evento rendirá un homenaje especial a las y los mayores de cada una de las comunidades y contará con la participación de delegaciones que harán muestras culturales durante los días de asamblea y al finalizar el PCN hará conocer las conclusiones de su **balance en temas como el proceso de paz entre el gobierno y el ELN, la implementación de los acuerdos de la Habana y el avance en las garantías de los derechos de las comunidades afro.**

###### Reciba toda la información de Contagio Radio en [[su correo]
