Title: ¿Cuándo se establecerán responsabilidades por cierre en vía al Llano?
Date: 2019-06-25 18:31
Author: CtgAdm
Category: Comunidad, Judicial
Tags: Coviandes, Derrumbe, Sarmiento Angulo, Vía al Llano
Slug: responsabilidades-cierre-via-al-llano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Vía-al-Llano.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto:] 

Organizaciones defensoras de derechos humanos emitieron un comunicado sobre el cierre en la vía al llano en el que señalaban algunas afectaciones a los derechos fundamentales de las comunidades habitantes de la Orinoquía, y **cuestionaban la falta de acciones penales que determinen los responsables por los constantes cierres a la vía.** Las organizaciones también pidieron que se tomen medidas urgentes por parte del Gobierno para solucionar la situación de fondo, y no solo mitigarla.

> [\#Villavicencio](https://twitter.com/hashtag/Villavicencio?src=hash&ref_src=twsrc%5Etfw) [\#AEstaHora](https://twitter.com/hashtag/AEstaHora?src=hash&ref_src=twsrc%5Etfw) adelantamos Consejo de Ministros ampliado con [@ViceColombia](https://twitter.com/ViceColombia?ref_src=twsrc%5Etfw) y participación de Gobernadores de Meta y Cundinamarca, así como alcaldes, congresistas y representantes de gremios. Revisamos medidas que nos permitan mitigar afectaciones en la Vía al Llano. [pic.twitter.com/uDwHbjA9Nn](https://t.co/uDwHbjA9Nn)
>
> — Iván Duque (@IvanDuque) [25 de junio de 2019](https://twitter.com/IvanDuque/status/1143606101042245632?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Derechos afectados tras cierre de la vía al Llano** 

Cerca de una decena de organizaciones agrupadas en la **Red de Llano y Selva,** expresaron que el cierre de esta vía genera una violación a las comunidades de Meta, Vichada y Guaviare porque impide el paso de alimentos, el derecho a la libre movilidad y encarece el costo del transporte; ello se evidencia en las tarifas que están cobrando empresas por tiquetes aéreos entre Bogotá y Villavicencio que superan incluso el millón de pesos.

El cierre significa un impacto para los comunidades, igualmente para comerciantes y quienes prestan servicios turísticos, de mercancía o venta de productos agrícolas a quienes vienen de Bogotá; razón por la que han pedido la declaratoria de emergencia económica, argumentando que las pérdidas ascienden a un billón de pesos. Por estas situaciones, este martes se realizaron manifestaciones pidiendo al Gobierno soluciones inmediatas, y aunque el presidente Duque dijo que tomaría medidas, pidieron que también se establezcan responsabilidades judiciales por el cierre de la Vía.

### **¿Quién debería responder por el desastre en la vía?**

Según lo explicó **Juan David Espinel, secretario técnico de la Red de llano y selva**, el contrato con que opera la vía es similar al establecido por el Distrito Capital con Transmilenio: "porque Coviandes, que hace parte de Corficolombiana, en el 94 inició el proyecto de mejoramiento a la vía al llano y fue Coviandes la responsable del manejo de la vía, que el Estado es la que repara, pero la construcción de la infraestructura estuvo en manos de Coviandes".

Por lo tanto, **en términos de infraestructura sí hubo errores**, porque "no se hicieron los estudios técnicos serios para que se hiciera lo necesario para remediar estas situaciones" o preverlas, tal como ocurrió con la construcción del puente de Chirajara. Desde esa perspectiva, habría un director responsable por la ejecución de estos proyectos desde hace 25 años y es Luis Carlos Sarmiento Angulo, como dueño de de Corficolombiana y por ende, de Coviandes.

### **La alternativa del Gobierno: Rutas más largas y en graves condiciones  
**

Espinel criticó que por parte del Estado se han brindado alternativas insuficientes, pues se han limitado a ofrecer vías alternas "que están a punto de colapsar y se han convertido en trochas"; razón por la que pidió una intervención seria del Estado en el asunto, establecer responsabilidades ante el "evidente mal manejo" que se dió a la vía, y una penalidad por los efectos que ha generado el cierre hasta el momento. (Le puede interesar: ["Escandalo Odebrecht salpica a familia Sarmiento y al Banco de Bogotá"](https://archivo.contagioradio.com/audiencias-de-caso-odebrecht-salpican-al-sarmiento-gutierrez-y-banco-de-bogota/))

> [\#FelizMiércoles](https://twitter.com/hashtag/FelizMi%C3%A9rcoles?src=hash&ref_src=twsrc%5Etfw) Las organizaciones que conformamos la Red Llano y Selva exigimos al gobierno nacional tomar medidas para restablecer la vía Bogotá-Villavicencio y penalizar los malos manejos de Luis Carlos Sarmiento Angulo, a través de su concesionario [@Coviandes](https://twitter.com/Coviandes?ref_src=twsrc%5Etfw) [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) [pic.twitter.com/SbbRM7LuST](https://t.co/SbbRM7LuST)
>
> — Corporación Vínculos (@CVinculos) [12 de junio de 2019](https://twitter.com/CVinculos/status/1138769326792749056?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37586722" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37586722_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
