Title: 'Mi cuerpo dice la verdad' una oportunidad para escuchar a las víctimas de violencia sexual en Colombia
Date: 2019-06-25 17:22
Author: CtgAdm
Category: Memoria, Paz
Tags: comision de la verdad, Encuentros por la verdad, violencia sexual
Slug: mi-cuerpo-dice-la-verdad-una-oportunidad-para-escuchar-a-las-victimas-de-violencia-sexual-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Violencia-sexual.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ComisionVerdadC ] 

Este 26 de junio en la ciudad de Cartagena se llevará a cabo el primer **Encuentro por la Verdad, liderado por la Comisión por el Esclarecimiento de la Verdad**, reconocerá la dignidad de las mujeres y personas LGBTI que han sido víctimas de violencias sexuales en el marco del conflicto.

**La comisionada Alejandra Miller** quien estará presente durante el evento se refirió a la importancia de abordar este tema, que ha sido evadido y silenciado por mucho tiempo en Colombia y en lo trascendental que será escuchar a las víctimas, a los responsables, dar una lectura y un sentido a los hechos ocurridos y avanzar en la construcción de compromiso y  confianza.

### **¿Por qué elegir las violencias sexuales como el primer tema a tratar en estos encuentros por la verdad?** 

Este encuentro precisamente es un encuentro de dignificación de víctimas mujeres y población LGBTI que han vivido las violencias sexuales en el marco del conflicto armado colombiano, **iniciamos por ahí porque sabemos que este  es uno de los crímenes más atroces, negados e invisibilizado de esta guerra y de todas las guerras en el mundo.**

Quisimos iniciar contándole al país lo que pasó con esas víctimas, no solamente su dolor sino también su fuerza, cómo lograron sobreponerse, pero también sus demandas y lo que nos piden como Estado pero también como sociedad, las transformaciones que necesitamos para que estas atrocidades no se repitan. En este encuentro las víctimas contarán sus testimonios a la sociedad colombiana y a la Comisión de la Verdad que harán un reconocimiento de su dignidad.

### **¿Cuál es la expectativa que tienen no solo frente a estos testimonios, sino también frente a la participación de algunos de los responsables de estos delitos quienes también estarán presentes?** 

Lo que buscamos es que la sociedad conozca y reconozca lo que pasó con estas personas y se enteré, porque hay muchas personas que ni se enteraron de que este pasó en la guerra colombiana y en segundo lugar abrir una puerta para que los responsables avancen al reconocimiento de sus responsabilidad, a veces les es más fácil reconocer un homicidio, un secuestro, que la misma violación, entre otras cosas porque es un delito naturalizado.

Creemos que escuchar a las víctimas de violencia sexual en condiciones de dignidad puede ayudar a que nos vayamos de ese lugar de la negación, de que eso no pasó, que es algo privado y que se reconozcan esas responsabilidades como un elemento fundamental de la reparación de las víctimas y avanzar hacia la reconciliación de este país. [(Lea también: Con telares, víctimas de violencia sexual exigieron procesos de reparación a la JEP)](https://archivo.contagioradio.com/con-telares-victimas-de-violencia-sexual-exigieron-procesos-de-reparacion-a-la-jep/)

### **Luego del diálogo para la no repetición sobre los líderes sociales ¿qué cosas quisieran potenciar para este encuentro?** 

Los diálogos para la no repetición tienen una metodología distinta, están programados tres sobre el mismo tema, esperamos profundizar en aquellas inquietudes que permanecen, mientras que el encuentro por la verdad de **'Mi Cuerpo dice la Verdad',** es el reconocimiento, es un proceso mucho más testimonial, de manera mucho más directa.

En todo caso, de ambas formas lo que la Comisión pretender es encontrar la explicación profunda de ambas cosas porque eso hace parte de los procesos explicativos en los que también tenemos que explicar las razones por las que sucedió la guerra en el país y sigue sucediendo en algunas regiones.

> “Les digo a las mujeres que han sido victimizadas que nosotros les creemos y tenemos la decisión de honrarlas”: Francisco De Roux, en la antesala del primer Encuentro por la Verdad [\#MiCuerpoDiceLaVerdad](https://twitter.com/hashtag/MiCuerpoDiceLaVerdad?src=hash&ref_src=twsrc%5Etfw) del 26 de junio a las 9:30 amSiga la transmisión en <https://t.co/8SaFUi9hWN> [pic.twitter.com/trSKdKaqit](https://t.co/trSKdKaqit)— Comisión de la Verdad (@ComisionVerdadC) [25 de junio de 2019](https://twitter.com/ComisionVerdadC/status/1143634710666645505?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Viendo el primer diálogo para lo no repetición con los líderes sociales, la actitud del Gobierno no fue la mejor, se esperaba mucho, como Comisión de la Verdad ¿están teniendo diálogos con otros entes del Estado que vayan logrando contestar esas preguntas?** 

Tenemos que seguir avanzando, escuchando a todo el mundo, incluidos todos los actores del Gobierno para ir construyendo este rompecabezas, nuestra obligación es que podamos seguir profundizando incluido el Estado que tiene la responsabilidad de velar por líderes y lideresas.

### **Cartagena fue elegida como escenario de este encuentro por la cantidad de casos que se conocieron en la costa Caribe, ¿Por qué esta región ha sido tan afectada por este tipo de delito?** 

Efectivamente, de las 25.000 personas que están documentadas como víctimas de violencias sexuales en el Registro Único de Víctimas, el 30% se encuentran en la región Caribe, la magnitud de este delito ejercida por los actores armados, especialmente por el paramilitarismo en esta región, fue enorme y eso nos da pie para hacer visible esa particularidad y para que las víctimas sepan que aquí estamos, que la Comisión las escucha y las abraza.

### **Constantemente se habla de víctimas de violencia sexual de las FARC, pero uno se pregunta ¿Qué hay de las demás? ** 

La violencia sexual la ejercieron todos los actores, de manera diferenciada sí, pero todos los actores, **acá vamos a tener víctimas de los paramilitares, víctimas de las guerillas, víctimas de la Fuerza Pública y hasta de terceros responsables**, creemos que evidenciar ese abanico de responsables es parte de nuestra responsabilidad.

Las víctimas van a contar las distintas modalidades que se ejercieron y vamos a escuchar qué fue lo que pasó. Un mensaje importante es que las víctimas en La Habana integraron la inclusión de las violencias sexuales en el sistema de verdad justicia y reparación porque consideraron que era una manera de garantizar sus derechos.

Quiero también dejar el mensaje que nos han planteado las víctimas a lo largo del proceso de construcción de este evento y lo último que las víctimas mencionan son palabras de venganza, no, ellas buscan con ahinco la verdad, que podamos sanar, dar pasos adelante en la transformación en este país.

<iframe id="audio_37584635" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37584635_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
