Title: Fuerza Pública y exparamilitares relatan cómo concertaban desapariciones forzadas
Date: 2019-09-24 18:39
Author: CtgAdm
Category: DDHH, Nacional
Tags: AUC, comision de la verdad, Desaparición forzada, FARC, Fuerza Pública
Slug: fuerza-publica-exparamilitares-concertaban-desapariciones-comision-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Desapariciones-HH.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad  
] 

La Comisión de la Verdad dio a conocer el testimonio de actores armados que reconocen su responsabilidad en hechos de desapariciónes forzadas. Dentro de las testimonios se encuentran los de **José Benítez Ramírez**, excomandante del Bloque Sur de las FARC-EP, el excombatiente de las AUC, **José Ever Veloza**, conocido como  alias "HH" y  el mayor del Ejército, **Gustavo Enrique Soto Bracamonte** quien describió algunos de los métodos usados para impedir que se identificara y encontrara a las víctimas.

Ante la responsabilidad de agentes del Estado. el comisionado Carlos Berinstain, afirma que los mayores obstáculos para el aporte a la verdad han sido **un pacto de silencio entre los responsables** y una **práctica de insensibilidad** que ha mantenido estos hechos en secreto.

Ejemplo de ello, es el testimonio de Soto Bracamonte, quien fue comandante del Gaula en Casanare de 2006 a 2007,  afirma que durante el tiempo que estuvo a cargo, permitió que se se asesinaran personas y que se desaparecieran también los documentos para hacer más difícil el proceso de identificación, en ese sentido, asegura que el aporte que puede hacer a los mecanismos del Sistema Integral será muy bien valorado sobretodo para las familias de las víctimas que son el centro.

"Por las estadísticas que se manejaban en el Ejército, siempre intente estar en el top 10 de las unidades y esto generó muchos problemas al pueblo colombiano y en particular a las víctimas", expresó. [(Lea también: Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos)](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/)

### Un aporte a la verdad de cara al esclarecimiento de las desapariciones

Por su parte, el comisionado Alejandro Valencia afirmó que algunos de los criterios de selección para realizar estos testimonios sobre desapariciones, fueron la forma voluntaria en que fueron entregados y el alto nivel de responsabilidad de los autores de  las desapariciones. [(Le puede interesar: "Queremos saber quién ordenó los Falsos positivos" Madres de Soacha)](https://archivo.contagioradio.com/queremos-saber-quien-ordeno-los-mal-llamados-falsos-positivos-madres-de-suacha/)

Jose Éver Veloza, comandante paramilitar del Bloque Calima y Bananeros, que operó en el Urabá, afirma que al principio los asesinatos que cometían sucedían en las veredas y en los pueblos sin embargo la Fuerza Pública les pidió "que no dejáramos los muertos en la carretera sino que los desapareciéramos porque eso les subía los índices de homicidio y los perjudicaba", rememora.

La comisionada Alejandra Miller afirma que en gran parte estas confesiones se han logrado gracias a la incidencia realizada por las buscadoras, mujeres que han sobrevivido al conflicto y que continúan buscando a sus familiares, **"yo vi que las madres llegaban al Caguán a buscar a sus seres queridos, ahí sentí que era necesario empezar a dar una respuesta",** afirma José Benítez, quien viene trabajando con la Unidad de Búsqueda de Personas dadas por Desaparecidas.

Con estos testimonios,  **Benítez Ramírez, Ever Veloza y Soto Bracamonte **reafirman el tejido de confianza que se ha establecido con la Comisión de la Verdad y su compromiso con esclarecer los sucesos ocurridos durante el conflicto armado.

<iframe src="https://drive.google.com/file/d/1yY6sdKJTZep5FeBavahY5CmDE7KrsDaY/preview" width="640" height="480"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
