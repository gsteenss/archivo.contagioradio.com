Title: Declaran crimen de lesa humanidad caso de Mario Calderón y Elsa Alvarado
Date: 2017-05-12 12:39
Category: DDHH, Nacional
Tags: Crimenes Lesa Humanidd, Defensores Derechos Humanos, Elsa Alvarado, Fiscalía, Mario Calderon
Slug: declarado-crimen-de-lesa-humanidad-caso-de-mario-calderon-y-elsa-alvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/27e98eccacc65c98966c4c9f956811d3_1463713816-e1494594662808.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [12 Mayo 2017] 

La Fiscalía General de la Nación **declaró crimen de lesa humanidad el asesinato de los investigadores y defensores de derechos humanos Mario Calderón y Elsa Alvarado.** La decisión se da a tan solo una semana de cumplirse 20 años de este crimen, cometido por grupos paramilitares y con la complicidad de estructuras militares, según lo avanzado hasta ahora en el proceso.

A través de un comunicado la Comisión Colombiana de Juristas, organización que lleva el caso, se conoció esta decisión en la que la Fiscalía declara: **“los delitos fueron cometidos dentro de un plan sistemático y generalizado contra la población civil”.** Por lo tanto, reitera y ratifica la calificación hecha en la resolución de situación jurídica del actualmente investigado Coronel en retiro Jorge Eliécer Plazas Acevedo. Le puede interesar: ["Defensores de derechos humanos contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

Según Sofia Zambrano, abogada de la Comisión Colombiana de Juristas, la declaración de la Fiscalía **“abre un espacio para que se esclarezca la verdad y se haga justicia en estos casos”,** sin embargo resaltó que a pesar del paso del tiempo solamente hay una persona condenada y un militar vinculado al proceso y el lider paramilitar alias "Don Berna".

En los 20 años que llevan las investigaciones de estos crímenes, **sólo hay una persona condenada como responsable** y por la inactividad del Estado y la justicia, no se ha podido determinar ningún otro responsable. Zambrano aseguró que “en este momento el Estado debe comprometerse a investigar, juzgar y sancionar”. Le puede interesar: ["Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/)

En cuanto a los familiares de Mario Calderón y Elsa Alvarado, **llevan 20 años exigiendo justicia y reparación.** La abogada Zambrano, afirmó que “no han sido atendidos por el Estado, pero han tomado la decisión con esperanza”.

### **El crímen de Mario Calderón y Elsa Alvarado** 

**El crimen sucedió en la madrugada del 19 de mayo de 1997 en Bogotá** cuando paramilitares fuertemente armados, se hicieron pasar por agentes del Cuerpo Técnico de Investigación de la Fiscalía, irrumpieron en el apartamento de los defensores. A la masacre sobrevivieron la madre de Elsa, Elvira Chacón e Iván, de 18 meses, a quién Elsa logró salvar porque lo escondió en el closet.

La madre de Elsa murió sin ningún tipo de atención del Estado colombiano, Sin embargo el hijo de la pareja sigue exigiendo, no solamente que se establezacan las responsabilidades de todos los implicados, sino que se garantice que ese tipo de hechos no se repitan con defensores y defensoras de DDHH o líderes sociales.

**La Fiscalía estableció que no se trató de un crimen aislado sino que fue planeado y sistemático.** La determinación del ente fiscal también declara el asesinato del padre de la defensora Carlos Alvarado como crimen de lesa humanidad.

<iframe id="audio_18655667" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18655667_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
