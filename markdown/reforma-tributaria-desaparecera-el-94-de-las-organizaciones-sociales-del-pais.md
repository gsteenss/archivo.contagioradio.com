Title: Reforma tributaria desaparecerá el 94% de las organizaciones sociales del país
Date: 2016-05-28 17:41
Category: Economía, Hablemos alguito
Tags: déficit fiscal, Impuestos, Organizaciones sociales, Reforma tributaria
Slug: reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reforma-tributaria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: acuda] 

###### [02 May 2016]

El gobierno presentará en el segundo semestre del año una reforma tributaria estructural para que sea tramitada en el congreso. En ella se plantea que se debe **resolver del déficit fiscal que supera los 6 billones de pesos**. Sin embargo no se tiene en cuenta que los pequeños contribuyentes hasta el momento asumen el 93% de la carga fiscal, mientras que la gran empresa nacional y multinacional solamente asume el 7%.

La reforma plantea que se debe aumentar el Impuesto al Valor Agregado (IVA) del 16% al 19% así como ampliar la base gravable y **extender el cobro del impuesto a los productos de la canasta básica que hasta ahora no pagan impuesto y gravarlos con una carga del 5%**.

La organización “la voz de la conciencia” ha asumido la tarea de contribuir en la reflexión sobre los riesgos que trae la reforma tributaria para la gran mayoría de las organizaciones sociales que se han registrado ante la Cámara de Comercio como fundaciones, asociaciones, corporaciones entre otras. Según Robinson Devia González y Pacha Canchay, voluntario, pareciera que el gobierno quiere acabar con **la base social en Colombia que debería estar fortalecida de cara a los acuerdos de paz, esa reforma sería para que solamente las grandes fundaciones sean las que administren y reciban los recursos del post conflicto.**

El estudio presentado por la comisión de expertos consultados por el gobierno plantea que todas las organizaciones sociales registradas ante la Cámara de Comercio que tengan ingresos inferiores a los 80 millones de pesos anuales deberán cumplir una serie de requisitos de ley para poder conservar sus estatus en el régimen especial. Uno de los requisitos es contratar a un gerente con todas las prestaciones sociales.

Actualmente en el país se registran más de 59 mil organizaciones registradas, de las cuales desaparecerían el 94% porque no pueden cumplir los requisitos que les impondría la reforma sobre todo si se tiene en cuenta que en promedio esas organizaciones solamente tendrían ingresos por 4’470.000 pesos.

Así las cosas esta reforma debería ser discutida también con la gran mayoría de las organizaciones registradas para lograr un balance de las reformas que se están planteando. Conversamos con “La Voz de la Conciencia” sobre este tema y las propuestas que se están haciendo para provocar una discusión más amplia.

<iframe src="http://co.ivoox.com/es/player_ej_11771702_2_1.html?data=kpakmZabdJOhhpywj5WVaZS1kZmSlaaWfY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiapdTRs4zgwpC%2Fx8vTts7VjLnfy8fZuMLmysaY0tTSqYzZz5Ddx9HNq9PjjMaYzsbXb7DmyMbby5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
