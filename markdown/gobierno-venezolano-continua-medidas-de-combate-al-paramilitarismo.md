Title: Venezuela continúa con las medidas de lucha contra el paramilitarismo
Date: 2015-08-24 16:01
Category: El mundo, Política
Tags: Central de abastos de Cúcuta, Colombia Informa, Cucuta, Medidas contra el paramilitarismo, Nicolas Maduro, Paramilitarismo, Venezuela
Slug: gobierno-venezolano-continua-medidas-de-combate-al-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/frontera_colombia_venezuela_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elnuevoherlad 

<iframe src="http://www.ivoox.com/player_ek_7529293_2_1.html?data=mJqfm5edd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPusbixt%2FczsbSs4zX0NPhy9OJh5SZo6bOjc7RtM3Zzsrb1sbSqNCfzsrRy8nFt4zYxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Remy Rivera, Colombia Informa] 

###### [24 Ago 2015] 

Parte de las medidas de seguridad que el gobierno venezolano ha implementado para combatir el paramilitarismo denunciado en Venezuela, ha sido el cierre extendido hasta nueva orden de la frontera colombo-venezolana en Maracaibo y Cúcuta. Según el gobierno de ese país **se ha logrado desmantelar un centro paramilitar en San Antonio del Táchira.**

Remy Rivera, reportera de Colombia Informa, asegura que la situación de orden social en la frontera es crítica, debido a varios factores. **Hay desabastecimiento de gasolina y alimentos, pues la mayoría de víveres que se venden en la Central de Abastos de Cúcuta** provienen de Venezuela y que la situación se agrava pues el combustible que se vende en esa zona proviene completamente del país vecino.

Otra de las situaciones que agrava la crisis por el cierre de la frontera es que el **72% de los habitantes de Cúcuta viven del contrabando que es controlado por grandes mafias**. Según Rivera la central de abastos de esa ciudad se provee de alimentos provenientes del contrabando y el tráfico ilegal de productos venezolanos.

Por otra parte, la región de frontera en el “Catatumbo” se reportan desplazamientos de comunidades habitantes de Hacari y Playa de Belén, zonas rurales del Catatumbo en las que se presentaron bombardeos que **incendiaron cultivos, en el marco de una operación militar contra el narcotraficante Víctor Ramón Navarro alias “Megateo”**, ante lo que el gobierno colombiano no se ha pronunciado.
