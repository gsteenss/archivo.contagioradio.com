Title: Medios de información ante la verdad y la  justicia
Date: 2016-01-26 11:03
Category: Abilio, Opinion
Tags: Comision historica, medios, medios de comunicación
Slug: medios-de-informacion-ante-la-verdad-y-la-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/medios-de-comunicacion-y-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vice 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - [@[Abiliopena]

###### 26 Ene 2016 

Nos preguntamos si agentes de medios masivos de información implicados de modo indirecto en la confrontación armada, dirán su verdad sobre dicho involucramiento.

[En el Acuerdo sobre  la Víctimas  del conflicto el gobierno nacional y la guerrilla de las Farc-Ep  acordaron   Sistema Integral de Verdad, Justicia,  Reparación y No Repetición en el que se incluyen la ya conocida Comisión para el Esclarecimiento de la Verdad y la Jurisdicción Especial para la Paz. En el mecanismo  es clara la invitación a comparecer ante las instancias acordadas a responsables directos e indirectos de la violencia en nuestro país.]

[En  los  documentos de la Comisión Histórica sobre el Origen del Conflicto y Sus víctimas, diversos autores, con particulares acentos, nombran como responsables de la violencia en nuestro país a las guerrillas, militares y paramilitares, agentes de la Iglesia Católica, políticos, empresarios, así como a  los medios de información.]

[De la gran mayoría de estos responsables, se han adelantado investigaciones que evidencian dicha implicación en el conflicto y, en algunos casos, las entidades de justicia han demostrado su responsabilidad penal. No ocurre lo mismo con la culpa que recae sobre sobre los medios. Poco o nada se ha escrito sobre las actuaciones  de agentes específicos  que apoyaron actuaciones paramilitares.]

[Un debate sobre la responsabilidad de reporteros, periodistas, columnistas se  suscitó luego de la captura en México de  Joaquín "El Chapo” Guzmán, por   la entrevista que le hiciera el actor estadounidense Sean Penn, quien se  encontró con el narcotraficante luego de su última fuga.]

[En  su columna “[Algo va de Penn a Reyes](http://www.semana.com/opinion/articulo/daniel-coronell-algo-va-de-penn-reyes/456769-3)”   de la revista Semana, Daniel Coronell, opina que el reconocido actor tiene una cierta licencia  de estilo para entrevistar al narcotraficante que no tendrían los reporteros. Coronell  describe  cómo él mismo y otros periodistas de Univisión  estaban a  punto de lograr la entrevista, mas se negaron  a realizarla porque el narcotraficante pedía que fueran  sus propias cámaras y no las del medio las que hicieran el registro;  lo que dejaría el material a merced del entrevistado. Ya en otra oportunidad uno de los reporteros de Univisión había rechazado entrevistar al  capo ante su solicitud  de revisarla antes de  su publicación. Concluye Coronel:  “para que una entrevista sea periodística debe garantizar la total independencia del entrevistador frente al entrevistado, sea criminal o no. Lo mismo frente al Chapo Guzmán que a su santidad el papa”.]

[Si medimos con  este rasero las responsabilidades éticas y legales de los medios y de los periodistas, varias personalidades  deberían contar la verdad de sus relación  con criminales que produjeron centenares de víctimas en nuestro país.  Algunos ejemplos.]

[En su momento, el periódico colombiano “El Pasquín”, publicó  información de la memoria usb de Carlos Castaño que entregó otro paramilitar a la Fiscalía. Aparecía  el cruce de mensajes de Castaño con la reconocida periodista corresponsal del Mundo de España, columnista del diario El Tiempo y panelista de Hora 20 de Caracol Radio, Salud Hernández. La prueba  revela una relación cercana y el ocultamiento de información a favor del paramilitar.  Le dice Castaño: "Apreciada Salud. Reciba mi saludo con afecto. Le ruego suavizar mi respuesta a la pregunta sobre cómo financiar las autodefensas abandonando el narcotráfico". La periodista  le respondió:] ["Estimado comandante, comprendo su precisión y así lo haré". La misma periodista estuvo involucrada en  la campaña de desprestigio del DAS contra la Corte Suprema de Justicia, por orden de la Casa de Nariño. Recibió información que luego fue publicada en una de sus [columnas en el diario El Tiempo](http://blog.unpasquin.com/2011/09/problemas-de-salud.html).]

“Un Pasquín”  reveló también   otros correos electrónicos de la memoria  usb de Castaño donde aparecen  involucrados otros periodistas: Astrid Lagarda, asilada en Estados Unidos, habría mediado entre Castaño y los Buitragueños para una venta de armas por suboficiales del ejército; Luis Jaime Acosta, corresponsal de Reuters alaba a dos comandantes paramilitares y queda en espera de “instrucciones” de Castaño; Harriet Yolanda  Hidalgo, entonces jefa de investigación del  programa La Noche de RCN, dirigido por Claudia Gurisatti,   envió[un poema personal a Carlos Castaño](http://blog.unpasquin.com/2011/09/usb-de-castano-la-memoria-perdida.html); Ernesto Yamhure, columnista de El Espectador, modificaba sus columnas a pedido de Carlos Castaño; las revelaciones hicieron que saliera de ese diario nacional.

[Esto son solo algunos casos. Urge adelantar la compilación de hechos  sobre la responsabilidad de los medios de comunicación  en la violencia en Colombia, que sirva como insumo  tanto para la Comisión de esclarecimiento de la Verdad  como para la Jurisdicción Especial de paz acordadas en la Habana. Del conocimiento y reconocimiento de las responsabilidades de los medios de información, de la reparación  y garantías de no repetición,  depende  buena parte de la  posibilidad real de construcción de paz en nuestro país.]
