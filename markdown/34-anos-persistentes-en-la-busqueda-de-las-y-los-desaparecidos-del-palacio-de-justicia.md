Title: 34 años persistentes en la búsqueda de las y los desaparecidos del Palacio de Justicia
Date: 2019-11-07 18:51
Author: CtgAdm
Category: Sin Olvido
Tags: Palacio Justicia, víctimas
Slug: 34-anos-persistentes-en-la-busqueda-de-las-y-los-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0231.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0242.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0258.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0260.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0266.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0272.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0275.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0311.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC0315.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Blanco/Contagio Radio] 

A treinta y cuatro años de la toma y retoma del **Palacio de Justicia**, las 18 familias de las víctimas de desaparición forzada continúan exigiendo respuestas al Estado Colombiano, durante la jornada de conmemoración de este año, las personas se reunieron en la Plaza de Bolívar en donde con imáges y aregas siguen recordando a sus familiares y luchando en contra del olvido.

De acuerdo con Sandra Beltrán, hermana de Bernardo Beltrán, desaparecido del Palacio de Justicia, ya han pasado tres generaciones de familiares que han acogido la búsqueda de las y los desaparecidos y son los nietos los que están recibiendo la misión de mantener viva la memoria de los hechos y la esperanza por saber qué sucedió con sus víctimas los días 6 y 7 de noviembre de 1985 (Le puede interesar: [34 años del Palacio de Justicia: Para nunca olvidar, para no repetir](https://archivo.contagioradio.com/34-anos-del-palacio-de-justicia-para-nunca-olvidar-para-no-repetir/))

\

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
