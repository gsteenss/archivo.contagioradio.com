Title: Rafael Correa en COP21 propone crear un Tribunal de Justicia ambiental
Date: 2015-12-01 11:57
Category: Ambiente, Otra Mirada
Tags: acuerdo de Kioto, Conferencia de las Naciones Unidas sobre Cambio Climático (COP21), COP20 Paris, COP21, Cumbre climática, La conferencia de las partes COP21, Rafael Correa propone crear un Tribunal de Justicia ambiental
Slug: rafael-correa-en-cop21-propone-crear-un-tribunal-de-justicia-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/correa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:eurativ 

###### [30 nov 2015]

En el marco de la cumbre climática COP21 el mandatario ecuatoriano Rafael Correa propone crear un Tribunal de Justicia ambiental para que sean protegidos los derechos de la naturaleza.

**“Nada, planeta entero, escúcheme, nada justifica que tengamos tribunales para proteger inversiones, para pagar deudas financieras, pero que no tengamos tribunales para proteger a la naturaleza y obligar a pagar las deudas ambientales"**, afirmó Correo  durante su intervención en la primera jornada de la Conferencia de las Naciones Unidas sobre Cambio Climático (COP21).

A su vez el presidente ecuatoriano menciona que los países ricos producen 38 veces más dióxido de carbono (CO2) que las naciones pobres. **“La conservación en los países más desposeídos no será posible si ésta no se genera a través de las mejoras en la calidad de vida de su población”. **

Adicionalmente Rafael Correa asegura realizar **“Un acuerdo ante esta emergencia, es hacer vinculante el acuerdo de Kioto y ampliarlo para compensar las emisiones netas evitadas. Pero también existe una deuda ecológica que debe pagarse aunque sobre todo no debe seguir aumentando”**
