Title: Víctimas piden renuncia del Ministro de Defensa por afirmaciones sobre asesinatos
Date: 2017-12-19 13:06
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, asesinatos de líderes sociales, lideres sociales, Ministro de defensa
Slug: victimas-piden-renuncia-del-ministro-de-defensa-por-afirmaciones-sobre-asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/asesinato-de-líderes.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [19 Dic 2017] 

Víctimas del Bajo Atrato pidieron la renuncia del Ministro de Defensa o el relevo en la cartera por parte del presidente en rechazo a las declaraciones de Luis Carlos Villegas, quien en una entrevista indicó que los asesinatos de líderes sociales del país han ocurrido en su mayoría por **líos de faldas, problemas de linderos y de peleas por rentas ilícitas**.

De acuerdo con uno de los líderes del Bajo Atrato Chocoano, lugar en donde en menos de 10 días asesinaron a los líderes Mario Castaño y Hernán Bedoya, las declaraciones son preocupantes en la medida en que **“se están burlando de los asesinados”**. Dijo que las palabras de Villegas “son una burla y una revictimización a las personas que han perdido la vida por una lucha”.

Además, indicó que el país “siempre ha maquillado las cosas, siempre se cambia la realidad a través de los medios de comunicación y terminan las causas perdidas”. Por esto, le exigió al presidente que destituya de su cargo al Ministro o que le pida que se retracte **“de las mentiras que está diciendo** porque es una injusticia contra la memoria de las víctimas”. (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos de líderes sociales": líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

### **Estado ha permitido que sucedan asesinatos y problemas de linderos** 

El líder manifestó que, si ha habido problemas de linderos, es porque **el Estado lo ha permitido**. Esto, teniendo en cuenta que “el Estado permitió que los empresarios tomaran las tierras de los campesinos”. Además, argumentó que el mismo Estado, a través de Ministro de Defensa, “cubra los asesinatos diciendo que son líos de faldas”.

Adicional a esto, el Ministro indicó que el 62% de los asesinatos de líderes se han esclarecido, cifra que **desmienten los líderes sociales** quienes en reiteradas ocasiones le han pedido a la Fiscalía General de la Nación que agilice las investigaciones y brinde respuestas oportunas. Por esto, el líder indicó que “los asesinatos están claros, sí sucedieron y está claro que el Estado no ha dado resultados”.

Si se trata de argumentar que ha habido resultados en las investigaciones, los defensores le piden al Gobierno que “muestre resultados, **que muestre a las personas que están detrás de los asesinos**”. Ellos dicen que no basta con mostrar al gatillero que disparó la pistola, sino que debe haber pruebas contundentes de los actores intelectuales “sin inventarse pruebas”. (Le puede interesar: ["MinDefensa está desactualizado de información": Gustavo Gallón"](https://archivo.contagioradio.com/min-defensa-esta-desactualizado-de-informacion-gustavo-gallon/))

El líder se refirió al proceso de investigación de Argenito Diaz, que fue asesinado en 2010 en Llano Rico. Dijo que aún las instituciones **no han judicializado a los asesinos** y los perpetradores y “nosotros los conocemos, sabemos quiénes son y ahí están paseándose por entre la Fuerza Pública todos los santos días”.

### **Líderes sociales se han reunido con diferentes instituciones estatales** 

Teniendo en cuenta el aumento significativo en los asesinatos de diferentes líderes, ellos y ellas se han reunido en Bogotá con diferentes instituciones del Estado para **pedir garantías de seguridad** y respuestas concretas para evitar que sigan ocurriendo estos hechos. El líder afirmó que la reunión con el Procurador General, Fernando Carrillo, fue productiva debido a que les brindó un respaldo importante.

Por parte del Ministerio del Interior, siguen esperando que se establezcan soluciones de fondo **“porque con una reunión no se solucionan los problemas**”.  También está esperando que el Ministerio de Defensa tome acciones para evitar que sigan ocurriendo asesinatos pues “es lamentable que, estando nosotros en Bogotá, sigan ocurriendo amenazas contra nosotros y contra los que se encuentran en la comunidad”.

Finalmente, los líderes se reunieron con el Defensor del Pueblo, Carlos Alfonso Negret, a quien le han pedido que **“garantice los derechos de los ciudadanos** como lo hizo el Procurador”. También le pidieron que los acompañe en las comunidades para que sea garante del cumplimiento de los acuerdos con el Ministerio de Defensa y del Interior. Ellos esperan poder volver a su territorio con delegados de la Defensoría del Pueblo para que “constante si se han desplegado los mecanismos de seguridad”.

<iframe id="audio_22739489" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22739489_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
