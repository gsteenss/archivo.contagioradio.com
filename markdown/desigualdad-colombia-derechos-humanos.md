Title: Colombia: el país más peligroso para defender los DDHH en América Latina
Date: 2017-03-26 07:09
Category: DDHH, Entrevistas
Tags: Christian Aid, desigualdad, mujeres
Slug: desigualdad-colombia-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_171.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: christian aid] 

###### [24 Mar 2017]

El más reciente informe sobre desigualdad en América Latina y el Caribe, ubica a Colombia como el país más peligroso de la región para los defensores y defensoras de Derechos Humanos. El documento elaborado desde 2012 por investigadores e integrantes de la organización Christian Aid, señala que Colombia sería más peligros que Brasil y algunos países de Centro América como Honduras, México y Guatemala.

El informe “el escándalo de la desigualdad” cuenta con 6 capítulos que incluyen como un eje central la desigualdad de género, que según Mara Luz, directora regional de la organización y Sophia Richmodn, oficial de incidencia, es la más grave de las desigualdades en todo el continente. Además resalta que ser mujer es un agravante cuando se habla de desigualdad y mucho más cuando se establecen comparativos entre mujer, mujer rural y mujer urbana entre otros. [Consulte el informe completo](https://es.scribd.com/document/342956343/Desigualdad-Informe-Christian-Aid).

### **Algunas de las cifras sobre violencia y desigualdad en América Latina** 

En cuanto a la violencia que genera desigualdad la investigación ubica a Latinoamérica como uno de los lugares más peligrosos puesto que vive el 8% de la población mundial pero presenta un índice de muertes violentas del 33%. Como agravante en Colombia se reportó que el 37,4% de las mujeres entre 15 y 49 años han sido víctimas de algún tipo de violencia, el 9% fueron víctimas de violencia sexual y el índice de violencia doméstica alcanza un 36%.

\[caption id="attachment\_38331" align="aligncenter" width="333"\]![de christian aid](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/desigualdad-christian-aid.png){.wp-image-38331 width="333" height="222"} Desigualdad Laboral\[/caption\]

Otro de los casos alarmantes es el de Brasil que presenta que en 2013 un 10% de las mujeres asesinadas son blancas mientras que el 54% son negras, indígenas o mestizas. Además en El Salvador la cifra de mujeres asesinadas es escandalosa puesto que asciende a 14 por cada 100.000 habitantes.

### **Alfabetización y desigualdad, una alianza vergonzosa** 

El informe señala que en Nicaragua solamente el 57% de las mujeres son alfabetizadas y que en el Salvador el analfabetismo en mujeres rurales alcanza el 37,5%, en Guatemala el 65,9% y en Bolivia el 45,8% lo que indica que el acceso a empleos formales, a cargos políticos u otros es mucho más difícil para las mujeres rurales que para quienes habitan las ciudades.

### **Cambio climático y desigualdad** 

Para 2050, el Cambio Climático tendrá un costo de 100.000 millones de dólares, esto si se tiene que cuenta que el gasto social no está siendo orientado hacia el acceso a energías bajas o libres de carbono, incluso, en muchos casos, lo que está sucediendo es que se siguen brindando beneficios fiscales a empresas generadoras de altos grados de contaminación como las de combustibles fósiles

“En toda América Latina y el Caribe hay un enorme potencial de energías renovables que está casi intacto. Sin embargo, para tener alguna oportunidad de alcanzar el objetivo de los 1,5º C, los países tienen que hacer la gran transición a las energías renovables y acabar con las inversiones y los subsidios a los combustibles fósiles.”

### **A manera de recomendación y sugerencias** 

Christian Aid afirma que aunque es posible que se reduzca la brecha de la desigualdad es necesario aplicar medidas de manera urgente. La organización tuvo cuidado en señalar varias recomendaciones en los 6 aspectos del informe y además resaltó que muchas de las esperanzas están centradas en las acciones a pequeña escala que desarrollan comunidades locales en varias partes del mundo.

[infografia](https://www.scribd.com/document/342956148/infografia-v10-sinMargenes#from_embed "View infografia_v10_sinMargenes on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_91305" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342956148/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-nhxFSkigSigoOOdWtS3I&amp;show_recommendations=true" width="80%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7066666666666667"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
