Title: Plan de Sustitución de cultivos de uso ilícito debe ser integral o no durará: COCCAM
Date: 2017-07-07 14:57
Category: Nacional, Paz
Tags: Erradicación Forzada, Sustitución de cultivos de uso ilícito
Slug: sustitucion-cultivos-ilicitos-cocam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/20070519manoserradicando2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo Particular] 

###### [07 Jul 2017] 

Durante estos primeros 6 meses del año la implementación del punto 4 de los Acuerdos de paz, sobre sustitución de cultivos de uso ilícito, ha dejado como balance **500 intentos de erradicación forzada por parte de Fuerzas Militares frustrados, mientras que 3000 campesinos acusados de narcotráfico continúan en las cárceles** del país, sin que se le dé trámite a su situación judicial.

De acuerdo con César Jerez, vocero de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana, el proceso de sustitución aún no ha iniciado debido a la falta de preparación del Estado y de las interpretaciones que se le han dado al Acuerdo de Paz, en donde el **gobierno sigue haciendo énfasis en el plan de Atención Inmediata y no en la firma de planes Integrales de Sustitución y Desarrollo Alternativos.**

### **No hay un censo de hectáreas ni de familias de que viven por el cultivo de la Coca** 

Sumado a ello estarían las presiones, que, según Jerez, realizan diferentes sectores internos y externos del país “**el gobierno atiende a una fuerte presión de las cifras, del Departamento de Estado de los Estados Unidos** y una fuerte presión de actores internos que quieren retornar a las fumigaciones aéreas y arreciar en las erradicaciones manuales, obviamente con intereses económicos y políticos de por medio”.

De igual forma Jerez señaló que las organizaciones campesinas han manifestado que estos planes deberían desarrollarse en los lugares con **mayor cantidad de cultivos ilícitos como lo son el Nudo de Paramillo, el Catatumbo, Nariño, Guaviare y la Amazonía**. (Le puede interesar:["9 recomendaciones para el gobierno sobre planes de sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/9-recomendaciones-para-el-gobierno-sobre-planes-de-sustitucion-de-cultivos-ilicitos/))

Adicionalmente el lider de ANZORC y COCAM afirmó que para aplicar planes integrales realmente efectivos es necesario tener información precisa en torno a la cantidad de **hectáreas de Coca sembrada que hay en el país, así como la cantidad de personas y la composición socioeconómica** de las familias que sobreviven con los cultivos de uso ilícito.

### **Los planes de sustitución de las comunidades** 

Para César Jerez actualmente hay dos tipos de enfoques de sustitución, el del gobierno que atienden a las cifras y el que han construido las comunidades en su territorio a partir de sus necesidades. Sin embargo, **para que los planes de sustitución sean efectivos Jerez afirmó que esto depende de la calidad de acuerdos que firmen los campesinos**.

“Sí las comunidades firman exclusivamente el Plan de Atención inmediata puede ser un fracaso porque este plan no soluciona el problema de la sustitución estructural e incluso del ingreso, acá lo que se requiere es implementar a cabalidad del acuerdo con políticas públicas e inversión social” expreso Jerez. (Le puede interesar: ["Mujeres cocaleras entregan 14 puntos mínimos para sustitución de cultivos ilícitos"](https://archivo.contagioradio.com/mujeres-cocaleras-entregan-14-puntos-minimos-para-sustitucion-de-cultivo-de-coca/))

<iframe id="audio_19683647" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19683647_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
