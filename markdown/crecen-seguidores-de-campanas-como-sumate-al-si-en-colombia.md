Title: Miles de personas se siguen sumando al SI
Date: 2016-09-12 15:20
Category: Nacional, Paz
Tags: Común Acuerdo, FARC, Juan Manuel Santos, plebiscito por la paz, Viva la ciudadanía
Slug: crecen-seguidores-de-campanas-como-sumate-al-si-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/sumate-al-si-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: reporterosasociados] 

###### [12 Sep 2016]

A 20 días de la votación del plebiscito la intensión de voto de los colombianos crece a favor del SI, según la última encuesta de IPSOS el 72% de las personas encuestadas aprobaría el SI el próximo 2 de Octubre. Al lado de ello las **campañas que impulsan el SI van ganando adeptos**, sobre todo porque estas se han basado en argumentos tan fuertes como el que prioriza la vida por encima de la muerte y la paz por encima de la guerra.

Súmate al SÍ es la campaña que junto a La Paz SÍ es contigo han logrado convencer a muchos escépticos y escépticas. Argumentos como que[el gasto en guerra ha sido 179 mil millones de dólares en 50 años de guerra](https://archivo.contagioradio.com/durante-los-ultimos-52-anos-colombia-ha-invertido-179-000-millones-de-dolares-en-la-guerra/) y que **solamente serían 320 mil millones de pesos para desmotar a las FARC como grupo armado,** siguen consiguiendo adeptos que se ven reflejados en casi todas las encuestas que se han revelado hasta el momento.

Otro de los argumentos que encuentra bastante fuerza, cunado de decidir entre el SÍ y el NO se trata, es en torno al perdón de las FARC. Mientras que algunos detractores afirman que no creen en el perdón de parte de la guerrilla, varios hechos como el **perdón a las familias de Bojayá, a los familiares de los diputados del Valle y las víctimas de la Masacre de la Chinita**, dan cuenta de que por lo menos hay que darle a la guerrilla la oportunidad de pedir perdón “*si alguien quiere hacerlo, por lo menos hay que dejar que lo haga*” afirma la campaña.

La página comunacuerdo.com también explica, de manera muy clara, el contenido de los acuerdos y deja, casi sin piso, a [quienes afirman que decir S](https://archivo.contagioradio.com/el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz/)Í es “entregarle el país a las FARC”, por ejemplo en el **punto de Reforma Agraria**, queda estipulado que lo que se tratará de hacer es permitir a los campesinos y campesinas que se les titulen las tierras y que sus productos tengan un acceso que les permita definir unos precios más justos.

En resumidas cuentas, definir el sentido del voto para el [plebiscito que se realizará el próximo 2 de Octubre](https://archivo.contagioradio.com/los-plebiscitos-de-1957-y-2016-1er-parte/) se ha convertido en una de las decisiones más importantes y en las que se ha visto involucradas la gran mayoría de las personas en el país, ni siquiera una campaña presidencial ha tenido tanto debate y tan profundo como esta campaña. Yiya Gómez, una de las voceras de Súmate al SI, **insiste en que es mejor votar bien informado que con la sola evaluación de lo que se dice en los medios masivos.**
