Title: Organizaciones nacionales e internacionales piden a la Corte Constitucional que no limite consultas populares
Date: 2017-10-06 17:35
Category: Ambiente, Nacional
Tags: consultas populares, Corte Constitucional, Ministerio de Ambiente
Slug: organizaciones-nacionales-e-internacionales-piden-a-la-corte-constitucional-que-no-limite-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo ] 

###### [7 Oct 2017] 

[14 organizaciones internacionales, 112 nacionales y 6 grupos de investigación rechazaron la posibilidad de que la Corte Constitucional limite los alcances de las consultas populares, luego de aceptar la revisión de una tutela de la empresa Mansarovar Energy contra la consulta que se realizó el pasado 4 de junio Cumaral, Meta.]

Así lo establecen mediante una carta dirigida a la Corte Constitucional. La tutela que interpuso la empresa busca “establecer los límites y el alcance de las consultas populares a nivel territorial”. De acuerdo con las organizaciones que firman el documento, se "**estaría  poniendo de presente un sesgo hacia una decisión que aún no ha surtido el procedimiento correspondiente".**

El documento está firmado por personas como el exministro de Medio Ambiente, Manuel Rodríguez Becerra; el exMinistro de Minas y Energía, Jorge Eduardo Cook; el exdirector del IDEAM, Pablo Leyva; y el expresidente de la Asamblea Constituyente y exMinistro de Energía y Minas de Ecuador, Alberto Acosta, quienes  solicitan que se  respete y respalde  la participación ciudadana en el marco de un Estado social de derecho, como el colombiano.

Asimismo, la carta está firmada por organizaciones internacionales como el M4, la Red de Solidaridad Global Sí a la Vida, No a la Minería (YLNM), el Center For International Environmental Law (CIEL) y Earthworks de Estados Unidos, además de otras organizaciones de Guatemala, Honduras, Salvador, Panamá, México, Suiza y Bélgica.

### **Los argumentos de la carta**

[De acuerdo con el documento, los firmantes consideran que la Corte Constitucional no pude dar pasos hacia atrás en materia de protección ambiental y de las comunidades. Con "la Sentencia T-445 de 2016 se avanzó notablemente en poner de presente el alcance de la autonomía territorial y la forma de materializarla, la grave afectación socio ambiental que genera la minería y la garantía que representan paralos asociados las consultas populares como mecanismo de participación ciudadana,precisando su alcance, finalidad y obligatoriedad".]{.a}

[En esa medida, **esperan que mas bien, la Corte profundice en la salvaguarda de los derechos al ambiente sano, la participación y el acceso a información** en materia ambiental y la autonomía territorial como pilar de la organización del Estado.]{.a}

<iframe id="doc_43246" class="scribd_iframe_embed" title="Carta a La Corte Constitucional " src="https://www.scribd.com/embeds/360713918/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ECNMiZaBtV11JMPYCrT4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
 
