Title: Gobierno Santos tiene desinterés por la vida: Cumbre Agraria
Date: 2017-03-10 12:33
Category: DDHH, Entrevistas
Tags: Cumbre Agraria Campesina Étnica y Popular, Presidente Santos
Slug: gobierno-santos-tiene-desinteres-por-la-vida-cumbre-agraria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Captura-de-pantalla-2016-03-17-a-las-11.34.02-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ONIC] 

###### [10 Mar 2017] 

La Cumbre Agraria Campesina, Étnica y Popular se retiró del edificio del Ministerio del Interior, después de dos días de espera por una reunión con el presidente Santos. Ante esto la Cumbre afirmó que **hay un desinterés por la vida de los defensores de derechos, líderes sociales y las condiciones de los campesinos en el país.**

Marylen Serna, vocera de Cumbre Agraria, afirmó que la respuesta del gobierno consistió en señalar que **“se necesitan puntos que se acomoden más al planteamiento del gobierno y no tanto una agenda de negociación con la Cumbre**”. Pasando por encima de lo ya pactado y de un documento de 8 puntos que se había construido para dialogar con el gobierno. Le puede interesar: ["Después de 3 años de incumplimientos Cumbre Agraria se toma Min Interior"](https://archivo.contagioradio.com/despues-de-3-anos-de-incumplimientos-cumbre-agraria-se-toma-min-interior/)

**“El gobierno no tiene interés en comprometerse con las garantías para los líderes sociales ni en el cumplimiento de los acuerdos con los campesinos”** agregó Serna, quién recordó que estos acuerdos se pactaron desde el 2014, después del paro que logro evidenciar las pésimas condiciones de vida que hay para los campesinos en Colombia.

La Cumbre ha informado que el paso siguiente es mantener los planes que ya han adelantado en las regiones y analizar la posibilidad de **hacer un llamado a la movilización,** esto con la finalidad de defender los acuerdos que ya se han firmado y que de acuerdo con Marylen Serna benefician a miles de personas en el país. Le puede interesar:["Cumbre Agraria suspende conversaciones con el Gobierno"](https://archivo.contagioradio.com/cumbre-agraria-suspende-negociaciones-con-gobierno/)

###### Reciba toda la información de Contagio Radio en [[su correo]
