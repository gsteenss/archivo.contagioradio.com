Title: Rectificación
Date: 2016-09-16 23:14
Category: Nacional
Slug: rectificacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/qfXLEEU1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

16 Sep 2016

Ofrecemos excusas a nuestros y nuestras lectores y lectoras por la información errada ofrecida en nuestra nota sobre un asesinato en Villa Alsacia. Aunque en un primer momento, personas que declararon ser testigos del hecho, relataron que un hombre que gritaba No a la Paz había asesinado a una mujer que se movilizaba en un atomovil, hemos podido comprobar que se trató de un robo.

Ofrecemos a los familiares de la víctima de este lamentable hecho nuestro abrazo de solidaridad y ofrecemos a nuestros oyentes una excusa por la información errada.

Aclaramos que en ningún caso nuestro objetivo es desinformar y por respeto a todos y todas ustedes ofrecemos esta rectificación.

De la misma manera exigimos respeto por nuestra labor y rechazamos los insultos y el lenguaje inadecuado que se ha usado en contra de nosotros y nosotras.
