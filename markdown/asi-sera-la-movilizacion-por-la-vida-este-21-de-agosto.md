Title: Así será la movilización por la vida este 21 de agosto
Date: 2020-08-21 10:50
Author: AdminContagio
Category: Actualidad, Movilización
Tags: jóvenes, Movilización por la Juventud, Movilización social, Nunca más Guerra Para la Juventud
Slug: asi-sera-la-movilizacion-por-la-vida-este-21-de-agosto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Nunca-mas-guerra-para-la-Juventud.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Movilizacion-por-la-juventud.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Foto: Contagio Radio/Carlos Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hoy viernes 21 de agosto desde las 9 de la mañana se adelanta una movilización por la vida, en varias ciudades del país,** cuya consigna principal es: **«Nunca más guerra para la Juventud».** (Lea también: [El terror de las masacres no puede frenar la movilización social en Colombia](https://archivo.contagioradio.com/el-terror-de-las-masacres-no-puede-frenar-la-movilizacion-social-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La movilización por la vida es convocada en rechazo a los recientes actos de violencia hacia los que jóvenes de diversas zonas del país que han sido asesinados, mutilados y desaparecidos. (Lea también: [Marcha por la Dignidad aportó para reactivar la movilización social](https://archivo.contagioradio.com/marcha-por-la-dignidad-aporto-para-reactivar-la-movilizacion-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los convocantes hacen un llamado a la participación de manera presencial con todas medidas de seguridad; y también de forma virtual expresándose en favor del derecho a tener una vida digna y sin miedo; y la exigencia de garantías de protección a la niñez y adolescencia, movilización por la vida de manera virtual que irá acompañada de los hashtag: ***\#QueSerJovenNoNosCuesteLaVida, \#MiGeneraciónMereceCambio  ***y ***\#NuncaMásGuerraParaLaJuventud***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Estos serán los puntos y horarios de encuentro en Bogotá:**

<!-- /wp:paragraph -->

<!-- wp:image {"id":88580,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Nunca-mas-guerra-para-la-Juventud-641x1024.jpg){.wp-image-88580}  

<figcaption>
Foto: Convocantes a la movilización por la juventud

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JUCOBTA/status/1296590186370015233","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JUCOBTA/status/1296590186370015233

</div>

<figcaption>
[JUCO](https://twitter.com/JUCOBTA)

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
