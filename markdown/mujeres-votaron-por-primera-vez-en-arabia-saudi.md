Title: Por primera vez las mujeres votaron en Arabia Saudi
Date: 2015-12-14 15:05
Category: El mundo, Otra Mirada
Tags: 17 mujeres electas en Arabia Saudi, Elecciones en Arabia Saudi, Monarquía en Arabia, mujeres
Slug: mujeres-votaron-por-primera-vez-en-arabia-saudi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Mujeres-eligen-en-Arabia-Saudi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: analítica.com 

###### [14 Dic 2015 ]

[Este fin de semana se llevaron a cabo las primeras elecciones abiertas para [mujeres](https://archivo.contagioradio.com/?s=mujeres) en Arabia Saudita, en las que **al menos 17 de ellas fueron elegidas como representantes de los consejos municipales**, lo que podría calificarse como un hecho histórico teniendo en cuenta las restricciones jurídicas y de derechos para la población femenina en esta nación.]

[De acuerdo con las autoridades electorales de 1.486.477 ciudadanos registrados para votar 130.637 eran mujeres y de los 6.916 candidatos en lista 978 correspondían a la cuota femenina, a quienes les fue **prohibido realizar campañas y acudir por separado a las urnas**. Hasta el momento se desconoce la totalidad de los nombres de las candidatas electas, en la lista figura Salma bint Hzab al Otaibi, quien ocupará el consejo municipal de La Meca.]

[Para algunos analistas la medida **no representa cambios profundos en el sistema político de esta nación árabe** sí se tiene en cuenta que un tercio de los consejos municipales es nombrado por la Casa Real y que además son muy pocas las competencias de estas figuras que no pasan de ratificar leyes emitidas por la monarquía saudita.  ]
