Title: 1000 organizaciones del mundo firman a favor de la paz en Colombia
Date: 2015-11-18 18:34
Category: Paz
Tags: Comunidad Internacional y organizaciones de la Sociedad Civil colombiana, Diálogos de La Habana, ELN, FARC-EP, S.O.S por la Paz
Slug: sociedad-civil-enviara-carta-a-la-guerrilla-de-las-farc-ep
Status: published

###### Foto:deracamandaca 

###### [18 nov 2015]

Este jueves más de 1000** organizaciones de la sociedad civil colombiana y  la comunidad internacional, harán pública la carta que se enviará ese mismo día a las delegaciones del gobierno nacional, de las FARC-EP, en La Habana y a la comandancia del ELN,** con el fin de hacer un llamado frente a la necesidad de que permanezcan los esfuerzos para alcanzar un acuerdo de paz.

La sociedad civil  hace un llamado al gobierno nacional y a la guerrilla de la FARC-EP para que continúen los diálogos que se están adelantando en La Habana, con el fin de proteger los acuerdos evitando que el neoparamilitarismo desestabilice los avances históricos a los que se ha llegado.

**Actualmente el proceso está pasando por un momento de estancamiento en La Habana** y de intensificación de los ataques a la guerrilla en Colombia, lo que ha generado que esa organización insurgente advierta que puede estar en riesgo el cese unilateral al fuego. Del mismo modo se hace un llamando al ELN para que inicie diálogos con el fin de lograr un real cese fuego.

La publicación de la carta se realizará **mañana desde las 7 am, en las instalaciones de la Comisión Intereclesial de Justicia y Paz,** donde se contará con la participación de las diferentes organizaciones sociales e internacionales que se acogen a este llamado.
