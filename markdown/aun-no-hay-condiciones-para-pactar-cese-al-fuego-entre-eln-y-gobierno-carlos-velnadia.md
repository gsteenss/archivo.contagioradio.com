Title: ELN y Gobierno alcanzarían punto de no retorno en Diciembre: Carlos Velandia
Date: 2017-06-07 17:12
Category: Entrevistas, Paz
Tags: ELN, Proceso de paz en Quito
Slug: aun-no-hay-condiciones-para-pactar-cese-al-fuego-entre-eln-y-gobierno-carlos-velnadia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

###### [07 Jun 2017] 

La guerrilla del ELN en su último comunicado hizo un llamado a la delegación del gobierno Nacional para que se pacte el cese al fuego **bilateral antes de la llegada del Papa Francisco a Colombia, sin embargo**, de acuerdo con el gestor de paz, Carlos Velandia, las condiciones que permitirían el cese al fuego aún no están dadas, sin embargo diciembre podría ser el mes para alcanzar el punto de no retorno en los diálogos.

Velandia señaló que en primera medida esta petición por parte del ELN, acelera los tiempos para las conversaciones, ya que la llegada del Papa sería en dos meses, “esto implica que tendría que negociarse con un alto grado del aprovechamiento del esfuerzo de diálogo **y tendría que llegarse a acuerdos muy pronto**, que de igual forma implica una mayor participación, con más delegados e integrantes del ELN”.

De alcanzarse estas condiciones, Velandia manifestó que, para diciembre, el proceso de paz en Quito podría conseguir estar en el punto de no retorno, no obstante, las elecciones para el 2018 podrían ser otro de los inconvenientes que este proceso deba afrontar para llegar a ser una realidad. Le puede interesar: ["ELN y Gobierno logran 3 acuerdos para continuar en la mesa de conversaciones de paz"](https://archivo.contagioradio.com/eln-y-gobierno-logran-3-acuerdos-para-continuar-en-la-mesa-de-conversaciones-de-paz/)

El partido Cambio Rádical ya hizo una solicitud para que su candidato Germán Vargas Lleras, termine el proceso de paz con esta guerrilla. Sin embargo Velandia asegura que en ese escenario no concluiría el proceso de paz, **pero si expone que las reglas bajo las que actualmente se está desarrollando se modificarían y no a favor de la construcción de la paz.**

La movilización social está llamada a defender del proceso de paz en Quito, para que presione a las partes para que agilicen los puntos de participación social **“la sociedad debe coadyuvar a que se produzcan los acuerdos de paz**”. Le puede interesar: ["ELN propone cese bilateral para recibir al Papa Francisco"](https://archivo.contagioradio.com/eln-propone-cese-bilateral-para-recibir-al-papa-francisco/)

En este segundo ciclo de conversaciones se han presentado avances importantes en los diálogos de conversaciones entre el ELN y el gobierno, uno de ellos es la conformación de un equipo de comunicaciones que se encargará de **manejar los canales de información entre la mesa y la opinión pública**, además ya se creo el fondo económico que apoyará el proceso.

<iframe id="audio_19141884" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19141884_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
