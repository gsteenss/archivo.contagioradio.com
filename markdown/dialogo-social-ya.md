Title: Diálogo social ¡Ya!
Date: 2020-06-18 10:49
Author: Antonio Jose Garcia
Category: Nacional, Opinion
Tags: Duque, pandemia
Slug: dialogo-social-ya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Trump-y-Duque.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/duque-covid-19.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Presidencia {#foto-presidencia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gran oportunidad se ha presentado en el mundo para fortalecer los autoritarismos locales y mundiales con la famosa pandemia.  Comienza con el mismísimo presidente Trump, quien hasta donde deja ver se cree el hombre más poderoso del mundo, dando muestras de chauvinismo,  autoritarismo, machismo y racismo vergonzosas. Comienza negando la realidad de la pandemia como riesgo universal, y queriendo imponer su autoridad a los estados y ciudades, con una actitud negacionista de las graves e irreversibles amenazas que se ciernen sobre la humanidad. Niega a pie juntillas la existencia del efecto invernadero y del cambio climático, así como también menosprecia el covid19.  Actitudes que solo se entienden en una persona que mira la realidad a través del cristal descolorido del desprecio  por la vida humana y por los desprotegidos de toda la humanidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que pasa es que en su país la gran mayoría de los desprotegidos son inmigrantes en su generalidad ilegales, provenientes de países y de etnias que poco o nada les gustan a los blancos racistas a los que realmente representa ese presidente. De ahí su afán de construir un muro para contener los ilegales en la frontera con México que de nada le sirvió frente a un microscópico virus. De ahí también la insensible desidia frente a la muerte del afroamericano George Floyd, y su posición de fuerza frente a las protestas que se generaron en su país (y en el mundo entero) como rechazo a ese hecho que demuestra una vez más el racismo que aun campea en los Estados Unidos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero, en gracia de discusión, reconozcámosle a Trump algo de originalidad en su desatinado comportamiento. Ese modelo autócrata dictatorial que lo llevó a la presidencia de su país, más el  maltusianismo que parece gobernar su pensamiento,  pretenden copiarlo otros presidentes y gobiernos, que no tienen el poder ni el respaldo armado y económico que tiene el gringo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por aquí tenemos autoritarismos como el de  Bolsonaro, en Brasil,  digno extremista de derecha que desprecia la vida de sus conciudadanos y se burla de las víctimas de la pandemia y pasa por encima de las medidas preventivas.  También tenemos a Maduro, autoritario en Venezuela, extremista del otro extremo, que maquilla las cifras para minimizar el impacto que ha tenido la pandemia de hambre y miseria, así como la del virus que amenaza a su país.  Al extremo centro, tenemos al presidente Duque en Colombia quien con su figura de [“niño bien](https://youtu.be/zq-_a7GRz9U)”, con entrenada voz meliflua y expresión corporal adiestrada y para hablar mucho sin decir nada, no se para pleitos para ejercer el autoritarismo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muy bien lo ha hecho como presentador en la televisión desde el primer día hasta no sabemos cuál día de la cuarentena o hasta el fin de su gobierno. Hace presencia permanente actualizando las cifras todos los días en la televisión y defendiendo del coronavirus a los “abuelitos” que no quieren ser defendidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las medidas que ha tomado frente a la pandemia han sido muy acertadas en su mayoría, los resultados se ven, al menos hasta ahora, pero hay que reconocer que ha sido motivado mas por salirle adelante a la alcaldesa de Bogotá, otra autoritaria total pero que parece tener más acierto en proponer, ordenar y asumir acciones de gobierno. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muy bien Colombia, dicen los medios nacionales cuando comparan la gestión de Duque con las de Trump, Bolsonaro y Maduro y el impacto que ha tenido el virus en los respectivos países.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero se deja a un lado, se les olvida, la grave problemática social en la que se ha debatido el país en los dos años de su gobierno.  Duque quiere proteger los abuelitos dándoles la casa por clínica, pero también quiere borrar de la memoria que hay una pandemia de asesinos matando a los líderes sociales.  Quiere sacar cuanto antes los centros de cuidados intensivos para los infectados del virus, pero aprovecha para dejar en el tintero  un dialogo social que venía ganando espacios  gracias las protestas masivas y organizadas de un país que demanda reformas urgentes y que está cada día más estancado en sus manos, no precisamente por la pandemia.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Duque manda a su comisionado de paz, - el de la permanente y socarrona sonrisa que lo hace parecer que se burla, que  no cree en lo que dice y hace,-  a dar el mensaje de abrir posibles diálogos para el sometimiento de grupos armados, siempre y cuando el resultado sean desmovilizaciones individuales o sea deserciones.     Se olvida que no hay legislación para ello o que la que hay les da risa a los alzados en armas, y que la credibilidad del gobierno de Iván Duque gobierno en la materia se acabó cuando al terminar los diálogos con el ELN desconoció los protocolos que se establecieron con los países garantes de esos los diálogos, precisamente para el caso en que dichos diálogos se suspendieran o terminaran. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desconoce ramplonamente que dichos compromisos en el derecho internacional son compromisos del Estado Colombiano, suscritos, no con el ELN sino con otros estados que de buena voluntad quisieron facilitar el proceso de paz. De esta forma también, el presidente y su comisionado de paz se pegan un tiro en un pie ellos mismos y se lo pegan también a la credibilidad tanto nacional como internacional de Colombia, violando el sagrado principio fundador de las relaciones internacionales de los estados:  el principio de “Pacta sunt Servanda”.  Los pactos deben ser cumplidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Duque, lo que hace con la mano, lo borra con el codo.   Logra poner un Fiscal General de bolsillo delante de todo el mundo y sin ninguna vergüenza, lo utiliza para generar cortinas de humo investigando y encarcelando gobernadores y exgobernadores, todos presidenciables, con y procesos judiciales traídos de los cabellos en momentos en que la Corte Suprema de Justica tiene que resolver que hace con su tutor, benefactor y consejero, quien avanza un proceso judicial   el que se evidencia la compra de testigos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También tiene el fiscal el “acierto” de promover un proceso penal por una situación contravencional que ameritó una sanción policial contra la alcaldesa de Bogotá y su esposa, dejando en evidencia el animo persecutorio.  Mientras esto pasa, los lideres sociales y los excombatientes siguen muriendo en un exterminio similar al de la Unión patriótica, que ya raya en el genocidio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito: ¿Qué será lo que no le gusta a Duque de la alcaldesa? ¿será que no le gusta que una mujer detente el cargo de mayor poder en Colombia después de la presidencia? ¿Qué represente otra opción política? ¿Qué haga valer su opción de genero casándose con una persona de su mismo género, en contra de los “principios morales” que promueve su partido? ¿o no le gusta qué la alcaldesa haya mostrado mas sentido de la oportunidad, decisión y liderazgo que el de su gobierno frente a la crisis por la pandemia?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 También tuvo el presidente Duque el “acierto“ de aprovechar la pandemia para traer militares norteamericanos al país, en momentos en que dicho país cierra un círculo de sanciones alrededor de la vecina Venezuela.  No tuvo el gesto de pedirle autorización al Congreso para ello, como lo ordena la Constitución nacional, pues afirma haber traído los militares norteamericanos a instruir a los militares colombianos sobre lucha contra el narcotráfico. Dudosa respuesta.  Como si en Colombia no se tuviera la mayor experiencia mundial en dicha lucha, pues aquí fue el primer país del mundo donde se inició; el país del mundo que más costos en vidas humanas ha puesto en dicha insensata guerra y como si en Colombia la guerra contra ese fenómeno no le correspondiera a la policía nacional.  Hubiera quedado mejor decir que trajo a los soldados americanos para que aprendieran de nuestros soldados y policías  sobre como se afronta la lucha contra el narcotráfico. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo en realidad son cortinas de humo que no logran opacar el oscuro paisaje y si generan una gran duda sobre las intenciones que mueven al gobierno. La presencia televisiva del presidente, que en tres meses lo único que ha cambiado son las cifras y la corbata,  bien la puede convertir por resúmenes ejecutivos de 5 o 10 minutos, donde puede decir lo mismo que le ocupa una hora o más todos los días. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que bien pudiera hacer el Presidente Duque es reasumir, reiniciar cuanto antes los diálogos con los diversos sectores de la sociedad civil,  dando continuidad a la expresión de los diversos problemas de carácter estructural distintos a coyuntura de la pandemia la que enfrenta el país y que continúan creciendo, aumentados por esta y otrosproblemas  que ésta ha evidenciado. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 El  acelerado desempleo, la falta de seguridad social, la inseguridad alimentaria o mejor la hambruna que se viene, la deserción académica en todos los niveles, la muy notoria desprotección al campesinado y a los sectores informales así como a los trabajadores independientes,  la pobrísima cobertura de internet y la capacidad de acceso de los sectores populares a los medios de comunicación;  la desarticulación de los sectores productivos y del comercio,  la quiebra de las principales fuentes de empleo, la recesión económica y la acelerada pauperización de la clase media colombiana que prácticamente desapareció en medio de la pandemia, sumado a la lumpenización de los más pobres y desprotegidos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los lideres sociales siguen siendo asesinados día tras día. La inconformidad social sobre el desempeño del gobierno  crece, y ” crecerá  como crecen las sombras cuando el sol declina”. Gran parte de la clase media colombiana, que ingenuamente se sentían ricos y privilegiados hasta principios de marzo, y que respaldaba al gobierno, a mediados de junio de 2020 ya es parte del lumpenproletariado desempleado, desconcertado y  lleno de necesidades. Pronto estarán unidos en las marchas exigiéndole resultados al gobierno.   Ya hay manifestaciones multitudinarias en las principales ciudades, ahora con todos los participantes con la cara tapada, pero seguirán ocurriendo en tanto no haya respuestas claras de parte del gobierno, a las apremiantes necesidades del pueblo colombiano.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Presidente Duque:   así sea con tapabocas y frasco de antibacterial, hay que abrir el dialogo social concreto y con resultados ¡!YA¡¡

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

**Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.**

<!-- /wp:paragraph -->
