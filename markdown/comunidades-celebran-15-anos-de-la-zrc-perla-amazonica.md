Title: Comunidades celebran 15 años de la ZRC Perla Amazónica
Date: 2015-12-28 09:46
Category: Comunidad, Nacional
Tags: 15 años Perla Amazónica, ADISPA, ZRC "Perla Amazónica" Putumayo
Slug: comunidades-celebran-15-anos-de-la-zrc-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/MUESTRA-DE-DANZA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [28 Dic 2015]

Desde tempranas horas del 12 de diciembre, 800 hombres y mujeres, ancianos, jóvenes, niños y niñas, se movilizaron desde diferentes rincones del territorio que comprende el corregimiento Perla Amazónica, ubicado en el municipio de Puerto Asís, Putumayo, para conmemorar los primeros 15 años de su constitución como Zona de Reserva Campesina.

Con el regocijo y entusiasmo que les caracteriza, los campesinos y campesinas caminaron una vez más, esta vez con paso de celebración, por el orgullo de ser defensores de la vida, el territorio y la biodiversidad de la hermosa región Amazónica del Putumayo.

Para los habitantes, representados en la Asociación de Desarrollo integral Sostenible Perla Amazónica (Adispa), han sido quince años de lucha y resistencia, quince años de caminar, disfrutar con las alegrías, sueños y recuerdos, pero también de exigir verdad y justicia en la búsqueda de construir la paz anhelada.

En el encuentro se realizaron diferentes actividades culturales y deportivas siendo la más representativa el reinado ecológico, infantil y juvenil, el cual tiene lugar cada año con el propósito de afianzar la apuesta de la Zona de Reserva Campesina Perla Amazónica por la defensa del territorio, la vida y la biodiversidad.

Las comunidades de las veredas Chufiyá, Camios, Agualongo, San Salvador, La Piña, Bajo Cuembí y Puerto Playa participantes del reinado, representaron mediante la confección de vestidos en material reciclable la importancia de proteger la riqueza natural, como fuente de vida para la población actual y futura.

Para el caso de las participantes del reinado juvenil, en el proceso de muestra del vestido ecológico se hizo la respectiva descripción y se respondieron algunas preguntas relacionadas con la protección del ambiente y el importante papel que desempeñan las comunidades en su defensa. En el reinado infantil, las niñas lucieron su vestido para el público y realizaron una presentación cultural.

El evento contó con 5 jurados, los cuales hacen parte de diferentes organizaciones e instituciones consecuentes con la apuesta política de la ZRC Perla Amazónica, entre ellos la Mesa Regional de Organizaciones Sociales del departamento del Putumayo.

Después de una difícil evaluación, y teniendo en cuenta los parámetros de calificación (vestido, respuesta y barra), el jurado dio su veredicto, sin dejar de reconocer el dedicado trabajo realizado por cada una de las veredas en la confección de los vestidos.

\[caption id="attachment\_18891" align="aligncenter" width="800"\][![REINADO JUVENIL](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/REINADO-JUVENIL.jpg){.wp-image-18891 .size-full width="800" height="450"}](https://archivo.contagioradio.com/comunidades-celebran-15-anos-de-la-zrc-perla-amazonica/reinado-juvenil/) Reinado Juvenil\[/caption\]

[  
](https://archivo.contagioradio.com/comunidades-celebran-15-anos-de-la-zrc-perla-amazonica/reinado-infantil/) En el reinado juvenil, la ganadora fue la señorita Yulitza Melo, proveniente de la vereda Puerto Playa, quien representó en su vestido la riqueza natural que posee la Zona de Reserva Campesina a través del logo de Adispa. El segundo puesto fue para la señorita Carolina Claro, de la vereda los Camios, quien representó las diferentes coberturas vegetales que existen en la zona así como los recursos naturales, mismos que exaltó con su atuendo la señorita Julieth Pérez, de la vereda San Salvador, quien obtuvo el tercer lugar con un vestido confeccionado en su mayoría con bolsa plástica.

En el reinado infantil ganó la representante de la vereda "la Piña", quien en su vestido representó un bello paisaje y dió un mensaje de respeto por el territorio a través de una poesía. El segundo puesto fue para la vereda San Salvador, con un traje muy colorido en forma de mariposa, enviando un mensaje de respeto hacia la mujer por medio de una fono mímica.

\[caption id="attachment\_18890" align="aligncenter" width="800"\]![REINADO INFANTIL](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/REINADO-INFANTIL.jpg){.wp-image-18890 .size-full width="800" height="450"} Reinado Infantil\[/caption\]

Como parte de la integración entre comunidades de las veredas participantes se realizó el encuentro deportivo de futbol, donde el ganador fue la vereda San Salvador. Se realizaron también presentaciones de canto y poesía. En representación de la vereda "Buen Samaritano", se entonaron varias canciones con letra compuesta por el rememorando la historia de la lucha campesina y de las veredas Piña y Agualongo se demostraron las habilidades para la danza principalmente de los niños y niñas.

Al finalizar la conmemoración, y después de participar de un gran día de integración e intercambio de experiencias, las comunidades retornaron a sus veredas para continuar con las diferentes labores que ponen de manifiesto su arraigo y amor por su territorio.

##### Equipo de comunicaciones Adispa - Conpaz
