Title: Recluso fallece en extrañas circunstancias en Cárcel de Valledupar
Date: 2020-12-07 22:13
Author: CtgAdm
Category: Actualidad
Tags: #CárcelValledupar, #LaTramacua, #MovimientoCarcelario, #Recluso
Slug: recluso-fallece-en-extranas-circunstancias-en-carcel-de-valledupar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/tramacua13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El movimiento carcelario de la cárcel de Valledupar denunció la muerte del prisionero **Luis Gómez Bermúdez, quien apareció con una soga en su cuello el pasado 4 de diciembre**. Además señalaron que el INPEC solo realizó el levantamiento del cuerpo hasta el día siguiente sobre 6:30 de la noche.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luis obtuvo como derecho al beneficio administrativo un permiso de 72 horas de salida del centro de reclusión, sin embargo este no llegó a efectuarse. El hombre oriundo de Barranquilla, fue encontrado en la celda que ocupaba, en el pabellón 2. (Le puede interesar: «[Todos tenemos derechos, ellos también](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las violaciones a DD.HH de la Tramacua colombiana
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Históricamente el movimiento carcelario ha denunciado las múltiples violaciones a derechos humanos cometidas en la cárcel de Valledupar. Una de las más importantes **es la ausencia de agua inexistente en todos los pabellones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, la población privada de la libertad denuncia que el suicidio es una problemática habitual en este centro penitenciario. Hecho que sería producto de "los denigrantes operativos, traslados de castigo**, aislamientos, golpizas, gaseadas, la mala alimentación,** la negligencia en la atención médica, la especulación con el servicio telefónico y de expendio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumado a ello se encuentra la reciente imposición del pago del IVA. Impuesto del que las y los reclusos están exentos por ser una población vulnerable que además no tiene los recursos para responder con el pago del mismo. (Le puede interesar: ["Movimiento carcelario denuncia riesgo de contagio de Covid-19 por requisas de INPEC"](https://archivo.contagioradio.com/movimiento-carcelario-denuncia-riesgo-de-contagio-de-covid-19-por-requisas-de-inpec/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En razón a estas problemáticas, el movimiento carcelario exige que se haga la **correspondiente investigación para esclarecer la muerte de Luis Gómez.** Asimismo reiteraron el llamado a las organizaciones de derechos humanos, la Comisión de Derechos Humanos y el Congreso de la República para que velen por la vida de quienes se encuentran en los centros de reclusión del país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
