Title: "Que no se repita": Día por la Dignidad de las víctimas de crímenes de Estado
Date: 2017-03-05 10:11
Category: DDHH, Nacional
Slug: que-no-se-repita-dia-por-la-dignidad-de-las-victimas-de-crimenes-de-estado
Status: published

###### [Foto: MOVICE] 

###### [3 Mar 2017] 

Cada 6 de marzo se conmemora en Colombia el Día por la Dignidad de las Víctimas de Crímenes de Estado, para este año el MOVICE ha planteado que las exigencias centrales son la verdad y la justicia para los casos que están en la impunidad, **el desmonte del paramilitarismo, la implementación del acuerdo de Paz y las conversaciones con ELN.** Doña Gloria Salcedo, madre de Carlos Pedraza, defensor de derechos humanos desaparecido, denunció que en el caso de su hijo no hay verdad y que “el Estado, la justicia, juega con nosotras las víctimas como se les da la gana”.

El MOVICE, señala que durante los últimos años y meses, en particular, ha quedado en evidencia que los paramilitares no se desmovilizaron con el Pacto de Ralito en 2005, pues las denuncias sobre presencia de hombres armados en los territorios, amenazas, hostigamientos, atentados y homicidios **“son una constante en el día a día de las comunidades”, puntualizaron que la paz “sigue siendo un sueño y un anhelo”**, al que muchos colombianos y colombianas ven distante “por la violencia sistemática de estos grupos armados que actúan con total impunidad”.

Por su parte, Gloria Salcedo, manifiesta que se ha sentido re victimizada en varias ocasiones por las instituciones estatales, en primera medida porque después de 2 años de lo sucedido, la Fiscalía no tiene “ningún indicio” de lo sucedido con el defensor, durante el proceso han realizado varios cambios de fiscal encargado del caso y por otra parte indica que **"desde el momento en que apareció el cadáver de mi hijo, todo ha sido manipulado".** ([Le puede interesar: “Presidente debió pedir perdón por víctimas de crímenes de estado” Jaime Caicedo](https://archivo.contagioradio.com/presidente-debio-haber-pedido-perdon-ante-el-pueblo-colombiano-por-victimas-de-crimenes-de-estado-jaime-caicedo/))

Salcedo, resalta que la actual situación de las victimas, quienes no han obtenido verdad y justicia en sus casos, hace confuso el panorama de la Paz, **"yo no entiendo la paz del Gobierno, cuando uno todos los días ve malas noticias, muertes"**, la madre de Carlos Pedraza resalta que hay mucho por trabajar por la Paz de Colombia, inicialmente en las garantías de seguridad y derechos constitucionales, “¿por qué mataron a mi hijo Carlos? ¿Por decir lo que no le gustaba?, así no es".

### **Que no se repita** 

Por otra parte, el MOVICE, organizaciones sociales, de derechos humanos y movimientos políticos y sindicales hacen un llamado a la sociedad colombiana para conmemorar a las víctimas y exigir **que no haya más impunidad, saliendo a las calles de las distintas ciudades del país.**

-   En Bogotá, se dará paso a la construcción de una gran galería de la memoria “que caminará” desde la plaza Eduardo Umaña en la Kr 7ª – Cll.20, a las 4:00pm hasta la Plaza de Bolívar.
-   En Barrancabermeja, habrá un plantón desde las 9:00am en la Plazoleta parqueadero de la Dian,
-   Cali, a la misma hora en la Plaza San Francisco, en Manizales desde las 10:00am ante el Palacio de Justicia,
-   Medellín a las 4:00pm en Plaza de Botero, en Pasto a las 9:00am frente a la Fiscalía de Justicia Transicional,
-   Sincelejo a las 10:00 am en la Plaza Olaya Herrera,
-   Villavicencio a las 10:00am en el Parque de Banderas.
-   Sogamoso, se realizará también un plantón, el día 11 de marzo a las 9:00 am en la Plaza de la Villa.

Por último, los barranquilleros están convocados a marchar el 8 de marzo a las 4:00pm desde el parque Estercita Forero hasta la Plaza de la Paz.

Las organizaciones convocantes, resaltan que es fundamental el compromiso de la sociedad colombiana en la exigencia de garantías de seguridad para los y las defensoras de derechos humanos, líderes y lideresas comunitarias y en la presión al Estado para que de **celeridad y cumplimiento a lo pactado en La Habana y a las conversaciones con el ELN en Quito.**

<iframe id="audio_17376015" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17376015_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
