Title: Desplazamiento forzado de lideresa Jani Silva es negado por la Fiscalía
Date: 2020-08-05 22:06
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Putumayo, Zona de reserva Campesina del Putumayo
Slug: desplazamiento-forzado-de-lideresa-jani-silva-es-negado-por-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Jany-SIlva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Lideresa Jani Silva/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 3 de agosto, **la lideresa Jani Silva,** fue notificada a través de correo electrónico sobre el cierre de la investigación por desplazamiento forzado que adelantaba la Fiscalía 03 Especializada Gaula. **La decisión, denuncian organizaciones sociales, desconoce la condición en la que se encuentra la defensora de DD.HH. **de la Zona de Reserva Campesina Perla Amazónica**** en Putumayo quien no ha podido retornar a su vivienda debido a las constantes intimidaciones en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El desplazamiento forzado de la lideresa y su familia ocurrido en diciembre de 2017 y que era investigado por la Fiscalía fue concluido por "inexistencia de desplazamiento forzado", denuncia la [Comisión de Justicia y Paz;](https://twitter.com/Justiciaypazcol)organización que ha alertado sobre reiteradas **amenazas de muerte, presiones, hostigamientos **que aún hoy mantienen en riesgo la vida e integridad de Jani y su familia.**** [(Se agudizan amenazas contra defensores de DD.HH. en Putumayo durante cuarentena)](https://archivo.contagioradio.com/se-agudizan-amenazas-contra-defensores-de-dd-hh-en-putumayo-durante-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el mes de marzo y hasta inicios del mes de julio, se denunció un nuevo plan de atentado **por parte de la estructura criminal identificada como La Mafia,** en contra la vida de la lideresa que ha promovido la sustitución voluntaria de cultivos de uso ilícito, a pesar de los incumplimientos del Gobierno. [(Le recomendamos leer: Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida)](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización también resalta la difícil situación que se vive en Putumayo, en particular en su zona rural donde ha sido evidenciada la movilidad de estructuras armadas irregulares en el territorio de la Zona de Reseva Campesina Perla Amazónica y sobre el río Putumayo y sus afluentes en medio de operaciones de control de unidades de las Fuerzas Militares incluida la Brigada XXVII de Selva y la Fuerza Naval del Sur.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las razones de su desplazamiento son conocidas por la Fiscalía, para organizaciones sociales que apoyan los procesos y comunidades del territorio, esta decisión "es una demostración más de la incapacidad del Estado para brindar garantías a **los líderes y lideresas sociales en particular quienes habitan en zonas de conflicto armado y defienden la protección ambiental**".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jani Silva está protegida por medidas cautelares

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que Jani Silva está protegida por la medida cautelar 204-17 de la Resultados de búsqueda Resultado web con enlaces de partes del sitio Comisión Interamericana de Derechos Humanos (CIDH), institución que reconoció la circunstancia de gravedad que viven los líderes de la Zona de Reserva Campesina, quienes en medio de la implementación del Acuerdo de Paz, siguen siendo víctimas de amenazas y desplazamiento forzado,

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a las amenazas y móviles en contra de la lideresa, no existen avances en el esclarecimiento de quiénes serían las personas responsables de su desplazamiento y continúas presiones.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
