Title: 3 trabajadores despedidos de ETB permanecen en huelga de hambre y encadenados
Date: 2016-06-28 12:38
Category: Economía, Nacional
Tags: administración Enrique Peñalosa, ATELCA ETB, ETB, privatizacion etb
Slug: 3-trabajadores-despedidos-de-etb-permanecen-en-huelga-de-hambre-y-encadenados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Sindicato-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Atelca ETB ] 

###### [28 Junio 2016]

El pasado jueves mientras se firmaba el [[cese de hostilidades en Cuba](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/)], el actual alcalde de Bogotá, Enrique Peñalosa y Jorge Castellanos, presidente de la ETB, **despidieron sin justificación alguna a más de 30 trabajadores** que llevaban en la empresa por lo menos 18 años y que se habían movilizado en defensa de la compañía y en oposición al intento de privatización, asegura Alejandra Wilches, presidenta de la Asociación Nacional de Técnicos de Telecomunicaciones de la ETB.

En este momento **hay 3 trabajadores que desde el pasado jueves permanecen en huelga de hambre y encadenados a sus puestos de trabajo**, quienes este lunes tuvieron que enfrentar que la presidencia de la ETB les quitara la luz, los trasladara junto con otros 50 empleados a otro piso y los custodiara con por lo menos cinco guardias de seguridad privada, agrega Wilches.

Según Wilches, estos despidos son justificados por la administración con la necesidad de recortar gastos; sin embargo, durante el primer trimestre de este año se han reportado sobrecostos administrativos en más de \$100 mil millones, por decisiones como **contratar a un ex coronel de la Policía por más de \$12 millones al mes**, mientras que a los trabajadores que fueron despedidos les pagaban salarios de \$1.200.000, una nómina que supera el 10% del total de los ingresos de la compañía.

Los recortes financieros no han afectado solamente a los empleados, pues según denuncia Wilches, se ha disminuido la inversión en las áreas de mantenimiento y prestación de los servicios, razón por la que no se explican el motivo real de los sobrecostos, porque además Jorge Castellanos ha **descolgado de la página los reportes financieros oficiales de 2015 y 2014 **y está impidiendo que la empresa brinde los servicios de fibra óptica cuando más del 80% de la ciudad ya está cubierta con 1.389.000 puntos que no requerirían más de \$300 mil para funcionar.

En contraste con estos recortes, se reportan aumentos en las tarifas de las bodegas en las que se guarda material porque en este momento **se han suspendido las conexiones a fibra óptica** y los demás servicios que presta la ETB, lo que para los sindicatos constituye "una política de marchitamiento en cabeza del señor Peñalosa y la persona que delego para administrar la empresa", el señor Jorge Castellanos quien se niega a reunirse con los trabajadores.

Frente a [[esta problemática](https://archivo.contagioradio.com/?s=etb+)], los sindicatos interpusieron una acción de nulidad simple de los artículos de enajenación de acciones, instauraron una **solicitud de investigación penal contra los Concejales y al alcalde Enrique Peñalosa** por las irregularidades cometidas durante la aprobación del Plan de Desarrollo y este martes convocan desde las 5 de la tarde, un cacerolazo en la Plaza Eduardo Umaña en rechazo a los masivos despidos.

<iframe src="http://co.ivoox.com/es/player_ej_12054693_2_1.html?data=kpedl5mafZShhpywj5aWaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs_O0MnWpYzLytHQysrXaZO3jKbBp7GnhY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
