Title: Debate en El Ecléctico: Ganó el No, ¿y ahora?
Date: 2016-10-03 10:58
Category: El Eclectico
Tags: colombia, debate, paz, Plebiscito
Slug: gano-el-no-y-ahora
Status: published

##### Foto: Acento 

##### 3 Oct 2016 

<iframe id="audio_14933597" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14933597_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En Colombia habita la incertidumbre; ni los promotores del Sí ni los del No tenían un discurso preparado para los asombrosos resultados que se dieron este 2 de octubre. Desde las Farc siempre dijeron que su intención de paz no dependía de unos resultados electorales mientras que el Gobierno Nacional viene hablando de las nefastas consecuencias que traería una eventual victoria de la oposición.

Fueron representantes de distintos sectores políticos, como un edil de Usaquén por el Partido Centro Democrático y la Representante a la Cámara, Ángela María Robledo quienes analizaron el porqué y el ahora qué de la coyuntural situación por la que atraviesa el país.

¿Por qué fallaron las encuestas? ¿Cuál fue el papel de la desinformación en los resultados del plebiscito? ¿Qué caminos tiene el país ahora para no desechar del todo los acuerdos? ¿Qué dice el Centro Democrático y los demás promotores del No? Éstas y más respuestas en el debate de hoy en El Ecléctico. Le puede interesar: [Plebiscito como mecanismo de refrendación](https://archivo.contagioradio.com/debate-en-el-eclectico-plebiscito-como-mecanismo-de-refrendacion/).
