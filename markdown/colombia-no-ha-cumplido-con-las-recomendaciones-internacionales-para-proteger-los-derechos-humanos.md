Title: Colombia no ha cumplido con las recomendaciones internacionales para proteger los derechos humanos
Date: 2018-04-04 13:18
Category: DDHH, Nacional
Tags: colombia, colombia se raja en derechos humanos, Derechos Humanos, Estado Colombiano, informe de derechos humanos, Naciones Unidas, Organizaciones sociales
Slug: colombia-no-ha-cumplido-con-las-recomendaciones-internacionales-para-proteger-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensore-de-derechos-humanos-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 Abr 2018] 

Más de 500 organizaciones sociales presentaron el informe de insumo para el **Examen Periódico Universal** al que será sometida Colombia el próximo 10 de mayo en la Organización de las Naciones Unidas. Allí, dichas organizaciones concluyen que “Colombia se raja en derechos humanos”, insistiendo en que el país no ha cumplido con las recomendaciones hechas por otros países en los dos exámenes pasados.

El informe comprende el periodo entre 2013 y 2017 y contiene la sistematización de la información otorgada por diferentes organizaciones **alrededor de 25 temas**. Estos recogen la situación de los derechos humanos en Colombia teniendo en cuenta las afectaciones para las mujeres, los niños y niñas, las personas en condición de discapacidad, los pueblos indígenas, afrodescendientes y la población LGBTI.

### **Acuerdo de paz es positivo, pero ha fallado su implementación** 

Uno de los puntos en los que hace énfasis el informe tiene que ver con la recomendación aceptada en 2013 e **implementada por parte del Estado colombiano** que tiene que ver con la consecución y firma del Acuerdo de Paz entre la hoy exguerrilla de las FARC y el Gobierno Nacional. Esto, teniendo en cuenta que ha habido un alivio humanitario y “se ha evitado la muerte de cerca de 2.300 personas”.

Sin embargo, recalcan que la implementación del mismo “avanza en medio de obstáculos e incumplimientos por parte del Gobierno”. Por ejemplo "**persiste la falta de garantías de seguridad para los excombatientes**”,  el fenómeno del paramilitarismo continúa y no ha sido reconocido por el Estado.  El informe establece que el conflicto armado persiste y “los actores continúan infringiendo el Derecho Internacional Humanitario”. (Le puede interesar:["Hurtan información sensible a tres organizaciones defensoras de derechos humanos en Bogotá"](https://archivo.contagioradio.com/robo_informacion_organizaciones_derechos_humanos/))

### **Recomendaciones incumplidas por Colombia** 

La Federación Rusa, en el examen previo de 2013, recomendó a Colombia garantizar la **integración social de las personas desmovilizadas** de los grupos armados pero el país no cumplió con esta recomendación. Así pues, “para el 2016 los paramilitares estaban organizados en 13 estructuras que actuaban en 344 municipios de los 31 departamentos”.

Dice el informe que “al menos **9.352 de los desmovilizados de las AUC** estarían por fuera de los mecanismos diseñados” para la reincorporación y justicia y “algunos de los reconocidos jefes ex paramilitares de las AUC pasaron a organizar los nuevos grupos paramilitares que se reestructuraron tras el proceso de justicia y paz”.

Adicionalmente, la recomendación de Francia y Suecia para detener las **ejecuciones extrajudiciales** no fue atendida adecuadamente en la medida en que “siguen ocurriendo decenas de casos al año producto de la victimización de los civiles en las operaciones militares y por la represión violenta y militarizada de las acciones de protesta social”.

Las organizaciones sociales expresaron su preocupación en el incumplimiento de las recomendaciones que tienen que ver con la **protección a los líderes sociales**, los dirigentes sindicales y las y los defensores de derechos humanos teniendo en cuenta que los asesinatos han incrementado. A esto se suma la preocupación por las garantías para la ejecución del proceso de restitución de tierras que ha puesto en peligro a quienes lo lideran.

### **¿Porqué Colombia no cumplió con las recomendaciones pasadas?** 

De acuerdo con Luz Estela Aponte, integrante de la Corporación Reiniciar, el Estado colombiano **no tuvo voluntad política** para cumplir las recomendaciones de los diferentes países. Argumentó que los funcionarios del Estado, en su mayoría, “no están enterados y no están capacitados” sobre las recomendaciones de los exámenes previos y esto dificulta su implementación. (Le puede interesar:["Colombia es el segundo país más peligroso para los defensores del ambiente"](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/))

Aponte afirmó también que, ha habido una **falta de recursos** para poder hacer efectivas las recomendaciones poniendo en riesgo la plena satisfacción de los derechos humanos, especialmente de quienes defienden los derechos humanos. Esto se evidencia en que, por ejemplo, la Unidad Nacional de Protección carece de los recursos suficientes para elaborar las acciones de protección.

El Estado tampoco ha puesto en **funcionamiento el aparato institucional** para cumplir con las recomendaciones y esto pone en riesgo temas como la superación de la impunidad. La integrante de Reiniciar, recordó que las recomendaciones frente a este tema han sido reiteradas e “implican para el modelo de justicia diseñar un nuevo modelo de investigación más calificado y que tenga en cuenta la víctima”.

Las organizaciones sociales presentarán este informe como insumo para el tercer documento que será utilizado para realizar el examen en las Naciones Unidas. Ya el Estado colombiano **presentó un informe oficial** sobre la situación de los derechos humanos en el país al igual que fue presentado el informe de los diferentes organismos de la ONU que realizaron pronunciamientos sobre el país. Será tarea del Estado recoger nuevas o las mismas recomendaciones, aceptarlas e implementarlas.

[Copia de Informe SITUACION DERECHOS ESPAÑOL 3 ABRIL 2018](https://www.scribd.com/document/375546147/Copia-de-Informe-SITUACION-DERECHOS-ESPAN-OL-3-ABRIL-2018#from_embed "View Copia de Informe SITUACION DERECHOS ESPAÑOL 3 ABRIL 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_53957" class="scribd_iframe_embed" title="Copia de Informe SITUACION DERECHOS ESPAÑOL 3 ABRIL 2018" src="https://www.scribd.com/embeds/375546147/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Avn7Jbw1CYarWiC04x79&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7069913589945012"></iframe><iframe id="audio_25113301" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25113301_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
