Title: Nuevas exigencias de la Alcaldía de Bogotá acabarían con los mercados campesinos
Date: 2016-05-18 16:08
Category: DDHH, Nacional
Tags: comité interlocución campesino, fao en colombia, mercados campesinos
Slug: administracion-acabara-los-mercados-campesinos-para-hacerlos-tipo-gourmet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/mercados-campesinos-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural  ] 

###### [18 Mayo 2016 ] 

El futuro de los mercados campesinos en la capital colombiana es incierto. Por lo menos 3 años y \$10 millones deberán invertir 2 mil campesinos de Cundinamarca, Tolima, Boyacá y Meta, para certificarse con los registros ICA e INVIMA que exige la administración de Bogotá a quienes quieran hacer parte de los mercados campesinos, que según la licitación de la FAO **ahora estarían en manos de empresarios y pasarían a convertirse en mercados 'tipo gourmet'**.

De acuerdo con esta licitación, el operador logístico para realizar un mercado campesino en la Plaza de Bolívar el próximo 24 de junio, debe presentar certificado de existencia y representación legal, así como haber ejecutado en los últimos cinco años **por lo menos tres contratos de logística para eventos de más de 800 personas, por un valor mínimo de 360 SMMLV**.

El documento también establece que sólo podrán participar 250 expositores, cuando han sido por lo menos 2000 campesinos los que han llegado a la capital para vender sus productos, entre ellos alimentos de la canasta básica familiar, frutos secos, **semillas y amasijos típicos que ahora tendrían que estar debidamente empacados y con registros ICA e INVIMA**.

Para un campesino venir a la ciudad a vender sus productos de calidad y a precios justos, podía significarle un gasto máximo de \$100 mil en transporte, ahora con los **tramites que pueden tardar entre 3 y 7 años deberá gastar entre \$10 y \$20 millones**, para ser certificado y así poder participar de los mercados campesinos.

Estos mercados tienen su amparo legal en la Política de Seguridad Alimentaria 2007-2015 de Bogotá, así como en el 'Plan Maestro de Abastecimiento y Seguridad Alimentaria' y el Acuerdo Distrital 455 de 2010, que buscan desarrollar una estrategia de reconocimiento de la economía campesina, valorando la agricultura familiar y defendiendo el derecho a la alimentación. Por lo tanto, la **administración distrital estaría en la obligación de promover su realización en las localidades capitalinas**.

Según denuncia Julián Corredor, integrante del 'Comité de Interlocución Campesino', pese a esta determinación legal, **la actual administración de la ciudad, en cabeza del Secretario de Desarrollo Económico, Freddy Castro, no ha renovado los convenios**; argumentando insostenibilidad financiera y posibles inconsistencias con las organizaciones firmantes, los campesinos que se benefician y la calidad de los productos que son vendidos.

Sin embargo, de acuerdo con Corredor, los campesinos que hacen parte del programa, son pequeños productores, **dueños o arrendatarios de modestos parcelas, quienes muchas veces deben aportar recursos propios** para traer sus productos a la ciudad y capacitarse en el cultivo y comercialización de sus productos, conforme a los estándares de calidad exigidos.

El pasado 5 de abril los campesinos se movilizaron frente a la Secretaria de Desarrollo Económico para exigir la renovación de contratos y la derogación del convenio propuesto por la RAPE y la FAO; sin embargo, el Secretario Freddy Castro, no les atendió; y pese a que el 29 de abril el alcalde les aseguró que su administración apoyaría la realización de los mercados, en **el artículo 153 del Plan de Desarrollo Distrital contempla la eliminación del acuerdo 455**.

"No sabemos a que está jugando está administración". **Por un lado dice que va a atender a los campesinos, y por otro, busca derogar el acuerdo que da vía a la política pública** de seguridad alimentaria y a los mercados campesinos, de los que se favorecen, no sólo las familias campesinas de la región central, sino los miles de consumidores que encuentran productos orgánicos, saludables y a precios justos, afirma Corredor.

Ante la negativa de la administración, en distintos parques de la localidad de Kennedy los campesinos y las organizaciones que les apoyan, han realizado durante este año mercados esporádicos que han contado con el aporte económico de distintas alcaldías municipales que han firmado **42 acuerdos de economía campesina para la selección y el transporte de los alimentos**.

(Entérese en la siguiente entrevista de cuáles son las apuestas de las comunidades para que no se acaben los mercados campesinos).

<iframe src="http://co.ivoox.com/es/player_ej_11579000_2_1.html?data=kpaimZ6UdJGhhpywj5iUaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncavpzc6SpZiJhZLijKjc1NfJqNDmjJKYpdTRrdWZpJiSo56PqMafqtPhx9fQs8TpxM6SpZiJhpTijKjOz9XJt8ri0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>Vea también: [[Administración distrital pondría fin a los mercados campesinos](https://archivo.contagioradio.com/administracion-distrital-pondria-fin-a-los-mercados-campesinos/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
