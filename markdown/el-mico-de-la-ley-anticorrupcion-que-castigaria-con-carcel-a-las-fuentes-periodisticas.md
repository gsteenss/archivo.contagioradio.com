Title: El mico de la ley anticorrupción que castigaría con cárcel a las fuentes periodísticas
Date: 2019-06-13 16:15
Author: CtgAdm
Category: Entrevistas, Política
Tags: Congreso, FLIP, Libertad de Prensa
Slug: el-mico-de-la-ley-anticorrupcion-que-castigaria-con-carcel-a-las-fuentes-periodisticas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/FUENTES-A-LA-CARCEL-ANTICORRUPCION.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

**El artículo 28 del proyecto de ley anticorrupción,** que se encuentra en último debate en la plenaria del Senado, castigaría de cuatro a ocho años de cárcel, a funcionarios públicos que suministren información de carácter reservado a los medios de comunicación, sin distinguir si hay daño o no con la filtración, una medida que atentaría contra la libertad de prensa y directamente el derecho que tiene el periodismo a la reserva de fuente.

Este es el artículo correspondiente: * “El servidor público que indebidamente dé a conocer documento o noticia que deba mantenerse en secreto o reserva, incurrirá en pena de prisión de cuatro (4) a ocho (8) años, multa de veinte (20) a ciento veinte (120) salario mínimos legales mensuales vigentes e inhabilitación para el ejercicio de funciones públicas por diez (10) años”*

**El director de la Fundación para la Libertad de Prensa (FLIP), Pedro Vaca,** calificó como irónica la aparición de un proyecto de ley que busca luchar contra la corrupción y sin embargo incorpora una medida que la facilite, " pensemos en el funcionario público que ve cómo se roban plata pública y asumamos que tiene los valores democráticos en el lugar correcto y quiere denunciar, con este proyecto lo pueden despedir además debe pagar 20 años de cárcel".

Vaca agregó que se trata de una medida que busca **"sembrar miedo en los funcionarios públicos a tal punto que no hablen"** teniendo como consecuencia, un  impacto no sobre el comunicador que ya está protegido por el secreto profesional periodístico, sino sobre la fuente, que resulta ser un "insumo para informar a la sociedad.

### "Los grandes perjudicados al final  son la ciudadanía y la institucionalidad" 

Vaca destacó el artículo del New York Times que denunció el Ejército, señalando que gracias a la participación y contribución de las fuentes que aportaron esta información, "hoy el Ministerio de Defensa tuvo que corregir una política  que dejaba serias dudas sobre estándares de DD.HH. y posibles ejecucions extrajudiciales".  [(Lea también: Duplicar resultados a toda costa, la orden a militares en el gobierno Duque)](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/)

Asimismo resaltó que pese a ingresar a la **Organización para la Cooperación y el Desarrollo Económicos (OCDE**) una organización que plantea brindar protección a los denunciantes, en Colombia en lugar de  "avanzar en  los estándares estamos cultivando una atmósfera de miedo".

El director de la FLIP resaltó la importancia que el Congreso sea consciente de la responsabilidad que tiene al aprobar un artículo de estas características, e indicó a modo de opinión que este artículo de la ley anticorrupción debería ser eliminado y plantearse desde la Procuraduría la creación de una protección de denunciantes.

<iframe id="audio_37072114" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37072114_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
