Title: "Siempre hemos denunciado que el caso Andino, es un falso positivo" Boris Rojas
Date: 2020-06-10 00:31
Author: CtgAdm
Category: Expreso Libertad, Nacional
Tags: Caso Andino, Falsos Positivos Judiciales
Slug: siempre-hemos-denunciado-que-el-caso-andino-es-un-falso-positivo-boris-rojas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Captura-de-pantalla-2020-06-10-a-las-00.25.36.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Boris Rojas, Camilo Pulido y Andrés Bohórquez, recobran la libertad luego de estar cerca de tres años en prisión por el caso Andino. A la salida de la cárcel La Picota reiteraron que este proceso desde el inicio ha sido un falso positivo judicial. Ver: [Caso Andino, un proceso cargado de injusticias y dilaciones](https://archivo.contagioradio.com/caso-andino-un-proceso-cargado-de-injusticias-y-dilaciones/)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F866594200514666%2F&amp;show_text=true&amp;width=552&amp;height=438&amp;appId" width="552" height="438" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->
