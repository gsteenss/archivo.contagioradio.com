Title: Los plebiscitos de 1957 y 2016: El miedo (Tercera parte)
Date: 2016-09-27 12:27
Category: Cesar, Opinion
Tags: paz, plebiscito por la paz
Slug: los-plebiscitos-de-1957-y-2016-el-miedo-tercera-parte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/plebiscito-1957-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sady Gonzalez ] 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 27 Sep 2016 

[Georges Lefebvre escribió]*[ El gran pánico de 1789. La revolución francesa y los campesinos]*[ (1932), libro clásico desde su publicación.  Una paciente investigación en los Archivos le permitió advertir que el campo tenía miedo a los “bandidos” y a los aristócratas; que los temerosos nobles y burgueses se armaban para detener al]*[Cuarto Estado]*[ y eliminar al plebeyo movimiento revolucionario; que  las “corrientes” del pánico utilizaban el]*[rumor]*[ para propagar la amenaza y que unos y otros actuaban en defensa del fantasma, como lo denominó Lefebvre, algo trágico que acontecería.]

[Como instrumento de control el miedo no es invención de este siglo; como mecanismo de contención hace parte de la]*[resistencia]*[ innata de los vencidos. El miedo acompaña al acontecimiento; sin aquel éste no es histórico. No hay neutralidad en su uso; más bien hay]*[intencionalidad]*[. En el uso del miedo la tragedia siempre es actual, en el sentido de que lo que viene se hará realidad inmediata; así, el miedo condensa como constelación (Benjamin) el pasado de los muertos, el presente de los sobrevivientes y el futuro de quienes serán perdedores o vencedores. El miedo pone en guardia, advierte, neutraliza, amenaza.]

[Pero el miedo es superable. La historia se ha hecho venciéndolo. Decenas de años de imposición autoritaria, de crímenes dictatoriales, de discursos de odio, de gestión y opresión burocrática estalinista, de infamias sin fin se han interrumpido en pocos días. La primavera árabe es apenas el caso más reciente. Desde este punto de vista la historia apenas si ha comenzado.  ]

[Como vimos en [columnas anteriores](https://archivo.contagioradio.com/cesar-torres-del-rio/), en Colombia la década de los cincuenta terminó con el plebiscito del  1° de enero de 1957, que selló la]**impunidad pactada**[ entre las elites políticas liberal-conservadoras e impuso el retardatario modelo bipartidista del Frente Nacional. La clase dirigente, la Junta Militar, El Siglo y El Tiempo, los empresarios, la jerarquía católica, todos a una como en Fuenteovejuna, utilizaron el miedo para promover el voto afirmativo. “Al votar el plebiscito del primero de diciembre dirá usted si está de acuerdo o no con la reforma que devolverá la paz a Colombia”, advertían las páginas del diario conservador un mes antes de la fecha prevista.]

[“La perspectiva del plebiscito, lejos de aplacarlo (el bandolerismo) parece haberlo enardecido”, agregaba el matutino para reforzar la idea del pasado violento que tenía que ser superado por la paz bipartidista. También un mes antes en el  diario liberal El Tiempo, el expresidente Alfonso López Pumarejo señalaba idéntico peligro: “En los Llanos no hay gobierno”. Y en medio de la disputa bipartidista sobre la candidatura única presidencial pos-plebiscito, el Laureanismo atacó el “trecejunismo” liberal que apoyaba a Guillermo León Valencia y pidió aplazar el acto de diciembre “hasta que se logre el restablecimiento de los acuerdos (Benidorm y Sitges)”.  Debemos mencionar así mismo que para evitar que el “caos triunfara” si el No se imponía, el discurso del miedo apeló a la mujer y la convocó a votar por primera vez.]

\[caption id="attachment\_29876" align="alignleft" width="285"\]![plebiscito-1957](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/plebiscito-19571.jpg){.wp-image-29876 width="285" height="353"}  El Siglo, noviembre 29 de 1957.\[/caption\]

[Y en clave de atentados directos contra la paz, en noviembre los principales diarios de la capital fueron corriendo el]*[rumor]*[ de un movimiento subversivo desestabilizador que buscaba atraer a las Fuerzas Armadas para dividirlas y que planeaba dar un golpe de Estado inmediato. El rumor lo ratificó el estamento castrense mediante comunicado en el que, además, denunciaba a ese movimiento como un “sabotaje contra el plebiscito” y como un intento de restablecimiento del “gobierno anterior”, o sea, el de Rojas Pinilla; incluso señaló que se habían producido capturas (El Siglo, “Vasto plan subversivo”, 18 de noviembre de 1957).]

[Como en la coyuntura de 1957, para el plebiscito del 2 de octubre de 2016 el miedo se desplaza con el]*[rumor]*[ y conscientemente se le utiliza para alienar.  Cuenta ahora con la moderna tecnología del universo digital y se ha fabricado en instituciones del Estado, como la Procuraduría General encabezada por el saliente Alejandro Ordoñez; en el Palacio de Nariño entre 2002 y 2010; y en el recinto del Congreso en los últimos años; calculado para suministrar en dosis mediáticas por algunos medios de comunicación; difundido en los discursos políticos del odio; proyectado según sectores sociales y culturales; “virtualizado” en las redes sociales y “secularizado” por las sectas religiosas. Quienes lo administran constituyen una]*[contracorriente]*[ conservadora, termidoriana y provinciana.]

[En el colmo del paroxismo la]*[contracorriente]*[ ha afirmado que con la firma del acuerdo entre el Estado y las FARC el “terrorismo” ha triunfado, que la Constitución ha sido pisoteada y las garantías individuales menoscabadas, que la propiedad privada está amenazada, que la ideología de género acabará con la familia, que el país se le entregará al “castro-chavismo” para que se implante el socialismo del siglo XXI, que el acuerdo formaliza la impunidad, que las Fuerzas Militares han sido divididas; y no contenta con llamar a votar por el No ha llegado a solicitar el aplazamiento del plebiscito.]

[Pero el miedo es superable. Millones de colombianos votarán por el SÍ este 2 de octubre de 2016. Y desde el 3 del mismo mes millones continuarán movilizándose por sus derechos, por la concreción de los acuerdos, por la verdad histórica, por la reparación y por la no repetición. La]*[contracorriente]*[ no pasará.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
