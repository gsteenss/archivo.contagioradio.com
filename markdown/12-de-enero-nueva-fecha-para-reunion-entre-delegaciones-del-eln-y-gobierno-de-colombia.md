Title: 12 de Enero, nueva fecha para reunión entre delegaciones del ELN y gobierno de Colombia
Date: 2017-01-08 12:53
Category: Nacional, Paz
Slug: 12-de-enero-nueva-fecha-para-reunion-entre-delegaciones-del-eln-y-gobierno-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/161023_notaELN_1800.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República] 

###### [8 Enero 2017] 

De acuerdo con un comunicado de prensa emitido por la delegación del gobierno para los diálogos con el ELN, la reactivación de las conversaciones para concretar la fecha de instalación de la mesa pública, **será el próximo jueves 12 de enero en Quito, Ecuador.**

En el documento también se aclara que los representantes del gobierno en la reunión con los delegados del ELN en Quito serán: **Juan Camilo Restrepo, la exministra Luz Helena Sarmiento, el Mayor General (r) Eduardo Herrera Berbel, Alberto Fergusson, Jaime Avendaño y María Alejandra Villamizar.** Le puede interesar: ["ELN confirma participación en reunión para destrabar el proceso de paz"](https://archivo.contagioradio.com/eln-confirma-participacion-en-reunion-para-destrabar-el-proceso-de-paz/)

Para finalizar en el texto, el gobierno colombiano agradeció la hospitalidad que Ecuador ha tenido para que se gesten allí las conversaciones. Le puede interesar: ["Sin mesa con el ELN no habrá paz completa: Eduardo Celis"](https://archivo.contagioradio.com/sin-mesa-con-el-eln-no-habra-paz-completa-eduardo-celis/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
