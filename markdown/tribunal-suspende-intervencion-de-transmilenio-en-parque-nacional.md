Title: Tribunal suspende intervención de TransMilenio en Parque Nacional
Date: 2019-05-31 16:15
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Alcalde Enrique Peñalosa, Parque Nacional, Transmilenio
Slug: tribunal-suspende-intervencion-de-transmilenio-en-parque-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-117.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El Tribunal Administrativo de Bogotá ordenó ayer la suspensión de cualquier intervención en el Parque Nacional Enrique Olaya Herrera para la construcción del troncal de Transmilenio por la Carrera Séptima, al otorgar medidas cautelares para la protección de esta zona verde de la capital.

Esta acción judicial responde a una solicitud de Karin Irina Kuhfeltdt, abogada del comité Defendamos La Séptima, presentada en abril del 2019. Según la decisión, la Alcaldía de Bogotá y el Instituto de Desarrollo Urbano no podrán proceder con el proyecto "hasta tanto se formule y apruebe el plan director de conformidad con lo establecido en el Artículo 252 del Decreto 190 de 2004 o se proferida la sentencia que ponga fin a la controversia".

“El proyecto de TransMilenio por la Séptima le quita al Parque Nacional alrededor de 4.000 metros cuadrados, y en esa medida el Tribunal Administrativo de Cundinamarca consideró que no se estaba cumpliendo con las normas que establecen cómo se deben intervenir este tipo de bienes de interés cultural”, dijo el ente judicial. (Le puede interesar: "[Ordenan a Peñalosa detener tala de árboles en Parque Japón](https://archivo.contagioradio.com/penalosa-detener-arboricidio-parque-japon/)")

[Herman Martínez, ex-director del Jardín Botánico de Bogotá y vocero del movimiento ambiental Marcha por los Árboles, afirmó que hay palmas patrimoniales ubicadas sobre el sendero del costado oriental de la Carrera Séptima, por el Parque Nacional, que podrían tener 100 años de edad y que serían afectados por la construcción de una estación de Transmilenio. En total, serían alrededor de 40 árboles patrimonales afectados, indicó e]l ex-director del Jardín Botánico.

"Nosotros no nos oponemos al desarrollo de la ciudad, creemos que se pueden encontrar otras alternativas sin necesidad de causar afectaciones sobre monumentos históricos y árboles patrimoniales", manifestó el vocero, indicando que, al respecto de la estación del Museo Nacional, podría ser diseñada subterráneo como la del Museo Nacional para mitigar los impactos en los ecosistemas aledaños.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
