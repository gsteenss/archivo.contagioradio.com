Title: Así será el Paro Nacional a partir de este lunes
Date: 2017-10-20 14:02
Category: Movilización, Nacional
Tags: COCCAM, marcha patriotica, Paro Nacional
Slug: 23-de-octubre-hora-cero-para-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro-civico-marcha-buenaventura-perdidas-actos-vandalicos-22-05-2017-e1496014083254.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [20 Oct 2017] 

De acuerdo con David Flores, vocero de Marcha Patriótica este paro se realizará en la ruralidad del país y será apoyada con acciones en las ciudades **“nos vamos a encontrar en movilizaciones por las diferentes carreteras departamentales del país”**, en ese sentido habrán marchas en el suroccidente, el noroccidente, el Urabá Antioqueño, Norte de Santander y el oriente colombiano.

Una de las reivindicaciones más importantes de este llamado a paro tiene que ver con el incumplimiento del **punto 4 del Acuerdo de Paz sobre sustitución de cultivos ilícito, razón por la cual también se espera que campesinos cultivadores de hoja de coc**a, amapola o marihuana participen de esta movilización e incluso de acuerdo con Flores, puedan convocar a otro paro cocalero como el del 2014.

“Los campesinos y las campesinas no se van a dejar quitar su sustento y el único producto que les permite costear su vida y la de sus familias, si no hay planes de sustitución” expresó Flores y añadió que a los incumplimientos se suma el trato violento de la Fuerza Pública hacia los campesinos**, ejemplo de ello es el saldo de los campesinos asesinados el 5 de octubre en Tumaco.** Le puede interesar: ["Policía sigue actuando de forma criminal en Tumaco"](https://archivo.contagioradio.com/policia-sigue-actuando-de-manera-criminal-en-tumaco/))

Esta problemática podría ser más catastrófica en el 2018, debido al poco presupuesto que se designó, no solo para la implementación de los acuerdos de paz, sino para poner en marcha los planes de sustitución regionales, para el vocero de Marcha Patriótica, este **presupuesto solo revela la falta de coherencia en el discurso de paz y de la voluntad política que debe verse reflejada en recursos**.

Flores señaló que si bien el paro inicia el 23 se espera que en el transcurso de la semana se vayan sumando más departamentos, para que al final haya una dinámica de paro a nivel nacional y se logre establecer **compromisos fuertes por parte del gobierno ante los múltiples incumplimientos a los campesinos y a la paz**.

El paro tendrá tres puntos de exigencia: el primero es el cumplimiento y la implementación del Acuerdo de Paz, el segundo son las garantías para la movilización y la protesta social y el tercero es el cumplimiento de las exigencias del sector agrario del país, reivindicadas por la Cumbre Agraria Étnica y Popular. Le puede interesar: ["Convocan a paro nacional indefinido el 23 de octubre"](https://archivo.contagioradio.com/convocan-a-paro-nacional-indefinido-a-partir-del-23-de-octubre/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
