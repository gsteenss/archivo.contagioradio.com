Title: Así opera el paramilitarismo en Bogotá
Date: 2019-07-05 16:27
Author: CtgAdm
Category: DDHH, Nacional
Tags: Bogotá, Enrique Pealosa, paramilitares, procuraduria
Slug: asi-opera-el-paramilitarismo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/47150047851_145dfd2798_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Young Shanahan  
] 

El pasado jueves 4 de julio defensores de derechos humanos y líderes políticos interpusieron ante la Procuraduría una queja disciplinaria contra el alcalde Enrique Peñalosa**, por no actuar sobre las amenazas del paramilitarismo en Bogotá.** Pese a las alertas tempranas emitidas por la Defensoría del Pueblo, sobre presencia de grupos armados ilegales en el sur de la ciudad y las recomendaciones hechas por la entidad, la Alcaldía no ha actuado para mitigar el riesgo sobre la población.

### **¿Cómo el paramilitarismo impacta a Bogotá?** 

El Sistema de Alertas Tempranas (SAT) de la Defensoría del Pueblo planteó en su última alerta sobre Bogotá que 1.400.000 bogotanos están en riesgo de ser víctimas de diferentes acciones del paramilitarismo en la ciudad; como explica el **analista político David Flórez,"**porque el paramilitarismo viene controlando buena parte del microtráfico en la Ciudad, porque crea redes importantes de extorsión, porque además estamos enfrentándonos a dinámicas de control territorial, están resurgiendo los panfletos y tenemos ya asesinatos de jóvenes de barrios del sur".

Flórez dijo que era grave especialmente lo que está pasando en Porvenir, Bosa, Perdomo y Ciudad Bolívar donde se ven dinámicas de violencia y control social mediante panfletos, e incluso patrullajes de hombres armados. En el caso de la Unidad de Planeamiento Zonal (UPZ) del Perdomo, por ejemplo, el Analista señaló la **presencia de cuatro grupos: Los paisas, Águilas Negras, las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y los Herederos del Bloque Capital.**

En esa UPZ hace poco se presentó un enfrentamiento armado que terminó con **la muerte de un patrullero de la Policía, algo que según Flórez, la Alcaldía presentó "como una riña con un grupo pequeño, pero esto fue una riña con fusiles de largo alcance;** no puede el Alcalde decir que una bandita pequeña tiene fusiles, metralletas y capacidad de fuego para desarrollar una acción de estas". (Le puede interesar: ["Con panfletos amenazantes advierten 'toque de queda´en Ciudad Bolivar"](https://archivo.contagioradio.com/panfletos-amenazantes-ciudad-bolivar/))

> Lamento mucho el fallecimiento de nuestro policía, intendente Eric Cruz, comandante del CAI Arborizadora en Ciudad Bolívar, enfrentando valientemente a delincuentes. Reacción oportuna permitió captura de varios de los responsables. Mi solidaridad a su familia y a Policía Nacional
>
> — Enrique Peñalosa (@EnriquePenalosa) [2 de julio de 2019](https://twitter.com/EnriquePenalosa/status/1146183010536103941?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según información oficial, el enfrentamiento ocurrió mientras los patrulleros atendían el llamado de la comunidad de Candelaria La Nueva, cerca a la Avenida Villavicencio, que había alertado sobre sujetos sospechosos que estaban en una camioneta. Cuando los uniformados abordaron el vehículo en cuestión, inició el cruce de disparos que terminó con uno de los policías muerto y otro herido. (Le puede interesar: ["En render se quedó la Bogotá de Peñalosa"](https://archivo.contagioradio.com/render-quedo-bogota-penalosa/))

La alerta temprana de la Defensoría también llama la atención de localidades como Kennedy, San Cristóbal, Rafael Uribe Uribe y Bosa, localidad sobre la que llama la atención Flórez porque en el barrio Porvenir se han presentado asesinatos e "incluso jóvenes desmembrados". **Por estas situaciones, el Líder concluyó que "el paramilitarismo viene creciendo en la ciudad y el alcalde Enrique Peñalosa no ha hecho nada para detenerlo".**

### **"El deber principal de Enrique Peñalosa es preservar la vida en la ciudad"**

Flórez recordó que el deber principal del Alcalde es preservar la vida en la ciudad, es su primer resposabilidad y por ello, también "es el jefe de policía en la ciudad". No obstante, desde su perspectiva, Peñalosa ha fallado con esta labor y con las exigencias hechas por la misma Defensoría del Pueblo para combatir el fenómeno de violencia, entre las que se encuentran: la creación de Planes Maestros para enfrentar la violencia, la construcción de mapas de riesgos específicos para cada localidad y el desarrollo de varios aspectos del Acuerdo de Paz, en el sentido de desmontar los grupos paramilitares.

De hecho, el Líder aseguró que el Alcalde le dió la espalda al Acuerdo de Paz, y muestra de ello es el Plan de Ordenamiento Territorial (POT), que no incluye en ninguno de sus apartados el Acuerdo, y por el contrario, "profundiza la segregación, sacando los pobres hacia la periferia de la Ciudad".  Ante estas situaciones, Florez, en conjunto con otros líderes sociales y políticos interpusieron **una queja disciplinaria contra el Alcalde, por no tomar las medidas para evitar estas situaciones de violencia en Bogotá, esperando que se tome una medida cautelar y sea suspendido de sus funciones.**

[Alerta Temprana N° 023-19](https://www.scribd.com/document/424309038/Alerta-Temprana-N-023-19#from_embed "View Alerta Temprana N° 023-19 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_92736" class="scribd_iframe_embed" title="Alerta Temprana N° 023-19" src="https://es.scribd.com/embeds/424309038/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-leZWFeZqJVk1gJtYYFVz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

**Síguenos en Facebook:**

> Radicamos queja disciplinaria contra [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) junto a líderes y lideresas de diferentes localidades de la ciudad, porque el alcalde no puede seguir desconociendo la presencia paramilitar en Bogotá. [pic.twitter.com/7m8ioRNGdy](https://t.co/7m8ioRNGdy)
>
> — David Flórez (@DavidFlorezMP) [4 de julio de 2019](https://twitter.com/DavidFlorezMP/status/1146794010364841984?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38148716" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38148716_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
