Title: Gestores audiovisuales se fortalecen en la V Cinemateca Rodante
Date: 2016-04-22 16:44
Author: AdminContagio
Category: 24 Cuadros
Tags: Cinemateca Distrital, Cinemateca rodante, Julian David Correa
Slug: gestores-audiovisuales-se-fortalecen-en-la-v-cinemateca-rodante
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/invitacion-cinemateca-rodante-v-temporada-e1461358073379.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cinemateca Distrital 

###### [22 Abr 2016]

Este jueves fue presentada la 5ta edición de **Cinemateca Rodante**, una iniciativa liderada por el Instituto Distrital para las Artes IDARTES y la Cinemateca Distrital de Bogotá, que busca fortalecer a las personas y grupos organizados que trabajan por el desarrollo audiovisual en todas las localidades de la Capital.

Julian David Correa, director de la Cinemateca, asegura que la propuesta busca "**fortalecer a los gestores audiovisuales que hay en Bogotá** y a sus organizaciones sean colectivos o cualquier tipo de figura jurídica" ubicados en las localidades de Puente Aranda, Santa Fe, Mártires, Teusaquillo, Usme, San Cristóbal, Rafael Uribe Uribe, Tunjuelito, Suba, Usaquén, Kenedy, Fontibon, Ciudad Bolívar y Candelaria.

\[caption id="attachment\_23081" align="aligncenter" width="478"\]![Cinemateca distrital](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/12524168_10153600392278997_9164265554156576824_n-e1461360656991.jpg){.wp-image-23081 width="478" height="334"} Lanzamiento Cinemateca Rodante - Foto: Vianney Herrera\[/caption\]

La inversión económica y logística que se hace en Cinemateca Rodante, apuesta por "el talento humano a través de la **formación audiovisual** en todos los niveles y roles de la producción", con becas y talleres prácticos que incluyen la realización de un cortometraje a lo largo de todo el proceso, en los que, como asegura Correa, **se va más allá del "pizarrón y tiza"** y aunque hay algo de eso se "**apunta al aprender haciendo**".

La **formación para la creación**, involucra talleres especializados solo de guion solo de producción, además de los **talleres para la vida cultural de los barrios**, en los que se promueven las experiencias de exhibición como son los festivales y Cine clubes, una especie de "**nuevos centros culturales de las localidades**" como los cataloga Correa y las videotecas locales, una recolección de películas en cualquier formato realizadas en las localidades de Bogotá, iniciativa que se viene realizando desde la creación de Cinemateca Rodante  "**para preservar el patrimonio de la ciudad**".

Del mismo modo, la iniciativa busca apoyar con asesoría y material audiovisual para proyectar en los espacios que hacen parte del programa de Salas Asociadas, centros de exhibición audiovisual de la ciudad en los que permanentemente se desarrollan proyectos artísticos y eventos culturales.[  
](https://archivo.contagioradio.com/gestores-audiovisuales-se-fortalecen-en-la-v-cinemateca-rodante/12524168_10153600392278997_9164265554156576824_n/)

Aunque la mayoría de las actividades de la Cinemateca Distrital de Bogotá están dirigidas a todos los públicos, para esta iniciativa "estamos buscando **encontrar multiplicadores**, gente que ya tenga un contacto con sus comunidades o de cualquier grupo social, gente **que ya tenga ideas de lo que quieren realizar audiovisualmente** de sus proyectos" recorrido y experiencia a partir de la cual se realiza el proceso de selección de los participantes.

Los interesados en hacer parte de la Cinemateca Rodante deben ingresar al sitio www.cinematecadistrital.gov.co donde se encuentran las bases de participación y formulario de inscripción.

<iframe src="http://co.ivoox.com/es/player_ej_11270715_2_1.html?data=kpafmZWbdZahhpywj5WdaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7O0JCns9PmxsaYj5Cnrc%2FZzsbhx8jFb6Xd1Nnfy9nFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
