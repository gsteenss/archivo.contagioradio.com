Title: 12 casos de ejecuciones extrajudiciales en Casanare serán revisados por la JEP
Date: 2018-08-16 15:28
Category: DDHH, Nacional
Tags: Brigada 16, Casanare, Ejecuciones Extrajudiciales, Henrry Torres Escalante, JEP
Slug: jep-revisara-12-casos-de-ejecuciones-extrajudiciales-en-casanare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/falsos-positivos-e1459276020392.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Europapress] 

###### [16 Ago 2018] 

El informe “Ni delincuentes ni combatientes” que recopila información sobre ejecuciones extrajudiciales en Casanare, ya está en manos de la Jurisdicción Especial para la paz. El documento presenta 12 casos de los mal llamados “falsos positivos” y señala que en este departamento podrían haberse dado hasta 200 ejecuciones que podrían estar vinculadas al accionar de la brigada 16 del Ejército, entre los años 2002 a 2010.

Los casos, agrupan a un conjunto de 20 personas que fueron ejecutadas por el Ejército, entre las que se encuentran **4 menores de edad, un adulto mayor, una persona en condición mental especial y una mujer en estado de embarazo y** se registraron en el periodo de tiempo de 2005 al 2007. (Le puede interesar:["JEP pide información de 9 brigadas implicadas en ejecuciones extrajudiciales"](https://archivo.contagioradio.com/9-brigadas-implicadas-en-ejecuciones-extrajudiciales/))

### **El Casanare y los “falsos positivos”** 

Este informe además presentará pruebas sobre la responsabilidad del General Herny Torres Escalante, comandante para ese entonces de la Brigada 16, que actualmente tiene una investigación abierta en la justicia ordinaría y quien solicitó comparecer en la JEP.

De acuerdo con Harold Viga, coordinador del equipo de Justicia Transicional de la Fundación Comité Solidaridad con Presos Políticos, este informe espera ilustrar a los Magistrados de la Sala de Reconocimiento “los principales patrones que se presentaron” en las ejecuciones extrajudiciales en Casanare.

Frente al accionar de la Fuerza Pública, hasta el año 2015 la Fiscalía General de la Nación informó que se tenía investigaciones sobre 50 militares pertenecientes al Batallón de Infantería Ramón Nonato Pérez o Birno 44, adscrito a la Brigada 16, sin embargo, la investigación reunió información que demostraría que por lo menos **7 de las 9 unidades que componían esa brigada estarían involucradas en ejecuciones extrajudiciales, junto con agentes del GAULA y del desaparecido DAS.**

De igual forma según el informe “Ni delincuentes ni combatientes” existiría un total de **200 ejecuciones de personas civiles**, perpetuadas por la Fuerza Pública entre los años 2002 a 2010 y responderían a la puesta en marcha de “la seguridad democrática” puesta en marcha por el ex presidente Álvaro Uribe Vélez.

La recopilación de la información se hizo durante 5 meses, en un trabajo mancomunado entre la Fundación Comité Solidaridad con Presos Políticos, el Movimiento de Víctimas de Crímenes de Estado y la Corporación Social para la Asesoría y Capacitación Comunitaria. El principal insumo fueron los expedientes de la Fiscalía de la Nación y las entrevistas con los familiares de las víctimas.

<iframe id="audio_27886974" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27886974_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
