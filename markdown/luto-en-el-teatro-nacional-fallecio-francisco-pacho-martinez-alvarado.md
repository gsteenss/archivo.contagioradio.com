Title: Luto en el teatro Nacional, falleció  Francisco  “Pacho” Martínez Alvarado
Date: 2015-09-10 11:23
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Camilo obra teatral, Fallece Francisco "Pacho" Martínez, Patricia Ariza, Santiago García, teatro la candelaria, Unión Patriótica
Slug: luto-en-el-teatro-nacional-fallecio-francisco-pacho-martinez-alvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/unnamed-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [10 Sep 2015]

El teatro la Candelaria de Bogotá, anunció la tarde del miércoles la triste noticia del fallecimiento de el actor Francisco "Pacho" Martínez, uno de los fundadores de la tradicional casa del arte dramático en Bogotá y militante de la Unión Patriótica.

"Hoy compartimos con ustedes el inmenso dolor por el fallecimiento de nuestro compañero cofundador del Teatro la Candelaria, Francisco Martínez Alvarado, “Pacho”, “Pachito”, fue el mensaje difundido por sus compañeros de vida artística por casi medio siglo.

Tras estar 15 días hospitalizado, un infarto fulminante acabo con la vida del actor y director nacido en Ciénaga, Magdalena, pionero de la "Casa de la Cultura" en los años 60, la misma que bajo la dirección del maestro Santiago García se convertiría en el Teatro La Candelaria.

La velación de "Pacho" se llevara acabo este jueves, en la sede de la Corporación artística ubicada en la Calle 12 No. 2-59 a partir de las 12 del día. "En su Teatro La Candelaria, su casa por más de 49 años, espacio que tanto amó, nos reuniremos para darle un último adiós que no es definitivo porque su memoria y su legado permanecerán siempre"

Recordado por personajes como Jerónimo Salcedo en "Guadalupe años sin cuenta", y en los últimos tiempos por su participación en algunas de las funciones de la obra "Camilo", misma que la compañía suspenderá en su honor, en las últimas funciones de la presente temporada.

Todos los aplausos por toda una vida de entrega al teatro y a la lucha por sus ideales.

<div id="videoread">

</div>

 
