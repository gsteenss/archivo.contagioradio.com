Title: Comité de Escogencia abre posibilidad de opiniones ciudadanas a candidatos
Date: 2017-09-12 16:00
Category: Nacional, Paz
Tags: candidatos, comision de la verdad, Comite de Escogencia, Desmatelamiento de grupos paramilitares
Slug: comite-de-escogencia-abre-posibilidad-de-opiniones-ciudadanas-a-candidatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Undp_co_comite_2017.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PNUD] 

###### [12 Sept. 2017]

A partir de este 12 de Septiembre se abre la etapa de las observaciones ciudadanas a las 23 candidaturas para la Unidad Especial de Investigación para el Desmantelamiento de Organizaciones Criminales y los 218 para la Comisión para el Esclarecimiento de la Verdad. El comité de escogencia abrió esta posibilidad en su página y estas observaciones serán claves para la definición de los cargos.

En el caso de la **Comisión para el Esclarecimiento de la Verdad, fueron seleccionados 218 personas,** de las cuales 82 son mujeres y 135 hombres. Se destaca la selección de 1 persona de opción genero diversa parra ser parte de esta comisión.  Así mismo, del total 15 son extranjeros y 203 son nacionales pertenecientes a etnias como afros, mestizos e indígenas.

Por su parte, para la Dirección de la Unidad Especial de Investigación para el Desmantelamiento de Organizaciones Criminales **se conoció de la selección de 10 mujeres y 13 hombres, para un total de 23 inscritos. **Le puede interesar: [La ardua tarea que tendrán los integrantes de la Comisión de la Verdad](https://archivo.contagioradio.com/comienza-convocatoria-para-integrantes-de-la-comision-de-la-verdad/)

### **¿Llama más la atención la JEP que los demás espacios?** 

Para **Camila Moreno, Directora del Centro Internacional para la Justicia Transicional en Colombia** habría que hacer un balance positivo a la fecha de lo que ha sucedido con las postulaciones y la selección, porque hubo una participación amplia para la JEP, pero habría que llamar la atención por la baja participación en las otras instancias.

“Ayer se publicó el listado de las personas que cumplen los requisitos mínimos para la Comisión de la Verdad, pero tenemos que preguntarnos por **el bajo nivel de postulaciones, quizás no se ha hecho suficiente pedagogía de qué es la Comisión de la Verdad** o de pronto llama más la atención la JEP. Le puede interesar: [Las recomendaciones de las víctimas para la Unidad de Búsqueda de desparecidos](https://archivo.contagioradio.com/las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos/)

### **¿Qué sigue luego de la publicación de los listados de aspirantes?** 

Ahora los ciudadanos y ciudadanas tendrán la posibilidad de realizar una participación activa en el proceso que ayudará a los integrantes del Comité de Escogencia a seleccionar los mejores candidatos. Las personas deben ingresar  a [www.comitedeescogencia.com](http://www.comitedeescogencia.com), entrar a la plataforma para observaciones, registrarse o digitar el usuario y la clave y una vez dentro del sistema puede buscar el candidato o candidata, seleccionar la Hoja de Vida y hacer los comentarios.

Dichas observaciones **deben ser hechas de “manera respetuosa, con información completa y verás”** recalcan en la página del Comité, para que de esta manera puedan ser tenidos en cuenta y se incluyan en los criterios para seleccionar o descartar a algún postulante. Le puede interesar: ["Queremos conocer la altura moral de los aspirantes a la JEP": Claudia Vaca](https://archivo.contagioradio.com/justicia_especial_para_la_paz_jep_claudia_vaca/)

Para personas que se encuentren en zonas alejadas o de difícil acceso a internet, el Comité habilitó el correo <observaciones@comitedeescogencia.com> en el cual se puede enviar un documento de máximo 2 mil caracteres en el que se especifique las observaciones frente a algún candidato.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
