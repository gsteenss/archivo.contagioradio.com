Title: Festival centro 10 años
Date: 2019-02-27 20:30
Author: AdminContagio
Category: 24 Cuadros
Slug: festival-centro-10-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

Con el slogan "Vuelve a tu centro" desde este 30 de enero inicia una edición más del Festival Centro, evento artístico y cultural que desde hace una década tiene lugar en la capital colombiana

Hasta el próximo 5 de febrero, agrupaciones como Los amigos invisibles de Venezuela, Las 1280 almas de Colombia, LosPetitFellas, Adriana Lucía, Nodjset Ruben Albarran, Billos Caracas, Nelson y sus estrellas, Lisandro Meza
