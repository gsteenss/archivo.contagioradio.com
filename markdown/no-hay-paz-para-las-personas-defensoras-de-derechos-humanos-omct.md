Title: "No hay paz para las personas defensoras de derechos humanos" OMCT
Date: 2018-05-18 13:32
Category: DDHH, Paz
Tags: defensores de derechos humanos, Fiscalía, Organización Mundial Contra la Tortura, Paramilitarismo
Slug: no-hay-paz-para-las-personas-defensoras-de-derechos-humanos-omct
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Caminata-por-la-paz-201.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 May 2018]

La Organización Mundial contra la Tortura presentó su último informe llamado “Colombia: No hay paz para las personas defensoras de derechos humanos”, un documento que revela que en el país, no habrán garantías para los defensores de derechos humanos si no se reconoce la existencia de estructuras **paramilitares que continúan operando y se tomen medidas sobre las mismas, se finalice la estigmatización al movimiento social, se deje de criminalizar la protesta social y hayan serios pasos institucionales para acaba la impunidad**.

### **El paramilitarismo principal responsable de asesinatos a defensores** 

De acuerdo con el documento, las estructuras paramilitares son las responsables del **84% de las agresiones y asesinatos a defensores de derechos humanos, presentados en el año 2017**. Mientras que en el 2016 fueron los responsables del 88% de las acciones violentas contra los defensores.

De igual forma en el documento se afirma que son estructuras paramilitares debido a que guardan características del fenómeno paramilitar como lo son el relacionamiento con la Fuerza Pública, así como con poderes económicos y políticos locales y regionales. Razón por la cual, instan al gobierno a que las reconozca y en esa misma medida genere políticas que las desmantelen y combatan. (Le puede interesar: ["Colombia recibió más de 200 recomendaciones frente a situación de derechos humanos"](https://archivo.contagioradio.com/colombia-recibio-mas-de-200-recomendaciones-frente-a-situacion-de-derechos-humanos/))

En ese sentido, en el archivo se nombran dos episodios particulares que demuestran el poder de estas estructuras paramilitares. El primero de ellos es la amenaza al líder social Eison Valencia Sinesterra, en Punta Icaco Buenaventura, donde fue víctima de hostigamientos por integrantes de las Autodefensas Gaitanistas de Colombia, el pasado 26 de junio de 2017, luego de reunirse con la delegación que produjo este informe.

El segundo hecho son las constantes denuncias que ha hecho la Comisión Intereclesial de Justicia y Paz, frente al control del **80% del territorio colectivo de Cacarica** por parte de estructuras paramilitares también pertenecientes a las Autodefensas Gaitanistas.

### **La estigmatización a la defensa de los derechos humanos y la protesta social** 

Otro de los puntos del informe afirma que en Colombia continúa la estigmatización en contra de los defensores de derechos humanos y a la protesta social desde las tres ramas del poder, los cuerpos de la Policía y el Ejército. Ejemplo de ello es la ya reconocida frase pronunciada por el Ministro de defensa en el año **2017, haciendo alusión a que el asesinato de líderes sociales era producto de “líos de faldas”**.

Frente al poder Legislativo, el informe recordó el intento desde el Congreso de la República por evitar que los magistrados que ya se habían escogido para la JEP se posesionaran si se habían desempeñado a lo largo de su carrera como defensores de derechos humanos. Hecho que se caracterizó por una **fehaciente discriminación contra los defensores de derechos humanos** y que provocó el rechazo de muchas organizaciones tanto nacionales como internacionales.

Sobre la discriminación hacia la defensa de los derechos humanos ejercida desde el poder Judicial, el informe resalto el proceso que se inició contra **la defensora y vocera de la Comisión de Interlocución del Sur de Bolívar Milena Quiroz,** quien fue detenida y acusada de “incitar a marchas”. Milena salió en libertad el pasado mes de noviembre del año 2017, mientras que la Fiscal María Bernarda Puente, que llevó su caso, se encuentra actualmente en una investigación por nexos con el paramilitarismo.

Sumado a estos tipos de estigmatización se encuentra la represión que en diferentes movilizaciones ha ejercido el Escuadrón Móvil Antidisturbios. Ejemplo de ello, fue el uso desmedido por parte del ESMAD, durante el marco del paro de Buenaventura el año pasado, en el que incluso un joven de **19 años fue asesinado por el accionar de una de las armas que usa esta Fuerza Pública**. (Le puede interesar:["Amenazan a dos líderes sociales del paro cívico de Buenaventura"](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/))

### **La impunidad continúa haciendo de las suyas en Colombia** 

Según la recopilación de la información de la Organización Mundial contra la Tortura, en Colombia, el 90% de las agresiones contra defensores de derechos humanos continúan en la impunidad, es decir, sin una investigación que rinda cuentas sobre quiénes son los autores materiales e intelectuales de los actos.

Las propias cifras de la Fiscalía revelan que en cuanto a las investigaciones de las acciones violentas contra defensores de derechos humanos, solo el **5.75% están en fase de esclarecimiento. Asimismo, esta institución afirmó que desde el 2016 hasta el 2018**, tiene un reporte de 218 casos de agresiones a defensores.

### **Las recomendaciones a Colombia** 

De acuerdo con Miguel Zumalacárregui, integrante de la Organización “hay una tendencia al incremento de asesinatos de defensores de derechos humanos”. Razón por  la cual, entre las recomendaciones que genera este informe se encuentra impulsar una política de estado para la protección de las personas defensoras de derechos humanos y **líderes sociales, que cuente con toda una coordinación de las diversas instituciones estatales**.

De igual forma le piden a la Fiscalía que avance en las investigaciones de casos sobre agresiones a los defensores, y a las instituciones que se ponga en marcha medidas eficaces contra el desmonte del paramilitarismo en todo el territorio colombiano.

Realizar actos públicos y ejercicios de sensibilización que pongan en conocimiento el valor y la función de los defensores de derechos humanos, **al igual que acoger las recomendaciones hechas por la Comisión Interamericana de Derechos humanos**.

<iframe id="audio_26056983" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26056983_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
