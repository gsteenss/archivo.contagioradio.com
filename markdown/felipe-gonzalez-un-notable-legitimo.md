Title: Felipe González: ¿un notable legítimo?
Date: 2017-04-20 08:12
Category: Opinion, Vicente Vallies
Tags: acuerdo de paz, Felipe González, JEP
Slug: felipe-gonzalez-un-notable-legitimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Felipe-González-un-notable-legítimo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alfredo Pérez 

#### [**  
**]**Parte 1 de 3 – Los GAL** 

#### [**Por Vicente Vallies**] 

###### 20 Abr 2017 

[El 23 de marzo, en su comunicado conjunto Nº 15, el Gobierno Nacional y las FARC-EP designaron "]*[como Notables del Componente Internacional de Verificación, al ex presidente español Felipe González y al ex presidente uruguayo José Alberto Mujica Cordano, quienes encabezarán el mecanismo de verificación y cumplirán con las funciones establecidas en el numeral 6.3.2.]*[" El texto de los acuerdos establece que los “]*[notables serán dos personas de representatividad internacional]*[” encargadas de “]*[realizar pronunciamientos e informes públicos en relación con los avances que se registren en la implementación de todos los acuerdos]*[”. Tomando en cuenta cuáles son los principales retos de la implementación de los acuerdos, es difícil considerar que el expresidente español Felipe González tenga legitimidad para cumplir con esta función.]

[El primero reto – objeto de esta columna - es]**la permanencia de las estructuras paramilitares**[ responsables de la mayoría de los asesinatos de defensores/as de Derechos Humanos y líderes/as sociales desde el inicio del proceso de paz; en 2016 se registraron entre 80 y 117 homicidios y en menos de 3 meses de 2017 han sido asesinadas 30 personas defensoras.]

[¿Cómo puede tener legitimidad Felipe González cuando durante los primeros años de su gobierno (1983 -1987) actuaron con total libertad los Grupos Antiterroristas de Liberación – GAL, agrupaciones armadas parapoliciales que practicaron el terrorismo de Estado?]

[Amnistía Internacional en su reciente informe "Afrontar el pasado para construir el futuro" define los GAL como "]*[una organización clandestina en la que participaron miembros de los cuerpos de seguridad del Estado y pistoleros a sueldo, que bajo el conocimiento de altos cargos del gobierno llevaron a cabo atentados indiscriminados, siendo responsables de 27 asesinatos, incluyendo 10 personas sin ninguna conexión con ETA, y de torturas, secuestros y extorsión]*[." ¿No ven un parecido a los grupos paramilitares colombianos; grupos creados desde la institucionalidad supuestamente para luchar contra las guerrillas pero que mataron y desplazaron a opositores políticos, campesinos, defensores y defensoras de derechos humanos?]

[Durante el proceso judicial contra los GAL se probó que fueron en parte financiados por altos funcionarios del Ministerio del Interior. Un ministro y varios funcionarios de alto rango del gobierno de González fueron condenados a penas de cárcel, entre otros][[José Barrionuevo]](https://es.wikipedia.org/wiki/José_Barrionuevo)[, ][[ministro de Interior]](https://es.wikipedia.org/wiki/Ministerio_del_Interior_de_España#Lista_de_Ministros_del_Interior_de_Espa.C3.B1a)[ y][[Rafael Vera]](https://es.wikipedia.org/wiki/Rafael_Vera)[, secretario de Estado para la Seguridad fueron sentenciados a penas de prisión por el][[secuestro]](https://es.wikipedia.org/wiki/Secuestro)[ del señor Segundo Marey y malversación de caudales públicos. Como lo determina la Sentencia del Tribunal Supremo sobre este caso los acusados sacaron dinero "]*[de las arcas públicas para destinarlo a la financiación de un delito tan grave como la detención ilegal o secuestro de una persona.]*[" En dicha sentencia se puede leer también que el Centro de Estudios para la Investigación y Defensa (CESID), órgano dependiente del Ministerio de Defensa, confeccionó un documento en el que se examinaban los pros y contras de las distintas posibilidades de intervención de los GAL en el sur de Francia, documento que concluye: "]*[En cualquier circunstancia se considera que la forma de acción más aconsejable es la desaparición por secuestro]*[". Más adelante considera el mismo Tribunal que "]*[no parece lógico organizar aisladamente desde Bilbao, sin autorización de los máximos responsables del Ministerio del Interior, una operación de tanta importancia como el secuestro de una persona en territorio extranjero que podía originar problemas diplomáticos de relevancia]*[." Si bien Felipe González negó su participación o conocimiento, la misma duda queda plantada ¿Era posible la actuación de los GAL sin su conocimiento?]

[En agosto de 1996 el Gobierno de José María Aznar negó a los jueces los papeles del CESID necesarios para continuar la investigación argumentando que afectaban a la seguridad del Estado, los documentos finalmente desclasificados en 1997 fueron solo una parte y en su mayoría ya eran conocidos por la opinión pública. Así, como concluye Amnistía Internacional "]*[una buena parte de los atentados cometidos por los GAL (…) han quedado en la impunidad]*["]

[En Colombia para la construcción de paz; la verdad, la justicia y la reparación a las víctimas son fundamentales. Conocer las responsabilidades de todos los actores implicados incluyendo de las altas esferas del gobierno es necesario. ¿Qué va a hacer Felipe González, proponer replicar el modelo español donde la impunidad ha sido intolerablemente alta y las investigaciones nunca han podido llegar a los más altos responsables?.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
