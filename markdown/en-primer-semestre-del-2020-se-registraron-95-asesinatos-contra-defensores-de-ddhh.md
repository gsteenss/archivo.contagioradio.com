Title: En primer semestre del 2020 se registraron 95 asesinatos contra defensores de DDHH
Date: 2020-11-11 21:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: defensores de ddhh, defensores de derechos humanos, Somos defensores
Slug: en-primer-semestre-del-2020-se-registraron-95-asesinatos-contra-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Agresiones-a-defensores-de-DD.HH_.-2019-2020.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Asinato-de-defensores-de-DD.HH_.-por-departamento.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Responsables-de-asesinatos-de-defensores-de-DD.HH_..png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Informe-Defensores-de-DD.HH_.-asesinados.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El programa [Somos Defensores](https://twitter.com/SomosDef) presentó su más reciente informe **"El virus de la violencia"** en el que recoge las cifras del **Sistema de Información sobre Agresiones contra Personas Defensoras de Derechos Humanos en Colombia (SIADDHH) revelando **que entre enero y junio de 2020 se registró un incremento considerable en los homicidios en contra de los y las defensores de DDHH. en Colombia. ([971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El estudio distingue** diferentes tipos de agresiones: Amenazas, atentados, detenciones arbitrarias, desapariciones forzadas, judicializaciones y robo de información.** En relación con la cifra general se reportó una disminución entre el primer semestre del año 2019 donde se registraron 591 agresiones y el mismo periodo del presente año donde se **registraron 463.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, se enfatizó en que las circunstancias del confinamiento y la imposibilidad de movilizarse hacia los territorios dificultó ostensiblemente la recopilación de información, por lo cual, la reducción de las cifras no puede entenderse como una merma efectiva de las agresiones.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De hecho, **la situación que ha acarreado la pandemia del Covid-19 ha traído efectos nocivos y mayores riesgos para los liderazgos sociales**, si se tiene en cuenta que el confinamiento y las estrictas restricciones de movilidad que caracterizaron el primer semestre del año, convirtieron a los líderes en blancos fáciles pues se hizo más sencillo para los agresores ubicar los lugar en los que permanecían.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De ahí que los homicidios hayan incrementado de 59 registrados en el primer semestre del 2019 a 95 en el mismo periodo del presente año 2020.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92691,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Agresiones-a-defensores-de-DD.HH_.-2019-2020.png){.wp-image-92691}  

<figcaption>
Comparativo de Agresiones entre 2019 y 2020

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El informe fue dedicado a cada una de las personas asesinadas en defensa de los DDHH. y documenta el nombre, edad, labor comunitaria y una breve historia de cada uno de los líderes y lideresas asesinados; así como el presunto responsable de perpetrar el crimen en los casos en los que se tiene indicio o información al respecto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Más defensores asesinados**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El dato más concluyente del informe es el desmedido incremento de asesinatos en contra de los líderes; **la cifra aumentó en un 61% respecto del primer semestre del año anterior donde se registraron 59 homicidios frente a los 95 registrados este año**. Esta cifra genera especial alerta pues según Somos Defensores,  se puede interpretar como una mayor concreción de muchas de las amenazas de las que son víctimas los y las defensores de DDHH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los departamentos más afectados son **Cauca que registró 26 asesinatos, seguido por Antioquia con 14 casos y Norte de Santander con 10.**  Las víctimas fueron 85 hombres y 10 mujeres. (Le puede interesar: ["Violencia contra líderes sociales se concentró en 29 de los 32 departamentos"](https://archivo.contagioradio.com/violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":92693,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Asinato-de-defensores-de-DD.HH_.-por-departamento.png){.wp-image-92693}  

<figcaption>
Fuente: Informe "El virus de la Violencia"

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Los responsables de las agresiones contra defensores de DDHH

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Los grupos paramilitares fueron a quienes se atribuyeron el mayor número de agresiones con un total de 138 casos correspondientes al 30% del total**, la denominación de estos grupos varía según la zona de influencia, entre ellos se encuentran las Águilas Negras, las Autodefensas Gaitanistas de Colombia -AGC, el Clan del Golfo, La Mafia, Los Caparrapos, Los Pachenca y Los Urabeños. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte, el segundo actor más responsable de agresiones fue la Fuerza Pública con la comisión de 54 casos (12%), seguida por las disidencias de las FARC con 48 agresiones (6%) y el ELN con 27 agresiones (5%).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En 172 casos (37%) no se pudo identificar el autor de la agresión**, lo cual, dificulta aún más, la ya bastante remota tarea de protección de los afectados por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92694,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Responsables-de-asesinatos-de-defensores-de-DD.HH_..png){.wp-image-92694}  

<figcaption>
Fuente: Informe "El virus de la Violencia"

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
