Title: Así debería ser la búsqueda de personas desaparecidas en La Escombrera
Date: 2016-04-05 16:19
Category: DDHH, Entrevistas, Nacional
Tags: CIDH, La escombrera, operación orion
Slug: asi-deberia-ser-la-busqueda-de-personas-desaparecidas-en-la-escombrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/cidh-escombrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [5 Abril 2016]

Para las organizaciones de víctimas, el proceso de búsqueda de personas desaparecidas y las excavaciones en [[La Escombrera](https://archivo.contagioradio.com/?s=la+escombrera+)] ha tenido falencias, relacionadas con la desarticulación de las investigaciones, en las que se han tenido en cuenta sólo las versiones y testimonios de paramilitares acogidos a la Ley 975, y en las que **las víctimas no han podido participar efectivamente**, según lo denunciaron ante la Corte Interamericana de Derechos Humanos CIDH, en este 157 periodo de sesiones.

Según denunció el peticionario Byron Góngora, pese a que la Sala de Justicia y Paz ordenó hace dos años, detener el arrojo de material de construcción en 'La Escombrera', así como el cierre de 'La Arenera', los dos lugares, identificados como **las fosas comunes más grandes de Medellín**, continúan abiertos y los cuerpos no identificados siguen siendo enterrados bajo arena, piedras y escombros.

El Plan Integral de Búsqueda de Personas Desaparecidas puesto en marcha, no acogió los resultados de las experiencias internacionales exitosas en la materia, y **tampoco tuvo en cuenta los testimonios de las víctimas**, ni los avances forenses y geológicos para iniciar las excavaciones en 'La Escombrera', afirma Góngora.

De acuerdo con Góngora, en 2014 la Fiscalía y la Alcaldía de Medellín estuvieron en 'La Escombrera' junto a un jefe paramilitar que señaló los polígonos 1, 2 y 3, y aunque las **organizaciones solicitaron que se cotejaran otras pruebas de modo, tiempo y lugar**, las autoridades dieron inicio a la excavación en el polígono 1, basadas únicamente en su testimonio.

Para las organizaciones de víctimas es urgente que se articulen las investigaciones que cursan en la Unidad de Justicia y Paz y las que la Fiscalía adelanta, a fin de que se esclarezcan los hechos que rodearon la **desaparición de por lo menos 130 personas, entre 2002 y 2004** en Medellín, en el marco de la implementación la [[Operación Orión](https://archivo.contagioradio.com/?s=orion+)].

De acuerdo con María Falan, integrante del Grupo Interdisciplinario por los Derechos Humanos, estas investigaciones tendrían que dar cuenta de los responsables de las casas clandestinas de paso, en las que se **interrogaron, torturaron, asesinaron y desaparecieron a cientos de personas entre el 2002 y el 2010**.

Diana Arango, directora de [[EQUITAS](https://archivo.contagioradio.com/innovadora-herramienta-para-buscar-personas-desaparecidas-en-colombia/)], aseguró que el Plan Integral de Búsqueda debe incluir un proceso de investigación preliminar que dé cuenta del contexto de estas desapariciones, así como la **identificación de las víctimas y las circunstancias de su desaparición**, teniendo en cuenta los momentos previos y posteriores de la Operación Orión.

Cotejar diferentes versiones y no sólo las de los paramilitares; documentar los casos que aún no están registrados, **cruzar la información judicial con la forense, realizar georeferenciación y** detener inmediatamente el arrojo de escombro en los lugares en los que podrían estar las personas desaparecidas, éstas fueron las exigencias que las víctimas le hicieron al Estado colombiano ante la CIDH para que la búsqueda de personas desaparecidas responda a los criterios de verdad, justicia y reparación integral como se establece en el acuerdo número 62 de la Mesa de La Habana.

Escuche la audiencia completa:

https://www.youtube.com/watch?v=2dVhQirAz9w  
 

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

######  
