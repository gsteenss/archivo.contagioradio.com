Title: Tras ser retenido por hombres armados, liberan al líder social Dailer Mosquera
Date: 2019-10-13 16:22
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Agresiones contra líderes sociales, Ipiales, nariño
Slug: tras-ser-retenido-por-hombres-armados-liberan-al-lider-social-dailer-mosquera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Dailer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

En horas de la tarde de este domingo, fue liberado el líder social, **Dailer Mosquera** quien fue secuestrado el mismo día por hombres armados sin identificar. Según información aportada por la Red de DDHH del Putumayo, Piamonte Cauca Cofanía Jardines de Sucumbíos Ipiales, Nariño, Dailer se encuentra actualmente fuera de peligro.

Cerca de las 2:00 pm, cuatro hombres armados que se desplazaban en tres motos de alto cilindraje llegaron hasta al caserío de la vereda Brisas de Rumiyaco, parte de este corregimiento de Nariño preguntando por el dirigente **a quien amarraron y se llevaron con rumbo desconocido.**

Acto seguido y según un tercer informe de la Red de DDHH del Putumayo, la comunidad se desplegó en el lugar donde fue secuestrado Dailer, quien fue dejado en libertad por parte de sus captores.[(Le puede interesar: Agresiones contra líderes sociales incrementaron un 49% en el primer semestre del 2019)](https://archivo.contagioradio.com/agresiones-contra-lideres-sociales-incrementaron-un-49-en-el-primer-semestre-del-2019/)

Dailer hace parte de Marcha Patriótica y es **representante legal de Consejo Comunitario Liberación y Futuro** ubicado en este corregimiento que históricamente, junto a las veredas La libertad y el Empalme han resistido a la presencia de grupos paramilitares que operan en la zona.

Noticia en desarrollo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
