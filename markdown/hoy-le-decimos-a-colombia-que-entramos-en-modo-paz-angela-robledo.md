Title: "Hoy le decimos a Colombia que entramos en modo paz": Ángela Robledo
Date: 2016-03-30 17:28
Category: Nacional
Tags: dialogo eln gobierno, Diálogos de paz en Colombia, ELN
Slug: hoy-le-decimos-a-colombia-que-entramos-en-modo-paz-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Rueda-de-prensa-Paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Iván Cepeda ] 

###### [30 Mar 2016 ] 

Tras los esfuerzos emprendidos por organizaciones sociales y de fe, así como por algunos Congresistas de llamar al Gobierno nacional y al ELN a formalizar los diálogos de paz, este miércoles se anunció el [[inicio de la fase pública de conversaciones](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)], una decisión frente a la que estos sectores expresaron su apoyo y en rueda de prensa extendieron la invitación a la sociedad civil colombiana para que participe activamente en este proceso de negociación, teniendo en cuenta que **el punto de la agenda referido a la participación de la sociedad civil "es el más innovador"** como afirmó, la representante Angela Maria Robledo.

Por su parte el representante Alirio Uribe, aseguró que desde las Comisiones de Paz de la Cámara y el Senado se ha hecho extensivo el llamado a que se logre una paz completa, estable y duradera para Colombia, pues "sí se acaba la guerra, ganamos todos". Uribe insistió en que aunque hayan dos mesas de negociación, es sólo uno el fin del conflicto; así mismo, extendió su agradecimiento al gobierno nacional y a los gobiernos de Ecuador, Chile, Cuba y Venezuela por el apoyo y la disponibilidad. El Representante afirmó que **el Congreso debe legislar en función de lo que se pacte y la sociedad debe aprovechar la oportunidad de participación** que abren estos diálogos de paz frente a la discusión de los puntos de negociación.

El Representante por el Polo Democrático Víctor Correa Vélez, aseguró que no habrá paz completa sí el movimiento social no participa activamente de su construcción, por lo que invitó a las **organizaciones sociales para que participen activamente de este proceso de negociación con el fin de que sus reivindicaciones hagan parte de la consolidación de la paz**, que debe implicar la garantía de derechos y el reconocimiento de las realidades que han sustentado el conflicto y necesitan ser cambiadas. Vélez insistió en que éste será un proceso complejo que requiere que el pueblo colombiano sea un agente activo para que las **falencias de la democracia en Colombia sean superadas y se puedan implementar los acuerdos**.

De esta rueda de prensa también participaron los senadores del Polo Democrático Alberto Castilla e Iván Cepeda Castro, quienes extendieron su **mensaje de esperanza al pueblo colombiano para la consolidación de una paz completa** que incluya cese al fuego trilateral entre las insurgencias y el Gobierno, y la ampliación de los escenarios de participación de la sociedad civil.

Escuche la rueda de prensa

<iframe src="http://co.ivoox.com/es/player_ej_10986737_2_1.html?data=kpWmmpubd5ihhpywj5eWaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncYamk63c25DQqYzYxsjWz9TXb8KfpNTZ0dLGrcKf0trSjcrSuNPVztTgjcrSb87jxdSY0sbeaZOmhpiujYqnd46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]
