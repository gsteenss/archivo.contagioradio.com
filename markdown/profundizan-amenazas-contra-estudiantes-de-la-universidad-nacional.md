Title: Agudizan amenazas contra estudiantes de la Universidad Nacional
Date: 2015-05-25 12:10
Category: Educación, Nacional
Tags: aguilas megras, amenaza, educacion, itayosara rojas, mane, Paramilitar, Santos, Universidad Nacional
Slug: profundizan-amenazas-contra-estudiantes-de-la-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AmenazaUN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_4546397_2_1.html?data=lZqhmJide46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlNPjx9rbxs7epc%2BfwtLS0MbepdSfxNTb1tfFb8bn1drRy8bSuMbnjMnSjdHFb7biytvS1NjNqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Itayosara Rojas, estudiante de sociología UN] 

###### [25 may 2015] 

De los panfletos electrónicos firmados por las **Águilas Negras** en que se amenazaba a estudiantes y profesores de universidades públicas, imponiendo una fecha límite para que no regresaran a las aulas universitarias; se pasó a las **amenazas directas y personales, cerca al lugar de residencia de los jóvenes.**

Es el caso de **Itayosara Rojas**, estudiante de **sociología de la Universidad Nacional** en Bogotá, que asegura que "me encontraba desplazandome hacia mi casa cuando 2 personas que se desplazaban en una moto se acercaron y me gritaron 'dejen de joder chinos hijueputas o los vamos a joder'.'

Otros compañeros suyos han recibido llamadas telefónicas que consideran sospechosas. "N*os hablan o dicen ciertas cosas, como si la intensión fuese sembrar miedo*", agrega la estudiante.

Para Itayosara, esta es una "**estrategia de escalar el terror**" para amedrentar al movimiento estudiantil. "Nosotros defendemos la educación publica y de calidad. Nosotros no hacemos nada que esté por fuera de la legalidad", afirma Rojas.

A pesar de los comunicados de rechazo por parte de las directivas de la Universidad, y el pronunciamiento del presidente Santos anunciando una exhaustiva investigación sobre el origen de estas amenazas, lo cierto es que **hasta el momento, estudiantes y profesores no han recibido medidas contundentes para salvaguardar su seguridad.**

Los y las estudiantes siguen esperando un proceso de investigación en que la fiscalía de con los responsables de las amenazas, y desactive la red que no solo afecta a las personas mencionadas en los panfletos, sino al conjunto de la comunidad universitaria.

"*Estas amenazas no solamente son contra personas en específico, sino que son un ataque frontal a la posibilidad de democracia en la universidad y en general en la sociedad colombiana* -asegura Itayosara Rojas-, *y no podemos seguir permitiendo que sucedan cosas de esta índole en una sociedad que busca la construcción de paz con justicia social*".
