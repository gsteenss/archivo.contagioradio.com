Title: Paramilitares ofrecen recompensas por asesinar líderes indígenas
Date: 2018-12-19 11:01
Author: AdminContagio
Category: DDHH, Entrevistas, Líderes sociales
Tags: Aguilas Negras, Líderes indígenas, ONIC, paramilitares
Slug: panfletos-firmados-por-aguilas-negras-anuncian-recompensas-a-quienes-asesinen-a-lideres-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicia Diario ] 

###### [19 Dic 2018] 

La Organización Nacional Indígena de Colombia, ONIC, denunció la aparición de panfletos en el norte del Cauca firmados por un grupo que se autodenomina como Águilas Negras, en que** ofrecen recompensas monetarias a quiénes asesinen a líderes indígenas e integrantes de los cabildos.**

De acuerdo al panfleto, fechado el 18 de Diciembre, por el asesinato de gobernadores indígenas y suplentes se darán 5 millones de pesos, por el de capitanes y coordinadores de la Guardia indígena 3 millones, por los guardias 2 millones y por quienes se desempeñan como alguaciles y colaboradores de esta organización un millón de pesos.

Además, el mensaje amenaza a las autoridades indígenas Esneyder Gómez Salamanca, Ruben Orley Velasco, Antonio Secue, Arcadio Troches Secue, Arcelio Silvia Noscue, Sigilfredo Pavi, Nora Elena Taquinas, Dora Mosquera, Noé Rivera y Lizardo Secue, responsabilizándolos del conflicto que se vive en la región.

Aida Quilcue, integrante de la Comisión de derechos humanos de la ONIC, señaló que este panfleto salió horas después de que finalizará la audiencia pública de los pueblos indígenas, en donde **reafirmaron su control territorial en el departamento del Cauca, tras el asesinato de la autoridad indígena Edwuin Dagua**, y como forma de garantizar la seguridad de las comunidades. (Le puede interesar:["No hay tregua para indígenas y líderes sociales en el Cauca"](https://archivo.contagioradio.com/no-tregua-indigenas-lideres-sociales-cauca/))

### **"Nos declaran la guerra y ponen precio a nuestros líderes indígenas"** 

La líder indígena afirmó que el asesinato de Dagua se cometió a 2 kilómetros del lugar en donde se encuentra un retén militar, y expresó que a pesar de que el departamento del Cauca se encuentra militarizado, no se explican cómo aumentan los asesinatos a líderes sociales y el control de estructuras paramilitares.

"Cuando ocurren estos hechos de muerte, de asesinatos, y fuera de eso aparecen los panfletos, nos queda la duda de qué esta haciendo la Fuerza  Pública frente a seguridad, y por eso el movimiento indígena ha dicho que la Fuerza Pública no es garantía para nosotros, porque en últimas **nos están matando delante de lo militares**" aseguró Quilcue.

[![paras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/paras-450x800.jpg){.alignnone .size-medium .wp-image-59414 .aligncenter width="450" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/paras.jpg)

<iframe id="audio_30906572" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30906572_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
