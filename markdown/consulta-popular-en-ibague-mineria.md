Title: Consulta popular y participación ambiental
Date: 2016-03-15 07:00
Category: Ambiente y Sociedad, Opinion
Tags: consulta popular, Ibagué, Mineria
Slug: consulta-popular-en-ibague-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: revistaccesos 

#### **[Natalia Gómez Peña - Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)** 

###### 15 Mar 2016

Hace algunos días se anunció que el Concejo municipal de Ibagué aprobó **la consulta popular** en la que se preguntará a los ciudadanos si quieren que haya minería en su territorio. Esta pregunta llega ante los posibles graves impactos que la minería causaría en las fuentes de agua que surten a la ciudad. La aprobación por parte del Consejo se dio a pesar de las presiones de diferentes entidades del gobierno central, que de acuerdo con lo expresado por los concejales a la prensa, estuvieron en Ibagué haciendo lobby para que no se aprobara la consulta popular\[[1](http://www.elespectador.com/noticias/medio-ambiente/primera-vez-un-concejo-municipal-aprueba-una-consulta-p-articulo-619483)\].

La aprobación de la consulta en Ibagué se convierte en un hecho de gran importancia y que dará luces acerca de varios temas vigentes en la realidad nacional como lo son el desarrollo de la autonomía territorial, la participación ambiental, la toma de decisiones sobre el futuro de los recursos naturales y  la protección del agua.

Es importante recordar que cuando hablamos de participación en materia ambiental hablamos de la posibilidad que tienen los ciudadanos de incidir en los procesos de toma de decisiones públicas que afecten el medio ambiente como bien de titularidad colectiva.  Para que haya una verdadera participación ambiental,  los mecanismos de participación deben permitir que las personas ejerzan una incidencia concreta en las decisiones, que quienes participan sean escuchadas por las autoridades y que sus opiniones y aportes sean tenidos en cuenta.

A nivel internacional, el derecho a la participación pública ambiental fue reconocido por el Principio 10 de la Declaración de Río de 1991 como la mejor forma de tratar las cuestiones ambientales, ya que cuando las personas pueden participar efectivamente en la toma de decisiones generalmente, esas decisiones,  son mejor acogidas y ambientalmente más sostenibles frente a las que se toman sin la participación ciudadana.  Actualmente,  en Latinoamérica y el Caribe se desarrolla un proceso de negociación internacional, del cual hace parte Colombia, para implementar por medio de un tratado el principio 10 en nuestra región\[[2](http://www.cepal.org/es/antecedentes-principio-10)\].

En Colombia,  la Constitución Política de 1991 es definida como una Constitución ecológica, donde hay una explícita consagración del derecho al disfrute de un medio ambiente sano, con un enfoque participativo.  Además, la Ley 99 de 1993 estableció que la política ambiental se debe guiar por unos principios rectores, entre los que se encuentra el principio del manejo ambiental democrático y participativo, y los principios contenidos en la Declaración de Río de 1992, donde se incluye el llamado Principio 10 sobre acceso a la información, participación y justicia ambiental. De estos principios se desprende la obligación del Estado y de sus autoridades ambientales de permitir tanto el acceso a la información como la participación ciudadana en todas sus actuaciones.

El derecho de acceso a la participación ambiental debe garantizarse desde el momento en que se está decidiendo realizar un proyecto o actividad, con el fin de prevenir la aparición de futuros conflictos socio ambientales, pues de nada sirve a los ciudadanos una participación tardía que no es efectiva y que no les permite incidir en los procesos de toma de decisiones. Para ello también es importante que los ciudadanos puedan acceder a la información necesaria acerca de las oportunidades para participar y el tema sobre el que se decidirá.

La razón de ser de la democracia se manifiesta en el derecho de acceso a la participación. La participación con respecto a asuntos ambientales es la piedra angular para la construcción de sociedades más equitativas, justas y  conscientes de los impactos que el hombre tiene sobre su hábitat natural. Veremos ahora cómo se desarrolla la consulta popular en Ibagué y como la opinión de la ciudadanía es tenida en cuenta por el gobierno nacional.
