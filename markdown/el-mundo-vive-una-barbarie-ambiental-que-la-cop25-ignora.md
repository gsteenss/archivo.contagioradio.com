Title: "El mundo vive una barbarie ambiental que la  COP25 ignora"
Date: 2019-12-02 18:40
Author: CtgAdm
Category: Ambiente, El mundo
Tags: Ambiente, Chile, COP25, Estados Unidos, fracking
Slug: el-mundo-vive-una-barbarie-ambiental-que-la-cop25-ignora
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EKc-kG-XYAI4Q2K.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:[[Fridays For Future Barcelona] 

Desde el 2 hasta el 13 de diciembre, se desarrolla la Conferencia de las Naciones Unidas sobre el Cambio Climático (COP25), en Madrid, donde delegados de  200 países tocarán temas como la explotación de combustibles fósiles, la crisis del agua, la responsabilidad entre países productores y consumidores y el  efecto invernadero; además uno de los temas de mayor interés entre las organizaciones ambientalistas, y es el cumplimiento del Acuerdo de París firmado en 2015, el cual la mayoría de los países subscritos no ha empezado a ejecutar.

Según Tom Kucharz miembro de Ecologistas en Acción, la cumbre que es organizada por la ONU cada año es una farsa, "los gobiernos no tienen voluntad política para cumplir las acciones por el clima, primero porque por ahora los acuerdos internacionales son voluntarias, lo que los hace invisibles ante los gobernantes de algunos países, y segundo porque hay intereses mas fuertes entre la política y las empresas para ejecutar acciones que no afecten su capital". (Le puede interesar: [Jóvenes de Colombia se movilizan en contra del cambio climático](https://archivo.contagioradio.com/jovenes-colombia-se-movilizan-del-cambio-climatico/))

Kucharz agregó también que las acciones reales por el planeta no están en este tipo de encuentros, como la Cop25, sino en las miles de movilizaciones que llevan haciendo organizaciones de jóvenes ambientalistas en el mundo, "no deberíamos esperarnos mucho de esta cumbre oficial, las acciones se están logrando por la sociedad civil que lleva un año trabajando y exigiendo políticas verdes, especialmente los jóvenes por generar un cambio, por medio de organizaciones como, Jóvenes por el Clima y  Fridays por Future, que no han parado y se han sumado a movimientos feministas, campesinos".

### "El mundo vive una barbarie climática que la COP 25 ignora"

Esta cumbre que inicialmente se tenia pensada para que se desarrollara en Chile fue cambiada a Madrid debido a la crisis social que esta atravesando el país suramericano, "principalmente por el miedo a un estallido social en reclamo por una justicia climática, y de alguna manera desviar también el foco internacional ante los 116 conflictos socio-ambientales registrados en el último año en Chile", añadió Kucharz; daños que giran en torno a temas como la explotación minera, agricultura industrial, abuso de millones de recursos naturales, y la privatización de fuentes naturales como el agua.

Para el ecologista esta cumbre era la oportunidad para que Chile y en general el mundo denunciara los más de 8 millones de desplazamientos territoriales causados este año por la poca viabilidad climática, "el mundo vive una barbarie climática que es ignorada por los gobiernos en la COP25, empezando por lo vergonzoso e ilógico que es ver a la ministra de medio ambiente de Chile, precediendo el evento,  teniendo en cuenta que hace parte de un gobierno  que ha generado atroces actos de violencia hacia el mismo pueblo", agregó Kucharz.

Por otro lado resaltó la irresponsabilidad por parte de los países más contaminantes quienes no han estado presentes en esta cumbre ni en ninguna acción por el planeta, países como Estados Unidos, China y Arabia Saudí, "en el  caso de Estados Unidos en la administración de Trump quien abandonó el Acuerdo de París, nosotros lo condenamos como un crimen de lesa humanidad, porque al no tomar medidas su acción acarrea muerte de miles de seres vivos y un impacto irreversible al planeta". (Le puede interesar: [Acuerdos de París no resuelven crisis del Cambio Climático](https://archivo.contagioradio.com/acuerdos-paris-cambio-climatico/))

Asimismo señaló que, "Europa se presenta de manera hipócrita, criticando a quienes no están en procesos ambientales, pero ignorando que los grandes banco europeos invierten en la explotación minera, y que su continente es el mayor importado de gas extraído con fracking", y finalizó afirmando que países como Colombia, Chile y Ecuador, representados por jóvenes y la Minga Indígena,  se sumarán a las miles de movilizaciones que se desarrollan en el mundo en torno a las exigencias que le hacen a los gobiernos en el marco de la COP25 y el cumplimiento de los compromisos firmados en el Acuerdo de París.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45281073" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45281073_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
