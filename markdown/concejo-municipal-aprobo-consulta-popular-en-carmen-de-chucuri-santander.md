Title: Concejo municipal aprobó Consulta popular en Carmen de Chucuri, Santander
Date: 2017-06-04 11:59
Category: Ambiente, Nacional
Tags: Carmen del Chucuri, consulta popular, Santander
Slug: concejo-municipal-aprobo-consulta-popular-en-carmen-de-chucuri-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/elcarmen2-e1496591985334.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [04 Jun 2017]

El municipio del Carmen de Chucuri, en Santander, avanza hacia la realización de la consulta popular minera tras el fallo a favor que otorgo el Concejo Municipal. Esta consulta busca **evitar que se realicen actividades de exploración y extracción minera, en un territorio que consideran ha tenido vocación histórica agrícola y pecuaria**.

La pregunta que aprobó el Concejo Municipal es **¿estad usted de acuerdo si o no que en la jurisdicción del municipio del Carmen de Chuchuri se realicen actividades de exploración y explotación mineras de carbón y de hidrocarburos?**

Este proceso lo inició la Corporación Compromiso junto con habitantes del Carmen de Chucurí, hace 10 años, con la intensión de ponerle un freno a la minería en sus territorios y hace 3 meses el alcalde del municipio, **Isaías Rueda, comenzó todos los trámites jurídicos para llevarla a cabo. **Le puede interesar:["Gobierno debe encontrar consenso frente a la minería"](https://archivo.contagioradio.com/gobierno_mineria_rodrigo_negrete_consultasoulares_tamesis/)

El paso siguiente es que **el Tribunal Contencioso Administrativo falle a favor de la comunidad y permita que se realice la consulta popular **y tendrá 25 días para analizar y evaluar si la pregunta formulada se aprueba o debe modificarse. Johanna Cárdenas, vocera del comité impulsor de la consulta, expresó que durante este tiempo seguirán haciendo pedagogía en la comunidad.

Actualmente, en el Carmen de Chucurí se han otorgado títulos mineros en un sector de 20 mil hectáreas a multinacionales extranjeras como Centromin y Colco, que para los habitantes han afectado recursos como el agua, principal fuente para mantener sus actividades económicas.

Otro de los municipios que también se encuentra en trámite para consulta popular es San Vicente de Chucuri que se sumarían a los departamentos y territorios de Colombia que activan este mecanismo. Le puede interesar: ["!Si se pudo¡ Cajamarca le dijo no a la minería en su territorio"](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)

######  Reciba toda la información de Contagio Radio en [[su correo]
