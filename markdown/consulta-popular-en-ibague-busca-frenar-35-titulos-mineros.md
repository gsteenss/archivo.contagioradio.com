Title: Consulta Popular en Ibagué busca frenar 35 títulos mineros
Date: 2016-10-12 15:03
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, consulta popualr, Megamineria
Slug: consulta-popular-en-ibague-busca-frenar-35-titulos-mineros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las2Orillas] 

###### [12 Oct de 2016] 

La Consulta Popular impulsada por el Comité Ambiental en Defensa del Agua, la Vida y el Territorio, **busca frenar más de 35 títulos concedidos a megaproyectos que ocupan 44.000 hectáreas, en la ciudad de Ibagué,** de continuar estos proyectos se vería gravemente afectado el acceso al agua, la vida y la soberanía en estos territorios**.** Son 349.124 las ciudadanas y ciudadanos habilitadas para votar a finales de este mes.

La meta que fijaron los participantes de las Asambleas comunitarias organizadas por el Comité, es de **140.000 votos, un poco más de lo que requiere el NO para que la decisión sea válida.** De los títulos concedidos, 27 fueron otorgados a la Anglo Gold Ashanti, la misma multinacional que adelanta gestiones para explotar el oro de las montañas de Cajamarca. Lea También: [Consulta Minera Busca Frenar Proyecto la Colosa en Cajamarca, Tolima](https://archivo.contagioradio.com/consulta-minera-busca-frenar-proyecto-la-colosa-en-cajamarca-tolima/)

En distintas regiones de Colombia, la minería se esta tomando los lugares más estratégicos, se trata de **mas de 20.9 millones de hectáreas, en un país donde sólo se cultivan 4.9 millones de hectáreas para alimentar a 40 millones de habitantes.** Jaime Tocora integrante del Comité Ambiental en Ibagué, comenta que en el departamento del Tolima “sólo una empresa, la Anglo Gold tiene más de 8.9 millones de hectáreas para explotación, y es frente a esta realidad que se dice no queremos mas este tipo de minería”.

"Es claro que no se debe hacer minería en zonas de alta montaña, pues es allí donde nace el agua que todos consumimos, se encuentran familias productoras agrícolas, zonas de reserva y páramos" manifestó Tocora, sin embargo, cerros como el Volcán el Machín se encuentran en grave peligro, 40% de su cuerpo montañoso está concedido a un proyecto de mega minería. Lea También: [Niños indígenas y campesinos dibujan los efectos de la minería en sus cuerpos](https://archivo.contagioradio.com/ninos-indigenas-dibujan-los-efectos-de-la-mineria-en-sus-cuerpos/)

Frente a este panorama, la ciudadanía de la capital tolimense ha venido movilizándose y realizando actividades pedagógicas como tomas culturales, difusión de información puerta a puerta, talleres en distintos barrios, charlas y conferencias. “**Estamos conformando 13 comités ambientales, uno por cada comuna en la ciudad** y uno en la parte rural donde nos estamos encontrando para realizar diferentes actividades”, afirma Tocora.

**“Lo que viene sucediendo en el Tolima incide en decisiones para todo el país**[, la primera consulta popular de asuntos mineros realizada en Piedras motivó la consulta en Ibagué” dice el integrante del Comité. ]La presión social ha facilitado que la Corte proporcione más herramientas a las comunidades, para decidir sobre el uso de suelos y el subsuelo.

La invitación es a participar de un encuentro previo a la Consulta, el **próximo viernes a las 5:30pm en el auditorio los Ocobos de la Universidad del Tolima** para fortalecer el proceso y articular otras iniciativas que surgen en la ciudad.

<iframe id="audio_13290350" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13290350_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_13290387" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13290387_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
