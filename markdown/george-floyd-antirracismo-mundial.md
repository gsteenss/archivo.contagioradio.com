Title: Así es el movimiento antirracista que conocimos después del asesinato de George Floyd
Date: 2020-06-18 19:46
Author: CtgAdm
Category: Actualidad, El mundo
Tags: EE.UU., George Floyd, Minneapolis
Slug: george-floyd-antirracismo-mundial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/49959004213_1a01b2642e_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto George Floyd / Lorie Shaull

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tres semanas después del asesinato del afroamericano George Floyd como consecuencia de la brutalidad policial en Minessota, hecho que suscitó manifestaciones en al menos 25 ciudades de Estados Unidos, ha llevado a que el movimiento trascienda fronteras llegando a otras naciones donde no solo la movilización social se ha tomado las calles sino que ha llevado a derribar antigua símobolos y figuras que históricamente estuvieron vinculados a la esclavitud.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Ofunshi, defensor de DD.HH. y residente de Minneapolis** relata que en EE.UU. el racismo una persona afroamericana tiene cinco veces más posibilidad de ser encarcelada que una blanca, por lo que afirma que tras la muerte de George Floyd, unió a los barrios circundantes que se fueron sumando, "fue tanta indignación, tanta la rabia que el pueblo se cansó, esa rabia se extendió por todos los Estados Unidos y también internacionalmente, blancos, negros y afroamericanos e indígenas". [(Le recomendamos leer: Los 8 minutos y 46 segundos que arrodillaron a demócratas de EE.UU. en memoria de George Floyd)](https://archivo.contagioradio.com/los-8-minutos-y-46-segundos-que-arrodillaron-a-democratas-de-ee-uu-en-memoria-de-george-floyd/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito, el defensor de DD.HH. resalta que Minnesota es uno de los Estados donde más población indígena existe y donde hace pocos días se derribo una estatua de Cristobal Colón en cercanías al Capitolio de Minnesota en St. Paul.en compañía del American Indian Movement (AIM), "este despertar ha sido la lucha de una hormiga en coalación donde hemos tenido resultado positivos (...) la escalada que hizo que la muerte de Floyd sea tan impactante repercutió en todo el mundo,, basta de tener representaciones racistas y que tuvieron que ver con la dominación" [(Lea también: George Floyd y los tres detonantes de las protestas en EE.UU.)](https://archivo.contagioradio.com/george-floyd-y-los-tres-detonantes-de-las-protestas-en-ee-uu/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El defensor de DD.HH. afirma que en el caso de Colombia, las personas afrodescendientes e indígenas enfrentan desafíos mayores en particular en las zonas rurales y urbanas, acudiendo a datos de la **[Asociación Nacional de Afrodescendientes Desplazados (Afrodes)](https://twitter.com/AfrodesColombia)** que reveló que más de 35.000 afrocolombianos han sido víctimas de desplazamiento forzado durante 2020, en particular en regiones como el **Catatumbo, Norte de Santander, Bajo Cauca, Antioquia y sur de Bolívar**, revelando que tipos de violencia como el desplazamiento también son muestras de racismo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### George Floyd, una señal de cambio

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ofunshi explica que es necesario entender "que el sistema y las instituciones son racistas entre ellos el sistema de salud, por lo que incluso en Estados Unidos se reportan más de 117.000 muertes, según el conteo de la Universidad Johns Hopkins, por lo que el covid-19 ha dejado ver el desequilibrio, demostrando que **el 70% de las muertes por la pandemia en el país norteamericano corresponden a personas afrodescendientes.**[(Lea también:El racismo permanece en las instituciones del Estado: CRIC)](https://archivo.contagioradio.com/el-racismo-permanece-en-las-instituciones-del-estado-cric/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La muerte de Floyd nos da la oportunidad de un cambio, Minneapolis están dando un ejemplo, quedó prohibido que el policía utilice la técnica de estrangulación, es la primera vez que el jefe de la Policía es afroamericano, y eso trae consigo que los otros estados estén conscientes y metan la mano en el fango", afirma el defensor de DD.HH. quien reitera que a nivel mundial hay cambios y finalmente se está discutiendo sobre la brutalidad policial, algo que nunca en la historia había sucedido en su país. [(Lea también: Informe revela las inhumanas acciones de EE.UU. con migrantes centroamericanos)](https://archivo.contagioradio.com/las-condiciones-de-los-migrantes-se-vuelven-inhumanas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a las elecciones presidenciales, Ofunshi señala que en esta ocasión es la población más joven la que esta exigiendo un cambio, seguro de que las personas no quieren un nuevo gobierno de Donald Trump, "pensamos que Biden va a ser la misma basura pero un poco más flexible, aunque también están politizando lo que aconteció, cualquier persona es mejor que Trump, tenemos que entender que debemos desarticular el sistema".

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fbusiness.facebook.com%2Fcontagioradio%2Fvideos%2F602068547092053%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
