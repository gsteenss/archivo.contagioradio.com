Title: 14 de Junio gran movilización Distrital de Docentes
Date: 2017-06-12 12:08
Category: Movilización
Tags: Ministerio de Educación, Paro Docente
Slug: 14-de-junio-movilizacion-de-docentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [12 Jun 2017] 

Este 14 de junio se realizará la movilización Distrital de docentes que tendrá 14 lugares de concentración en diferentes localidades de la capital.  Las maestras y los maestros, han señalado que hasta que no exista una propuesta concreta sobre **la financiación al para acabar con el déficit presupuestal del Sistema Nacional de Educación, el paro continuará.**

Durante la última movilización realizada por los docentes, el pasado 9 de junio, los maestro y maestras denunciaron las agresiones por parte del Escuadrón Móvil Anti Disturbios, que de acuerdo con Over Dorado, integrante de la dirección de FECODE, **pretendía afectar mentalmente y físicamente una marcha pacífica**.

Referente al **asesinato de 3 docentes en el marco de la movilización y el paro, Dorado señaló que estos hechos son preocupantes** “nosotros hemos venido pidiendo garantías en ese sentido al gobierno Nacional en torno al derecho a la protesta, la lucha social y a la libertad sindical” afirmó el docente y agregó que le exigen al gobierno investigaciones inmediatas y prontas respuestas de quienes estarían detrás de los hechos.

Hoy los docentes se encontraran en 3 puntos de Bogotá: el colegio Manuela Beltrán, en la calle 57 con avenida Caracas, en el SENA de la carrera 30 con avenida primera de mayo y en el monumento Héroes de la calle 80 con avenida Caracas. Le puede interesar:["Padres, Madres y estudiantes respaldan el paro de Docentes"](https://archivo.contagioradio.com/con-pedagogia-maestros-incluyen-a-padres-de-familia-y-estudiantes-en-las-movilizaciones/)

### **¿Cómo va la mesa de diálogos con el Ministerio de Educación?** 

Tanto el magisterio como el Ministerio de Educación han expresado su interés por continuar con las conversaciones para levantar el paro. Sin embargo, de acuerdo con Over Dorado **hace falta voluntad política, voluntad jurídica y voluntad financiera para levantar el cese de actividades**. Le puede interesar: ["Esperamos que Estado invierta en Educación los recursos que se necesitan"](https://archivo.contagioradio.com/fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones/)

Los puntos en los que todavía no hay acuerdos son cuatro: la financiación del déficit presupuestal del sistema de Educación, que es de 700 mil millones de pesos, **la salud y prestación de servicios, la política educativa y carrera docente y las garantías sindicales**.

“Aspiramos a que el gobierno comprenda que los niños no están las aulas de clase no porque FECODE lo quiera, **sino porque las condiciones económicas, educativas y pedagógicas no lo permiten**” afirmó Over Dorado.

<iframe id="audio_19215422" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19215422_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
