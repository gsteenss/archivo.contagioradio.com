Title: ¿Y qué garantías ofrece el Gobierno a los excombatientes frente al Covid 19?
Date: 2020-03-19 18:53
Author: CtgAdm
Category: Actualidad, Paz
Tags: Covid-19, FARC
Slug: y-que-garantias-ofrece-el-gobierno-a-los-excombatientes-frente-al-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-19-at-7.17.36-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Excombatientes: Cortesía FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el fin de prevenir riesgos en medio de la pandemia del Covid 19, los 24 Espacios Territoriales de Capacitación y Reincorporación donde habitan 2.893 excombatientes, además de las 73 nuevas áreas de reincorporación han decidido realizar cierres preventivos y limitar el ingreso a sus territorios con el fin de priorizar el bienestar de la comunidad que afirma no tener garantías de salud por parte de un Estado que no ha cumplido a cabalidad la implementación del Acuerdo de Paz y que tampoco ha ofrecido la atención suficiente a la población rural en Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las medidas incluyen limitar el ingreso de delegaciones internacionales, integrantes de ONG, cooperación internacional y otras personas que desarrollen procesos o actividades salvo que sea una necesidad, limitar la salida de los espacios territoriales y suspender las solicitudes de viajes de quienes trabajan en la Agencia para la Reincorporación y la Normalización. [(Le puede interesar: El Pato, el río que cambió para la paz)](https://archivo.contagioradio.com/el-pato-el-rio-que-cambio-para-la-paz-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cindy Rodríguez, medica cirujana y encargada del componente de salud del Consejo Nacional de Reincorporación se refirió al aislamiento a nivel rural de estos espacios, en su mayoría con una alta concentración de niños y población vulnerable **donde no solo es evidente una casi falencia del Estado y recursos limitados sino a su vez un flujo continuo de personas que transitan por estos espacios,** razón por lo que se declaró un aislamiento colectivo y temporal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La médica expresó su preocupación pues el sistema de salud de Colombia, que aún tiene problemas de acceso de atención médica a las poblaciones rurales, no puede compararse con el de otras naciones como Italia o España donde pese a sus avances ya superan los 41.000 y 18.000 casos de contagio respectivamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos preocupados por la irresponsabilidad del Gobierno al tomar medidas que benefician directamente a los empresarios y no a la población en general" afirma la doctora quien a su vez asegura que dichas medidas **demuestran una total indiferencia del Gobierno frente a las poblaciones.** [(Le puede interesar: El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes)](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El sistema de salud de Colombia no está preparado" advierten los excombatientes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde los ETCR los excombatientes han expresado inquietudes relacionadas a cómo va a ser la ruta de atención teniendo en cuenta que e**n muchos espacios no se cuenta con centros ni profesionales de la salud y que para llegar a los puntos de atención se deben recorrer largas distancias donde tampoco existen los insumos suficientes para tratar enfermedades** o pandemias como la del Covid 19. Otros de los temores de la población tienen que ver con el desabastecimiento de los víveres en sus territorios y la posible pausa que podrían sufrir los proyectos productivos de cara a una crisis en el territorio nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Abelardo Caicedo, coordinador del Espacio Territorial de Tierra Grata en Manaure, La Guajira** relata que la comunidad de excombatientes de [FARC](https://twitter.com/PartidoFARC)está asumiendo con responsabilidad las recomendaciones e hizo referencia a las medidas de sanidad con respecto a espacios comunes como los baños y los comedores que son compartidos por la población, de igual forma expresa que la tienda se está dotando de todo lo necesario para no tener que salir a comprar a los centros urbanos,. [(Lea también: Garantías de vida y seguridad para excombatientes se quedaron en el papel)](https://archivo.contagioradio.com/garantias-de-vida-y-seguridad-para-excombatientes-se-quedaron-en-el-papel/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como medida adicional se han suspendido las reuniones masivas incluidas las pedagogías de paz, las integraciones con las comunidades aledañas o campeonatos deportivos, de igual forma se han limitado las salidas de los espacios territoriales.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Aquí nos va a ir muy mal por la mala atención en salud si esto se llega a extender, sin pandemia la atención es mala, imagínese usted donde esto se generalice, tenemos que estar listos en todo lo que tiene que ver con la prevención pero no estamos preparados" - Abelardo Caicedo

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La situación, expresa el coordinador es tan crítica que en los casos urbanos cercanos, impulsada por el miedo y la desinformación la población ha agotado los tapabocas, razón por la que desde el espacio territorial han empezado a fabricarlos en la comunidad con la esperanza de que en medio de la contigencia, esta puede ser una forma de emprendimiento. [(Le puede intersar: No tiene por qué haber inflación de precios, si Gobierno toma medidas urgentes ante Covid 19)](https://archivo.contagioradio.com/no-tiene-por-que-haber-inflacion-de-precios-si-gobierno-toma-medidas-urgentes-ante-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
