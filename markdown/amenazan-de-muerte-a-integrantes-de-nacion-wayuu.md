Title: Asesinatos y amenazas contra líderes Wayúu que denuncian al ICBF y al Cerrejón
Date: 2018-01-15 16:47
Category: DDHH, Nacional
Tags: desnutrición niños Wayúu, ICBF, indigenas wayuu, La Guajira
Slug: amenazan-de-muerte-a-integrantes-de-nacion-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/indigenas_wayuu_0-e1516041332156.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona cero] 

###### [15 Ene 2018] 

[Cuatro son los integrantes de la organización Nación Wayuu que han sido víctimas de  amenazas y dos han sido asesinados por defender los territorios pertenecientes las comunidades ancestrales de los intereses de las empresas en la Guajira o porque han denunciado la posible corrupción al interior del **Instituto Colombiano de Bienestar Familiar, ICBF Regional Guajira.**]

"Por denuncias contra el ICBF, hemos recibido amenazas, y también por denuncias contra El Cerrejón donde hemos pedido compensaciones", señala José Silva, vocero de Nación Wayúu, quien agrega que la sede del ICBF de la Guajira sigue "adjudicando los contratos según sus conveniencias sin contar con la Consulta Previa de las comunidades afectadas", mientras que la única respuesta que han recibido son amenazas.  (Le puede interesar: [El drama de las comunidades Wayuú por la minería)](https://archivo.contagioradio.com/comunidades-de-la-guajira-exige/)

### **Amenazas y asesinatos** 

Tras las amenazas algunos han desistido de las denuncias, mientras otros han preferido resistir. De acuerdo con la organización, son cerca de siete líderes los que han sido amenazados e incluso asesinados. **José Silva, ya fue víctima de un atentado cuando iba con su familia hacia su comunidad indígena.** Su carro fue impactado  con un disparo de arma de fuego en uno de los vidrios laterales. En otra ocasión el mismo líder fue capturado de manera ilegal el municipio de Manaure, luego de notificar a unas autoridades tradicionales la admisión de una tutela en contra de ICBF.

Otra de las víctimas es María del Rosario Ruíz, Autoridad Tradicional y Ancestral del Territorio Indígena KATSALIAMANA, también Ever Epieyue e Isilia Pimienta, luego de haber radicado en el Tribunal Superior de Riohacha un Incidente de Desacato a una acción de tutela que el movimiento ganó en contra del ICBF Regional Guajira, mediante la cual ordenaban llevar a cabo la Consulta Previa en las Comunidades indígenas para el ingreso de los operadores.

La alerta es máxima, **debido a que también han asesinado a líderes de esa comunidad. Rafael Lubo Aguilar quien fue ultimado al parecer por haber denunciado actos de corrupción en la educación.**  La docente Juana Almazo Uriana, fue hallada muerta en zona rural de Manaure. Las autoridades aún investigan si su deceso se produjo luego de una fuerte denuncia que formuló en el municipio de Uribía donde realizaba labores como educadora.

### **La respuesta del ICBF** 

Según  el comunicado de Nación Wayúu, existen alrededor de cinco tutelas falladas en primera y segunda instancia, que **le ordenan a ICBF Regional Guajira hacer Consulta Previa para el ingreso de los operadores a los territorios étnicos** pero ninguna ha sido acatada por la institución, que se defiende asegurando que se trata de un ente autónomo al que no aplica la consulta previa.

Cabe recordar, que para la vigencia 2017, **más de 67 organizaciones indígenas Territoriales, avaladas por las Autoridades Tradicionales, que venían operando en los territorios afro y de la comunidad Wayúu,** fueron desvinculadas sin razón alguna, como asegura la denuncia, con el fin "de darle ingreso a organizaciones filiales a los intereses políticos y económicos de la Dirección de ICBF Regional Guajira. Dicho ingreso no fue consultado con las autoridades tradicionales indígenas, por el contrario, fueron impuestos".

<iframe id="audio_23162034" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23162034_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

###### 

<div class="osd-sms-wrapper">

<div class="osd-sms-title">

Compartir:

</div>

</div>
