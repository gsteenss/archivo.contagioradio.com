Title: La desaparición de Santrich no puede ser un impedimento para buscar la paz
Date: 2019-07-02 17:56
Author: CtgAdm
Category: Nacional, Paz
Tags: Corte Suprema de Justicia, FARC, Jesús Santrich, Unidad Nacional del Protección
Slug: la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Jesus-Santrich-en-entrevista-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado 29 de junio la Unidad Nacional de Protección (UNP) advirtió que **Seuxis Pausias Hernández Solarte, más conocido como Jesús Santrich**, abandonó su esquema de protección en el Espacio Territorial de Capacitación y Normalización (ETCR) de Tierra Grata, en César. Aunque este hecho ha generado expectativa, su abogado **Eduardo Matías** manifestó que pudo haber sido producto de algún tema de seguridad, mientras que la periodista Patricia Lara, aseguró que hay que continuar con la implementación y no dejar que este tema desvíe la situación de los excombatientes.

### Situación de seguridad influyó en el ánimo de Santrich

"Santrich ha sido criminalizado, a través de los medios de información, por el Presidente de la República, el Ministerio de Defensa, la Vicepresidenta de la República e incluso el Procurador", afirmó Matías, quien agregó que existe una campaña en contra del integrante del partido FARC, que **"ha puesto en riesgo su integridad física y libertad".**

Estas situaciones, según el abogado, pudieron haber ocasionado que Santrich tomará acciones para evitar algún atentado en contra de su vida. De otro lado, Matias aseguró que para el juicio que se llevará acabo el próximo 9 de julio en la Corte Suprema de Justicia, por narcotráfico en contra de Hernández Solarte, la defensa asistirá y espera que su apoderado se presente. En caso contrario será la Corte quien decida el proceder del caso.

### **"Hay un proceso de paz"** 

Para **la periodista y analista Patricia Lara**, la salida de Hernández Solarte fue desconcertante, sin embargo indicó que "Santrich no es la paz, y no podemos dejar desviar el debate sobre más de 10 mil excombatientes que se han acogido al proceso y que están cumpliendo en cuanto a acogerse a los Acuerdos". [(Lea también:¿Qué viene para Santrich tras recuperar su libertad?)](https://archivo.contagioradio.com/que-viene-para-santrich-libertad/)

En tal medida, expresó que en términos objetivos lo de Santrich no debería tener mayor repercusión, y aseguró que "el proceso tiene que seguir y la paz tiene que seguir. Si Santrich no se presenta el 9 de julio le habrá hecho daño al proceso, porque le daría argumentos a los retractores de la paz que dirán: Se los dije".

 

**Síguenos en Facebook:**<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
