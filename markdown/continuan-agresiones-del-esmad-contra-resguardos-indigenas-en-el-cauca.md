Title: Continúan agresiones del ESMAD contra resguardos indígenas en el Cauca
Date: 2019-02-22 08:02
Category: Comunidad, DDHH
Tags: Abuso de fuerza ESMAD, Agresiones contra indígenas, Cauca
Slug: continuan-agresiones-del-esmad-contra-resguardos-indigenas-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Dz8K67OWoAEKCvy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ACINCAUCA 

###### 21 Feb 2019

Desde horas de la mañana, escuadrones del ESMAD realizaron operativos contras las comunidades indígenas del **Resguardo de Huellas** quienes realizaban, acorde a sus tradiciones el ejercicio de liberación de la madre tierra en la hacienda **La Emperatriz en el municipio de Caloto, Cauca.** Durante los enfrentamientos cinco indígenas resultaron heridos y cuatro hectáreas de maiz, yuca, plátano y hortaliza fueron destruidas.

Según relata **Luis Acosta, coordinador nacional de la Guardia Indígena**, en horas de la mañana, fuerzas del ESMAD ingresaron a la hacienda La Emperatriz  destruyendo a su paso los cultivos de pancoger de las comunidades lo que resalta el coordinador es "una ofensa al territorio y  a la comida, pues mientras hay niños que mueren de hambre, el se tumba la comida que la gente siembre para su manutención y para venderlo en el pueblo".

De igual forma, según la Asociación de Cabildos Indígena del Norte del Cauca (ACIN) se presentaron agresiones contra los indígenas con gases y disparo de fusil dejando un saldo de cinco heridos, de los cuales dos fueron heridos de gravedad por las armas de fuego; uno de ellos fue remitido a hospital de alta complejidad de la ciudad de Cali,  mientras los demás fueron trasladados al Hospital La Niña María de Caloto. [(Lea también Desalojos del ESMAD dejan cuatro indígenas heridos en el Norte del Cauca) ](https://archivo.contagioradio.com/desalojos-del-esmad-dejan-cuatro-indigenas-heridos-en-el-norte-del-cauca/)

**"A nosotros nos dispara la guerrilla, nos disparan los paramilitares y ahora nos dispara el Estado"** expresa Acosta quien rechaza el accionar de la Fuerza Pública contra los pueblos indígenas y afirma que seguirán posicionados en el lugar para continuar en el encuentro nacional de guardias indígenas, evento durante el que sucedió el enfrentamiento.

Finalmente señala que también permanecerán en la tierra para resembrar lo perdido, pues las cosechas perdidas durante el suceso suman cerca de cuatro hectáreas, **"en cuatro meses podremos cosechar nuevamente el maíz pero el plátano si nos dolió porque ese plátano tarde un año en dar cosecha, pero si nos tumban una vez, sembraremos mil veces",** recalca el coordinador indígena.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F194516893995802%2Fvideos%2F2464238973646253%2F&amp;show_text=0&amp;width=261" width="261" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_32766687" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32766687_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
