Title: Continua asesinatos en contra de los normalistas de Ayotzinapa
Date: 2016-10-07 15:27
Category: DDHH, El mundo
Tags: 43 desaparecidos Ayotzinapa, normalistas de Ayotzinapa
Slug: continua-asesinatos-en-contra-de-los-normalistas-de-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ayotzinapa.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 de Oct 2016]

El pasado martes fueron **asesinados dos estudiantes identificados como Jonatan Morales y Filemón Tacuba**, pertenecientes a la Normal Raúl Isidro Burgos de Ayotzinapa, sus cuerpos fueron encontrados en la carretera Chipalcingo – Tixtla, con varios impactos de bala, las autoridades informaron que al parecer este hecho podría ser producto de un atraco, sin embargo las familias de los estudiantes ponen en duda esta versión.

Estudiantes, maestros y familiares, exigieron que se aclaren las circunstancias en las que fueron asesinados los normalistas y **que este acto no se sume a la impunidad**, además miembros de la Federación de Estudiantes Campesinos Socialistas de México, denunciaron que el[Estado tendría la intención de acabar con la Normal Raúl Isidro Burgos de Ayotzinapa.](https://archivo.contagioradio.com/militares-mexicanos-estarian-implicados-en-asesinatos-de-ayotzinapa/)

El asesinato de los estudiantes se habría producido cuando se movilizaban en transporte público de Chipalcingo a Tixtla, al finalizar su jornada de servicio social en Oxaca, posteriormente fueron interceptados por un grupo de hombres  con armas de fuego que obligo a todos los pasajeros a bajarse de la camioneta.

Según la versión oficial, los dos estudiantes de cuarto año, se rehusaron a bajar del carro y los ladrones dispararon en contra de ellos y tres personas más. Uno de ellos habría llegado con vida hasta el hospital, en donde falleció. Sin embargo **otras versiones dicen que los estudiantes podrían haber sido asesinados por ser de la Normal de Ayotzinapa**. En el hecho fue asesinada una persona más.

Hace dos años en México desaparecieron [43 estudiantes de la Normal Superior Rural "Raúl Isidro Burgos" de Ayotzinapa](https://archivo.contagioradio.com/ayotzinapa-2-anos-sin-verdad-ni-justicia/), los familiares de los jovenes permanecen con la misma incertidumbre del primer día, a pesar que según el último informe público sobre el caso, se han presentado 130 detenciones, 422 resoluciones judiciales, 850 declaraciones, 1.651 actuaciones periciales y un expediente de 240 tomos y un cuarto de millón de folios.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
