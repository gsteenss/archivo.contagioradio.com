Title: "Pese a que se ha avanzado, aún queda un largo camino de compromiso con la paz": Massieu
Date: 2020-10-20 16:40
Author: CtgAdm
Category: Actualidad, Paz
Slug: pese-a-que-se-ha-avanzado-aun-queda-un-largo-camino-de-compromiso-con-la-paz-massieu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Massieu-ONU.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 19 de octubre se las personas integrantes del Consejo de Seguridad de la ONU reiteraron su apoyo generalizado al proceso de paz en Colombia, al mismo tiempo que **reafirmaron su compromiso de continuar trabajando estrechamente con Colombia para apoyar la implementación integral del Acuerdo Final de Paz.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MisionONUCol/status/1318359528338001920","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MisionONUCol/status/1318359528338001920

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al mismo tiempo expresaron su preocupación por las persistentes amenazas, ataques y asesinatos contra líderes comunitarios y sociales, destacando los riesgo de, *"mujeres lideresas y líderes y lideresas de las comunidades indígenas y afrocolombianas".* ([«Violencia contra líderes sociales se concentró en 29 de los 32 departamentos»](https://archivo.contagioradio.com/violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como el rechazo en contra de los asesinatos de jóvenes y menores de edad, y junto a ellos el aumento de los homicidios de excombatientes de Farc que se acogieron al Acuerdo de Paz, y suman un total de 214 firmantes asesinados. ([Con Cristian Sánchez ascienden a 230 los firmantes de paz asesinados](https://archivo.contagioradio.com/con-cristian-sanchez-ascienden-a-230-los-firmantes-de-paz-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al igual que reafirmaron su apoyo a los tres componentes del Sistema Integral de Verdad, Justicia, Reparación y No Repetición y, *"destacaron la necesidad de respetar su plena independencia y autonomía"*. [Tras la verdad, hay un sector que no quiere perder el poder del control ideológico](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Expresiones de responsabilidad por crímenes cometidos durante el conflicto son pasos positivos hacia el cumplimiento de los compromisos en materia de verdad, justicia y reconciliación requeridos por el Acuerdo Final de Paz"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Reconociendo los avances también destacaron la importancia de implementar todos los aspectos del Acuerdo Final de Paz, los cuales señalan que deben incluir *"la reforma rural, la participación política, la lucha contra las drogas ilícitas, incluyendo los programas de sustitución de cultivos, y la justicia transicional".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último en medio de este [encuentro virtual](https://colombia.unmissions.org/comunicado-de-prensa-del-consejo-de-seguridad-sobre-colombia-sc14332)los miembros del Consejo de reiteraron la esperanza que se haga posible un cese al fuego inmediato a nivel mundial, al que se se deben sumar *"todas las partes en Colombia por poner fin a la violencia, facilitar la respuesta a la pandemia y afianzar aún más la paz"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este comunicado del Consejo de Seguridad de las Naciones Unidas sobre Colombia, se suma también la declaración del **Representante Especial del Secretario General, Carlos Ruiz Massieu**, quién inicialmente señaló que pese al reconocimiento de logros históricos e innegables en el camino hacia la paz, *"aún quedan por delante enormes retos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Retos que según Massieu parten del compromiso de las partes conformantes a la implementación del Acuerdo, y junto a ello *"la participación activa de las entidades del Estado y la sociedad civil colombiana para garantizar que sea posible seguir construyendo sobre estos logros y que sean realmente sostenibles"*. ([ELN está preparado para negociar un cese al fuego bilateral](https://archivo.contagioradio.com/eln-esta-preparado-para-negociar-un-cese-al-fuego-bilateral/)).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Sr. presidente a pesar de los continuos ataques la gran mayoría de excombatientes siguen comprometidos con la paz": Massieu

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dentro de su declaración Massieu destaca su preocupación en qué avance la consolidación de los Espacios Territoriales de Capacitación y Reincorporación, reconociendo las necesidades que afrontan más de 9.000 excombatientes que viven fuera de ellos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y que además, *"trabajan de la mano con sus familias y las comunidades locales, para resistir a los efectos de la pandemia sobre la salud y la economía en el marco de este 2020".* ([Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto suma también la aprobación de l**a primera hoja de ruta de la reincorporación con enfoque étnico, realizada en el resguardo indígena de Mayasquer, en dónde por medio de las autoridades indígenas** se logró apoyar el proceso formal de reincorporación de 114 excombatientes de origen indígena, un hecho que para el Secretario *"demuestra la importancia de garantizar una perspectiva étnica en el proceso de reincorporación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo destacó que pese a *"los ataques y las estigmatizaciones en contra de los y las firmantes de paz"*, estos continúan con su compromiso en el cumplimiento del Acuerdo de Paz; un compromiso que ha costado la vida de varios de ellos y ellas por tanto Massieu señaló,*"**confío en que el Gobierno y FARC acuerden pronto un enfoque conjunto sobre las formas de prestar un apoyo adecuado a estas nuevas áreas de reincorporación cuyos problemas de seguridad y otras vulnerabilidades ya han sido evidencias en vario informes"**.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Encontrar formas de detener esta violencia es fundamental para cumplir la promesa del Acuerdo de Paz"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último **Massieu resaltó la importancia de implementar la política pública para el desmantelamiento de los grupos armados ilegales**, las organizaciones criminales y sus redes de apoyo, esto ante el aumento de la violencia en varios de los territorios del país, reflejado en el *"asesinato de líderes sociales, defensoras y defensores de Derechos Humanos, excombatientes y comunidades enteras".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Las recientes masacres en varios departamentos han servido como un doloroso recordatorio de como civiles inocentes están siendo víctimas de las acciones de los grupos armados".*
>
> <cite>**Carlos Ruiz Massieu**, **Representante Especial del Secretario General**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Señalo qué *"el trabajo dejación de armas voluntarias por parte del mayor grupo armado del país ha contribuido a la reducción general de la violencia"*, pero pese a ello la violencia continúa y por tanto es fundamental que las organizaciones integrantes de la Comisión Nacional de Garantías de Seguridad den cuenta del trabajo realizado en los últimos dos años para una rápida implementación de esta política pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Junto a ellos también indicó en qué es esencial trabajar de la mano con las comunidades para diseñar medidas de seguridad eficaces adaptadas a los contextos locales, *"me alienta el trabajo conjunto en el marco de los comités técnicos que dieron luz al Decreto 660, pero ahora es necesario avanzar hacia su pronta implementación en el territorio".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/AP95ipkcNWc","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/AP95ipkcNWc

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
