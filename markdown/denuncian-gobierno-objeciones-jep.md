Title: Denuncian fuerte arremetida de Gobierno para que senadores aprueben objeciones a la JEP
Date: 2019-04-28 20:29
Author: CtgAdm
Category: Paz, Política
Tags: Cambio Radical, Congreso, Objeciones a la JEP, Partido de la U, Partido Liberal, Senado
Slug: denuncian-gobierno-objeciones-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A pocas horas de que se discutan en el Senado de la República las objeciones presentadas por el presidente Iván Duque, las denuncias por la presión del Gobierno comienzan a aflorar. E**l senador Armando Benedettí**, a través de su cuenta en Twitter denunció que arrecian las llamadas de los ministros y altos funcionarios a congresistas, para cambiar su posición y lograr que se aprueben las intenciones del uribismo.

Según lo expresado por Benedetti las postura de los congresistas ya habría cambiado “si el viernes creía que las objeciones estaban hundidas ¡HOY NO LO CREO! Ha habido llamadas de los ministros y super asesores de Palacio a senadores de la U, PL y CR, y todo indica que ahora se están moviendo a favor de ellas. [(Lea también: Gobierno buscará ganar batalla por las objeciones a la JEP en el Senado) ](https://archivo.contagioradio.com/gobierno-batalla-objeciones-jep-senado/)

> OBJECIONES: si el viernes creía que las objeciones estaban hundidas ¡HOY NO CREO! Ha habido llamadas de los ministros y de súper asesores de Palacio a senadores de la U, PL y CR, y todo indica que ahora se están moviendo a favor de ellas.
>
> — Armando Benedetti (@AABenedetti) [April 29, 2019](https://twitter.com/AABenedetti/status/1122652382150713344?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Esta denuncia fue confirmada por el senador del Polo Democrático Iván Cepeda, quien sostuvo que el "evidente desespero" del Gobierno, tras la decisión de la Cámara de Representantes de rechazar las objeciones de Duque, ha llevado a que el ejecutivo cambie de estrategia para lograr la aprobación de los cambios a la ley estatutaria.

Por un lado, el senador sostiene que tiene conocimiento de que "se está llamando a los congresistas individualmente a pedirles que pasen por el Ministerio de Hacienda o por otros ministerios para que puedan recibir su cuota respectiva para cambiar el voto en la mañana de hoy". (Le puede interesar: "[Denuncian fuerte arremetida de Gobierno para que senadores aprueben objeciones a la JEP](https://archivo.contagioradio.com/denuncian-gobierno-objeciones-jep/)")

Además, el senador indica que en el debate que se realizó en el Senado sobre las modificaciones a la ley estatutaria, el Gobierno ha concentrado sus argumentos en solo dos de los seis objeciones, que tienen que ver con la entrada de terceros al Sistema Integral de Verdad, Justicia, Reparación y No repetición (SIVJRNR) y su presunto beneficios a la no extradición.

### **Precedentes**

Esta no es la primera vez que se denuncia que el Gobierno, a través del senador Ernesto Macías, retrasa la discusión para buscar mover la postura de varios congresistas. Uno de los primeros en advertir esta situación fue **el senador Cepeda que interpuso una recusación en contra de los magistrados** que estudiarían la tutela interpuesta por Macías por supuestos vicios de trámite.

Así las cosas, podría desatarse una nueva discusión dado que algunos analistas aseguran que si una de las dos cámaras niega las objeciones y la otra las aprueba se definen como no debatidas y tendrían que volver a la Corte Constitucional. Sin embargo la intensión del gobierno es otra y seguramente habrá otras interpretaciones que retrasarían aún más el avance de la JEP:

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
