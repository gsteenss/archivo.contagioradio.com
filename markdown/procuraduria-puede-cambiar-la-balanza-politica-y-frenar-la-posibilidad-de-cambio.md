Title: Procuraduría puede cambiar la balanza política y frenar la posibilidad de cambio
Date: 2020-08-14 20:11
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Fernando Carrillo, Gobierno Duque, Procuraduría General de la Nación
Slug: procuraduria-puede-cambiar-la-balanza-politica-y-frenar-la-posibilidad-de-cambio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/image.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/17758501_1444936275558999_873846949564684177_o-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Procuraduría

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Corte Suprema de Justicia y el Consejo de Estado eligieron dos de los integrantes de la terna para escoger al nuevo procurador general de la nación en reemplazo de Fernando Carrillo, a su vez, el Gobierno postuló a su candidata en medio de advertencias de sectores que señalan, tendría como objetivo generar un desbalance en las ramas del poder.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Diana Sánchez, vocera de la Asociación Minga** reconoce que con la llegada de Fernando Carrillo a la Procuraduría en 2017, esta pudo llegar a los territorios y reconocer la labor de los defensores y defensoras de DD.HH. y los liderazgos sociales además de apoyar la implementación del Acuerdo de Paz y respaldar la lucha de las víctimas para alcanzar la garantía de sus derechos, "a Fernando Carrillo le correspondió levantar la imagen de la institución al venir de un periodo nefasto con Alejandro Ordóñez que cerró la Procuraduría a los DD.HH, la pluralidad y a la diversidad".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Daniela Gómez , coordinadora democracia y gobernabilidad en[Paz y Reconciliación](https://twitter.com/parescolombia)** resalta que aunque el procurador Carrillo significó un cambio importante respecto a su antecesor, no hay que dejar de lado que el actual procurado más allá de defender los DD.HH. sido suficiente más que defender los DD.HH. está en medio de una carrera política.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Expresa, que la investigación a agentes del Estado que por no cumplir con sus labores por acción u omisión han comprometido la vida de los ciudadano no ha sido suficiente. Por ejemplo, según el Índice Global de Impunidad, Colombia ocupa el quinto lugar en América Latina en el Índice Global de Impunidad sólo detrás de Venezuela, México, Perú y Brasil, y el octavo en el ámbito internacional de los 59 países que se pudieron medir.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gobierno busca coptar instituciones del Estado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La analista de Paz y Reconciliación explica que **la Procuraduría cuenta con más de 4.100 cargos, por lo que se convierte en un importante botín burocrático para los partidos,** pero además tiene un poder sancionador que es de importancia para los Gobiernos. [(Lea también: Defensoría del Pueblo será repartida como cuota política del gobierno Duque)](https://archivo.contagioradio.com/defensoria-del-pueblo-cuota-politica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con la postulación de J**uan Carlos Corté**s, antiguo viceprocurador, postulado por parte del Consejo de Estado, **Wilson Ruíz,** procurador delegado durante el periodo de Alejandro Ordóñez, propuesto por la Corte Suprema de Justicia y la confirmación de la postulación de **Margarita Cabello,** ministra de Justicia de Iván Duque, finalmente se conoce la terna de la que surgiría el próximo procurador.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante estas opciones, Diana Sánchez señala que se debe elegir a la personas más independiente, que no haga parte del partido de Gobierno para mantener un equilibrio entre las ramas del poder y sobre todo que sea una persona que se haya pronunciado en favor del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Ana María Rodríguez, vocera de la Alianza de Organizaciones sociales y afines y subdirectora de Comisión Colombiana de Juristas.** por su parte, resalta la necesidad de que la Procuraduría debe utilizar sus herramientas para hacer un seguimiento a instituciones no solo en la teoría sino en la práctica, agrega que quien esté a la cabeza de la institución debe poseer conocimientos en Derecho Internacional Humanitario, implementación del Acuerdo de Paz y voluntad de avanzar en la protección de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Igualdad de oportunidades en las ternas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las tres analistas concuerdan en la necesidad de una paridad de género, conscientes de la importancia que existen mujeres con la capacidad de cumplir con estos cargos y que pueden dar "un salto en las prácticas machistas".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Estamos cansadas de que las mujeres sean utilizadas para rellenar las ternas de los hombre cercanos a partidos políticos que ya tienen cocinada su elección"**, denuncia Ana María Rodríguez con relación a cómo se evidencian ternas prefabricadas donde ya se sabe quién es el ganador, mientras los demás participantes son solo una apariencia ser transparente y meritocrático,

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Daniela Gómez advierte que es factible que Margarita Cabello, cuota del presidente Duque puede alcanzar el cargo de procuradora, expresa que la actual ministra de Justicia no representa los intereses de la mujer en la Procuraduría sino que por el contrario es más cercana al pensamiento del exprocurador Ordóñez.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Si los partidos se quedan con la Procuraduría, nos podemos olvidar de los procesos de control, esta representa un poder de esconder mis errores y de perseguir a los sectores que piensan diferente, es un riesgo grande
>
> <cite>Daniela Gómez</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la analista señala que aunque en la mayoría de países el ministerio público que recoge Procuraduría, Contraloría y Defensoría en un solo cargo es nombrado por el presidente, Colombia posee un cierto equilibrio en las instituciones, sin embargo sus cargos se siguen politizando, lo que trasciende de un problema de procedimiento a un problema ético.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué pasaría si la Procuraduría pierde su independencia?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la posiblidad de que se repita una Procuraduría como la de Alejandro Ordóñez, con candidatos como Wilson Ruiz o Margarita Cabello; Ana María Rodríguez señala que lo más preocupante es la posibilidad de que existan retrocesos en la garantía de DD.HH. como mandato constitucional de la institución, "por eso no cualquier persona puede ocupar este asiento, ya vimos en el pasado lo que es contar con un procurador que persigue a sectores de la sociedad por su filiación política, su orientación sexual o religión y no queremos volver a eso".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Gómez advierte sobre el riesgo de que se engaveten los procesos de investigación frente a abusos de poder por parte de miembros de instituciones y de los entes estatales, además del riesgo de una persecución contra sectores políticos que piensan diferente, en particular **"un momento de apertura sin precedentes en el que las pasadas elecciones, la mayor parte de candidatos representaron partidos alternativos"**, por lo que el tener influencia en instituciones como la Fiscalía, Registraduría y Procuraduría puede cambiar la balanza política y frenar el cambio que se ve en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como advertencia final, la vocera de la Alianza de Organizaciones sociales y afines alerta sobre los cambios que puede haber desde la Procuraduría hacia el Acuerdo de Paz y el Sistema Integral de Justicia que en la actualidad ha dado un amplio respaldo a las medidas cautelares implementadas por la JEP, y **que de no ser respaldadas tendría un impacto negativa sobre todo en los procesos judiciales que se adelanten en esta jurisdicción.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
