Title: Con fallas técnicas y tecnología obsoleta en Europa, se estrenó flota de Transmilenio
Date: 2019-06-19 16:45
Author: CtgAdm
Category: Ambiente, Política
Tags: Bogotá, Movilidad, Transmilenio
Slug: con-fallas-tecnicas-y-tecnologia-obsoleta-en-europa-se-estreno-flota-de-transmilenio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Transmilenio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Nataliaescribe] 

Aunque tan solo han transcurrido un par de días desde que entraron en operación l**os primeros 336 buses, de los 1.441 que renovarán la flota de Transmilenio**. Sin embargo, varios de estos vehículos presentaron fallas e inconvenientes diversos que provocaron un descontento aún mayor en la ciudadanía bogotana.

**Según el concejal Manuel Sarmiento**, estos buses  son recién ensamblados pero cuentan con una tecnología Euro V, la que "ya está prohibida en Europa debido a sus niveles de contaminación" y lo que se debió hacer, sería recurrir a la Euro VI que es considerada la más avanzada en términos de transporte público.

> [\#TransmiSeRenueva](https://twitter.com/hashtag/TransmiSeRenueva?src=hash&ref_src=twsrc%5Etfw) en serio? Señores y señoras de [@TransMilenio](https://twitter.com/TransMilenio?ref_src=twsrc%5Etfw) les parecen estas las llantas de un bus nuevo? Esto no es detrimento patrimonial [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) ? [@personeriabta](https://twitter.com/personeriabta?ref_src=twsrc%5Etfw) [pic.twitter.com/anOMNNqFzw](https://t.co/anOMNNqFzw)
>
> — Andres Felipe Garcia (@afgarcialo) [18 de junio de 2019](https://twitter.com/afgarcialo/status/1141056503769686016?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Sarmiento señala que  fue licitado este tipo de tecnología porque según la gerencia de Transmilenio, Ecopetrol no producía el diesel requerido para la tecnología Euro VI, afirmación que desmiente el concejal quien encontró una carta dirigida desde la presidencia de Ecopetrol a Transmilenio explicando que sí estaban en capacidad de producir combustible. [(Lea también: Frenar el cambio climático en Bogotá no será posible con la nueva flota de Transmilenio)](https://archivo.contagioradio.com/nueva-flota-de-transmi/)

Agregó que el asesor de la Alcaldía se reunió en al menos 12 ocasiones con Volvo, empresa que únicamente maneja la tecnología Euro V y que pese a ofrecer vehículos obsoletos fue la empresa que al final se quedó con la mitad de la nueva licitación.

### Nueva flota de Transmilenio "impone una tecnología obsoleta" 

Aunque Transmilenio afirma que esos fallos son normales, el concejal indica que esta no es una explicación satisfactoria para los usuarios y para la ciudad, **"No es la primera vez que entran nuevos buses al sistema y no han sucedido estos problemas de calibración, ahí hay algo que falta por explicar"**

Sarmiento señala además, que por ser en su mayoría buses biarticulados, las puertas de los vehículos no coinciden con las compuertas de las estaciones, las que no están adecuadas para la nueva flota, lo que pone en riesgo la integridad de los usuarios y afecta la operación del sistema, algo que según el concejal pone en evidencia **"la improvisación del alcalde Peñalosa".**

Aunque la nueva flota garantiza una menor contaminación, el concejal señaló que esta no es una forma de justificar la adquisición de los nuevos modelos, pues se pudo dar un paso mucho más avanzado y que contribuyeran a la mejor del ambiente y el aire que respiran los bogotanos.

¿Era cierto que la administración de [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) no podía exigir la tecnología más avanzada en los "nuevos" buses de Transmilenio?

> La verdad de los buses prohibidos en Europa que rodarán en Bogotá ?
>
> Video completo: <https://t.co/kROTd7vngJ> [pic.twitter.com/WWU2Nrz0dy](https://t.co/WWU2Nrz0dy)
>
> — Manuel Sarmiento (@mjsarmientoa) [17 de junio de 2019](https://twitter.com/mjsarmientoa/status/1140768018068361216?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37332141" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37332141_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
