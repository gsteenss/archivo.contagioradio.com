Title: No se detienen hostigamientos a líderes sociales en Colombia
Date: 2018-07-16 12:18
Category: DDHH, Nacional
Tags: Catatumbo, Cauca, Hostigamientos, lideres sociales, violencia
Slug: dos-lideres-sociales-fueron-victimas-de-hostigamientos-durante-el-fin-de-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Jul 2018] 

Dos líderes sociales fueron víctimas de hostigamientos durante este fin de semana en Colombia. Se trata de **Héctor Marino Carabalí,** líder del municipio de Buenos Aires, Cauca y de **Olgar Antonio Pérez**, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT), víctima de un intento de asesinato cuando se dirigía en su esquema de seguridad a Ocaña en Norte de Santander.

### **Este año han sido asesinados 3 líderes de ASCAMCAT** 

De acuerdo con un comunicado de prensa de la organización ASCAMANCAT, Olgar Pérez se dirigía a una reunión con la mesa Humanitaria, cuando su esquema se percató que en la carretera había **4 hombres fuertemente armados, razón por la cual decidieron aumentar la velocidad y pasarlos.**  Sin embargo, la camioneta alcanzó a recibir varios disparos de bala.

Según el comunicado, Pérez salió ileso del atentado debido a la rápida acción de su esquema de seguridad. Actualmente el líder es el responsable desde la Asociación Campesina del Catatumbo, de hacer seguimiento a la implementación de los Acuerdos de Paz de La Habana y cabe recordar que su **hermano Álvaro Pérez**, también líder e integrante de esta plataforma, fue asesinado el pasado 5 de abril en Tibú, Norte de Santander, hechos a los que se suman el asesinato de dos integrantes más de esta organización, durante este semestre.

### **Líderes del Cauca continúan siendo amenazados y perseguidos** 

Durante el fin de semana, se dio a conocer un panfleto firmado por el grupo paramilitar las Águilas Negras en donde se amenazaba a diferentes organizaciones defensoras de derechos humanos, políticos y líderes sociales, **entre ellos Héctor Marino, quien al mismo tiempo denuncio el secuestro de uno de sus familiares.**

Héctor Marino Carabalí es líder social de víctimas y étnico en el municipio de Buenos Aires Cauca, además es el representante legal del Consejo Comunitario de la Cuenca del Río Timba Marilópez y representante legal de la Asociación de Víctimas Renacer siglo XXI.

En las últimas horas, en este mismo departamento, la red de derechos humanos Francisco Isaías Cifuentes, denunció el asesinato del líder comunitario Luis Eduardo Dagua, en el municipio de Caloto. (Le puede interesar:["Hallado sin vida el líder social Luis Eduardo Dagua en Caloto Cauca"](https://archivo.contagioradio.com/encontrado-sin-vida-el-lider-social-luis-eduardo-dagua-en-caloto-cauca/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
