Title: Captura de abogada Natalia Trujillo sería otro falso positivo judicial para mostrar “resultados”
Date: 2017-06-24 21:11
Category: Judicial, Nacional
Tags: capturas masivas, congreso de los pueblos, Mateo Gutierrez
Slug: natalia-trujillo-falso-positivo-judicial-para-mostrar-resultados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Natalia-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AP] 

###### [24 Jun 2017]

La noche de este 24 de Junio se conoció la noticia de la captura de 8 personas, que en principio, estarían implicadas con el llamado Movimiento Revolución Popular al que se le atribuyen varias explosiones en Bogotá. Entre las personas capturadas hay varios egresados de la universidad nacional y defensoras de DDHH de amplia trayectoria como Natalia Trujillo a favor de quien ya se han escuchado varios pronunciamientos.

Según varias organizaciones de Derechos Humanos con los que está vinculada la abogada Natalia Trujillo Nova, su captura,  podría ser otro falso positivo judicial y un nuevo ataque contra organizaciones de Derechos Humanos, como contra la Universidad Nacional, dado que varios de los capturados la tarde de hoy serían egresados del Alma Mater.

Natalia Trujillo es muy reconocida por su trabajo en varios espacios como Sisma Mujer o Idipron y actualmente se desempeña como abogada asociada de la organización Defensa de Niños Internacional, como defensora de los derechos de los niños y las niñas víctimas de la guerra en nuestro país.

Según Fernando Sabogal, director de DNI  en su desempeño profesional ha sido excelente y por ello extraña mucho que sea acusada de delitos imputados al Movimiento Revolucionario Popular, además señala que es muy sospechoso que se trate de egresados de la UN. Natalia además es abogada de la Universidad Nacional y especialista en derecho constitucional.

### **Nuevamente los titulares de los medios masivos no corresponden a las acusaciones reales** 

Adicionalmente, algunas organizaciones de Derechos Humanos han señalado que nuevamente los medios de información están haciendo acusaciones que no tendrían que ver con los cargos que se imputan realmente por parte de la fiscalía, dado que mientras que algunos titulares señalan que las capturas se dieron como resultados de las investigaciones por explosión en el Andino, la fiscalía señala que se trata de integrantes del MRP y responsables de otras explosiones. [Lea También: Líderes del Congreso de los Pueblos denuncian persecución judicial.](https://archivo.contagioradio.com/persecucion-judicial-congreso-de-los-pueblos/)

Situaciones parecidas, en que los titulares de la prensa no corresponden con los cargos que imputa la fiscalía, tienen que ver con los 12 estudiantes detenidos en 2015 a quienes los medios responsabilizaron de una serie de atentados en Bogotá, **pero la fiscalía solamente los acuso por una protesta en la Universidad Nacional**.

Otro de los casos es el de Mateo Gutierrez a quien se señaló como responsable de la explosión en las inmediaciones de la Plaza de Toros, en que murió un policía. Durante los primeros días de la captura se conoció que la acusación era por una bomba panfletaria en 2016. [Lea también: Mateo Gutierrez es otra víctima de Falso Positivo Judicial](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
