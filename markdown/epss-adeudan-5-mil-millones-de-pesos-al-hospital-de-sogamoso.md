Title: EPS's adeudan 5 mil millones de pesos al Hospital de Sogamoso
Date: 2015-09-29 12:03
Category: Movilización, Nacional
Tags: ANIR, crisis de la salud, Derechos Humanos, Hospital de Sogamoso, Movilizaciones, movimiento nacional octava papeleta por la salud y seguridad social, Radio derechos Humanos
Slug: epss-adeudan-5-mil-millones-de-pesos-al-hospital-de-sogamoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Sogamosos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 20minutos.es 

<iframe src="http://www.ivoox.com/player_ek_8685966_2_1.html?data=mZull56aeo6ZmKiak5eJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibHHhpek1ZDFqMbpxcbbjZqPscrgjNLWztHTssbnjMnSjdXJt9DnjMbZja3Tt9Hd1cbZjcnJb7TjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fabio Serna, Octava Papeleta] 

###### [29 sep 2015] 

El hospital Regional de Sogamoso en Boyacá también se ha visto afectado por la **crisis de la salud.** Incumplimientos de las EPS's con los pagos por **5000 millones de pesos**, deudas de los entes gubernamentales por los recobros, falta de médicos y falta de garantías laborales son algunos problemas con los que hoy se enfrenta el centro hospitalario.

En este hospital trabajan** entre 600 y 700 personas**, las cuáles se ven duplicadas por los pacientes que **diariamente oscilan entre los 2000 y 3000**, quienes asisten desde más de 10 cabeceras municipales cercanas.

Al problema de la falta de trabajadores de planta, se le suma la **tercerización laboral y falta de garantías** a las que se ven expuestos los trabajadores, según denuncia Fabio Serna, integrante y dirigente del movimiento nacional Octava Papeleta por la salud y seguridad social como derechos fundamentales en Colombia.

Serna también denuncia que las EPS "*prefieren contratar con clínicas de garaje, que con el hospital público*", el cual, pese a las dificultades ha realizado una labor reconocida por la comunidad.

Por ello el próximo miércoles 30 de Septiembre se iniciará una de las rondas de audiencias públicas en el marco del juicio ético y político contra el "negocio" de la salud. El evento tendrá lugar en el auditorio principal de la UPTC y concluirá con una marcha hasta el amenazado hospital.

Para esta jornada los estudiantes de la Universidad Pedagógica y Tecnológica participarán con una ponencia que indica cómo desde la academia se forman profesionales “***deshumanizados en contra del derecho a la salud***”, bajo el modelo de la ley 100, afirma Serna.

Después de Sogamoso, estas audiencias se realizarán en Cali, Bogotá y otros departamentos, en donde se buscará realizar enjuiciamientos éticos y políticos sobre la salud. La jornada en Sogamoso finalizará mañana con movilizaciones.
