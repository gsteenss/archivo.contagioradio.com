Title: Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada
Date: 2016-08-30 18:13
Category: DDHH, Nacional
Tags: Desaparición en Colombia, Desaparición forzada Colombia, impunidad en Colombia
Slug: fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Investigaciones-Fiscalía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo ] 

###### [30 Ago 2016] 

Frente al número de personas desaparecidas en Colombia no hay certeza, por un lado Medicina Legal registra 23.441 desapariciones forzadas y la Unidad de Víctimas establece que hay al menos 45.944 desaparecidos en el marco del conflicto armado interno y un total de 116.344 víctimas indirectas. A esta falta de precisión, se suma el índice de impunidad que es del 99%, principalmente porque **en la Fiscalía General están en curso solo 6 mil investigaciones y 35.700 han prescrito**.

De acuerdo con Luz Marina Hache, integrante del Movimiento Nacional de Víctimas de Crímenes de Estado MOVICE, ninguna de las investigaciones que realice la Fiscalía sobre desaparición forzada pueden ser suspendidas porque **este delito ha sido catalogado como imprescriptible**, por lo que para las organizaciones de víctimas y familiares estas acciones terminan agravando el problema y revictimizando a las [[cientos de familias que esperan que el Estado colombiano restituya sus derechos](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)].

Buenaventura es una de las regiones en las que el fenómeno de la desaparición forzada se ha manifestado de una forma mucho más cruel, asegura Marta Giraldo quien en insiste en que [[el Estado colombiano no se ha esforzado como debería](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/)] para encontrar a las personas desaparecidas por lo que espera, junto a más familiares de víctimas, **que la implementación de los acuerdos de paz permita dar con el paradero de quienes han desaparecido** para terminar con la incansable búsqueda y darles cristiana sepultura.

Al final de la jornada, la organización Hijas e Hijos por la identidad y la justicia contra el olvido y el silencio, denunciaron mediante twitter que durante la jornada no contaron con el apoyo de la Alcadía de Bogotá "Lamentable [~~@~~**victimasbogota**](https://twitter.com/VictimasBogota){.twitter-atreply .pretty-link .js-nav} y [~~@~~**centromemoria**](https://twitter.com/centromemoria){.twitter-atreply .pretty-link .js-nav} no apoyaron Día de las Víctimas de Desaparición Forzada, nueva administración solo palabras¡".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
