Title: Cultura una apuesta de transformación en las Cárceles
Date: 2018-07-16 11:01
Category: Expreso Libertad
Tags: carceles, Cultura, presos politicos
Slug: cultura-transformacion-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/156528863_women-in-art-800x500_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: The free weekly 

###### 10 Jul 2018 

Luis Hernando Tangarife, ex prisionero político de las FARC, en su paso por diferentes cárceles del país pudo presenciar cómo a través de la cultura sus compañeros lograban escapar de la realidad de centros de reclusión que para él, nunca dieron una verdadera opción de re-socialización a quienes allí se encuentran.

En este programa del Expreso Libertad hablaremos de la cultura como un escenario para las y los prisioneros del país de generar otros escenarios de encuentro, compañerismo y superación de las adversidades.

<iframe id="audio_27083796" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27083796_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
