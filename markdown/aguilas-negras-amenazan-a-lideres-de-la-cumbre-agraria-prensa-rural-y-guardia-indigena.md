Title: Águilas Negras amenazan a líderes de la Cumbre Agraria, Prensa Rural y Guardia Indígena
Date: 2015-10-07 17:46
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas, Andrés, CGT Colombia, Cumbre Agraria, Derechos Humanos, Eberto Díaz, Gil, guardia indígena, Prensa Rural, Robert Daza
Slug: aguilas-negras-amenazan-a-lideres-de-la-cumbre-agraria-prensa-rural-y-guardia-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Cumbre-Agraria-770x4001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [7 Oct 2015]

Líderes sociales y defensores de derechos humanos integrantes de la Cumbre Agraria, Prensa Rural y la Guardia Indígena, son el nuevo blanco de amenazas por parte del grupo **"Águilas Negras".**

A través de un panfleto que llegó por medio de correo electrónico desde la cuenta libertarioscolombia@gmail.com el pasado martes 6 de octubre a las 8:33 de la noche, el grupo paramilitar **los amenazó de muerte y se declaran en contra del proceso de paz.**

Frente a este hecho los líderes sociales, hacen un llamado a "las organizaciones defensoras de derechos humanos y de la libertad de prensa a que se pronuncien y solidaricen en estos momentos en que Colombia tiene la posibilidad de acabar con décadas de conflicto armado, guerra que los sectores retardatarios quieren continuar a toda costa", según comunicó Prensa Rural.

[![Aguilas negras 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Aguilas-negras-1.png){.wp-image-15446 .alignnone width="302" height="431"}](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-lideres-de-la-cumbre-agraria-prensa-rural-y-guardia-indigena/aguilas-negras-1/) [![Aguilas Negras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Aguilas-Negras.png){.wp-image-15447 .alignnone width="317" height="430"}](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-lideres-de-la-cumbre-agraria-prensa-rural-y-guardia-indigena/aguilas-negras/)
