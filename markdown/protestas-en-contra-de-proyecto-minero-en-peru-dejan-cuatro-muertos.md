Title: Protestas en contra de proyecto minero en Perú dejan cuatro muertos
Date: 2015-09-30 16:07
Author: AdminContagio
Category: El mundo, Resistencias
Tags: Cotabambas, Dirección Regional de Salud del Cusco, Guoxin International Investment Corporation y Citic Metal Co, minería en Perú, Perú, Protestas por proyecto minero, Proyecto minero las Bambas
Slug: protestas-en-contra-de-proyecto-minero-en-peru-dejan-cuatro-muertos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Protestas-proyecto-minero-Perú.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [eltiempo.pe]

Cuatro muertes de campesinos es el saldo que ha dejado la jornada de paro de la comunidad en la provincia andina de Cotabambas, Perú, en rechazo al proyecto minero Las Bambas, ya que según los habitantes de la zona, **la empresa MMG (Guoxin International Investment Corporation y Citic Metal Co),** modificó el estudio de impacto ambiental sin consultar a la comunidad.

Durante la jornada de protestas del martes, la movilización fue dispersada con gases lacrimógenos y disparos al aire, lo que generó enfrentamientos **dejando 4 muertos, 23 heridos, y 22 detenidos.**

Según la Dirección Regional de Salud del Cusco, las personas fallecidas son Uriel Elguera Chilca, (34), Beto Chahuallo Huillca (24), Alberto Cárdenas Chalco (23) y Exaltación Huamaní (30).

La comunidad denuncia que la actividad minera de la empresa  ha aumentado los **índices de contaminación, los niños y niñas presentan plomo en la sangre, la salud de los habitantes está en peligro y los cultivos** de la zona podrían verse gravemente afectados.

Aunque las autoridades peruanas activaron el despliegue de fuerzas y aplicaron la suspensión de los derechos de libertad individual, reunión, libre tránsito e inviolabilidad domiciliaria, las manifestaciones de los habitantes de Cotabambas continúan.
