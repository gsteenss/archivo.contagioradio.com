Title: ¡Desde ahora se penalizará el maltrato animal en Colombia!
Date: 2016-01-06 16:42
Category: Animales, Nacional
Tags: Juan Manuel Santos, Ley Animal, Maltrato animal
Slug: desde-ahora-se-penalizara-el-maltrato-animal-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-9-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagio Radio 

###### [6 enero 2015]

Hace algunos minutos, el presidente Juan Manuel Santos firmó la Ley 172 que penaliza el maltrato animal en Colombia, lo que quiere decir que desde el día de hoy **el país cuenta con una Ley que ve a los animales como seres sintientes.**

Gracias al trabajo de organizaciones animalistas junto a Juan Carlos Losada, autor del proyecto, ahora los colombianos y colombianas podrán denunciar penalmente alguna situación de maltrato animal de la que conozcan.

La aprobación del proyecto de [Ley 172 tipifica el delito de maltrato animal](https://archivo.contagioradio.com/?s=ley+animalista), como lo explica el autor de la Ley, **“da de 12 a 36 meses de cárcel con unos agravantes, cuando se comete el maltrato enfrente de menores de edad, cuando el maltrato es de tipo sexual, conocido como zoofilia o bestialismo, eso tendrá penas mayores, cuando se trate de un funcionario público”, **además  genera multas de hasta 60 salarios mínimos mensuales vigentes para quienes maltraten un animal, así mismo abandonar a un animal será una conducta que será castigada con multas entre dos y 20 salarios mínimos.
