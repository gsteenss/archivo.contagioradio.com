Title: Bogotá ¿cómo vamos?, treinta y un días de gestión
Date: 2016-02-01 10:45
Category: Opinion, Tatianna
Tags: Alcaldía Enrique Peñalosa, Gustavo Petro, Reserva Vander Hammen
Slug: bogota-como-vamos-treinta-y-un-dias-de-gestion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/penalosa-enr07pvg003x.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ola política 

#### **[Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]

###### 1 Feb 2016. 

Todo inició con una venganza al partido que, se “robó” a Bogotá; pensar que el alcalde saliente no era del Polo, y que el supuesto señor que se robó la plata por cuenta del carrusel no ha sido condenado. Pensar que Clara López se iba a robar las elecciones de Bogotá a punta de testigos electorales –lo que dejaba ver lo poco que conocen de la función de un testigo electoral- o que Pacho Santos era el único que sabía el valor de una tarifa mínima en taxi, y el valor del pasaje en transmilenio, pues bien, el ganador fue el señor que posó de independiente, el que recogió firmas en Unicentro para su campaña, el que creíamos que su negocio era vivir de la reposición de votos y no de grandes tajadas de contratos a sus amigos pagando favores.

Pues bien; ningún medio hará un balance de gestión a los treinta o treinta y un días del mejor alcalde urbanista del mundo, porque claro, éste si ganó con la mayoría de votos – el mismo porcentaje que eligió al alcalde saliente- , éste si sabe lo que es dirigir una ciudad y sobretodo sabe lo que es tener conciencia verde – ya ni verde tiene el partido-.

Uno de los primeros anuncios del alcalde fue el recorte de presupuesto a hospitales públicos en un 40%, no sé si paradójicamente, una señora muere por falta de atención médica y colapsa la ciudad; un anuncio de la subida de la tarifa de transmilenio, escandalosa por la inseguridad, la ineficiencia y la incomodidad del sistema, ¡quién se atreverá a decir algo! Si es que su promotor –para el que transmilenio y un metro hacen lo mismo en la práctica-, es quién está haciendo el ajuste, porque claro, un pasaje mantiene su valor comercial, se crean unos subsidios pero el pasaje bajó, y todo es culpa del ojibrotado guerrillero que se negó a subir el pasaje.

Mucho se habló de la percepción de seguridad, claro, en el gobierno del alcalde guerrillero la percepción de inseguridad era altísima: todos los días en todas las emisiones de los noticieros veíamos atracos, muertes –ninguna en la calle aclaro-, peleas, accidentes, los huecos cumplían años, en cambio, en este gobierno, seguimos viendo lo mismo pero con una policía ineficiente casi que perspicaz, siempre dije que era cuestión de perspectiva, ahora creo que es cuestión de tener a los medios en el bolsillo o no.

En treinta y algo de días, se han cerrado jardines, se suspendieron las licitaciones para el transmilenio por la avenida Boyacá, el cambio de buses ecológicos, se desalojaron a vendedores ambulantes, pero no se preocupen, que viene lo mejor, lo mejor para él y sus amigos: la urbanización de la Reserva Van der Hammen, la construcción de la ALO, la privatización de la ETB y de la Empresa de Energía de Bogotá, el cierre de la alcahuetería de los CAMAD, por la séptima volverán los carros–ya quitaron las materas que guardaban drogas y eran baños de indigentes- se acabarán los subsidios para estratos bajos, cambiará el modelo de recolección de basuras porque, el anterior alcalde todo lo improvisaba, en cambio él no decide cada cosa mientras se lava los dientes.

Finalmente, aunque treinta y un días no sirven para evaluar la gestión de un alcalde, ni mucho menos sirven para una revocatoria –propuesta que me parece estúpida- auguro para Bogotá muchas más de estas peñalosadas, jugadas en favor de unos pocos disfrazadas de gestiones para el bien de todos, y claro, ustedes pueden celebrar que un alcalde quite un grafitti, o que quite publicidad de un poste y le ponga un letrero diciendo que lo recuperó, sigue siendo como lo dije en el gobierno de Petro –por el que tampoco voté- que todo es cuestión de perspectiva.

Le va quedar entonces muy duro gobernar al alcalde que tiene al concejo y a los medios a su favor, que llegó siendo el Mesías –ya existió un Mesías que disfrazó los falsos positivos de seguridad democrática- , endiosado por miles de personas que olvidaron las cifras de pobreza, desempleo e inequidad que dejó en su alcaldía, pero bueno, todos merecemos equivocarnos de nuevo, ya que se equivocaron votando, ¡Que Dios –el que sea- se apiade de Bogotá!
