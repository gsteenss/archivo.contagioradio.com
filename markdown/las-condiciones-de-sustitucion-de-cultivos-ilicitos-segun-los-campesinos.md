Title: Las condiciones de sustitución de cultivos ilícitos según los campesinos
Date: 2016-07-18 19:20
Category: Nacional, Paz
Tags: cultivos de usos ilícito, gobierno FARC, paz
Slug: las-condiciones-de-sustitucion-de-cultivos-ilicitos-segun-los-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Cultivos-de-coca-e1468887460603.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [18 Julio 2016 ]

El 95% de la población en Briceño, Antioquía, vive directa e indirectamente de los cultivos de uso ilícito, es decir, por lo menos 600 familias de once veredas, que sienten temor por los desafíos que plantea el **cambiar en menos de 2 meses lo que se ha constituido su modo de vida de los últimos 20 años**.

Pese al miedo, que es apenas natural, **tienen toda la disposición para aportar al plan piloto de sustitución** que se pondrá en marcha en el marco de las negociaciones de paz, y según afirma Fabio Muñoz del Movimiento Ríos Vivos, parte de las exigencias que plantean a las partes son el desmonte de los grupos paramilitares, mayor inversión en infraestructura y educación, todo ello en el marco del punto uno de la agenda pactada en La Habana.

Muñoz asevera que esperan que en este plan piloto se tengan en cuenta las **propuestas que han construido desde hace cuatro años las familias campesinas** en lo que han denominado los espirales agroecológicos, para construir planes de vida digna para las comunidades en la lógica de la preservación del medio ambiente y la protección de los derechos humanos.

<iframe src="http://co.ivoox.com/es/player_ej_12263474_2_1.html?data=kpefmJiYe5Whhpywj5aaaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjbLZaaSnhqee0d-JdqSfs4qwlYqliNDnjLvW2NTXb6Li1ZOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
