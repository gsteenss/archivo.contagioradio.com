Title: ANLA y Ministerio del Interior adelantaban consulta previa ilegal en Parque Tayrona
Date: 2015-04-20 12:24
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Álvaro Echeverry Londoño, ANLA, Autoridad Nacional de Licencias Ambientales, Consulta Previa, Dirección de Consultas Previas, Fernando Uregui Mejía, MinInterior, Ministerio del Interior, Parque Tayrona, Reserva Los Ciruelos, Santa Marta
Slug: anla-y-ministerio-del-interior-adelantaban-consulta-previa-ilegal-en-parque-tayrona
Status: published

##### Foto: [decolombiacasa.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4387994_2_1.html?data=lZilmZ6deI6ZmKiakp6Jd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRha%2FAopDmjbLNssrn1crfy9SPqMbgjK7b1srWrdDmjMbRx9HFstXVw8bbjcjTstTpzdnOjdXWqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Arias, abogado de la Confederación Indígena Tayrona] 

La ANLA y la Dirección de Consulta Previa del Ministerio del Interior, adelantaron procedimientos ilegales de consulta previa en las comunidades indígenas que habitan en el Parque el Tayrona, con el objetivo de llevar a cabo **la construcción hotelera Los Ciruelos.**

**Fernando Uregui Mejía, director de la Autoridad Nacional de Licencias Ambientales, ANLA, y Álvaro Echeverry Londoño**, de la Dirección de Consulta Previa del Ministerio del Interior, serán sancionados por el Tribunal Administrativo del Magdalena, debido a que los entes desacataron un fallo que ese tribunal había emitido en el año 2013, a través del cual se decidiría sobre la posible construcción del hotel Los Ciruelos en el parque Tayrona.

El Tribunal había ordenado que la Unidad de Parques Nacionales tendría la última palabra sobre el proyecto hotelero, de acuerdo a su concepto técnico, donde finalmente se determinó que “las actividades que desarrollaría la Reserva Los Ciruelos S.A.S. en el interior del Parque Tayrona **generarán un daño grave e irreversible al área protegida ya que se afectarán los ecosistemas presentes en la zona,** principalmente el bosque seco presente en el área de influencia. Esto debido a que el proyecto implica el establecimiento de infraestructura para alojamiento que reemplazaría los ecosistemas naturales”.

Es decir, que el proyecto Los Ciruelos no sería viable y por lo tanto, **era imposible adelantar procedimientos de consulta previa.** Sin embargo, pese a esa conclusión, las comunidades indígenas, representadas en el líder indígena arhuaco Gelver Zapata Izquierdo, denunciaron que funcionarios de la ANLA y Dirección de Consultas Previas del Mininterior, empezaron a visitar a los pobladores  convocando a reuniones de preconsulta.

En febrero de 2015, la comunidad indígena acompañada del abogado Alejandro Arias, periodista y abogado de la Confederación Indígena Tayrona,  demostraron que las entidades habían desobedecido el concepto de Parques Nacionales.

Por el desacato, el director de la **ANLA, y Álvaro Echeverry del Mininterior, deberán pagar \$3’221.700, cada uno**. Así mismo, Fernando Uregui, deberá decidir sobre el proyecto Los Ciruelos, además, los procedimientos de las consultas se deben detener.
