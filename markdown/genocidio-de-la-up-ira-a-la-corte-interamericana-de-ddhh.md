Title: Genocidio de la UP irá a la Corte Interamericana de DDHH
Date: 2018-07-26 16:25
Category: DDHH, Política
Tags: Aida Avella, CIDH, Corte IDH, genocidio, Unión Patriótica
Slug: genocidio-de-la-up-ira-a-la-corte-interamericana-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/up.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha  Patriótica] 

###### [26 Jul 2018] 

La Comisión Interamericana de Derechos Humanos (CIDH) presentó el caso del genocidio del partido político Unión Patriótica (UP) ocurrido desde 1984 y por más de 20 años, ante la Corte Interamericana de Derechos Humanos (Corte IDH). Adicionalmente formuló algunas recomendaciones al Estado colombiano para atender a las víctimas.

Aida Avella, presidenta de la UP y actual Senadora de la República por la Lista de la Decencia, señaló que es una buena señal pues llevan más de 22 años de lucha en los estrados judiciales buscando justicia, por los **más de 6500 casos documentados de graves violaciones de los Derechos Humanos contra integrantes del partido y sus familiares.**

Según la Senadora, también es una noticia importante dado que **la impunidad en casos de la UP es del 97,6%**; razón que le permite pensar que es posible encontrar justicia en este nuevo proceso ante la Corte Interamericana, cuyos fallos no pueden ser apelados y son de obligatorio cumplimiento. (Le puede interesar: ["Aquí no se han desmontado las estructuras paramilitares: Aida Avella"](https://archivo.contagioradio.com/aqui-no-se-han-desmontado-las-estructuras-paramilitares-aida-avella/))

### **La CIDH determinó la responsabilidad del Estado en el caso** 

La Comisión Internacional investigó los hechos que involucran amenazas; hostigamientos; desplazamientos forzados; tentativas de homicidios; y desapariciones forzadas, y determinó que fueron **perpetrados "por agentes estatales como actores no estatales con la tolerancia y aquiescencia de aquellos".**

Por ello, Avella señaló que las pruebas presentadas ante la Comisión fueron tan relevantes que el Estado debió asumir su responsabilidad por el incumplimiento, en su deber de garantizar la protección de los ciudadanos, y por no prevenir los asesinatos y demás actos de violencia que vivieron los miembros del partido político.

También, la Comisión determinó que "las víctimas del presente caso fueron constantemente **estigmatizadas a través de declaraciones de funcionarios públicos y actores no estatales"**, afirmaciones que incluyen la acusación de terroristas, hecho que tuvo un grave efecto en la violencia sufrida por la UP.

### **"Buscaremos llevar el tema de los líderes sociales ante la Corte IDH"** 

La Senadora, y también víctima de la persecución sufrida por la UP, dijo que la situación que están viviendo los líderes sociales en la actualidad es similar a la padecida por los integrantes de su movimiento político, pues "nadie sabía nada pero **luego se demostró que el Estado estaba presente en buena parte de los crímenes de la UP".**

Precisamente, una de las recomendaciones hechas por la Comisión al Estado colombiano es que se implementaran mecanismos de no repetición, de tal forma que, personas o grupos políticos que desean participar en la vida pública del país no vuelvan a sufrir graves violaciones a los Derechos Humanos. (Le puede interesar: ["Son fundamentales las garantías de no repetición: Aida Avella"](https://archivo.contagioradio.com/son-fundamentales-las-garantias-de-no-repeticion-aida-avella/))

Para Avella, resulta incomprensible que no se haya establecido un esquema de protección colectiva en una zona como Caloto, Cauca, a pesar de la alta militarización del territorio; razón por la cual, pidió tanto al Fiscal como al Ministro de defensa, que se investiguen las amenazas y asesinatos, y afirmó que "cuando el Estado quiere, se saben las cosas; entonces **el Estado no quiere descubrir quienes están matando a los líderes sociales".**

### **Primer paso: Respeto por la diferencia** 

Aida Avella aseguró que **la primera medida de reparación que recibió la UP fue el voto para que ella, en representación del movimiento político, volviera al Senado.** Sin embargo, rescató que el empeño más grande de ahora en adelante será la defensa por la vida, y apuntó, que "hay urgencia de que las personas empiecen a entender que podemos vivir todos con diferencias políticas, sin necesidad de que la gente tenga que morir por sus convicciones". (Le puede interesar: ["El respeto a la diferencia debe ser patrimonio de los colombianos: Gestores de Paz del Valle"](https://archivo.contagioradio.com/la-personas-deben-poder-expresar-ideas-sin-temor-a-ser-asesinadas-red-de-gestores-de-paz/))

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
