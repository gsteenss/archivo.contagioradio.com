Title: En Colombia hay un déficit de 300 controladores aéreos
Date: 2017-10-09 13:02
Category: Entrevistas, Nacional
Tags: ACDAC, controladores aéreos, Pilotos, seguridad aérea, tránsito aéreo
Slug: en-colombia-hay-un-deficit-de-300-controladores-aereos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/controladores-2p.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia Liberal ] 

###### [09 Oct 2017] 

Tras el anuncio de la Asociación Colombiana de Controladores de Tránsito Aéreo de  iniciar la operación reglamento y apoyar la huelga de los pilotos de ACDAC, voceros del gremio manifestaron que dentro de la Aeronáutica Civil hay problemas que **ponen en riesgo la seguridad aérea** y a los trabajadores les están violando sus derechos.

De acuerdo con Diana Pulido, vocera de la Asociación Colombiana de Controladores de Tránsito Aéreo, hay falencias administrativas de la Aeronáutica Civil como "**la falta de personal, la falta de adecuación de la infraestructura**, la escasez de técnicos aeronáuticos, controladores aéreos y bomberos que hacen parte del equipo misional de la entidad". (Le puede interesar: ["Respaldo de controladores aéreos a pilotos del sindicato de ACDAC"](https://archivo.contagioradio.com/controladores-aereos-inician-operacion-reglamento-en-colombia/))

### **En Colombia hay 630 controladores de los 930 necesarios** 

Pulido manifestó que la operación reglamento indica que los controladores están en la obligación de continuar con sus labores en cumplimiento de la reglamentación aeronáutica. Si bien **siempre han realizado su labor**, están haciendo un llamado a que la Aeronáutica Civil cumpla también las normas.

Así mismo, indicó que **no hay el personal suficiente para desarrollar las labores de los controladores** en la medida que su vinculación está regulada por un presupuesto establecido. Dijo que, “a los controladores los vinculan más no los contratan y los directores de la Aeronáutica han contratado empleados pero las vinculaciones de los controladores no se han hecho”.

Según Pulido, un controlador aéreo **debe trabajar 6 horas durante su jornada laboral** para garantizar la seguridad aérea. Sin embargo, desde el 2012 habían llegado a un acuerdo de trabajar 6 horas extras durante 10 días al mes mientras que se cumplía con la capacitación de personal adicional y se vinculaban a la planta de trabajadores.  Pero, llevan 5 años “sobrepasando el ideal que indica que un controlador no puede hacer horas extras".

Esto sucede no solamente en el área de controladores aéreos sino también en áreas como la de los **bomberos, la seguridad aérea y el personal técnico**. La controladora reiteró que éste grupo de trabajadores exceden su tiempo de trabajo reglamentado y “no se les está pagando”. (Le puede interesar: ["Continua huelga de pilotos por "soberbia" de directivos de Avianca"](https://archivo.contagioradio.com/continua-huelga-de-pilotos-por-soberbia-de-directivos-de-avianca/))

### **Controladores tienen embolatada la pensión** 

A esta problemática, se suma que los trabajadores, que ya cumplieron con los requisitos para lograr una pensión, **“no se las están dando o está mal liquidada**” por lo que los controladores no dejan su cargo mientras que otros argumentan que no quieren dejar su puesto.

Según la controladora, en Colombia hay un aproximado de 100 controladores que ya cumplieron los requisitos de pensión y **no se la han garantizado**. “Algunos están pendientes de que les resuelvan su situación pensional, a otros se las han negado y los demás están pendientes de que se las reliquiden”.

Finalmente, Pulido manifestó que la misión de los controladores aéreos es **evitar que los aviones colisionen en vuelo**, “se llama vuelo desde el momento en que el avión cierra puertas y comienza el remolque”. (Le puede interesar:["MinTrabajo no podría haber convocado tribunal de arbitramento según la ley: ACDAC"](https://archivo.contagioradio.com/pilotos_avianca_ministerio_trabajo_huelga/))

En repetidas ocasiones, le han pedido a la Aeronáutica Civil que se establezca con claridad **cómo determinan el número de operaciones** que debe realizar cada trabajador para dar garantías de seguridad y no han recibido una respuesta concreta.

<iframe id="audio_21355018" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21355018_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
