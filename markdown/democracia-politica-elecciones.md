Title: Lo antidemocrático en Colombia no es la política, es la sociedad
Date: 2018-06-18 17:49
Category: Nacional
Tags: Centro Democrático, elecciones, Petro
Slug: democracia-politica-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/democracia-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Registraduría Nacional 

###### 18 Jun 2018 

Analizando los resultados de las recientes elecciones presidenciales en Colombia, el profesor Fernando Giraldo destacó el rol que tuvo la personalización de la política en la figura de ambos candidatos **"creo que no es con Petro, ni con Duque ni con las personas que actualmente están que la política ha llegado a unos límites insospechados de excesiva personalización e individualización y donde los partidos políticos cumplen un papel completamente secundario**"

El analista aseguró que los intereses de las colectividades han terminado "sacrificados o replegados" a los intereses individuales de sus candidatos o sus dirigentes, lo que explica aludiendo que "los ciudadanos cada vez creen menos en los partidos y cada vez tratan de encontrar una solución a sus problemas expresados políticamente a través de identificar  personas que lideren alguna postura".

Giraldo se refirió a demás al programa de gobierno presentado por Gustavo Petro, asegurando que en muchos años no había escuchado una persona que tuviera uno tan claro "no solo para sus amigos y su partido, su cúpula y la cúpula de los otros partidos , ni para un debate en los medios de comunicación sino que cuando lo escuchan los ciudadanos entienden de que esta hablando"

Destacó además de su campaña el haber entrado en un territorio monopolizado por el uribismo, particularmente sectores populares, con un ideario que aunque no fue construido por un colectivo **"es más claro prácticamente que la que tienen todos los partidos " incluyendo al Centro Democrático"**.

En relación con la posibilidad de encontrar alternativas de reconciliación luego de las elecciones, Giraldo cuestiona si se le esta preguntando que tan democrática puede ser la sociedad de derecha, de izquierda, de centro o a toda la sociedad, lo que asegura que es que "el sistema político colombiano en su formalidad, en su régimen político, en sus instituciones, en sus normas, en sus reglas de juego es muy democrático la que es profundamente antidemocrática es la sociedad" (Ver: [Los retos de Gustavo Petro en el Congreso de la República](https://archivo.contagioradio.com/los-retos-de-petro-y-angela-robledo-en-el-congreso-de-la-republica/))

Frente  a lo anterior, el analista asegura que el sistema político democrático debe asentarse y construirse sobre una sociedad que  "**no esta basada ni en el compromiso ni en la confianza, ni en la tolerancia ni en una cultura política democrática ni en la inclusión**" por lo que resulta muy difícil poder hablar  de "como  superar la radicalización de la polarización si no democratizamos esta sociedad y eso no depende de Petro".

En cuanto a la posición de líderes como **Jorge Robledo**, el profesor asegura que le ha llamado la atención que un dirigente de la década del 60 siga siendo destacado y controle un partido en la segunda década del siglo XXI, ante lo que reflexiona que los partidos y movimientos tienen la obligación de estar cambiando y modificando permanentemente su vocabulario  y que tienen que haber cambios generacionales.

Adicionalmente, el profesor Giraldo  asegura que los 8 millones de votos conseguidos por Petro, hacen  que por su decisión de  de votar  en blanco Robledo " va a sufrir las consecuencias gravísimas va a tener un impacto muy fuerte políticamente para el y al interior del Polo".

<iframe id="audio_26602624" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26602624_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
