Title: Águilas Negras amenazan de muerte a mujeres lideres, congresistas y periodistas
Date: 2017-11-28 21:22
Category: DDHH, Nacional
Tags: Aguilas Negras, amenazas de muerte, lideres sociales
Slug: aguilas_negras_amenzas_mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/amenaza-a-lideresas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [28 Nov 2017] 

[En un panfleto el Bloque Capital de las Águilas Negras amenazó de muerte a varias de las lideresas, periodistas y congresistas que han apoyado el acuerdo de paz firmado en La Habana, y que defienden los derechos de las mujeres. Entre ellas están, las congresistas** Ángela María Robledo y Claudia López, las periodista Jineth Bedoya y Salud Hernández, así como Imelda Daza, Luz Marina Díaz y Ángela Anzola.**]

<iframe src="https://co.ivoox.com/es/player_ek_22336578_2_1.html?data=k5eglZuZe5mhhpywj5WdaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8PgxsncjdjTptPZjNHOjdvTuMLXyoqwlYqmd8-fxcqYzsaPjqbEjMrbjaiJh5SZopbaw9fFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según la representante a Ángela María Robledo, la amenaza le llegó mediante al correo institucional del Partido Verde, y se percató de este el lunes en la mañana. **"Es es un panfleto horroroso de amenaza de muerte",** y señaló que la amenaza aparece  justo después de que ella publicara una foto en su cuenta de Twitter junto al candidato de la FARC a la presidencia, Rodrigo Londoño, que recibió toda clase de comentarios contra ella.

### **La investigación** 

Ante la situación, y aprovechando que en medio de la debate de la Ley Estatutaria de la JEP, el lunes estaba Luis Carlos Villegas, Ministro de Defensa y el Fiscal General, Néstor Humberto Martínez, se decidió conformar un grupo de investigación que de con los responsables. [Lea también: Águilas Negras siguen amenazando a líderes sociales](https://archivo.contagioradio.com/?s=aguilas+negras).

El equipo de la representante Robledo realizó una curaduría de medios y de trinos donde la congresista es señalada, y encontraron cuentas desde donde se han emitido esa clase de comentarios que estarían relacionadas con **integrantes del Centro Democrático.**

[Frente  esta situación se ha exigido la debida investigación, garantías y protección para sus vidas. En ese sentido, la congresista pide que se ]investigue particularmente las cuentas de  representantes del Centro Democrático como la de** Álvaro Hernán Prada y Margarita Restrepo,** quienes en medio del debe de la JEP, de manera reiterada han acusado a la parlamentaria de ser terrorista o de hacer parte de la guerrilla.

![DPqJe\_aXUAEC8Gg](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DPqJe_aXUAEC8Gg-450x600.jpg){.wp-image-49530 .size-large .aligncenter width="450" height="600"}

 

###### Reciba toda la información de Contagio Radio en [[su correo]
