Title: Unidad de Búsqueda de personas desaparecidas a un decreto de convertirse en realidad
Date: 2018-01-23 16:28
Category: DDHH, Otra Mirada
Tags: acuerdos de paz, Luz Marina Monsón, Unidad de Búsqueda de Desaparecidos
Slug: unidad-de-busqueda-de-personas-desaparecidas-a-un-decreto-de-convertirse-en-realidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Desaparición-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particual] 

###### [23 Ene 2018] 

La Unidad de Búsqueda de Personas Desaparecidas estaría a un decreto de iniciar sus labores. Se prevé que esta unidad marche paralelamente a la JEP y CV y por ello ya hay un proyecto aprobado por 2 millones de dólares para el 2018 y ya cuenta con el acumulado del trabajo de instituciones como la Comisión de Búsqueda, el Centro de Memoria y el conocimiento de las organizaciones de familiares de los cerca de 50 mil desaparecidos que hay  en el país.

De acuerdo con L**uz Marina Monzón**, directora de esta unidad y defensora de derechos humanos, debe existir una armonización y sincronización en la implementación de la unidad “de manera que en realidad pueda ser parte de un sistema de verdad, justicia y reparación que tiene que estar en los **mismos tiempos para poder ofrecer las respuestas para los cuales están establecidos los mecanismos**”.

Aunque el Estado ya había avanzado en parte del diseño institucional, Monzón afirma que esta Unidad no puede estar regida por los mismo parámetros de cualquier otra institución con oficinas regionales, sino que debe tener en cuenta a las víctimas y sus lugares de origen para acercarse a ellas y no al contrario. Para ello ya hay un proyecto aprobado por 2 millones de dólares del fondo multipropósito que cuenta con aportes estatales e internacionales.

### **El funcionamiento de la Unidad de Búsqueda de desaparecidos** 

Este proyecto contará con dos fases, la primera será el diseño de la Unidad de búsqueda de personas desaparecidas y 12 personas estarán encargadas de llevarla a cabo, “la unidad es una institución que entre en el escenario de la institucionalidad colombiana con características distintas a las que tradicionalmente han existido en la búsqueda de los desaparecidos”, afirmó Monzón.

Las características son, la primera que es extrajudicial y la segunda que es humanitaria, con la finalidad de que la unidad sea eficiente y eficaz con el desarrollo de este mandato. Además en esta primera fase articulara ese funcionamiento institucional con la operación tanto departamental como nacional que tendrá la unidad.

“El tema de la búsqueda de las personas desaparecidas, tendría que **tener un diseño flexible a tomar contacto las víctimas, a ir a las comunidades donde la desaparición forzada ha tenido lugar** y a poder actuar de manera un poco más con las características de esta institución” señaló Monzón. (Le puede interesar: ["Mónica Molina, integrante del M-19 retornó a los brazos de su familia después de 32 años de impunidad"](https://archivo.contagioradio.com/monica-molina-integrante-del-m-19-retorno-a-los-brazos-de-su-familia-despues-de-32-anos-de-impunidad/))

La segunda fase consiste en el diseño de las herramientas que usara la Unidad y que la dejarán lista para iniciar sus actividades, algunas de ellas tendrán que ver con la recolección de información, el análisis de la información y la seguridad de la misma.

### **Qué hacer mientras se implementa la Unidad de Búsqueda** 

Luz Marina Monzón, fue enfática al asegurar que todos los demás mecanismos de búsqueda de personas desaparecidas están activos, “**en este momento tanto la Fiscalía como la Unidad de Búsqueda están en la obligación legal de recibir, tramitar y dar respuesta  la búsqueda** de personas que se encuentren desaparecidas”. (Le puede interesar: ["Unidad de Búsqueda tendrá que encontrar a más de 50.000 desaparecidos"](https://archivo.contagioradio.com/unidad-de-busqueda-tendra-que-encontrar-a-mas-de-50-000-desaparecidos-en-el-pais/))

De igual forma señaló que hay distintas instituciones como el Centro Nacional de Memoria que han alimentado bases de datos sobre desaparición forzada, secuestro y reclutamiento que será entregada a la Unidad, una vez se cree. Además hizo un llamado a las organizaciones para que se establezca la cooperación que permita avanzar en esta tarea. Así mismo manifestó que se trabajará con las organizaciones de víctimas que han sido las primeras en realizar el trabajo en Colombia por la búsqueda de sus desaparecidos.

<iframe id="audio_23318216" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23318216_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
