Title: Rastrojos asesinan a ocho personas en el Catatumbo
Date: 2020-07-18 18:36
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Los Rastrojos, paramilitares
Slug: masacre-catatumbo-rastrojos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EdPmwxOXgAIpGdk.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/mapa-catatumbo.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Según la información que ha sido compartida por la Asociación Campesina del Catatumbo, ASCAMCAT, en la mañana de este 18 de Julio se habría perpetrado una masacre por parte del grupo paramilitar conocido como "Los Rastrojos".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La denuncia de la organización campesina asegura que por lo menos 8 personas habrían sido asesinadas en la vereda "Totumito Carboneras" del Municipio de Tibú en Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La información difundida hacia las 3 de la tarde este sábado y que aún es preliminar, es que una de las víctimas pertenecía a esa organización campesina y también a la COCAM, que agrupa a las personas que respaldando el acuerdo de paz se acogen a los proyectos de sustitución de cultivos de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Habitar el Catatumbo es estar en riesgo de muerte constante

<!-- /wp:heading -->

<!-- wp:paragraph -->

Lastimosamente la región del Catatubmo es una de las más golpeadas por la guerra, provocada no solamente por los grupos armados ilegales sino también por las FFMM. Recientemente se realizó la denuncia en la que una persona habría sido asesinada por militares pues tenía un parecido físico con un integrante del Ejército de Liberación Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La víctima fue Salvador Jaime Durán y luego del asesinato los habitantes de la región detuvieron a la patrulla militar que habría perpetrado el asesinato sin que hasta el momento haya avances en las investigaciones. [Le puede interesar: Denuncian posible ejecución extrajudicial de Salvador Durán en Teorama](https://archivo.contagioradio.com/ascamcat-denuncia-posible-ejecucion-extrajudicial-de-campesino-en-teorama/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente la lucha en contra de la erradicación forzada de los cultivos de uso ilícito ha provocado varios heridos ya que las FFMM se empeñan en erradicar de manera forzada el único medio de subsistencia de cientos de familias que además insisten en planes de sustitución voluntaria. [Lea también Más de 30 personas han sido asesinadas en medio de la protesta social y la cuarentena](https://archivo.contagioradio.com/abuso-de-la-fuerza-publica-ha-dejado-30-homicidios-en-medio-de-la-movilizacion-social-en-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#Alerta](https://twitter.com/hashtag/Alerta?src=hash&ref_src=twsrc%5Etfw) [\#DesplazamientoForzado](https://twitter.com/hashtag/DesplazamientoForzado?src=hash&ref_src=twsrc%5Etfw) ante [\#Masacre](https://twitter.com/hashtag/Masacre?src=hash&ref_src=twsrc%5Etfw) en la mañana de hoy que dejó 8 personas muertas en los que se encontraría un miembro de [\#Ascamcat](https://twitter.com/hashtag/Ascamcat?src=hash&ref_src=twsrc%5Etfw) y [\#Coccam](https://twitter.com/hashtag/Coccam?src=hash&ref_src=twsrc%5Etfw) en la vereda Totumito-Carboneras [\#Tibú](https://twitter.com/hashtag/Tib%C3%BA?src=hash&ref_src=twsrc%5Etfw), según la comunidad la masacre habría sido cometida por grupo paramilitar LOS RASTROJOS [pic.twitter.com/wNg2Fvjaf2](https://t.co/wNg2Fvjaf2)
>
> — AscamcatOficial (@AscamcatOficia) [July 18, 2020](https://twitter.com/AscamcatOficia/status/1284625819294486535?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->
