Title: La apuesta por la vida de comunidades campesinas en Putumayo
Date: 2020-05-01 18:09
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Erradicación Forzada, Putumayo, Zona de Reserva Campesina
Slug: la-apuesta-por-la-vida-de-comunidades-campesinas-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo {#foto-archivo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Zona de Reserva Campesina de la Perla Amazónica (ZRCPA), ubicada en zona rural del muncipio de Puerto Asís, Putumayo, las comunidades siguen apostando por la vida y la restauración del territorio pese a enfrentar dinámicas de conflicto, erradicación forzada y, en estos momentos, dificultad en el acceso a alimentos por cuenta de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La situación de la ZRCPA en medio del Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph -->

Wilson Medina, integrante de la Zona de Reserva y presidente de la Junta de Acción Comunal de una vereda al interior de la Zona señala que se está viviendo una situación compleja en la comunidad debido al aislamiento al que se han tenido que someter porque la movilidad en el territorio es restringida, y las personas mayores no pueden salir de sus casas a hacer compras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, como no han podido trabajar, las personas no pueden llevar sus productos de vender a los cascos urbanos y " de mercado todo está caro". Medina también sostiene que en su comunidad, compuesta por 35 personas, de todas las ayudas que anuncia el Gobierno solo 2 personas han resultado beneficiadas con kits de mercado que alcanzan para solo 10 personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No llegan los mercados, llega la Fuerza Pública...

<!-- /wp:heading -->

<!-- wp:paragraph -->

La [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/entrada-de-erradicadores-a-comunidades-de-la-zrcpa/) denunció el pasado 29 de abril que helicópteros sobrevolaron territorio de la Zona de Reserva Campesina y dejaron 80 integrantes de la Fuerza Pública en el territorio para adelantar labores de erradicación forzada. La[Organización](https://www.justiciaypazcolombia.com/riesgo-de-contagio-por-presencia-de-erradicadores-sin-medidas-de-prevencion-de-expansion-de-covid-19/) denunció que la situación pone en riesgo a la comunidad, en tanto se desconoce si los uniformados son portadores de Coronavirus, y en el territorio hay población vulnerable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Medina afirma que los uniformados no han adelantado labores de erradicación porque los erradicadores no han llegado, pero les informaron que lo harían en el curso de la próxima semana. Adicionalmente señala que la mayoría de las personas que están en la zona hacen parte del Plan Nacional Integral de Sustitución (PNIS), pero el mismo no ha avanzado y "está quieto".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La apuesta por la vida y el futuro en Putumayo

<!-- /wp:heading -->

<!-- wp:paragraph -->

La [Comisión](https://www.justiciaypazcolombia.com/no-paran-de-asesinar-en-putumayo/) ha denunciado constantemente asesinatos en Puerto Asís y algunos corredores cercanos a la Zona de Reserva, en lugares donde opera la estructura conocida como La Mafia pese al control territorial que ejerce la Fuerza Pública. Pese a esta situación, el incumplimiento de las obligaciones y pactos del Estado como en el caso del PNIS y la Pandemia, Medina afirma que la comunidad apuesta por la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Nosotros apostamos por la vida y la seguridad de nuestro territorio porque queremos vivir, queremos que nuestros hijos e hijas vivan acá", y añade que por eso están dejando ejemplos de proyectos como los viveros, que pronto les permitirán reforestar con especies nativas que están en vía de extinción. Medina concluye que quieren apostarle a conservar el ambiente, para que las generaciones tengan un lugar que habitar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Por daños ambientales en Putumayo congelan millonaria cifra a petrolera Amerisur](https://archivo.contagioradio.com/por-danos-ambientales-en-putumayo-congelan-millonaria-cifra-a-petrolera-amerisur/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
