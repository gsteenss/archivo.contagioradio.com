Title: En medio de protestas policía de EEUU asesina afrodescendiente en Detroit
Date: 2015-04-29 15:49
Author: CtgAdm
Category: El mundo, Movilización, Otra Mirada
Tags: asesinado joven negro en Detroit, Baltimore, Disturbios raciales EEUU, ferguson, Policía de EEUU racista
Slug: en-medio-de-protestas-en-baltimore-policia-de-eeuu-asesinan-afrodescendiente-en-detroit
Status: published

###### Foto:Es.us.noticias.yahoo.com 

###### Entrevista con[ Liliana Gómez], activista de Occupy Wall Street: 

<iframe src="http://www.ivoox.com/player_ek_4423950_2_1.html?data=lZmflZ6ZdI6ZmKiakpyJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrn1drfxM7Tt4zmwsjWw9HJt4zZz5Cvw9HYrc7j08qah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El 19 de Abril falleció **el joven afrodescendiente Freddie Grey**, después de que la policía de Baltimore lo detuviese desarmado. En el transcurso de la **detención la policía le rompió una vértebra**, entrando así en coma irreversible para después fallecer en el hospital.

Video de la detención de Freddie Grey:

\[embed\]https://www.youtube.com/watch?v=Cxkf8afgVMM\[/embed\]

El lunes 27 después **del funeral la ciudad estalló en disturbios** que se saldaron con 236 detenidos, 144 coches y 15 inmuebles incendiados, y 20 oficiales de policía heridos.

Cientos de jóvenes de **institutos marcharon en repulsa del asesinato del joven afrodescendiente** y lo que consideran un caso más de racismo por parte de la policía en una ciudad donde las dos terceras partes son afroamericanos

Tras los disturbios el gobernador de Maryland Larry Hogan declaró **el estado de emergencia y envió a la Guardia Nacional** (policía militar), decretando el **toque de queda** desde las 10 de la noche hasta las 5 de la mañana.

Hasta ahora han sido **suspendidos 6 policías de su cargo** y el caso continúa a la espera de juicio. Barack Obama se ha pronunciado expresando su preocupación por la actuación de la policía y espetando a la población a mantener la calma.

Pero **los asesinatos han continuado** en medio de las protestas, esta vez ha sido en **Detroit**, repitiéndose el mismo patrón, el joven **Terrance Kellom ha fallecido a manos de la policía** cuando iba a detenerlo supuestamente por su implicación en dos robos.

Entrevistamos a **Liliana Gómez, activista de Occupy Wall Street**, quién nos informa de que estos últimos casos responden a una continuidad de **la segregación racial**, que encuentra su máxima expresión en la brutalidad policial y el patrón racista de las propias fuerzas de seguridad del país.

Además nos explica que el toque de queda y la militarización de la sociedad responden a una **estrategia para reprimir y crear miedo** entre la población indignada con los últimos asesinatos.

Mapa de la brutalidad policial en EEUU:

<iframe src="https://anandkatakam.cartodb.com/viz/e13166bc-854f-11e4-abdb-0e018d66dc29/embed_map" width="50%" height="520" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
