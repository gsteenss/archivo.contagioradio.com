Title: Presencia del ESMAD alarma nuevamente a defensores de "La Conejera"
Date: 2015-04-21 16:24
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alcaldía, ESMAD, Fontanar del Río, Gloria Díaz, Gustavo Petro, Humedales, La Conejera, Suba
Slug: presencia-del-esmad-alarma-nuevamente-a-defensores-de-la-conejera
Status: published

##### Foto: Conejera Resiste 

<iframe src="http://www.ivoox.com/player_ek_4388012_2_1.html?data=lZilmpWVdo6ZmKiakp6Jd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmY0NrJusLhxtPhx5DMs9ToyszOjcaPsNDnjMnSyMrSt9DmxtiY25DIqcfZz9jc1MbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gina Díaz, defensora del humedal La Conejera] 

Nuevamente  defensores y defensoras del humedal la conejera, están siendo hostigados por el ESMAD, que llegaron esta mañana acompañados de funcionarios de la constructora Fontanar del Río, debido a que **intentan ingresar material de construcción al humedal.**

Gina Díaz, quien hace parte del campamento que defiende la conejera denuncia esta situación, afirma que **desde las 6:30 de la mañana 25 motos del ESMAD y la policía con 4 volquetas, rodearon el campamento en defensa del  humedal.** De acuerdo Díaz, no se entiende el por qué nuevamente hay presencia de la fuerza pública, ya que el jueves pasado se había llegado a un acuerdo con la Secretaría de Ambiente y la Secretaria de Gobierno, Gloria Flórez, donde habían acordado que hasta que no se conociera la declaración del magistrado sobre los alcances de la medida cautelar, la administración distrital se comprometía a que **no se iba a ingresar ningún material de construcción.**

Este lunes, el personero Ricardo Cañón, radicó una aclaración sobre las medidas cautelares, ya que **así haya solo un 4% de la obra, con ese porcentaje deben hacer una modificación de la licencia u obtener una nueva,** lo que significa que hasta no tener esa nueva licencia el proyecto Fontanar del Río no puede continuar.

Los defensores y defensoras se encuentran preocupados, ya que se trata de “**un acoso constante que genera mucha inseguridad e incertidumbre”,** dice Gina Díaz, quien agrega que todavía no hay responsables sobre lo qué pasó el jueves pasado, donde resultaron agredidos físicamente varios ambientalistas. Según la defensora, la alcaldía afirmó que ni Gustavo Petro, ni la secretaria Gloria Flórez habían dado la orden al ESMAD para el desalojo.
