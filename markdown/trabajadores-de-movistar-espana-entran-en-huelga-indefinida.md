Title: Trabajadores de Movistar España entran en huelga indefinida
Date: 2015-04-17 17:41
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Confederación General De trabajadores España, españa, Huelga de trabajadores de Movistar España, Movistar, Subcontratistas Movistar España en huelga
Slug: trabajadores-de-movistar-espana-entran-en-huelga-indefinida
Status: published

###### Foto:Bandancha.eu 

###### **Entrevista con [Pepe], trabajador de Movistar España y delegado sindical de CGT:** 

<iframe src="http://www.ivoox.com/player_ek_4369083_2_1.html?data=lZijm5Wcd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkNbXycaYxsqPsNDnjNnfw8fFrsLY0NfS1ZDIqYzB0NvW1dnFtozZz5Cy1dXFaaSnhqeew5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El **sábado 28 de Marzo** entraron en **huelga indefinida** más de **1200 trabajadores** de la filial de Telefónica conocida como **Movistar** en contra de la precariedad laboral y de los **abusos en la subcontratación** de las distintas empresas que utiliza Movistar para poner a disposición a los trabajadores.

Pero ha sido el anuncio de un **nuevo tipo de contrato laboral** presentado por Telefónica lo que ha caldeado los ánimos de los trabajadores, al ver que sus **condiciones empeoraban y casi la totalidad de la plantilla pasaba a ser subcontratada.**

En el transcurso de la **huelga** se fueron sumando trabajadores de las distintas provincias del estado español hasta llegar a **14 provincias secundándola**. Después de que casi todas las filiales se sumaran el **7 de Abril comenzó una huelga indefinida estatal** de todos los técnicos de Movistar España.

Las **reivindicaciones** de los técnicos de la fibra óptica, pasan por el **fin de la jornada laboral indefinida**, que conlleva a **jornadas de hasta 18 horas**, el pago del transporte de los clientes, que hasta ahora estaban acarreando los trabajadores o el fin de la subcontratación a través del conocido **trabajador autónomo**, y el **aumento salarial de 700 euros a 2.000.**

Es decir la formalización de un **contrato laboral definido** y consensuado entre la dirección de las empresas y los representantes sindicales que permita a los trabajadores tener unas condiciones laborales dignas.

Para **Pepe, delegado sindical de CGT Telefónica** (Confederación General de Trabajadores), este ha sido un trabajo de más de tres meses de los delegados sindicales, **uniendo distintas luchas, informando y enlazando a las distintas filiales** para poder realizar una huelga conjunta.

Y es que es de vital importancia que este sector haya convocado una huelga, es decir los técnicos de Movistar pertenecen al **sector servicios que es el más precarizado** en la economía española, pero también a nivel mundial. Al tener una contratación poco definida es muy **complicado poder cohesionar a los trabajadores** para realizar una jornada de lucha.

Pepe también nos explica que han sido los **sindicatos mayoritarios que a penas tienen presencia en el sector quienes han intentado frenar la huelga**, pero que al final todos los trabajadores han decidido a poyar el carácter indefinido de la propia huelga.
