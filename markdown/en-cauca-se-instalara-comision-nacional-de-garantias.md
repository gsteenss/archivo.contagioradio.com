Title: Se instala Comisión Nacional de Garantías de seguridad en el Cauca
Date: 2017-02-22 16:30
Category: Nacional, Paz
Tags: acuerdos de paz, Comision Nacional de Garantías de Seguridad, paramilitares
Slug: en-cauca-se-instalara-comision-nacional-de-garantias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/COMISION-DE-SEGUIMIENTO-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ministerio del Interior] 

###### [21 Feb. 2017] 

Este jueves se instala oficialmente, en el departamento del Cauca, **la Comisión Nacional de Garantías de Seguridad**, la cual tendrá que diseñar, hacer seguimiento y **desmantelar las organizaciones como los paramilitares** que amenacen la implementación del Acuerdo Final de Paz. Le puede interesar:[ La Comisión que tendrá la tarea de combatir el paramilitarismo](https://archivo.contagioradio.com/asesinatos-de-lideres-y-defensores-no-obedecen-a-la-mineria-criminal-y-al-narcotrafico/)

**Gustavo Gallón,** director de la Comisión Colombiana de Juristas e integrante de la Comisión aseguró que este será un acto protocolario, pero que **una vez instalada debe comenzar a trabajar inmediatamente para intentar prevenir más daños a defensores de DDHH y líderes sociales.** “Tenemos un retraso como de 6 meses de lo que estaba previsto de cuando se acordó crearla y su puesta en funcionamiento formal”.

“Ha hecho falta” asegura el jurista que “esta Comisión comience a trabajar en lo que le ha sido encargado, que consiste en definir y en supervisar una política para **enfrentar a las organizaciones ilegales que actúan contra activistas sociales y defensores de derechos humanos incluidas el paramilitarismo”.**

El integrante de la Comisión Nacional de Garantías, reitera que **si el paramilitarismo no se erradica en el país va a ser muy difícil que efectivamente lograr una sociedad en paz** y con confianza para desarrollar sus actividades y “para que se tramiten civilizadamente las contradicciones que toda sociedad tiene”. Además, es muy positivo que exista un grupo de personas que trabajen de tiempo completo para adelantar trabajo. Le puede interesar: [Participación de la sociedad civil será clave en desmonte del paramilitarismo](https://archivo.contagioradio.com/participacion-de-la-sociedad-civil-sera-clave-en-desmonte-del-paramilitarismo/)

“Esperamos que primero tengamos la presentación de la radiografía del fenómeno y luego un cronograma de visitas a las regiones para ver más de cerca las dinámicas y los recursos objetos de apropiación, para después actuar” recalcó Gallón. Le puede interesar: [Sí hay paramilitares en el Catatumbo: Comisión de verificación](https://archivo.contagioradio.com/comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar/)

**Estas son las funciones de la Comisión Nacional de Garantías de Seguridad.**

La Comisión Nacional de Garantías de Seguridad, sin perjuicio de las funciones y competencias correspondientes a las diferentes autoridades y entidades públicas, cumplirá las siguientes funciones:

1.  Diseñar, hacer seguimiento, coordinar intersectorialmente y promover la coordinación a nivel departamental y municipal para el cumplimiento del plan de acción que el Gobierno Nacional lleve adelante para combatir y desmantelar las organizaciones y perseguir las conductas punibles a que hace referencia el artículo 10 del presente decreto.
2.  Formular y evaluar el Plan de acción permanente para combatir y desmantelar las organizaciones y conductas punibles a que hace referencia el acto 1 de este decreto, que será adoptado por el Gobierno Nacional.
3.  Evaluar la respuesta institucional y el impacto de los resultados en la desarticulación de las organizaciones y conductas punibles a que hace referencia el artículo 10 del presente decreto.
4.  Coordinar con las autoridades departamentales y municipales, la generación de mesas técnicas para hacer seguimiento a las manifestaciones criminales objeto de esta Comisión, incluyendo la recepción de reportes y denuncias, que contribuyan a complementar el esfuerzo estatal.
5.  Recomendar reformas que contribuyan a eliminar cualquier posibilidad de que el Estado, sus instituciones o sus agentes puedan crear, apoyar o mantener relaciones con las organizaciones a que hace referencia el artículo 10 del presente decreto.
6.  Solicitar a las autoridades la remisión de informes sobre cualquier materia relacionada con las organizaciones y conductas de que trata el artículo 1 del presente Decreto y hacer seguimiento del contenido de dichos informes.
7.  Diseñar y construir las estrategias para identificar las fuentes de financiación y los patrones de actividad criminal de las organizaciones y conductas punibles a que hace referencia el artículo 10 del presente decreto, entre dichos patrones se tendrán en cuenta aquellos que afectan de manera particular a las mujeres, niñas, niños, adolescentes y población LGTBI.
8.  Hacer recomendaciones para modificar o derogar las normas que, directa o indirectamente, posibiliten y/o promuevan la creación de las organizaciones y conductas a que hace referencia el artículo 10 del presente decreto.
9.  Proponer a las autoridades competentes mecanismos para la revisión de antecedentes de los servidores/as públicos en todas las instituciones del Estado, con el fin de verificar cualquier involucramiento que hayan tenido con grupos y/o actividades de paramilitarismo o violaciones de Derechos Humanos.
10. lnformar periódicamente a las Ramas del Poder Público, a la opinión pública y a los organismos internacionales, los avances y obstáculos en la lucha contra las organizaciones y conductas a que hace referencia el artículo 10 del presente decreto.
11. Garantizar el suministro de información por parte de las entidades o instituciones que participen de la Comisión Nacional de Garantías de Seguridad, a la "Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición" y a la Unidad de investigación y desmantelamiento de organizaciones criminales y sucesoras del paramilitarismo.
12. Hacer recomendaciones a las Ramas del Poder público para ajustar y priorizar las acciones y estrategias de la política y legislación de inteligencia del Estado en la lucha contra las organizaciones y conductas a que hace referencia el artículo 10 del presente decreto.
13. Hacer seguimiento al régimen de controles sobre los servicios de vigilancia y seguridad privada y formular propuestas para actualizar las normas que regulan los servicios de vigilancia y seguridad privada, con el propósito de que sus servicios correspondan al fin para el que fueron creados y que en ningún caso, de manera directa o indirecta, faciliten la acción de las organizaciones y conductas criminales a que hace referencia el artículo 10 del presente decreto.
14. Diseñar políticas para el sometimiento a la justicia de las organizaciones criminales y sus redes de apoyo a que hace referencia el artículo 10 del presente decreto, definiendo tratamientos específicos para los integrantes dichas organizaciones y redes, incentivando y promoviendo un rápido y definitivo desmantelamiento de las mismas.  
   Dichas medidas nunca significarán reconocimiento político.
15. 15 Garantizar la aplicación de los enfoques territoriales, diferencial y de género en el diseño, implementación y seguimiento de las políticas y estrategias que sean objeto comisión.
16. Participar en el diseño de un nuevo Sistema prevención y la reacción rápida a la presencia, operaciones y/o actividades las y conductas criminales a que hace el artículo decreto.

######  Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
