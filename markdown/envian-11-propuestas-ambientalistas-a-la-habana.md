Title: Envían 11 propuestas ambientalistas a La Habana
Date: 2016-08-16 20:26
Category: Ambiente, Nacional, Paz
Tags: Ambiente, justicia socio-ambiental, La Habana, paz, proceso de paz
Slug: envian-11-propuestas-ambientalistas-a-la-habana
Status: published

###### Foto: Contagio Radio 

###### [16 Ago 2016] 

Representantes de varias organizaciones ambientalistas colombianas presentaron ante la mesa de conversaciones de paz de La Habana un compendio de once propuestas para que los temas ambientales también sean tenidos en cuenta en el marco del proceso de negociaciones entre la guerrilla de las FARC y el gobierno, además de uno posible con el ELN.

Construir una nueva ética e institucionalidad ambiental; propiciar la paz territorial, teniendo en cuenta las los procesos locales; Incorporar conceptos novedosos como el PIB ambiental; trabajar por los objetivos del desarrollo sostenible para 2030; redefinir las relaciones entre la Colombia rural y urbana; revisar modelo extractivista y prohibir el fracking; una educación integral en torno a la conservación del ambiente; fortalecer la investigación e innovación científica; y finalmente, construir una agenda ambiental y climática son algunas de las propuestas que esperan ser revisadas por las partes de la mesa de diálogos.

Recomendaciones que llegan a La Habana debido a las preocupaciones que tienen grupos ambientalistas y animalistas que aseguran que los temas en defensa de la naturaleza se han quedado por fuera de los acuerdos, teniendo en cuenta la resistencia del gobierno nacional para discutir el modelo económico del país, lo que genera mayores dudas para ese movimiento en Colombia, temiendo que el modelo extractivista se profundice en el posconflicto.

"Se trata de un documento hecho por académicos y ambientalistas que venimos trabajando desde 1997 cuando comenzamos a estructurar unas propuestas para la reparación del ambiente", explica uno de los firmantes, Diego García, quien resalta la buena disposición por parte de la delegación de paz de las FARC para escuchar las propuestas, y en cambio habla de la poca voluntad política del gobierno frente a los temas ambientalistas.

<iframe src="http://co.ivoox.com/es/player_ej_12566164_2_1.html?data=kpeimJuVepWhhpywj5aVaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjazFtsSZpJiSo6nFaZO3jKbaxM7JstXVzc7g1saPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Documento de La Habana Ambientalismo Para La Paz](https://es.scribd.com/document/321485566/Documento-de-La-Habana-Ambientalismo-Para-La-Paz#from_embed "View Documento de La Habana Ambientalismo Para La Paz on Scribd")

<iframe class="scribd_iframe_embed" src="https://www.scribd.com/embeds/321485566/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="undefined" scrolling="no" id="doc_92638" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
