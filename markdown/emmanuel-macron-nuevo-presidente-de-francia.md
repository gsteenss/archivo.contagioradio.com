Title: Con un 65% de votos Emmanuel Macron es el nuevo presidente de Francia
Date: 2017-05-07 14:51
Category: El mundo, Política
Tags: Elecciones en Francia, francia, Le Pen, Macron
Slug: emmanuel-macron-nuevo-presidente-de-francia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Macron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: New Statesman] 

###### [07 May. 2017] 

Francia ya tiene un nuevo presidente, **Emmanuel Macron, fundador del partido ¡En Marche! y el candidato más joven de la contienda con tan solo 39 años.** Macron llegará para reemplazar a quien fuera su jefe, François Hollande.

Según cifras conocidas a través de diversos medios de comunicación, **Emmanuel Macron alcanzó el 65 % de los votos, frente al 35 % de Le Pen**, con una participación del 75% de los y las votantes, siendo la más baja en toda la historia de las votaciones en segunda vuelta en Francia.

A través de su Twitter, Emmanuel Macron saludo a su contrincante Marine Le Pen y aseguró que trabajará por un nuevo país. Así mismo, **agradeció a los y las votantes por su confianza depositada en la urnas.**

> Mes chers compatriotes, vous avez choisi de m’accorder votre confiance et je tiens à vous exprimer ma profonde gratitude.
>
> — Emmanuel Macron (@EmmanuelMacron) [7 de mayo de 2017](https://twitter.com/EmmanuelMacron/status/861296869988761601)

Luego de más de 60 años de que gobernaran el partido de izquierda o derecha en Francia, es la primera vez que ninguno de estos dos partidos llegan a la última fase de las elecciones presidenciales.  Le puede interesar: [Extrema derecha pierde elecciones regionales en Francia](https://archivo.contagioradio.com/extrema-derecha-pierde-elecciones-regionales-en-francia/)

Macron ha asegurado que en su gobierno trabajará por disminuir las cotizaciones patronales, que son utilizados para financiar la seguridad social y las pensiones. De igual manera, **ha dicho que en 5 años pretende suprimir 120 mil empleos públicos y no aumentará el salario mínimo ni las pensiones. **En contexto: [Persiste movilización contra la reforma laboral en Francia](https://archivo.contagioradio.com/persiste-movilizacion-contra-la-reforma-laboral-en-francia/)

Entre tanto, Macron se ha mostrado **a favor de los Tratados de Libre Comercio -TLC - con Estados Unidos y Canadá** y pretenderá igualar las tasas de impuestos para las empresas.  Le puede interesar: [Francia aprueba ley que permite a fuerzas de seguridad espiar sin orden judicial](https://archivo.contagioradio.com/francia-aprueba-ley-que-permite-a-espionaje-actuar-sin-orden-judicial/)

Por su parte, **Jean- Luc Mélechon** dio una alocución en la que invitó a sus seguidores a resistir y a unirse en un "nuevo movimiento de masa a la cabeza de un nuevo país". Y agregó que **"si usted reconoce el humanismo, el ecologismo y lo social, yo me esforzaré para tomar la palabra".**

Y finalizó haciendo un llamado a que se mantengan la esperanza y la fuerza en las elecciones legislativas que cumplen su segunda vuelta este 18 de junio.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

