Title: El cine necesita de nuevas plataformas para ser visible
Date: 2016-12-12 15:05
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Javier Porta Fouz, Latinoamérica, Qubit
Slug: cine-plataformas-vod-qubit
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/FullSizeRender-e1481550360121.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Qubit.tv 

##### 12 Dic 2016 

El panorama cinematográfico en Latinoamérica y Colombia, viene evidenciado en los últimos años un **crecimiento significativo y reconocimiento a nivel internacional**. Para hacer un análisis sobre la actualidad de nuestros cines, conversamos con **Javier Porta Fouz, director del Festival de Cine Independiente de Buenos Aires (Bafici) y curador de la nueva plataforma especializada en cine Qubit.tv**.

Luego de la creación de la Ley del Cine en el 2003, **el número de producciones colombianas, mejoró notablemente**, no sólo a nivel de cantidad sino en calidad, aunque, las grandes producciones estadounidenses siguen siendo las preferidas del público, un fenómeno que se replica a lo largo del continente.

Asi mismo, **muchos directores iberoamericanos han recibido llamados de los grandes estudios hollywoodenses**, lo que para Porta Fouz significa "**reconocer el talento y la capacidad de los cineastas latinoamericanos** pero además demuestra el cambio de la lógica del cine mundial".

De igual manera, como respuesta a este cambio, ha surgido el VOD (video bajo demanda), plataformas web donde se puede encontrar todo tipo de cine. Para Porta Fouz el VOD "**es un lugar natural, donde la gente que no vive en las grandes ciudades, o que aprecia las producciones que no se ven en las salas de cine, pueden encontrarlas y verlas cuando quiera**".

Entre estos nuevos portales se encuentra **Qubit.tv**, una plataforma que a diferencia de las más conocidas, está **especializada en cine**. Porta Fouz como curador de Qubit, quiere **ofrecer un menú variado, desde stop motion y animación hasta cortos y largometrajes**. Para esta plataforma es un reto ofrecer producciones que no se ven en las salas de cine. Le puede interesar: [Convocatoria abierta para el 4to Festival de Cine por los Derechos Humanos](https://archivo.contagioradio.com/convocatoria-festival-de-cine-ddhh/).

Finalmente, Porta Fouz hace unas recomendaciones para iniciarse en este portal, tales como: **Pequeños Jugadores**, una serie francesa en técnica de Stop Motion; la comedia **Afternoon Delight**, **Madagascar 3** en animación; la colombiana **El Vuelco del Cangrejo** o alguna película de las varias películas de **Luis Buñuel** que se encuentran disponibles en la plataforma.

Si usted quiere acceder a esta plataforma, puede hacerlo a través de Qubit.tv y le redireccionará a su país, donde podrá hacer la suscripción. Los y las invitamos a escuchar completa la entrevista para conocer más sobre esta nueva experiencia.

<iframe id="audio_14898946" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14898946_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
