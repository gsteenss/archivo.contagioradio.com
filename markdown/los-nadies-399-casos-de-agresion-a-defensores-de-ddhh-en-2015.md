Title: “Los Nadies” 399 casos de agresión a defensores de DDHH en 2015
Date: 2015-08-17 10:33
Category: DDHH, Nacional
Tags: "Los Nadie", DDHH, Programa Somos Defensores, video de lanzamiento
Slug: los-nadies-399-casos-de-agresion-a-defensores-de-ddhh-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/LOSNADIES1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: somosdefensores 

###### [17 ago 2015] 

Con el fin de promocionar el más reciente  informe semestral del **Programa Somos Defensores**, será publicado el vídeo “Los Nadies", una producción que intenta narrar la labor de defensores de derechos humanos en Colombia ante la criminalización de la que han sido víctimas.

El informe titulado “**Sistema de información sobre agresiones contra defensores de Derechos Humanos**” notificará entre otros eventos, los 399 casos de agresión que desde enero hasta junio del presente año han padecido defensores y defensoras, casos compuestos por 332 amenazas de las que han sido parte, 34 asesinatos, 25 víctimas de atentados, 4 detenidos arbitrariamente y 3 judicializados.

El lanzamiento del informe será el próximo 18 de agosto en la página web del Programa Somos Defensores.

\[embed\]https://www.youtube.com/watch?v=1FD51FjhW7Q\[/embed\]
