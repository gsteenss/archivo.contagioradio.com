Title: ¿Qué ha pasado con la implementación de la JEP?
Date: 2018-06-18 16:33
Category: Expreso Libertad
Tags: Congreso, Fiscalía, JEP
Slug: implementacion-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/1521051977_757465_1521052414_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [12 Jun 2018] 

El abogado Juan David Bonilla, integrante de la Corporación Solidaridad Jurídica manifestó que una de las grandes diferencias entre la Jurisdicción Especial para la Paz y la justicia ordinaria, que imparte la Fiscalía general en Colombia, tiene que ver con su voluntad de ser restaurativa y reparar el tejido social que se afectó una vez cometido el delito, hecho que podría comprobar que incluso este tipo de justicia sea mucho más eficiente y eficaz a la hora de generar un proceso de reincorporación social.

<iframe id="audio_26601329" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26601329_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
