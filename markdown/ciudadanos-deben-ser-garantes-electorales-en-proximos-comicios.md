Title: Ciudadanos deben ser garantes electorales en próximos comicios
Date: 2018-06-01 12:28
Category: Nacional, Política
Tags: elecciones, formularios E-14, MOE, resultados electorales
Slug: ciudadanos-deben-ser-garantes-electorales-en-proximos-comicios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/WhatsApp-Image-2018-05-30-at-5.55.47-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Popayán] 

###### [01 Jun 2018] 

La Misión de Observación Electoral presentó el informe de su balance final sobre las observaciones de las denuncias sobre el proceso de revisión de escrutinios realizado en la primera vuelta de las elecciones presidenciales. En el proceso detectó anomalías en las cifras de votación de **363 formularios de delgados E-14.** Esto representa una variación de 12.522 votos.

De acuerdo con la MOE, el análisis se hizo sobre el 13% del total de las mesas de votación que hay disponibles en Colombia por lo que analizaron **13.135 formularios**. Dice la entidad que “si se considera que la muestra es representativa en la votación de 19,6 millones de sufragios podría haber anomalías equivalentes a casi 70.000 votos”.

### **Informe responde a denuncias de los ciudadanos** 

Según Alejandra Barrios, directora de la MOE, el análisis se hizo “para dar respuesta a las inquietudes sobre las presuntas modificaciones de los Formularios E14 de Delegados** **publicados en la página web de la Registraduría”. Para esto, tuvieron en cuenta los formularios de los puestos de votación **mayor al 70%** de 160 municipios de 24 departamentos.

Esta muestra se hizo gracias al **trabajo de los ciudadanos** que realizaron las denuncias en redes sociales sobre las alteraciones de los formularios y los que tienen anomalías reflejaron que “no había concordancia con el formulario posterior de escrutinio”. Esto, teniendo en cuenta que el proceso de conteo requiere de tres copias iguales en donde la información debe ser la misma. (Le puede interesar:["Votos de Fajardo y De la Calle el escenario de disputa para elecciones 2018"](https://archivo.contagioradio.com/votos-de-fajardo-y-de-la-calle-el-escenario-de-disputa-para-elecciones-0218/))

### **Alteración de votos se hizo en su mayoría para aumentar cifras que para reducirlas** 

Con esto, se concluyó que 11.726 votos se usaron para aumentar las cifras de votos para unos candidatos y 796 votos redujeron la cifra de votos. Con esta información, la MOE realizó una proyección **frente al total de votos registrados** por lo que concuerdan que pudo haber una afectación en 70.000 votos “que no afectan el resultado” debido a que la diferencia de votos entre un candidato y el otro no supera esa cifra.

Si bien la cifra es alta, la MOE ha venido realizando diferentes recomendaciones que **no han sido tenidas en cuenta** por los entes que regulan el proceso electoral. Dentro de esas está que “no debería haber tres formularios E-14 que funcionan como un teléfono roto pero en papel” pues esto facilita la existencia de errores por parte de los jurados de votación.

Sin embargo, **“si se quisiera modificar un resultado, es muy fácil hacerlo”**. Por esto, debe haber un mecanismo que modifique la forma como se están marcando los número dentro del E-14 “para que la marcación sea más clara y no se puedan hacer remiendos o enmendaduras”. También debe haber “auditorias antes y después del proceso electoral para brindar confiabilidad”. (Le puede interesar:["Hay empate electoral en Colombia, todo puede pasar": Fernando Giraldo"](https://archivo.contagioradio.com/hay-un-empate-electoral-en-colombia-todo-puede-pasar-fernando-giraldo/))

### **Cuando Fiscal revele corrupción en compra de votos se sabrá cómo funciona la compra de votos en el país** 

Frente a los anuncios del Fiscal General de Nación, quien indicó que en las elecciones a Congreso **hubo compra de votos**, la MOE afirmó que “no estamos frente a nada nuevo y es un parte positivo de la Fiscalía”. Aseveró que cuando se hagan las pruebas, la entidad podrá tener más ejemplos de cómo funciona la compra y manipulación de los votos.

Recordó que aún no está clara la información que maneja el ente acusador pero “por lo que dijo el Fiscal, se entiende que **va a presentar la cadena de corrupción** de la que hacen parte candidatos, líderes que compran votos y jurados de votación”. Dentro de esta cadena, “hay una parte que afecta la manipulación o errores y modificaciones de los formularios donde se consignan los resultados cuando termina la jornada electoral”.

Finalmente, la MOE recordó la **importancia de que los ciudadanos** realicen un ejercicio de vigilancia durante los procesos electorales. Indicó que los jurados de votación del pasado 27 de mayo serán los mismos para la segunda vuelta presidencial y enfatizó en que el sistema electoral permite los errores humanos sin dejar por fuera que hubo errores sin justificación donde se incurrió en una falta.

<iframe id="audio_26304556" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26304556_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
