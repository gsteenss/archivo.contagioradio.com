Title: "Sola en la Oscuridad" suspenso en el Teatro La Mama
Date: 2016-09-21 15:19
Category: Cultura, eventos
Tags: Bogotá, teatro, Teatro la mama
Slug: sola-en-la-oscuridad-suspenso-en-el-teatro-la-mama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Sola-en-la-Oscuridad-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teatro La Mama 

###### [21 Sep 2016] 

Desde hoy y hasta el próximo 24 de Septiembre se presenta en el Teatro La Mama de Bogotá, en corta temporada, la obra "**Sola en la oscuridad**", segunda producción de la compañía **Teatro de Subida**, dirigida por Sebastián Payan.

La dramaturgia de "Sola en la oscuridad" esta inspirada en la película homónima de 1967, dirigida por Terence Young y protagonizada por Audrey Hepburn, retomando varios de los elementos que conforman el argumento original y trasladando a un entorno más local los personajes que hacen parte de esta.

**Nicolas Torres,** quien interpreta a Cabrera asegura que en la adaptación "cambian muchísimas cosas por que estamos halblando de dos lenguajes completamente distintos como el cinematográfico y el escénico pero queremos conservar esa tensión y esa forma de crear suspenso en el público" lo que ha llevado a los actores a realizar mucho trabajo de campo al momento de construir sus personajes.

El Teatro de Subida es una agrupación creada en 2015 por un grupo de amigos profesionales y estudiantes de diferentes disciplinas, unidos por su amor a las artes dramáticas y su entusiasmo por experimentar y crear su propio estilo. Su debút en la escena se dió con la adaptación de la obra de Miguel de Unamunno "**Niebla**".

El equipo de producción de "Sola en la oscuridad" esta compuesto por el director Sebastián Payan, July Fajardo en la Producción, Mónica Feldman encargada de la Escenografía y Juliana Pinto, directora de Ambientes, con un elenco integrado por Wendy Sánchez, Daniel Tocaría, Andrés Gaitán, Nicolás Torres, Esteban Zapata y Catalina Morales.

**Sinopsis**

Samuel Hoyos conoció a su esposa Susana después de un trágico accidente donde ella perdió la capacidad de ver. El trabajo de Samuel lo obliga a dejarla por largos periodos de tiempo sola en casa, pero esta vez todo será diferente. En lo que parece un juego entre policías y ladrones, tres hombres intentarán manipular a Susana para obtener una valiosa muñeca que creen está en su casa. ¿Lograrán estos hombres engañar a Susana?

**Lugar:** Teatro La Mama Calle 63 No. 9-60

**Hora**: 7:30 p.m.

**Boletería:** 25.000 general, 15.000 estudiantes

<iframe id="audio_13001519" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13001519_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
