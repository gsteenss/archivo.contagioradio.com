Title: "Que la finalización de la dejación de armas no sea el fin de los esfuerzos de paz"
Date: 2017-09-22 16:43
Category: Nacional, Paz
Tags: Dejación de armas, FARC, Gobierno, proceso de paz
Slug: finalizacion-de-la-dejacion-de-arma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/dejacion-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República] 

###### [22 Sept. 2017] 

En un acto que contó con la presencia del jefe de la Misión de la ONU, Jean Arnault y el presidente de Colombia Juan Manuel Santos, se realizó **el acto de inhabilitación final de las armas luego de la dejación de armas de esa guerrilla en Funza**, Cundinamarca estaban almacenadas en los contenedores que salieron de las diversas zonas veredales.

De esta manera, aseguró Jean Arnault **se da por terminada esta fase, quedando pendiente solamente la realización de 3 monumentos y** cumpliendo con el primer mandato de la ONU acordado para esta fase del proceso de paz. El próximo mandato inicia el 26 de septiembre y tendrá la tarea de acompañar la implementación de los Acuerdos de Paz.

En su discurso Arnault dijo que **“confiamos que la finalización de la dejación de armas no sea el fin de los esfuerzos de paz,** (…) la Misión de las Naciones Unidas en Colombia formaliza el fin de las actividades del Cese al Fuego Bilateral y Definitivo entre el Gobierno Nacional y las FARC-EP, y el proceso de Dejación de Armas de las FARC-EP, culminando satisfactoriamente lo establecido en el punto 3 “Fin del conflicto” del Acuerdo Final de Paz”.

### **¿Qué se entregó e inhabilitó finalmente?** 

Aseguró la Misión de la ONU que en el consolidado final pudieron contabilizar 8.994 armas, entre largas, cortas y  de apoyo al combate como lanzacohetes y lanzagranadas. Así mismo, se inhabilitaron 1.765.862 municiones de diversas armas, 38.255 kilogramos de explosivos diversos, 51.911 metros de cordón detonante y mecha lenta, 11.015 granadas, de mano, 3.528 minas antipersonal, entre otros elementos.

Con respecto a las caletas **la Misión neutralizó 750 y 351 fueron entregadas por parte del Ministerio de Defensa**, para un total de 1101 caletas desmanteladas. Le puede interesar: [Con la dejación de armas se está cerrando la página de la guerra en Colombia](https://archivo.contagioradio.com/dejacion-armas-farc/)

Cuando se habla de inhabilitación lo que se hace con las armas es cortarlas y posteriormente verificar que no sirven más, todo ello se consigna en un documento en el cual también se incluye el tipo de armas, el nombre de quién realizo la deshabilitación y que forma se uso para dejarla inhabilitada.

Por último, **la Misión de la ONU agradeció a todos quienes han participado en este proceso,** pero de manera especial a todos los pobladores de las comunidades que acompañaron el día a día durante este proceso del Cese al Fuego y de Hostilidades. Le puede interesar: [Dejación de armas de las FARC –EP marca un cambio en la historia del país](https://archivo.contagioradio.com/42169/).

Al envento no asistió Iván Marquez ni ningún integrante de las FARC pues muchos de ellos estaban asistiendo al acto de homenaje a Víctor Julio Suarez Rojas, conocido como el Mono Jojoy o Jorge Briceño. Sin embargo han sido varias las voces de esa organización que han señalado los reiterados incumplimientos del gobierno por lo que la no asistencia a este evento podría interpretarse como un desagravio al gobierno y al presidente Santos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radi](http://bit.ly/1ICYhVU)o
