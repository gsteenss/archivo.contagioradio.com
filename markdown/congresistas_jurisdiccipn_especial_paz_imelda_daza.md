Title: "Se está usando el soborno para que los congresistas hagan lo que deben hacer": Imelda Daza
Date: 2017-11-01 13:52
Category: Nacional, Política
Tags: circunscripciones especiales de paz, jurisdicción especial para la paz, Reforma Política, voces de paz
Slug: congresistas_jurisdiccipn_especial_paz_imelda_daza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/542667_1-e1509559241548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: León Darío Péez, Semana] 

###### [1 Nov 2017] 

[Por lo menos **20 impedimentos tendrán que ser resueltos hoy antes de la votación de la Ley estatutaria de la Jurisdicción Especial de Paz.** Mientras continúan las demoras en el congreso para aprobar la JEP, las organizaciones sociales piden que sea aprobada la iniciativa que puede poner en marcha el Sistema Integral de Verdad Justicia, reparación y no repetición.]

Imelda Daza, integrante de Voces de Paz, denuncia que sectores políticos están usando los impedimentos al artículado de la JEP para relentizar su trámite legislativo. Asimismo, expresa en la Cámara de representantes no se ha querido dar curso normal, ágil y rápido a la reforma política, a las circunscripciones, y la Jurisdicción, para ella **los impedimentos "se convirtieron en la manera más eficaz para extorcionar al gobierno para darle paso a las aprobaciones de los proyectos"**, dice.

Este miércoles, la discusión sobre la reforma política continúa en la Cámara, y se esperaría que se avance también el debate en torno a las 16 Circunscripciones Especiales de paz, sin embargo,  no se podría iniciar la discusión sobre ese punto hasta que **Rodrigo Lara, presidente de la Cámara, no presente la ponencia referente a ese tema.**

### **Continúa la "mermelada"** 

"Aprobar los proyectos de Ley es una tarea prioritaria para que se normativice el acuerdo y las instituciones tengan herramientas de Ley para implementar el acuerdo", señala Imelda Daza, sin embargo, pese a que este 11 de noviembre tendrían que estar inscritos los candidatos para las circunscripciones de paz, dicha Ley aún no está lista.

Para la integrante de Voces de Paz, si bien, el presidente Juan Manuel Santos, muestra "la mejor disposición para implementar el acuerdo de paz", el problema es que "la coalición en el congreso que lo respalda no funciona". De hecho según denuncia Daza, en los pasillos del Congreso se dice que se está **repartiendo mermelada, y para que se votara la Ley estatutaria de la JEP, el gobierno "tuvo que dar la Aeronáutica civil".**

Este miércoles a las 3 de la tarde empezarían las discusiones tanto en Cámara como en Senado, sobre la Reforma Política, la JEP y posiblemente las circunscripciones; ejes centrales del acuerdo de paz con las FARC.

<iframe id="audio_21825786" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21825786_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

<div class="osd-sms-title">

Compartir:

</div>

</div>
