Title: 'La Revolución de las Sotanas' sigue vigente: Javier Dario Restrepo
Date: 2016-04-21 13:23
Category: eventos, Nacional
Tags: Camilo Sacerdote y Revolucionario, Javier Darío Restrepo, La Revolución de las Sotanas, Rescoldo Bajo Cenizas
Slug: la-revolucion-de-las-sotanas-sigue-vigente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Javier-Dario-Restrepo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Premio ggm ] 

###### [21 Abril 2016 ] 

Este jueves desde las 5 de la tarde en el Colegio Americano de Bogotá, se presenta la segunda edición de 'La Revolución de las Sotanas', libro escrito por el periodista Javier Darío Restrepo y publicado por primera vez en 1995, en el que se reflexiona sobre el aporte de la iglesia en la construcción de la historia de Colombia.

De acuerdo con el periodista las consideraciones del libro mantienen vigencia, hoy 21 años después de su publicación un amplio sector de la **iglesia católica le apuesta a la preferencia por los pobres**, tal como lo hicieron [[Camilo Torres](https://archivo.contagioradio.com/?s=camilo+torres)] y los sacerdotes de la Golconda, quienes como el Papa Francisco insistieron en poner el Evangelio al servicio de la sociedad.

Los [[discursos del Papa](https://archivo.contagioradio.com/historico-discurso-del-papa-francisco-ante-la-onu/)], evidencian el dinámico desarrollo que ha tenido el pensamiento de la iglesia, desde los preceptos de la teología de la liberación, el Concilio Vaticano Segundo y la Asamblea Episcopal de Medellín; la gran novedad está en el énfasis del Sumo Pontífice en la importancia social de los pobres, en su llamado a la sociedad para que no continúe siendo dócil adoradora del dinero y para que se libere de la **doble dictadura del dinero y la tecnología**,que impulsa [[acciones destructivas](https://archivo.contagioradio.com/?s=mineria+)] como las autorizaciones a empresas mineras.

Para el periodista el que en las parroquias no se hable de este discurso liberador es un problema de vieja data, que tiene que ver con las **distintas posiciones que adoptan sacerdotes y religiosas** como "signos de contradicción" que no desaparecerán, pero que no impedirán que mueran las obras de quienes "hicieron el bien sin hacer ruido", como lo hizo el padre Javier de Nicoló.

El verdadero cristianismo se expresa en el amor al prójimo, "la iglesia avanzará a paso franco desde el momento en que para ella lo principal sea el amor al prójimo, **la iglesia se estancará en la medida en que para ella lo principal sean los ritos y los sacramentos**, que deben ser parte de la pedagogía para enseñar el amor al prójimo, sí no lo están enseñando se están convirtiendo en elementos inútiles", afirma Restrepo.

"La apuesta no es para tener cada vez más feligreses, a la iglesia ese elemento cuantitativo no tendría por qué importarle, lo importante es que la humanidad entienda que el camino es por el amor al prójimo, el camino no es por el odio por el rencor, por la rivalidad, la discriminación, el camino es por la **aceptación del otro, y amar al otro es la gran clave de la paz, el progreso y el desarrollo de la sociedad**", concluye el autor.

El lanzamiento de 'La Revolución de las Sotanas', junto con el de 'Camilo Sacerdote y Revolucionario' de Francisco Trujillo y 'Rescoldo Bajo Cenizas' de de Ignacio Betancourt, se hará en el Centro Americano ubicado en la Carrera 22 \#45 - 51 desde las 5 de la tarde.

<iframe src="http://co.ivoox.com/es/player_ej_11255363_2_1.html?data=kpafl5qXepShhpywj5ebaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncavV187S1JCopdOZpJiSo6nTb7PZ1Nnfx9XTb46fscrfy9TIrdTowpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 
