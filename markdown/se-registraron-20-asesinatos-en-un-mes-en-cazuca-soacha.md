Title: En un mes han asesinado a 20 personas en Cazuca
Date: 2016-11-25 13:11
Category: DDHH, Nacional
Tags: asesinatos en Soacha, Educación Popular
Slug: se-registraron-20-asesinatos-en-un-mes-en-cazuca-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Soacha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Soacha] 

###### [25 Nov 2016] 

Miembros de la Escuela de Educación Popular Fe y Esperanza ubicada en la comuna 4 “Cazuca” en Soacha, denunciaron que se viene gestando un plan de **“limpieza social”** al interior de 70 barrios, que ya ha cobrado la vida de dos familiares de personas que hacen parte de la Escuela  y de **20 personas más, sin que las autoridades o entes administrativos, se pronuncien frente a este hecho.**

De acuerdo con una de las integrantes de esta Escuela, esta no es una situación nueva o que no sea de conocimiento público para las autoridades, sino que es producto “**del olvido estatal, el cinturón de miseria, tráfico y negocios ilegales de paramilitarismo y bandas criminales que ejercen el control y dominio en el territorio”.**

En este último mes se hizo el anuncio de una mal llamada “limpieza social” que hasta el momento ha cobrado la vida de 20 personas que según la integrante de la Escuela Fe y Esperanza, habría sido ordenada por grupos paramilitares, en cabeza del Bloque Capital paramilitar, que ha tenido presencia histórica en este lugar. A su vez la integrante señala que al mismo tiempo se ha **iniciado una persecución a los miembros de la Escuela Fe y Esperanza.** Le puede interesar[:"77 personas asesinadas entre enero y julio en Soacha"](https://archivo.contagioradio.com/77-personas-asesinadas-entre-enero-y-julio-en-soacha/)

Hace tres años uno de los fundadores de la Escuela tuvo que desplazarse de la comuna 4 por diversas amenazas, hace dos semana fue asesinado  Mario Rivera, familiar del fundador de la escuela que se desplazó, su cuerpo fue encontrado con señales de tortura. De acuerdo con la integrante de la Escuela, se ha intentado **poner el denuncio de estos hechos, sin embargo los familiares de las personas asesinas están siendo hostigados por los paramilitares para que desistan de esta acción**

La Escuela ha decidido que pese a los riesgos que implica continuar en el territorio lo harán y esperan que más organizaciones se sumen a visibilizar esta denuncia que ahora no solo amenaza la posibilidad de que continúe un proyecto popular en Cazuca, sino que además **expone la realidad de uno de los cordones de miseria más olvidados del país como lo son las comuna de Soacha y Ciudad Bolívar.**

<iframe id="audio_14020699" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14020699_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
