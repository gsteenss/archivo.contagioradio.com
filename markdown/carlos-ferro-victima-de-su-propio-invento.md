Title: "Carlos Ferro fue víctima de su propio invento", Ricardo Montenegro
Date: 2016-02-18 14:02
Category: LGBTI, Nacional
Tags: Carlos Ferro, la comunidad del Anillo, LGBTI
Slug: carlos-ferro-victima-de-su-propio-invento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Carlos-Ferro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

<iframe src="http://www.ivoox.com/player_ek_10486923_2_1.html?data=kpWhmpuddpShhpywj5aXaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncYamk6jO1NHTt4y6xtff0ZDKucaf14qwlYqliMToytLOjcnJb9TpjNXf0dXNs4zdz9vS0NnTaZOmhpewjbfNp8LmjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ricardo Montenegro] 

###### 18 Feb 2016 

**“Personas como Carlos Ferro, que no llevan una vida abiertamente homosexual o bisexual y que además engañan a su familia o pareja son víctimas de su propio invento**, porque este tipo de personas al tratar de encubrir su secreto van a seguir haciendo hasta lo imposible para que su secreto no se revele”, dice Ricardo Montenegro, abogado y defensor de DDHH de la comunidad LGBTI, frente a la publicación con contenido sexual por el cual Ferro decidió retirarse de su cargo como viceministro del Interior.

Este miércoles renunció la periodista y directora de La FM, Vicky Dávila, tras los comentarios desde diferentes esferas de la sociedad colombiana, por haber decidido publicar ese video. Sin embargo, para el abogado Montenegro, no se trató de una violación a la intimidad del exviceministro, sino que “**hay un personaje público que ha tenido posiciones de poder y que se ha hecho el bobo cuando ha sido senador y utiliza esa condición de ocultamiento para seguir ocultando otro tipo de delitos”**, dice el abogado.

De acuerdo con él, las declaraciones en Blu Radio del exsenador, están cargadas de “cinismo... meses atrás el exsenador había dicho que no conocía esta persona, que no tenía ningún tipo de contacto homosexual con nadie”,  pero con el video, “entre líneas se revela que hay un tema entre cadetes y personas con poder…  ahora a nadie le queda duda de que algo sucede al interior de la policía”.

El abogado, además denuncia que cuando Ferro era senador votaba o entorpecía proyectos de Ley que buscaban garantizar los derechos de la comunidad LGBTI, como sucedió con la iniciativa del senador **Armando Benedetti**, quien proponía **regular la unión civil entre las parejas del mismo sexo, mediante la celebración de un contrato ante los notario.** Un proyecto que ha sido presentado 9 veces en el congreso, y que senadores como Carlos Ferro no apoyaron.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
