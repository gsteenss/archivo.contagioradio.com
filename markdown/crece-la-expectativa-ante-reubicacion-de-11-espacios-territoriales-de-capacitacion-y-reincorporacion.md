Title: Crece la expectativa ante reubicación de 11 Espacios Territoriales de Capacitación y Reincorporación
Date: 2019-05-27 16:32
Author: CtgAdm
Category: Nacional, Paz
Tags: etcr, FARC, Implementación de Acuerdo de paz
Slug: crece-la-expectativa-ante-reubicacion-de-11-espacios-territoriales-de-capacitacion-y-reincorporacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/MG_5591.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ETCR.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Foto: @ETCRUriasRondon] 

A propósito del cambio de figura jurídica que sufrirán a partir de agosto los **Espacios Territoriales para la Capacitación y la Reincorporación (ETCR)**, donde habitan cerca de **8.000 excombatientes de FARC** que se acogieron al Acuerdo de Paz,  el Gobierno anunció que serán reubicados 11 de estos 24 lugares. Tal anuncio incrementó la expectativa que existe al interior de esta población  que no ven garantías frente a un Gobierno que no ha cumplido la implementación de los Acuerdos de Paz.

Emilio Archila, consejero presidencial para la Estabilización y Consolidación, argumentó que no se puede dar continuidad a estas 11 zonas en dichas ubicaciones y **deben ser trasladadas por problemas de adquisición de predios, pues muchos de estos territorios son parte de zonas indígenas o de reservas forestales**, e incluso argumentó que algunos deben ser trasladadas por condiciones de seguridad.

Freddy Escobar, habitante del ETCR, Simón Trinidad ubicada en el municipio de **Manaure, en La Guajira,** donde actualmente viven 291 personas, señala que mientras el anuncio se hace oficial, "son noticias que reciben con expectativa". Agregó, que la principal inquietud es la "miseria y pobreza " al interior de la comunidad, que aguarda a que los proyectos productivos sean apoyados oportunamente por el Gobierno.

El habitante del ETCR  indicó que en la costa Caribe, el asesinato del menor Samuel David en abril de este año fue el único incidente de agresiones contra excombatientes. Sin embargo, señaló que la mayor de sus preocupaciones es la incertidumbre, derivada de la recaptura de **Jesús Santrich** lo que sienten, no garantiza su seguridad jurídica. [(Lea también; Samuel David, hijo de excombatientes fue víctima de la guerra con tan solo siete meses)](https://archivo.contagioradio.com/samuel-david-hijo-de-excombatientes-fue-victima-de-la-guerra-con-tan-solo-siete-meses/)

Durante los dos años en que el ETCR Simón Trinidad lleva funcionando, sus habitantes han consolidado poco a poco un proyecto de vivienda y una reunificación familiar que ha permitido un aumento en la población **"para reafirmar nuestro compromiso con ese territorio".** [(Le puede interesar: La campaña que busca llevar agua al Espacio Territorial con más habitantes de Colombia)](https://archivo.contagioradio.com/la-campana-que-busca-llevar-agua-al-espacio-territorial-con-mas-habitantes-de-colombia/)

Aunque el Gobierno afirmó que continuará aportando la renta básica mensual y trabajando en el acceso a territorios para desarrollar proyectos, algo que continúan esperando tal como lo señaló **Marco Martínez, líder del ETCR de Pondores, La Guajira**, espacio que permanecerá en su ubicación original, quien apuntó, "lo que hay que hacer son acciones concretas con proyectos productivos que impacten la región y también beneficien las comunidades que están en los alrededores".**  
**

### ¿Cuáles son los Espacios Territoriales que serán reinstalados?

Desde los Espacios Territoriales afirman que continúan a la espera de saber cuáles serán las zonas que serían reinstaladas definitivamente,  se estableció que los ETCR que serán reubicados son:

-   Yarí (La Macarena, Meta)
-   Filipinas (Arauquita, Arauca)
-   Caracolí (Carmen del Darién, Chocó)
-   Carrizal (Remedios, Antioquia)
-   Charras (San José del Guaviare, Guaviare)
-   La Variante (Tumaco, Nariño)
-   Santa Lucía (Ituango, Antioquia)
-   La Pradera (Puerto Asís, Putumayo).
-   El Ceral-La Elvira (Buenos Aires, Cauca)
-   Caño El Indio (Tibú, Norte de Santander)
-   San Antonio (Caldono, Cauca)

Según organizaciones sociales, en el espacio de Carmen del Darién, Chocó, sus líderes adelantan toda la gestión para poderse quedar en el territorio y esperan reunirse con la Agencia para la Reincorporación y la Normalización (ARN) y el integrante del partido FARC, Pastor Alape.

### Ordenamiento territorial

De igual forma, se vienen adelantando reuniones con las Alcaldías y Gobernaciones de los municipios en cada ETCR para  incluir estos territorios en sus planes de ordenamiento territorial, una información que confirmó Escobar, "nosotros ya habíamos avanzado en eso, la finca en que estamos fue incorporada al plan de ordenamiento territorial en el municipio de Manaure para que se pueda construir, es decir, paso de zona rural a zona suburbana", explicó.

Escobar espera que se den las garantias para que no solo se trate de un tema de reubicación, ya que  al llegar a Manaure los frentes 19 y 41 sumaban 162 personas, cifra que hoy es casi el doble, **demostrando un aumento notable en la población, que hace necesario un reacondicionamiento de las condiciones del ETCR** para hacer frente a problemas como el hacinamiento.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
