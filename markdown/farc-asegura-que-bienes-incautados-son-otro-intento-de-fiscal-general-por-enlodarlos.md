Title: FARC asegura que bienes incautados son otro intento de Fiscal General por "enlodarlos"
Date: 2018-02-21 12:04
Category: Nacional, Paz
Tags: FARC, Fiscalía General de la Nación, Néstor Humberto Martíne
Slug: farc-asegura-que-bienes-incautados-son-otro-intento-de-fiscal-general-por-enlodarlos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Nestor-Humberto-Martinez-e1468345311281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicidiario] 

###### [21 Feb 2018] 

A través de un comunicado de prensa, la FARC afirmó que la información suministrada por el Fiscal General, Néstor Humberto Martínez, sobre los bienes que están siendo expropiados y que no aparecían en la lista de esta organización, hacen parte de una “noticia-espectáculo” que tiene el interés de **“enlodar” el nombre del nuevo partido político y que al mismo tiempo viola las garantías procesales de las personas capturadas**.

Los bienes que, de acuerdo con el Néstor Humberto Martínez, pertenecen a la FARC y no aparecen en la lista enviada el pasado 5 de agosto de 2017, son los diferentes supermercados Supercundi y Merkandrea, ubicados en diferentes lugares del departamento de Cundinamarca, **que estarían avaluados por 650 mil millones de pesos**.

En el comunicado la FARC afirmó que todos sus bienes fueron debidamente incluidos en el inventario que le entregaron al gobierno, en atención estricta a lo establecido en el Dercreto – Ley 903 de 2015. Así mismo, el nuevo partido político señaló que todas las afirmaciones que ha hecho el Fiscal sobre **“fabulosas cifras de bienes no declarados”** tendrán que ser demostradas.

“No puede hacer carrera el juzgamiento y la condena a través de los medios de comunicación, por cuenta de un sistema de justicia que evidencia la existencia de un cartel de testigos”, señaló la FARC. (Le puede interesar: ["La telaraña de falsos testigos y colaboradores del Álvaro Uribe Vélez"](https://archivo.contagioradio.com/la-telarana-de-falsos-testigos-y-colaboradores-de-uribe-velez/))

 Así mismo la organización aseveró que esta no es la primera vez que Néstor Humberto Martínez intenta estropear la construcción de paz en el país y el tránsito de la FARC a la vida política, ejemplo de ello fue su oposición a la creación y puesta en marcha de la Unidad de Investigación Especial de los crímenes del paramilitarismo, “su trabajo sistemático de alteración del sentido y los contenidos de la Ley Estatutaria de la Jurisdicción Especial para la Paz durante su trámite en el Congreso de la República” y la bofetada que le dio a las víctimas del conflicto, al excluir de la JEP a los altos mandos militares y a los llamados civiles terceros y los funcionarios civiles del Estado.

### **¿Quiénes son los capturados?** 

Las personas acusadas son Uriel, Norberto, Alirio y Yaneht Mora Urrea, frente a ellos el partido político afirmó que “de manera irresponsable la entidad a su cargo ha señalado a los propietarios de una reconocida cadena de supermercados y otras propiedades, como supuestos testaferros de las FARC-EP", **provocando saqueos y disturbios en diferentes territorios del país**.  (Le puede interesar: ["Intromisión del Físcal ha sido funesta para la JEP: Imelda Daza"](https://archivo.contagioradio.com/47601/))

###### Reciba toda la información de Contagio Radio en [[su correo]
