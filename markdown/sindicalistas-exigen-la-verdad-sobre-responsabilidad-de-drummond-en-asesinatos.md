Title: Sindicalistas exigen la verdad sobre responsabilidad de Drummond en asesinatos
Date: 2015-05-27 16:18
Category: DDHH, Nacional
Tags: Alfredo Araujo, cesar, Dairo Mosquera, Drummond, Hugo Orcasita Amaya, Jorge 40., Sindicalismo, Sintramienergética, Valmore Locarno Rodríguez
Slug: sindicalistas-exigen-la-verdad-sobre-responsabilidad-de-drummond-en-asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/sintramienergetica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: www.eldia.co 

<iframe src="http://www.ivoox.com/player_ek_4559090_2_1.html?data=lZqim5WddI6ZmKiak5WJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy4ws7f0ZCxs9Tl1srfw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Dairo Mosquera Tesorero de SINTRAMIENERGETICA] 

Este 26 de Mayo fue capturado **Alfredo Araujo Castro**, Ex Gerente de relaciones públicas de la multinacional **Drummond** en el departamento del Cesar, por su presunta responsabilidad intelectual en el asesinato de los sindicalistas **Valmore Locarno Rodríguez** y **Víctor Hugo Orcasita Amaya.** Esta captura se produce luego de 14 años de los hechos y, según las autoridades, solamente tiene fines de indagatoria.

**Jorge 40, "Viejo Miguel" y Jaime Blanco,** Paramilitares y coautores del asesinato, ya han sido condenados por el crimen. **Dairo Mosquera**, tesorero nacional de **Sintramienergética,** asegura que los mismos paramilitares han declarado que la Minera **Drummond** ha financiado el accionar criminal del paramilitarismo en Colombia, lo que para Mosquera es una clara evidencia de la responsabilidad de la empresa en estos asesinatos.

Según Mosquera, los funcionarios presionaron al sindicato como reacción a las inconformidades que **Valmore** expresó sobre la alimentación de los trabajadores de la Minera, que, a su criterio, no era adecuada. Los asesinatos de presidente y vicepresidente de SINTRAMIENERGETICA, se habrían producido por estas y otras reivindicaciones de los trabajadores.

Mosquera también recordó que la familia **Araujo** tiene poder en el **Cesar**, en el **Magdalena** y **Atlántico.** Sin embargo, no hay claridad de la implicación en estos crímenes de otros integrantes de la familia Araujo.

El sindicato de **Sintramienergética** condena todas las acciones que atentan contra sus trabajadores y están en busca de que se aclaren los hechos; que se continúe en la investigación, y que la situación se haga pública para que se preste atención a la clase trabajadora del país.

El asesinato ocurrió el **12 de Marzo de 2001** en el municipio de El Paso (Cesar). Los buses en los que viajaban los sindicalistas fueron interceptados por miembros de autodefensas que asesinaron inicialmente a Locarno, mientras que el cadáver de **Ocarsita** fue hallado al día siguiente en el corregimiento Loma Colorada.
