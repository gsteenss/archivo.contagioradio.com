Title: "Nos tomamos en serio nuestros sueños" Podemos en Madrid
Date: 2015-02-02 19:36
Author: CtgAdm
Category: El mundo, Política
Slug: nos-tomamos-en-serio-nuestros-suenos-podemos-en-madrid
Status: published

###### Foto:Publico.es 

##### [Javier Sanvicente]:<iframe src="http://www.ivoox.com/player_ek_4028718_2_1.html?data=lZWfmpyVfI6ZmKiakpyJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTkwoqwlYqmdcKZlKaYstTIqc7j1JDQ0dPas8TVjNLO1MjMpYzk0NeYx9GPp9Dhysrb3NSPqMbgjMjOz5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Podemos ha convocado a más de 300.000 personas en la madrileña plaza de la puerta del Sol."... s**oñamos pero nos tomamos en serio nuestros sueños..**", estas fueron las palabras más escuchadas durante el discurso que dieron los dirigentes en el acto político central del sábado 31 de Enero.

En lo que ha sido calificado por el partido como el inicio del cambio social y político en el país, su máximo dirigente, Pablo Iglesias, dio un discurso cargado de esperanza y fuertes **críticas contra las medidas de austeridad, la Troika, el gobierno alemán de Ángela Merkel y el actual gobierno español.**

También agradeció a todos los colectivos que han luchado desde el comienzo de la crisis, elogiando su perseverancia y su apoyo a la formación. Aprovechó para apoyar a Syriza, pero dejar claro que lo que suceda en España no será igual a lo que haga el partido hermano en Grecia, sino que será fruto de los propios españoles.

En otro lugares del mundo también hubo concentraciones de apoyo, así, es el caso de Bogotá, donde se reunieron cientos de militantes de ese partido político que habitan Colombia.

Para comprender mejor la magnitud del evento, entrevistamos a Javier Sanvicente, portavoz de Podemos Bogotá, quien nos explica las valoraciones sobre la marcha del sábado, así como de los posibles gobiernos de cara a las elecciones de Mayo de 2015.

Javier puntualiza que es necesario la **unión entre fuerzas de izquierda,** pero que esta no será sencilla debido a problemas internos de los propios partidos o a la amenaza que sienten algunos partidos clásicos por el elevado crecimiento de la formación de Pablo Iglesias.

En un contexto en el que el partido Syriza ha negado la interlocución de la Troika como válida, siendo apoyado por Obama, o el comisario de la Comisión Económica Europea, quién además ha añadido que **estaría dispuesto a disolverla para que Grecia pueda asumir la deuda de otra manera**, priorizando su crecimiento económico. Ángela Merkel continúa con su política hacia Grecia indicando que no hay negociación posible de la deuda.
