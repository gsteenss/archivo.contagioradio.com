Title: Fiscal argentino imputó a Macri por revelaciones en Papeles de Panamá
Date: 2016-04-07 11:20
Category: El mundo, Judicial
Tags: Mauricio Macri, Panama Papers, Paraisos fiscales latinoamerica
Slug: fiscal-argentino-imputa-cargos-a-macri-por-revelaciones-en-papeles-de-panama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/55e51135ac3ab_1420_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Mendoza Post 

##### [7 Abr 2016] 

Luego de relacionarse el nombre del presidente argentino Mauricio Macri, en los denominados "Papeles de Panamá", el fiscal federal Federico Delgado ordenó abrir investigación que permita determinar si el mandatario omitió en sus declaraciones juramentadas "de manera maliciosa" su participación en la firma Fleg Tranding Ltda.

El fiscal Delgado solicitó al juez Sebastián Casanello, investigar además las posibles irregularidades en la actividad de la sociedad registrada en Bahamas desde 1998 y con funcionamiento hasta 2009 por Franco Macri, padre del mandatario, causa resultante de la denuncia penal presentada por el diputado Darío Martínez.

En su compulsa de evidencias, el diputado solicita investigar si existe alguna irregularidad en la conformación y vida de esas sociedades, y si el presidente Macri como socio tuvo participación alguna en esas faltas.

Por su parte el fiscal Delgado no descartó que, en su rol como ciudadano sometido a un proceso penal, el mandatario sea llamado a declarar dentro de la investigación, para cual deberá comprobarse primero la veracidad de la denuncia.

En desarrollo...

   
 
