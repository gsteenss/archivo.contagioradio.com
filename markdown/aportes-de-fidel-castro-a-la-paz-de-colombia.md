Title: Fidel Castro un revolucionario comprometido con la Paz de Colombia
Date: 2016-11-26 10:43
Category: El mundo, Paz
Tags: Cuba, Fidel Castro, Revolución Cubana
Slug: aportes-de-fidel-castro-a-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/fidel-y-la-paz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Nov 2016] 

Durante sus 47 años al frente de Cuba, Fidel Castro procuró mantener buenas relaciones con países latinoamericanos, entre ellos está Venezuela, Ecuador, Argentina, Bolivia y Brasil. De igual forma, Castro también estuvo muy pendiente del co**nflicto que se gestaba al interior de Colombia y en muchas ocasiones estuvo en la disposición de mediar para que la paz fuese un hecho al interior del país.**

Uno de esos acercamientos al conflicto armado colombiano se dio bajo la presidencia de Álvaro Uribe en  agosto del 2010, en ese entonces la ex senadora Piedad Córdoba, en compañía del Defensor de Derechos Humanos Danilo Rueda, viajaron a Cuba para reunirse con el Comandante en Jefe y hablar de temas con el proceso de liberación de detenidos por la guerrilla de las FARC-EP y el ELN. Le puede interesar: ["Los tres discursos Históricos de Fidel Castro"](https://archivo.contagioradio.com/adios-a-fidel-castro-padre-de-la-revolucion-cubana/)

En la reunión a puerta cerrada se analizó **la importancia del diálogo político con ambas guerrillas para lograr cimentar la paz en Colombia**. Además se tocaron otros temas como la crisis alimentaria y ambiental del mundo, la eventualidad de dinámicas de avasallamiento de las fuerzas de vida y los Estados nación por las corporaciones. Dos días después en un nuevo encuentro con Castro, Piedad Córdoba convocó al lado de Danilo Rueda a Lisandro Duque, Hernando Gómez, Javier Giraldo y Carlos Ruiz, de Colombianos por la Paz, para profundizar en esos intentos por construir paz para Colombia.

Este es uno de los tantos acercamientos que finalmente permitió que Cuba fuese la sede de las conversaciones desde un principio entre el Gobierno Nacional de Colombia y la guerrilla de las FARC-EP, **convirtiendo a Cuba en el escenario más importante de diálogo para posteriormente dar el fruto de los Acuerdos de Paz firmados el 24 de noviembre.**

Esta disposición desde un inició de Castro y Cuba por ser garantes de paz para Colombia ha sido agradecido en escenario tan importantes como los discursos de cierre de la firma del Acuerdo de Paz, en donde Rodrigo Londoño, máximo comandante de las FARC-EP señaló **“Si nos preguntan por el modelo de hombre nuevo, por el paradigma del revolucionario y el estadista universal, sin dudarlo responderemos: Fidel, siempre Fidel”. **

######  Reciba toda la información de Contagio Radio en [[su correo]
