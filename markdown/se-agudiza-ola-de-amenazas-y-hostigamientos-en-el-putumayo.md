Title: Se agudiza ola de amenazas y hostigamientos en el Putumayo
Date: 2016-12-21 16:10
Category: DDHH, Nacional
Tags: amenazas contra líderes sociales, amenazas paramilitares, Líderes indígenas amenazados
Slug: se-agudiza-ola-de-amenazas-y-hostigamientos-en-el-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Dic 2016] 

La Red de Derechos Humanos del Putumayo, La Baja Bota Caucana y Cofanía Jardines de Sucumbíos de Ipiales- Nariño denunció que luego del asesinato de Guillermo Veldaño, líder comunitario de la vereda Buenos Aires en el corredor Puerto Vega Teteyé, se han presentado una serie de amenazas en contra de líderes sociales y políticos así como **el intento de allanamiento de la sede de una de las organizaciones sociales del departamento.**

La primera situación denunciada se produjo el 14 de Diciembre contra los concejales del municipio de Puerto Leguízamo **Luis Eduardo Cortés Vargas del Partido Alianza Verde; Fabian Daniel Toro del Polo Democrático y Hugo Enrique García del Movimiento Alternativo Indígena y Social – MAIS**. Los concejales recibieron una llamada en la que se les declara objetivo militar. Le puede interesar: [Asesinan a Guillermo Veldaño en](https://archivo.contagioradio.com/aseinan-a-guillermo-veldano-en-putumayo/) Putumayo.

Según el comunicado el 15 de Diciembre fue amenazado el presidente de la Junta de Acción Comunal de la vereda La Florida en el corredor Puerto Vega – Teteyé y el 19 de Diciembre **fue amenazado de muerte el comunicador comunitario Aldemar Giraldo a través de un mensaje de texto.**

También el 19 de Diciembre se presentó un intento de robo puesto que las chapas externas de las puertas de la Oficina de la organización Pladia – MEROS, sin embargo, los asaltantes no habrían logrado acceder por las medidas de seguridad con que cuenta la sede del movimiento social ubicado en Puerto Asis.

La Red de Derechos Humanos exigió al gobierno nacional en cabeza del Ministerio del Interior realizar todas las investigaciones pertinentes para aclarar estos hechos, dar con los responsables y evitar daños a la integridad de las personas amenazadas, así como **que se brinden las garantías para el libre ejercicio de las labores de los concejales amenazados.**

###### Reciba toda la información de Contagio Radio en [[su correo]
