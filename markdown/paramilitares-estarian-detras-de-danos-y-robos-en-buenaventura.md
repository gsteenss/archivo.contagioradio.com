Title: Paramilitares estarían detrás de daños y robos en Buenaventura
Date: 2017-05-22 18:19
Category: DDHH, Entrevistas
Tags: buenaventura, ESMAD, grupos paramilitares, paramilitares
Slug: paramilitares-estarian-detras-de-danos-y-robos-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/La-14.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [22 May. 2017] 

Luego de 7 días de Paro Cívico los pobladores de Buenaventura han dado a conocer una grave denuncia en la que **aseguran que paramilitares autodenominados “Autodefensas Gaitanistas de Colombia” estarían detrás de los daños** y los robos que se dieron durante este fin de semana en ese municipio.

María Riascos, líderesa en Buenaventura y parte del Paro Cívico, dijo que **durante el paro han podido constatar “una vez más” que el paramilitarismo sigue vivo y sigue presente en Buenaventura** al punto que “nos llegan comunicaciones de que en los hechos que han llamado vandalismo o los desórdenes que se dieron en el día 5 del Paro Cívico, eso estuvo auspiciado por paramilitarismo y la fuerza pública”.

Añade Riascos que el objetivo de dichos hechos fue desvirtuar las acciones de las comunidades y “enlodar nuestro movimiento de paro cívico”. Le puede interesar: ["Buenaventura sigue en paro pese a represión, militarización y toque de queda".](https://archivo.contagioradio.com/buenaventura-sigue-en-paro-a-pesar-de-la-represion-la-militarizacion-y-el-toque-de-queda/)

Riascos hace referencia a los saqueos que se dieron en la cadena vallecaucana de almacenes La 14 y otros locales pequeños, razón por la cual el alcalde Eliécer Arboleda decretó el toque de queda en Buenaventura.

“Nos han contado nuestros líderes que en los diferentes sitios como La 14 **vinieron hombres de civil, abrieron La 14, llamaron a la gente a que entren y esos hombres no sacaron absolutamente nada**, llamaban a la gente que pasaba por ahí y que sacaran lo que estaba allí. Así fue en Pueblo Nuevo, les ordenaban en qué establecimiento entraban y en cual no, dependía de quien les pagaba vacuna” relató la lideresa. Le puede interesar: ["Pescadores de Buenaventura que están en paro cívico son amenazados por paramilitares"](https://archivo.contagioradio.com/pescadores-de-buenaventura-son-amenazados-por-paramilitares/)

Además afirma que estos hechos se presentaron con la connivencia de la Fuerza Pública, dado que éstos fueron avisados y llegaron después “cómo es posible que supuestamente era vandalismo de la población de Buenaventura y **tantos hombres armados, tantos efectivos armados que hay aquí y no llegaron, solo después de 3 horas”.**

### **Gobierno respondió con la Fuerza Pública** 

Según Riascos, las movilizaciones y diversas actividades del Paro Cívico se han estado realizando de forma pacífica “somos hombres y mujeres buenos y buenas, pero **el Gobierno nos respondió con su aparataje armado como siempre lo hace**, solo que esta vez no lo pudo hacer disimulado, fue frentero, nos mandó a golpear”.

### **“Ya no tenemos ni siquiera miedo”** 

Manifiesta Riascos que seguirán “con dignidad exigiendo sus derechos, porque estamos **cansados, ya no aguantamos más, porque nos han quitado todo que nos quitaron hasta el miedo**, ya no tenemos ni siquiera miedo y queremos que estas voces lleguen a toda Colombia y a nivel internacional (…) porque el pueblo no se rinde carajo”.

Además, continuarán exigiendo que **el Gobierno nacional declare la emergencia social, económica y ecológica en Buenaventura** “no desfalleceremos porque esta tierra es nuestra y no nos la vamos a dejar quitar de las multinacionales que confabuladas con el Gobierno nos quieren desterritorilizar” concluyó. Le puede interesar: [¿Dónde estaba la policía de Buenaventura o el ESMAD durante saqueos?](https://archivo.contagioradio.com/confirman-que-arremetida-del-esmad-provoco-disturbios-en-buenaventura/)

<iframe id="audio_18834810" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18834810_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="ssba ssba-wrap">

</div>
