Title: Hallan sin vida a joven lideresa en Peque, Antioquia
Date: 2018-09-10 13:03
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Antioquia, Autodefensas Gaitanistas de Colombia, El Peque, Leidy Correa
Slug: hallan-sin-vida-a-joven-lideresa-en-peque-antioquia
Status: published

###### [Foto:RCN Radio] 

###### [10 Sept 2018] 

Una vez más la violencia golpea al departamento de Antioquia, esta vez con el asesinato de la joven lideresa Leidy Correa Valle, de 25 años, reportada como desaparecida y días después  **hallada sin vida y con signos de tortura, el hecho sucedió** en la vereda de Guayabal, municipio del Peque.

Oscar Zapata, integrante del nodo Antioquia de la Coordinación Colombia Europa Estados Unidos, aseguró que Leidy se desempeñaba como la secretaría de la Junta de Acción Comunal de su vereda, y **había desempeñado un papel importante desde el empoderamiento de la juventud. **(Le puede interesar: "[Colombia pierde un líder innato: Asesinan a Norberto Jaramillo, en Tarazá"](https://archivo.contagioradio.com/asesinan-norberto-jaramillo-taraza/))

Zapata señaló además que la lideresa no había recibido ningún tipo de amenaza, sin embargo, advirtió que en la zona hay una fuerte presencia de las Autodefensas Gaitanistas de Colombia (AGC), que desde el año 2010 ejercen control territorial, situación que fue puesta en conocimiento a las autoridades correspondientes, sin que tomaran las medidas necesarias, quienes por el contrario, negaron el hecho.

### **El Estado se olvidó de Antioquia** 

Con Leidy Correa, son 26 los líderes sociales asesinados en lo corrido de este año en el departamento de Antioquia, lo que, en criterio de Zapata, **hace parte de una estrategia selectiva y sistemática en contra del Movimiento de defensores de derechos humanos en el territorio.**

"Las acciones no son reales de parte del Estado, no hay una voluntad real, y todas las políticas en materia de prevención y protección demuestran con estas cifras que estamos perdiendo la vida de hombres y mujeres muy valiosos para el país y la democracia " afirmó Zapata. (Le puede interesar:["110 líderes han renunciado en su labor en Antioquia por amenazas"](https://archivo.contagioradio.com/110-lideres-sociales-han-renunciado/))

<iframe id="audio_28459077" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28459077_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
