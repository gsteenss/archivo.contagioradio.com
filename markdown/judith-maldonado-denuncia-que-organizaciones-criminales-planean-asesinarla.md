Title: Judith Maldonado denuncia que organizaciones criminales planean asesinarla
Date: 2016-08-26 17:45
Category: DDHH, Nacional
Tags: Judith Maldonado, Norte de Santander
Slug: judith-maldonado-denuncia-que-organizaciones-criminales-planean-asesinarla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Judith-Maldonado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: YouTube ] 

###### [26 Ago 2016] 

La abogada defensora de derechos humanos y ex candidata a la gobernación de Norte de Santander, Judith Maldonado Mojica denuncia que conoció de un testigo directo que las organizaciones criminales de la región preparan en una finca de Puerto Santander **un plan para atentar contra su vida e integridad**.

La ex candidata tiene una trayectoria de 18 años de trabajo en la defensa de los derechos humanos de comunidades indígenas y campesinas, así como de víctimas, en los territorios de Santander y Norte de Santander, y considera que **este plan pone en riesgo el normal desarrollo de sus proyectos** sociales y políticos.

La defensora **ya denunció la situación ante las autoridades competentes** y espera que inicien las investigaciones pertinentes con el fin de que le sean garantizadas las [[medidas urgentes y necesarias para la protección](https://archivo.contagioradio.com/denuncian-agresiones-contra-judit-maldonado-candidata-a-gobernacion-de-norte-de-santander/)] de su vida e integridad física.

La [[V Caravana de Juristas Internacionales](https://archivo.contagioradio.com/continua-riesgo-para-defensores-de-derechos-humanos-y-victimas-en-colombia-caravana-de-juristas/)] durante su visita en Cúcuta ratificó su apoyo a la abogada, quién aseguró que **las Naciones Unidas han considerado su situación como de extrema gravedad**, por lo que espera que el Estado colombiano ponga en marcha todas las medidas necesarias para proteger su vida.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
