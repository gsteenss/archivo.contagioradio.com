Title: Las razones para no llevar caso de Jaime Garzón ante la JEP
Date: 2018-06-01 16:48
Category: DDHH, Nacional
Tags: Jaime Garzon, Jurisdicción Especial de Paz, plazas acevedo
Slug: las-razones-para-no-llevar-caso-de-jaime-garzon-ante-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jaime-garzon-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 Jun 2018] 

El Juzgado Séptimo del Circuito Especializado de Bogotá decidió suspender la audiencia que llevaba contra el **Coronel en retiro Plazas Acevedo**, investigado por haber adelantado actividades de inteligencia contra el periodista asesinado Jaime Garzón, para remitir este caso a la Jurisdicción Especial de paz, pese a que en el 2017** la secretaría ejecutiva de ese organismo había rechazado el caso.**

Esta decisión se dio tras la solicitud de Plazas Acevedo de remitir este proceso a la JEP. Sin embargo la secretaría declaró que este caso no procedía en el tribunal de la JEP debido a que el nombre de Plazas Acevedo no estaba en la lista que presentó el Ministerio de Defensa para postular a la ley 1820 de Amnistías, Indultos y otros tratamientos penales especiales, **ya que el delito por el cual fue condenado, que es el homicidio del comerciante Benjamín Koudari**, no esta relacionado con el conflicto armado en Colombia.

De acuerdo con el abogado Sebastían Escobar, defensor de la familia de Jaime Garzón, en este momento esa decisión del Juzgado Séptimo aún se está analizando, “más allá de que el caso vaya en un futuro o no a la JEP, **tienen que definirse criterios muy claros para establecer qué es una relación con el conflicto** y más concretamente qué es una relación indirecta con el conflicto” afirmó Escobar.

Para el abogado este debate podría abrir un escenario en donde aquellos hechos que no necesariamente están ligados a la coyuntura de la confrontación armada pasen a la JEP. En esa vía la defensa de Jaime Garzón ha manifestado que el asesinato del periodista **se dio como un crimen relacionado con violencia sociopolítica en donde el Estado fue el autor del crimen a través de sus agentes**.

### **El compromiso con la Verdad** 

Escobar aseguró que la opción de someterse a la JEP tiene que ver con asumir un compromiso con la verdad sobre el conflicto armado, sin embargo, **hasta el momento Plazas Acevedo no ha hecho un compromiso real de aceptar responsabilidades y mucho menos de decir la verdad**. (Le puede interesar: ["A 17 años del asesinato de Jaime Garzón se reconoce que fue un crimen de Estado"](https://archivo.contagioradio.com/a-17-anos-del-asesinato-de-jaime-garzon-se-reconoce-que-fue-un-crimen-de-estado/))

“Ven en el escenario de la Jurisdicción Especial para la Paz no un escenario de contar la verdad y aceptar las responsabilidades, sino de revivir unos juicios que como este ya estaba en marcha y a punto de terminar” afirmó Escobar y agregó que finalmente lo que termina sucediendo es una dilatación del proceso que terminará afectando la continuidad de un juicio y **un caso que lleva más de 19 años por esclarecer respuestas sobre el asesinato de Jaime Garzón**.

<iframe id="audio_26308022" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26308022_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
