Title: El sistema penitenciario de Colombia: una violación a DD.HH
Date: 2020-08-07 16:52
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: carceles de colombia, Expreso Libertad, Sistema Penitenciario, violación a derechos humanos
Slug: el-sistema-penitenciario-de-colombia-una-violacion-a-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/17-02-09-Colombia-prison-la-modelo.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/carceles_4.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### La comisión de verificación de seguimiento a la sentencia T- 388 con la que se declara el estado cosas inconstitucionales en el sistema penitenciario colombiano, reveló su más reciente informe sobre violaciones a derechos humanos en las cárceles.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este **Expreso Libertad**, Adriana Benjumea, directora de la[Corporación Humanas](https://www.humanas.org.co/alfa/index.php), y Marcela Olarte integrante del [Centro de Investigación en Política Criminal de la Universidad Externado](https://www.uexternado.edu.co/centro-de-investigacion-en-politica-criminal/), hablaron del papel que cumple esta comisión y la situación de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con ese informe, en las cárceles siguen presentandose altas tazas de hacinamiento, la negación a derechos como la salud y la alimentación. Asimismo, se presentan tratos crueles o torturas contra las y los reclusos y abusos de poder por parte de la Fuerza Púbica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo ello, de acuerdo con las dos mujeres, son situaciones que podrían transformarse si el presidente Iván Duque tomará acciones urgentes. Hecho que no se evidencia luego del decreto 546 de excarcelación, hasta la fecha, solo haya permitido la salida del 10% de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55665687" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55665687_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

[Vea mas programas de Expreso Libertad](https://archivo.contagioradio.com/categoria/programas/expreso-libertad/)

<!-- /wp:paragraph -->
