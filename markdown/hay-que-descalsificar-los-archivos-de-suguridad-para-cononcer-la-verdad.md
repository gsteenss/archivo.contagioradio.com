Title: Hay que desclasificar los archivos de seguridad para conocer la verdad
Date: 2017-05-17 12:27
Category: Nacional, Paz
Tags: Desclasificación de archivos, JEP, organizaciones de DDHH, víctimas de estado
Slug: hay-que-descalsificar-los-archivos-de-suguridad-para-cononcer-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-05-at-3.43.02-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 mayo 2017] 

La solicitud de desclasificación de archivos de seguridad del Estado, es la insistencia de diversas organizaciones de Derechos Humanos y de víctimas quienes exigen que en el marco de la Jurisdicción Especial para la Paz, **se garanticen los procesos de verdad, justicia restaurativa, reparación, no repetición y se garantice el derecho a la memoria colectiva.**

Durante una de las audiencias sobre relgamentación del SIVJRNR, las organizaciones solicitaron a la Corte Constitucional que interprete de manera más amplia el concepto de **mantener la reserva de archivos clasificados** de la Comisión de la Verdad con miras a garantizar los derechos de las víctimas a la verdad y se salvaguarde la memoria. Le puede interesar: ["Víctimas de Estado piden que se desclasifiquen archivos militares"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/)

Según Jomari Ortegón, abogada del Colectivo de Abogados José Alvear Restrepo, **los jueces están en la facultad de ordenar la desclasificación de los archivos** para que la Comisión para el Esclarecimiento de la Verdad pueda tener acceso a estos documentos, sin embargo también es necesario que se levante la orden a la Comisión de la Verdad que restringiría el conocimiento de dichos archivos.

"Lo que necesitamos es que la normativa que le exige a la Comisión mantener la reserva de los documentos **sea retirada porque se trata de información de violación a los derechos humanos".** Ortegón además afirma que "hay una esperanza de que la Corte Constitucional se pronuncie sobre temas de acceso a la información porque es importante para el esclarecimiento de los hechos".

### **Desclasificar los archivo es la materia prima para la transición hacia la paz** 

El centro de estudios jurídicos y sociales, Dejustica, ha hecho énfasis en que la **información es la materia prima de la transición hacia la paz.** De allí la necesidad de que  se realice la desclasificación de los archivos de seguridad del Estado.

En el libro "Acceso a las archivos de inteligencia y contra inteligencia en el marco del posacuerdo" Dejusticia establece que "sin la información de la Fiscalía, de la Procuraduría, de la Fuerza Pública y de la guerrilla los jueces no pueden juzgar a las partes y las víctimas **no pueden ser reparadas, ni los hechos que se le atribuyen a víctimas y victimarios podrán ser esclarecidos en términos de reputaciones y motivaciones".** Le pude interesar:[¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/)

Para Dejusticia, la reserva de los archivos de seguridad no solo es perjudicial para los asuntos jurídicos. "De no tener la información suficiente, **la sociedad no puede conocer las violaciones a los derechos humanos,** los periodistas no pueden informar, los colegios no pueden educar y los museos no pueden aportar a la reconstrucción de la memoria y la verdad."

### **Integrantes de la Fuerza Pública deben cumplir requisitos para acceder a la JEP** 

Según Jomari Ortegón, el Colectivo de Abogados José Alvear Restrepo y las víctimas de crimenes de Estado, han hecho una petición a la JEP de que **se cumplan los compromisos de la fuerza publica** y los miembros que están siendo beneficiarios de las ordenes de libertad. "Los jueces deben decir que apenas entre a funcionar la JEP, ellos se deben presentar para que se respeten los derechos de las víctimas a conocer la verdad".

Los integrantes del MOVICE ya habían dicho que la Comisión de Búsqueda y Esclarecimiento de la Verdad necesita la desclasificación de los archivos de inteligencia y contrainteligencia porque **“allí está gran parte de la verdad y los insumos que permitirían establecer como es qué ha funcionado la criminalidad de Estado”.**

<iframe id="audio_18753950" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18753950_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
