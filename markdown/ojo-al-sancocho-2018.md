Title: "Ojo" convocatorias abiertas para el 11° Ojo al Sancocho
Date: 2018-08-06 09:40
Author: AdminContagio
Category: 24 Cuadros
Tags: cine comunitario, Festival, Ojo al sancocho
Slug: ojo-al-sancocho-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ojoalsancocho-portada_copy_copy_copy_copy_copy-e1533565917665.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ojo al sancocho 

###### 06 Agos 2018 

Teniendo como temática general **"El derecho a soñar"** el Festival Internacional de Cine y Video Alternativo y Comunitario **Ojo al Sancocho**, tiene abiertas sus convocatorias para su edición número 11, que se realizará **del 6 al 13 de octubre** próximos en la localidad bogotana Ciudad Bolívar.

 

La convocatoria, que estará abierta **hasta el próximo 13 de agosto**, busca recibir producciones nacionales e internacionales, en los géneros **documental, ficción, producción local, animación, video clip, video infantil, cine accesible y video arte** entre otras.

[![ojo el sancocho](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/7180b32b-6d53-4332-8d0a-9c6c77ccd3fd-720x266.jpg){.alignnone .size-medium .wp-image-55399 width="720" height="266"}](Ojo%20)

Algunas de las temáticas transversales de la presente edición son dignidad, convivencia pacifica, innovación social, comunidad, participación, democracia, cambio, climático, soberanía alimentaria, LGTBI, Diversidad y género, comunidades afros, infancia y adolescencia, nuevas narrativas y cine comunitario, entre otras.

Para postular sus propuestas, las personas interesadas pueden enviar vía correo electrónico el link de su producción a **festival@ojoalsancocho.org** en alguno de los formatos .MOV, .AVI, .MP4, por plataformas online: Youtube/Vimeo/Dropbox/wetransfer/Servidores FTP. También es posible enviar vía correo certificado dos (2) copias en DVD y/o BLU RAY.  solicitando la dirección de correspondencia al mismo correo electrónico.

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
