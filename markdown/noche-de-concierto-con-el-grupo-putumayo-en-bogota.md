Title: Noche de Concierto con el Grupo Putumayo en Bogotá
Date: 2015-08-18 13:17
Category: Cultura, eventos
Tags: Fundación Gilberto Alzate Avendaño, Grupo Putumayo, Que quieres hacer hoy
Slug: noche-de-concierto-con-el-grupo-putumayo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/grupoputumayo_9992.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: laplataforma.net 

###### [18, Ago, 2015] 

[Hoy martes, 18 de agosto se dará lugar en la Fundación Gilberto Alzate Avendaño el concierto del Grupo Putumayo, agrupación de música latinoamericana fundada en 1985 con el propósito de eternizar melodías de su entorno a través del sonido mágico producido por instrumentos autóctonos.]

[Su música recrea melodías de la selva, aires colombianos y música andina, muestra clara de diversas influencias musicales que no se apartan del compromiso social con nuestro país y de la realidad Latinoamericana.]

<iframe src="https://www.youtube.com/embed/SWXVNBU2gPs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[Hora: 7:00pm-8:30pm  
Dirección: Calle 10 \# 3-16  
Aporte: \$5.000]

[![mas eventos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/mas-eventos.jpg){.aligncenter .wp-image-12328 .size-full width="700" height="200"}](https://www.facebook.com/yoquierohacer?fref=ts)
