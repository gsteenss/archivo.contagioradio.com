Title: "El fuero militar no es la salida frente a reclamos de las víctimas" Defensores de DH ante CIDH
Date: 2015-03-19 20:23
Author: CtgAdm
Category: DDHH, Nacional
Tags: CIDH, Comisión Interamericana de Derechos Humanos, conversaciones de paz, Conversaciones de paz en Colombia, doctrina militar, Ejecuciones Extrajudiciales, FFMM
Slug: el-fuero-militar-no-es-la-salida-frente-a-reclamos-de-las-victimas-defensores-de-dh-ante-cidh
Status: published

###### Foto: CIDH 

<iframe src="http://www.ivoox.com/player_ek_4237873_2_1.html?data=lZegmZ2bd46ZmKiakp6Jd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptGYyNrJttCfzs7Zy9nFtozi0JDS1ZDQpYznwtHWxsaPqtPZz9nSjcaPsNCf0trSjdfJp83VjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Danilo Rueda CIJP] 

La **Mesa de Ejecuciones Extrajudiciales**, que reúne varias organizaciones de DDHH en Colombia, plantea ante la Comisión Interamericana de Derechos Humanos, CIDH, que el presidente Juan Manuel Santos debe tomar decisiones de cara a la ampliación del **Fuero Penal Militar** que cursa en 7 proyectos en el congreso. También solicita a la CIDH que intervenga para que las investigaciones por estos hechos **identifiquen a los máximos responsables**, ya que sólo 4 Coroneles son investigados en más de 5000 casos.

Según las organizaciones de Derechos Humanos hay material probatorio suficiente para demostrar que las ejecuciones extrajudiciales **son una práctica orientada por los máximos responsables de las FFMM**, varios generales y no solamente los 4 coroneles que están siendo investigados.

En el escenario de las conversaciones de paz, las organizaciones de DDHH solicitan que se abra la posibilidad de **discutir la doctrina militar,** dado que según ellos, esa doctrina **concibe a la población civil como enemigos internos** y por ello hay responsabilidad de esa doctrina en la comisión de las ejecuciones extrajudiciales, porque justifica la acción contra la población civil.

Danilo Rueda, defensor de DDHH de la **Comisión de Justicia y Paz**, afirma que el **informe intermedio de la CPI** señala que la práctica de las ejecuciones extrajudiciales es sistemática. De otro lado y frente a la respuesta del gobierno nacional en que se hizo referencia al más reciente **informe de la ONU** en que se indica una disminución de las ejecuciones extrajudiciales, Danilo Rueda afirma que “disminuir no significa que no estén ocurriendo” y que además es evidente que existen reductos que siguen realizando esa práctica.

Según los defensores de DDHH en lo corrido del gobierno de Juan Manuel Santos han ocurrido **230 ejecuciones extrajudiciales**, y aunque es evidente la reducción no es suficiente lo que se hace para evitar que sigan sucediendo.

\[embed\]https://www.youtube.com/watch?v=-IZdzl6fdXM\[/embed\]

##### **Audiencia: Legislación sobre Justicia Penal Militar** 

Además, la ampliación del Fuero Penal Militar facilita que los tribunales internacionales investiguen y sanciones a los militares, puesto que con lo que está en trámite no se garantiza la satisfacción de las víctimas en sus derechos a la verdad, la justicia y la reparación integral, señala Rueda. Para el defensor de DDHH es necesario que en el marco de las conversaciones de paz se establezcan mecanismo para responder a las víctimas y no para facilitar la impunidad, **“El fuero militar no es la salida frente a lo que reclaman las víctimas”**
