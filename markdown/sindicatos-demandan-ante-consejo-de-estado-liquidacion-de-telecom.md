Title: Sindicatos demandan ante Consejo de Estado liquidación de Telecom
Date: 2016-03-22 17:37
Category: Economía, Judicial, Nacional
Tags: escuela nacional sindical, telecom, venta de telecom
Slug: sindicatos-demandan-ante-consejo-de-estado-liquidacion-de-telecom
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Telecom.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semanario Voz ] 

<iframe src="http://co.ivoox.com/es/player_ek_10900336_2_1.html?data=kpWmkpWXd5ehhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbTdz8nWxcbQrdTowtiY1dTQrcTd1cbbjdjJpYzmxsbPy8rWuNCf0dfcxcrXs4zYxpDZy9bZrcWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Julio Díaz, Escuela Nacional Sindical] 

###### [22 Mar 2016 ] 

La 'Unión Sindical de Trabajadores de las Comunicaciones' junto con la 'Unión Nacional de Pensionados de las Comunicaciones' demandaron en 2012 ante el Consejo de Estado el proceso de liquidación de 'Telecom', pues **no contó con una valoración de los activos ni un inventario que garantizara la venta de la compañía en su valor real**. En diciembre del año pasado el Consejo falló contra los demandantes, quienes solicitan sea re abierto el proceso, teniendo en cuenta las irregularidades que lo rodearon y que implicaron la vulneración de derechos laborales y el detrimento patrimonial.

Según afirma Carlos Julio Díaz, director de la 'Escuela Nacional Sindical' desde 2003 el Estado implementó **"gestiones administrativas irresponsables" que llevaron a 'Telecom' a dificultades financieras que justificaron su "liquidación arbitraria"** en el año 2006.  Una acción que tenía como propósito además de acabar con la organización sindical, beneficiar al capital privado con la transferencia del 70% de las acciones a empresas privadas como 'Movistar'.

La no valoración de los activos, ni la realización de un inventario impidió que el dinero que el Estado recibió por la venta de 'Telecom' pagara las pensiones de los trabajadores, pues lo que se hizo fue pagar las deudas que había adquirido la compañía, como asevera Díaz quien agrega que los **derechos laborales que tienen rasgo de constitucionalidad deben prevalecer sobre el interés privado** que fue el mayor beneficiario con esta enajenación.

Las deudas adquiridas por esta compañía tuvieron que ver con el pago de las sanciones producto de las demandas interpuestas por multinacionales como 'Nortel', 'Siemens', 'Alcatel', 'Ericsson', 'Nec' y 'Itochu' que en los 90 suscribieron con 'Telecom' contratos bajo la modalidad 'Join Venture' o de riesgo compartido para la instalación de líneas telefónicas en lugares a los que la empresa no había podido llegar. Contratos que presentaron serias irregularidades desde sus inicios y que l**levaron a que 'Telecom' asumiera la mayor parte de las pérdidas**.

Las anomalías en estos contratos y las pérdidas que representaron para la compañía llevaron a que la Junta Directiva determinará que 'Telecom' debía fortalecer su posición jurídica ante los contratistas asociados y buscar opciones jurídicas adicionales que le permitieran defender sus intereses ante los tribunales, de acuerdo con el Acta 1782 del 28 de febrero de 2003; sin embargo la abogada Marcela Monroy Parra quien habría sido seleccionada para tal fin, terminó suscribiendo un contrato de \$20 mil millones para asegurar que 'Telecom' **invirtiera el dinero de su liquidación en el pago de US \$1.800 millones a las multinacionales y no de las pensiones y derechos laborales de sus trabajadores**.

De acuerdo con Díaz, varios de los reintegros que quedaron pendientes tras la venta de 'Telecom' hasta la fecha no se han efectuado, por lo que aún continúan las demandas ante los incumplimientos, acciones jurídicas emprendidas por los sindicatos para defender el patrimonio público, **exigiendo que cese la impunidad en los casos de corrupción que se presentan en nuestro país** como la venta subvalorada de empresas estatales para el beneficio del capital extranjero.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
