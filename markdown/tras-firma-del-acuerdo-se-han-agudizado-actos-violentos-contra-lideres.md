Title: Tras firma del Acuerdo se han agudizado actos violentos contra líderes sociales
Date: 2016-12-02 16:02
Category: DDHH, Paz
Tags: Amenazas, Asesinatos, marcha patriotica, paz, proceso de paz
Slug: tras-firma-del-acuerdo-se-han-agudizado-actos-violentos-contra-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/colombia-7sep16.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: International Crisis Group] 

###### [2 Dic. 2016 ] 

Más de 12 organizaciones sociales han lanzado un **llamado de urgencia a propósito de los recientes actos violentos en los que se han visto comprometidas la vida de más de 28 defensores y defensoras de DH y líderes sociales de base**, todas reconocidas por su labor en pro de la defensa del territorio, el ambiente y en general personas que han propendido por la paz en sus comunidades y en el país.

Según estas organizaciones, es importante reconocer que el cese al fuego ha tenido unos efectos positivos en la disminución de la confrontación armada, sin embargo **“los ataques contra defensores y defensoras de Derechos Humanos y líderes locales demuestran que esta violencia no está asociada únicamente al conflicto armado interno, sino a otros factores de violencia estructural y sociopolítica**” aseguran en el comunicado

Además dicen que “tras la firma del acuerdo, se pueden agudizar - la violencia- si no se toman las medidas necesarias para protegerles”.

En la comunicación, las organizaciones manifiestan que preocupa que **la mayoría de estos ataques estén dirigidos a integrantes del movimiento Marcha Patriótica y que adicionalmente se ponga precio a la vida de líderes y lideresas sociales y defensores de Derechos Humanos. **Le puede interesar: [“No les vamos a dar el gusto de desfallecer” Marcha Patriótica](https://archivo.contagioradio.com/cuanto-mas-podra-resistir-marcha-patriotica/)

**La cifra es preocupante, durante el año 2016 se han registrado más de 377 agresiones contra defensores y defensoras, de las cuales 79 de ellas son homicidios.**

Por lo anterior, dichas organizaciones han realizado una serie de solicitudes al Gobierno Nacional dentro de las cuales se encuentra reactivar la Mesa Nacional de Garantías, junto a sus sub mesas de investigación y protección, Invitar oficialmente al Relator Especial de las Naciones Unidas sobre la situación de las y los defensores de Derechos Humanos, Michel Forst.

Adicionalmente, piden que se implemente de manera prioritaria e inmediata el punto 2 y 3.4 del Acuerdo de Paz e Informar desde el Ministerio de Interior los resultados y conclusiones de la Comisión de Alto Nivel de Investigaciones. Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

Por último **han reiterado su apoyo su respaldo al proceso de paz firmado entre el Gobierno y las Farc, no sin antes asegurar que es necesario avanzar con prontitud en las investigaciones por agresiones contra defensores de Derechos humanos, que ya asciende a un 99.96% de impunidad.**

Estas son las organizaciones firmantes:

1.  Pensamiento y Acción Social - PAS
2.  Protection International - PI
3.  Peace Watch Switzerland - PWS
4.  Programa Somos Defensores
5.  Movimiento Sueco por la Reconciliación – SweFOR
6.  Colectivo ANSUR
7.  Fondo de Acción Urgente para América Latina y el Caribe – FAU-AL
8.  Diakonia
9.  Fondo Defender la Tierra - Centro de Investigación y Educación Popular / Programa por la Paz – CINEP/PPP
10. El Movimiento de Víctimas de Crímenes de Estado – MOVICE

Fondo para ayudas de emergencia y fortalecimiento organizacional en protección y autoprotección – FFP.

Integrado por:

-   Instituto Latinoamericano para una Sociedad y un Derecho Alternativos – ILSA
-   Corporación AVRE
-   Asociación Cristiana Menonita para Justicia, Paz y Acción Noviolenta - Justapaz
-   Humanidad Vigente Corporación Jurídica

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
