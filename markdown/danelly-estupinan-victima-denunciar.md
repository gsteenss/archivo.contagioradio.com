Title: Danelly Estupiñán, lideresa que pasó de víctima a víctimaria por denunciar
Date: 2019-07-02 17:43
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: amenazas contra líderes sociales, buenaventura, Paro Cívico de Buenaventura, Seguimientos
Slug: danelly-estupinan-victima-denunciar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Danelly-Estupiñan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PVasco\_EuskoL  
] 

**Danelly Estupiñán, una de las promotoras del paro cívico de Buenaventura (Valle del Cauca)** y líder afro denunció que además de ser víctima de seguimientos por parte de personas extrañas, se la está intimidando judicialmente por sus procesos de denuncia en el Puerto. Pese a que Estupiñan ha seguido el proceso de denuncia por estos hechos con la Fiscalía; en el ente acusador ella pasó de víctima a victimaria. (Le puede interesar: ["Amenazan a dos líderes sociales del paro cívico de Buenaventura"](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/))

### **¿Quién es Danelly Estupiñan?**

Danelly tiene una trayectoria de 23 años como activista por los derechos de las comunidades afro, y hace parte del **Proceso de Comunidades Negras (PCN),** organización que trabaja "por la defensa de los derechos para comunidades negras". En los últimos 10 años, Estupiñan trabajó en diferentes investigaciones sobre el impacto negativo para las comunidades que tienen "las dinámicas de enclave que se desarrollan en Buenaventura", **negando el acceso a la tierra de las personas.**

Fue así como en 2014 participó en una investigación que se llamó **"Buenaventura, puerto sin comunidad",** desarrollada por el Centro Nacional de Memoria Histórica y en el mismo año, participó de otro estudio llamado **"Más puerto, menos comunidad"** en el que se tomaban los casos de dos territorios de la Comuna 5 de Buenaventura, "que estan sufriendo los impactos por la construcción de un puerto" cerca a su comunidad.

Luego de ser parte de estas investigaciones, la líder fue amenazasada a través de un mensaje de texto y decide desplazarse del Puerto por cerca de dos meses; en ese momento, la Unidad Nacional de Protección (UNP) le asingó un esquema de protección y ella regresó a Buenaventura y continuó su trabajo. Posteriormente, Estupiñán y otros líderes, como [Temístocles Machado](https://archivo.contagioradio.com/verdad-temistocles-machado-quedo-medias/) decidieron unirse en el paro cívico que se tomó la Ciudad en 2017.

### **En 2018 llegaron las amenazas y el exilio**

Tras la realización del Paro Cívico, a principios del año pasado, varios de los líderes que promovieron esta protesta comenzaron a denunciar seguimientos en sus rutinas por parte de hombres "con porte militar" que tomaban fotos y videos de lo que hacían. En julio de 2018, **por decisón del PCN, Danelly Estupiñan se vió obligada a salir del país para mitigar el riesgo contra su vida pero retornó a Colombia en noviembre del mismo año.**

En enero de este año regresaron también los seguimientos, y con ayuda de las personas que integran su esquema de protección recolectó fotos y videos de los hombres que la seguían para instaurar una denuncia ante la Fiscalía en febrero. Luego, en mayo se amplió la denuncia, adjuntando más pruebas documentales (fotos y videos) sobre los seguimientos. (Le puede interesar: ["Si el Gobierno no cumple retomaremos el paro cívico: habitantes de Buenaventura"](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/))

### **De víctima a victimaria**

No obstante, la Activista se llevó una sorpresa cuando una fiscal de Buenaventura se comunicó con ella en junio para informarle que había dos denuncias por injuría y calumnia en su contra por parte de agentes del Cuerpo Técnico de Investigación (CTI) de la Fiscalía. En la denuncia, los agentes la acusan de estos delitos porque supuestamente ella ha dicho que la están siguiendo, y que ellos son responsables de cualquier ataque en su contra; no obstante, Estupiñan aseguró que esto no había pasado porque ella no conoce a las personas que la están siguiendo, y tampoco se ha referido en esos términos sobre los seguimientos de los que es víctima.

Sobre los seguimientos, Danelly Estupiñan cree que pueden estar a cargo de algún cuerpo de seguridad del Estado, y por la denuncia en su contra, todo indica que sí estarían vinculados con el CTI. La otra hipótesis posible es que los seguimientos estén a cargo de hombres que hacen parte de cuerpos de seguridad privada, que disponen a estas personas para seguir las rutinas de la líder siguiendo la premisa de : **"Dime que denuncias y te diré por qué te amenazan"**.

### **Ni la Fiscalía avanza en las investigaciones, ni la UNP en protección** 

Danelly Estupiñan concluyó que la situación en Buenaventura es difícil para los líderes porque la Fiscalía no ha avanzado sobre las investigaciones respecto a quienes los amenazan; situación que se presenta en en su caso y el de otras dos líderes de PCN en el Puerto. Mientras no se tienen resultados y sigue la zozobra sobre quiénes los siguen y como mitigar estas situaciones de riesgo, la Líder también pidió protección para su hija, pues es madre soltera y está preocupada por las amenazas en su contra.

A pesar del riesgo, y que la UNP podría extender la protección a familiares de Estupiñan, el organismo se ha negado a que esto ocurra; razón por la que la Defensora pidió **que se avance en respuestas integrales para proteger la vida de los líderes**, como en las investigaciones para determinar los autores materiales e intelectuales de las amenazas. [(Lea también: Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados)](https://archivo.contagioradio.com/asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
