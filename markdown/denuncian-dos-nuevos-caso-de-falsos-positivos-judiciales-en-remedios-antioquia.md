Title: Denuncian dos nuevos casos de falsos positivos judiciales en Antioquia
Date: 2017-09-22 17:13
Category: DDHH, Nacional
Tags: ejercito, ELN, Falsos Positivos Judiciales
Slug: denuncian-dos-nuevos-caso-de-falsos-positivos-judiciales-en-remedios-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Militaress-Boyaca-e1524691374383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### [22 Sep 2017] 

En la madrugada de este viernes, dos campesinos habitantes del municipio de Remedios en Antioquia fueron capturados por unidades del Ejército Nacional, acusándolos de ser integrantes de la guerrilla de las ELN.

Según la organización Cahucopana, desde la noche del jueves, la comunidad se encontraba atemorizada debido al **desembarco de fuerza pública en Altos de Manilas**, jurisdicción de Remedios. "La comunidad teme por las represalias que puedan ocurrir ante la falta de garantías para la población en medio del proceso de diálogo con el ELN", dice la denuncia.

El relato de Cahucopana señala que la captura se dio sobre las 5 de la madrugada cuando integrantes del Ejército Nacional pertenecientes al Batallón Energético y Vial Nº 8 de Segovia, soldados del Batallón de Operaciones Terrestres 107 y miembros de la SIJIN de Barrancabermeja, llegaron a la vereda Cancha de Manila **y allanaron el hogar de Miguel Córdoba Bejarano y su esposa Lina Irene Tenorio,** a quienes señalaron de pertenecer a la insurgencia del ELN, y por ello los trasladaron en helicóptero hacia el municipio de Segovia.

### **¿Quienes son los capturados?** 

Los detenidos son dos conocidos habitantes de la zona.  La organización de DDHH, explica que se trata de **dos campesinos inscritos a la Junta de Acción Comunal de la vereda. Miguel Córdoba es minero y comerciante y Lina es ama de casa. **

"La población señala que las camionetas que llegaron al lugar y en las cuales subieron a los dos detenidos, tenían las placas tapadas con plástico, pero aseguran que se trata de la dotación de camionetas donde se movilizan el componente de fuerza pública del Mecanismo Tripartito ubicado en la vereda Carrizal", dice el comunicado.

Integrantes de Cahucopana intentaron dialogar con los militares para impedir que se llevaran a la pareja, pero según indican, la fuerza pública no permitió revisar las ordenes de captura y el allanamiento. Asimismo, **en el momento de la captura tomaron fotografías a la cédula de Carlos Palacios, una de las personas que buscaba defender a los campesinos.**

Tras este hecho, la comunidad y la Corporación Cahucopana asegura que no es la primera vez que suceden este tipo de hechos desde la instalación de la zona veredal. Para ellos son acciones que "no contribuyen a la construcción de confianza entre comunidad y fuerza pública". De igual forma exigen garantías para que se respete el debido proceso de los detenidos pues estos "NO hacen parte del ELN y cuentan con el respaldo de las comunidades y las organizaciones quienes conocen su historia de vida en el territorio", concluyen.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
