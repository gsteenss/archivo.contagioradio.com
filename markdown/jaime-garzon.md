Title: Sin Olvido - Jaime Garzón
Date: 2015-01-06 15:19
Author: CtgAdm
Category: Sin Olvido
Slug: jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Jaime-Garzón-e1457652708929.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/jaime-garzon-y-claudia-morales.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jaime-garzon-.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jaime-garzon-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/jaime-garzon.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Jaime-Garzón.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Aljeandro-Angel.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cortesía de la familia 

[Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsfeI4Wi.mp3)

Con su risa de dientes desordenados, un pensamiento que le daba vuelco a los análisis políticos, que aun hoy siguen vigentes en la historia de Colombia. Constructor de paz y de una cultura política con el humor, entre la indignación y su fuerza por aportar a este país gente con opinión. Jaime se abrió camino entre cientos de colombianos que lo quisieron, respetaron y siguieron, incluso después de cegada su vida.

Ese 13 de Agosto a muy tempranas horas, cuando iba camino a su trabajo en la radio, Jaime fue interceptado en el barrio Quinta Paredes, en la ciudad de Bogotá; dos sicarios que se transportaban en una motocicleta de alto cilindraje, con placas ocultas, le dispararon en 5 ocasiones causándole la muerte. Sí, ese día despedimos a nuestro querido Jaime Hernando Garzón Forero, el estudiante abogado que fue más allá de las leyes, el que desafió la amargura y el dolor con su apropiada capacidad de reírse de sí mismo y de todas las formas de poder.
