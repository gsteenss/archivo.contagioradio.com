Title: Estos son los cargos contra Antonio Ledezma en Venezuela
Date: 2015-02-20 17:26
Author: CtgAdm
Category: Otra Mirada, Política
Tags: Antonio Ledezma, Democracia en Venezuela, Golpe de Estado en Venezuela, Nicolas Maduro, Venezuela
Slug: detienen-en-venezuela-a-antonio-ledezma-acusado-de-participar-en-plan-de-golpe-de-estado
Status: published

###### Foto: loreportesdelichi.com 

<iframe src="http://www.ivoox.com/player_ek_4112403_2_1.html?data=lZaelJmUd46ZmKiak5qJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKjO1NHTt4zGwtKSpZiJhaXmxt%2BYxsrQb6Lgw8aYxsqPsNDnjLXix8fQs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Ramírez, Alba de los Pueblos] 

La tarde del pasado 19 de Febrero fue detenido el alcalde de **Caracas, Venezuela, Antonio Ledezma**, político de oposición que es acusado por **la fiscalía de participar en los planes de Golpe de Estado**, denunciados por el gobierno de Nicolás Maduro y en el que también participaron María Corina Machado y Leopoldo López, detenido hace un año y en proceso de juicio.

Antonio Ledezma también es señalado como responsable de las muertes de civiles a manos de la policía en el llamado **“caracazo” el 27 de Febrero de 1989**, en el que fueron asesinadas cerca de 300 personas y otras 2000 fueron desaparecidas forzadamente. Durante la represión, Antonio Ledezma, era el jefe de la policía que salió a las calles a reprimir a los manifestantes. Además es señalado de ser la **herencia política del expresidente de derecha Carlos Andrés Pérez.**

Según Carlos Ramírez, la situación en Caracas, tras la captura de Ledezma,  es de normalidad pero se podrían esperar nuevos focos de violencia como los de 2014, conocidos como **“guarimbas” que provocaron la muerte de 43 personas y por las cuales está detenido Leopoldo López.**

Sin embargo la situación política en Venezuela es grave en la medida en que sistemas de medios internacionales, asociados a intereses empresariales, están afirmando que tanto López como Ledezma son prisioneros políticos, señala Ramírez.

El analista resalta que hasta hace unos meses la victoria de la oposición en las urnas podría ser clara, sin embargo las medidas económicas aplicadas por el gobierno y la presión a los empresarios para que se produzca de acuerdo a la exigencia de abastecimiento, así como las capturas a militares y políticos vinculados con los planes de des estabilización han dado un nuevo aire al gobierno y han provocado una reacción positiva por parte de los venezolanos.
