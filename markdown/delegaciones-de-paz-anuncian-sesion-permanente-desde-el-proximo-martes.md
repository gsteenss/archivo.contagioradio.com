Title: Delegaciones de paz anuncian sesión permanente desde el próximo martes
Date: 2016-05-25 15:31
Category: Nacional, Paz
Tags: conversaciones de paz, delegaciones de paz, dialogos de paz
Slug: delegaciones-de-paz-anuncian-sesion-permanente-desde-el-proximo-martes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Delegaciones-paz_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tu Semanario ] 

###### [25 Mayo 2016 ]

De acuerdo con el más reciente comunicado emitido por la mesa de conversaciones de La Habana, las delegaciones acordaron declararse **en sesión permanente con el fin de llegar lo antes posible a acuerdos en el punto de 'fin del conflicto'**. En los próximos cuatro días las FARC-EP y el Gobierno trabajaran de forma separada, analizando las propuestas que ya han intercambiado en relación con el cese al fuego, la dejación de armas, las garantías de seguridad y lucha contra organizaciones criminales, así como las características y el funcionamiento del mecanismo de monitoreo y verificación de la ONU.

Las delegaciones insisten en la necesidad de que los grupos sucesores del paramilitarismo y sus redes de apoyo, sean perseguidos y desmontados, teniendo en cuenta que son los máximos responsables de los homicidios contra los defensores de Derechos Humanos, e integrantes de movimientos sociales y políticos; un **hecho que amenaza la implementación de los acuerdos y la construcción de la paz**.

En el comunicado también destacan los importantes [[acuerdos alcanzados en los últimos días](https://archivo.contagioradio.com/gobierno-y-farc-ep-blindan-acuerdos-de-paz/)], en relación con la seguridad jurídica del Acuerdo Final y la [[salida de menores de edad de los campamentos](https://archivo.contagioradio.com/se-acabo-la-guerra-para-menores-de-las-farc-ep/)] de las FARC-EP, como "acuerdos vitales para la seguridad de todas las partes involucradas en el conflicto y medidas de confianza para la sociedad". Anuncian que **el próximo martes reanudarán las discusiones** con el propósito de "lograr un nuevo y decisivo paso hacia el fin de la guerra y alcanzar la paz que tanto anhelamos".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU⁠⁠⁠⁠)] ] 
