Title: ESMAD desaloja violentamente a 38 familias en Moravia, Medellín
Date: 2015-11-18 11:39
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, Desalojo en Medellín, ESMAD desaloja violentamente a 38 familias en Moravia, Medellin, Moravia
Slug: esmad-desaloja-violentamente-a-38-familias-en-moravia-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/VID-20151118-WA0007_thumb0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Erika Prieto 

<iframe src="http://www.ivoox.com/player_ek_9430081_2_1.html?data=mpmgkpWcdY6ZmKiakpmJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmYxsrXpc3jy8aY2M7TsMbi1cbax9PYqYzVjJiljcvFscrgysbgjcrSb67j08bjy8aJdqSfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Johan Giraldo, habitante del sector] 

###### 18 Nov 2015

[Sobre las 2 de la madrugada el **ESMAD inició el desalojó violento contra 38 familias** del barrio Moravia, sector el Oasis en la ciudad de Medellín, victimas en su mayoría de un incendio registrado en 2007 en la capital antioqueña, así como del conflicto armado, a quienes el **Gobierno nacional les ha incumplido las promesas de vivienda digna** y hoy **ataca con gases, bombas y aturdidoras,** según denuncia la comunidad.]

[De acuerdo con Johan Giraldo, habitante del sector, en este desalojo han sido **agredidas personas en situación de discapacidad, adultos mayores, niños y niñas**, por cuenta de las actuaciones del ESMAD. “Cuando se estaba realizando el procedimiento de desalojo nosotros estábamos ayudando con la interlocución, **nos sacaron del terreno y tan pronto nos sacaron, rompieron a la gente con bombas, aturdidoras y gases lacrimógenos**… por el momento tenemos un niño que sufre parálisis cerebral herido, una joven que está muy afectada por el tema de los gases lacrimógenos y una persona detenida”, agrega Giraldo.]  
\[KGVID height="352"\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/VID-20151118-WA0007.mp4\[/KGVID\]

[Según Johan, el desalojo se realiza con la intención de adelantar megaproyectos de infraestructura por parte de la alcaldía de Medellín, afectando no sólo a las **comunidades víctimas del conflicto armado** que habitan en el barrio Oasis por medio de la construcción de asentamientos, sino a aquellos pobladores que por no cumplir esta condición o por no ser discapacitados o de la tercera edad, tendrán que “quedarse en la calle” porque **no cuentan con una oferta institucional de reasentamiento**.  ]

[Autoridades de control departamentales y municipales como “la Personería no ha hecho nada” por defender los derechos de estas comunidades, desacatando la reiterada jurisprudencia de la Corte Constitucional y la CIDH frente a garantías institucionales de reasentamiento, así mismo ha **impedido el acompañamiento de organizaciones defensoras de derechos humanos, aseguran las personas de Oasis, quienes cuentan que e**n estos momentos el sector se encuentra rodeado por **5 tanquetas del ESMAD y varios efectivos**.     ]
