Title: Luego de 13 años condenan a militares por 'Falsos Positivos' en Vista Hermosa, Meta
Date: 2019-10-29 17:24
Author: CtgAdm
Category: DDHH, Judicial
Tags: Ejecuciones Extrajudiciales, falsos positivos, Meta, Vista Hermosa
Slug: luego-de-13-anos-condenan-a-militares-por-falsos-positivos-en-vista-hermosa-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Militares-en-Vista-Hermosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz  
] 

El pasado mes de septiembre, el Juzgado Penal del Circuito de Granada, Meta, condenó a 43 años de prisión a Manuel Ramiro Gaitán Ortiz y Miguel Antonio Beltrán Chacón, y a 16 años de prisión a Fernando Bazurto Contreras por la ejecución extrajudicial de tres campesinos en la vereda Loma Linda de Vista Hermosa, Meta. Se trata de los jóvenes Carlos Julio Gutiérrez, Elder Toloza Zulvarán y Ángel Gabriel Virguez García, asesinados y luego presentados como bajas en combate.

### **Los uniformados retuvieron a los jóvenes, los amarraron y posteriormente los asesinaron** 

Ramilo Orjulea, abogado del Colectivo Orlando Fals Borda (COFB) explicó que los hechos ocurrieron en octubre de 2006, mientras los 3 jóvenes transitaban por la vereda Vista Hermosa en horas de la tarde, en ese momento fueron retenidos por tropas del Ejército para requisarlos, pero nunca fueron dejados en libertad. Ante la desaparición de los jóvenes, los habitantes de la vereda le pidieron a un integrante de la Cruz Roja que estaba allí, desarrollando su trabajo, que preguntara a los militares por Gutiérrez, Toloza y Virgüez, pero ellos respondieron que no tenían ningún detenido.

Luego de que la comunidad insistiera a los militares sobre el paradero de los hombres, dijeron que los buscarán en el municipio de Granada, Meta, y allá los encontraron en la morgue. Según investigaciones hechas por organizaciones de derechos humanos y la Fiscalía, se supo que a los tres los habían amarrado, los tuvieron detenidos durante varias horas y luego los habían asesinado en estado de indefensión. (Le puede interesar:["Con demandas, altos mandos militares quieren censurar mural sobre ‘falsos positivos’"](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/))

Aunque los jóvenes fueron presentados como bajas en combate, se probó que no pertenecían a ningún grupo armado ilegal, por lo que se dictaron cuatro sentencias condenatorias contra soldados que aceptaron los cargos y confesaron el crimen. En una decisión reciente, el Juzgado Penal del Circuito de Granada condenó a los uniformados Gaitán Ortíz y Beltrán Chacón a purgar una condena de 43 años por estos asesinatos.

Por su parte, Bazurto Contreras fue condenado a 16 años, según explicó el Abogado, porque no hacía parte de la Fuerza Pública, y era uno de los informantes que usaba el Ejército y eventualmente los acompaña (a veces cubriendo su rostro y a veces al descubierto), señalando a las personas que podrían ser guerrilleros. (Le puede interesar: ["Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos"](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/))

### **"El Ejército los patrocina y los sigue defendiendo"** 

El Abogado afirmó que los uniformados que cometieron estos crímenes seguían directrices de carácter nacional, "no se trata de soldados que amanecieron de mal genio y les dió por matar personas, esto es una política oficial del Ejército mandada por Álvaro Uribe Vélez para asesinar campesinos y presentarlos como guerrilleros dados de baja en combate, y demostrar ante la prensa que le estaba ganando la guerra a los terroristas, como ellos llamaban". Orjuela también cuestionó que algunos militares sigan vinculados a la Fuerza Pública, "siguen ganando sueldos, siguen ascendiendo y no han tenido ninguna sanción, osea que el Ejército los patrocina y los sigue defendiendo".

Adicionalmente, afirmó que también se podría investigar la participación en el asesinato de Gutiérrez, Toloza y Virgüez del mayor del Ejército Miguel Beltrán Chacón; y por responsabilidad de mando, abrirse un proceso contra el general Guillermo Quiñonez Quiros, que ejercía como comandante de la Cuarta División, y fue la persona que recibió las denuncias de que en la zona se presentaban ejecuciones extrajudiciales "ante las cuales no hizo nada", aseguró el abogado.

### **'Falsos Positivos': ¿Política de Estado?** 

El integrante del Colectivo Sociojurídico afirmó que ellos llevan más casos similares en la zona, "de hecho, tenemos el caso de 2 niños que por sentencia judicial, el próximo 15 de noviembre el Ministro de Defensa debe venir a pedir disculpas públicas". Este caso también se presentó en el mismo municipio Vista Hermosa, "eso significa que sí fue una política de Estado", expresó el Abogado.

Para concluír, Orjuela señaló que algunos de los militares que ya han recibido sentencia en este caso se acogieron a la Jurisdicción Especial para la Paz (JEP), donde esperan que digan la verdad de los hechos y acepten su responsabilidad, de forma tal que sea posible la verdad, justicia, reparación y garantías de no repetición para las víctimas. (Le puede interesar: ["Nueve comandantes del Ejército estarían implicados en falsos positivos: Human Rights Watch"](https://archivo.contagioradio.com/nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
