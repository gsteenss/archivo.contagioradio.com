Title: ¿Aníbal Gaviria, el Gobernador de Antioquia o el mandatario que tiene la “democracia empeñada”?
Date: 2020-01-14 16:15
Author: Carlos Montoya
Category: Nacional, Opinion
Tags: Aníbal Gaviria, Antioquia, empresas, familia Gaviria Correa, Medellin, Uniban
Slug: anibal-gaviria-el-gobernador-de-antioquia-o-el-mandatario-que-tiene-la-democracia-empenada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/familia-Gaviria-Correa-.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/familia-Gaviria-Correa-1-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"normal"} -->

**Por: Carlos Montoya  
**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","dropCap":true} -->

El nuevo **Gobernador de Antioquia Aníbal Gaviria Correa** ganó las elecciones con 831.646 votos, es decir el 36% del total de la votación. Su deber constitucional es representar a los casi seis millones de habitantes del departamento con políticas que busquen reducir los altos índices de pobreza y desigualdad que presenta el territorio. No obstante, estas nobles misiones se podrían ver truncadas por las financiaciones a su campaña política y los intereses de empresas familiares que depositaron grandes sumas de dinero para hacerlo gobernador.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un estudio de la Misión de Observación Electoral –MOE- titulado “[Democracias Empeñadas](https://moe.org.co/wp-content/uploads/2018/11/Democracias-Empe%C3%B1adas_Digital-1.pdf)” , permite dimensionar el problema, cuando analiza el vínculo de los aportantes a las campañas electorales de los mandatarios locales del año 2015 y la adjudicación de contratos públicos con estos financiadores. 

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *485 donantes hicieron 534 aportes a campañas políticas por un monto de \$4,4 mil millones de pesos*
>
> <cite>MOE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El resultado de la investigación muestra que “*485 donantes hicieron 534 aportes a campañas políticas por un monto de \$4,4 mil millones de pesos. Estas 485 personas recibieron 2410 contratos de parte de las administraciones públicas que financiaron, por un monto de \$169 mil millones de pesos. El valor de los contratos adjudicados equivale a 39 veces el dinero donado en campaña.”* Además, la Gobernación de Luis Pérez, fue la que presentó el mayor número de personas que donaron a la campaña política y luego se beneficiaron de contratos. De las 103 personas naturales o jurídicas que realizaron 112 donaciones (\$663 millones de pesos), recibieron 616 contratos por \$85,9 mil millones de pesos.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el caso de Aníbal Gaviria, el **33% del dinero que ingresó a su campaña está relacionada con el proyecto portuario Puerto Antioquia**, donde su familia, personas cercanas y aliados empresariales participan de manera directa e indirecta.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El tema es complejo para el nuevo mandatario porque según la Ley de Contratación Estatal, las personas que hayan aportado más del 2.5% de las sumas máximas de las campañas políticas tienen inhabilidades e incompatibilidades para contratar, e incluye a sociedades existentes o que vayan a constituirse. Hechos que podrían estarse presentando en el proyecto Puerto Antioquia, donde cuatro de los siete socios pueden tener conflicto de interés, sea porque aportaron dineros a la campaña directamente o son controlados por familiares o personas cercanas a la familia Gaviria Correa. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En otras palabras, UNIBAN, C.I Tropical, C.I Banafrut S.A y Agrícola Santamaría, además de financiar el proyecto portuario, aportaron dineros a la campaña política de Gaviria y poseen en sus juntas directivas a familiares del actual gobernador. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lazos de sangre, dinero y política**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para comprender la relación de las empresas involucradas en Puerto Antioquia con la financiación de la campaña de Aníbal Gaviria, se presenta la relación de gastos presentados al Consejo Nacional Electoral.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **La campaña de Aníbal Gaviria reportó el ingreso de \$3.409.177.000**

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Según la información del portal Cuentas Claras, la campaña de Aníbal Gaviria reportó el ingreso de \$3.409.177.000. Distribuidos de la siguiente manera:  

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  --------------------------------------------------------------------------------------------------------------------------------------------------- -------------------
  **Concepto**                                                                                                                                        **Ingresos**
  Créditos o aportes que provengan del patrimonio de los candidatos, de sus cónyuges o de sus compañeros permanentes o de sus parientes               1.006.509.379
  Contribuciones, donaciones y créditos, en dinero o especie, que realicen los particulares                                                           768.913.000
  Créditos obtenidos en entidades financieras legalmente autorizadas                                                                                  1.493.490.621
  Ingresos originados en actos públicos, publicaciones y/o cualquier otra actividad lucrativa del partido o movimiento                                110.264.000
  Recursos propios de origen privado que los partidos y movimientos políticos destinen para el financiamiento de las campañas en las que participen   30.000.000
  **Total**                                                                                                                                           **3.409.177.000**
  --------------------------------------------------------------------------------------------------------------------------------------------------- -------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"justify"} -->

Hay dos rubros para analizar en detalle. El aporte realizado por sus familiares \$1.006.509.379, hechos de la siguiente forma: Su madre, Adela Correa de Gaviria, con \$479.813.734 y sus hermanos: León Toné Gaviria Correa y Pedro Sergio Gaviria Correa. El primero con \$270.593.360 y su otro con \$256.102.285. Representando el 29% del total ingresado a la campaña.  

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  ------------------------------ ---------------- -------------------------
  **Nombre**                     **Parentesco**   **Monto**
  ADELA CORREA DE GAVIRIA        MADRE            \$       479.813.734 
  LEON TONE  GAVIRIA CORREA      HERMANO          \$          270.593.360
  PEDRO SERGIO  GAVIRIA CORREA   HERMANO          \$       256.102.285 
  **Total**                      \                **\$    1.006.509.379**
  ------------------------------ ---------------- -------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"justify"} -->

El segundo rubro analizado son los aportados por particulares \$768.913.000, donde el 17%, 130 millones, lo realizaron tres empresas: AUGURA con 60 millones, La Promotora Bananera –PROBAN- y la Bananera Génesis, cada una con 35 millones.

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  ------------------------------------- ---------------------------
  **Empresa**                           **Monto**
  ASOCIACION DE BANANEROS DE COLOMBIA   \$          60.000.000 
  PROMOTORA BANANERA S.A (PROBAN)       \$          35.000.000 
  BANANERA GENESIS S.A                  \$          35.000.000 
  **Total**                             **\$       130.000.000 **
  ------------------------------------- ---------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"justify"} -->

Teniendo en cuenta los ingresos recibidos, se presenta la relación de las empresas involucradas con el proyecto Puerto Antioquia y su relación con la financiación de la campaña de Aníbal Gaviria:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **UNIBAN**

<!-- /wp:list -->

<!-- wp:paragraph {"align":"justify"} -->

Con el fin de mostrar la relación de la junta directiva de UNIBAN y la financiación de la campaña del nuevo Gobernador de Antioquia, se presenta el siguiente cuadro que muestra que 15 de los 19 integrantes de la estructura organizativa, hacen parte de empresas que aportaron dinero a la campaña de Aníbal Gaviria como son AUGURA y la Promotora Bananera –PROBAN-.  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"very-light-gray","customBackgroundColor":"#006699"} -->

**Tabla de la junta directiva de UNIBAN y la participación de sus integrantes en otras empresas**

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  --------------------- ------------------------------- -------------------------------------------------------------------------------
  **Cargo**             **Nombre**                      **Otras empresas donde participa**
  Presidente            Juan Luis Cardona Sierra        Proban - Uniban - Nueva Plantacion - Uberaba
  Representante Legal   Irene Gaviria Correa            Proban - Uniban - Grupo 20
  Representante Legal   Jaime Henríquez Gallo           Proban - Uniban - Augura
  Representante Legal   Juan Esteban Álvarez Bermudez   Proban - Sara Palma - Uniban - Augura - Nueva Plantacion - Uberaba - Grupo 20
  Representante Legal   Oscar Enrique Penagos Garces    Proban - Sara Palma - Uniban - Augura - Nueva Plantacion - Uberaba
  Representante Legal   Carlos Aníbal Trujillo Gomez    Proban - Sara Palma - Uniban
  Representante Legal   Marcela Estrada Montoya          
  Representante Legal   Ramiro Aurelio Tobón Gaviria     
  Poder General         Jesús Alirio Correa Pérez        
  Principales           Jaime Henríquez Gallo           Proban - Uniban - Augura
  Principales           Juan Esteban Álvarez Bermudez   Proban - Sara Palma - Uniban - Augura - Nueva Plantacion - Uberaba - Grupo 20
  Principales           Jose Gentil Silva Holguin       Proban - Augura - A. Argentina - Uniban
  Principales           Carlos Anibal Trujillo Gomez    Proban - Sara Palma - Uniban - Nueva Plantacion - Uniban
  Principales           Carla Henriquez Fattoni         Proban - Santamaria - Uniban
  Suplentes             Oscar Aristizabal Vahos         Proban - Uniban
  Suplentes             Irene Gaviria Correa            Proban - Uniban - Grupo 20
  Suplentes             Juan Jose Gonzalez Serna         
  Suplentes             Jorge Arango Zuluaga            Proban - Uniban
  Suplentes             Oscar Enrique Penagos Garces    Proban - Sara Palma - Uniban - Augura - Nueva Plantacion - Uberaba
  --------------------- ------------------------------- -------------------------------------------------------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"center","fontSize":"small"} -->

Fuente: Construcción propia con base a los Certificados de Cámara de Comercio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","fontSize":"normal"} -->

  
Adicionalmente, **UNIBAN**, según la Cámara de Comercio tiene control sobre varias de las empresas que aportaron financieramente a la campaña de Aníbal Gaviria:  

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  -------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Empresa que controla**   **Empresa controlada**

  UNIBAN                     Promotora Bananera S.A. (Empresa que aportó a la campaña de Aníbal Gaviria) Agricola Sara Palma S.A. nueva Plantacion S.A. Agrícola Uberaba S.A. Polyban Internacional S Atropical Marketing Associated "T.M.A "Uniban International Ltda Isabella Shipping Company Limited Proban International Corporation Turbana Marketing Ltdbananera Genesis S.A. a Través de Polyban  
                              
                              
                            Internacional S A (Empresa controlada indirectamente) Empresa que aportó a la campaña de Aníbal Gaviria  
 -------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"center","fontSize":"small"} -->

Fuente: Construcción propia con base a los Certificados de Cámara de Comercio  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **C.I Tropical**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Carolina Sanín Restrepo, es la representante legal de la C.I Tropical S.A, empresa asociada al proyecto portuatio, igualmente es integrante de la junta directiva de la Asociación de Bananeros de Colombia –AUGURA- organización que aportó 60 millones a la campaña de Aníbal Gaviria.    

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **C.I Banafrut S.A**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sandra María Estrada Yepes, integrante de la junta directiva de la empresa de C.I Banafrut S.A, también hace parte de las directivas de la Asociación de Bananeros de Colombia –AUGURA- organización que aportó 60 millones a la campaña de Aníbal Gaviria y financia el Puerto de Urabá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Agrícola Santamaría**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

John James Alcaraz Restrepo, es el segundo representante legal de Agrícola Santamaría, empresa asociada al proyecto, y a su vez, integrante de la junta directiva de la Bananera Génesis, organización que aportó 35 millones a la campaña de Aníbal Gaviria. Además, es controlada indirectamente por UNIBAN según lo demuestra el certificado de la Cámara de Comercio. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Agropecuaria Grupo 20**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la Agropecuaria Grupo 20 no hace parte de las empresas asociadas a Puerto Antioquia, sus integrantes se benefician del proyecto. El control empresarial según lo muestra el certificado de la Cámara de Comercio está a cargo de la familia Gaviria Correa mostrando que cinco de los seis hermanos, más la participación de Daniel Arturo Gaviria Vélez (Hijo de Guillermo Gaviria) toman las decisiones. Sofía Gaviria, es la única que no está en la junta directiva de la empresa porque en la actualidad es embajadora en Suiza.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El representante legal de la empresa es Juan Esteban Álvarez Bermúdez, hombre de confianza de la familia, quien el mismo Aníbal lo considera como “hermano de la vida” y según esta investigación, hace parte de cinco juntas directivas de empresas bananeras (Promotora Bananera –PROBAN-, Agropecuaria Sara Palma, Nueva Plantación S.A., Agrícola Uberaba S.A y AUGURA) y representante legal en dos empresas (UNIBAN y Agropecuaria Grupo 20). Varias de ellas están relacionadas con Puerto Antioquia como el caso de UNIBAN y la financiación de la campaña de Aníbal Gaviria, AUGURA y la Promotora Bananera –PROBAN-

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto permite interpretar, que Juan Estaban Álvarez es el representante de la familia Gaviria Correa en varios espacios empresariales y políticos, ya que los Certificados de Cámara de Comercio de las empresas mencionadas y artículos de prensa, muestran que fue asesor durante la primera Gobernación de Aníbal, pero también fue considerado un “Rasputín” durante el periodo en la Alcaldía de Medellín. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","customTextColor":"#ffffff","customBackgroundColor":"#006699"} -->

**Además, hay otros dos datos relevantes: Los hermanos Jorge Julián Gaviria y Irene Gaviria participan en otras juntas directivas relacionadas con el proyecto Puerto Antioquia y la financiación de la campaña de su hermano:**

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  ----------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Hermanos/as de Aníbal Gaviria**   **Juntas directivas donde participan**
  Jorge Julián Gaviria Correa         Agropecuaria Sara PalmaNueva Plantación S.APromotora Bananera –PROBAN- (Empresa que aportó a la campaña de Aníbal Gaviria)AUGURAAgrícola Uberaba S.AAgropecuaria Grupo 20
  Irene Gaviria Correa                Promotora Bananera –PROBAN- (Empresa que aportó a la campaña de Aníbal Gaviria)UNIBAN (Representante legal y suplente en la junta directiva)
  León Toné Gaviria Correa            Agropecuaria Grupo 20
  Pedro Sergio Gaviria Correa         Agropecuaria Grupo 20
  Adelaida Gaviria Correa             Agropecuaria Grupo 20
  ----------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"fontSize":"small"} -->

Fuente: Construcción propia con base a los Certificados de Cámara de Comercio  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","fontSize":"normal"} -->

En el siguiente mapa se revelan las relaciones entre familiares, empresas y aportes a la campaña de Aníbal Gaviria que representaría un conflicto de interés, sobre el proyecto portuario Puerto de Urabá. 

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![C:\\Users\\Usuario\\Documents\\BACKUP ASUS 14.JUNIO\\DOCUMENTOS\\Personal\\Artículos\\ANIBAL GAVIRIA\\INFO DE ANIBAL-01.png](https://lh4.googleusercontent.com/YxK_79O_6Bs_Z_Xb29EJLbtTImmrVGAXLqY-qCC2tBtEE5Sl4kniVI8vqNMXXBL5ZSOJUs6RGoND1kKN4BdR104bPFrN0A5O3RQIfhSjd6o4ssgfd2KxLaHBDL2xjAgVS27MGvmvCXLmbgm1zQ)

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"fontSize":"small"} -->

Fuente: Construcción propia con base a los Certificados de Cámara de Comercio

<!-- /wp:paragraph -->

<!-- wp:heading -->

**¿Empresas bananeras financiaron a otros candidatos?**
-------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Andrés Guerra fue el otro candidato que recibió aportes económicos de empresas bananeras por 60 millones. Son los casos de Agrícola Santamaría y AUGURA cada una con 30 millones respectivamente, representando menos de la mitad de los 130 millones que recibió Aníbal Gaviria. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, varios integrantes de la junta directiva de la Agrícola Santamaría también hacen parte de empresas donde participa la familia Gaviria Correa como son UNIBAN, Agrícola Sara Palma, Agrícola UBERABA, Nueva Plantación S.A. y PROBAN. Lo que muestra al parecer una convivencia para hacer negocios y también una estructura empresarial para financiar proyectos con candidatos afines.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Tabla de la junta directiva de Agrícola Santamaría y la participación de sus integrantes en otras empresas**

<!-- /wp:paragraph -->

<!-- wp:table {"backgroundColor":"subtle-light-gray"} -->

<figure class="wp-block-table">
  ------------------------------- ---------------------------------- ------------------------------------------------------
  **Cargo**                       **Nombre**                         **Otras empresas donde participa**
  Gerente Y Representante Legal   Gallego Davila Federico            Nueva Plantacion - Uberaba - Sara Palma - Santamaria
  Suplente 1                      Alcaraz Restrepo John James        Santamaria - Genesis
  Suplente 2                      Henriquez Fattoni Carla Cristina   Proban - Santamaria - Uniban
  Poder General                   Rocio Valencia Castro               
  ------------------------------- ---------------------------------- ------------------------------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"align":"center","fontSize":"small"} -->

Fuente: Construcción propia con base a los Certificados de Cámara de Comercio  

<!-- /wp:paragraph -->

<!-- wp:heading -->

**Las entidades de control deberían actuar con urgencia**
---------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Lo anterior permite inferir que la familia Gaviria Correa aportó el 33% del dinero que ingresó a la campaña de Aníbal Gaviria empleando varios mecanismos con el fin de reportar fuentes distintas al Consejo Nacional Electoral. Por un lado, el formato de financiación directa de familiares y por el otro, por medio de las empresas donde participan o tienen control.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, el monto aportado por la familia Gaviria aunque que se puede considerar alto, no representa ni el 1% de los 700 millones de dólares que supuestamente cuesta el [proyecto Puerto Antioquia](https://abceconomia.co/2019/03/22/puerto-antioquia-a-un-paso-de-conectar-a-colombia-con-el-mundo/) y donde ellos serían grandes beneficiados. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a esa situación, el abogado **Luis Alfonso Bravo**, representante legal del gobernador, le respondía al [portal Verdad Abierta](https://verdadabierta.com/las-sombras-que-rodean-al-nuevo-gobernador-de-antioquia/) que el nuevo mandatario iba a realizar un trámite ante la Procuraduría General de la Nación con el fin de consultar si existe un conflicto de interés y si era el caso, nombrar un gobernador Ad Hoc para el asunto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo las entidades de control ya deberían haber realizado acciones urgentes porque el 5 de diciembre del 2019, antes de posesionarse como nuevo mandatario departamental, Aníbal Gaviria se reunió con empresarios involucrados en el proyecto Puerto Antioquia con el fin de “*tratar los temas del sistema portuario de Urabá…”*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de las personas que participó del encuentro fue Jaime Enríquez Gallo, integrante de las juntas directivas de varias empresas, entre ellas, UNIBAN, socia del proyecto Puerto Antioquia, pero también de AUGURA y PROBAN, ambas aportantes a la campaña del nuevo mandatario departamental.  

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![ En la foto están: el nuevo Gobernador de Antioquia Aníbal Gaviria, Daniel Quintero, el Alcalde de Medellín y el empresario bananero Enrique Gallo integrante de las juntas directivas de UNIBAN (socia de Puerto Antioquia) y de AUGURA y Promotora Bananera –PROBAN- (Empresas que aportaron dinero a la campaña de Aníbal Gaviria). Foto 5 de diciembre de 2019](https://lh6.googleusercontent.com/-LhCABc7sdVvnAvzU55484ynZdllMhHRHuiBDL6P-ybd5VJTUd8cDF6sKsVvcKMRHcK0MtehGhgEQNpJ3rDJ33-qGe-FqUvcxOoYA-KZ11dvW7Y7idqk-FDK1bt_jdqbIDXaCJDi7U3CAWywOg)  

<figcaption>
En la foto están: el nuevo Gobernador de Antioquia Aníbal Gaviria, Daniel Quintero, el Alcalde de Medellín y el empresario bananero Enrique Gallo integrante de las juntas directivas de UNIBAN (socia de Puerto Antioquia) y de AUGURA y Promotora Bananera –PROBAN- (Empresas que aportaron dinero a la campaña de Aníbal Gaviria). Foto 5 de diciembre de 2019

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Lo engorroso de estos tejemanejes es que no solo involucra dineros públicos sino también violaciones a los derechos humanos, porque al parecer parte del terreno donde se va desarrollar el proyecto, hay 81 solicitudes de restitución de tierras hechas por familias y un proceso colectivo étnico por parte el Consejo Comunitario de Puerto Girón. Además, varios empresarios, como el mismo Jaime Enríquez Gallo, son opositores dentro del proceso de reclamación de las comunidades. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este contexto genera que el nuevo mandatario no solo debe hacer la consulta a los entes de control por su posible conflicto de interés, sino que adicionalmente, la Contraloría y la Procuraduría deben delegar una comisión especial de seguimiento con el fin de garantizar la transparencia en la ejecución de los recursos públicos en el proyecto y la garantía de los derechos a la restitución de tierras de las familias afectadas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además, las autoridades electorales deberían acoger algunos de las recomendaciones realizadas por la MOE en su informe sobre las “Democracias Empeñadas” en la campaña de Aníbal Gaviria:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Conocer la procedencia de los recursos de los donantes. Todas las personas que aporten a campañas deberán estar dispuestas a entregar toda su información financiera del año anterior a la donación. La Superintendencia de Sociedades y la DIAN son los socios naturales de este proceso.” *
>
> <cite>MOE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

En este caso, se podría saber si el dinero que aportaron los hermanos León Tome y Pedro Sergio Gaviria y reportados ante el Consejo Nacional Electoral como personales, son en realidad las ganancias de las empresas involucradas con el proyecto Puerto Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por estas razones, el nuevo mandatario departamental tiene el reto de demostrar que es un gobernante que va impulsar acciones integrales para todas las subregiones de Antioquia y no uno que puede tener la “Democracia Empeñada” siendo el gerente soñado e indirecto de Puerto Antioquia.  

<!-- /wp:paragraph -->
