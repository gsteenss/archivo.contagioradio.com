Title: Presidente de Sintrainagro en Guacarí fue asesinado en el Valle del Cauca
Date: 2017-07-02 12:03
Category: DDHH, Nacional
Tags: sindicalista, sintrainagro, Valle del Cauca
Slug: presidente-de-sintrainagro-fue-asesinado-en-el-valle-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Alberto-roman-e1499014082341.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sintrainagro ] 

###### [02 Jul. 2017] 

Por medio de un comunicado el Sindicato Nacional de Trabajadores de la Industria Agropecuaria – SINTRAINAGRO – aseguró que este sábado fue asesinado uno de sus integrantes, **Alberto Román Acosta, quien era un reconocido líder en Guacarí, departamento del Valle del Cauca.**

Los hechos se presentaron mientras Román veía a su hijo jugar fútbol, en una cancha múltiple en el Barrio Santa Bárbara del municipio de Cerrito en el Valle del Cauca, momento en el que **dos hombres le dispararon. El líder fue llevado a un hospital donde falleció debido a las heridas con arma de fuego.**

Sintrainagro aseguró que seguirán trabajando por sus derechos “creemos en la justa lucha por los derechos de los trabajadores y **continuaremos luchando por mejores condiciones laborales y sociales para todas y todos los trabajadores afiliados** a la organización y sus familias”.

De igual modo solicitaron a las autoridades investigar y esclarecer este crimen “para que no quede en la impunidad, como tantos otros en nuestro país”. Le puede interesar: [Cortero de caña en cuidados intensivos tras agresión del ESMAD](https://archivo.contagioradio.com/esmad-habria-atacado-con-un-hacha-en-la-cabeza-a-cortero-de-cana-en-risaralda/)

Sintrainagro, **el sindicato al cual pertenecía Alberto Román, agrupa corteros de caña en el Valle**, mientras en Antioquia tiene entre sus afiliados, trabajadores de las fincas de bananos, y flores en el centro del país, así como trabajadores de la fincas palmeras.

![sindicato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/sindicato.jpg){.alignnone .size-full .wp-image-43045 width="742" height="960"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
