Title: A salvo el páramo de Santurbán de Eco Oro Minerals
Date: 2016-12-20 18:03
Category: Ambiente, Voces de la Tierra
Tags: Eco Oro Minerals, Gran Minería en Colombia, Páramo de Santurbán, Retiro de inversión extractivista
Slug: a-salvo-paramo-de-santurban-eco-oro-minerals
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/santurbán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corponor] 

###### [20 Dic 2016] 

Después de cuatro años de espera la comunidad defensora del páramo de Santurbán, recibió la noticia que la Corporación Financiera Internacional tomó la decisión de **retirar su inversión a la empresa minera canadiense Eco Oro Minerals, que desarrolla el proyecto minero Angostura en el páramo de Santurbán**, ecosistema de alta montaña que abastece de agua a millones de personas en Colombia.

Alix Mancilla, representante del Comité por la Defensa del Agua y el Páramo de Santurbán, manifestó que “aplaudimos esta decisión del Banco, que le da la razón Comité sobre la inviabilidad de la minería en el páramo (…) **ahora pedimos al gobierno colombiano que se abstenga de otorgar licencias ambientales a cualquier proyecto minero** **que afecte a Santurbán”. **Le puede interesar: [Ambientalistas exigen al banco mundial no financiar proyecto en Santurbán.](https://archivo.contagioradio.com/ambientalistas-exigen-al-banco-mundial-no-financiar-proyecto-santurban/)

Por otra parte, Carlos Lozano Acosta integrante de la Asociación Interamericana para la Defensa del Ambiente –AIDA–, aseguró que el retiro de la inversión por parte de la CFI al proyecto que pretendía extraer de Santurbán 12,6 millones de onzas de oro, **“es un duro golpe político y financiero para la minería en el páramo de Santurbán** (…) el gobierno colombiano debe reflexionar sobre su actitud permisiva hacia la minería a gran escala en páramos, que es ilegal”.

### **¿Ahora qué viene?** 

Como respuesta, Eco Oro Minerals ha interpuesto una **demanda de arbitraje contra el gobierno de Colombia bajo el Tratado de Libre Comercio con Canadá** y ante el Centro Internacional de Arreglo de Diferencias Relativas a Inversiones que hace parte del Banco Mundial.

Carla García Zendejas del Centro para el Derecho Internacional Ambiental, denunció que “el interés de Eco Oro Minerals en Colombia ya no es la mina, sino extorsionar a un Estado soberano por millones de dólares y presionar para debilitar las protecciones para el agua colombiana (…) con el retiro, **se resalta la existencia de proyectos mineros desacertados en el páramo colombiano, así como la ilegitimidad de esta demanda".**

Por último la defensora ambiental Alix Mancilla, señaló que "Eco Oro Minerals tenía concesión y permiso para explorar pero no para explotar (...) **esperamos que con el retiro de la inversión, la empresa abandone el páramo y saque la maquinaria". **

###### Reciba toda la información de Contagio Radio en [[su correo]
