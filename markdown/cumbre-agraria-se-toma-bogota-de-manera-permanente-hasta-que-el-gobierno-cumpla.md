Title: Cumbre Agraria se toma Bogotá de manera permanente hasta que el gobierno cumpla
Date: 2015-09-01 13:12
Category: Movilización, Nacional
Tags: Cumbre Agraria, Cumbre Agraria Étnica y Popular, Cumbre agraria se reune con el presidente Santos, Toma de Bogotá por parte de la Cumbre Agraria
Slug: cumbre-agraria-se-toma-bogota-de-manera-permanente-hasta-que-el-gobierno-cumpla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cumbre_agraria_2_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: congreso de los pueblos 

<iframe src="http://www.ivoox.com/player_ek_7745662_2_1.html?data=mJyhl5uado6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9bhw9fSjcbLtsLmysaY1cqPuNDhwpDP0czTuIa3lIqlk5DIqYzhwtPS1MaPtMbmzsbbx9PYqYzcwtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rueda de prensa- Voceros Cumbre Agraria] 

###### [01 Sept 2015] 

En rueda de prensa los voceros de la Cumbre anunciaron **que regresarán a los territorios cuando se consolide una ruta clara de cumplimiento por parte del Gobierno colombiano**. La política minero energética, el Plan Nacional de Desarrollo, el establecimiento de las ZIDRES, el estatuto rural y la falta de garantías en materia de derechos humanos para la movilización son algunas de las razones de fondo de esta toma permanente de Bogotá

Además anunciaron que se prepara un gran paro nacional para el 2016 y **desmiente que los dineros invertidos en el sector agrario han llegado a manos de pequeños campesinos, como lo ha anunciado el ministro Aurelio Iragorri,** el presupuesto nacional está siendo asignado a proyectos de inversión de grandes empresarios y transnacionales.

Tras el decreto 804 de mayo de 2014, que estableció la Mesa de Interlocución entre la Cumbre y el Gobierno, en la que se acordó un pliego único de negociación con puntos mínimos sobre derechos humanos, territorio, proyectos de infraestructura y seguridad alimentaria, **no han habido acciones concretas por parte del presidente ni de los ministros.**

La situación de **derechos humanos en los territorios es cada vez más crítica tanto para los líderes como para las bases sociales**, lo que resulta inconsecuente con los acuerdos a los que está llegando el Gobierno en La Habana, por lo que se exige un proceso de paz con garantías que incluyan la anulación de leyes que favorezcan la impunidad y la criminalización de la protesta, afirman los voceros.

Los voceros y voceras de la Cumbre Agraria reiteraron el llamado al presidente Juan Manuel Santos y a los ministros de agricultura y del interior para que asuman responsabilidad y **voluntad política en la discusión y el cumplimiento de los puntos del pliego de negociaciones.**

Desde el pasado 31 de agosto se dio inicio a las Jornadas de Indignación Campesina, Étnica y Popular, liderada por la Cumbre Agraria, integrada por movimientos indígenas, campesinos y afro descendientes, con el objetivo de **exigirle al gobierno colombiano el cumplimiento de los acuerdos pactados en el marco de las movilizaciones de los años 2013 y 2014.**
