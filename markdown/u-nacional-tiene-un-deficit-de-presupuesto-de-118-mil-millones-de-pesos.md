Title: U.Nacional en crisis por déficit presupuesto de 118 mil millones de pesos
Date: 2017-05-11 16:47
Category: Educación, Nacional
Tags: Paro Estudiantil, Universidad Nacional
Slug: u-nacional-tiene-un-deficit-de-presupuesto-de-118-mil-millones-de-pesos
Status: published

###### [Foto:] 

###### [11 May 2017]

Estudiantes de la Universidad Nacional sede Medellín y sede Manizales, entraron a paro tras no llegar a ningún acuerdo con las directivas frente a la reforma del Acuerdo 044/09 al estatuto estudiantil, hecha por el Consejo Superior Universitario, debido a que consideran que es **antidemocrática y que en vez de brindar soluciones a la crisis de** **desfinanciación que podría llegar a los 118 mil millones, la profundiza**. Las demás sedes, se encuentran en Asamblea Permanente.

### **Bienestar universitario en estado de coma** 

Los tres puntos coyunturales en los que los estudiantes exigen transformaciones inmediatas son: el primero, **el encubrimiento a la crisis en términos de financiación para el bienestar universitario**, que, de acuerdo con Cristian Guzmán, estudiante de Ciencias políticas de la Universidad Nacional, con la reforma permitiría que el presupuesto que se designa a este rubro sea cada vez menor.

“En el caso de la Universidad Nacional, sede Antioquía, aquí el restaurante que tenemos está completamente lleno de restaurantes privados que venden el almuerzo entre 7 mil u 8 mil pesos, la población de esta sede ronda entre los **12 mil y 15 mil estudiantes y solo se dan 300 bonos de almuerzos y 100 para desayuno**, estas son las condiciones del bienestar precario” afirma Guzmán.

Los estudiantes hacen un llamado a las directivas para que recuerden que el bienestar universitario **“son condiciones de posibilidad económicas, de salud, materiales que el estudiante necesita para poder estudiar**” y que es en esta medida en la que se debe invertir y no a partir de las crisis que afronte la universidad. Le puede interesar: ["Déficit en Educación es de 600 mil millones: FECODE"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

### **Reforma a las sanciones disciplinarias** 

La reforma estatutaria genera un cambio en las sanciones disciplinarias y expone tres escenarios; el primero es un arreglo directo entre las personas involucradas, el segundo y que mayor desazón ha generado, es la mediación, **proceso nuevo que tendría como instancia un comité disciplinario**, conformado por un docente, un estudiante y un delegado del decano en donde se intentaría llegar a mejores términos entre las partes.

Sin embargo, los estudiantes señalan que la conformación de este comité está viciado por la representación tan pequeña que tendría el estudiantado y que en temas como la venta de mercancía por parte de los estudiantes, no habría tal mediación, por el peso que tendría el docente y el delegado del decano, y **se recurriría al tercer escenario que es la sanción disciplinaria.**

### **La democracia en vilo en la Universidad Nacional** 

El tercer punto tiene que ver con el espacio de participación democrática, ya que Cristian asevera, que con la reforma se pretende que el CSU pueda determinar si un representante puede o no participar en este escenario, a través del **cumplimiento de unos criterios y de un proceso ejecutivo que se realizaría en el mismo espacio, en donde la comunidad estudiantil es minoría.**

En dado caso de que esos procesos determinen que el representante no puede participar, se anularía la intervención de los estudiantes. Le puede interesar: ["Por falta de presupuesto se está cayendo la Universidad Nacional"](https://archivo.contagioradio.com/por-falta-de-presupuesto-se-esta-cayendo-la-universidad-nacional/)

Además, los estudiantes han venido señalado que las formas en las que se consultó y expuso a la comunidad universitaria sobre la reforma al estatuto estudiantil, no fue democrática, **porque nunca se generó un espacio de interlocución entre todos los sectores que permitiera la construcción de un acuerdo más incluyente**.

### **Las preguntas sin respuesta** 

Otra de las exigencias de los estudiantes en este paro, es que las directivas **expongan los presupuestos e inversiones que se hacen con los dineros que se reciben**, para poder establecer de cuanto es la crisis financiera que afronta la Universidad Nacional. Le puede interesar:["Universidad Nacional se raja en infraestructura"](https://archivo.contagioradio.com/23-edificios-en-riesgo-y-4-con-amenaza-de-ruina-estudiantes-unal-se-movilizan/)

“Queremos darle al país los números que vean junto con nosotros lo que está pasando con la universidad, no hemos podido recopilar los datos porque **la posición de los directivos es no dar la cara y no mencionar la palabra crisis**” señala Guzmán.

Los estudiantes expresan que ese monto **podría acercarse a los 118 mil millones de pesos**, sin embargo, continúan a la espera de conocer la cifra oficial por parte de las directivas.

### **¿Qué harán los estudiantes?** 

La propuesta de los estudiantes para finiquitar el paro y las asambleas permanentes, es que las directivas de la **Universidad Nacional accedan a generar un encuentro con todos los sectores de la institución** que permita modificar la reforma al acuerdo 0044 y donde se permita escuchar las posturas de los estudiantes.

Mientras se logra este escenario de participación, los estudiantes adelantan asambleas y encuentros más pequeños por facultades para decidir si el resto de sedes se suman al paro. Le puede intresar:["Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de Educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

<iframe id="audio_18633448" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18633448_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
