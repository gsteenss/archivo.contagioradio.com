Title: Desplazamiento forzado de 16 familias indígenas en Buenaventura
Date: 2017-01-27 12:02
Category: DDHH, Nacional
Tags: buenaventura, confinados, Desplazamiento, indígenas
Slug: hombres-armados-desplazan-16-familias-indigenas-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/3a1156b6-95ba-4ecc-b860-35594a3e70a4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [27 Ene. 2017] 

Desde el 8 de enero, **cerca de 38 personas agrupadas en 16 familias se encuentran desplazadas y confinadas** en la Casa Grande (Tambo Mayor) ubicado en el Cabildo Indígena Valledupar del pueblo Wounaan Nonam, en zona rural de Buenaventura, límites con el departamento del Chocó, **debido a la presencia de 5 hombres vestidos de camuflado y armados** en un sitio habituado para el cultivo. Le puede interesar: [300 neoparamilitares transitan por territorios en Chocó sin respuesta del Estado](https://archivo.contagioradio.com/300-neoparamilitares-transitan-por-territorios-en-choco-sin-respuesta-del-estado/)

Los indígenas tomaron esa decisión, por temor a posibles represalias por parte de dichos hombres armados, razón por la que **no pueden realizar sus actividades habituales y no tienen acceso a bienes y servicios básicos.**

En la comunicación, dada a conocer a través de la página web de la Oficina de la ONU para la Coordinación de Asuntos Humanitarios en Colombia (OCHA), se da cuenta de la situación que viven los menores integrantes de estas familias y se confirma que **“varios niños han empezado a presentar afectaciones de salud como gripe y fiebre”.**

En años anteriores, esta comunidad ya había sido víctima de varias amenazas y restricciones a su movilidad por presencia de grupos armados en la zona. Le puede interesar: [Lideres indígenas, blanco de asesinatos y amenazas](https://archivo.contagioradio.com/lider-indigena-del-cauca-amenzado-por-los-urabenos/)

Según la comunidad, los patrullajes que realiza de manera continua la Armada Nacional cerca a sus territorios **“aumentan el temor y la preocupación de sus habitantes por posibles enfrentamientos con los grupos armados”.**

Pese a que el 9 de enero diversas organizaciones como ACNUR de la mano de la Defensoría del Pueblo, la Secretaría de Gobierno y la Unidad para las Víctimas conocieron la situación y visitaron a la comunidad **hasta el momento no se conoce que haya sido tomada alguna medida efectiva. **Le puede interesar: [Indígenas confirman presencia paramilitar en resguardo de Chocó](https://archivo.contagioradio.com/indigenas-confirman-presencia-paramilitar-en-resguardo-de-choco/)

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>
