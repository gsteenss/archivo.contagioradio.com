Title: Cineastas colombianos suman su apoyo a Gustavo Petro
Date: 2018-06-05 15:12
Category: Nacional
Tags: Cine Colombiano, elecciones, Petro
Slug: cineastas-apoyo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/gustavo_petro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### 05 Jun 2018 

Bajo el título "Cineastas por el cambio" un grupo de más de 100 miembros del gremio cinematográfico colombiano firmaron una carta en la que manifiestan su preocupación por la actual situación política del país y a las elecciones del próximo 17 de junio como una oportunidad única de cambio, encarnada en la figura del candidato Gustavo Petro.

En la misiva que incluye directores y directoras de cine y televisión, productores, guionistas, sonidistas, investigadores, periodistas,  y gestores culturales entre otros representantes del gremio, reflejan su necesidad de expresarse  "con preocupación, pero también con esperanza" frente a lo que será la segunda vuelta presidencial.

Adicionalmente, manifiestan que el voto por Iván Duque representa un "regreso al pasado que en parte se ha logrado superar" ´por lo que representan sus advertencias frente a los Acuerdos de Paz y su implementación, y sus propuestas que atentan contra la separación de los poderes públicos y otras de sus propuestas que  "van contra los derechos de distintos sectores sociales, políticos, de las víctimas y las minorías".

A continuación el texto completo de la carta:

**Cineastas por el cambio**

##### Mates, 5 de junio de 2018 

Nosotros que firmamos esta carta somos miembros del gremio cinematográfico colombiano. Dentro de nuestro círculo hay directoras y directores de cine y TV, productores y productoras, guionistas, directores de fotografía, sonidistas, investigadores, periodistas, gestores culturales y representantes de varios cargos más.

El cine colombiano está permeado por aquello que nos ha afectado como sociedad: el conflicto armado, el desplazamiento, la desigualdad social, por mencionar algunos temas. En los últimos años ha habido una ola de cine nuevo que ha abordado temas distintos. Esto se debe a que se venía sintiendo la necesidad de contar otro tipo de historias, pero también al hecho de que podíamos darnos el lujo de no tener que enfocarnos solamente en un cine de denuncia.

La mayoría de nosotros nos sentimos privilegiados, pues hemos tenido acceso a educación superior de calidad y hemos viajado a otros países del mundo, representando a Colombia en mercados y festivales internacionales.

Nuestra profesión nos ha permitido conocer a fondo las contradicciones sociales de nuestro país. En la coyuntura política que actualmente se vive frente a la segunda vuelta de las elecciones presidenciales de 2018, sentimos la necesidad de expresarnos con preocupación, pero también con esperanza.

Las elecciones del 17 de junio presentan una oportunidad única de cambio, una oportunidad para romper el ciclo de clientelismos, corrupción, violencia y violación a los derechos humanos que muchos de nosotros hemos podido conocer de cerca debido a nuestras historias. Este cambio solo se logra si Gustavo Petro es elegido presidente, un candidato cuyo programa enfatiza la Paz, el respeto por los derechos humanos, la lucha contra la corrupción, el cuidado del medio ambiente y los animales, un programa para las artes y la cultura.

Consideramos el voto por Iván Duque un regreso al pasado que en parte se ha logrado superar. Él ha amenazado cambiar los Acuerdos de Paz, lo que pone en riesgo la implementación de los mismos. Sus propuestas que atentan contra la separación de poderes son preocupantes y otras ideas suyas van contra los derechos de distintos sectores sociales, políticos, de las víctimas y las minorías. Entre sus propuestas se encuentra la creación del viceministerio de la "economía naranja" en el Ministerio de Cultura, propuesta económica que básicamente privatiza el arte y la cultura, convirtiéndolas en un negocio y no en un derecho para todos. El arte y la cultura son la expresión máxima de los pueblos y las sociedades, y deberían tener un tratamiento especial.

Por todo esto consideramos que un voto por Duque solo va a agudizar la desigualdad, el clientelismo y la violencia - precisamente los problemas que Colombia tanto necesita superar. Consideramos además que Gustavo Petro es el candidato que representa el cambio y una oportunidad para que Colombia progrese a varios niveles, además de ser el candidato de la Paz.

Por eso los que abajo firmamos, quienes hacemos parte del gremio cinematográfico colombiano, provenientes de diferentes regiones del país, abiertamente apoyamos su candidatura:

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[A]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Adolfo Ayala

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Alex Andrés López

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ana Cristina Monroy

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Andrés Franco

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Andrea Echeverri Jaramillo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Andrés Felipe Ardila

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Andrés Felipe Carrillo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Andrés Jiménez Suárez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Angélica Sabina Piñeros

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Adriana Villamizar Ceballos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Adriana Rojas Espitia

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ana María Ruiz Navia

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Armando Russi

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[B]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Bibata Uribe

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[C]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Camilo Mejía

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Mario Bernal

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Claudia Betancur L

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Camilo Aguilera Toro

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Esteban Valderrama

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Camilo Villamizar Plazas

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Camilo Cely

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Andrés Cabas

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Gaviria

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Rodríguez Aristizábal

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carol Ann Figueroa

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carolina Blanco

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

César Orlando Gaitán Pinilla

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

César Salazar

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

César Augusto Barrera Burgos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Cindy Hernández

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Christian Bitar Giraldo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Cristina Gallego

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Carlos Mario Bernal Acevedo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[D]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Daniel Bejarano

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Daniel Baena

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Daniel Mateo Vallejo Gutiérrez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Daniela Carolina Bernal

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

David Salomón Fontecha

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diana Marcela Giraldo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diana Wiswell

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diego Fernando Rodríguez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diego Alejandro Martinez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diego Rojas Romero

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Diego Andrés Duque López

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[E]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Edith Sierra Montaño

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Edna Juliet Sierra

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Efrén José López Hernández

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Enrico Mandirola

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ernesto Correa

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ernesto Lozano

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Exler Felipe Puerta Velasco

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[F]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Felipe Guerrero

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Franco Lolli Gómez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Felipe Moreno Salazar

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[G]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Germán Ayala

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Gerylee Polanco Uribe

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Gustavo Fernández

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[H]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Hans Dieter Fressen

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Helkin René Díaz

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Henry Melo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[I]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Iván Camilo Valenzuela

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Iván Darío Hernández Jaramillo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Iván David Gaona Morales

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Iván Guarnizo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[J]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jhonan Cardona

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Esteban Rengifo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jenny David Piedrahíta

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jerónimo Atehortúa Arteaga

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jhina Hernández Ospina

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jhonnatan Ramírez Flórez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jhon Narváez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jonas Radziunas

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

José Urbano

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jorge Mario Álvarez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Jorge Andrés Botero

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Josephine Landertinger Forero

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juana Suárez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Carlos Arias

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Carlos Sánchez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Carlos Villamizar

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Cristóbal Cobo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan David Ospina Páez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Felipe Ríos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Felipe Grisales

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Lugo Quebrada

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Pablo Caballero

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juan Sebastián Mora

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juliana Zuluaga

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Juli Angela Quintero Quijano

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[K]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Kathalina Luque

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Katheryn Guzmán

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Kia Santos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[L]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Laura Betancur Fernández

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Laura Huertas Millan

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Laura Matyas Carvajal

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Leidy Mendoza

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Leonardo Mancilla

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lili Cabrejo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Liliana Carolina Beltrán

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lina Marcela Rizo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lina María Benavides

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lina María Garavito

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lina Sampedro Cárdenas

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Liuvoff Irina Morales

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Luis Montealegre

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lucas Silva

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Lucía González

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Luna Acosta

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Laia Alba Ceballos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[M]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

María Camila Sanabria

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

María Alejandra Caicedo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Mary Alejandra Figueroa

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

María Fernanda Prada Suárez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Manuel Cortázar

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Manuel Francisco Puerta

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Manuel Ruiz Montealegre

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

María Paunks

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Marta Rodríguez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Mateo Ramírez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Mauricio Casilimas

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Melissa Saavedra Gil

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Miguel Antonio Zanguña Villalba

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Mónica Taboada Tapia

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[N]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Natalie Adorno Ortiz

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nataly Lara

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nelson Mauricio Vásquez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nestor Camilo Fonseca

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nicolás Casanova Sampayo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nicolás Correa

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Nina Díaz

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[O]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Oscar Ruiz Navia

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[P]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Paula Andrea Molina Ospina

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Paula Moreno Vergara

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Paula Murcia Restrepo

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Paula Toro

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Pedro Adrián Zuluaga

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Pilar Perdomo Munévar

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[R]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Reyson Velásquez Gutiérrez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ricardo Coral Dorado

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Ricardo Ospina

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Rodrigo Ramos Estrada

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[S]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Santiago Forero-Alarcón

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Santiago Parra Barrios

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Santiago Henao Vélez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Santiago Otálora Cruz

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Sara Fernández Martínez

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Sara Piñeros Cortés

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Samani Estrada Ramos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Sebastián Múnera

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Sergio Alfonso Fransisko Ruiz Betancourt

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Sol Laura Emi Matsuyama Hoyos

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[T]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Tatiana Marcela Londoño Riaño

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[V]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Víctor Gaviria González

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

Vlamyr Vizcaya

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

[W]{._4yxo}

</div>

<div class="_2cuy _3dgx _2vxa" style="text-align: left;">

William Jones Prada

</div>

<div class="_2cuy _3dgx _2vxa">

</div>

###### Reciba toda la información de Contagio Radio en [[su correo]
