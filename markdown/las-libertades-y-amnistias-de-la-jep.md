Title: Las libertades y amnistías de la JEP
Date: 2020-04-23 15:08
Author: AdminContagio
Category: Columnistas, Columnistas invitados, Opinion
Tags: FARC-EP, JEP, Ley de Amnistía e Indulto, Partido FARC, partido políticos de las farc, Prisioneros políticos FARC-EP
Slug: las-libertades-y-amnistias-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/colombia-JEF-jurisdiccion-especial-paz-e1553519186203.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Justice for Colombia {#foto-por-justice-for-colombia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: Camilo Fagua, Integrante de la defensa FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Colombia, en los  
albores del siglo XX con la culminación de la guerra de los mil días, surgen  
diversas herramientas jurídico-políticas destinadas a promover amnistías para  
quienes habían participado en dicha confrontación, instrumentos que hacen parte  
de una extensa tradición jurídica, que incluso data de finales del siglo IXX y  
que no solo buscaba “perdonar” todos los delitos relacionados con los  
diferentes conflictos sino que a su vez fueron elemento clave para dar  
confianza y solución a algunas causas socio políticas que dieron origen a las  
guerras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, pese al desarrollo de una gama de procesos que fueron caracterizando el “delito político” hasta elevarlo a rango Constitucional en 1991, varios sectores políticos han impulsado la desnaturalización de esta figura; basta mirar por el retrovisor, para ver, como la legislación colombiana con sus reformas penales fue creando nuevos delitos como “el terrorismo”, inexistente en legislaciones internacionales, imputaciones de delitos comunes y la imposición de penas interminables para quienes se levantaron en armas contra el Estado o contra quienes ejercen la oposición política en lo que de forma peligrosa, jueces y fiscales, denominaron la “rebelión sin armas”, castrando toda posibilidad de disentir, o reiterando la teoría del “enemigo interno”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la firma del Acuerdo de La Habana, el delito político retoma su esencia original, esta vez amparado en todos los desarrollos del Derecho Internacional Humanitario, y no solo busca dar confianza y seguridad jurídica a miles de firmantes del tratado de Paz, sino que a su vez fija la senda por la que pueden transitar organizaciones rebeldes que suscriban nuevos acuerdos en pro de materializar la Paz en nuestro país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Por supuesto restablece plenamente el derecho a la protesta, a disentir, a luchar por la vida, el medio ambiente, la salud, la educación, la vivienda y la participación política. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el pasado 30 de diciembre de 2016, el Congreso colombiano aprobó la ley de amnistía e indulto, **ley 1820** que cobija a quienes fueron integrantes de las **FARC-EP;** aunque su aplicación debería ser inmediata, funcionarios judiciales como el ex Fiscal Martínez, se atravesaron como palo en la rueda para impedir que cientos de ex guerrilleros presos salieran de las cárceles e iniciaran su proceso de reincorporación a la vida civil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Innumerables son las acciones que se han realizado para lograr que  
todas aquellas personas privadas de la libertad vinculadas con las FARC-EP por  
delitos ocurridos en el contexto del conflicto pasaran de la justicia ordinaria  
a su juez natural representado en la Jurisdicción Especial para la Paz, las  
cifras son claras y contundentes, la JEP tiene suscritas 9737 actas de ex  
guerrilleros comprometidos con la justicia, la verdad  y la reparación, comparadas con apenas 2680  
actas de sometimiento de integrantes del Fuerza Pública, eso sin contar la  
participación efectiva en el esclarecimiento de la verdad en los 5 casos que  
vinculan a las FARC de los 7 casos que ha abierto la Sala de Reconocimiento de  
la JEP.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin temor a equívocos los ex guerrilleros/as de las FARC representan en la actualidad más del 80% de la actividad judicial, de una jurisdicción, que tiene por objetivo juzgar a  todas aquellas personas, que sin distinción, participaron directa o indirectamente de la confrontación. Y en ese sentido, en honor a la verdad, se debe reconocer que los antiguos guerrilleros han respondido de manera ejemplar ante la JEP.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A la fecha más de 200 firmantes han concurrido a todas las audiencias citadas por la magistratura, han ampliado versiones, ha respondido con verdad frente a las víctimas dando cumplimiento ejemplar a lo acordado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hoy la seguridad jurídica está en el terreno de la JEP, particularmente en la Sala de Amnistía, pues es precisamente allí donde de manera prioritaria y ante la actual situación, debe agilizarle la salida de la cárcel de casi 400 personas reconocidas como ex guerrilleros de las FARC-EP.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Resulta inconcebible que luego de 3 años de entrada en vigencia del acto legislativo 01, esta Sala solo haya concedido beneficios para un poco más de 180 privados de la libertad y apenas 210 amnistías, mientras  ha negado, en su mayoría sin el debido análisis a la luz de la Justicia Transicional, más de 1200 solicitudes de aplicación de beneficios de la ley 1820.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Acaso personas  
como  Rodolfo Julio Quintero, quien se  
encuentra privado en el patio 6 de la Cárcel de Cúcuta y quien sufre Hepatitis  
B crónica, o Mario Ancizar Montero privado de la libertad en el Bloque 7 de la  
Cárcel Picaleña de Ibagué quien se encuentra enfermo y los demás firmantes y  
beneficiarios del Acuerdo merecen seguir siendo víctimas del hacinamiento  
carcelario y estar expuestos al riesgo de ser un caso más de la actual pandemia  
por la no aplicación del Acuerdo de Paz?

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es el momento de hacer un alto en el camino:

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una Jurisdicción que pretende ser tribunal de cierre de un conflicto de más de 5 décadas tiene la posibilidad de poner en el debate nacional la eficacia del sistema carcelario sumido en el hacinamiento y la violación sistemática de derechos fundamentales, máxime con la reciente expedición de normas inocuas y restrictivas como el reciente decreto 546.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es la hora de actuar en consecuencia con los pilares que deben conducir a la Paz y a la reconciliación de Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Le puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
