Title: La familia que Jesús quería
Date: 2016-10-26 05:00
Category: Abilio, Opinion
Tags: Teologia
Slug: la-familia-que-jesus-queria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/av120510_cah0027.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:domingocosenza 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) -[@[Abiliopena]

###### 26 Oct 2016 

[Es frecuente el uso del  sentimiento cristiano  para  imponer un  determinado  tipo de familia. Conviene ir a los Evangelios  a indagar por la familia que Jesús quería.]

[Lo hacemos de la mano del reconocido **biblista alemán Gerhard Lohkfink** quien en su obra traducida como "[La iglesia que Jesús quería](https://adelmovd.files.wordpress.com/2015/05/lohfink-gerhard-la-iglesia-que-jesus-queria.pdf)", publicada en 1986, muestra los rasgos de la familia que da cuenta de la voluntad de Dios en la historia. (1 ).]

### **Jesús relativiza el clan, los padres,  las madres, los hijos, los hermanos,  la hacienda** 

[«Yo os aseguro: nadie que haya dejado casa, hermanos, hermanas, madre, padre, hijos o hacienda da por mí o por el evangelio quedará sin recibir el ciento por uno: ahora al presente, casas, hermanos, hermanas, madres, hijos y hacienda, con persecuciones; y, en el tiempo venidero, vida eterna» (Mc 10,29s).]

[Aquí hermanas y hermanos significa la  pertenencia a un clan por parentesco de sangre, al que se rinde cuentas y del que se recibe protección. Padres y madres se refiere al  muy antiguo modelo de familia patriarcal considerado como sagrado. Los hijos, los hombres, son la alegría suprema del oriental de la época y a su vez su seguro de vejez. La tierra,  era la herencia que afirmaba a los hombres como israelitas.]

[Ese abandono literal de la familia o la transformación de los valores  que la sustentan, configuran la irrupción del Reino de Dios y la conformación de una nueva familia, en la que hay muchos hermanos, hijos, hermanos, hermanas,  madres pero no padres en el rol tradicional. El abandono simboliza el fin  del orden patriarcal preestablecido y el surgimiento de  una comunidad  familia sobre la base de la justicia presente en el anuncio del Reino y que está ilustrada en la comunidad de la mesa,  comensalía abierta,  donde  se comparte el pan, muy presente en el cristianismo primitivo.]

### **Jesús contrapuso de modo radical la familia natural  a la comunidad familiar** 

[El llamó discípulos y discípulas para que lo siguieran en el trabajo por el reinado de Dios que se concretizaba en la vida comunitaria. Está en contravía de los roles, prácticas y el tipo de relación que se vivían en la familia de la época. Así lo recogen los evangelios de modo tremendamente chocante:]

[“*El que ama a su padre  más que a mí, no es digno de mí; el que ama a su hijo a su hija, más que a mí, no es digno de mí*” (Mat 10, 37). Y en Lucas es mucho más fuerte como se puede leer en 14, 26.]

[Se trata, dice Lohfink de "entrega completa al Evangelio del Reino de Dios, conversión radical a un nuevo orden de vida, reunión en una comunidad de hermanos y hermanas";  de constituir una comunidad tipo para mostrar a la sociedad los valores experimentados por Jesús.]

### **Jesús constituye otra familia con quienes cumplen la voluntad de Dios** 

[Esta familia nueva se basa en los valores del Reino y no en los del estrecho vínculo de sangre. Tampoco en los roles patriarcales preestablecidos por el clan, al que se tiene que rendir cuentas. Por eso los Evangelios  muestran  algunas desavenencias de Jesús con su familia de sangre, por estar provocando escándalo público y  por creer  ésta que se había enloquecido:]

[“*Vuelve a su casa, se aglomera otra vez la muchedumbre de modo que no podían comer. Se enteraron sus parientes y fueron a hacerse cargo de él pues decían: 'está fuera de sí'*.” (Mc 3, 20)]

[“Llega su madre y sus hermanos, quedándose fuera, le envían a llamar. Estaba mucha gente sentada a su alrededor. Le dicen: '¡oye tu madre,  tus hermanos y  tus hermanas  están fuera y te buscan.' Él les responde: 'quién es mi madre y mis hermanos?'. Y mirando  en torno a los que estaban sentados en corro, a su alrededor, dice: 'Estos son mi madre y mis hermanos. Quien cumpla la voluntad de Dios, éste es mi hermano, y mi hermana y mi madre' (Mc 31-35).]

### **Por el anuncio de Jesús hay tensión en las familias** 

[El anuncio del Reino, la instrucción ética de Jesús,  está  consignada de modo sintético en el “Sermón del Monte”: La poción de Dios por los pobres, los mansos, los que lloran. La invitación de Jesús a ser  misericordiosos, a ser limpios de corazón, a trabajar por la paz y  a trabajar por la justicia,  así vengan persecuciones (cfr Mat 5, 1-12).]

[Ese anuncio y el riesgo que  significa asumirlo, crea fracturas al interior de la familia, pero también de los pueblos, en este caso,   del que Jesús y sus discípulos hacían parte:]

[“Porque desde ahora  habrá cinco en una casa y estarán divididos; tres contra dos, y dos contra tres; estarán divididos el padre contra el hijo y el hijo contra el padre; la madre contra la hija y la hija contra la madre; la suegra contra la nuera y la nuera contra la suegra” ( Lc  12, 52-53).]

[Dice Lohfink que seguramente se produjeron cambios positivos en las familias, donde  Jesús fue acogido con sus mensajeros y hubo apertura, entran en relación con otras personas, “dejan de dar vueltas al rededor de ellas mismas”. Pero también pudo ocurrir algo distinto, que las familias se rompen y quienes  quisieran seguir a Jesús, se fueran con él, convirtiéndose en “signo de contradicción”.]

### **El fin de los padres** 

[Jesús, en los Evangelios, promete madres, hermanos, hermanas e hijos, pero no padres. Pareciera un descuido en la secuencia, pero no es así. Los evangelios son cuidadosos en mostrar que  en la promesa de una nueva familia no se incluye a los padres. La razón está en el rol que jugaban  en el clan y en la actividad religiosa de la época. A los padres había que servirles, lavarles los pies. Ellos decidían el futuro de los miembros de la familia. Ese papel en las instancias religiosas de la épocas lo desempeñaba el maestro cuyos discípulos eran como hijos que estaban para servir al líder, a quien llamaban Rabbi y Padre, que eran títulos honoríficos.]

[“Ni llamareis a nadie 'Padre vuestro en la tierra' porque uno solo es vuestro padre: el del cielo” (Mat 23, 9).]

[Jesús en contra vía de la usanza común,  prohibía a sus discípulos  los títulos honoríficos. En la última cena prohibió que le lavaran los pies y  él mismo   se los lava; estuvo en medio de ellos como el que sirve. El mayor en  su  ética es el que sirve.]

[Así desaparecen los padres como  título, aquellos que ejercen el poder y el dominio. “Ya no hay sitio para la soberanía patriarcal en la nueva familia. Sólo caben la maternidad, la hermandad y la filiación ante Dios Padre” concluye Lohfink.]

[Tenemos entonces a Jesús, de acuerdo con los testimonios de los Evangelios,  en evidente conflicto con la familia patriarcal cerrada en sí misma, de la época. El la reconfigura invitándola a vivir con base en la ética del Reino, comprometida con los más débiles, abierta a todas y todos, de mesa común, ejemplo de justicia, de construcción de paz y dispuesta a  las persecuciones.  Sin los  honores que se le rinden a algunos de sus miembros, en su caso, a los padres. Promete, a cambio  más madres, hermanas, hermanos, más tierras, si se trabaja por los valores que anuncia, donde quepan todas y todos, en especial los excluidos.]

-   ###### 1. https://adelmovd.files.wordpress.com/2015/05/lohfink-gerhard-la-iglesia-que-jesus-queria.pdf


