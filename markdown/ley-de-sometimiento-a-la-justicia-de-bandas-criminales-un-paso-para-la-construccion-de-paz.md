Title: La ley de Sometimiento de Bandas Criminales no implica darles reconocimiento político: Santos
Date: 2018-07-10 16:24
Category: Nacional, Paz
Tags: Bandas crímin, Fiscalía, Ley de sometimiento a la justicia, lideres sociales
Slug: ley-de-sometimiento-a-la-justicia-de-bandas-criminales-un-paso-para-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/BACRIM_3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canal Trópical] 

###### [10 Jul 2018] 

La nueva ley para el sometimiento a la justicia de las bandas criminales, Ley 1908 de 2018, que busca cumplir con los Acuerdos de La Habana, tendrá múltiples beneficios para las estructuras ilegales que decidan acogerse a ella en grupo. Algunos tienen que ver con rebajas de hasta el 50% de las condenas o pagar las mismas en granjas agrícolas. Sin embargo, el presidente Santos afirmó que este mecanismo no implica darles reconocimiento político a las Bandas Criminales ni tratamientos del tipo de Justicia Transicional.

La norma sancionada por el presidente Juan Manuel Santos, permite que los integrantes de grupos al margen de la ley tipificados como bandas criminales, accedan a rebajas de penas en la Justicia de acuerdo con los aportes que realicen a la misma referentes a sus acciones en el marco del conflicto.

### **El objetivo de la Ley de sometimiento a la justicia de bandas criminales ** 

Santos señaló que además de cumplir uno de los grandes objetivos del Acuerdo de paz con el desmonte de este tipo de estructuras, se evitará caer en un ciclo de violencia de postconflicto como en países de Centroamérica. En donde estas bandas incrementaron su accionar y control sobre la población, con la dejación de armas de las guerrillas.

En ese sentido, el presidente afirmó que la presente ley "no implica darles reconocimiento político a las Bandas Criminales ni tratamientos del tipo de Justicia Transicional", es decir que tendrán acceso a otros beneficios de la **justicia como rebajas de pena, pero no existirá ni una justicia especial ni una ley de amnistía**.

### **Las deficiencia de la ley** 

De acuerdo con el analista y director de Indepaz, Camilo González Posso, es un “buen mensaje” para la sociedad que debe ir acompañado de un paquete más amplio de acciones desde el Estado y la isntitucionalidad para superar las formas de violencia que golpean tanto a líderes sociales como a comunidades en el país.

Sin embargo, queda la duda sobre qué mecanismos garantizarían el avance en hallar a los responsables intelectuales y sus intereses tras los asesinatos a líderes sociales. González Posso, señaló que esta herramienta jurídica **“no se ocupa de establecer mecanismos nuevos de investigación hacia los determinadores y autores intelectuales”** de los asesinatos a líderes sociales.

Asimismo expresó que, aunque se podrán obtener declaraciones sobre complicidad de testaferros y colaboradores, no hay elementos vinculantes que permitan profundizar en el esclarecimiento de responsabilidades. (Le puede interesar: ["17 líderes sociales han sido asesinados este año en Antioquia"](https://archivo.contagioradio.com/17-lideres-sociales-han-sido-asesinados-este-ano-en-antioquia/))

Además, aseguró que, si bien es cierto que esta ley tiene la intención de continuar en la ruta de construcción de paz acogiendo a quienes se desmovilicen de bandas criminales, hace falta ver la capacidad disuasiva que la misma tenga, razón por la cual **se necesitan más medidas de acompañamiento desde la institucionalidad a este proceso. **

### **Los asesinatos a líderes sociales son políticos** 

Con respecto a las denuncias que viene generando el movimiento Colombia Humana sobre una persecución a sus integrantes, González manifestó que es muy prematuro asegurar que se está ante un escenario de exterminio, pero que en efecto los asesinatos a líderes sociales e integrantes de campañas electorales tienen un sentir político que no se puede invisibilizar.

“Hay una postura política en contra de los Acuerdos de Paz, **hay un fomento de una metodología del terror** para imponer los intereses sobre tierras, recursos naturales, sobre oro, rutas de narcotráfico, que esta articulado a dinámicas del poder” afirmó González Posso. (Le puede interesar: "[¿Hay sistematicidad en el asesinato a líderes sociales en el país?](https://archivo.contagioradio.com/hay-sistematicidad-en-asesinatos-a-lideres-sociales-en-colombia/)")

<iframe id="audio_26990775" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26990775_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
