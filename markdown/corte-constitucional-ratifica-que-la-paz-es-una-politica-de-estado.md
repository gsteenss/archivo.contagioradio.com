Title: Corte Constitucional ratifica que la paz es una política de Estado
Date: 2016-12-14 12:19
Category: Nacional, Política
Slug: corte-constitucional-ratifica-que-la-paz-es-una-politica-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [14 Dic 2016] 

Tras la decisión de la Corte Constitucional en torno al acto legislativo por la paz y la aprobación del Fast Track, así como la validez a la refrendación del acuerdo de paz con las FARC por parte del congreso de la república, la **Corte confirmó que la paz es una política de Estado. Así lo señaló el representante a la Cámara Alirio Uribe y lo respaldó el Doctor Gustavo Gallón**, director de la Comisión Colombiana de Juristas.

Gustavo Gallón por su parte, aseguró que la **Corte Constitucional fijó unas líneas de interpretación jurisprudenciales** frente a todo lo referente al acuerdo de paz y aunque no se pronunció sobre la refrendación popular, dejó en manos del congreso la posibilidad de sustanciar ese tema, es decir que se estrechan los vínculos necesarios entre las ramas del poder para asegurar la implementación y la construcción de la paz a partir del acuerdo. ([Lea también Arranca el histórico capítulo de la implementación](https://archivo.contagioradio.com/con-fast-track-arranca-la-historia-de-la-implementacion-del-acuerdo-de-paz/))

En ese sentido señaló que la decisión de la Corte definió las pautas de lo que sería una argumentación al analizar la constitucionalidad el artículo 5 del acto legislativo. Una de ellas es que el congreso es el órgano de mayor representación popular, es decir, que el congreso si es competente para **refrendar el acuerdo de paz con las FARC-EP.**

Por otra parte la Corte subrayó que la refrendación no es un acto simple que se limita a un momento sino que es un acto de tracto sucesivo. Es decir que no se pueden entender la refrendación como un mero acto de votación sino que tiene **"tracto sucesivo", son una serie de acciones las que le dan la legitimidad y por ende la legalidad.**

Para el legislador Alirio Uribe, la aprobación del mecanismo abreviado para la implementación de los acuerdos de paz, contribuye en que se dé un trámite rápido y expedito a los asuntos urgentes del acuerdo como la amnistía y permite que haya un control estricto en el respeto a la bilateralidad del acuerdo alcanzado. (Le puede interesar [Las tres medidas urgentes para la implementación luego de aprobación del Fast Track](https://archivo.contagioradio.com/las-tres-medidas-urgentes-para-implementar-el-acuerdo-de-paz-luego-del-fast-track/))

Frente a la comisión de segumiento a la implementación de los acuerdos que estará conformada por 3 integrantes de las FARC y 3 del gobierno, Uribe aseguró que varias comisiones han venido funcionando independientemente de estar instaladas oficialmente, pero que seguramente **cuando se apruebe la ley de amnistía**, que se espera para finales de este mes, se podrán instalar formalmente para que lo que se ha definido tenga la legalidad necesaria.

<iframe id="audio_14984699" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14984699_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_14984798" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14984798_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
