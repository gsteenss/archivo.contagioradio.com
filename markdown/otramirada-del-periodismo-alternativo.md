Title: OtraMirada del periodismo alternativo.
Date: 2020-06-28 11:17
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: medios de comunicación alternativos, Medios independientes, periodismo, Periodismo tradicional
Slug: otramirada-del-periodismo-alternativo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/periodistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Periodismo alternativo/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El periodismo y los medios de comunicación alternativos e independientes, aun enfrentándose a los grandes medios tradicionales y a los abusos de poder, han ganado reconocimiento al luchar diariamente por narrar la realidad desde otras perspectivas, con nuevas propuestas y nuevos esquemas de información y análisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, informar y reportar es aún más complicado desde los territorios, pues las herramientas y el acceso son más escasos y deben lidiar con los problemas de seguridad (paramilitares, guerrillas, ejército) actuales. (Si quiere saber sobre nuestros programas anteriores: [la cadena perpetua no reduce la criminalidad ni](https://archivo.contagioradio.com/la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas/) [repara los daños a](https://archivo.contagioradio.com/la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas/) [las](https://archivo.contagioradio.com/la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas/) [victimas](https://archivo.contagioradio.com/la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, para conocer cómo se está impulsando este ejercicio desde diferentes territorios, tuvimos invitados que además, tratan de contar la historia desde cada una de sus realidades. Nelly Kuiru, coordinadora de la Escuela Indígena de Comunicación de la Macro Amazonía Ka+ Jana Uai y Hernando González comunicador rural OPDS Montes de María. Desde la capital, Natalia Arenas, editora de la revista Cerosetenta y desde Argentina, Gabriel Chávez de la Garganta Poderosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este espacio permitió responder a preguntas sobre qué significaba hacer periodismo alternativo e independiente y cómo es narrar la realidad desde los territorios, sobre todo, cuando los medios tradicionales no lo hacen adecuadamente o de acuerdo a los intereses de los territorios. De igual manera, se dio un análisis con respecto a qué hace falta para narrar la historia desde una visión escondida y qué tanto sigue creyendo la sociedad en estos medios empresariales que en muchas ocasiones responden a ciertos intereses.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por último, es un espacio que invita a conocer y valorar el trabajo de periodistas, editores, instructores y directores alternativos e independientes que hacen todo lo posible por llegar a cada persona, aportar a la construcción de una sociedad más justa y transparente, y cambiar esa realidad que los medios tradicionales han decidido contar. (Si desea escuchar el programa del 24 de junio :<https://www.facebook.com/contagioradio/videos/473187150189431/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/293233818721405/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/293233818721405/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
