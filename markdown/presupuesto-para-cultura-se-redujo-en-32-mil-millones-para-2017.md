Title: Presupuesto para Cultura se redujo en 32 mil millones para 2017
Date: 2017-03-27 17:12
Category: Cultura, Entrevistas
Tags: 27M, Cultura
Slug: presupuesto-para-cultura-se-redujo-en-32-mil-millones-para-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cultura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Mar 2017]

En el día mundial del teatro, miles de artistas, agrupados bajo el movimiento 27M, se dieron cita en las diferentes plazas del país, para exigirle al gobierno que amplíe el presupuesto actual para la cultura, que es del **0,16%, y que no se consoliden políticas públicas como la del decreto 092 de 2017**, que de acuerdo con los artistas lo que provoca es una mayor precariedad para continuar con su labor de gestar cultura en el país, en el escenario histórico de la paz.

Durante una audiencia pública convocada por el senador Iván Cepeda y los representantes Víctor Correa y Ángela María Robledo, artistas denunciaron que la **cultura le aporta al país el 3% del producto interno bruto**, mientras, que el Estado no realiza inversiones lo suficientemente fuertes que logren generar en Colombia herramientas para fortalecer la producción cultural. Le puede interesar: ["Nace el Movimiento 27 de Marzo por la dignidad de los artistas en Colombia"](https://archivo.contagioradio.com/teatrocolombiamovilizacion/)

La semana pasada el Gobierno presento un proyecto de ley de adición por **6.2 billones de pesos, sin embargo, los artistas señalaron que ven con preocupación que la cultura no se haya escogido a como uno de los beneficiarios de este aumento,** cuando se habla de la transformación de un país para un escenario de paz.

### **La Economía Naranja contra la diversidad popular** 

A su vez, los artistas explicaron por qué la Ley de “Economía Naranja”, **proyecto de ley presentado por el Centro Democrático**, que ya va en su cuarto debate, podría ser aún más nocivo para la difícil producción cultural que realizan diferentes colectivos y organizaciones en el país:

“Aclaramos que no estamos en contra de las industrias culturales, ni de que existan incentivos para las empresas que invierten en cultura, pero creemos que tal y como está planeado en ese proyecto de ley, **se niega la importancia y el valor de la diversidad cultural y del arte que deben ser protegidos por el estado**” afirmaron los artistas durante la lectura de lo que denominaron el “peticionario”. Le puede interesar: ["Artesanías indígenas en Ciudad Bolívar, salvaguardan la cultura ancestral"](https://archivo.contagioradio.com/mujeres-indigenas-tejen/)

De igual forma, representantes de la cultura de otras regiones como el poeta Fernando Rendón, de Medellín, Gladis Quintero, gestora cultural en Quindío, entre otros, expusieron la necesidad de crear un presupuesto **“urgente”, para la cultura popular del país que se construye a partir de las particularidades del territorio** y que de acuerdo con el actor Julián Días, es casi inexistente.

### **La inversión en educación para la cultura** 

Otra de las preocupaciones de los artistas se encuentra en la falta de financiación de las escuelas y facultades públicas del país en donde se enseñan artes como la danza, pintura, la actuación, el cine, artes plásticas, entre otras “Hay un abandono sistemático que nos hace pensar que esto es un suicidio artístico, estudiamos 10 años, para salir y que no haya nada, en el 2016 el presupuesto fue de **\$132 mil millones a \$100 mil millones en el 2017**”  afirmó, Angélica Riaño, presidenta del Consejo Cultural Distrital de Bogotá.

Frente a estas problemáticas, los artistas exigen que un colectivo de artistas, en conjunto con expertos en legislación, **elaboren el plan Nacional de Cultura a 15 años**, que se revise la actual ley general de Cultura y que se establezcan compromisos de consenso con los artistas a la hora de la presentación de estos proyectos, para no debilitarlos aún más. Además evidencia que es necesario que se destine **al menos el 2% del presupuesto general para la Cultura.** Le puede interesar: ["Ser Pilo paga ha gastado más de 350 mil millones del presupuesto de educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

Estas propuestas serán evaluadas el día de mañana en una reunión que se dará entre la Ministra de Cultura Mariana Garcés y el comité delegado de 27M, sin embargo, los artistas expresaron que desde **hoy las calles serán el escenario para revivir y construir una cultura de paz.**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154305215945812%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
