Title: Mapiripán y un reto histórico: superar la tragedia y construir la paz
Date: 2016-02-23 11:46
Category: Opinion
Tags: Masacre de Mapiripan, Paramilitarismo Meta, Zidres
Slug: mapiripan-y-un-reto-historico-superar-la-tragedia-y-construir-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/mapiripan-e1456245733494.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CAJAR 

###### [23 Feb 2016] 

#### **Por: Colectivo de Abogados José Alvear Restrepo** 

Cinco días fueron suficientes para que paramilitares provenientes del noreste antioqueño, comandados por Carlos Castaño, y en complicidad con las Fuerzas Militares, perpetrarán una de las peores masacres en Colombia: la de Mapiripán.

Miembros de las Autodefensas Unidas de Colombia llegaron hasta del departamento del Meta con la cooperación del Ejército Colombiano, quienes colaboraron con el traslado y aterrizaje desde San José del Guaviare, suministrándoles uniformes y armas de corto y largo alcance. Rodeando el municipio por vía terrestre y fluvial, las AUC impidieron la libre circulación de los habitantes; los secuestraron, torturaron, descuartizaron y degollaron, tirando sus cuerpos al río Guaviare con el fin de eliminar cualquier tipo de evidencia.

El día de la independencia de Colombia, data como el día en que cesó la muerte y se instauró el terror casi permanente sobre este municipio. Casi 20 años después, el paramilitarismo sigue vigente, el exilio de sus habitantes permanece, las tierras despojadas no han retornado a los legítimos dueños, y de hecho los negocios con estas tierras, parecen querer prosperar bajo la legitimidad del Estado.

Ante la tragedia acaecida en este municipio, y habiendo tantas otras tierras en el país para desarrollar proyectos agroindustriales, para quien se entere del proyecto de Poligrow en Mapiripán, le surge irremediablemente una pregunta: ¿cómo fue posible que un grupo de inversionistas, al parecer de origen italiano y español, adquirieran inicialmente 3 predios (Macondo I, Macondo II y Macondo III) para cultivar palma aceitera? Así efectivamente lo hizo esta empresa, la cual desde 2008 aproximadamente, hace presencia en la zona y desde entonces ha adquirido y pretende adquirir extensiones cada vez mayores de tierra. Los señalamientos sobre esta empresa recaen desde la violación de normas en materia de tierras, hasta de conformación de grupos paramilitares.

Comencemos por lo primero. Las adquisiciones de los tres Macondos, presuntamente se hicieron violando la Ley agraria vigente (Ley 160 de 1994), pues los tres predios fueron adjudicados como baldíos, y los tres englobados superan el área de la Unidad Agrícola Familiar de la zona (de 1360 a 1840 hectáreas, de acuerdo con la resolución 041 de 1996 del extinto INCORA), lo cual haría nulas las transacciones a la luz del artículo 72 de la Ley 160 de 1994. Así lo revelan denuncias desde el Congreso de la República, que han señalado a la compañía desde 2011, denuncias que al igual que otros casos (Cargill, RioPaila Castilla, Manuelita S.A., etc.), según las autoridades competentes, en la actualidad están en las instancias de investigación, y no presentan resoluciones de fondo.

No obstante, ante denuncias como Poligrow y otras empresas y personas acumuladoras de predios con antecedentes de baldíos, recogidas en distintos informes de la Contraloría General de la República, la respuesta estatal ha sido nula. Todo lo contrario, la política pública parece seguir la tradición que hace carrera desde el nacimiento de la república, validando las usurpaciones de tierras.

Muestra de esto es la recién sancionada Ley 1776 de 2016, por medio de la cual se crean las ZIDRES (Zonas de Interés de Desarrollo Rural, Económico y Social), mecanismo que bien podría utilizar Poligrow para validar las compras de tierras realizadas, y de hecho ampliar su proyecto de palma por todo Mapiripán. Con la Ley 1776 de 2016, estos predios pasarían de despojo, abandono estatal previo, a convertirse en grandes proyectos agroindustriales, donde los campesinos no serían dueños de la tierra, sino siervos en alianzas de halcones con palomas.

La tierra usurpada bajo medios violentos, o que fue vendida bajo la presión del conflicto armado, debe ser objeto de restitución. Algo deberían hacer las fuerzas militares y el ministerio de defensa para resarcir en parte su participación en la tragedia de este municipio: generar condiciones de seguridad, suprimir el paramilitarismo y por tanto macro y microfocalizar la jurisdicción de este municipio con el fin de garantizar el derecho de las víctimas a la restitución de sus predios y posterior retorno. Si esto no se da, es difícil superar este capítulo triste de la historia con un final justo.

Por todo lo acaecido en este territorio, en el que conviven campesinos e indígenas desde mucho tiempo atrás, la idea de convertirlo en una experiencia democrática y alternativa de paz y no es una ZIDRE (o cualquier modelo equivalente), es una apuesta por la construcción exitosa de paz en una sociedad atravesada por el conflicto.
