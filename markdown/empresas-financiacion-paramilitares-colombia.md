Title: CIDH realiza audiencia sobre Jurisdicción Especial de Paz y "terceros en la guerra"
Date: 2017-07-06 10:58
Category: DDHH, Nacional
Tags: empresas, paramilitares
Slug: empresas-financiacion-paramilitares-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/financiacion-de-la-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Jul 2017] 

En el marco del periodo 163 de sesiones de la CIDH se abrirá el espacio para una audiencia sobre la Jurisdicción Especial de Paz y las restricciones que podría sufrir, puesto que habría intensiones de limitar la responsabilidad de terceros en el conflicto, es decir, empresas que se han beneficiado de la guerra, que la han financiado o que han participado de manera directa.

Sin embargo esas conductas delictivas no entrarían todas  en la JEP dado que una de las condiciones para que se comparezca ante ese tribunal es que se debe comprobar la participación "determinante".

Según las organizaciones, entre las empresas mencionadas y que tendrían posibles vínculos con los paramilitares se encuentran **Drummond, Chiquita Brands, Postobón, Ecopetrol, Termotasajero, la Federación Nacional de Ganaderos** y comerciantes, bananeros y ganaderos de Urabá, Agromar y Poligrow. ([Le puede interesar: "La macabra alianza entre los paramilitares y las empresas bananeras"](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/))

Además, denuncian que **se habrían compulsado copias de por lo menos 15291 expedientes desde los estrados de la ley 975** para que la justicia ordinaria investigue la participación de terceros, civiles en el conflicto, muchos de los cuales hacen parte del sector empresarial del país. Sin embargo las organizaciones han constatado que no hay una investigación eficaz y diligente por parte de las instituciones del Estado como la Fiscalía.

Esta discusión es calve en este momento, dado que tanto el gobierno como el congreso y la Corte Constitucional adelantan sendos debates en torno a la reglamentación del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, que incluye la Jurisdicción Especial de Paz y sus alcances. [(Le puede interesar: "20 empresas deberán devolver 53.821 hectáreas de tierras despojadas"](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/))

### **Las empresas cuestionadas y que podrían hacer parte de los "terceros en la guerra"** 

Chiquita Brands, una de las empresas, habría generado más de 100 pagos a las AUC por valores superiores a los **1.7 millones de dólares, los cuales contribuyeron en el accionar de estos grupos causando cerca de 3700 asesinatos entre 2002 y 2004**. Estos aportes también sirvieron para la rentabilidad de la empresa que reportó ganancias superiores a los 2600 millones de pesos, siendo la más rentable de la región. ([Le puede interesar: "La multinacional Chiquita Brands podría asumir juicio en Estados Unidos")](https://archivo.contagioradio.com/la-multinacional-chiquita-brands-podria-afrontar-juicio-en-estados-unidos/)

Existen empresas que se habrían beneficiado de operaciones paramilitares, o que sus operaciones coinciden con lugares de actuación paramilitar como es el caso de la palmera italiana Poligrow  en Mapiripán y las de infraestructura en Buenaventura, lugares en los que se han cometido crímenes de Lesa Humanidad.

Funcionarios de dichas empresas sabrían de estas actuaciones, lo que implicaría un grado de responsabilidad, sin embargo, dicha responsabilidad quedaría oculta dado que Cambio Radical, en el marco de la discusión del Acto Legislativo 1820 haría que estas no comparezcan si son denunciadas ante la JEP.

En otros casos, por ejemplo Multifruits o Asoprobeca, los funcionarios habrían operado de manera directa con personas de las estructuras paramilitares y civiles nacionales e internacionales en proyectos de agronegocios en la región del Bajo Atrato.

Banacol y Agromar son dos empresas que tambirén serían beneficiarias de las operaciones paramilitares e ilegales para el control de los territorios en la región del Bajo Atrato Chocoano, en el cual habrían **sido beneficiarias de operaciones de despojo de tierras con extensiones de más de 100.000 hectárea**s, que habrían sido alquiladas por más de 50 años y de manera fraudulenta a consejos comunitarios que habrían sido cooptados o sobornados.

Otras de las empresas cuestionadas serían Cartón Colombia en Trujillo, Valle, dado que sus operaciones coinciden en lugares como "La Sonora" en donde se cometieron crímenes como desapariciones forzadas  u otros, incluso, recientemente se ha hablado de operaciones paramilitares de "Los Rastrojos".

Las organizaciones insisten en que la Ju**risdicción Especial de Paz sería un escenario ideal para que las víctimas tuvieran acceso a la verdad, la justicia y la reparación integral**, si se resuelven las diferencias establecidas luego de la renegociación del acuerdo de paz de la Habana y que dejaría sin efectos a la JEP para juzgar también a este tipo de empresas y empresarios.

<iframe id="ls_embed_1499365582" src="https://livestream.com/accounts/15201642/events/7572774/videos/159400867/player?width=640&amp;height=360&amp;enableInfo=true&amp;defaultDrawer=&amp;autoPlay=true&amp;mute=false" width="640" height="360" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"> </iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
