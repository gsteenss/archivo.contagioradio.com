Title: Simulación de Saúl Cruz fue un ataque a la libertad de prensa
Date: 2017-06-07 13:17
Category: DDHH, Nacional
Tags: FLIP, Libertad de Prensa, Noticias Uno, Senado
Slug: simulacion-de-saul-cruz-viola-obligaciones-de-libertad-de-prensa-del-estado-fllip
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/saulcruz-e1496855114235.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol Radio] 

###### [07 jun 2017] 

En una votación en el Senado, donde por petición del senador Jorge Robledo, se discutía la renuncia del subsecretario Saúl Cruz por los hechos protagonizados con el equipo de prensa de Noticias Uno, **sólo 12 senadores se atrevieron a pedir la renuncia del funcionario a pesar de haber 85 senadores registrados.**

**Esto demostró el apoyo que tiene el subsecretario en el Congreso y el poderío que tiene en el recinto.** Cabe resaltar que Cruz es cuota política del ex procurador y precandidato a la presidencia Alejandro Ordoñez por lo que su credibilidad no es puesta en duda en el Senado.

Entre los senadores que votaron a favor de la renuncia de Cruz estuvieron Claudia López, Jorge Iván Ospina, Jorge Prieto (Alianza Verde), Jorge Enrique Robledo, Jesús Alberto Castilla (Polo Democrático), Marco Avirama (Alianza Social Independiente), Armando Benedetti (Partido de La U), Javier Álvarez, Guillermo Santos, Guillermo García, Sofía Gaviria y  Juan Manuel Galán (Partido Liberal).

Los grandes ausentes en la votación fueron los senadores del Centro Democrático, quienes en su momento arremetieron contra los periodistas de Noticias Uno y llegaron **a solicitar prisión para ellos**.

El senador José Obdulio Gaviria, por ejemplo, afirmó que “**los senadores hemos tenido, casi todos, las más graves agresiones por parte del noticiero Noticias Uno**, lo que pasa es que la agresión hasta ahora era moral, ahora saltan a la agresión física, a un dignatario de esta corporación”. Le puede interesar:["Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar"](https://archivo.contagioradio.com/alvaro-uribe-estigmatiza-a-periodismo-de-noticias-uno/)

Saúl Cruz es un funcionario que según la ley, tiene funciones que se limitan a asistir al secretario general y reemplazarlo en caso de su ausencia. Sin embargo, en el Congreso cualquier documento pasa por su revisión y **lleva más de 20 años trabajando en el Congreso.** Cruz ha utilizado su despacho para hacer movimientos políticos y le resuelve a los senadores favores relacionados con tiquetes aéreo y excusas.

Por su parte la Fundación para la Libertad de Prensa en un **comunicado condenó la agresión de los congresistas contra Noticias Uno.** Según la Flip, “Durante la sesión, el subsecretario del Senado, Saúl Cruz, simuló ser golpeado en la cara por el camarógrafo. Luego denunció la inexistente golpiza ante policías que custodiaban el recinto”.

De igual manera, la Flip dijo que “estos hechos son de la mayor gravedad pues sucedieron en el Congreso de la República, en donde **deben existir las mayores garantías para el cubrimiento periodístico y el derecho a la información.** En segundo lugar, porque están enmarcados dentro de una serie de agresiones repetitivas por parte de congresistas y que están generando un ambiente cada vez más hostil para la prensa en el congreso”.

La FLIP recordó además que “La simulación de la agresión por parte de Saúl Cruz, la posterior falsa denuncia presentada ante la policía y el Congreso y la andanada de llamados a encarcelar, censurar, demandar y obstruir el trabajo de Noticias Uno por parte de senadores de la República son una **violación de las obligaciones internacionales adquiridas por el Estado colombiano** en materia de la libertad de expresión”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

######  
