Title: Por agresiones del ESMAD al Paro Nacional 33 eurodiputados envían carta al presidente Santos
Date: 2016-06-09 12:25
Category: Paro Nacional
Tags: Cumbre Agraria, ESMAD, Eurodiputados, Minga Nacional, Paro Nacional
Slug: por-agresiones-del-esmad-al-paro-nacional-33-eurodiputados-envian-carta-al-presidente-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/eurodiputados-cumbre-agraria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [9 Jun 2016]

Un grupo de 33 eurodiputados escribieron una carta al presidente de Colombia, Juan Manuel Santos, en la que manifiestan su preocupación por la actuación del [ESMAD](https://archivo.contagioradio.com/?s=esmad) en contra de los manifestantes en el marco del Paro Nacional (Minga) entre las que se encuentran los **asesinatos de 3 indígenas y las [amenazas de los paramilitares](https://archivo.contagioradio.com/alcalde-y-personero-de-valdivia-criminalizan-al-movimiento-rios-vivos/)** a participantes del paro en el departamento de Antioquia.

En la misiva los diputados señalan que hay varias acusaciones contra integrantes de la Fuerza Pública “*en particular, nos preocupa especialmente el comportamiento del ESMAD que ha sido acusado de atacar verbal y físicamente a los manifestantes y de realizar detenciones arbitrarias*” además reafirman que el **[Paro Nacional](https://archivo.contagioradio.com/?s=paro+nacional) ha sido pacífico y obedece a incumplimientos del gobierno.**

*“Estas violaciones han ocurrido en el contexto de un paro pacífico que están llevando a cabo junto a movimientos sociales para solicitar el cumplimiento de acuerdos gubernamentales previos, que datan del año 2013, sobre reformas a nivel agrario”* señala el comunicado y agrega que **es fundamental que se le dé un tratamiento respetuoso y pacífico a las protestas sociales en medio de un proceso de paz con las FARC y con el ELN** “*Es fundamental que su gobierno garantice el derecho a la protesta social pacífica*”.

Por último instaron al gobierno a **garantizar que el ESMAD, paramilitares u otro actor no viole los Derechos Humanos**, que se investiguen las [agresiones contra los manifestantes](https://archivo.contagioradio.com/un-indigena-nasa-muerto-y-otro-mas-herido-en-la-minga-agraria-en-cauca/), tanto los asesinatos como las agresiones, que se atienda a los heridos, que se libere a los detenidos o se les garanticen sus derechos y por último que se establezcan todas las **acciones necesarias para que el diálogo sea productivo** y se establezcan los acuerdos de manera pronta y por la vía pacífica del diálogo.

[![eurodiputados-carta-santos-contagioradio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/eurodiputados-carta-santos-contagioradio.jpg){.aligncenter .wp-image-25179 width="396" height="462"}](https://archivo.contagioradio.com/por-agresiones-del-esmad-al-paro-nacional-33-eurodiputados-envian-carta-al-presidente-santos/eurodiputados-carta-santos-contagioradio/)

En este mismo sentido se manifestó la organización Amnistía Internacional y exigió que el ESMAD se abstenga de usar la fuerza excesiva en contra de las manifestaciones pacíficas y justificadas de las comunidades campesinas,negras e indígenas en Colombia

[![amnistia-contagioradio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/amnistia-contagioradio.jpg){.aligncenter .wp-image-25181 width="428" height="433"}](https://archivo.contagioradio.com/por-agresiones-del-esmad-al-paro-nacional-33-eurodiputados-envian-carta-al-presidente-santos/amnistia-contagioradio/)
