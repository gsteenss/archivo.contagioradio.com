Title: La Guajira, volver a soñar un territorio libre y vivo
Date: 2019-03-11 08:21
Author: ContagioRadio
Category: CENSAT, Opinion
Tags: Carbones del Cerrejón, Cerrejón, comunidad Wayúu, DANE, Guajira
Slug: la-guajira-volver-a-sonar-un-territorio-libre-y-vivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

##### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

##### 9 Mar 2019 

[Carbones del Cerrejón es el proyecto de explotación de carbón de mayor extensión en Colombia y la mina de carbón a cielo abierto más grande de Sur América. Tras más de 36 años de presencia y actividad en el territorio guajiro, este proyecto ha logrado mostrarse al mundo como líder en los procesos de responsabilidad social y ambiental, como lo asegura Bettercoal en su reciente informe sobre las buenas prácticas de la empresa. En su página web y sus cuentas en redes sociales son múltiples las imágenes, videos y relatos conmovedores y realmente “sorprendentes” que promocionan una Guajira fértil, próspera y exuberante en términos ambientales, culturales y económicos. Pero en contraste con lo que expone la empresa, hoy en día la Guajira es el segundo departamento más pobre del país, y está entre los cinco más desiguales, según datos del DANE (2017).]

[La larga lista de daños ambientales y sociales ha sido documentada largamente por organizaciones y comunidades. Bien conocidos son los impactos de la minería de carbón en el agua, el suelo, el aire, la biodiversidad y la salud de trabajadores y comunidades aledañas. Además de los daños ambientales, el pueblo Wayúu está siendo víctima de un verdadero etnocidio, pues la pérdida de áreas cultivables y el desplazamiento de sus territorios ancestrales, ha eliminado prácticas tradicionales que van desapareciendo  Además, la transformación y deterioro de su entorno natural, el ruido del tren y de las explosiones en la mina, han despojado a las personas Wayúu de su bien más preciado: el sueño. El pueblo Wayúu es un pueblo onírico, recuerda, vive, aprende, toma decisiones y proyecta a través de los sueños. Una comunidad Wayúu que no sueña, ha perdido elementos fundamentales de su identidad y sentidos de vida.]

[El conjunto de daños enunciados, ha devenido en una verdadera crisis humanitaria en el departamento. Desde hace algunos años las cifras de niños y niñas muertas en La Guajira por desnutrición y falta de agua potable, han ocupado páginas enteras en la prensa nacional e internacional. Sin embargo, las y los gobernantes del país y la región, así como la sociedad en general, parece no vincular la crisis con la extracción de carbón. Ante esta situación, ni la empresa ni el Estado han tomado medidas para afrontarla de manera estructural. La empresa por un lado, ha generado grandes impactos sociales y ambientales por los cuales no se ha responsabilizado El Estado por su parte, no solo ha sido ausente y permisivo, sino que ha obrado de manera irregular en algunos procesos asociados al otorgamiento de permisos ambientales requeridos para la operación minera.]

[Frente a esta grave situación, las comunidades afectadas por la minería de carbón en conjunto con algunas organizaciones regionales y nacionales   el pasado 22 de febrero presentaron una Acción de Nulidad Simple que busca anular la licencia ambiental de Carbones del Cerrejón y con ello detener definitivamente la operación de la empresa en el departamento. Esta demanda hace una revisión detallada del expediente ambiental del proyecto Carbones del Cerrejón. Tras el estudio se identificaron varias ilegalidades en el proceso: falsa motivación, desviación de poder, expedición irregular, violación a la norma superior y violación al derecho de la defensa y la participación. Sumado a ello, se han emitido una serie de sentencias de la Corte constitucional referidas a diversos casos en que la empresa de una forma u otra debe reparar daños o ejecutar acciones que ha omitido y que eran de su obligación. No obstante, el cumplimiento de estas sentencias se ha quedado en procedimientos obsoletos y amañados que en vez de generar reparaciones pretenden limpiar el nombre de la empresa.]

[Lo que nos presenta el actual escenario es la necesidad de cambiar el estado de cosas en el departamento, un llamado a soñar una Guajira diferente, a dialogar con el pasado sobre el futuro e imaginar formas de vida que garanticen la dignidad de sus pueblos y ecosistemas. Salir de la dependencia al carbón y crear nuevas formas de producción energética es urgente, pero estas alternativas deben garantizar a las comunidades autonomía y justicia. Por ello es necesaria una transición energética justa, popular y pos extractiva que permita al departamento dejar de ser la zona de sacrificio que por décadas se ha puesto a disposición del capital transnacional, y volver a soñar como el territorio libre y vivo que otrora fue y se puede construir. Por esta razón hacemos un llamado a realizar un vehemente ejercicio de veeduría por parte del gobierno, organizaciones y de la sociedad en general respecto a la demanda, los incumplimientos y obligaciones de Cerrejón, y la situación actual de La Guajira.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
