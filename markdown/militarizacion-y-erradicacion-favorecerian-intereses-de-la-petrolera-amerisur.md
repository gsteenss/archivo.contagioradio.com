Title: Militarización y erradicación favorecerían intereses de la petrolera Amerisur
Date: 2016-07-05 12:23
Category: DDHH, Nacional
Tags: Amerisur, Brigada de Selva No. 27, erradicación cultivos ilícitos, Puerto Asís Putumayo, Zona de Reserva Campesina Perla Amazónica
Slug: militarizacion-y-erradicacion-favorecerian-intereses-de-la-petrolera-amerisur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Doral News] 

###### [5 Julio 2016]

Según denuncian las comunidades de la Zona de Reserva Campesina Perla Amazónica, en Puerto Asís, Putumayo, miembros de la Brigada de Selva No. 27 han incrementado sus operaciones militares para favorecer a la empresa británica 'Amerisur'. De acuerdo con líderes, los **militares tienen en su poder una lista con nombres de pobladores que al ser señalados como presuntos sospechosos, son retenidos e indagados.** En zonas de influencia de la petrolera, los efectivos han erradicado cultivos ilícitos afectando las demás siembras de las familias y desconociendo los espacios de diálogo para la sustitución de cultivos.

El pasado martes 28 de junio en horas de la mañana, miembros de la Brigada de Selva No. 27, al mando del Sargento Segundo Mahecha, realizaron acciones de erradicación en el caserío Camelias, **afectaron los cultivos de 3 familias y amenazaron con  la continuidad de las erradicaciones**. Al día siguiente, los militares retuvieron por varios minutos a Edwis Ramírez, líder de la comunidad El Baldío, quien estaba en la entrada de la Plataforma 1 de la empresa 'Amerisur'. A Ramírez le pidieron su documento de identificación para compararlo con un listado en el que los militares señalan supuestos sospechosos.

Las comunidades denuncian que está situación es recurrente y que en los últimos días se ha agudizado porque ha incrementado el pie de fuerza militar en la zona. Por lo que buscaron dialogar con el Sargento Mahecha y explicarle el proceso que adelanta la 'Mesa Regional de Organizaciones Sociales del Putumayo' con el gobierno, para concertar planes de sustitución de cultivos; sin embargo, **el alto mando aseveró que la erradicación continuaría porque es una orden del ejecutivo**.

Los pobladores insisten en que se están desconociendo los autos y sentencias de la Corte Constitucional que establecen la obligación previa para cualquier proceso de erradicación, así como aplicar planes de contingencia que les permitan no quedar en un estado de insubsistencia que los lleve a nuevos desplazamientos. Para ellos es lamentable que en **las zonas en las que se ha militarizado y erradicado, sean las mismas áreas donde la petrolera 'Amerisur' quiere ampliar su rango de explotación**, con la apertura de nuevas plataformas y la exploración sísmica de un nuevo bloque petrolero.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
