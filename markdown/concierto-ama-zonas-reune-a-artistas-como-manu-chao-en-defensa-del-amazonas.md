Title: Concierto Ama-Zonas reune a artistas como Manu Chao en defensa del Amazonas
Date: 2015-03-09 22:59
Author: CtgAdm
Category: Ambiente, Otra Mirada
Tags: Colectivo jaguar, Concierto Ama-Zonas, Doctor Krapula concierto en Leticia Amazonas, Manu Chao
Slug: concierto-ama-zonas-reune-a-artistas-como-manu-chao-en-defensa-del-amazonas
Status: published

##### Foto:Revistametronomo.com 

<iframe src="http://www.ivoox.com/player_ek_4190254_2_1.html?data=lZamkpeZeI6ZmKiak5iJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DixM7S1NnTb6LhwpLH0dPFt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### **[Entrevista con Augusto Falcón:]** 

Artistas como **David Jaramillo de Doctor Krápula, Manu Chao, Chucho Merchán, o Moyenei Valdés**, participaron del **encuentro intercultural Ama-Zonas**, para **denunciar** la tala de arboles, la imposición de los cultivos transgénicos, la privatización del agua o la falta de recursos para la población joven del Amazonas.

Los artistas **firmaron** la **declaración de Leticia** **denunciando**  las **situación de violación de DDHH** y de exclusión que vive la región debido a sus recursos naturales y al extractivismo de las multinacionales.

**Augusto Falcón,** miembro de la Asociación de **cabildos indígenas Trapecio del Amazonas**,  nos cuenta el simbolismo que ha tenido este concierto que motiva a las comunidades para  continuar en resistencia y defensa por el territorio.

Por otro lado dentro de  los procesos de la Amazonía nos encontramos con el **colectivo Jaguar**, quienes trabajan por la defensa de los derechos de la región amazónica. Su principal objetivo es generar **conciencia sobre la resistencia de las culturas indígenas** y de la preservación de los bosques donde habitan.

Una de los ejes de trabajo consistió en organizar el concierto como trabajo de difusión de la cultura de la comunidad indígena el Progreso, propiciando el espacio y el interés de los artistas de otras partes del mundo para que apoyen la lucha por la **concienciación sobre el respeto a la selva virgen y sus habitantes.**
