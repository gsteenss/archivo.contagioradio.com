Title: "Jorge 40", la llave que abre otras puertas a la verdad
Date: 2020-09-29 18:02
Author: AdminContagio
Category: Actualidad, Paz
Tags: JEP, Jorge 40, Mancuso, Paramilitarismo
Slug: jorge-40-la-llave-que-abre-otras-puertas-a-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Jorge-40-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Jorge 40 / Fiscalía General

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el regreso a Colombia del exjefe paramilitar Rodrigo Tovar Pupo, más conocido como “Jorge 40”, las víctimas de los crímenes del paramilitarismo esperan esta sea la oportunidad de conocer la verdad de los hechos que durante más de 20 años han exigido las víctimas del Bloque Norte de las extintas Autodefensas Unidas de Colombia (AUC) del conflicto. Esta es a su vez la llave que abriría la puerta para que, al menos otros dos, excomandantes de las AUC realicen su aporte a la reparación y no repetición . Al respecto, “Jorge 40” ha expresado que su compromiso con las víctimas es pleno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con su llegada continúa a su vez el lento éxodo de varios excomandantes de las AUC, entre ellos Salvatore Mancuso, quien pese a ser autorizado para salir de los Estados Unidos aún debe definir si es extraditado a Colombia o si es deportado a Italia. [(Le recomendamos leer: Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político")](https://archivo.contagioradio.com/mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otro exjefe que está ad portas de regresar el país es Hernán Giraldo Serna, quien después de pagar una condena de 13 años por narcotráfico quedaría en libertad el 1 de abril de 2021, según información consultada al l Federal Bureau of Prisons. Por su parte, Diego Murillo, álias "Don Berna", el último de los 14 exjefes extraditados por el gobierno de Álvaro Uribe, quedaría en libertad el 1 de diciembre de 2032. La llegada de ambos excomandantes dependerá de los procesos que enfrentan ante la justicia nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Rodrigo Tovar Pupo había solicitado al ingreso a la JEP en 2019 y aunque fue rechazado, el caso está en apelación, pues según su defensa, el exjefe ha expresado su voluntad de aportar a la verdad, tal como también lo han manifestado otros exintegrantes de las AUC como **Carlos Mario Jiménez, conocida en tiempos de guerra como alias “Macaco”; Carlos Antonio Moreno Tuberquia y Héctor Germán Buitrago, alias “Martín Llanos”** han mostrado su intención de solicitar ingreso a esta justicia especial para contar lo que saben a través de una carta enviada a Álvaro Leyva. ([Lea también: Exparamilitares dicen sí a la verdad)](https://archivo.contagioradio.com/exparamilitares-dicen-si-a-la-verdad-para-reparar-a-las-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, los magistrados de la JEP han señalado que este organismo no es una justicia subsidiaria a la de Justicia y Paz y que los exjefes paramilitares serán recibidos únicamente **si cometieron delitos como terceros civiles, es decir, como financiadores del paramilitarismo** antes de hacer parte de las AUC. Agregando que siempre podrán acudir a otras instancias, como la Comisión para el Esclarecimiento de la Verdad, la Unidad de Búsqueda de Personas Dadas por Desaparecidas o la Unidad Especial de Investigación de la Fiscalía General de la Nación con el fin de resarcir a las víctimas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "No evadiré mi responsabilidad , pero no quiero poner en riesgo mi vida y la de mi familia": Jorge 40

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Semanas antes de regresar a Colombia, el exjefe paramilitar expresó por medio de una carta enviada desde una prisión de migración en Pensilvania, Estados Unidos, **su interés de asumir la responsabilidad en el conflicto armado en Colombia y su compromiso con la verdad y las víctimas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la misiva Tovar, destaca que luego de 12 años y medio recluido en una cárcel norteamericana, señala ,que e**s momento de asumir las responsabilidades derivadas de su participación directa en el conflicto armado colombiano**. Agrega que **"esa posibilidad me ha sido esquiva, por qué el Estado colombiano nunca me ha brindado las garantías mínimas de seguridad para poder hacerlo"**, poniendo como prueba, el asesinato de su hermano Sergio Tovar, el 24 de diciembre del 2009, afectando, según el su disposición de rendir versión libre luego de ser extraditado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dirigiéndose a la JEP, afirma que **"tienen en sus manos la posibilidad de otorgarle a las víctimas y al país la verdad y la justicia que todos reclaman***"*, y que con su rechazo también niegan una parte fundamental de esa verdad. Una verdad, que según Tovar, no está disponible en ninguna otra jurisdicción, y que agregó, *"**necesito contar para reincorporarme a la sociedad**, … una verdad que necesita Colombia para intentar un proceso de reconciliación y superar la espiral de violencia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado refiriéndose a quiénes conforman el Sistema Integral de Verdad Justicia Reparación y No Repetición, indico **que su compromiso con las víctimas es pleno** , y que además esto *"**es lo mínimo que podemos hacer por nuestros hijos e hijas para darles una oportunidad de vivir en un país diferente al que nos tocó vivir a nosotros**"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Los procesos penales derivan en una o varias condenas, por algunos cuantos hechos, pero nunca en un espacio para reconstruir la verdad amplia y plena (...) o hacer una reparación integral a las víctimas, y mucho menos emprender acciones concretas y restaurativas de perdón y reconciliación".*
>
> <cite>Apartado carta de **Rodrigo Tovar Pupo**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A su vez le comunicó al presidente Iván Duque, que conocer esta verdad del Caribe colombiano aportarían a comprender las violencias emergentes, y reconociendo el poder de esta verdad señaló que también es *"**consciente de lo que ello implica para mí y para mi familia por ese motivo pido al Estado colombiano que nos brinde todas las garantías de seguridad para mi regreso**".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Decirnos la verdad será un proceso complejo y doloroso pero necesario para entender lo que nos pasó**"*
>
> <cite>Apartado carta de **Rodrigo Tovar Pupo**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Ante ante esta carta y los diferentes pronunciamientos que se han dado por parte también de Salvatore Mancuso, diferentes organizaciones han **exigido públicamente que garantice, la vida de los exjefes paramilitares y junto a ellos la verdad que esperan las víctimas en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Iniciativa a la que se sumó el senador Roy Barreras, anunciando que iniciará un trámite en el que pretende **presentar un proyecto de ley para que ex paramilitares puedan acudir a la JEP,** estableciendo así, según el congresista, quiénes fueron los actores y determinadores de crímenes de la maquinaria de guerra del paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que a tan solo días de la llegada de "Jorge 40" al país, su hijo, Rodrigo Tovar Vélez, presentó su renuncia a la Dirección de Víctimas del Ministerio del Interior argumentando razones personales para dejar el cargo, pese a ello continuará trabajando junto a la entidad en calidad de asesor y en funcioness transversales. [(Lea también: Nombramiento de Jorge Tovar, una burla a la dignidad y memoria de las víctimas)](https://archivo.contagioradio.com/nombramiento-de-jorge-tovar-una-burla-a-la-dignidad-y-memoria-de-las-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque organizaciones como el Movimiento de Víctimas de Crímenes de Estado (MOVICE) resaltan la labor de contribución que ha adelantado Tovar con víctimas del Estado, han considerado que dicho nombramiento es «una afrenta a la dignidad y a la memoria de las más de 21.000 de víctimas fatales» que según el Centro Nacional de Memoria Histórica ha dejado el paramilitarismo en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lo que exigen las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Mientras al Bloque Norte de las AUC comandado por Tovar se le atribuyen **333 masacres en los departamentos de Atlántico, César, Magdalena y Guajira, con un total de 1.573** víctimas entre 1996 y 2005,** solo 99 casos pudieron ser tratados en las versiones libres a las que ‘Jorge 40’ asistió antes de su extradición. En su caso, existen en la Fiscalía 35 órdenes de captura y 38 medidas de aseguramiento, sumadas a otras 65 órdenes de captura y 109 detenciones relacionadas a investigaciones ligadas a delitos como homicidio en persona protegida, desplazamiento forzado, violencia sexual, y reclutamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Algunos de estos casos incluyen **una condena de 26 años por el asesinato del profesor Alfredo Correa de Andréis, en 2004; otra de 19 años por el asesinato de dos líderes sindicales de la Asociación Nacional de Trabajadores** en Atlántico; una condena de 25 años por la desaparición de 7 investigadores del CTI de la Fiscalía en zona rural del municipio de La Paz, Cesar en 2000. otra de 23 años por el homicidio de un líder sindical y además una sentencia anticipada por el asesinato, en 2001, de dos sindicalistas de la Drummond. [(Le recomendamos leer : Estado pide perdón por el asesinato del sociólogo Alfredo Correa de Andréis)](https://archivo.contagioradio.com/estado-pide-perdon-por-el-asesinato-del-sociologo-alfredo-correa-de-andreis/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con su llegadas, las víctimas han expresado su deseo de que este reconozca las afectaciones hechas en el territorio. Mientras desde la Mesa Departamental de Víctimas del Cesar, uno de los departamentos más golpeados por el accionar del paramilitarismo, han expresado que esperan ser reparados por parte de las AUC, en el departamento existen registros de cerca de 34.000 víctimas del conflicto armado, de las que el 80% fue afectada por el Bloque Norte de las AUC.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, desde El Salado, Bolívar, víctimas como la lideresa Yirley Velasco señalan que pese a las audiencias realizadas sobre la masacre, ocurrida en el 2000 y que dejó más de 100 víctimas mortales, es necesario saber la verdad y realizar un acto de perdón y reconocimiento. Las víctimas han sido enfáticas en que es necesario conocer además, **quiénes financiaron a los grupos paramilitares en el país y quienes se beneficiaron políticamente con sus acciones.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
