Title: Persiste arremetida del ESMAD contra población de Buenaventura
Date: 2017-05-31 13:32
Category: DDHH, Nacional
Tags: ESMAD, Paro Buenaventura
Slug: persiste-arremetida-del-esmad-contra-poblacion-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/el-tabloide-e1496255458901.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tablón] 

###### [31 May 2017] 

Desde las 3 de la mañana, los habitantes de Buenaventura denunciaron una fuerte arremetida por parte de integrantes del ESMAD, que con gases lacrimógenos intentaron disipar los bloqueos, en el punto de Sabrosuras, que mantiene la población como parte del Paro que se adelanta en esta ciudad del Valle del Cauca. **Hecho que habría dejado como resultado 4 personas heridas.**

De acuerdo con María Miyela Riascos, lideresa de la comunidad e intengrante del comité impulsor del paro cívico, la agresiones que se registraron en la madrugada de hoy son producto de la falta de interés del gobierno de conversar y por intentar dilatar la movilización, “el gobierno Nacional la única manera de respondernos es con la violencia, mandándonos toda la fuerza del ESMAD, **2.500 efectivos, a esta comunidad de Buenaventura que no es armada, que es pacífica y que está exigiendo unos derechos constitucionales**”.

Las comunidades, desde que inició el paro, han denunciado una serie de violaciones a derechos humanos entre ellas que la Armada Nacional y la Policía escolta al ESMAD en las arremetidas contra la población, incluso con balas de armas de fuego, prueba de ellos señaló María Mireya es el ataque de esta madrugada dejo entre los heridos al joven identificado como **César Augusto Guerrero quién recibió un impacto de bala a la altura del pecho. **

Además los habitantes han venido manifestando que el ESMAD lanza gases lacrimógenos a las viviendas en horas de la madrugada, cuando las personas se encuentran durmiendo, **disparan balas de goma a quemaropa a los ciudadanos que les hagan reclamos y hay helicópteros que en lanzan gas pimienta en las calles**. Le puede interesar:["Hasta los niños son víctimas del ESMAD en Buenaventura"](https://archivo.contagioradio.com/se-recrudece-la-represion-a-paro-civico-de-buenaventura/)

Estas denuncias se han hecho en la mesa de dialogo de defensa de los derechos humanos que se instaló con el gobierno y de igual forma, se le han dado a conocer a la Defensoría del Pueblo y a la Personería. Le puede interesar: ["Piden suspender el toque de queda en Buenaventura"](https://archivo.contagioradio.com/piden-suspender-toque-de-queda-en-buenaventura/)

El comité de impulso del paro cívico estaría interlocuntando con la Cumbre Agraría, que mañana se reunirá con Santos, para llevar el pliego de exigencias del paro de Buenaventura y **pedirle al presidente medidas urgentes frente a las problemáticas que presenta el pueblo Bonaverense**.

<iframe id="audio_19007019" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19007019_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
