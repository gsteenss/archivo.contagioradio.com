Title: "Antígonas" la lucha de las madres de Soacha en el Teatro
Date: 2017-07-25 14:00
Category: eventos
Tags: Bogotá, madres de soacha, teatro
Slug: antigonas-mujeres-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Antigone_-085-e1500918041113.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:CC de Teatro 

###### 26 Jul 2017 

3 años después de su estreno "Antígonas, tribunal de mujeres" vuelve al teatro. La Creación Colectiva de Tramaluna Teatro, dirigida por el Maestro Carlos Satizábal, se presentará en corta temporada del 26 al 29 de Julio en la Sala Seki Sano de Bogotá.

Antígonas, es una creación sinérgica entre artistas de la escena y mujeres vulneradas en sus derechos humanos. Están las madres de Soacha, cuyos hijos fueron asesinados en los mal llamados “falsos positivos” ejecuciones extrajudiciales de jóvenes de barriadas pobres presentados por el ejército a la prensa como guerrilleros dados de baja en combate.

Otras de las mujeres que hacen parte del montaje teatral son las sobrevivientes del genocidio político contra la Unión Patriótica, defensoras de Derechos Humanos víctimizadas por la seguridad del Estado y mujeres líderes estudiantiles víctimas de montajes judiciales e injustos encarcelamientos.

Ellas han convertido su dolor y su memoria en poesía, en grito poético de denuncia y demanda de justicia. Su presencia en la escena como poetas y actrices de su propia tragedia le concede una estremecedora verdad a esta obra. Las funciones serán a las 7:30 P.M en la Sala Seki Sano / Calle 12 \# 2- 65, boletería para estudiantes: \$12.000 / General: \$20.000.

<iframe src="https://www.youtube.com/embed/pBpGY2idTlI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
