Title: Casos de leishmaniasis incrementan en zona de influencia de Hidroituango
Date: 2019-04-17 12:37
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Empresas Públicas de Medellín, Hidroituango, leishmaniasis
Slug: casos-de-leishmaniasis-incrementan-en-zona-de-influencia-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Skin_ulcer_due_to_leishmaniasis_hand_of_Central_American_adult_3MG0037_lores.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Leishmaniasis-zona-de-influencia-de-Hidroituango-.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tal como lo denunció la periodista Claudia Julieta Duque en la plataforma Nizkor, el número de casos de leishmaniasis se dispararon entre los habitantes de Sabanalarga, Antioquia desde el 2016, año en que Empresas Públicas de Medellín (EPM) comenzó a talar a más de [2.000 hectáreas de bosques en la zona para la construcción de la hidroeléctrica y que hoy se encuentran en alto estado de descomposición, condiciones aptas para la propagación de lo que las comunidades han denominado como "enfermedad de Hidroituango".]

[Según cifras de la Secretaría de Salud del municipio, en años anteriores a 2015 no se registraban más de cinco casos de esta enfermedad por año en Sabanalarga. Sin embargo, en 2016 se presentó un incremento exponencial, **la cifra llegó a 43** y desde ese año sigue en aumento el número de casos por leishmaniasis. **En 2017 fueron reportados 75, en 2018, 85 y en 2019, se identificaron 15 personas infectadas** a lo largo del año. En consecuencia, en el municipio han fallecido cinco personas por la enfermedad, señaló el Movimiento Ríos Vivos - Antioquia.]

Como lo indica Isabel Cristina Zuleta, integrante del Movimiento, previo a la construcción de la hidroélectrica en 2016, los casos de leishmaniasis se presentaban mayormente en zonas cercanas a la selva, como en los municipios de Ituango y Valdivia o el Parque Nacional Natural Paramillo, mientras que Sabanalarga por estar apartada de estas regiones, se veía poco afectada. Para la ambientalista, el crecimiento de esta enfermedad en los últimos tres años se debe a la intervención del bosque y la inadecuada disposición de ese material orgánico en estado de descomposición.

"**Hay pilas inmensas de todos los árboles talados de bosque seco tropical y estos son los sitios de incubación donde se reproduce los vectores que producen la leishmaniasis**. Nosotros no vemos otro factor de incidencia que tenga relación para esta dramática crecimiento de personas afectadas por esta enfermedad", indicó Zuleta.

### **Las solicitudes de las comunidades** 

En primer lugar, los habitantes de Sabanalarga piden que el Ministerio de Salud investigue una posible relación entre la construcción de Hidroituango y el aumento de estos casos. Además, solicitan que las poblaciones afectadas sean tratadas de manera adecuada por las instituciones de salud. Zuleta señala que las personas enfermas han tenido que trasladarse desde Sabanalarga a hospitales de Ituango o Validivia para recibir la atención y las medicinas necesarias para recuperarse.

La ambientalista también indicó que en una audiencia el próximo 6 de mayo instarán a la Fiscalía General de la Nación a identificar los casos de leishmaniasis como uno de los riesgos inminentes sobre las regiones aledañas a Hidroituango, provocadas por su construcción. Así mismo, Zuleta indica que el ente investigador debería solicitar medidas cautelares para dar una solución a esta grave problemática. (Le puede interesar: "[Fiscalía pide medidas cautelares para comunidades aledañas a Hidroituango](https://archivo.contagioradio.com/fiscalia-pide-medidas-cautelares-para-comunidades-aledanas-a-hidroituango/)")

<iframe id="audio_34603880" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34603880_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
