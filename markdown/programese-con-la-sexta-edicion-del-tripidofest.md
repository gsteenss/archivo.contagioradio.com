Title: Prográmese con la sexta edición del TripidoFest
Date: 2016-11-17 12:17
Category: Cultura, Nacional
Tags: Diego Felipe Becerra, jovenes, TripidoFest
Slug: programese-con-la-sexta-edicion-del-tripidofest
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/tripidofest6-e1479401544160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tripido Fest] 

###### [17 Nov 2016] 

**Este 19 de noviembre, se llevará a cabo en Engativa el Tripido Fest en su sexta versión**, un espacio que a partir de muestras culturales como la música, el teatro y el graffiti, pretende resignificar y apropiarse de los barrios y buscar que la juventud tenga más escenarios de participación e incidencia en la capital, con las propuestas que las y los jóvenes construyen en sus cotidianidades.

De igual forma, este festival **plantea alternativas a la militarización, estigmatización y criminalización de la vida juvenil** a partir de la articulación de diferentes procesos culturales y a su vez, estos jóvenes intentan reconstruir el tejido social que se ha roto en muchos barrios a causa del conflicto armado.

Esta iniciativa además, es un homenaje a Diego Felipe Becerra, que se identificaba en sus dibujos como Tripido y quién fue asesinado el 19 de agosto del 2011 por miembros de la Fuerza Pública, mientras realizaba un mural en Bogotá. Le puede interesar:["Continúan las dilaciones en el caso de Diego Felipe Becerra"](https://archivo.contagioradio.com/continuan-las-dilaciones-en-el-caso-de-diego-felipe-becerra/)

**Para esta ocasión el festival contará con espació musical diverso que reunirá en una misma tarima géneros** como el rock, punk y rap, con artistas como Yoki Barrios, Bestiario, RedNoise, Valdavia, Furia Femenina, entre otros artistas. De igual forma se llevará a cabo un campeonato de banquitas y se tendrán pistas de Break dance.

De acuerdo con Cindy Lu, una de las organizadoras del evento y  miembro de Te Juntas “otra de las motivaciones que tiene el Tripido Fest es **repudiar el nuevo Código de Policía, que criminaliza** hacer malabares en la calle, pintar en la calle, y que va en contra vía de las expresiones juveniles y de la misma construcción de paz y proyecto de país"

El evento iniciará a partir de las 9 de la mañana, en la casa de la juventud de la localidad de Engativá, en el barrio La Florida. **La entrada es totalmente gratis y habrá olla comunitaria para la hora del almuerzo.**

<iframe id="audio_13805566" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13805566_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
