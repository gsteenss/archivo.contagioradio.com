Title: Siguen los atentados en contra de los líderes de la Comunidad Wayúu
Date: 2018-10-03 15:23
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: desnutrición, Guajira, Lideres, líderesa, nación wayuu
Slug: siguen-los-atentados-en-contra-de-los-lideres-de-la-comunidad-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 Oct 2018] 

La comunidad de Katsaliamana, en la Guajira, denunció que en las últimas semanas han aumentado los atentados en contra de sus líderes y líderesas de la organización Nación Wayúu, producto de las denuncias que han emprendido **los activistas en defensa de la primera infancia, el territorio y el derecho a la vida.**

El hecho de violencia más reciente se presentó en contra de la vida de José Silva, presidente del Movimiento Indígena Nación Wayúu, quien denunció que el pasado 1 de octubre, fue víctima de un atentado cuando iba en su esquema de seguridad, acompañado por el también líder Ramón Viecco. Los hombres se dirigían hacia la vivienda de otro integrante de esta plataforma, minutos más tardes los escoltas les señalaron que habían hombres armados y se presentó un intercambio de disparos. **Silva ya había sido víctima en otras tres ocasiones de actos de violencia en su contra, mientras que Viecco, denunció un intento de asesinato.**

Asimismo, el pasado 28 de septiembre, la líderesa Matilde López, ganadora del premio a la defensa de los derechos humanos e integrante también de Nación Wayúu, fue objeto de un atentado cuando se dirigía a su hogar. De acuerdo con el relato de la líderesa, su esquema de seguridad fue interceptado por cuatro hombres que iban en dos motos, **uno de ellos abrió fuego contra el vehículo. **

López aseguró que gracias a la rápida acción de los escoltas, pudieron salir ilesos, sin embargo aseguró que este hecho sería producto de la labor que ha emprendido en defensa de la primera infancia en la Guajira, **debido a que en lo corrido de este año 60 niños han muerto por desnutrición. **(Le puede interesar[: "Atentan contra la vida de líder indígena Wayúu, en Rioacha-Guajira"](https://archivo.contagioradio.com/atentan-contra-la-vida-de-lider-indigena-wayuu/))

### **La Comunidad de Katsaliamana en defensa de la vida** 

La Nación Wayúu ha hecho una serie de denuncias frente a la dramática situación que afrontan las niñas y los niños de la comunidad de Katsaliamana como consecuencia de diversas situaciones que se han presentando en el territorio de la Guajira.

La primera de ellas tiene que ver con la actividad minera en la región, que de acuerdo con los indígenas, ha alterado sus métodos tradicionales de subsistencia y de abastecimiento de agua**, secando más de 26 afluentes y ocasionando la ruptura del tejido social**.

Además de ello se encuentra la construcción de la Represa El Cercado, con la que se prometía garantizar agua a 9 municipios y según la Nación Wayúu después de 8 años esto aún no sucede. **Asimismo se denunció que el agua de esa represa estaría siendo utilizada por la multinacional El Cerrejón**.

Finalmente esta la exigencia que le han hecho al gobierno para que se respete su derecho a la consulta previa en la administración de los recursos estatales para la atención a la primera infancia que en este momento se dan desde el Instituto Colombiano de Bienestar Familiar, debido a que para ellos es evidente la corrupción **que ha llevado a los niños y niñas a morir de hambre. **(Le puede interesar: ["Pueblo Wayúu a punto de desaparecer por incumplimiento estatal"](https://archivo.contagioradio.com/pueblo-wayuu-al-borde-del-exterminio-por-incumplimiento-estatal/))

###### Reciba toda la información de Contagio Radio en [[su correo]
