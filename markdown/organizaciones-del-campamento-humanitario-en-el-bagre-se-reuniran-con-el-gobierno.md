Title: Organizaciones del campamento humanitario en el Bagre se reunirán con el Gobierno
Date: 2016-07-13 12:58
Category: DDHH, Nacional
Tags: Antioquia, Campamento Humanitari, El Bagre
Slug: organizaciones-del-campamento-humanitario-en-el-bagre-se-reuniran-con-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/el-bagre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:[[derechoalatierraelbagre] 

###### [13 de Jul] 

El día de hoy se llevará a cabo en el municipio del Bagre, Antioquía una reunión entre el gobierno nacional y los voceros de la Asociación de Hermandades Agroecológicas y Mineras (AHERAMIGUA) de Guamocó, con el fin de exponer la **precaria situación de derechos humanos que afrontan las comunidades** para permanecer en sus propios territorios y visibilizar las **amenazas a líderes del campamento humanitario**.

De acuerdo con el comunicado de prensa de la organización AHERAMIGUA, les ha llegado información en donde dicen que "**apenas los líderes den papaya, van a matar a alguno de ellos**". El vocero Manuel Garavito, argumenta que las amenazas se generaron después de que las comunidades decidieran[instalar el campamento humanitario debido a la violencia que han vivido ](https://archivo.contagioradio.com/200-campesinos-instalan-refugio-humanitario-en-el-bagre-antioquia/)en lo corrido del año.

"Todo el que se pronuncie a favor de la paz o a defienda los derechos es objetivo militar de estos grupos, esta es nuestra preocupación y lo que queremos hoy, es pedirle al gobierno nacional que **tome acciones efectivas para las comunidades**, porque se hacen propuestas y acuerdos, pero a la larga no vemos resultado de ello" afirmo el vocero de la organización AHERMAIGUA.

El municipio del Bagre, Antioquía, ha sido uno [de los territorio disputados por diferentes grupos armados al margen de la ley](https://archivo.contagioradio.com/comunidades-instalaran-campamento-humanitario-frente-a-hostigamiento-paramilitar/), que han provocado diferentes desplazamientos en la región, motivo por el cual la organización AHERAMIGUA en conjunto con otras organizaciones iniciaron la instalación del campamento humanitario en Guamocó. El día de **hoy se llevará a cabo un plantón en el Ministerio del Interior** a las dos de la tarde en respaldo al campamento humanitario, por el territorio, la vida digna y la paz.

<iframe src="http://co.ivoox.com/es/player_ej_12212184_2_1.html?data=kpefk5eVfJWhhpywj5WaaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpCrpdPV187h0YqWh4zq0MjS1NSPham5s6a6q6y5hY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
