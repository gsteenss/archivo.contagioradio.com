Title: Suspenden diplomado financiado por Anglo Gold Ashanti en la Universidad de Antioquia
Date: 2019-01-23 17:05
Author: AdminContagio
Category: Ambiente, Educación
Tags: Anglogold Ashanti, Quebradona, Universidad de Antioquia
Slug: suspenden-diplomado-anglo-gold-ashanti-la-universidad-antioquia
Status: published

###### [Foto: Melissa Sanchez A.] 

###### [23 ene 2019] 

[La suspensión se da luego de que organizaciones ambientales de Jericó, Antioquia manifestaran su inconformidad con el lanzamiento, por parte de la Universidad de Antioquia, de un diplomado de fortalecimiento organizacional, **financiado por AngloGold Ashanti, empresa multinacional que está desarrollando un proyecto de exploración minera en este mismo municipio.**]

En diciembre, la universidad anunció que la Facultad de Ingeniería estaría realizando un diplomado, entre el 17 de enero y el 16 de febrero de 2019, en Jericó con el objetivo de “contribuir al empoderamiento de las organizaciones sociales” del municipio para mejorar el impacto de su labor. Según el comunicado, este curso estaría entrenando a 40 líderes de Jericó en temas de liderazgo, autogestión y sostenibilidad.

Sin embargo, la administración municipal rechazó la propuesta y actualmente, el plan de estudio se encuentra suspendido. [En un comunicado público, más de 65 organizaciones ambientales **cuestionaron las intenciones de la multinacional minera de contratar a la Universidad para realizar este diplomado**. Según los firmantes, la implementación de esta iniciativa podría "alimentar la consolidación de una línea de base para disfrazar, con una falsa legitimidad, al proyecto minero Quebradona".]

[José Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó, manifestó su preocupación que la universidad se hubiera aliado con una empresa minera que ha sido contestada "de manera contundente" por la población de Jericó. El año pasado, el Concejo Municipal de Jericó] prohibió la actividad minera en el territorio mediante un acuerdo municipal. Además, las organizaciones sociales han emprendido diferentes acciones para frenar la Quebradona, que se encuentra en fase de exploración.

Por esta razón, Jaramillo afirmó que la universidad ha debido consultar las organizaciones locales para conocer el trabajo que han emprendido en contra de la multinacional y de los nocivos efectos ambientales que ha ocasionado la primera etapa del proyecto minero. (Le puede interesar: "[Jericó, Antioquia le dice ¡no! a la minería por segunda ocasión](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/)")

Tras el anuncio de este diplomado, organizaciones ambientales de Jericó le pidieron a los funcionarios de la Universidad presentar el contrato que firmaron con la empresa minera para conocer las condiciones y dineros acordados.  Actualmente, estas organizaciones han radicado un derecho de petición ante la Institución para obtener el documento.

<iframe id="audio_31726056" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31726056_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
