Title: Amenazan a líderes del CRIC y ACIN que participan en Minga indígena
Date: 2019-04-08 23:41
Author: CtgAdm
Category: Líderes sociales, Movilización, Nacional
Tags: Amenazas contra indígenas, CRIC, ONIC, Panfletos amenazantes
Slug: amenazan-a-lideres-del-cric-y-acin-que-participan-en-minga-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/aguilas-negras-770x400-770x400.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Minga-amenaza.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A través de panfletos conocidos en las últimas horas, las Águilas Negras lanzaron amenazas contra los dirigentes del **Consejo Regional Indígena del Cauca (CRIC) y la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)** en represalia por el bloqueo de la Vía Panamericana como consecuencia del ejercicio de la Minga Nacional por la Vida. La ACIN señala que este panfleto podría estar relacionado con el reciente trino de Álvaro Uribe.

En particular, el panfleto invita al asesinato de consejeros, gobernadores y coordinadores de la guardia indígena colocando un precio de \$100.000.000  por la vida de los líderes de los procesos organizativos del Norte del Cauca. Ante este amedrantamiento, los tejidos de DD.HH. de los pueblos indígenas han puesto en alerta a todos los organismos de control y seguimiento de derechos,  **pero en especial a la comunidad en sí para fortalecer las estrategias de cuidado de sus integrantes y dirigentes.**

> Confidencialmente con las declaraciones de [@AlvaroUribeVel](https://twitter.com/AlvaroUribeVel?ref_src=twsrc%5Etfw) , denunciamos la aparición de estas amenazas en contra de nuestro proceso y nuestros guardias indígenas, uno de ellos sufrió un atentado por parte de las disidencias de las FARC hace 2 meses. [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [pic.twitter.com/sYBwEj0vVb](https://t.co/sYBwEj0vVb)
>
> — NASAACIN (@ACIN\_Cauca) [8 de abril de 2019](https://twitter.com/ACIN_Cauca/status/1115303610466930688?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Estas amenazas surgen, solo horas después de que el senador Álvaro Uribe se manifestara en contra de los acuerdos  parciales alcanzados entre el Gobierno y la Minga afirmando que los pueblos indígenas estaban ligados al terrorismo y justificando el suceso de las masacres en contra de la protesta social.[(Le puede interesar: La conducta guerrerista del senador Uribe)](https://archivo.contagioradio.com/conducta-guerrerista-senador-uribe/)

> Si la autoridad, serena, firme y con criterio social implica una masacre es porque del otro lado hay violencia y terror más que protesta
>
> — Álvaro Uribe Vélez (@AlvaroUribeVel) [7 de abril de 2019](https://twitter.com/AlvaroUribeVel/status/1114894520758489096?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
