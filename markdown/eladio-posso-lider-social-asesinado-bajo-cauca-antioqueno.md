Title: Eladio Posso, otro líder social asesinado en el Bajo Cauca Antioqueño
Date: 2018-11-01 15:02
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Antioquia, lideres sociales
Slug: eladio-posso-lider-social-asesinado-bajo-cauca-antioqueno
Status: published

###### Fotografía: @ANTIOQUIACCEEU 

###### 1 Nov 2018 

Este jueves 1 de noviembre fue hallado sin vida por heridas ocasionadas con arma blanca el líder comunitario **Eladio de Jesús Posso Espinosa, ** tesorero de la Junta de Acción Comunal de la vereda 'El Triunfo' en el corregimento La Caucana, municipio de Tarazá, quien **además pertenecía al programa de sustitución de cultivos de uso ilícito**.

Según información suministrada por Óscar Zapata, integrante de la Coordinación Colombia Europa Estados Unidos, **la fuerte presencia de las denominadas Autodefensas Gaitanistas, de los llamados Caparrapos en la región**, apuntaría a que uno de estos grupos podrían ser los perpetradores de este nuevo crimen.

Zapata agregó que a pesar del aumento en la presencia de la fuerza pública en la región, es posible que algunos miembros de la institucionalidad del Bajo Cauca tuvieran vinculos con dichas estructuras criminales, lo que posibilitaría su libre accionar por la zona (Le puede interesar: **[Colombia pierde a un líder “innato”: asesinan a Norberto Jaramillo en Tarazá)](https://archivo.contagioradio.com/asesinan-norberto-jaramillo-taraza/)**

El integrante de la **CCEEUU**,  hizo un llamado a las autoridades locales para que se brinde respaldo a los líderes sociales de la región, pues las acciones que se están ejecutando no son suficientes para darle a la población las mínimas garantías de derechos fundamentales "no necesariamente la militarización de la zona da resultados frente al tema de seguridad en el territorio, creemos que tiene que haber voluntad para el desmantelamiento sometimiento y negociación con estas estructuras" señaló.

**En un año las víctimas han incrementado en un 20%**

Con el caso de Eladio de Jesús en Tarazá, Antioquia, son 29 los líderes sociales y comunitarios asesinados en este departamento convirtiéndola en la zona del país donde más líderes han sido asesinados. **Según el Observatorio de Derechos Humanos de la Fundación Sumapáz, dicha cifra representa un aumento en los homicidios del 20% con respecto al año pasado.**

<iframe id="audio_29778808" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29778808_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
