Title: Lo que poco se cuenta del desastre ambiental en Colombia
Date: 2015-06-16 17:47
Category: Ambiente, Entrevistas
Tags: Desastre ambiental, Manuel Rodríguez Becerra, Misniterio del Medio Ambiente, Putumayo
Slug: lo-que-poco-se-cuenta-del-desastre-ambiental-en-colombia
Status: published

###### Foto: contagioradio.com 

###### [Jun 16 2015] 

<iframe src="http://www.ivoox.com/player_ek_4649840_2_1.html?data=lZuhm52YdI6ZmKiak5mJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNCf0trSjdXTp9Cf1MqYxdrJstXVjMnSzpDIqdTV1Nnfx5DFscPdxtPhw9GPqc%2BfpNTZ0dLGrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Rodríguez Becerra. Ex Ministro de Ambiente] 

Aunque el daño ambiental producido por el atentado de las FARC es muy condenable y hace mucho daño, este tipo de hechos hace parte de un daño ambiental que está siendo perpetrado por varios actores tanto legales como ilegales en todo el país. “***El daño ambiental en Colombia está desbordado***” señala **Manuel Rodríguez Becerra**, quien agrega que el Estado ha sido incapaz de controlar este tipo de daños.

Según el ex ministro de ambiente, los daños ambientales son múltiples en todo el país, uno de los ejemplo que cita es el de la Isla de Salamanca, o el drenado de la ciénaga de Santa Marta para la ganadería, el desastre ambiental del Casanare originado por la ganadería, la actividad petrolera y otras actividades, en el Magdalena se está deforestando para agregar tierras a la ganadería, en la Serranía de San Lucas se incrementa también el daño desde hace más de 20 años.

Frente a las responsabilidades el ex ministro afirma que hay que buscar en la historia reciente del país, si un empresario ha salido a denunciar el desastre ambiental. “Ninguno en los último años” y esta situación evidencia que **los intereses empresariales privados, han estado por encima de los desastres que se evidencian en donde quiera que se les mire**. Además el mismo plan de desarrollo reconoce que el país está en una senda de insostenibilidad, sin embargo, lo que plantea no es salida a esta problemática.

Rodríguez afirma que hay que **darle despliegue a todos los hechos, y hacer un balance entre unos y otros**. Al caso del Putumayo se le ha dado suficiente despliegue, pero es necesario que el país conozca también los desastres causados por otros responsables. Además el Ministerio de Medio Ambiente, aunque con pocos recursos, es responsable del despliegue que debe tener todas esas situaciones en el país.
