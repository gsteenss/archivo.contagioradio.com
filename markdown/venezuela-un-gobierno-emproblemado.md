Title: Venezuela: Un gobierno emproblemado
Date: 2015-02-13 15:26
Author: CtgAdm
Category: Cesar, Opinion
Tags: chavistas, gobierno de Nicolás Maduro, Socialista Unido de Venezuela
Slug: venezuela-un-gobierno-emproblemado
Status: published

###### Foto: kirakar/Flickr 

Por **[[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)]**

Difícil la tiene el gobierno de Nicolás Maduro.  La intervención norteamericana contra  la administración central se ubica en distintos niveles, incluida la propaganda política en distintos medios de comunicación en el interior y en el exterior.  La oposición callejera de la derecha nacional (Capriles, Machado, López) es altanera y cuenta con apoyo continental, como se apreció en la reciente visita de los expresidentes de Colombia, México y Chile. La situación económica es apretada (inflación, desabastecimiento, sistema cambiario) y no se expide un paquete de medidas que enfrente directamente a la burguesía roja rojita y a usureros, acaparadores y saboteadores.

El sistema cambiario acaba de ser modificado, con todos los traumas que origina: en un contexto centralizado como el cubano la búsqueda de equilibrio en el mercado de divisas ha logrado mantener a flote la economía, pero en el desarticulado tejido venezolano el asunto adquiere otro color pues[ ]{.Apple-converted-space}la burocracia estatal es cómplice del gran capital y hace parte de la nueva burguesía roja; en estas condiciones el Estado no logra asumir el monopolio del manejo de la divisa internacional.

La fuga de capitales no ha podido controlarse; entre el 2003 y el 2013 la suma de dólares[ ]{.Apple-converted-space}por ese fenómeno alcanzó los 150.000 millones (50% del PIB). La inseguridad aumenta el malestar popular y de los sectores trabajadores. La caída de los precios del petróleo ha afectado notoriamente el mercado interno, por lo cual la lucha de la oposición de derecha y de la oligarquía improductiva[ ]{.Apple-converted-space}por retomar el control de la renta petrolera se incrementa; quiere todo. Pero lo destacable hasta[ ]{.Apple-converted-space}ahora es que el gobierno de Maduro ha logrado mantener el pago de la deuda externa y sus respectivos intereses. *Pero si la economía atraviesa una fase de crisis se debe ante todo a la ofensiva inmisericorde de Estados Unidos y de la vieja y nueva burguesía venezolana contra el pueblo chavista*.

Todo lo anterior se complica si apreciamos la gestión estatal. El actual gobierno se sustenta en el [**Programa de Gobierno 2013-2019**]{.s1}, conocido como Plan de la Patria. Pero es un programa no llevado a cabo. La democracia participativa y protagónica planteada en la letra[ ]{.Apple-converted-space}se ve truncada por el centralismo y autoritarismo del Partido Socialista Unido de Venezuela (PSUV) en el que no precisamente tienen el mando los trabajadores. Una contradicción de tal magnitud deberá ser resuelta en el mediano plazo en favor de estos últimos; se trata de avanzar en la democracia directa, bien en el actual período o ya en una fase de transición. Tal medida sí que frenaría la ofensiva de la oposición.

Lo propio ocurre[ ]{.Apple-converted-space}con respecto a las “nuevas formas de organización de la producción”, de las cuales se espera, dice el programa, que “impulsen la generación de tejido productivo bajo un nuevo metabolismo para la transición al socialismo”. Buena parte del mandato al chavismo ha transcurrido y aún no se adelantan medidas para[ ]{.Apple-converted-space}esto; frente a las largas filas y el desabastecimiento un efectivo control del “tejido productivo”[ ]{.Apple-converted-space}permitiría recuperar la producción estatal en el sector de alimentos y otros servicios. Las concesiones al[ ]{.Apple-converted-space}gran sector empresarial colocan una barra de acero en el engranaje social[ ]{.Apple-converted-space}y desbaratan el Plan de la Patria; Maduro y su equipo deberían recordar que las medidas gubernamentales del 6 de noviembre de 2013 dieron resultados positivos en lo económico y en lo político.

Pero no todo reside en lo que el gobierno pueda decidir. Bastante experiencia ha ganado el pueblo bolivariano y chavista como para que se deje arrebatar las conquistas obtenidas.[ ]{.Apple-converted-space}Su auto-organización y su independencia frente al PSUV, frente al gobierno y frente a la burguesía vieja y nueva será difícil; pero si de transición se trata por ahí es la cosa.
