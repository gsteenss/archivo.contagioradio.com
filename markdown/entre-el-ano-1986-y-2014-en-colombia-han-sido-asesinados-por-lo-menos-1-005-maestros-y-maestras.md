Title: Entre el año 1986 y 2014, en Colombia han sido asesinados por lo menos 1.005 maestros y maestras
Date: 2014-12-26 16:10
Author: CtgAdm
Category: Otra Mirada
Slug: entre-el-ano-1986-y-2014-en-colombia-han-sido-asesinados-por-lo-menos-1-005-maestros-y-maestras
Status: published

### Crédito foto: semana.com

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsHJ4IAx.mp3)  
 

Para Rafael Cuello, presidente nacional de FECODE,** las y los docentes son los profesionales al servicio del Estado con los peores salarios, en comparación con otros profesionales, sino que además son los más victimizados.**

Entre el año 1986 y 2014, en Colombia han sido asesinados por lo menos 1.005 maestros y maestras. Sólo en el 2014, ya se cuentan 12 asesinatos, 5.500 maestros y maestras amenazadas, 1.600 desplazados y desplazadas, y 70 exiliados.

Para Cuello, el Estado no brinda las garantías suficientes a la profesión docente, que en muchos territorios del país es la única presencia que se conoce del gobierno. "En Colombia habrá democracia cuando la sociedad entienda que los maestros son promotores de cultura y pedagogía, y que eso no puede ser un delito, ni puede ser victimizado", señaló.
