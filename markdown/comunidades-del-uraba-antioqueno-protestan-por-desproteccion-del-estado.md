Title: Comunidades del Urabá antioqueño protestan por desprotección del Estado
Date: 2018-02-08 15:32
Category: DDHH, Movilización
Tags: acuerdos de paz, Antioquia, comunidades de antioquia, incumplimientos del Gobierno, uraba
Slug: comunidades-del-uraba-antioqueno-protestan-por-desproteccion-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Por-la-defensa-del-territorio-e1518112574739.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Penca de Sábila] 

###### [08 Feb 2018] 

Diferentes asociaciones campesinas y étnicas de Urabá y el occidente de Antioquia, se encuentran realizando una movilización sobre la vía Medellín-Urabá. Protestan por la **persecución, estigmatización y asesinato de líderes sociales** como también por los incumplimientos por parte del Gobierno Nacional frente a los acuerdos firmados en la Habana. Han pedido que se establezca una mesa de negociación para que se tengan en cuenta los planes de vida y desarrollo de las comunidades.

Ante la situación la población campesina de la zona publicó un pliego de peticiones para presentarle al Gobierno Nacional en donde hacen referencia a preocupaciones, como por ejemplo,  la construcción de peajes **cada 30 km en la región del Urabá**, pues indican que se debieron haber construido “cada 80km como dice la Ley”.

Además, argumentan que debe haber una **socialización con las comunidades** afectadas por el megaproyecto Mar 2, la doble calzada de Urabá, teniendo en cuenta que fueron desalojadas para la construcción del proyecto. Ante esto, han pedido que se establezca una mesa de participación que cuente con la presencia de la Agencia Nacional de Infraestructura.

### **Campesinos exigen reconocimiento de pequeños productores de hojas de coca** 

Dentro de sus peticiones, los campesinos manifestaron que el Gobierno Nacional y el Ministerio del Interior deben reconocer a las familias pequeñas productoras de hoja de coca que **aceptaron el plan de sustitución voluntaria** de los cultivos de uso ilícito. Manifestaron que hubo un acuerdo concertado con las comunidades para realizar este plan, pero mientras tanto el gobierno ha continuado con la erradicación forzada en algunos territorios.

De igual forma, han pedido que se ponga en marcha el punto de la **reforma rural integral** para “garantizar las tierras y la legalización de las mismas a nuestras comunidades indígenas, campesinas y afro descendientes del territorio”. (Le puede interesar:["Aumentan amenazas contra líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

A su vez dijeron que debe haber un plan de desarrollo en Antioquia que **involucre a las diferentes comunidades** “dado que el conflicto en nuestros territorios en su gran parte es el resultado de un plan de desarrollo no participativo que ha generado grandes inequidades”.

### **Comunidades están en alerta por desprotección del Estado** 

De acuerdo con Agustín Tobón, habitante de Dabeiba, Antioquia e integrante de la Cumbre Agraria étnica y popular, el plan de desarrollo el departamento se ha enfocado en los **grandes proyectos de infraestructura** “que desplazan y cambian la vocación de los campesinos y que suman en la más grande pobreza a la mayor parte de la población”.

Las comunidades, ven con preocupación **la continuación de los asesinatos de líderes sociales** y la presencia de diferentes grupos armados sin que el Gobierno Nacional preste atención a lo que está ocurriendo. “Se ha puesto en peligro la participación de la sociedad  y se ha roto todo el lazo entre la comunidad y el Estado”, indicó. (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos": Líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Teniendo de presente todas estas situaciones, han pedido que las autoridades a nivel nacional hagan presencia en el territorio para consolidar la **mesa de participación** pues “la comunidad quiere una negociación colectiva para atender la problemática de manera integral donde las familias no se vean afectadas” y que se involucren las visiones de desarrollo de ellos y ellas.

<iframe id="audio_23643273" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23643273_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
