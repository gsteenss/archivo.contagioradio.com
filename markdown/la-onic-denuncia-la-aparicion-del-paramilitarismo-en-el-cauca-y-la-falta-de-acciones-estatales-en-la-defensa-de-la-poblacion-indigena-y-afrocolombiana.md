Title: En riesgo indígenas caucanos por persistencia paramilitar
Date: 2016-04-08 16:24
Category: DDHH, Nacional
Tags: Aumento paramilitarismo, comunidades indígenas, estado
Slug: la-onic-denuncia-la-aparicion-del-paramilitarismo-en-el-cauca-y-la-falta-de-acciones-estatales-en-la-defensa-de-la-poblacion-indigena-y-afrocolombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ONIC-e1460134357679.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Los Mundos de Hachero] 

###### [8 Abril 2016 ]

La Organización Nacional de Indígenas de Colombia ONIC, denunció en rueda de prensa la **persistencia de grupos paramilitares en el Cauca** y la falta de acciones estatales para dar solución la problemática de seguridad y persecución de la que son objeto las comunidades indígenas y afrocolombianas de esta región del país.

De acuerdo con Luz Eyda Julicue, Consejera de la 'Asociación de Cabildos Indígenas del Norte del Cauca' ACIN, **líderes e integrantes de los procesos organizativos indígenas de este departamento han recibido amenazas** por parte de miembros paramilitares quienes con armas se han visto merodeando constantemente los territorios.

"Se han registrado muchas violaciones a derecho humanos a los pueblos indígenas, hemos tenido casos de desaparición forzada, asesinatos, tortura, amenazas, pero también hemos vivido la violación a los territorios, cuando la fuerza pública ocupa un sitio sagrado o las reservas, cuando las destruyen violan nuestros derechos humanos, hemos generado una serie de **demandas por todos los hechos que han ocurrido y aún no tenemos respuesta del Estado**", aseguró Gabriel Pavi, Gobernador del resguardo indígena de Toribio.

Durante la rueda de prensa la ONIC ratificó su apoyo a los Diálogos de Paz de La Habana y su apuesta por construir una **paz duradera y estable que mejore las condiciones de vida de las comunidades indígenas**, por lo que piden a los equipos negociadores permitir la presencia de una delegación indígena y afrodescendiente para la discusión de las problemáticas que enfrentan, así como sus apuestas de construcción de paz.

La próxima semana la ONIC presentará el proyecto de revitalización de los territorios de vida, como resultado de las discusiones de los diferentes resguardos indígenas frente a sus concepciones de paz, en las que se establece entre otras, que la **reparación no debe sólo ser de carácter individual**.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
