Title: Las razones para que el Gobierno mantenga las zonas de reincorporación
Date: 2018-12-28 15:21
Author: AdminContagio
Category: Nacional, Paz
Tags: carlos medina gallego, etcr, FARC, ONU, Raúl Rosende
Slug: gobierno-zonas-reincorporacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/zona-veredal-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Dic 2018] 

Según lo estipulado en el Acuerdo de Paz firmado en noviembre de 2016 entre Gobierno y FARC, **los espacios en los cuales se concentrarían los ex-guerrilleros existirían con apoyo estatal hasta agosto de 2019.** No obstante, diferentes voces han señalado varias razones por las cuales es necesario extender la vigencia de estos apoyos, y plantear nuevas fechas para el fin de los mismos.

Una de las voces más importantes que ha promovido la prorroga de los 24  Espacios Territoriales de Capacitación y Reincorporación (ETCR's) es la de Raúl Rosende, director de la misión de verificación de las Naciones Unidas en Colombia. En una entrevista con el diario El Tiempo, **Rosende manifestó que era necesario continuar el abastecimiento a los ETCR más allá de este año**, y pensar en extender el plazo de funcionamiento mismo de los Espacios.

De igual forma, el profesor de la Universidad Nacional que participó en el censo realizado por esa institución a los exguerrilleros en 2016, **Carlos Medina Gallego**, sentenció que el jefe de la delegación de la ONU tenía razón; y era necesario prolongar la atención a los ETCR dados los grandes problemas que tuvieron estos espacios al iniciar el proceso de reincorporación. (Le puede interesar: ["Sin vida fueron encontrados los cuerpos de dos integrantes de FARC"](https://archivo.contagioradio.com/sin-vida-integrantes-farc/))

Medina afirmó que "estos espacios se han ido convirtiendo en refugio de pobres del campo porque los exguerrilleros, al reunirse con sus familias, encontraron una situación crítica y no tuvieron otra opción que refugiarse en los ETCR"; situación que se suma a **problemas muy graves que están enfrentando para constituirse a sí mismos en zonas donde se desarrollen proyectos productivos exitosos**, capaces de sostener a los exguerrilleros.

La preocupación radica en que **las posibilidades económicas de los Espacios no permite vivir de forma digna**, situación que se evidencia en que solo 2 proyectos productivos formulados por exmiembros de las FARC han recibido el apoyo económico para su funcionamiento, 12 más están marchando pero de esos, únicamente 5 tienen dinero para sostenerse financieramente. (Le puede interesar: ["Duque, no desconozca el camino ya andado: FARC"](https://archivo.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc/))

### **Exguerrilleros enfrentan los mismos problemas que el campesinado** 

En múltiples ocasiones el movimiento campesino ha señalado que es necesario establecer mercados para comercializar los productos que se cosechan, esta acción requiere la construcción de vías terciarias y secundarias para sacar las cosechas de sus lugares de origen, así como entablar lazos comerciales con el extranjero. La misma situación afecta a **los excombatientes que logran generar una producción agrícola, pero al estar desconectada de mercados locales e internacionales, termina generando perdidas.**

De igual forma, Medina sostuvo que **no se ha logrado asignar en propiedad los terrenos en los cuales están los ETCR, mientras los costos de arrendamiento son altos**. Aunque en algunos Espacios se han intentado comprar predios para lograr la titulación de la tierra, los arrendatarios han inflado su costo a valores imposibles de pagar, por lo tanto, **después de agosto de 2019 los exguerrilleros quedarían como invasores de esas tierras**.

### **Compromiso del Estado con excombatientes garantiza implementación del Acuerdo** 

Medina manifestó que el plazo de funcionamiento de los ETCR está limitado por la capacidad de éxito que tengan para garantizar la vida de los exguerrilleros, y agregó que **otra buena razón para mantener los Espacios es la facilidad para que las instituciones del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), cumplan con su mandato.** (Le puede interesar: ["FARC se presenta ante la JEP"](https://archivo.contagioradio.com/ex-integrantes-de-las-farc-comparecen-ante-la-jep/))

Con los Espacios funcionando la Jurisdicción Especial para la Paz (JEP), la Comisión de la Verdad y la Unidad de Búsqueda de Personas Desaparecidas podrían tener a disposición los testimonios de quienes se encuentren allí concentrados, facilitando su labor y garantizando la ya difícil labor de impartir justicia, construir la verdad para las víctimas y trabajar por la no repetición de los hechos.

Por estas razones, y teniendo en cuenta que ha sido "la solidaridad internacional la que ha acompañado los proyectos productivos más exitosos", **Medina pidió al Gobierno que asuma una política que garantice las condiciones de económicas, sociales y políticas de vida digna que garantice el no retorno a la violencia de los exguerrilleros**; de igual forma, esperó que se ponga en consideración la situación actual de los espacios de tal forma que se prorrogue el plazo de funcionamiento de los ETCR.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
