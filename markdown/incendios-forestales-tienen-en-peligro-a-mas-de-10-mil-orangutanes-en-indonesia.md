Title: Incendios forestales tienen en peligro a más de 10 mil orangutanes en Indonesia
Date: 2015-10-23 13:14
Category: Animales, Voces de la Tierra
Tags: 10 mil orangutanes en peligro, animales en vías de extinción, Bernat Ripoll, cigüeña de Storm, Fenómeno de El Niño, gibones del sur de Borneo, incendios forestales, Indonesia, las Selvas de Borneo, Orangunates, Orangutan Tropical Peatland Project, OuTrop, Pantera nebulosa, Parque Nacional de Sabangau, Selvas de Borneoç
Slug: incendios-forestales-tienen-en-peligro-a-mas-de-10-mil-orangutanes-en-indonesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/9397519_orig.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [pasionanimalblog.tumblr.com]

###### [23 Oct 2015]

Aproximadamente **10 mil orangutanes, que habitan en el Parque Nacional de Sabangau  en Indonesia** se encuentran en peligro debido a la gran cantidad de incendios forestales ocasionados por empresas interesadas en la explotación de los bosques para la **creación de monocultivos.**

Se trata de la población de orangutanes más grande del mundo, que se ha visto afectada en la última semana por la generación de 238 incendio.

Mediante un video, la organización Orangutan Tropical Peatland Project (OuTrop), denuncia la situación, el director del proyecto Bernat Ripoll, asegura que “**Estos fuegos, provocados por terratenientes y grandes empresas son no sólo una amenaza para la biodiversidad,** sino también para los cientos de miles de habitantes de Kalimantan, que están sometidos a las emisiones de humos permanentes”, generando graves efectos para la salud humana y para la superviviencia de los animales.

Ripoll, añade que por cuenta de la actividad empresarial el fenómeno climático de El Niño ha aumentado, **produciendo inundaciones y sequías extremas en los trópicos más fuertes desde 1997** generando graves pérdidas ecológicas, económicas y sociales. De acuerdo con el biólogo, se espera que El Niño irá hasta enero 2016 con consecuencias iguales o peores que las de 1997.

El Parque de Sabangau, representa una zona de biodiversidad muy importante, teniendo en cuenta que además de los 10 mil orangutanes, tiene otras  especies emblemáticas y amenazadas como **los gibones del sur de Borneo, la Pantera nebulosa y allí, también se encuentran los 500 últimos ejemplares de cigüeña de Storm.**

Mientras tanto, "OuTrop está trabajando para proteger las Selvas de Borneo, luchando los fuegos con los equipos locales de extinción de incendios desde hace años, educando para la concienciación, empoderando a la gente local y haciendo presión política a diferentes niveles. A pesar de los esfuerzos y la lucha diaria, nos faltan recursos, los incendios queman y destruyen a diario cientos de hectáreas en el Parque Nacional de Sabangau, poniendo en peligro **la selva continúa mas grande del sur de Borneo, las especies que habitan y los recursos que proveen a las comunidades**"concluye Bernat Ripoll.

https://youtu.be/oXCK7MXDJjY
