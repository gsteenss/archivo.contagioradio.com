Title: Hidroeléctrica Piedra del Sol no tiene viabilidad ambiental
Date: 2016-10-19 13:39
Category: Ambiente
Tags: defensa ambiental, ISAGEN, Tutela contra Hidroeléctrica
Slug: hidroelectrica-piedra-del-sol-no-tiene-viabilidad-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Captura-de-pantalla-2016-10-19-a-las-12.57.22.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ISAGEN] 

###### [19 Oct 2016 ] 

Con el fin de denunciar que ISAGEN no ha garantizado la efectiva participación de las comunidades que se verán afectadas por la construcción de la hidroeléctrica Piedra Del Sol, el pasado 31 de diciembre de 2015,  **las comunidades afectadas junto a la Corporación Colectivo de Abogados Luis Carlos Pérez, interpusieron una acción de tutela **ante la ANLA para poner freno al Proyecto Hidroeléctrico Piedra del Sol en Santander.

Las comunidades de Pinchote, San Gil, Cabrera y Socorro, manifestaron que ISAGEN hasta el momento no ha adelantado la socialización de los resultados finales del Estudio de Impacto Ambiental. Marcela Pérez abogada integrante del Colectivo, señala que “en las reuniones presentaban resultados parciales e inacabados, pues **solo hasta el 2014 y por requerimiento de la ANLA, ISAGEN actualizó el deficiente EIA presentado inicialmente en el 2011”**.

Pérez, comenta que fue necesario interponer una acción de tutela ante la ANLA para que el colectivo y las comunidades pudiesen **conocer la valoración técnica del proyecto, puesto que en ocasiones anteriores se negaron a socializar dicho concepto,** que es el soporte de las decisiones y actos administrativos, que realiza la entidad para decidir si se otorga o no una licencia ambiental.

Al revisar el documento, el colectivo se encuentra con que este proyecto a filo de agua, que quiere instalarse en el río Fonce, **“no tiene viabilidad ambiental, puesto que la información presentada por ISAGEN y HMV ingenieros, es insuficiente y presenta varios fallos”** resalta Pérez. Además, “no hay claridad respecto de las medidas de manejo ambiental en las zonas afectadas, ni se informó claramente sobre los impactos negativos en los lugares donde el proyecto tendrá desarrollo”.

En el documento de acción de tutela, interpuesto por el colectivo, expresamente se evidencia la exigencia de la comunidad: **“Solicitamos se realice una evaluación, socialización y participación suficiente, amplia, informada e incluyente**, de manera que se haga especial énfasis en los impactos sobre el recurso hídrico, teniendo en cuenta las experiencias negativas  de proyectos similares desarrollados con la técnica a filo de agua, ejecutados por ISAGEN”. Le puede interesar: [Ríos más biodiversos del mundo se encuentran en peligro por hidroeléctricas.](https://archivo.contagioradio.com/biodiversidad-de-los-rios-en-peligro-por-represas/)

En estos momentos, la licencia ambiental está en trámite, no se ha emitido una decisión final, señala Marcela Pérez, que allí radica la importancia de una de las etapas del procedimiento de licenciamiento, el concepto técnico de ANLA, “que es previo y contrasta la información que dan las empresas”. Resalta, que también **es importante la socialización del fallo de la tutela interpuesta por el Colectivo**, “para garantizar los derechos de participación e información ambiental de las comunidades”.

Luego del proceso de tutela, el tribunal administrativo, ordenó gestionar la realización de una nueva Audiencia Pública Ambiental para socializar los resultados de la valoración. La abogada manifiesta que este es un precedente para otros territorios, ya que “genera una modificación en cómo se entregan las licencias, pues **siempre el ANLA es pasiva y en esta ocasión deberá socializar la información con las comunidades, antes de expedir la licencia”.**

Por otra parte, Pérez indicó que la técnica de filo de agua, consiste en la instalación de turbinas internas en un túnel, por el que se desvía una parte del caudal del río para generar energía y luego éste vuelve a su cauce. “En Antioquia lo hicieron y quedó demostrado que la construcción del túnel **generó afectaciones a nacederos y cuerpos de agua que nunca volvieron”. **Le puede interesar: [Cambios climáticos afectan con mayor fuerza a la población vulnerable del mundo.](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/)

Frente a ello también se encontraron inconsistencias, pues “ISAGEN presentó estudios de caudales muy superiores a los que realmente tiene el río, que en estos momentos atraviesa graves consecuencias por pasadas sequías” aclara Pérez. Esta es una evidencia más de estas son “decisiones que responden a intereses de particulares y de autoridades que están fuera del territorio, **los daños ambientales no los viven los que se lucran de los proyectos”** concluye Marcela Pérez.

<iframe id="audio_13389320" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13389320_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
