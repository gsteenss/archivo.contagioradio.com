Title: Procesos de formación artísticos para excombatientes también reconstruyen la paz
Date: 2020-07-17 11:25
Author: CtgAdm
Category: Cultura, yoreporto
Tags: arte, FARC
Slug: procesos-de-formacion-artisticos-para-excombatientes-tambien-reconstruyen-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Excombatientes FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Consejo Nacional de Reincorporación, el Partido FARC y ANA DC, invitan a los firmantes del Acuerdo de paz y sus familias a participar en los procesos de formación, danza y creación artística que actualmente se están realizando en la ciudad de Bogotá como resultado de un proceso de articulación con el Instituto Distrital de las Artes (IDARTES) y que representan una serie encuentros donde el cuerpo se presenta como territorio de reconciliación, a partir de ejercicios de creación coreográfica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es importante que los y las firmantes de la paz puedan acceder a esta oferta cultural entendiendo que el derecho a la cultura también hacer parte del ejercicio de reconstrucción de paz", expresan desde el Consejo Nacional de Reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"has-text-align-justify"} -->

Dicha articulación cuenta con los Programa Nidos: procesos de creación artística, experiencias sensoriales y aprendizaje dirigido a las mujeres gestantes, niñas y niños de 0 a 5 años con sus familias. [](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/)[(Le puede interesar: "A pesar de las dificultad, aún se deben sumar esfuerzos para la paz")](https://archivo.contagioradio.com/a-pesar-de-las-dificultad-aun-se-deben-sumar-esfuerzos-para-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma la formación para excombatientes cuenta con los espacios de formación Crea: procesos de formación y creación, para todas las edades:  danza,  arte escénicas, plásticas, audiovisuales, música, literatura y nuevas tecnologías. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si estás interesado llena la siguiente encuesta: <https://forms.gle/ichMKuxsQfNKxtT89>  
  
Para más información sobre la oferta también se puede ingresar a: <https://idartesencasa.gov.co/>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si estás interesado llena la siguiente encuesta:  <https://forms.gle/FFEmncYKNzTXu2DJA>

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
