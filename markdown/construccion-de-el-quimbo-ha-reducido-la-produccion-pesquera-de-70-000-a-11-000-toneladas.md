Title: Producción pesquera del Río Magdalena ha disminuido de 70.000 a 11.000 toneladas
Date: 2015-08-20 15:16
Category: Ambiente, Nacional
Tags: ASOQUIMBO, colombia, EMGESA, Magdalena, Miller Dussán investigador de ASOQUIMBO, Pesca, Plan de Aprovechamiento del Río Magdalena, Quimbo
Slug: construccion-de-el-quimbo-ha-reducido-la-produccion-pesquera-de-70-000-a-11-000-toneladas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.dhradio.co]

<iframe src="http://www.ivoox.com/player_ek_7129888_2_1.html?data=mJafm52cfI6ZmKiakp2Jd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fstrWz8fTb8nVjNfSxtrHrcXjjNHOjdXWs8XpxMjWh6iXaaOnz5Ddx9jVucbmwpDRx5CbdI%2BkkZWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Perdomo, presidente Asociación Pescadores Río Magdalena] 

###### 19 ago 2015 

El presidente de la Asociación de pescadores del Río Magdalena, Alirio Perdomo, denunció que en el municipio del Hobo, en el departamento del Huila, ha descendido de manera alarmante la producción pesquera, como consecuencia de la construcción de la hidroeléctrica El Quimbo, realizada por la empresa **EMGESA, en el marco del Plan de Aprovechamiento del Río Magdalena.**

La preocupación de los pescadores se centra en que la situación actual se debe a la construcción de apenas una represa, de las 19 que contempla **["El Plan Maestro de Aprovechamiento del Río Magdalena”](https://archivo.contagioradio.com/plan-maestro-de-aprovechamiento-del-rio-magdalena-cuando-se-hizo-y-por-quienes/).** Por lo que se espera que los efectos de la implementación de este plan sean  mucho peores.

Debido a ese proyecto en la actualidad aproximadamente **400 habitantes de la región que viven de la pesca artesanal** se ven gravemente afectados, pues su economía y supervivencia depende de esa actividad, así mismo sucede con los pescadores de los municipios de Campo Alegre y Yaguará.

Perdomo indicó que "por la construcción de El Quimbo, por los químicos y las excavaciones en el río, ha aumentado la mortalidad del pescado". De acuerdo con la Autoridad Nacional de Acuicultura y Pesca, AUNAP, en tan solo once años, **la producción pesquera ha descendido de 70.000 a 11.000 toneladas durante los últimos 40 años.  
**

Pese a que la comunidad ha estado trabajando de la mano del investigador de **ASOQUIMBO,** Miller Dussán, para demostrar que EMGESA ha afectado el ambiente y la vida de las familias que viven del río Magdalena, la empresa no ha querido reconocerlos como víctimas, razón por la cual, **la comunidad exige la realización de un censo en la zona de influencia,** para que se tomen las medidas correspondientes y se atienda a los afectados.
