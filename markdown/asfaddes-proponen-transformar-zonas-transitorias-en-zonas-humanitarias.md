Title: ASFADDES propone transformar zonas transitorias en zonas humanitarias
Date: 2016-10-06 17:03
Category: Nacional, Paz
Tags: acuerdo de paz, acuerdo de victimas
Slug: asfaddes-proponen-transformar-zonas-transitorias-en-zonas-humanitarias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/asffades.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 de Oct 2016] 

[La Asociación de Familiares de Detenidos Desaparecidos señalaron en un comunicado de prensa que el gobierno **debe cumplir el mandato constitucional de ratificar el proceso de paz con las FARC-EP y avanzar en los diálogos con las guerrillas del ELN y el EPL,** además proponen que se conforme un Frente Amplio por la Paz, en el que participen  organizaciones sociales y de víctimas.]

En el comunicado, ASFADDES también expresan que las **zonas que se habían establecido en el proceso de paz, como transitorias, se transformen en zonas humanitarias y territorios de paz**, para evitar enfrentamientos entre las Fuerzas Militares y las FARC-EP, de igual modo le solicitan a la comunidad internacional que hagan acompañamiento de estos escenarios para que no se vulnere ningún derecho.

De acuerdo con Gloria Gómez, coordinadora de ASFADDES, por parte de los movimientos de víctimas no se aceptarán cambios en el acuerdo que afecten los derechos de las mismas, “**creo que hacer arreglos a espaldas de las víctimas y de los sectores sociales no garantiza una construcción de un verdadero acuerdo** que repare y recoja las propuestas que el movimiento social tiene”

Frente a las propuestas que ha expuesto el Centro Democrático, Gómez considera que se debe penalizar tanto la conducta y violación de derechos humanos cometidos por los miembros de las FARC-EP como los de las Fuerzas Militares, ya que esto[modificaría el punto quinto sobre víctimas y ampliaria el marco de la impunidad.  ](https://archivo.contagioradio.com/estos-son-los-5-puntos-que-se-firmaran-en-la-habana-con-el-cese-bilateral/)

[ASFADDES informó que a partir de los resultados del 2 de octubre, estarán en las calles con la galería de la memoria para hacer visible y dignificar la memoria de sus seres queridos desaparecidos detenidos. Esta organización  demás de 35 años de trayectoria **ha vivido todos los procesos de desmovilización de diferentes grupos armados** del país, motivo por el cual señalan que es importante que no se fracture la agenda y las propuestas que para este acuerdo, gestaron las víctimas.]

<iframe id="audio_13210354" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13210354_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
