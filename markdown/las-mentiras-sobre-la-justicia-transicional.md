Title: Las mentiras sobre la Justicia Transicional
Date: 2016-03-07 12:36
Author: AdminContagio
Category: Cindy, Opinion
Tags: Amnisitia, justicia transicional, proceso de paz
Slug: las-mentiras-sobre-la-justicia-transicional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/amnistia-proceso-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Delegación de paz FARC 

#### **[Cindy Espitia Murcia](https://archivo.contagioradio.com/cindy-espitia/) - [@CindyEspitiaM](https://twitter.com/CindyEspitiaM)** 

###### 7 Mar 2016 

[Mucha información circula diariamente sobre la Justicia Transicional a través de medios de comunicación, redes sociales y memes. Y no es para menos. Sin lugar a dudas, éste es uno de los puntos más controversiales e importantes del Proceso de Paz, pues debe apuntar a 3 objetivos diferentes de manera simultánea: Primero, responder a la incansable búsqueda de verdad y justicia de las cerca de 8 millones de víctimas que ha dejado el conflicto armado; segundo, mantener los diálogos con las Farc hasta la firma del acuerdo final – lo que supone necesariamente una mutua concesión, ya que]**se está ante una negociación y no ante un sometimiento a la justicia**[ – y, por último, lograr un acuerdo que esté blindado jurídicamente, en cumplimiento de los estándares de Derecho Internacional sobre la materia.]

No obstante, se han ido arraigando en el discurso, especialmente de la oposición, diferentes afirmaciones que, aunque gozan de popularidad, recordación y difusión, carecen ciertamente de profundidad y, como se demostrará, de precisión.

La premisas más reiteradas, que se abordarán en esta columna, están relacionadas con las amnistías. Por un lado, se ha señalado que están completamente prohibidas en el Derecho Internacional y que cualquier disposición al respecto supone el incumplimiento del Estado al deber de investigar, juzgar y sancionar.

[Esta afirmación es falsa. La Corte Interamericana, y en general los diferentes órganos internacionales de derechos humanos, no se han opuesto en términos generales a las amnistías. De hecho, en el caso ‘Masacres del Mozote vs. El Salvador’, el Tribunal, con base en las normas del DIH, estableció que en los procesos de transición de conflicto armado a paz, deberá otorgarse la]**amnistía más amplia posible**[, salvo a los responsables de cometer crímenes de guerra y delitos de lesa humanidad.]

Si bien es cierto que la Corte IDH ha declarado la responsabilidad internacional de Estados como Chile, Uruguay, Perú, El Salvador y Brasil y ha ordenado la derogación de sus leyes de amnistía, los argumentos que llevaron al Tribunal a tomar tal decisión no se centran en la mera promulgación de este mecanismo de justicia transicional sino en el otorgamiento del beneficio a responsables de cometer graves violaciones a los derechos humanos y a la falta de condiciones que eviten la consolidación de un obstáculo infranqueable al acceso a la justicia.

Si realmente las amnistías estuvieran proscritas, las disposiciones de 24 procesos de paz llevados a cabo en América, África y Europa, en los últimos 30 años, carecerían de efectos jurídicos, afectando la estabilidad y duración de la paz alcanzada.

[Por otro lado, se ha señalado de forma reiterada que todos los guerrilleros de las Farc, sin importar sus crímenes cometidos, gozarán de una]**amnistía incondicionada**[. Esto también es falso. Basta sólo con leer rápidamente el comunicado sobre Justicia Transicional para concluir, de manera inequívoca, que los responsables de cometer crímenes internacionales no recibirán tal beneficio y que quienes sean elegibles para acceder a la amnistía deberán comprometerse a dejar las armas, reinsertarse en la vida civil, reconocer su responsabilidad y contribuir a la ‘satisfacción de los derechos de las víctimas a la verdad, reparación y no repetición’.]

Así, la difusión de estas afirmaciones no demuestran más que la mala fe de los líderes políticos que las promulgan, pues ellos deberían contar con la obligación, al menos ética, de contrastar la información antes de publicarla, y la falta de interés y diligencia de los colombianos que, a pesar de contar con las herramientas para acceder a datos verídicos disponibles, prefieren ‘retuitear’ un meme o una declaración, sin profundizar al respecto.

Colombianos: La construcción de una paz firme y duradera requiere necesariamente de un disenso y un consenso con fundamento. Nada puede ser más perjudicial que ‘infinito sí’ y un ‘infinito no’ sin un sustento verídico.
