Title: Nueve personas han sido asesinadas a manos de paramilitares en Córdoba
Date: 2016-02-17 21:18
Category: DDHH, Nacional
Tags: cordoba, Paramilitarismo
Slug: nueve-personas-han-sido-asesinadas-a-manos-de-paramilitares-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [17 Feb 2016] 

La Asociación de Campesinos del Sur de Córdoba, denuncia que la población se encuentra atemorizada por las últimas acciones de tipo paramilitar que se viven en el departamento. En el último hecho asesinaron nueve personas que hacían labor social con los jóvenes de la comunidad.

De acuerdo con uno de los líderes de la zona, en municipios del Bajo y Alto Sinú cordobés en menos de ocho días han ocurrido nueve asesinatos bajo la misma modalidad criminal, pese a que en el departamento hay gran presencia militar. **"Nos preocupa el silencio de la dirigencia política de Córdoba frente a amenazas y asesinatos, vemos a los militares tomando cerveza con los paramilitares”**, dice líder el campesino.

Panfletos amenazando con limpieza social, es otra de las modalidades que las estructuras paramilitares pertenecientes a las denominadas Autodefensas Gaitanistas de Colombia o Clan Úsuga, están usando para generar pánico en la población. En los panfletos, anuncian los nombres de las personas que asesinarían, entre ellos se encuentra en nombre de 7 defensores de derechos humanos.

“El problema de raíz, que debe ser abordado y confrontado es el paramilitarismo que nunca ha dejado de existir en los pueblos del Alto Sinú, zona San Jorge, en la ciudad capital y zona costanera; la razón: **EL CONTROL DEL NARCOTRAFICO y la dominación NARCO-POLITICA", dice la denuncia de la asociación de campesinos del sur de Córdoba**.

 La población exige una actuación inmediata por parte de las autoridades regionales y nacionales para que se garantice la libertad, seguridad, derecho a la vida y la paz.
