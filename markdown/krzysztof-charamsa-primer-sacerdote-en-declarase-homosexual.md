Title: Monseñor Krzysztof Charamsa se declara homosexual
Date: 2015-10-05 17:26
Category: El mundo, LGBTI
Tags: Federico Lombardi, Hermano Alfonso, Homosexualidad, Krzysztof Charamsa, LGBTI, Papa Francisco, Vaticano
Slug: krzysztof-charamsa-primer-sacerdote-en-declarase-homosexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Krzysztof-Charamsa-primer-sacerdote-en-declarase-homosexual.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.elperiodico.com]

###### [5 Oct 2015]

Monseñor Krzysztof Charamsa, sacerdote del Vaticano reveló su homosexualidad el pasado sábado, por medio  de una  entrevista en el Il Corriere della Sera,  **el  diario más  importante  de  Italia, donde expresó que se  siente  orgulloso de  ser gay.**

Esa declaración se dio un día antes de que se iniciara el Sínodo Ordinario de Obispos para la Familia, que se celebrará hasta el próximo 25 de octubre y en el que se debatirá sobre temas como el trato a los divorciados o a los homosexuales.

**"Quiero que la iglesia y mi comunidad sepan quién soy: un sacerdote homosexual, feliz y orgulloso de su identidad"**, expresó Charamsa.

Charamsa llevaba  17  años  ejerciendo  en  Roma siendo   oficial  de  congregación  De la Doctrina  de La Fe, también era  secretario  de la  comisión  Teológica Internacional del Vaticano, "estoy dispuesto a pagar las consecuencias, pero es hora de que la Iglesia abra sus ojos a los creyentes homosexuales", dijo el sacerdote tras conocer que había sido expulsado por el Vaticano.

**Las  declaraciones  de Krzysztof   fueron   calificadas  como “irresponsables”  por  parte  de Federico Lombardi portavoz  del Vaticano.**  Puesto que  fueron  en vísperas  del Sínodo ordinario para  la  familia, reunión internacional  de  obispos para  tocar temas de  importancia  frente  a  la  conformación  de la  familia.

Por otra parte, el sacerdote en formación Hermano Alfonso, califica de “doble  moral”  califica   el  comportamiento de  este  sacerdote, puesto que  el  sacerdocio  y el celibato es  una  decisión. **“Dura  más  de  8  años  preparase  para  ser sacerdote, en donde es importante   alejarse  de  los pecados carnales”…** “Existen  sacerdotes   con  muchos  años  de  celibato  y  no lo ven  como inhumano” expresó el feligrés.
