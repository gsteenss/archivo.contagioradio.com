Title: Amnistía Internacional: "Se siguen vulnerando los DDHH pese a los avances en el proceso de paz"
Date: 2015-02-25 16:20
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Amnistía Internacional, colombia, Derechos Humanos, Fuerzas militares, Informe 2014
Slug: amnistia-internacional-en-colombia-se-siguen-vulnerando-los-ddhh-pese-a-los-avances-en-el-proceso-de-paz
Status: published

##### Foto: Contagio Radio 

En el informe 2014 de Amnistía Internacional, sobre la situación de los derechos humanos en el mundo, se trata específicamente el caso colombiano. Allí, se concluye que “pese a las negociaciones de paz en curso, **ambas partes siguen cometiendo violaciones de derechos humanos y del derecho internacional humanitario** (DIH), al igual que lo hacen los grupos paramilitares, ya fuera actuando solos o con la connivencia o aquiescencia de sectores de las fuerzas de seguridad”.

Puntualmente se refiere a los derechos de los pueblos indígenas, las comunidades afrodescendientes y campesinas, las mujeres y niñas, los defensores y defensoras de los derechos humanos, los activistas comunitarios y los sindicalistas. Las víctimas se ven afectadas por **“desplazamiento forzado, los homicidios ilegítimos, toma de rehenes y secuestros, amenazas de muerte, desapariciones forzadas, tortura y violencia sexual”,** según evidencia el informe.

Pese a que Amnistía Internacional asegura que las conversaciones de paz han avanzado y son necesarias para contribuir a la defensa de los derechos humanos, en el documento se evidencian cifras alarmantes sobre las víctimas del conflicto armado, ya que e**n 2013 se registraron 30 homicidios y 3.185 víctimas de desplazamiento forzado.**

También, se refieren al tema de la protesta social, donde se habla sobre el paro agrario del 2013, en el que las autoridades afirmaron que había infiltrados de grupos guerrilleros, lo que generó que los campesinos y campesinas, fueran blanco de represalias por parte de los paramilitares.

Por otro lado, respecto al apartado sobre “Fuerzas de seguridad”, se subrayan **las 48 ejecuciones extrajudiciales cometidas por las fuerzas militares durante 2013,** aunque en el periodo del mandato de Álvaro Uribe Vélez, el número de casos fue mucho más alto, según Amnistía Internacional.

Así mismo, el informe aborda temas como la Ley de Justicia y Paz, los grupos guerrilleros, los derechos de las mujeres y niñas, la impunidad, restitución de tierras, situación de los defensores y defensoras de derechos humanos, la ayuda de Estados Unidos y el escrutinio internacional.
