Title: La versión de los jóvenes capturados por el atentado en el Centro Comercial Andino
Date: 2018-02-02 11:49
Category: DDHH, Entrevistas
Tags: Boris Rojas, Centro comercial andino, Lizeth Rodríguez, Universidad pública
Slug: entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fot0: Contagio Radio] 

###### [02 Feb 2018] 

En entrevista para los micrófonos de Contagio Radio, Boris Rojas y Lizeth Rodríguez, detenidos por los hechos del pasado 17 de junio de 2017, cuando un artefacto explosivo detonó en el Centro Comercial Andino, re afirmaron que son inocentes y que todo el despliegue mediático que se ha hecho culpabilizándolos, hace parte de una estrategia por parte de las autoridades para demostrar resultados.

Boris y Lizeth contestaron esta entrevista en representación de sus demás compañeros sindicados de haber participado en estos hechos: Natalia Trujillo, Andrés Bohorquez, César Barrera, Alejandra Méndez, Juan Camilo Pulido, Cristian Sandoval, Lina Jiménez e Iván Ramírez.

### **Contagio Radio: ¿Quiénes son realmente los detenidos y acusados del atentando en el Centro Andino, entre otras situaciones de bombas panfletarias ubicadas en Bogotá y otras ciudades?** 

Boris Rojas: Nosotros somos estudiantes y egresados la mayoría de la Universidad Nacional, nos conocimos desde hace mucho tiempo en la universidad viendo materias juntos y en general, compartiendo en las cafeterías y en la vida universitaria. Eso cimentó nuestra relación como grupo de amigos que siempre ha estado preocupada por la discusión acerca de la situación del país y de la situación social.

No hay que buscarle más patas al gato, muchas de las cosas que han querido plantear los medios de comunicación son completamente falsas, nosotros no somos un grupo de espías especializados, ni venimos del extranjero, no hay mayor misterio en eso.

### **CR: Es decir que ustedes eran un grupo de amigos y se reunían constantemente** 

BR: Permanentemente, hay entre nosotros parejas constituidas desde hace mucho tiempo, compartíamos y vivíamos juntos. Es muy fácil recoger las fotos de los grados de nosotros y ahí vamos a estar todos, siempre los mismos y otra cantidad de gente muy cercana que afortunadamente no se vio envuelta en este problema.

### **CR: ¿Cómo ha sido vivir toda la exposición y señalamientos por parte de los medios de información?** 

LR: Ha sido una situación bastante compleja porque desde el comienzo, en el momento de la captura, empezó una filtración deliberada y sistemática de información que presentan como pruebas, por eso es importante señalar que, en primer lugar no se puede hablar de pruebas, lo que se presenta es información manipulada que está apuntando a un doble juicio, es más, son prejuzgamientos que ejercen presión sobre el aparato judicial y sobre los jueces que llevan nuestro caso.

En términos de defensa técnica es muy difícil que un abogado nos defienda con todo este argumento procesal, afortunadamente nos hemos encontrado con una solidaridad muy grande de parte de amigos, familiares e incluso de organizaciones defensoras de de derechos humanos que han hecho incidencia y presión para demostrar que todos esos señalamientos son completamente falsos y errados.

Es muy difícil cuando hay un señalamiento público que no está quieto, sino que realmente ejerce una presión como lo manifestó la Juez 47 de control de garantías cuando dijo que estaba siendo presionada para dar un fallo. Y no es solo una presión mediática, es cómo los medios han mostrado al Fiscal general de la Nación, al Presidente, al Alcalde Peñalosa diciendo que se necesita una sanción ejemplarizante. Tenemos a Semana haciendo este despliegue desmedido y declarando nuestra culpabilidad, construyendo sobre información falsa la teoría de nuestra culpabilidad.

Hemos tratado de asumir, no solo nosotros sino quienes nos rodean, de una manera muy digna y clara, insistir que no hay pruebas de los señalamientos y que no tenemos nada que ver con lo que se nos acusa.

### **CR: ¿Las pruebas que se han mostrado en los medios de información son ciertas, les pertenecen o son producto de los allanamientos?** 

BR: Es muy probable que en 15 allanamientos que hizo la Fiscalía hayan encontrado celulares, es posible, está dentro de lo normal, en uno de los allanamientos que se hicieron había un san alejo, cuarto de chécheres, y ahí entiendo yo que sacaron una cantidad de celulares viejos, cables y cosas que han mostrado como insumos para fabricar explosivos, y ahí la Fiscalía y la Policía han orientado de una manera tendenciosa esos hallazgos.

### **CR: Es decir… ¿nada de lo hallado puede ser relacionado con los explosivos ubicados en el Centro Comercial Andino y en otros lugares del país?** 

BR: A mí me parece que es muy difícil, eso no tiene un asidero real y es parte del trabajo que ha hecho la Fiscalía para ambientar los vacío que ellos tienen en su trabajo y para ambientar y presionar una decisión de los jueces.

### **CR: Concretamente ¿en qué allanamiento fue que se inspeccionó el cuarto de San Alejo?** 

BR: En el allanamiento que se hizo a la casa del papá de César, e incluso hay que decirlo, le robaron unos relojes y unas cuestiones personales que se les perdieron.

### **CR: Si las pruebas son encontradas en la casa de una sola persona, ¿por qué se imputan al grupo completo?** 

BR: Hay que hacer una valiosa aclaración, por ejemplo, la Juez 47 de control de garantías y el juez 48 de conocimiento, fueron capaces de mentir de manera descarada diciendo que se habían encontrado explosivos cuando eso nunca sucedió en ninguno de los allanamientos que se hicieron.

El Juez 48 de conocimiento dijo que en uno de los allanamientos se encontraron chispitas de vaquero y agregó que con un cierto liquido especial se podrían convertir en un peligroso explosivo, y con eso, justificó la refrendación de la medida de aseguramiento. Hay muchas mentiras.

### **CR: ¿Ninguna de esas pruebas ha sido presentada en la etapa de juicio?** 

BR: No, ni siquiera la Fiscalía fue capaz de decir ese tipo de cosas, la Fiscalía presentó unas chispitas en vaquero como si fueran el precursor de un peligroso explosivo, ahí hay una mentira vulgar.

### **CR: ¿Qué de lo que se ha mostrado en medios y de lo que se ha dicho por fuera, ha aparecido realmente en el proceso?** 

BR: Teléfonos han aparecido en el proceso, que se encontraron en los allanamientos. Así como usted tiene una cantidad de teléfonos viejos que se le pierden, que se le van dañando, que se le van quedando, de ese tipo de cosas la Fiscalía se ha valido para sustentar que son insumos para preparar explosivos.

### **CR: ¿Pasamontañas, banderas alusivas al MRP, guantes, overoles, eso fue encontrado en sus casas?** 

BR: No sé si en alguna de las casas se encontraron banderas del MRP o pasamontañas, la Fiscalía lo mostró, eso me parece que tiene que verificarse en la cadena de custodia y en las actas de allanamiento.

Actas que por lo menos, nosotros no hemos conocido y tenemos serias dudas sobre la cadena de custodia, que tiene que ver con un enfoque que ha venido planteando la Fiscalía y la mayoría de jueces que han afrontado el caso, allí usan la transversalidad de pruebas en donde las pruebas que se le encuentren a uno se le adjudica a todo el grupo, entonces hay que verlo con detalle.

### **CR: ¿Es decir que las pruebas se están planteando de forma conjunta?** 

BR: Así lo han venido planteando todos los jueces y la teoría de la Fiscalía. Entonces con la transversalidad de pruebas se saltan la cadena de custodia, por ejemplo, en la casa de Iván Ramírez hicieron un allanamiento ilegal, sin actas, sin ningún tipo de herramienta técnica para verificar las cosas que se llevaron de la casa de Iván. Sin embargo, esas cosas que supuestamente le quitaron y que le adjudicaron a Iván fueron declaradas ilegales por una Juez que se vio obligada hacerlo, cuando las pruebas fueron declaradas ilegales simplemente se le pasaron a otro, porque la transversalidad de pruebas, según la Fiscalía, le permite adjudicar pruebas, aunque sean ilegales, a cualquier otro de los imputados.

A mi particularmente, no me quitaron ni computadores ni USB, es decir ningún elemento de almacenamiento electrónico, sin embargo, la Fiscalía sustenta que a mí me quitaron 2 computadores y varias USB que no sabemos de dónde salieron porque como le digo, en muchos casos las actas de allanamiento ni siquiera se las dejaron leer a las personas que estaban presentes, en muchos casos las actas de allanamiento ni siquiera se elaboraron en los lugares de los hechos y en otro casos no había nadie en las casas que pudiera verificar el procedimiento.

### **CR: ¿De dónde salen los alias y el lugar que se le da a cada uno de ustedes al interior de la estructura del MRP?** 

BR: Los alias cualquiera puede darse cuenta que son ridículos, cómo vamos a tener de alias nuestros propios nombres. La Fiscalía no sé qué trabajo ha hecho ahí, pero quiere a toda costa demostrar que existe una estructura donde no existe, quieren demostrar la pertenencia al MRP, y a partir de demostrar nuestra pertenencia, sustentar toda su acusación y teoría del caso y para hacerlo se inventan la estructura. Los alias son un ejemplo de esa improvisación.

En algunos casos usaron los nombres de los sitios de trabajo los lugares en dónde las personas trabajaron. El mismo Fiscal ante el juez segundo especializado tuvo que reconocer que esos alias, que ellos filtraron a los medios de información, nos los había puesto la misma Policía.

(Le puede interesar: ["Cárcel para implicados en el caso Andino esta motivada por posturas ideológicas"](https://archivo.contagioradio.com/carcel-para-implicados-en-caso-andino-esta-motivada-por-posturas-ideologicas/))

### **CR:¿ Por qué consideran entonces que son ustedes capturados?** 

BR: Para nosotros es claro que nos estaban siguiendo, todos los días nos encontrábamos policías. Un día yo los abordé y les pregunté cuál era el problema. Yo creo que, a partir de nuestra preocupación por la universidad, de estar discutiendo en todos los eventos y ámbitos universitarios alrededor de la situación política del país, marcamos esos perfiles que le permitieron a la Policía y a la Fiscalía venir hoy a sustentar y argumentar que estaban tras una organización criminal.

Los vídeos que la Policía y la Fiscalía recogieron y que muestran, no nos revelan en una situación diferente a la de nuestra vida, estar compartiendo, caminando por la calle, por la universidad, compartiendo con nuestros amigos.

LR: Este tipo de prácticas por parte del Estado son sistemáticas, usualmente y más en temporada pre electoral, es necesario mostrar un resultado positivo de capturas. Más allá de los perfiles que tenemos como estudiantes de universidad pública, algunos de nosotros nos desempeñábamos como defensores de derechos humanos, entonces no es raro que se desplieguen este tipo de seguimientos y señalamientos buscando precisar resultados.

El resultado a nuestros seguimientos son realmente actividades cotidianas, todas las etnografías son de nosotros en la calle, conversando, de ninguna manera eso constituye lo que quieren señalar imaginando los diálogos en los que estábamos hablando de planear delitos.

### **CR: ¿Ustedes hacen parte del Movimiento Revolucionario Popular (MRP) y tienen alguna responsabilidad en los crímenes de los que se les acusa?** 

BR: Yo no, y puedo decir lo mismo de mis compañeros.

### **CR: ¿Existe el MRP?** 

LR: Es claro que existe, hay una serie de hechos comprobables y que en la prensa se pueden encontrar, son las acciones, las banderas, el nombre, más allá de toda duda existe.

### **CR: ¿Y ustedes antes de su captura tenían conocimiento de la existencia del MRP?** 

LR: Por supuesto, ha habido un cubrimiento mediático de muchas de las cosas que ha hecho este grupo, basta con escribir el nombre en google y te vas a encontrar con una cantidad de noticias y con información que es de acceso público.

### **CR: Lizeth, los medios de información señalaron que en su bolso se encontró una pistola que fue presentada como prueba ¿usted tiene que ver con eso?** 

LR: Por supuesto que no, cuando llega el momento de la captura, nosotros estábamos en un balneario, allí nos llevan a cada uno a un quiosco, en ese momento yo no llevo mi bolso, me separan de mis pertenencias. Tanto la maleta como de mi bolso personal están lejos de mi cuando me requisan. Llega un policía, un activo de la SIJIN con el bolso en la mano y le dice a otro policía que encienda la cámara y que grabe, abre el bolso me dice saque lo que hay dentro, yo le respondo no, sáquelo usted y ahí en ese momento saca el arma.

Yo no sabía, porque yo no la llevaba conmigo, me parece increíble la coincidencia de la cámara, de que me hayan separado de mi bolso y que más adelante la Juez 47 diga que no hay manera de aceptar mi afirmación de que no es mía porque no está por escrito, cuando ya se había filtrado un vídeo en el cual afirmo claramente que el bolso es mío, que ahí están mis pertenencias pero que el arma no es mía.

### **CR: ¿Esa irregularidad que usted está mencionando se repitió con las demás personas en el momento de la captura?** 

LR: Sí, claro que sí. Por ejemplo, en la captura de Iván Ramírez se dio una irregularidad muy grave, a él lo detienen mientras está esperando el bus cerca a su casa, lo conducen a una camioneta, le dan vueltas por el barrio, lo llevan a su casa, hacen el allanamiento y ahí lo capturan, lo que indica que no había una orden previa cuando lo detienen, el Fiscal sostiene en la audiencia que lo detienen en su casa, en flagrancia, tratando de ingerir una memoria USB, osea todo es falso, porque no lo detienen en su casa, no se estaba comiendo una memoria, pero a partir de esa irregularidad pretendieron en ese momento privarlo de la libertad.

Posteriormente el proceso contra él se agudiza, él se va, ese mismo día sale porque se declara ilegal la captura, luego lo detienen y lo exhiben en medios como culpable, se ensañaron bastante con él, pero su privación de la libertad fue ilegal. A su familia también la han hostigado.

### **CR: ¿Conocen a Violeta y es realmente la líder del MRP?** 

BR: Hacer esa afirmación es ridículo, como lo han presentado los medios de comunicación y como la ha presentado la Fiscalía, siendo que Violeta se ha dedicado al estudio y a la rumba, en ningún momento ha podido formarse como la líder terrorista que pretende mostrar la Fiscalía.

¿La conocemos? pues claro, vimos clase con ella, es la compañera de César Barrera, mucho tiempo compartimos en la universidad, tomando tinto y echando carreta, no vamos a decir que no la conocemos, pero las afirmaciones que hace la Fiscalía son ridículas.

Yo quería hablar de otra irregularidad, en la captura de César Barrera, lo capturaron y se preguntaban, ¿este es? Este como que no es, este no está en la lista y su orden de captura también la tuvieron mucho tiempo después de haberlo capturado.

### **CR: Volviendo al tema de Violeta, sí ella es inocente, sí no hay pruebas contundentes contra ella ¿Por qué no se presenta ante el estrado y demuestra su inocencia?** 

BR: A mí me parece que en Colombia plantear eso es bastante ingenuo. Aquí estamos nosotros llevamos 7 meses presos, 7 meses en los que se nos han truncado nuestros proyectos y sueños, que ahora están aplazados, y estamos enfrentándonos a 60 o 70 años de cárcel. En una justicia como la colombiana eso es mucho pedir.

(Le puede interesar: ["Esta es la carta de Violeta Arango acusada por explosión en el C.C Andino"](https://archivo.contagioradio.com/esta-es-la-carta-de-violeta-arango-acusada-de-explosion-en-el-cc-andino/))

### **CR: El caso de ustedes también ha sido vinculado con el caso de Mateo Gutiérrez, sabemos que se solicitó en la última audiencia conexidad del caso de Mateo con el de ustedes, ¿por qué se hace esta petición?** 

BR: Sí claro, con Mateo nos conocíamos de la facultad de Sociología, Derecho y Ciencia Política, que están muy cercanos en la Universidad Nacional y es de ese ambiente común que nos conocemos. Creemos nosotros que los seguimientos que nos empezaron a hacer a nosotros tienen que ver y están en relación con Mateo Gutiérrez. A partir de la captura de Mateo Gutiérrez empiezan a seguirnos a nosotros, a partir de la captura de Mateo empiezan a andar detrás y a tratar de vincularnos con Mateo.

Teniendo en cuenta que a Mateo lo acusan de un hecho que él no comete, lo acusan a partir de testigos falsos. Cuando la Fiscalía enfrenta a la defensa de Mateo y demuestra en las preparatorias que Mateo no estuvo en el lugar de los hechos que le adjudican, cuando la Fiscalía se enfrenta a las pruebas que presenta la defensa que iban a dejar mal parado al testigo mentiroso que tiene la Fiscalía, entonces empiezan a buscar la conexidad con el caso nuestro para vincularlo por concierto, ahí hay mala fe, hay mala intención, ahí no hay el deseo ni la idea de explicar qué pasó.

(Le puede interesar: ["Fiscalía esta haciendo un proceso paralelo en los medios: Mamá de Mateo Gutiérrez"](https://archivo.contagioradio.com/49094/))

### **CR: Sin embargo, la Fiscalía había señalado que se habían encontrado pruebas dentro de la celda de Mateo que lo vinculaban con el MRP ¿Qué saben de esto?** 

BR: Ahí están las cartas que encontraron en la celda de Mateo, incluso hay una carta personal que le escribe Mateo a Tony López, que es la persona que los recibió en Cuba. Tony López envío una rectificación a la Revista Semana, en la Revista aparece esta carta de manera marginal, pero nadie se ha detenido a verificar qué es lo que dice Tony López, que rectifiquen y cuál ha sido la estupidez de la Revista Semana sobre ello, de resto hay una cantidad de cosas ahí que no son ciertas, que se caen de su peso. Fíjense que la Revista Semana está tratando de decir, ahora en su última edición, que en los allanamientos a las celdas encontraron cosas que nos vinculan a nosotros con los atentados de Barranquilla, ellos no tienen ni hora, ni fechas, ni lugares, pero que tienen serias sospechas, esos es una vulgaridad.

### **CR: ¿Qué pasó en la última audiencia y qué esperan ustedes de este proceso?** 

LR: La última audiencia fue la de formulación de acusación el 15 de diciembre, luego de que la Fiscalía solicitará la acumulación con el proceso de Mateo. En este momento la Fiscalía cuenta con 3 días para descubrir la totalidad del material probatorio a la defensa, pero nos hemos encontrado con que la Fiscalía no ha hecho un descubrimiento completo, es decir presenta una lista, y de esta lista del material probatorio no entrega a la defensa la totalidad, entregó discos con virus, entregó archivos encriptados, archivos dañados, estamos hablando de varias teras, es decir, un volumen sumamente grande de información con el cual la defensa realmente no ha podido hacer su trabajo. Llevamos casi dos meses y nuestra defensa no ha podido conocer el material probatorio.

A pesar de que el Fiscal había dicho que lo iba a entregar, esto entorpece mucho más la defensa y es otro palo en la rueda. Otra cosa en la que se ha insistido bastante en los medios de información es que hay muchos testigos, más de 30 testigos, pero es importante señalar que no hay ni un solo testigo que pueda afirmar que nosotros cometimos los delitos de lo que se nos acusa y los que señalan como testigos son realmente los activos de la Policía que presentaran sus informes. Las audiencias preparatorias están agendadas para el 2,3 y 4 de abril para hacer el seguimiento al caso.

<iframe id="audio_23518556" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23518556_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
