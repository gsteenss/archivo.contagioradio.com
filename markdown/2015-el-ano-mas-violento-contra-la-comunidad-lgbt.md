Title: 2015 el año más violento contra la comunidad LGBT
Date: 2016-10-13 16:21
Category: LGBTI, Nacional
Tags: Activistaas, amanazas, Bogotá, Colombia Diversa, Derechos, Derechos Humanos, LGBT
Slug: 2015-el-ano-mas-violento-contra-la-comunidad-lgbt
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/LGBT-e1476393950966.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País 

###### 13 Oct 2016 

El conflicto armado ha dejado en Colombia cerca de  405 víctimas de la comunidad LGBT, donde Antioquia ocupa el primer lugar seguido por Bogotá. Estas son algunas de las cifras que se conocen gracias al **informe de Derechos Humanos sobre la violencia hacia personas lesbianas, gay, bisexuales y trans (LGBT)** en 2015, que lleva por título “Cuerpos excluidos, rostros de impunidad” realizado por Colombia Diversa en conjunto con otras organizaciones.  También puede leer: [Informe da cuenta de victimización LGBTI en el conflicto armado colombiano](https://archivo.contagioradio.com/informe-da-cuenta-de-victimizacion-lgbti-en-el-conflicto-armado-colombiano/)

Las cifras presentadas están enfocadas en el tema de **homicidios, la violencia ejercida por parte de la policía, amenazas y hechos que victimizan a la comunidad LGBT** en el conflicto armado.

Algunas de las conclusiones conocidas en este informe, es que **los homicidios aumentan y la impunidad continúa**. Algunos datos muestran que para **el 2015, fueron asesinadas 110 personas de la comunidad LGBT**, cifra alarmante que se ha convertido en la más alta desde el año 2012. De este número, el 39%  estuvieron motivados por prejuicios con respecto a la identidad de género y la orientación sexual.

A su vez, el informe se interesa por develar cómo ha sido **la violencia policial y la criminalización** de las personas LGBT. La cifra asciende a **91 personas afectadas por la fuerza pública**, de los cuales Bogotá ocupó el primer lugar durante 2015.

Adicionalmente, las amenazas a esta comunidad  continúan en lugares donde la presencia de grupos armados sigue. La cifra va en aumento y en comparación con el 2014 asciende casi en un 50%.

Otra de las líneas del informe hace referencia al **desplazamientos forzado**, el cual no se presenta solo a nivel regional o nacional sino también es de carácter intraurbano y al respecto agregan en el documento  que “el rearme de grupos paramilitares bajo distintas modalidades y denominación, es una realidad que hace que las violencias contra las personas LGBT se mantengan y se agudicen en algunos escenarios”. Lea también: [¿Es necesaria la comunidad LGBTI para generar progreso?](https://archivo.contagioradio.com/es-necesaria-la-comunidad-lgbti-para-generar-progreso/)

El informe además deja una serie de recomendaciones al Gobierno Nacional, a Medicina Legal, a la Fiscalía General de la Nación, Policía Nacional, Defensoría del Pueblo y a la Unidad de Atención a Víctimas, entre las que sugiere implementar políticas públicas y de atención a la comunidad LGBT que propendan por la garantía de los derechos de sus derechos.

Lea también: [Un paso más para garantizar los derechos de la comunidad LGBTI en Colombia](https://archivo.contagioradio.com/un-paso-mas-para-garantizar-los-derechos-de-la-comunidad-lgbti-en-colombia/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
