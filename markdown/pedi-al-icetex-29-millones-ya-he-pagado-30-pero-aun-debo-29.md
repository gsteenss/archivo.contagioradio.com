Title: "Pedí al ICETEX $29 millones, ya he pagado 30 pero aún debo 29"
Date: 2016-07-26 14:27
Category: Educación, Entrevistas
Tags: créditos icetex, crisis educación Colombia, educación superior colombia
Slug: pedi-al-icetex-29-millones-ya-he-pagado-30-pero-aun-debo-29
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ICETEX.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [26 Julio 2016]

"Yo pedí prestados \$29 millones, de esos 29 yo ya he pagado 30 y aún debo 29 más (...) me faltan sesenta cuotas, **voy a terminar pagando \$76 millones, \$46 millones de sólo intereses**" afirma Daniel Torres, un joven que solicitó al ICETEX un crédito educativo para financiar sus estudios de ingeniería en la Universidad de los Andes. Como él son cientos los jóvenes que han compartido por redes sociales sus denuncias sobre lo interminable que termina convirtiéndose el pago de su deuda con la entidad.

De acuerdo con Torres la razón por la que los créditos se vuelven impagables radica en que **la entidad capitaliza los intereses, es decir, cobra intereses sobre intereses**. "Durante la época de estudio se van creando intereses y luego estos intereses se vuelven capital y sobre esta base el ICETEX comienza a calcular los intereses". En el periodo de gracia también se generan intereses.

Otro de los casos es el de una joven que pidió prestado al ICETEX \$16 millones, ha pagado \$21 millones pero aún debe \$19 millones; o el de un joven que solicitó \$46 millones, ha cancelado \$47 millones y aún debe \$46 millones. "Ocurre que muchos de los deudores no se enteran porque no le hacen seguimiento a sus créditos y **después se encuentran con que han pagado más del doble del valor del crédito**", agrega Torres.

El ICETEX decidió tomar un crédito con un banco mundial de 400 millones de dólares, con un interés por el orden de los \$250 mil millones, **lo que hace la entidad es trasladarnos esa deuda a nosotros**, afirma Torres y asegura que hay que tener cuidado con los anuncios que ha hecho la Ministra Gina Parody sobre más ofertas de créditos para los jóvenes, por lo que invita a la ciudadanía a unirse a la campaña [['ICETEX te arruina'](http://bit.ly/2aeKdaB)].

<iframe src="http://co.ivoox.com/es/player_ej_12344738_2_1.html?data=kpeglpmbd5mhhpywj5aWaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncaXVz87SzpC4s9PmxtiSlKiPaZOnqsjS1srcmMa109fiy9PFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
