Title: Gobierno continúa incumpliéndole a la Zona de Reserva Campesina Perla Amazónica
Date: 2016-03-16 14:10
Category: Movilización, Nacional
Tags: Amerisur, Putumayo, Zona de Reserva Campesina
Slug: gobierno-continua-incumpliendole-a-la-zona-de-reserva-campesina-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/petroleo-putumayo-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Contagio Radio 

<iframe src="http://co.ivoox.com/es/player_ek_10828719_2_1.html?data=kpWllJ2bdZqhhpywj5WaaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncajjw87S1NPTb8Tjz9nW0Iqnd4a2osaYy9PHuc7kzc6SpZiJhZrixdTZx5DFb83VjL%2Fc0MaPqMafs8rgx9fapY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sandra Lagos, ADISPA] 

###### [16 Mar 2016] 

Más de 40 comunidades en el departamento de Putumayo, se movilizan desde el pasado 24 de febrero, manifestando su rechazo a la actividad petrolera de la empresa Amerisur y solicitando **la presencia del gobierno nacional para que atienda las exigencias de las comunidades cercanas a la Zona de Reserva Campesina Perla Amazónica.**

“Hay mucha indignación de las comunidades porque ayer se esperaba que llegara una delegación del gobierno para que se reuniera la mesa regional minero-energética, pero hubo un incumplimiento”, dice Sandra Lagos, integrante de ADISPA, Asociación de Desarrollo Integral Sostenible Perla Amazónica.

Según Lagos, **este tipo de incumplimientos constantes del gobierno evidencian que la institucionalidad está de parte de los intereses de las petroleras** y no de las comunidades que se han visto afectadas por el poco control estatal que existe frente a estos proyectos minero-energéticos, lo que ha generado que los pobladores de esta zona de Putumayo deban caminar 8 kilómetros para poder tomar agua de la única fuente del sector que no se ha contaminado, pues el resto están con desechos industriales que bota la empresa.

Las comunidades se han visto mayormente afectadas desde que se inició la construcción del oleoducto (línea de transferencia) que conectaría y permitirá transportar el petróleo, lo que preocupa a los pobladores pues el **Río Putumayo podría verse afectado por este tipo de intervenciones.**

Además de continuar con la movilización que se espera articular con otras zonas del departamento, las comunidades se encuentran a la espera del fallo de la Corte Constitucional, frente a una tutela que **el gobernador indígena Juvencio Nastacuas Pai, interpuso con el objetivo de suspender 27 pozos petroleros que se encuentran en esa región.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
