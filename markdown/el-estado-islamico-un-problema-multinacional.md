Title: El Estado Islámico: La amenaza creciente
Date: 2015-02-04 20:19
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: Al qaeda, Estado Islámico, Irak, islamofobia, Jordania ejecuta a dos miembros del Estado Islamico
Slug: el-estado-islamico-un-problema-multinacional
Status: published

###### Foto:Laprensa.com.ini 

<iframe src="http://www.ivoox.com/player_ek_4039028_2_1.html?data=lZWgm5WWfI6ZmKiak5mJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDmxcbby8aPqcvZxNrhw5DFb8Xj1JDay8rRptPj1JDRx9GPidTowsncja7XsIa3lIquk9LNp9Chhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Víctor de Currea-Lugo:] 

En los últimos tiempos han crecido **grupos de carácter extremista islámicos, en contextos como las guerras de Siria, o la ocupación de Irak por parte de EEUU**; uno de ellos es el Estado Islámico.

**Víctor de Currea-Lugo**, profesor de la Universidad Javeriana y analista político internacional, explica que el origen del EI se da en distintas ocupaciones imperialistas contra Estados de mayoría islámica como Afganistán o Irak y su posterior desmembramiento después de la guerra. También en la creciente islamofóbia occidental, o estados como Siria, Libia o Egipto donde han sido reprimidos en vez de haber sido incluidos como fuerza política con representación legal.

Otro factor clave ha sido la instrumentalización de los textos del Corán, siendo este internacionalizado a raíz de las distintas agresiones contra el mundo islámico. El contexto político donde surge se ubica alrededor de la revolución iraní, el triunfo de los talibanes, y por último la organización político militar conocida como Alqaeda, explica DeCurrea.

La financiación **proviene casi directamente del sector privado** o en algunos casos de financiación por parte de potencias hegemónicas como EEUU.

Frente a la respuesta militar que recibe el EI, DeCurrea afirma que la experiencia ha demostrado que no solo con bombardeos es posible combatirlos, sino **que la mayor importancia son las fuerzas terrestres**, de los propios países afectados, **como ha sido el caso de Kobane** en la región Kurda de Siria.
