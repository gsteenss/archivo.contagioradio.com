Title: Represión en Buenaventura deja 9 heridos por arma de fuego
Date: 2017-06-01 13:57
Category: DDHH, Movilización
Tags: buenaventura, ESMAD, Paro cívico, Puerto de Buenaventura
Slug: represion-a-paro-civico-en-buenaventura-dejo-9-heridos-por-arma-de-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/unidades-del-esmad-despejan-barricadas-del-paro-civico-en-el-sector-de-la-delfina-via-buenaventura-19-05-2017.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio.com] 

###### [01 Jun 2017]

Según el informe del Comité del Paro Cívico, solamente en la noche del 31 de Mayo, la represión en Buenaventura por parte del ESMAD dejó el saldo de **9 personas heridas por arma de fuego en por lo menos 2 barrios de esta ciudad** y solamente en la noche del 31 de Mayo. Varios de los heridos no estaban en las protestas sino que transitaban por la zona.

Según Javier Torres, a pesar del compromiso del alcalde y de las diferentes delegaciones del gobierno nacional de cesar la represión, en varios puntos neurálgicos de la ciudad la acción del ESMAD ha sido sorpresiva atacando incluso el interior de las viviendas “A mi me tocó salir de la casa a las 4 de la madrugada porque no soporté los gases” relata el líder de la movilización.

Incluso, debido a la acción del ESMAD y el efecto de los gases lacrimógenos usados contra la población civil el hospital se ha visto afectado en varias ocasiones y se han tenido que suspender labores de atención médica a las personas que se encuentran internadas, relató el lider de la movilzación.

Según un primer reporte por parte del personal del hospital, muchos de los heridos se encuentran en atención quirúrgica y uno de ellos podría tener comprometidos varios órganos, sin embargo la gran mayoría de ellos presenta afectaciones menores y luego las cirugías podrían ser dados de alta. Ante esa situación no hay una respuesta por parte de los órganos de control sobre las investigaciones contra quienes realizaron los disparos por parte de la Fuerza Pública. [Lea también: Persiste arremetida del ESMAD](https://archivo.contagioradio.com/persiste-arremetida-del-esmad-contra-poblacion-de-buenaventura/)

### **Quienes vienen a negociar no tiene poder de decisión** 

Aunque se espera que la tarde de este jueves haga presencia en Buenaventura el Ministro del Interior, los acercamientos que se han tenido hasta el momento con diversas instituciones del Estado no han rendido frutos, dado que las propuestas de los promotores del Paro Cívico son recibidas y luego se manifiesta que deben ser consultadas con los órganos centrales en Bogotá. [Lea también: Buenaventura pide del 2.5% de lo que le aporta al país anualmente](https://archivo.contagioradio.com/fondo-autonomo-seria-la-solucion-al-paro-civico-de-buenaventura/)

Así las cosas, la propuesta del Fondo Autónomo con capacidad administrativa que comprendería más de 10 billones de pesos podría ser la salida que apruebe hoy el gobierno nacional. Torres explica también que t**ienen establecidos varios mecanismos para que los dineros sean invertidos en las obras que necesitan** los habitantes del puerto y no se pierdan fruto de la histórica corrupción.

<iframe id="audio_19026619" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19026619_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
