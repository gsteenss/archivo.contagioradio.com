Title: Corte Penal Internacional afirma que no habrá impunidad en acuerdos de paz
Date: 2016-09-01 15:58
Category: Entrevistas, Paz
Tags: acuerdo final, Corte Penal Internacional, FARC, Gobierno, jurisdicción especial para la paz, proceso de paz
Slug: comunicado-de-la-fiscal-de-la-cpi-evidencia-que-no-habra-impunidad-juan-carlos-henao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Corte-Penal-Internacional-e1472760697575.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reporteros Asociados] 

###### [1 Sep 2016] 

Meses atrás Fatou Bensouda, **fiscal General de la Corte Penal Internacional había afirmado que revisaría "con ojo de lupa**" las condiciones acordadas en materia de justicia entre el gobierno y las FARC. Y así fue, por medio de su más reciente comunicado la fiscal confirmó que el acuerdo final no implicará impunidad alguna, como lo asegura [Juan Carlos Henao](https://archivo.contagioradio.com/acercamientos-en-punto-de-justicia-y-victimas-entre-gobierno-y-farc/), exmagistrado de la Corte Constitucional.

“El comunicado da cuenta que todos los fantasmas que ha querido generar la oposición sobre que el acuerdo no está conforme al Derecho Internacional, **todo lo que se ha dicho se desvanece. Está clarísimo**, no hay que buscarle mucha interpretación a esa carta”, expresa Henao.

En la carta Fatou Bensouda hace especial énfasis en las sanciones efectivas que se acordaron en la [Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/la-evolucion-del-derecho-en-el-acuerdo-final-de-paz-en-colombia/), teniendo en cuenta que el país reconoció que **“los crímenes más graves constituyen una amenaza a la paz, la seguridad y al bienestar general del mundo”**, y agrega, “noto con satisfacción que el [texto final del acuerdo de paz](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/) excluye amnistías e indultos para crímenes de lesa humanidad y crímenes de guerra bajo el Estatuto de Roma”.

Frente a este tema, el exmagistrado, ha hecho claridad sobre un informe de la sala de Justicia y Paz del Tribunal Superior de Bogotá, donde se señala a más de 50 empresas por su presunta colaboración a grupos ilegales durante el conflicto armado. “Sobre el tema de terceros y una lista que sería investigada, no es cierto que eso se vaya a volver una casería de brujas”, pues solo serán investigadas por la [JEP](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz), “**las personas que tuvieron una participación determinante en la comisión de crímenes de guerra y de lesa humanidad, más no quienes hayan pagado vacuna”.**

En ese sentido, la fiscal indica que “**el acuerdo reconoce el lugar central de las víctimas en el proceso y sus legítimas aspiraciones de justicia**” y adicionalmente afirma que “Se espera que la [Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz) que se establecerá en Colombia lleve a cabo esta función y que se centre en los máximos responsables de los crímenes más graves cometidos durante el conflicto armado”.

Teniendo en cuenta esos aspectos, la carta de la fiscal evidencia la satisfacción de la [CPI](https://archivo.contagioradio.com/?s=corte+penal+internacional)por el acuerdo logrado entre las partes que duraron 4 años negociando la paz en La Habana.“Es sin lugar a dudas un logro histórico para Colombia y para el pueblo colombiano, cuyas vidas se han visto profundamente afectadas por el conflicto armado de 52 años”, y finaliza expresando que la oficina **continuará apoyando sus esfuerzos de conformidad con su mandato bajo el Estatuto de Roma con independencia, imparcialidad y objetividad.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
