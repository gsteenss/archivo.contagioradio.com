Title: Ciudadanía pide concepto favorable del Concejo sobre Consulta Antitaurina
Date: 2015-07-27 13:06
Category: Animales, Nacional
Tags: Concejo de Bogotá, Consulta Antitaurina, consulta popular, corridas de toros, Diego Lópes, Maltrato animal, María Fernanda Rojas, Natalia Parra Osorio, Partido Verde, Plataforma Alto, tauromaquia
Slug: concejo-de-bogota-decide-si-hay-o-no-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/MG_97891.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Juam-Pablo-Olmos.mp3"\]\[/audio\]

##### [Juan Pablo Olmos, Plataforma ALTO] 

###### [27 de Julio 2015]

Este lunes empezó nuevamente el debate en el Concejo de Bogotá, sobre la posibilidad que en las elecciones del próximo 25 de octubre, la ciudadanía pueda demostrar si existe o no arraigo cultural de las corridas de toros en Bogotá, respondiendo la pregunta **¿Está usted de acuerdo, si o no, con que se realicen corridas de toros y novilladas en Bogotá D.C.?**

Desde múltiples organizaciones ciudadanas, que conforman Bogotá Sin Toreo, se lleva a cabo un plantón frente al Concejo, pidiéndole un concepto favorable a la consulta popular. De un lado de la calle se encuentran los defensores de la vida de los toros, **aproximadamente unas cuarenta personas y al otro lado, unos diez taurinos gritando “toros si, marihuana no”.**

En los últimos días los taurinos han intentado deslegitimar la consulta popular, sin embargo, abogados y expertos como Diego López, afirman que la consulta popular es legal y constitucional, teniendo en cuenta que “el derecho le dice al ciudadano cómo se debe comportar, pero con frecuencia **el ciudadano también le puede pedir cosas al derecho”.**

Por otro lado, Juan Pablo Olmos, integrante de la Plataforma ALTO, asegura que los **taurinos únicamente son una minoría numérica, pero no cultural ni étnica y mucho menos constitucional.**

A más tardar hasta el miércoles debe emitirse una decisión del Concejo sobre la consulta, así mismo continúan los plantones en defensa de la vida de los toros, expresando que “**la ciudadana es la que debe decidir si se quiere matar o no animales por entretenimiento”,** como lo señala Olmos. “Hay algunos que queremos tener a los toros vivos y otros los quieren muertos”, dice.

Para Juan Pablo, esta **semana se tiene la posibilidad de evolucionar como sociedad** y respetar la vida de los animales, “esperamos que se tome una decisión correcta pensado en la vida, en un país que viva en paz y respeto hacia todas las formas de vida… **solo 8 países continúan con la tradición de la tauromaquia en el resto la consideran un delito**.”

Cabe recordar, que María Fernanda Rojas, concejal de Bogotá, por la Alianza Verde, aseguró que en términos generales el Concejo tiene una postura favorable a la Consulta Antitaurina, “**puede que el toreo tenga elementos artísticos pero el eje central es la tortura de un animal”**, expresa la concejal.

\

 
