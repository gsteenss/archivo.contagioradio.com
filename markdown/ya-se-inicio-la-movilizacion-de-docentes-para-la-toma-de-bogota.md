Title: Ya se inició la movilización de docentes para la Toma de Bogotá
Date: 2017-06-05 14:45
Category: Educación
Tags: Paro FECODE, Toma a Bogotá
Slug: ya-se-inicio-la-movilizacion-de-docentes-para-la-toma-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/84841b8f-f84e-4697-8226-6c3ad2604df0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [05 Jun 2017] 

Miles de docentes están llegando a Bogotá, para realizar mañana la gran toma a la Capital del país, que pretende hacer un llamado al Ministerio de Educación para que realice propuestas efectivas a la desfinanciación de la Educación y **garantice condiciones dignas tanto para los maestros como para los estudiantes a la hora de tomar clases**.

Desde departamentos como Antioquía, Meta, Valle del Cauca, Santander, Magdalena, entre otros, los docentes han iniciado largas jornadas para llegar caminando hasta la capital, mientras que otros **se encuentran preparando la logística para que buses transporten a los maestros hacia Bogotá**. Le puede interesar:["El 6 de junio los docentes de Colombia se toman Bogotá"](https://archivo.contagioradio.com/el-6-de-junio-los-docentes-de-colombia-se-toman-bogota/)

Desde Medellín se espera que **salgan aproximadamente 27 buses, 12 de ellos desde la ciudad y otros 15 desde los municipios de Antioquía**. De acuerdo con Patricia Zuluaga, integrante de Asociación de Institutores de Antioquia (ADIDA), una vez se dé la movilización, algunos maestros acamparán en la Plaza de Bolívar y otros se quedarán en colegios que se han dispuesto para albergarlos.

![WhatsApp Image 2017-06-04 at 9.47.12 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-04-at-9.47.12-PM.jpeg){.alignnone .size-full .wp-image-41680 width="960" height="720"}

Además, señaló que la participación de los padres de familia y estudiantes en las movilizaciones **demuestra el apoyo que tiene el paro magisterial y la indignación que también tiene la sociedad en general hacia la falta de apoyos para el Sistema Nacional** de Educación. Le puede interesar:["Secretaría de Educación ya habría advertido la grave situación de la Educación"](https://archivo.contagioradio.com/secretarias_educacion_paro_educadores/)

“Estamos defendiendo la educación de todos los colombianos, desde la reforma constitucional se le empezaron a quitar recursos a todo el Sistema General de Participación, en donde a la educación le han quitado **20 puntos del Producto Interno Bruto**” señaló Zuluaga y agregó que lo que se mantienen las comunicaciones continuamente con padres de familia y estudiantes.

<iframe id="audio_19085675" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19085675_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
