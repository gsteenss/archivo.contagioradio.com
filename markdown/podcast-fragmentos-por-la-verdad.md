Title: Podcast | Fragmentos por la verdad
Date: 2019-12-06 12:43
Author: AdminContagio
Category: Cultura, Memoria
Slug: podcast-fragmentos-por-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/thomas-william-4qGbMEZb56c-unsplash.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/diego-guzman-u_4bPYOXujE-unsplash.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/aaron-burden-1zR3WNSTnvY-unsplash.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/aaron-burden-1zR3WNSTnvY-unsplash-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/084194976_prevstill.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/depositphotos_179253932-stock-video-rain-black-background.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/animation-of-rain-on-a-black-background_hlsbaqm8g_thumbnail-full07.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/iStock-890093344-1260x840.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/s42.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/puñosenaltoP.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Untitled-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/marcha-por-la-paz57.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0680-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/podcast-por-la-verdad.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

?PODCAST?

FRAGMENTOS POR LA VERDAD
========================

[  
![logo contagio radio web](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/logo-contagio-radio-web-1-o62lxz6c5i2xadazjbtrg546pfwdkbjuc9oblgasfw.png "logo contagio radio web")](https://archivo.contagioradio.com/)  
![periodismo de verdad - logo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/periodismo-de-verdad-logo-o9gniuv9stcrcq6jd73mh9xtqrmc9w1ivjh4kzq31k.png "periodismo de verdad – logo")

Iniciamos un recorrido hacia el reconocimiento de la memoria histórica,  múltiples voces narran desde su conocimiento la construcción de paz y verdad desde diferentes orillas, procesos y territorios. 

Fragmentos por la Verdad, reúne historias de quienes han vivido y atestiguado la guerra y quienes están comprometidos con la construcción de una nueva sociedad en la que se priorice  el derecho a la verdad y a la no repetición.

Esta serie de podcast es posible gracias a la alianza de medios Periodismo de Verdad.

###### Iniciativas de TRANSFORMACIÓN

**Cine desde el Chocó**
-----------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Cine-desde-el-Choc-e9cj0k" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### En la memoria

**Ambiente**
------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Ambiente-y-Memoria-e9cj01" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### INICIATIVAS DE TRANSFORMACIÓN

**Educación**
-------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Educacin-e9ciua" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### paz y reconciliación

**Excombatientes**
------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Excombatientes-e9cit4" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### Cultura y memoria

**Baile y Memoria**
-------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Danza-e9cis1" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### Iniciativas de transformación

**Nuevas Narrativas**
---------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Narrativas-en-el-posconflicto-e9cird" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### CULTURA Y MEMORIA

**Poesía**
----------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Poesa-e9ciqp" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### Cultura y memoria

**Teatro**
----------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Teatro-e9cilr" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### PAZ Y RECONCILIACIÓN

**Deporte**
-----------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Deporte-y-Reconciliacin-e9cice" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### libertades de expresión

**Periodismo**
--------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Periodismo-e9ci3t" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### cultura y memoria

**Música**
----------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Msica-e9ci14" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### en la memoria

**Líderes Sociales**
--------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Lderes-Sociales-e9cht6" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### libertades de expresión

**Comunidad LGBTI**
-------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Comunidad-LGBTI-e9chs3" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### \#norepetición

**Violencia Sexual**
--------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Violencia-Sexual-e9chli" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### en la memoria

**Indígenas**
-------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Indgenas-e9cfs5" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### CULTURA Y MEMORIA

**Fotografía**
--------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Fotografa-e9c65t" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### \#norepetición

**Desaparicion Forzada**
------------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Desaparicin-Forzada-e9c60p" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### Iniciativas de trasnformación

**Comisión interétnica**
------------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Comisin-intertnica-e9c5s4" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### PAZ Y RECONCILIACIÓN

**Desmovilización AUC** 
-----------------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Desmovilizacin-Autodefensas-e9c5d2" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

###### \#norepetición

**Sin Olvido**
--------------

<iframe src="https://anchor.fm/contagio-radio/embed/episodes/Fragmentos-por-la-Verdad--Sin-olvido-e9ea8o" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>  
[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Dribbble  
]()

Un podcast de Contagio Radio en la Alianza Periodismo de Verdad

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)
