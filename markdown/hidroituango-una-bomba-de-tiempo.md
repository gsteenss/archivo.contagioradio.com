Title: Hidroituango, una bomba de tiempo
Date: 2018-05-13 07:00
Category: Ambiente, DDHH, Nacional
Tags: Antioquia, búsqueda de desaparecidos, emergencia en hidroituango, EPM, Hidroituango
Slug: hidroituango-una-bomba-de-tiempo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/HIDROITUANGO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Svenswikipedia] 

###### [13 May 2018] 

La crisis en el Bajo Cauca Antioqueño continúa producto del taponamiento de uno de los túneles de llenado del proyecto hidroeléctrico Hidroituango. Las comunidades han denunciado que viven en zozobra constante y EPM se ha limitado a decir que tiene la situación controlada. Contagio Radio recopiló proceso que ha tenido el proyecto, sus afectaciones a las comunidades, la búsqueda de desaparecidos y la crisis actual que vive la población.

**¿Qué es Hidroituago?**

\[caption id="" align="alignnone" width="800"\]![SONY DSC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/puente-pescadero-800x532.jpg){.size-medium .wp-image-53320 width="800" height="532"} Puente Pescadero, río Cauca - Foto: John Jairo Jaramillo\[/caption\]

El proyecto hidroeléctrico Hidroituango es la obra de infraestructura más grande que se haya construido en el país. Busca generar energía y comercializarla a nivel nacional e internacional ,y está situada al noroccidente del departamento de Antioquia. El proyecto se localiza sobre el río Cauca, específicamente sobre el Cañón entre las cordilleras central y occidental por lo que tiene una afectación directa sobre 12 municipios.

De acuerdo con el proyecto mismo, se tiene pensado que empiece a funcionar a partir de diciembre de 2018 generando 13.939 gigavatios por hora de energía media anual. Para esto, han tenido que desviar el cauce de uno de los ríos más importantes del país y han construido una presa de 225 metros de altura y 20 millones de metros cúbicos de volumen.

Debido a la magnitud de proyecto, los encargados de la obra que son Empresas Públicas de Medellín, han realizado la tala de árboles que comprende por lo menos 4.500 hectáreas de bosque seco tropical afectando diferentes especies de flora y fauna. De acuerdo con el libro Biodiversidad 2015 del Instituto Alexander Von Humboldt, **sólo queda el 8% de los 9 millones de hectáreas de ese ecosistem**a. (Le puede interesar:["EPM inició tala de 4.500 hectáreas de bosque seco tropical"](https://archivo.contagioradio.com/epm-inicio-tala-de-4-500-hectareas-de-bosque-seco-tropical/))

Esto ha afectado también a las comunidades que utilizan los árboles como georreferenciación para encontrar los cuerpos de cerca de 2 mil personas que se cree que fueron asesinadas y enterradas en el bosque en el marco del conflicto armado en Colombia. Cuerpos que finalmente no serán encontrados pues, como lo hizo saber el Estado colombiano en el periodo 168 de audiencias de la Comisión Interamericana de Derechos Humanos, no hay una certeza ni una cifra consolidada sobre el número de personas que puede haber en la zona de influencia del proyecto.

### **¿Cuáles han sido las afectaciones para las comunidades?** 

\[caption id="attachment\_53318" align="alignnone" width="800"\]![damnificados hidroituango](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/damnificados-hidroituango-800x541.jpg){.wp-image-53318 .size-medium width="800" height="541"} Comunidades damnificadas por Hidroituango improvisa una cocina en  Sabanalarga\[/caption\]

Si bien el proyecto contempla una forma de producción de energía limpia, su construcción ha generado múltiples afectaciones para las comunidades que no han sido tenidas en cuenta por parte de EPM. El Movimiento Ríos Vivos, ha venido denunciando desalojos forzados, amenazas, persecuciones y asesinatos de sus integrantes.

Desde el momento de su construcción, cerca de 500 familias han sido desalojadas de sus territorios sin las garantías de reubicación y muchas perdieron sus enseres. A esto se suma que sus actividades ancestrales también se han visto en riesgo como lo es la pesca y la barequería, debido al desvío del caudal del río. Los pescadores han tenido que trasladarse o simplemente buscar una alternativa para su trabajo. (Le puede interesar:["Memoria y Resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

De acuerdo con Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos de Antioquia, a la empresa EPM y a las autoridades regionales y nacionales, “no les interesa qué pasa con los medios de vida de las comunidades. EPM le ha dicho que no son de aquí por el hecho de haber sido desplazados y que son unos avivatos”. Esto ha terminado por generar persecuciones y hostigamientos por parte de la Policía Nacional que en reiteradas ocasiones ha utilizado el ESMAD para desalojar por la fuerza a las comunidades.

### **Comunidades temen no encontrar los cuerpos de sus seres queridos** 

\[caption id="attachment\_50784" align="alignnone" width="800"\]![Foto: Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/hidroituango-800x533.jpg){.size-medium .wp-image-50784 width="800" height="533"} Foto: Puente pescadero/ Contagio Radio\[/caption\]

A estas afectaciones se suma el hecho de que, para realizar la hidroeléctrica, la empresa deberá generar una inundación de 4.500 hectáreas de bosque seco tropical a lo largo de 70 kilómetros donde las comunidades han denunciado que se encuentras fosas comunes con por lo menos 2 mil cuerpos de personas desaparecidas.

Sin embargo, no ha habido un esfuerzo institucional por hacer un registro de las personas desaparecidas en la zona de influencia Hidroituango. Por una parte, el Centro de Memoria Histórica ha manifestado que en la zona se encuentran alrededor de 643 víctimas de desaparición forzada. Por otra, el Registro Único de Víctimas ha contabilizado 622 víctimas mientras que Registro Nacional de Desaparecidos ha manifestado que tiene el dato de 343 desaparecidos allí.

En el marco del periodo 168 de audiencias ante la CIDH, el Movimiento Ríos Vivos cuestionó la labor de entidades como la Fiscalía, quien argumentó que las labores de búsqueda de los desaparecidos están planeadas para desarrollarse entre los meses de mayo y junio de 2018. Esto para Ríos Vivos y las comunidades es difícil de comprender, puesto que “la inundación ya comenzó de manera ilegal”. Se espera que el proyecto comience a andar en diciembre del mismo año y no hay certeza de la cantidad de cuerpos que puede haber ni sobre su ubicación. (Le puede interesar:["Búsqueda de desaparecidos en Hidroituango debe incluir a las víctimas: Ríos Vivos"](https://archivo.contagioradio.com/busqueda-de-desaparecidos-en-hidroituango-debe-incluir-a-las-victimas-rios-vivos/))

Desde la organización denunciaron que “**no es posible realizar un proceso de búsqueda que incluya a las víctimas y las comunidades en tan sólo dos meses en la totalidad de los 12 municipios afectados**”. Sobre esto, han sido reiterativos y han denunciado que las víctimas no han sido tenidas en cuenta en los procesos de búsqueda que se han desarrollado. Razón por la cual los comisionados de la CIDH cuestionaron la forma en la que ha participado la sociedad civil y la manera cómo han funcionado los planes de búsqueda.

Según **Ríos Vivos**, en los 12 municipios que abarca la obra, durante el conflicto armado se presentaron 62 masacres que dejaron un total de 372 asesinatos distribuidas de la siguiente manera: Valdivia (6 masacres y 68 víctimas), Ituango (11 masacres y 71 víctimas), Yarumal (6 masacres y 38 víctimas), San Andrés de Cuerquia (5 masacres y 32 víctimas), Santa Fe de Antioquia (5 masacres y 23 víctimas), Briceño (5 masacres y 22 víctimas), Peque (4 masacres y 31 víctimas), Buriticá (5 masacres y 22 víctimas), Sabana Larga (4 masacres y 26 víctimas), Toledo (4 masacres y 23 víctimas) y Liborna (3 masacre y 19 víctimas). (Le puede interesar:["62 masacres en los 12 municipios donde se desarrolla el proyecto Hidroituango"](https://archivo.contagioradio.com/62-masacres-los-12-municipios-donde-se-desarrolla-proyecto-hidroituango/))

\[caption id="attachment\_53322" align="alignnone" width="705"\]![Cuerpos Gramaticales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Captura-de-pantalla-2018-05-12-a-las-14.55.41-705x393.png){.wp-image-53322 .size-medium width="705" height="393"} Cuerpos Gramaticales - Puente pescadero\[/caption\]

Como una forma de resistencia y protesta, las comunidades afectadas han realizado en varias ocasiones diferentes actividades como la siembra de “Cuerpos Gramaticales”, en donde se reunieron en el puente Pescadero, que comunica a Ituango con Toledo, para hacer duelo por las víctimas arrojadas y desaparecidas en el Río Cauca. Allí denunciaron que la inundación de Hidroituango va a dejar las puertas cerradas para continuar con la búsqueda de sus seres queridos, ahogando la posibilidad de un encuentro con la verdad, y además, va a transformar sus actividades culturales y desarrollo económico que siempre han dependido del río.

### **¿Qué está sucediendo en este momento con la represa Hidroituango?** 

La emergencia se presentó el 28 de abril de 2018 cuando el taponamiento de uno de los túneles de llenado y desvío de la presa ocasionó una inundación en la parte alta de la misma. Esto provocó que en la parte baja, el caudal del río disminuyera y alertara a las comunidades que temen que ocurra una avalancha.

> Así estan matando el [\#RioCauca](https://twitter.com/hashtag/RioCauca?src=hash&ref_src=twsrc%5Etfw) con [\#Hidroituango](https://twitter.com/hashtag/Hidroituango?src=hash&ref_src=twsrc%5Etfw) se escucha la naturaleza gritar, el río silenciado, las comunidades acorraladas por el miedo, la violencia que impide acompañar el grito de dolor dolor [pic.twitter.com/qJ2Y0Rruzc](https://t.co/qJ2Y0Rruzc)
>
> — Movimiento RíosVivos (@RiosVivosCol) [8 de mayo de 2018](https://twitter.com/RiosVivosCol/status/993668202247917568?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
EPM afirmó que la emergencia se presentó producto de una falla geológica inesperada, mientras que Ríos Vivos informó que el taponamiento lo ocasionó la gran cantidad de material vegetal que fue lanzado al río y no fue desechado de la manera correcta por parte de la empresa.

</p>
![hidro1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/hidro1-800x495.jpg){.alignnone .size-medium .wp-image-53323 width="800" height="495"}

Si bien desde las autoridades afirmaron que la situación estaba controlada, en reiteradas oportunidades las comunidades manifestaron que la empresa no ha sido clara al explicar lo que ha sucedido y tampoco hay un plan de contingencia que garantice la seguridad de las comunidades que habitan en las riveras del río Cauca. Desde Ríos Vivos, Isabel Cristina Zuleta manifestó que “es indignante que EPM siga diciendo que tiene bajo control la situación”. (Le puede interesar:["Es indignante posición de EPM frente a tragedia en Hidroituango": Ríos Vivos"](https://archivo.contagioradio.com/es-indignante-posicion-de-epm-frente-a-tragedia-por-hidroituango-rios-vivos/))

Adicionalmente, denunciaron que la empresa debió haber previsto que “en el Cañón las montañas y laderas se derrumban con mucha frecuencia”. Zuleta recordó que la empresa ha negado la existencia de comunidades en las riveras del río por lo que no les han dicho qué acciones se van a realizar, como lo es el llenado prioritario, que para Ríos Vivos es “absurdo” si se tiene en cuenta que para esto se debe llenar la totalidad de la presa.

### **Ríos Vivos ha sido víctima de hostigamiento y persecuciones** 

A esta situación se suma la descalificación y hostigamiento que se ha hecho contra el Movimiento Ríos Vivos, que se opone al desarrollo del proyecto. En entrevista para Contagio Radio, Isabel Cristina Zuleta manifestó que desde la Gobernación de Antioquia los líderes y lideresas de diferentes comunidades han recibido amenazas contra su vida.

\[caption id="attachment\_53324" align="alignnone" width="739"\]![Hugo Albeiro Georgio Pérez y Alberto Torres, líderes asesinados integrantes de Río Vivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/asesinatos-rios-vivos-739x464.jpg){.size-medium .wp-image-53324 width="739" height="464"} Hugo Albeiro Georgio Pérez y Alberto Torres, líderes asesinados integrantes de Río Vivos\[/caption\]

Cabe recordar que en el tiempo que se ha presentado la emergencia, han ocurrido dos asesinatos de líderes sociales que hacen parte del Movimiento Ríos Vivos. Se trata de **Hugo Albeiro Georgio Pérez y Alberto Torres**, de quien la secretaria de Gobierno de Antioquia, Victoria Eugenia Ramírez descalificó su pertenencia a la organización y que además, manifestó que Ríos Vivos carece de personería jurídica.

Frente a esto, Zuleta indicó que no hace falta tener personería jurídica, pues en la Constitución colombiana está avalada la libre asociación “más cuando se trata de un movimiento que lucha por las garantías de verdad, justicia, reparación y no repetición”. Dijo además que, “las descalificaciones contra Ríos Vivos ponen en riesgo a sus integrantes teniendo en cuenta que han dicho que somos unos mentirosos”. (Le puede interesar:["Asesinan a Hugo Alberto George líder social y afectado por Hidroituango"](https://archivo.contagioradio.com/asesinan-hugo-albeiro-george-lider-social-afectado-hidroituango/))

**Zuleta informó que en la CIDH los comisionados** pudieron evidenciar la preocupación de la sociedad civil y la gravedad de los acontecimientos, que fueron descritos en referencia a las afectaciones a los derechos humanos de las poblaciones. Allí, los comisionados le pidieron al Estado colombiano realizar una reunión “con carácter urgente” que integre a la sociedad civil “para trabajar el tema de protección y avanzar en la identificación de los sitios donde hay posibles cuerpos”.

\[caption id="attachment\_53327" align="alignnone" width="640"\]![Audiencia CIDH /Fran Afonso/CIDH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/41956470902_a5fb92cf04_z-640x427.jpg){.size-medium .wp-image-53327 width="640" height="427"} Audiencia CIDH /Fran Afonso\[/caption\]

Finalmente, la Procuraduría General de la Nación, pidió que se active el Comité Nacional para el Manejo de Desastre y garantizar la ejecución de funciones de prevención y asesoría en lo que tiene que ver con el represamiento del río Cauca. Medios de comunicación nacionales informaron que, a través de una carta, el procurador delegado, Gilberto Augusto Blanco, le solicitó a la Unidad Nacional para la Gestión del Riesgo de Desastres, donde le comunicó la necesidad de planear y hacer seguimiento a la emergencia.

###### Reciba toda la información de Contagio Radio en [[su correo]
