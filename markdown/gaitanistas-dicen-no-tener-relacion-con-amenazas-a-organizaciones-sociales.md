Title: Gaitanistas dicen no tener relación con amenazas a organizaciones sociales
Date: 2016-04-22 18:34
Category: Nacional, Paz
Tags: Autodefensas Gaitanistas de Colombia, FFMM. FFAA, paramilitares
Slug: gaitanistas-dicen-no-tener-relacion-con-amenazas-a-organizaciones-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/gaitanistas-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: vanguardia] 

###### [22 Abril 2016]

Este viernes llegó a nuestra redacción en horas de la tarde una copia de un comunicado del grupo paramilitar **Autodefensas Gaitanistas de Colombia**, dirigido a varias organizaciones sociales, de Derechos Humanos y sindicales, en la que afirman no ser  los responsables de las amenazas y hostigamientos que han sido proferidos en su nombre.

El escrito, que ha sido verificado por tres fuentes, dejaría entrever que el nombre de esa organización está siendo usado por otras estructuras de este tipo.

Politólogos consultados antes de divulgar el comunicado expresaron que se trataría de una tensión en el interior de las estructuras herederas del paramilitarismo, unas que eventualmente estarían dispuestas a respetar los acuerdos con las guerrillas de las FARC EP y el ELN, y que se expresaría en la originarias AGC y otros que estarían con los mismos propósitos de antaño de carácter contrainsurgente y de control social.

Agregan que entre líneas se puede interpretar que están proyectando un sometimiento a la justicia, mientras que los disidentes o distintos a las AGC continuarían con el narcoparamilitarismo como proyecto contraisurgente y de control social.

Por consiederarlo de interés y para su análisis divulgamos el comunicado completo.

[![1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/1.jpeg){.aligncenter .wp-image-23093 width="543" height="745"}](https://archivo.contagioradio.com/gaitanistas-dicen-no-tener-relacion-con-amenazas-a-organizaciones-sociales/1-13/)  
[![2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/2.jpeg){.aligncenter .wp-image-23094 width="537" height="738"}](https://archivo.contagioradio.com/gaitanistas-dicen-no-tener-relacion-con-amenazas-a-organizaciones-sociales/2-9/)  
   
 
