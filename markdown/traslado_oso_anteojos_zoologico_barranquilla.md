Title: Trasladan a oso de anteojos de una reserva natural a un zoológico en Barranquilla
Date: 2017-06-19 08:00
Category: Ambiente, Animales, Nacional
Tags: oso de anteojos
Slug: traslado_oso_anteojos_zoologico_barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/oso-anteojos-barranquilla-e1497876741977.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### [19 Jun 2017] 

El oso de anteojos, de nombre Chucho, que desde hace más de 20 años habitaba en la Reserva Forestal Río Blanco, fue traslado al zoológico de Barranquilla. El anuncio se dio le 14 de junio por Juan David Arango Gartner director de Corpocaldas, señalando como motivos de su traslado un **“bienestar para el ejemplar, mediante cuidados específicos propios para estos animales, una hembra como compañera permanent**e, y la posibilidad de integrarlo a programas de educación ambiental que aportan a la conservación de su especie”.

La noticia generó el rechazo de la población, que además se enteró cuando ya estaba en marcha el traslado. Enseguida inició una movilización en redes sociales con \#TodosSomosRíoBlanco. Y una serie de profesionales empezaron a argumentar por qué no fue una buena decisión dicho traslado.

**Chucho llegó a Manizales proveniente de la Reserva Natural La Planada de Ricaute (Nariño), cuando tenía 4 años de edad** junto con su hermana Clarita, la cual murió hace 9 años. Desde entonces habitó solitario la Reserva Forestal Protectora Río Blanco. Según Coporpocaldas, allí “recibió un manejo en condiciones de sostenimiento sin un objetivo claro para el ejemplar”.

### Las críticas 

El doctor Luis Fernando Canizales ex Director Ejecutivo del Zoológico Matecaña de Pereira, señala que “si tenemos en cuenta todo el trabajo que requiere la adaptación de una especie silvestre a un nuevo medio natural similar a su hábitat propio, se puede concluir fácilmente que es mucho más difícil para un ejemplar, en particular, adaptarse a un medio que ni siquiera, en lo más mínimo, corresponde al de su origen ancestral de especie”.

Los interrogantes ante este hecho por parte del el doctor Canizales sugieren: “¿Cómo un animal que ha pasado toda su vida a una temperatura promedio de 11 grados, recibe ahora como regalo de parte de la autoridad ambiental una ubicación a nivel del mar que supera una temperatura de 30 grados?”

Según el criterio profesional del doctor Canizales, **si este cambio de hábitat fuese para el establecimiento de un programa reproductivo y de preservación de la especie, lo primero que hubiese ocurrido es que todos los ciudadanos se habrían enterado** ampliamente de la diligente decisión administrativa.

Para el profesional esa decisión no tiene características de responder a un proceso de este tipo y entonces aparece un nuevo cuestionamiento: “¿Por qué someter a este ejemplar a un cambio de vida tan brusco, máxime si está en un periodo de vida ya longevo para la especie?, ¿Cuál es la determinación técnica o de conservación para hacerlo?, ¿Hubo solicitud de Corpocaldas para realizar alguna alianza con la Universidad de Caldas frente al tema de este ejemplar?”, muchas preguntas y pocas respuestas para los manizaleños que exigen que se devuelva al oso.

Finalmente, llama la atención que el traslado del animal **se da en el marco de la discusión que ha traído el proyecto de urbanización de la empresa constructora CFC. Que planea construir 2200 viviendas** en  una de las zonas más biodiversas del mundo, y cuyo lugar es el hogar de Chucho. [(Le puede interesar: Manizales está de carnaval en defensa del Río Blanco)](https://archivo.contagioradio.com/manizales-se-moviliza-para-defender-el-rio-blanco/)

###### Reciba toda la información de Contagio Radio en [[su correo]
