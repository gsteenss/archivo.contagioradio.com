Title: El 10 de Enero podría destrabarse proceso de paz con el ELN
Date: 2016-12-30 12:14
Category: Nacional, Paz
Tags: conversaciones de paz, ELN, Pablo Beltrán
Slug: el-proximo-10-de-enero-podrian-destrabarse-las-conversaciones-de-paz-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/entrevista-eln.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: hispantv] 

###### [30 Dic 2016]

Tanto la delegación de paz del ELN, en cabeza de Pablo Beltrán, como la delegación del gobierno nacional en cabeza del ex ministro Juan Camilo Restrepo, tendrán una nueva cita el 10 de Enero en Quito, Ecuador, para evaluar y definir los procedimientos y los acuerdos que darían fin a una etapa confusa en el marco de las conversaciones de paz.

### **Gobierno y ELN ¿a consultas?** 

Los integrantes de la delegación de paz del ELN afirmaron, a principios del mes de Diciembre que volverían a Colombia a hacer consultas con sus frentes, tomar decisiones y volver a Quito. Es conocido que los mecanismos de decisión del esa guerrilla son diferentes por lo que es necesario llevar a consensos para que se tengan salidas concretas a las situaciones que se presentan en la mesa. ([Le puede interesar: Ejercito de Liberación Nacional ratifica su disposición de paz](https://archivo.contagioradio.com/eln-ratifica-su-compromiso-con-la-paz/))

Por su parte, el gobierno nacional insistió en que la liberación de Odín Sánchez era un punto indispensable para el inicio de la mesa de conversaciones, pero también, frente al acuerdo de nombrar gestores de paz a dos integrantes del ELN, afirmó que quienes sean delegados deben cumplir con unas condiciones que tampoco se han conocido públicamente.

### **Algunos afirman que conocer los avances es un punto clave para el respaldo al** **proceso** 

Y es que esa es justo una de las grandes exigencias de algunos sectores sociales como la Mesa Social para la Paz. Si la mayoría de los colombianos desconocen las razones por las que se ha retrasado el inicio de la mesa, es necesario que se revelen los acuerdos previos, como el que definió la liberación de Sánchez y el nombramiento de dos gestores de paz detenidos en las cárceles colombianas. ([Lea también 10 de Enero día clave](https://archivo.contagioradio.com/10-enero-se-retomaran-dialogos-eln/))

Lo cierto es que aunque los rumores y las especulaciones son muchas, también hay muchas esperanzas de que se destraben las conversaciones de paz con esa guerrilla y se pueda entrar en una fase de post acuerdos que también podría significar la concentración de la fuerza en el combate y desarticulación de los grupos paramilitares.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)
