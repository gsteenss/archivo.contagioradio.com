Title: "El ambiente debe ser incluido en los acuerdos de paz", Mario Godinez
Date: 2015-09-23 15:33
Category: Ambiente, Nacional
Tags: Ambiente, Bogotá, colombia, Encuentro de las Américas, Guatemala, Madre tierra, posconflicto, proceso de paz, Universidad de San Carlos, víctimas de la guerra
Slug: el-ambiente-debe-ser-incluido-en-los-acuerdos-de-paz-mario-godinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/La-madre-tierra-bombardeada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [confidencialcolombia.com]

<iframe src="http://www.ivoox.com/player_ek_8571140_2_1.html?data=mZqkk5aYdI6ZmKiak5WJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fwtLPy8rSuMafxcrPx5DXqdOfytPQztrNqNCfxtOYztTXb8LX1srfxtTXb8XZjNXO3JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Godinez, Guatemala] 

En el marco del Encuentro de las Américas frente al Cambio Climático y teniendo en cuenta que Colombia está próxima a vivir un escenario de posconflicto, uno de los temas de los que se habla esta semana, **es sobre la naturaleza como una de las una de las víctimas del conflicto armado.**

Mario Godínez, quien hizo parte de la comisión de paz y desminado del congreso de Guatemala y hoy es el decano electo de la Facultad de Agronomía de la Universidad de San Carlos, de ese mismo país, es uno de los panelistas que hace parte de los foros que se realizan durante el Encuentro de la Américas.

**Él señala que los bombardeos, minas, incendios de bosques naturales y la tala de árboles, entre otras, son acciones usuales que se reproducen en medio de la dinámica de la guerra** y que alteran la naturaleza impulsando los efectos del cambio climático.

En el caso guatemalteco, según Godinez, el ambiente como víctima  fue un tema que en los acuerdos de paz fue invisibilizado, es por eso, que el profesor afirma que **en Colombia el actual proceso de paz y el movimiento social tienen todo la oportunidad para demostrar la importancia que tiene el ambiente en el post-conflicto** y así mismo, que este sea incluido en los acuerdos.

El decano electo de la Universidad de San Carlos, asegura que lo importante es que tanto en Colombia como en Guatemala, los movimientos de mujeres, jóvenes, campesino e indígenas se están empezando generar acciones de resistencia en contra de los proyectos como la megaminería o las represas que empeoran la crisis climática.
