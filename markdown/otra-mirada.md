Title: Otra Mirada
Date: 2014-11-25 15:50
Author: AdminContagio
Slug: otra-mirada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/OTRA-MIRADA-5.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/OTRA-MIRADA-6.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/OTRA-MIRADA-8.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/OTRA-MIRADA.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/OTRA-MIRADA-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/OTRA-MIRADA.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/OTRA-MIRADA.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/OTRA-MIRADA-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Feminicidio.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/GAB3316.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/marcha-por-la-dignidad.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/ETCR-ITUANGO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ecopetrol-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/La-verdad-del-pueblo-negro.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/independencia-bandera.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Congreso-de-la-República.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Jornada-por-la-vida-de-líderes.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/salud.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Marcha-por-la-dignidad-4.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/justicia-transicional-contagioradio.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Catatumbo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/campamento-matrícula-cero.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/2943408701_ae1ba4c356_c.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Nariño.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/procuraduría.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/asentamientos-i.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/aspersion-aerea-de-cultivos-ilicitos.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/corte-suprema-de-justicia-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-10-at-23.50.49.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/paro.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### OTRA MIRADA

[![56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400.jpg "56 niños palestinos fueron asesinados en 2018 por el ejército Israelí"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)

###### [56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)

[<time title="2019-02-21T14:29:29+00:00" datetime="2019-02-21T14:29:29+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)Un informe de la organización Defense for Children International- Palestine revela que 56 niños palestinos fueron asesinados por Israel durante 2018[LEER MÁS](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)  
[![Las contradicciones en nueva condena contra Lula](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Luiz-Inacio.jpg "Las contradicciones en nueva condena contra Lula"){width="700" height="469" sizes="(max-width: 700px) 100vw, 700px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Luiz-Inacio.jpg 700w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Luiz-Inacio-300x201.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Luiz-Inacio-370x248.jpg 370w"}](https://archivo.contagioradio.com/contradicciones-nueva-condena-lula/)

###### [Las contradicciones en nueva condena contra Lula](https://archivo.contagioradio.com/contradicciones-nueva-condena-lula/)

[<time title="2019-02-07T12:32:46+00:00" datetime="2019-02-07T12:32:46+00:00">febrero 7, 2019</time>](https://archivo.contagioradio.com/2019/02/07/)En el proceso conocido como “casa de campo Atibaia” no se han presentado evidencias que den cuenta de los beneficios que argumentan recibió Lula da Silva[LEER MÁS](https://archivo.contagioradio.com/contradicciones-nueva-condena-lula/)  
[![El mensaje de despedida del diputado gay amenazado en Brasil](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/FB_IMG_1548463892081-720x550.jpg "El mensaje de despedida del diputado gay amenazado en Brasil"){width="720" height="550"}](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)

###### [El mensaje de despedida del diputado gay amenazado en Brasil](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)

[<time title="2019-01-26T14:41:26+00:00" datetime="2019-01-26T14:41:26+00:00">enero 26, 2019</time>](https://archivo.contagioradio.com/2019/01/26/)Jean Wyllys primer diputado abiertamente gay en la historia de Brasil se vió obligado a no asumir su curul por tercera vez consecutiva tras recibir amenazas[LEER MÁS](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)  
[![“El final de mi vida esta cerca…” Fujimori tras volver a prisión](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400.jpg "“El final de mi vida esta cerca…” Fujimori tras volver a prisión"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/fujimori-vuelve-prision/)

###### [“El final de mi vida esta cerca…” Fujimori tras volver a prisión](https://archivo.contagioradio.com/fujimori-vuelve-prision/)

[<time title="2019-01-24T12:30:55+00:00" datetime="2019-01-24T12:30:55+00:00">enero 24, 2019</time>](https://archivo.contagioradio.com/2019/01/24/)Según la Corte de Justicia y el Instituto de medicina legal los problemas cardíacos de Fujimori pueden atenderse de forma ambulatoria en centro carcelario[LEER MÁS](https://archivo.contagioradio.com/fujimori-vuelve-prision/)

###### [Hijo de tigre… las cuestionadas acciones de Flávio Bolsonaro](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/)

[<time title="2019-01-23T13:57:53+00:00" datetime="2019-01-23T13:57:53+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Además de las dudas por sus cuentas bancarias, se cuestiona a Bolsonaro su relación con la familia del principal sospechoso del asesinato de Marielle Franco[LEER MÁS](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/)

###### [MIgrantes venezolanos piden calma tras ola de agresiones en Ecuador](https://archivo.contagioradio.com/60265-2/)

[<time title="2019-01-22T11:55:00+00:00" datetime="2019-01-22T11:55:00+00:00">enero 22, 2019</time>](https://archivo.contagioradio.com/2019/01/22/)diante solicitud presentada a la cancillería de Ecuador, organizaciones rechazaron los actos de violencia desatados tras un feminicidio ocurrido en Ibarra[LEER MÁS](https://archivo.contagioradio.com/60265-2/)
