Title: Según Jorge Riechmann para salvar el planeta debemos...
Date: 2015-09-29 17:14
Category: Ambiente, infografia, Nacional
Tags: Bogotá, cambio climatico, Crisis climática, Encuentro de las Américas Frente al Cambio Climático, Jorge Riechmann, Universidad Minutos de Dios
Slug: segun-jorge-riechmann-para-salvar-el-planeta-debemos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/foto-riechmann.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [pendientedemigracion.ucm.es]

<iframe src="http://www.ivoox.com/player_ek_8711037_2_1.html?data=mZyek5WXe46ZmKiakpmJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bbhqigh6elsoy%2B0NfUx5C2rcbXydLO0NOPtMLmwpDgw9HapdOfxtGY0tHFssbowpDRx8fJsdDnj5Oah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jorge Riechmann] 

###### [22 de Abril] 

En el marco del **Encuentro de las Américas Frente al Cambio climático,** en Bogotá, se realizaron diferentes eventos en la ciudad sobre temas que tienen que con el calentamiento global, que ha aumentado cada vez más rápido por cuenta de las actividades humanas y por el modelo económico actual.

**Jorge Riechmann,** filósofo, escritor y poeta. Doctor en Ciencias Políticas de la Universidad Autónoma de Barcelona. Profesor titular de Filosofía Moral de la Universidad Autónoma de Madrid y autor de más de una veintena de libros sobre ética ecológica, ecosocialismo y ecología política, hizo parte de estos eventos, uno de ellos, en la Universidad Minutos de Dios, donde realizó una conferencia sobre **"Cambio Climático extractivismo, animales y ética ecológica".**

Allí planteó el panorama actual del planeta tierra que vive según él en una **"crisis climática", causada por el modelo capitalista y con él, el extractivista.** Ante el desalentador escenario Riechmann planteó desde su perspectiva y los diversos estudios científicos sobre el clima, las acciones necesarias y urgentes que se deben empezar a materializar desde los países desarrollados y las élites de los países subdesarrollados, para enfrentar el cambio climático y generar verdaderos cambios, entre ellos dejar los combustibles fósiles en el subsuelo.

![1424367841 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/1424367841-1.jpg){.wp-image-39539 .aligncenter width="624" height="858"}
