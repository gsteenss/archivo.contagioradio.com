Title: Odebrecht la punta del Iceberg de la corrupción en Colombia
Date: 2017-01-17 10:39
Category: Nacional, Política
Tags: Corrupción en Colombia, Odebrecht, Otto Bula
Slug: odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/odebrecht.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [17 Ene 2017] 

En diciembre del año pasado, la Compañía Odebrecht se declaró culpable ante una Corte de Estados Unidos por violar normas a través del pago de sobornos para la adjudicación de obras en su país de origen, Brasil. **Estas declaraciones destaparon redes de corrupción en diferentes países  en donde la organización tenía presencia, Colombia no quedo exenta.**

En la audiencia, Odebrecth admitió  que en Colombia, país en el que está desde hace 24 años, **hizo pagos por  11 millones de dólares para que le adjudicaran obras** de infraestructura como La Ruta del Sol y el tramo de la carretera Ocaña-Gamarra. Luego de estas confesiones, la Fiscalía se comprometió a dar una “guerra frontal” contra la corrupción dando como primeros resultados las capturas de García Morales, exviceministro de Transporte y Otto Bula, exsenador del partido Liberal.

**¿Quién es Gabriel García Morales?**

El primer resultado fue la captura de Gabriel García Morales, exviceministro de Transporte durante el segundo periodo de Álvaro Uribe Vélez, y quién de acuerdo con la investigación, habría **recibido 6.5 millones de dólares por garantizar que Odebrecht obtuviera la licitación del Tramo Dos de la Ruta del Sol**. García aceptó los cargos de cohecho, celebración indebida de contratos y enriquecimiento ilícito, motivo por el cual no tendrá juicio y podrá acceder a rebaja de pena.

**¿Quién es Otto Nicolas Bula Bula?**

El segundo capturado fue el exsenador por el partido Liberar Otto Nicolas Bula, quién habría **recibido una suma de 4.5 millones de dólares** por parte de la organización brasilera, para incluir en el contrato de la vía Ocaña-Gamarra una serie de condiciones que permitieran una contratación directa y no a través de una licitación pública.

Sin embargo, este no es el primer hecho de corrupción en el que Otto Bula se ve inmerso. **Desde 1998 hasta el año 2006, Bula ocupó la curul de Mario Uribe, primo del expresidente Álvaro Uribe, quien salió del congreso por parapolític**a, Bula ha negado todo tipo de negocios con Uribe, sin embargo durante la audiencia de imputación de cargos a Uribe fue mencionado 20 veces.

De otro lado, Otto Bula, de acuerdo con el portal Verdad Abierta, está **involucrado en la compra de la finca Oso Negro y otras tierras en Montes de María, inmersas en el conflicto armado**, adquiridas a muy bajo costo, por la presión de la violencia. Las tierras fueron registradas a nombre de Agropecuaria Montes de María, empresa que no existía cuando Bula firmó las compraventas.

**¿Qué hacer con la corrupción en Colombia?**

De acuerdo con la senadora Claudia López, las alianzas de corrupción entre políticos y empresarios, “le cuesta a los colombianos aproximadamente **60 billones de pesos al año**” y pese que el país tiene un conjunto de normas que en teoría deberían impedir la corrupción, en la realidad esta es una práctica común.

Por lo tanto López afirma que es necesario que se tomen acciones concretas: en primer lugar expone que se debe **cancelar, de manera inmediata, todos los contratos que Colombia tenga con la Constructora Odebrecht**, que además tiene demandado al Estado por un billón de pesos.

En segundo lugar Claudia López insiste en que debe darse un cambio en la forma de hacer política “**mientras las políticas en Colombia compren votos**, existan presidentes que compran congresistas y congresistas que acepten coimas y compran contratistas” no van a haber mayores transformaciones. López expresa que es importante que sea la ciudadanía la que haga el papel de veedurías, ya que finalmente son los ciudadanos los que eligen a los miembros del Congreso hoy implicados en estos casos con Odebrecth.

Ya por último la senadora señala que además, **la Fiscalía y las autoridades correspondientes deben estar en la capacidad de otorgar justicia** “la Fiscalía General de la Nación se hizo la ciega durante  8 años porque están metidas personas muy importantes, el vicepresidente, el señor Luis Carlos Sarmiento Angulo, entonces yo espero que la justicia no solo coja a los funcionarios públicos sino también a los privados”.

<iframe id="audio_16421779" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16421779_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
