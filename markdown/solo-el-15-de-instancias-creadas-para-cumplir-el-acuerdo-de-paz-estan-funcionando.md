Title: Solo el 15% de instancias creadas para cumplir el Acuerdo de Paz están funcionando
Date: 2019-09-05 18:06
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz, firma del acuerdo, paz
Slug: solo-el-15-de-instancias-creadas-para-cumplir-el-acuerdo-de-paz-estan-funcionando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/firma-acuerdo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Efraín Herrera - SIG] 

Ad portas de cumplirse tres años de la firma del **Acuerdo de paz** entre el Gobierno y las FARC EP, más de 120 delegados y delegadas de la sociedad civil se han reunido para realizar la primera “Cumbre de Delegados y Delegadas No Estatales a las Instancias Derivadas del Acuerdo Final de Paz” con el fin de encontrar alternativas que permitan dar fin a la parálisis en la que se encuentra la implementación.

Con la firma del Acuerdo se crearon 20 instancias nacionales y territoriales, encargadas de coordinar diversos aspectos como la participación política, la transformación estructural del campo, la reparación para las víctimas y el establecimiento de garantías a personas defensoras de derechos humanos, partes integrales de la  mesa de La Habana y que hoy no avanzan al ritmo esperado.

**Franklin Castañeda, presidente de la Fundación Comité de Solidaridad con los Presos Políticos** asegura que este es el momento ideal para realizar esta cumbre ante el difícil momento que atraviesa la paz en Colombia sin embargo señala que no solo se convoca a esta tras la decisión que tomaron Iván Márquez y Jesús Santrich al retomar las armas, argumentando que existe una gran responsabilidad por parte del Gobierno.

### Si no hay participación, no hay implementación del Acuerdo 

El defensor de DD.HH. afirma que aunque desde la sociedad civil se contempló y siempre se tuvo pensado en plantear estrategias ante malestares que podrían surgir a lo largo de la implementación, han logrado constatar que   el principal y actual riesgo es la ausencia de voluntad del Gobierno para cumplir a cabalidad el acuerdo, algo que se ve reflejando en su discurso y en  mecanismos paralelos.

**El 85% de las instancias que nacen del Acuerdo no están funcionando o no lo hacen cómo deberían. Hacemos un llamado a la falta de convocatoria por parte del Gobierno"** destacó el director del Instituto de estudios para el desarrollo y la paz (Indepaz), Camilo González Posso durante el encuentro.

Castañeda señala que actualmente existe una "crisis étnica, de género y de protección" ante la inoperancia de la Comisión de Garantías de Seguridad pactada como parte del acuerdo, "lo que tenemos en concreto es que  no hay voluntad del Estado pera permitir que estos motores del acuerdo puedan funcionar" sentencia. [(Lea también: Cuba y Noruega reiteran su apoyo al Proceso de Paz)](https://archivo.contagioradio.com/cuba-y-noruega-reiteran-su-apoyo-al-proceso-de-paz/)

**"Queremos hacer un llamado conjunto al Estado colombiano para que cambien la narrativa que tienen frente al acuerdo y manifiesten su voluntad"** explicó Castañeda agregando que cuando exjefes de las FARC acuden a rearme, es claro que que se requiere un mayor liderago por parte del Gobierno. [(Le puede interesar: Colombianos en Alemania piden que se avance con la implementación del Acuerdo de Paz)](https://archivo.contagioradio.com/colombianos-en-alemania-piden-que-se-avance-con-la-implementacion-del-acuerdo-de-paz/)

Adicionalmente, el defensor expresó que se propondrá una grna cumbre nacional que incluya al Gobierno, al partido FARC y a los delegados de la sociedad civil para el próximo  24 de noviembre y así buscar estrategias que permitan que para 2020 se dé una implementación acelerada del acuerdo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no" data-mce-fragment="1"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
