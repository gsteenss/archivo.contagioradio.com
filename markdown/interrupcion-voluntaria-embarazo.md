Title: Una nueva barrera de la Corte para la interrupción voluntaria del embarazo
Date: 2018-10-17 14:00
Author: AdminContagio
Category: DDHH, Mujer
Tags: aborto, Corte Constitucional, interrupción del embarazo
Slug: interrupcion-voluntaria-embarazo
Status: published

###### [Foto: @agualaboca] 

###### [16 Oct 2018] 

Este miércoles, la **Corte Constitucional podría tomar una decisión sobre recortar el tiempo de gestación en el que una mujer puede practicar la interrupción voluntaria de su embarazo** en caso de las 3 causales establecidas por el alto tribunal, en la sentencia **C-355 de 2006.** La nueva decisión la tomaría la Corte gracias a una demanda que interpuso una persona natural, pidiendo que se regule este derecho.

El año pasado, un ciudadano interpuso una demanda para que se regulara el tiempo de gestación hasta el que las mujeres pueden interrumpir voluntariamente su embarazo; esto, en razón de que según el demandante, el Congreso debía legislar para determinar hasta qué etapa de la gestación se podía desarrollar el procedimiento. (Le puede interesar: ["Mujeres imparables, 20 años de lucha por la despenalización del aborto en Colombia"](https://archivo.contagioradio.com/mujeres-imparables-20-anos-de-lucha-por-la-despenalizacion-del-aborto-en-colombia/))

Sin embargo, desde diferentes organizaciones han señalado que la medida podría ser una obstrucción más para que las mujeres accedan al derecho de decidir sobre su cuerpo. Para **Laura Torres, integrante de Católicas por el Derecho a Decidir**, **fijar un límite para la interrupción voluntaria del embarazo hasta los 6 meses sería desconocer las barreras de acceso a los servicios de salud y de justicia**, para cumplir con lo reglamentado en la sentencia de 2006.

Torres sostuvo que, tras 12 años de esta sentencia, **el hecho de que no hubiese un límite a la edad gestacional en la que se puede interrumpir el embarazo es una gran ventaja,** porque muchas de las mujeres que pueden someterse al procedimiento, llegan a un estado avanzado del embarazo por las ineficiencias en el sistema de salud, y por decisiones judiciales que no les garantizan lo consagrado en la sentencia de la Corte. (Le puede interesar: ["A 12 años de la despenalización del aborto, Colombia aún tienen barreras"](https://archivo.contagioradio.com/a-12-anos-de-la-despenalizacion-del-aborto-colombia/))

La activista recordó que la decisión sobre la maternidad no se remite únicamente a la gestación, sino que es un proyecto de vida a largo y plazo; y manifestó su preocupación ante la posible determinación de la Corte, razón por la que le pidió al alto tribunal **que escuche a las mujeres, y garantice lo que ya está establecido en la sentencia**, "que es lo mínimo que se debe garantizar" a las mujeres.

<iframe id="audio_29403342" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29403342_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo] [[Contagio Radio]
