Title: Ante la dictadura de Caracol Radio, RCN y Caracol TV: guerrilla comunicativa
Date: 2017-10-31 09:16
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Canal RCN, Caracol Noticias, Caracol TV, medios de comunicación, RCN
Slug: ante-la-dictadura-de-caracolradio-rcn-y-caracol-tv-guerrilla-comunicativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/guerrilla-comunicativa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 31 Oct 2017 

[¿Cuándo descubrimos que las posiciones editoriales de Caracol Radio, Caracol TV y RCN se constituyen en una tremenda dictadura?]

[En primer lugar, el carácter corporativo de dichos medios masivos de información, ya debería hacer sospechar a cualquiera, pues ese carácter corporativo se manifiesta en su gigantesca capacidad técnica para generar coberturas que medios de información alternativa no podría financiar. Incluso, el propósito mercantil de la publicidad paga bastante bien, pero si se mira objetivamente, el dueño de una empresa capaz de pagar cientos de pautas publicitarias, maneja capitales relativos a los del dueño del canal… Simio apoya a simio.]

[En segundo lugar, cabe aclarar que tener dinero no es un crimen, no comenzaré aquí con mamertiaderas al mejor estilo de algunos derechistas cuando critican a un socialista porque toma whisky o como otros mamertos que creen que son capitalistas porque se gastan su salario. La verdad, el dinero no es el problema,]**el problema es cuando una razón económica dominante se constituye en una razón política que se quiere presentar como “neutral”, pues de éstas dos, emerge una necesidad de auto-protección.**

[Todo comienza por la fuerza: un carro blindado, unos escoltas. Luego continúa la protección sociológica: la afiliación a un club, las altas amistades en común, la foto en la revista del jet set, la invitación a la boda, la platica en Panamá para no pagar impuestos en Colombia, el juego de golf los domingos, etc.]

[Por último, se curan en salud tratando de que la razón económica dominante permanezca intacta, incuestionable… ¿Cómo logran esto? ¿Cómo hacer para que pequeño-burgueses, que pasan más tiempo humillados que viviendo, defiendan la causa que los humilla? ¿cómo hacer para que la voz de los más pobres sea tergiversada elegantemente llamando “]*[enfrentamiento]*[” a una clara represión policial contra indígenas en Bogotá o llamando “]*[tiroteo]*[” a un claro fusilamiento de campesinos en Tumaco? ¿cómo hacen para creer que todos somos imbéciles?]

[Sencillo: despolitizando la sociedad, estableciendo un consenso previo sobre los problemas que se supone todos deberíamos andar mencionando, eso Bourdieu ya lo había planteado pero… ¿cómo establecer consensos frente a una temática? ¿cómo generar opiniones sobre cosas que la gente ni conoce? ¿cómo lograr que se pongan filtros en Facebook de una bandera francesa cuando estalla una bomba en París, pero que no se coloquen filtros de la bandera Siria cuando estallan bombas en Damasco?]

[¿Cómo hacer para que la gente no sospeche como es que hablan con María Corina Machado cerca de 5 horas mensuales en Caracolradio y no hablen ni media hora con los familiares de campesinos fusilados por la fuerza pública en Tumaco? ¿Cómo logran que nadie tilde de violentos a Darío Arizmendi y su combo promoviendo golpes de Estado contra Venezuela y que el título de este artículo sí parezca “ofensivo”? y sobre el ataque a la sede de Agencia Prensa Rural ¿por qué los grandes medios no arman un escándalo pasando día y noche “la nota”?... ¿podríamos si acaso imaginar el tamaño del escándalo donde alguien hubiese ingresado al canal RCN a robarse los computadores y a dejar amenazas?... ¿por qué se hace evidente solo un problema y no otro?   ]

**Es fácil: quien domina la economía invierte en los medios masivos de información.**

[Una razón económica dominante, se constituye en una razón política dominante cuando se invierte mucho dinero en medios masivos de información ¡es igual que en las campañas!, entonces se invierte no solo en uno sino en varios, ya que dueños, directores e inversionistas se tejen sociológicamente como un grupo de poder, generando múltiples expresiones de su libertad que se manifiestan en varios periódicos, varios canales, varios sitios web, incluso varios pasquines que se regalan en las calles… aparentemente muchos, pero esencialmente todos protegiendo un solo interés particular (el dominio en lo económico).]

[Aparentemente hay mucha libertad de expresión en los medios masivos de información, pero toda se halla anclada a la razón económica dominante. ¿Cuál es el resultado de esto? Una posición editorial que no cambia, que se vuelve dictatorial,]**no porque exista un único medio de información, sino en tanto grandes medios masivos, así sean varios, todos responden al interés particular de un grupo de poder… allí está la dictadura.**

[Caracol Radio y su hostigante, desgastante, fastidioso y hasta degradante fetichismo Peñalosista, RCN y su trasformación en aparato propagandístico del partido ultra derechista colombiano “Centro Democrático”, Caracol TV y su amañada entrevista de 40 y tantos minutos en horario principal al candidato del partido más corrupto de Colombia señor Vargas Lleras; Semana y su falaz “posición crítica” es una revista de un Santos; El Tiempo de Sarmiento Angulo tratando de dividir con la portada del 25 de octubre de 2017 la lucha cocalera y campesina; es que incluso “como para llorar” el youtuber más famoso de Colombia es de apellido Samper… claro, visto con rapidez hay diferencia, visto con rapidez, son varios medios y no uno solo…. Entonces, ¿cuál dictadura?]

[Pero si uno se detiene… Arizmendi, Gurisatti, Santos, Sarmiento, Santo Domingo, Samper, si bien todos no son conceptualmente una “oligarquía” ¿alguien podría negar que constituyen todos juntos ese]**único grupo de poder**[ que es dueño de casi todo en Colombia? …   simio no mata simio… de pronto se halan de las mechas un ratico, pero esos se contentan rapidito, con un jueguito de golf o un tintico en la cafetería del Nogal.]

[El interés económico dominante de un único grupo de poder, constituye una razón política que quiere ser dominante a través de la posición editorial que viaja en los medios masivos de información. Por eso hace poco Luis Carlos Vélez de Caracol Radio elogiaba entre líneas a Trump, por considerarlo un pragmático que no se detiene en la política sino que opera en función de la economía. Claaaaro…. Cuando tienes dominada la economía, la política incomoda, pues lo único que puede romper ese dominio es la politización de la sociedad, por eso “menos política y más economía” grita el neoliberalismo, por eso los partidos políticos en Colombia más parecen empresas y no componentes ideológicos de un proyecto común, colectivo. Por eso no poca gente dice]*[“la política es una mierda”]*[ ¡qué bien domesticados están!  ]

[¿Qué hacer? Bueno, pues contra los escoltas nada que hacer, no nos dejarían ingresar al club a jugar golf a ver si así nos dan un consulado como a Lord Ricostillla; ni modo de enviar la platica a Panamá para evadir impuestos (no hay pa tanto); ni modo de decir que tenemos doctorado sin la compulsa de La Haya y el ministerio de educación… como dice una famosísima politóloga de los Andes,]*[que estudien los vagos]*[ ¿o no?  ]

[¿Qué hacer contra los millones de fanáticos seguidores de los medios masivos de información que mientras opinan lo que les ponen a opinar, no se dan cuenta que tejen de manera muy fuerte su propia servidumbre al sistema político fracasado que existe en Colombia?  ]

[¿Decirles que están equivocados para que en vez de evaluarlo crean que es un capricho que atenta contra su supuesta individualidad? ¿seguir perdiendo amigos por supuestas “opiniones políticas”? que a la final no son políticas sino una repetición de lo que esta dictadura mediática impone sin la fuerza pues, ¿para qué la fuerza cuando tienes asegurada la capacidad técnica?  ¿qué hacer entonces contra un poder mediático tan gigantesco?]

**Guerrilla comunicativa. No hay de otra.**

[Guerrilla comunicativa sin ningún tipo de estructura organizada bajo lógicas de cuadros de mando. No. Nada de eso. Simplemente Guerrilla comunicativa. Un cuerpo compuesto por personas comunes y corrientes indignadas por lo que sucede en Colombia y sobre todo, indignadas con la forma en que los grandes medios masivos de información narran la realidad, hablando en función de ese interés particular, mostrándose neutrales, pero eso sí, saliéndose de casillas y siendo bastante ofensivos cuando el dedo con el que siempre apuntan da la vuelta y les señala.]

[Guerrilla comunicativa, ese cuerpo sin estructura visible, compuesto por personas que con celular en mano, y que con algunas nociones de castellano logren comunicar y no sólo informar. Logren ser la voz de los que no tienen voz, logren desmentir mediante pequeños golpes de opinión la dictadura mediática que hoy existe en Colombia.]

[Guerrilla comunicativa para sentirnos más éticos. Guerrilla comunicativa, para aportar a la descomposición del poder dominante en Colombia. Guerrilla comunicativa para que no crean los gurús de los grandes medios, que sus opiniones son valiosas]*[per se]*[, sino que alcancen siquiera a dudar, que sus opiniones tan sólo son importantes porque tienen la ventaja de la técnica: antenas, satélites y equipos más grandes, producto de la gigantesca inversión que el grupo dominante ha hecho.]

[Guerrilla comunicativa, para que con celular en mano, con conocimiento de la situación antes de publicar, con menos groserías y más argumentos, desmontemos la dictadura de los medios masivos de información en Colombia.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
