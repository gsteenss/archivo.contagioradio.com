Title: Habitantes de Iscuande, Nariño, en paro porque sus niños no tienen educación
Date: 2018-04-11 15:03
Category: Educación, Nacional
Tags: educacion, Iscuande, nariño
Slug: habitantes-de-iscuande-narino-en-paro-porque-sus-ninos-no-tienen-eduacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/WhatsApp-Image-2018-04-11-at-11.58.45-AM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paro Iscuande] 

###### [11 Abr 2018]

Los habitantes de Iscuande, en el departamento de Nariño denunció que **100 estudiantes de la Insitución Educativa Politécnica de Santa Bárbara, se encuentran sin clases y 200** más han migrado a otras instituciones, debido al incumplimiento del gobierno departamental en la contratación de más docentes, la falta de una estructura apropiada para las clases y dotación. Razón por la cual, la comunidad académica irá a paro.

En total, son **17 docentes los que hacen falta en las institución para poder dictar clases y son más de 1.100** estudiantes, tanto del casco urbano como de las veredas aledañas a este municipio, los que se han visto afectados con la falta de garantías por parte de la gobernación para poder estudiar.

De acuerdo con la información de la comunidad, **los 200 estudiantes que han migrado hacia otros centros educativos,** lo han hecho principalmente hacia instituciones del Valle del Cauca para continuar con su educación. (Le puede interesar: ["Estudiantes de U. públicas y privadas le apuestan a la lucha por la educación superior"](https://archivo.contagioradio.com/estudiantes-de-universidades-publicas-y-privadas-le-apostaran-a-la-lucha-por-la-educacion-superior/))

### **Las acciones de los habitantes para exigir medidas** 

Hoy los habitantes realizaron una movilización y un encuentro en el que se decidió cerra la Alcaldía, el Banco, el Juzgado y el comercio con la finalidad de exigir la presencia de las autoridades correspondientes, como la secretaría de educación y el gobierno departamental, y acordaron que **el paro no se levantará hasta que las mismas no hagan presencia en el territorio y de soluciones permanentes a esta problemática**.

Desde el pasado 3 de febrero, la comunidad ya venía haciendo jornadas de paro que fueron intermitentes hasta inicios del mes de marzo. A este hecho se suma el paro de los trabajadores de la EPS municipal que habría iniciado desde el pasado 12 de marzo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
