Title: Con Francia como invitado de honor inicia Eurocine 2017
Date: 2017-04-21 08:52
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Cine, colombia, Eurocine, europeo
Slug: eurocine-bogota-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Cartel-Eurocine.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Eurocine 

##### 20 Abr 2017 

Con una muestra de **46 películas de 15 países**, llega a Bogotá, Medellín, Cali, Pereira, Barranquilla y Bucaramanga el Festival de Cine Europeo en Colombia EUROCINE 2017, que se realizará **ente el 19 de abril y el 10 de mayo**, en las principales salas  y otros espacios culturales de esas ciudades.

Para este año el festival traerá de nuevo toda la **diversidad social y cinematográfica del viejo continente**, presentando como **invitado de honor a Francia**, con una selección de 12 películas y una retrospectiva del **director francés Francois Ozon**, quien dirigió mas de 15 producciones en su carrera fílmica.

Como ya es costumbre, Eurocine presentará algunas de sus secciones habituales: S**ección oficial, En Foco, Arte y Cine, Clásicas, Eurocine Comunitario y Adentro/Afuera**, siendo esta última el focal de la presente edición, la cual abordará cintas dedicadas al tema de la **exclusión e inclusión**. Le puede interesar: [Silence más que una película perfecta para Semana Santa](https://archivo.contagioradio.com/silence-cine-scorsese/).

<iframe src="https://www.youtube.com/embed/G3TqPNFfzI8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Como novedad, Bogotá no sera la única ciudad de Cundinamarca a donde llega Eurocine, **la capital contará con Cajica como “ciudad hermana”** donde se exhibirá una selección de 8 **películas del festival durante 8 días en 16 funciones**. Otras 13 poblaciones podrán disfrutar del Festival gracias a la sección comunitaria del evento.

Los 46 largometrajes que hacen parte de la [programación](http://www.festivaleurocine.com/programacio%CC%81nEUROCINE2017%202.pdf) de Eurocine 2017 provienen de **Alemania, Bélgica, Países Bajos, Irlanda, Austria, Dinamarca, España, Italia, Portugal, Finlandia, República Checa, Suiza, Bulgaria y Hungría**.

 
