Title: Un año sin Andrés Camilo, momento para conmemorarlo y discutir sobre la violencia de Estado
Date: 2019-07-03 16:06
Author: CtgAdm
Category: yoreporto
Tags: Andrés Camilo Ortíz, Violencia de Estado
Slug: un-ano-sin-andres-camilo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/DSC0176.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo de Trabajo Nosotros  
] 

El jueves 13 de junio el Colectivo de Trabajo Nosotros, en colaboración con las directivas de la Facultad de Ciencias Económicas de la Universidad Nacional de Colombia y la familia de [Andrés Camilo Ortiz](https://archivo.contagioradio.com/universidad-nacional-se-moviliza-contra-asesinato-de-andres-camilo-ortiz/) convocó a la comunidad universitaria a una jornada de diálogo y reflexión para construir memoria y conocer, a partir de los testimonios de familias de víctimas de la violencia del Estado, sus historias, preocupaciones y posturas frente a los procesos que han vivido.

El contexto del evento denominado ***“Violencia del Estado en Colombia: Reflexiones desde la memoria, la justicia, la reparación y la verdad”***  se realizó a propósito de la conmemoración de un año del asesinato de nuestro compañero Andrés Camilo Ortíz Jiménez, estudiante de tercer semestre de contaduría pública de la Universidad Nacional, que dados los fatídicos sucesos ocurridos en la noche del 15 de junio de 2018, perdió su vida a causa de un disparo de un integrante de la fuerza pública.

Éste es uno de muchos otros casos que, por desgracia, a diario acontecen en nuestro país. La violencia del Estado y de los miembros de sus instituciones es clara y manifiesta. La manera a través de la cual se materializa esta violencia tiene múltiples formas y atroces presentaciones como asesinatos selectivos, "falsos positivos", desaparición forzada, entre otros, generando profundas implicaciones en las familias y los amigos de las víctimas.

A partir de estos hechos y de la oportunidad de reflexión y discusión que nos brindan este tipo de espacios, creemos en la posibilidad de seguir formando y luchando por una Universidad crítica y con una enorme potencialidad de transformar nuestra realidad a partir del trabajo colectivo y el diálogo amplio con diferentes sectores de nuestro país. Por ello, reconocemos la complejidad de estos procesos y consideramos que la discusión en torno a la v*iolencia del Estado*, *la memoria, la justicia, la reparación y la verdad* debe recoger múltiples posturas, puntos de vista y vivencias para construir sentidos colectivos y sólidas formas de lucha; más aún, en épocas en donde la *verdad oficial* busca instaurarse a como dé lugar y los mecanismos institucionales funcionan en desmedro del bien común y de la propia verdad.

Para ello, se realizó esta jornada conmemorativa a la que asistieron algunos docentes e investigadores como **Rosembert Ariza Santamaría, Martha Elena Huertas, Frank Molano y César Santoyo Santo** que compartieron con la comunidad académica algunas reflexiones y comentarios sobre este tipo de procesos y hechos que acaecen en nuestro país, teniendo presente que algunos de ellos en su trayectoria académica y profesional los han conocido y acompañado. También nos acompañaron algunos familiares de varias víctimas de crímenes por abuso de la fuerza pública, como **Érika Ortiz Jiménez, Jacqueline Castillo, Ómar Zambrano, Gloria Alvarado y Luis Concha,** quienes tuvieron la oportunidad de comentar sus vivencias y sentires, luego de los lamentables hechos que han acabado con la vida de sus seres queridos.

Al final de la jornada tuvimos la oportunidad de realizar una velatón frente al mural que ha sido pintado en memoria de Andrés Camilo en la Faculta de Ciencias Económicas. Este acto conmemorativo tuvo el acompañamiento musical del grupo distrital Canto Vital, quienes interpretan música latinoamericana y tienen gran sensibilidad en temas sociales.

\[caption id="attachment\_69918" align="aligncenter" width="482"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/DSC0176-300x200.jpg){.wp-image-69918 width="482" height="321"} Foto: Colectivo de Trabajo Nosotros\[/caption\]

Agradecemos a todos y todas quienes asistieron y participación en este evento conmemorativo. Hacemos un llamado hacer memoria y a seguir en la búsqueda de una justicia social.

¡Por nuestros muertos, ni un minuto de silencio!

***Colectivo de Trabajo Nosotros***

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>
