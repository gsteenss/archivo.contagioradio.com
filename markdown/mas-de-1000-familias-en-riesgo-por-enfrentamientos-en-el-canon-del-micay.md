Title: Más de 1000 familias en riesgo por enfrentamientos en el Cañón del Micay
Date: 2020-01-29 16:52
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cañón del Micay, El Tambo, ELN
Slug: mas-de-1000-familias-en-riesgo-por-enfrentamientos-en-el-canon-del-micay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cañon-del-Micay.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Río-Micay.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @COL\_EJERCITO {#foto-col_ejercito .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Habitantes de la cuenca del Río Micay, en Cauca, denunciaron que por enfrentamientos entre el Ejército de Liberación Nacional (ELN) y el Ejército se presentaron tres personas detenidas, nueve desaparecidas y una muerta, todas, integrantes de la comunidad. La situación se viene presentando desde el pasado 25 de enero, y las más de mil familias que habitan la zona temen que los hechos se repitan.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Las afectaciones no fueron a la insurgencia"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un comunicado de la Red por la Vida y los Derechos Humanos del Cauca, las comunidades denunciaron que desde el pasado sábado 25 de enero "vienen sufriendo una difícil situación humanitaria" por cuenta de acciones bélicas que **involucran la Fudra N°4 (Fuerza de Despliegue Rápido) del Ejército y al ELN.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades señalan que tras los bombardeos a cargo de la Fuerza Pública y los enfrentamientos entre ambos actores armados, se ha presentado **"un muerto en Medicina Legal, José Antonio Riascos, quien pertenecía a la comunidad e integrante de la Junta del Consejo Comunitario; al menos 3 civiles retenidos por el Ejército**, 9 personas desaparecidas de la comunidad" y una herida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según señaló Guido Rivera, integrante de la Red por la Vida y quien pudo establecer comunicación con habitantes de la zona, el bombardeo se registró a media hora del caserio Betania, en el Consejo Comunitario Afro Renacer. "Los habitantes de allá dicen que las afectaciones no fueron a la insurgencia sino a ellos", porque aún no se conoce de heridos en ninguno de los actores armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, Rivera dijo que las personas reportadas como desaparecidas ya han comenzado a salir a poblados, tras el cese de los combates. De igual forma, las tres personas capturadas y señaladas por el Ejército de ser integrantes del ELN fueron dejadas en libertad porque no se encontraron pruebas en su contra, mientras está por esclarecer los hechos alrededor de la muerte del poblador de la zona.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RedVidaDHCauca/status/1221934514445877248","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RedVidaDHCauca/status/1221934514445877248

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Habitantes del Cañón del Micay temen por nuevos enfrentamientos armados**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades de la zona solicitaron que el próximo viernes se desarrollará una misión humanitaria para verificar la situación en la zona, y ante el temor de nuevos combates. El miedo estaría sustentado en el constante despliegue de tropas que habitantes han evidenciado en las montañas que conforman el cañón del Río Micay.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a Rivera, en el Consejo Comunitario habitan más de mil familias, que además de los combates, temen que el desembarco de tropas signifique la llegada de la erradicación forzada al Cañón, "porque la mayoría del terreno está cultivado en Coca". (Le puede interesar: ["Seis personas asesinadas y dos heridas en López de Micay, Cauca"](https://archivo.contagioradio.com/masacre-en-lopez-de-micay-en-el-cauca/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Un caldo de cultivo para el conflicto"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rivera explicó que el Cañón del Micay es una zona estratégica que comunica al departamento de Cauca, y el suroccidente con el mar pacífico. De hecho, **las mismas comunidades sin apoyo del Estado se han hecho cargo de la construcción de la vía que conduce hacía el mar**, pero hasta ahora solo llega hasta cerca del municipio de López de Micay.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dada la importancia estratégica del cañón, y la presencia de cultivos de uso ilícito, las comunidades afirman que en la zona además del ELN, hacen presencia las disidencias de las FARC y hombres pagos por el Cártel de Sinaloa. Razón por la que Rivera sostiene que **el territorio es "caldo de cultivo para el conflicto".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de estos intereses, el integrante de la Red por la Vida sostuvo que se espera la construcción de un Hidroeléctrica sobre el Río, y se sabe de solicitudes de concesiones mineras para la extracción de oro. (Le puede interesar: ["La esperanza del Cauca está en sus jóvenes"](https://archivo.contagioradio.com/la-esperanza-del-cauca-esta-en-sus-jovenes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de estos intereses, el integrante de la Red por la Vida sostuvo que se espera la construcción de un Hidroeléctrica sobre el Río, y se sabe de solicitudes de concesiones mineras para la extracción de oro. Por su parte, el Ejército ha señalado que sólo tiene interés en detener a los subversivos, pero la comunidad teme que también inicie la erradicación de la mayoritaria fuente económica que hay en la zona.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
