Title: Asesinan a funcionario de Parques Nacionales en la Sierra Nevada
Date: 2019-01-15 18:36
Author: AdminContagio
Category: Ambiente, DDHH
Tags: Amenazas contra guardabosques, parques nacionales
Slug: asesinan-a-funcionario-de-parques-nacionales-en-la-sierra-nevada
Status: published

###### Foto: @parquesnacionales 

###### 15 Ene 2019 

[Según Información preliminar En horas de la noche, Wilton Orrego, contratista de la Unidad de Parques Nacionales Naturales, adscrito al Parque Sierra Nevada de Santa Marta fue asesinado cerca a su vivienda en la vereda Perico Aguao en Guachaca, área rural de Santa Marta tras ser alcanzado por cinco disparos de arma de fuego a manos de hombres desconocidos.]

[Herido, el contratista fue trasladado hasta el puesto de salud de la localidad donde fue estabilizado, sin embargo no fue posible transportarlo hasta Santa Marta en ambulancia por lo que  fue llevado por la Policía hasta el hospital Universitario ‘Julio Méndez Barreneche’ donde ingresó sin signos vitales.]

Wilton Orrego, quien hacía funciones de control y vigilancia en el parque, no había denunciado amenazas en su contra sin embargo otros integrantes de la institución sí han sido amedrentados, es el caso de Tito Rodríguez, director del parque de La Sierra Nevada, dichas amenazas ya han sido denunciadas por la Defensoría del Pueblo y puestas en conocimiento del Ministerio de Interior y de la fuerza pública un año atrás.

[Según la directora de Parques Nacionales, Julia Miranda Londoño, son actualmente al  menos 12 los parques nacionales en los que sus empleados han recibido amenazas por parte de  grupos al margen de la ley, enre ellos el parque del Tayrona, El Paramillo, La Paya, El Tayrona y El Catatumbo.  
]

###### Reciba toda la información de Contagio Radio en [[su correo]
