Title: Los Monstruos del Yarí
Date: 2016-11-10 12:02
Category: Otra Mirada, Reportajes
Tags: Acu, FARC-EP, Llanos del Yarí
Slug: los-monstruos-de-los-llanos-del-yari
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/X-conferencia-guerrilera110516_129.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Contagio Radio/Gabriel Galindo] 

###### [10 de nov, 2016] 

Desde que llegué de los Llanos del Yarí me he cuestionado el significado del perdón, del castigo y del amor. Fue muy poco tiempo, una semana no es suficiente para comprender las historias detrás de quienes decidieron hacer parte de una de las guerrillas más antiguas de Latinoamérica, las FARC-EP, que hoy decide, bajo la esperanza y la voluntad, dejar las armas para construir Colombia desde las ideas; **sin duda alguna, una página que nos permite como sociedad transformar al país.**

Debo confesar que estaba llena de prejuicios, que imaginaba a los guerrilleros y a las guerrilleras como personas toscas, secas y serias. Llevaba conmigo la construcción que por 23 años me dieron los medios;** imaginé que eran monstruos.** Sin embargo, la vida tenía planeado confrontarme a mi falsa realidad desde que puse los pies en la tierra del sol.

Bernardo, el casero 
-------------------

\[caption id="attachment\_32025" align="alignnone" width="1000"\]![Los monstruos del Yari, escrito por Sandra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/X-conferencia-guerrilera110516_58.jpg){.size-full .wp-image-32025 width="1000" height="667"} Bernardo, integrante  del frente 15 de las FARC, es el encargado de la logística en el campamento.\[/caption\]

Para mi buena suerte, el camping de los periodistas estaba repleto, y solo podíamos dormir en los campamentos guerrilleros. Al equipo de Contagio Radio nos correspondió el campamento oriental, que estaba a cargo de Bernardo, un hombre de no más de 1,60m, con ojos cafés, cabello negro y tez morena.

Él fue quien nos dio la bienvenida al campamento, y desde que nos recibió fue sumamente atento. **Me recordó esa calidez campesina** **de esa gente que aún, con lo poco que tiene, no le importa partir el pan en cuatro pedazos más para que todos estén bien**. De Bernardo recuerdo sus manos y su voz; era suave, tranquila, imperturbable. Nos contó que llevaba mucho tiempo en la guerrilla; aún no me animaba a saber por qué había ingresado ahí. Con él se podía hablar de la vida, del llano, de cómo instalar una caleta y, eso, sin lugar a dudas, me pareció más interesante.

Todos los días había rueda de prensa a las 7:00 a.m, luego se hacían entrevistas. A las 4:00 p.m había otra rueda de prensa y desde las 7:00 p.m comenzaban los actos culturales. Mi primera noche en el Yarí fue una oda a las raíces; Aries Vigoth acompañó la velada y puso a periodistas, guerrilleros y guerrilleras, e invitados a azotar el piso. Yo confieso que me invadió la alegría, me la contagiaban las caras de las y los guerrilleros que por tantas noches acompañaron sus travesías con el arpa y las maracas de Vigoth,y que, por primera vez, podían bailarlo en vivo y en directo. El ambiente se llenó de sonrisas y **creo que, tal vez, eso era un poco la paz; bailar sin miedo a la guerra.**

**Antonio, a bailar tango** 
---------------------------

\[caption id="attachment\_32037" align="alignnone" width="1000"\]![monstruos-del-yari](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/monstruos-del-yari.jpg){.wp-image-32037 .size-full width="1000" height="667"} Antonio, le gusta la tecnología,  por un momento vemos las fotografías de mi celular.\[/caption\]

Al día siguiente el plan era, básicamente, hacer lo más que se pudiera. Nosotros encontramos en el campamento a Antonio de 24 años. Al igual que Bernardo, no era serio, ni tosco, ni seco. Nos relató que ingresó porque quería aprender y la guerrilla le daba la posibilidad. Yo pensé:  "unas por otras", pero esa es una afirmación facilista; es difícil, es doloroso. La guerra es el sacrificio más grande que alguien puede asumir; **de un lado o del otro los costos son altos; dejas de ver a tu familia; tu vida está en constante riesgo.**

Antonio ya había adquirido un sueño más grande. Decía que Simón Rodríguez le enseñó la importancia que tiene que todos se eduquen por igual, y ese es el camino que quiere recorrer. Me mostró los apuntes en su cuaderno, convicciones que asegura no dejará, porque, luego, cuando no empuñe un fusil, quiere regresar a su vereda, hacer política, educar a su comunidad, estudiar sistemas y bailar tango.

Sirley, la enamorada de Mono Jojoy 
----------------------------------

\[caption id="attachment\_32031" align="alignnone" width="1000"\]![Los monstruos del Yari, escrito por Sandra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/X-conferencia-guerrilera110516_233.jpg){.size-full .wp-image-32031 width="1000" height="667"} Sirley fue compañera de Jorge Briceño Suárez,  "*Mono Jojoy*".\[/caption\]

Entre ires y venires nos presentaron a la eterna enamorada del Mono Jojoy. Sirley, una mujer que sólo pudo inspirarme ternura. Su voz es delgada, aguda, piel blanca. Me contó de sus 20 años de amor a uno de los guerrilleros más importantes de las FARC-EP, **del dolor de su muerte y de la ilusión que ahora tiene de construir paz.** El día de la clausura de la X Conferencia de las FARC, me confesó que había estado llorando y recordando cosas buenas y malas, pero que finalmente había llegado a la conclusión de que ese paso tan significativo valía la pena. Luego me abrazó y se me escurrieron un par de lágrimas.

Jorge, el artesano 
------------------

\[caption id="attachment\_32021" align="alignnone" width="1000"\]![Los monstruos del Yari, escrito por Sandra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/X-conferencia-guerrilera100616_94.jpg){.size-full .wp-image-32021 width="1000" height="667"} Jorge aprendió a tejer dentro de la guerrilla. Él me regaló una de sus lagartijas.\[/caption\]

Por último está Jorge. Él me ayudó con mi torpeza en el campamento; me decía que debía caminar más agachada, mirar hacia adelante y no hacia abajo, y asegurarme de no pisar en tierra resbalosa. Un día antes de irnos nos acercamos a su caleta; nos habló de armas y de lo pesadas que son. Yo no podía creer que alguien tan delgado y bajito pudiese cargar tanto peso recorriendo los llanos de Colombia. Nos contó que lo que más disfruta hacer en sus tiempos libres es tejer animales, pulseras o cinturones; que lo aprendió allí en las FARC-EP. Luego sacó de su maleta una lagartija hecha por él y me la regaló. **Un gesto más de bondad que se juntan a todos los que las y los guerrilleros tuvieron con nosotros en el Yarí**.

Caía el sol del último día, y con él **se me acortaba el tiempo para escuchar historias; **relatos, por los que ahora sé que ninguno de mis miedos eran reales; que son personas que aman, lloran, sonríen; que, al igual que yo, temen; que tomaron sus decisiones y asumirán la responsabilidad por las mismas. Sin embargo, me quedo con un anhelo: deseo que todos los colombianos y colombianas nos demos la oportunidad de escucharnos para dejar de vernos como enemigos y enemigas; tal vez para comprendernos.

Hoy me pregunto qué piensan ellos y ellas de todo lo que está pasando; del plebiscito, de las negociaciones, ¿será qué revalúan su decisión?, **¿tendrán miedo de nosotros?, ¿podrían confiar en quienes que no vivimos en la Colombia profunda?,** los monstruos eran de barro, y los seres humanos habitamos en la posibilidad del mañana. Yo espero una lección más de su parte: voluntad.

La guerra nunca debió ser una opción, y acá estamos, ellas, ellos, ustedes y nosotros, claro, con miedo, pero éste no debe primar sobre la dilección. Si las víctimas, las campesinas y campesinos, las y los indígenas construyen paz, yo no seré un obstáculo, porque quiero ver a Antonio bailar tango, a Bernardo sembrando la tierra, a Sirley amando en libertad y a Jorge tejiendo su esperanza. **Quiero volver a abrazarles algún día en una Colombia diferente**, una que no tema, que no juzgue con prejuicios en el corazón, y que se permita perdonar, porque mi mayor aprendizaje en los llanos del Yarí fue haber tenido la oportunidad de escuchar el relato de la Colombia profunda, negada, que ya hace parte de mi historia.

\
