Title: El relleno Doña Juana: la piedra de Sísifo que sigue rodando sobre el sur de Bogotá
Date: 2020-04-29 18:54
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Doña Juana, Relleno Sanitario
Slug: el-relleno-dona-juana-la-piedra-de-sisifo-que-sigue-rodando-sobre-el-sur-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/El-relleno-sanitario-Doña-Juana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @anateresabernal {#foto-anateresabernal .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El martes de esta semana la Unidad Administrativa Especial de Servicios Públicos (UAESP) de Bogotá confirmó un nuevo deslizamiento en el relleno sanitario Doña Juana que superaría las 60 mil toneladas de residuos. Esta **sería la tercera emergencia de este tipo que se presenta después de la más grande que ocurrió en 1997** y otra mediana en 2015.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Uaesp/status/1255385638574186496","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Uaesp/status/1255385638574186496

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

**Del mito de sísifo a la realidad en el sur de Bogotá**
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Javier Reyes, integrante de Asamblea Sur, manifiesta que la situación no es muy diferente a otras que han vivido en los barrios que colindan con el relleno sanitario, "es una tragedia que ya todos conocemos; si bien es doloroso, es más doloroso que se siga repitiendo lo mismo". (Le puede interesar: ["Distrito ampliaría hasta 2070 la existencia del relleno sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reyes explica que en el sur de la ciudad se sienten como viviendo el mito de Sísifo: llevando una piedra hasta el final de la colina para que al final, la piedra ruede de vuelta hacia abajo y el trabajo tenga que iniciar de nuevo. Esto, porque después del primer derrumbe que removió más de un millón de toneladas de basura que alcanzaron el río Tunjuelo, las [comunidades afectadas](https://twitter.com/PsuUsme/status/1255527235378917376) han reclamado soluciones de fondo al manejo de la basura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras ese derrumbe, se siguió acumulando basura en las laderas de Ciudad Bolívar y nuevamente en 2015 se presentó otro de más de 700 mil toneladas de basura. Reyes afirma que después de estas tragedias el único beneficiado es el concesionario que maneja el relleno: CGR-Doña Juana, que tras el último derrumbe "logra hacerle una modificación a la tarifa que casi triplica lo que se estaba pagando, con el argumento de que se iba a mejorar el manejo del relleno y hoy vemos que es una falacia".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La piedra que arrolla una y otra vez a las comunidades en el sur de Bogotá**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de 32 años de operación del relleno sanitario, el integrante de Asamblea Sur señala que la comunidad ya se ha adaptado a los malos olores, y naturalizó "la tragedia que vivimos: las enfermedades extrañas en la gente, la pérdida de la memoria en las personas, la locura y la histeria colectiva que hay en estos barrios...". (Le puede interesar: ["¿Cómo podría aprovecharse la basura del relleno sanitario Doña Juana?"](https://archivo.contagioradio.com/como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Añade que el abandono de la población es tan grande que ni siquiera se ha hecho un estudio psicosocial para evaluar los efectos en la salud mental de los habitantes cercanos al relleno, y la secretaría de salud tampoco tiene "estudios epidemiológicos serios". Todo ello, pese a que se ha hecho denuncia de lo que significa vivir cerca al lugar, y de la repetición de posibles derrumbes si se sigue acumulando la basura de la ciudad en la montaña.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Poner fín al relleno sanitario Doña Juana**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el corto, mediano y largo plazo se deben tomar medidas para atender las necesidades urgentes de la población en términos de salud y bienestar, pero también, para solucionar de fondo el problema de las basuras en Bogotá. En ese sentido, Reyes aseguró que debe hacerse una transición al pago de la deuda social con la zona para reparar los efectos de haber vivido 30 años (y tres derrumbes) con la basura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mediano plazo, sostiene que es necesario reconocer a las comunidades como interlocutores válidos, que tienen propuestas en beneficio de la ciudad y el ambiente. Y por último, manifiesta la importancia de dar el paso a la transformación en el tratamiento de la basura, dando cabida a procesos como el de la termólisis o la gasificación como opciones más limpias, y que evitarían que la piedra ruede una vez más montaña abajo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/heidy_up/status/1255533494845018112","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/heidy\_up/status/1255533494845018112

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
