Title: Rusia despide a director teatral por presiones de la Iglesia Ortodoxa
Date: 2015-03-31 19:32
Author: CtgAdm
Category: El mundo, Política
Tags: Despedido director teatral Moscó, Elena Bogush, Iglesia Ortodoxa rusa, Obra teatral rusa "Tanhaussen"
Slug: rusia-despide-a-director-teatral-por-presiones-de-la-iglesia-ortodoxa
Status: published

###### Foto:Larepublica.ec 

###### **Entrevista con[ Elena Bogush], socióloga de la Universidad de Moscú:** 

<iframe src="http://www.ivoox.com/player_ek_4290739_2_1.html?data=lZemkpyXfY6ZmKiak5iJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRltbnysaYxsrXuMro1t7SjcbQb8Xd08rQ1tTWb8XZzZDhx8bYttCfxtjhw9nFsIzk0NeY0tTQaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A pesar de que numerosas personalidades rusas defendieron al director teatral, ayer el **ministro de cultura Vladímir Medinski despidió a Boris Mezdrich director del Teatro Estatal de Ópera y Ballet de Novosibirsk**, capital de Siberia occidental, debido a **presiones de la Iglesia Ortodóxa** por la interpretación de la obra de Richard Wagner*,* *Tannhäuser.*

En la versión de la obra de Tannhäuser, **el autor cambia el argumento original,** y en lugar de un trovador es un director de cine que pone a concurso una película sobre una supuesta etapa de la vida de **Jesucristo donde era preso y convivía con la deidad**. Además en el flyer publicitario aparece una mujer con un Cristo entre las piernas.

Todo esto causo un **gran revuelo entre la comunidad ortodoxa de Siberia**, que pidieron la retirada de la obra y la destitución del director.

El 26 de Enero el arzobispo metropolitano denunció ante la fiscalía la obre por considerar que ofendía los sentimientos de los feligreses, pero un tribunal consideró que no atentaba contra la sensibilidad de los fieles.

En **2013** el gobierno añadió un ar**ticulo al código penal que permite juzgar a quienes ofendan los intereses o sensibilidades de la religión ortodoxa**, condenando con penas de hasta tres años dichos actos.

Después de una manifestación de 800 personas organizada por grupos extremistas en contra de la versión de Wagner, en un comunicado el ministro de cultura alegó la intervención por el clima tenso que se había creado en la región rusa.

Según **Elena Bogush, socióloga** e historiadora de la Universidad de Moscú, este es otro paso más en la **censura** que está aplicando el gobierno ruso en consonancia con las directrices de la Iglesia Ortodoxa, contra quién critica o caricaturiza todo lo referente a la doctrina cristiana. También añade que el estado ruso se está convirtiendo en una **democracia autoritaria** desde que Crimea fue anexionada y comenzaron las sanciones por parte de la UE.

En la entrevista recuerda que quién más está pagando las consecuencias de la censura es la **comunidad LGBTI, quién está sufriendo una persecución orquestada desde el propio estado.**
