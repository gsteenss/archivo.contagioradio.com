Title: Abramos la puerta, pero prendamos la luz
Date: 2016-09-27 12:36
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: acuerdo farc y gobierno, firma de acuerdo, proceso de paz
Slug: abramos-la-puerta-pero-prendamos-la-luz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Abramos-la-puerta-pero-prendamos-la-luz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: milenadisgrafi.] 

#### ***Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)*** 

[Muchísimas personas continuamos en el mejor estado de ánimo por la firma del acuerdo y el fin del conflicto entre el Estado Colombiano y la Farc-Ep. A pesar de los colombianos que están a favor del]*[no]*[, y a pesar de las falencias que pueda tener el acuerdo, sería ilógico y hasta odioso negar desde las letras o las palabras habladas, la posibilidad de reconocer en este hecho, un elemento que ha podido adornar de movilización política al país como hace rato no se veía. Para bien o para mal, la gente opina, habla claro, habla enredado, dice coherencias, dice incoherencias, pero en esencia desea hablar y eso es un gran avance, para un país cuyos temas no pasaban de la típica noticia macabra, o el desenlace de un]*[reality]*[ pasados por algún medio masivo.]

[Desde el más profundo respeto por la alegría y el carnaval en que se ha constituido gran parte de la campaña por el sí, con cautela, con sensatez, con algo incluso de preocupación, un sector seguimos clamando para que este tema tan serio no se adopte como si se tratara de un partido de fútbol o una publicación en redes sociales, como esas que se emiten y estamos acostumbrados a apreciar por la característica típica que tienen de no suscitar ningún tipo de huella o a lo sumo un “like”.]

[Es importante reconocer que los líderes de base siguen en riesgo inminente, que ni el modelo democrático liberal, ni el modelo económico liberal se negociaron y que ante la dicha de poder acabar una guerra, el acuerdo se presenta como esa puerta que la sociedad colombiana puede abrir, no sólo para dejarla abierta “a ver qué pasa” “igual eso no es conmigo” o en el peor de los casos, abrir la puerta e ingresar a esa habitación llamada]*[futuro político]*[ con la luz apagada y tropezarnos con la primera cosa que no tengamos presente, sino para resolver lo que seremos y cómo será nuestra sociedad.]

[El acuerdo en marcha, es una puerta abierta. Pero el problema tan tremendo que algunos hemos presenciado es el de una polarización poco fundamentada entre el sí y el no. Es una posición que muchos creen que se resuelve leyendo el documento final… ¿pero y la historia? ¿y la discusión reflexiva y no sólo informativa? La verdad, estos dos polos representan en apariencia dos cosmovisiones diferentes, pero cuando no se tiene presente la historia del país, la concepción del modelo económico y la lógica particular de nuestra democracia representativa, la luz queda apagada porque si no sabíamos de dónde veníamos, mucho menos sabremos para dónde iremos,]

[¡Eso sí! ¡la puerta está allí! ¡está abierta! Pero venimos caminando de la noche buscando refugio y algo de luz para leer lo que hemos sido, lo que somos y lo que podremos ser; si detrás de esa puerta todo sigue oscuro, cómo responderemos al momento histórico, cómo le diremos más adelante a los que hoy creen, que no creyeron en vano…]

[La luz encendida es movilizar todos los esfuerzos en lo que se constituirá el reto más grande de la sociedad colombiana: comprender por qué se genera tanta violencia, comprender cómo puede parar, comprender que para leer hay que tener la luz encendida o si no, solo evocaremos quimeras poéticas muy válidas sobre un deber ser, que no tendrá surco por donde correr; con la luz apagada, veremos cada vez más a la ideología liberal apoyando el sí por conveniencia individual y no por conclusión colectiva.  ]

[Abramos la puerta, pero prendamos la luz, para que no nos obliguen a leer, para que tampoco nos obliguen a odiar, para que nos enseñen a soñar, pero sobre todo reconociendo qué es un sueño y qué realmente no lo es. Abramos la puerta y prendamos la luz, para que del mismo modo como cuando una fiesta acaba, nos demos cuenta con quién bailamos, prendamos la luz para poder leer por dónde queda la puerta de salida, pues indudablemente el acuerdo es la puerta de entrada.]

[Abramos la puerta y prendamos la luz, para que la propaganda y el marketing político que han generado polarizaciones en estos últimos días (que inclusive han llegado al mal trato u odio incandescente) logren quedar al margen de una reflexión colectiva, social, histórica y con perspectiva real, para que la gente no se limite a dar el sí, porque “va a derrotar a alguien” para que no se limiten a decir “no” basándose en la idea de justicia vista sólo como “cárcel”.]

[Abramos la puerta y prendamos la luz, para que los militares y familiares de militares, no confundan los rostros de aquellos que hoy los empujan al odio, y vean claramente que aquellos políticos que hoy los están instrumentalizando, son los mismos que los han empujado a una incomprensible muerte, son los mismos que sólo los ven como unidades reemplazables, y nunca como seres humanos, parte de una historia, tan íntima, tan nuestra… Abramos la puerta, pero por favor encendamos la luz.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
