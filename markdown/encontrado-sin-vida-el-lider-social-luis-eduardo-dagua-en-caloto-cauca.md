Title: Hallado sin vida el líder social Luis Eduardo Dagua en Caloto, Cauca
Date: 2018-07-16 10:50
Category: Nacional
Tags: lideres sociales, Luis Eduardo dagua conda
Slug: encontrado-sin-vida-el-lider-social-luis-eduardo-dagua-en-caloto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Luis-Eduardo-dagua-conda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunidad 

16 Jul 2018

En la mañana de este lunes fue encontrado el cuerpo sin vida del líder comunal **Luis Eduardo Dagua Conda** en la vereda El Carmelo, del municipio de Caloto, Cauca. Dagua era reconocido por ser uno de los fundadores de esa vereda, integrante de **Pupsoc, Fensuagro y Marcha Patriótica y** padre de un ex combatiente de las FARC.

Según la información suministrada por la **Red de Derechos Humanos Francisco Isaias Cifuentes**, cuando se desplazaban a iniciar un trabajo comunitario los pobladores de la vereda encontraron al líder social en el suelo con golpes en su rostro, según el comunicado  "*el cuerpo sin vida tiene señales de tortura en cuello y rostro. Por los elementos encontrados en las inmediaciones el asesinato se presentó a causa de golpes propinados con piedras*". En el lugar se encuentra el ejercito pero no se ha efectuado el levantamiento del cuerpo.

Versiones de la comunidad aseguran que un grupo de militares acampó la noche anterior al crimen en la finca de Dagua y hacia presencia cerca al lugar donde fue encontrado el cuerpo sin vida.  Ver: [Euro diputados piden intervención inmediata para frenar asesinato de líderes sociales](https://archivo.contagioradio.com/?p=54751)

Este caso se suma al de **José Bayardo Montoya** quien fue asesinado en el Municipio de Miranda, Cauca el pasado 15 de julio, el crimen se cometió con el mismo modo, golpeando el cráneo de la víctima.

La organización Red de Derechos Humanos Francisco Isaias Cifuentes, a través de un comunicado exige al gobierno desarrollar  acciones legales necesarias para determinar las responsabilidades colectivas e individuales del homicidio y el inmediato cumplimiento de las recomendaciones sobre el respeto y acatamiento del Derecho Internacional de los Derechos Humanos.

###### Reciba toda la información de Contagio Radio en [[su correo]
