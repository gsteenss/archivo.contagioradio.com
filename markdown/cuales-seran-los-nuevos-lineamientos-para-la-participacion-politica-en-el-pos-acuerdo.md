Title: Ya hay  consensos entre organizaciones para una reforma electoral
Date: 2016-07-06 13:26
Category: Entrevistas, Política
Tags: Diálogos de paz Colombia, Misión de Observación Electoral Colombia, Reforma sistema de participación política Colombia
Slug: cuales-seran-los-nuevos-lineamientos-para-la-participacion-politica-en-el-pos-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Participación-política-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia ] 

###### [6 Julio 2016 ] 

De acuerdo con el [[Comunicado Conjunto No. 80 de la Mesa de Conversaciones](https://archivo.contagioradio.com/gobierno-y-farc-saldan-puntos-pendientes-en-acuerdo-de-participacion-politica/)], una vez se firme el acuerdo final, la Misión de Observación Electoral MOE, junto con 6 expertos más, contarán con seis meses para trabajar en la definición de los nuevos parámetros para el sistema de participación política en un eventual escenario de pos conflicto.

Según afirma Alejandra Barrios, directora de la MOE, las transformaciones tendrán que ver con cambios en el Consejo Nacional Electoral, la creación de un Tribunal Electoral, modificaciones profundas de la Registraduría, y el establecimiento de **mecanismos para la profundización de la democracia al interior de los partidos políticos y la eliminación de los vínculos entre política y corrupción**.

Esta tarea iniciará con el abordaje de los diagnósticos elaborados por la MOE junto con instituciones y partidos políticos, asegura Barrios, e insiste en que "por primera vez las propuestas a las reformas políticas y electorales vendrán de un sector externo a las organizaciones políticas", lo que es muy interesante, si se tiene en cuenta que las **discusiones llegaran al Congreso para acordar transformaciones sin estar calculando beneficios para determinado partido**.** **

Según la directora no es conveniente que se adopte la figura de voto obligatorio, pues son los **partidos políticos los que deben esforzarse para que la ciudadanía comprenda que son capaces de gobernar alejados de la corrupción** y en beneficio del bien común, eligiendo por vías democráticas candidatos honestos, con capacidad de interlocución con la gente y que comprendan que son funcionarios públicos que están al servicio del ciudadano que debe tener el derecho de saber de dónde provienen los dineros con los que se financian tanto los partidos como los políticos.

<iframe src="http://co.ivoox.com/es/player_ej_12140035_2_1.html?data=kpeelpWUd5ahhpywj5aYaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs_O0MnWpYy2wtffy9TXaZO3jLK8p5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
