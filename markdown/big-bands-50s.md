Title: El jazz y su influencia en la salsa
Date: 2015-10-14 12:18
Category: En clave de son
Tags: Big bands, Historia de la Salsa, Influencia del Jazz en la Salsa, Machito, Mario Bauzá, Salsa en los años 50
Slug: big-bands-50s
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/24x28-blk-leroy-campbell-big-band-AV106-e1444842727833.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [14 Oct, 2015]

La salsa se ha nutrido con el sonido de otros géneros musicales, que con el pasar de los años se han incorporado a su estructura y hoy pueden considerarse fundamentales. Durante un periodo importante de su historia, el jazz, al igual que otras manifestaciones culturales provenientes de Norteamérica, ejerció una profunda influencia en la música caribeña.

En la década del 50, surgirían las célebres "Big Bands", agrupaciones que asemejaban a las orquestas de Jazz, en el número de integrantes y en el uso de algunos instrumentos y sonidos tradicionales de la música Afroamericana, pero consevando de los sextetos y septetos la percusión y la clave.

El escenario ideal para la creación de las "Big Bands" resulto ser un club nocturno que no pasaba entonces por sus mejores días, "El Palladium", propiedad de un productor de apellido Moore, quien encontró en la música latina el mejor camino para dar nueva vida a su establecimiento.

La gran afluencia de latinos en Nueva York y el llamado "Boom" de la música latina favoreció a la consolidación de músicos como Mario Bauzá o Machito en estas nuevas agrupaciones, con las que llenaron hoteles, casinos y clubes, grabaron discos y realizaron giras por ciudades y países.

Si quiere conocer más de este periodo, fundamental para comprender lo que es la salsa que disfrutamos hoy, lo  invitamos a escuchar el presente Podcast de En Clave de Son.

<iframe src="http://www.ivoox.com/player_ek_8981374_2_1.html?data=mZ6lk5ibeI6ZmKiakpeJd6Kkkoqgo5qZcYarpJKfj4qbh46kjoqkpZKUcYarpJKvy8yPhsLixZCem5qUaZO3jMrbjcjQpdfZjMnSjdjTso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
