Title: Crece preocupación en Urabá por garantías de seguridad para restitución de tierras
Date: 2019-04-29 18:27
Author: CtgAdm
Category: Comunidad, Judicial
Tags: Fundación Forjando Futuros, Policía Nacional, Restitución de tierras, uraba
Slug: crece-preocupacion-en-uraba-por-garantias-de-seguridad-en-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Restitucion-de-Tierras-Urabá.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-29-at-11.56.40-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Prensa IPC] 

La Policía Nacional estaría cesando el acompañamiento de comisiones de restitución de tierras en la subregión de Urabá, tras el ataque que sostuvo una unidad de la Policía durante actividades de restitución que dejaron a seis heridos el pasado 11 de abril. Así lo afirman seis organizaciones de derechos humanos, quienes representan a **21 familias campesinas** que esperaban regresar a la vereda Guacamayas.

Según Gerardo Vega, director de la Fundación Forjando Futuros, la entrega de 1.000 hectáreas de terreno está programada para el 14, 15 y 16 de mayo, en cumplimiento del fallo de la Corte Constitucional de 2017 que favorece a estas familias vícrtimas de  desplazados por las Autodefensas Unidas de Colombia entre 1996 y 1997. Sin embargo, la Policía Nacional mandó un oficio la semana pasada, que dificultaría esta proceso.

En la carta, los agentes policiales anunciaron a los magistrados y jueces de la Comisión de Restitución de Tierras en Urabá que levantarían las medidas de seguridad que brindaban a esta organización. Dichas medidas serían evaluadas en una reunión **el próximo 3 de mayo** por el Ministerio del Interior, la Unidad de Restitución de Tierras y la Policía Nacional, para determinar si continuarían.

Estos cambios se dan después de que una unidad fuese atacada en una ofensiva sorpresa el pasado 11 de abril, por integrantes de grupos armados ilegales de la zona, mientras que la Policía prestaban protección a reclamantes de tierras quienes realizaban un estudio de un predio.

"¿Si la policía no puede cumplir, no puede acompañar los jueces de tierras para que se cumplan las sentencias, la pregunta es **quién está a cargo de la seguridad ciudadana en la región de Urabá**?" preguntó Vega, durante una rueda de prensa. (Le puede interesar: "[Lucha entre cifras y hechos, los resultados de la restitución de tierras](https://archivo.contagioradio.com/asi-trabaja-unidad-restitucion-tierras/)")

Por su parte, el departamento de Policía del Urabá desmintió la denuncia de las seis organizaciones y aseguró que ha mantenido y mantendrá el acompañamiento de restitución de tierras en los municipios de esta región, ubicada entre los departamentos de Antioquia y Chocó.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-29-at-11.56.40-AM-225x300.jpeg){.size-medium .wp-image-66056 .alignleft width="225" height="300"}

Vega resaltó que en la zona han operado grupos paramilitares al servicio de terratenientes que buscan obstaculizar la restitución de tierras. Asimismo señaló que solo 163 de 6.824 solicitudes para reclamar tierras han sido resueltas desde 2011, es decir, solo el 4%. Además, **20 reclamantes de tierras han sido asesinados** desde 2008 y solo en dos de estos casos, la Fiscalía ha detenido presuntos responsables mientras que los otros han quedado en la impunidad.

Por tal razón, Nely Osorno, presidenta del Instituto Popular de Capacitación, indicó que la deficiente seguridad en la región se ha utilizado como una excusa para que no avance la entrega de estos terrenos. "Estos procesos se quedan paralizados hasta que se venza la ley", recordando que la Unidad de Restitución de Tierras tiene plazo hasta 2021 para entregar estas dos fincas a dichas familias campesinas, según lo establece la ley.

<iframe id="audio_35104197" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35104197_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [English Version](https://archivo.contagioradio.com/growing-concern-in-uraba-for-guarantees-policies-about-land-restitution/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
