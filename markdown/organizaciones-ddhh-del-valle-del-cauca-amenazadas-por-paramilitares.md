Title: Paramilitares amenazan a organizaciones de derechos humanos del Valle del Cauca
Date: 2016-10-14 17:35
Category: DDHH, Nacional
Tags: Agresiones contra defensoras, Autodefensas Gaitanistas de Colombia
Slug: organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paramilitares_gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [14 de Oct 2016] 

Organizaciones defensoras de derechos humanos fueron amenazadas por grupos que se autodenominan Autodefensas Gaitanistas de Colombia. En el texto señalan a **los y las defensoras de derechos humanos de ser “milicianos” de las FARC y el ELN, motivo por el cual van a ser “exterminados”.** Lea también:["Líderes sociales blanco de ataques de grupos paramilitares en Cartagena"](https://archivo.contagioradio.com/lideres-sociales-blanco-de-ataques-de-grupos-paramilitares-en-cartagena/)

Una de las personas amenazadas es Walter Agredo, miembro del comité de Solidaridad con los Presos Políticos, quien afirma que estas amenazas se han incrementado debido a la firma de los acuerdos de la Habana, el trabajo social realizado en los territorios en torno a la paz y a la movilización social de estos días que genera en la **“extrema derecha y los grupos paramilitares incomodidad”.**

Agredo indicó que en abril de este año ya habían llegado amenazas a de defensores de derechos humanos por parte de grupos identificados como AGC y se había puesto en conocimiento a la política de estos hechos, sin embargo Agredo también comenta que se ha comprobado como miembros de la **Fuerza Pública y la SIJIN han perseguido a personas que pertenecen al movimiento social, hasta el momento las autoridades no se han pronunciado frente a estos a**[**acontecimientos. **]

Entre julio y agosto se registraron en Valle del Cauca **63 hechos de riesgo para defensores de derechos humanos**, **38 de ellas fueron amenazas de muerte**, de igual forma en lo que va corrido del año han asesinado a 6 defensores en esta región del país.

<iframe id="audio_13324696" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13324696_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
