Title: Antioquia y Chocó registran el mayor número de reclutamiento a menores
Date: 2020-02-12 19:01
Author: AdminContagio
Category: DDHH, Nacional
Slug: antioquia-y-choco-registran-el-mayor-numero-de-reclutamiento-a-menores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQl0teYWsAQn-d-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: [Contagio Radio](https://twitter.com/juancbocanegra)*

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/jose-luis-campo-representante-legal-benposta-nacion_md_47745118_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Jose Luis Campo | representante legal de Benposta Nación De Muchachos

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El 12 de febrero se conmemora el Día **Internacional de las Manos Rojas**, una fecha en la que se pide el cese al reclutamiento de niños, niñas y adolescentes para la guerra. (Le puede interesar: <https://archivo.contagioradio.com/en-colombia-los-ninos-crecen-hablando-de-muerte-oxfam/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La iniciativa nace en 1997 en Alemania como protesta ante el reclutamiento de niños en el conflicto armado de Ruanda y que posteriormente se instauró como fecha oficial dentro del calendario de los derechos fundamentales de los menores de edad en todo el mundo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La situación de los niños y niñas en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el **Registro Único de Víctimas** en el periodo de 1985 y 2019 se han reportado 8.075 víctimas mortales de reclutamiento y 8.648 eventos que involucraron a menores de edad. De igual forma el ** Instituto Colombiano de Bienestar Familiar** informó que entre 1999 y 2019 fueron atendidos 6.715 niños, niñas y adolescentes víctimas de estos eventos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la Defensoría del Pueblo señaló que en 2019 realizaron 108 alertas tempranas, advirtiendo sobre el el riesgo de reclutamiento de menores en 182 municipios de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Coalico en el último año, el departamento con mayor registro de reclutamiento a menores es Antioquia con 40 casos, seguido por Chocó con 34, Norte de Santander con 26, Valle del Cauca con 22 y Cauca con 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma señalan que estos actos son atribuidos a grupos armados ilegales como disidencias de FARC, Ejército de Liberación Nacional (ELN), Clan del Golfo, Águilas Negras, entre otros. Asimismo destacan que las poblaciones más afectadas son indígenas, comunidades negras y migrantes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Esto pasa en el mundo con los niños y niñas

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización de derechos humanos Child Soldiers International ha reportado que países como Corea del Norte, Mauritania, Zambia, Emiratos Árabes e Irán, reclutan en sus filas del ejercito a menores desde los 15 años de edad. Y que Reino Unido, Francia, Países Bajos, Alemania, Austria, EEUU y China, integran en sus fuerzas armadas desde los 17 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado según la Organización el único lugar de Sudamérica donde los niños corren riesgos de ser reclutados por grupos insurgentes es Colombia, mientras que en África, Malí, Libia, Sudán, Nigeria, Camerún, Congo, República Centroafricana y Somalia, registran reclutamiento forzado a menores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Asia los casos se presentan en países como Siria, Irak, Líbano, Yemen, Afganistán, Pakistán, India, Birmania y Filipinas. (Le puede interesar: <https://archivo.contagioradio.com/en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos/>).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Con la firma del acuerdo de paz pensamos que iba a mitigar este reclutamiento"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según José Luis Campo representante legal de Benposta Nación De Muchachos, *"cuando se firmó la paz pensamos que todo este riesgo se iba a mitigar, pero vimos que lo que pasó fue lo contrario"*, y manifestó que con la salida de los grupos armados se debió ver la presencia del Estado Social de Derecho y no de la fuerza armada.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Lógicamente llegaron otros grupos como ELN y EPL a pelearse por esos territorios, y los niños fueron los más afectados en ese plan de los grupos por fortalecerse y conseguir gente para sus tropas"*
>
> <cite>José Luis Campos</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y agregó que los niños ingresan a estos grupos ilegales no forzados por un persona, sino por multiples factores como el hambre, la pobreza, la violencia y el olvido estatal, *"hay situaciones estructurales en los territorios donde la deuda social del estado es gigante y aún no se han abordado".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Educar para la paz y no para la guerra

<!-- /wp:heading -->

<!-- wp:image {"id":20311,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Manos-Rojas1.jpg){.wp-image-20311}  

<figcaption>
Foto: Archivo de Contagio Radio

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El ultimo informe de la Organización de las Naciones Unidad, mencionó las campañas que promovían en algunas regiones los valores militares, *"no hemos tomado conciencia de lo que significa la participación en la lógica de la guerra, no se ha visibilizado lo que el impacto de esto en la vida de los niños y niñas en los territorios"*, señaló Campos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y afirmó que para Benposta*, "los niños y las niñas se van a la guerra porque no tienen otra cosa que hacer, mientras no se generen propuestas a nivel, educativo, cultural y participación es complicado poder mitigar este problema"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su lado el defensor del Pueblo, Carlos Negret realizó un llamado a los grupos armados ilegales para que cese el reclutamiento y la utilización de menores, alertando que pese a la firma del Acuerdo de Paz, los actores armados buscan controlar nuevas zonas por lo que vinculan a niños, niñas y adolescentes para que realicen labores de vigilancia, cobro de extorsiones y domicilios.

<!-- /wp:paragraph -->
