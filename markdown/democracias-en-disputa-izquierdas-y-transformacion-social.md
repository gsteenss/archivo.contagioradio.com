Title: “Democracias en disputa, izquierdas y transformación social”
Date: 2015-09-02 13:00
Category: Nacional, Política
Tags: democracia, extractivismo, Foro debate democracias en disputa, Fundación Rosa Luxemburgo, Modelo económico, Universidad Nacional
Slug: democracias-en-disputa-izquierdas-y-transformacion-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/democracia-en-disputa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat,org 

<iframe src="http://www.ivoox.com/player_ek_7762110_2_1.html?data=mJyjlJaVdI6ZmKiak5qJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56niMbh0Mjfw8jNpdSfxtOYxs7XtNbowoqfpZDNvtLpysrfxsbXb9qf1dfO0NjKs9PhwsjWh6iXaaOnz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Myriam Lang, Fundación Rosa Luxemburg] 

###### [Sep 02 2015] 

Entre hoy y mañana en el auditorio Camilo Torres de la facultad de Derecho de la Universidad Nacional de Bogotá, se realizará el foro que gira en torno a las estrategias de transformación que tienen las personas y organizaciones frente a la crisis que vive el capitalismo actualmente.

Myriam Lang, representante de la fundación Rosa Luxemburgo, afirma que el evento contará con las experiencias de invitados internacionales entre los que se encuentran Mónica Baltodano, ex dirigente del Frente de Liberación Nacional; Esperanza Martínez (Ecuador) Bióloga y miembro de Acción Ecológica de Ecuador; Sergio Tischler (Guatemala/México), investigador de la Benemérita Universidad Autónoma de Puebla, México, entre otros.

Los panelistas basándose en modelos de gobiernos calificados como progresistas, darán pistas de las maneras para incidir desde lo local a lo global, es decir, desde los movimientos sociales y organizaciones y comunidades hacia realidades nacionales o regionales.

Se tratarán temas como la diferencia entre gobiernos progresistas y conservadores, el modelo económico, alternativas a la economía capitalista, modelos de integración a la izquierda,  derechos de la naturaleza y la autonomía en las comunidades.

Encuentre aquí toda la información sobre el evento

[Agenda Foro Debate Democracias en Disputa](https://es.scribd.com/doc/277818205/Agenda-Foro-Debate-Democracias-en-Disputa "View Agenda Foro Debate Democracias en Disputa on Scribd")

<iframe id="doc_250" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/277818205/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
